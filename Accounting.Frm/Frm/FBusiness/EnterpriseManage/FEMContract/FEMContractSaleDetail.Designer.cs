﻿namespace Accounting
{
    partial class FEMContractSaleDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance116 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance123 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance127 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance129 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance130 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance131 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance132 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance133 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance134 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance135 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance136 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance137 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance138 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance139 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance140 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance141 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance142 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance143 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance144 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance145 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance146 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance147 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance148 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance149 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance150 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance151 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance152 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance153 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance154 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance155 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance156 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance157 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance158 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance159 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance160 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance161 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance162 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance163 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance164 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance165 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance166 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance167 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance168 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance169 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance170 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance171 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance172 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance173 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance174 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance175 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance176 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance177 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance178 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance179 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance180 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance181 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance182 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance183 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance184 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance185 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance186 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance187 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance188 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance189 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance190 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance191 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance192 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance193 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance194 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance195 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance196 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance197 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance198 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance199 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance200 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance201 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance202 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance203 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance204 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance205 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance206 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance207 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance208 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance209 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance210 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance211 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance212 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance213 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance214 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance215 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance216 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance217 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance218 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance219 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance220 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance221 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance222 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance223 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance224 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance225 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance226 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance227 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance228 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance229 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance230 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance231 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance232 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance233 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance234 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance235 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance236 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance237 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.chkIsWatchForCostPrice = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraDateTimeEditor4 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtClosedReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtClosedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel33 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.txtSignerTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSignerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbContractState = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtCompanyTel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtCompanyName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel29 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel30 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.chkIsBillPaid = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbAccountingObjectBankAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectFax = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccountingObjectTel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectSignerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtDueDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDA = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblDA = new Infragistics.Win.Misc.UltraLabel();
            this.cbbOrderID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAmountOriginal = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtAmountBeforCancel = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtExchangeRate = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDeliverDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtSignedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.cbbContractEmployeeID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbDepartmentID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbCurrencyID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtEMContractCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.lblGTHDQD = new Infragistics.Win.Misc.UltraLabel();
            this.lblGTHD = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox6 = new Infragistics.Win.Misc.UltraGroupBox();
            this.lblToDate1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel35 = new Infragistics.Win.Misc.UltraLabel();
            this.lblFromDate1 = new Infragistics.Win.Misc.UltraLabel();
            this.uGridChungTuChi = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.btnChungTuChi = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel32 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridChungTuThu = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.lblToDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblFromDate = new Infragistics.Win.Misc.UltraLabel();
            this.btnChungTuThu = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel31 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridChi = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridEMContractMG = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.btnEdit = new Infragistics.Win.Misc.UltraButton();
            this.uGridEMContractAttachment = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.optContract = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.utEMContractSale = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.btnSua = new Infragistics.Win.Misc.UltraButton();
            this.btnGhidoanhso = new Infragistics.Win.Misc.UltraButton();
            this.btnBoghidoanhso = new Infragistics.Win.Misc.UltraButton();
            this.btnXoa = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsWatchForCostPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtClosedReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClosedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignerTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbContractState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsBillPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectSignerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbOrderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliverDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbContractEmployeeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDepartmentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrencyID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMContractCode)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).BeginInit();
            this.ultraGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridChungTuChi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.ultraGroupBox5.SuspendLayout();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridChungTuThu)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridChi)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEMContractMG)).BeginInit();
            this.ultraTabPageControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEMContractAttachment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optContract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.utEMContractSale)).BeginInit();
            this.utEMContractSale.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.chkIsWatchForCostPrice);
            this.ultraTabPageControl1.Controls.Add(this.ultraDateTimeEditor4);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox3);
            this.ultraTabPageControl1.Controls.Add(this.chkIsBillPaid);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel26);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox2);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(855, 558);
            // 
            // chkIsWatchForCostPrice
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextHAlignAsString = "Left";
            appearance1.TextVAlignAsString = "Middle";
            this.chkIsWatchForCostPrice.Appearance = appearance1;
            this.chkIsWatchForCostPrice.BackColor = System.Drawing.Color.Transparent;
            this.chkIsWatchForCostPrice.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsWatchForCostPrice.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsWatchForCostPrice.Location = new System.Drawing.Point(15, 524);
            this.chkIsWatchForCostPrice.Name = "chkIsWatchForCostPrice";
            this.chkIsWatchForCostPrice.Size = new System.Drawing.Size(126, 20);
            this.chkIsWatchForCostPrice.TabIndex = 3021;
            this.chkIsWatchForCostPrice.Text = "Theo dõi giá thành";
            this.chkIsWatchForCostPrice.CheckedChanged += new System.EventHandler(this.chkIsWatchForCostPrice_CheckedChanged);
            // 
            // ultraDateTimeEditor4
            // 
            appearance2.TextVAlignAsString = "Middle";
            this.ultraDateTimeEditor4.Appearance = appearance2;
            this.ultraDateTimeEditor4.AutoSize = false;
            this.ultraDateTimeEditor4.Location = new System.Drawing.Point(722, 523);
            this.ultraDateTimeEditor4.Name = "ultraDateTimeEditor4";
            this.ultraDateTimeEditor4.Size = new System.Drawing.Size(109, 22);
            this.ultraDateTimeEditor4.TabIndex = 5;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.txtClosedReason);
            this.ultraGroupBox3.Controls.Add(this.txtClosedDate);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel33);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel25);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel24);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel23);
            this.ultraGroupBox3.Controls.Add(this.txtSignerTitle);
            this.ultraGroupBox3.Controls.Add(this.txtSignerName);
            this.ultraGroupBox3.Controls.Add(this.txtReason);
            this.ultraGroupBox3.Controls.Add(this.cbbContractState);
            this.ultraGroupBox3.Controls.Add(this.txtCompanyTel);
            this.ultraGroupBox3.Controls.Add(this.txtCompanyName);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel29);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel28);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel30);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel27);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 369);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(855, 138);
            this.ultraGroupBox3.TabIndex = 2;
            this.ultraGroupBox3.Text = "Thông tin khác";
            // 
            // txtClosedReason
            // 
            appearance3.TextVAlignAsString = "Middle";
            this.txtClosedReason.Appearance = appearance3;
            this.txtClosedReason.AutoSize = false;
            this.txtClosedReason.Location = new System.Drawing.Point(500, 47);
            this.txtClosedReason.Name = "txtClosedReason";
            this.txtClosedReason.Size = new System.Drawing.Size(332, 21);
            this.txtClosedReason.TabIndex = 3;
            // 
            // txtClosedDate
            // 
            appearance4.TextVAlignAsString = "Middle";
            this.txtClosedDate.Appearance = appearance4;
            this.txtClosedDate.AutoSize = false;
            this.txtClosedDate.Location = new System.Drawing.Point(708, 19);
            this.txtClosedDate.Name = "txtClosedDate";
            this.txtClosedDate.Size = new System.Drawing.Size(124, 22);
            this.txtClosedDate.TabIndex = 5;
            // 
            // ultraLabel33
            // 
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel33.Appearance = appearance5;
            this.ultraLabel33.Location = new System.Drawing.Point(9, 103);
            this.ultraLabel33.Name = "ultraLabel33";
            this.ultraLabel33.Size = new System.Drawing.Size(68, 22);
            this.ultraLabel33.TabIndex = 0;
            this.ultraLabel33.Text = "Chức vụ";
            // 
            // ultraLabel25
            // 
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel25.Appearance = appearance6;
            this.ultraLabel25.Location = new System.Drawing.Point(9, 75);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(68, 22);
            this.ultraLabel25.TabIndex = 0;
            this.ultraLabel25.Text = "Người ký";
            // 
            // ultraLabel24
            // 
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel24.Appearance = appearance7;
            this.ultraLabel24.Location = new System.Drawing.Point(9, 47);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(71, 22);
            this.ultraLabel24.TabIndex = 0;
            this.ultraLabel24.Text = "Điện thoại";
            // 
            // ultraLabel23
            // 
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel23.Appearance = appearance8;
            this.ultraLabel23.Location = new System.Drawing.Point(9, 19);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(71, 22);
            this.ultraLabel23.TabIndex = 0;
            this.ultraLabel23.Text = "Người liên hệ";
            // 
            // txtSignerTitle
            // 
            appearance9.TextVAlignAsString = "Middle";
            this.txtSignerTitle.Appearance = appearance9;
            this.txtSignerTitle.AutoSize = false;
            this.txtSignerTitle.Location = new System.Drawing.Point(104, 102);
            this.txtSignerTitle.Name = "txtSignerTitle";
            this.txtSignerTitle.Size = new System.Drawing.Size(292, 22);
            this.txtSignerTitle.TabIndex = 1;
            // 
            // txtSignerName
            // 
            appearance10.TextVAlignAsString = "Middle";
            this.txtSignerName.Appearance = appearance10;
            this.txtSignerName.AutoSize = false;
            this.txtSignerName.Location = new System.Drawing.Point(104, 75);
            this.txtSignerName.Name = "txtSignerName";
            this.txtSignerName.Size = new System.Drawing.Size(292, 22);
            this.txtSignerName.TabIndex = 1;
            // 
            // txtReason
            // 
            this.txtReason.AutoSize = false;
            this.txtReason.Location = new System.Drawing.Point(500, 75);
            this.txtReason.Multiline = true;
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(332, 49);
            this.txtReason.TabIndex = 1;
            // 
            // cbbContractState
            // 
            appearance11.TextVAlignAsString = "Middle";
            this.cbbContractState.Appearance = appearance11;
            this.cbbContractState.AutoSize = false;
            appearance12.Image = global::Accounting.Properties.Resources.plus;
            editorButton1.Appearance = appearance12;
            appearance13.Image = global::Accounting.Properties.Resources.plus_press;
            editorButton1.PressedAppearance = appearance13;
            this.cbbContractState.ButtonsRight.Add(editorButton1);
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbContractState.DisplayLayout.Appearance = appearance14;
            this.cbbContractState.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbContractState.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbContractState.DisplayLayout.GroupByBox.Appearance = appearance15;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbContractState.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.cbbContractState.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance17.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance17.BackColor2 = System.Drawing.SystemColors.Control;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbContractState.DisplayLayout.GroupByBox.PromptAppearance = appearance17;
            this.cbbContractState.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbContractState.DisplayLayout.MaxRowScrollRegions = 1;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbContractState.DisplayLayout.Override.ActiveCellAppearance = appearance18;
            appearance19.BackColor = System.Drawing.SystemColors.Highlight;
            appearance19.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbContractState.DisplayLayout.Override.ActiveRowAppearance = appearance19;
            this.cbbContractState.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbContractState.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            this.cbbContractState.DisplayLayout.Override.CardAreaAppearance = appearance20;
            appearance21.BorderColor = System.Drawing.Color.Silver;
            appearance21.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbContractState.DisplayLayout.Override.CellAppearance = appearance21;
            this.cbbContractState.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbContractState.DisplayLayout.Override.CellPadding = 0;
            appearance22.BackColor = System.Drawing.SystemColors.Control;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbContractState.DisplayLayout.Override.GroupByRowAppearance = appearance22;
            appearance23.TextHAlignAsString = "Left";
            this.cbbContractState.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.cbbContractState.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbContractState.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            this.cbbContractState.DisplayLayout.Override.RowAppearance = appearance24;
            this.cbbContractState.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbContractState.DisplayLayout.Override.TemplateAddRowAppearance = appearance25;
            this.cbbContractState.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbContractState.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbContractState.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbContractState.Location = new System.Drawing.Point(500, 19);
            this.cbbContractState.Name = "cbbContractState";
            this.cbbContractState.Size = new System.Drawing.Size(161, 22);
            this.cbbContractState.TabIndex = 2;
            this.cbbContractState.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbContractState_RowSelected);
            this.cbbContractState.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbContractState_ItemNotInList);
            this.cbbContractState.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbContractState_EditorButtonClick);
            // 
            // txtCompanyTel
            // 
            appearance26.TextVAlignAsString = "Middle";
            this.txtCompanyTel.Appearance = appearance26;
            this.txtCompanyTel.AutoSize = false;
            this.txtCompanyTel.Location = new System.Drawing.Point(104, 47);
            this.txtCompanyTel.Name = "txtCompanyTel";
            this.txtCompanyTel.Size = new System.Drawing.Size(292, 22);
            this.txtCompanyTel.TabIndex = 1;
            // 
            // txtCompanyName
            // 
            appearance27.TextVAlignAsString = "Middle";
            this.txtCompanyName.Appearance = appearance27;
            this.txtCompanyName.AutoSize = false;
            this.txtCompanyName.Location = new System.Drawing.Point(104, 19);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(292, 22);
            this.txtCompanyName.TabIndex = 1;
            // 
            // ultraLabel29
            // 
            appearance28.TextVAlignAsString = "Middle";
            this.ultraLabel29.Appearance = appearance28;
            this.ultraLabel29.Location = new System.Drawing.Point(402, 75);
            this.ultraLabel29.Name = "ultraLabel29";
            this.ultraLabel29.Size = new System.Drawing.Size(72, 22);
            this.ultraLabel29.TabIndex = 0;
            this.ultraLabel29.Text = "Diễn giải";
            // 
            // ultraLabel28
            // 
            appearance29.TextVAlignAsString = "Middle";
            this.ultraLabel28.Appearance = appearance29;
            this.ultraLabel28.Location = new System.Drawing.Point(402, 47);
            this.ultraLabel28.Name = "ultraLabel28";
            this.ultraLabel28.Size = new System.Drawing.Size(45, 22);
            this.ultraLabel28.TabIndex = 0;
            this.ultraLabel28.Text = "Lý do";
            // 
            // ultraLabel30
            // 
            appearance30.TextVAlignAsString = "Middle";
            this.ultraLabel30.Appearance = appearance30;
            this.ultraLabel30.Location = new System.Drawing.Point(667, 19);
            this.ultraLabel30.Name = "ultraLabel30";
            this.ultraLabel30.Size = new System.Drawing.Size(35, 22);
            this.ultraLabel30.TabIndex = 0;
            this.ultraLabel30.Text = "Ngày";
            // 
            // ultraLabel27
            // 
            appearance31.TextVAlignAsString = "Middle";
            this.ultraLabel27.Appearance = appearance31;
            this.ultraLabel27.Location = new System.Drawing.Point(402, 19);
            this.ultraLabel27.Name = "ultraLabel27";
            this.ultraLabel27.Size = new System.Drawing.Size(89, 22);
            this.ultraLabel27.TabIndex = 0;
            this.ultraLabel27.Text = "Tình trạng HĐ (*)";
            // 
            // chkIsBillPaid
            // 
            appearance32.BackColor = System.Drawing.Color.Transparent;
            appearance32.TextHAlignAsString = "Left";
            appearance32.TextVAlignAsString = "Middle";
            this.chkIsBillPaid.Appearance = appearance32;
            this.chkIsBillPaid.BackColor = System.Drawing.Color.Transparent;
            this.chkIsBillPaid.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsBillPaid.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsBillPaid.Location = new System.Drawing.Point(489, 524);
            this.chkIsBillPaid.Name = "chkIsBillPaid";
            this.chkIsBillPaid.Size = new System.Drawing.Size(112, 20);
            this.chkIsBillPaid.TabIndex = 3021;
            this.chkIsBillPaid.Text = "Đã xuất hóa đơn";
            this.chkIsBillPaid.CheckedChanged += new System.EventHandler(this.chkIsBillPaid_CheckedChanged);
            // 
            // ultraLabel26
            // 
            appearance33.TextVAlignAsString = "Middle";
            this.ultraLabel26.Appearance = appearance33;
            this.ultraLabel26.Location = new System.Drawing.Point(607, 523);
            this.ultraLabel26.Name = "ultraLabel26";
            this.ultraLabel26.Size = new System.Drawing.Size(109, 22);
            this.ultraLabel26.TabIndex = 0;
            this.ultraLabel26.Text = "Ngày xuất hóa đơn";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.cbbAccountingObjectBankAccount);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel17);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel18);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel16);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel15);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox2.Controls.Add(this.txtAccountingObjectFax);
            this.ultraGroupBox2.Controls.Add(this.cbbAccountingObjectID);
            this.ultraGroupBox2.Controls.Add(this.txtAccountingObjectTel);
            this.ultraGroupBox2.Controls.Add(this.txtAccountingObjectName);
            this.ultraGroupBox2.Controls.Add(this.txtAccountingObjectAddress);
            this.ultraGroupBox2.Controls.Add(this.txtAccountingObjectBankName);
            this.ultraGroupBox2.Controls.Add(this.txtAccountingObjectSignerName);
            this.ultraGroupBox2.Controls.Add(this.txtAccountingObjectTitle);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel22);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel21);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel20);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel19);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 222);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(855, 147);
            this.ultraGroupBox2.TabIndex = 1;
            this.ultraGroupBox2.Text = "Bên mua";
            // 
            // cbbAccountingObjectBankAccount
            // 
            appearance34.TextVAlignAsString = "Middle";
            this.cbbAccountingObjectBankAccount.Appearance = appearance34;
            this.cbbAccountingObjectBankAccount.AutoSize = false;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Appearance = appearance35;
            this.cbbAccountingObjectBankAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountingObjectBankAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance36.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance36.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance36.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectBankAccount.DisplayLayout.GroupByBox.Appearance = appearance36;
            appearance37.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance37;
            this.cbbAccountingObjectBankAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance38.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance38.BackColor2 = System.Drawing.SystemColors.Control;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance38.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance38;
            this.cbbAccountingObjectBankAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountingObjectBankAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance39.BackColor = System.Drawing.SystemColors.Window;
            appearance39.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.ActiveCellAppearance = appearance39;
            appearance40.BackColor = System.Drawing.SystemColors.Highlight;
            appearance40.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.ActiveRowAppearance = appearance40;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.CardAreaAppearance = appearance41;
            appearance42.BorderColor = System.Drawing.Color.Silver;
            appearance42.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.CellAppearance = appearance42;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.CellPadding = 0;
            appearance43.BackColor = System.Drawing.SystemColors.Control;
            appearance43.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance43.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance43.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.GroupByRowAppearance = appearance43;
            appearance44.TextHAlignAsString = "Left";
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.HeaderAppearance = appearance44;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            appearance45.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.RowAppearance = appearance45;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance46.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance46;
            this.cbbAccountingObjectBankAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectBankAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountingObjectBankAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountingObjectBankAccount.Location = new System.Drawing.Point(500, 75);
            this.cbbAccountingObjectBankAccount.Name = "cbbAccountingObjectBankAccount";
            this.cbbAccountingObjectBankAccount.Size = new System.Drawing.Size(332, 22);
            this.cbbAccountingObjectBankAccount.TabIndex = 4;
            this.cbbAccountingObjectBankAccount.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccountingObjectBankAccount_RowSelected);
            // 
            // ultraLabel17
            // 
            appearance47.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance47;
            this.ultraLabel17.Location = new System.Drawing.Point(9, 103);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(48, 22);
            this.ultraLabel17.TabIndex = 0;
            this.ultraLabel17.Text = "Địa chỉ";
            // 
            // ultraLabel18
            // 
            appearance48.TextVAlignAsString = "Middle";
            this.ultraLabel18.Appearance = appearance48;
            this.ultraLabel18.Location = new System.Drawing.Point(253, 75);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(28, 22);
            this.ultraLabel18.TabIndex = 0;
            this.ultraLabel18.Text = "Fax";
            // 
            // ultraLabel16
            // 
            appearance49.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance49;
            this.ultraLabel16.Location = new System.Drawing.Point(9, 75);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(71, 22);
            this.ultraLabel16.TabIndex = 0;
            this.ultraLabel16.Text = "Điện thoại";
            // 
            // ultraLabel15
            // 
            appearance50.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance50;
            this.ultraLabel15.Location = new System.Drawing.Point(9, 47);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(48, 22);
            this.ultraLabel15.TabIndex = 0;
            this.ultraLabel15.Text = "Tên KH";
            // 
            // ultraLabel8
            // 
            appearance51.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance51;
            this.ultraLabel8.Location = new System.Drawing.Point(9, 19);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(71, 22);
            this.ultraLabel8.TabIndex = 0;
            this.ultraLabel8.Text = "Mã KH(*)";
            // 
            // txtAccountingObjectFax
            // 
            appearance52.TextVAlignAsString = "Middle";
            this.txtAccountingObjectFax.Appearance = appearance52;
            this.txtAccountingObjectFax.AutoSize = false;
            this.txtAccountingObjectFax.Location = new System.Drawing.Point(285, 75);
            this.txtAccountingObjectFax.Name = "txtAccountingObjectFax";
            this.txtAccountingObjectFax.Size = new System.Drawing.Size(111, 22);
            this.txtAccountingObjectFax.TabIndex = 1;
            // 
            // cbbAccountingObjectID
            // 
            appearance53.TextVAlignAsString = "Middle";
            this.cbbAccountingObjectID.Appearance = appearance53;
            this.cbbAccountingObjectID.AutoSize = false;
            appearance54.Image = global::Accounting.Properties.Resources.plus;
            editorButton2.Appearance = appearance54;
            appearance55.Image = global::Accounting.Properties.Resources.plus_press;
            editorButton2.PressedAppearance = appearance55;
            this.cbbAccountingObjectID.ButtonsRight.Add(editorButton2);
            appearance56.BackColor = System.Drawing.SystemColors.Window;
            appearance56.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountingObjectID.DisplayLayout.Appearance = appearance56;
            this.cbbAccountingObjectID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountingObjectID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance57.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance57.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance57.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance57.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectID.DisplayLayout.GroupByBox.Appearance = appearance57;
            appearance58.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance58;
            this.cbbAccountingObjectID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance59.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance59.BackColor2 = System.Drawing.SystemColors.Control;
            appearance59.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance59.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectID.DisplayLayout.GroupByBox.PromptAppearance = appearance59;
            this.cbbAccountingObjectID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountingObjectID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance60.BackColor = System.Drawing.SystemColors.Window;
            appearance60.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountingObjectID.DisplayLayout.Override.ActiveCellAppearance = appearance60;
            appearance61.BackColor = System.Drawing.SystemColors.Highlight;
            appearance61.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountingObjectID.DisplayLayout.Override.ActiveRowAppearance = appearance61;
            this.cbbAccountingObjectID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountingObjectID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance62.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectID.DisplayLayout.Override.CardAreaAppearance = appearance62;
            appearance63.BorderColor = System.Drawing.Color.Silver;
            appearance63.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountingObjectID.DisplayLayout.Override.CellAppearance = appearance63;
            this.cbbAccountingObjectID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountingObjectID.DisplayLayout.Override.CellPadding = 0;
            appearance64.BackColor = System.Drawing.SystemColors.Control;
            appearance64.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance64.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance64.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance64.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectID.DisplayLayout.Override.GroupByRowAppearance = appearance64;
            appearance65.TextHAlignAsString = "Left";
            this.cbbAccountingObjectID.DisplayLayout.Override.HeaderAppearance = appearance65;
            this.cbbAccountingObjectID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountingObjectID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance66.BackColor = System.Drawing.SystemColors.Window;
            appearance66.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountingObjectID.DisplayLayout.Override.RowAppearance = appearance66;
            this.cbbAccountingObjectID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance67.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountingObjectID.DisplayLayout.Override.TemplateAddRowAppearance = appearance67;
            this.cbbAccountingObjectID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountingObjectID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountingObjectID.Location = new System.Drawing.Point(104, 19);
            this.cbbAccountingObjectID.Name = "cbbAccountingObjectID";
            this.cbbAccountingObjectID.Size = new System.Drawing.Size(292, 22);
            this.cbbAccountingObjectID.TabIndex = 2;
            this.cbbAccountingObjectID.AfterCloseUp += new System.EventHandler(this.cbbAccountingObjectID_AfterCloseUp);
            this.cbbAccountingObjectID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccountingObjectID_RowSelected);
            this.cbbAccountingObjectID.ValueChanged += new System.EventHandler(this.cbbAccountingObjectID_ValueChanged);
            this.cbbAccountingObjectID.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbAccountingObjectID_ItemNotInList);
            this.cbbAccountingObjectID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbAccountingObjectID_EditorButtonClick);
            // 
            // txtAccountingObjectTel
            // 
            appearance68.TextVAlignAsString = "Middle";
            this.txtAccountingObjectTel.Appearance = appearance68;
            this.txtAccountingObjectTel.AutoSize = false;
            this.txtAccountingObjectTel.Location = new System.Drawing.Point(104, 75);
            this.txtAccountingObjectTel.Name = "txtAccountingObjectTel";
            this.txtAccountingObjectTel.Size = new System.Drawing.Size(128, 22);
            this.txtAccountingObjectTel.TabIndex = 1;
            // 
            // txtAccountingObjectName
            // 
            appearance69.TextVAlignAsString = "Middle";
            this.txtAccountingObjectName.Appearance = appearance69;
            this.txtAccountingObjectName.AutoSize = false;
            this.txtAccountingObjectName.Location = new System.Drawing.Point(104, 47);
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(292, 22);
            this.txtAccountingObjectName.TabIndex = 1;
            // 
            // txtAccountingObjectAddress
            // 
            appearance70.TextVAlignAsString = "Middle";
            this.txtAccountingObjectAddress.Appearance = appearance70;
            this.txtAccountingObjectAddress.AutoSize = false;
            this.txtAccountingObjectAddress.Location = new System.Drawing.Point(104, 103);
            this.txtAccountingObjectAddress.Name = "txtAccountingObjectAddress";
            this.txtAccountingObjectAddress.Size = new System.Drawing.Size(292, 22);
            this.txtAccountingObjectAddress.TabIndex = 1;
            // 
            // txtAccountingObjectBankName
            // 
            appearance71.TextVAlignAsString = "Middle";
            this.txtAccountingObjectBankName.Appearance = appearance71;
            this.txtAccountingObjectBankName.AutoSize = false;
            this.txtAccountingObjectBankName.Location = new System.Drawing.Point(500, 104);
            this.txtAccountingObjectBankName.Name = "txtAccountingObjectBankName";
            this.txtAccountingObjectBankName.Size = new System.Drawing.Size(332, 21);
            this.txtAccountingObjectBankName.TabIndex = 1;
            // 
            // txtAccountingObjectSignerName
            // 
            appearance72.TextVAlignAsString = "Middle";
            this.txtAccountingObjectSignerName.Appearance = appearance72;
            this.txtAccountingObjectSignerName.AutoSize = false;
            this.txtAccountingObjectSignerName.Location = new System.Drawing.Point(500, 48);
            this.txtAccountingObjectSignerName.Name = "txtAccountingObjectSignerName";
            this.txtAccountingObjectSignerName.Size = new System.Drawing.Size(332, 21);
            this.txtAccountingObjectSignerName.TabIndex = 1;
            // 
            // txtAccountingObjectTitle
            // 
            appearance73.TextVAlignAsString = "Middle";
            this.txtAccountingObjectTitle.Appearance = appearance73;
            this.txtAccountingObjectTitle.AutoSize = false;
            this.txtAccountingObjectTitle.Location = new System.Drawing.Point(500, 20);
            this.txtAccountingObjectTitle.Name = "txtAccountingObjectTitle";
            this.txtAccountingObjectTitle.Size = new System.Drawing.Size(332, 21);
            this.txtAccountingObjectTitle.TabIndex = 1;
            // 
            // ultraLabel22
            // 
            appearance74.TextVAlignAsString = "Middle";
            this.ultraLabel22.Appearance = appearance74;
            this.ultraLabel22.Location = new System.Drawing.Point(412, 103);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel22.TabIndex = 0;
            this.ultraLabel22.Text = "Mở tại NH";
            // 
            // ultraLabel21
            // 
            appearance75.TextVAlignAsString = "Middle";
            this.ultraLabel21.Appearance = appearance75;
            this.ultraLabel21.Location = new System.Drawing.Point(412, 75);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(82, 22);
            this.ultraLabel21.TabIndex = 0;
            this.ultraLabel21.Text = "Tài khoản NH";
            // 
            // ultraLabel20
            // 
            appearance76.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance76;
            this.ultraLabel20.Location = new System.Drawing.Point(412, 47);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel20.TabIndex = 0;
            this.ultraLabel20.Text = "Chức vụ";
            // 
            // ultraLabel19
            // 
            appearance77.TextVAlignAsString = "Middle";
            this.ultraLabel19.Appearance = appearance77;
            this.ultraLabel19.Location = new System.Drawing.Point(412, 19);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel19.TabIndex = 0;
            this.ultraLabel19.Text = "Đại diện";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.txtDueDate);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.cbbDA);
            this.ultraGroupBox1.Controls.Add(this.lblDA);
            this.ultraGroupBox1.Controls.Add(this.cbbOrderID);
            this.ultraGroupBox1.Controls.Add(this.txtAmountOriginal);
            this.ultraGroupBox1.Controls.Add(this.txtAmountBeforCancel);
            this.ultraGroupBox1.Controls.Add(this.txtAmount);
            this.ultraGroupBox1.Controls.Add(this.txtExchangeRate);
            this.ultraGroupBox1.Controls.Add(this.txtName);
            this.ultraGroupBox1.Controls.Add(this.txtDeliverDate);
            this.ultraGroupBox1.Controls.Add(this.txtSignedDate);
            this.ultraGroupBox1.Controls.Add(this.cbbContractEmployeeID);
            this.ultraGroupBox1.Controls.Add(this.cbbDepartmentID);
            this.ultraGroupBox1.Controls.Add(this.cbbCurrencyID);
            this.ultraGroupBox1.Controls.Add(this.txtEMContractCode);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel12);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel11);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox1.Controls.Add(this.lblGTHDQD);
            this.ultraGroupBox1.Controls.Add(this.lblGTHD);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(855, 222);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Hợp đồng";
            // 
            // txtDueDate
            // 
            appearance78.TextVAlignAsString = "Middle";
            this.txtDueDate.Appearance = appearance78;
            this.txtDueDate.AutoSize = false;
            this.txtDueDate.Location = new System.Drawing.Point(310, 188);
            this.txtDueDate.Name = "txtDueDate";
            this.txtDueDate.Size = new System.Drawing.Size(89, 22);
            this.txtDueDate.TabIndex = 26;
            this.txtDueDate.Value = null;
            // 
            // ultraLabel2
            // 
            appearance79.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance79;
            this.ultraLabel2.Location = new System.Drawing.Point(203, 188);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel2.TabIndex = 25;
            this.ultraLabel2.Text = "Hạn thanh toán";
            // 
            // cbbDA
            // 
            appearance80.TextVAlignAsString = "Middle";
            this.cbbDA.Appearance = appearance80;
            this.cbbDA.AutoSize = false;
            appearance81.BackColor = System.Drawing.SystemColors.Window;
            appearance81.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbDA.DisplayLayout.Appearance = appearance81;
            this.cbbDA.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbDA.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance82.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance82.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance82.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance82.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDA.DisplayLayout.GroupByBox.Appearance = appearance82;
            appearance83.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDA.DisplayLayout.GroupByBox.BandLabelAppearance = appearance83;
            this.cbbDA.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance84.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance84.BackColor2 = System.Drawing.SystemColors.Control;
            appearance84.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance84.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDA.DisplayLayout.GroupByBox.PromptAppearance = appearance84;
            this.cbbDA.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbDA.DisplayLayout.MaxRowScrollRegions = 1;
            appearance85.BackColor = System.Drawing.SystemColors.Window;
            appearance85.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbDA.DisplayLayout.Override.ActiveCellAppearance = appearance85;
            appearance86.BackColor = System.Drawing.SystemColors.Highlight;
            appearance86.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbDA.DisplayLayout.Override.ActiveRowAppearance = appearance86;
            this.cbbDA.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbDA.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance87.BackColor = System.Drawing.SystemColors.Window;
            this.cbbDA.DisplayLayout.Override.CardAreaAppearance = appearance87;
            appearance88.BorderColor = System.Drawing.Color.Silver;
            appearance88.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbDA.DisplayLayout.Override.CellAppearance = appearance88;
            this.cbbDA.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbDA.DisplayLayout.Override.CellPadding = 0;
            appearance89.BackColor = System.Drawing.SystemColors.Control;
            appearance89.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance89.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance89.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance89.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDA.DisplayLayout.Override.GroupByRowAppearance = appearance89;
            appearance90.TextHAlignAsString = "Left";
            this.cbbDA.DisplayLayout.Override.HeaderAppearance = appearance90;
            this.cbbDA.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbDA.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance91.BackColor = System.Drawing.SystemColors.Window;
            appearance91.BorderColor = System.Drawing.Color.Silver;
            this.cbbDA.DisplayLayout.Override.RowAppearance = appearance91;
            this.cbbDA.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance92.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbDA.DisplayLayout.Override.TemplateAddRowAppearance = appearance92;
            this.cbbDA.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbDA.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbDA.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbDA.Location = new System.Drawing.Point(107, 51);
            this.cbbDA.Name = "cbbDA";
            this.cbbDA.Size = new System.Drawing.Size(292, 22);
            this.cbbDA.TabIndex = 24;
            this.cbbDA.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbDA_RowSelected);
            this.cbbDA.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbDA_ItemNotInList);
            this.cbbDA.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbDA_EditorButtonClick);
            // 
            // lblDA
            // 
            appearance93.TextVAlignAsString = "Middle";
            this.lblDA.Appearance = appearance93;
            this.lblDA.Location = new System.Drawing.Point(12, 51);
            this.lblDA.Name = "lblDA";
            this.lblDA.Size = new System.Drawing.Size(88, 22);
            this.lblDA.TabIndex = 23;
            this.lblDA.Text = "Thuộc dự án ";
            // 
            // cbbOrderID
            // 
            appearance94.TextVAlignAsString = "Middle";
            this.cbbOrderID.Appearance = appearance94;
            this.cbbOrderID.AutoSize = false;
            appearance95.BackColor = System.Drawing.SystemColors.Window;
            appearance95.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbOrderID.DisplayLayout.Appearance = appearance95;
            this.cbbOrderID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbOrderID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance96.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance96.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance96.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance96.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbOrderID.DisplayLayout.GroupByBox.Appearance = appearance96;
            appearance97.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbOrderID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance97;
            this.cbbOrderID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance98.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance98.BackColor2 = System.Drawing.SystemColors.Control;
            appearance98.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance98.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbOrderID.DisplayLayout.GroupByBox.PromptAppearance = appearance98;
            this.cbbOrderID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbOrderID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance99.BackColor = System.Drawing.SystemColors.Window;
            appearance99.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbOrderID.DisplayLayout.Override.ActiveCellAppearance = appearance99;
            appearance100.BackColor = System.Drawing.SystemColors.Highlight;
            appearance100.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbOrderID.DisplayLayout.Override.ActiveRowAppearance = appearance100;
            this.cbbOrderID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbOrderID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance101.BackColor = System.Drawing.SystemColors.Window;
            this.cbbOrderID.DisplayLayout.Override.CardAreaAppearance = appearance101;
            appearance102.BorderColor = System.Drawing.Color.Silver;
            appearance102.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbOrderID.DisplayLayout.Override.CellAppearance = appearance102;
            this.cbbOrderID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbOrderID.DisplayLayout.Override.CellPadding = 0;
            appearance103.BackColor = System.Drawing.SystemColors.Control;
            appearance103.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance103.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance103.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance103.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbOrderID.DisplayLayout.Override.GroupByRowAppearance = appearance103;
            appearance104.TextHAlignAsString = "Left";
            this.cbbOrderID.DisplayLayout.Override.HeaderAppearance = appearance104;
            this.cbbOrderID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbOrderID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance105.BackColor = System.Drawing.SystemColors.Window;
            appearance105.BorderColor = System.Drawing.Color.Silver;
            this.cbbOrderID.DisplayLayout.Override.RowAppearance = appearance105;
            this.cbbOrderID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance106.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbOrderID.DisplayLayout.Override.TemplateAddRowAppearance = appearance106;
            this.cbbOrderID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbOrderID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbOrderID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbOrderID.Location = new System.Drawing.Point(107, 131);
            this.cbbOrderID.Name = "cbbOrderID";
            this.cbbOrderID.Size = new System.Drawing.Size(292, 22);
            this.cbbOrderID.TabIndex = 3;
            this.cbbOrderID.AfterCloseUp += new System.EventHandler(this.cbbOrderID_AfterCloseUp);
            this.cbbOrderID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbOrderID_RowSelected);
            this.cbbOrderID.ValueChanged += new System.EventHandler(this.cbbOrderID_ValueChanged);
            this.cbbOrderID.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbOrderID_ItemNotInList);
            // 
            // txtAmountOriginal
            // 
            appearance107.TextHAlignAsString = "Right";
            this.txtAmountOriginal.Appearance = appearance107;
            this.txtAmountOriginal.AutoSize = false;
            this.txtAmountOriginal.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtAmountOriginal.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.txtAmountOriginal.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtAmountOriginal.InputMask = " -n,nnn,nnn,nnn,nnn.nn";
            this.txtAmountOriginal.Location = new System.Drawing.Point(687, 24);
            this.txtAmountOriginal.Name = "txtAmountOriginal";
            this.txtAmountOriginal.PromptChar = ' ';
            this.txtAmountOriginal.Size = new System.Drawing.Size(148, 22);
            this.txtAmountOriginal.TabIndex = 21;
            this.txtAmountOriginal.Text = " 0";
            this.txtAmountOriginal.TextChanged += new System.EventHandler(this.txtAmountOriginal_TextChanged);
            // 
            // txtAmountBeforCancel
            // 
            appearance108.TextHAlignAsString = "Right";
            this.txtAmountBeforCancel.Appearance = appearance108;
            this.txtAmountBeforCancel.AutoSize = false;
            this.txtAmountBeforCancel.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtAmountBeforCancel.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.txtAmountBeforCancel.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtAmountBeforCancel.InputMask = " -n,nnn,nnn,nnn,nnn.nn";
            this.txtAmountBeforCancel.Location = new System.Drawing.Point(503, 79);
            this.txtAmountBeforCancel.Name = "txtAmountBeforCancel";
            this.txtAmountBeforCancel.PromptChar = ' ';
            this.txtAmountBeforCancel.Size = new System.Drawing.Size(332, 22);
            this.txtAmountBeforCancel.TabIndex = 21;
            this.txtAmountBeforCancel.Text = " ";
            // 
            // txtAmount
            // 
            appearance109.TextHAlignAsString = "Right";
            this.txtAmount.Appearance = appearance109;
            this.txtAmount.AutoSize = false;
            this.txtAmount.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtAmount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.txtAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtAmount.Enabled = false;
            this.txtAmount.InputMask = " -n,nnn,nnn,nnn,nnn.nn";
            this.txtAmount.Location = new System.Drawing.Point(687, 51);
            this.txtAmount.MinValue = "0";
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.PromptChar = ' ';
            this.txtAmount.Size = new System.Drawing.Size(148, 22);
            this.txtAmount.TabIndex = 21;
            this.txtAmount.Text = " 0";
            this.txtAmount.TextChanged += new System.EventHandler(this.txtAmount_TextChanged);
            // 
            // txtExchangeRate
            // 
            this.txtExchangeRate.AutoSize = false;
            this.txtExchangeRate.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtExchangeRate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.txtExchangeRate.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtExchangeRate.InputMask = " -n,nnn,nnn,nnn,nnn.nn";
            this.txtExchangeRate.Location = new System.Drawing.Point(503, 51);
            this.txtExchangeRate.Name = "txtExchangeRate";
            this.txtExchangeRate.PromptChar = ' ';
            this.txtExchangeRate.Size = new System.Drawing.Size(68, 22);
            this.txtExchangeRate.TabIndex = 20;
            this.txtExchangeRate.Text = " ";
            this.txtExchangeRate.TextChanged += new System.EventHandler(this.txtExchangeRate_TextChanged);
            // 
            // txtName
            // 
            appearance110.TextVAlignAsString = "Top";
            this.txtName.Appearance = appearance110;
            this.txtName.AutoSize = false;
            this.txtName.Location = new System.Drawing.Point(107, 79);
            this.txtName.Multiline = true;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(292, 46);
            this.txtName.TabIndex = 6;
            // 
            // txtDeliverDate
            // 
            appearance111.TextVAlignAsString = "Middle";
            this.txtDeliverDate.Appearance = appearance111;
            this.txtDeliverDate.AutoSize = false;
            this.txtDeliverDate.Location = new System.Drawing.Point(310, 160);
            this.txtDeliverDate.Name = "txtDeliverDate";
            this.txtDeliverDate.Size = new System.Drawing.Size(89, 22);
            this.txtDeliverDate.TabIndex = 5;
            this.txtDeliverDate.Value = null;
            // 
            // txtSignedDate
            // 
            appearance112.TextVAlignAsString = "Middle";
            this.txtSignedDate.Appearance = appearance112;
            this.txtSignedDate.AutoSize = false;
            this.txtSignedDate.Location = new System.Drawing.Point(107, 160);
            this.txtSignedDate.Name = "txtSignedDate";
            this.txtSignedDate.Size = new System.Drawing.Size(90, 22);
            this.txtSignedDate.TabIndex = 5;
            this.txtSignedDate.ValueChanged += new System.EventHandler(this.txtSignedDate_ValueChanged);
            // 
            // cbbContractEmployeeID
            // 
            appearance113.TextVAlignAsString = "Middle";
            this.cbbContractEmployeeID.Appearance = appearance113;
            this.cbbContractEmployeeID.AutoSize = false;
            appearance114.Image = global::Accounting.Properties.Resources.plus;
            editorButton3.Appearance = appearance114;
            appearance115.Image = global::Accounting.Properties.Resources.plus_press;
            editorButton3.PressedAppearance = appearance115;
            this.cbbContractEmployeeID.ButtonsRight.Add(editorButton3);
            appearance116.BackColor = System.Drawing.SystemColors.Window;
            appearance116.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbContractEmployeeID.DisplayLayout.Appearance = appearance116;
            this.cbbContractEmployeeID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbContractEmployeeID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance117.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance117.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance117.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance117.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbContractEmployeeID.DisplayLayout.GroupByBox.Appearance = appearance117;
            appearance118.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbContractEmployeeID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance118;
            this.cbbContractEmployeeID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance119.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance119.BackColor2 = System.Drawing.SystemColors.Control;
            appearance119.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance119.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbContractEmployeeID.DisplayLayout.GroupByBox.PromptAppearance = appearance119;
            this.cbbContractEmployeeID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbContractEmployeeID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance120.BackColor = System.Drawing.SystemColors.Window;
            appearance120.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbContractEmployeeID.DisplayLayout.Override.ActiveCellAppearance = appearance120;
            appearance121.BackColor = System.Drawing.SystemColors.Highlight;
            appearance121.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbContractEmployeeID.DisplayLayout.Override.ActiveRowAppearance = appearance121;
            this.cbbContractEmployeeID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbContractEmployeeID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance122.BackColor = System.Drawing.SystemColors.Window;
            this.cbbContractEmployeeID.DisplayLayout.Override.CardAreaAppearance = appearance122;
            appearance123.BorderColor = System.Drawing.Color.Silver;
            appearance123.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbContractEmployeeID.DisplayLayout.Override.CellAppearance = appearance123;
            this.cbbContractEmployeeID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbContractEmployeeID.DisplayLayout.Override.CellPadding = 0;
            appearance124.BackColor = System.Drawing.SystemColors.Control;
            appearance124.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance124.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance124.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance124.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbContractEmployeeID.DisplayLayout.Override.GroupByRowAppearance = appearance124;
            appearance125.TextHAlignAsString = "Left";
            this.cbbContractEmployeeID.DisplayLayout.Override.HeaderAppearance = appearance125;
            this.cbbContractEmployeeID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbContractEmployeeID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance126.BackColor = System.Drawing.SystemColors.Window;
            appearance126.BorderColor = System.Drawing.Color.Silver;
            this.cbbContractEmployeeID.DisplayLayout.Override.RowAppearance = appearance126;
            this.cbbContractEmployeeID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance127.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbContractEmployeeID.DisplayLayout.Override.TemplateAddRowAppearance = appearance127;
            this.cbbContractEmployeeID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbContractEmployeeID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbContractEmployeeID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbContractEmployeeID.Location = new System.Drawing.Point(503, 139);
            this.cbbContractEmployeeID.Name = "cbbContractEmployeeID";
            this.cbbContractEmployeeID.Size = new System.Drawing.Size(332, 22);
            this.cbbContractEmployeeID.TabIndex = 2;
            this.cbbContractEmployeeID.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbContractEmployeeID_ItemNotInList);
            this.cbbContractEmployeeID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbContractEmployeeID_EditorButtonClick);
            // 
            // cbbDepartmentID
            // 
            appearance128.TextVAlignAsString = "Middle";
            this.cbbDepartmentID.Appearance = appearance128;
            this.cbbDepartmentID.AutoSize = false;
            appearance129.BackColor = System.Drawing.SystemColors.Window;
            appearance129.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbDepartmentID.DisplayLayout.Appearance = appearance129;
            this.cbbDepartmentID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbDepartmentID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance130.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance130.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance130.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance130.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDepartmentID.DisplayLayout.GroupByBox.Appearance = appearance130;
            appearance131.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDepartmentID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance131;
            this.cbbDepartmentID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance132.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance132.BackColor2 = System.Drawing.SystemColors.Control;
            appearance132.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance132.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDepartmentID.DisplayLayout.GroupByBox.PromptAppearance = appearance132;
            this.cbbDepartmentID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbDepartmentID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance133.BackColor = System.Drawing.SystemColors.Window;
            appearance133.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbDepartmentID.DisplayLayout.Override.ActiveCellAppearance = appearance133;
            appearance134.BackColor = System.Drawing.SystemColors.Highlight;
            appearance134.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbDepartmentID.DisplayLayout.Override.ActiveRowAppearance = appearance134;
            this.cbbDepartmentID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbDepartmentID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance135.BackColor = System.Drawing.SystemColors.Window;
            this.cbbDepartmentID.DisplayLayout.Override.CardAreaAppearance = appearance135;
            appearance136.BorderColor = System.Drawing.Color.Silver;
            appearance136.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbDepartmentID.DisplayLayout.Override.CellAppearance = appearance136;
            this.cbbDepartmentID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbDepartmentID.DisplayLayout.Override.CellPadding = 0;
            appearance137.BackColor = System.Drawing.SystemColors.Control;
            appearance137.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance137.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance137.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance137.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDepartmentID.DisplayLayout.Override.GroupByRowAppearance = appearance137;
            appearance138.TextHAlignAsString = "Left";
            this.cbbDepartmentID.DisplayLayout.Override.HeaderAppearance = appearance138;
            this.cbbDepartmentID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbDepartmentID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance139.BackColor = System.Drawing.SystemColors.Window;
            appearance139.BorderColor = System.Drawing.Color.Silver;
            this.cbbDepartmentID.DisplayLayout.Override.RowAppearance = appearance139;
            this.cbbDepartmentID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance140.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbDepartmentID.DisplayLayout.Override.TemplateAddRowAppearance = appearance140;
            this.cbbDepartmentID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbDepartmentID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbDepartmentID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbDepartmentID.Location = new System.Drawing.Point(503, 110);
            this.cbbDepartmentID.Name = "cbbDepartmentID";
            this.cbbDepartmentID.Size = new System.Drawing.Size(332, 22);
            this.cbbDepartmentID.TabIndex = 2;
            this.cbbDepartmentID.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbDepartmentID_ItemNotInList);
            // 
            // cbbCurrencyID
            // 
            appearance141.TextVAlignAsString = "Middle";
            this.cbbCurrencyID.Appearance = appearance141;
            this.cbbCurrencyID.AutoSize = false;
            appearance142.BackColor = System.Drawing.SystemColors.Window;
            appearance142.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbCurrencyID.DisplayLayout.Appearance = appearance142;
            this.cbbCurrencyID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbCurrencyID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance143.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance143.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance143.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance143.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCurrencyID.DisplayLayout.GroupByBox.Appearance = appearance143;
            appearance144.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCurrencyID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance144;
            this.cbbCurrencyID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance145.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance145.BackColor2 = System.Drawing.SystemColors.Control;
            appearance145.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance145.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCurrencyID.DisplayLayout.GroupByBox.PromptAppearance = appearance145;
            this.cbbCurrencyID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbCurrencyID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance146.BackColor = System.Drawing.SystemColors.Window;
            appearance146.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbCurrencyID.DisplayLayout.Override.ActiveCellAppearance = appearance146;
            appearance147.BackColor = System.Drawing.SystemColors.Highlight;
            appearance147.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbCurrencyID.DisplayLayout.Override.ActiveRowAppearance = appearance147;
            this.cbbCurrencyID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbCurrencyID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance148.BackColor = System.Drawing.SystemColors.Window;
            this.cbbCurrencyID.DisplayLayout.Override.CardAreaAppearance = appearance148;
            appearance149.BorderColor = System.Drawing.Color.Silver;
            appearance149.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbCurrencyID.DisplayLayout.Override.CellAppearance = appearance149;
            this.cbbCurrencyID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbCurrencyID.DisplayLayout.Override.CellPadding = 0;
            appearance150.BackColor = System.Drawing.SystemColors.Control;
            appearance150.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance150.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance150.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance150.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCurrencyID.DisplayLayout.Override.GroupByRowAppearance = appearance150;
            appearance151.TextHAlignAsString = "Left";
            this.cbbCurrencyID.DisplayLayout.Override.HeaderAppearance = appearance151;
            this.cbbCurrencyID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbCurrencyID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance152.BackColor = System.Drawing.SystemColors.Window;
            appearance152.BorderColor = System.Drawing.Color.Silver;
            this.cbbCurrencyID.DisplayLayout.Override.RowAppearance = appearance152;
            this.cbbCurrencyID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance153.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbCurrencyID.DisplayLayout.Override.TemplateAddRowAppearance = appearance153;
            this.cbbCurrencyID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbCurrencyID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbCurrencyID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbCurrencyID.Location = new System.Drawing.Point(503, 23);
            this.cbbCurrencyID.Name = "cbbCurrencyID";
            this.cbbCurrencyID.Size = new System.Drawing.Size(68, 22);
            this.cbbCurrencyID.TabIndex = 2;
            this.cbbCurrencyID.Text = "VND";
            this.cbbCurrencyID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbCurrencyID_RowSelected);
            this.cbbCurrencyID.TextChanged += new System.EventHandler(this.cbbCurrencyID_TextChanged);
            // 
            // txtEMContractCode
            // 
            appearance154.TextVAlignAsString = "Middle";
            this.txtEMContractCode.Appearance = appearance154;
            this.txtEMContractCode.AutoSize = false;
            this.txtEMContractCode.Location = new System.Drawing.Point(107, 23);
            this.txtEMContractCode.Name = "txtEMContractCode";
            this.txtEMContractCode.Size = new System.Drawing.Size(292, 22);
            this.txtEMContractCode.TabIndex = 1;
            this.txtEMContractCode.ValueChanged += new System.EventHandler(this.txtEMContractCode_ValueChanged);
            // 
            // ultraLabel5
            // 
            appearance155.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance155;
            this.ultraLabel5.Location = new System.Drawing.Point(12, 79);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(81, 22);
            this.ultraLabel5.TabIndex = 0;
            this.ultraLabel5.Text = "Trích yếu ";
            // 
            // ultraLabel6
            // 
            appearance156.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance156;
            this.ultraLabel6.Location = new System.Drawing.Point(203, 160);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel6.TabIndex = 0;
            this.ultraLabel6.Text = "Ngày giao hàng";
            // 
            // ultraLabel4
            // 
            appearance157.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance157;
            this.ultraLabel4.Location = new System.Drawing.Point(12, 160);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(65, 22);
            this.ultraLabel4.TabIndex = 0;
            this.ultraLabel4.Text = "Ngày ký (*)";
            // 
            // ultraLabel3
            // 
            appearance158.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance158;
            this.ultraLabel3.Location = new System.Drawing.Point(12, 131);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(78, 22);
            this.ultraLabel3.TabIndex = 0;
            this.ultraLabel3.Text = "Số đơn hàng";
            // 
            // ultraLabel12
            // 
            appearance159.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance159;
            this.ultraLabel12.Location = new System.Drawing.Point(415, 139);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(88, 22);
            this.ultraLabel12.TabIndex = 0;
            this.ultraLabel12.Text = "Người thực hiện";
            // 
            // ultraLabel11
            // 
            appearance160.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance160;
            this.ultraLabel11.Location = new System.Drawing.Point(415, 110);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(88, 22);
            this.ultraLabel11.TabIndex = 0;
            this.ultraLabel11.Text = "Đơn vị thực hiện";
            // 
            // ultraLabel10
            // 
            appearance161.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance161;
            this.ultraLabel10.Location = new System.Drawing.Point(415, 81);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(82, 22);
            this.ultraLabel10.TabIndex = 0;
            this.ultraLabel10.Text = "Giá trị thanh lý";
            // 
            // ultraLabel9
            // 
            appearance162.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance162;
            this.ultraLabel9.Location = new System.Drawing.Point(415, 52);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(46, 22);
            this.ultraLabel9.TabIndex = 0;
            this.ultraLabel9.Text = "Tỷ giá";
            // 
            // lblGTHDQD
            // 
            appearance163.TextVAlignAsString = "Middle";
            this.lblGTHDQD.Appearance = appearance163;
            this.lblGTHDQD.Location = new System.Drawing.Point(577, 52);
            this.lblGTHDQD.Name = "lblGTHDQD";
            this.lblGTHDQD.Size = new System.Drawing.Size(104, 23);
            this.lblGTHDQD.TabIndex = 0;
            this.lblGTHDQD.Text = "Giá trị HĐ QĐ";
            // 
            // lblGTHD
            // 
            appearance164.TextVAlignAsString = "Middle";
            this.lblGTHD.Appearance = appearance164;
            this.lblGTHD.Location = new System.Drawing.Point(577, 23);
            this.lblGTHD.Name = "lblGTHD";
            this.lblGTHD.Size = new System.Drawing.Size(77, 23);
            this.lblGTHD.TabIndex = 0;
            this.lblGTHD.Text = "Giá trị HĐ ";
            // 
            // ultraLabel7
            // 
            appearance165.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance165;
            this.ultraLabel7.Location = new System.Drawing.Point(415, 23);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel7.TabIndex = 0;
            this.ultraLabel7.Text = "Loại tiền";
            // 
            // ultraLabel1
            // 
            appearance166.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance166;
            this.ultraLabel1.Location = new System.Drawing.Point(12, 23);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(89, 22);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Số hợp đồng (*)";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.ultraGroupBox6);
            this.ultraTabPageControl2.Controls.Add(this.ultraGroupBox5);
            this.ultraTabPageControl2.Controls.Add(this.ultraGroupBox4);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(855, 558);
            // 
            // ultraGroupBox6
            // 
            this.ultraGroupBox6.Controls.Add(this.lblToDate1);
            this.ultraGroupBox6.Controls.Add(this.ultraLabel35);
            this.ultraGroupBox6.Controls.Add(this.lblFromDate1);
            this.ultraGroupBox6.Controls.Add(this.uGridChungTuChi);
            this.ultraGroupBox6.Controls.Add(this.btnChungTuChi);
            this.ultraGroupBox6.Controls.Add(this.ultraLabel32);
            this.ultraGroupBox6.Location = new System.Drawing.Point(4, 363);
            this.ultraGroupBox6.Name = "ultraGroupBox6";
            this.ultraGroupBox6.Size = new System.Drawing.Size(842, 167);
            this.ultraGroupBox6.TabIndex = 2;
            this.ultraGroupBox6.Text = "Thực chi";
            // 
            // lblToDate1
            // 
            this.lblToDate1.Location = new System.Drawing.Point(319, 23);
            this.lblToDate1.Name = "lblToDate1";
            this.lblToDate1.Size = new System.Drawing.Size(100, 23);
            this.lblToDate1.TabIndex = 9;
            // 
            // ultraLabel35
            // 
            this.ultraLabel35.Location = new System.Drawing.Point(245, 23);
            this.ultraLabel35.Name = "ultraLabel35";
            this.ultraLabel35.Size = new System.Drawing.Size(100, 23);
            this.ultraLabel35.TabIndex = 8;
            this.ultraLabel35.Text = "đến hết ngày";
            // 
            // lblFromDate1
            // 
            this.lblFromDate1.Location = new System.Drawing.Point(139, 23);
            this.lblFromDate1.Name = "lblFromDate1";
            this.lblFromDate1.Size = new System.Drawing.Size(100, 23);
            this.lblFromDate1.TabIndex = 7;
            // 
            // uGridChungTuChi
            // 
            appearance167.BackColor = System.Drawing.SystemColors.Window;
            appearance167.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridChungTuChi.DisplayLayout.Appearance = appearance167;
            this.uGridChungTuChi.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridChungTuChi.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance168.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance168.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance168.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance168.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridChungTuChi.DisplayLayout.GroupByBox.Appearance = appearance168;
            appearance169.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridChungTuChi.DisplayLayout.GroupByBox.BandLabelAppearance = appearance169;
            this.uGridChungTuChi.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance170.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance170.BackColor2 = System.Drawing.SystemColors.Control;
            appearance170.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance170.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridChungTuChi.DisplayLayout.GroupByBox.PromptAppearance = appearance170;
            appearance171.BackColor = System.Drawing.SystemColors.Window;
            appearance171.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridChungTuChi.DisplayLayout.Override.ActiveCellAppearance = appearance171;
            appearance172.BackColor = System.Drawing.SystemColors.Highlight;
            appearance172.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridChungTuChi.DisplayLayout.Override.ActiveRowAppearance = appearance172;
            this.uGridChungTuChi.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridChungTuChi.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance173.BackColor = System.Drawing.SystemColors.Window;
            this.uGridChungTuChi.DisplayLayout.Override.CardAreaAppearance = appearance173;
            appearance174.BorderColor = System.Drawing.Color.Silver;
            appearance174.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridChungTuChi.DisplayLayout.Override.CellAppearance = appearance174;
            this.uGridChungTuChi.DisplayLayout.Override.CellPadding = 0;
            appearance175.BackColor = System.Drawing.SystemColors.Control;
            appearance175.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance175.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance175.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance175.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridChungTuChi.DisplayLayout.Override.GroupByRowAppearance = appearance175;
            appearance176.TextHAlignAsString = "Left";
            this.uGridChungTuChi.DisplayLayout.Override.HeaderAppearance = appearance176;
            this.uGridChungTuChi.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance177.BackColor = System.Drawing.SystemColors.Window;
            appearance177.BorderColor = System.Drawing.Color.Silver;
            this.uGridChungTuChi.DisplayLayout.Override.RowAppearance = appearance177;
            appearance178.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridChungTuChi.DisplayLayout.Override.TemplateAddRowAppearance = appearance178;
            this.uGridChungTuChi.Location = new System.Drawing.Point(7, 48);
            this.uGridChungTuChi.Name = "uGridChungTuChi";
            this.uGridChungTuChi.Size = new System.Drawing.Size(829, 113);
            this.uGridChungTuChi.TabIndex = 6;
            this.uGridChungTuChi.Text = "ultraGrid3";
            // 
            // btnChungTuChi
            // 
            this.btnChungTuChi.Location = new System.Drawing.Point(679, 18);
            this.btnChungTuChi.Name = "btnChungTuChi";
            this.btnChungTuChi.Size = new System.Drawing.Size(155, 24);
            this.btnChungTuChi.TabIndex = 5;
            this.btnChungTuChi.Text = "Chọn chứng từ chi";
            this.btnChungTuChi.Click += new System.EventHandler(this.btnChungTuChi_Click);
            // 
            // ultraLabel32
            // 
            appearance179.TextVAlignAsString = "Middle";
            this.ultraLabel32.Appearance = appearance179;
            this.ultraLabel32.Location = new System.Drawing.Point(8, 19);
            this.ultraLabel32.Name = "ultraLabel32";
            this.ultraLabel32.Size = new System.Drawing.Size(125, 22);
            this.ultraLabel32.TabIndex = 3;
            this.ultraLabel32.Text = "Số đã chi tính từ ngày";
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.Controls.Add(this.ultraPanel2);
            this.ultraGroupBox5.Controls.Add(this.ultraPanel1);
            this.ultraGroupBox5.Location = new System.Drawing.Point(2, 179);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(845, 178);
            this.ultraGroupBox5.TabIndex = 1;
            this.ultraGroupBox5.Text = "Thực thu";
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.uGridChungTuThu);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel2.Location = new System.Drawing.Point(3, 65);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(839, 110);
            this.ultraPanel2.TabIndex = 5;
            // 
            // uGridChungTuThu
            // 
            appearance180.BackColor = System.Drawing.SystemColors.Window;
            appearance180.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridChungTuThu.DisplayLayout.Appearance = appearance180;
            this.uGridChungTuThu.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridChungTuThu.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance181.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance181.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance181.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance181.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridChungTuThu.DisplayLayout.GroupByBox.Appearance = appearance181;
            appearance182.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridChungTuThu.DisplayLayout.GroupByBox.BandLabelAppearance = appearance182;
            this.uGridChungTuThu.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance183.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance183.BackColor2 = System.Drawing.SystemColors.Control;
            appearance183.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance183.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridChungTuThu.DisplayLayout.GroupByBox.PromptAppearance = appearance183;
            appearance184.BackColor = System.Drawing.SystemColors.Window;
            appearance184.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridChungTuThu.DisplayLayout.Override.ActiveCellAppearance = appearance184;
            appearance185.BackColor = System.Drawing.SystemColors.Highlight;
            appearance185.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridChungTuThu.DisplayLayout.Override.ActiveRowAppearance = appearance185;
            this.uGridChungTuThu.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridChungTuThu.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance186.BackColor = System.Drawing.SystemColors.Window;
            this.uGridChungTuThu.DisplayLayout.Override.CardAreaAppearance = appearance186;
            appearance187.BorderColor = System.Drawing.Color.Silver;
            appearance187.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridChungTuThu.DisplayLayout.Override.CellAppearance = appearance187;
            this.uGridChungTuThu.DisplayLayout.Override.CellPadding = 0;
            appearance188.BackColor = System.Drawing.SystemColors.Control;
            appearance188.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance188.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance188.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance188.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridChungTuThu.DisplayLayout.Override.GroupByRowAppearance = appearance188;
            appearance189.TextHAlignAsString = "Left";
            this.uGridChungTuThu.DisplayLayout.Override.HeaderAppearance = appearance189;
            this.uGridChungTuThu.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance190.BackColor = System.Drawing.SystemColors.Window;
            appearance190.BorderColor = System.Drawing.Color.Silver;
            this.uGridChungTuThu.DisplayLayout.Override.RowAppearance = appearance190;
            appearance191.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridChungTuThu.DisplayLayout.Override.TemplateAddRowAppearance = appearance191;
            this.uGridChungTuThu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridChungTuThu.Location = new System.Drawing.Point(0, 0);
            this.uGridChungTuThu.Name = "uGridChungTuThu";
            this.uGridChungTuThu.Size = new System.Drawing.Size(839, 110);
            this.uGridChungTuThu.TabIndex = 3;
            this.uGridChungTuThu.Text = "ultraGrid2";
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel14);
            this.ultraPanel1.ClientArea.Controls.Add(this.lblToDate);
            this.ultraPanel1.ClientArea.Controls.Add(this.lblFromDate);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnChungTuThu);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel31);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel1.Location = new System.Drawing.Point(3, 16);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(839, 49);
            this.ultraPanel1.TabIndex = 4;
            // 
            // ultraLabel14
            // 
            this.ultraLabel14.Location = new System.Drawing.Point(244, 23);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(77, 23);
            this.ultraLabel14.TabIndex = 5;
            this.ultraLabel14.Text = "đến hết ngày";
            // 
            // lblToDate
            // 
            this.lblToDate.Location = new System.Drawing.Point(318, 23);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(100, 23);
            this.lblToDate.TabIndex = 4;
            // 
            // lblFromDate
            // 
            this.lblFromDate.Location = new System.Drawing.Point(138, 23);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(100, 23);
            this.lblFromDate.TabIndex = 3;
            // 
            // btnChungTuThu
            // 
            this.btnChungTuThu.Location = new System.Drawing.Point(681, 18);
            this.btnChungTuThu.Name = "btnChungTuThu";
            this.btnChungTuThu.Size = new System.Drawing.Size(155, 24);
            this.btnChungTuThu.TabIndex = 2;
            this.btnChungTuThu.Text = "Chọn chứng từ thu";
            this.btnChungTuThu.Click += new System.EventHandler(this.btnChungTuThu_Click);
            // 
            // ultraLabel31
            // 
            appearance192.TextVAlignAsString = "Middle";
            this.ultraLabel31.Appearance = appearance192;
            this.ultraLabel31.Location = new System.Drawing.Point(10, 19);
            this.ultraLabel31.Name = "ultraLabel31";
            this.ultraLabel31.Size = new System.Drawing.Size(122, 22);
            this.ultraLabel31.TabIndex = 0;
            this.ultraLabel31.Text = "Số đã thu tính từ ngày  ";
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.uGridChi);
            this.ultraGroupBox4.Location = new System.Drawing.Point(2, 3);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(844, 169);
            this.ultraGroupBox4.TabIndex = 0;
            this.ultraGroupBox4.Text = "Dự kiến chi";
            // 
            // uGridChi
            // 
            this.uGridChi.ContextMenuStrip = this.contextMenuStrip1;
            appearance193.BackColor = System.Drawing.SystemColors.Window;
            appearance193.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridChi.DisplayLayout.Appearance = appearance193;
            this.uGridChi.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridChi.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance194.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance194.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance194.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance194.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridChi.DisplayLayout.GroupByBox.Appearance = appearance194;
            appearance195.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridChi.DisplayLayout.GroupByBox.BandLabelAppearance = appearance195;
            this.uGridChi.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance196.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance196.BackColor2 = System.Drawing.SystemColors.Control;
            appearance196.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance196.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridChi.DisplayLayout.GroupByBox.PromptAppearance = appearance196;
            appearance197.BackColor = System.Drawing.SystemColors.Window;
            appearance197.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridChi.DisplayLayout.Override.ActiveCellAppearance = appearance197;
            appearance198.BackColor = System.Drawing.SystemColors.Highlight;
            appearance198.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridChi.DisplayLayout.Override.ActiveRowAppearance = appearance198;
            this.uGridChi.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridChi.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance199.BackColor = System.Drawing.SystemColors.Window;
            this.uGridChi.DisplayLayout.Override.CardAreaAppearance = appearance199;
            appearance200.BorderColor = System.Drawing.Color.Silver;
            appearance200.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridChi.DisplayLayout.Override.CellAppearance = appearance200;
            this.uGridChi.DisplayLayout.Override.CellPadding = 0;
            appearance201.BackColor = System.Drawing.SystemColors.Control;
            appearance201.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance201.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance201.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance201.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridChi.DisplayLayout.Override.GroupByRowAppearance = appearance201;
            appearance202.TextHAlignAsString = "Left";
            this.uGridChi.DisplayLayout.Override.HeaderAppearance = appearance202;
            this.uGridChi.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance203.BackColor = System.Drawing.SystemColors.Window;
            appearance203.BorderColor = System.Drawing.Color.Silver;
            this.uGridChi.DisplayLayout.Override.RowAppearance = appearance203;
            appearance204.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridChi.DisplayLayout.Override.TemplateAddRowAppearance = appearance204;
            this.uGridChi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridChi.Location = new System.Drawing.Point(3, 16);
            this.uGridChi.Name = "uGridChi";
            this.uGridChi.Size = new System.Drawing.Size(838, 150);
            this.uGridChi.TabIndex = 0;
            this.uGridChi.Text = "ultraGrid1";
            this.uGridChi.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridChi_AfterCellUpdate);
            this.uGridChi.BeforeRowActivate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.uGridChi_BeforeRowActivate);
            this.uGridChi.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.uGridChi_Error);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmDelete});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(204, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(203, 22);
            this.tsmAdd.Text = "Thêm một dòng";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(203, 22);
            this.tsmDelete.Text = "Xóa một dòng";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.uGridEMContractMG);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(855, 558);
            // 
            // uGridEMContractMG
            // 
            this.uGridEMContractMG.ContextMenuStrip = this.contextMenuStrip1;
            appearance205.BackColor = System.Drawing.SystemColors.Window;
            appearance205.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEMContractMG.DisplayLayout.Appearance = appearance205;
            this.uGridEMContractMG.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEMContractMG.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance206.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance206.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance206.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance206.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEMContractMG.DisplayLayout.GroupByBox.Appearance = appearance206;
            appearance207.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEMContractMG.DisplayLayout.GroupByBox.BandLabelAppearance = appearance207;
            this.uGridEMContractMG.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance208.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance208.BackColor2 = System.Drawing.SystemColors.Control;
            appearance208.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance208.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEMContractMG.DisplayLayout.GroupByBox.PromptAppearance = appearance208;
            appearance209.BackColor = System.Drawing.SystemColors.Window;
            appearance209.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEMContractMG.DisplayLayout.Override.ActiveCellAppearance = appearance209;
            appearance210.BackColor = System.Drawing.SystemColors.Highlight;
            appearance210.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEMContractMG.DisplayLayout.Override.ActiveRowAppearance = appearance210;
            this.uGridEMContractMG.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEMContractMG.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance211.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEMContractMG.DisplayLayout.Override.CardAreaAppearance = appearance211;
            appearance212.BorderColor = System.Drawing.Color.Silver;
            appearance212.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEMContractMG.DisplayLayout.Override.CellAppearance = appearance212;
            this.uGridEMContractMG.DisplayLayout.Override.CellPadding = 0;
            appearance213.BackColor = System.Drawing.SystemColors.Control;
            appearance213.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance213.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance213.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance213.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEMContractMG.DisplayLayout.Override.GroupByRowAppearance = appearance213;
            appearance214.TextHAlignAsString = "Left";
            this.uGridEMContractMG.DisplayLayout.Override.HeaderAppearance = appearance214;
            this.uGridEMContractMG.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance215.BackColor = System.Drawing.SystemColors.Window;
            appearance215.BorderColor = System.Drawing.Color.Silver;
            this.uGridEMContractMG.DisplayLayout.Override.RowAppearance = appearance215;
            appearance216.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEMContractMG.DisplayLayout.Override.TemplateAddRowAppearance = appearance216;
            this.uGridEMContractMG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridEMContractMG.Location = new System.Drawing.Point(0, 0);
            this.uGridEMContractMG.Name = "uGridEMContractMG";
            this.uGridEMContractMG.Size = new System.Drawing.Size(855, 558);
            this.uGridEMContractMG.TabIndex = 1;
            this.uGridEMContractMG.Text = "ultraGrid1";
            this.uGridEMContractMG.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEMContractMG_AfterCellUpdate_1);
            this.uGridEMContractMG.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEMContractMG_CellChange);
            this.uGridEMContractMG.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.uGridEMContractMG_Error);
            this.uGridEMContractMG.SummaryValueChanged += new Infragistics.Win.UltraWinGrid.SummaryValueChangedEventHandler(this.uGridEMContractMG_SummaryValueChanged);
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.btnAdd);
            this.ultraTabPageControl4.Controls.Add(this.btnDelete);
            this.ultraTabPageControl4.Controls.Add(this.btnEdit);
            this.ultraTabPageControl4.Controls.Add(this.uGridEMContractAttachment);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(855, 558);
            // 
            // btnAdd
            // 
            appearance217.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.btnAdd.Appearance = appearance217;
            this.btnAdd.Location = new System.Drawing.Point(596, 500);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 30);
            this.btnAdd.TabIndex = 14;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            appearance218.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.btnDelete.Appearance = appearance218;
            this.btnDelete.Location = new System.Drawing.Point(764, 500);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 13;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            appearance219.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.btnEdit.Appearance = appearance219;
            this.btnEdit.Location = new System.Drawing.Point(680, 500);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 30);
            this.btnEdit.TabIndex = 12;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // uGridEMContractAttachment
            // 
            this.uGridEMContractAttachment.ContextMenuStrip = this.contextMenuStrip1;
            appearance220.BackColor = System.Drawing.SystemColors.Window;
            appearance220.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEMContractAttachment.DisplayLayout.Appearance = appearance220;
            this.uGridEMContractAttachment.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridEMContractAttachment.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEMContractAttachment.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance221.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance221.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance221.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance221.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEMContractAttachment.DisplayLayout.GroupByBox.Appearance = appearance221;
            appearance222.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEMContractAttachment.DisplayLayout.GroupByBox.BandLabelAppearance = appearance222;
            this.uGridEMContractAttachment.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance223.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance223.BackColor2 = System.Drawing.SystemColors.Control;
            appearance223.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance223.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEMContractAttachment.DisplayLayout.GroupByBox.PromptAppearance = appearance223;
            appearance224.BackColor = System.Drawing.SystemColors.Window;
            appearance224.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEMContractAttachment.DisplayLayout.Override.ActiveCellAppearance = appearance224;
            appearance225.BackColor = System.Drawing.SystemColors.Highlight;
            appearance225.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEMContractAttachment.DisplayLayout.Override.ActiveRowAppearance = appearance225;
            this.uGridEMContractAttachment.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEMContractAttachment.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance226.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEMContractAttachment.DisplayLayout.Override.CardAreaAppearance = appearance226;
            appearance227.BorderColor = System.Drawing.Color.Silver;
            appearance227.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEMContractAttachment.DisplayLayout.Override.CellAppearance = appearance227;
            this.uGridEMContractAttachment.DisplayLayout.Override.CellPadding = 0;
            appearance228.BackColor = System.Drawing.SystemColors.Control;
            appearance228.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance228.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance228.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance228.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEMContractAttachment.DisplayLayout.Override.GroupByRowAppearance = appearance228;
            appearance229.TextHAlignAsString = "Left";
            this.uGridEMContractAttachment.DisplayLayout.Override.HeaderAppearance = appearance229;
            this.uGridEMContractAttachment.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance230.BackColor = System.Drawing.SystemColors.Window;
            appearance230.BorderColor = System.Drawing.Color.Silver;
            this.uGridEMContractAttachment.DisplayLayout.Override.RowAppearance = appearance230;
            appearance231.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEMContractAttachment.DisplayLayout.Override.TemplateAddRowAppearance = appearance231;
            this.uGridEMContractAttachment.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridEMContractAttachment.Location = new System.Drawing.Point(0, 0);
            this.uGridEMContractAttachment.Name = "uGridEMContractAttachment";
            this.uGridEMContractAttachment.Size = new System.Drawing.Size(855, 494);
            this.uGridEMContractAttachment.TabIndex = 0;
            this.uGridEMContractAttachment.Text = "ultraGrid5";
            // 
            // optContract
            // 
            valueListItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem1.DataValue = true;
            valueListItem1.DisplayText = "Hợp đồng";
            valueListItem1.Tag = "HD";
            valueListItem2.DataValue = false;
            valueListItem2.DisplayText = "Dự án";
            valueListItem2.Tag = "DA";
            this.optContract.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.optContract.Location = new System.Drawing.Point(1, 4);
            this.optContract.Name = "optContract";
            this.optContract.Size = new System.Drawing.Size(295, 17);
            this.optContract.TabIndex = 22;
            this.optContract.ValueChanged += new System.EventHandler(this.optContract_ValueChanged);
            // 
            // btnClose
            // 
            appearance232.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance232;
            this.btnClose.Location = new System.Drawing.Point(753, 613);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(81, 36);
            this.btnClose.TabIndex = 57;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance233.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance233;
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(666, 613);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(81, 36);
            this.btnSave.TabIndex = 56;
            this.btnSave.Text = "Lưu";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // utEMContractSale
            // 
            this.utEMContractSale.Controls.Add(this.ultraTabSharedControlsPage1);
            this.utEMContractSale.Controls.Add(this.ultraTabPageControl1);
            this.utEMContractSale.Controls.Add(this.ultraTabPageControl2);
            this.utEMContractSale.Controls.Add(this.ultraTabPageControl3);
            this.utEMContractSale.Controls.Add(this.ultraTabPageControl4);
            this.utEMContractSale.Location = new System.Drawing.Point(1, 27);
            this.utEMContractSale.Name = "utEMContractSale";
            this.utEMContractSale.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.utEMContractSale.Size = new System.Drawing.Size(857, 581);
            this.utEMContractSale.TabIndex = 0;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "&1. Thông tin chung";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "&2. Dự toán";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "&3. Hàng hóa, dịch vụ";
            ultraTab4.TabPage = this.ultraTabPageControl4;
            ultraTab4.Text = "&4.  Tài liệu đính kèm";
            this.utEMContractSale.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3,
            ultraTab4});
            this.utEMContractSale.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(855, 558);
            // 
            // btnSua
            // 
            appearance234.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.btnSua.Appearance = appearance234;
            this.btnSua.Location = new System.Drawing.Point(491, 613);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(81, 36);
            this.btnSua.TabIndex = 58;
            this.btnSua.Text = "Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnGhidoanhso
            // 
            appearance235.Image = global::Accounting.Properties.Resources.ubtnPost;
            this.btnGhidoanhso.Appearance = appearance235;
            this.btnGhidoanhso.Location = new System.Drawing.Point(404, 613);
            this.btnGhidoanhso.Name = "btnGhidoanhso";
            this.btnGhidoanhso.Size = new System.Drawing.Size(81, 36);
            this.btnGhidoanhso.TabIndex = 59;
            this.btnGhidoanhso.Text = "Ghi doanh số";
            this.btnGhidoanhso.Click += new System.EventHandler(this.btnGhidoanhso_Click);
            // 
            // btnBoghidoanhso
            // 
            appearance236.Image = global::Accounting.Properties.Resources.ubtnPost;
            this.btnBoghidoanhso.Appearance = appearance236;
            this.btnBoghidoanhso.Location = new System.Drawing.Point(404, 614);
            this.btnBoghidoanhso.Name = "btnBoghidoanhso";
            this.btnBoghidoanhso.Size = new System.Drawing.Size(81, 36);
            this.btnBoghidoanhso.TabIndex = 60;
            this.btnBoghidoanhso.Text = "Bỏ ghi doanh số";
            this.btnBoghidoanhso.Click += new System.EventHandler(this.btnBoghidoanhso_Click);
            // 
            // btnXoa
            // 
            appearance237.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.btnXoa.Appearance = appearance237;
            this.btnXoa.Location = new System.Drawing.Point(579, 613);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(81, 36);
            this.btnXoa.TabIndex = 61;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // FEMContractSaleDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 655);
            this.Controls.Add(this.btnXoa);
            this.Controls.Add(this.btnBoghidoanhso);
            this.Controls.Add(this.btnGhidoanhso);
            this.Controls.Add(this.btnSua);
            this.Controls.Add(this.optContract);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.utEMContractSale);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FEMContractSaleDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thêm mới hợp đồng bán";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FEMContractSaleDetail_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FEMContractSaleDetail_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkIsWatchForCostPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtClosedReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClosedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignerTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbContractState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsBillPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectSignerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbOrderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliverDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbContractEmployeeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDepartmentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrencyID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMContractCode)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).EndInit();
            this.ultraGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridChungTuChi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.ultraGroupBox5.ResumeLayout(false);
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridChungTuThu)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridChi)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ultraTabPageControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridEMContractMG)).EndInit();
            this.ultraTabPageControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridEMContractAttachment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optContract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.utEMContractSale)).EndInit();
            this.utEMContractSale.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinTabControl.UltraTabControl utEMContractSale;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtEMContractCode;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCurrencyID;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtDeliverDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtSignedDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel lblGTHD;
        private Infragistics.Win.Misc.UltraLabel lblGTHDQD;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbContractEmployeeID;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDepartmentID;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectFax;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectTel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectBankName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectSignerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectTitle;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSignerTitle;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSignerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel29;
        private Infragistics.Win.Misc.UltraLabel ultraLabel28;
        private Infragistics.Win.Misc.UltraLabel ultraLabel27;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtClosedDate;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbContractState;
        private Infragistics.Win.Misc.UltraLabel ultraLabel30;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsWatchForCostPrice;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsBillPaid;
        private Infragistics.Win.Misc.UltraLabel ultraLabel26;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor4;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel31;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridChi;
        private Infragistics.Win.Misc.UltraButton btnChungTuThu;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridChungTuThu;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox6;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridChungTuChi;
        private Infragistics.Win.Misc.UltraButton btnChungTuChi;
        private Infragistics.Win.Misc.UltraLabel ultraLabel32;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEMContractAttachment;
        private Infragistics.Win.Misc.UltraButton btnAdd;
        private Infragistics.Win.Misc.UltraButton btnDelete;
        private Infragistics.Win.Misc.UltraButton btnEdit;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtName;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtExchangeRate;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAmountOriginal;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAmount;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAmountBeforCancel;
        private Infragistics.Win.Misc.UltraLabel ultraLabel33;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtClosedReason;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbOrderID;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet optContract;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDA;
        private Infragistics.Win.Misc.UltraLabel lblDA;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectBankAccount;
        private Infragistics.Win.Misc.UltraLabel lblToDate1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel35;
        private Infragistics.Win.Misc.UltraLabel lblFromDate1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel lblToDate;
        private Infragistics.Win.Misc.UltraLabel lblFromDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtDueDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEMContractMG;
        private Infragistics.Win.Misc.UltraButton btnSua;
        private Infragistics.Win.Misc.UltraButton btnGhidoanhso;
        private Infragistics.Win.Misc.UltraButton btnBoghidoanhso;
        private Infragistics.Win.Misc.UltraButton btnXoa;
    }
}