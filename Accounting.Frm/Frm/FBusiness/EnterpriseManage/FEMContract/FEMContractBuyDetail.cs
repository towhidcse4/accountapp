﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FEMContractBuyDetail : CustormForm
    {
        public readonly IAccountingObjectService _IAccountingObjectService;
        public readonly IContractStateService _IContractStateService;
        public readonly ICurrencyService _ICurrencyService;
        public readonly ICostSetService _ICostSetService;
        public readonly IEMContractService _IEmContractService;
        public readonly IEMContractAttachmentService _IEMContractAttachmentService;
        public readonly IGenCodeService _IGenCodeService;
        public readonly IAccountingObjectBankAccountService _IAccountingObjectBankAccountService;
        public readonly IPPOrderService _IPPOrderService;
        public readonly IPPOrderDetailService _IPPOrderDetailService;
        public readonly IEMContractDetailMGService _IEMContractDetailMGService;
        public readonly IMaterialGoodsService _IMaterialGoodsService;
        public readonly IPPInvoiceDetailService _IPPInvoiceDetailService;
        public readonly ISAOrderDetailService _ISAOrderDetailService;
        public readonly IPPDiscountReturnDetailService _IPPDiscountReturnDetailService;
        private List<EMContractAttachment> listEMContractAttachment = new List<EMContractAttachment>();
        private List<EMContractDetailMG> listEMContractDetailMG = new List<EMContractDetailMG>();
        private List<PPOrder> listPPOrder = new List<PPOrder>();
        public static bool isclose = true;
        private EMContract _select;
        private bool them = true;
        public decimal amountTemp;
        public decimal amountOriTemp;
        private bool check1 = false;
        private bool check2 = false;
        List<string> err = new List<string>();
        private Guid _id;
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public FEMContractBuyDetail()
        {
            InitializeComponent();
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            _IContractStateService = IoC.Resolve<IContractStateService>();
            _ICurrencyService = IoC.Resolve<ICurrencyService>();
            _ICostSetService = IoC.Resolve<ICostSetService>();
            _IEmContractService = IoC.Resolve<IEMContractService>();
            _IEMContractAttachmentService = IoC.Resolve<IEMContractAttachmentService>();
            _IGenCodeService = IoC.Resolve<IGenCodeService>();
            _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            _IPPOrderService = IoC.Resolve<IPPOrderService>();
            _IPPOrderDetailService = IoC.Resolve<IPPOrderDetailService>();
            _IEMContractDetailMGService = IoC.Resolve<IEMContractDetailMGService>();
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            _IPPInvoiceDetailService = IoC.Resolve<IPPInvoiceDetailService>();
            _IPPDiscountReturnDetailService = IoC.Resolve<IPPDiscountReturnDetailService>();
            InitializeGUI();
            this.Text = "Thêm mới hợp đồng mua";
            ViewGirdAddData();
            them = true;
            check1 = false;
            check2 = false;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
            txtClosedDate.Enabled = false;
            txtClosedReason.Enabled = false;
            txtEMContractCode.Text = Utils.TaoMaChungTu(_IGenCodeService.getGenCode(85));
            cbbPPOrder.DataSource = _IPPOrderService.GetAllOrderByNoAndNotInContract(Guid.Empty);
            Utils.ConfigGrid(cbbPPOrder, ConstDatabase.PPOrder_TableName);
            UltraGridColumn c = cbbPPOrder.DisplayLayout.Bands[0].Columns.Add();
            if (c.Key == "")
                c.Key = "Selected";
            c.Header.Caption = string.Empty;
            c.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;
            c.DataType = typeof(bool);
            c.Header.VisiblePosition = 0;
            cbbPPOrder.CheckedListSettings.ListSeparator = "/";
            cbbPPOrder.CheckedListSettings.ItemCheckArea = ItemCheckArea.Item;
            cbbPPOrder.CheckedListSettings.CheckStateMember = "Selected";
            cbbPPOrder.CheckedListSettings.EditorValueSource = EditorWithComboValueSource.CheckedItems;
            cbbPPOrder.ValueMember = "ID";
            cbbPPOrder.DisplayMember = "No";
        }

        public FEMContractBuyDetail(EMContract temp)
        {
            InitializeComponent();
            _IContractStateService = IoC.Resolve<IContractStateService>();
            _IEmContractService = IoC.Resolve<IEMContractService>();
            _IEMContractAttachmentService = IoC.Resolve<IEMContractAttachmentService>();
            _IGenCodeService = IoC.Resolve<IGenCodeService>();
            _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            _IPPOrderService = IoC.Resolve<IPPOrderService>();
            _IPPOrderDetailService = IoC.Resolve<IPPOrderDetailService>();
            _IEMContractDetailMGService = IoC.Resolve<IEMContractDetailMGService>();
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            _IPPInvoiceDetailService = IoC.Resolve<IPPInvoiceDetailService>();
            _ISAOrderDetailService = IoC.Resolve<ISAOrderDetailService>();
            _IPPDiscountReturnDetailService = IoC.Resolve<IPPDiscountReturnDetailService>();
            _select = temp;
            cbbPPOrder.DataSource = _IPPOrderService.GetOrderByContract(_select.ID);
            Utils.ConfigGrid(cbbPPOrder, ConstDatabase.PPOrder_TableName);
            UltraGridColumn c = cbbPPOrder.DisplayLayout.Bands[0].Columns.Add();
            if (c.Key == "")
                c.Key = "Selected";
            c.Header.Caption = string.Empty;
            c.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;
            c.DataType = typeof(bool);
            c.Header.VisiblePosition = 0;
            cbbPPOrder.CheckedListSettings.ListSeparator = "/";
            cbbPPOrder.CheckedListSettings.ItemCheckArea = ItemCheckArea.Item;
            cbbPPOrder.CheckedListSettings.CheckStateMember = "Selected";
            cbbPPOrder.CheckedListSettings.EditorValueSource = EditorWithComboValueSource.CheckedItems;
            cbbPPOrder.ValueMember = "ID";
            cbbPPOrder.DisplayMember = "No";
            //var listPPOrderDetail = _IPPOrderDetailService.GetPPOrderDetailbyID(temp.ID);
            listEMContractAttachment = _IEMContractAttachmentService.GetEMContractAttachmentByContractID(temp.ID);
            listEMContractDetailMG = _IEMContractDetailMGService.GetEMContractDetailMGByContractID(temp.ID);
            _IEMContractDetailMGService.UnbindSession(listEMContractDetailMG);
            listEMContractDetailMG = _IEMContractDetailMGService.GetEMContractDetailMGByContractID(temp.ID);
            check1 = false;
            check2 = false;
            txtEMContractCode.Enabled = false;
            this.Text = "Hợp đồng mua";
            ViewGirdAddData();
            them = false;
            //if (chkIsWatchForCostPrice.Checked == true)
            //{
            //    lblCostset.Visible = true;
            //    cbbCostSet.Visible = true;
            //}
            //else
            //{
            //    lblCostset.Visible = false;
            //    cbbCostSet.Visible = false;
            //}

            InitializeGUI();

            ObjandGUI(temp, false);
        }

        private void ViewGirdAddData()
        {
            uGridDk.DataSource = listEMContractAttachment;
            Utils.ConfigGrid(uGridDk, TextMessage.ConstDatabase.EMContractAttachment_TableName);
        }

        private void InitializeGUI()
        {
            cbbAccountingObjectBankAccount.DataSource = new List<AccountingObjectBankAccount>();
            Utils.ConfigGrid(cbbAccountingObjectBankAccount, ConstDatabase.AccountingObjectBankAccount_TableName);
            cbbAccountingObjectBankAccount.DisplayMember = "BankAccount";
            cbbAccountingObjectBankAccount.ValueMember = "ID";
            cbbAccountingObjectBankAccount.DropDownStyle = UltraComboStyle.DropDownList;

            cbbAccountingObjectID.DataSource = Utils.ListAccountingObject.Where(x => (x.ObjectType == 1 || x.ObjectType == 2) && x.IsActive).ToList();
            Utils.ConfigGrid(cbbAccountingObjectID, ConstDatabase.AccountingObject_TableName);
            cbbAccountingObjectID.ValueMember = "ID";
            cbbAccountingObjectID.DisplayMember = "AccountingObjectCode";

            cbbContractState.DataSource = _IContractStateService.GetAll();
            Utils.ConfigGrid(cbbContractState, ConstDatabase.ContractState_TableName);
            cbbContractState.ValueMember = "ID";
            cbbContractState.DisplayMember = "ContractStateName";

            cbbCurrencyID.DataSource = Utils.ListCurrency;
            Utils.ConfigGrid(cbbCurrencyID, ConstDatabase.Currency_TableName);
            cbbCurrencyID.ValueMember = "ID";

            txtExchangeRate.FormatNumberic(ConstDatabase.Format_Rate);
            txtAmountOriginal.FormatNumberic(ConstDatabase.Format_TienVND);
            txtAmount.FormatNumberic(ConstDatabase.Format_TienVND);


            uGridEMContractMG.DataSource = new BindingList<EMContractDetailMG>(listEMContractDetailMG);
            UltraGridBand band1 = uGridEMContractMG.DisplayLayout.Bands[0];
            foreach (UltraGridColumn column in band1.Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGridEMContractMG);
            }
            Utils.ConfigGrid(uGridEMContractMG, ConstDatabase.EMContractDetailBuyMG_TableName, new List<TemplateColumn>(), isBusiness: true);
            //select cả hàng hay ko? add by cuongpv
            uGridEMContractMG.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            //uGridEMContractMG.DisplayLayout.Override.AllowUpdate = DefaultableBoolean.True;
            band1.Columns["MaterialGoodsID"].CellClickAction = CellClickAction.Edit;
            band1.Columns["Quantity"].CellClickAction = CellClickAction.Edit;
            band1.Columns["UnitPriceOriginal"].CellClickAction = CellClickAction.Edit;
            band1.Columns["DiscountRate"].CellClickAction = CellClickAction.Edit;
            band1.Columns["VATRate"].CellClickAction = CellClickAction.Edit;
            band1.Columns["TotalAmountOriginal"].CellClickAction = CellClickAction.Edit;
            band1.Columns["UnitPrice"].CellClickAction = CellClickAction.Edit;
            band1.Columns["DiscountRate"].CellClickAction = CellClickAction.Edit;
            band1.Columns["VATRate"].CellClickAction = CellClickAction.Edit;
            band1.Columns["TotalAmount"].CellClickAction = CellClickAction.Edit;
            band1.Columns["DiscountAmount"].CellClickAction = CellClickAction.Edit;
            band1.Columns["DiscountAmountOriginal"].CellClickAction = CellClickAction.Edit;
            band1.Columns["VATAmount"].CellClickAction = CellClickAction.Edit;
            band1.Columns["VATAmountOriginal"].CellClickAction = CellClickAction.Edit;
            //end add by cuongpv
            band1.Summaries.Clear();
            band1.Columns["PPOrderCode"].CellActivation = Activation.NoEdit;
            band1.Columns["QuantityReceipt"].CellActivation = Activation.NoEdit;
            //band1.Columns["SAOrderID"].Hidden = true;
            //band1.Columns["PPOrderID"].Hidden = true;
            //band1.Columns["SAOrderCode"].Hidden = true;
            //band1.Columns["MaterialGoodsCode"].Hidden = true;
            foreach (UltraGridRow row in uGridEMContractMG.Rows)
            {
                if (!them && (decimal)row.Cells["QuantityReceipt"].Value > 0)
                    row.Cells["MaterialGoodsID"].Activation = Activation.NoEdit;
            }
            SummarySettings summary = band1.Summaries.Add("Count", SummaryType.Count, band1.Columns["MaterialGoodsID"]);
            summary.DisplayFormat = "Số dòng = {0:N0}";
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            uGridEMContractMG.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False;
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            uGridEMContractMG.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            uGridEMContractMG.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
            uGridEMContractMG.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;

            summary = band1.Summaries.Add("sumAmount", SummaryType.Sum, band1.Columns["TotalAmount"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            band1.Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridEMContractMG.Rows.SummaryValues["sumAmount"].Appearance.TextHAlign = HAlign.Right;

            summary = band1.Summaries.Add("sumDiscountAmount", SummaryType.Sum, band1.Columns["DiscountAmount"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            band1.Columns["DiscountAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridEMContractMG.Rows.SummaryValues["sumDiscountAmount"].Appearance.TextHAlign = HAlign.Right;

            summary = band1.Summaries.Add("sumVATAmount", SummaryType.Sum, band1.Columns["VATAmount"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            band1.Columns["VATAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridEMContractMG.Rows.SummaryValues["sumVATAmount"].Appearance.TextHAlign = HAlign.Right;

            summary = band1.Summaries.Add("sumAmountOriginal", SummaryType.Sum, band1.Columns["TotalAmountOriginal"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            band1.Columns["TotalAmountOriginal"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridEMContractMG.Rows.SummaryValues["sumAmountOriginal"].Appearance.TextHAlign = HAlign.Right;

            summary = band1.Summaries.Add("sumDiscountAmountOriginal", SummaryType.Sum, band1.Columns["DiscountAmountOriginal"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            band1.Columns["DiscountAmountOriginal"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridEMContractMG.Rows.SummaryValues["sumDiscountAmountOriginal"].Appearance.TextHAlign = HAlign.Right;

            summary = band1.Summaries.Add("sumVATAmountOriginal", SummaryType.Sum, band1.Columns["VATAmountOriginal"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            band1.Columns["VATAmountOriginal"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridEMContractMG.Rows.SummaryValues["sumVATAmountOriginal"].Appearance.TextHAlign = HAlign.Right;

            cbbAccountingObjectBankAccount.DropDownStyle = UltraComboStyle.DropDownList;
        }

        #region event

        private void cbbAccountingObjectID_RowSelected(object sender, RowSelectedEventArgs e)
        {
            List<int> type = new List<int>() { 1, 2 };
            cbbAccountingObjectID.UpdateData();
            if (e.Row == null) return;
            if (e.Row.ListObject == null) return;

            AccountingObject accountingObject = e.Row.ListObject as AccountingObject;
            txtAccountingObjectName.Text = accountingObject.AccountingObjectName;
            txtAccountingObjectTitle.Text = accountingObject.ContactName;
            txtAccountingObjectSignerName.Text = accountingObject.ContactTitle;
            txtAccountingObjectAddress.Text = accountingObject.Address;
            txtAccountingObjectTel.Text = accountingObject.ContactMobile;
            txtAccountingObjectFax.Text = accountingObject.Fax;
            cbbAccountingObjectBankAccount.DataSource = _IAccountingObjectBankAccountService.GetByAccountingObjectType((Guid)(cbbAccountingObjectID.Value), type);
            check2 = true;
        }
        private void cbbAccountingObjectID_AfterCloseUp(object sender, EventArgs e)
        {
            if (check2)
            {
                check2 = false;
                if (cbbAccountingObjectID.Text == "") return;
                AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(x => x.ID == (Guid)cbbAccountingObjectID.Value);
                if (cbbPPOrder.Text == "") cbbPPOrder.DataSource = _IPPOrderService.GetAllOrderByNoAndNotInContract((Guid)cbbAccountingObjectID.Value);
                else
                {
                    foreach (UltraGridRow r in cbbPPOrder.CheckedRows)
                    {
                        var model = (PPOrder)r.ListObject;
                        if (model.AccountingObjectID != accountingObject.ID)
                        {
                            MSG.Warning("Bạn phải chọn số đơn hàng thuộc cùng nhà cung cấp!!");
                            return;
                        }
                    }
                }
            }
        }
        //private void ultraCheckEditor1_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkIsWatchForCostPrice.Checked == false)
        //    {
        //        lblCostset.Visible = false;
        //        cbbCostSet.Visible = false;
        //        cbbCostSet.ResetText();
        //    }
        //    else
        //    {
        //        lblCostset.Visible = true;
        //        cbbCostSet.Visible = true;
        //    }
        //}

        private void cbbMoney_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null)
            {
                cbbCurrencyID.Value = "VND";
            }
            if (e.Row != null)
            {
                Currency temp = e.Row.ListObject as Currency;
                if (temp.ID != "VND") txtExchangeRate.Value = temp.ExchangeRate;
                if (temp.ID == "VND") txtExchangeRate.Value = 1;
                UltraGridBand band1 = uGridEMContractMG.DisplayLayout.Bands[0];
                band1.Columns["UnitPrice"].Hidden = temp.ID == "VND";
                band1.Columns["TotalAmount"].Hidden = temp.ID == "VND";
                band1.Columns["DiscountAmount"].Hidden = temp.ID == "VND";
                band1.Columns["VATAmount"].Hidden = temp.ID == "VND";
                band1.Columns["UnitPriceOriginal"].FormatNumberic(temp.ID == "VND" ? ConstDatabase.Format_DonGiaQuyDoi : ConstDatabase.Format_DonGiaNgoaiTe);
                band1.Columns["TotalAmountOriginal"].FormatNumberic(temp.ID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
                band1.Columns["DiscountAmountOriginal"].FormatNumberic(temp.ID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
                band1.Columns["VATAmountOriginal"].FormatNumberic(temp.ID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
                foreach (var row in uGridEMContractMG.Rows)
                {
                    row.Cells["UnitPrice"].Value = (decimal)row.Cells["UnitPriceOriginal"].Value * temp.ExchangeRate;
                    row.Cells["TotalAmount"].Value = (decimal)row.Cells["TotalAmountOriginal"].Value * temp.ExchangeRate;
                    row.Cells["DiscountAmount"].Value = (decimal)row.Cells["DiscountAmountOriginal"].Value * temp.ExchangeRate;
                    row.Cells["VATAmount"].Value = (decimal)row.Cells["VATAmountOriginal"].Value * temp.ExchangeRate;
                }
                txtAmountOriginal.FormatNumberic(temp.ID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            }
        }

        private void cbbMoney_TextChanged(object sender, EventArgs e)
        {
            if (cbbCurrencyID.Text != "VND")
            {
                lblGiaTriQĐ.Visible = true;
                lblRate.Visible = true;
                txtExchangeRate.Visible = true;
                txtAmount.Visible = true;
            }
            else
            {
                lblGiaTriQĐ.Visible = false;
                lblRate.Visible = false;
                txtExchangeRate.Visible = false;
                txtAmount.Visible = false;
                txtExchangeRate.Value = 1;
            }

            if ((txtAmountOriginal.Value != null) && (txtAmountOriginal.Text.Trim() != ""))
            {
                decimal rateEchangeTemp;
                decimal orgTemp;
                if ((txtExchangeRate.Value != null) && (txtExchangeRate.Text.Trim() != ""))
                {
                    rateEchangeTemp = Convert.ToDecimal(txtExchangeRate.Value);
                    orgTemp = Convert.ToDecimal(txtAmountOriginal.Value);
                    txtAmount.Value = rateEchangeTemp * orgTemp;
                    amountOriTemp = orgTemp;
                }
            }
        }

        private void txtRateEchange_TextChanged(object sender, EventArgs e)
        {
            if (txtExchangeRate.Value != null)
            {
                txtAmount.Value = ((txtExchangeRate.Value as decimal?) ?? 0) * ((txtAmountOriginal.Value as decimal?) ?? 0);
            }
        }

        private void txtOrg_TextChanged(object sender, EventArgs e)
        {
            if ((txtAmountOriginal.Value != null) && (txtAmountOriginal.Text.Trim() != ""))
            {
                decimal rateEchangeTemp;
                decimal orgTemp;
                if ((txtExchangeRate.Value != null) && (txtExchangeRate.Text.Trim() != ""))
                {
                    rateEchangeTemp = Convert.ToDecimal(txtExchangeRate.Value);
                    orgTemp = Convert.ToDecimal(txtAmountOriginal.Value);
                    txtAmount.Value = rateEchangeTemp * orgTemp;
                    amountOriTemp = orgTemp;
                }
            }
        }

        private void cbbAccountingObjectID_EditorButtonClick(object sender,
            Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            new FAccountingObjectCustomersDetail().ShowDialog(this);
            cbbAccountingObjectID.DataSource = Utils.ListAccountingObject.Where(x => (x.ObjectType == 1 || x.ObjectType == 2) && x.IsActive).ToList();
            Utils.ConfigGrid(cbbAccountingObjectID, ConstDatabase.AccountingObject_TableName);
            cbbAccountingObjectID.ValueMember = "ID";
            cbbAccountingObjectID.DisplayMember = "AccountingObjectCode";
            Utils.ClearCacheByType<AccountingObject>();
            cbbAccountingObjectID.SelectedText = FAccountingObjectCustomersDetail.AccounttingObjectCode;
        }

        private void txtMobile_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void txtFax_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        #endregion event

        #region event EMContractAttachment

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddData frm = new AddData();
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.ShowDialog(this);
            if (frm.isclose)
            {
                listEMContractAttachment.Add(frm.input);
                uGridDk.DataSource = listEMContractAttachment.ToArray();
                Utils.ConfigGrid(uGridDk, TextMessage.ConstDatabase.EMContractAttachment_TableName);
                checkgridDK();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (uGridDk.Selected.Rows.Count > 0)
            {
                EMContractAttachment temp = uGridDk.Selected.Rows[0].ListObject as EMContractAttachment;
                listEMContractAttachment.Remove(temp);
                AddData frm = new AddData(temp);
                frm.ShowDialog(this);
                listEMContractAttachment.Add(frm.input);
                uGridDk.DataSource = listEMContractAttachment.ToArray();
                uGridDk.Update();
                checkgridDK();
            }
            else
            {
                MSG.Warning("Bạn chưa chọn bản ghi nào để sửa");
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (uGridDk.Selected.Rows.Count > 0)
            {
                EMContractAttachment temp = uGridDk.Selected.Rows[0].ListObject as EMContractAttachment;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, "tệp đính kèm")) == DialogResult.Yes)
                {
                    listEMContractAttachment.Remove(temp);
                    uGridDk.DataSource = listEMContractAttachment.ToArray();
                    uGridDk.Update();
                    checkgridDK();
                }
            }
            else
            {
                MSG.Warning("Bạn chưa chọn bản ghi nào để xóa");
            }
        }

        #endregion event EMContractAttachment

        #region template

        //private List<TemplateColumn> GetTemplateColumns()
        //{
        //    return new List<TemplateColumn>
        //    {
        //        new TemplateColumn
        //        {
        //            IsVisibleCbb = false,
        //            IsReadOnly = true,
        //            ColumnWidth = 100,
        //            ColumnName = "NameFile",
        //            ColumnCaption = "Tên tài liệu"
        //        },
        //          new TemplateColumn
        //        {
        //            IsVisibleCbb = false,
        //            IsReadOnly = true,
        //            ColumnWidth = 100,
        //            ColumnName = "SizeFile",
        //            ColumnCaption = "Kích thước"
        //        },
        //          new TemplateColumn
        //        {
        //            IsVisibleCbb = false,
        //            IsReadOnly = true,
        //            ColumnWidth = 100,
        //            ColumnName = "FormatFile",
        //            ColumnCaption = "Định dạng tệp"
        //        },
        //           new TemplateColumn
        //        {
        //            IsVisibleCbb = false,
        //            IsReadOnly = true,
        //            ColumnWidth = 100,
        //            ColumnName = "Depcription",
        //            ColumnCaption = "Diễn giải"
        //        },
        //      };
        //}

        #endregion template

        private void Edit(EMContract temp)
        {
            _select = temp;
            them = false;
            ObjandGUI(temp, false);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cbbContractState_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            new FContractStateDetail().ShowDialog(this);
            cbbContractState.DataSource = _IContractStateService.GetAll();
            Utils.ConfigGrid(cbbContractState, ConstDatabase.ContractState_TableName);
            cbbContractState.ValueMember = "ID";
            cbbContractState.DisplayMember = "ContractStateName";
            Utils.ClearCacheByType<ContractState>();
            cbbContractState.SelectedRow = cbbContractState.Rows.Last();
        }

        //private void cbbCostSet_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        //{
        //    new FCostSetDetail().ShowDialog(this);
        //    cbbCostSet.DataSource = _ICostSetService.GetAll();
        //    Utils.ConfigGrid(cbbCostSet, ConstDatabase.CostSet_TableName);
        //    cbbCostSet.ValueMember = "ID";
        //    cbbCostSet.DisplayMember = "CostSetCode";
        //    Utils.ClearCacheByType<CostSet>();
        //    cbbCostSet.SelectedText = FCostSetDetail.CostSetCode;
        //}

        private EMContract ObjandGUI(EMContract input, bool isGet)
        {
            if (isGet == true)
            {
                if (input.ID == null || input.ID == Guid.Empty)  //Thêm mới
                    input.ID = Guid.NewGuid();
                input.Code = txtEMContractCode.Text;
                if (txtSignedDate.Value != null)
                {
                    input.SignedDate = DateTime.Parse(txtSignedDate.Value.ToString());
                }
                if (txtEffectiveDate.Value != null)
                {
                    input.EffectiveDate = DateTime.Parse(txtEffectiveDate.Value.ToString());
                }
                input.SignerName = txtSignerName.Text;
                input.SignerTitle = txtSignerTitle.Text;
                input.CurrencyID = (string)(cbbCurrencyID.Value);
                if ((txtExchangeRate.Value != null) && (txtExchangeRate.Text.Trim() != ""))
                {
                    input.ExchangeRate = decimal.Parse(txtExchangeRate.Value.ToString());
                }
                if ((txtAmountOriginal.Value != null) && (txtAmountOriginal.Text.Trim() != ""))
                {
                    input.AmountOriginal = decimal.Parse(txtAmountOriginal.Value.ToString());
                }
                if (txtAmount.Value != null)
                {
                    input.Amount = decimal.Parse(txtAmount.Value.ToString());
                }

                AccountingObject accountingObject = (AccountingObject)Utils.getSelectCbbItem(cbbAccountingObjectID);
                if (accountingObject == null) input.AccountingObjectID = null;
                else input.AccountingObjectID = accountingObject.ID;

                input.AccountingObjectName = txtAccountingObjectName.Text;
                input.AccountingObjectTitle = txtAccountingObjectTitle.Text;
                input.AccountingObjectSignerName = txtAccountingObjectSignerName.Text;
                input.AccountingObjectAddress = txtAccountingObjectAddress.Text;
                input.AccountingObjectTel = txtAccountingObjectTel.Text;
                input.AccountingObjectFax = txtAccountingObjectFax.Text;
                input.AccountingObjectBankAccountDetailID = (Guid?)cbbAccountingObjectBankAccount.Value;
                input.AccountingObjectBankName = txtAccountingObjectBankName.Text;
                input.Name = txtName.Text;

                ContractState contractState = (ContractState)Utils.getSelectCbbItem(cbbContractState);
                if (contractState == null) input.ContractState = 0;
                else input.ContractState = contractState.ID;

                if (txtClosedDate.Value != null)
                {
                    input.ClosedDate = DateTime.Parse(txtClosedDate.Value.ToString());
                }
                input.ClosedReason = txtClosedReason.Text;
                //input.IsWatchForCostPrice = chkIsWatchForCostPrice.Checked;
                //input.CostSetID = (Guid?)(cbbCostSet.Value);
                input.TypeID = 850;
                input.IsActive = true;
                input.DeliverDate = (txtDeliverDate.Value != null) ? (DateTime?)DateTime.Parse(txtDeliverDate.Value.ToString()) : null;
                input.DueDate = (txtDueDate.Value != null) ? (DateTime?)DateTime.Parse(txtDueDate.Value.ToString()) : null;
                List<string> lstID = new List<string>();
                foreach (var item in cbbPPOrder.Rows)
                {
                    PPOrder pporder = item.ListObject as PPOrder;
                    if ((bool)item.Cells["Selected"].Value == true)
                        lstID.Add(pporder.ID.ToString() + ";");

                }
                foreach (var item in lstID)
                {
                    input.OrderID += item;
                }
            }
            else
            {
                txtEMContractCode.Text = input.Code;
                if (input.SignedDate != null)
                {
                    txtSignedDate.Value = input.SignedDate;
                }
                if (input.EffectiveDate != null)
                {
                    txtEffectiveDate.Value = input.EffectiveDate;
                }
                txtSignerName.Text = input.SignerName;
                txtSignerTitle.Text = input.SignerTitle;
                foreach (var item in cbbCurrencyID.Rows)
                {
                    if ((item.ListObject as Currency).ID == input.CurrencyID) cbbCurrencyID.SelectedRow = item;
                }
                if (input.ExchangeRate != null)
                {
                    txtExchangeRate.Value = input.ExchangeRate;
                }
                if (input.AmountOriginal != null)
                {
                    txtAmountOriginal.Value = input.AmountOriginal;
                }
                if (input.Amount != null)
                {
                    txtAmount.Value = input.Amount;
                }
                cbbAccountingObjectID.Value = input.AccountingObjectID;
                txtAccountingObjectName.Text = input.AccountingObjectName;
                txtAccountingObjectTitle.Text = input.AccountingObjectTitle;
                txtAccountingObjectSignerName.Text = input.AccountingObjectSignerName;
                txtAccountingObjectAddress.Text = input.AccountingObjectAddress;
                txtAccountingObjectTel.Text = input.AccountingObjectTel;
                txtAccountingObjectFax.Text = input.AccountingObjectFax;
                cbbAccountingObjectBankAccount.Value = input.AccountingObjectBankAccountDetailID;
                txtAccountingObjectBankName.Text = input.AccountingObjectBankName;
                txtName.Text = input.Name;
                foreach (var item in cbbContractState.Rows)
                {
                    if ((item.ListObject as ContractState).ID == input.ContractState)
                    {
                        cbbContractState.SelectedRow = item;
                    }
                }
                if (input.ClosedDate != null)
                {
                    txtClosedDate.Value = input.ClosedDate;
                }
                //chkIsWatchForCostPrice.Checked = input.IsWatchForCostPrice;
                //foreach (var item in cbbCostSet.Rows)
                //{
                //    if ((item.ListObject as CostSet).ID == input.CostSetID) cbbCostSet.SelectedRow = item;
                //}
                txtDeliverDate.Value = input.DeliverDate;
                txtDueDate.Value = input.DueDate;
                if (input.OrderID != "" && input.OrderID != null)
                {
                    string[] ss = input.OrderID.Substring(0, input.OrderID.Length - 1).Split(';');
                    List<Guid> guid = new List<Guid>();
                    foreach (var item in ss)
                    {
                        guid.Add(Guid.Parse(item));
                    }
                    foreach (var item in cbbPPOrder.Rows)
                    {
                        foreach (var item2 in guid)
                        {
                            if ((item.ListObject as PPOrder).ID == item2)
                            {
                                item.Cells["Selected"].Value = true;
                            }
                        }
                    }
                }
                //cbbPPOrder.Value = input.OrderID;
                //List<Guid> lstPPOrder = _IPPOrderService.GetOrderIDByContractID(input.ID);
                //if (lstPPOrder.Count > 0)
                //{
                //    foreach (var item in cbbPPOrder.Rows)
                //    {
                //        PPOrder pporder = item.ListObject as PPOrder;
                //        for (int i = 0; i < lstPPOrder.Count; i++)
                //        {
                //            if (pporder.ID == lstPPOrder[i])
                //                item.Cells["Selected"].Value = true;
                //        }
                //    }
                //}
            }
            return input;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (checkerror() == false) return;
            EMContract temp = new EMContract();
            //temp = ObjandGUI(temp, true);
            //
            try
            {
                _IEmContractService.BeginTran();
                if (them == true)
                {
                    temp = ObjandGUI(temp, true);
                    _IEmContractService.CreateNew(temp);
                    _IGenCodeService.UpdateGenCodeCatalog(85, txtEMContractCode.Text);

                    if (cbbPPOrder.Text != "")
                    {
                        foreach (var item1 in cbbPPOrder.CheckedRows)
                        {
                            PPOrder saorder = (PPOrder)item1.ListObject;
                            List<PPOrderDetail> lstsaorderdeail = _IPPOrderDetailService.GetPPOrderDetailByPPOrderCode(saorder.ID, temp.ID);
                            if (lstsaorderdeail.Count == 0)
                                lstsaorderdeail = _IPPOrderDetailService.GetPPOrderDetailByPPOrderCode(saorder.ID, null);
                            if (lstsaorderdeail.Count > 0)
                            {
                                foreach (var item in lstsaorderdeail)
                                {
                                    item.ContractID = temp.ID;
                                    _IPPOrderDetailService.Update(item);
                                }
                            }
                        }
                    }

                    if (listEMContractAttachment.Count > 0)
                    {
                        foreach (EMContractAttachment item in listEMContractAttachment)
                        {
                            item.EMContractID = temp.ID;
                            _IEMContractAttachmentService.CreateNew(item);
                        }
                    }
                    _IEMContractDetailMGService.BeginTran();
                    if (uGridEMContractMG.Rows.Count > 0)
                    {
                        var listEMContractDetailMG2 = uGridEMContractMG.DataSource as BindingList<EMContractDetailMG>;
                        foreach (EMContractDetailMG item in listEMContractDetailMG2)
                        {
                            item.ContractID = temp.ID;
                            item.ID = Guid.NewGuid();
                            _IEMContractDetailMGService.CreateNew(item);
                        }
                    }
                    _IEMContractDetailMGService.CommitTran();
                    _select = temp;
                }
                else
                {
                    var listEmContractAttDb = _IEMContractAttachmentService.GetEMContractAttachmentByContractID(temp.ID);
                    var listEmContractMG = _IEMContractDetailMGService.GetEMContractDetailMGByContractID(temp.ID);
                    var listPPOrderDetail = _IPPOrderDetailService.GetPPOrderDetailbyContractID(temp.ID);
                    temp = ObjandGUI(_select, true);
                    if (listPPOrderDetail.Count > 0)
                    {
                        foreach (var item in listPPOrderDetail)
                        {
                            PPOrderDetail pporderdeail = _IPPOrderDetailService.Getbykey(item.ID);
                            //pporderdeail.PPOrderID = null;
                            pporderdeail.ContractID = null;
                            _IPPOrderDetailService.Update(pporderdeail);
                        }
                    }

                    if (cbbPPOrder.Text != "")
                    {
                        foreach (var item1 in cbbPPOrder.CheckedRows)
                        {
                            PPOrder saorder = (PPOrder)item1.ListObject;
                            List<PPOrderDetail> lstsaorderdeail = _IPPOrderDetailService.GetPPOrderDetailByPPOrderCode(saorder.ID, temp.ID);
                            if (lstsaorderdeail.Count == 0)
                                lstsaorderdeail = _IPPOrderDetailService.GetPPOrderDetailByPPOrderCode(saorder.ID, null);
                            if (lstsaorderdeail.Count > 0)
                            {
                                foreach (var item in lstsaorderdeail)
                                {
                                    item.ContractID = temp.ID;
                                    _IPPOrderDetailService.Update(item);
                                }
                            }
                        }
                    }

                    if (listEmContractAttDb.Count > 0)
                    {
                        foreach (var item in listEmContractAttDb)
                        {
                            _IEMContractAttachmentService.Delete(item);
                        }
                    }
                    if (listEMContractAttachment.Count > 0)
                    {
                        foreach (EMContractAttachment item in listEMContractAttachment)
                        {
                            item.EMContractID = temp.ID;
                            _IEMContractAttachmentService.CreateNew(item);
                        }
                    }
                    if (listEmContractMG.Count > 0)
                    {
                        foreach (var item in listEmContractMG)
                        {
                            _IEMContractDetailMGService.Delete(item);
                        }
                    }
                    _IEMContractDetailMGService.BeginTran();
                    if (uGridEMContractMG.Rows.Count > 0)
                    {
                        var listEMContractDetailMG2 = uGridEMContractMG.DataSource as BindingList<EMContractDetailMG>;
                        foreach (EMContractDetailMG item in listEMContractDetailMG2)
                        {
                            if (item.ContractID == null || item.ContractID == Guid.Empty)
                                item.ContractID = temp.ID;
                            if (item.ID == Guid.Empty || item.ID == null)
                                item.ID = Guid.NewGuid();
                            _IEMContractDetailMGService.CreateNew(item);
                        }
                    }
                    _IEMContractDetailMGService.CommitTran();
                    _IEmContractService.Update(temp);
                }
                _IEmContractService.CommitTran();
                MSG.Information("Lưu thành công");
                Utils.ClearCacheByType<EMContract>();
                Utils.ClearCacheByType<EMContractDetailMG>();
                _select = Utils.IEMContractService.Getbykey(_select.ID);
                _id = temp.ID;
                ObjandGUI(_select, false);

            }
            catch (Exception ex)
            {
                MSG.Error("Lưu không thành công!");
                _IEmContractService.RolbackTran();
                _IEMContractDetailMGService.RolbackTran();
            }
            //Reset();
            isclose = false;
            Close();
        }

        public virtual void Reset()
        {
            Utils.ClearCache();
            IEMContractService testInvoke = IoC.Resolve<IEMContractService>();
            PropertyInfo propId = _select.GetType().GetProperty("ID", BindingFlags.Public | BindingFlags.Instance);
            if (null != propId && propId.CanRead)
            {
                object temp = propId.GetValue(_select, null);
                if (temp is Guid)
                {
                    var currentId = (Guid)temp;
                    try
                    {
                        _select = testInvoke.Getbykey(currentId);
                        testInvoke.UnbindSession(_select);
                        _select = testInvoke.Getbykey(currentId);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(resSystem.MSG_Error_13);
                        Close();
                    }
                }
            }
        }

        public void checkgridDK()
        {
            if (uGridDk.Rows.Count > 0)
            {
                btnEdit.Enabled = true;
                btnDelete.Enabled = true;
            }
            else
            {
                btnEdit.Enabled = false;
                btnDelete.Enabled = false;
            }
        }

        private void cbbContractState_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if ((cbbContractState.Text == "Hủy bỏ") || (cbbContractState.Text == "Thanh lý") || (cbbContractState.Text == "Kết thúc"))
            {
                txtClosedDate.Enabled = true;
                txtClosedReason.Enabled = true;
            }
            else
            {
                txtClosedDate.Enabled = false;
                txtClosedReason.Enabled = false;
                txtClosedReason.ResetText();
                txtClosedDate.ResetText();
            }
        }

        private bool checkerror()
        {
            bool result = true;
            //if ((txtAmount.Value.ToInt() == 0) || (txtAmount.Text.Trim() == "0"))
            //{
            //    MSG.Warning("Không được để trống giá trị Hợp đồng");
            //    return false;
            //}
            if (txtEMContractCode.Text == "")
            {
                MSG.Warning("Không được để trống số Hợp đồng");
                txtEMContractCode.Focus();
                return false;
            }
            //if (((cbbCurrencyID.Text != "VND") && (txtAmountOriginal.Text.Trim() == "0")) || txtAmountOriginal.Text.Trim() == "0" || txtAmountOriginal.Value.ToInt() == 0)
            //{
            //    MSG.Warning("Không được để trống giá trị Hợp đồng");
            //    return false;
            //}

            if ((cbbContractState.Text == "") || (cbbContractState.Value == null))
            {
                MSG.Warning("Không được để trống tình trạng hợp đồng");
                cbbContractState.Focus();
                return false;
            }
            if (cbbAccountingObjectID.Text.Trim() == "")
            {
                MSG.Warning("Không được để trống Nhà cung cấp!");
                return false;
            }
            if (cbbAccountingObjectID.Text.Trim() != "")
            {
                List<AccountingObject> listAccountingObject = Utils.ListAccountingObject.Where(x => (x.ObjectType == 1 || x.ObjectType == 2) && x.IsActive).ToList();
                if (!listAccountingObject.Exists(x => x.AccountingObjectCode == cbbAccountingObjectID.Text.ToString()))
                {
                    MSG.Warning("Dữ liệu không có trong danh mục.Đề nghị điều chỉnh lại");
                    return false;
                }
            }

            List<ContractState> listContractState = _IContractStateService.GetAll();
            if (!listContractState.Exists(x => x.ContractStateName == cbbContractState.Text.ToString()) && cbbContractState.Text != "")
            {
                MSG.Warning("Dữ liệu không có trong danh mục. Đề nghị điều chỉnh lại");
            }

            List<EMContract> lstemcontract = _IEmContractService.GetAllContractBuy();
            foreach (var item in lstemcontract)
            {
                if (item.Code == txtEMContractCode.Text.Trim() && them)
                {
                    if (MSG.Question("Mã hợp đồng này đã tồn tại, bạn có muốn hệ thống tự sinh mã mới không") == DialogResult.Yes)
                    {
                        txtEMContractCode.Text = Utils.TaoMaChungTu(_IGenCodeService.getGenCode(85));
                        _IGenCodeService.UpdateGenCodeCatalog(85, txtEMContractCode.Text);
                    }
                    else
                    {
                        txtEMContractCode.Focus();
                    }
                    return false;
                }
            }
            foreach (UltraGridRow r in cbbPPOrder.CheckedRows)
            {
                var md = (PPOrder)r.ListObject;
                if ((Guid)cbbAccountingObjectID.Value != md.AccountingObjectID)
                {
                    MSG.Warning("Bạn phải chọn số đơn hàng thuộc cùng nhà cung cấp!");
                    return false;
                }
            }
            return result;
        }

        private void cbbContractState_ItemNotInList(object sender, Infragistics.Win.UltraWinEditors.ValidationErrorEventArgs e)
        {
            List<ContractState> listContractState = _IContractStateService.GetAll();
            if (!listContractState.Exists(x => x.ContractStateName == cbbContractState.Text.ToString()) && cbbContractState.Text != "")
            {
                MSG.Warning("Dữ liệu không có trong danh mục. Đề nghị điều chỉnh lại");
            }
        }

        private void cbbAccountingObjectID_ItemNotInList(object sender, Infragistics.Win.UltraWinEditors.ValidationErrorEventArgs e)
        {
            List<AccountingObject> listAccountingObject = Utils.ListAccountingObject.Where(x => (x.ObjectType == 1 || x.ObjectType == 2) && x.IsActive).ToList();
            if (!listAccountingObject.Exists(x => x.AccountingObjectCode == cbbAccountingObjectID.Text.ToString()) && cbbAccountingObjectID.Text != "")
            {
                MSG.Warning("Dữ liệu không có trong danh mục.Đề nghị điều chỉnh lại");
                cbbAccountingObjectID.Focus();
            }

            if (cbbAccountingObjectID.Value == null || cbbAccountingObjectID.Text.Trim() == "")
            {
                txtAccountingObjectName.Text = "";
                txtAccountingObjectTitle.Text = "";
                txtAccountingObjectSignerName.Text = "";
                txtAccountingObjectAddress.Text = "";
                txtAccountingObjectTel.Text = "";
                txtAccountingObjectFax.Text = "";
                cbbAccountingObjectBankAccount.Text = cbbAccountingObjectBankAccount.NullText;
                txtAccountingObjectBankName.Text = "";
            }
        }

        private void cbbAccountingObjectBankAccount_RowSelected(object sender, RowSelectedEventArgs e)
        {
            cbbAccountingObjectBankAccount.UpdateData();
            if (e.Row == null) return;
            if (e.Row.ListObject == null) return;
            AccountingObjectBankAccount accoutingObjectBanAccount = e.Row.ListObject as AccountingObjectBankAccount;
            txtAccountingObjectBankName.Text = accoutingObjectBanAccount.BankName;
        }

        private void cbbPPOrder_RowSelected(object sender, RowSelectedEventArgs e)
        {
            cbbPPOrder.UpdateData();
            if (e.Row == null) return;
            if (e.Row.ListObject == null) return;
        }

        private void cbbPPOrder_ItemNotInList(object sender, Infragistics.Win.UltraWinEditors.ValidationErrorEventArgs e)
        {
            //List<PPOrder> lstPPOrder = _IPPOrderService.GetAll();
            //if(!lstPPOrder.Exists(x => x.No == cbbPPOrder.Text.Trim()) && cbbPPOrder.Text != "")
            //{
            //    MSG.Warning("Dữ liệu không có trong danh mục.Đề nghị điều chỉnh lại");
            //}
        }

        private void uGridEMContractMG_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("MaterialGoodsID"))
            {
                if (!e.Cell.Row.Cells["MaterialGoodsID"].IsFilterRowCell)
                {
                    bool checkMaterialGood = false;
                    var combo = (UltraCombo)e.Cell.ValueListResolved;
                    if (combo.SelectedRow != null)
                    {
                        var model = (MaterialGoods)combo.SelectedRow.ListObject;
                        if (model != null)
                        {
                            //add by cuongpv
                            var lstRowCheck = uGridEMContractMG.Rows.Where(x => x.Cells["MaterialGoodsName"].Value != null).ToList();
                            foreach (var item in lstRowCheck)
                            {
                                if (model.ID == (Guid)item.Cells["MaterialGoodsID"].Value)
                                {
                                    checkMaterialGood = true;
                                    break;
                                }
                            }
                            if (checkMaterialGood)
                            {
                                MessageBox.Show("Đã có mã hàng này trong hợp đồng, xin kiểm tra lại.");
                                return;
                            }
                            else
                            {
                                decimal quantityReturn = 0;
                                decimal quantity = 0;
                                e.Cell.Row.Cells["MaterialGoodsName"].Value = model.MaterialGoodsName;
                                e.Cell.Row.Cells["Unit"].Value = model.Unit;
                                e.Cell.Row.Cells["VATRate"].Value = model.TaxRate;
                                if (_select != null)
                                {
                                    quantityReturn = _IPPDiscountReturnDetailService.GetPPDiscountReturnDetailQuantity(model.ID, _select.ID);//add by cuongpv
                                    quantity = _IPPInvoiceDetailService.GetPPInvoiceQuantity(model.ID, _select.ID) - quantityReturn;//edit by cuongpv
                                }
                                e.Cell.Row.Cells["QuantityReceipt"].Value = quantity;
                            }
                            //end add by cuongpv
                        }
                    }
                    if (e.Cell.Row.Cells["MaterialGoodsID"].Text == "")
                    {
                        e.Cell.Row.Cells["MaterialGoodsName"].Value = "";
                        e.Cell.Row.Cells["Unit"].Value = "";
                        e.Cell.Row.Cells["QuantityReceipt"].Value = 0;
                    }
                }
            }

            if (e.Cell.Column.Key.Equals("UnitPriceOriginal"))
            {
                decimal ex = 1;
                try
                {
                    ex = Convert.ToDecimal(txtExchangeRate.Text);
                }
                catch (Exception)
                {
                    ex = 1;
                }
                e.Cell.Row.Cells["UnitPrice"].Value = (decimal)e.Cell.Value * ex;
                if (Decimal.Parse(e.Cell.Row.Cells["Quantity"].Value.ToString()) != 0)
                {                    
                    e.Cell.Row.Cells["TotalAmountOriginal"].Value = (Decimal.Parse(e.Cell.Row.Cells["UnitPriceOriginal"].Value.ToString()) != 0) ? Decimal.Parse(e.Cell.Row.Cells["UnitPriceOriginal"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["Quantity"].Value.ToString()) : 0;
                }
            }
            if (e.Cell.Column.Key.Equals("UnitPrice"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["Quantity"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["TotalAmount"].Value = (Decimal.Parse(e.Cell.Row.Cells["UnitPrice"].Value.ToString()) != 0) ? Decimal.Parse(e.Cell.Row.Cells["UnitPrice"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["Quantity"].Value.ToString()) : 0;
                }
            }

            if (e.Cell.Column.Key.Equals("Quantity"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["UnitPrice"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["TotalAmount"].Value = (Decimal.Parse(e.Cell.Row.Cells["Quantity"].Value.ToString()) != 0) ? Decimal.Parse(e.Cell.Row.Cells["UnitPrice"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["Quantity"].Value.ToString()) : 0;
                    e.Cell.Row.Cells["TotalAmountOriginal"].Value = (Decimal.Parse(e.Cell.Row.Cells["UnitPriceOriginal"].Value.ToString()) != 0) ? Decimal.Parse(e.Cell.Row.Cells["UnitPriceOriginal"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["Quantity"].Value.ToString()) : 0;
                }

                if (e.Cell.Row.Cells["MaterialGoodsID"].Text != null)
                {
                    decimal quantityReturn = 0;
                    decimal quantity = 0;
                    var Mid = e.Cell.Row.Cells["MaterialGoodsID"].Value;
                    MaterialGoods material = _IMaterialGoodsService.Getbykey((Guid)Mid);
                    if (_select != null)
                    {
                        quantityReturn = _IPPDiscountReturnDetailService.GetPPDiscountReturnDetailQuantity(material.ID, _select.ID);//add by cuongpv
                        quantity = _IPPInvoiceDetailService.GetPPInvoiceQuantity(material.ID, _select.ID) - quantityReturn;//edit by cuongpv
                    }
                    e.Cell.Row.Cells["QuantityReceipt"].Value = quantity;
                }
            }

            if (e.Cell.Column.Key.Equals("DiscountRate"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["TotalAmount"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["DiscountAmount"].Value = (Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) != 0) ? Decimal.Parse(e.Cell.Row.Cells["TotalAmount"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) / 100 : 0;
                }
                if (Decimal.Parse(e.Cell.Row.Cells["TotalAmountOriginal"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["DiscountAmountOriginal"].Value = (Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) != 0) ? Decimal.Parse(e.Cell.Row.Cells["TotalAmountOriginal"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) / 100 : 0;
                }
            }

            if (e.Cell.Column.Key.Equals("VATRate"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["TotalAmount"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["VATAmount"].Value = ((decimal)e.Cell.Row.Cells["VATRate"].Value != 0 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -1 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -2) ?
                        (Decimal.Parse(e.Cell.Row.Cells["TotalAmount"].Value.ToString()) - Decimal.Parse(e.Cell.Row.Cells["DiscountAmount"].Value.ToString())) * Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) / 100 : 0;
                }
                if (Decimal.Parse(e.Cell.Row.Cells["TotalAmountOriginal"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["VATAmountOriginal"].Value = ((decimal)e.Cell.Row.Cells["VATRate"].Value != 0 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -1 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -2) ?
                        (Decimal.Parse(e.Cell.Row.Cells["TotalAmountOriginal"].Value.ToString()) - Decimal.Parse(e.Cell.Row.Cells["DiscountAmountOriginal"].Value.ToString())) * Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) / 100 : 0;
                }
            }

            if (e.Cell.Column.Key.Contains("TotalAmount"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["DiscountAmount"].Value = (Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) != 0) ? Decimal.Parse(e.Cell.Row.Cells["TotalAmount"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) / 100 : 0;                    
                }
                if(Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["VATAmount"].Value = ((decimal)e.Cell.Row.Cells["VATRate"].Value != 0 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -1 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -2) ?
                        (Decimal.Parse(e.Cell.Row.Cells["TotalAmount"].Value.ToString()) - Decimal.Parse(e.Cell.Row.Cells["DiscountAmount"].Value.ToString())) * Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) / 100 : 0;
                }
            }
            if (e.Cell.Column.Key.Contains("TotalAmountOriginal"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) != 0)
                {
                  
                    e.Cell.Row.Cells["DiscountAmountOriginal"].Value = (Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) != 0) ? Decimal.Parse(e.Cell.Row.Cells["TotalAmountOriginal"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) / 100 : 0;
                }
                if (Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["VATAmountOriginal"].Value = ((decimal)e.Cell.Row.Cells["VATRate"].Value != 0 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -1 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -2) ?
                        (Decimal.Parse(e.Cell.Row.Cells["TotalAmountOriginal"].Value.ToString()) - Decimal.Parse(e.Cell.Row.Cells["DiscountAmountOriginal"].Value.ToString())) * Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) / 100 : 0;
                }
            }
            if (e.Cell.Column.Key.Contains("DiscountAmount"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["VATAmount"].Value = ((decimal)e.Cell.Row.Cells["VATRate"].Value != 0 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -1 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -2)
                        ? (Decimal.Parse(e.Cell.Row.Cells["TotalAmount"].Value.ToString()) - Decimal.Parse(e.Cell.Row.Cells["DiscountAmount"].Value.ToString())) * Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) / 100 : 0;
                  
                }

            }
            if (e.Cell.Column.Key.Contains("DiscountAmountOriginal"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) != 0)
                {
                   
                    e.Cell.Row.Cells["VATAmountOriginal"].Value = ((decimal)e.Cell.Row.Cells["VATRate"].Value != 0 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -1 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -2)
                       ? (Decimal.Parse(e.Cell.Row.Cells["TotalAmountOriginal"].Value.ToString()) - Decimal.Parse(e.Cell.Row.Cells["DiscountAmountOriginal"].Value.ToString())) * Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) / 100 : 0;
                }

            }

        }

        private void cbbPPOrder_ValueChanged(object sender, EventArgs e)
        {
            List<EMContractDetailMG> lstEMContractDetailMG = new List<EMContractDetailMG>();
            if (_select != null)
                lstEMContractDetailMG = Utils.ListEMContractDetailMG.Where(x => x.ContractID == _select.ID).ToList();
            List<Guid> lstMaterialGoodsID = new List<Guid>();
            foreach (var item in lstEMContractDetailMG)
            {
                lstMaterialGoodsID.Add((Guid)item.MaterialGoodsID);
            }
            uGridEMContractMG.DataSource = new BindingList<EMContractDetailMG>(lstEMContractDetailMG);
            uGridEMContractMG.UpdateData();
            foreach (UltraGridRow r in cbbPPOrder.CheckedRows)
            {
                List<PPOrderDetail> lstPPOrderDetail = Utils.ListPPOrderDetail.Where(x => x.PPOrderID == (Guid)r.Cells["ID"].Value).ToList();
                if (lstPPOrderDetail.Count > 0)
                {
                    foreach (var item in lstPPOrderDetail)
                    {
                        MaterialGoods material = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == (Guid)item.MaterialGoodsID);
                        if (!lstMaterialGoodsID.Contains(material.ID))
                        {
                            uGridEMContractMG.AddNewRow4Grid();
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["MaterialGoodsID"].Activation = Activation.NoEdit;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["PPOrderCode"].Value = r.Cells["No"].Value;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["UnitPrice"].Value = item.UnitPrice;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["MaterialGoodsID"].Value = material.ID;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["MaterialGoodsCode"].Value = material.MaterialGoodsCode;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["MaterialGoodsName"].Value = material.MaterialGoodsName;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["Quantity"].Value = item.Quantity;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["TotalAmount"].Value = item.Amount.FormatNumberic(ConstDatabase.Format_TienVND);
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["DiscountRate"].Value = item.DiscountRate;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["DiscountAmount"].Value = item.DiscountAmount;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["VATRate"].Value = item.VATRate;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["VATAmount"].Value = item.VATAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["Unit"].Value = material.Unit;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["QuantityReceipt"].Value = item.QuantityReceipt;
                        }
                    }
                }
            }
            if (cbbPPOrder.CheckedRows.Count > 0)
            {
                var md = cbbPPOrder.CheckedRows[0].ListObject as PPOrder;
                txtDeliverDate.Value = md.DeliverDate;
                txtDueDate.Value = md.DueDate;
            }
            else
            {
                txtDeliverDate.Value = null;
                txtDueDate.Value = null;
            }
            decimal a = (decimal)lstEMContractDetailMG.Sum(x => x.TotalAmount);
            decimal b = (decimal)lstEMContractDetailMG.Sum(x => x.DiscountAmount);
            decimal c = (decimal)lstEMContractDetailMG.Sum(x => x.VATAmount);
            txtAmount.Value = lstEMContractDetailMG.Count > 0 ? (a - b + c) : 0;
            check1 = true;
        }
        private void cbbPPOrder_AfterCloseUp(object sender, EventArgs e)
        {
            if (check1)
            {
                check1 = false;
                if (cbbPPOrder.Text == "")
                {
                    cbbAccountingObjectID.Value = null;
                    txtAccountingObjectName.Text = null;
                    txtAccountingObjectAddress.Text = null;
                    cbbAccountingObjectBankAccount.DataSource = new List<AccountingObjectBankAccount>();
                    txtAccountingObjectFax.Text = null;
                    txtAccountingObjectTel.Text = null;
                    return;
                }
                var model = (PPOrder)cbbPPOrder.CheckedRows[0].ListObject;
                if (cbbAccountingObjectID.Text == null || cbbAccountingObjectID.Text == "")
                {
                    cbbAccountingObjectID.Value = model.AccountingObjectID;
                }
                foreach (UltraGridRow r in cbbPPOrder.CheckedRows)
                {
                    var md = (PPOrder)r.ListObject;
                    if ((Guid)cbbAccountingObjectID.Value != md.AccountingObjectID)
                    {
                        MSG.Warning("Bạn phải chọn số đơn hàng thuộc cùng nhà cung cấp!");
                        return;
                    }
                }
            }
        }
        private void uGridEMContractMG_SummaryValueChanged(object sender, SummaryValueChangedEventArgs e)
        {
            if (uGridEMContractMG.Rows.Count > 0)
            {
                txtAmount.Value = Decimal.Parse(uGridEMContractMG.Rows.SummaryValues["sumAmount"].Value.ToString()) - Decimal.Parse(uGridEMContractMG.Rows.SummaryValues["sumDiscountAmount"].Value.ToString()) + Decimal.Parse(uGridEMContractMG.Rows.SummaryValues["sumVATAmount"].Value.ToString());
                txtAmountOriginal.Value = Decimal.Parse(uGridEMContractMG.Rows.SummaryValues["sumAmountOriginal"].Value.ToString()) - Decimal.Parse(uGridEMContractMG.Rows.SummaryValues["sumDiscountAmountOriginal"].Value.ToString()) + Decimal.Parse(uGridEMContractMG.Rows.SummaryValues["sumVATAmountOriginal"].Value.ToString());
            }
                
        }

        private void uGridEMContractMG_Error(object sender, ErrorEventArgs e)
        {
            UltraGridCell cell = uGridEMContractMG.ActiveCell;
            if (uGridEMContractMG.ActiveCell == null) return;
            if (cell.Column.Key.Equals("MaterialGoodsID"))
            {
                int count = Utils.ListMaterialGoods.Count(x => x.MaterialGoodsCode == cell.Text);
                if (count == 0)
                {
                    MSG.Error("Dữ liệu không có trong danh mục");

                }
            }
            e.Cancel = true;
        }
        private void tsmDelete_Click(object sender, EventArgs e)
        {
            if (uGridEMContractMG.ActiveRow != null || uGridEMContractMG.ActiveCell != null)
            {
                UltraGridRow row = null;
                if (uGridEMContractMG.ActiveCell != null || uGridEMContractMG.ActiveRow != null)
                {
                    row = uGridEMContractMG.ActiveCell != null ? uGridEMContractMG.ActiveCell.Row : uGridEMContractMG.ActiveRow;
                }
                else
                {
                    if (uGridEMContractMG.Rows.Count > 0) row = uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1];
                }
                if (row != null) row.Delete(false);
                uGridEMContractMG.Update();
            }
            //var lstSelectPre = uGridEMContractMG.Selected.Rows;
            //decimal checkQuantityRe = 0;
            //foreach (var item in lstSelectPre)
            //{
            //    checkQuantityRe = decimal.Parse(item.Cells["QuantityReceipt"].Value.ToString());
            //    if (checkQuantityRe > 0)
            //    {
            //        item.Selected = false;
            //        checkQuantityRe = 0;
            //    }
            //}

            //var lstSelectAfter = uGridEMContractMG.Selected.Rows;
            ////xoa nhung dong ko co so luong da nhan
            //if (lstSelectAfter.Count > 0)
            //    uGridEMContractMG.DeleteSelectedRows(false);

            //uGridEMContractMG.UpdateData();
        }

        private void uGridEMContractMG_CellChange(object sender, CellEventArgs e)
        {
            
        }

        private void FEMContractBuyDetail_Resize(object sender, EventArgs e)
        {
            //this.Height = this.Height - 10;
        }

        private void FEMContractBuyDetail_Load(object sender, EventArgs e)
        {
            //  this.Height = this.Height - 10;
        }

        private void FEMContractBuyDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        private void FEMContractBuyDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }
}