﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.Text.RegularExpressions;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using Accounting.Model;


namespace Accounting
{
    public partial class FEMShareHolderDetail : CustormForm
    {
        #region khai báo
        private readonly IEMShareHolderService _IEMShareHolderService;
        private readonly IEMShareHolderTransactionService _IEMShareHolderTransactionService;
        private readonly IStockCategoryService _IStockCategoryService;
        private readonly IShareHolderGroupService _IShareHolderGroupService;
        private IEMRegistrationService _IEMRegistrationService;
        private readonly IBankService _IBankService;
        private readonly IBankAccountDetailService _IBankAccountDetailService;

        EMShareHolder _Select = new EMShareHolder();
        EMRegistration _Select1 = new EMRegistration();
        List<string> ltemp = new List<string>() { "Ông", "Bà" };

        bool Them = true;

        public static bool isClose = true;


        #endregion

        #region khởi tạo
        public FEMShareHolderDetail()
        {//Thêm

            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IEMShareHolderService = IoC.Resolve<IEMShareHolderService>();
            _IEMShareHolderTransactionService = IoC.Resolve<IEMShareHolderTransactionService>();
            _IStockCategoryService = IoC.Resolve<IStockCategoryService>();
            _IShareHolderGroupService = IoC.Resolve<IShareHolderGroupService>();
            _IBankService = IoC.Resolve<IBankService>();
            _IEMRegistrationService = IoC.Resolve<IEMRegistrationService>();
            _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();

            this.uGrid2.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            Config(uGrid2);
            #endregion

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            test2();


            dtContactIssueDate.DateTime = DateTime.Now;
            dtBusinessIssuedDate.DateTime = DateTime.Now;
            dtBookIssuedDate.DateTime = DateTime.Now;
            this.Text = "Thêm mới cổ đông";

            #endregion
        }

        public FEMShareHolderDetail(EMShareHolder temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();

            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            txtShareHolderCode.Enabled = false;

            //Khai báo các webservices
            _IEMShareHolderService = IoC.Resolve<IEMShareHolderService>();
            _IEMShareHolderTransactionService = IoC.Resolve<IEMShareHolderTransactionService>();
            _IStockCategoryService = IoC.Resolve<IStockCategoryService>();
            _IShareHolderGroupService = IoC.Resolve<IShareHolderGroupService>();
            _IEMRegistrationService = IoC.Resolve<IEMRegistrationService>();

            this.uGrid2.DisplayLayout.Override.SelectTypeRow = SelectType.Single;


            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)

            #endregion


            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);

            this.Text = "Sửa cổ đông";
            test1(temp);
            #endregion
        }
        public FEMShareHolderDetail(EMRegistration temp1)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select1 = temp1;
            Them = false;

            //Khai báo các webservices
            _IEMRegistrationService = IoC.Resolve<IEMRegistrationService>();

            InitializeGUI();

            #endregion

            #region Fill dữ liệu Obj vào control

            FillEMRegistration(temp1);
            #endregion

        }

        private void FillEMRegistration(EMRegistration temp1)
        {

            txtShareHolderName.Text = temp1.RegistrationName;
            txtContactIdentificationNo.Text = temp1.ContactIdentificationNo;
            txtContactIssueBy.Text = temp1.ContactIssueBy;

            if (!dtContactIssueDate.Equals("")) { } else { dtContactIssueDate.DateTime = (DateTime)temp1.ContactIssueDate; }

            txtContactOfficeTel.Text = temp1.ContactOfficeTel;
            txtContactMobile.Text = temp1.ContactMobile;
            txtAddress.Text = temp1.Address;
            txtBusinessRegistrationNumber.Text = temp1.BusinessRegistrationNumber;

            if (!dtBusinessIssuedDate.Equals("")) { } else { dtBusinessIssuedDate.DateTime = (DateTime)temp1.BusinessRegistrationIssueDate; }

            txtBusinessIssueBy.Text = temp1.BusinessRegistrationIssueBy;
            txtTel.Text = temp1.Tel;
            txtFax.Text = temp1.Fax;


        }

        void loadcbb()
        {
            cbbShareHolderCategoryID.DataSource = _IShareHolderGroupService.GetAll();
            cbbShareHolderCategoryID.DisplayMember = "ShareHolderGroupName";
            Utils.ConfigGrid(cbbShareHolderCategoryID, ConstDatabase.ShareHolderGroup_TableName);
        }
        private void InitializeGUI()
        {
            loadcbb();

            cbbContactPrefix.DataSource = ltemp;
            cbbContactPrefix.SelectedIndex = -1;
            ultraOptionSet1.CheckedIndex = 0;
        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion
            if (txtContactEmail.Text != "")
            {
                if (!isValidEmail(txtContactEmail.Text)) return;
            }
            if (txtContactOfficeTel.Text != "")
            {
                if (!isPhone(txtContactOfficeTel.Text)) return;
            }
            if (txtContactHomeTel.Text != "")
            {
                if (!isPhone(txtContactHomeTel.Text)) return;
            }
            if (txtContactMobile.Text != "")
            {
                if (!isPhone(txtContactMobile.Text)) return;
            }

            //Gui to object
            #region Fill dữ liệu control vào obj
            EMShareHolder temp = Them ? new EMShareHolder() : _IEMShareHolderService.Getbykey(_Select.ID);
            ObjandGUI(temp, true);

            #endregion

            #region Thao tác CSDL
            _IEMShareHolderService.BeginTran();
            if (Them) _IEMShareHolderService.CreateNew(temp);
            else _IEMShareHolderService.Update(temp);



            //if (IsAdd) _IEMShareHolderService.CreateNew(temp);
            //else _IEMShareHolderService.Update(temp);
            //thao tac vao bang AccountingObjectBankAccount
            //foreach (EMRegistration item in FEMRegistration.dsEMRegistration) item.EMShareHolderID = temp.ID;
            //List<EMRegistration> EMRegistrations = _IEMRegistrationService.Query.Where(k => k.EMShareHolderID == temp.ID).ToList();
            //foreach (EMRegistration item in EMRegistrations) _IEMRegistrationService.Delete(item);
            //foreach (EMRegistration item in FEMRegistration.dsEMRegistration) _IEMRegistrationService.CreateNew(item);
            _IEMShareHolderService.CommitTran();
            //temp.BankAccounts.Clear(); temp.BankAccounts = (List<EMRegistration>)Utils.CloneObject(FEMRegistration.dsEMRegistration);
            #endregion

            #region xử lý form, kết thúc form

            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý

        EMShareHolder ObjandGUI(EMShareHolder input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.ShareHolderCode = txtShareHolderCode.Text;
                input.ShareHolderName = txtShareHolderName.Text;
                input.PersonalTaxCode = txtPersonalTaxCode.Text;
                input.Address = txtAddress.Text;

                if (cbbContactPrefix.SelectedItem != null)
                    input.ContactPrefix = cbbContactPrefix.SelectedItem.ToString();

                ShareHolderGroup temp0 = (ShareHolderGroup)Utils.getSelectCbbItem(cbbShareHolderCategoryID);
                if (temp0 == null) input.ShareHolderCategoryID = null;
                else input.ShareHolderCategoryID = temp0.ID;

                input.ContactIssueDate = dtContactIssueDate.DateTime;
                input.ContactIssueBy = txtContactIssueBy.Text;
                input.PersonalTaxCode = txtPersonalTaxCode.Text;
                input.BookIssuedDate = dtBookIssuedDate.DateTime;
                input.BusinessRegistrationNumber = txtBusinessRegistrationNumber.Text;
                input.BusinessIssuedDate = dtBusinessIssuedDate.DateTime;
                input.BusinessIssueBy = txtBusinessIssueBy.Text;
                //input.BankAccount = txtBankAccount.Text;
                input.BankAccountDetailID = _IBankAccountDetailService.GetGuildFromBankAccount(txtBankAccount.Text);
                input.BankName = txtBankName.Text;
                input.Tel = txtTel.Text;
                input.Fax = txtFax.Text;
                input.Email = txtEmail.Text;
                input.Website = txtWebsite.Text;
                input.ContactName = txtContactName.Text;
                input.ContactTitle = txtContactTitle.Text;
                input.ContactAddress = txtContactAddress.Text;
                input.ContactIdentificationNo = txtContactIdentificationNo.Text;
                input.ContactIssueDate = dtContactIssueDate.DateTime;
                input.ContactIssueBy = txtContactIssueBy.Text;
                input.ContactOfficeTel = txtContactOfficeTel.Text;
                input.ContactHomeTel = txtContactHomeTel.Text;
                input.ContactMobile = txtContactMobile.Text;
                input.ContactEmail = txtContactEmail.Text;

                //input.InvestorType = ultraOptionSet1.CheckedIndex;

            }
            else
            {
                txtShareHolderCode.Text = input.ShareHolderCode;
                txtShareHolderName.Text = input.ShareHolderName;
                cbbContactPrefix.SelectedText = input.ContactPrefix;
                txtAddress.Text = input.Address;
                dtContactIssueDate.DateTime = (DateTime)input.ContactIssueDate;
                txtContactIssueBy.Text = input.ContactIssueBy;
                txtPersonalTaxCode.Text = input.PersonalTaxCode;
                dtBookIssuedDate.DateTime = (DateTime)input.BookIssuedDate;
                txtBusinessRegistrationNumber.Text = input.BusinessRegistrationNumber;
                dtBusinessIssuedDate.DateTime = (DateTime)input.BusinessIssuedDate;
                txtBusinessIssueBy.Text = input.BusinessIssueBy;


                txtBankName.Text = input.BankName;
                txtTel.Text = input.Tel;
                txtFax.Text = input.Fax;
                txtEmail.Text = input.Email;
                txtWebsite.Text = input.Website;
                txtContactName.Text = input.ContactName;
                txtContactTitle.Text = input.ContactTitle;
                txtContactAddress.Text = input.ContactAddress;
                txtContactIdentificationNo.Text = input.ContactIdentificationNo;
                dtContactIssueDate.DateTime = (DateTime)input.ContactIssueDate;
                txtContactIssueBy.Text = input.ContactIssueBy;
                txtContactOfficeTel.Text = input.ContactOfficeTel;
                txtContactHomeTel.Text = input.ContactHomeTel;
                txtContactMobile.Text = input.ContactMobile;
                txtContactEmail.Text = input.ContactEmail;
                //input.BankAccount = txtBankAccount.Text;
                input.BankAccountDetailID = _IBankAccountDetailService.GetGuildFromBankAccount(txtBankAccount.Text);
                //từ tài khoản
                foreach (var item in cbbShareHolderCategoryID.Rows)
                {
                    if ((item.ListObject as ShareHolderGroup).ID == input.ShareHolderCategoryID) cbbShareHolderCategoryID.SelectedRow = item;
                }

            }
            return input;
        }

        bool CheckCode()
        {
            bool kq = true;

            if (string.IsNullOrEmpty(txtShareHolderCode.Text) || string.IsNullOrEmpty(txtShareHolderName.Text) || string.IsNullOrEmpty(txtContactName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            //Check Error Chung
            List<string> hh = _IEMShareHolderService.Query.Select(a => a.ShareHolderCode).ToList();
            foreach (var item in hh)
            {
                if (item.Equals(txtShareHolderCode.Text))
                {
                    MessageBox.Show("Mã cổ đông" + item + " đã tồn tại trong danh sách", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            return kq;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtShareHolderCode.Text) || string.IsNullOrEmpty(txtShareHolderName.Text) || cbbShareHolderCategoryID.SelectedRow == null)
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            return kq;
        }
        bool isPhone(string inputPhone)
        {
            string strRegex = @"0\d{9,10}";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputPhone))
                return (true);
            else
                MessageBox.Show("Số điện thoại phải là số (có 10 hoặc 11 số) " + "\n" + " bắt đầu bằng số 0  !!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return (false);
        }
        bool isMST(string inputContaxPerfix)
        {
            string strRegex = @"^d{9,10}";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputContaxPerfix))
                return (true);
            else
                MessageBox.Show("Mã số thuế phải là số (có 10, 13 hoặc 14 số)!!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return (false);
        }
        bool isValidEmail(string inputEmail)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputEmail))
                return (true);
            else
                MessageBox.Show("Sai định dạng Email", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return (false);
        }
        #endregion


        private void ultraOptionSet1_ValueChanged_1(object sender, EventArgs e)
        {
            if (ultraOptionSet1.CheckedIndex == 0)
            {
                this.Size = new Size(680, 545);
                panel1.Size = new Size(662, 480);
                btnSave.Location = new Point(480, 480);
                btnClose.Location = new Point(579, 480);
                ultraGroupBox1.Size = new Size(656, 270);
                ultraGroupBox3.Location = new Point(3, 300);
                ultraGroupBox2.Hide();


                panel2.TabIndex = 2;
                panel3.TabIndex = 3;
                panel17.TabIndex = 4;
                panel7.TabIndex = 5;
                panel5.TabIndex = 6;
                panel4.TabIndex = 7;
                panel6.TabIndex = 8;
                panel21.TabIndex = 9;
                panel22.TabIndex = 10;
                panel23.TabIndex = 11;
                panel11.TabIndex = 12;
                panel12.TabIndex = 13;
                panel24.TabIndex = 14;
                panel25.TabIndex = 15;
                panel27.TabIndex = 16;
                btnSave.TabIndex = 17;
                btnClose.TabIndex = 18;
                panel2.Location = new Point(10, 20);
                panel3.Location = new Point(216, 19);
                panel17.Location = new Point(10, 50);
                panel7.Location = new Point(216, 50);
                panel5.Location = new Point(10, 81);
                panel4.Location = new Point(10, 112);
                panel6.Location = new Point(215, 112);
                panel21.Location = new Point(10, 142);
                panel22.Location = new Point(215, 142);
                panel23.Location = new Point(430, 142);
                panel11.Location = new Point(11, 173);
                panel12.Location = new Point(217, 173);
                panel24.Location = new Point(11, 205);
                panel25.Location = new Point(215, 204);
                panel27.Location = new Point(10, 236);


                panel8.Hide();
                panel9.Hide();
                panel10.Hide();
                panel13.Hide();
                panel14.Hide();
                panel15.Hide();
                panel16.Hide();
                panel18.Hide();
                panel19.Hide();
                panel20.Hide();
                panel5.Size = new Size(639, 31);
                txtAddress.Size = new Size(544, 21);
                dtBookIssuedDate.Size = new Size(118, 21);
                panel6.Size = new Size(215, 31);
                //panel27.Size = new Size(281, 21);
                ultraGroupBox1.Controls.Add(panel17);
                ultraGroupBox1.Controls.Add(panel21);
                ultraGroupBox1.Controls.Add(panel22);
                ultraGroupBox1.Controls.Add(panel23);
                ultraGroupBox1.Controls.Add(panel24);
                ultraGroupBox1.Controls.Add(panel25);

                ultraGroupBox1.Controls.Add(panel27);
                //test1(cd);
            }
            if (ultraOptionSet1.CheckedIndex == 1)
            {
                this.Size = new Size(680, 730);
                panel1.Size = new Size(662, 666);
                btnSave.Location = new Point(480, 670);
                btnClose.Location = new Point(579, 670);
                ultraGroupBox1.Size = new Size(656, 245);
                ultraGroupBox2.Location = new Point(3, 278);
                ultraGroupBox3.Location = new Point(3, 488);
                ultraGroupBox2.Show();

                //ultraGroupBox3.Show();

                ultraGroupBox2.Controls.Add(panel17);
                ultraGroupBox2.Controls.Add(panel21);
                ultraGroupBox2.Controls.Add(panel22);
                ultraGroupBox2.Controls.Add(panel23);
                ultraGroupBox2.Controls.Add(panel24);
                ultraGroupBox2.Controls.Add(panel25);
                ultraGroupBox2.Controls.Add(panel27);
                panel2.TabIndex = 1;
                panel3.TabIndex = 2;
                panel4.TabIndex = 3;
                panel5.TabIndex = 4;
                panel6.TabIndex = 5;
                panel7.TabIndex = 6;
                panel8.TabIndex = 7;
                panel9.TabIndex = 8;
                panel10.TabIndex = 9;
                panel11.TabIndex = 10;
                panel12.TabIndex = 11;
                panel13.TabIndex = 12;
                panel14.TabIndex = 13;
                panel15.TabIndex = 14;
                panel16.TabIndex = 15;
                panel17.TabIndex = 16;
                panel18.TabIndex = 17;
                panel19.TabIndex = 18;
                panel20.TabIndex = 19;
                panel21.TabIndex = 20;
                panel22.TabIndex = 21;
                panel23.TabIndex = 22;
                panel24.TabIndex = 23;
                panel25.TabIndex = 24;
                panel27.TabIndex = 25;
                btnSave.TabIndex = 26;
                btnClose.TabIndex = 27;
                panel4.Location = new Point(10, 50);
                panel5.Location = new Point(216, 50);
                panel6.Location = new Point(10, 81);
                panel7.Location = new Point(216, 81);
                panel11.Location = new Point(10, 142);
                panel12.Location = new Point(216, 142);
                panel17.Location = new Point(10, 20);
                panel21.Location = new Point(10, 112);
                panel22.Location = new Point(215, 112);
                panel23.Location = new Point(430, 113);
                panel24.Location = new Point(10, 142);
                panel25.Location = new Point(216, 142);
                panel27.Location = new Point(10, 176);
                panel8.Show();
                panel9.Show();
                panel10.Show();
                panel13.Show();
                panel14.Show();
                panel15.Show();
                panel16.Show();
                panel18.Show();
                panel19.Show();
                panel20.Show();
                dtBookIssuedDate.Size = new Size(100, 21);
                panel6.Size = new Size(199, 31);
                txtShareHolderName.Text = txtShareHolderName.Text;
            }
        }

        private void cbbShareHolderCategoryID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            new FShareHolderGroupDetail().ShowDialog(this);
            loadcbb();
        }

        private void txtPersonalTaxCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtContactOfficeTel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtContactHomeTel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtContactMobile_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtBusinessRegistrationNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtContactIndenticationNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtBankAcount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void Ugrid_AfterCellUpdate(object sender, CellEventArgs e)
        {

        }

        private void Ugrid_AfterRowInsert(object sender, RowEventArgs e)
        {

        }

        private void Ugrid_AfterBeforCellUpdate(object sender, BeforeCellUpdateEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("TotalShare"))
            {//trước khi cell AmountOriginal cập nhật thì kiểm tra nếu giá trị mới rỗng thì mặc định quay trở về giá trị cũ
                double temp = 0;
                string value = e.NewValue.ToString();
                if (string.IsNullOrEmpty(value))    // || double.TryParse(value, out temp)
                {
                    e.Cancel = true;
                }
            }
        }

        private void Ugrid_BeforeRowInsert(object sender, BeforeRowInsertEventArgs e)
        {

        }

        private void Ugrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {

        }

        private void Ugrid_InitializeTemplateAddRow(object sender, InitializeTemplateAddRowEventArgs e)
        {

        }

        private void Ugrid_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (uGrid2.ActiveCell.Column.DataType == typeof(Guid))
                {
                    return;
                }
                else if (uGrid2.ActiveCell.Column.DataType == typeof(string))
                {
                    uGrid2.ActiveCell.SetValue(string.Empty, true);
                }
            }
        }
        public void test1(EMShareHolder cd)
        {
            List<Cophansohuu> listcp = new List<Cophansohuu>();
            //List<EMShareHolderTransaction> test1s = _IEMShareHolderTransactionService.Query.Where(k => k.EMShareHolderID == cd.ID).ToList();
            List<EMShareHolderTransaction> test1s = _IEMShareHolderTransactionService.GetListEMShareHolderTransactionEMShareHolder(cd.ID);
            //List<EMShareHolder> a = _IEMShareHolderService.Query.ToList();

            foreach (var item in test1s)
            {
                var it = _IStockCategoryService.Getbykey(item.StockCategoryID);
                if (it == null)
                    continue;
                Cophansohuu cophan = new Cophansohuu();
                cophan.Loaicophan = Accounting.Core.Utils.SetNameStockKind(it.StockKind);
                cophan.Hinhthucuudai = it.Incentives;
                cophan.Dieukienchuyennhuong = it.TransferCondition;
                cophan.Socophansohuu = cd.TotalShare;
                cophan.Tongmenhgiacophan = 10000 * cophan.Socophansohuu;

                listcp.Add(cophan);
            }
            BindingList<Cophansohuu> bindingList = new BindingList<Cophansohuu>(listcp);
            uGrid2.DataSource = bindingList;
            Config(uGrid2);
            ConfigGridDong(mauGiaoDien);
        }

        public void test2()
        {

            List<Cophansohuu> listcp = new List<Cophansohuu>();

            BindingList<Cophansohuu> bindingList = new BindingList<Cophansohuu>(listcp);
            uGrid2.DataSource = bindingList;
            Config(uGrid2);
            ConfigGridDong(mauGiaoDien);
        }

        public EMShareHolder cd { get; set; }

        // Load tab "uGrid2"
        private void Config(UltraGrid utralGrid)
        {
            List<TemplateColumn> dstemplatecolums = new List<TemplateColumn>();

            Dictionary<string, Dictionary<string, TemplateColumn>> dicTemp = new Dictionary<string, Dictionary<string, TemplateColumn>>();
            string nameTable = string.Empty;
            List<string> strColumnName, strColumnCaption, strColumnToolTip = new List<string>();
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb = new List<bool>();
            List<int> intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition = new List<int>();
            List<int> VTbolIsVisible, VTbolIsVisibleCbb = new List<int>();

            strColumnName = new List<string>() { "Loaicophan", "Hinhthucuudai", "Dieukienchuyennhuong", "Socophansohuu", "Tongmenhgiacophan" };
            strColumnCaption = strColumnToolTip = new List<string>() { "Loại cổ phần", "Hình thức ưu đãi", "Điều kiện chuyển nhượng", "Số cổ phần sở hữu", "Tổng mệnh giá cổ phần" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() { 0, 1, 2, 3, 4 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 1, 2 };
            for (int i = 0; i < 5; i++)
            {
                bolIsVisible.Add(VTbolIsVisible.Contains(i));
                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            dstemplatecolums = ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();

            Utils.ConfigGrid(utralGrid, string.Empty, dstemplatecolums, 0);
        }



        private void ConfigGridDong(Template mauGiaoDien)
        {


            uGrid2.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
            uGrid2.DisplayLayout.Override.TemplateAddRowPrompt = resSystem.MSG_System_07;
            uGrid2.DisplayLayout.Override.SpecialRowSeparator = SpecialRowSeparator.None;


            //hiển thị 1 band
            uGrid2.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            //tắt lọc cột
            uGrid2.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            uGrid2.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGrid2.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGrid2.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGrid2.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid2.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            //Hiển thị SupportDataErrorInfo
            uGrid2.DisplayLayout.Override.SupportDataErrorInfo = SupportDataErrorInfo.CellsOnly;

            uGrid2.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGrid2.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;

            uGrid2.DisplayLayout.UseFixedHeaders = true;

            uGrid2.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;

            Infragistics.Win.UltraWinGrid.UltraGridBand band = uGrid2.DisplayLayout.Bands[0];


            //band.Summaries.Clear();
            //if (mauGiaoDien != null)
            //    Utils.ConfigGrid(uGrid2, mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 100).TemplateColumns, 1);

            ////bỏ dòng tổng số
            //uGrid2.DisplayLayout.Bands[0].Summaries.Clear();
            //uGrid2.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Default;
            //uGrid2.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            //uGrid2.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;

            //uGrid2.DisplayLayout.UseFixedHeaders = true;

            foreach (var item in band.Columns)
            {

                if (item.Key.Equals("Loaicophan"))
                {
                    List<StockCategory> cbb = _IStockCategoryService.GetAll();
                    item.Editor = this.CreateCombobox(cbb, ConstDatabase.StockCategory_TableName, "StockCategoryCode", "StockCategoryName");
                }
            }

        }


        public Template mauGiaoDien { get; set; }

        private void ultraButton1_Click(object sender, EventArgs e)
        {

            FRegistrationNew tmpForm = new FRegistrationNew();
            tmpForm.ShowDialog(this);
            EMRegistration selected = tmpForm.EMRegistrationSelect;
            FillEMRegistration(selected);



        }


    }
}
