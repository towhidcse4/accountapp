﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.ComponentModel;
using System.Linq;

namespace Accounting
{
    public partial class FRegistrationNew : CustormForm
    {
         #region khai báo
        private readonly IEMRegistrationService _IEMRegistrationService;
        public static List<EMRegistration> dsEMRegistration = new List<EMRegistration>();
        #endregion

        EMRegistration _Select = new EMRegistration();
        public EMRegistration EMRegistrationSelect
        {
            get { return _Select; }
            set { _Select = value; }
        }


        #region khởi tạo

        public FRegistrationNew()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            _IEMRegistrationService = IoC.Resolve<IEMRegistrationService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            Loaddulieu();
        }

        void Loaddulieu()
        {
            Loaddulieu(true);
        }

        private void Loaddulieu(bool configGrid)
        {
            #region Lấy dữ liệu từ CSDL
            List<EMRegistration> list = _IEMRegistrationService.GetAll();
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list.ToArray();
            //mặc định chọn dòng đầu tiên.
            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }
            if (configGrid) ConfigGrid(uGrid);

            #endregion
        }

        #endregion

        #region Nghiệp vụ
       
        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            System.Drawing.Point point = new System.Drawing.Point(e.X, e.Y);
            //lấy kiểu của đối tượng sender (đối tượng gây ra sự kiện)
            System.Type type = sender.GetType();
            UltraGrid uGrid = (UltraGrid)sender;
            //lấy UltraGridCell từ vị trí
            UltraGridCell cell = (UltraGridCell)((UltraGridBase)sender).DisplayLayout.UIElement.ElementFromPoint(point).GetContext(typeof(UltraGridCell));
            //nếu đang bấm chuột phải và đối tượng đang chọn khác null thì thao tác
            if (cell != null && cell.Row.Index >= 0)
            {
                uGrid.Rows[cell.Row.Index].Selected = true;
            }
        }
        #endregion

        #region Utils
        //void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        //{//hàm chung
        //    Utils.ConfigGrid(utralGrid, fEMShareHolder.database.EMRegistrations_TableName, false);
        //}

        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {
            List<TemplateColumn> dstemplatecolums = new List<TemplateColumn>();

            Dictionary<string, Dictionary<string, TemplateColumn>> dicTemp = new Dictionary<string, Dictionary<string, TemplateColumn>>();
            string nameTable = string.Empty;
            List<string> strColumnName, strColumnCaption, strColumnToolTip = new List<string>();
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb = new List<bool>();
            List<int> intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition = new List<int>();
            List<int> VTbolIsVisible, VTbolIsVisibleCbb = new List<int>();

            strColumnName = new List<string>() { "ID", "RegistrationCode", "RegistrationName", "EMPublishPeriodID", "EMPublishPeriodName", "RegistrationGroupID", "TypeID", "Address", "Tel", "Fax", "Email", "Website", "BusinessRegistrationNumber", "BusinessRegistrationIssueBy", "BusinessRegistrationIssueDate", "BankAccount", "BankName", "ContactTaxCode", "ContactName", "ContactTile", "ContactPrefix", "ContactMobile", "ContactEmail", "ContactOfficeTel", "ContactHomeTel", "ContactAddress", "IsPersonal", "ContactIdentificationNo", "ContactIssueDate", "ContactIssueBy", "Status", "EMShareHolderID", "TotalQuantityBuyable", "TotalQuantityRegisted", "TotalQuantityApproved", "TotalQuantityBought", "TotalAmountBought", "Recorded" };
            strColumnCaption = strColumnToolTip = new List<string>() { "ID", "Mã đăng ký", "Tên đăng ký", "EMPublishPeriodID", "EMPublishPeriodName", "RegistrationGroupID", "TypeID", "Address", "Tel", "Fax", "Email", "Website", "BusinessRegistrationNumber", "BusinessRegistrationIssueBy", "BusinessRegistrationIssueDate", "BankAccount", "BankName", "ContactTaxCode", "Tên", "ContactTile", "ContactPrefix", "ContactMobile", "ContactEmail", "ContactOfficeTel", "ContactHomeTel", "ContactAddress", "IsPersonal", "ContactIdentificationNo", "ContactIssueDate", "ContactIssueBy", "Status", "EMShareHolderID", "SL được mua", "SL đăng ký", "SL được duyệt", "SL đã nộp tiền", "Số tiền đã nộp", "Recorded" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() {1, 2 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 1, 2 };
            for (int i = 0; i < 38; i++)
            {
                bolIsVisible.Add(VTbolIsVisible.Contains(i));
                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            dstemplatecolums = ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();

            Utils.ConfigGrid(utralGrid, fEMShareHolder.database.EMRegistrations_TableName, dstemplatecolums, 0);
        }
        #endregion


        private void btnClose_Click_1(object sender, EventArgs e)
        {
            //EMRegistrationSelect = null;
            this.Close();
        }

        private void BtnSave_Click_1(object sender, EventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                this.EMRegistrationSelect = uGrid.Selected.Rows[0].ListObject as EMRegistration;
            }

            this.Close();
        }

       
    }
}
