﻿namespace Accounting
{
    partial class FEMShareHolderDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.panel23 = new System.Windows.Forms.Panel();
            this.ultraLabel34 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactIssueBy = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel19 = new System.Windows.Forms.Panel();
            this.ultraLabel42 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel27 = new System.Windows.Forms.Panel();
            this.ultraLabel33 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactEmail = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel22 = new System.Windows.Forms.Panel();
            this.dtContactIssueDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel35 = new Infragistics.Win.Misc.UltraLabel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.ultraLabel46 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactIdentificationNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel25 = new System.Windows.Forms.Panel();
            this.txtContactMobile = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel36 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactHomeTel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel43 = new Infragistics.Win.Misc.UltraLabel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.ultraLabel45 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel24 = new System.Windows.Forms.Panel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactOfficeTel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel18 = new System.Windows.Forms.Panel();
            this.ultraLabel47 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel17 = new System.Windows.Forms.Panel();
            this.cbbContactPrefix = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel48 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtPersonalTaxCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel10 = new System.Windows.Forms.Panel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.txtBusinessIssueBy = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel9 = new System.Windows.Forms.Panel();
            this.dtBusinessIssuedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.txtWebsite = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.txtFax = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel15 = new System.Windows.Forms.Panel();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.txtEmail = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel13 = new System.Windows.Forms.Panel();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.txtTel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel8 = new System.Windows.Forms.Panel();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.txtBusinessRegistrationNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel7 = new System.Windows.Forms.Panel();
            this.cbbShareHolderCategoryID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.txtBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.dtBookIssuedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.ultraLabel30 = new Infragistics.Win.Misc.UltraLabel();
            this.txtBankAccount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ultraLabel31 = new Infragistics.Win.Misc.UltraLabel();
            this.txtShareHolderName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ultraLabel32 = new Infragistics.Win.Misc.UltraLabel();
            this.txtShareHolderCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraOptionSet1 = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactIssueBy)).BeginInit();
            this.panel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactTitle)).BeginInit();
            this.panel27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactEmail)).BeginInit();
            this.panel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtContactIssueDate)).BeginInit();
            this.panel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactIdentificationNo)).BeginInit();
            this.panel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactMobile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactHomeTel)).BeginInit();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactAddress)).BeginInit();
            this.panel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactOfficeTel)).BeginInit();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).BeginInit();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbContactPrefix)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPersonalTaxCode)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBusinessIssueBy)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtBusinessIssuedDate)).BeginInit();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite)).BeginInit();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax)).BeginInit();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).BeginInit();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTel)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBusinessRegistrationNumber)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbShareHolderCategoryID)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtBookIssuedDate)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccount)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtShareHolderName)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtShareHolderCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            appearance1.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance1;
            this.btnSave.Location = new System.Drawing.Point(480, 670);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 338;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            appearance2.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.btnClose.Appearance = appearance2;
            this.btnClose.Location = new System.Drawing.Point(579, 670);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 339;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.ultraButton1);
            this.panel1.Controls.Add(this.ultraGroupBox3);
            this.panel1.Controls.Add(this.ultraGroupBox2);
            this.panel1.Controls.Add(this.ultraGroupBox1);
            this.panel1.Controls.Add(this.ultraOptionSet1);
            this.panel1.Location = new System.Drawing.Point(1, -2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(662, 666);
            this.panel1.TabIndex = 896;
            // 
            // ultraButton1
            // 
            this.ultraButton1.Location = new System.Drawing.Point(492, 3);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(167, 23);
            this.ultraButton1.TabIndex = 900;
            this.ultraButton1.Text = "Chọn từ dang sách đăng ký...";
            this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.uGrid2);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox3.Location = new System.Drawing.Point(3, 488);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(656, 176);
            this.ultraGroupBox3.TabIndex = 899;
            this.ultraGroupBox3.Text = "Cổ phần ban đầu";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel1
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel1.Appearance = appearance3;
            this.ultraLabel1.Location = new System.Drawing.Point(489, 19);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(161, 19);
            this.ultraLabel1.TabIndex = 10;
            this.ultraLabel1.Text = "Mệnh giá một cổ phần : 10.000";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.panel23);
            this.ultraGroupBox2.Controls.Add(this.panel19);
            this.ultraGroupBox2.Controls.Add(this.panel27);
            this.ultraGroupBox2.Controls.Add(this.panel22);
            this.ultraGroupBox2.Controls.Add(this.panel21);
            this.ultraGroupBox2.Controls.Add(this.panel25);
            this.ultraGroupBox2.Controls.Add(this.panel20);
            this.ultraGroupBox2.Controls.Add(this.panel24);
            this.ultraGroupBox2.Controls.Add(this.panel18);
            this.ultraGroupBox2.Controls.Add(this.panel17);
            this.ultraGroupBox2.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox2.Location = new System.Drawing.Point(3, 278);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(656, 210);
            this.ultraGroupBox2.TabIndex = 898;
            this.ultraGroupBox2.Text = "Người liên hệ";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.Transparent;
            this.panel23.Controls.Add(this.ultraLabel34);
            this.panel23.Controls.Add(this.txtContactIssueBy);
            this.panel23.Location = new System.Drawing.Point(430, 113);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(220, 31);
            this.panel23.TabIndex = 17;
            // 
            // ultraLabel34
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel34.Appearance = appearance4;
            this.ultraLabel34.Location = new System.Drawing.Point(0, 6);
            this.ultraLabel34.Name = "ultraLabel34";
            this.ultraLabel34.Size = new System.Drawing.Size(49, 19);
            this.ultraLabel34.TabIndex = 6;
            this.ultraLabel34.Text = "  Nơi cấp";
            // 
            // txtContactIssueBy
            // 
            this.txtContactIssueBy.Location = new System.Drawing.Point(59, 5);
            this.txtContactIssueBy.Name = "txtContactIssueBy";
            this.txtContactIssueBy.Size = new System.Drawing.Size(160, 21);
            this.txtContactIssueBy.TabIndex = 8;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Transparent;
            this.panel19.Controls.Add(this.ultraLabel42);
            this.panel19.Controls.Add(this.txtContactTitle);
            this.panel19.Location = new System.Drawing.Point(9, 50);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(639, 31);
            this.panel19.TabIndex = 16;
            // 
            // ultraLabel42
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel42.Appearance = appearance5;
            this.ultraLabel42.Location = new System.Drawing.Point(7, 7);
            this.ultraLabel42.Name = "ultraLabel42";
            this.ultraLabel42.Size = new System.Drawing.Size(61, 19);
            this.ultraLabel42.TabIndex = 6;
            this.ultraLabel42.Text = "Chức danh";
            // 
            // txtContactTitle
            // 
            this.txtContactTitle.Location = new System.Drawing.Point(95, 3);
            this.txtContactTitle.Name = "txtContactTitle";
            this.txtContactTitle.Size = new System.Drawing.Size(544, 21);
            this.txtContactTitle.TabIndex = 5;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.Transparent;
            this.panel27.Controls.Add(this.ultraLabel33);
            this.panel27.Controls.Add(this.txtContactEmail);
            this.panel27.Location = new System.Drawing.Point(10, 176);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(639, 31);
            this.panel27.TabIndex = 15;
            // 
            // ultraLabel33
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel33.Appearance = appearance6;
            this.ultraLabel33.Location = new System.Drawing.Point(8, 7);
            this.ultraLabel33.Name = "ultraLabel33";
            this.ultraLabel33.Size = new System.Drawing.Size(41, 19);
            this.ultraLabel33.TabIndex = 6;
            this.ultraLabel33.Text = "Email";
            // 
            // txtContactEmail
            // 
            this.txtContactEmail.Location = new System.Drawing.Point(94, 3);
            this.txtContactEmail.Name = "txtContactEmail";
            this.txtContactEmail.Size = new System.Drawing.Size(544, 21);
            this.txtContactEmail.TabIndex = 1001;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.Transparent;
            this.panel22.Controls.Add(this.dtContactIssueDate);
            this.panel22.Controls.Add(this.ultraLabel35);
            this.panel22.Location = new System.Drawing.Point(215, 112);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(215, 31);
            this.panel22.TabIndex = 7;
            // 
            // dtContactIssueDate
            // 
            appearance7.TextHAlignAsString = "Center";
            appearance7.TextVAlignAsString = "Middle";
            this.dtContactIssueDate.Appearance = appearance7;
            this.dtContactIssueDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtContactIssueDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtContactIssueDate.Location = new System.Drawing.Point(94, 4);
            this.dtContactIssueDate.MaskInput = "";
            this.dtContactIssueDate.Name = "dtContactIssueDate";
            this.dtContactIssueDate.Size = new System.Drawing.Size(118, 21);
            this.dtContactIssueDate.TabIndex = 7;
            this.dtContactIssueDate.Value = null;
            // 
            // ultraLabel35
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel35.Appearance = appearance8;
            this.ultraLabel35.Location = new System.Drawing.Point(6, 8);
            this.ultraLabel35.Name = "ultraLabel35";
            this.ultraLabel35.Size = new System.Drawing.Size(62, 19);
            this.ultraLabel35.TabIndex = 6;
            this.ultraLabel35.Text = "Ngày cấp";
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.Transparent;
            this.panel21.Controls.Add(this.ultraLabel46);
            this.panel21.Controls.Add(this.txtContactIdentificationNo);
            this.panel21.Location = new System.Drawing.Point(10, 112);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(198, 31);
            this.panel21.TabIndex = 6;
            // 
            // ultraLabel46
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel46.Appearance = appearance9;
            this.ultraLabel46.Location = new System.Drawing.Point(8, 8);
            this.ultraLabel46.Name = "ultraLabel46";
            this.ultraLabel46.Size = new System.Drawing.Size(78, 19);
            this.ultraLabel46.TabIndex = 6;
            this.ultraLabel46.Text = "Số CMND/HC";
            // 
            // txtContactIdentificationNo
            // 
            this.txtContactIdentificationNo.Location = new System.Drawing.Point(94, 3);
            this.txtContactIdentificationNo.Name = "txtContactIdentificationNo";
            this.txtContactIdentificationNo.Size = new System.Drawing.Size(100, 21);
            this.txtContactIdentificationNo.TabIndex = 6;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.Transparent;
            this.panel25.Controls.Add(this.txtContactMobile);
            this.panel25.Controls.Add(this.ultraLabel36);
            this.panel25.Controls.Add(this.txtContactHomeTel);
            this.panel25.Controls.Add(this.ultraLabel43);
            this.panel25.Location = new System.Drawing.Point(216, 142);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(433, 31);
            this.panel25.TabIndex = 10;
            // 
            // txtContactMobile
            // 
            this.txtContactMobile.Location = new System.Drawing.Point(273, 4);
            this.txtContactMobile.Name = "txtContactMobile";
            this.txtContactMobile.Size = new System.Drawing.Size(161, 21);
            this.txtContactMobile.TabIndex = 17;
            // 
            // ultraLabel36
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel36.Appearance = appearance10;
            this.ultraLabel36.Location = new System.Drawing.Point(217, 5);
            this.ultraLabel36.Name = "ultraLabel36";
            this.ultraLabel36.Size = new System.Drawing.Size(45, 19);
            this.ultraLabel36.TabIndex = 18;
            this.ultraLabel36.Text = "Di động";
            // 
            // txtContactHomeTel
            // 
            this.txtContactHomeTel.Location = new System.Drawing.Point(93, 3);
            this.txtContactHomeTel.Name = "txtContactHomeTel";
            this.txtContactHomeTel.Size = new System.Drawing.Size(118, 21);
            this.txtContactHomeTel.TabIndex = 19;
            // 
            // ultraLabel43
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel43.Appearance = appearance11;
            this.ultraLabel43.Location = new System.Drawing.Point(5, 3);
            this.ultraLabel43.Name = "ultraLabel43";
            this.ultraLabel43.Size = new System.Drawing.Size(76, 19);
            this.ultraLabel43.TabIndex = 6;
            this.ultraLabel43.Text = "ĐT nhà riêng";
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.Transparent;
            this.panel20.Controls.Add(this.ultraLabel45);
            this.panel20.Controls.Add(this.txtContactAddress);
            this.panel20.Location = new System.Drawing.Point(10, 82);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(639, 31);
            this.panel20.TabIndex = 5;
            // 
            // ultraLabel45
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel45.Appearance = appearance12;
            this.ultraLabel45.Location = new System.Drawing.Point(8, 7);
            this.ultraLabel45.Name = "ultraLabel45";
            this.ultraLabel45.Size = new System.Drawing.Size(52, 19);
            this.ultraLabel45.TabIndex = 6;
            this.ultraLabel45.Text = "Địa chỉ";
            // 
            // txtContactAddress
            // 
            this.txtContactAddress.Location = new System.Drawing.Point(94, 3);
            this.txtContactAddress.Name = "txtContactAddress";
            this.txtContactAddress.Size = new System.Drawing.Size(545, 21);
            this.txtContactAddress.TabIndex = 5;
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.Transparent;
            this.panel24.Controls.Add(this.ultraLabel12);
            this.panel24.Controls.Add(this.txtContactOfficeTel);
            this.panel24.Location = new System.Drawing.Point(10, 142);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(199, 31);
            this.panel24.TabIndex = 9;
            // 
            // ultraLabel12
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel12.Appearance = appearance13;
            this.ultraLabel12.Location = new System.Drawing.Point(8, 6);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(67, 19);
            this.ultraLabel12.TabIndex = 10;
            this.ultraLabel12.Text = "ĐT cơ quan";
            // 
            // txtContactOfficeTel
            // 
            this.txtContactOfficeTel.Location = new System.Drawing.Point(94, 3);
            this.txtContactOfficeTel.Name = "txtContactOfficeTel";
            this.txtContactOfficeTel.Size = new System.Drawing.Size(99, 21);
            this.txtContactOfficeTel.TabIndex = 9;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Transparent;
            this.panel18.Controls.Add(this.ultraLabel47);
            this.panel18.Controls.Add(this.txtContactName);
            this.panel18.Location = new System.Drawing.Point(216, 19);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(433, 31);
            this.panel18.TabIndex = 2;
            // 
            // ultraLabel47
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel47.Appearance = appearance14;
            this.ultraLabel47.Location = new System.Drawing.Point(5, 8);
            this.ultraLabel47.Name = "ultraLabel47";
            this.ultraLabel47.Size = new System.Drawing.Size(76, 19);
            this.ultraLabel47.TabIndex = 6;
            this.ultraLabel47.Text = "Họ và Tên(*)";
            // 
            // txtContactName
            // 
            this.txtContactName.Location = new System.Drawing.Point(93, 4);
            this.txtContactName.Name = "txtContactName";
            this.txtContactName.Size = new System.Drawing.Size(340, 21);
            this.txtContactName.TabIndex = 1001;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Transparent;
            this.panel17.Controls.Add(this.cbbContactPrefix);
            this.panel17.Controls.Add(this.ultraLabel48);
            this.panel17.Location = new System.Drawing.Point(10, 20);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(197, 31);
            this.panel17.TabIndex = 1;
            // 
            // cbbContactPrefix
            // 
            this.cbbContactPrefix.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbContactPrefix.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbContactPrefix.Location = new System.Drawing.Point(94, 3);
            this.cbbContactPrefix.Name = "cbbContactPrefix";
            this.cbbContactPrefix.Size = new System.Drawing.Size(100, 21);
            this.cbbContactPrefix.TabIndex = 17;
            // 
            // ultraLabel48
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel48.Appearance = appearance15;
            this.ultraLabel48.Location = new System.Drawing.Point(8, 7);
            this.ultraLabel48.Name = "ultraLabel48";
            this.ultraLabel48.Size = new System.Drawing.Size(52, 19);
            this.ultraLabel48.TabIndex = 6;
            this.ultraLabel48.Text = "Xưng hô";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.panel5);
            this.ultraGroupBox1.Controls.Add(this.panel4);
            this.ultraGroupBox1.Controls.Add(this.panel10);
            this.ultraGroupBox1.Controls.Add(this.panel9);
            this.ultraGroupBox1.Controls.Add(this.panel16);
            this.ultraGroupBox1.Controls.Add(this.panel14);
            this.ultraGroupBox1.Controls.Add(this.panel15);
            this.ultraGroupBox1.Controls.Add(this.panel13);
            this.ultraGroupBox1.Controls.Add(this.panel8);
            this.ultraGroupBox1.Controls.Add(this.panel7);
            this.ultraGroupBox1.Controls.Add(this.panel12);
            this.ultraGroupBox1.Controls.Add(this.panel6);
            this.ultraGroupBox1.Controls.Add(this.panel11);
            this.ultraGroupBox1.Controls.Add(this.panel3);
            this.ultraGroupBox1.Controls.Add(this.panel2);
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 27);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(656, 245);
            this.ultraGroupBox1.TabIndex = 897;
            this.ultraGroupBox1.Text = "Thông tin cổ đông";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.Controls.Add(this.txtAddress);
            this.panel5.Controls.Add(this.ultraLabel6);
            this.panel5.Location = new System.Drawing.Point(216, 50);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(433, 31);
            this.panel5.TabIndex = 1004;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(93, 4);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(340, 21);
            this.txtAddress.TabIndex = 1001;
            // 
            // ultraLabel6
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel6.Appearance = appearance16;
            this.ultraLabel6.Location = new System.Drawing.Point(5, 7);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(46, 19);
            this.ultraLabel6.TabIndex = 6;
            this.ultraLabel6.Text = "Địa chỉ";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.Controls.Add(this.ultraLabel2);
            this.panel4.Controls.Add(this.txtPersonalTaxCode);
            this.panel4.Location = new System.Drawing.Point(10, 50);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(198, 30);
            this.panel4.TabIndex = 15;
            // 
            // ultraLabel2
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel2.Appearance = appearance17;
            this.ultraLabel2.Location = new System.Drawing.Point(8, 7);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(70, 19);
            this.ultraLabel2.TabIndex = 1003;
            this.ultraLabel2.Text = "Mã số thuế";
            // 
            // txtPersonalTaxCode
            // 
            this.txtPersonalTaxCode.Location = new System.Drawing.Point(94, 6);
            this.txtPersonalTaxCode.Name = "txtPersonalTaxCode";
            this.txtPersonalTaxCode.Size = new System.Drawing.Size(101, 21);
            this.txtPersonalTaxCode.TabIndex = 1002;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Transparent;
            this.panel10.Controls.Add(this.ultraLabel10);
            this.panel10.Controls.Add(this.txtBusinessIssueBy);
            this.panel10.Location = new System.Drawing.Point(430, 115);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(220, 28);
            this.panel10.TabIndex = 8;
            // 
            // ultraLabel10
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel10.Appearance = appearance18;
            this.ultraLabel10.Location = new System.Drawing.Point(3, 3);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(49, 19);
            this.ultraLabel10.TabIndex = 6;
            this.ultraLabel10.Text = "  Nơi cấp";
            // 
            // txtBusinessIssueBy
            // 
            this.txtBusinessIssueBy.Location = new System.Drawing.Point(58, 3);
            this.txtBusinessIssueBy.Name = "txtBusinessIssueBy";
            this.txtBusinessIssueBy.Size = new System.Drawing.Size(161, 21);
            this.txtBusinessIssueBy.TabIndex = 8;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Transparent;
            this.panel9.Controls.Add(this.dtBusinessIssuedDate);
            this.panel9.Controls.Add(this.ultraLabel11);
            this.panel9.Location = new System.Drawing.Point(215, 112);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(215, 31);
            this.panel9.TabIndex = 7;
            // 
            // dtBusinessIssuedDate
            // 
            appearance19.TextHAlignAsString = "Center";
            appearance19.TextVAlignAsString = "Middle";
            this.dtBusinessIssuedDate.Appearance = appearance19;
            this.dtBusinessIssuedDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtBusinessIssuedDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtBusinessIssuedDate.Location = new System.Drawing.Point(94, 3);
            this.dtBusinessIssuedDate.MaskInput = "";
            this.dtBusinessIssuedDate.Name = "dtBusinessIssuedDate";
            this.dtBusinessIssuedDate.Size = new System.Drawing.Size(121, 21);
            this.dtBusinessIssuedDate.TabIndex = 7;
            this.dtBusinessIssuedDate.Value = null;
            // 
            // ultraLabel11
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel11.Appearance = appearance20;
            this.ultraLabel11.Location = new System.Drawing.Point(6, 7);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(52, 19);
            this.ultraLabel11.TabIndex = 6;
            this.ultraLabel11.Text = "Ngày cấp";
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Transparent;
            this.panel16.Controls.Add(this.txtWebsite);
            this.panel16.Controls.Add(this.ultraLabel14);
            this.panel16.Location = new System.Drawing.Point(216, 204);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(433, 31);
            this.panel16.TabIndex = 14;
            // 
            // txtWebsite
            // 
            this.txtWebsite.Location = new System.Drawing.Point(93, 4);
            this.txtWebsite.Name = "txtWebsite";
            this.txtWebsite.Size = new System.Drawing.Size(340, 21);
            this.txtWebsite.TabIndex = 13;
            // 
            // ultraLabel14
            // 
            appearance21.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel14.Appearance = appearance21;
            this.ultraLabel14.Location = new System.Drawing.Point(5, 6);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(42, 19);
            this.ultraLabel14.TabIndex = 6;
            this.ultraLabel14.Text = "Website";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Transparent;
            this.panel14.Controls.Add(this.ultraLabel15);
            this.panel14.Controls.Add(this.txtFax);
            this.panel14.Location = new System.Drawing.Point(217, 173);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(432, 31);
            this.panel14.TabIndex = 12;
            // 
            // ultraLabel15
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel15.Appearance = appearance22;
            this.ultraLabel15.Location = new System.Drawing.Point(4, 6);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(28, 19);
            this.ultraLabel15.TabIndex = 6;
            this.ultraLabel15.Text = "Fax";
            // 
            // txtFax
            // 
            this.txtFax.Location = new System.Drawing.Point(92, 6);
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(340, 21);
            this.txtFax.TabIndex = 12;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Transparent;
            this.panel15.Controls.Add(this.ultraLabel21);
            this.panel15.Controls.Add(this.txtEmail);
            this.panel15.Location = new System.Drawing.Point(11, 204);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(197, 31);
            this.panel15.TabIndex = 13;
            // 
            // ultraLabel21
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel21.Appearance = appearance23;
            this.ultraLabel21.Location = new System.Drawing.Point(7, 9);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(48, 19);
            this.ultraLabel21.TabIndex = 6;
            this.ultraLabel21.Text = "Email";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(93, 4);
            this.txtEmail.Multiline = true;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(101, 21);
            this.txtEmail.TabIndex = 13;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Transparent;
            this.panel13.Controls.Add(this.ultraLabel22);
            this.panel13.Controls.Add(this.txtTel);
            this.panel13.Location = new System.Drawing.Point(11, 173);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(197, 31);
            this.panel13.TabIndex = 11;
            // 
            // ultraLabel22
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel22.Appearance = appearance24;
            this.ultraLabel22.Location = new System.Drawing.Point(7, 6);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(61, 19);
            this.ultraLabel22.TabIndex = 6;
            this.ultraLabel22.Text = "Điện thoại";
            // 
            // txtTel
            // 
            this.txtTel.Location = new System.Drawing.Point(94, 4);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(100, 21);
            this.txtTel.TabIndex = 11;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Transparent;
            this.panel8.Controls.Add(this.ultraLabel23);
            this.panel8.Controls.Add(this.txtBusinessRegistrationNumber);
            this.panel8.Location = new System.Drawing.Point(10, 112);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(198, 31);
            this.panel8.TabIndex = 6;
            // 
            // ultraLabel23
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel23.Appearance = appearance25;
            this.ultraLabel23.Location = new System.Drawing.Point(8, 6);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(61, 19);
            this.ultraLabel23.TabIndex = 6;
            this.ultraLabel23.Text = "Số ĐKKD";
            // 
            // txtBusinessRegistrationNumber
            // 
            this.txtBusinessRegistrationNumber.Location = new System.Drawing.Point(95, 3);
            this.txtBusinessRegistrationNumber.Name = "txtBusinessRegistrationNumber";
            this.txtBusinessRegistrationNumber.Size = new System.Drawing.Size(99, 21);
            this.txtBusinessRegistrationNumber.TabIndex = 6;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Transparent;
            this.panel7.Controls.Add(this.cbbShareHolderCategoryID);
            this.panel7.Controls.Add(this.ultraLabel26);
            this.panel7.Location = new System.Drawing.Point(216, 81);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(434, 31);
            this.panel7.TabIndex = 4;
            // 
            // cbbShareHolderCategoryID
            // 
            editorButton1.Text = "Thêm";
            this.cbbShareHolderCategoryID.ButtonsRight.Add(editorButton1);
            this.cbbShareHolderCategoryID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbShareHolderCategoryID.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbShareHolderCategoryID.Location = new System.Drawing.Point(93, 3);
            this.cbbShareHolderCategoryID.Name = "cbbShareHolderCategoryID";
            this.cbbShareHolderCategoryID.NullText = "<Chọn dữ liệu>";
            this.cbbShareHolderCategoryID.Size = new System.Drawing.Size(339, 22);
            this.cbbShareHolderCategoryID.TabIndex = 1003;
            this.cbbShareHolderCategoryID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbShareHolderCategoryID_EditorButtonClick);
            // 
            // ultraLabel26
            // 
            appearance26.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel26.Appearance = appearance26;
            this.ultraLabel26.Location = new System.Drawing.Point(5, 7);
            this.ultraLabel26.Name = "ultraLabel26";
            this.ultraLabel26.Size = new System.Drawing.Size(46, 19);
            this.ultraLabel26.TabIndex = 6;
            this.ultraLabel26.Text = "Nhóm";
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Transparent;
            this.panel12.Controls.Add(this.txtBankName);
            this.panel12.Controls.Add(this.ultraLabel27);
            this.panel12.Location = new System.Drawing.Point(216, 142);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(433, 31);
            this.panel12.TabIndex = 10;
            // 
            // txtBankName
            // 
            this.txtBankName.Location = new System.Drawing.Point(93, 4);
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Size = new System.Drawing.Size(339, 21);
            this.txtBankName.TabIndex = 13;
            // 
            // ultraLabel27
            // 
            appearance27.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel27.Appearance = appearance27;
            this.ultraLabel27.Location = new System.Drawing.Point(5, 9);
            this.ultraLabel27.Name = "ultraLabel27";
            this.ultraLabel27.Size = new System.Drawing.Size(64, 19);
            this.ultraLabel27.TabIndex = 6;
            this.ultraLabel27.Text = "Mở tại NH";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.Controls.Add(this.dtBookIssuedDate);
            this.panel6.Controls.Add(this.ultraLabel28);
            this.panel6.Location = new System.Drawing.Point(10, 81);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(199, 31);
            this.panel6.TabIndex = 3;
            // 
            // dtBookIssuedDate
            // 
            appearance28.TextHAlignAsString = "Center";
            appearance28.TextVAlignAsString = "Middle";
            this.dtBookIssuedDate.Appearance = appearance28;
            this.dtBookIssuedDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtBookIssuedDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtBookIssuedDate.Location = new System.Drawing.Point(95, 4);
            this.dtBookIssuedDate.MaskInput = "";
            this.dtBookIssuedDate.Name = "dtBookIssuedDate";
            this.dtBookIssuedDate.Size = new System.Drawing.Size(100, 21);
            this.dtBookIssuedDate.TabIndex = 8;
            this.dtBookIssuedDate.Value = null;
            // 
            // ultraLabel28
            // 
            appearance29.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel28.Appearance = appearance29;
            this.ultraLabel28.Location = new System.Drawing.Point(8, 3);
            this.ultraLabel28.Name = "ultraLabel28";
            this.ultraLabel28.Size = new System.Drawing.Size(72, 19);
            this.ultraLabel28.TabIndex = 6;
            this.ultraLabel28.Text = "Ngày vào sổ";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Transparent;
            this.panel11.Controls.Add(this.ultraLabel30);
            this.panel11.Controls.Add(this.txtBankAccount);
            this.panel11.Location = new System.Drawing.Point(10, 142);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(199, 31);
            this.panel11.TabIndex = 9;
            // 
            // ultraLabel30
            // 
            appearance30.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel30.Appearance = appearance30;
            this.ultraLabel30.Location = new System.Drawing.Point(8, 3);
            this.ultraLabel30.Name = "ultraLabel30";
            this.ultraLabel30.Size = new System.Drawing.Size(88, 19);
            this.ultraLabel30.TabIndex = 6;
            this.ultraLabel30.Text = "TK Ngân hàng";
            // 
            // txtBankAccount
            // 
            this.txtBankAccount.Location = new System.Drawing.Point(95, 4);
            this.txtBankAccount.Name = "txtBankAccount";
            this.txtBankAccount.Size = new System.Drawing.Size(99, 21);
            this.txtBankAccount.TabIndex = 9;
            this.txtBankAccount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBankAcount_KeyPress);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.ultraLabel31);
            this.panel3.Controls.Add(this.txtShareHolderName);
            this.panel3.Location = new System.Drawing.Point(216, 19);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(433, 31);
            this.panel3.TabIndex = 2;
            // 
            // ultraLabel31
            // 
            appearance31.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel31.Appearance = appearance31;
            this.ultraLabel31.Location = new System.Drawing.Point(3, 4);
            this.ultraLabel31.Name = "ultraLabel31";
            this.ultraLabel31.Size = new System.Drawing.Size(84, 19);
            this.ultraLabel31.TabIndex = 6;
            this.ultraLabel31.Text = "Tên cổ đông(*)";
            // 
            // txtShareHolderName
            // 
            this.txtShareHolderName.Location = new System.Drawing.Point(91, 4);
            this.txtShareHolderName.Name = "txtShareHolderName";
            this.txtShareHolderName.Size = new System.Drawing.Size(342, 21);
            this.txtShareHolderName.TabIndex = 1001;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.ultraLabel32);
            this.panel2.Controls.Add(this.txtShareHolderCode);
            this.panel2.Location = new System.Drawing.Point(10, 20);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(197, 31);
            this.panel2.TabIndex = 1;
            // 
            // ultraLabel32
            // 
            appearance32.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel32.Appearance = appearance32;
            this.ultraLabel32.Location = new System.Drawing.Point(8, 5);
            this.ultraLabel32.Name = "ultraLabel32";
            this.ultraLabel32.Size = new System.Drawing.Size(77, 19);
            this.ultraLabel32.TabIndex = 6;
            this.ultraLabel32.Text = "Mã cổ đông(*)";
            // 
            // txtShareHolderCode
            // 
            this.txtShareHolderCode.Location = new System.Drawing.Point(94, 3);
            this.txtShareHolderCode.Name = "txtShareHolderCode";
            this.txtShareHolderCode.Size = new System.Drawing.Size(100, 21);
            this.txtShareHolderCode.TabIndex = 1000;
            // 
            // ultraOptionSet1
            // 
            this.ultraOptionSet1.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem1.DataValue = "Default Item";
            valueListItem1.DisplayText = "1.Cá Nhân";
            valueListItem2.DataValue = "ValueListItem1";
            valueListItem2.DisplayText = "2. Tổ Chức";
            this.ultraOptionSet1.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.ultraOptionSet1.Location = new System.Drawing.Point(3, 3);
            this.ultraOptionSet1.Name = "ultraOptionSet1";
            this.ultraOptionSet1.Size = new System.Drawing.Size(161, 18);
            this.ultraOptionSet1.TabIndex = 891;
            this.ultraOptionSet1.ValueChanged += new System.EventHandler(this.ultraOptionSet1_ValueChanged_1);
            // 
            // uGrid2
            // 
            this.uGrid2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid2.Location = new System.Drawing.Point(3, 16);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(650, 157);
            this.uGrid2.TabIndex = 11;
            this.uGrid2.Text = "ultraGrid1";
            this.uGrid2.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // FEMShareHolderDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 707);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FEMShareHolderDetail";
            this.Text = "Cổ đông";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactIssueBy)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactTitle)).EndInit();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactEmail)).EndInit();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtContactIssueDate)).EndInit();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactIdentificationNo)).EndInit();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactMobile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactHomeTel)).EndInit();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactAddress)).EndInit();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactOfficeTel)).EndInit();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).EndInit();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbContactPrefix)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPersonalTaxCode)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBusinessIssueBy)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtBusinessIssuedDate)).EndInit();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax)).EndInit();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).EndInit();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTel)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBusinessRegistrationNumber)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbShareHolderCategoryID)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtBookIssuedDate)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccount)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtShareHolderName)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtShareHolderCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private System.Windows.Forms.Panel panel19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel42;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactTitle;
        private System.Windows.Forms.Panel panel27;
        private Infragistics.Win.Misc.UltraLabel ultraLabel33;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactEmail;
        private Infragistics.Win.Misc.UltraLabel ultraLabel34;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactIssueBy;
        private System.Windows.Forms.Panel panel22;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtContactIssueDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel35;
        private System.Windows.Forms.Panel panel21;
        private Infragistics.Win.Misc.UltraLabel ultraLabel46;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactIdentificationNo;
        private System.Windows.Forms.Panel panel25;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactMobile;
        private Infragistics.Win.Misc.UltraLabel ultraLabel36;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactHomeTel;
        private Infragistics.Win.Misc.UltraLabel ultraLabel43;
        private System.Windows.Forms.Panel panel20;
        private Infragistics.Win.Misc.UltraLabel ultraLabel45;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactAddress;
        private System.Windows.Forms.Panel panel24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactOfficeTel;
        private System.Windows.Forms.Panel panel18;
        private Infragistics.Win.Misc.UltraLabel ultraLabel47;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactName;
        private System.Windows.Forms.Panel panel17;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbContactPrefix;
        private Infragistics.Win.Misc.UltraLabel ultraLabel48;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private System.Windows.Forms.Panel panel5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAddress;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private System.Windows.Forms.Panel panel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPersonalTaxCode;
        private System.Windows.Forms.Panel panel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBusinessIssueBy;
        private System.Windows.Forms.Panel panel9;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtBusinessIssuedDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private System.Windows.Forms.Panel panel16;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtWebsite;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private System.Windows.Forms.Panel panel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFax;
        private System.Windows.Forms.Panel panel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtEmail;
        private System.Windows.Forms.Panel panel13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTel;
        private System.Windows.Forms.Panel panel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBusinessRegistrationNumber;
        private System.Windows.Forms.Panel panel7;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbShareHolderCategoryID;
        private Infragistics.Win.Misc.UltraLabel ultraLabel26;
        private System.Windows.Forms.Panel panel12;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel27;
        private System.Windows.Forms.Panel panel6;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtBookIssuedDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel28;
        private System.Windows.Forms.Panel panel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel30;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankAccount;
        private System.Windows.Forms.Panel panel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel31;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtShareHolderName;
        private System.Windows.Forms.Panel panel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel32;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtShareHolderCode;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet ultraOptionSet1;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private System.Windows.Forms.Panel panel23;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
    }
}