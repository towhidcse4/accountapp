﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.fEMShareHolder
{
    public class database
    {
        //Đăng ký mua cổ phần
        public const string EMRegistrations_TableName = "EMRegistrations";
        //Nhà đầu tư
        public const string Investor_TableName = "Investor";
        //cổ đông
        public const string EMShareHolder_TableName = "EMShareHolder";
        //dự toán
        public const string EMEstimate_TableName = "EMEstimate";
        //Chi tiết cấp phát
        public const string EMAllocationDetail_TableName = "EMAllocationDetail";
        //Yêu cầu cấp chi phí
        public const string EMAllocationRequired_TableName = "EMAllocationRequired";
        //Chi tiết yêu cầu cấp chi phí
        public const string EMAllocationRequiredDetail_TableName = "EMAllocationRequiredDetail";
       
        static object BuildConfig()
        {
            //List<TemplateColumn> dstemplatecolums = new List<TemplateColumn>();
            Dictionary<string, Dictionary<string, TemplateColumn>> dicTemp = new Dictionary<string, Dictionary<string, TemplateColumn>>();
            string nameTable = string.Empty;
            List<string> strColumnName, strColumnCaption, strColumnToolTip = new List<string>();
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb = new List<bool>();
            List<int> intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition = new List<int>();
            List<int> VTbolIsVisible, VTbolIsVisibleCbb = new List<int>();


            // Loại cổ phần
            #region EMRegistrations
            nameTable = EMRegistrations_TableName;
            strColumnName = new List<string>() { "ID", "RegistrationCode", "RegistrationName", "EMPublishPeriodID", "EMPublishPeriodName", "RegistrationGroupID", "TypeID", "Address", "Tel", "Fax", "Email", "Website", "BusinessRegistrationNumber", "BusinessRegistrationIssueBy", "BusinessRegistrationIssueDate", "BankAccount", "BankName", "ContactTaxCode", "ContactName", "ContactTile", "ContactPrefix", "ContactMobile", "ContactEmail", "ContactOfficeTel", "ContactHomeTel", "ContactAddress", "IsPersonal", "ContactIdentificationNo", "ContactIssueDate", "ContactIssueBy", "Status", "EMShareHolderID", "TotalQuantityBuyable", "TotalQuantityRegisted", "TotalQuantityApproved", "TotalQuantityBought", "TotalAmountBought", "Recorded" };
            strColumnCaption = strColumnToolTip = new List<string>() { "ID", "Mã đăng ký", "Tên đăng ký", "EMPublishPeriodID", "EMPublishPeriodName", "RegistrationGroupID", "TypeID", "Address", "Tel", "Fax", "Email", "Website", "BusinessRegistrationNumber", "BusinessRegistrationIssueBy", "BusinessRegistrationIssueDate", "BankAccount", "BankName", "ContactTaxCode", "Tên", "ContactTile", "ContactPrefix", "ContactMobile", "ContactEmail", "ContactOfficeTel", "ContactHomeTel", "ContactAddress", "IsPersonal", "ContactIdentificationNo", "ContactIssueDate", "ContactIssueBy", "Status", "EMShareHolderID", "SL được mua", "SL đăng ký", "SL được duyệt", "SL đã nộp tiền", "Số tiền đã nộp", "Recorded" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() {1, 2};    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 1, 2 };
            for (int i = 0; i < 38; i++)
            {
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            dicTemp.Add(nameTable, CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition));
            #endregion

            // Nhà đầu tư
            #region Investor
            nameTable = EMShareHolder_TableName;
            strColumnName = new List<string>() { "ID", "InvestorCode", "InvestorName", "InvestorCategoryID", "InvestorCategoryIDVIEW", "Address", "Tel", "Fax", "Email", "Website",
                                                "BusinessRegistrationNumber", "IssueDate", "IssueBy", "BankAccount", "BankName", "TaxCode", "ContactName", "ContactTitle", "ContactPrefix", "ContactMobile",
                                                "ContactEmail", "ContactOfficeTel", "ContactAddress", "InvestorType", "IdenticationNumber", "ContactIssueDate", "ContactIssueBy", "IsActive" };
            strColumnCaption = strColumnToolTip = new List<string>() {  "ID", "Mã nhà đầu tư", "Tên nhà đầu tư", "InvestorCategoryID", "Nhóm nhà đầu tư", "Địa chỉ", "Tel", "Fax", "Email", "Website",
                                                "BusinessRegistrationNumber", "IssueDate", "IssueBy", "BankAccount", "BankName", "TaxCode", "ContactName", "ContactTitle", "ContactPrefix", "ContactMobile",
                                                "ContactEmail", "Điện thoại cơ quan", "ContactAddress", "InvestorType", "IdenticationNumber", "ContactIssueDate", "ContactIssueBy", "IsActive"};
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() { 1, 2 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 1, 2 };
            for (int i = 0; i < 28; i++)
            {
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            dicTemp.Add(nameTable, CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition));
            #endregion

            // cổ đông
            #region EMShareHolder
            nameTable = EMShareHolder_TableName;
            strColumnName = new List<string>() { "ID", "ShareHolderCategoryID", "ShareHolderCode", "ShareHolderName", "PersonalTaxCode", "BookIssuedDate", "BusinessRegistrationNumber", "BusinessIssuedDate", "BusinessIssueBy", "BankAccount", "BankName", "Tel", "Fax", "Email", "Website", "Address", "ContactPrefix", "ContactName", "ContactTitle", "ContactIdentificationNo", "ContactIssueDate", "ContactIssueBy", "ContactOfficeTel", "ContactHomeTel", "ContactMobile", "ContactEmail", "ContactAddress", "IsPersonal", "TypeID", "TotalShare", "IsActive" };
            strColumnCaption = strColumnToolTip = new List<string>() { "ID cổ đông", "ID nhóm cổ đông", "Mã cổ đông", "Tên cổ đông", "Mã số thuế cá nhân", "Ngày cấp sổ", "Số đăng ký kinh doanh", "Ngày cấp đăng ký kinh doanh ", "Nơi cấp đăng ký kinh doanh", "TK ngân hàng", "Tên ngân hàng", "Số điện thoại tổ chức", "Số phách tổ chức", "Email tổ chức", "Website tổ chức", "Địa chỉ", "Xưng hô", "Người liên hệ", "Chức vụ ", "Số CMND người liên hệ", "Ngày cấp CMND", "Nơi cấp CMND", "ĐT cơ quan", "ĐT nhà riêng", "ĐT di động", "Email người liên hệ", "Địa chỉ người liên hệ", "IsPersonal", "Loại chứng từ", "Số CP sở hữu", "Tình trạng hoạt động" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() { 2, 3 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 1, 2 };
            for (int i = 0; i < 31; i++)
            {
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            dicTemp.Add(nameTable, CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition));
            #endregion


            // dự toán
            #region EMEstimate
            nameTable = EMEstimate_TableName;
            strColumnName = new List<string>() { "ID", "BranchID", "EstimateBudgetYear", "Type", "DepartmentID", "DepartmentView", "BudgetItemID", "BudgetItemCode", "BudgetItemName", "SumAmount", "AmountMonth1", "AmountMonth2", "AmountMonth3", "AmountMonth4", "AmountMonth5", "AmountMonth6", "AmountMonth7", "AmountMonth8", "AmountMonth9", "AmountMonth10", "AmountMonth11", "AmountMonth12", "WarningLevel", "WarningLevelPercent" };
            strColumnCaption = strColumnToolTip = new List<string>() { "ID", "ID chi nhánh", "Năm", "Type", "ID Phòng ban", "Phòng ban", "ID Mục thu/chi", "Mã mục thu/chi ", " Mục thu/chi", "Tổng tiền", "Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7 ", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12", "Ngưỡng cảnh báo", "% ngưỡng cảnh báo" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 1, 2 };
            for (int i = 0; i < 24; i++)
            {
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            dicTemp.Add(nameTable, CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition));
            #endregion

            // Ci tiêt cấp phát
            #region EMAllocationDetail
            nameTable = EMAllocationDetail_TableName;
            strColumnName = new List<string>() { "ID", "EMAllocationID", "DateView", "BudgetMonthView", "BudgetYearView", "BudgetItemName", "RequestAmountView", "AprovedAmountView", "Amount", "Description", "OrderPriority", "BudgetItemID" };
            strColumnCaption = strColumnToolTip = new List<string>() { "ID ", "ID cấp phát", "Ngày cấp phát", "Tháng", "Năm", "Mục Chi", "Số yêu cầu", "Số được duyệt", "Số cấp phát", "Ghi chú", "OrderPriority", "BudgetItemID" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() { 5, 6, 7, 8, 9 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 1, 2 };
            for (int i = 0; i < 12; i++)
            {
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            dicTemp.Add(nameTable, CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition));
            #endregion
            // Yêu cầu cấp chi phí.
            #region EMAllocationRequired
            nameTable = EMAllocationRequired_TableName;
            strColumnName = new List<string>() { "ID", "BranchID", "RequestDate", "AprovedDate", "BudgetMonth", "BudgetYear", "RequestReason", "ApovedReason" };
            strColumnCaption = strColumnToolTip = new List<string>() { "ID ", "BranchID", "Ngày yêu cầu ", "Ngày duyệt cấp phát", "Tháng", "Năm", "Lý do", "Diễn giải" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() { 2, 4, 5, 7 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 1, 2 };
            for (int i = 0; i < 8; i++)
            {
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            dicTemp.Add(nameTable, CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition));
            #endregion
         
            // Chi tiết Yêu cầu cấp chi phí.
            #region EMAllocationRequiredDetail
            nameTable = EMAllocationRequiredDetail_TableName;
            strColumnName = new List<string>() { "ID", "EMAllocationRequiredID", "BudgetItemID", "BudgetItemName", "RequestAmount", "AprovedAmount", "Amount", "RestAmount", "RequestDescription", "AprovedDescription", "OrderPriority" };
            strColumnCaption = strColumnToolTip = new List<string>() { "ID ", "EMAllocationRequiredID", "BudgetItemID ", "Mục chi", "Số yêu cầu", "Số được duyệt", "Số cấp phát" , "Số còn lại" , "Ghi chú", "AprovedDescription", "OrderPriority" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() { 3, 4, 8 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 1, 2 };
            for (int i = 0; i < 11; i++)
            {
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            dicTemp.Add(nameTable, CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition));
            #endregion
         

            return dicTemp;
        }
        static Dictionary<string, TemplateColumn> CreateData(List<string> strColumnName, List<string> strColumnCaption, List<string> strColumnToolTip,
       List<bool> bolIsReadOnly, List<bool> bolIsVisible, List<bool> bolIsVisibleCbb,
       List<int> intColumnWidth, List<int> intColumnMaxWidth, List<int> intColumnMinWidth, List<int> intVisiblePosition)
        {
            Dictionary<string, TemplateColumn> temp = new Dictionary<string, TemplateColumn>();
            for (int i = 0; i < strColumnName.Count; i++)
            {
                TemplateColumn tcTemp = new TemplateColumn();
                tcTemp.ColumnName = strColumnName[i];
                tcTemp.ColumnCaption = strColumnCaption[i];
                tcTemp.ColumnToolTip = strColumnToolTip[i];
                tcTemp.IsReadOnly = bolIsReadOnly[i];
                tcTemp.IsVisible = bolIsVisible[i];
                tcTemp.IsVisibleCbb = bolIsVisibleCbb[i];
                tcTemp.ColumnWidth = intColumnWidth[i];
                tcTemp.ColumnMaxWidth = intColumnMaxWidth[i];
                tcTemp.ColumnMinWidth = intColumnMinWidth[i];
                tcTemp.VisiblePosition = intVisiblePosition[i];
                temp.Add(strColumnName[i], tcTemp);
            }
            return temp;
        }
    }
}
