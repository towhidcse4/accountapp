﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.Linq;
using Accounting.Model;




namespace Accounting
{
    public partial class FEMShareHolder : CustormForm
    {
        #region khai báo
        private readonly IEMShareHolderService _IEMShareHolderService;
        //private readonly IEMShareHolderDetailService _IEMShareHolderService;
        private readonly IStockCategoryService _IStockCategoryService;
        private readonly IShareHolderGroupService _IShareHolderGroupService;
        private readonly IBankService _IBankService;
        private readonly IEMShareHolderTransactionService _IEMShareHolderTransactionService;
        public static bool isClose = true;

        List<StockCategory> ListStockCategory = new List<StockCategory>();
        List<EMShareHolder> ListEMShareHolder = new List<EMShareHolder>();
        List<ShareHolderGroup> ListShareHolderGroup = new List<ShareHolderGroup>();
        List<Bank> dsBank = new List<Bank>();
        List<EMShareHolderTransaction> ListEMShareHolderTransaction = new List<EMShareHolderTransaction>();

        #endregion

        #region khởi tạo
        public FEMShareHolder()
        {
            WaitingFrm.StartWaiting();
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.CenterToScreen();
            #endregion

            #region Thiết lập ban đầu cho Form
            _IEMShareHolderService = IoC.Resolve<IEMShareHolderService>();
            _IStockCategoryService = IoC.Resolve<IStockCategoryService>();
            _IEMShareHolderTransactionService = IoC.Resolve<IEMShareHolderTransactionService>();
            this.uGrid1.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            this.uGrid2.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            this.uGrid3.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
            WaitingFrm.StopWaiting();
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid = true)
        {
            #region Lấy dữ liệu từ CSDL
            List<EMShareHolder> List = _IEMShareHolderService.GetAll();
            List<StockCategory> List1 = _IStockCategoryService.GetAll();
            List<EMShareHolderTransaction> List2 = _IEMShareHolderTransactionService.GetAll();

            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid1.DataSource = List.ToArray();
            //uGrid2.DataSource = List1.ToArray();
            //uGrid3.DataSource = List2.ToArray();


            if (uGrid1.Rows.Count > 0)
            {
                uGrid1.Rows[0].Selected = true;
            }

            if (configGrid) ConfigGrid(uGrid1);
            if (configGrid) Config(uGrid2);
            if (configGrid) Config1(uGrid3);
            #endregion
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click_1(object sender, EventArgs e)
        {
            addFunction();
        }
        private void tsmEdit_Click_1(object sender, EventArgs e)
        {
            editFunction();
        }
        private void tsmDelete_Click_1(object sender, EventArgs e)
        {
            deleteFunction();
        }

        private void tmsReLoad_Click_1(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }

        #endregion

        #region Button
        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            addFunction();
        }
        private void btnEdit_Click_1(object sender, EventArgs e)
        {
            editFunction();
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            deleteFunction();
        }
        #endregion

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid1, cms4Grid);
        }

        private void uGrid_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {

        }
        private void uGrid1_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            editFunction();
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        void addFunction()
        {

            new FEMShareHolderDetail().ShowDialog(this);
            if (!FEMShareHolderDetail.isClose) LoadDuLieu();
            //uGrid2.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
            //uGrid2.DisplayLayout.Override.TemplateAddRowPrompt = resSystem.MSG_HeThong_07;
            //uGrid2.DisplayLayout.Override.SpecialRowSeparator = SpecialRowSeparator.None;
        }


        /// Nghiệp vụ Edit
        /// </summary>
        void editFunction()
        {

            if (uGrid1.Selected.Rows.Count > 0)
            {
                EMShareHolder temp = uGrid1.Selected.Rows[0].ListObject as EMShareHolder;
                test1(temp);

                new FEMShareHolderDetail(temp).ShowDialog(this);
                if (!FEMShareHolderDetail.isClose) LoadDuLieu();
            }
            else
                Accounting.TextMessage.MSG.Error(Accounting.TextMessage.resSystem.MSG_System_04);
        }


        /// Nghiệp vụ Delete
        /// </summary>
        void deleteFunction()
        {
            if (uGrid1.Selected.Rows.Count > 0)
            {
                EMShareHolder temp = uGrid1.Selected.Rows[0].ListObject as EMShareHolder;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, temp.ShareHolderName)) == DialogResult.Yes)
                {
                    _IEMShareHolderService.BeginTran();
                    //List<EMShareHolderTransaction> listGenTemp = _IEMShareHolderTransactionService.Query.Where(p => p.EMShareHolderID == temp.ID).ToList();
                    List<EMShareHolderTransaction> listGenTemp = _IEMShareHolderTransactionService.GetListEMShareHolderTransactionEMShareHolder(temp.ID);
                    foreach (var shareholdertransaction in listGenTemp)
                    {
                        _IEMShareHolderTransactionService.Delete(shareholdertransaction);
                    }
                    _IEMShareHolderService.Delete(temp);
                    _IEMShareHolderService.CommitTran();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.EMShareHolder_TableName);
            uGrid1.DisplayLayout.Bands[0].Columns["BookIssuedDate"].Format = Constants.DdMMyyyy;
            uGrid1.DisplayLayout.Bands[0].Columns["BusinessIssuedDate"].Format = Constants.DdMMyyyy;
            uGrid1.DisplayLayout.Bands[0].Columns["ContactIssueDate"].Format = Constants.DdMMyyyy;

            Infragistics.Win.UltraWinGrid.UltraGridBand band = utralGrid.DisplayLayout.Bands[0];
            Infragistics.Win.UltraWinGrid.SummarySettings summaryQuantity = band.Summaries.Add("TotalShare", Infragistics.Win.UltraWinGrid.SummaryType.Sum, band.Columns["TotalShare"]);
            summaryQuantity.SummaryPosition = Infragistics.Win.UltraWinGrid.SummaryPosition.UseSummaryPositionColumn;
            summaryQuantity.DisplayFormat = "{0:N0}";
            summaryQuantity.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.BottomFixed;

        }
        #endregion
        // Load tab "Cổ phần sở hữu"
        private void Config(UltraGrid utralGrid)
        {
            List<TemplateColumn> dstemplatecolums = new List<TemplateColumn>();

            Dictionary<string, Dictionary<string, TemplateColumn>> dicTemp = new Dictionary<string, Dictionary<string, TemplateColumn>>();
            string nameTable = string.Empty;
            List<string> strColumnName, strColumnCaption, strColumnToolTip = new List<string>();
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb = new List<bool>();
            List<int> intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition = new List<int>();
            List<int> VTbolIsVisible, VTbolIsVisibleCbb = new List<int>();

            strColumnName = new List<string>() { "Loaicophan", "Hinhthucuudai", "Dieukienchuyennhuong", "Socophansohuu", "Tongmenhgiacophan" };
            strColumnCaption = strColumnToolTip = new List<string>() { "Loại cổ phần", "Hình thức ưu đãi", "Điều kiện chuyển nhượng", "Số cổ phần sở hữu", "Tổng mệnh giá cổ phần" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() { 0, 1, 2, 3, 4 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 1, 2 };
            for (int i = 0; i < 5; i++)
            {
                bolIsVisible.Add(VTbolIsVisible.Contains(i));
                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            dstemplatecolums = ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();

            Utils.ConfigGrid(utralGrid, string.Empty, dstemplatecolums, 0);
        }

        // config "Lịch sử giao dịch"
        private void Config1(UltraGrid utralGrid)
        {
            List<TemplateColumn> dstemplatecolums = new List<TemplateColumn>();

            Dictionary<string, Dictionary<string, TemplateColumn>> dicTemp = new Dictionary<string, Dictionary<string, TemplateColumn>>();
            string nameTable = string.Empty;
            List<string> strColumnName, strColumnCaption, strColumnToolTip = new List<string>();
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb = new List<bool>();
            List<int> intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition = new List<int>();
            List<int> VTbolIsVisible, VTbolIsVisibleCbb = new List<int>();

            strColumnName = new List<string>() { "Loaicophan", "Ngay", "Tang", "Giam", "Socophan", "Giatricophan", "Lydothaydoi" };
            strColumnCaption = strColumnToolTip = new List<string>() { "Loại cổ phần", "Ngày", "Tăng", "Giảm", "Số cổ phần", "Giá trị cổ phần", "Lý do thay đổi" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() { 0, 1, 2, 3, 4, 5, 6 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 1, 2 };
            for (int i = 0; i < 7; i++)
            {
                bolIsVisible.Add(VTbolIsVisible.Contains(i));
                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            dstemplatecolums = ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();

            Utils.ConfigGrid(utralGrid, string.Empty, dstemplatecolums, 0);
        }

        private void uGrid1_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGrid1.Selected.Rows.Count < 1) return;
            object listObject = uGrid1.Selected.Rows[0].ListObject;
            if (listObject == null) return;
            EMShareHolder cd = listObject as EMShareHolder;
            if (cd == null) return;
            //ugrid2
            test1(cd);
            // ugrid3
            test2(cd);

            #region Config Grid
            //hiển thị 1 band
            uGrid2.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            //tắt lọc cột
            uGrid2.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            uGrid2.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGrid2.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            uGrid2.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;
            //Hiện những dòng trống?
            uGrid2.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGrid2.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            //Fix Header
            uGrid2.DisplayLayout.UseFixedHeaders = true;

            UltraGridBand band = uGrid2.DisplayLayout.Bands[0];
            band.Summaries.Clear();


            //Thêm tổng số ở cột "số cổ phần sở hữu"  và cột "tổng mệnh giá cổ phần"
            SummarySettings summary = band.Summaries.Add("Socophansohuu", SummaryType.Sum, band.Columns["Socophansohuu"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

            SummarySettings summary1 = band.Summaries.Add("Tongmenhgiacophan", SummaryType.Sum, band.Columns["Tongmenhgiacophan"]);
            summary1.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary1.DisplayFormat = "{0:N0}";
            summary1.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            #endregion

        }
        public void test2(EMShareHolder cd)
        {
            List<tonghop> listTH = new List<tonghop>();

            //List<EMShareHolderTransaction> test2s = _IEMShareHolderTransactionService.Query.Where(k => k.EMShareHolderID == cd.ID).ToList();
            List<EMShareHolderTransaction> test2s = _IEMShareHolderTransactionService.GetListEMShareHolderTransactionEMShareHolder(cd.ID);
            foreach (var item in test2s)
            {
                var it = _IStockCategoryService.Getbykey(item.StockCategoryID);
                if (it == null)
                    continue;

                tonghop objTonghop = new tonghop();
                objTonghop.Ngay = item.TransactionDate;
                objTonghop.Tang = item.IncrementQuantity;
                objTonghop.Giam = item.DecrementQuantity;
                objTonghop.Lydothaydoi = Accounting.Core.Utils.SetNameTransactionType(item.TransactionType);
                objTonghop.Loaicophan = Accounting.Core.Utils.SetNameStockKind(it.StockKind);
                objTonghop.Socophan = cd.TotalShare;
                objTonghop.Giatricophan = 10000 * cd.TotalShare;

                listTH.Add(objTonghop);
            }
            uGrid3.DataSource = listTH.ToArray();
        }
        public void test1(EMShareHolder cd)
        {
            List<Cophansohuu> listcp = new List<Cophansohuu>();
            //List<EMShareHolderTransaction> test1s = _IEMShareHolderTransactionService.Query.Where(k => k.EMShareHolderID == cd.ID).ToList();
            List<EMShareHolderTransaction> test1s = _IEMShareHolderTransactionService.GetListEMShareHolderTransactionEMShareHolder(cd.ID);

            foreach (var item in test1s)
            {
                var it = _IStockCategoryService.Getbykey(item.StockCategoryID);
                if (it == null)
                    continue;
                Cophansohuu cophan = new Cophansohuu();

                cophan.Loaicophan = Accounting.Core.Utils.SetNameStockKind(it.StockKind);
                cophan.Hinhthucuudai = it.Incentives;
                cophan.Dieukienchuyennhuong = it.TransferCondition;
                cophan.Socophansohuu = cd.TotalShare;
                cophan.Tongmenhgiacophan = 10000 * cd.TotalShare;

                listcp.Add(cophan);
            }
            uGrid2.DataSource = listcp.ToArray();
        }

        private void FEMShareHolder_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid1.ResetText();
            uGrid1.ResetUpdateMode();
            uGrid1.ResetExitEditModeOnLeave();
            uGrid1.ResetRowUpdateCancelAction();
            uGrid1.DataSource = null;
            uGrid1.Layouts.Clear();
            uGrid1.ResetLayouts();
            uGrid1.ResetDisplayLayout();
            uGrid1.Refresh();
            uGrid1.ClearUndoHistory();
            uGrid1.ClearXsdConstraints();
            
            uGrid2.ResetText();
            uGrid2.ResetUpdateMode();
            uGrid2.ResetExitEditModeOnLeave();
            uGrid2.ResetRowUpdateCancelAction();
            uGrid2.DataSource = null;
            uGrid2.Layouts.Clear();
            uGrid2.ResetLayouts();
            uGrid2.ResetDisplayLayout();
            uGrid2.Refresh();
            uGrid2.ClearUndoHistory();
            uGrid2.ClearXsdConstraints();

            uGrid3.ResetText();
            uGrid3.ResetUpdateMode();
            uGrid3.ResetExitEditModeOnLeave();
            uGrid3.ResetRowUpdateCancelAction();
            uGrid3.DataSource = null;
            uGrid3.Layouts.Clear();
            uGrid3.ResetLayouts();
            uGrid3.ResetDisplayLayout();
            uGrid3.Refresh();
            uGrid3.ClearUndoHistory();
            uGrid3.ClearXsdConstraints();
        }

        private void FEMShareHolder_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
