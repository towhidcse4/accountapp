﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;

using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.Linq;

namespace Accounting
{
    public partial class FEMAllocation : CustormForm
    {
        #region Khai Báo
        private readonly IEMAllocationService _IEMAllocationService;
        private readonly IEMAllocationDetailService _IEMAllocationDetailService;
        private readonly IEMAllocationRequiredService _IEMAllocationRequiredService;
        private readonly IEMAllocationRequiredDetailService _IEMAllocationRequiredDetailService;
        private readonly IBudgetItemService _IBudgetItemService;
        #endregion

        #region khởi tạo
        public FEMAllocation()
        {
            WaitingFrm.StartWaiting();
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.CenterToScreen();
            #endregion

            #region Thiết lập ban đầu cho Form

            _IEMAllocationService = IoC.Resolve<IEMAllocationService>();
            _IEMAllocationDetailService = IoC.Resolve<IEMAllocationDetailService>();
            _IEMAllocationRequiredService = IoC.Resolve<IEMAllocationRequiredService>();
            _IEMAllocationRequiredDetailService = IoC.Resolve<IEMAllocationRequiredDetailService>();
            _IBudgetItemService = IoC.Resolve<IBudgetItemService>();

            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            this.uGridPosted.DisplayLayout.Override.SelectTypeRow = SelectType.Single;

            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
            WaitingFrm.StopWaiting();
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid = true)
        {
            #region Lấy dữ liệu từ CSDL
            List<EMAllocation> list = _IEMAllocationService.GetAll();
            List<EMAllocationDetail> list1 = _IEMAllocationDetailService.GetAll();
            List<EMAllocationRequired> list2 = _IEMAllocationRequiredService.GetAll();
            List<EMAllocationRequiredDetail> list3 = _IEMAllocationRequiredDetailService.GetAll();
            //List<BudgetItem> list4 = _IBudgetItemService.Query.Where(c => c.BudgetItemName.StartsWith("1") || c.BudgetItemName.StartsWith("2") || c.BudgetItemName.StartsWith("3") || c.BudgetItemName.StartsWith("4")).ToList();
            List<BudgetItem> list4 = _IBudgetItemService.GetListBudgetItemBudgetItemNameStartWith("1;2;3;4");
            #endregion

            #region Xử lý dữ liệu
            foreach (var item in list)
            {
                foreach (var ls2 in list1)
                {
                    if (item.ID == ls2.EMAllocationID)
                    {
                        item.AmountView = ls2.Amount;

                    }
                }
            }
            foreach (var item in list1)
            {
                foreach (var ls2 in list4)
                {
                    if (item.BudgetItemID == ls2.ID)
                    {
                        item.BudgetItemName = ls2.BudgetItemName;

                    }
                }
            }


            foreach (var item in list)
            {

                foreach (var ls2 in list2)
                {
                    if (item.EMAllocationRequiredID == ls2.ID)
                    {
                        foreach (var ls1 in list3)
                        {
                            if (ls2.ID == ls1.EMAllocationRequiredID)
                            {
                                item.RequestAmountView = ls1.RequestAmount;
                                item.AprovedAmountView = ls1.AprovedAmount;
                            }

                        }
                    }

                }
            }

            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list.ToArray();
            uGridPosted.DataSource = list1.ToArray();
            //uGrid3.DataSource = List2.ToArray();

            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }

            if (configGrid) ConfigGrid(uGrid);
            if (configGrid) ConfigGrid1(uGridPosted);

            #endregion
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            addFunction();
        }
        private void tsmEdit_Click(object sender, EventArgs e)
        {
            editFunction();
        }
        private void tsmDelete_Click(object sender, EventArgs e)
        {
            deleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }


        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            addFunction();
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            editFunction();
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            deleteFunction();
        }
        #endregion

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            editFunction();
        }



        // <summary>
        // Nghiệp vụ Add
        // </summary>
        void addFunction()
        {
            new FEMAllocationDetail().ShowDialog(this);
            if (!FEMAllocationDetail.isClose) LoadDuLieu();

        }


        // Nghiệp vụ Edit
        // </summary>
        void editFunction()
        {

            if (uGrid.Selected.Rows.Count > 0)
            {
                EMAllocation temp = uGrid.Selected.Rows[0].ListObject as EMAllocation;
                new FEMAllocationDetail(temp).ShowDialog(this);
                if (!FEMAllocationDetail.isClose) LoadDuLieu();
            }
            else
                Accounting.TextMessage.MSG.Error(Accounting.TextMessage.resSystem.MSG_System_04);
        }


        // Nghiệp vụ Delete
        // </summary>
        void deleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                EMAllocation temp = uGrid.Selected.Rows[0].ListObject as EMAllocation;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, temp)) == DialogResult.Yes)
                {
                    _IEMAllocationService.BeginTran();
                    //List<EMShareHolderTransaction> listGenTemp = _IEMShareHolderTransactionService.Query.Where(p => p.EMShareHolderID == temp.ID).ToList();
                    //foreach (var shareholdertransaction in listGenTemp)
                    //{
                    //    _IEMShareHolderTransactionService.Delete(shareholdertransaction);
                    //}
                    _IEMAllocationService.Delete(temp);
                    _IEMAllocationService.CommitTran();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion
        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.EMAllocation_TableName);
            //uGrid1.DisplayLayout.Bands[0].Columns["BookIssuedDate"].Format = Constants.DdMMyyyy;
        }

        void ConfigGrid1(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.EMAllocationDetail_TableName);
            //uGrid1.DisplayLayout.Bands[0].Columns["BookIssuedDate"].Format = Constants.DdMMyyyy;
        }




        #endregion

        private void FEMAllocation_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
            
            uGridPosted.ResetText();
            uGridPosted.ResetUpdateMode();
            uGridPosted.ResetExitEditModeOnLeave();
            uGridPosted.ResetRowUpdateCancelAction();
            uGridPosted.DataSource = null;
            uGridPosted.Layouts.Clear();
            uGridPosted.ResetLayouts();
            uGridPosted.ResetDisplayLayout();
            uGridPosted.Refresh();
            uGridPosted.ClearUndoHistory();
            uGridPosted.ClearXsdConstraints();
        }

        private void FEMAllocation_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
