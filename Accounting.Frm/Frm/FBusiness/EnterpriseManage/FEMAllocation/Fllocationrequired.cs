﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.ComponentModel;
using System.Linq;


namespace Accounting
{
    public partial class Fllocationrequired : CustormForm
    {
        #region khai báo
        private readonly IEMAllocationRequiredService _IEMAllocationRequiredService;
        public static List<EMAllocationRequired> dsEMRegistration = new List<EMAllocationRequired>();
        #endregion

        //EMRegistration _Select = new EMRegistration();
        //public EMRegistration EMRegistrationSelect
        //{
        //    get { return _Select; }
        //    set { _Select = value; }
        //}


        #region khởi tạo

        public Fllocationrequired()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            _IEMAllocationRequiredService = IoC.Resolve<IEMAllocationRequiredService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            Loaddulieu();
        }

        void Loaddulieu()
        {
            Loaddulieu(true);
        }

        private void Loaddulieu(bool configGrid)
        {
            #region Lấy dữ liệu từ CSDL
            List<EMAllocationRequired> list = _IEMAllocationRequiredService.GetAll();
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list.ToArray();
            //mặc định chọn dòng đầu tiên.

            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }

            if (configGrid) ConfigGrid(uGrid);

            #endregion
        }

        #endregion

        #region Nghiệp vụ
       
        //private void uGrid_MouseDown(object sender, MouseEventArgs e)
        //{
        //    System.Drawing.Point point = new System.Drawing.Point(e.X, e.Y);
        //    //lấy kiểu của đối tượng sender (đối tượng gây ra sự kiện)
        //    System.Type type = sender.GetType();
        //    UltraGrid uGrid = (UltraGrid)sender;
        //    //lấy UltraGridCell từ vị trí
        //    UltraGridCell cell = (UltraGridCell)((UltraGridBase)sender).DisplayLayout.UIElement.ElementFromPoint(point).GetContext(typeof(UltraGridCell));
        //    //nếu đang bấm chuột phải và đối tượng đang chọn khác null thì thao tác
        //    if (cell != null && cell.Row.Index >= 0)
        //    {
        //        uGrid.Rows[cell.Row.Index].Selected = true;
        //    }
        //}
        #endregion

        #region Utils
        //void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        //{//hàm chung
        //    Utils.ConfigGrid(utralGrid, fEMShareHolder.database.EMRegistrations_TableName, false);
        //}

        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {
            List<TemplateColumn> dstemplatecolums = new List<TemplateColumn>();

            Dictionary<string, Dictionary<string, TemplateColumn>> dicTemp = new Dictionary<string, Dictionary<string, TemplateColumn>>();
            string nameTable = string.Empty;
            List<string> strColumnName, strColumnCaption, strColumnToolTip = new List<string>();
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb = new List<bool>();
            List<int> intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition = new List<int>();
            List<int> VTbolIsVisible, VTbolIsVisibleCbb = new List<int>();

            strColumnName = new List<string>() { "ID", "BranchID", "RequestDate", "AprovedDate", "BudgetMonth", "BudgetYear", "RequestReason", "ApovedReason" };
            strColumnCaption = strColumnToolTip = new List<string>() { "ID ", "BranchID", "Ngày yêu cầu ", "Ngày duyệt cấp phát", "Tháng", "Năm", "Lý do", "Diễn giải" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() { 2, 4, 5, 7 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 1, 2 };
            for (int i = 0; i < 8; i++)
            {
                bolIsVisible.Add(VTbolIsVisible.Contains(i));
                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            dstemplatecolums = ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();

            Utils.ConfigGrid(utralGrid, fEMShareHolder.database.EMAllocationRequired_TableName, dstemplatecolums, 0);
        }
        #endregion

        private void BtnSave_Click(object sender, EventArgs e)
        {
            dsEMRegistration = ((BindingList<EMAllocationRequired>)uGrid.DataSource).ToList();
            Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
