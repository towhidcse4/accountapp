﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.Linq;


namespace Accounting
{
    public partial class FEMEstimate : CustormForm
    {
        #region khai báo
        private readonly IEMEstimateService _IEMEstimateService;
        private readonly IBudgetItemService _IBudgetItemService;
        private readonly IDepartmentService _IDepartmentService;

        //List<EMEstimate> _dsEMTransfer = new List<EMTransfer>();
        // List<EMTransferDetail> _dsEMTransferDetail = new List<EMTransferDetail>();
        #endregion

        #region khởi tạo
        public FEMEstimate()
        {
            WaitingFrm.StartWaiting();
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _IEMEstimateService = IoC.Resolve<IEMEstimateService>();
            _IBudgetItemService = IoC.Resolve<IBudgetItemService>();
            _IDepartmentService = IoC.Resolve<IDepartmentService>();

            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            uGridPosted.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu


            LoadDuLieu();

            //chọn tự select dòng đầu tiên
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            #endregion
            WaitingFrm.StopWaiting();
        }
        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }

        private void LoadDuLieu(bool configGrid)
        {
            #region Lấy dữ liệu từ CSDL
            List<EMEstimate> lt = _IEMEstimateService.GetAll();
            //List<BudgetItem> list = _IBudgetItemService.Query.Where(c => c.BudgetItemName.StartsWith("1") || c.BudgetItemName.StartsWith("2") || c.BudgetItemName.StartsWith("3") || c.BudgetItemName.StartsWith("4")).ToList();
            List<BudgetItem> list = _IBudgetItemService.GetListBudgetItemBudgetItemNameStartWith("1;2;3;4");
            //List<Department> list1 = _IDepartmentService.Query.Where(k => k.IsActive == true).ToList();
            List<Department> list1 = _IDepartmentService.GetListDepartmentIsActive(true);
            #endregion

            #region Xử lý dữ liệu
            foreach (var item in lt)
            {

                foreach (var ls2 in list1)
                {
                    if (item.DepartmentID == ls2.ID)
                    {
                        item.DepartmentView = ls2.DepartmentName;
                    }
                }
            }
            foreach (var item in lt)
            {


                foreach (var ls2 in list)
                {
                    if (item.BudgetItemID == ls2.ID)
                    {
                        item.BudgetItemName = ls2.BudgetItemName;

                    }
                }
            }
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = lt.ToArray();
            uGridPosted.DataSource = lt.ToArray();

            if (configGrid) ConfigGrid(uGrid);
            if (configGrid) ConfigGrid1(uGridPosted);
            #endregion
        }
        #endregion


        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            EditFunction();
        }
        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        private void tmsReLoad_Click_1(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }

        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void uGrid_MouseDown_1(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClick(object sender, EventArgs e)
        {
            EditFunction();
        }



        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                EMEstimate temp = uGrid.Selected.Rows[0].ListObject as EMEstimate;
                new FEMEstimateDetail(temp).ShowDialog(this);
                if (!FEMEstimateDetail.isClose) LoadDuLieu();
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                EMEstimate temp = uGrid.Selected.Rows[0].ListObject as EMEstimate;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, temp.EstimateBudgetYear)) == DialogResult.Yes)
                {
                    _IEMEstimateService.BeginTran();
                    //List<EMTransferDetail> listGenTemp = _IEMTransferDetailService.Query.Where(p => p.EMTransferID == temp.ID).ToList();
                    //foreach (var i in listGenTemp)
                    //{
                    //    _IEMTransferDetailService.Delete(i);
                    //}
                    _IEMEstimateService.Delete(temp);
                    _IEMEstimateService.CommitTran();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion

        #region Utils
        void ConfigGrid(UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, ConstDatabase.EMEstimate_TableName);

        }

        void ConfigGrid1(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {
            List<TemplateColumn> dstemplatecolums = new List<TemplateColumn>();

            Dictionary<string, Dictionary<string, TemplateColumn>> dicTemp = new Dictionary<string, Dictionary<string, TemplateColumn>>();
            string nameTable = string.Empty;
            List<string> strColumnName, strColumnCaption, strColumnToolTip = new List<string>();
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb = new List<bool>();
            List<int> intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition = new List<int>();
            List<int> VTbolIsVisible, VTbolIsVisibleCbb = new List<int>();

            strColumnName = new List<string>() { "ID", "BranchID", "EstimateBudgetYear", "Type", "DepartmentID", "DepartmentView", "BudgetItemID", "BudgetItemCode", "BudgetItemName", "SumAmount", "AmountMonth1", "AmountMonth2", "AmountMonth3", "AmountMonth4", "AmountMonth5", "AmountMonth6", "AmountMonth7", "AmountMonth8", "AmountMonth9", "AmountMonth10", "AmountMonth11", "AmountMonth12", "WarningLevel", "WarningLevelPercent" };
            strColumnCaption = strColumnToolTip = new List<string>() { "ID", "ID chi nhánh", "Năm", "Type", "ID Phòng ban", "Phòng ban", "ID Mục thu/chi", "Mã mục thu/chi ", "Mục thu/chi", "Tổng tiền", "Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7 ", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12", "Ngưỡng cảnh báo", "% ngưỡng cảnh báo" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 1, 2 };
            for (int i = 0; i < 24; i++)
            {
                bolIsVisible.Add(VTbolIsVisible.Contains(i));
                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            dstemplatecolums = ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();

            Utils.ConfigGrid(utralGrid, fEMShareHolder.database.EMRegistrations_TableName, dstemplatecolums, 0);
        }
        #endregion



        private void uGrid_AfterSelectChange_1(object sender, AfterSelectChangeEventArgs e)
        {
            try
            {
                if (uGrid.Selected.Rows.Count < 1) return;
                object listObject = uGrid.Selected.Rows[0].ListObject;
                if (listObject == null) return;
                EMEstimate cd = listObject as EMEstimate;
                if (cd == null) return;
                //



                ////if (cd.EMTransferDetails.Count == 0) cd.EMTransferDetails = new List<EMTransferDetail>();
                //uGridPosted.DataSource = cd.EMTransferDetails.ToList();
            }
            catch
            {
                MessageBox.Show("Đang lỗi đấy, hihi!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #region Config Grid
            //hiển thị 1 band
            uGridPosted.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            //tắt lọc cột
            uGridPosted.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGridPosted.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGridPosted.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGridPosted.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;

            //Hiện những dòng trống?
            uGridPosted.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridPosted.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            //Fix Header
            uGridPosted.DisplayLayout.UseFixedHeaders = true;

            UltraGridBand band = uGridPosted.DisplayLayout.Bands[0];
            band.Summaries.Clear();
            ////Thêm tổng số ở cột "Số tiền"
            //SummarySettings summary = band.Summaries.Add("Quantity", SummaryType.Sum, band.Columns["Quantity"]);
            //summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            //summary.DisplayFormat = "{0:N0}";
            //summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            #endregion
        }

        private void FEMEstimate_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();


            uGridPosted.ResetText();
            uGridPosted.ResetUpdateMode();
            uGridPosted.ResetExitEditModeOnLeave();
            uGridPosted.ResetRowUpdateCancelAction();
            uGridPosted.DataSource = null;
            uGridPosted.Layouts.Clear();
            uGridPosted.ResetLayouts();
            uGridPosted.ResetDisplayLayout();
            uGridPosted.Refresh();
            uGridPosted.ClearUndoHistory();
            uGridPosted.ClearXsdConstraints();
        }

        private void FEMEstimate_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
