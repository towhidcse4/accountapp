﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinSchedule.CalendarCombo;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

namespace Accounting
{
    public partial class FEMEstimateDetail : CustormForm
    {
        #region khai báo
        private readonly IEMEstimateService _IEMEstimateService;
        private readonly IDepartmentService _IDepartmentService;
        private readonly IBudgetItemService _IBudgetItemService;


        EMEstimate _Select = new EMEstimate();

        bool Them = true;

        public static bool isClose = true;

        #endregion

        #region khởi tạo
        public FEMEstimateDetail()
        {//Thêm

            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IEMEstimateService = IoC.Resolve<IEMEstimateService>();
            _IDepartmentService = IoC.Resolve<IDepartmentService>();
            _IBudgetItemService = IoC.Resolve<IBudgetItemService>();

            this.uGridPosted.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)

            #endregion

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();


            #endregion
        }
        public FEMEstimateDetail(EMEstimate temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();

            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            //txtShareHolderCode.Enabled = false;

            //Khai báo các webservices
            _IEMEstimateService = IoC.Resolve<IEMEstimateService>();
            _IDepartmentService = IoC.Resolve<IDepartmentService>();
            _IBudgetItemService = IoC.Resolve<IBudgetItemService>();
            //List<EMEstimate> lt = _IEMEstimateService.GetAll();
            //List<Department> list1 = _IDepartmentService.GetAll();
            //List<BudgetItem> list = _IBudgetItemService.GetAll();

            this.uGridPosted.DisplayLayout.Override.SelectTypeRow = SelectType.Single;


            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)

            //foreach (var item in lt)
            //{

            //    foreach (var ls2 in list)
            //    {
            //        if (item.BudgetItemID == ls2.ID)
            //        {
            //            item.BudgetItemName = ls2.BudgetItemName;

            //        }
            //    }
            //}
            #endregion
            //uGridPosted.DataSource = lt.ToArray();

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();

            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);

            this.Text = "Dự toán";

            #endregion
        }


        void loadcbb()
        {
            //cbbDepartmentID.DataSource = _IDepartmentService.Query.Where(k => k.IsActive == true).ToList();
            cbbDepartmentID.DataSource = _IDepartmentService.GetListDepartmentIsActive(true);
            cbbDepartmentID.DisplayMember = "DepartmentName";
            Utils.ConfigGrid(cbbDepartmentID, ConstDatabase.Department_TableName);
        }

        private void InitializeGUI()
        {
            loadcbb();

            #region Lấy dữ liệu từ CSDL

            List<EMEstimate> list = _IEMEstimateService.GetAll();
            BindingList<EMEstimate> bdEMEstimate = new BindingList<EMEstimate>(list);
            //List<BudgetItem> list1 = _IBudgetItemService.Query.Where(c => c.BudgetItemName.StartsWith("1") || c.BudgetItemName.StartsWith("2") || c.BudgetItemName.StartsWith("3") || c.BudgetItemName.StartsWith("4")).ToList();
            List<BudgetItem> list1 = _IBudgetItemService.GetListBudgetItemBudgetItemNameStartWith("1;2;3;4");

            foreach (var item in list)
            {


                foreach (var ls2 in list1)
                {
                    if (item.BudgetItemID == ls2.ID)
                    {
                        item.BudgetItemName = ls2.BudgetItemName;

                    }
                }
            }
            #endregion

            #region hiển thị và xử lý hiển thị
            uGridPosted.DataSource = bdEMEstimate;

            //Hiện những dòng trống?
            uGridPosted.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridPosted.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.ExtendFirstCell;
            //tắt lọc cột
            uGridPosted.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.Default;
            //tắt tiêu đề
            uGridPosted.DisplayLayout.CaptionVisible = DefaultableBoolean.False;

            ConfigGridDong(mauGiaoDien);
            ConfigGrid1(uGridPosted);
            #endregion


        }
        #endregion

        //void ConfigGrid(UltraGrid utralGrid)
        //{//hàm chung
        //    Utils.ConfigGrid(utralGrid, ConstDatabase.EMEstimate_TableName);

        //}

        void ConfigGrid1(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {
            List<TemplateColumn> dstemplatecolums = new List<TemplateColumn>();

            Dictionary<string, Dictionary<string, TemplateColumn>> dicTemp = new Dictionary<string, Dictionary<string, TemplateColumn>>();
            string nameTable = string.Empty;
            List<string> strColumnName, strColumnCaption, strColumnToolTip = new List<string>();
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb = new List<bool>();
            List<int> intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition = new List<int>();
            List<int> VTbolIsVisible, VTbolIsVisibleCbb = new List<int>();

            strColumnName = new List<string>() { "ID", "BranchID", "EstimateBudgetYear", "Type", "DepartmentID", "DepartmentView", "BudgetItemID", "BudgetItemCode", "BudgetItemName", "SumAmount", "AmountMonth1", "AmountMonth2", "AmountMonth3", "AmountMonth4", "AmountMonth5", "AmountMonth6", "AmountMonth7", "AmountMonth8", "AmountMonth9", "AmountMonth10", "AmountMonth11", "AmountMonth12", "WarningLevel", "WarningLevelPercent" };
            strColumnCaption = strColumnToolTip = new List<string>() { "ID", "ID chi nhánh", "Năm", "Type", "ID Phòng ban", "Phòng ban", "ID Mục thu/chi", "Mã mục thu/chi ", "Mục thu/chi", "Tổng tiền", "Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7 ", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12", "Ngưỡng cảnh báo", "% ngưỡng cảnh báo" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 1, 2 };
            for (int i = 0; i < 24; i++)
            {
                bolIsVisible.Add(VTbolIsVisible.Contains(i));
                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            dstemplatecolums = ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();

            Utils.ConfigGrid(utralGrid, fEMShareHolder.database.EMEstimate_TableName, dstemplatecolums, 0);
        }

        private void ConfigGridDong(Template mauGiaoDien)
        {

            #region Grid động
            //uGridPosted.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
            //uGridPosted.DisplayLayout.Override.TemplateAddRowPrompt = resSystem.MSG_HeThong_07;
            //uGridPosted.DisplayLayout.Override.SpecialRowSeparator = SpecialRowSeparator.None;

            //hiển thị 1 band
            uGridPosted.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            //tắt lọc cột
            uGridPosted.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGridPosted.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGridPosted.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGridPosted.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            //Hiển thị SupportDataErrorInfo
            uGridPosted.DisplayLayout.Override.SupportDataErrorInfo = SupportDataErrorInfo.CellsOnly;

            uGridPosted.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridPosted.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;

            uGridPosted.DisplayLayout.UseFixedHeaders = true;

            uGridPosted.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;

            Infragistics.Win.UltraWinGrid.UltraGridBand band = uGridPosted.DisplayLayout.Bands[0];

            #endregion
        }

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        private void btnSave_Click_1(object sender, EventArgs e)
        {
            #region Fill dữ liệu control vào obj
            EMEstimate temp = Them ? new EMEstimate() : _IEMEstimateService.Getbykey(_Select.ID);
            ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            try
            {

                _IEMEstimateService.BeginTran();
                if (Them) _IEMEstimateService.CreateNew(temp);

                //List<EMTransferDetail> EMTransferDetails = _IEMTransferDetailService.Query.Where(k => k.EMTransferID == temp.ID).ToList();
                //foreach (EMTransferDetail item in EMTransferDetails) _IEMTransferDetailService.Delete(item);
                //foreach (EMTransferDetail item in temp.EMTransferDetails) _IEMTransferDetailService.Save(item);

                _IEMEstimateService.Update(temp);
                _IEMEstimateService.CommitTran();
            }
            catch
            {
                _IEMEstimateService.RolbackTran();
            }

            #endregion

            #region xử lý form, kết thúc form

            this.Close();
            isClose = false;

            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }
        #endregion



        #region Utils
        EMEstimate ObjandGUI(EMEstimate input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.EstimateBudgetYear = Int32.Parse(txtEstimateBudgetYear.Value.ToString());


                //if (cbbContactPrefix.SelectedItem != null)
                //    input.ContactPrefix = cbbContactPrefix.SelectedItem.ToString();

                Department temp0 = (Department)Utils.getSelectCbbItem(cbbDepartmentID);
                if (temp0 == null) input.DepartmentID = null;
                else input.DepartmentID = temp0.ID;


            }
            else
            {
                txtEstimateBudgetYear.Text = input.EstimateBudgetYear.ToString();


                //từ tài khoản
                foreach (var item in cbbDepartmentID.Rows)
                {
                    if ((item.ListObject as Department).ID == input.DepartmentID) cbbDepartmentID.SelectedRow = item;
                }

            }
            return input;
        }

        #endregion

        public Template mauGiaoDien { get; set; }


    }
}
