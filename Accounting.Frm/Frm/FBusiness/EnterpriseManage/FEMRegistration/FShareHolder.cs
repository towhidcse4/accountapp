﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.ComponentModel;
using System.Linq;

namespace Accounting
{
    public partial class FShareHolder : CustormForm
    {
        #region khai báo
        public static List<EMShareHolder> dsEMShareHolders = new List<EMShareHolder>();
        private readonly IEMShareHolderService _IEMShareHolderService;
        private List<EMShareHolder> list;
        EMShareHolder _Select = new EMShareHolder();

        public EMShareHolder EMShareHolderSelect
        {
            get { return _Select; }
            set { _Select = value; }
        }
        #endregion

        public FShareHolder()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            _IEMShareHolderService = IoC.Resolve<IEMShareHolderService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            Loaddulieu();
            //uGrid.DataSource = new BindingList<EMShareHolder>(dsEMShareHolders);

            ////config
            //uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            //uGrid.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            //uGrid.DisplayLayout.Override.TemplateAddRowPrompt = "Bấm vào đây để thêm mới....";
            //uGrid.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.None;
            //uGrid.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;
            ////Utils.ConfigGrid(uGrid, fEMShareHolder.database.EMRegistrations_TableName, dstemplatecolums, 0);
            //ConfigGrid(uGrid);
        }


        #region khởi tạo
        public FShareHolder(List<EMShareHolder> dsEMShareHolders)
        {
            this.list = dsEMShareHolders;
            //InitializeComponent();

            //uGrid.DataSource = new BindingList<EMShareHolder>(dsEMShareHolders);

            ////config
            //uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            //uGrid.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            //uGrid.DisplayLayout.Override.TemplateAddRowPrompt = "Bấm vào đây để thêm mới....";
            //uGrid.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.None;
            //uGrid.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;
            ////Utils.ConfigGrid(uGrid, fEMShareHolder.database.EMRegistrations_TableName, dstemplatecolums, 0);
            //ConfigGrid(uGrid);
        }

        void Loaddulieu()
        {
            Loaddulieu(true);
        }
        private void Loaddulieu(bool configGrid)
        {
            #region Lấy dữ liệu từ CSDL
            List<EMShareHolder> list = _IEMShareHolderService.GetAll();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list.ToArray();
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            if (configGrid) ConfigGrid(uGrid);

            #endregion
        }

      
        #endregion

        #region Nghiệp vụ
       
        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, null);
        }
        #endregion

        #region Utils
     

        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {
            List<TemplateColumn> dstemplatecolums = new List<TemplateColumn>();

            Dictionary<string, Dictionary<string, TemplateColumn>> dicTemp = new Dictionary<string, Dictionary<string, TemplateColumn>>();
            string nameTable = string.Empty;
            List<string> strColumnName, strColumnCaption, strColumnToolTip = new List<string>();
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb = new List<bool>();
            List<int> intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition = new List<int>();
            List<int> VTbolIsVisible, VTbolIsVisibleCbb = new List<int>();

            strColumnName = new List<string>() { "ID", "ShareHolderCategoryID", "ShareHolderCode", "ShareHolderName", "PersonalTaxCode", "BookIssuedDate", "BusinessRegistrationNumber", "BusinessIssuedDate", "BusinessIssueBy", "BankAccount", "BankName", "Tel", "Fax", "Email", "Website", "Address", "ContactPrefix", "ContactName", "ContactTitle", "ContactIdentificationNo", "ContactIssueDate", "ContactIssueBy", "ContactOfficeTel", "ContactHomeTel", "ContactMobile", "ContactEmail", "ContactAddress", "IsPersonal", "TypeID", "TotalShare", "IsActive" };
            strColumnCaption = strColumnToolTip = new List<string>() { "ID cổ đông", "ID nhóm cổ đông", "Mã cổ đông", "Tên cổ đông", "Mã số thuế cá nhân", "Ngày cấp sổ", "Số đăng ký kinh doanh", "Ngày cấp đăng ký kinh doanh ", "Nơi cấp đăng ký kinh doanh", "TK ngân hàng", "Tên ngân hàng", "Số điện thoại tổ chức", "Số phách tổ chức", "Email tổ chức", "Website tổ chức", "Địa chỉ", "Xưng hô", "Người liên hệ", "Chức vụ ", "Số CMND người liên hệ", "Ngày cấp CMND", "Nơi cấp CMND", "ĐT cơ quan", "ĐT nhà riêng", "ĐT di động", "Email người liên hệ", "Địa chỉ người liên hệ", "IsPersonal", "Loại chứng từ", "Số CP sở hữu", "Tình trạng hoạt động" };

         
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() {2, 3 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 1, 2 };
            for (int i = 0; i < 31; i++)
            {
                bolIsVisible.Add(VTbolIsVisible.Contains(i));
                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            dstemplatecolums = ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();

            Utils.ConfigGrid(utralGrid, fEMShareHolder.database.EMShareHolder_TableName, dstemplatecolums, 0);
        }
        #endregion


        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }


        private void btnSave_Click_1(object sender, EventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                this.EMShareHolderSelect = uGrid.Selected.Rows[0].ListObject as EMShareHolder;
            }

            this.Close();
        }

        private void uGrid_MouseDown_1(object sender, MouseEventArgs e)
        {
            System.Drawing.Point point = new System.Drawing.Point(e.X, e.Y);
            //lấy kiểu của đối tượng sender (đối tượng gây ra sự kiện)
            System.Type type = sender.GetType();
            UltraGrid uGrid = (UltraGrid)sender;
            //lấy UltraGridCell từ vị trí
            UltraGridCell cell = (UltraGridCell)((UltraGridBase)sender).DisplayLayout.UIElement.ElementFromPoint(point).GetContext(typeof(UltraGridCell));
            //nếu đang bấm chuột phải và đối tượng đang chọn khác null thì thao tác
            if (cell != null && cell.Row.Index >= 0)
            {
                uGrid.Rows[cell.Row.Index].Selected = true;
            }
        }
    }
}
