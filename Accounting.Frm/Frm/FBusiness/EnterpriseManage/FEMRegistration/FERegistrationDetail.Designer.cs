﻿namespace Accounting
{
    partial class FERegistrationDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            this.panel23 = new System.Windows.Forms.Panel();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContaxtName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel25 = new System.Windows.Forms.Panel();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAddress1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactIssueBy = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel24 = new System.Windows.Forms.Panel();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel7 = new System.Windows.Forms.Panel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactMobile = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel12 = new System.Windows.Forms.Panel();
            this.dtContactIssueDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.txtDTCoQuan = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbContactPrefix = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactHomeTel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel5 = new System.Windows.Forms.Panel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtIndentication = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.txtEmail = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.uGridLM = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel35 = new System.Windows.Forms.Panel();
            this.ultraLabel33 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor8 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel27 = new System.Windows.Forms.Panel();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.txtIssueBy = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel26 = new System.Windows.Forms.Panel();
            this.dtePostedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.txtWebsite = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel21 = new System.Windows.Forms.Panel();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.txtFax = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel20 = new System.Windows.Forms.Panel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.txtEmail1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel19 = new System.Windows.Forms.Panel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.txtTel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel18 = new System.Windows.Forms.Panel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.txtBusinessRegistrationNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel11 = new System.Windows.Forms.Panel();
            this.cbbInvestorCategoryID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.txtBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel9 = new System.Windows.Forms.Panel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.txtTaxCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel10 = new System.Windows.Forms.Panel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.txtBankAcount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtRegistrationName1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblInvestorCode = new Infragistics.Win.Misc.UltraLabel();
            this.txtRegistrationCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.ultraOptionSet1 = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContaxtName)).BeginInit();
            this.panel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactIssueBy)).BeginInit();
            this.panel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactTitle)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactMobile)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtContactIssueDate)).BeginInit();
            this.panel16.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDTCoQuan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbContactPrefix)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactHomeTel)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIndentication)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridLM)).BeginInit();
            this.panel35.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            this.panel27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueBy)).BeginInit();
            this.panel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtePostedDate)).BeginInit();
            this.panel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite)).BeginInit();
            this.panel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax)).BeginInit();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail1)).BeginInit();
            this.panel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTel)).BeginInit();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBusinessRegistrationNumber)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbInvestorCategoryID)).BeginInit();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxCode)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAcount)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegistrationName1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegistrationCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.Transparent;
            this.panel23.Controls.Add(this.ultraLabel21);
            this.panel23.Controls.Add(this.txtContaxtName);
            this.panel23.Location = new System.Drawing.Point(219, 19);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(354, 31);
            this.panel23.TabIndex = 16;
            // 
            // ultraLabel21
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel21.Appearance = appearance1;
            this.ultraLabel21.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(65, 19);
            this.ultraLabel21.TabIndex = 6;
            this.ultraLabel21.Text = "Họ và tên";
            // 
            // txtContaxtName
            // 
            this.txtContaxtName.Location = new System.Drawing.Point(72, 6);
            this.txtContaxtName.Name = "txtContaxtName";
            this.txtContaxtName.Size = new System.Drawing.Size(279, 21);
            this.txtContaxtName.TabIndex = 16;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.Transparent;
            this.panel25.Controls.Add(this.ultraLabel23);
            this.panel25.Controls.Add(this.txtAddress1);
            this.panel25.Location = new System.Drawing.Point(12, 85);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(561, 31);
            this.panel25.TabIndex = 18;
            // 
            // ultraLabel23
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel23.Appearance = appearance2;
            this.ultraLabel23.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(65, 19);
            this.ultraLabel23.TabIndex = 6;
            this.ultraLabel23.Text = "Địa chỉ";
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(94, 6);
            this.txtAddress1.Multiline = true;
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(463, 21);
            this.txtAddress1.TabIndex = 18;
            // 
            // ultraLabel14
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel14.Appearance = appearance3;
            this.ultraLabel14.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(49, 19);
            this.ultraLabel14.TabIndex = 6;
            this.ultraLabel14.Text = "Nơi cấp";
            // 
            // txtContactIssueBy
            // 
            this.txtContactIssueBy.Location = new System.Drawing.Point(50, 3);
            this.txtContactIssueBy.Name = "txtContactIssueBy";
            this.txtContactIssueBy.Size = new System.Drawing.Size(115, 21);
            this.txtContactIssueBy.TabIndex = 21;
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.Transparent;
            this.panel24.Controls.Add(this.ultraLabel22);
            this.panel24.Controls.Add(this.txtContactTitle);
            this.panel24.Location = new System.Drawing.Point(12, 52);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(561, 31);
            this.panel24.TabIndex = 17;
            // 
            // ultraLabel22
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel22.Appearance = appearance4;
            this.ultraLabel22.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(65, 19);
            this.ultraLabel22.TabIndex = 6;
            this.ultraLabel22.Text = "Chức danh";
            // 
            // txtContactTitle
            // 
            this.txtContactTitle.Location = new System.Drawing.Point(94, 6);
            this.txtContactTitle.Name = "txtContactTitle";
            this.txtContactTitle.Size = new System.Drawing.Size(463, 21);
            this.txtContactTitle.TabIndex = 17;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Transparent;
            this.panel7.Controls.Add(this.ultraLabel5);
            this.panel7.Controls.Add(this.txtContactMobile);
            this.panel7.Location = new System.Drawing.Point(404, 151);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(170, 31);
            this.panel7.TabIndex = 24;
            // 
            // ultraLabel5
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel5.Appearance = appearance5;
            this.ultraLabel5.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(49, 19);
            this.ultraLabel5.TabIndex = 6;
            this.ultraLabel5.Text = "ĐTDĐ";
            // 
            // txtContactMobile
            // 
            this.txtContactMobile.Location = new System.Drawing.Point(51, 3);
            this.txtContactMobile.Name = "txtContactMobile";
            this.txtContactMobile.Size = new System.Drawing.Size(116, 21);
            this.txtContactMobile.TabIndex = 24;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Transparent;
            this.panel12.Controls.Add(this.dtContactIssueDate);
            this.panel12.Controls.Add(this.ultraLabel10);
            this.panel12.Location = new System.Drawing.Point(219, 118);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(179, 31);
            this.panel12.TabIndex = 20;
            // 
            // dtContactIssueDate
            // 
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.dtContactIssueDate.Appearance = appearance6;
            this.dtContactIssueDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtContactIssueDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtContactIssueDate.Location = new System.Drawing.Point(72, 3);
            this.dtContactIssueDate.MaskInput = "";
            this.dtContactIssueDate.Name = "dtContactIssueDate";
            this.dtContactIssueDate.Size = new System.Drawing.Size(104, 21);
            this.dtContactIssueDate.TabIndex = 20;
            this.dtContactIssueDate.Value = null;
            // 
            // ultraLabel10
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel10.Appearance = appearance7;
            this.ultraLabel10.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(65, 19);
            this.ultraLabel10.TabIndex = 6;
            this.ultraLabel10.Text = "Ngày cấp";
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Transparent;
            this.panel16.Controls.Add(this.ultraLabel14);
            this.panel16.Controls.Add(this.txtContactIssueBy);
            this.panel16.Location = new System.Drawing.Point(405, 118);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(168, 31);
            this.panel16.TabIndex = 21;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Transparent;
            this.panel8.Controls.Add(this.ultraLabel6);
            this.panel8.Controls.Add(this.txtDTCoQuan);
            this.panel8.Location = new System.Drawing.Point(11, 148);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(200, 31);
            this.panel8.TabIndex = 22;
            // 
            // ultraLabel6
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel6.Appearance = appearance8;
            this.ultraLabel6.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(77, 19);
            this.ultraLabel6.TabIndex = 6;
            this.ultraLabel6.Text = "ĐT cơ quan";
            // 
            // txtDTCoQuan
            // 
            this.txtDTCoQuan.Location = new System.Drawing.Point(95, 3);
            this.txtDTCoQuan.Name = "txtDTCoQuan";
            this.txtDTCoQuan.Size = new System.Drawing.Size(99, 21);
            this.txtDTCoQuan.TabIndex = 22;
            // 
            // cbbContactPrefix
            // 
            this.cbbContactPrefix.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbContactPrefix.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbContactPrefix.Location = new System.Drawing.Point(93, 4);
            this.cbbContactPrefix.Name = "cbbContactPrefix";
            this.cbbContactPrefix.Size = new System.Drawing.Size(98, 21);
            this.cbbContactPrefix.TabIndex = 15;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.Controls.Add(this.cbbContactPrefix);
            this.panel4.Controls.Add(this.ultraLabel2);
            this.panel4.Location = new System.Drawing.Point(13, 19);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(198, 31);
            this.panel4.TabIndex = 15;
            // 
            // ultraLabel2
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel2.Appearance = appearance9;
            this.ultraLabel2.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(49, 19);
            this.ultraLabel2.TabIndex = 6;
            this.ultraLabel2.Text = "Xưng hô";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Transparent;
            this.panel13.Controls.Add(this.ultraLabel11);
            this.panel13.Controls.Add(this.txtContactHomeTel);
            this.panel13.Location = new System.Drawing.Point(220, 148);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(180, 31);
            this.panel13.TabIndex = 23;
            // 
            // ultraLabel11
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel11.Appearance = appearance10;
            this.ultraLabel11.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(65, 19);
            this.ultraLabel11.TabIndex = 6;
            this.ultraLabel11.Text = "ĐT NR";
            // 
            // txtContactHomeTel
            // 
            this.txtContactHomeTel.Location = new System.Drawing.Point(71, 3);
            this.txtContactHomeTel.Name = "txtContactHomeTel";
            this.txtContactHomeTel.Size = new System.Drawing.Size(104, 21);
            this.txtContactHomeTel.TabIndex = 23;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.Controls.Add(this.ultraLabel3);
            this.panel5.Controls.Add(this.txtIndentication);
            this.panel5.Location = new System.Drawing.Point(11, 118);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(200, 31);
            this.panel5.TabIndex = 19;
            // 
            // ultraLabel3
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel3.Appearance = appearance11;
            this.ultraLabel3.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(77, 19);
            this.ultraLabel3.TabIndex = 6;
            this.ultraLabel3.Text = "Số CMND/HC";
            // 
            // txtIndentication
            // 
            this.txtIndentication.Location = new System.Drawing.Point(94, 3);
            this.txtIndentication.Name = "txtIndentication";
            this.txtIndentication.Size = new System.Drawing.Size(99, 21);
            this.txtIndentication.TabIndex = 19;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.panel25);
            this.ultraGroupBox2.Controls.Add(this.panel24);
            this.ultraGroupBox2.Controls.Add(this.panel23);
            this.ultraGroupBox2.Controls.Add(this.panel7);
            this.ultraGroupBox2.Controls.Add(this.panel16);
            this.ultraGroupBox2.Controls.Add(this.panel12);
            this.ultraGroupBox2.Controls.Add(this.panel13);
            this.ultraGroupBox2.Controls.Add(this.panel5);
            this.ultraGroupBox2.Controls.Add(this.panel14);
            this.ultraGroupBox2.Controls.Add(this.panel8);
            this.ultraGroupBox2.Controls.Add(this.panel4);
            this.ultraGroupBox2.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox2.Location = new System.Drawing.Point(12, 245);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(585, 226);
            this.ultraGroupBox2.TabIndex = 33;
            this.ultraGroupBox2.Text = "Người liên hệ";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Transparent;
            this.panel14.Controls.Add(this.ultraLabel12);
            this.panel14.Controls.Add(this.txtEmail);
            this.panel14.Location = new System.Drawing.Point(11, 185);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(563, 34);
            this.panel14.TabIndex = 25;
            // 
            // ultraLabel12
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel12.Appearance = appearance12;
            this.ultraLabel12.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(49, 19);
            this.ultraLabel12.TabIndex = 6;
            this.ultraLabel12.Text = "Email";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(95, 3);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(465, 21);
            this.txtEmail.TabIndex = 25;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ultraGroupBox3);
            this.panel1.Controls.Add(this.ultraGroupBox2);
            this.panel1.Controls.Add(this.ultraGroupBox1);
            this.panel1.Controls.Add(this.chkIsActive);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Location = new System.Drawing.Point(12, 36);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(606, 669);
            this.panel1.TabIndex = 1;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.panel17);
            this.ultraGroupBox3.Controls.Add(this.panel35);
            this.ultraGroupBox3.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox3.Location = new System.Drawing.Point(12, 480);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(585, 124);
            this.ultraGroupBox3.TabIndex = 34;
            this.ultraGroupBox3.Text = "Cổ phần đăng ký mua";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.uGridLM);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(3, 16);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(579, 105);
            this.panel17.TabIndex = 706;
            // 
            // uGridLM
            // 
            this.uGridLM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridLM.Location = new System.Drawing.Point(0, 0);
            this.uGridLM.Name = "uGridLM";
            this.uGridLM.Size = new System.Drawing.Size(579, 105);
            this.uGridLM.TabIndex = 1;
            this.uGridLM.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_AfterCellUpdate);
            // 
            // panel35
            // 
            this.panel35.Controls.Add(this.ultraLabel33);
            this.panel35.Controls.Add(this.ultraTextEditor8);
            this.panel35.Location = new System.Drawing.Point(11, 185);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(563, 34);
            this.panel35.TabIndex = 705;
            // 
            // ultraLabel33
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel33.Appearance = appearance13;
            this.ultraLabel33.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel33.Name = "ultraLabel33";
            this.ultraLabel33.Size = new System.Drawing.Size(49, 19);
            this.ultraLabel33.TabIndex = 6;
            this.ultraLabel33.Text = "Email";
            // 
            // ultraTextEditor8
            // 
            this.ultraTextEditor8.Location = new System.Drawing.Point(95, 3);
            this.ultraTextEditor8.Name = "ultraTextEditor8";
            this.ultraTextEditor8.Size = new System.Drawing.Size(465, 21);
            this.ultraTextEditor8.TabIndex = 25;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.panel6);
            this.ultraGroupBox1.Controls.Add(this.panel27);
            this.ultraGroupBox1.Controls.Add(this.panel26);
            this.ultraGroupBox1.Controls.Add(this.panel22);
            this.ultraGroupBox1.Controls.Add(this.panel21);
            this.ultraGroupBox1.Controls.Add(this.panel20);
            this.ultraGroupBox1.Controls.Add(this.panel19);
            this.ultraGroupBox1.Controls.Add(this.panel18);
            this.ultraGroupBox1.Controls.Add(this.panel11);
            this.ultraGroupBox1.Controls.Add(this.panel15);
            this.ultraGroupBox1.Controls.Add(this.panel9);
            this.ultraGroupBox1.Controls.Add(this.panel10);
            this.ultraGroupBox1.Controls.Add(this.panel3);
            this.ultraGroupBox1.Controls.Add(this.panel2);
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(12, 3);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(585, 239);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.Controls.Add(this.ultraLabel4);
            this.panel6.Controls.Add(this.txtAddress);
            this.panel6.Location = new System.Drawing.Point(14, 202);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(558, 31);
            this.panel6.TabIndex = 5;
            // 
            // ultraLabel4
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel4.Appearance = appearance14;
            this.ultraLabel4.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(49, 19);
            this.ultraLabel4.TabIndex = 6;
            this.ultraLabel4.Text = "Địa chỉ";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(92, 3);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(465, 21);
            this.txtAddress.TabIndex = 5;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.Transparent;
            this.panel27.Controls.Add(this.ultraLabel25);
            this.panel27.Controls.Add(this.txtIssueBy);
            this.panel27.Location = new System.Drawing.Point(399, 111);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(173, 31);
            this.panel27.TabIndex = 8;
            // 
            // ultraLabel25
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel25.Appearance = appearance15;
            this.ultraLabel25.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(49, 19);
            this.ultraLabel25.TabIndex = 6;
            this.ultraLabel25.Text = "Nơi cấp";
            // 
            // txtIssueBy
            // 
            this.txtIssueBy.Location = new System.Drawing.Point(56, 3);
            this.txtIssueBy.Name = "txtIssueBy";
            this.txtIssueBy.Size = new System.Drawing.Size(116, 21);
            this.txtIssueBy.TabIndex = 8;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.Transparent;
            this.panel26.Controls.Add(this.dtePostedDate);
            this.panel26.Controls.Add(this.ultraLabel24);
            this.panel26.Location = new System.Drawing.Point(219, 111);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(179, 31);
            this.panel26.TabIndex = 77;
            // 
            // dtePostedDate
            // 
            appearance16.TextHAlignAsString = "Center";
            appearance16.TextVAlignAsString = "Middle";
            this.dtePostedDate.Appearance = appearance16;
            this.dtePostedDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtePostedDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtePostedDate.Location = new System.Drawing.Point(70, 3);
            this.dtePostedDate.MaskInput = "";
            this.dtePostedDate.Name = "dtePostedDate";
            this.dtePostedDate.Size = new System.Drawing.Size(106, 21);
            this.dtePostedDate.TabIndex = 7;
            this.dtePostedDate.Value = null;
            // 
            // ultraLabel24
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel24.Appearance = appearance17;
            this.ultraLabel24.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(65, 19);
            this.ultraLabel24.TabIndex = 6;
            this.ultraLabel24.Text = "Ngày cấp";
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.Transparent;
            this.panel22.Controls.Add(this.ultraLabel20);
            this.panel22.Controls.Add(this.txtWebsite);
            this.panel22.Location = new System.Drawing.Point(219, 80);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(353, 31);
            this.panel22.TabIndex = 14;
            // 
            // ultraLabel20
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel20.Appearance = appearance18;
            this.ultraLabel20.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(65, 19);
            this.ultraLabel20.TabIndex = 6;
            this.ultraLabel20.Text = "Website";
            // 
            // txtWebsite
            // 
            this.txtWebsite.Location = new System.Drawing.Point(70, 6);
            this.txtWebsite.Name = "txtWebsite";
            this.txtWebsite.Size = new System.Drawing.Size(282, 21);
            this.txtWebsite.TabIndex = 14;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.Transparent;
            this.panel21.Controls.Add(this.ultraLabel19);
            this.panel21.Controls.Add(this.txtFax);
            this.panel21.Location = new System.Drawing.Point(219, 172);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(354, 31);
            this.panel21.TabIndex = 12;
            // 
            // ultraLabel19
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel19.Appearance = appearance19;
            this.ultraLabel19.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(65, 19);
            this.ultraLabel19.TabIndex = 6;
            this.ultraLabel19.Text = "Fax";
            // 
            // txtFax
            // 
            this.txtFax.Location = new System.Drawing.Point(70, 6);
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(282, 21);
            this.txtFax.TabIndex = 12;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.Transparent;
            this.panel20.Controls.Add(this.ultraLabel18);
            this.panel20.Controls.Add(this.txtEmail1);
            this.panel20.Location = new System.Drawing.Point(14, 80);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(197, 31);
            this.panel20.TabIndex = 13;
            // 
            // ultraLabel18
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel18.Appearance = appearance20;
            this.ultraLabel18.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(88, 19);
            this.ultraLabel18.TabIndex = 6;
            this.ultraLabel18.Text = "Email";
            // 
            // txtEmail1
            // 
            this.txtEmail1.Location = new System.Drawing.Point(92, 4);
            this.txtEmail1.Multiline = true;
            this.txtEmail1.Name = "txtEmail1";
            this.txtEmail1.Size = new System.Drawing.Size(99, 21);
            this.txtEmail1.TabIndex = 13;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Transparent;
            this.panel19.Controls.Add(this.ultraLabel17);
            this.panel19.Controls.Add(this.txtTel);
            this.panel19.Location = new System.Drawing.Point(14, 171);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(197, 31);
            this.panel19.TabIndex = 11;
            // 
            // ultraLabel17
            // 
            appearance21.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel17.Appearance = appearance21;
            this.ultraLabel17.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(88, 19);
            this.ultraLabel17.TabIndex = 6;
            this.ultraLabel17.Text = "Điện thoại";
            // 
            // txtTel
            // 
            this.txtTel.Location = new System.Drawing.Point(92, 4);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(99, 21);
            this.txtTel.TabIndex = 11;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Transparent;
            this.panel18.Controls.Add(this.ultraLabel16);
            this.panel18.Controls.Add(this.txtBusinessRegistrationNumber);
            this.panel18.Location = new System.Drawing.Point(14, 112);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(197, 31);
            this.panel18.TabIndex = 6;
            // 
            // ultraLabel16
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel16.Appearance = appearance22;
            this.ultraLabel16.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(72, 19);
            this.ultraLabel16.TabIndex = 6;
            this.ultraLabel16.Text = "Mã ĐKKD";
            // 
            // txtBusinessRegistrationNumber
            // 
            this.txtBusinessRegistrationNumber.Location = new System.Drawing.Point(92, 5);
            this.txtBusinessRegistrationNumber.Name = "txtBusinessRegistrationNumber";
            this.txtBusinessRegistrationNumber.Size = new System.Drawing.Size(99, 21);
            this.txtBusinessRegistrationNumber.TabIndex = 6;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Transparent;
            this.panel11.Controls.Add(this.cbbInvestorCategoryID);
            this.panel11.Controls.Add(this.ultraLabel9);
            this.panel11.Location = new System.Drawing.Point(219, 49);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(353, 31);
            this.panel11.TabIndex = 4;
            // 
            // cbbInvestorCategoryID
            // 
            editorButton1.Text = "Thêm";
            this.cbbInvestorCategoryID.ButtonsRight.Add(editorButton1);
            this.cbbInvestorCategoryID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbInvestorCategoryID.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbInvestorCategoryID.Location = new System.Drawing.Point(70, 3);
            this.cbbInvestorCategoryID.Name = "cbbInvestorCategoryID";
            this.cbbInvestorCategoryID.NullText = "<Chọn dữ liệu>";
            this.cbbInvestorCategoryID.Size = new System.Drawing.Size(281, 22);
            this.cbbInvestorCategoryID.TabIndex = 4;
            this.cbbInvestorCategoryID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbInvestorCategoryID_EditorButtonClick_1);
            // 
            // ultraLabel9
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel9.Appearance = appearance23;
            this.ultraLabel9.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(51, 19);
            this.ultraLabel9.TabIndex = 6;
            this.ultraLabel9.Text = "Nhóm(*)";
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Transparent;
            this.panel15.Controls.Add(this.ultraLabel13);
            this.panel15.Controls.Add(this.txtBankName);
            this.panel15.Location = new System.Drawing.Point(219, 141);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(354, 31);
            this.panel15.TabIndex = 10;
            // 
            // ultraLabel13
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel13.Appearance = appearance24;
            this.ultraLabel13.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(65, 19);
            this.ultraLabel13.TabIndex = 6;
            this.ultraLabel13.Text = "Mở tại NH";
            // 
            // txtBankName
            // 
            this.txtBankName.Location = new System.Drawing.Point(70, 4);
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Size = new System.Drawing.Size(282, 21);
            this.txtBankName.TabIndex = 10;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Transparent;
            this.panel9.Controls.Add(this.ultraLabel7);
            this.panel9.Controls.Add(this.txtTaxCode);
            this.panel9.Location = new System.Drawing.Point(14, 50);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(197, 31);
            this.panel9.TabIndex = 3;
            // 
            // ultraLabel7
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel7.Appearance = appearance25;
            this.ultraLabel7.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(77, 19);
            this.ultraLabel7.TabIndex = 6;
            this.ultraLabel7.Text = "Mã số thuế";
            // 
            // txtTaxCode
            // 
            this.txtTaxCode.Location = new System.Drawing.Point(92, 3);
            this.txtTaxCode.MaxLength = 14;
            this.txtTaxCode.Name = "txtTaxCode";
            this.txtTaxCode.Size = new System.Drawing.Size(99, 21);
            this.txtTaxCode.TabIndex = 3;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Transparent;
            this.panel10.Controls.Add(this.ultraLabel8);
            this.panel10.Controls.Add(this.txtBankAcount);
            this.panel10.Location = new System.Drawing.Point(14, 143);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(197, 31);
            this.panel10.TabIndex = 9;
            // 
            // ultraLabel8
            // 
            appearance26.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel8.Appearance = appearance26;
            this.ultraLabel8.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(85, 19);
            this.ultraLabel8.TabIndex = 6;
            this.ultraLabel8.Text = "TK ngân hàng";
            // 
            // txtBankAcount
            // 
            this.txtBankAcount.Location = new System.Drawing.Point(92, 2);
            this.txtBankAcount.Name = "txtBankAcount";
            this.txtBankAcount.Size = new System.Drawing.Size(99, 21);
            this.txtBankAcount.TabIndex = 9;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.ultraLabel1);
            this.panel3.Controls.Add(this.txtRegistrationName1);
            this.panel3.Location = new System.Drawing.Point(219, 18);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(352, 31);
            this.panel3.TabIndex = 2;
            // 
            // ultraLabel1
            // 
            appearance27.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel1.Appearance = appearance27;
            this.ultraLabel1.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(50, 19);
            this.ultraLabel1.TabIndex = 6;
            this.ultraLabel1.Text = "Tên(*)";
            // 
            // txtRegistrationName1
            // 
            this.txtRegistrationName1.Location = new System.Drawing.Point(70, 4);
            this.txtRegistrationName1.Name = "txtRegistrationName1";
            this.txtRegistrationName1.Size = new System.Drawing.Size(282, 21);
            this.txtRegistrationName1.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.lblInvestorCode);
            this.panel2.Controls.Add(this.txtRegistrationCode);
            this.panel2.Location = new System.Drawing.Point(14, 19);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(197, 31);
            this.panel2.TabIndex = 1;
            // 
            // lblInvestorCode
            // 
            appearance28.BackColor = System.Drawing.Color.Transparent;
            this.lblInvestorCode.Appearance = appearance28;
            this.lblInvestorCode.Location = new System.Drawing.Point(1, 3);
            this.lblInvestorCode.Name = "lblInvestorCode";
            this.lblInvestorCode.Size = new System.Drawing.Size(49, 19);
            this.lblInvestorCode.TabIndex = 6;
            this.lblInvestorCode.Text = "Mã (*):";
            // 
            // txtRegistrationCode
            // 
            this.txtRegistrationCode.Location = new System.Drawing.Point(92, 3);
            this.txtRegistrationCode.Name = "txtRegistrationCode";
            this.txtRegistrationCode.Size = new System.Drawing.Size(99, 21);
            this.txtRegistrationCode.TabIndex = 1;
            // 
            // chkIsActive
            // 
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Location = new System.Drawing.Point(12, 627);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(103, 20);
            this.chkIsActive.TabIndex = 26;
            this.chkIsActive.Text = "Là cổ đông";
            // 
            // btnClose
            // 
            appearance29.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.btnClose.Appearance = appearance29;
            this.btnClose.Location = new System.Drawing.Point(517, 622);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 28;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance30.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance30;
            this.btnSave.Location = new System.Drawing.Point(427, 622);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 27;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // ultraOptionSet1
            // 
            this.ultraOptionSet1.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem1.DataValue = "Default Item";
            valueListItem1.DisplayText = "1.Cá Nhân";
            valueListItem2.DataValue = "ValueListItem1";
            valueListItem2.DisplayText = "2. Tổ Chức";
            this.ultraOptionSet1.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.ultraOptionSet1.Location = new System.Drawing.Point(12, 12);
            this.ultraOptionSet1.Name = "ultraOptionSet1";
            this.ultraOptionSet1.Size = new System.Drawing.Size(161, 18);
            this.ultraOptionSet1.TabIndex = 888;
            this.ultraOptionSet1.ValueChanged += new System.EventHandler(this.ultraOptionSet1_ValueChanged_1);
            // 
            // ultraButton1
            // 
            this.ultraButton1.Location = new System.Drawing.Point(363, 7);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(115, 23);
            this.ultraButton1.TabIndex = 889;
            this.ultraButton1.Text = "Chọn nhà đầu tư";
            this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
            // 
            // ultraButton2
            // 
            this.ultraButton2.Location = new System.Drawing.Point(491, 7);
            this.ultraButton2.Name = "ultraButton2";
            this.ultraButton2.Size = new System.Drawing.Size(115, 23);
            this.ultraButton2.TabIndex = 890;
            this.ultraButton2.Text = "Chọn cổ đông";
            this.ultraButton2.Click += new System.EventHandler(this.ultraButton2_Click);
            // 
            // FERegistrationDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 717);
            this.Controls.Add(this.ultraButton2);
            this.Controls.Add(this.ultraButton1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ultraOptionSet1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FERegistrationDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thêm mới Đăng ký mua cổ phần - Đợt phát hành.";
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContaxtName)).EndInit();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactIssueBy)).EndInit();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactTitle)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactMobile)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtContactIssueDate)).EndInit();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDTCoQuan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbContactPrefix)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactHomeTel)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIndentication)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridLM)).EndInit();
            this.panel35.ResumeLayout(false);
            this.panel35.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueBy)).EndInit();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtePostedDate)).EndInit();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite)).EndInit();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax)).EndInit();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail1)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTel)).EndInit();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBusinessRegistrationNumber)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbInvestorCategoryID)).EndInit();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxCode)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAcount)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegistrationName1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegistrationCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel23;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContaxtName;
        private System.Windows.Forms.Panel panel25;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAddress1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactIssueBy;
        private System.Windows.Forms.Panel panel24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactTitle;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel12;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtContactIssueDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDTCoQuan;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbContactPrefix;
        private System.Windows.Forms.Panel panel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private System.Windows.Forms.Panel panel13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactHomeTel;
        private System.Windows.Forms.Panel panel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIndentication;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet ultraOptionSet1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.Misc.UltraButton ultraButton2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private System.Windows.Forms.Panel panel27;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIssueBy;
        private System.Windows.Forms.Panel panel26;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtePostedDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private System.Windows.Forms.Panel panel22;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtWebsite;
        private System.Windows.Forms.Panel panel21;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFax;
        private System.Windows.Forms.Panel panel20;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtEmail1;
        private System.Windows.Forms.Panel panel19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTel;
        private System.Windows.Forms.Panel panel18;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBusinessRegistrationNumber;
        private System.Windows.Forms.Panel panel11;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbInvestorCategoryID;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private System.Windows.Forms.Panel panel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankName;
        private System.Windows.Forms.Panel panel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTaxCode;
        private System.Windows.Forms.Panel panel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankAcount;
        private System.Windows.Forms.Panel panel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtRegistrationName1;
        private System.Windows.Forms.Panel panel2;
        private Infragistics.Win.Misc.UltraLabel lblInvestorCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtRegistrationCode;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private System.Windows.Forms.Panel panel35;
        private Infragistics.Win.Misc.UltraLabel ultraLabel33;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor8;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactMobile;
        private System.Windows.Forms.Panel panel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtEmail;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAddress;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridLM;

    }
}