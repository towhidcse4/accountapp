﻿namespace Accounting
{
    partial class FEMPublishPeriodDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.lblFromDate = new Infragistics.Win.Misc.UltraLabel();
            this.dtePeriodDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtPeriodName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtPeriodCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblMaterialQuantumName = new Infragistics.Win.Misc.UltraLabel();
            this.lblMaterialQuantumCode = new Infragistics.Win.Misc.UltraLabel();
            this.chkActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.dtePeriodDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            appearance1.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.btnClose.Appearance = appearance1;
            this.btnClose.Location = new System.Drawing.Point(419, 431);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 30;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblFromDate
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            this.lblFromDate.Appearance = appearance2;
            this.lblFromDate.Location = new System.Drawing.Point(11, 92);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(121, 19);
            this.lblFromDate.TabIndex = 23;
            this.lblFromDate.Text = "Ngày phát hành (*) :";
            // 
            // dtePeriodDate
            // 
            this.dtePeriodDate.FormatString = "dd/MM/yyyy";
            this.dtePeriodDate.Location = new System.Drawing.Point(144, 90);
            this.dtePeriodDate.Name = "dtePeriodDate";
            this.dtePeriodDate.Size = new System.Drawing.Size(141, 21);
            this.dtePeriodDate.TabIndex = 9;
            // 
            // btnSave
            // 
            appearance3.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance3;
            this.btnSave.Location = new System.Drawing.Point(335, 431);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 29;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Near;
            this.ultraGroupBox1.Controls.Add(this.lblFromDate);
            this.ultraGroupBox1.Controls.Add(this.dtePeriodDate);
            this.ultraGroupBox1.Controls.Add(this.txtPeriodName);
            this.ultraGroupBox1.Controls.Add(this.txtPeriodCode);
            this.ultraGroupBox1.Controls.Add(this.lblMaterialQuantumName);
            this.ultraGroupBox1.Controls.Add(this.lblMaterialQuantumCode);
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(4, 6);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(501, 132);
            this.ultraGroupBox1.TabIndex = 27;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtPeriodName
            // 
            this.txtPeriodName.Location = new System.Drawing.Point(144, 58);
            this.txtPeriodName.Name = "txtPeriodName";
            this.txtPeriodName.Size = new System.Drawing.Size(346, 21);
            this.txtPeriodName.TabIndex = 6;
            // 
            // txtPeriodCode
            // 
            this.txtPeriodCode.Location = new System.Drawing.Point(144, 29);
            this.txtPeriodCode.Name = "txtPeriodCode";
            this.txtPeriodCode.Size = new System.Drawing.Size(346, 21);
            this.txtPeriodCode.TabIndex = 5;
            // 
            // lblMaterialQuantumName
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            this.lblMaterialQuantumName.Appearance = appearance4;
            this.lblMaterialQuantumName.Location = new System.Drawing.Point(11, 62);
            this.lblMaterialQuantumName.Name = "lblMaterialQuantumName";
            this.lblMaterialQuantumName.Size = new System.Drawing.Size(121, 19);
            this.lblMaterialQuantumName.TabIndex = 1;
            this.lblMaterialQuantumName.Text = "Tên đợt phát hành (*) :";
            // 
            // lblMaterialQuantumCode
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            this.lblMaterialQuantumCode.Appearance = appearance5;
            this.lblMaterialQuantumCode.Location = new System.Drawing.Point(11, 29);
            this.lblMaterialQuantumCode.Name = "lblMaterialQuantumCode";
            this.lblMaterialQuantumCode.Size = new System.Drawing.Size(121, 23);
            this.lblMaterialQuantumCode.TabIndex = 0;
            this.lblMaterialQuantumCode.Text = "Mã đợt phát hành (*) :";
            // 
            // chkActive
            // 
            this.chkActive.BackColor = System.Drawing.Color.Transparent;
            this.chkActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkActive.Location = new System.Drawing.Point(5, 436);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(116, 20);
            this.chkActive.TabIndex = 28;
            this.chkActive.Text = "Ngừng hoạt động";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Near;
            this.ultraGroupBox2.Controls.Add(this.uGrid);
            this.ultraGroupBox2.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox2.Location = new System.Drawing.Point(5, 144);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(500, 284);
            this.ultraGroupBox2.TabIndex = 31;
            this.ultraGroupBox2.Text = "Cổ phần phát hành";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // uGrid
            // 
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(3, 16);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(494, 265);
            this.uGrid.TabIndex = 2;
            // 
            // FEMPublishPeriodDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 468);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.chkActive);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FEMPublishPeriodDetail";
            this.Text = "Thêm mới Đợt phát hành";
            ((System.ComponentModel.ISupportInitialize)(this.dtePeriodDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraLabel lblFromDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtePeriodDate;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPeriodName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPeriodCode;
        private Infragistics.Win.Misc.UltraLabel lblMaterialQuantumName;
        private Infragistics.Win.Misc.UltraLabel lblMaterialQuantumCode;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkActive;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
    }
}