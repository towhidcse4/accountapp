﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Accounting.Properties;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FEMPublishPeriodDetail : CustormForm //UserControl
    {
        #region khai báo
        private readonly IEMPublishPeriodService _IEMPublishPeriodService;
        private readonly IEMPublishPeriodDetailService _IEMPublishPeriodDetailService;
        private readonly IStockCategoryService _IStockCategoryService;
        List<EMPublishPeriod> dsEMPublishPeriod = new List<EMPublishPeriod>();
        List<EMPublishPeriodDetail> dsEMPublishPeriodDetail = new List<EMPublishPeriodDetail>();
        List<StockCategory> dsStockCategory = new List<StockCategory>();



        EMPublishPeriod _Select = new EMPublishPeriod();
        bool Them = true;
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FEMPublishPeriodDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.Text = "Thêm mới Đợt phát hành.";
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IEMPublishPeriodService = IoC.Resolve<IEMPublishPeriodService>();
            _IEMPublishPeriodDetailService = IoC.Resolve<IEMPublishPeriodDetailService>();
            _IStockCategoryService = IoC.Resolve<IStockCategoryService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
            this.chkActive.Visible = false;
        }

        public FEMPublishPeriodDetail(EMPublishPeriod temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            txtPeriodCode.Enabled = false;

            //Khai báo các webservices
            _IEMPublishPeriodService = IoC.Resolve<IEMPublishPeriodService>();
            _IEMPublishPeriodDetailService = IoC.Resolve<IEMPublishPeriodDetailService>();
            _IStockCategoryService = IoC.Resolve<IStockCategoryService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            this.Text = "Sửa Đợt phát hành.";
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }

        private void InitializeGUI()
        {
            #region Lấy dữ liệu từ CSDL

            List<EMPublishPeriodDetail> list1 = new List<EMPublishPeriodDetail>();
            BindingList<EMPublishPeriodDetail> bdEMPublishPeriodDetail = new BindingList<EMPublishPeriodDetail>(list1);
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = bdEMPublishPeriodDetail;
            uGrid.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            uGrid.DisplayLayout.Override.TemplateAddRowPrompt = "Bấm vào đây để thêm mới....";
            uGrid.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.None;
            //Hiện những dòng trống?
            uGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.ExtendFirstCell;
            //tắt lọc cột
            uGrid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGrid.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            // tạo style cho grid
            CreaterColumsStyle(uGrid);
            ConfigGrid(uGrid);
            #endregion
        }
        #endregion

        void CreaterColumsStyle(Infragistics.Win.UltraWinGrid.UltraGrid uGrid)
        {
            #region Grid động
            Infragistics.Win.UltraWinGrid.UltraGridBand band = uGrid.DisplayLayout.Bands[0];


            foreach (var item in band.Columns)
            {
                if (item.Key.Equals("StockCategoryID")) //Tên cột cần hiển thị combobox.
                {
                    List<StockCategory> list = _IStockCategoryService.GetAll();
                    item.Editor = this.CreateCombobox(list, ConstDatabase.StockCategory_TableName, "ID", "StockCategoryName");
                }

                else

                    if (item.Key.Equals("UnitPrice"))
                    {
                        item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                        item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";
                        // item.DefaultCellValue = "1";
                        item.CellAppearance.TextHAlign = HAlign.Right;
                    }
                    else if (item.Key.Equals("Quantity"))
                    {
                        item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                        item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";
                        item.CellAppearance.TextHAlign = HAlign.Right;

                    }
                    else if (item.Key.Equals("Amount"))
                    {
                        item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;

                        item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";
                        item.CellAppearance.TextHAlign = HAlign.Right;
                    }
                    else if (item.Key.Equals("LimitTransferYear"))
                    {
                        item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                        item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";
                        item.DefaultCellValue = "3";
                        item.CellAppearance.TextHAlign = HAlign.Right;
                    }


            }
            #endregion
        }
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.EMPublishPeriodDetail_TableName, new List<TemplateColumn>(), 0);
            Infragistics.Win.UltraWinGrid.UltraGridBand band = utralGrid.DisplayLayout.Bands[0];
            Infragistics.Win.UltraWinGrid.SummarySettings summaryCount = band.Summaries.Add("StockCategoryID", Infragistics.Win.UltraWinGrid.SummaryType.Count, band.Columns["StockCategoryID"]);
            summaryCount.SummaryPosition = Infragistics.Win.UltraWinGrid.SummaryPosition.UseSummaryPositionColumn;
            summaryCount.DisplayFormat = "Số dòng = {0:N0}";
            summaryCount.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.BottomFixed;

            Infragistics.Win.UltraWinGrid.SummarySettings summaryQuantity = band.Summaries.Add("Quantity", Infragistics.Win.UltraWinGrid.SummaryType.Sum, band.Columns["Quantity"]);
            summaryQuantity.SummaryPosition = Infragistics.Win.UltraWinGrid.SummaryPosition.UseSummaryPositionColumn;
            summaryQuantity.DisplayFormat = "{0:N0}";
            summaryQuantity.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.BottomFixed;


            Infragistics.Win.UltraWinGrid.SummarySettings summaryAmount = band.Summaries.Add("Amount", Infragistics.Win.UltraWinGrid.SummaryType.Sum, band.Columns["Amount"]);
            summaryAmount.SummaryPosition = Infragistics.Win.UltraWinGrid.SummaryPosition.UseSummaryPositionColumn;
            summaryAmount.DisplayFormat = "{0:N0}";
            summaryAmount.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.BottomFixed;


        }

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            EMPublishPeriod temp = Them ? new EMPublishPeriod() : _IEMPublishPeriodService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            _IEMPublishPeriodService.BeginTran();
            try
            {
                if (Them)
                {
                    if (!CheckCode()) return;

                    #region fill dữ liệu từ uGrid vào đối tượng EMPublishPeriodDetail
                    List<EMPublishPeriodDetail> uEMPublishPeriodDetail = ((BindingList<EMPublishPeriodDetail>)uGrid.DataSource).ToList();
                    foreach (EMPublishPeriodDetail item in uEMPublishPeriodDetail)
                    {
                        item.ID = Guid.NewGuid();
                        temp.EMPublishPeriodDetails = uEMPublishPeriodDetail;
                    }
                    #endregion

                    _IEMPublishPeriodService.CreateNew(temp);
                    foreach (EMPublishPeriodDetail item in temp.EMPublishPeriodDetails)
                        _IEMPublishPeriodDetailService.Save(item);
                }

                else
                {

                    #region set ID cho đối tượng EMPublishPeriodDetail
                    List<EMPublishPeriodDetail> uEMPublishPeriodDetail = ((BindingList<EMPublishPeriodDetail>)uGrid.DataSource).ToList();
                    foreach (EMPublishPeriodDetail item in uEMPublishPeriodDetail)
                    {

                        if (item.ID.ToString() == "00000000-0000-0000-0000-000000000000")
                        {
                            item.ID = Guid.NewGuid();
                        }
                        temp.EMPublishPeriodDetails = uEMPublishPeriodDetail;
                    }
                    #endregion

                    foreach (EMPublishPeriodDetail item in temp.EMPublishPeriodDetails)
                        _IEMPublishPeriodDetailService.Save(item);
                    _IEMPublishPeriodService.Update(temp);
                }

                _IEMPublishPeriodService.CommitTran();
            }
            catch (Exception ex)
            {
                _IEMPublishPeriodService.RolbackTran();
            }
            #endregion

            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }
        #endregion


        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        EMPublishPeriod ObjandGUI(EMPublishPeriod input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.PeriodCode = txtPeriodCode.Text;
                input.PeriodName = txtPeriodName.Text;
                input.PeriodDate = dtePeriodDate.DateTime;
                input.TypeID = 1;
                input.IsActive = chkActive.CheckState == CheckState.Checked ? false : true;
                #region fill dữ liệu từ uGrid vào đối tượng EMPublishPeriodDetail
                List<EMPublishPeriodDetail> uEMPublishPeriodDetail = ((BindingList<EMPublishPeriodDetail>)uGrid.DataSource).ToList();
                foreach (EMPublishPeriodDetail item in uEMPublishPeriodDetail)
                {
                    item.EMPublishPeriodID = input.ID;
                    input.EMPublishPeriodDetails = uEMPublishPeriodDetail;
                }
                #endregion
                List<StockCategory> AllSt = _IStockCategoryService.GetAll().ToList();
                input.QuantityCommonShare = ((from St in AllSt
                                              join up in uEMPublishPeriodDetail on St.ID equals up.StockCategoryID
                                              where St.StockKind == 0
                                              select up).ToList()
                                            ).Sum(a => a.Quantity);
                input.QuantityPreferredShare = ((from St in AllSt
                                                 join up in uEMPublishPeriodDetail on St.ID equals up.StockCategoryID
                                                 where St.StockKind == 1
                                                 select up).ToList()
                                            ).Sum(a => a.Quantity);
                input.QuantityTotal = (input.QuantityCommonShare + input.QuantityPreferredShare);
            }
            else
            {
                txtPeriodCode.Value = input.PeriodCode;
                txtPeriodName.Value = input.PeriodName;
                dtePeriodDate.Value = input.PeriodDate;
                chkActive.CheckState = input.IsActive ? CheckState.Unchecked : CheckState.Checked;
                // list cổ phần
                //BindingList<EMPublishPeriodDetail> bdEMPublishPeriodDetail = new BindingList<EMPublishPeriodDetail>(_IEMPublishPeriodDetailService.Query.Where(p => p.EMPublishPeriodID == input.ID).ToList());
                BindingList<EMPublishPeriodDetail> bdEMPublishPeriodDetail = _IEMPublishPeriodDetailService.GetBindingListEMPulishPeriodDetailID(input.ID);
                uGrid.DataSource = bdEMPublishPeriodDetail;
                CreaterColumsStyle(uGrid);
                ConfigGrid(uGrid);


            }
            return input;
        }

        #region sự kiện khi thay đổi dữ liệu trên Grid con
        private void uGrid_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {

        }

        #endregion

        private void uGrid_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {

        }



        bool CheckCode()
        {
            bool kq = true;
            //Check Error Chung

            //List<string> hh = _IEMPublishPeriodService.Query.Select(a => a.PeriodCode).ToList();
            List<string> hh = _IEMPublishPeriodService.GetListPublishPeriodCode();
            foreach (var item in hh)
            {
                if (item.Equals(txtPeriodCode.Text))
                {
                    MessageBox.Show("Mã cổ phần " + item + " đã tồn tại trong danh sách", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            return kq;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtPeriodCode.Text) || string.IsNullOrEmpty(txtPeriodName.Text) || string.IsNullOrEmpty(dtePeriodDate.Value.ToString()))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            return kq;
        }

        #endregion
    }
}
