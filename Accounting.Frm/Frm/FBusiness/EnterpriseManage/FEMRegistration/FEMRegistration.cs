﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using System.Linq;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;


namespace Accounting
{
    public partial class FEMRegistration : CustormForm //UserControl
    {
        #region khai báo
        private readonly IEMRegistrationService _IEMRegistrationService;
        private readonly IEMPublishPeriodDetailService _IEMPublishPeriodDetailService;
        private readonly IEMPublishPeriodService _IEMPublishPeriodService;
        private readonly IEMRegistrationDetailService _IEMRegistrationDetailService;
        private readonly IStockCategoryService _IStockCategoryService;
        //      private readonly IViewListPeriodService _IViewListPeriodService;
        List<EMRegistration> dsEMRegistration = new List<EMRegistration>();
        List<EMRegistrationDetail> dsEMRegistrationDetail = new List<EMRegistrationDetail>();
        List<EMPublishPeriod> dsEMPublishPeriod = new List<EMPublishPeriod>();
        List<EMPublishPeriodDetail> dsEMPublishPeriodDetail = new List<EMPublishPeriodDetail>();
        List<StockCategory> dsStockCategory = new List<StockCategory>();
        List<ViewListPeriod> dsViewListPeriod = new List<ViewListPeriod>();
        private List<EMRegistration> list;

        #endregion

        #region khởi tạo
        public FEMRegistration()
        {
            WaitingFrm.StartWaiting();
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _IStockCategoryService = IoC.Resolve<IStockCategoryService>();
            _IEMRegistrationService = IoC.Resolve<IEMRegistrationService>();
            _IEMRegistrationDetailService = IoC.Resolve<IEMRegistrationDetailService>();
            _IEMPublishPeriodService = IoC.Resolve<IEMPublishPeriodService>();
            _IEMPublishPeriodDetailService = IoC.Resolve<IEMPublishPeriodDetailService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();

            #endregion
            WaitingFrm.StopWaiting();
        }

        public FEMRegistration(List<EMRegistration> list)
        {
            // TODO: Complete member initialization
            this.list = list;
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            #region Lấy dữ liệu từ CSDL
            List<EMPublishPeriod> list = _IEMPublishPeriodService.GetAll();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list.ToArray();
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            if (configGrid) ConfigGrid(uGrid);

            #endregion
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click_1(object sender, EventArgs e)
        {
            addFunction();
        }

        private void tsmEdit_Click_1(object sender, EventArgs e)
        {
            editFunction();
        }

        private void tsmDelete_Click_1(object sender, EventArgs e)
        {
            deleteFunction();
        }


        private void tmsReLoad_Click_1(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }

        //private void toolStripMenuItem2_Click(object sender, EventArgs e)
        //{
        //    addFunction();
        //}

        //private void toolStripMenuItem3_Click(object sender, EventArgs e)
        //{
        //    editFunction();
        //}

        //private void toolStripMenuItem4_Click(object sender, EventArgs e)
        //{
        //    deleteFunction();
        //}

        //private void toolStripMenuItem5_Click(object sender, EventArgs e)
        //{
        //    LoadDuLieu();
        //}

        //private void uGrid_MouseDown_1(object sender, MouseEventArgs e)
        //{
        //    Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        //}

        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            addFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            editFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            deleteFunction();


        }
        #endregion

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index == -1)
                return;
            editFunction();
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        void addFunction()
        {
            new FEMPublishPeriodDetail().ShowDialog(this);
            if (!FEMPublishPeriodDetail.isClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        void editFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                EMPublishPeriod temp = uGrid.Selected.Rows[0].ListObject as EMPublishPeriod;
                new FEMPublishPeriodDetail(temp).ShowDialog(this);
                if (!FEMPublishPeriodDetail.isClose) LoadDuLieu();
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        void deleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {

                EMPublishPeriod temp = uGrid.Selected.Rows[0].ListObject as EMPublishPeriod;


                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.PeriodName)) == System.Windows.Forms.DialogResult.Yes)
                {
                    try
                    {
                        _IEMPublishPeriodService.BeginTran();
                        _IEMPublishPeriodService.Delete(temp);
                        _IEMPublishPeriodService.CommitTran();

                    }
                    catch
                    {
                        _IEMPublishPeriodService.RolbackTran();
                    }
                }



            }
            else
                MSG.Error(resSystem.MSG_System_06);
            LoadDuLieu();

        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.EMPublishPeriod_TableName);
        }
        #endregion

        private void uGrid_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {

        }

        private void cms4Grid_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void uGrid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            #region Config Grid
            #region GirdListRegistration
            //hiển thị 1 band
            if (uGrid.Selected.Rows.Count > 0)
            {
                EMPublishPeriod temp = uGrid.Selected.Rows[0].ListObject as EMPublishPeriod;
                //List<EMRegistration> list = _IEMRegistrationService.Query.Where(a => a.EMPublishPeriodID == temp.ID ).ToList();
                List<EMRegistration> list = _IEMRegistrationService.GetListEMRegistrationID(temp.ID);
                uGridListRegistration.DataSource = list.ToArray();
                if (uGridListRegistration.Rows.Count > 0) uGridListRegistration.Rows[0].Selected = true;
                uGridListRegistration.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
                //tắt lọc cột
                uGridListRegistration.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
                uGridListRegistration.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
                //tự thay đổi kích thước cột
                uGridListRegistration.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                //tắt tiêu đề
                uGridListRegistration.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
                //select cả hàng hay ko?
                uGridListRegistration.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
                uGridListRegistration.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;

                //Hiện những dòng trống?
                uGridListRegistration.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
                uGridListRegistration.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
                //Fix Header
                uGridListRegistration.DisplayLayout.UseFixedHeaders = true;

                UltraGridBand band = uGridListRegistration.DisplayLayout.Bands[0];
                band.Summaries.Clear();
                Utils.ConfigGrid(uGridListRegistration, TextMessage.ConstDatabase.EMRegistration_TableName);
            }
            #endregion
            #region GridListPeriod
            //hiển thị 1 band
            if (uGrid.Selected.Rows.Count > 0)
            {
                EMPublishPeriod temp = uGrid.Selected.Rows[0].ListObject as EMPublishPeriod;
                List<StockCategory> AllSt = _IStockCategoryService.GetAll().ToList();
                List<EMPublishPeriodDetail> AllPpd = _IEMPublishPeriodDetailService.GetAll().ToList();
                var list = (from Ppd in AllPpd
                            join St in AllSt on Ppd.StockCategoryID equals St.ID
                            where Ppd.EMPublishPeriodID == temp.ID
                            select new
                            {
                                EMPublishPeriodID = Ppd.EMPublishPeriodID,
                                StockCategoryName = St.StockCategoryName,
                                UnitPrice = Ppd.UnitPrice,
                                Quantity = Ppd.Quantity,
                                Amount = Ppd.Amount,
                                LimitTransferYear = Ppd.LimitTransferYear
                            }
                                             ).ToList();
                uGridListPeriod.DataSource = list.ToArray();
                if (uGridListPeriod.Rows.Count > 0) uGridListPeriod.Rows[0].Selected = true;
                uGridListPeriod.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
                //tắt lọc cột
                uGridListPeriod.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
                uGridListPeriod.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
                //tự thay đổi kích thước cột
                uGridListPeriod.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                //tắt tiêu đề
                uGridListPeriod.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
                //select cả hàng hay ko?
                uGridListPeriod.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
                uGridListPeriod.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;

                //Hiện những dòng trống?
                uGridListPeriod.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
                uGridListPeriod.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
                //Fix Header
                uGridListPeriod.DisplayLayout.UseFixedHeaders = true;

                UltraGridBand band = uGridListPeriod.DisplayLayout.Bands[0];
                band.Summaries.Clear();
                Utils.ConfigGrid(uGridListPeriod, TextMessage.ConstDatabase.ViewListPeriod_TableName);
            }

            #endregion
            #endregion

        }
        //BUTTON___________________________________________________//////

        private void bttRegistration_Click(object sender, EventArgs e)
        {
            Add();
        }
        private void bttGhiso_Click(object sender, EventArgs e)
        {
            Ghiso();

        }
        private void bttEditRegistration_Click(object sender, EventArgs e)
        {
            Edit();

        }

        private void bttDeleteRegistration_Click(object sender, EventArgs e)
        {
            Delete();

        }



        //menutrip.....
        private void tmsAdd_Registration_Click(object sender, EventArgs e)
        {
            Add();
        }

        private void tsmEdit_Registration_Click(object sender, EventArgs e)
        {
            Edit();
        }

        private void tsmDelete_Registration_Click(object sender, EventArgs e)
        {
            Delete();
        }

        private void tmsBoghi_Click(object sender, EventArgs e)
        {
            Ghiso();
        }

        private void uGridListRegistration_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index == -1) return;
            Edit();
        }

        private void uGridListRegistration_MouseDown(object sender, MouseEventArgs e)
        {
            //Utils.uGridListRegistration_MouseDown(sender, e, uGrid, cms4Grid);
            Utils.uGrid_MouseDown(sender, e, uGridListRegistration, cms4Grid1);
        }

        /// <summary>
        /// NGHIỆP VỤ.....................
        /// </summary>

        void Add()
        {

            new FERegistrationDetail().ShowDialog(this);
            if (!FERegistrationDetail.isClose) LoadDuLieu();

        }
        void Edit()
        {
            if (uGridListRegistration.Selected.Rows.Count > 0)
            {
                EMRegistration temp = uGridListRegistration.Selected.Rows[0].ListObject as EMRegistration;
                new FERegistrationDetail(temp).ShowDialog(this);
                if (!FERegistrationDetail.isClose) LoadDuLieu();
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }
        void Delete()
        {
            if (uGridListRegistration.Selected.Rows.Count > 0)
            {

                EMRegistration temp = uGridListRegistration.Selected.Rows[0].ListObject as EMRegistration;



                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.RegistrationName)) == System.Windows.Forms.DialogResult.Yes)
                {
                    try
                    {
                        _IEMRegistrationService.BeginTran();
                        _IEMRegistrationService.Delete(temp);
                        _IEMRegistrationService.CommitTran();

                    }
                    catch
                    {
                        _IEMRegistrationService.RolbackTran();
                    }
                }



            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        void Ghiso()
        {
            if (uGridListRegistration.Selected.Rows.Count > 0)
            {
                EMRegistration temp = uGridListRegistration.Selected.Rows[0].ListObject as EMRegistration;
                if (temp.Recorded == true)
                {
                    try
                    {

                        temp.Recorded = false;
                        _IEMRegistrationService.BeginTran();
                        _IEMRegistrationService.Update(temp);
                        _IEMRegistrationService.CommitTran();
                        bttGhiso.Text = "Bỏ ghi";
                    }
                    catch (Exception ex)
                    {
                        _IEMRegistrationService.RolbackTran();
                    }

                }
                else
                {
                    try
                    {

                        temp.Recorded = true;
                        _IEMRegistrationService.BeginTran();
                        _IEMRegistrationService.Update(temp);
                        _IEMRegistrationService.CommitTran();
                        bttGhiso.Text = "Ghi sổ";
                    }
                    catch (Exception ex)
                    {
                        _IEMRegistrationService.RolbackTran();
                    }
                }

            }
            else
            {
                MSG.Error(resSystem.MSG_System_06);
            }
        }



        private void uGridListRegistration_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {

        }

        private void uGridListRegistration_Click(object sender, EventArgs e)
        {

            checkGhisoBtt(sender);
        }

        void checkGhisoBtt(object sJender)
        {
            EMRegistration temp = uGridListRegistration.Selected.Rows[0].ListObject as EMRegistration;
            if (temp.Recorded == true)
            {
                bttDeleteRegistration.Enabled = false;
                bttEditRegistration.Enabled = false;
                bttGhiso.Text = "Bỏ ghi";
            }
            else
            {

                bttDeleteRegistration.Enabled = true;
                bttEditRegistration.Enabled = true;
                bttGhiso.Text = "Ghi sổ";
            }
        }

        private void FEMRegistration_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
            
            uGridListRegistration.ResetText();
            uGridListRegistration.ResetUpdateMode();
            uGridListRegistration.ResetExitEditModeOnLeave();
            uGridListRegistration.ResetRowUpdateCancelAction();
            uGridListRegistration.DataSource = null;
            uGridListRegistration.Layouts.Clear();
            uGridListRegistration.ResetLayouts();
            uGridListRegistration.ResetDisplayLayout();
            uGridListRegistration.Refresh();
            uGridListRegistration.ClearUndoHistory();
            uGridListRegistration.ClearXsdConstraints();

            uGridListPeriod.ResetText();
            uGridListPeriod.ResetUpdateMode();
            uGridListPeriod.ResetExitEditModeOnLeave();
            uGridListPeriod.ResetRowUpdateCancelAction();
            uGridListPeriod.DataSource = null;
            uGridListPeriod.Layouts.Clear();
            uGridListPeriod.ResetLayouts();
            uGridListPeriod.ResetDisplayLayout();
            uGridListPeriod.Refresh();
            uGridListPeriod.ClearUndoHistory();
            uGridListPeriod.ClearXsdConstraints();
        }

        private void FEMRegistration_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
