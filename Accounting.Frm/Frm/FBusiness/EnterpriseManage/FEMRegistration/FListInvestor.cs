﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.ComponentModel;
using System.Linq;

namespace Accounting
{
    public partial class FListInvestor : CustormForm
    {
        #region khai báo 
        private readonly IInvestorService _IInvestorService;
        public static List<Investor> dsInvestor = new List<Investor>();
        //private List<Investor> list;
        Investor _Select = new Investor();

        public Investor InvestorSelect
        {
            get { return _Select; }
            set { _Select = value; }
        }

        #endregion

        public FListInvestor()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            _IInvestorService = IoC.Resolve<IInvestorService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            Loaddulieu();
           
        }

        void Loaddulieu()
        {
            Loaddulieu(true);
        }

        private void Loaddulieu(bool configGrid)
        {
            #region Lấy dữ liệu từ CSDL
            List<Investor> list = _IInvestorService.GetAll();
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list.ToArray();
            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }
            if (configGrid) ConfigGrid(uGrid);

            #endregion
        }

        #region Utils
        //void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        //{//hàm chung
        //    Utils.ConfigGrid(utralGrid, fEMShareHolder.database.EMRegistrations_TableName, false);
        //}

        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {
            List<TemplateColumn> dstemplatecolums = new List<TemplateColumn>();

            Dictionary<string, Dictionary<string, TemplateColumn>> dicTemp = new Dictionary<string, Dictionary<string, TemplateColumn>>();
            string nameTable = string.Empty;
            List<string> strColumnName, strColumnCaption, strColumnToolTip = new List<string>();
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb = new List<bool>();
            List<int> intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition = new List<int>();
            List<int> VTbolIsVisible, VTbolIsVisibleCbb = new List<int>();

            strColumnName = new List<string>() {"ID", "InvestorCode", "InvestorName", "InvestorCategoryID", "InvestorCategoryIDVIEW", "Address", "Tel", "Fax", "Email", "Website",
                                                "BusinessRegistrationNumber", "IssueDate", "IssueBy", "BankAccount", "BankName", "TaxCode", "ContactName", "ContactTitle", "ContactPrefix", "ContactMobile",
                                                "ContactEmail", "ContactOfficeTel", "ContactAddress", "InvestorType", "IdenticationNumber", "ContactIssueDate", "ContactIssueBy", "IsActive" };
            strColumnCaption = strColumnToolTip = new List<string>() { "ID", "Mã nhà đầu tư", "Tên nhà đầu tư", "InvestorCategoryID", "Nhóm nhà đầu tư", "Địa chỉ", "Tel", "Fax", "Email", "Website",
                                                "BusinessRegistrationNumber", "IssueDate", "IssueBy", "BankAccount", "BankName", "TaxCode", "ContactName", "ContactTitle", "ContactPrefix", "ContactMobile",
                                                "ContactEmail", "Điện thoại cơ quan", "ContactAddress", "InvestorType", "IdenticationNumber", "ContactIssueDate", "ContactIssueBy", "IsActive"};

         
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() {1, 2 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 1, 2 };
            for (int i = 0; i < 28; i++)
            {
                bolIsVisible.Add(VTbolIsVisible.Contains(i));
                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            dstemplatecolums = ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();

            Utils.ConfigGrid(utralGrid, fEMShareHolder.database.EMShareHolder_TableName, dstemplatecolums, 0);
        }
        #endregion


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                this.InvestorSelect = uGrid.Selected.Rows[0].ListObject as Investor;
            }
            
             this.Close();
        }

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            System.Drawing.Point point = new System.Drawing.Point(e.X, e.Y);
            //lấy kiểu của đối tượng sender (đối tượng gây ra sự kiện)
            System.Type type = sender.GetType();
            UltraGrid uGrid = (UltraGrid)sender;
            //lấy UltraGridCell từ vị trí
            UltraGridCell cell = (UltraGridCell)((UltraGridBase)sender).DisplayLayout.UIElement.ElementFromPoint(point).GetContext(typeof(UltraGridCell));
            //nếu đang bấm chuột phải và đối tượng đang chọn khác null thì thao tác
            if (cell != null && cell.Row.Index >= 0)
            {
                uGrid.Rows[cell.Row.Index].Selected = true;
            }
        }

    }
   
}
