﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.Text.RegularExpressions;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Accounting.Properties;


namespace Accounting
{
    public partial class FERegistrationDetail : CustormForm
    {

        #region khai báo
        public Guid IdPublishPeriod;
        private readonly IEMRegistrationService _IEMRegistrationService;
        private readonly IEMRegistrationDetailService _IEMRegistrationDetailService;
        private readonly IEMPublishPeriodService _IEMPublishPeriodService;
        private readonly IEMPublishPeriodDetailService _IEMPublishPeriodDetailService;
        private readonly IStockCategoryService _IStockCategoryService;
        private readonly IRegistrationGroupService _IRegistrationGroupService;
        private readonly IGenCodeService _IGenCodeService;
        private readonly IInvestorService _IInvestorService;
        private readonly IEMShareHolderService _IEMShareHolderService;
        private readonly IBankAccountDetailService _IBankAccountDetailService;

        EMRegistration _Select = new EMRegistration();
        Investor _Select1 = new Investor();
        EMShareHolder _Select2 = new EMShareHolder();
        List<string> ltemp = new List<string>() { "Ông", "Bà" };
        bool Them = true;
        public static bool isClose = true;
        private Investor temp;

        #endregion
        #region khởi tạo
        public FERegistrationDetail()
        {//Thêm

            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.Text = "Thêm mới Đăng ký mua cổ phần-Đợt phát hành.";
            #endregion
            //txtRegistrationCode.Enabled = false;
            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IEMRegistrationService = IoC.Resolve<IEMRegistrationService>();
            _IEMRegistrationDetailService = IoC.Resolve<IEMRegistrationDetailService>();
            _IEMPublishPeriodService = IoC.Resolve<IEMPublishPeriodService>();
            _IEMPublishPeriodDetailService = IoC.Resolve<IEMPublishPeriodDetailService>();
            _IRegistrationGroupService = IoC.Resolve<IRegistrationGroupService>();
            _IGenCodeService = IoC.Resolve<IGenCodeService>();
            _IStockCategoryService = IoC.Resolve<IStockCategoryService>();
            _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            //IdPublishPeriod =  temp.ID;
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion


            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            dtePostedDate.DateTime = DateTime.Now;
            dtContactIssueDate.DateTime = DateTime.Now;
            this.Text = "Thêm mới Đăng ký mua cổ phần-Đợt phát hành.";
            chkIsActive.Visible = false;
            #endregion
        }
        public FERegistrationDetail(EMRegistration temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            txtRegistrationCode.Enabled = false;

            //Khai báo các webservices
            _IEMRegistrationService = IoC.Resolve<IEMRegistrationService>();
            _IEMRegistrationDetailService = IoC.Resolve<IEMRegistrationDetailService>();
            _IEMPublishPeriodService = IoC.Resolve<IEMPublishPeriodService>();
            _IEMPublishPeriodDetailService = IoC.Resolve<IEMPublishPeriodDetailService>();
            _IRegistrationGroupService = IoC.Resolve<IRegistrationGroupService>();
            _IGenCodeService = IoC.Resolve<IGenCodeService>();
            _IStockCategoryService = IoC.Resolve<IStockCategoryService>();
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //IdPublishPeriod = temp.EMPublishPeriodID.Value;

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();

            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            this.Text = "Sửa Đăng ký mua cổ phần-Đợt phát hành.";

            #endregion
        }

        public FERegistrationDetail(Investor temp1)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select1 = temp1;
            Them = false;

            //Khai báo các webservices
            _IInvestorService = IoC.Resolve<IInvestorService>();

            InitializeGUI();

            #endregion

            #region Fill dữ liệu Obj vào control

            FillInvestorInfo(temp1);
            #endregion

        }

        public FERegistrationDetail(EMShareHolder temp2)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select2 = temp2;
            Them = false;

            //Khai báo các webservices
            _IEMShareHolderService = IoC.Resolve<IEMShareHolderService>();

            InitializeGUI();

            #endregion

            #region Fill dữ liệu Obj vào control

            FillEMShareHolder(temp2);
            #endregion

        }

        private void FillInvestorInfo(Investor temp1)
        {
            txtRegistrationName1.Text = temp1.InvestorName;
            txtIndentication.Text = temp1.IdenticationNumber;
            txtContactIssueBy.Text = temp1.ContactIssueBy;
            if (!dtContactIssueDate.Equals("")) { } else { dtContactIssueDate.DateTime = (DateTime)temp1.ContactIssueDate; }

            txtDTCoQuan.Text = temp1.ContactOfficeTel;
            txtContactMobile.Text = temp1.ContactMobile;
            txtAddress.Text = temp1.Address;
            txtBusinessRegistrationNumber.Text = temp1.BusinessRegistrationNumber;
            if (!dtePostedDate.Equals("")) { } else { dtePostedDate.DateTime = (DateTime)temp1.IssueDate; }

            txtIssueBy.Text = temp1.IssueBy;
            txtTel.Text = temp1.Tel;
            txtFax.Text = temp1.Fax;

        }
        private void FillEMShareHolder(EMShareHolder temp2)
        {
            txtRegistrationName1.Text = temp2.ShareHolderName;
            txtIndentication.Text = temp2.ContactIdentificationNo;
            txtContactIssueBy.Text = temp2.ContactIssueBy;
            if (!dtContactIssueDate.Equals("")) { } else { dtContactIssueDate.DateTime = (DateTime)temp2.ContactIssueDate; }

            txtDTCoQuan.Text = temp2.ContactOfficeTel;
            txtContactMobile.Text = temp2.ContactMobile;
            txtAddress.Text = temp2.Address;
            txtBusinessRegistrationNumber.Text = temp2.BusinessRegistrationNumber;
            if (!dtePostedDate.Equals("")) { } else { dtePostedDate.DateTime = (DateTime)temp2.BusinessIssuedDate; }

            txtIssueBy.Text = temp2.BusinessIssueBy;
            txtTel.Text = temp2.Tel;
            txtFax.Text = temp2.Fax;

        }



        void loadcbb()
        {
            cbbInvestorCategoryID.DataSource = _IRegistrationGroupService.GetAll();
            cbbInvestorCategoryID.DisplayMember = "RegistrationGroupName";
            Utils.ConfigGrid(cbbInvestorCategoryID, ConstDatabase.RegistrationGroup_TableName);
        }
        private void InitializeGUI()
        {
            //Tự sinh số chứng từ
            GenCode gencode = _IGenCodeService.getGenCode(90);
            string curValue = string.Empty;
            for (int i = 0; i < gencode.Length - gencode.CurrentValue.ToString().Length; i++) curValue += "0";
            txtRegistrationCode.Text = string.Format("{0}{1}{2}{3}", gencode.Prefix, curValue, gencode.CurrentValue, gencode.Suffix);
            //khởi tạo cbb

            loadcbb();

            cbbContactPrefix.DataSource = ltemp;
            cbbContactPrefix.SelectedIndex = -1;
            ultraOptionSet1.CheckedIndex = 0;
            #region Lấy dữ liệu từ CSDL

            List<EMRegistrationDetail> list1 = new List<EMRegistrationDetail>();
            BindingList<EMRegistrationDetail> bdEMRegistrationDetail = new BindingList<EMRegistrationDetail>(list1);
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGridLM.DataSource = bdEMRegistrationDetail;
            uGridLM.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            uGridLM.DisplayLayout.Override.TemplateAddRowPrompt = "Bấm vào đây để thêm mới....";
            uGridLM.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.None;
            //Hiện những dòng trống?
            uGridLM.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridLM.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.ExtendFirstCell;
            //tắt lọc cột
            uGridLM.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            uGridLM.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.Default;
            //tắt tiêu đề
            uGridLM.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            // tạo style cho grid
            CreaterColumsStyle(uGridLM);
            ConfigGrid(uGridLM);
            #endregion
        }
        #endregion
        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click_1(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion
            if (txtEmail.Text != "")
            {
                if (!isValidEmail(txtEmail.Text)) return;
            }
            if (txtTel.Text != "")
            {
                if (!isPhone(txtTel.Text)) return;
            }
            if (txtContactMobile.Text != "")
            {
                if (!isPhone(txtContactMobile.Text)) return;
            }

            #region Fill dữ liệu control vào obj
            EMRegistration temp = Them ? new EMRegistration() : _IEMRegistrationService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            try
            {
                _IEMRegistrationService.BeginTran();
                if (Them)
                {
                    if (!CheckCode()) return;
                    #region fill dữ liệu từ uGrid vào đối tượng EMPublishPeriodDetail
                    List<EMRegistrationDetail> uEMPublishPeriodDetail = ((BindingList<EMRegistrationDetail>)uGridLM.DataSource).ToList();
                    foreach (EMRegistrationDetail item in uEMPublishPeriodDetail)
                    {
                        item.ID = Guid.NewGuid();
                        item.EMRegistrationID = temp.ID;
                        temp.EMRegistrationDetails = uEMPublishPeriodDetail;
                    }
                    #endregion

                    _IEMRegistrationService.CreateNew(temp);

                    foreach (EMRegistrationDetail item in temp.EMRegistrationDetails)
                        _IEMRegistrationDetailService.Save(item);
                    Dictionary<string, string> kq = Utils.CheckNo(txtRegistrationCode.Text);
                    if (kq != null)
                    {
                        GenCode gencode = _IGenCodeService.getGenCode(90);
                        gencode.Prefix = kq["Prefix"];
                        gencode.Suffix = kq["Suffix"];
                        gencode.CurrentValue = decimal.Parse(kq["Value"]) + 1;
                        _IGenCodeService.Update(gencode);
                    }
                }
                else
                {
                    #region set ID cho đối tượng EMPublishPeriodDetail
                    List<EMRegistrationDetail> uEMRegistrationDetail = ((BindingList<EMRegistrationDetail>)uGridLM.DataSource).ToList();
                    foreach (EMRegistrationDetail item in uEMRegistrationDetail)
                    {

                        if (item.ID.ToString() == "00000000-0000-0000-0000-000000000000")
                        {
                            item.ID = Guid.NewGuid();
                        }
                        temp.EMRegistrationDetails = uEMRegistrationDetail;
                    }
                    #endregion
                    foreach (EMRegistrationDetail item in temp.EMRegistrationDetails)
                        _IEMRegistrationDetailService.Save(item);
                    _IEMRegistrationService.Update(temp);
                }
                _IEMRegistrationService.CommitTran();

            }
            catch (Exception)
            {
                _IEMRegistrationService.RolbackTran();
            }
            #endregion

            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }
        #endregion
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        EMRegistration ObjandGUI(EMRegistration input, bool isGet)
        {

            if (isGet)
            {


                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới
                input.EMPublishPeriodID = IdPublishPeriod;
                input.RegistrationCode = txtRegistrationCode.Text;
                input.RegistrationName = txtRegistrationName1.Text;
                input.Address = txtAddress.Text;
                //input.BankAccount = txtBankAcount.Text;
                input.BankAccountDetailID = _IBankAccountDetailService.GetGuildFromBankAccount(txtBankAcount.Text);
                input.ContactOfficeTel = txtDTCoQuan.Text;
                input.TypeID = 1;
                input.IsPersonal = ultraOptionSet1.CheckedIndex == 1 ? true : false;
                input.ContactIdentificationNo = txtIndentication.Text;
                input.BusinessRegistrationNumber = txtBusinessRegistrationNumber.Text;
                input.Tel = txtTel.Text;
                input.ContactTaxCode = txtTaxCode.Text;
                input.ContactOfficeTel = txtDTCoQuan.Text;
                input.BankName = txtBankName.Text;
                input.Fax = txtFax.Text;
                input.ContactIssueBy = txtContactIssueBy.Text;
                input.BusinessRegistrationIssueBy = txtIssueBy.Text;
                input.ContactMobile = txtContactMobile.Text;
                input.Website = txtWebsite.Text;
                input.Email = txtEmail.Text;
                input.ContactHomeTel = txtContactHomeTel.Text;
                input.ContactIssueDate = dtContactIssueDate.DateTime;
                if (cbbContactPrefix.SelectedItem != null)
                    input.ContactPrefix = cbbContactPrefix.SelectedItem.ToString();
                input.BusinessRegistrationIssueDate = dtePostedDate.DateTime;
                //từ tài khoản
                RegistrationGroup temp0 = (RegistrationGroup)Utils.getSelectCbbItem(cbbInvestorCategoryID);
                if (temp0 == null) input.RegistrationGroupID = null;
                else input.RegistrationGroupID = temp0.ID;
                //========> input.OrderFixCode chưa xét
                input.Status = chkIsActive.CheckState == CheckState.Checked ? false : true;
                #region fill dữ liệu từ uGrid vào đối tượng EMPublishPeriodDetail
                List<EMRegistrationDetail> uEMRegistrationDetail = ((BindingList<EMRegistrationDetail>)uGridLM.DataSource).ToList();
                foreach (EMRegistrationDetail item in uEMRegistrationDetail)
                {
                    item.EMRegistrationID = input.ID;
                    input.EMRegistrationDetails = uEMRegistrationDetail;
                }

                input.TotalAmountBought = ((from uRd in uEMRegistrationDetail where uRd.EMRegistrationID == input.ID select uRd).ToList()).Sum(a => a.AmountBought);
                input.TotalQuantityApproved = ((from uRd in uEMRegistrationDetail where uRd.EMRegistrationID == input.ID select uRd).ToList()).Sum(a => a.QuantityApproved);
                input.TotalQuantityBought = ((from uRd in uEMRegistrationDetail where uRd.EMRegistrationID == input.ID select uRd).ToList()).Sum(a => a.QuantityBought);
                input.TotalQuantityBuyable = ((from uRd in uEMRegistrationDetail where uRd.EMRegistrationID == input.ID select uRd).ToList()).Sum(a => a.QuantityBuyable);
                input.TotalQuantityRegisted = ((from uRd in uEMRegistrationDetail where uRd.EMRegistrationID == input.ID select uRd).ToList()).Sum(a => a.QuantityRegistered);

                #endregion

            }
            else
            {
                txtRegistrationCode.Text = input.RegistrationCode;
                txtRegistrationName1.Text = input.RegistrationName;
                ultraOptionSet1.CheckedIndex = input.IsPersonal == true ? 1 : 0;
                txtIndentication.Text = input.ContactIdentificationNo;
                cbbContactPrefix.SelectedText = input.ContactPrefix;
                txtBusinessRegistrationNumber.Text = input.BusinessRegistrationNumber;
                txtAddress.Text = input.Address;
                //txtBankAcount.Text = input.BankAccount;
                input.BankAccountDetailID = _IBankAccountDetailService.GetGuildFromBankAccount(txtBankAcount.Text);
                txtBankName.Text = input.BankName;
                txtDTCoQuan.Text = input.ContactOfficeTel;
                txtTel.Text = input.Tel;
                txtContactHomeTel.Text = input.ContactHomeTel;
                dtePostedDate.DateTime = (DateTime)input.BusinessRegistrationIssueDate;
                dtContactIssueDate.DateTime = (DateTime)input.ContactIssueDate;
                txtTaxCode.Text = input.ContactTaxCode;
                txtBankName.Text = input.BankName;
                txtFax.Text = input.Fax;
                txtIssueBy.Text = input.BusinessRegistrationIssueBy;
                txtContactIssueBy.Text = input.ContactIssueBy;
                txtContactMobile.Text = input.ContactMobile;
                txtWebsite.Text = input.Website;
                txtEmail.Text = input.Email;
                //từ tài khoản
                foreach (var item in cbbInvestorCategoryID.Rows)
                {
                    if ((item.ListObject as RegistrationGroup).ID == input.RegistrationGroupID) cbbInvestorCategoryID.SelectedRow = item;
                }
                chkIsActive.CheckState = input.Status ? CheckState.Unchecked : CheckState.Checked;
                //BindingList<EMRegistrationDetail> bdEMRegistrationDetail = new BindingList<EMRegistrationDetail>(_IEMRegistrationDetailService.Query.Where(p => p.EMRegistrationID == input.ID).ToList());
                BindingList<EMRegistrationDetail> bdEMRegistrationDetail = _IEMRegistrationDetailService.GetBinddingEMRegistrationDetailID(temp.ID);
                uGridLM.DataSource = bdEMRegistrationDetail;
                CreaterColumsStyle(uGridLM);
                ConfigGrid(uGridLM);

            }
            return input;
        }

        bool CheckCode()
        {
            bool kq = true;
            //Check Error Chung
            //List<string> hh = _IEMRegistrationService.Query.Select(a => a.RegistrationCode).ToList();
            List<string> hh = _IEMRegistrationService.GetListEMRegistrationCode();
            foreach (var item in hh)
            {
                if (item.Equals(txtRegistrationCode.Text))
                {
                    MessageBox.Show("Mã " + item + " đã tồn tại trong danh sách", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            return kq;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtRegistrationCode.Text) || string.IsNullOrEmpty(txtRegistrationName1.Text) || cbbInvestorCategoryID.SelectedRow == null)
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            return kq;
        }
        bool isPhone(string inputPhone)
        {
            string strRegex = @"0\d{9,10}";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputPhone))
                return (true);
            else
                MessageBox.Show("Số điện thoại phải là số (có 10 hoặc 11 số) " + "\n" + " bắt đầu bằng số 0  !!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return (false);
        }
        bool isMST(string inputContaxPerfix)
        {
            string strRegex = @"^d{9,10}";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputContaxPerfix))
                return (true);
            else
                MessageBox.Show("Mã số thuế phải là số (có 10, 13 hoặc 14 số)!!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return (false);
        }
        bool isValidEmail(string inputEmail)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputEmail))
                return (true);
            else
                MessageBox.Show("Sai định dạng Email", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return (false);
        }
        #region Utils

        #endregion
        #region CreaterColumsStyle
        void CreaterColumsStyle(Infragistics.Win.UltraWinGrid.UltraGrid uGrid)
        {
            #region Grid động
            Infragistics.Win.UltraWinGrid.UltraGridBand band = uGrid.DisplayLayout.Bands[0];

            foreach (var item in band.Columns)
            {
                if (item.Key.Equals("StockCategoryID"))
                {
                    //List<EMPublishPeriodDetail> listPpd = _IEMPublishPeriodDetailService.Query.Where(a => a.EMPublishPeriodID == IdPublishPeriod).ToList();
                    List<EMPublishPeriodDetail> listPpd = _IEMPublishPeriodDetailService.GetListEMPublishPeriodDetail(IdPublishPeriod);

                    List<StockCategory> listAll = _IStockCategoryService.GetAll();
                    List<StockCategory> listSt = (
                                                    from St in listAll
                                                    where (from abc in listPpd
                                                           select abc.StockCategoryID
                                                            ).Contains(St.ID)
                                                    select St
                                                  ).ToList();
                    item.Editor = this.CreateCombobox(listSt, ConstDatabase.StockCategory_TableName, "ID", "StockCategoryName");

                }

                else

                    if (item.Key.Equals("UnitPrice"))
                    {
                        item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                        item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";
                        item.CellAppearance.TextHAlign = HAlign.Right;
                    }
                    else if (item.Key.Equals("QuantityBuyable"))
                    {
                        item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                        item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";
                        item.CellAppearance.TextHAlign = HAlign.Right;

                    }
                    else if (item.Key.Equals("QuantityRegistered"))
                    {
                        item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;

                        item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";
                        item.CellAppearance.TextHAlign = HAlign.Right;
                    }
                    else if (item.Key.Equals("QuantityApproved"))
                    {
                        item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;

                        item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";
                        item.CellAppearance.TextHAlign = HAlign.Right;
                    }
                    else if (item.Key.Equals("QuantityBought"))
                    {
                        item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;

                        item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";
                        item.CellAppearance.TextHAlign = HAlign.Right;
                    }

                    else if (item.Key.Equals("AmountBought"))
                    {
                        item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;

                        item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";
                        item.CellAppearance.TextHAlign = HAlign.Right;
                    }



            }
            #endregion
        }
        #endregion
        #region sự kiện khi thay đổi dữ liệu trên Grid con
        private void uGrid_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {

            if (e.Cell.Column.Key.Equals("StockCategoryID"))
            {
                Guid StId = new Guid(e.Cell.Row.Cells["StockCategoryID"].Value.ToString());
                //EMPublishPeriodDetail select = _IEMPublishPeriodDetailService.Query.Where(a => a.EMPublishPeriodID == IdPublishPeriod && a.StockCategoryID == StId).Single();
                EMPublishPeriodDetail select = _IEMPublishPeriodDetailService.GetEMPublishPeriodDetailIDStockCategoryID(IdPublishPeriod, StId);
                e.Cell.Row.Cells["UnitPrice"].Value = select.UnitPrice;
                EMRegistrationDetail temp = e.Cell.Row.ListObject as EMRegistrationDetail;

            }

            else if (e.Cell.Column.Key.Equals("QuantityBuyable"))
            {


                e.Cell.Row.Cells["QuantityRegistered"].Value = double.Parse(e.Cell.Row.Cells["QuantityBuyable"].Value.ToString());
                e.Cell.Row.Cells["QuantityApproved"].Value = double.Parse(e.Cell.Row.Cells["QuantityBuyable"].Value.ToString());
                e.Cell.Row.Cells["QuantityBought"].Value = double.Parse(e.Cell.Row.Cells["QuantityBuyable"].Value.ToString());
                EMRegistrationDetail temp = e.Cell.Row.ListObject as EMRegistrationDetail;

            }



        }
        #endregion
        #region ConfigGrid
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.EMRegistrationDetail_TableName, new List<TemplateColumn>(), 0);
            Infragistics.Win.UltraWinGrid.UltraGridBand band = utralGrid.DisplayLayout.Bands[0];

        }
        #endregion

        #region sự kiện khác
        private void ultraOptionSet1_ValueChanged_1(object sender, EventArgs e)
        {
            if (ultraOptionSet1.CheckedIndex == 0)
            {
                ultraGroupBox1.Text = "Thông tin cá nhân";
                this.Size = new Size(650, 540);
                panel1.Size = new Size(620, 690);
                ultraGroupBox1.Size = new Size(585, 248);
                ultraGroupBox3.Size = new Size(585, 170);
                ultraGroupBox3.Controls.Add(panel17);
                ultraGroupBox3.Location = new Point(14, 260);
                chkIsActive.Location = new Point(14, 440);
                btnSave.Location = new Point(440, 440);
                btnClose.Location = new Point(524, 440);
                ultraGroupBox2.Hide();
                panel2.TabIndex = 660;
                panel3.TabIndex = 661;
                panel4.TabIndex = 662;
                panel11.TabIndex = 663;
                panel6.TabIndex = 664;
                panel5.TabIndex = 665;
                panel12.TabIndex = 666;
                panel16.TabIndex = 667;
                panel10.TabIndex = 668;
                panel15.TabIndex = 669;
                panel8.TabIndex = 700;
                panel13.TabIndex = 701;
                panel7.TabIndex = 702;
                panel9.TabIndex = 703;
                panel14.TabIndex = 705;
                chkIsActive.TabIndex = 706;
                btnSave.TabIndex = 707;
                btnClose.TabIndex = 708;
                panel16.Location = new Point(400, 112);
                panel15.Location = new Point(214, 140);
                panel14.Location = new Point(216, 206);
                panel13.Location = new Point(214, 173);
                panel12.Location = new Point(212, 112);
                panel11.Location = new Point(216, 50);
                panel10.Location = new Point(14, 140);
                panel9.Location = new Point(14, 206);
                panel8.Location = new Point(11, 171);
                panel7.Location = new Point(398, 173);
                panel6.Location = new Point(14, 80);
                panel5.Location = new Point(12, 110);
                panel4.Location = new Point(14, 50);
                ultraGroupBox1.Controls.Add(panel4);
                panel3.Location = new Point(216, 19);
                panel2.Location = new Point(14, 19);

                panel18.Hide();
                panel19.Hide();
                panel20.Hide();
                panel21.Hide();
                panel22.Hide();
                panel23.Hide();
                panel24.Hide();
                panel25.Hide();
                panel26.Hide();
                panel27.Hide();
                panel14.Size = new Size(367, 34);
                txtEmail.Location = new Point(69, 3);
                txtEmail.Size = new Size(281, 21);
                ultraGroupBox1.Controls.Add(panel16);
                ultraGroupBox1.Controls.Add(panel14);
                ultraGroupBox1.Controls.Add(panel7);
                ultraGroupBox1.Controls.Add(panel13);
                ultraGroupBox1.Controls.Add(panel8);
                ultraGroupBox1.Controls.Add(panel5);
                ultraGroupBox1.Controls.Add(panel12);
            }
            if (ultraOptionSet1.CheckedIndex == 1)
            {
                ultraGroupBox1.Text = "Thông tin chung";
                this.Size = new Size(634, 740);
                panel1.Size = new Size(634, 760);
                ultraGroupBox3.Size = new Size(585, 170);
                ultraGroupBox3.Controls.Add(panel17);
                ultraGroupBox3.Location = new Point(14, 475);
                chkIsActive.Location = new Point(14, 650);
                btnSave.Location = new Point(440, 650);
                btnClose.Location = new Point(524, 650);
                ultraGroupBox1.Size = new Size(585, 241);
                ultraGroupBox2.Size = new Size(585, 226);
                ultraGroupBox2.Show();
                panel16.Location = new Point(405, 118);
                ultraGroupBox2.Controls.Add(panel16);
                panel15.Location = new Point(219, 142);
                panel14.Location = new Point(11, 185);
                ultraGroupBox2.Controls.Add(panel14);
                panel13.Location = new Point(220, 148);
                ultraGroupBox2.Controls.Add(panel13);
                panel2.TabIndex = 1;
                panel3.TabIndex = 2;
                panel9.TabIndex = 3;
                panel11.TabIndex = 4;
                panel6.TabIndex = 14;
                //panel8.TabIndex = 22;
                panel26.TabIndex = 8;
                panel27.TabIndex = 9;
                panel10.TabIndex = 10;
                panel15.TabIndex = 11;
                panel18.TabIndex = 7;
                panel19.TabIndex = 12;
                panel21.TabIndex = 13;
                panel20.TabIndex = 5;
                panel22.TabIndex = 6;
                panel4.TabIndex = 15;
                panel23.TabIndex = 16;
                panel24.TabIndex = 17;
                panel25.TabIndex = 18;
                panel5.TabIndex = 19;
                panel12.TabIndex = 20;
                panel16.TabIndex = 21;
                panel8.TabIndex = 22;
                panel13.TabIndex = 23;
                panel7.TabIndex = 24;
                panel14.TabIndex = 25;
                chkIsActive.TabIndex = 26;
                btnSave.TabIndex = 27;
                btnClose.TabIndex = 28;
                panel12.Location = new Point(219, 118);
                ultraGroupBox2.Controls.Add(panel12);
                panel11.Location = new Point(216, 50);
                panel18.Location = new Point(11, 112);
                panel19.Location = new Point(12, 171);

                panel10.Location = new Point(12, 142);
                panel9.Location = new Point(10, 53);
                panel8.Location = new Point(11, 148);
                ultraGroupBox2.Controls.Add(panel8);
                panel7.Location = new Point(404, 151);
                ultraGroupBox2.Controls.Add(panel7);
                panel6.Location = new Point(14, 202);
                panel5.Show();
                panel5.Location = new Point(12, 118);
                ultraGroupBox2.Controls.Add(panel5);
                panel4.Show();
                panel4.Location = new Point(13, 19);

                ultraGroupBox2.Controls.Add(panel4);
                panel3.Location = new Point(216, 19);
                panel2.Location = new Point(10, 20);
                panel20.Location = new Point(11, 80);
                panel22.Location = new Point(218, 80);

                panel18.Show();
                panel19.Show();
                panel20.Show();
                panel21.Show();
                panel22.Show();
                panel23.Show();
                panel24.Show();
                panel25.Show();
                panel26.Show();
                panel27.Show();
                panel14.Size = new Size(563, 34);

                txtEmail.Location = new Point(95, 3);
                txtEmail.Size = new Size(465, 21);
                txtContaxtName.Text = txtRegistrationName1.Text;
            }
        }

        private void cbbInvestorCategoryID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            new FInvestorGroupDetail().ShowDialog(this);
            loadcbb();
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }

        #endregion

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            FListInvestor tmpForm = new FListInvestor();
            tmpForm.ShowDialog(this);
            Investor selected = tmpForm.InvestorSelect;
            FillInvestorInfo(selected);
        }

        private void ultraButton2_Click(object sender, EventArgs e)
        {
            //new FShareHolder().ShowDialog(this);
            FShareHolder tmpForm = new FShareHolder();
            tmpForm.ShowDialog(this);
            EMShareHolder selected = tmpForm.EMShareHolderSelect;
            FillEMShareHolder(selected);
        }

        private void cbbInvestorCategoryID_EditorButtonClick_1(object sender, EditorButtonEventArgs e)
        {
            new FRegistrationGroupDetail().ShowDialog(this);
            loadcbb();
        }
    }
}
