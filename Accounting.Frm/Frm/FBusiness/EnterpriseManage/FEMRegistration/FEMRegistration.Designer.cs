﻿namespace Accounting
{
    partial class FEMRegistration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FEMRegistration));
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridListRegistration = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bttGhiso = new Infragistics.Win.Misc.UltraButton();
            this.bttDeleteRegistration = new Infragistics.Win.Misc.UltraButton();
            this.bttEditRegistration = new Infragistics.Win.Misc.UltraButton();
            this.bttRegistration = new Infragistics.Win.Misc.UltraButton();
            this.tab1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridListPeriod = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.TabInformation = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.btnEdit = new Infragistics.Win.Misc.UltraButton();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cms4Grid1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tmsAdd_Registration = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEdit_Registration = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete_Registration = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsBoghi = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridListRegistration)).BeginInit();
            this.panel2.SuspendLayout();
            this.tab1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridListPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.cms4Grid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabInformation)).BeginInit();
            this.TabInformation.SuspendLayout();
            this.panel1.SuspendLayout();
            this.cms4Grid1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGridListRegistration);
            this.ultraTabPageControl1.Controls.Add(this.panel2);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1048, 243);
            // 
            // uGridListRegistration
            // 
            this.uGridListRegistration.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGridListRegistration.Location = new System.Drawing.Point(0, 15);
            this.uGridListRegistration.Name = "uGridListRegistration";
            this.uGridListRegistration.Size = new System.Drawing.Size(1048, 200);
            this.uGridListRegistration.TabIndex = 1;
            this.uGridListRegistration.Text = "uGridListRegistration";
            this.uGridListRegistration.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.uGridListRegistration_InitializeLayout);
            this.uGridListRegistration.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridListRegistration_DoubleClickRow);
            this.uGridListRegistration.Click += new System.EventHandler(this.uGridListRegistration_Click);
            this.uGridListRegistration.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uGridListRegistration_MouseDown);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.bttGhiso);
            this.panel2.Controls.Add(this.bttDeleteRegistration);
            this.panel2.Controls.Add(this.bttEditRegistration);
            this.panel2.Controls.Add(this.bttRegistration);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 215);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1048, 28);
            this.panel2.TabIndex = 0;
            // 
            // bttGhiso
            // 
            this.bttGhiso.Dock = System.Windows.Forms.DockStyle.Right;
            this.bttGhiso.Location = new System.Drawing.Point(536, 0);
            this.bttGhiso.Name = "bttGhiso";
            this.bttGhiso.Size = new System.Drawing.Size(128, 28);
            this.bttGhiso.TabIndex = 3;
            this.bttGhiso.Text = "Ghi sổ";
            this.bttGhiso.Click += new System.EventHandler(this.bttGhiso_Click);
            // 
            // bttDeleteRegistration
            // 
            this.bttDeleteRegistration.Dock = System.Windows.Forms.DockStyle.Right;
            this.bttDeleteRegistration.Location = new System.Drawing.Point(664, 0);
            this.bttDeleteRegistration.Name = "bttDeleteRegistration";
            this.bttDeleteRegistration.Size = new System.Drawing.Size(128, 28);
            this.bttDeleteRegistration.TabIndex = 2;
            this.bttDeleteRegistration.Text = "Xoá đăng ký";
            this.bttDeleteRegistration.Click += new System.EventHandler(this.bttDeleteRegistration_Click);
            // 
            // bttEditRegistration
            // 
            this.bttEditRegistration.Dock = System.Windows.Forms.DockStyle.Right;
            this.bttEditRegistration.Location = new System.Drawing.Point(792, 0);
            this.bttEditRegistration.Name = "bttEditRegistration";
            this.bttEditRegistration.Size = new System.Drawing.Size(128, 28);
            this.bttEditRegistration.TabIndex = 1;
            this.bttEditRegistration.Text = "Sửa đăng ký";
            this.bttEditRegistration.Click += new System.EventHandler(this.bttEditRegistration_Click);
            // 
            // bttRegistration
            // 
            this.bttRegistration.Dock = System.Windows.Forms.DockStyle.Right;
            this.bttRegistration.Location = new System.Drawing.Point(920, 0);
            this.bttRegistration.Name = "bttRegistration";
            this.bttRegistration.Size = new System.Drawing.Size(128, 28);
            this.bttRegistration.TabIndex = 0;
            this.bttRegistration.Text = "Đăng ký mới";
            this.bttRegistration.Click += new System.EventHandler(this.bttRegistration_Click);
            // 
            // tab1
            // 
            this.tab1.Controls.Add(this.uGridListPeriod);
            this.tab1.Location = new System.Drawing.Point(1, 23);
            this.tab1.Name = "tab1";
            this.tab1.Size = new System.Drawing.Size(1048, 243);
            // 
            // uGridListPeriod
            // 
            this.uGridListPeriod.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridListPeriod.Location = new System.Drawing.Point(0, 0);
            this.uGridListPeriod.Name = "uGridListPeriod";
            this.uGridListPeriod.Size = new System.Drawing.Size(1048, 243);
            this.uGridListPeriod.TabIndex = 1;
            this.uGridListPeriod.Text = "uGridListPeriod";
            // 
            // uGrid
            // 
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 47);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(1052, 258);
            this.uGrid.TabIndex = 11;
            this.uGrid.Text = "uGrid";
            this.uGrid.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.uGrid_AfterSelectChange);
            this.uGrid.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid_DoubleClickRow);
            this.uGrid.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uGrid_MouseDown);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1048, 243);
            // 
            // cms4Grid
            // 
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tsmAdd,
            this.tsmEdit,
            this.tsmDelete,
            this.tmsReLoad});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(150, 98);
            this.cms4Grid.Opening += new System.ComponentModel.CancelEventHandler(this.cms4Grid_Opening);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(146, 6);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.Size = new System.Drawing.Size(149, 22);
            this.tsmAdd.Text = "Thêm";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click_1);
            // 
            // tsmEdit
            // 
            this.tsmEdit.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.tsmEdit.Name = "tsmEdit";
            this.tsmEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.tsmEdit.Size = new System.Drawing.Size(149, 22);
            this.tsmEdit.Text = "Sửa";
            this.tsmEdit.Click += new System.EventHandler(this.tsmEdit_Click_1);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.Size = new System.Drawing.Size(149, 22);
            this.tsmDelete.Text = "Xóa";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click_1);
            // 
            // tmsReLoad
            // 
            this.tmsReLoad.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tmsReLoad.Name = "tmsReLoad";
            this.tmsReLoad.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.tmsReLoad.Size = new System.Drawing.Size(149, 22);
            this.tmsReLoad.Text = "Tải Lại";
            this.tmsReLoad.Click += new System.EventHandler(this.tmsReLoad_Click_1);
            // 
            // TabInformation
            // 
            this.TabInformation.Controls.Add(this.ultraTabSharedControlsPage1);
            this.TabInformation.Controls.Add(this.ultraTabPageControl1);
            this.TabInformation.Controls.Add(this.tab1);
            this.TabInformation.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TabInformation.Location = new System.Drawing.Point(0, 305);
            this.TabInformation.Name = "TabInformation";
            this.TabInformation.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.TabInformation.Size = new System.Drawing.Size(1052, 269);
            this.TabInformation.TabIndex = 12;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "1 Danh sách đăng ký";
            ultraTab2.TabPage = this.tab1;
            ultraTab2.Text = "2  Cổ phần phát hành.";
            this.TabInformation.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // btnEdit
            // 
            appearance1.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.btnEdit.Appearance = appearance1;
            this.btnEdit.Location = new System.Drawing.Point(100, 11);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 30);
            this.btnEdit.TabIndex = 12;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnDelete
            // 
            appearance2.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.btnDelete.Appearance = appearance2;
            this.btnDelete.Location = new System.Drawing.Point(194, 11);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 13;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            appearance3.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.btnAdd.Appearance = appearance3;
            this.btnAdd.Location = new System.Drawing.Point(8, 11);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 30);
            this.btnAdd.TabIndex = 14;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1052, 47);
            this.panel1.TabIndex = 10;
            // 
            // cms4Grid1
            // 
            this.cms4Grid1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.tmsAdd_Registration,
            this.tsmEdit_Registration,
            this.tsmDelete_Registration,
            this.tmsBoghi});
            this.cms4Grid1.Name = "cms4Grid";
            this.cms4Grid1.Size = new System.Drawing.Size(189, 98);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(185, 6);
            // 
            // tmsAdd_Registration
            // 
            this.tmsAdd_Registration.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tmsAdd_Registration.Name = "tmsAdd_Registration";
            this.tmsAdd_Registration.Size = new System.Drawing.Size(188, 22);
            this.tmsAdd_Registration.Text = "Đăng ký mới...";
            this.tmsAdd_Registration.Click += new System.EventHandler(this.tmsAdd_Registration_Click);
            // 
            // tsmEdit_Registration
            // 
            this.tsmEdit_Registration.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.tsmEdit_Registration.Name = "tsmEdit_Registration";
            this.tsmEdit_Registration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.tsmEdit_Registration.Size = new System.Drawing.Size(188, 22);
            this.tsmEdit_Registration.Text = "Sửa Đăng ký...";
            this.tsmEdit_Registration.Click += new System.EventHandler(this.tsmEdit_Registration_Click);
            // 
            // tsmDelete_Registration
            // 
            this.tsmDelete_Registration.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.tsmDelete_Registration.Name = "tsmDelete_Registration";
            this.tsmDelete_Registration.Size = new System.Drawing.Size(188, 22);
            this.tsmDelete_Registration.Text = "Xóa Đăng ký...";
            this.tsmDelete_Registration.Click += new System.EventHandler(this.tsmDelete_Registration_Click);
            // 
            // tmsBoghi
            // 
            this.tmsBoghi.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tmsBoghi.Name = "tmsBoghi";
            this.tmsBoghi.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.tmsBoghi.Size = new System.Drawing.Size(188, 22);
            this.tmsBoghi.Text = "Bỏ ghi.";
            this.tmsBoghi.Click += new System.EventHandler(this.tmsBoghi_Click);
            // 
            // FEMRegistration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1052, 574);
            this.Controls.Add(this.uGrid);
            this.Controls.Add(this.TabInformation);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FEMRegistration";
            this.Text = "Đợt phát hành.";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FEMRegistration_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FEMRegistration_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridListRegistration)).EndInit();
            this.panel2.ResumeLayout(false);
            this.tab1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridListPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.cms4Grid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TabInformation)).EndInit();
            this.TabInformation.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.cms4Grid1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmEdit;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private System.Windows.Forms.ToolStripMenuItem tmsReLoad;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl TabInformation;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tab1;
        private Infragistics.Win.Misc.UltraButton btnEdit;
        private Infragistics.Win.Misc.UltraButton btnDelete;
        private Infragistics.Win.Misc.UltraButton btnAdd;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridListPeriod;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridListRegistration;
        private System.Windows.Forms.Panel panel2;
        private Infragistics.Win.Misc.UltraButton bttGhiso;
        private Infragistics.Win.Misc.UltraButton bttDeleteRegistration;
        private Infragistics.Win.Misc.UltraButton bttEditRegistration;
        private Infragistics.Win.Misc.UltraButton bttRegistration;
        private System.Windows.Forms.ContextMenuStrip cms4Grid1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tmsAdd_Registration;
        private System.Windows.Forms.ToolStripMenuItem tsmEdit_Registration;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete_Registration;
        private System.Windows.Forms.ToolStripMenuItem tmsBoghi;
    }
}