﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FEMContractDetailMG : FEMContractDetailBase
    {
        #region khai bao
        public static bool isClose = true;
        #endregion
        #region khởi tạo
        public FEMContractDetailMG(EMContractDetailMG temp, List<EMContractDetailMG> lstEMContractDetailMG, int statusForm)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _statusForm = statusForm;
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.TypeID = 861;
            }
            else
            {
                _select = temp;
            }
            _listSelects.AddRange(lstEMContractDetailMG);
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, _listSelects, statusForm);
            #endregion

        }
        #endregion
        #region
        #endregion
        #region sử lý các viewgrid trên form
        #endregion
        #region event

        #endregion
        #region Hàm override
        //public override void InitializeGUI(EMContractDetailMG input)
        //{
        //    Template mauGiaoDien = Utils.GetMauGiaoDien(input.TypeID, input.TemplateID);
        //    _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add && mauGiaoDien != null ? mauGiaoDien.ID : input.TemplateID;
        //    input.TemplateID = mauGiaoDien.ID;

        //    BindingList<TIAdjustmentDetail> dsIncrementDetails = new BindingList<TIAdjustmentDetail>();
        //    if (_statusForm != ConstFrm.optStatusForm.Add)
        //        dsIncrementDetails = new BindingList<TIAdjustmentDetail>(input.TIAdjustmentDetails);
        //    #region cau hinh ban dau cho form

        //    _listObjectInput = new BindingList<System.Collections.IList> { dsIncrementDetails };

        //    this.ConfigGridByTemplete_General<TIAdjustment>(pnlUgrid, mauGiaoDien);
        //    this.ConfigGridByTemplete<TIAdjustment>(input.TypeID, mauGiaoDien, true, _listObjectInput);
        //    this.ConfigTopVouchersNo<TIAdjustment>(pnlDateNo, "No", "PostedDate", "Date")

        //    #endregion
        //}
        #endregion
    }
    public class FEMContractDetailBase : DetailBase<EMContractDetailMG> { }
}
