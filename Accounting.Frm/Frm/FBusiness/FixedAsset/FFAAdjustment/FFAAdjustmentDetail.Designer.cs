﻿namespace Accounting
{
    partial class FFAAdjustmentDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.pnlDateNo = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraPanel13 = new Infragistics.Win.Misc.UltraPanel();
            this.lblDiffAcDepreciationAmount = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel23 = new Infragistics.Win.Misc.UltraPanel();
            this.txtNewAcDepreciationAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel24 = new Infragistics.Win.Misc.UltraPanel();
            this.lblOldAcDepreciationAmount = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel25 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.checkSD = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.checkKH = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraPanel21 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel20 = new Infragistics.Win.Misc.UltraPanel();
            this.lblDifferMonthlyDepreciationAmount = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel19 = new Infragistics.Win.Misc.UltraPanel();
            this.lblDiffUsedTime = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel18 = new Infragistics.Win.Misc.UltraPanel();
            this.lblDiffDepreciationAmount = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel17 = new Infragistics.Win.Misc.UltraPanel();
            this.lblDifferRemainingAmount = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel16 = new Infragistics.Win.Misc.UltraPanel();
            this.lblDifferOrgPrice = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel15 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel22 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraMaskedEdit1 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.cbbNewDepreciationMethod = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraPanel14 = new Infragistics.Win.Misc.UltraPanel();
            this.txtNewMonthlyDepreciationAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel12 = new Infragistics.Win.Misc.UltraPanel();
            this.txtNewUsedTime = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel11 = new Infragistics.Win.Misc.UltraPanel();
            this.txtNewDepreciationAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel10 = new Infragistics.Win.Misc.UltraPanel();
            this.lblNewRemainingAmount = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel9 = new Infragistics.Win.Misc.UltraPanel();
            this.txtNewOrgPrice = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel8 = new Infragistics.Win.Misc.UltraPanel();
            this.lblOldDepreciationMethod = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel7 = new Infragistics.Win.Misc.UltraPanel();
            this.lblOldMonthlyDepreciationAmount = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel5 = new Infragistics.Win.Misc.UltraPanel();
            this.lblOldUsedTime = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel4 = new Infragistics.Win.Misc.UltraPanel();
            this.lblOldDepreciationAmount = new Infragistics.Win.Misc.UltraLabel();
            this.panelKhauHaoCu = new Infragistics.Win.Misc.UltraPanel();
            this.lblOldRemainingAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.panelNguyenGiaCu = new Infragistics.Win.Misc.UltraPanel();
            this.lblOldOrgPrice = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel6 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel41 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel39 = new Infragistics.Win.Misc.UltraPanel();
            this.cbbMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel38 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel37 = new Infragistics.Win.Misc.UltraPanel();
            this.utrallabel = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel36 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtFixedAssetName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbFixedAssetID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.pnlUgrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.pnlDateNo.SuspendLayout();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.ultraPanel13.ClientArea.SuspendLayout();
            this.ultraPanel13.SuspendLayout();
            this.ultraPanel23.ClientArea.SuspendLayout();
            this.ultraPanel23.SuspendLayout();
            this.ultraPanel24.ClientArea.SuspendLayout();
            this.ultraPanel24.SuspendLayout();
            this.ultraPanel25.ClientArea.SuspendLayout();
            this.ultraPanel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkSD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkKH)).BeginInit();
            this.ultraPanel21.SuspendLayout();
            this.ultraPanel20.ClientArea.SuspendLayout();
            this.ultraPanel20.SuspendLayout();
            this.ultraPanel19.ClientArea.SuspendLayout();
            this.ultraPanel19.SuspendLayout();
            this.ultraPanel18.ClientArea.SuspendLayout();
            this.ultraPanel18.SuspendLayout();
            this.ultraPanel17.ClientArea.SuspendLayout();
            this.ultraPanel17.SuspendLayout();
            this.ultraPanel16.ClientArea.SuspendLayout();
            this.ultraPanel16.SuspendLayout();
            this.ultraPanel15.ClientArea.SuspendLayout();
            this.ultraPanel15.SuspendLayout();
            this.ultraPanel22.ClientArea.SuspendLayout();
            this.ultraPanel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNewDepreciationMethod)).BeginInit();
            this.ultraPanel14.ClientArea.SuspendLayout();
            this.ultraPanel14.SuspendLayout();
            this.ultraPanel12.ClientArea.SuspendLayout();
            this.ultraPanel12.SuspendLayout();
            this.ultraPanel11.ClientArea.SuspendLayout();
            this.ultraPanel11.SuspendLayout();
            this.ultraPanel10.ClientArea.SuspendLayout();
            this.ultraPanel10.SuspendLayout();
            this.ultraPanel9.ClientArea.SuspendLayout();
            this.ultraPanel9.SuspendLayout();
            this.ultraPanel8.ClientArea.SuspendLayout();
            this.ultraPanel8.SuspendLayout();
            this.ultraPanel7.ClientArea.SuspendLayout();
            this.ultraPanel7.SuspendLayout();
            this.ultraPanel5.ClientArea.SuspendLayout();
            this.ultraPanel5.SuspendLayout();
            this.ultraPanel4.ClientArea.SuspendLayout();
            this.ultraPanel4.SuspendLayout();
            this.panelKhauHaoCu.ClientArea.SuspendLayout();
            this.panelKhauHaoCu.SuspendLayout();
            this.panelNguyenGiaCu.ClientArea.SuspendLayout();
            this.panelNguyenGiaCu.SuspendLayout();
            this.ultraPanel6.ClientArea.SuspendLayout();
            this.ultraPanel6.SuspendLayout();
            this.ultraPanel41.ClientArea.SuspendLayout();
            this.ultraPanel41.SuspendLayout();
            this.ultraPanel39.ClientArea.SuspendLayout();
            this.ultraPanel39.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMonth)).BeginInit();
            this.ultraPanel38.ClientArea.SuspendLayout();
            this.ultraPanel38.SuspendLayout();
            this.ultraPanel37.ClientArea.SuspendLayout();
            this.ultraPanel37.SuspendLayout();
            this.ultraPanel36.ClientArea.SuspendLayout();
            this.ultraPanel36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedAssetName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbFixedAssetID)).BeginInit();
            this.pnlUgrid.ClientArea.SuspendLayout();
            this.pnlUgrid.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(997, 72);
            this.ultraPanel1.TabIndex = 0;
            // 
            // ultraGroupBox1
            // 
            appearance1.FontData.BoldAsString = "True";
            appearance1.FontData.SizeInPoints = 13F;
            this.ultraGroupBox1.Appearance = appearance1;
            this.ultraGroupBox1.Controls.Add(this.pnlDateNo);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(997, 72);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Điều chỉnh TSCĐ";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // pnlDateNo
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            this.pnlDateNo.Appearance = appearance2;
            this.pnlDateNo.Location = new System.Drawing.Point(6, 20);
            this.pnlDateNo.Name = "pnlDateNo";
            this.pnlDateNo.Size = new System.Drawing.Size(421, 49);
            this.pnlDateNo.TabIndex = 0;
            this.pnlDateNo.TabStop = false;
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraGroupBox2);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 72);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(997, 311);
            this.ultraPanel2.TabIndex = 1;
            // 
            // ultraGroupBox2
            // 
            appearance3.FontData.SizeInPoints = 10F;
            this.ultraGroupBox2.Appearance = appearance3;
            this.ultraGroupBox2.Controls.Add(this.ultraPanel13);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel23);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel24);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel25);
            this.ultraGroupBox2.Controls.Add(this.checkSD);
            this.ultraGroupBox2.Controls.Add(this.checkKH);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel21);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel20);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel19);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel18);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel17);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel16);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel15);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel14);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel12);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel11);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel10);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel9);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel8);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel7);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel5);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel4);
            this.ultraGroupBox2.Controls.Add(this.panelKhauHaoCu);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox2.Controls.Add(this.panelNguyenGiaCu);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel6);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel41);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel39);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel38);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel37);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel36);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox2.Controls.Add(this.txtReason);
            this.ultraGroupBox2.Controls.Add(this.txtFixedAssetName);
            this.ultraGroupBox2.Controls.Add(this.cbbFixedAssetID);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(997, 311);
            this.ultraGroupBox2.TabIndex = 0;
            this.ultraGroupBox2.Text = "Thông tin chung";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraPanel13
            // 
            this.ultraPanel13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel13.Appearance = appearance4;
            this.ultraPanel13.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel13.ClientArea
            // 
            this.ultraPanel13.ClientArea.Controls.Add(this.lblDiffAcDepreciationAmount);
            this.ultraPanel13.Location = new System.Drawing.Point(859, 275);
            this.ultraPanel13.Name = "ultraPanel13";
            this.ultraPanel13.Size = new System.Drawing.Size(123, 33);
            this.ultraPanel13.TabIndex = 87;
            // 
            // lblDiffAcDepreciationAmount
            // 
            this.lblDiffAcDepreciationAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.TextHAlignAsString = "Right";
            appearance5.TextVAlignAsString = "Middle";
            this.lblDiffAcDepreciationAmount.Appearance = appearance5;
            this.lblDiffAcDepreciationAmount.Location = new System.Drawing.Point(5, 5);
            this.lblDiffAcDepreciationAmount.Name = "lblDiffAcDepreciationAmount";
            this.lblDiffAcDepreciationAmount.Size = new System.Drawing.Size(110, 22);
            this.lblDiffAcDepreciationAmount.TabIndex = 41;
            // 
            // ultraPanel23
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel23.Appearance = appearance6;
            this.ultraPanel23.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel23.ClientArea
            // 
            this.ultraPanel23.ClientArea.Controls.Add(this.txtNewAcDepreciationAmount);
            this.ultraPanel23.Location = new System.Drawing.Point(566, 275);
            this.ultraPanel23.Name = "ultraPanel23";
            this.ultraPanel23.Size = new System.Drawing.Size(296, 33);
            this.ultraPanel23.TabIndex = 86;
            // 
            // txtNewAcDepreciationAmount
            // 
            appearance7.TextHAlignAsString = "Right";
            this.txtNewAcDepreciationAmount.Appearance = appearance7;
            this.txtNewAcDepreciationAmount.AutoSize = false;
            this.txtNewAcDepreciationAmount.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtNewAcDepreciationAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtNewAcDepreciationAmount.InputMask = " -n,nnn,nnn,nnn,nnn,nnn.nn";
            this.txtNewAcDepreciationAmount.Location = new System.Drawing.Point(6, 6);
            this.txtNewAcDepreciationAmount.Name = "txtNewAcDepreciationAmount";
            this.txtNewAcDepreciationAmount.Size = new System.Drawing.Size(279, 22);
            this.txtNewAcDepreciationAmount.TabIndex = 10;
            this.txtNewAcDepreciationAmount.TabStop = false;
            this.txtNewAcDepreciationAmount.Text = " ";
            this.txtNewAcDepreciationAmount.Validated += new System.EventHandler(this.txtNewAcDepreciationAmount_Validated);
            // 
            // ultraPanel24
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel24.Appearance = appearance8;
            this.ultraPanel24.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel24.ClientArea
            // 
            this.ultraPanel24.ClientArea.Controls.Add(this.lblOldAcDepreciationAmount);
            this.ultraPanel24.Location = new System.Drawing.Point(242, 275);
            this.ultraPanel24.Name = "ultraPanel24";
            this.ultraPanel24.Size = new System.Drawing.Size(326, 33);
            this.ultraPanel24.TabIndex = 85;
            // 
            // lblOldAcDepreciationAmount
            // 
            this.lblOldAcDepreciationAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.TextHAlignAsString = "Right";
            appearance9.TextVAlignAsString = "Middle";
            this.lblOldAcDepreciationAmount.Appearance = appearance9;
            this.lblOldAcDepreciationAmount.Location = new System.Drawing.Point(7, 6);
            this.lblOldAcDepreciationAmount.Name = "lblOldAcDepreciationAmount";
            this.lblOldAcDepreciationAmount.Size = new System.Drawing.Size(310, 20);
            this.lblOldAcDepreciationAmount.TabIndex = 38;
            // 
            // ultraPanel25
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel25.Appearance = appearance10;
            this.ultraPanel25.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel25.ClientArea
            // 
            this.ultraPanel25.ClientArea.Controls.Add(this.ultraLabel15);
            this.ultraPanel25.Location = new System.Drawing.Point(12, 275);
            this.ultraPanel25.Name = "ultraPanel25";
            this.ultraPanel25.Size = new System.Drawing.Size(232, 33);
            this.ultraPanel25.TabIndex = 84;
            // 
            // ultraLabel15
            // 
            this.ultraLabel15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance11.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance11;
            this.ultraLabel15.Location = new System.Drawing.Point(2, 6);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(221, 21);
            this.ultraLabel15.TabIndex = 0;
            this.ultraLabel15.Text = "Hao mòn lũy kế";
            // 
            // checkSD
            // 
            this.checkSD.BackColor = System.Drawing.Color.Transparent;
            this.checkSD.BackColorInternal = System.Drawing.Color.Transparent;
            this.checkSD.Location = new System.Drawing.Point(246, 69);
            this.checkSD.Name = "checkSD";
            this.checkSD.Size = new System.Drawing.Size(213, 20);
            this.checkSD.TabIndex = 83;
            this.checkSD.Text = "Điều chỉnh thời gian sử dụng còn lại";
            this.checkSD.AfterCheckStateChanged += new Infragistics.Win.CheckEditor.AfterCheckStateChangedHandler(this.checkSD_AfterCheckStateChanged);
            this.checkSD.CheckedChanged += new System.EventHandler(this.checkSD_CheckedChanged);
            // 
            // checkKH
            // 
            this.checkKH.BackColor = System.Drawing.Color.Transparent;
            this.checkKH.BackColorInternal = System.Drawing.Color.Transparent;
            this.checkKH.Checked = true;
            this.checkKH.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkKH.Location = new System.Drawing.Point(12, 69);
            this.checkKH.Name = "checkKH";
            this.checkKH.Size = new System.Drawing.Size(191, 20);
            this.checkKH.TabIndex = 82;
            this.checkKH.Text = "Điều chỉnh giá trị tính khấu hao";
            this.checkKH.AfterCheckStateChanged += new Infragistics.Win.CheckEditor.AfterCheckStateChangedHandler(this.checkKH_AfterCheckStateChanged);
            this.checkKH.CheckedChanged += new System.EventHandler(this.checkKH_CheckedChanged);
            // 
            // ultraPanel21
            // 
            this.ultraPanel21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel21.Appearance = appearance12;
            this.ultraPanel21.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraPanel21.Location = new System.Drawing.Point(859, 275);
            this.ultraPanel21.Name = "ultraPanel21";
            this.ultraPanel21.Size = new System.Drawing.Size(123, 36);
            this.ultraPanel21.TabIndex = 81;
            this.ultraPanel21.Visible = false;
            // 
            // ultraPanel20
            // 
            this.ultraPanel20.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel20.Appearance = appearance13;
            this.ultraPanel20.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel20.ClientArea
            // 
            this.ultraPanel20.ClientArea.Controls.Add(this.lblDifferMonthlyDepreciationAmount);
            this.ultraPanel20.Location = new System.Drawing.Point(859, 243);
            this.ultraPanel20.Name = "ultraPanel20";
            this.ultraPanel20.Size = new System.Drawing.Size(123, 33);
            this.ultraPanel20.TabIndex = 80;
            // 
            // lblDifferMonthlyDepreciationAmount
            // 
            this.lblDifferMonthlyDepreciationAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance14.TextHAlignAsString = "Right";
            appearance14.TextVAlignAsString = "Middle";
            this.lblDifferMonthlyDepreciationAmount.Appearance = appearance14;
            this.lblDifferMonthlyDepreciationAmount.Location = new System.Drawing.Point(5, 5);
            this.lblDifferMonthlyDepreciationAmount.Name = "lblDifferMonthlyDepreciationAmount";
            this.lblDifferMonthlyDepreciationAmount.Size = new System.Drawing.Size(110, 22);
            this.lblDifferMonthlyDepreciationAmount.TabIndex = 41;
            // 
            // ultraPanel19
            // 
            this.ultraPanel19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel19.Appearance = appearance15;
            this.ultraPanel19.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel19.ClientArea
            // 
            this.ultraPanel19.ClientArea.Controls.Add(this.lblDiffUsedTime);
            this.ultraPanel19.Location = new System.Drawing.Point(859, 211);
            this.ultraPanel19.Name = "ultraPanel19";
            this.ultraPanel19.Size = new System.Drawing.Size(123, 33);
            this.ultraPanel19.TabIndex = 79;
            // 
            // lblDiffUsedTime
            // 
            this.lblDiffUsedTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance16.TextHAlignAsString = "Right";
            appearance16.TextVAlignAsString = "Middle";
            this.lblDiffUsedTime.Appearance = appearance16;
            this.lblDiffUsedTime.Location = new System.Drawing.Point(5, 5);
            this.lblDiffUsedTime.Name = "lblDiffUsedTime";
            this.lblDiffUsedTime.Size = new System.Drawing.Size(110, 22);
            this.lblDiffUsedTime.TabIndex = 41;
            // 
            // ultraPanel18
            // 
            this.ultraPanel18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel18.Appearance = appearance17;
            this.ultraPanel18.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel18.ClientArea
            // 
            this.ultraPanel18.ClientArea.Controls.Add(this.lblDiffDepreciationAmount);
            this.ultraPanel18.Location = new System.Drawing.Point(859, 179);
            this.ultraPanel18.Name = "ultraPanel18";
            this.ultraPanel18.Size = new System.Drawing.Size(123, 33);
            this.ultraPanel18.TabIndex = 78;
            // 
            // lblDiffDepreciationAmount
            // 
            this.lblDiffDepreciationAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance18.TextHAlignAsString = "Right";
            appearance18.TextVAlignAsString = "Middle";
            this.lblDiffDepreciationAmount.Appearance = appearance18;
            this.lblDiffDepreciationAmount.Location = new System.Drawing.Point(5, 5);
            this.lblDiffDepreciationAmount.Name = "lblDiffDepreciationAmount";
            this.lblDiffDepreciationAmount.Size = new System.Drawing.Size(110, 22);
            this.lblDiffDepreciationAmount.TabIndex = 41;
            // 
            // ultraPanel17
            // 
            this.ultraPanel17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel17.Appearance = appearance19;
            this.ultraPanel17.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel17.ClientArea
            // 
            this.ultraPanel17.ClientArea.Controls.Add(this.lblDifferRemainingAmount);
            this.ultraPanel17.Location = new System.Drawing.Point(859, 147);
            this.ultraPanel17.Name = "ultraPanel17";
            this.ultraPanel17.Size = new System.Drawing.Size(123, 33);
            this.ultraPanel17.TabIndex = 77;
            // 
            // lblDifferRemainingAmount
            // 
            this.lblDifferRemainingAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance20.TextHAlignAsString = "Right";
            appearance20.TextVAlignAsString = "Middle";
            this.lblDifferRemainingAmount.Appearance = appearance20;
            this.lblDifferRemainingAmount.Location = new System.Drawing.Point(5, 5);
            this.lblDifferRemainingAmount.Name = "lblDifferRemainingAmount";
            this.lblDifferRemainingAmount.Size = new System.Drawing.Size(110, 22);
            this.lblDifferRemainingAmount.TabIndex = 40;
            // 
            // ultraPanel16
            // 
            this.ultraPanel16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance21.BackColor = System.Drawing.Color.Transparent;
            appearance21.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel16.Appearance = appearance21;
            this.ultraPanel16.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel16.ClientArea
            // 
            this.ultraPanel16.ClientArea.Controls.Add(this.lblDifferOrgPrice);
            this.ultraPanel16.Location = new System.Drawing.Point(859, 115);
            this.ultraPanel16.Name = "ultraPanel16";
            this.ultraPanel16.Size = new System.Drawing.Size(123, 33);
            this.ultraPanel16.TabIndex = 76;
            // 
            // lblDifferOrgPrice
            // 
            this.lblDifferOrgPrice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance22.TextHAlignAsString = "Right";
            appearance22.TextVAlignAsString = "Middle";
            this.lblDifferOrgPrice.Appearance = appearance22;
            this.lblDifferOrgPrice.Location = new System.Drawing.Point(6, 4);
            this.lblDifferOrgPrice.Name = "lblDifferOrgPrice";
            this.lblDifferOrgPrice.Size = new System.Drawing.Size(110, 22);
            this.lblDifferOrgPrice.TabIndex = 39;
            // 
            // ultraLabel8
            // 
            this.ultraLabel8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(104)))), ((int)(((byte)(189)))));
            appearance23.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance23.BorderColor = System.Drawing.Color.Gray;
            appearance23.FontData.BoldAsString = "True";
            appearance23.ForeColor = System.Drawing.Color.White;
            appearance23.TextHAlignAsString = "Center";
            appearance23.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance23;
            this.ultraLabel8.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel8.Location = new System.Drawing.Point(859, 93);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(123, 23);
            this.ultraLabel8.TabIndex = 75;
            this.ultraLabel8.Text = "Chênh lệch";
            // 
            // ultraPanel15
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            appearance24.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel15.Appearance = appearance24;
            this.ultraPanel15.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel15.ClientArea
            // 
            this.ultraPanel15.ClientArea.Controls.Add(this.ultraPanel22);
            this.ultraPanel15.ClientArea.Controls.Add(this.cbbNewDepreciationMethod);
            this.ultraPanel15.Location = new System.Drawing.Point(566, 275);
            this.ultraPanel15.Name = "ultraPanel15";
            this.ultraPanel15.Size = new System.Drawing.Size(296, 36);
            this.ultraPanel15.TabIndex = 74;
            this.ultraPanel15.Visible = false;
            // 
            // ultraPanel22
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            appearance25.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel22.Appearance = appearance25;
            this.ultraPanel22.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel22.ClientArea
            // 
            this.ultraPanel22.ClientArea.Controls.Add(this.ultraMaskedEdit1);
            this.ultraPanel22.Location = new System.Drawing.Point(-1, 34);
            this.ultraPanel22.Name = "ultraPanel22";
            this.ultraPanel22.Size = new System.Drawing.Size(296, 34);
            this.ultraPanel22.TabIndex = 72;
            // 
            // ultraMaskedEdit1
            // 
            appearance26.TextHAlignAsString = "Right";
            this.ultraMaskedEdit1.Appearance = appearance26;
            this.ultraMaskedEdit1.AutoSize = false;
            this.ultraMaskedEdit1.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.ultraMaskedEdit1.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.ultraMaskedEdit1.InputMask = " -n,nnn,nnn,nnn,nnn,nnn.nn";
            this.ultraMaskedEdit1.Location = new System.Drawing.Point(6, 6);
            this.ultraMaskedEdit1.Name = "ultraMaskedEdit1";
            this.ultraMaskedEdit1.Size = new System.Drawing.Size(279, 22);
            this.ultraMaskedEdit1.TabIndex = 1;
            this.ultraMaskedEdit1.Text = " ";
            // 
            // cbbNewDepreciationMethod
            // 
            this.cbbNewDepreciationMethod.AutoSize = false;
            this.cbbNewDepreciationMethod.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbNewDepreciationMethod.Enabled = false;
            valueListItem1.DataValue = 0;
            valueListItem1.DisplayText = "Đường thẳng";
            valueListItem2.DataValue = 1;
            valueListItem2.DisplayText = "Khấu hao nhanh";
            valueListItem3.DataValue = 2;
            valueListItem3.DisplayText = "Sản lượng";
            this.cbbNewDepreciationMethod.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3});
            this.cbbNewDepreciationMethod.Location = new System.Drawing.Point(6, 6);
            this.cbbNewDepreciationMethod.Name = "cbbNewDepreciationMethod";
            this.cbbNewDepreciationMethod.Size = new System.Drawing.Size(279, 22);
            this.cbbNewDepreciationMethod.TabIndex = 9;
            // 
            // ultraPanel14
            // 
            appearance27.BackColor = System.Drawing.Color.Transparent;
            appearance27.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel14.Appearance = appearance27;
            this.ultraPanel14.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel14.ClientArea
            // 
            this.ultraPanel14.ClientArea.Controls.Add(this.txtNewMonthlyDepreciationAmount);
            this.ultraPanel14.Location = new System.Drawing.Point(566, 243);
            this.ultraPanel14.Name = "ultraPanel14";
            this.ultraPanel14.Size = new System.Drawing.Size(296, 33);
            this.ultraPanel14.TabIndex = 73;
            // 
            // txtNewMonthlyDepreciationAmount
            // 
            appearance28.TextHAlignAsString = "Right";
            this.txtNewMonthlyDepreciationAmount.Appearance = appearance28;
            this.txtNewMonthlyDepreciationAmount.AutoSize = false;
            this.txtNewMonthlyDepreciationAmount.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtNewMonthlyDepreciationAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtNewMonthlyDepreciationAmount.InputMask = " -n,nnn,nnn,nnn,nnn,nnn";
            this.txtNewMonthlyDepreciationAmount.Location = new System.Drawing.Point(6, 5);
            this.txtNewMonthlyDepreciationAmount.Name = "txtNewMonthlyDepreciationAmount";
            this.txtNewMonthlyDepreciationAmount.Size = new System.Drawing.Size(279, 22);
            this.txtNewMonthlyDepreciationAmount.TabIndex = 8;
            this.txtNewMonthlyDepreciationAmount.Text = " ";
            this.txtNewMonthlyDepreciationAmount.Validated += new System.EventHandler(this.txtNewMonthlyDepreciationAmount_Validated);
            // 
            // ultraPanel12
            // 
            appearance29.BackColor = System.Drawing.Color.Transparent;
            appearance29.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel12.Appearance = appearance29;
            this.ultraPanel12.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel12.ClientArea
            // 
            this.ultraPanel12.ClientArea.Controls.Add(this.txtNewUsedTime);
            this.ultraPanel12.Location = new System.Drawing.Point(566, 211);
            this.ultraPanel12.Name = "ultraPanel12";
            this.ultraPanel12.Size = new System.Drawing.Size(296, 33);
            this.ultraPanel12.TabIndex = 72;
            // 
            // txtNewUsedTime
            // 
            appearance30.TextHAlignAsString = "Right";
            this.txtNewUsedTime.Appearance = appearance30;
            this.txtNewUsedTime.AutoSize = false;
            this.txtNewUsedTime.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtNewUsedTime.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtNewUsedTime.InputMask = " -n,nnn,nnn,nnn,nnn,nnn";
            this.txtNewUsedTime.Location = new System.Drawing.Point(6, 6);
            this.txtNewUsedTime.Name = "txtNewUsedTime";
            this.txtNewUsedTime.Size = new System.Drawing.Size(279, 22);
            this.txtNewUsedTime.TabIndex = 7;
            this.txtNewUsedTime.Text = " ";
            this.txtNewUsedTime.Validated += new System.EventHandler(this.txtNewUsedTime_Validated);
            // 
            // ultraPanel11
            // 
            appearance31.BackColor = System.Drawing.Color.Transparent;
            appearance31.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel11.Appearance = appearance31;
            this.ultraPanel11.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel11.ClientArea
            // 
            this.ultraPanel11.ClientArea.Controls.Add(this.txtNewDepreciationAmount);
            this.ultraPanel11.Location = new System.Drawing.Point(566, 179);
            this.ultraPanel11.Name = "ultraPanel11";
            this.ultraPanel11.Size = new System.Drawing.Size(296, 33);
            this.ultraPanel11.TabIndex = 71;
            // 
            // txtNewDepreciationAmount
            // 
            appearance32.TextHAlignAsString = "Right";
            this.txtNewDepreciationAmount.Appearance = appearance32;
            this.txtNewDepreciationAmount.AutoSize = false;
            this.txtNewDepreciationAmount.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtNewDepreciationAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtNewDepreciationAmount.InputMask = " -n,nnn,nnn,nnn,nnn,nnn.nn";
            this.txtNewDepreciationAmount.Location = new System.Drawing.Point(6, 6);
            this.txtNewDepreciationAmount.Name = "txtNewDepreciationAmount";
            this.txtNewDepreciationAmount.Size = new System.Drawing.Size(279, 22);
            this.txtNewDepreciationAmount.TabIndex = 5;
            this.txtNewDepreciationAmount.Text = " ";
            this.txtNewDepreciationAmount.ValueChanged += new System.EventHandler(this.txtNewDepreciationAmount_ValueChanged);
            this.txtNewDepreciationAmount.Validated += new System.EventHandler(this.txtNewDepreciationAmount_Validated);
            // 
            // ultraPanel10
            // 
            appearance33.BackColor = System.Drawing.Color.Transparent;
            appearance33.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel10.Appearance = appearance33;
            this.ultraPanel10.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel10.ClientArea
            // 
            this.ultraPanel10.ClientArea.Controls.Add(this.lblNewRemainingAmount);
            this.ultraPanel10.Location = new System.Drawing.Point(566, 147);
            this.ultraPanel10.Name = "ultraPanel10";
            this.ultraPanel10.Size = new System.Drawing.Size(296, 33);
            this.ultraPanel10.TabIndex = 70;
            // 
            // lblNewRemainingAmount
            // 
            this.lblNewRemainingAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance34.TextHAlignAsString = "Right";
            appearance34.TextVAlignAsString = "Middle";
            this.lblNewRemainingAmount.Appearance = appearance34;
            this.lblNewRemainingAmount.Location = new System.Drawing.Point(6, 6);
            this.lblNewRemainingAmount.Name = "lblNewRemainingAmount";
            this.lblNewRemainingAmount.Size = new System.Drawing.Size(279, 22);
            this.lblNewRemainingAmount.TabIndex = 2;
            // 
            // ultraPanel9
            // 
            appearance35.BackColor = System.Drawing.Color.Transparent;
            appearance35.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel9.Appearance = appearance35;
            this.ultraPanel9.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel9.ClientArea
            // 
            this.ultraPanel9.ClientArea.Controls.Add(this.txtNewOrgPrice);
            this.ultraPanel9.Location = new System.Drawing.Point(566, 115);
            this.ultraPanel9.Name = "ultraPanel9";
            this.ultraPanel9.Size = new System.Drawing.Size(296, 33);
            this.ultraPanel9.TabIndex = 69;
            // 
            // txtNewOrgPrice
            // 
            appearance36.TextHAlignAsString = "Right";
            this.txtNewOrgPrice.Appearance = appearance36;
            this.txtNewOrgPrice.AutoSize = false;
            this.txtNewOrgPrice.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtNewOrgPrice.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtNewOrgPrice.InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            this.txtNewOrgPrice.Location = new System.Drawing.Point(6, 5);
            this.txtNewOrgPrice.Name = "txtNewOrgPrice";
            this.txtNewOrgPrice.Size = new System.Drawing.Size(279, 22);
            this.txtNewOrgPrice.TabIndex = 4;
            this.txtNewOrgPrice.Validated += new System.EventHandler(this.txtNewOrgPrice_Validated);
            // 
            // ultraLabel7
            // 
            appearance37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(104)))), ((int)(((byte)(189)))));
            appearance37.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance37.BorderColor = System.Drawing.Color.Gray;
            appearance37.FontData.BoldAsString = "True";
            appearance37.ForeColor = System.Drawing.Color.White;
            appearance37.TextHAlignAsString = "Center";
            appearance37.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance37;
            this.ultraLabel7.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel7.Location = new System.Drawing.Point(566, 93);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(296, 23);
            this.ultraLabel7.TabIndex = 68;
            this.ultraLabel7.Text = "Giá trị mới";
            // 
            // ultraPanel8
            // 
            appearance38.BackColor = System.Drawing.Color.Transparent;
            appearance38.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel8.Appearance = appearance38;
            this.ultraPanel8.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel8.ClientArea
            // 
            this.ultraPanel8.ClientArea.Controls.Add(this.lblOldDepreciationMethod);
            this.ultraPanel8.Location = new System.Drawing.Point(242, 279);
            this.ultraPanel8.Name = "ultraPanel8";
            this.ultraPanel8.Size = new System.Drawing.Size(326, 36);
            this.ultraPanel8.TabIndex = 67;
            this.ultraPanel8.Visible = false;
            // 
            // lblOldDepreciationMethod
            // 
            this.lblOldDepreciationMethod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance39.TextVAlignAsString = "Middle";
            this.lblOldDepreciationMethod.Appearance = appearance39;
            this.lblOldDepreciationMethod.Location = new System.Drawing.Point(7, 7);
            this.lblOldDepreciationMethod.Name = "lblOldDepreciationMethod";
            this.lblOldDepreciationMethod.Size = new System.Drawing.Size(310, 21);
            this.lblOldDepreciationMethod.TabIndex = 38;
            this.lblOldDepreciationMethod.Text = "Đường thẳng";
            // 
            // ultraPanel7
            // 
            appearance40.BackColor = System.Drawing.Color.Transparent;
            appearance40.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel7.Appearance = appearance40;
            this.ultraPanel7.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel7.ClientArea
            // 
            this.ultraPanel7.ClientArea.Controls.Add(this.lblOldMonthlyDepreciationAmount);
            this.ultraPanel7.Location = new System.Drawing.Point(242, 243);
            this.ultraPanel7.Name = "ultraPanel7";
            this.ultraPanel7.Size = new System.Drawing.Size(326, 33);
            this.ultraPanel7.TabIndex = 66;
            // 
            // lblOldMonthlyDepreciationAmount
            // 
            this.lblOldMonthlyDepreciationAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance41.TextHAlignAsString = "Right";
            appearance41.TextVAlignAsString = "Middle";
            this.lblOldMonthlyDepreciationAmount.Appearance = appearance41;
            this.lblOldMonthlyDepreciationAmount.Location = new System.Drawing.Point(7, 6);
            this.lblOldMonthlyDepreciationAmount.Name = "lblOldMonthlyDepreciationAmount";
            this.lblOldMonthlyDepreciationAmount.Size = new System.Drawing.Size(310, 20);
            this.lblOldMonthlyDepreciationAmount.TabIndex = 38;
            // 
            // ultraPanel5
            // 
            appearance42.BackColor = System.Drawing.Color.Transparent;
            appearance42.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel5.Appearance = appearance42;
            this.ultraPanel5.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel5.ClientArea
            // 
            this.ultraPanel5.ClientArea.Controls.Add(this.lblOldUsedTime);
            this.ultraPanel5.Location = new System.Drawing.Point(242, 211);
            this.ultraPanel5.Name = "ultraPanel5";
            this.ultraPanel5.Size = new System.Drawing.Size(326, 33);
            this.ultraPanel5.TabIndex = 65;
            // 
            // lblOldUsedTime
            // 
            this.lblOldUsedTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance43.TextHAlignAsString = "Right";
            appearance43.TextVAlignAsString = "Middle";
            this.lblOldUsedTime.Appearance = appearance43;
            this.lblOldUsedTime.Location = new System.Drawing.Point(7, 5);
            this.lblOldUsedTime.Name = "lblOldUsedTime";
            this.lblOldUsedTime.Size = new System.Drawing.Size(310, 21);
            this.lblOldUsedTime.TabIndex = 38;
            // 
            // ultraPanel4
            // 
            appearance44.BackColor = System.Drawing.Color.Transparent;
            appearance44.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel4.Appearance = appearance44;
            this.ultraPanel4.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel4.ClientArea
            // 
            this.ultraPanel4.ClientArea.Controls.Add(this.lblOldDepreciationAmount);
            this.ultraPanel4.Location = new System.Drawing.Point(242, 179);
            this.ultraPanel4.Name = "ultraPanel4";
            this.ultraPanel4.Size = new System.Drawing.Size(326, 33);
            this.ultraPanel4.TabIndex = 64;
            // 
            // lblOldDepreciationAmount
            // 
            this.lblOldDepreciationAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance45.TextHAlignAsString = "Right";
            appearance45.TextVAlignAsString = "Middle";
            this.lblOldDepreciationAmount.Appearance = appearance45;
            this.lblOldDepreciationAmount.Location = new System.Drawing.Point(7, 7);
            this.lblOldDepreciationAmount.Name = "lblOldDepreciationAmount";
            this.lblOldDepreciationAmount.Size = new System.Drawing.Size(310, 20);
            this.lblOldDepreciationAmount.TabIndex = 38;
            // 
            // panelKhauHaoCu
            // 
            appearance46.BackColor = System.Drawing.Color.Transparent;
            appearance46.BorderColor = System.Drawing.Color.Gray;
            this.panelKhauHaoCu.Appearance = appearance46;
            this.panelKhauHaoCu.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // panelKhauHaoCu.ClientArea
            // 
            this.panelKhauHaoCu.ClientArea.Controls.Add(this.lblOldRemainingAmountOriginal);
            this.panelKhauHaoCu.Location = new System.Drawing.Point(242, 147);
            this.panelKhauHaoCu.Name = "panelKhauHaoCu";
            this.panelKhauHaoCu.Size = new System.Drawing.Size(326, 33);
            this.panelKhauHaoCu.TabIndex = 63;
            // 
            // lblOldRemainingAmountOriginal
            // 
            this.lblOldRemainingAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance47.TextHAlignAsString = "Right";
            appearance47.TextVAlignAsString = "Middle";
            this.lblOldRemainingAmountOriginal.Appearance = appearance47;
            this.lblOldRemainingAmountOriginal.Location = new System.Drawing.Point(7, 6);
            this.lblOldRemainingAmountOriginal.Name = "lblOldRemainingAmountOriginal";
            this.lblOldRemainingAmountOriginal.Size = new System.Drawing.Size(310, 20);
            this.lblOldRemainingAmountOriginal.TabIndex = 1;
            // 
            // ultraLabel6
            // 
            appearance48.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(104)))), ((int)(((byte)(189)))));
            appearance48.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance48.BorderColor = System.Drawing.Color.Gray;
            appearance48.FontData.BoldAsString = "True";
            appearance48.ForeColor = System.Drawing.Color.White;
            appearance48.TextHAlignAsString = "Center";
            appearance48.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance48;
            this.ultraLabel6.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel6.Location = new System.Drawing.Point(242, 93);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(326, 23);
            this.ultraLabel6.TabIndex = 62;
            this.ultraLabel6.Text = "Giá hiện thời";
            // 
            // panelNguyenGiaCu
            // 
            appearance49.BackColor = System.Drawing.Color.Transparent;
            appearance49.BorderColor = System.Drawing.Color.Gray;
            this.panelNguyenGiaCu.Appearance = appearance49;
            this.panelNguyenGiaCu.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // panelNguyenGiaCu.ClientArea
            // 
            this.panelNguyenGiaCu.ClientArea.Controls.Add(this.lblOldOrgPrice);
            this.panelNguyenGiaCu.Location = new System.Drawing.Point(242, 115);
            this.panelNguyenGiaCu.Name = "panelNguyenGiaCu";
            this.panelNguyenGiaCu.Size = new System.Drawing.Size(326, 33);
            this.panelNguyenGiaCu.TabIndex = 61;
            // 
            // lblOldOrgPrice
            // 
            this.lblOldOrgPrice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance50.TextHAlignAsString = "Right";
            appearance50.TextVAlignAsString = "Middle";
            this.lblOldOrgPrice.Appearance = appearance50;
            this.lblOldOrgPrice.Location = new System.Drawing.Point(7, 7);
            this.lblOldOrgPrice.Name = "lblOldOrgPrice";
            this.lblOldOrgPrice.Size = new System.Drawing.Size(310, 20);
            this.lblOldOrgPrice.TabIndex = 37;
            this.lblOldOrgPrice.BindingContextChanged += new System.EventHandler(this.lblOldOrgPrice_BindingContextChanged);
            // 
            // ultraPanel6
            // 
            appearance51.BackColor = System.Drawing.Color.Transparent;
            appearance51.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel6.Appearance = appearance51;
            this.ultraPanel6.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel6.ClientArea
            // 
            this.ultraPanel6.ClientArea.Controls.Add(this.ultraLabel4);
            this.ultraPanel6.Location = new System.Drawing.Point(12, 243);
            this.ultraPanel6.Name = "ultraPanel6";
            this.ultraPanel6.Size = new System.Drawing.Size(232, 33);
            this.ultraPanel6.TabIndex = 60;
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance52.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance52;
            this.ultraLabel4.Location = new System.Drawing.Point(2, 6);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(221, 21);
            this.ultraLabel4.TabIndex = 0;
            this.ultraLabel4.Text = "Giá trị khấu hao tháng :";
            // 
            // ultraPanel41
            // 
            appearance53.BackColor = System.Drawing.Color.Transparent;
            appearance53.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel41.Appearance = appearance53;
            this.ultraPanel41.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel41.ClientArea
            // 
            this.ultraPanel41.ClientArea.Controls.Add(this.ultraLabel12);
            this.ultraPanel41.Location = new System.Drawing.Point(12, 274);
            this.ultraPanel41.Name = "ultraPanel41";
            this.ultraPanel41.Size = new System.Drawing.Size(232, 36);
            this.ultraPanel41.TabIndex = 59;
            this.ultraPanel41.Visible = false;
            // 
            // ultraLabel12
            // 
            this.ultraLabel12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance54.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance54;
            this.ultraLabel12.Location = new System.Drawing.Point(2, 6);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(221, 21);
            this.ultraLabel12.TabIndex = 0;
            this.ultraLabel12.Text = "Phương pháp tính khấu hao :";
            // 
            // ultraPanel39
            // 
            appearance55.BackColor = System.Drawing.Color.Transparent;
            appearance55.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel39.Appearance = appearance55;
            this.ultraPanel39.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel39.ClientArea
            // 
            this.ultraPanel39.ClientArea.Controls.Add(this.cbbMonth);
            this.ultraPanel39.ClientArea.Controls.Add(this.ultraLabel9);
            this.ultraPanel39.Location = new System.Drawing.Point(12, 211);
            this.ultraPanel39.Name = "ultraPanel39";
            this.ultraPanel39.Size = new System.Drawing.Size(232, 33);
            this.ultraPanel39.TabIndex = 57;
            // 
            // cbbMonth
            // 
            this.cbbMonth.AutoSize = false;
            this.cbbMonth.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbMonth.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbMonth.Location = new System.Drawing.Point(126, 5);
            this.cbbMonth.Name = "cbbMonth";
            this.cbbMonth.Size = new System.Drawing.Size(94, 22);
            this.cbbMonth.TabIndex = 6;
            this.cbbMonth.ValueChanged += new System.EventHandler(this.cbbMonth_ValueChanged);
            // 
            // ultraLabel9
            // 
            this.ultraLabel9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance56.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance56;
            this.ultraLabel9.Location = new System.Drawing.Point(2, 5);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(118, 21);
            this.ultraLabel9.TabIndex = 0;
            this.ultraLabel9.Text = "Thời gian SD còn lại :";
            // 
            // ultraPanel38
            // 
            appearance57.BackColor = System.Drawing.Color.Transparent;
            appearance57.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel38.Appearance = appearance57;
            this.ultraPanel38.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel38.ClientArea
            // 
            this.ultraPanel38.ClientArea.Controls.Add(this.ultraLabel10);
            this.ultraPanel38.Location = new System.Drawing.Point(12, 179);
            this.ultraPanel38.Name = "ultraPanel38";
            this.ultraPanel38.Size = new System.Drawing.Size(232, 33);
            this.ultraPanel38.TabIndex = 56;
            // 
            // ultraLabel10
            // 
            this.ultraLabel10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance58.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance58;
            this.ultraLabel10.Location = new System.Drawing.Point(2, 6);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(221, 21);
            this.ultraLabel10.TabIndex = 0;
            this.ultraLabel10.Text = "Giá trị tính khấu hao :";
            // 
            // ultraPanel37
            // 
            appearance59.BackColor = System.Drawing.Color.Transparent;
            appearance59.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel37.Appearance = appearance59;
            this.ultraPanel37.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel37.ClientArea
            // 
            this.ultraPanel37.ClientArea.Controls.Add(this.utrallabel);
            this.ultraPanel37.Location = new System.Drawing.Point(12, 147);
            this.ultraPanel37.Name = "ultraPanel37";
            this.ultraPanel37.Size = new System.Drawing.Size(232, 33);
            this.ultraPanel37.TabIndex = 55;
            // 
            // utrallabel
            // 
            this.utrallabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance60.TextVAlignAsString = "Middle";
            this.utrallabel.Appearance = appearance60;
            this.utrallabel.Location = new System.Drawing.Point(2, 6);
            this.utrallabel.Name = "utrallabel";
            this.utrallabel.Size = new System.Drawing.Size(221, 24);
            this.utrallabel.TabIndex = 0;
            this.utrallabel.Text = "Giá trị còn lại :";
            // 
            // ultraPanel36
            // 
            appearance61.BackColor = System.Drawing.Color.Transparent;
            appearance61.BorderColor = System.Drawing.Color.Gray;
            this.ultraPanel36.Appearance = appearance61;
            this.ultraPanel36.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel36.ClientArea
            // 
            this.ultraPanel36.ClientArea.Controls.Add(this.ultraLabel3);
            this.ultraPanel36.Location = new System.Drawing.Point(12, 115);
            this.ultraPanel36.Name = "ultraPanel36";
            this.ultraPanel36.Size = new System.Drawing.Size(232, 33);
            this.ultraPanel36.TabIndex = 54;
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance62.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance62;
            this.ultraLabel3.Location = new System.Drawing.Point(2, 7);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(221, 20);
            this.ultraLabel3.TabIndex = 0;
            this.ultraLabel3.Text = "Nguyên giá :";
            // 
            // ultraLabel5
            // 
            appearance63.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(104)))), ((int)(((byte)(189)))));
            appearance63.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance63.BorderColor = System.Drawing.Color.Gray;
            appearance63.FontData.BoldAsString = "True";
            appearance63.ForeColor = System.Drawing.Color.White;
            appearance63.TextHAlignAsString = "Center";
            appearance63.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance63;
            this.ultraLabel5.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel5.Location = new System.Drawing.Point(12, 93);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(232, 23);
            this.ultraLabel5.TabIndex = 6;
            this.ultraLabel5.Text = "Chỉ tiêu";
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason.AutoSize = false;
            this.txtReason.Location = new System.Drawing.Point(118, 45);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(864, 22);
            this.txtReason.TabIndex = 3;
            // 
            // txtFixedAssetName
            // 
            this.txtFixedAssetName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFixedAssetName.AutoSize = false;
            this.txtFixedAssetName.Location = new System.Drawing.Point(259, 18);
            this.txtFixedAssetName.Name = "txtFixedAssetName";
            this.txtFixedAssetName.Size = new System.Drawing.Size(723, 22);
            this.txtFixedAssetName.TabIndex = 2;
            // 
            // cbbFixedAssetID
            // 
            appearance64.BorderColor = System.Drawing.Color.Transparent;
            this.cbbFixedAssetID.Appearance = appearance64;
            this.cbbFixedAssetID.AutoSize = false;
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            appearance65.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbFixedAssetID.DisplayLayout.Appearance = appearance65;
            this.cbbFixedAssetID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbFixedAssetID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance66.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance66.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance66.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance66.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbFixedAssetID.DisplayLayout.GroupByBox.Appearance = appearance66;
            appearance67.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbFixedAssetID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance67;
            this.cbbFixedAssetID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance68.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance68.BackColor2 = System.Drawing.SystemColors.Control;
            appearance68.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance68.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbFixedAssetID.DisplayLayout.GroupByBox.PromptAppearance = appearance68;
            this.cbbFixedAssetID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbFixedAssetID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance69.BackColor = System.Drawing.SystemColors.Window;
            appearance69.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbFixedAssetID.DisplayLayout.Override.ActiveCellAppearance = appearance69;
            appearance70.BackColor = System.Drawing.SystemColors.Highlight;
            appearance70.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbFixedAssetID.DisplayLayout.Override.ActiveRowAppearance = appearance70;
            this.cbbFixedAssetID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbFixedAssetID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            this.cbbFixedAssetID.DisplayLayout.Override.CardAreaAppearance = appearance71;
            appearance72.BorderColor = System.Drawing.Color.Silver;
            appearance72.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbFixedAssetID.DisplayLayout.Override.CellAppearance = appearance72;
            this.cbbFixedAssetID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbFixedAssetID.DisplayLayout.Override.CellPadding = 0;
            appearance73.BackColor = System.Drawing.SystemColors.Control;
            appearance73.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance73.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance73.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance73.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbFixedAssetID.DisplayLayout.Override.GroupByRowAppearance = appearance73;
            appearance74.TextHAlignAsString = "Left";
            this.cbbFixedAssetID.DisplayLayout.Override.HeaderAppearance = appearance74;
            this.cbbFixedAssetID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbFixedAssetID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance75.BackColor = System.Drawing.SystemColors.Window;
            appearance75.BorderColor = System.Drawing.Color.Silver;
            this.cbbFixedAssetID.DisplayLayout.Override.RowAppearance = appearance75;
            this.cbbFixedAssetID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance76.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbFixedAssetID.DisplayLayout.Override.TemplateAddRowAppearance = appearance76;
            this.cbbFixedAssetID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbFixedAssetID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbFixedAssetID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbFixedAssetID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbFixedAssetID.Location = new System.Drawing.Point(118, 18);
            this.cbbFixedAssetID.Name = "cbbFixedAssetID";
            this.cbbFixedAssetID.Size = new System.Drawing.Size(135, 22);
            this.cbbFixedAssetID.TabIndex = 1;
            this.cbbFixedAssetID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbTS_RowSelected);
            this.cbbFixedAssetID.Validating += new System.ComponentModel.CancelEventHandler(this.cbbFixedAssetID_Validating);
            // 
            // ultraLabel2
            // 
            appearance77.BackColor = System.Drawing.Color.Transparent;
            appearance77.BorderColor = System.Drawing.Color.Black;
            appearance77.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance77;
            this.ultraLabel2.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel2.Location = new System.Drawing.Point(12, 45);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(100, 21);
            this.ultraLabel2.TabIndex = 1;
            this.ultraLabel2.Text = "Diễn giải :";
            // 
            // ultraLabel1
            // 
            appearance78.BackColor = System.Drawing.Color.Transparent;
            appearance78.BorderColor = System.Drawing.Color.Black;
            appearance78.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance78;
            this.ultraLabel1.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel1.Location = new System.Drawing.Point(12, 18);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(100, 21);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Tài sản cố định :";
            // 
            // pnlUgrid
            // 
            // 
            // pnlUgrid.ClientArea
            // 
            this.pnlUgrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.pnlUgrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlUgrid.Location = new System.Drawing.Point(0, 393);
            this.pnlUgrid.Name = "pnlUgrid";
            this.pnlUgrid.Size = new System.Drawing.Size(997, 157);
            this.pnlUgrid.TabIndex = 1;
            this.pnlUgrid.TabStop = false;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance79.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance79;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(896, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 8;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 383);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 200;
            this.ultraSplitter1.Size = new System.Drawing.Size(997, 10);
            this.ultraSplitter1.TabIndex = 3;
            // 
            // FFAAdjustmentDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(997, 550);
            this.Controls.Add(this.pnlUgrid);
            this.Controls.Add(this.ultraSplitter1);
            this.Controls.Add(this.ultraPanel2);
            this.Controls.Add(this.ultraPanel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FFAAdjustmentDetail";
            this.Text = "FAAdjustmentDetail";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FFAAdjustmentDetail_FormClosing);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.pnlDateNo.ResumeLayout(false);
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraPanel13.ClientArea.ResumeLayout(false);
            this.ultraPanel13.ResumeLayout(false);
            this.ultraPanel23.ClientArea.ResumeLayout(false);
            this.ultraPanel23.ResumeLayout(false);
            this.ultraPanel24.ClientArea.ResumeLayout(false);
            this.ultraPanel24.ResumeLayout(false);
            this.ultraPanel25.ClientArea.ResumeLayout(false);
            this.ultraPanel25.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkSD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkKH)).EndInit();
            this.ultraPanel21.ResumeLayout(false);
            this.ultraPanel20.ClientArea.ResumeLayout(false);
            this.ultraPanel20.ResumeLayout(false);
            this.ultraPanel19.ClientArea.ResumeLayout(false);
            this.ultraPanel19.ResumeLayout(false);
            this.ultraPanel18.ClientArea.ResumeLayout(false);
            this.ultraPanel18.ResumeLayout(false);
            this.ultraPanel17.ClientArea.ResumeLayout(false);
            this.ultraPanel17.ResumeLayout(false);
            this.ultraPanel16.ClientArea.ResumeLayout(false);
            this.ultraPanel16.ResumeLayout(false);
            this.ultraPanel15.ClientArea.ResumeLayout(false);
            this.ultraPanel15.ResumeLayout(false);
            this.ultraPanel22.ClientArea.ResumeLayout(false);
            this.ultraPanel22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbNewDepreciationMethod)).EndInit();
            this.ultraPanel14.ClientArea.ResumeLayout(false);
            this.ultraPanel14.ResumeLayout(false);
            this.ultraPanel12.ClientArea.ResumeLayout(false);
            this.ultraPanel12.ResumeLayout(false);
            this.ultraPanel11.ClientArea.ResumeLayout(false);
            this.ultraPanel11.ResumeLayout(false);
            this.ultraPanel10.ClientArea.ResumeLayout(false);
            this.ultraPanel10.ResumeLayout(false);
            this.ultraPanel9.ClientArea.ResumeLayout(false);
            this.ultraPanel9.ResumeLayout(false);
            this.ultraPanel8.ClientArea.ResumeLayout(false);
            this.ultraPanel8.ResumeLayout(false);
            this.ultraPanel7.ClientArea.ResumeLayout(false);
            this.ultraPanel7.ResumeLayout(false);
            this.ultraPanel5.ClientArea.ResumeLayout(false);
            this.ultraPanel5.ResumeLayout(false);
            this.ultraPanel4.ClientArea.ResumeLayout(false);
            this.ultraPanel4.ResumeLayout(false);
            this.panelKhauHaoCu.ClientArea.ResumeLayout(false);
            this.panelKhauHaoCu.ResumeLayout(false);
            this.panelNguyenGiaCu.ClientArea.ResumeLayout(false);
            this.panelNguyenGiaCu.ResumeLayout(false);
            this.ultraPanel6.ClientArea.ResumeLayout(false);
            this.ultraPanel6.ResumeLayout(false);
            this.ultraPanel41.ClientArea.ResumeLayout(false);
            this.ultraPanel41.ResumeLayout(false);
            this.ultraPanel39.ClientArea.ResumeLayout(false);
            this.ultraPanel39.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbMonth)).EndInit();
            this.ultraPanel38.ClientArea.ResumeLayout(false);
            this.ultraPanel38.ResumeLayout(false);
            this.ultraPanel37.ClientArea.ResumeLayout(false);
            this.ultraPanel37.ResumeLayout(false);
            this.ultraPanel36.ClientArea.ResumeLayout(false);
            this.ultraPanel36.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedAssetName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbFixedAssetID)).EndInit();
            this.pnlUgrid.ClientArea.ResumeLayout(false);
            this.pnlUgrid.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbFixedAssetID;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFixedAssetName;
        private Infragistics.Win.Misc.UltraPanel ultraPanel11;
        private Infragistics.Win.Misc.UltraPanel ultraPanel10;
        private Infragistics.Win.Misc.UltraPanel ultraPanel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraPanel ultraPanel8;
        private Infragistics.Win.Misc.UltraPanel ultraPanel7;
        private Infragistics.Win.Misc.UltraPanel ultraPanel5;
        private Infragistics.Win.Misc.UltraPanel ultraPanel4;
        private Infragistics.Win.Misc.UltraPanel panelKhauHaoCu;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraPanel panelNguyenGiaCu;
        private Infragistics.Win.Misc.UltraPanel ultraPanel21;
        private Infragistics.Win.Misc.UltraPanel ultraPanel20;
        private Infragistics.Win.Misc.UltraPanel ultraPanel19;
        private Infragistics.Win.Misc.UltraPanel ultraPanel18;
        private Infragistics.Win.Misc.UltraPanel ultraPanel17;
        private Infragistics.Win.Misc.UltraPanel ultraPanel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraPanel ultraPanel15;
        private Infragistics.Win.Misc.UltraPanel ultraPanel14;
        private Infragistics.Win.Misc.UltraPanel ultraPanel12;
        private Infragistics.Win.Misc.UltraPanel ultraPanel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraPanel ultraPanel41;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraPanel ultraPanel39;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraPanel ultraPanel38;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraPanel ultraPanel37;
        private Infragistics.Win.Misc.UltraLabel utrallabel;
        private Infragistics.Win.Misc.UltraPanel ultraPanel36;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor checkSD;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor checkKH;
        private Infragistics.Win.Misc.UltraLabel lblOldDepreciationMethod;
        private Infragistics.Win.Misc.UltraLabel lblOldMonthlyDepreciationAmount;
        private Infragistics.Win.Misc.UltraLabel lblOldUsedTime;
        private Infragistics.Win.Misc.UltraLabel lblOldDepreciationAmount;
        private Infragistics.Win.Misc.UltraLabel lblOldRemainingAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel lblOldOrgPrice;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtNewMonthlyDepreciationAmount;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtNewDepreciationAmount;
        private Infragistics.Win.Misc.UltraPanel pnlUgrid;
        private Infragistics.Win.Misc.UltraPanel pnlDateNo;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtNewOrgPrice;
        private Infragistics.Win.Misc.UltraLabel lblDifferOrgPrice;
        private Infragistics.Win.Misc.UltraLabel lblNewRemainingAmount;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbNewDepreciationMethod;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtNewUsedTime;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbMonth;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraLabel lblDifferMonthlyDepreciationAmount;
        private Infragistics.Win.Misc.UltraLabel lblDifferRemainingAmount;
        private Infragistics.Win.Misc.UltraLabel lblDiffUsedTime;
        private Infragistics.Win.Misc.UltraLabel lblDiffDepreciationAmount;
        private Infragistics.Win.Misc.UltraPanel ultraPanel22;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit ultraMaskedEdit1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel13;
        private Infragistics.Win.Misc.UltraLabel lblDiffAcDepreciationAmount;
        private Infragistics.Win.Misc.UltraPanel ultraPanel23;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtNewAcDepreciationAmount;
        private Infragistics.Win.Misc.UltraPanel ultraPanel24;
        private Infragistics.Win.Misc.UltraLabel lblOldAcDepreciationAmount;
        private Infragistics.Win.Misc.UltraPanel ultraPanel25;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
    }
}