﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Itenso.TimePeriod;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace Accounting
{

    public partial class FFAAdjustmentDetail : FaadjustmentBase
    {
        #region khai báo


        private readonly List<AccountingObject> _lstaAccountingObjects = new List<AccountingObject>();
        private readonly List<FAAdjustment> _lstFAAdjustment = new List<FAAdjustment>();
        private BindingList<FAAdjustmentDetail> _lstFAAdjustmentDetails = new BindingList<FAAdjustmentDetail>();
        private List<Account> _lstAccounts = new List<Account>();
        private IFixedAssetLedgerService _IFixedAssetLedgerService = Utils.IFixedAssetLedgerService;

        private readonly List<FixedAsset> _lstFixedAssets = new List<FixedAsset>();
        private UltraGrid ultraGrid;
        private FixedAssetLedger fal = new FixedAssetLedger();
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        UltraGrid ugrid2 = null;
        #endregion

        public FFAAdjustmentDetail(FAAdjustment temp, List<FAAdjustment> dsFAdjustments, int statusForm)
        {
            #region khởi tạo ban đầu

            InitializeComponent();
            base.InitializeComponent1();

            _statusForm = statusForm;
            _select = statusForm == ConstFrm.optStatusForm.Add ? (new FAAdjustment { TypeID = 530 }) : (temp);
            _typeID = _select.TypeID;
            _listSelects.AddRange(dsFAdjustments);
            #endregion

            InitializeGUI(_select);
            ReloadToolbar(_select, _listSelects, _statusForm);

            Thongtinchung();
            // ViewGrid();
        }

        #region view

        public void Thongtinchung()
        {
            txtReason.Value = "Điều chỉnh TSCĐ";
            DisPanel(panelKhauHaoCu);
            txtNewUsedTime.Enabled = false;
            txtNewUsedTime.ReadOnly = true;
        }

        #endregion
        #region event

        private void cbbTS_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            if (_statusForm == ConstFrm.optStatusForm.View) return;
            if (e.Row == null) return;
            if (e.Row.ListObject == null) return;
            var fixedAsset = e.Row.ListObject as FixedAsset;
            fal = Utils.IGeneralLedgerService.GetLastFixedAssetLedger(fixedAsset.ID, _select.PostedDate);
            var k = cbbFixedAssetID.Value;
            if (fal != null)
            {
                if (_select.PostedDate < fixedAsset.UsedDate) return;
                txtFixedAssetName.Value = fixedAsset.FixedAssetName;
                lblOldOrgPrice.Text = fal.OriginalPrice.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
                _select.NewOrgPrice = fal.OriginalPrice ?? 0;

                lblOldDepreciationAmount.Text = fal.DepreciationAmount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
                txtNewDepreciationAmount.Text = fal.DepreciationAmount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);

                lblOldMonthlyDepreciationAmount.Text = fal.MonthPeriodDepreciationAmount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);

                lblOldUsedTime.Text = fal.UsedTimeRemain.ToStringNumbericFormat(ConstDatabase.Format_Quantity);

                lblOldRemainingAmountOriginal.Text = fal.RemainingAmount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
                lblNewRemainingAmount.Text = fal.RemainingAmount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);

                var AcDepreciationAmount = Utils.IFixedAssetLedgerService.GetTotalAcDepreciationAmount(fixedAsset.ID, _select.PostedDate);
                lblOldAcDepreciationAmount.Text = AcDepreciationAmount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
                txtNewAcDepreciationAmount.Text = AcDepreciationAmount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);

                decimal a = _select.NewOrgPrice;
                decimal b = _select.OldOrgPrice;
                decimal c = a - b;
                lblDifferOrgPrice.Text = c.ToStringNumbericFormat(ConstDatabase.Format_TienVND);

                //if (fixedAsset.UsedDate != null && fal.MonthDepreciationRate != null)
                //{
                //    var monthlyDepreciationAmount = Utils.IFADepreciationService.GetDepreciationByMonth((DateTime)fixedAsset.UsedDate,
                //                                                                  _select.PostedDate, fal.ID);
                //    var remainingAmount = fal.OriginalPrice ?? 0 - fal.AcDepreciationAmount ?? 0;

                //    lblOldRemainingAmountOriginal.Text =
                //        remainingAmount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
                //    lblNewRemainingAmount.Text = remainingAmount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);

                //    _select.NewRemainingAmount = remainingAmount;

                //}
                lblDifferRemainingAmount.Text = c.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
                txtNewMonthlyDepreciationAmount.Value = fal.MonthPeriodDepreciationAmount;
                //
                _select.NewDepreciationAmount = txtNewDepreciationAmount.Value != null ? Convert.ToDecimal(txtNewDepreciationAmount.Value) : 0;
                decimal i = _select.NewDepreciationAmount;
                decimal j = _select.OldDepreciationAmount;
                lblDiffDepreciationAmount.Text = (i - j).ToStringNumbericFormat(ConstDatabase.Format_TienVND);
                //
                cbbNewDepreciationMethod.SelectedItem =
                    (ValueListItem)
                    cbbNewDepreciationMethod.Items.All.FirstOrDefault(
                        x => Convert.ToInt32(((ValueListItem)x).DataValue) == 0);
                lblOldDepreciationMethod.Text = fixedAsset.DepreciationMethod == 0
                                                    ? "Đường thẳng"
                                                    : (fixedAsset.DepreciationMethod == 1
                                                           ? "Khấu hao nhanh"
                                                           : (fixedAsset.DepreciationMethod == 2 ? "Sản lượng" : "Đường thẳng"));
                switch (cbbMonth.Text)
                {
                    case "Năm":
                        lblOldUsedTime.Text = fal.UsedTimeRemain.ToStringNumbericFormat(ConstDatabase.Format_Quantity);
                        //if (fixedAsset.UsedDate != null)
                        //{
                        //    var usedMonths = (new DateDiff((DateTime)fixedAsset.UsedDate, _select.PostedDate)).Months;
                        //    if (fal.UsedTimeRemain != null)
                        //    {
                        //        var newUsedTime = (((decimal)fal.UsedTime * 12) - usedMonths) / 12; ;
                        //        txtNewUsedTime.Value = newUsedTime < 0 ? 0 : newUsedTime;
                        //    }
                        //}
                        break;
                    case "Tháng":
                        lblOldUsedTime.Text = (fal.UsedTimeRemain ?? 0).ToStringNumbericFormat(ConstDatabase.Format_Quantity);
                        //if (fixedAsset.UsedDate != null)
                        //{
                        //    var usedMonths = (new DateDiff((DateTime)fixedAsset.UsedDate, _select.PostedDate)).Months;
                        //    if (fal.UsedTime != null)
                        //    {
                        //        var newUsedTime = ((decimal)fal.UsedTime) - usedMonths;
                        //        txtNewUsedTime.Value = newUsedTime < 0 ? 0 : newUsedTime;
                        //    }
                        //}
                        break;
                }
                //
                try
                {
                    _select.NewMonthlyDepreciationAmount = Convert.ToDecimal(txtNewMonthlyDepreciationAmount.Value.ToString());
                }
                catch (Exception)
                {
                    _select.NewMonthlyDepreciationAmount = 0;
                }

                decimal g = _select.NewMonthlyDepreciationAmount;
                decimal h = _select.OldMonthlyDepreciationAmount;
                lblDifferMonthlyDepreciationAmount.Text = (g - h).ToStringNumbericFormat(ConstDatabase.Format_TienVND);
                //
                _select.NewUsedTime = _select.OldUsedTime;
                decimal? n = _select.NewUsedTime;
                decimal? o = _select.OldUsedTime;
                if (txtNewUsedTime.Enabled == false) lblDiffUsedTime.Text = "";
                else lblDiffUsedTime.Text = (a - b).ToStringNumbericFormat(ConstDatabase.Format_Quantity);
                //
                cbbFixedAssetID.Value = fixedAsset.ID;
                if (!e.Row.Selected) return;
                ChangeGridByNewOrgPrice(fal, fixedAsset);
            }
        }

        void ChangeGridByNewOrgPrice(FixedAssetLedger fixAsset, FixedAsset fa)
        {
            if (_statusForm == ConstFrm.optStatusForm.View) return;
            if (ultraGrid.DisplayLayout.Bands.Count == 0) return;
            if (_select.OldOrgPrice < _select.NewOrgPrice)
            {
                if (ultraGrid.Rows.Count == 0 /*|| ultraGrid.Rows[0].Tag == null || (ultraGrid.Rows[0].Tag != null && !((Dictionary<string, object>)ultraGrid.Rows[0].Tag).Any(x => x.Key == "IsAdjustmentFixedAsset"))*/)
                {
                    ultraGrid.DisplayLayout.Bands[0].AddNew();
                    ultraGrid.Rows[0].Cells["Description"].Value = string.Format(resSystem.String_06, fa.FixedAssetName);
                    var row = ultraGrid.Rows[0];
                    row.ParentCollection.Move(row, 0);
                    ultraGrid.ActiveRowScrollRegion.ScrollRowIntoView(row);
                    row.Tag = new Dictionary<string, object>() { { "IsAdjustmentFixedAsset", true } };
                }
                else
                {
                    bool isContain = false;
                    foreach (UltraGridRow row in ultraGrid.Rows)
                    {
                        if (row.Cells["Description"].Text.ToString().Contains("giá trị tính khấu hao"))
                        {
                            row.ParentCollection.Move(row, 0);
                            row.Cells["Description"].Value = string.Format(resSystem.String_06, fa.FixedAssetName);
                            isContain = true;
                            break;
                        }
                    }
                    if (!isContain)
                    {
                        var row2 = ultraGrid.DisplayLayout.Bands[0].AddNew();
                        if (row2 != null)
                        {
                            row2.ParentCollection.Move(row2, 0);
                            ultraGrid.ActiveRowScrollRegion.ScrollRowIntoView(row2);
                            row2.Tag = new Dictionary<string, object>() { { "IsAdjustmentFixedAsset", true } };
                            ultraGrid.Rows[0].Cells["Description"].Value = string.Format(resSystem.String_06, fa.FixedAssetName);
                        }
                    }
                }
                if (!string.IsNullOrEmpty(ultraGrid.Rows[0].Cells["DebitAccount"].Value as string))
                    ultraGrid.Rows[0].Cells["CreditAccount"].Value = ultraGrid.Rows[0].Cells["DebitAccount"].Value;
                ultraGrid.Rows[0].Cells["DebitAccount"].Value = fixAsset.OriginalPriceAccount;
                ultraGrid.Rows[0].Cells["Amount"].Value = _select.DifferOrgPrice;
                ultraGrid.Rows[0].Cells["DebitAccount"].SetErrorForCell(ultraGrid.CheckAccount<FAAdjustment>(ultraGrid.Rows[0].Cells["DebitAccount"]));
                ultraGrid.Rows[0].Cells["CreditAccount"].SetErrorForCell(ultraGrid.CheckAccount<FAAdjustment>(ultraGrid.Rows[0].Cells["CreditAccount"]));
                ultraGrid.Rows[0].Cells["DepartmentID"].Value = fal.DepartmentID;
                ultraGrid.Rows[0].Cells["CostSetID"].Value = fa.CostSetID;
                ultraGrid.Rows[0].Cells["BudgetItemID"].Value = fa.BudgetItemID;
                ultraGrid.Rows[0].Cells["StatisticsCodeID"].Value = fa.StatisticsCodeID;
            }
            else if (_select.OldOrgPrice > _select.NewOrgPrice)
            {
                if (ultraGrid.Rows.Count == 0 /*|| ultraGrid.Rows[0].Tag == null || (ultraGrid.Rows[0].Tag != null && !((Dictionary<string, object>)ultraGrid.Rows[0].Tag).Any(x => x.Key == "IsAdjustmentFixedAsset"))*/)
                {
                    ultraGrid.DisplayLayout.Bands[0].AddNew();
                    ultraGrid.Rows[0].Cells["Description"].Value = string.Format(resSystem.String_07, fa.FixedAssetName);
                    var row = ultraGrid.Rows[0];
                    row.ParentCollection.Move(row, 0);
                    ultraGrid.ActiveRowScrollRegion.ScrollRowIntoView(row);
                    row.Tag = new Dictionary<string, object>() { { "IsAdjustmentFixedAsset", true } };
                }
                else
                {
                    bool isContain = false;
                    foreach (UltraGridRow row in ultraGrid.Rows)
                    {
                        if (row.Cells["Description"].Text.ToString().Contains("giá trị tính khấu hao"))
                        {
                            row.ParentCollection.Move(row, 0);
                            row.Cells["Description"].Value = string.Format(resSystem.String_07, fa.FixedAssetName);
                            isContain = true;
                            break;
                        }
                    }
                    if (!isContain)
                    {
                        var row2 = ultraGrid.DisplayLayout.Bands[0].AddNew();
                        ultraGrid.Rows[0].Cells["Description"].Value = string.Format(resSystem.String_07, fa.FixedAssetName);
                        row2.ParentCollection.Move(row2, 0);
                        ultraGrid.ActiveRowScrollRegion.ScrollRowIntoView(row2);
                        row2.Tag = new Dictionary<string, object>() { { "IsAdjustmentFixedAsset", true } };
                    }
                }
                if (!string.IsNullOrEmpty(ultraGrid.Rows[0].Cells["CreditAccount"].Value as string))
                    ultraGrid.Rows[0].Cells["DebitAccount"].Value = ultraGrid.Rows[0].Cells["CreditAccount"].Value;
                ultraGrid.Rows[0].Cells["CreditAccount"].Value = fixAsset.OriginalPriceAccount;
                ultraGrid.Rows[0].Cells["Amount"].Value = -_select.DifferOrgPrice;
                ultraGrid.Rows[0].Cells["DebitAccount"].SetErrorForCell(ultraGrid.CheckAccount<FAAdjustment>(ultraGrid.Rows[0].Cells["DebitAccount"]));
                ultraGrid.Rows[0].Cells["CreditAccount"].SetErrorForCell(ultraGrid.CheckAccount<FAAdjustment>(ultraGrid.Rows[0].Cells["CreditAccount"]));
                ultraGrid.Rows[0].Cells["CostSetID"].Value = fa.CostSetID;
                ultraGrid.Rows[0].Cells["BudgetItemID"].Value = fa.BudgetItemID;
                ultraGrid.Rows[0].Cells["StatisticsCodeID"].Value = fa.StatisticsCodeID;
                ultraGrid.Rows[0].Cells["DepartmentID"].Value = fal.DepartmentID;

            }
            else
            {
                if (ultraGrid.Rows.Count > 0 && (ultraGrid.Rows[0].Tag != null && ((Dictionary<string, object>)ultraGrid.Rows[0].Tag).Any(x => x.Key == "IsAdjustmentFixedAsset")))
                    ultraGrid.Rows[0].Delete(false);
            }
        }

        private void checkKH_CheckedChanged(object sender, EventArgs e)
        {
            if (checkKH.Checked == false)
            {
                DisText(txtNewOrgPrice, true);
                DisText(txtNewDepreciationAmount, true);
                DisText(txtNewAcDepreciationAmount, true);
                DisText(txtNewMonthlyDepreciationAmount, true);
            }
            else
            {
                DisText(txtNewOrgPrice, false);
                DisText(txtNewDepreciationAmount, false);
                DisText(txtNewAcDepreciationAmount, false);
                DisText(txtNewMonthlyDepreciationAmount, false);
            }

        }

        private void checkSD_CheckedChanged(object sender, EventArgs e)
        {

            if (checkSD.Checked == false)
            {
                txtNewUsedTime.Value = Decimal.Parse((lblOldUsedTime.Text.Trim() == "" || lblOldUsedTime.Text == null) ? "0" : lblOldUsedTime.Text.ToString());
                txtNewUsedTime.Enabled = false;
                txtNewUsedTime.ReadOnly = true;
                lblDiffUsedTime.Text = "";
            }
            else
            {
                txtNewUsedTime.Enabled = true;
                txtNewUsedTime.ReadOnly = false;
                if (cbbFixedAssetID.Value != null)
                {

                    _select.NewUsedTime = txtNewUsedTime.Value != null ? Convert.ToDecimal(txtNewUsedTime.Value) : 0;
                    decimal? a = _select.NewUsedTime;
                    decimal? b = _select.OldUsedTime;
                    lblDiffUsedTime.Text = (a - b).ToStringNumbericFormat(ConstDatabase.Format_Quantity);
                    cbbFixedAssetID.UpdateData();
                }
            }

        }

        private void checkKH_AfterCheckStateChanged(object sender, EventArgs e)
        {
            if (checkKH.Checked == false & checkSD.Checked == false)
            {
                MSG.Error((Accounting.TextMessage.resSystem.MSG_Error_12));
                checkKH.Checked = true;
            }

        }

        private void checkSD_AfterCheckStateChanged(object sender, EventArgs e)
        {
            if (checkKH.Checked == false & checkSD.Checked == false)
            {
                MSG.Error((Accounting.TextMessage.resSystem.MSG_Error_12));
                checkSD.Checked = true;
            }

        }

        #endregion
        #region utils

        public void DisText(UltraMaskedEdit textEditor, bool isDis)
        {
            textEditor.ReadOnly = isDis;
            textEditor.Enabled = !isDis;
        }

        public void DisPanel(UltraPanel panelEditor)
        {
            panelEditor.Enabled = false;
        }
        #endregion
        #region override

        public override void InitializeGUI(FAAdjustment input)
        {
            #region lay cac gia tri const tu database

            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)

            Template mauGiaoDien = Utils.GetMauGiaoDien(input.TypeID, input.TemplateID,
                FFAAdjustment.DsTemplates);
            BindingList<FAAdjustmentDetail> _dsFAAdjustmentDetails = new BindingList<FAAdjustmentDetail>();
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;

            #endregion

            #endregion

            #region thiết lập dữ liệu và fill dữ liệu Obj vào control

            #endregion

            _dsFAAdjustmentDetails =
                new BindingList<FAAdjustmentDetail>(_statusForm == ConstFrm.optStatusForm.Add
                    ? new List<FAAdjustmentDetail>()
                    : input.FAAdjustmentDetails);
            BindingList<RefVoucher> bldRefVoucher = new BindingList<RefVoucher>();
            if (_statusForm != ConstFrm.optStatusForm.Add)
                bldRefVoucher = new BindingList<RefVoucher>(input.RefVouchers);
            _listObjectInput = new BindingList<System.Collections.IList> { _dsFAAdjustmentDetails };
            _listObjectInputPost = new BindingList<System.Collections.IList> { bldRefVoucher };
            this.ConfigGridByTemplete_General<FAAdjustment>(pnlUgrid, mauGiaoDien);
            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { true, false };
            this.ConfigGridByManyTemplete<FAAdjustment>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
            ugrid2 = Controls.Find("uGrid1", true).FirstOrDefault() as UltraGrid;
            ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);

            ultraGrid = (UltraGrid)this.Controls.Find("uGrid0", true).FirstOrDefault();
            ultraGrid.SummaryValueChanged += new SummaryValueChangedEventHandler(ultraGrid_SummaryValueChanged);
            this.ConfigTopVouchersNo<FAAdjustment>(pnlDateNo, "No", "PostedDate", "Date");

            this.ConfigCombo(Utils.ListFixedAsset, cbbFixedAssetID, "FixedAssetCode", "ID", input, "FixedAssetID", DataSourceUpdateMode.OnPropertyChanged, haveEditorButton: false);

            Utils.ConfigComboFixedAsset(cbbFixedAssetID, 0);
            DataBinding(new List<Control> { txtFixedAssetName, txtReason },
                "FixedAssetName,Reason");
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
            }
            lblOldOrgPrice.DataBindings.Clear();
            lblOldOrgPrice.BindControl("Text", input, "OldOrgPrice", ConstDatabase.Format_TienVND);

            lblOldRemainingAmountOriginal.DataBindings.Clear();
            lblOldRemainingAmountOriginal.BindControl("Text", input, "OldRemainingAmountOriginal", ConstDatabase.Format_TienVND);

            lblOldDepreciationAmount.DataBindings.Clear();
            lblOldDepreciationAmount.BindControl("Text", input, "OldDepreciationAmount", ConstDatabase.Format_TienVND);

            lblOldMonthlyDepreciationAmount.DataBindings.Clear();
            lblOldMonthlyDepreciationAmount.BindControl("Text", input, "OldMonthlyDepreciationAmount", ConstDatabase.Format_TienVND);

            lblOldUsedTime.DataBindings.Clear();
            lblOldUsedTime.BindControl("Text", input, "OldUsedTime", ConstDatabase.Format_Quantity);

            lblNewRemainingAmount.DataBindings.Clear();
            lblNewRemainingAmount.BindControl("Text", input, "NewRemainingAmount", ConstDatabase.Format_TienVND);

            lblDifferOrgPrice.DataBindings.Clear();
            lblDifferOrgPrice.BindControl("Text", input, "DifferOrgPrice", ConstDatabase.Format_TienVND);

            lblOldAcDepreciationAmount.DataBindings.Clear();
            lblOldAcDepreciationAmount.BindControl("Text", input, "OldAcDepreciationAmount", ConstDatabase.Format_TienVND);

            txtNewAcDepreciationAmount.DataBindings.Clear();
            txtNewAcDepreciationAmount.BindControl("Text", input, "NewAcDepreciationAmount", ConstDatabase.Format_TienVND);

            lblDiffAcDepreciationAmount.DataBindings.Clear();
            lblDiffAcDepreciationAmount.BindControl("Text", input, "DifferAcDepreciationAmount", ConstDatabase.Format_TienVND);

            lblDifferMonthlyDepreciationAmount.DataBindings.Clear();
            lblDifferMonthlyDepreciationAmount.BindControl("Text", input, "DifferMonthlyDepreciationAmount", ConstDatabase.Format_TienVND);

            lblDiffDepreciationAmount.DataBindings.Clear();
            lblDiffDepreciationAmount.BindControl("Text", input, "DifferDepreciationAmount", ConstDatabase.Format_TienVND);

            lblDifferRemainingAmount.DataBindings.Clear();
            lblDifferRemainingAmount.BindControl("Text", input, "DifferRemainingAmount", ConstDatabase.Format_TienVND);

            lblDiffUsedTime.DataBindings.Clear();
            lblDiffUsedTime.BindControl("Text", input, "DifferUsedTime", ConstDatabase.Format_TienVND);

            txtNewUsedTime.DataBindings.Clear();
            txtNewUsedTime.BindControl("Text", input, "NewUsedTime", ConstDatabase.Format_TienVND);

            txtNewMonthlyDepreciationAmount.DataBindings.Clear();
            txtNewMonthlyDepreciationAmount.BindControl("Text", input, "NewMonthlyDepreciationAmount", ConstDatabase.Format_TienVND);

            txtNewDepreciationAmount.DataBindings.Clear();
            txtNewDepreciationAmount.BindControl("Text", input, "NewDepreciationAmount", ConstDatabase.Format_TienVND);

            txtNewOrgPrice.DataBindings.Clear();
            txtNewOrgPrice.BindControl("Text", input, "NewOrgPrice", ConstDatabase.Format_TienVND);

            cbbMonth.Items.Add(true, "Tháng");
            cbbMonth.Items.Add(false, "Năm");

            cbbMonth.SelectedIndex = 0;

            txtNewOrgPrice.FormatNumberic(ConstDatabase.Format_TienVND);
            txtNewDepreciationAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            txtNewMonthlyDepreciationAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            txtNewUsedTime.FormatNumberic(ConstDatabase.Format_Quantity);
            lblDifferRemainingAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            lblDiffDepreciationAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            txtNewAcDepreciationAmount.FormatNumberic(ConstDatabase.Format_TienVND);

            _select.IsDisplayByMonth = true;
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 530)//trungnq thêm vào để  làm task 6234
                {
                    txtReason.Value = "Điều chỉnh tài sản cố định";
                    txtReason.Text = "Điều chỉnh tài sản cố định";
                    _select.Reason = "Điều chỉnh tài sản cố định";
                };
            }
        }


        void ultraGrid_SummaryValueChanged(object sender, SummaryValueChangedEventArgs e)
        {
            if (e.SummaryValue.Key == "SumAmount")
                _select.TotalAmount = (decimal)e.SummaryValue.Value;
        }

        #endregion

        private void cbbMonth_ValueChanged(object sender, EventArgs e)
        {
            var combo = (UltraComboEditor)sender;
            if (!combo.DroppedDown) return;
            if (cbbFixedAssetID.SelectedRow == null) return;
            if (cbbFixedAssetID.SelectedRow.ListObject == null) return;
            var fixAsset = cbbFixedAssetID.SelectedRow.ListObject as FixedAsset;
            if (fixAsset == null) return;
            if (_select.PostedDate < fixAsset.UsedDate) return;
            _select.IsDisplayByMonth = (bool)combo.Value;
            switch ((bool)combo.Value)
            {
                case false:
                    if (fixAsset.UsedTime != null)
                    {
                        lblOldUsedTime.Text = fixAsset.UsedTime.ToStringNumbericFormat(ConstDatabase.Format_Quantity);
                        if (fixAsset.UsedDate != null)
                        {
                            var usedMonths = (new DateDiff((DateTime)fixAsset.UsedDate, _select.PostedDate)).Months;
                            var newUsedTime = (((decimal)fixAsset.UsedTime * 12) - usedMonths) / 12;
                            txtNewUsedTime.Value = newUsedTime < 0 ? 0 : newUsedTime;
                        }
                        else
                        {
                            txtNewUsedTime.Value = Decimal.Parse(txtNewUsedTime.Text.ToString()) / 12;
                        }
                    }
                    if (_select.NewUsedTime != null && _select.NewUsedTime != 0)
                        txtNewMonthlyDepreciationAmount.Value = _select.NewRemainingAmount * (100 / Decimal.Round((decimal)_select.NewUsedTime * 12, 0)) / 100;
                    break;
                case true:
                    if (fixAsset.UsedTime != null)
                    {
                        lblOldUsedTime.Text = ((decimal)fixAsset.UsedTime * 12).ToStringNumbericFormat(ConstDatabase.Format_Quantity);
                        if (fixAsset.UsedDate != null)
                        {
                            var usedMonths = (new DateDiff((DateTime)fixAsset.UsedDate, _select.PostedDate)).Months;
                            var newUsedTime = ((decimal)fixAsset.UsedTime * 12) - usedMonths;
                            txtNewUsedTime.Value = newUsedTime < 0 ? 0 : newUsedTime;
                        }
                        else
                        {
                            txtNewUsedTime.Value = Decimal.Parse(txtNewUsedTime.Text.ToString()) * 12;
                        }
                    }
                    if (_select.NewUsedTime != null && _select.NewUsedTime != 0)
                        txtNewMonthlyDepreciationAmount.Value = _select.NewRemainingAmount * (100 / (decimal)_select.NewUsedTime) / 100;
                    break;
            }
        }

        private void txtNewDepreciationAmount_ValueChanged(object sender, EventArgs e)
        {
            switch (cbbMonth.Text)
            {
                case "Năm":
                    if (_select.NewUsedTime != null && _select.NewUsedTime != 0)
                        txtNewMonthlyDepreciationAmount.Value = _select.NewRemainingAmount * (100 / Decimal.Round((decimal)_select.NewUsedTime * 12, 0)) / 100;
                    break;
                case "Tháng":
                    if (_select.NewUsedTime != null && _select.NewUsedTime != 0)
                        txtNewMonthlyDepreciationAmount.Value = _select.NewRemainingAmount * (100 / (decimal)_select.NewUsedTime) / 100;
                    break;
            }
        }

        private void lblOldOrgPrice_BindingContextChanged(object sender, EventArgs e)
        {

        }

        private void txtNewOrgPrice_Validated(object sender, EventArgs e)
        {
            cbbFixedAssetID.UpdateData();
            if (cbbFixedAssetID.SelectedRow == null) return;
            if (cbbFixedAssetID.SelectedRow.ListObject == null) return;
            var fixedAsset = cbbFixedAssetID.SelectedRow.ListObject as FixedAsset;
            _select.NewOrgPrice = txtNewOrgPrice.Value != null ? Convert.ToDecimal(txtNewOrgPrice.Value) : 0;
            decimal a = _select.NewOrgPrice;
            decimal b = _select.OldOrgPrice;
            lblDifferOrgPrice.Text = (a - b).ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            var c = _select.DifferOrgPrice + _select.OldRemainingAmountOriginal;
            lblNewRemainingAmount.Text = c.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            if (_select.NewDepreciationAmount == 0)
            {
                _select.NewDepreciationAmount = c;
                txtNewDepreciationAmount.Value = c;
            }
            lblDifferRemainingAmount.Text = (a - b).ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            decimal d = _select.OldDepreciationAmount;
            decimal k = _select.NewDepreciationAmount;
            lblDiffDepreciationAmount.Text = (k - d).ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            switch (cbbMonth.Text)
            {
                case "Năm":
                    if (_select.NewUsedTime != null && _select.NewUsedTime != 0)
                        txtNewMonthlyDepreciationAmount.Value = _select.NewRemainingAmount * (100 / Decimal.Round((decimal)_select.NewUsedTime * 12, 0)) / 100;
                    break;
                case "Tháng":
                    if (_select.NewUsedTime != null && _select.NewUsedTime != 0)
                    {
                        decimal temp = _select.NewRemainingAmount * (100 / (decimal)_select.NewUsedTime) / 100;
                        if (temp < 0)
                            txtNewMonthlyDepreciationAmount.EditAs = EditAsType.UseSpecifiedMask;
                        txtNewMonthlyDepreciationAmount.Value = temp;
                    }
                    break;
            }
            ChangeGridByNewOrgPrice(fal, fixedAsset);
            txtNewDepreciationAmount.Value = txtNewOrgPrice.Value;
            calculateNewDepreciationAmount();
            decimal g = _select.NewMonthlyDepreciationAmount;
            decimal h = _select.OldMonthlyDepreciationAmount;
            lblDifferMonthlyDepreciationAmount.Text = (g - h).ToStringNumbericFormat(ConstDatabase.Format_TienVND);

        }

        private void txtNewMonthlyDepreciationAmount_Validated(object sender, EventArgs e)
        {
            cbbFixedAssetID.UpdateData();
            if (cbbFixedAssetID.SelectedRow == null) return;
            if (cbbFixedAssetID.SelectedRow.ListObject == null) return;
            _select.NewMonthlyDepreciationAmount = txtNewMonthlyDepreciationAmount.Value != null ? Convert.ToDecimal(txtNewMonthlyDepreciationAmount.Value) : 0;
            switch (cbbMonth.Text)
            {
                case "Năm":
                    if (_select.NewUsedTime != null && _select.NewUsedTime != 0)
                        txtNewMonthlyDepreciationAmount.Value = _select.NewRemainingAmount * (100 / Decimal.Round((decimal)_select.NewUsedTime * 12, 0)) / 100;
                    break;
                case "Tháng":
                    if (_select.NewUsedTime != null && _select.NewUsedTime != 0)
                        txtNewMonthlyDepreciationAmount.Value = _select.NewRemainingAmount * (100 / (decimal)_select.NewUsedTime) / 100;
                    break;
            }
            decimal a = _select.NewMonthlyDepreciationAmount;
            decimal b = _select.OldMonthlyDepreciationAmount;
            lblDifferMonthlyDepreciationAmount.Text = (a - b).ToStringNumbericFormat(ConstDatabase.Format_TienVND);

        }

        private void txtNewDepreciationAmount_Validated(object sender, EventArgs e)
        {
            calculateNewDepreciationAmount();
            calculateNewRemainingAmount();
        }

        private void calculateNewDepreciationAmount()
        {
            cbbFixedAssetID.UpdateData();
            if (cbbFixedAssetID.SelectedRow == null) return;
            if (cbbFixedAssetID.SelectedRow.ListObject == null) return;
            _select.NewDepreciationAmount = txtNewDepreciationAmount.Value != null ? Convert.ToDecimal(txtNewDepreciationAmount.Value) : 0;
            decimal a = _select.NewDepreciationAmount;
            decimal b = _select.OldDepreciationAmount;
            lblDiffDepreciationAmount.Text = (a - b).ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            switch (cbbMonth.Text)
            {
                case "Năm":
                    if (_select.NewUsedTime != null && _select.NewUsedTime != 0)
                        txtNewMonthlyDepreciationAmount.Value = _select.NewRemainingAmount * (100 / Decimal.Round((decimal)_select.NewUsedTime * 12, 0)) / 100;
                    break;
                case "Tháng":
                    if (_select.NewUsedTime != null && _select.NewUsedTime != 0)
                        txtNewMonthlyDepreciationAmount.Value = _select.NewRemainingAmount * (100 / (decimal)_select.NewUsedTime) / 100;
                    break;
            }
        }

        private void txtNewUsedTime_Validated(object sender, EventArgs e)
        {
            cbbFixedAssetID.UpdateData();
            if (cbbFixedAssetID.SelectedRow == null) return;
            if (cbbFixedAssetID.SelectedRow.ListObject == null) return;
            switch (cbbMonth.Text)
            {
                case "Năm":
                    if (_select.NewUsedTime != null && _select.NewUsedTime != 0)
                        txtNewMonthlyDepreciationAmount.Value = _select.NewRemainingAmount * (100 / Decimal.Round((decimal)_select.NewUsedTime * 12, 0)) / 100;
                    break;
                case "Tháng":
                    if (_select.NewUsedTime != null && _select.NewUsedTime != 0)
                        txtNewMonthlyDepreciationAmount.Value = _select.NewRemainingAmount * (100 / (decimal)_select.NewUsedTime) / 100;
                    break;
            }
            _select.NewUsedTime = txtNewUsedTime.Value != null ? Convert.ToDecimal(txtNewUsedTime.Value) : 0;
            decimal? a = _select.NewUsedTime;
            decimal? b = _select.OldUsedTime;
            lblDiffUsedTime.Text = (a - b).ToStringNumbericFormat(ConstDatabase.Format_Quantity);
            _select.NewMonthlyDepreciationAmount = txtNewMonthlyDepreciationAmount.Value != null ? Convert.ToDecimal(txtNewMonthlyDepreciationAmount.Value) : 0;
            decimal n = _select.NewMonthlyDepreciationAmount;
            decimal o = _select.OldMonthlyDepreciationAmount;
            lblDifferMonthlyDepreciationAmount.Text = (n - o).ToStringNumbericFormat(ConstDatabase.Format_TienVND);
        }

        private void txtNewAcDepreciationAmount_Validated(object sender, EventArgs e)
        {
            cbbFixedAssetID.UpdateData();
            if (cbbFixedAssetID.SelectedRow == null) return;
            if (cbbFixedAssetID.SelectedRow.ListObject == null) return;
            _select.NewAcDepreciationAmount = txtNewAcDepreciationAmount.Value != null ? Convert.ToDecimal(txtNewAcDepreciationAmount.Value) : 0;
            decimal a = _select.NewAcDepreciationAmount;
            decimal b = _select.OldAcDepreciationAmount;
            lblDiffAcDepreciationAmount.Text = (a - b).ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            calculateNewRemainingAmount();

            var fixedAsset = cbbFixedAssetID.SelectedRow.ListObject as FixedAsset;
            _select.NewOrgPrice = txtNewOrgPrice.Value != null ? Convert.ToDecimal(txtNewOrgPrice.Value) : 0;

            ChangeGridByNewAcDepreciation(fal, fixedAsset);
        }

        private void calculateNewRemainingAmount()
        {
            decimal a = _select.NewAcDepreciationAmount;
            decimal b = _select.NewDepreciationAmount;
            lblNewRemainingAmount.Text = (b - a).ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            decimal c = _select.NewRemainingAmount;
            decimal d = _select.OldRemainingAmountOriginal;
            lblDifferRemainingAmount.Text = (c - d).ToStringNumbericFormat(ConstDatabase.Format_TienVND);
        }

        void ChangeGridByNewAcDepreciation(FixedAssetLedger fixAsset, FixedAsset fa)
        {
            if (_statusForm == ConstFrm.optStatusForm.View) return;
            if (ultraGrid.DisplayLayout.Bands.Count == 0) return;
            if (_select.OldAcDepreciationAmount < _select.NewAcDepreciationAmount)
            {
                if (ultraGrid.Rows.Count == 0 /*|| ultraGrid.Rows[0].Tag == null || (ultraGrid.Rows[0].Tag != null && !((Dictionary<string, object>)ultraGrid.Rows[0].Tag).Any(x => x.Key == "IsAdjustmentFixedAsset"))*/)
                {
                    ultraGrid.DisplayLayout.Bands[0].AddNew();
                    var row = ultraGrid.Rows.Last();
                    if (row != null)
                    {
                        row.ParentCollection.Move(row, 0);
                        ultraGrid.ActiveRowScrollRegion.ScrollRowIntoView(row);
                        row.Tag = new Dictionary<string, object>() { { "IsAdjustmentFixedAsset", true } };
                        ultraGrid.Rows[0].Cells["Description"].Value = string.Format("Điều chỉnh tăng hao mòn lũy kế của TSCĐ {0}", fa.FixedAssetName);
                    }
                }
                else
                {
                    bool isContain = false;
                    foreach (UltraGridRow row in ultraGrid.Rows)
                    {
                        if (row.Cells["Description"].Text.ToString().Contains("hao mòn lũy kế"))
                        {
                            row.ParentCollection.Move(row, 0);
                            row.Cells["Description"].Value = string.Format("Điều chỉnh tăng hao mòn lũy kế của TSCĐ {0}", fa.FixedAssetName);
                            isContain = true;
                            break;
                        }
                    }
                    if (!isContain)
                    {
                        ultraGrid.DisplayLayout.Bands[0].AddNew();
                        var row2 = ultraGrid.Rows.Last();
                        if (row2 != null)
                        {
                            row2.ParentCollection.Move(row2, 0);
                            ultraGrid.ActiveRowScrollRegion.ScrollRowIntoView(row2);
                            row2.Tag = new Dictionary<string, object>() { { "IsAdjustmentFixedAsset", true } };
                            ultraGrid.Rows[0].Cells["Description"].Value = string.Format("Điều chỉnh tăng hao mòn lũy kế của TSCĐ {0}", fa.FixedAssetName);
                        }
                        else
                            return;
                    }
                }
                if (!string.IsNullOrEmpty(ultraGrid.Rows[0].Cells["DebitAccount"].Value as string))
                    ultraGrid.Rows[0].Cells["CreditAccount"].Value = ultraGrid.Rows[0].Cells["DebitAccount"].Value;
                ultraGrid.Rows[0].Cells["DebitAccount"].Value = fixAsset.OriginalPriceAccount;
                ultraGrid.Rows[0].Cells["Amount"].Value = _select.DifferAcDepreciationAmount;
                ultraGrid.Rows[0].Cells["DebitAccount"].SetErrorForCell(ultraGrid.CheckAccount<FAAdjustment>(ultraGrid.Rows[0].Cells["DebitAccount"]));
                ultraGrid.Rows[0].Cells["CreditAccount"].SetErrorForCell(ultraGrid.CheckAccount<FAAdjustment>(ultraGrid.Rows[0].Cells["CreditAccount"]));
                ultraGrid.Rows[0].Cells["CostSetID"].Value = fa.CostSetID;
                ultraGrid.Rows[0].Cells["BudgetItemID"].Value = fa.BudgetItemID;
                ultraGrid.Rows[0].Cells["StatisticsCodeID"].Value = fa.StatisticsCodeID;
                ultraGrid.Rows[0].Cells["DepartmentID"].Value = fal.DepartmentID;
            }
            else if (_select.OldAcDepreciationAmount > _select.NewAcDepreciationAmount)
            {
                if (ultraGrid.Rows.Count == 0 /*|| ultraGrid.Rows[0].Tag == null || (ultraGrid.Rows[0].Tag != null && !((Dictionary<string, object>)ultraGrid.Rows[0].Tag).Any(x => x.Key == "IsAdjustmentFixedAsset"))*/)
                {

                    ultraGrid.DisplayLayout.Bands[0].AddNew();
                    var row = ultraGrid.Rows.Last();
                    if (row != null)
                    {
                        row.ParentCollection.Move(row, 0);
                        ultraGrid.ActiveRowScrollRegion.ScrollRowIntoView(row);
                        row.Tag = new Dictionary<string, object>() { { "IsAdjustmentFixedAsset", true } };
                        ultraGrid.Rows[0].Cells["Description"].Value = string.Format("Điều chỉnh giảm hao mòn lũy kế của TSCĐ {0}", fa.FixedAssetName);
                    }
                }

                else
                {
                    bool isContain = false;
                    foreach (UltraGridRow row in ultraGrid.Rows)
                    {
                        if (row.Cells["Description"].Text.ToString().Contains("hao mòn lũy kế"))
                        {
                            row.ParentCollection.Move(row, 0);
                            row.Cells["Description"].Value = string.Format("Điều chỉnh giảm hao mòn lũy kế của TSCĐ {0}", fa.FixedAssetName);
                            isContain = true;
                            break;
                        }
                    }
                    if (!isContain)
                    {
                        ultraGrid.DisplayLayout.Bands[0].AddNew();
                        var row2 = ultraGrid.Rows.Last();
                        if (row2 != null)
                        {
                            row2.ParentCollection.Move(row2, 0);
                            ultraGrid.ActiveRowScrollRegion.ScrollRowIntoView(row2);
                            row2.Tag = new Dictionary<string, object>() { { "IsAdjustmentFixedAsset", true } };
                            ultraGrid.Rows[0].Cells["Description"].Value = string.Format("Điều chỉnh giảm hao mòn lũy kế của TSCĐ {0}", fa.FixedAssetName);
                        }
                        else
                            return;
                    }
                }
                if (!string.IsNullOrEmpty(ultraGrid.Rows[0].Cells["CreditAccount"].Value as string))
                    ultraGrid.Rows[0].Cells["DebitAccount"].Value = ultraGrid.Rows[0].Cells["CreditAccount"].Value;
                ultraGrid.Rows[0].Cells["CreditAccount"].Value = fixAsset.OriginalPriceAccount;
                ultraGrid.Rows[0].Cells["Amount"].Value = -_select.DifferAcDepreciationAmount;
                ultraGrid.Rows[0].Cells["DebitAccount"].SetErrorForCell(ultraGrid.CheckAccount<FAAdjustment>(ultraGrid.Rows[0].Cells["DebitAccount"]));
                ultraGrid.Rows[0].Cells["CreditAccount"].SetErrorForCell(ultraGrid.CheckAccount<FAAdjustment>(ultraGrid.Rows[0].Cells["CreditAccount"]));
                ultraGrid.Rows[0].Cells["CostSetID"].Value = fa.CostSetID;
                ultraGrid.Rows[0].Cells["BudgetItemID"].Value = fa.BudgetItemID;
                ultraGrid.Rows[0].Cells["StatisticsCodeID"].Value = fa.StatisticsCodeID;
                ultraGrid.Rows[0].Cells["DepartmentID"].Value = fal.DepartmentID;

            }
            else
            {
                //if (ultraGrid.Rows.Count > 0 && (ultraGrid.Rows[0].Tag != null && ((Dictionary<string, object>)ultraGrid.Rows[0].Tag).Any(x => x.Key == "IsAdjustmentFixedAsset")))
                //ultraGrid.Rows[0].Delete(false);
            }
        }

        private void cbbFixedAssetID_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(cbbFixedAssetID.Text))
            {
                _select.FixedAssetID = null;
                _select.FixedAssetName = null;
                _select.OldOrgPrice = 0;
                _select.OldDepreciationAmount = 0;
                _select.OldEachYearDepreciationAmount = 0;
                _select.OldMonthlyDepreciationAmount = 0;
                _select.OldAcDepreciationAmount = 0;
                _select.OldRemainingAmountOriginal = 0;
                _select.NewOrgPrice = 0;
                _select.DifferOrgPrice = 0;
                _select.DifferAcDepreciationAmount = 0;
                _select.DifferMonthlyDepreciationAmount = 0;
                _select.DifferRemainingAmount = 0;
                _select.TotalAmount = 0;
                _select.NewDepreciationAmount = 0;
                _select.NewEachYearDepreciationAmount = 0;
                _select.NewMonthlyDepreciationAmount = 0;
                _select.NewAcDepreciationAmount = 0;
                _select.NewRemainingAmount = 0;
                _select.NewDepreciationMethod = 0;
                _select.IsDisplayByMonth = false;
                _select.IsAdjustDepreciationAmount = false;
                _select.IsAdjustUsedTime = null;
                _select.IsAdjustDepreciation = null;
                _select.NewUsedTime = null;
                _select.OldDepreciationMethod = null;
                _select.OldUsedTime = null;
                _select.OldEachYearDepreciationRate = null;
                _select.DifferUsedTime = null;
                _select.DifferDepreciationAmount = null;
            }
        }

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucher>();
                    var f = new FViewVoucherOriginal("VND", datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void FFAAdjustmentDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }

    public class FaadjustmentBase : DetailBase<FAAdjustment> { }

}