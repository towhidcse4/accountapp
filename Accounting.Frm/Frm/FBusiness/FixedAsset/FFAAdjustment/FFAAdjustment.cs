﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.Text.RegularExpressions;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using System.Globalization;
using System.Threading;
using Infragistics.Win.UltraWinGrid;
namespace Accounting
{
    public partial class FFAAdjustment : CustormForm
    {
        #region khai báo
        //public BindingList<FAAdjustment>LstFAAdjustments = new BindingList<FAAdjustment>();
        List<FAAdjustment> LstFAAdjustments = new List<FAAdjustment>();
        private readonly IFAAdjustmentService _IFAAdjustmentService;
        private readonly IGeneralLedgerService _IGeneralLedgerService;
        private readonly IFAAdjustmentDetailService _IFAAdjustmentDetailService;
        public static Dictionary<string, Template> DsTemplates = new Dictionary<string, Template>();
        // private bool isClose = true;
        #endregion
        #region khoi tao
        public FFAAdjustment()
        {
            #region khởi tạo mặc định cho form
            InitializeComponent();
            _IFAAdjustmentService = IoC.Resolve<IFAAdjustmentService>();
            _IGeneralLedgerService = IoC.Resolve<IGeneralLedgerService>();
            #endregion
            #region khởi tạo giá trị ban đầu cho form

            #endregion

            LoadDuLieu(true);
            ViewGridFaa();
            ViewGridFaaDetail();
            //tu chon select dong dau tien
            if (UgridFAA.Rows.Count > 0) UgridFAA.Rows[0].Selected = true;
            UgridFAA.DisplayLayout.Override.CellClickAction = CellClickAction.RowSelect;

        }
        #endregion

        private void LoadDuLieu(bool configGrid = true)
        {
            #region lay du lieu tu csdl

            DateTime? dbStartDateTime = Utils.StringToDateTime(ConstFrm.DbStartDate);
            int nam = (dbStartDateTime == null ? DateTime.Now.Year : dbStartDateTime.Value.Year);
            LstFAAdjustments = _IFAAdjustmentService.GetAll();
            UgridFAA.DataSource = LstFAAdjustments;
           if (configGrid) ConfigGrid(UgridFAA);

            #endregion
        }
        #region button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        void AddFunction()
        {
            FAAdjustment temp = UgridFAA.Selected.Rows.Count <= 0
                ? new FAAdjustment()
                : UgridFAA.Rows[0].ListObject as FAAdjustment;
            new FFAAdjustmentDetail(temp, LstFAAdjustments, ConstFrm.optStatusForm.Add).ShowDialog(this);
            if (!FFAAdjustmentDetail.IsClose) LoadDuLieu();
        }

        void EditFunction()
        {
            if (UgridFAA.Selected.Rows.Count > 0)
            {
                FAAdjustment temp = UgridFAA.Rows[0].ListObject as FAAdjustment;
                new FFAAdjustmentDetail(temp, LstFAAdjustments, ConstFrm.optStatusForm.View).ShowDialog(this);
                if (!FFAAdjustmentDetail.IsClose) LoadDuLieu();

            }
            else
            {
                MSG.Error(resSystem.MSG_System_04);
            }
        }

        void DeleteFunction()
        {
            if (UgridFAA.Selected.Rows.Count > 0)
            {
                FAAdjustment temp = UgridFAA.Selected.Rows[0].ListObject as FAAdjustment;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, temp.No)) == DialogResult.Yes)
                {
                    _IFAAdjustmentService.BeginTran();
                    List<GeneralLedger> listgGeneralLedgers = _IGeneralLedgerService.GetByReferenceID(temp.ID).ToList();
                    foreach (var generalLedgerService in listgGeneralLedgers)
                    {
                        _IGeneralLedgerService.Delete(generalLedgerService);
                    }
                    _IFAAdjustmentService.Delete(temp);
                    _IFAAdjustmentService.CommitTran();
                    LoadDuLieu();
                }

            }
            else
            {
                MSG.Error(resSystem.MSG_System_06);
            }
        }
        #endregion
        void ViewGridFaa()
        {

        }

        void ViewGridFaaDetail()

        {
            Ugrid.DataSource = new List<FAAdjustmentDetail> {new FAAdjustmentDetail()};
            Utils.ConfigGrid(Ugrid, ConstDatabase.FAAdjustmentDetail_TableName);
            //if (configGrid) configGrid(ugridTS);
            Ugrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            Ugrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            Ugrid.DisplayLayout.Bands[0].Summaries.Clear();
            Ugrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;

            
            Ugrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            Ugrid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
            UltraGridBand band = Ugrid.DisplayLayout.Bands[0];
          
            Ugrid.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
        }
        #region utils

        void ConfigGrid(UltraGrid ultraGrid)
        {
            Utils.ConfigGrid(UgridFAA, ConstDatabase.FAAdjustment_TableName);
            ultraGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Format = Constants.DdMMyyyy;
            ultraGrid.DisplayLayout.Bands[0].Columns["Date"].Format = Constants.DdMMyyyy;
            ultraGrid.DisplayLayout.Bands[0].Columns["PostedDate"].CellAppearance.TextHAlign = HAlign.Center;
            ultraGrid.DisplayLayout.Bands[0].Columns["Date"].CellAppearance.TextHAlign = HAlign.Center;
        }

        public static Template getMauGiaoDien(int typeId,Guid? templateID, Dictionary<string, Template> dsTemplates) // lay chuoi string keyofdsTemplate
        {
            string keyofdsTemplate = string.Format("{0}@{1}", typeId, templateID);
            if (!dsTemplates.Keys.Contains(keyofdsTemplate))
            {
                //Note: các chứng từ được sinh tự động từ nghiệp vụ khác thì phải truyền TypeID của nghiệp vụ khác đó
                int typeIdTemp = GetTypeIdRef(typeId);
                Template template = Utils.GetTemplateInDatabase(templateID, typeIdTemp);
                dsTemplates.Add(keyofdsTemplate, template);
                return template;
            }
            //keyofdsTemplate = string.Empty;
            return dsTemplates[keyofdsTemplate];
        }
        static int GetTypeIdRef(int typeId)
        {
            return typeId;
        }
        #endregion


        #region Event
        private void UgridFAA_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (UgridFAA.Selected.Rows.Count < 1) return;
            object listOject = UgridFAA.Selected.Rows[0].ListObject;
            if (listOject == null) return;
            FAAdjustment lstFAAdjustment = listOject as FAAdjustment;
            if (lstFAAdjustment == null) return;

            UltraGridBand band = Ugrid.DisplayLayout.Bands[0];

            Template mauGiaoDien = getMauGiaoDien(lstFAAdjustment.TypeID,lstFAAdjustment.TemplateID,DsTemplates);

            Utils.ConfigGrid(Ugrid, ConstDatabase.FAAdjustment_TableName, mauGiaoDien == null ? new List<TemplateColumn>() : mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 0).TemplateColumns, 0);
            
            if (lstFAAdjustment.FAAdjustmentDetails.Count == 0) lstFAAdjustment.FAAdjustmentDetails = new List<FAAdjustmentDetail>();
            Ugrid.DataSource = lstFAAdjustment.FAAdjustmentDetails;

            Ugrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            Ugrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            Ugrid.DisplayLayout.Bands[0].Summaries.Clear();
            Ugrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;

           // Ugrid.DisplayLayout.Bands[0].Columns["TotalAmount"].CellAppearance.TextHAlign = HAlign.Right;
            //Ugrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            //Ugrid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
           
            //SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["PostedDate"]);
            //summary.DisplayFormat = "Số dòng = {0:N0}";
            //summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            //Ugrid.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            ////
            //Ugrid.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
            //summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            //summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            //summary.DisplayFormat = "{0:N0}";
            //summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            //DisColums(band.Columns["PostedDate"]);
            //DisColums(band.Columns["Date"]);
            //DisColums(band.Columns["No"]);
            //DisColums(band.Columns["Reason"]);
            //DisColums(band.Columns["FixedAssetID"]);
          
        }

        public void DisColums(UltraGridColumn eUltraGrid)
        {
            eUltraGrid.CellActivation = Activation.NoEdit;
            eUltraGrid.CellClickAction = CellClickAction.RowSelect;
        }
        #endregion

        private void tsmAdd_Click_1(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click_1(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void UgridFAA_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunction();
        }

    }
}
