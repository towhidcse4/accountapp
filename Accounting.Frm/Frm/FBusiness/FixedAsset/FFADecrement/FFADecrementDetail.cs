﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Castle.Windsor.Installer;
using FX.Core;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Accounting
{
    public partial class FFADecrementDetail : FASDecrementBase
    {
        #region khai bao
        public static bool isClose = true;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        UltraGrid ugrid2 = null;
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        #endregion
        #region khởi tạo
        public FFADecrementDetail(FADecrement temp, List<FADecrement> lstDecrements, int statusForm)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();
            #endregion

            #region Thiết lập ban đầu cho Form
            _statusForm = statusForm;
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.TypeID = 520;
            }
            else
            {
                _select = temp;
            }
            _listSelects.AddRange(lstDecrements);
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, _listSelects, statusForm);
            #endregion

            //this.WindowState = FormWindowState.Maximized;
         

            

        }
        #endregion
        #region
        #endregion
        #region sử lý các viewgrid trên form
        #endregion
        #region event

        #endregion

        #region Hàm override
        public override void InitializeGUI(FADecrement input)
        {
            Template mauGiaoDien = Utils.CloneObject(Utils.GetMauGiaoDien(input.TypeID, input.TemplateID));
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;
            BindingList<FADecrementDetail> dsDecrementDetails = new BindingList<FADecrementDetail>();
            BindingList<FADecrementDetail> dsDecrementDetailGroups = new BindingList<FADecrementDetail>();
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            List<FADecrementDetailPost> lst = new List<FADecrementDetailPost>();
            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                dsDecrementDetails = new BindingList<FADecrementDetail>(input.FADecrementDetails);
                bdlRefVoucher = new BindingList<RefVoucher>(input.RefVouchers);
                //lst = Utils.getDecrementDetailGroup(input.FADecrementDetails.ToList());
                foreach (FADecrementDetail fadecrementdetail in input.FADecrementDetails)
                {
                    lst.AddRange(fadecrementdetail.FADecrementDetailPosts);
                }
            }


            #region cau hinh ban dau cho form
            _select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmount;
            _select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add ? "VND" : input.CurrencyID;

            _listObjectInput = new BindingList<System.Collections.IList> { dsDecrementDetails };
            _listObjectInputGroup = new BindingList<System.Collections.IList> { lst };
            _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
            this.ConfigGridByTemplete_General<FADecrement>(pnlUgrid, mauGiaoDien);
            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInputGroup, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { true, true, false };
            this.ConfigGridByManyTemplete<FADecrement>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
            ugrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
            ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            this.ConfigTopVouchersNo<FADecrement>(pnlDateNo, "No", "PostedDate", "Date");
            //// add by Hautv
            //Control dteP = this.Controls.Find("dtePostedDate", true).First();
            //Control lbeP = this.Controls.Find("lblPostedDate", true).First();
            //if (dteP != null && lbeP != null)
            //{
            //    dteP.Visible = false;
            //    lbeP.Visible = false;
            //}
            ///*---------------------------------------*/
            DataBinding(new List<Control> { txtReason }, "Reason");
            if (_statusForm == ConstFrm.optStatusForm.Add)
                _select.ID = Guid.NewGuid();
            #endregion
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 520)//trungnq thêm vào để  làm task 6234
                {
                    txtReason.Value = "Giảm tài sản cố định";
                    txtReason.Text = "Giảm tài sản cố định";
                    _select.Reason = "Giảm tài sản cố định";
                };
            }
        }
        #endregion

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucher>();
                    var f = new FViewVoucherOriginal(_select.CurrencyID, datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void FFADecrementDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }

    public class FASDecrementBase : DetailBase<FADecrement> { }
}
