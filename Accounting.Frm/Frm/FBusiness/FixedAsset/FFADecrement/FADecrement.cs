﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FFADecrement : CustormForm
    {
        #region khai báo
       private readonly IFADecrementService _ifaDecrementService;
        private readonly IFADecrementDetailService _IDecrementDetailService;
        private readonly IFixedAssetService _iFixedAssetService;
        private readonly IDepartmentService _iDepartmentService;
        public readonly IGeneralLedgerService _IGeneralLedgerService;
        public readonly IFixedAssetLedgerService _IFixedAssetLedgerService;
        public List<FADecrement> lstDecrements = new List<FADecrement>();
        public BindingList<FADecrementDetail>LstDecrementDetails = new BindingList<FADecrementDetail>();

        public static Dictionary<string, Template> DsTemplate = new Dictionary<string, Template>();
        #endregion
        #region khởi tạo
        public FFADecrement()
        {
            #region khởi tạo mặc định
            InitializeComponent();
            #endregion
            #region khởi tạo ban đầu cho form
          _ifaDecrementService = IoC.Resolve<IFADecrementService>();
           // _IFADecrementDetailService = IoC.Resolve<IFADecrementDetailService>();
            _iFixedAssetService = IoC.Resolve<IFixedAssetService>();
            _iDepartmentService = IoC.Resolve<IDepartmentService>();
            _IGeneralLedgerService = IoC.Resolve<IGeneralLedgerService>();
            _IFixedAssetLedgerService = IoC.Resolve<IFixedAssetLedgerService>();
            ugridCT.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion
            #region tạo dữ liệu ban đầu cho form
          
            ViewGridTS();
            loadding();

            #endregion

        }

        private void loadding(bool configGrid=true)
        {
            if (ugridTS.Rows.Count > 0) ugridTS.Rows[0].Selected = true;
            if (configGrid) ConfigGrid(ugridTS);
            ugridTS.DataSource = _ifaDecrementService.GetAll();
         //   ugridCT.DataSource = _;
        }
        void ConfigGrid(UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, ConstDatabase.FADecrement_TableName);
            utralGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Format = Constants.DdMMyyyy;
            utralGrid.DisplayLayout.Bands[0].Columns["Date"].Format = Constants.DdMMyyyy;
            utralGrid.DisplayLayout.Bands[0].Columns["PostedDate"].CellAppearance.TextHAlign = HAlign.Center;
            utralGrid.DisplayLayout.Bands[0].Columns["Date"].CellAppearance.TextHAlign = HAlign.Center;
        }

        #endregion
        #region view grid
        public void ViewGridTS(bool configGrid = true)
        {
            #region hiển thị và xử lý sự kiện

           lstDecrements = _ifaDecrementService.GetAll();
            ugridTS.DataSource = lstDecrements;
            Utils.ConfigGrid(ugridTS, ConstDatabase.FADecrement_TableName);
            //if (configGrid) configGrid(ugridTS);
            ugridTS.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            ugridTS.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            ugridTS.DisplayLayout.Bands[0].Summaries.Clear();
            ugridTS.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            ugridTS.DisplayLayout.Bands[0].Columns["TotalAmount"].Format = "#,###,###,##đ";
            ugridTS.DisplayLayout.Bands[0].Columns["TotalAmount"].CellAppearance.TextHAlign = HAlign.Right;
            ugridTS.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            ugridTS.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
            UltraGridBand band = ugridTS.DisplayLayout.Bands[0];
            SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["PostedDate"]);
            summary.DisplayFormat = "Số dòng = {0:N0}";
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            ugridTS.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            //
            ugridTS.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            summary = band.Summaries.Add("TotalMoney", SummaryType.Sum, band.Columns["TotalAmount"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            DisColums(band.Columns["PostedDate"]);
            DisColums(band.Columns["Date"]);
            DisColums(band.Columns["No"]);
            DisColums(band.Columns["Reason"]);
            DisColums(band.Columns["TotalAmount"]);

            #endregion

        }

        #endregion
        #region button
        private void btnThem_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnXem_Click(object sender, EventArgs e)
        {
           
            EditFunction();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            Savelege();
        }

        private void btnBG_Click(object sender, EventArgs e)
        {
            RemoveLeged();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion
        #region event
        private void ugridTS_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index == -1) return;
            EditFunction();
        }
        private void ugridTS_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            ugridTS.UpdateData();
            if (ugridTS.Selected.Rows.Count < 1) return;
            var listObject = ugridTS.Selected.Rows[0].ListObject;
            //var lstObjDetail = ugridCT.Selected.Rows[0].ListObject ;
          //  var model = lstObjDetail as FADecrementDetail;
            if (ugridTS.Selected.Rows.Count > 0)
            {
               // var index= ugridCT.ActiveCell.Row.Index;
                if (listObject == null) return;
                var ojecFDepartment = listObject as FADecrement;
                if (ojecFDepartment == null) return;
                if (ojecFDepartment.FADecrementDetails.Count == 0)
                    ojecFDepartment.FADecrementDetails = new List<FADecrementDetail>();
                ugridCT.DataSource = ojecFDepartment.FADecrementDetails;    //Utils.CloneObject(mcPayment.MCPaymentDetails);


                ugridCT.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
                ugridCT.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
                ugridCT.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                //tắt tiêu đề
                ugridCT.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
                //select cả hàng hay ko?
                UltraGridBand band = ugridCT.DisplayLayout.Bands[0];
                ugridCT.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
                ugridCT.DisplayLayout.Bands[0].Columns["Amount"].Format = "#,###,###,##đ";
                ugridCT.DisplayLayout.Bands[0].Columns["Amount"].CellAppearance.TextHAlign = HAlign.Right;
                SummarySettings summary = band.Summaries.Add("Count1", SummaryType.Sum, band.Columns["AmountOriginal"]);
                summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                summary.DisplayFormat = "{0:N0}";
                summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
                //Hiện những dòng trống?
                ugridCT.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
                ugridCT.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;

                //if (model!=null)
                //{
                //    LstDecrementDetails[index].Description = model.Description;
                //    LstDecrementDetails[index].DepartmentID = model.DepartmentID;
                //    LstDecrementDetails.AddNew();
                //    LstDecrementDetails[index+1].Description = model.Description;
                //    LstDecrementDetails[index + 1].FixAssetID = model.FixAssetID;
                //    LstDecrementDetails[index+1].DepartmentID = model.DepartmentID;
                //}
                
               //Fix Header
                ugridCT.DisplayLayout.UseFixedHeaders = true;

               band.Summaries.Clear();
                Template mauGiaoDien = GetMauGiaoDien(ojecFDepartment.TypeID, ojecFDepartment.TemplateID, DsTemplate);
                Utils.ConfigGrid(ugridCT, ConstDatabase.FADecrementDetail_TableName, mauGiaoDien == null ? new List<TemplateColumn>() : mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 1).TemplateColumns, 1);
                foreach (var item in band.Columns)
                {
                    if (item.Key == "FixedAssetID")
                    {
                        List<FixedAsset> lstFixedAssets = _iFixedAssetService.GetAll();
                        item.Editor = this.CreateCombobox(lstFixedAssets, ConstDatabase.FixedAsset_TableName, "ID", "FixedAssetCode");
                    }

                    else if (item.Key.Equals("DepartmentID"))
                    {
                        List<Department> lstDepartments = _iDepartmentService.GetAll();
                        item.Editor = this.CreateCombobox(lstDepartments, ConstDatabase.Department_TableName, "ID", "DepartmentCode");

                    }

                    }
                DisColums(band.Columns["FixedAssetID"]);
                DisColums(band.Columns["DepartmentID"]);
                DisColums(band.Columns["Description"]);
                DisColums(band.Columns["DebitAccount"]);
                DisColums(band.Columns["CreditAccount"]);
                DisColums(band.Columns["AmountOriginal"]);
            }
        }
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }
        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            loadding(true);
        }

        #endregion
        #region utils
        public static Template GetMauGiaoDien(int typeId, Guid? templateId, Dictionary<string, Template> dsTemplate)    //out string keyofdsTemplate
        {
            string keyofdsTemplate = string.Format("{0}@{1}", typeId, templateId);
            if (!dsTemplate.Keys.Contains(keyofdsTemplate))
            {
                //Note: các chứng từ được sinh tự động từ nghiệp vụ khác thì phải truyền TypeID của nghiệp vụ khác đó
                int typeIdTemp = GetTypeIdRef(typeId);
                Template template = Utils.GetTemplateInDatabase(templateId, typeIdTemp);
                dsTemplate.Add(keyofdsTemplate, template);
                return template;
            }
            //keyofdsTemplate = string.Empty;
            return dsTemplate[keyofdsTemplate];
        }
        static int GetTypeIdRef(int typeId)
        {
            return typeId;
        }
        private void DeleteFunction()
        {

            if (ugridTS.Selected.Rows.Count > 0)
            {
                var temp = ugridTS.Selected.Rows[0].ListObject as FADecrement;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, temp.No)) == DialogResult.Yes)
                {
                    _ifaDecrementService.BeginTran();
                    _ifaDecrementService.Delete(temp);
                    _ifaDecrementService.CommitTran();
                    ViewGridTS();
                }
            }
        }

        private void Savelege()
        {
            //try
            //{
            //    FADecrement temp = ugridTS.Selected.Rows[0].ListObject as FADecrement;
            //    if (temp == null)
            //    {
            //        return;
            //    }
            //    List<FADecrementDetail> lstDecrementDetails = _IDecrementDetailService.GetByFADecrementDetailID(temp.ID);

            //    List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            //    //ghi so tscd
            //    for (int i = 0; i < lstDecrementDetails.Count; i++)
            //    {
            //        var firstOrDefault = _iFixedAssetService.GetTimeUsed(lstDecrementDetails[i].FixedAssetID ?? Guid.NewGuid()).FirstOrDefault();
            //        FixedAssetLedger genFixedAssetLedger = new FixedAssetLedger
            //        {
            //            ID = Guid.NewGuid(),
            //            ReferenceID = temp.ID,
            //            No = temp.No,
            //            PostedDate = temp.PostedDate,
            //            Date = temp.Date,
            //            FixedAssetID = lstDecrementDetails[i].FixedAssetID,
            //            DepartmentID = lstDecrementDetails[i].DepartmentID ?? new Guid(),
            //            UsedTime = firstOrDefault.UsedTime,
            //            DepreciationAmount = firstOrDefault.MonthPeriodDepreciationAmount,
            //            DepreciationRate = firstOrDefault.MonthDepreciationRate,
            //            OriginalPriceAccount = lstDecrementDetails[i].CreditAccount,
            //            OriginalPriceDebitAmount = null,
            //            OriginalPriceCreditAmount = lstDecrementDetails[i].AmountOriginal,
            //            DepreciationAccount = lstDecrementDetails[i].DebitAccount,
            //            DepreciationCreditAmount = lstDecrementDetails[i].AmountOriginal,
            //            Reason = temp.Reason,
            //            Description = lstDecrementDetails[i].Description,
            //            OrderPriority = lstDecrementDetails[i].OrderPriority,
            //            IsIrrationalCost = lstDecrementDetails[i].IsIrrationalCost

            //        };
            //    }
            //    // ghi so cai 
            //    for (int i = 0; i < lstDecrementDetails.Count; i++)
            //    {
            //        GeneralLedger genTemp = new GeneralLedger
            //        {
            //            ID = Guid.NewGuid(),
            //            BranchID = null,
            //            ReferenceID = temp.ID,
            //            TypeID = temp.TypeID,
            //            Date = temp.Date,
            //            PostedDate = temp.PostedDate,
            //            No = temp.No,
            //            Account = lstDecrementDetails[i].DebitAccount,
            //            AccountCorresponding = lstDecrementDetails[i].CreditAccount,
            //            DebitAmountOriginal = lstDecrementDetails[i].AmountOriginal,
            //            CurrencyID = temp.CurrencyID,
            //            ExchangeRate = temp.ExchangeRate,
            //            DebitAmount = lstDecrementDetails[i].Amount,
            //            DetailID = lstDecrementDetails[i].ID,
            //            Reason = temp.Reason,
            //            Description = lstDecrementDetails[i].Description,
            //            AccountingObjectID = lstDecrementDetails[i].AccountingObjectID,
            //            EmployeeID = lstDecrementDetails[i].AccountingObjectID,
            //            BudgetItemID = lstDecrementDetails[i].BudgetItemID,
            //            CostSetID = lstDecrementDetails[i].CostSetID,
            //            ContractID = lstDecrementDetails[i].ContractID,
            //            StatisticsCodeID = lstDecrementDetails[i].StatisticsCodeID,
            //            RefNo = temp.No,
            //            RefDate = temp.Date,
            //            DepartmentID = lstDecrementDetails[i].DepartmentID,
            //            ExpenseItemID = lstDecrementDetails[i].ExpenseItemID,
            //            IsIrrationalCost = lstDecrementDetails[i].IsIrrationalCost

            //        };
            //        if (((lstDecrementDetails[i].CreditAccount.StartsWith("133")) || lstDecrementDetails[i].CreditAccount.StartsWith("33311")))
            //        {
            //            genTemp.VATDescription = lstDecrementDetails[i].Description;
            //        }
            //        listGenTemp.Add(genTemp);
            //        GeneralLedger genTempCorresponding = new GeneralLedger
            //        {
            //            ID = Guid.NewGuid(),
            //            BranchID = null,
            //            ReferenceID = temp.ID,
            //            TypeID = temp.TypeID,
            //            Date = temp.Date,
            //            PostedDate = temp.PostedDate,
            //            No = temp.No,
            //            Account = lstDecrementDetails[i].DebitAccount,
            //            AccountCorresponding = lstDecrementDetails[i].CreditAccount,
            //            CurrencyID = temp.CurrencyID,
            //            ExchangeRate = temp.ExchangeRate,
            //            CreditAmount = lstDecrementDetails[i].Amount,
            //            CreditAmountOriginal = lstDecrementDetails[i].AmountOriginal,
            //            DetailID = lstDecrementDetails[i].ID,
            //            Reason = temp.Reason,
            //            Description = lstDecrementDetails[i].Description,
            //            AccountingObjectID = lstDecrementDetails[i].AccountingObjectID,
            //            EmployeeID = lstDecrementDetails[i].AccountingObjectID,
            //            BudgetItemID = lstDecrementDetails[i].BudgetItemID,
            //            CostSetID = lstDecrementDetails[i].CostSetID,
            //            ContractID = lstDecrementDetails[i].ContractID,
            //            StatisticsCodeID = lstDecrementDetails[i].StatisticsCodeID,
            //            RefNo = temp.No,
            //            RefDate = temp.Date,
            //            DepartmentID = lstDecrementDetails[i].DepartmentID,
            //            ExpenseItemID = lstDecrementDetails[i].ExpenseItemID,
            //            IsIrrationalCost = lstDecrementDetails[i].IsIrrationalCost
            //        };
            //        if (((lstDecrementDetails[i].CreditAccount.StartsWith("133")) || lstDecrementDetails[i].CreditAccount.StartsWith("33311")))
            //        {
            //            genTempCorresponding.VATDescription = lstDecrementDetails[i].Description;
            //        }
            //        listGenTemp.Add(genTempCorresponding);
            //        _ifaDecrementService.BeginTran();
            //        foreach (var generalLedger in listGenTemp)
            //        {
            //            _IGeneralLedgerService.CreateNew(generalLedger);
            //        }
            //        temp.Recorded = true;
            //        _ifaDecrementService.Update(temp);
            //        _ifaDecrementService.CommitTran();
            //    }
            //}
            //catch (Exception)
            //{

            //    _ifaDecrementService.RolbackTran();
            //}
        }
        private void EditFunction()
        {
            if (ugridTS.Selected.Rows.Count > 0)
            {
                FADecrement temp = ugridTS.Selected.Rows[0].ListObject as FADecrement;
                new FFADecrementDetail(temp, lstDecrements, ConstFrm.optStatusForm.View).ShowDialog(this);
               if (!FFADecrementDetail.IsClose) loadding();
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }

        void RemoveLeged()
        {
            FADecrement _select = ugridTS.Selected.Rows[0].ListObject as FADecrement;

            try
            {
                _ifaDecrementService.BeginTran();
                //xoa du lieu vao cac bang so cai 
                _select.Recorded = false;
                List<GeneralLedger> lstGeneralLedgers = _IGeneralLedgerService.GetListByReferenceID(_select.ID);
                List<FixedAssetLedger> lstFixedAssetLedgers = _IFixedAssetLedgerService.GetByReferenceID(_select.ID);
                foreach (var fixedAssetLedger in lstFixedAssetLedgers)
                {
                    _IFixedAssetLedgerService.Delete(fixedAssetLedger);
                }
                foreach (var generalLedger in lstGeneralLedgers)
                {
                    _IGeneralLedgerService.Delete(generalLedger);
                }
                _ifaDecrementService.Update(_select);
                _ifaDecrementService.CommitTran();

            }

            catch (Exceptions)
            {
                _ifaDecrementService.RolbackTran();
            }
        }
        private void AddFunction()
        {
            var temp = ugridTS.Selected.Rows.Count <= 0 ? new FADecrement() : ugridTS.Selected.Rows[0].ListObject as FADecrement;
            new FFADecrementDetail(temp, lstDecrements, ConstFrm.optStatusForm.Add).ShowDialog(this);
          if (!FFADecrementDetail.IsClose) loadding();

        }

        public void DisColums(UltraGridColumn eUltraGrid)
        {
            eUltraGrid.CellActivation = Activation.NoEdit;
            eUltraGrid.CellClickAction = CellClickAction.RowSelect;
        }


        #endregion

    }

}
