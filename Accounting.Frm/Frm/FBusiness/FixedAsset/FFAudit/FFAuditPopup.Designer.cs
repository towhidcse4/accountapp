﻿namespace Accounting
{
    partial class FFAAuditPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            this.lblMonth = new Infragistics.Win.Misc.UltraLabel();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnApply = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.dtpInventory = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpInventory)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMonth
            // 
            appearance1.TextVAlignAsString = "Middle";
            this.lblMonth.Appearance = appearance1;
            this.lblMonth.Location = new System.Drawing.Point(6, 19);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(106, 23);
            this.lblMonth.TabIndex = 0;
            this.lblMonth.Text = "Kiểm kê đến ngày:";
            // 
            // btnClose
            // 
            appearance2.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance2;
            this.btnClose.Location = new System.Drawing.Point(202, 80);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 340;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnApply
            // 
            appearance3.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnApply.Appearance = appearance3;
            this.btnApply.Location = new System.Drawing.Point(121, 80);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 30);
            this.btnApply.TabIndex = 341;
            this.btnApply.Text = "Đồng ý";
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.dtpInventory);
            this.ultraGroupBox1.Controls.Add(this.lblMonth);
            this.ultraGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(265, 59);
            this.ultraGroupBox1.TabIndex = 345;
            // 
            // dtpInventory
            // 
            appearance4.TextVAlignAsString = "Middle";
            this.dtpInventory.Appearance = appearance4;
            this.dtpInventory.AutoSize = false;
            this.dtpInventory.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dtpInventory.Location = new System.Drawing.Point(118, 20);
            this.dtpInventory.MaskInput = "dd/mm/yyyy";
            this.dtpInventory.Name = "dtpInventory";
            this.dtpInventory.Size = new System.Drawing.Size(143, 22);
            this.dtpInventory.TabIndex = 3021;
            // 
            // FFAAuditPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 117);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnApply);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FFAAuditPopup";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kiểm kê tài sản cố định";
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtpInventory)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel lblMonth;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnApply;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtpInventory;
    }
}