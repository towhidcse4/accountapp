﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FFAAuditDetail : FFAAuditBase
    {
        #region khai bao
        readonly Dictionary<int, Dictionary<string, List<Account>>> _dsAccountDefault = new Dictionary<int, Dictionary<string, List<Account>>>();
        private readonly IFAAuditDetailService fAAuditDetailService = Utils.IFAAuditDetailService;
        private UltraGrid uGridDS;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        UltraGrid ugrid2 = null; private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        //Tập các dữ liệu cần dùng
        #endregion
        public FFAAuditDetail(FAAudit temp, List<FAAudit> dsAudits, int statusForm)
        {
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                WaitingFrm.StopWaiting();
                var frm = new FFAAuditPopup(temp, dsAudits, statusForm);
                if (frm.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
                {
                    base.Close();
                    return;
                }
                WaitingFrm.StartWaiting();
                statusForm = frm._status;
                if (statusForm == ConstFrm.optStatusForm.View)
                    temp = dsAudits.FirstOrDefault(k => k.ID == frm.FAAudit.ID);
                else temp.PostedDate = frm.FAAudit.PostedDate;
            }
            #region Khởi tạo giá trị mặc định của Form
            //base.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            //this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            InitializeComponent();
            base.InitializeComponent1();
            #endregion

            #region Thiết lập ban đầu cho Form
            _statusForm = statusForm;
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                temp.TypeID = 560;
                _select = temp;
            }
            else
            {
                _select = temp;
            }

            _listSelects.AddRange(dsAudits);

            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, dsAudits, statusForm);


            //if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            //{
            //    this.WindowState = FormWindowState.Normal;
            //    this.StartPosition = FormStartPosition.CenterParent;
            //    //this.StartPosition = FormStartPosition.CenterParent;
            //    this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
            //    this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
            //}
            //else
            //{
            //    base.WindowState = FormWindowState.Maximized;
            //    this.WindowState = FormWindowState.Maximized;
            //    //base.StartPosition = FormStartPosition.CenterParent;
            //    //this.StartPosition = FormStartPosition.CenterParent;
            //}
            #endregion

            WaitingFrm.StopWaiting();
        }

        public override void ShowPopup(FAAudit input, int statusForm)
        {
            WaitingFrm.StopWaiting();
            var frm = new FFAAuditPopup(input, _listSelects, statusForm);
            if (frm.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }
            WaitingFrm.StartWaiting();
            statusForm = frm._status;
            if (statusForm == ConstFrm.optStatusForm.View)
                input = _listSelects.FirstOrDefault(k => k.ID == frm.FAAudit.ID);
            else
            {
                input = frm.FAAudit;
            }

            _statusForm = statusForm;
            _select = input;

            InitializeGUI(_select);
        }
        public override void InitializeGUI(FAAudit input)
        {
            if (input.FAAuditDetails.Count == 0) input.FAAuditDetails = new List<FAAuditDetail>();

            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)

            BindingList<FAAuditDetail> dsAuditDetails = new BindingList<FAAuditDetail>();
            BindingList<FAAuditMemberDetail> dsAuditDetailGroup = new BindingList<FAAuditMemberDetail>();
            Template mauGiaoDien = Utils.GetTemplateUIfromDatabase(input.TypeID,
                _statusForm == ConstFrm.optStatusForm.Add, input.TemplateID);
            _select.TemplateID = _statusForm == 2 ? mauGiaoDien.ID : input.TemplateID;
            if (_statusForm == ConstFrm.optStatusForm.Add) _select.Description = string.Format("Kiểm kê đến ngày {0}", _select.InventoryDate.ToString("dd/MM/yyyy"));
            #endregion
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {

                dsAuditDetails = new BindingList<FAAuditDetail>(fAAuditDetailService.FindAllFaInitAndIncrementFixedAsset(_select.InventoryDate, _select.InventoryDate));
            }
            else
            {
                bdlRefVoucher = new BindingList<RefVoucher>(input.RefVouchers);
                dsAuditDetails = new BindingList<FAAuditDetail>(input.FAAuditDetails);
                dsAuditDetailGroup = new BindingList<FAAuditMemberDetail>(input.FAAuditMemberDetails);
            }

            _listObjectInput = new BindingList<System.Collections.IList> { dsAuditDetails };
            _listObjectInputGroup = new BindingList<System.Collections.IList> { dsAuditDetailGroup };
            _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
            this.ConfigGridByTemplete_General<FAAudit>(pnlUgrid, mauGiaoDien);
            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInputGroup, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { false, true, false };
            this.ConfigGridByManyTemplete<FAAudit>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
            ugrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
            ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            uGridDS = (UltraGrid)this.Controls.Find("uGrid0", true).FirstOrDefault();

            this.ConfigTopVouchersNo<FAAudit>(pnlDateNo, "No", "PostedDate", "Date", _select.InventoryDate);
            DataBinding(new List<System.Windows.Forms.Control> { txtDG }, "Description");
            DataBinding(new List<System.Windows.Forms.Control> { ultraTextEditor1 }, "Summary");
            // Add by Hautv
            Control dteP = this.Controls.Find("dtePostedDate", true).First();
            Control lbeP = this.Controls.Find("lblPostedDate", true).First();
            if (dteP != null && lbeP != null)
            {
                dteP.Visible = false;
                lbeP.Visible = false;
            }
            /*---------------------------------------*/
            if (_statusForm == ConstFrm.optStatusForm.Add)
                _select.ID = Guid.NewGuid();
        }

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucher>();
                    var f = new FViewVoucherOriginal("VND", datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
               
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }
        private void FFAAuditDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }
    public class FFAAuditBase : DetailBase<FAAudit>
    {
        public IGenCodeService IgenCodeService { get { return IoC.Resolve<IGenCodeService>(); } }
    }



}
