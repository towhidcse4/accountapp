﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;


namespace Accounting.Frm
{
    public partial class FFAAudit : CustormForm
    {
        #region khai bao

        private readonly IFAAuditService _IFAAuditService = Utils.IFAAuditService;
        private readonly IFAAuditDetailService _IFAAuditDetailService = Utils.IFAAuditDetailService;
        private readonly IGeneralLedgerService _iGeneralLedgerService = Utils.IGeneralLedgerService;
        private List<FAAudit> _dsFAAudits = new List<FAAudit>();
        private readonly FAAudit _temp;
        List<FAAuditDetail> _dsAuditDetails = new List<FAAuditDetail>();
        public static Dictionary<string, Template> DsTemplate = new Dictionary<string, Template>();
        #endregion
        #region khoi tao
        public FFAAudit()
        {

            InitializeComponent();

            _iGeneralLedgerService = IoC.Resolve<IGeneralLedgerService>();
            _IFAAuditDetailService = IoC.Resolve<IFAAuditDetailService>();
            LoadDuLieu();
            //ViewGridDS();
            //ViewGridCT();

        }
        #endregion

        private void LoadDuLieu(bool config = true)
        {
            WaitingFrm.StartWaiting();
            #region lay du lieu tu co so du lieu
            _dsFAAudits = _IFAAuditService.GetAll();
            //if (config)
            //{
            //    _IFAAuditService.UnbindSession(_dsFAAudits);
            //    _dsFAAudits = _IFAAuditService.GetAll();
            //}
            uGridDS.DataSource = _dsFAAudits;
            if (config) ConfigGrid(uGridDS);
            // tu chon select dong dau
            if (uGridDS.Rows.Count > 0) uGridDS.Rows[0].Selected = true;
            uGridCT.DisplayLayout.Override.CellClickAction = CellClickAction.RowSelect;

            #endregion
            WaitingFrm.StopWaiting();
        }
        #region view grid
        void ViewGridDS()
        {
            //uGridDS.DataSource = _dsFAAudits.ToArray();
            //Utils.ConfigGrid(uGridDS,ConstDatabase.FAAudit_TableName);
            uGridDS.DisplayLayout.Bands[0].Columns["PostedDate"].Header.VisiblePosition = 0;
            uGridDS.DisplayLayout.Bands[0].Columns["No"].Header.VisiblePosition = 1;
            uGridDS.DisplayLayout.Bands[0].Columns["Reason"].Header.VisiblePosition = 2;
            uGridDS.DisplayLayout.Bands[0].Columns["TotalAmountOriginal"].Header.VisiblePosition = 3;
            UltraGridBand band = uGridDS.DisplayLayout.Bands[0];
            band.Summaries.Clear();
            SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["PostedDate"]);
            summary.DisplayFormat = "Số dòng = {0:N0}";
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            uGridDS.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False; //ẩn
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            uGridDS.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            uGridDS.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
            uGridDS.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

            summary = band.Summaries.Add("SumMonthAmount", SummaryType.Sum, band.Columns["TotalAmountOriginal"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            band.Columns["TotalAmountOriginal"].MaskInput = "nn,nnn,nnn,nnn,nnn,nnn,nnn,nnn";
            band.Columns["TotalAmountOriginal"].Format = "#,###,###,##đ";
            band.Columns["TotalAmountOriginal"].CellAppearance.TextHAlign = HAlign.Right;

        }

        //void ViewGridCT()
        //{
        //    uGridCT.DataSource = _dsAuditDetails.ToArray();
        //    Utils.ConfigGrid(uGridCT, ConstDatabase.FAAuditDetail_TableName);
        //    uGridCT.DisplayLayout.Bands[0].Columns["FixedAssetID"].Header.VisiblePosition = 0;
        //    uGridCT.DisplayLayout.Bands[0].Columns["FixedAssetName"].Header.VisiblePosition = 1;
        //    uGridCT.DisplayLayout.Bands[0].Columns["ExpenseItemID"].Header.VisiblePosition = 2;
        //    uGridCT.DisplayLayout.Bands[0].Columns["BudgetItemID"].Header.VisiblePosition = 3;
        //    uGridCT.DisplayLayout.Bands[0].Columns["DepartmentID"].Header.VisiblePosition = 4;
        //    uGridCT.DisplayLayout.Bands[0].Columns["OrgPriceOriginal"].Header.VisiblePosition = 5;
        //    uGridCT.DisplayLayout.Bands[0].Columns["AmountOriginal"].Header.VisiblePosition = 6;
        //    uGridCT.DisplayLayout.Bands[0].Columns["EachMonthAuditRate"].Header.VisiblePosition = 7;
        //    uGridCT.DisplayLayout.Bands[0].Columns["MonthlyAuditAmountOriginal"].Header.VisiblePosition = 8;
        //    UltraGridBand band = uGridCT.DisplayLayout.Bands[0];
        //    band.Summaries.Clear();

        //    SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["FixedAssetID"]);
        //    summary.DisplayFormat = "Số dòng = {0:N0}";
        //    summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
        //    uGridCT.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False; //ẩn
        //    summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
        //    uGridCT.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
        //    uGridCT.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
        //    uGridCT.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
        //    summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

        //    summary = band.Summaries.Add("SumMonthAmount", SummaryType.Sum, band.Columns["MonthlyAuditAmountOriginal"]);
        //    summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
        //    summary.DisplayFormat = "{0:N0}";
        //    summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

        //    summary = band.Summaries.Add("OrgPriceOriginal", SummaryType.Sum, band.Columns["OrgPriceOriginal"]);
        //    summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
        //    summary.DisplayFormat = "{0:N0}";
        //    summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

        //    summary = band.Summaries.Add("SumAmount", SummaryType.Sum, band.Columns["AmountOriginal"]);
        //    summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
        //    summary.DisplayFormat = "{0:N0}";
        //    summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

        //    band.Columns["OrgPriceOriginal"].MaskInput = "nn,nnn,nnn,nnn,nnn,nnn,nnn,nnn";
        //    band.Columns["OrgPriceOriginal"].Format = "#,###,###,##đ";
        //    band.Columns["OrgPriceOriginal"].CellAppearance.TextHAlign = HAlign.Right;

        //    band.Columns["AmountOriginal"].MaskInput = "nn,nnn,nnn,nnn,nnn,nnn,nnn,nnn";
        //    band.Columns["AmountOriginal"].Format = "#,###,###,##đ";
        //    band.Columns["AmountOriginal"].CellAppearance.TextHAlign = HAlign.Right;

        //    band.Columns["MonthlyAuditAmountOriginal"].MaskInput = "nn,nnn,nnn,nnn,nnn,nnn,nnn,nnn";
        //    band.Columns["MonthlyAuditAmountOriginal"].Format = "#,###,###,##đ";
        //    band.Columns["MonthlyAuditAmountOriginal"].CellAppearance.TextHAlign = HAlign.Right;

        //}
        //#endregion

        //#region Function

        void AddFunction()
        {
            FAAudit temp = uGridDS.Selected.Rows.Count <= 0 ? new FAAudit() : uGridDS.Selected.Rows[0].ListObject as FAAudit;
            new FFAAuditPopup(temp, _dsFAAudits, ConstFrm.optStatusForm.Add).ShowDialog(this);
            if (!FFAAuditDetail.IsClose) LoadDuLieu();
        }

        void EditFunction()
        {
            if (uGridDS.Selected.Rows.Count > 0)
            {
                WaitingFrm.StartWaiting();
                FAAudit temp = uGridDS.Selected.Rows[0].ListObject as FAAudit;
                new FFAAuditDetail(temp, _dsFAAudits, ConstFrm.optStatusForm.View).ShowDialog(this);
                if (!FFAAuditDetail.IsClose) LoadDuLieu();
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }

        void DeleteFunction()
        {
            if (uGridDS.Selected.Rows.Count > 0)
            {
                FAAudit temp = uGridDS.Selected.Rows[0].ListObject as FAAudit;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, temp.No)) == DialogResult.Yes)
                {
                    _IFAAuditService.BeginTran();
                    //List<GeneralLedger> listGenTemp = _iGeneralLedgerService.Query.Where(p => p.ReferenceID == temp.ID).ToList();
                    List<GeneralLedger> listGenTemp = _iGeneralLedgerService.GetByReferenceID(temp.ID);
                    foreach (var generalLedger in listGenTemp)
                    {
                        _iGeneralLedgerService.Delete(generalLedger);
                    }
                    _IFAAuditService.Delete(temp);
                    _IFAAuditService.CommitTran();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }

        //#endregion
        //#region button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
                EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
                DeleteFunction();
        }
        //#endregion
        //#region utils
        //public static Template GetMauGiaoDien(int typeId, Guid? templateId, Dictionary<string, Template> dsTemplate)    //out string keyofdsTemplate
        //{
        //    string keyofdsTemplate = string.Format("{0}@{1}", typeId, templateId);
        //    if (!dsTemplate.Keys.Contains(keyofdsTemplate))
        //    {
        //        //Note: các chứng từ được sinh tự động từ nghiệp vụ khác thì phải truyền TypeID của nghiệp vụ khác đó
        //        int typeIdTemp = GetTypeIdRef(typeId);
        //        Template template = Utils.GetTemplateInDatabase(templateId, typeIdTemp);
        //        dsTemplate.Add(keyofdsTemplate, template);
        //        return template;
        //    }
        //    //keyofdsTemplate = string.Empty;
        //    return dsTemplate[keyofdsTemplate];
        //}

        //static int GetTypeIdRef(int typeId)
        //{
        //    return typeId;
        //}
        void ConfigGrid(UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(uGridDS, ConstDatabase.FAAudit_TableName);

            utralGrid.DisplayLayout.Bands[0].Columns["TotalAmountOriginal"].CellAppearance.TextHAlign = HAlign.Center;
        }


        //#endregion
        //#region event
        private void uGridDS_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            //    if (uGridDS.Selected.Rows.Count < 1) return;
            //    object listObject = uGridDS.Selected.Rows[0].ListObject;
            //    if (listObject == null) return;
            //    FAAudit dsAudit = listObject as FAAudit;
            //    if (dsAudit == null) return;

            //    Template mauGiaoDien = GetMauGiaoDien(dsAudit.TypeID, dsAudit.TemplateID, DsTemplate);
            //    Utils.ConfigGrid(uGridCT, ConstDatabase.MCPaymentDetail_TableName, mauGiaoDien == null ? new List<TemplateColumn>() : mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 0).TemplateColumns, 0);
        }
        #endregion
    }
}
