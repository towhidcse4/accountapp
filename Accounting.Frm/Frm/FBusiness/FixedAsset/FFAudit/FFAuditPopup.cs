﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinSchedule;

namespace Accounting
{


    public partial class FFAAuditPopup : CustormForm
    {
        readonly List<FAAudit> _dsFAAudits;
        public FAAudit FAAudit;
        public int _status;
        public FFAAuditPopup(FAAudit temp, List<FAAudit> dsFAAudits, int statusForm)
        {
            FAAudit = temp;
            _dsFAAudits = dsFAAudits;
            _status = statusForm;
            InitializeComponent();

        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            FAAudit.InventoryDate = dtpInventory.DateTime;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }




    }
}
