﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Frm.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;


namespace Accounting.Frm
{
    public partial class FFADepreciation : CustormForm
    {
        #region khai bao

        private readonly IFADepreciationService _IFADepreciationService = Utils.IFADepreciationService;
        private readonly IFADepreciationDetailService _IFADepreciationDetailService = Utils.IFADepreciationDetailService;
        private readonly IGeneralLedgerService _iGeneralLedgerService = Utils.IGeneralLedgerService;
        private List<FADepreciation> _dsFADepreciations = new List<FADepreciation>();
        private readonly FADepreciation _temp;
        List<FADepreciationDetail> _dsDepreciationDetails = new List<FADepreciationDetail>();
        public static Dictionary<string, Template> DsTemplate = new Dictionary<string, Template>();
        #endregion
        #region khoi tao
        public FFADepreciation()
        {

            InitializeComponent();

            //_IFADepreciationService = IoC.Resolve<IFADepreciationService>();
            //_iGeneralLedgerService = IoC.Resolve<IGeneralLedgerService>();
            //_IFADepreciationDetailService = IoC.Resolve<IFADepreciationDetailService>();
            LoadDuLieu();
            ViewGridDS();
            ViewGridCT();

        }
        #endregion

        private void LoadDuLieu(bool config = true)
        {
            WaitingFrm.StartWaiting();
            #region lay du lieu tu co so du lieu
            _dsFADepreciations = _IFADepreciationService.GetAll_OrderBy();
            if (config)
            {
                _IFADepreciationService.UnbindSession(_dsFADepreciations);
                _dsFADepreciations = _IFADepreciationService.GetAll_OrderBy();
            }
            uGridDS.DataSource = _dsFADepreciations;
            if (config) ConfigGrid(uGridDS);
            // tu chon select dong dau
            if (uGridDS.Rows.Count > 0) uGridDS.Rows[0].Selected = true;
            uGridCT.DisplayLayout.Override.CellClickAction = CellClickAction.RowSelect;

            #endregion
            WaitingFrm.StopWaiting();
        }
        #region view grid
        void ViewGridDS()
        {
            //uGridDS.DataSource = _dsFADepreciations.ToArray();
            //Utils.ConfigGrid(uGridDS,ConstDatabase.FADepreciation_TableName);
            uGridDS.DisplayLayout.Bands[0].Columns["PostedDate"].Header.VisiblePosition = 0;
            uGridDS.DisplayLayout.Bands[0].Columns["No"].Header.VisiblePosition = 1;
            uGridDS.DisplayLayout.Bands[0].Columns["Reason"].Header.VisiblePosition = 2;
            uGridDS.DisplayLayout.Bands[0].Columns["TotalAmountOriginal"].Header.VisiblePosition = 3;
            UltraGridBand band = uGridDS.DisplayLayout.Bands[0];
            band.Summaries.Clear();
            SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["PostedDate"]);
            summary.DisplayFormat = "Số dòng = {0:N0}";
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            uGridDS.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False; //ẩn
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            uGridDS.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            uGridDS.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
            uGridDS.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

            summary = band.Summaries.Add("SumMonthAmount", SummaryType.Sum, band.Columns["TotalAmountOriginal"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            band.Columns["TotalAmountOriginal"].MaskInput = "nn,nnn,nnn,nnn,nnn,nnn,nnn,nnn";
            band.Columns["TotalAmountOriginal"].Format = "#,###,###,##đ";
            band.Columns["TotalAmountOriginal"].CellAppearance.TextHAlign = HAlign.Right;

        }

        void ViewGridCT()
        {
            uGridCT.DataSource = _dsDepreciationDetails.ToArray();
            Utils.ConfigGrid(uGridCT, ConstDatabase.FADepreciationDetail_TableName);
            uGridCT.DisplayLayout.Bands[0].Columns["FixedAssetID"].Header.VisiblePosition = 0;
            uGridCT.DisplayLayout.Bands[0].Columns["FixedAssetName"].Header.VisiblePosition = 1;
            uGridCT.DisplayLayout.Bands[0].Columns["ExpenseItemID"].Header.VisiblePosition = 2;
            uGridCT.DisplayLayout.Bands[0].Columns["BudgetItemID"].Header.VisiblePosition = 3;
            uGridCT.DisplayLayout.Bands[0].Columns["DepartmentID"].Header.VisiblePosition = 4;
            uGridCT.DisplayLayout.Bands[0].Columns["OrgPriceOriginal"].Header.VisiblePosition = 5;
            uGridCT.DisplayLayout.Bands[0].Columns["AmountOriginal"].Header.VisiblePosition = 6;
            uGridCT.DisplayLayout.Bands[0].Columns["EachMonthDepreciationRate"].Header.VisiblePosition = 7;
            uGridCT.DisplayLayout.Bands[0].Columns["MonthlyDepreciationAmountOriginal"].Header.VisiblePosition = 8;
            UltraGridBand band = uGridCT.DisplayLayout.Bands[0];
            band.Summaries.Clear();

            SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["FixedAssetID"]);
            summary.DisplayFormat = "Số dòng = {0:N0}";
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            uGridCT.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False; //ẩn
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            uGridCT.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            uGridCT.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
            uGridCT.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

            summary = band.Summaries.Add("SumMonthAmount", SummaryType.Sum, band.Columns["MonthlyDepreciationAmountOriginal"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

            summary = band.Summaries.Add("OrgPriceOriginal", SummaryType.Sum, band.Columns["OrgPriceOriginal"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

            summary = band.Summaries.Add("SumAmount", SummaryType.Sum, band.Columns["AmountOriginal"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

            band.Columns["OrgPriceOriginal"].MaskInput = "nn,nnn,nnn,nnn,nnn,nnn,nnn,nnn";
            band.Columns["OrgPriceOriginal"].Format = "#,###,###,##đ";
            band.Columns["OrgPriceOriginal"].CellAppearance.TextHAlign = HAlign.Right;

            band.Columns["AmountOriginal"].MaskInput = "nn,nnn,nnn,nnn,nnn,nnn,nnn,nnn";
            band.Columns["AmountOriginal"].Format = "#,###,###,##đ";
            band.Columns["AmountOriginal"].CellAppearance.TextHAlign = HAlign.Right;

            band.Columns["MonthlyDepreciationAmountOriginal"].MaskInput = "nn,nnn,nnn,nnn,nnn,nnn,nnn,nnn";
            band.Columns["MonthlyDepreciationAmountOriginal"].Format = "#,###,###,##đ";
            band.Columns["MonthlyDepreciationAmountOriginal"].CellAppearance.TextHAlign = HAlign.Right;

        }
        #endregion

        #region Function

        void AddFunction()
        {
            FADepreciation temp = uGridDS.Selected.Rows.Count <= 0 ? new FADepreciation() : uGridDS.Selected.Rows[0].ListObject as FADepreciation;
            //new FFADepreciationDetail(temp, _dsFADepreciations, ConstFrm.optStatusForm.Add).ShowDialog(this);
            //if (!FFADepreciationDetail.IsClose) LoadDuLieu();
            new CKTKH(temp, _dsFADepreciations, ConstFrm.optStatusForm.Add).ShowDialog(this);
            if (!FFADepreciationDetail.IsClose) LoadDuLieu();
        }

        void EditFunction()
        {
            if (uGridDS.Selected.Rows.Count > 0)
            {
                WaitingFrm.StartWaiting();
                FADepreciation temp = uGridDS.Selected.Rows[0].ListObject as FADepreciation;
                new FFADepreciationDetail(temp, _dsFADepreciations, ConstFrm.optStatusForm.View, 0, 0).ShowDialog(this);
                if (!FFADepreciationDetail.IsClose) LoadDuLieu();
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }

        void DeleteFunction()
        {
            if (uGridDS.Selected.Rows.Count > 0)
            {
                FADepreciation temp = uGridDS.Selected.Rows[0].ListObject as FADepreciation;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, temp.No)) == DialogResult.Yes)
                {
                    _IFADepreciationService.BeginTran();
                    //List<GeneralLedger> listGenTemp = _iGeneralLedgerService.Query.Where(p => p.ReferenceID == temp.ID).ToList();
                    List<GeneralLedger> listGenTemp = _iGeneralLedgerService.GetByReferenceID(temp.ID);
                    foreach (var generalLedger in listGenTemp)
                    {
                        _iGeneralLedgerService.Delete(generalLedger);
                    }
                    _IFADepreciationService.Delete(temp);
                    _IFADepreciationService.CommitTran();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }

        #endregion
        #region button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion
        #region utils
        public static Template GetMauGiaoDien(int typeId, Guid? templateId, Dictionary<string, Template> dsTemplate)    //out string keyofdsTemplate
        {
            string keyofdsTemplate = string.Format("{0}@{1}", typeId, templateId);
            if (!dsTemplate.Keys.Contains(keyofdsTemplate))
            {
                //Note: các chứng từ được sinh tự động từ nghiệp vụ khác thì phải truyền TypeID của nghiệp vụ khác đó
                int typeIdTemp = GetTypeIdRef(typeId);
                Template template = Utils.GetTemplateInDatabase(templateId, typeIdTemp);
                dsTemplate.Add(keyofdsTemplate, template);
                return template;
            }
            //keyofdsTemplate = string.Empty;
            return dsTemplate[keyofdsTemplate];
        }

        static int GetTypeIdRef(int typeId)
        {
            return typeId;
        }
        void ConfigGrid(UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(uGridDS, ConstDatabase.FADepreciation_TableName);

            utralGrid.DisplayLayout.Bands[0].Columns["TotalAmountOriginal"].CellAppearance.TextHAlign = HAlign.Center;
        }


        #endregion
        #region event
        private void uGridDS_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGridDS.Selected.Rows.Count < 1) return;
            object listObject = uGridDS.Selected.Rows[0].ListObject;
            if (listObject == null) return;
            FADepreciation dsDepreciation = listObject as FADepreciation;
            if (dsDepreciation == null) return;

            Template mauGiaoDien = GetMauGiaoDien(dsDepreciation.TypeID, dsDepreciation.TemplateID, DsTemplate);
            Utils.ConfigGrid(uGridCT, ConstDatabase.MCPaymentDetail_TableName, mauGiaoDien == null ? new List<TemplateColumn>() : mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 0).TemplateColumns, 0);
        }
        #endregion
    }
}
