﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Itenso.TimePeriod;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FFADepreciationDetail : FFADepreciationBase
    {
        #region khai bao
        readonly Dictionary<int, Dictionary<string, List<Account>>> _dsAccountDefault = new Dictionary<int, Dictionary<string, List<Account>>>();
        private UltraGrid uGridDS;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        UltraGrid ugrid2 = null;
        List<FADepreciationDetail> lstGroup = new List<FADepreciationDetail>();
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        //Tập các dữ liệu cần dùng
        #endregion
        public FFADepreciationDetail(FADepreciation temp, List<FADepreciation> dsDepreciations, int statusForm)
        {
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                WaitingFrm.StopWaiting();
                var frm = new CKTKH(temp, dsDepreciations, statusForm);
                if (frm.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
                {
                    base.Close();
                    return;
                }
                WaitingFrm.StartWaiting();
                statusForm = frm._status;
                if (statusForm == ConstFrm.optStatusForm.View)
                    temp = dsDepreciations.FirstOrDefault(k => k.ID == frm.FADepreciation.ID);
                else temp.PostedDate = frm.FADepreciation.PostedDate;
            }
            #region Khởi tạo giá trị mặc định của Form
            //base.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            //this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            InitializeComponent();
            base.InitializeComponent1();
            #endregion

            #region Thiết lập ban đầu cho Form
            _statusForm = statusForm;
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                temp.TypeID = 540;
                _select = temp;
                //_select.PostedDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));
            }
            else
            {
                _select = temp;
            }

            _listSelects.AddRange(dsDepreciations);

            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, dsDepreciations, statusForm);

            #endregion
            Utils.ClearCacheByType<SystemOption>();

             
            WaitingFrm.StopWaiting();
        }

        public override void ShowPopup(FADepreciation input, int statusForm)
        {
            WaitingFrm.StopWaiting();
            var frm = new CKTKH(input, _listSelects, statusForm);
            if (frm.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }
            WaitingFrm.StartWaiting();
            statusForm = frm._status;
            if (statusForm == ConstFrm.optStatusForm.View)
                input = _listSelects.FirstOrDefault(k => k.ID == frm.FADepreciation.ID);
            else
            {
                input = frm.FADepreciation;
            }

            _statusForm = statusForm;
            _select = input;

            InitializeGUI(_select);
        }
        #region override

        public override void InitializeGUI(FADepreciation input)
        {
            if (input.FADepreciationDetails.Count == 0) input.FADepreciationDetails = new List<FADepreciationDetail>();

            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)

            BindingList<FADepreciationDetail> dsDepreciationDetails = new BindingList<FADepreciationDetail>();
            BindingList<FADepreciationAllocation> dsFADepreciationAllocations = new BindingList<FADepreciationAllocation>();
            BindingList<FADepreciationPost> dsFADepreciationPosts = new BindingList<FADepreciationPost>();
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            Template mauGiaoDien = Utils.GetTemplateUIfromDatabase(input.TypeID,
                _statusForm == ConstFrm.optStatusForm.Add, input.TemplateID);
            _select.TemplateID = _statusForm == 2 ? mauGiaoDien.ID : input.TemplateID;
            if (_statusForm == ConstFrm.optStatusForm.Add) _select.Reason = string.Format("Khấu hao TSCĐ tháng {0} năm {1}", _select.Month, _select.Year);
            ConstFrm.TTH = _select.PostedDate.ToString("dd/MM/yyyy");
            #endregion
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
               
                var lst = new List<FADepreciationDetail>();
                var lstallo = new List<FADepreciationAllocation>();
                var lstPost = new List<FADepreciationPost>();
                foreach (var fixedAsset in IFixedAssetService.GetFAForDepreciation(_select.PostedDate))
                {
                    FixedAssetLedger FixedAssetLedger = Utils.IGeneralLedgerService.GetLastFixedAssetLedger(fixedAsset.ID, _select.PostedDate);
                    var detail = new FADepreciationDetail
                    {
                        FixedAssetID = fixedAsset.ID,
                        FixedAssetName = fixedAsset.FixedAssetName,
                        ObjectID = FixedAssetLedger.DepartmentID,
                        CurrencyID = "VND",
                        DebitAccount = fixedAsset.ExpenditureAccount,
                        CreditAccount = fixedAsset.DepreciationAccount,
                        Reason = _select.Reason,
                    };
                    var allo = new FADepreciationAllocation
                    {
                        FixedAssetID = fixedAsset.ID,
                        ObjectID = fixedAsset.DepartmentID,
                        Rate = 100,
                        AccountNumber = fixedAsset.ExpenditureAccount,
                        //BudgetItemID = fixedAsset.BudgetItemID,
                        //StatisticsCodeID = fixedAsset.StatisticsCodeID
                    };
                    var post = new FADepreciationPost
                    {
                        FixedAssetID = fixedAsset.ID,
                        FixedAssetName = fixedAsset.FixedAssetName,
                        ObjectID = FixedAssetLedger.DepartmentID,
                        DebitAccount = fixedAsset.ExpenditureAccount,
                        CreditAccount = fixedAsset.DepreciationAccount,
                        Description = _select.Reason,
                        BudgetItemID = fixedAsset.BudgetItemID,
                        //CostSetID = fixedAsset.CostSetID,
                        StatisticsCodeID = fixedAsset.StatisticsCodeID
                    };
                    if (fixedAsset.IsDepreciationAllocation || fixedAsset.No == "OPN")
                    {
                        post.CostSetID = fixedAsset.CostSetID;
                        allo.CostSetID = fixedAsset.CostSetID;
                    }
                    var faFixedAssetCategory = IFixedAssetCategoryService.Query.Where(fads => fads.ID == fixedAsset.FixedAssetCategoryID).ToList().LastOrDefault();
                    if (faFixedAssetCategory != null)
                    {
                        detail.FixedAssetCategory = faFixedAssetCategory;
                        detail.FixedAssetCategoryID = faFixedAssetCategory.ID;
                    }
                    //var faAdjustment = IFAAdjustmentService.Query.Where(f => f.FixedAssetID == fixedAsset.ID).OrderBy(f => f.PostedDate).ThenBy(f => f.No).ToList().LastOrDefault();
                    //if (faAdjustment != null)
                    //{
                    detail.OrgPrice = FixedAssetLedger.OriginalPrice ?? 0;
                    detail.OrgPriceOriginal = FixedAssetLedger.OriginalPrice ?? 0;
                    detail.EachYearDepreciationAmount = FixedAssetLedger.MonthPeriodDepreciationAmount * 12 ?? 0;
                    detail.EachYearDepreciationAmountOriginal = FixedAssetLedger.MonthPeriodDepreciationAmount * 12 ?? 0;
                    detail.EachMonthDepreciationRate = FixedAssetLedger.MonthDepreciationRate ?? 0;
                    detail.Amount = FixedAssetLedger.DepreciationAmount ?? 0;
                    detail.AmountOriginal = FixedAssetLedger.DepreciationAmount ?? 0;
                    decimal monthDepreciationAmount = Utils.IFixedAssetLedgerService.FindmonthDepreciationAmount(fixedAsset.ID, _select.PostedDate);
                    post.Amount = post.AmountOriginal = detail.TotalOrgPrice = detail.TotalOrgPriceOriginal = detail.MonthlyDepreciationAmount = detail.MonthlyDepreciationAmountOriginal = Math.Round(this.GetMonthlyDepreciationAmount(fixedAsset.ID, fixedAsset.DepreciationDate.GetValueOrDefault(), FixedAssetLedger.DepreciationAmount.GetValueOrDefault(), monthDepreciationAmount, FixedAssetLedger.RemainingAmount.GetValueOrDefault(), FixedAssetLedger.UsedTimeRemain.GetValueOrDefault()));
                    allo.Amount = detail.AllocationAmountOriginal = detail.AllocationAmount = detail.MonthlyDepreciationAmountOriginal;

                    lst.Add(detail);
                    lstallo.Add(allo);
                    lstPost.Add(post);
                }
                dsDepreciationDetails = new BindingList<FADepreciationDetail>(lst);
                dsFADepreciationAllocations = new BindingList<FADepreciationAllocation>(lstallo);
                dsFADepreciationPosts = new BindingList<FADepreciationPost>(lstPost);
            }
            else
            {
                dsDepreciationDetails = new BindingList<FADepreciationDetail>(input.FADepreciationDetails.OrderBy(x=>x.OrderPriority).ToList());
                dsFADepreciationAllocations = new BindingList<FADepreciationAllocation>(_select.FADepreciationAllocations.OrderBy(x => x.OrderPriority).ToList());
                dsFADepreciationPosts = new BindingList<FADepreciationPost>(_select.FADepreciationPosts.OrderBy(x => x.OrderPriority).ToList());
                bdlRefVoucher = new BindingList<RefVoucher>(input.RefVouchers);
            }

            _listObjectInput = new BindingList<System.Collections.IList> { dsDepreciationDetails };
            _listObjectInputGroup = new BindingList<System.Collections.IList> { dsFADepreciationAllocations };
            _listObjectInputPost = new BindingList<System.Collections.IList> { dsFADepreciationPosts, bdlRefVoucher };
            this.ConfigGridByTemplete_General<FADepreciation>(pnlUgrid, mauGiaoDien);

            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInputGroup, _listObjectInputPost, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { false, false, false, false };
            this.ConfigGridByManyTemplete<FADepreciation>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard);
            uGridDS = (UltraGrid)this.Controls.Find("uGrid0", true).FirstOrDefault();
            ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
            ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            this.ConfigTopVouchersNo<FADepreciation>(pnlDateNo, "No", "PostedDate", "Date", null, true);
            if (_statusForm == ConstFrm.optStatusForm.Add) input.Date = input.PostedDate;
            DataBinding(new List<System.Windows.Forms.Control> { txtDG }, "Reason");
            if(_statusForm == ConstFrm.optStatusForm.Add)
                _select.ID = Guid.NewGuid();
        }

        public decimal GetMonthlyDepreciationAmount(Guid ID, DateTime startDate, decimal DepreciationAmount, decimal MonthDepreciationAmount, decimal remainAmount, decimal usedTimeRemain)
        {
            if (remainAmount == 0)
                return remainAmount;
            if (usedTimeRemain == 1)
            {
                return remainAmount;
            }
            // Kiểm tra xem đã ghi giảm trong tháng này chưa, nếu đã ghi giảm, chỉ tính số tiền khấu hao từ đầu tháng
            // đến lúc ghi giảm TSCD
            DateTime? decrementDate = IFADecrementDetailService.FindDecrementDate(ID, _select.PostedDate);
            DateTime date = decrementDate ?? _select.PostedDate;
            if (startDate.Month == _select.PostedDate.Month && startDate.Year == _select.PostedDate.Year)
            {
                // Nếu không phải ngày đầu của tháng
                if (startDate.Day != 1)
                {
                    var dteDiff = new DateDiff(startDate, _select.PostedDate);
                    return MonthDepreciationAmount * (dteDiff.Days + 1) / DateTime.DaysInMonth(_select.PostedDate.Year, _select.PostedDate.Month);
                }
            }
            if (decrementDate != null)
            {
                return MonthDepreciationAmount * (date.Day - 1) / DateTime.DaysInMonth(date.Year, date.Month);
            }
            return MonthDepreciationAmount;
        }

        #endregion

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucher>();
                    var f = new FViewVoucherOriginal("VND", datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void FFADepreciationDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }
    public class FFADepreciationBase : DetailBase<FADepreciation>
    {
        public IGenCodeService IgenCodeService { get { return IoC.Resolve<IGenCodeService>(); } }
    }



}
