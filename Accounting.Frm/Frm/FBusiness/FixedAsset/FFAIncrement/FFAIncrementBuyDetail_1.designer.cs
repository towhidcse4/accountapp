﻿namespace Accounting.Frm
{
    partial class FFAIncrementBuyDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton6 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance116 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance123 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance127 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance129 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance130 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance131 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance132 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance133 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance134 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance135 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance136 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance137 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance138 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance139 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance140 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance141 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance142 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance143 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance144 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance145 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance146 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance147 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance148 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance149 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton7 = new Infragistics.Win.UltraWinEditors.EditorButton();
            this.ultraPanel_KhungTren = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.rbt_Thanhtoanngay = new System.Windows.Forms.RadioButton();
            this.rbt_Chuathanhtoan = new System.Windows.Forms.RadioButton();
            this.cbbChonThanhToan = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.palTopVouchers = new Infragistics.Win.Misc.UltraPanel();
            this.pl_ThongTinTheoHinhThucTT = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel10 = new Infragistics.Win.Misc.UltraPanel();
            this.pnSCK = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBoxDonViTraTienSCK = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbBankAccountDetailIDSCK = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtBankNameSCK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtReasonSCK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblReasonSCK = new Infragistics.Win.Misc.UltraLabel();
            this.lblBankAccountDetailIDSCK = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraPanel_SCK = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel8 = new Infragistics.Win.Misc.UltraPanel();
            this.txtAccountingObjectNameSCK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraPanel9 = new Infragistics.Win.Misc.UltraPanel();
            this.uButtonCongNoSCK = new Infragistics.Win.Misc.UltraButton();
            this.cbbAccountingObjectBankAccountSCK = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbAccountingObjectIDSCK = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccountingObjectBankNameSCK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectBankAccountSCK = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressSCK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectAddressSCK = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObjectIDSCK = new Infragistics.Win.Misc.UltraLabel();
            this.pnTTD = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBoxHoaDonTTD = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBox1TTD = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraPanel11 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel12 = new Infragistics.Win.Misc.UltraPanel();
            this.txtAccountingObjectNameTTD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraPanel13 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.cbbAccountingObjectBankAccountTTD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbAccountingObjectIDTTD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccountingObjectBankNameTTD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectBankAccountTTD = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressTTD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectAddressTTD = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObjectIDTTD = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBoxDonViTraTienTTD = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbCreditCardNumberTTD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblOwnerCardTTD = new Infragistics.Win.Misc.UltraLabel();
            this.lblCreditCardTypeTTD = new Infragistics.Win.Misc.UltraLabel();
            this.txtReasonTTD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblReasonTTD = new Infragistics.Win.Misc.UltraLabel();
            this.lblBankAccountDetailIDTTD = new Infragistics.Win.Misc.UltraLabel();
            this.grCTT = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraPanel5 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel6 = new Infragistics.Win.Misc.UltraPanel();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraPanel7 = new Infragistics.Win.Misc.UltraPanel();
            this.uButtonCongNo = new Infragistics.Win.Misc.UltraButton();
            this.txtCompanyTaxCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblCompanyTaxCode = new Infragistics.Win.Misc.UltraLabel();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblReason = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectAddress = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObjectID = new Infragistics.Win.Misc.UltraLabel();
            this.pnSTM = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBoxHoaDonSTM = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBox1STM = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtIssueBy = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblIssueBy = new Infragistics.Win.Misc.UltraLabel();
            this.dteIssueDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblIssueDate = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel1STM = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel3STM = new Infragistics.Win.Misc.UltraPanel();
            this.txtAccountingObjectNameSTM = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraPanelSTM2 = new Infragistics.Win.Misc.UltraPanel();
            this.uButtonCongNoSTM = new Infragistics.Win.Misc.UltraButton();
            this.mkeIdentificationNoSTM = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.cbbAccountingObjectIDSTM = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblIdentificationNoSTM = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactNameSTM = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblContactNameSTM = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObjectIDSTM = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBoxDonViTraTienSTM = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbBankAccountDetailIDSTM = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtBankNameSTM = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtReasonSTM = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblReasonSTM = new Infragistics.Win.Misc.UltraLabel();
            this.lblBankAccountDetailIDSTM = new Infragistics.Win.Misc.UltraLabel();
            this.grTM = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraPanel2TM = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel3TM = new Infragistics.Win.Misc.UltraPanel();
            this.txtAccountingObjectNameTM = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraPanel4TM = new Infragistics.Win.Misc.UltraPanel();
            this.uButtonCongNoTM = new Infragistics.Win.Misc.UltraButton();
            this.lblChungTuGocTM = new Infragistics.Win.Misc.UltraLabel();
            this.lblNumberAttachTM = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObjectIDTM = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtCompanyTaxCodeTM = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblCompanyTaxCodeTM = new Infragistics.Win.Misc.UltraLabel();
            this.txtReasonTM = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblReasonTM = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactNameTM = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblContactNameTM = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressTM = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectAddressTM = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObjectIDTM = new Infragistics.Win.Misc.UltraLabel();
            this.txtNumberAttachTM = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraPanel4 = new Infragistics.Win.Misc.UltraPanel();
            this.pltab_chitiet = new Infragistics.Win.Misc.UltraPanel();
            this.pnlGrid = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalDiscountAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalVATAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalDiscountAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalVATAmount = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.palBot = new System.Windows.Forms.Panel();
            this.uGridControl = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cbbAccountingObjectBankAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbBankAccountDetailID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbCreditCardNumber = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraPanel_KhungTren.ClientArea.SuspendLayout();
            this.ultraPanel_KhungTren.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChonThanhToan)).BeginInit();
            this.palTopVouchers.SuspendLayout();
            this.pl_ThongTinTheoHinhThucTT.ClientArea.SuspendLayout();
            this.pl_ThongTinTheoHinhThucTT.SuspendLayout();
            this.ultraPanel10.ClientArea.SuspendLayout();
            this.ultraPanel10.SuspendLayout();
            this.pnSCK.ClientArea.SuspendLayout();
            this.pnSCK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxDonViTraTienSCK)).BeginInit();
            this.ultraGroupBoxDonViTraTienSCK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetailIDSCK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankNameSCK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonSCK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.ultraPanel_SCK.ClientArea.SuspendLayout();
            this.ultraPanel_SCK.SuspendLayout();
            this.ultraPanel8.ClientArea.SuspendLayout();
            this.ultraPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameSCK)).BeginInit();
            this.ultraPanel9.ClientArea.SuspendLayout();
            this.ultraPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccountSCK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDSCK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectBankNameSCK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressSCK)).BeginInit();
            this.pnTTD.ClientArea.SuspendLayout();
            this.pnTTD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxHoaDonTTD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1TTD)).BeginInit();
            this.ultraGroupBox1TTD.SuspendLayout();
            this.ultraPanel11.ClientArea.SuspendLayout();
            this.ultraPanel11.SuspendLayout();
            this.ultraPanel12.ClientArea.SuspendLayout();
            this.ultraPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameTTD)).BeginInit();
            this.ultraPanel13.ClientArea.SuspendLayout();
            this.ultraPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccountTTD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDTTD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectBankNameTTD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressTTD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxDonViTraTienTTD)).BeginInit();
            this.ultraGroupBoxDonViTraTienTTD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardNumberTTD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonTTD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grCTT)).BeginInit();
            this.grCTT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).BeginInit();
            this.ultraPanel5.ClientArea.SuspendLayout();
            this.ultraPanel5.SuspendLayout();
            this.ultraPanel6.ClientArea.SuspendLayout();
            this.ultraPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            this.ultraPanel7.ClientArea.SuspendLayout();
            this.ultraPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).BeginInit();
            this.pnSTM.ClientArea.SuspendLayout();
            this.pnSTM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxHoaDonSTM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1STM)).BeginInit();
            this.ultraGroupBox1STM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteIssueDate)).BeginInit();
            this.ultraPanel1STM.ClientArea.SuspendLayout();
            this.ultraPanel1STM.SuspendLayout();
            this.ultraPanel3STM.ClientArea.SuspendLayout();
            this.ultraPanel3STM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameSTM)).BeginInit();
            this.ultraPanelSTM2.ClientArea.SuspendLayout();
            this.ultraPanelSTM2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDSTM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactNameSTM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxDonViTraTienSTM)).BeginInit();
            this.ultraGroupBoxDonViTraTienSTM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetailIDSTM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankNameSTM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonSTM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grTM)).BeginInit();
            this.grTM.SuspendLayout();
            this.ultraPanel2TM.ClientArea.SuspendLayout();
            this.ultraPanel2TM.SuspendLayout();
            this.ultraPanel3TM.ClientArea.SuspendLayout();
            this.ultraPanel3TM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameTM)).BeginInit();
            this.ultraPanel4TM.ClientArea.SuspendLayout();
            this.ultraPanel4TM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDTM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodeTM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonTM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactNameTM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressTM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberAttachTM)).BeginInit();
            this.ultraPanel4.ClientArea.SuspendLayout();
            this.ultraPanel4.SuspendLayout();
            this.pltab_chitiet.ClientArea.SuspendLayout();
            this.pltab_chitiet.SuspendLayout();
            this.pnlGrid.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            this.palBot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetailID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardNumber)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraPanel_KhungTren
            // 
            // 
            // ultraPanel_KhungTren.ClientArea
            // 
            this.ultraPanel_KhungTren.ClientArea.Controls.Add(this.ultraGroupBox3);
            this.ultraPanel_KhungTren.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel_KhungTren.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel_KhungTren.Name = "ultraPanel_KhungTren";
            this.ultraPanel_KhungTren.Size = new System.Drawing.Size(856, 79);
            this.ultraPanel_KhungTren.TabIndex = 1;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.rbt_Thanhtoanngay);
            this.ultraGroupBox3.Controls.Add(this.rbt_Chuathanhtoan);
            this.ultraGroupBox3.Controls.Add(this.cbbChonThanhToan);
            this.ultraGroupBox3.Controls.Add(this.palTopVouchers);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance2.FontData.BoldAsString = "True";
            appearance2.FontData.SizeInPoints = 13F;
            this.ultraGroupBox3.HeaderAppearance = appearance2;
            this.ultraGroupBox3.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(856, 79);
            this.ultraGroupBox3.TabIndex = 0;
            this.ultraGroupBox3.Text = "Mua tài sản cố định và ghi tăng";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // rbt_Thanhtoanngay
            // 
            this.rbt_Thanhtoanngay.AutoSize = true;
            this.rbt_Thanhtoanngay.BackColor = System.Drawing.Color.Transparent;
            this.rbt_Thanhtoanngay.Location = new System.Drawing.Point(569, 38);
            this.rbt_Thanhtoanngay.Name = "rbt_Thanhtoanngay";
            this.rbt_Thanhtoanngay.Size = new System.Drawing.Size(106, 17);
            this.rbt_Thanhtoanngay.TabIndex = 41;
            this.rbt_Thanhtoanngay.TabStop = true;
            this.rbt_Thanhtoanngay.Text = "Thanh toán ngay";
            this.rbt_Thanhtoanngay.UseVisualStyleBackColor = false;
            // 
            // rbt_Chuathanhtoan
            // 
            this.rbt_Chuathanhtoan.AutoSize = true;
            this.rbt_Chuathanhtoan.BackColor = System.Drawing.Color.Transparent;
            this.rbt_Chuathanhtoan.Location = new System.Drawing.Point(434, 38);
            this.rbt_Chuathanhtoan.Name = "rbt_Chuathanhtoan";
            this.rbt_Chuathanhtoan.Size = new System.Drawing.Size(104, 17);
            this.rbt_Chuathanhtoan.TabIndex = 40;
            this.rbt_Chuathanhtoan.TabStop = true;
            this.rbt_Chuathanhtoan.Text = "Chưa thanh toán";
            this.rbt_Chuathanhtoan.UseVisualStyleBackColor = false;
            // 
            // cbbChonThanhToan
            // 
            this.cbbChonThanhToan.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbChonThanhToan.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbChonThanhToan.Location = new System.Drawing.Point(681, 36);
            this.cbbChonThanhToan.Name = "cbbChonThanhToan";
            this.cbbChonThanhToan.Size = new System.Drawing.Size(165, 21);
            this.cbbChonThanhToan.TabIndex = 39;
            // 
            // palTopVouchers
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.palTopVouchers.Appearance = appearance1;
            this.palTopVouchers.AutoSize = true;
            this.palTopVouchers.Location = new System.Drawing.Point(10, 26);
            this.palTopVouchers.Name = "palTopVouchers";
            this.palTopVouchers.Size = new System.Drawing.Size(403, 48);
            this.palTopVouchers.TabIndex = 37;
            // 
            // pl_ThongTinTheoHinhThucTT
            // 
            // 
            // pl_ThongTinTheoHinhThucTT.ClientArea
            // 
            this.pl_ThongTinTheoHinhThucTT.ClientArea.Controls.Add(this.ultraPanel10);
            this.pl_ThongTinTheoHinhThucTT.Dock = System.Windows.Forms.DockStyle.Top;
            this.pl_ThongTinTheoHinhThucTT.Location = new System.Drawing.Point(0, 0);
            this.pl_ThongTinTheoHinhThucTT.Name = "pl_ThongTinTheoHinhThucTT";
            this.pl_ThongTinTheoHinhThucTT.Size = new System.Drawing.Size(856, 178);
            this.pl_ThongTinTheoHinhThucTT.TabIndex = 2;
            // 
            // ultraPanel10
            // 
            // 
            // ultraPanel10.ClientArea
            // 
            this.ultraPanel10.ClientArea.Controls.Add(this.pnSCK);
            this.ultraPanel10.ClientArea.Controls.Add(this.pnTTD);
            this.ultraPanel10.ClientArea.Controls.Add(this.grCTT);
            this.ultraPanel10.ClientArea.Controls.Add(this.pnSTM);
            this.ultraPanel10.ClientArea.Controls.Add(this.grTM);
            this.ultraPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel10.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel10.Name = "ultraPanel10";
            this.ultraPanel10.Size = new System.Drawing.Size(856, 178);
            this.ultraPanel10.TabIndex = 46;
            // 
            // pnSCK
            // 
            // 
            // pnSCK.ClientArea
            // 
            this.pnSCK.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.pnSCK.ClientArea.Controls.Add(this.ultraGroupBoxDonViTraTienSCK);
            this.pnSCK.ClientArea.Controls.Add(this.ultraGroupBox2);
            this.pnSCK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnSCK.Location = new System.Drawing.Point(0, 0);
            this.pnSCK.Name = "pnSCK";
            this.pnSCK.Size = new System.Drawing.Size(856, 178);
            this.pnSCK.TabIndex = 75;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance3.FontData.BoldAsString = "True";
            appearance3.FontData.SizeInPoints = 10F;
            this.ultraGroupBox1.HeaderAppearance = appearance3;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 73);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(856, 7);
            this.ultraGroupBox1.TabIndex = 72;
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraGroupBoxDonViTraTienSCK
            // 
            this.ultraGroupBoxDonViTraTienSCK.Controls.Add(this.cbbBankAccountDetailIDSCK);
            this.ultraGroupBoxDonViTraTienSCK.Controls.Add(this.txtBankNameSCK);
            this.ultraGroupBoxDonViTraTienSCK.Controls.Add(this.txtReasonSCK);
            this.ultraGroupBoxDonViTraTienSCK.Controls.Add(this.lblReasonSCK);
            this.ultraGroupBoxDonViTraTienSCK.Controls.Add(this.lblBankAccountDetailIDSCK);
            this.ultraGroupBoxDonViTraTienSCK.Dock = System.Windows.Forms.DockStyle.Top;
            appearance19.FontData.BoldAsString = "True";
            appearance19.FontData.SizeInPoints = 10F;
            this.ultraGroupBoxDonViTraTienSCK.HeaderAppearance = appearance19;
            this.ultraGroupBoxDonViTraTienSCK.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxDonViTraTienSCK.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxDonViTraTienSCK.Name = "ultraGroupBoxDonViTraTienSCK";
            this.ultraGroupBoxDonViTraTienSCK.Size = new System.Drawing.Size(856, 73);
            this.ultraGroupBoxDonViTraTienSCK.TabIndex = 71;
            this.ultraGroupBoxDonViTraTienSCK.Text = "Đơn vị trả tiền";
            this.ultraGroupBoxDonViTraTienSCK.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbBankAccountDetailIDSCK
            // 
            appearance4.TextHAlignAsString = "Left";
            this.cbbBankAccountDetailIDSCK.Appearance = appearance4;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.Appearance = appearance5;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.Override.CellAppearance = appearance12;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance14.TextHAlignAsString = "Left";
            this.cbbBankAccountDetailIDSCK.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.Color.Silver;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.Override.RowAppearance = appearance15;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.Override.TemplateAddRowAppearance = appearance16;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbBankAccountDetailIDSCK.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbBankAccountDetailIDSCK.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbBankAccountDetailIDSCK.Location = new System.Drawing.Point(131, 21);
            this.cbbBankAccountDetailIDSCK.Name = "cbbBankAccountDetailIDSCK";
            this.cbbBankAccountDetailIDSCK.NullText = "<Chọn đối tượng cha>";
            this.cbbBankAccountDetailIDSCK.Size = new System.Drawing.Size(157, 22);
            this.cbbBankAccountDetailIDSCK.TabIndex = 38;
            // 
            // txtBankNameSCK
            // 
            this.txtBankNameSCK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBankNameSCK.Location = new System.Drawing.Point(294, 22);
            this.txtBankNameSCK.Name = "txtBankNameSCK";
            this.txtBankNameSCK.Size = new System.Drawing.Size(552, 21);
            this.txtBankNameSCK.TabIndex = 83;
            // 
            // txtReasonSCK
            // 
            this.txtReasonSCK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReasonSCK.Location = new System.Drawing.Point(131, 46);
            this.txtReasonSCK.Name = "txtReasonSCK";
            this.txtReasonSCK.Size = new System.Drawing.Size(715, 21);
            this.txtReasonSCK.TabIndex = 79;
            this.txtReasonSCK.Text = "Ghi tăng tài sản cố định";
            // 
            // lblReasonSCK
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            this.lblReasonSCK.Appearance = appearance17;
            this.lblReasonSCK.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblReasonSCK.Location = new System.Drawing.Point(4, 46);
            this.lblReasonSCK.Name = "lblReasonSCK";
            this.lblReasonSCK.Size = new System.Drawing.Size(121, 19);
            this.lblReasonSCK.TabIndex = 78;
            this.lblReasonSCK.Text = "Nội dung TT";
            // 
            // lblBankAccountDetailIDSCK
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            this.lblBankAccountDetailIDSCK.Appearance = appearance18;
            this.lblBankAccountDetailIDSCK.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblBankAccountDetailIDSCK.Location = new System.Drawing.Point(4, 25);
            this.lblBankAccountDetailIDSCK.Name = "lblBankAccountDetailIDSCK";
            this.lblBankAccountDetailIDSCK.Size = new System.Drawing.Size(121, 19);
            this.lblBankAccountDetailIDSCK.TabIndex = 73;
            this.lblBankAccountDetailIDSCK.Text = "Tài khoản (*)";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.ultraPanel_SCK);
            this.ultraGroupBox2.Controls.Add(this.cbbAccountingObjectBankAccountSCK);
            this.ultraGroupBox2.Controls.Add(this.cbbAccountingObjectIDSCK);
            this.ultraGroupBox2.Controls.Add(this.txtAccountingObjectBankNameSCK);
            this.ultraGroupBox2.Controls.Add(this.lblAccountingObjectBankAccountSCK);
            this.ultraGroupBox2.Controls.Add(this.txtAccountingObjectAddressSCK);
            this.ultraGroupBox2.Controls.Add(this.lblAccountingObjectAddressSCK);
            this.ultraGroupBox2.Controls.Add(this.lblAccountingObjectIDSCK);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            appearance37.FontData.BoldAsString = "True";
            appearance37.FontData.SizeInPoints = 10F;
            this.ultraGroupBox2.HeaderAppearance = appearance37;
            this.ultraGroupBox2.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 80);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(856, 98);
            this.ultraGroupBox2.TabIndex = 73;
            this.ultraGroupBox2.Text = "Đơn vị nhận tiền";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraPanel_SCK
            // 
            this.ultraPanel_SCK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // ultraPanel_SCK.ClientArea
            // 
            this.ultraPanel_SCK.ClientArea.Controls.Add(this.ultraPanel8);
            this.ultraPanel_SCK.ClientArea.Controls.Add(this.ultraPanel9);
            this.ultraPanel_SCK.Location = new System.Drawing.Point(294, 23);
            this.ultraPanel_SCK.Name = "ultraPanel_SCK";
            this.ultraPanel_SCK.Size = new System.Drawing.Size(552, 23);
            this.ultraPanel_SCK.TabIndex = 71;
            // 
            // ultraPanel8
            // 
            // 
            // ultraPanel8.ClientArea
            // 
            this.ultraPanel8.ClientArea.Controls.Add(this.txtAccountingObjectNameSCK);
            this.ultraPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel8.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel8.Name = "ultraPanel8";
            this.ultraPanel8.Size = new System.Drawing.Size(439, 23);
            this.ultraPanel8.TabIndex = 1;
            // 
            // txtAccountingObjectNameSCK
            // 
            this.txtAccountingObjectNameSCK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectNameSCK.Location = new System.Drawing.Point(1, 1);
            this.txtAccountingObjectNameSCK.Name = "txtAccountingObjectNameSCK";
            this.txtAccountingObjectNameSCK.Size = new System.Drawing.Size(438, 21);
            this.txtAccountingObjectNameSCK.TabIndex = 68;
            // 
            // ultraPanel9
            // 
            // 
            // ultraPanel9.ClientArea
            // 
            this.ultraPanel9.ClientArea.Controls.Add(this.uButtonCongNoSCK);
            this.ultraPanel9.Dock = System.Windows.Forms.DockStyle.Right;
            this.ultraPanel9.Location = new System.Drawing.Point(439, 0);
            this.ultraPanel9.Name = "ultraPanel9";
            this.ultraPanel9.Size = new System.Drawing.Size(113, 23);
            this.ultraPanel9.TabIndex = 0;
            // 
            // uButtonCongNoSCK
            // 
            this.uButtonCongNoSCK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uButtonCongNoSCK.Location = new System.Drawing.Point(0, 0);
            this.uButtonCongNoSCK.Name = "uButtonCongNoSCK";
            this.uButtonCongNoSCK.Size = new System.Drawing.Size(113, 23);
            this.uButtonCongNoSCK.TabIndex = 69;
            this.uButtonCongNoSCK.Text = "Công nợ";
            // 
            // cbbAccountingObjectBankAccountSCK
            // 
            appearance20.TextHAlignAsString = "Left";
            this.cbbAccountingObjectBankAccountSCK.Appearance = appearance20;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.Appearance = appearance21;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance22.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.GroupByBox.Appearance = appearance22;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.GroupByBox.BandLabelAppearance = appearance23;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance24.BackColor2 = System.Drawing.SystemColors.Control;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.GroupByBox.PromptAppearance = appearance24;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.MaxRowScrollRegions = 1;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.Override.ActiveCellAppearance = appearance25;
            appearance26.BackColor = System.Drawing.SystemColors.Highlight;
            appearance26.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.Override.ActiveRowAppearance = appearance26;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.Override.CardAreaAppearance = appearance27;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            appearance28.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.Override.CellAppearance = appearance28;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.Override.CellPadding = 0;
            appearance29.BackColor = System.Drawing.SystemColors.Control;
            appearance29.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance29.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.Override.GroupByRowAppearance = appearance29;
            appearance30.TextHAlignAsString = "Left";
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.Override.HeaderAppearance = appearance30;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.Override.RowAppearance = appearance31;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.Override.TemplateAddRowAppearance = appearance32;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountingObjectBankAccountSCK.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountingObjectBankAccountSCK.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectBankAccountSCK.Location = new System.Drawing.Point(131, 70);
            this.cbbAccountingObjectBankAccountSCK.Name = "cbbAccountingObjectBankAccountSCK";
            this.cbbAccountingObjectBankAccountSCK.NullText = "<Chọn đối tượng cha>";
            this.cbbAccountingObjectBankAccountSCK.Size = new System.Drawing.Size(157, 22);
            this.cbbAccountingObjectBankAccountSCK.TabIndex = 39;
            // 
            // cbbAccountingObjectIDSCK
            // 
            appearance33.Image = global::Accounting.Frm.Properties.Resources.ubtnAdd4;
            editorButton1.Appearance = appearance33;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectIDSCK.ButtonsRight.Add(editorButton1);
            this.cbbAccountingObjectIDSCK.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectIDSCK.Location = new System.Drawing.Point(131, 24);
            this.cbbAccountingObjectIDSCK.Name = "cbbAccountingObjectIDSCK";
            this.cbbAccountingObjectIDSCK.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectIDSCK.Size = new System.Drawing.Size(157, 22);
            this.cbbAccountingObjectIDSCK.TabIndex = 67;
            // 
            // txtAccountingObjectBankNameSCK
            // 
            this.txtAccountingObjectBankNameSCK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectBankNameSCK.Location = new System.Drawing.Point(294, 70);
            this.txtAccountingObjectBankNameSCK.Name = "txtAccountingObjectBankNameSCK";
            this.txtAccountingObjectBankNameSCK.Size = new System.Drawing.Size(552, 21);
            this.txtAccountingObjectBankNameSCK.TabIndex = 63;
            // 
            // lblAccountingObjectBankAccountSCK
            // 
            appearance34.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectBankAccountSCK.Appearance = appearance34;
            this.lblAccountingObjectBankAccountSCK.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblAccountingObjectBankAccountSCK.Location = new System.Drawing.Point(4, 71);
            this.lblAccountingObjectBankAccountSCK.Name = "lblAccountingObjectBankAccountSCK";
            this.lblAccountingObjectBankAccountSCK.Size = new System.Drawing.Size(121, 19);
            this.lblAccountingObjectBankAccountSCK.TabIndex = 62;
            this.lblAccountingObjectBankAccountSCK.Text = "Tài khoản";
            // 
            // txtAccountingObjectAddressSCK
            // 
            this.txtAccountingObjectAddressSCK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddressSCK.Location = new System.Drawing.Point(131, 47);
            this.txtAccountingObjectAddressSCK.Name = "txtAccountingObjectAddressSCK";
            this.txtAccountingObjectAddressSCK.Size = new System.Drawing.Size(715, 21);
            this.txtAccountingObjectAddressSCK.TabIndex = 59;
            // 
            // lblAccountingObjectAddressSCK
            // 
            appearance35.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectAddressSCK.Appearance = appearance35;
            this.lblAccountingObjectAddressSCK.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblAccountingObjectAddressSCK.Location = new System.Drawing.Point(4, 49);
            this.lblAccountingObjectAddressSCK.Name = "lblAccountingObjectAddressSCK";
            this.lblAccountingObjectAddressSCK.Size = new System.Drawing.Size(121, 19);
            this.lblAccountingObjectAddressSCK.TabIndex = 58;
            this.lblAccountingObjectAddressSCK.Text = "Địa chỉ";
            // 
            // lblAccountingObjectIDSCK
            // 
            appearance36.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectIDSCK.Appearance = appearance36;
            this.lblAccountingObjectIDSCK.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblAccountingObjectIDSCK.Location = new System.Drawing.Point(4, 27);
            this.lblAccountingObjectIDSCK.Name = "lblAccountingObjectIDSCK";
            this.lblAccountingObjectIDSCK.Size = new System.Drawing.Size(121, 19);
            this.lblAccountingObjectIDSCK.TabIndex = 57;
            this.lblAccountingObjectIDSCK.Text = "Nhà cung cấp (*)";
            // 
            // pnTTD
            // 
            // 
            // pnTTD.ClientArea
            // 
            this.pnTTD.ClientArea.Controls.Add(this.ultraGroupBoxHoaDonTTD);
            this.pnTTD.ClientArea.Controls.Add(this.ultraGroupBox1TTD);
            this.pnTTD.ClientArea.Controls.Add(this.ultraGroupBoxDonViTraTienTTD);
            this.pnTTD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnTTD.Location = new System.Drawing.Point(0, 0);
            this.pnTTD.Name = "pnTTD";
            this.pnTTD.Size = new System.Drawing.Size(856, 178);
            this.pnTTD.TabIndex = 78;
            // 
            // ultraGroupBoxHoaDonTTD
            // 
            this.ultraGroupBoxHoaDonTTD.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance38.FontData.BoldAsString = "True";
            appearance38.FontData.SizeInPoints = 10F;
            this.ultraGroupBoxHoaDonTTD.HeaderAppearance = appearance38;
            this.ultraGroupBoxHoaDonTTD.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxHoaDonTTD.Location = new System.Drawing.Point(0, 73);
            this.ultraGroupBoxHoaDonTTD.Name = "ultraGroupBoxHoaDonTTD";
            this.ultraGroupBoxHoaDonTTD.Size = new System.Drawing.Size(856, 10);
            this.ultraGroupBoxHoaDonTTD.TabIndex = 75;
            this.ultraGroupBoxHoaDonTTD.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraGroupBox1TTD
            // 
            this.ultraGroupBox1TTD.Controls.Add(this.ultraPanel11);
            this.ultraGroupBox1TTD.Controls.Add(this.cbbAccountingObjectBankAccountTTD);
            this.ultraGroupBox1TTD.Controls.Add(this.cbbAccountingObjectIDTTD);
            this.ultraGroupBox1TTD.Controls.Add(this.txtAccountingObjectBankNameTTD);
            this.ultraGroupBox1TTD.Controls.Add(this.lblAccountingObjectBankAccountTTD);
            this.ultraGroupBox1TTD.Controls.Add(this.txtAccountingObjectAddressTTD);
            this.ultraGroupBox1TTD.Controls.Add(this.lblAccountingObjectAddressTTD);
            this.ultraGroupBox1TTD.Controls.Add(this.lblAccountingObjectIDTTD);
            this.ultraGroupBox1TTD.Dock = System.Windows.Forms.DockStyle.Bottom;
            appearance55.FontData.BoldAsString = "True";
            appearance55.FontData.SizeInPoints = 10F;
            this.ultraGroupBox1TTD.HeaderAppearance = appearance55;
            this.ultraGroupBox1TTD.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox1TTD.Location = new System.Drawing.Point(0, 83);
            this.ultraGroupBox1TTD.Name = "ultraGroupBox1TTD";
            this.ultraGroupBox1TTD.Size = new System.Drawing.Size(856, 95);
            this.ultraGroupBox1TTD.TabIndex = 76;
            this.ultraGroupBox1TTD.Text = "Đơn vị nhận tiền";
            this.ultraGroupBox1TTD.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraPanel11
            // 
            this.ultraPanel11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // ultraPanel11.ClientArea
            // 
            this.ultraPanel11.ClientArea.Controls.Add(this.ultraPanel12);
            this.ultraPanel11.ClientArea.Controls.Add(this.ultraPanel13);
            this.ultraPanel11.Location = new System.Drawing.Point(293, 24);
            this.ultraPanel11.Name = "ultraPanel11";
            this.ultraPanel11.Size = new System.Drawing.Size(553, 21);
            this.ultraPanel11.TabIndex = 75;
            // 
            // ultraPanel12
            // 
            // 
            // ultraPanel12.ClientArea
            // 
            this.ultraPanel12.ClientArea.Controls.Add(this.txtAccountingObjectNameTTD);
            this.ultraPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel12.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel12.Name = "ultraPanel12";
            this.ultraPanel12.Size = new System.Drawing.Size(440, 21);
            this.ultraPanel12.TabIndex = 1;
            // 
            // txtAccountingObjectNameTTD
            // 
            this.txtAccountingObjectNameTTD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAccountingObjectNameTTD.Location = new System.Drawing.Point(0, 0);
            this.txtAccountingObjectNameTTD.Name = "txtAccountingObjectNameTTD";
            this.txtAccountingObjectNameTTD.Size = new System.Drawing.Size(440, 21);
            this.txtAccountingObjectNameTTD.TabIndex = 68;
            // 
            // ultraPanel13
            // 
            // 
            // ultraPanel13.ClientArea
            // 
            this.ultraPanel13.ClientArea.Controls.Add(this.ultraButton1);
            this.ultraPanel13.Dock = System.Windows.Forms.DockStyle.Right;
            this.ultraPanel13.Location = new System.Drawing.Point(440, 0);
            this.ultraPanel13.Name = "ultraPanel13";
            this.ultraPanel13.Size = new System.Drawing.Size(113, 21);
            this.ultraPanel13.TabIndex = 0;
            // 
            // ultraButton1
            // 
            this.ultraButton1.Dock = System.Windows.Forms.DockStyle.Right;
            this.ultraButton1.Location = new System.Drawing.Point(0, 0);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(113, 21);
            this.ultraButton1.TabIndex = 69;
            this.ultraButton1.Text = "Công nợ";
            // 
            // cbbAccountingObjectBankAccountTTD
            // 
            appearance39.TextHAlignAsString = "Left";
            this.cbbAccountingObjectBankAccountTTD.Appearance = appearance39;
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            appearance40.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.Appearance = appearance40;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance41.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance41.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance41.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance41.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.GroupByBox.Appearance = appearance41;
            appearance42.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance42;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance43.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance43.BackColor2 = System.Drawing.SystemColors.Control;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance43.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.GroupByBox.PromptAppearance = appearance43;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance44.BackColor = System.Drawing.SystemColors.Window;
            appearance44.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.Override.ActiveCellAppearance = appearance44;
            appearance45.BackColor = System.Drawing.SystemColors.Highlight;
            appearance45.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.Override.ActiveRowAppearance = appearance45;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.Override.CardAreaAppearance = appearance46;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            appearance47.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.Override.CellAppearance = appearance47;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.Override.CellPadding = 0;
            appearance48.BackColor = System.Drawing.SystemColors.Control;
            appearance48.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance48.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance48.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance48.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.Override.GroupByRowAppearance = appearance48;
            appearance49.TextHAlignAsString = "Left";
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.Override.HeaderAppearance = appearance49;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance50.BackColor = System.Drawing.SystemColors.Window;
            appearance50.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.Override.RowAppearance = appearance50;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance51.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.Override.TemplateAddRowAppearance = appearance51;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountingObjectBankAccountTTD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountingObjectBankAccountTTD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectBankAccountTTD.Location = new System.Drawing.Point(131, 70);
            this.cbbAccountingObjectBankAccountTTD.Name = "cbbAccountingObjectBankAccountTTD";
            this.cbbAccountingObjectBankAccountTTD.NullText = "<Chọn đối tượng cha>";
            this.cbbAccountingObjectBankAccountTTD.Size = new System.Drawing.Size(157, 22);
            this.cbbAccountingObjectBankAccountTTD.TabIndex = 39;
            // 
            // cbbAccountingObjectIDTTD
            // 
            editorButton2.Appearance = appearance33;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectIDTTD.ButtonsRight.Add(editorButton2);
            this.cbbAccountingObjectIDTTD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectIDTTD.Location = new System.Drawing.Point(131, 24);
            this.cbbAccountingObjectIDTTD.Name = "cbbAccountingObjectIDTTD";
            this.cbbAccountingObjectIDTTD.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectIDTTD.Size = new System.Drawing.Size(157, 22);
            this.cbbAccountingObjectIDTTD.TabIndex = 67;
            // 
            // txtAccountingObjectBankNameTTD
            // 
            this.txtAccountingObjectBankNameTTD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectBankNameTTD.Location = new System.Drawing.Point(294, 70);
            this.txtAccountingObjectBankNameTTD.Name = "txtAccountingObjectBankNameTTD";
            this.txtAccountingObjectBankNameTTD.Size = new System.Drawing.Size(552, 21);
            this.txtAccountingObjectBankNameTTD.TabIndex = 63;
            // 
            // lblAccountingObjectBankAccountTTD
            // 
            appearance52.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectBankAccountTTD.Appearance = appearance52;
            this.lblAccountingObjectBankAccountTTD.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblAccountingObjectBankAccountTTD.Location = new System.Drawing.Point(4, 71);
            this.lblAccountingObjectBankAccountTTD.Name = "lblAccountingObjectBankAccountTTD";
            this.lblAccountingObjectBankAccountTTD.Size = new System.Drawing.Size(121, 19);
            this.lblAccountingObjectBankAccountTTD.TabIndex = 62;
            this.lblAccountingObjectBankAccountTTD.Text = "Tài khoản";
            // 
            // txtAccountingObjectAddressTTD
            // 
            this.txtAccountingObjectAddressTTD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddressTTD.Location = new System.Drawing.Point(131, 47);
            this.txtAccountingObjectAddressTTD.Name = "txtAccountingObjectAddressTTD";
            this.txtAccountingObjectAddressTTD.Size = new System.Drawing.Size(715, 21);
            this.txtAccountingObjectAddressTTD.TabIndex = 59;
            // 
            // lblAccountingObjectAddressTTD
            // 
            appearance53.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectAddressTTD.Appearance = appearance53;
            this.lblAccountingObjectAddressTTD.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblAccountingObjectAddressTTD.Location = new System.Drawing.Point(4, 49);
            this.lblAccountingObjectAddressTTD.Name = "lblAccountingObjectAddressTTD";
            this.lblAccountingObjectAddressTTD.Size = new System.Drawing.Size(121, 19);
            this.lblAccountingObjectAddressTTD.TabIndex = 58;
            this.lblAccountingObjectAddressTTD.Text = "Địa chỉ";
            // 
            // lblAccountingObjectIDTTD
            // 
            appearance54.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectIDTTD.Appearance = appearance54;
            this.lblAccountingObjectIDTTD.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblAccountingObjectIDTTD.Location = new System.Drawing.Point(4, 27);
            this.lblAccountingObjectIDTTD.Name = "lblAccountingObjectIDTTD";
            this.lblAccountingObjectIDTTD.Size = new System.Drawing.Size(121, 19);
            this.lblAccountingObjectIDTTD.TabIndex = 57;
            this.lblAccountingObjectIDTTD.Text = "Nhà cung cấp (*)";
            // 
            // ultraGroupBoxDonViTraTienTTD
            // 
            this.ultraGroupBoxDonViTraTienTTD.Controls.Add(this.cbbCreditCardNumberTTD);
            this.ultraGroupBoxDonViTraTienTTD.Controls.Add(this.lblOwnerCardTTD);
            this.ultraGroupBoxDonViTraTienTTD.Controls.Add(this.lblCreditCardTypeTTD);
            this.ultraGroupBoxDonViTraTienTTD.Controls.Add(this.txtReasonTTD);
            this.ultraGroupBoxDonViTraTienTTD.Controls.Add(this.lblReasonTTD);
            this.ultraGroupBoxDonViTraTienTTD.Controls.Add(this.lblBankAccountDetailIDTTD);
            this.ultraGroupBoxDonViTraTienTTD.Dock = System.Windows.Forms.DockStyle.Top;
            appearance61.FontData.BoldAsString = "True";
            appearance61.FontData.SizeInPoints = 10F;
            this.ultraGroupBoxDonViTraTienTTD.HeaderAppearance = appearance61;
            this.ultraGroupBoxDonViTraTienTTD.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxDonViTraTienTTD.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxDonViTraTienTTD.Name = "ultraGroupBoxDonViTraTienTTD";
            this.ultraGroupBoxDonViTraTienTTD.Size = new System.Drawing.Size(856, 73);
            this.ultraGroupBoxDonViTraTienTTD.TabIndex = 74;
            this.ultraGroupBoxDonViTraTienTTD.Text = "Đơn vị trả tiền";
            this.ultraGroupBoxDonViTraTienTTD.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbCreditCardNumberTTD
            // 
            appearance56.Image = global::Accounting.Frm.Properties.Resources.ubtnAdd4;
            editorButton3.Appearance = appearance56;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbCreditCardNumberTTD.ButtonsRight.Add(editorButton3);
            this.cbbCreditCardNumberTTD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCreditCardNumberTTD.Location = new System.Drawing.Point(131, 23);
            this.cbbCreditCardNumberTTD.Name = "cbbCreditCardNumberTTD";
            this.cbbCreditCardNumberTTD.NullText = "<chọn dữ liệu>";
            this.cbbCreditCardNumberTTD.Size = new System.Drawing.Size(157, 22);
            this.cbbCreditCardNumberTTD.TabIndex = 83;
            // 
            // lblOwnerCardTTD
            // 
            this.lblOwnerCardTTD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance57.BackColor = System.Drawing.Color.Transparent;
            this.lblOwnerCardTTD.Appearance = appearance57;
            this.lblOwnerCardTTD.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblOwnerCardTTD.Location = new System.Drawing.Point(493, 25);
            this.lblOwnerCardTTD.Name = "lblOwnerCardTTD";
            this.lblOwnerCardTTD.Size = new System.Drawing.Size(353, 19);
            this.lblOwnerCardTTD.TabIndex = 82;
            this.lblOwnerCardTTD.Text = "Chủ thẻ";
            // 
            // lblCreditCardTypeTTD
            // 
            appearance58.BackColor = System.Drawing.Color.Transparent;
            this.lblCreditCardTypeTTD.Appearance = appearance58;
            this.lblCreditCardTypeTTD.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblCreditCardTypeTTD.Location = new System.Drawing.Point(294, 25);
            this.lblCreditCardTypeTTD.Name = "lblCreditCardTypeTTD";
            this.lblCreditCardTypeTTD.Size = new System.Drawing.Size(193, 19);
            this.lblCreditCardTypeTTD.TabIndex = 81;
            // 
            // txtReasonTTD
            // 
            this.txtReasonTTD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReasonTTD.Location = new System.Drawing.Point(131, 46);
            this.txtReasonTTD.Name = "txtReasonTTD";
            this.txtReasonTTD.Size = new System.Drawing.Size(715, 21);
            this.txtReasonTTD.TabIndex = 79;
            this.txtReasonTTD.Text = "Ghi tăng tài sản cố định";
            // 
            // lblReasonTTD
            // 
            appearance59.BackColor = System.Drawing.Color.Transparent;
            this.lblReasonTTD.Appearance = appearance59;
            this.lblReasonTTD.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblReasonTTD.Location = new System.Drawing.Point(4, 46);
            this.lblReasonTTD.Name = "lblReasonTTD";
            this.lblReasonTTD.Size = new System.Drawing.Size(121, 19);
            this.lblReasonTTD.TabIndex = 78;
            this.lblReasonTTD.Text = "Nội dung TT";
            // 
            // lblBankAccountDetailIDTTD
            // 
            appearance60.BackColor = System.Drawing.Color.Transparent;
            this.lblBankAccountDetailIDTTD.Appearance = appearance60;
            this.lblBankAccountDetailIDTTD.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblBankAccountDetailIDTTD.Location = new System.Drawing.Point(4, 25);
            this.lblBankAccountDetailIDTTD.Name = "lblBankAccountDetailIDTTD";
            this.lblBankAccountDetailIDTTD.Size = new System.Drawing.Size(121, 19);
            this.lblBankAccountDetailIDTTD.TabIndex = 73;
            this.lblBankAccountDetailIDTTD.Text = "Tài khoản (*)";
            // 
            // grCTT
            // 
            this.grCTT.Controls.Add(this.cbbAccountingObjectID);
            this.grCTT.Controls.Add(this.ultraPanel5);
            this.grCTT.Controls.Add(this.txtCompanyTaxCode);
            this.grCTT.Controls.Add(this.lblCompanyTaxCode);
            this.grCTT.Controls.Add(this.txtReason);
            this.grCTT.Controls.Add(this.lblReason);
            this.grCTT.Controls.Add(this.txtAccountingObjectAddress);
            this.grCTT.Controls.Add(this.lblAccountingObjectAddress);
            this.grCTT.Controls.Add(this.lblAccountingObjectID);
            this.grCTT.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance66.FontData.BoldAsString = "True";
            appearance66.FontData.SizeInPoints = 10F;
            this.grCTT.HeaderAppearance = appearance66;
            this.grCTT.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.grCTT.Location = new System.Drawing.Point(0, 0);
            this.grCTT.Name = "grCTT";
            this.grCTT.Size = new System.Drawing.Size(856, 178);
            this.grCTT.TabIndex = 45;
            this.grCTT.Text = "Thông tin chung";
            this.grCTT.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbAccountingObjectID
            // 
            editorButton4.Appearance = appearance33;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectID.ButtonsRight.Add(editorButton4);
            this.cbbAccountingObjectID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectID.Location = new System.Drawing.Point(131, 26);
            this.cbbAccountingObjectID.Name = "cbbAccountingObjectID";
            this.cbbAccountingObjectID.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID.Size = new System.Drawing.Size(156, 22);
            this.cbbAccountingObjectID.TabIndex = 75;
            // 
            // ultraPanel5
            // 
            this.ultraPanel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // ultraPanel5.ClientArea
            // 
            this.ultraPanel5.ClientArea.Controls.Add(this.ultraPanel6);
            this.ultraPanel5.ClientArea.Controls.Add(this.ultraPanel7);
            this.ultraPanel5.Location = new System.Drawing.Point(293, 26);
            this.ultraPanel5.Name = "ultraPanel5";
            this.ultraPanel5.Size = new System.Drawing.Size(553, 21);
            this.ultraPanel5.TabIndex = 74;
            // 
            // ultraPanel6
            // 
            // 
            // ultraPanel6.ClientArea
            // 
            this.ultraPanel6.ClientArea.Controls.Add(this.txtAccountingObjectName);
            this.ultraPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel6.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel6.Name = "ultraPanel6";
            this.ultraPanel6.Size = new System.Drawing.Size(440, 21);
            this.ultraPanel6.TabIndex = 1;
            // 
            // txtAccountingObjectName
            // 
            this.txtAccountingObjectName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAccountingObjectName.Location = new System.Drawing.Point(0, 0);
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(440, 21);
            this.txtAccountingObjectName.TabIndex = 68;
            // 
            // ultraPanel7
            // 
            // 
            // ultraPanel7.ClientArea
            // 
            this.ultraPanel7.ClientArea.Controls.Add(this.uButtonCongNo);
            this.ultraPanel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.ultraPanel7.Location = new System.Drawing.Point(440, 0);
            this.ultraPanel7.Name = "ultraPanel7";
            this.ultraPanel7.Size = new System.Drawing.Size(113, 21);
            this.ultraPanel7.TabIndex = 0;
            // 
            // uButtonCongNo
            // 
            this.uButtonCongNo.Dock = System.Windows.Forms.DockStyle.Right;
            this.uButtonCongNo.Location = new System.Drawing.Point(0, 0);
            this.uButtonCongNo.Name = "uButtonCongNo";
            this.uButtonCongNo.Size = new System.Drawing.Size(113, 21);
            this.uButtonCongNo.TabIndex = 69;
            this.uButtonCongNo.Text = "Công nợ";
            // 
            // txtCompanyTaxCode
            // 
            this.txtCompanyTaxCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCompanyTaxCode.Location = new System.Drawing.Point(131, 73);
            this.txtCompanyTaxCode.Multiline = true;
            this.txtCompanyTaxCode.Name = "txtCompanyTaxCode";
            this.txtCompanyTaxCode.Size = new System.Drawing.Size(715, 21);
            this.txtCompanyTaxCode.TabIndex = 65;
            // 
            // lblCompanyTaxCode
            // 
            appearance62.BackColor = System.Drawing.Color.Transparent;
            this.lblCompanyTaxCode.Appearance = appearance62;
            this.lblCompanyTaxCode.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblCompanyTaxCode.Location = new System.Drawing.Point(10, 72);
            this.lblCompanyTaxCode.Name = "lblCompanyTaxCode";
            this.lblCompanyTaxCode.Size = new System.Drawing.Size(115, 19);
            this.lblCompanyTaxCode.TabIndex = 64;
            this.lblCompanyTaxCode.Text = "Mã số thuế";
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason.Location = new System.Drawing.Point(131, 95);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(715, 21);
            this.txtReason.TabIndex = 63;
            this.txtReason.Text = "Ghi tăng tài sản cố định";
            // 
            // lblReason
            // 
            appearance63.BackColor = System.Drawing.Color.Transparent;
            this.lblReason.Appearance = appearance63;
            this.lblReason.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblReason.Location = new System.Drawing.Point(10, 95);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(115, 19);
            this.lblReason.TabIndex = 62;
            this.lblReason.Text = "Diễn giải";
            // 
            // txtAccountingObjectAddress
            // 
            this.txtAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddress.Location = new System.Drawing.Point(131, 51);
            this.txtAccountingObjectAddress.Name = "txtAccountingObjectAddress";
            this.txtAccountingObjectAddress.Size = new System.Drawing.Size(715, 21);
            this.txtAccountingObjectAddress.TabIndex = 59;
            // 
            // lblAccountingObjectAddress
            // 
            appearance64.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectAddress.Appearance = appearance64;
            this.lblAccountingObjectAddress.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblAccountingObjectAddress.Location = new System.Drawing.Point(10, 50);
            this.lblAccountingObjectAddress.Name = "lblAccountingObjectAddress";
            this.lblAccountingObjectAddress.Size = new System.Drawing.Size(115, 19);
            this.lblAccountingObjectAddress.TabIndex = 58;
            this.lblAccountingObjectAddress.Text = "Địa chỉ";
            // 
            // lblAccountingObjectID
            // 
            appearance65.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectID.Appearance = appearance65;
            this.lblAccountingObjectID.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblAccountingObjectID.Location = new System.Drawing.Point(10, 29);
            this.lblAccountingObjectID.Name = "lblAccountingObjectID";
            this.lblAccountingObjectID.Size = new System.Drawing.Size(115, 19);
            this.lblAccountingObjectID.TabIndex = 57;
            this.lblAccountingObjectID.Text = "Nhà cung cấp (*)";
            // 
            // pnSTM
            // 
            // 
            // pnSTM.ClientArea
            // 
            this.pnSTM.ClientArea.Controls.Add(this.ultraGroupBoxHoaDonSTM);
            this.pnSTM.ClientArea.Controls.Add(this.ultraGroupBox1STM);
            this.pnSTM.ClientArea.Controls.Add(this.ultraGroupBoxDonViTraTienSTM);
            this.pnSTM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnSTM.Location = new System.Drawing.Point(0, 0);
            this.pnSTM.Name = "pnSTM";
            this.pnSTM.Size = new System.Drawing.Size(856, 178);
            this.pnSTM.TabIndex = 76;
            // 
            // ultraGroupBoxHoaDonSTM
            // 
            this.ultraGroupBoxHoaDonSTM.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance67.FontData.BoldAsString = "True";
            appearance67.FontData.SizeInPoints = 10F;
            this.ultraGroupBoxHoaDonSTM.HeaderAppearance = appearance67;
            this.ultraGroupBoxHoaDonSTM.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxHoaDonSTM.Location = new System.Drawing.Point(0, 73);
            this.ultraGroupBoxHoaDonSTM.Name = "ultraGroupBoxHoaDonSTM";
            this.ultraGroupBoxHoaDonSTM.Size = new System.Drawing.Size(856, 7);
            this.ultraGroupBoxHoaDonSTM.TabIndex = 74;
            this.ultraGroupBoxHoaDonSTM.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraGroupBox1STM
            // 
            this.ultraGroupBox1STM.Controls.Add(this.txtIssueBy);
            this.ultraGroupBox1STM.Controls.Add(this.lblIssueBy);
            this.ultraGroupBox1STM.Controls.Add(this.dteIssueDate);
            this.ultraGroupBox1STM.Controls.Add(this.lblIssueDate);
            this.ultraGroupBox1STM.Controls.Add(this.ultraPanel1STM);
            this.ultraGroupBox1STM.Controls.Add(this.mkeIdentificationNoSTM);
            this.ultraGroupBox1STM.Controls.Add(this.cbbAccountingObjectIDSTM);
            this.ultraGroupBox1STM.Controls.Add(this.lblIdentificationNoSTM);
            this.ultraGroupBox1STM.Controls.Add(this.txtContactNameSTM);
            this.ultraGroupBox1STM.Controls.Add(this.lblContactNameSTM);
            this.ultraGroupBox1STM.Controls.Add(this.lblAccountingObjectIDSTM);
            this.ultraGroupBox1STM.Dock = System.Windows.Forms.DockStyle.Bottom;
            appearance74.FontData.BoldAsString = "True";
            appearance74.FontData.SizeInPoints = 10F;
            this.ultraGroupBox1STM.HeaderAppearance = appearance74;
            this.ultraGroupBox1STM.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox1STM.Location = new System.Drawing.Point(0, 80);
            this.ultraGroupBox1STM.Name = "ultraGroupBox1STM";
            this.ultraGroupBox1STM.Size = new System.Drawing.Size(856, 98);
            this.ultraGroupBox1STM.TabIndex = 73;
            this.ultraGroupBox1STM.Text = "Đơn vị nhận tiền";
            this.ultraGroupBox1STM.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtIssueBy
            // 
            this.txtIssueBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIssueBy.Location = new System.Drawing.Point(503, 72);
            this.txtIssueBy.Name = "txtIssueBy";
            this.txtIssueBy.Size = new System.Drawing.Size(341, 21);
            this.txtIssueBy.TabIndex = 78;
            // 
            // lblIssueBy
            // 
            appearance68.BackColor = System.Drawing.Color.Transparent;
            appearance68.TextVAlignAsString = "Middle";
            this.lblIssueBy.Appearance = appearance68;
            this.lblIssueBy.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblIssueBy.Location = new System.Drawing.Point(447, 73);
            this.lblIssueBy.Name = "lblIssueBy";
            this.lblIssueBy.Size = new System.Drawing.Size(50, 19);
            this.lblIssueBy.TabIndex = 77;
            this.lblIssueBy.Text = "Nơi cấp";
            // 
            // dteIssueDate
            // 
            appearance69.TextHAlignAsString = "Center";
            appearance69.TextVAlignAsString = "Middle";
            this.dteIssueDate.Appearance = appearance69;
            this.dteIssueDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteIssueDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteIssueDate.Location = new System.Drawing.Point(293, 73);
            this.dteIssueDate.MaskInput = "";
            this.dteIssueDate.Name = "dteIssueDate";
            this.dteIssueDate.Size = new System.Drawing.Size(109, 21);
            this.dteIssueDate.TabIndex = 75;
            this.dteIssueDate.Value = null;
            // 
            // lblIssueDate
            // 
            appearance70.BackColor = System.Drawing.Color.Transparent;
            appearance70.TextVAlignAsString = "Middle";
            this.lblIssueDate.Appearance = appearance70;
            this.lblIssueDate.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblIssueDate.Location = new System.Drawing.Point(251, 73);
            this.lblIssueDate.Name = "lblIssueDate";
            this.lblIssueDate.Size = new System.Drawing.Size(36, 19);
            this.lblIssueDate.TabIndex = 76;
            this.lblIssueDate.Text = "Ngày";
            // 
            // ultraPanel1STM
            // 
            this.ultraPanel1STM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // ultraPanel1STM.ClientArea
            // 
            this.ultraPanel1STM.ClientArea.Controls.Add(this.ultraPanel3STM);
            this.ultraPanel1STM.ClientArea.Controls.Add(this.ultraPanelSTM2);
            this.ultraPanel1STM.Location = new System.Drawing.Point(295, 25);
            this.ultraPanel1STM.Name = "ultraPanel1STM";
            this.ultraPanel1STM.Size = new System.Drawing.Size(549, 23);
            this.ultraPanel1STM.TabIndex = 74;
            // 
            // ultraPanel3STM
            // 
            // 
            // ultraPanel3STM.ClientArea
            // 
            this.ultraPanel3STM.ClientArea.Controls.Add(this.txtAccountingObjectNameSTM);
            this.ultraPanel3STM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel3STM.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel3STM.Name = "ultraPanel3STM";
            this.ultraPanel3STM.Size = new System.Drawing.Size(436, 23);
            this.ultraPanel3STM.TabIndex = 1;
            // 
            // txtAccountingObjectNameSTM
            // 
            this.txtAccountingObjectNameSTM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectNameSTM.Location = new System.Drawing.Point(1, 1);
            this.txtAccountingObjectNameSTM.Name = "txtAccountingObjectNameSTM";
            this.txtAccountingObjectNameSTM.Size = new System.Drawing.Size(435, 21);
            this.txtAccountingObjectNameSTM.TabIndex = 68;
            // 
            // ultraPanelSTM2
            // 
            // 
            // ultraPanelSTM2.ClientArea
            // 
            this.ultraPanelSTM2.ClientArea.Controls.Add(this.uButtonCongNoSTM);
            this.ultraPanelSTM2.Dock = System.Windows.Forms.DockStyle.Right;
            this.ultraPanelSTM2.Location = new System.Drawing.Point(436, 0);
            this.ultraPanelSTM2.Name = "ultraPanelSTM2";
            this.ultraPanelSTM2.Size = new System.Drawing.Size(113, 23);
            this.ultraPanelSTM2.TabIndex = 0;
            // 
            // uButtonCongNoSTM
            // 
            this.uButtonCongNoSTM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uButtonCongNoSTM.Location = new System.Drawing.Point(0, 0);
            this.uButtonCongNoSTM.Name = "uButtonCongNoSTM";
            this.uButtonCongNoSTM.Size = new System.Drawing.Size(113, 23);
            this.uButtonCongNoSTM.TabIndex = 69;
            this.uButtonCongNoSTM.Text = "Công nợ";
            // 
            // mkeIdentificationNoSTM
            // 
            this.mkeIdentificationNoSTM.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.mkeIdentificationNoSTM.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.String;
            this.mkeIdentificationNoSTM.InputMask = "####-####-####";
            this.mkeIdentificationNoSTM.Location = new System.Drawing.Point(134, 72);
            this.mkeIdentificationNoSTM.Name = "mkeIdentificationNoSTM";
            this.mkeIdentificationNoSTM.Size = new System.Drawing.Size(97, 20);
            this.mkeIdentificationNoSTM.TabIndex = 73;
            this.mkeIdentificationNoSTM.Text = "--";
            // 
            // cbbAccountingObjectIDSTM
            // 
            editorButton5.Appearance = appearance33;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectIDSTM.ButtonsRight.Add(editorButton5);
            this.cbbAccountingObjectIDSTM.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectIDSTM.Location = new System.Drawing.Point(131, 25);
            this.cbbAccountingObjectIDSTM.Name = "cbbAccountingObjectIDSTM";
            this.cbbAccountingObjectIDSTM.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectIDSTM.Size = new System.Drawing.Size(160, 22);
            this.cbbAccountingObjectIDSTM.TabIndex = 67;
            // 
            // lblIdentificationNoSTM
            // 
            appearance71.BackColor = System.Drawing.Color.Transparent;
            appearance71.TextVAlignAsString = "Middle";
            this.lblIdentificationNoSTM.Appearance = appearance71;
            this.lblIdentificationNoSTM.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblIdentificationNoSTM.Location = new System.Drawing.Point(4, 73);
            this.lblIdentificationNoSTM.Name = "lblIdentificationNoSTM";
            this.lblIdentificationNoSTM.Size = new System.Drawing.Size(124, 19);
            this.lblIdentificationNoSTM.TabIndex = 62;
            this.lblIdentificationNoSTM.Text = "Số CMND";
            // 
            // txtContactNameSTM
            // 
            this.txtContactNameSTM.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtContactNameSTM.Location = new System.Drawing.Point(131, 49);
            this.txtContactNameSTM.Name = "txtContactNameSTM";
            this.txtContactNameSTM.Size = new System.Drawing.Size(713, 21);
            this.txtContactNameSTM.TabIndex = 59;
            // 
            // lblContactNameSTM
            // 
            appearance72.BackColor = System.Drawing.Color.Transparent;
            this.lblContactNameSTM.Appearance = appearance72;
            this.lblContactNameSTM.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblContactNameSTM.Location = new System.Drawing.Point(4, 51);
            this.lblContactNameSTM.Name = "lblContactNameSTM";
            this.lblContactNameSTM.Size = new System.Drawing.Size(124, 19);
            this.lblContactNameSTM.TabIndex = 58;
            this.lblContactNameSTM.Text = "Họ tên người lĩnh tiền";
            // 
            // lblAccountingObjectIDSTM
            // 
            appearance73.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectIDSTM.Appearance = appearance73;
            this.lblAccountingObjectIDSTM.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblAccountingObjectIDSTM.Location = new System.Drawing.Point(4, 29);
            this.lblAccountingObjectIDSTM.Name = "lblAccountingObjectIDSTM";
            this.lblAccountingObjectIDSTM.Size = new System.Drawing.Size(124, 19);
            this.lblAccountingObjectIDSTM.TabIndex = 57;
            this.lblAccountingObjectIDSTM.Text = "Nhà cung cấp (*)";
            // 
            // ultraGroupBoxDonViTraTienSTM
            // 
            this.ultraGroupBoxDonViTraTienSTM.Controls.Add(this.cbbBankAccountDetailIDSTM);
            this.ultraGroupBoxDonViTraTienSTM.Controls.Add(this.txtBankNameSTM);
            this.ultraGroupBoxDonViTraTienSTM.Controls.Add(this.txtReasonSTM);
            this.ultraGroupBoxDonViTraTienSTM.Controls.Add(this.lblReasonSTM);
            this.ultraGroupBoxDonViTraTienSTM.Controls.Add(this.lblBankAccountDetailIDSTM);
            this.ultraGroupBoxDonViTraTienSTM.Dock = System.Windows.Forms.DockStyle.Top;
            appearance90.FontData.BoldAsString = "True";
            appearance90.FontData.SizeInPoints = 10F;
            this.ultraGroupBoxDonViTraTienSTM.HeaderAppearance = appearance90;
            this.ultraGroupBoxDonViTraTienSTM.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxDonViTraTienSTM.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxDonViTraTienSTM.Name = "ultraGroupBoxDonViTraTienSTM";
            this.ultraGroupBoxDonViTraTienSTM.Size = new System.Drawing.Size(856, 73);
            this.ultraGroupBoxDonViTraTienSTM.TabIndex = 72;
            this.ultraGroupBoxDonViTraTienSTM.Text = "Đơn vị trả tiền";
            this.ultraGroupBoxDonViTraTienSTM.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbBankAccountDetailIDSTM
            // 
            appearance75.TextHAlignAsString = "Left";
            this.cbbBankAccountDetailIDSTM.Appearance = appearance75;
            appearance76.BackColor = System.Drawing.SystemColors.Window;
            appearance76.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.Appearance = appearance76;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance77.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance77.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance77.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance77.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.GroupByBox.Appearance = appearance77;
            appearance78.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.GroupByBox.BandLabelAppearance = appearance78;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance79.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance79.BackColor2 = System.Drawing.SystemColors.Control;
            appearance79.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance79.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.GroupByBox.PromptAppearance = appearance79;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.MaxRowScrollRegions = 1;
            appearance80.BackColor = System.Drawing.SystemColors.Window;
            appearance80.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.Override.ActiveCellAppearance = appearance80;
            appearance81.BackColor = System.Drawing.SystemColors.Highlight;
            appearance81.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.Override.ActiveRowAppearance = appearance81;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance82.BackColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.Override.CardAreaAppearance = appearance82;
            appearance83.BorderColor = System.Drawing.Color.Silver;
            appearance83.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.Override.CellAppearance = appearance83;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.Override.CellPadding = 0;
            appearance84.BackColor = System.Drawing.SystemColors.Control;
            appearance84.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance84.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance84.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance84.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.Override.GroupByRowAppearance = appearance84;
            appearance85.TextHAlignAsString = "Left";
            this.cbbBankAccountDetailIDSTM.DisplayLayout.Override.HeaderAppearance = appearance85;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance86.BackColor = System.Drawing.SystemColors.Window;
            appearance86.BorderColor = System.Drawing.Color.Silver;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.Override.RowAppearance = appearance86;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance87.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.Override.TemplateAddRowAppearance = appearance87;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbBankAccountDetailIDSTM.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbBankAccountDetailIDSTM.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbBankAccountDetailIDSTM.Location = new System.Drawing.Point(131, 21);
            this.cbbBankAccountDetailIDSTM.Name = "cbbBankAccountDetailIDSTM";
            this.cbbBankAccountDetailIDSTM.NullText = "<Chọn đối tượng cha>";
            this.cbbBankAccountDetailIDSTM.Size = new System.Drawing.Size(160, 22);
            this.cbbBankAccountDetailIDSTM.TabIndex = 38;
            // 
            // txtBankNameSTM
            // 
            this.txtBankNameSTM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBankNameSTM.Location = new System.Drawing.Point(294, 22);
            this.txtBankNameSTM.Name = "txtBankNameSTM";
            this.txtBankNameSTM.Size = new System.Drawing.Size(552, 21);
            this.txtBankNameSTM.TabIndex = 83;
            // 
            // txtReasonSTM
            // 
            this.txtReasonSTM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReasonSTM.Location = new System.Drawing.Point(131, 46);
            this.txtReasonSTM.Name = "txtReasonSTM";
            this.txtReasonSTM.Size = new System.Drawing.Size(715, 21);
            this.txtReasonSTM.TabIndex = 79;
            this.txtReasonSTM.Text = "Ghi tăng tài sản cố định";
            // 
            // lblReasonSTM
            // 
            appearance88.BackColor = System.Drawing.Color.Transparent;
            this.lblReasonSTM.Appearance = appearance88;
            this.lblReasonSTM.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblReasonSTM.Location = new System.Drawing.Point(4, 46);
            this.lblReasonSTM.Name = "lblReasonSTM";
            this.lblReasonSTM.Size = new System.Drawing.Size(124, 19);
            this.lblReasonSTM.TabIndex = 78;
            this.lblReasonSTM.Text = "Nội dung TT";
            // 
            // lblBankAccountDetailIDSTM
            // 
            appearance89.BackColor = System.Drawing.Color.Transparent;
            this.lblBankAccountDetailIDSTM.Appearance = appearance89;
            this.lblBankAccountDetailIDSTM.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblBankAccountDetailIDSTM.Location = new System.Drawing.Point(4, 25);
            this.lblBankAccountDetailIDSTM.Name = "lblBankAccountDetailIDSTM";
            this.lblBankAccountDetailIDSTM.Size = new System.Drawing.Size(124, 19);
            this.lblBankAccountDetailIDSTM.TabIndex = 73;
            this.lblBankAccountDetailIDSTM.Text = "Tài khoản (*)";
            // 
            // grTM
            // 
            this.grTM.Controls.Add(this.ultraPanel2TM);
            this.grTM.Controls.Add(this.lblChungTuGocTM);
            this.grTM.Controls.Add(this.lblNumberAttachTM);
            this.grTM.Controls.Add(this.cbbAccountingObjectIDTM);
            this.grTM.Controls.Add(this.txtCompanyTaxCodeTM);
            this.grTM.Controls.Add(this.lblCompanyTaxCodeTM);
            this.grTM.Controls.Add(this.txtReasonTM);
            this.grTM.Controls.Add(this.lblReasonTM);
            this.grTM.Controls.Add(this.txtContactNameTM);
            this.grTM.Controls.Add(this.lblContactNameTM);
            this.grTM.Controls.Add(this.txtAccountingObjectAddressTM);
            this.grTM.Controls.Add(this.lblAccountingObjectAddressTM);
            this.grTM.Controls.Add(this.lblAccountingObjectIDTM);
            this.grTM.Controls.Add(this.txtNumberAttachTM);
            this.grTM.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance98.FontData.BoldAsString = "True";
            appearance98.FontData.SizeInPoints = 10F;
            this.grTM.HeaderAppearance = appearance98;
            this.grTM.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.grTM.Location = new System.Drawing.Point(0, 0);
            this.grTM.Name = "grTM";
            this.grTM.Size = new System.Drawing.Size(856, 178);
            this.grTM.TabIndex = 75;
            this.grTM.Text = "Thông tin chung";
            this.grTM.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraPanel2TM
            // 
            this.ultraPanel2TM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // ultraPanel2TM.ClientArea
            // 
            this.ultraPanel2TM.ClientArea.Controls.Add(this.ultraPanel3TM);
            this.ultraPanel2TM.ClientArea.Controls.Add(this.ultraPanel4TM);
            this.ultraPanel2TM.Location = new System.Drawing.Point(296, 18);
            this.ultraPanel2TM.Name = "ultraPanel2TM";
            this.ultraPanel2TM.Size = new System.Drawing.Size(550, 23);
            this.ultraPanel2TM.TabIndex = 73;
            // 
            // ultraPanel3TM
            // 
            // 
            // ultraPanel3TM.ClientArea
            // 
            this.ultraPanel3TM.ClientArea.Controls.Add(this.txtAccountingObjectNameTM);
            this.ultraPanel3TM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel3TM.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel3TM.Name = "ultraPanel3TM";
            this.ultraPanel3TM.Size = new System.Drawing.Size(437, 23);
            this.ultraPanel3TM.TabIndex = 1;
            // 
            // txtAccountingObjectNameTM
            // 
            this.txtAccountingObjectNameTM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectNameTM.Location = new System.Drawing.Point(1, 1);
            this.txtAccountingObjectNameTM.Name = "txtAccountingObjectNameTM";
            this.txtAccountingObjectNameTM.Size = new System.Drawing.Size(448, 21);
            this.txtAccountingObjectNameTM.TabIndex = 68;
            // 
            // ultraPanel4TM
            // 
            // 
            // ultraPanel4TM.ClientArea
            // 
            this.ultraPanel4TM.ClientArea.Controls.Add(this.uButtonCongNoTM);
            this.ultraPanel4TM.Dock = System.Windows.Forms.DockStyle.Right;
            this.ultraPanel4TM.Location = new System.Drawing.Point(437, 0);
            this.ultraPanel4TM.Name = "ultraPanel4TM";
            this.ultraPanel4TM.Size = new System.Drawing.Size(113, 23);
            this.ultraPanel4TM.TabIndex = 0;
            // 
            // uButtonCongNoTM
            // 
            this.uButtonCongNoTM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uButtonCongNoTM.Location = new System.Drawing.Point(0, 0);
            this.uButtonCongNoTM.Name = "uButtonCongNoTM";
            this.uButtonCongNoTM.Size = new System.Drawing.Size(113, 23);
            this.uButtonCongNoTM.TabIndex = 69;
            this.uButtonCongNoTM.Text = "Công nợ";
            // 
            // lblChungTuGocTM
            // 
            this.lblChungTuGocTM.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance91.BackColor = System.Drawing.Color.Transparent;
            this.lblChungTuGocTM.Appearance = appearance91;
            this.lblChungTuGocTM.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblChungTuGocTM.Location = new System.Drawing.Point(731, 114);
            this.lblChungTuGocTM.Name = "lblChungTuGocTM";
            this.lblChungTuGocTM.Size = new System.Drawing.Size(115, 19);
            this.lblChungTuGocTM.TabIndex = 72;
            this.lblChungTuGocTM.Text = "Chứng từ gốc";
            // 
            // lblNumberAttachTM
            // 
            appearance92.BackColor = System.Drawing.Color.Transparent;
            this.lblNumberAttachTM.Appearance = appearance92;
            this.lblNumberAttachTM.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblNumberAttachTM.Location = new System.Drawing.Point(10, 112);
            this.lblNumberAttachTM.Name = "lblNumberAttachTM";
            this.lblNumberAttachTM.Size = new System.Drawing.Size(115, 19);
            this.lblNumberAttachTM.TabIndex = 71;
            this.lblNumberAttachTM.Text = "Kèm theo";
            // 
            // cbbAccountingObjectIDTM
            // 
            editorButton6.Appearance = appearance33;
            editorButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectIDTM.ButtonsRight.Add(editorButton6);
            this.cbbAccountingObjectIDTM.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectIDTM.Location = new System.Drawing.Point(131, 18);
            this.cbbAccountingObjectIDTM.Name = "cbbAccountingObjectIDTM";
            this.cbbAccountingObjectIDTM.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectIDTM.Size = new System.Drawing.Size(156, 22);
            this.cbbAccountingObjectIDTM.TabIndex = 67;
            // 
            // txtCompanyTaxCodeTM
            // 
            this.txtCompanyTaxCodeTM.Location = new System.Drawing.Point(131, 64);
            this.txtCompanyTaxCodeTM.Multiline = true;
            this.txtCompanyTaxCodeTM.Name = "txtCompanyTaxCodeTM";
            this.txtCompanyTaxCodeTM.Size = new System.Drawing.Size(156, 21);
            this.txtCompanyTaxCodeTM.TabIndex = 65;
            // 
            // lblCompanyTaxCodeTM
            // 
            appearance93.BackColor = System.Drawing.Color.Transparent;
            this.lblCompanyTaxCodeTM.Appearance = appearance93;
            this.lblCompanyTaxCodeTM.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblCompanyTaxCodeTM.Location = new System.Drawing.Point(10, 65);
            this.lblCompanyTaxCodeTM.Name = "lblCompanyTaxCodeTM";
            this.lblCompanyTaxCodeTM.Size = new System.Drawing.Size(115, 19);
            this.lblCompanyTaxCodeTM.TabIndex = 64;
            this.lblCompanyTaxCodeTM.Text = "Mã số thuế";
            // 
            // txtReasonTM
            // 
            this.txtReasonTM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReasonTM.Location = new System.Drawing.Point(131, 89);
            this.txtReasonTM.Name = "txtReasonTM";
            this.txtReasonTM.Size = new System.Drawing.Size(715, 21);
            this.txtReasonTM.TabIndex = 63;
            this.txtReasonTM.Text = "Ghi tăng tài sản cố định";
            // 
            // lblReasonTM
            // 
            appearance94.BackColor = System.Drawing.Color.Transparent;
            this.lblReasonTM.Appearance = appearance94;
            this.lblReasonTM.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblReasonTM.Location = new System.Drawing.Point(10, 89);
            this.lblReasonTM.Name = "lblReasonTM";
            this.lblReasonTM.Size = new System.Drawing.Size(115, 19);
            this.lblReasonTM.TabIndex = 62;
            this.lblReasonTM.Text = "Lý do chi";
            // 
            // txtContactNameTM
            // 
            this.txtContactNameTM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtContactNameTM.Location = new System.Drawing.Point(430, 65);
            this.txtContactNameTM.Name = "txtContactNameTM";
            this.txtContactNameTM.Size = new System.Drawing.Size(416, 21);
            this.txtContactNameTM.TabIndex = 61;
            // 
            // lblContactNameTM
            // 
            appearance95.BackColor = System.Drawing.Color.Transparent;
            this.lblContactNameTM.Appearance = appearance95;
            this.lblContactNameTM.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblContactNameTM.Location = new System.Drawing.Point(296, 65);
            this.lblContactNameTM.Name = "lblContactNameTM";
            this.lblContactNameTM.Size = new System.Drawing.Size(129, 19);
            this.lblContactNameTM.TabIndex = 60;
            this.lblContactNameTM.Text = "Họ tên người nhận tiền";
            // 
            // txtAccountingObjectAddressTM
            // 
            this.txtAccountingObjectAddressTM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddressTM.Location = new System.Drawing.Point(131, 41);
            this.txtAccountingObjectAddressTM.Name = "txtAccountingObjectAddressTM";
            this.txtAccountingObjectAddressTM.Size = new System.Drawing.Size(715, 21);
            this.txtAccountingObjectAddressTM.TabIndex = 59;
            // 
            // lblAccountingObjectAddressTM
            // 
            appearance96.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectAddressTM.Appearance = appearance96;
            this.lblAccountingObjectAddressTM.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblAccountingObjectAddressTM.Location = new System.Drawing.Point(10, 43);
            this.lblAccountingObjectAddressTM.Name = "lblAccountingObjectAddressTM";
            this.lblAccountingObjectAddressTM.Size = new System.Drawing.Size(115, 19);
            this.lblAccountingObjectAddressTM.TabIndex = 58;
            this.lblAccountingObjectAddressTM.Text = "Địa chỉ";
            // 
            // lblAccountingObjectIDTM
            // 
            appearance97.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectIDTM.Appearance = appearance97;
            this.lblAccountingObjectIDTM.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblAccountingObjectIDTM.Location = new System.Drawing.Point(10, 21);
            this.lblAccountingObjectIDTM.Name = "lblAccountingObjectIDTM";
            this.lblAccountingObjectIDTM.Size = new System.Drawing.Size(115, 19);
            this.lblAccountingObjectIDTM.TabIndex = 57;
            this.lblAccountingObjectIDTM.Text = "Nhà cung cấp (*)";
            // 
            // txtNumberAttachTM
            // 
            this.txtNumberAttachTM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNumberAttachTM.Location = new System.Drawing.Point(131, 112);
            this.txtNumberAttachTM.Name = "txtNumberAttachTM";
            this.txtNumberAttachTM.Size = new System.Drawing.Size(594, 21);
            this.txtNumberAttachTM.TabIndex = 53;
            // 
            // ultraPanel4
            // 
            // 
            // ultraPanel4.ClientArea
            // 
            this.ultraPanel4.ClientArea.Controls.Add(this.pltab_chitiet);
            this.ultraPanel4.ClientArea.Controls.Add(this.pl_ThongTinTheoHinhThucTT);
            this.ultraPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel4.Location = new System.Drawing.Point(0, 79);
            this.ultraPanel4.Name = "ultraPanel4";
            this.ultraPanel4.Size = new System.Drawing.Size(856, 493);
            this.ultraPanel4.TabIndex = 3;
            // 
            // pltab_chitiet
            // 
            // 
            // pltab_chitiet.ClientArea
            // 
            this.pltab_chitiet.ClientArea.Controls.Add(this.pnlGrid);
            this.pltab_chitiet.ClientArea.Controls.Add(this.ultraPanel1);
            this.pltab_chitiet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pltab_chitiet.Location = new System.Drawing.Point(0, 178);
            this.pltab_chitiet.Name = "pltab_chitiet";
            this.pltab_chitiet.Size = new System.Drawing.Size(856, 315);
            this.pltab_chitiet.TabIndex = 5;
            // 
            // pnlGrid
            // 
            this.pnlGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGrid.Location = new System.Drawing.Point(0, 0);
            this.pnlGrid.Name = "pnlGrid";
            this.pnlGrid.Size = new System.Drawing.Size(856, 151);
            this.pnlGrid.TabIndex = 7;
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraPanel2);
            this.ultraPanel1.ClientArea.Controls.Add(this.palBot);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 151);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(856, 164);
            this.ultraPanel1.TabIndex = 6;
            // 
            // ultraPanel2
            // 
            appearance99.TextHAlignAsString = "Left";
            appearance99.TextVAlignAsString = "Middle";
            this.ultraPanel2.Appearance = appearance99;
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel2);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel1);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel21);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel3);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel22);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel23);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel4);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel24);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalDiscountAmountOriginal);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalAmountOriginal);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel17);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalVATAmountOriginal);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel18);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel5);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel19);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel20);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalDiscountAmount);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalAmount);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalVATAmount);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel13);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel14);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel9);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel15);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel16);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(856, 107);
            this.ultraPanel2.TabIndex = 25;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance100.TextHAlignAsString = "Left";
            appearance100.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance100;
            this.ultraLabel2.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel2.Location = new System.Drawing.Point(73, 34);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(131, 17);
            this.ultraLabel2.TabIndex = 1;
            this.ultraLabel2.Text = "Tiền chiết khấu";
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance101.TextHAlignAsString = "Left";
            appearance101.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance101;
            this.ultraLabel1.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel1.Location = new System.Drawing.Point(73, 11);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(131, 17);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Giá mua";
            // 
            // ultraLabel21
            // 
            this.ultraLabel21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance102.TextHAlignAsString = "Right";
            appearance102.TextVAlignAsString = "Middle";
            this.ultraLabel21.Appearance = appearance102;
            this.ultraLabel21.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel21.Location = new System.Drawing.Point(733, 80);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(120, 17);
            this.ultraLabel21.TabIndex = 23;
            this.ultraLabel21.Text = "0";
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance103.TextHAlignAsString = "Left";
            appearance103.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance103;
            this.ultraLabel3.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel3.Location = new System.Drawing.Point(73, 57);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(131, 17);
            this.ultraLabel3.TabIndex = 2;
            this.ultraLabel3.Text = "Tiền thuế GTGT";
            // 
            // ultraLabel22
            // 
            this.ultraLabel22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance104.TextHAlignAsString = "Right";
            appearance104.TextVAlignAsString = "Middle";
            this.ultraLabel22.Appearance = appearance104;
            this.ultraLabel22.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel22.Location = new System.Drawing.Point(733, 57);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(120, 17);
            this.ultraLabel22.TabIndex = 22;
            this.ultraLabel22.Text = "0";
            // 
            // ultraLabel23
            // 
            this.ultraLabel23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance105.TextHAlignAsString = "Right";
            appearance105.TextVAlignAsString = "Middle";
            this.ultraLabel23.Appearance = appearance105;
            this.ultraLabel23.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel23.Location = new System.Drawing.Point(733, 34);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(120, 17);
            this.ultraLabel23.TabIndex = 21;
            this.ultraLabel23.Text = "0";
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance106.TextHAlignAsString = "Left";
            appearance106.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance106;
            this.ultraLabel4.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel4.Location = new System.Drawing.Point(73, 80);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(131, 17);
            this.ultraLabel4.TabIndex = 3;
            this.ultraLabel4.Text = "Tổng tiền thanh toán";
            // 
            // ultraLabel24
            // 
            this.ultraLabel24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance107.TextHAlignAsString = "Right";
            appearance107.TextVAlignAsString = "Middle";
            this.ultraLabel24.Appearance = appearance107;
            this.ultraLabel24.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel24.Location = new System.Drawing.Point(733, 11);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(120, 17);
            this.ultraLabel24.TabIndex = 20;
            this.ultraLabel24.Text = "0";
            // 
            // lblTotalDiscountAmountOriginal
            // 
            this.lblTotalDiscountAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance108.TextHAlignAsString = "Right";
            appearance108.TextVAlignAsString = "Middle";
            this.lblTotalDiscountAmountOriginal.Appearance = appearance108;
            this.lblTotalDiscountAmountOriginal.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalDiscountAmountOriginal.Location = new System.Drawing.Point(210, 34);
            this.lblTotalDiscountAmountOriginal.Name = "lblTotalDiscountAmountOriginal";
            this.lblTotalDiscountAmountOriginal.Size = new System.Drawing.Size(120, 17);
            this.lblTotalDiscountAmountOriginal.TabIndex = 5;
            this.lblTotalDiscountAmountOriginal.Text = "0";
            // 
            // lblTotalAmountOriginal
            // 
            this.lblTotalAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance109.TextHAlignAsString = "Right";
            appearance109.TextVAlignAsString = "Middle";
            this.lblTotalAmountOriginal.Appearance = appearance109;
            this.lblTotalAmountOriginal.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalAmountOriginal.Location = new System.Drawing.Point(210, 11);
            this.lblTotalAmountOriginal.Name = "lblTotalAmountOriginal";
            this.lblTotalAmountOriginal.Size = new System.Drawing.Size(120, 17);
            this.lblTotalAmountOriginal.TabIndex = 4;
            this.lblTotalAmountOriginal.Text = "0";
            // 
            // ultraLabel17
            // 
            this.ultraLabel17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance110.TextHAlignAsString = "Right";
            appearance110.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance110;
            this.ultraLabel17.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel17.Location = new System.Drawing.Point(607, 80);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(120, 17);
            this.ultraLabel17.TabIndex = 19;
            this.ultraLabel17.Text = "0";
            // 
            // lblTotalVATAmountOriginal
            // 
            this.lblTotalVATAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance111.TextHAlignAsString = "Right";
            appearance111.TextVAlignAsString = "Middle";
            this.lblTotalVATAmountOriginal.Appearance = appearance111;
            this.lblTotalVATAmountOriginal.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalVATAmountOriginal.Location = new System.Drawing.Point(210, 57);
            this.lblTotalVATAmountOriginal.Name = "lblTotalVATAmountOriginal";
            this.lblTotalVATAmountOriginal.Size = new System.Drawing.Size(120, 17);
            this.lblTotalVATAmountOriginal.TabIndex = 6;
            this.lblTotalVATAmountOriginal.Text = "0";
            // 
            // ultraLabel18
            // 
            this.ultraLabel18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance112.TextHAlignAsString = "Right";
            appearance112.TextVAlignAsString = "Middle";
            this.ultraLabel18.Appearance = appearance112;
            this.ultraLabel18.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel18.Location = new System.Drawing.Point(607, 57);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(120, 17);
            this.ultraLabel18.TabIndex = 18;
            this.ultraLabel18.Text = "0";
            // 
            // ultraLabel5
            // 
            this.ultraLabel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance113.TextHAlignAsString = "Right";
            appearance113.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance113;
            this.ultraLabel5.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel5.Location = new System.Drawing.Point(210, 80);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(120, 17);
            this.ultraLabel5.TabIndex = 7;
            this.ultraLabel5.Text = "0";
            // 
            // ultraLabel19
            // 
            this.ultraLabel19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance114.TextHAlignAsString = "Right";
            appearance114.TextVAlignAsString = "Middle";
            this.ultraLabel19.Appearance = appearance114;
            this.ultraLabel19.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel19.Location = new System.Drawing.Point(607, 34);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(120, 17);
            this.ultraLabel19.TabIndex = 17;
            this.ultraLabel19.Text = "0";
            // 
            // ultraLabel20
            // 
            this.ultraLabel20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance115.TextHAlignAsString = "Right";
            appearance115.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance115;
            this.ultraLabel20.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel20.Location = new System.Drawing.Point(607, 11);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(120, 17);
            this.ultraLabel20.TabIndex = 16;
            this.ultraLabel20.Text = "0";
            // 
            // lblTotalDiscountAmount
            // 
            this.lblTotalDiscountAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance116.TextHAlignAsString = "Right";
            appearance116.TextVAlignAsString = "Middle";
            this.lblTotalDiscountAmount.Appearance = appearance116;
            this.lblTotalDiscountAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalDiscountAmount.Location = new System.Drawing.Point(336, 34);
            this.lblTotalDiscountAmount.Name = "lblTotalDiscountAmount";
            this.lblTotalDiscountAmount.Size = new System.Drawing.Size(120, 17);
            this.lblTotalDiscountAmount.TabIndex = 9;
            this.lblTotalDiscountAmount.Text = "0";
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance117.TextHAlignAsString = "Right";
            appearance117.TextVAlignAsString = "Middle";
            this.lblTotalAmount.Appearance = appearance117;
            this.lblTotalAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalAmount.Location = new System.Drawing.Point(336, 11);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(120, 17);
            this.lblTotalAmount.TabIndex = 8;
            this.lblTotalAmount.Text = "0";
            // 
            // lblTotalVATAmount
            // 
            this.lblTotalVATAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance118.TextHAlignAsString = "Right";
            appearance118.TextVAlignAsString = "Middle";
            this.lblTotalVATAmount.Appearance = appearance118;
            this.lblTotalVATAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalVATAmount.Location = new System.Drawing.Point(336, 57);
            this.lblTotalVATAmount.Name = "lblTotalVATAmount";
            this.lblTotalVATAmount.Size = new System.Drawing.Size(120, 17);
            this.lblTotalVATAmount.TabIndex = 10;
            this.lblTotalVATAmount.Text = "0";
            // 
            // ultraLabel13
            // 
            this.ultraLabel13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance119.TextHAlignAsString = "Left";
            appearance119.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance119;
            this.ultraLabel13.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel13.Location = new System.Drawing.Point(472, 80);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(129, 17);
            this.ultraLabel13.TabIndex = 15;
            this.ultraLabel13.Text = "Nguyên giá";
            // 
            // ultraLabel14
            // 
            this.ultraLabel14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance120.TextHAlignAsString = "Left";
            appearance120.TextVAlignAsString = "Middle";
            this.ultraLabel14.Appearance = appearance120;
            this.ultraLabel14.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel14.Location = new System.Drawing.Point(472, 57);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(129, 17);
            this.ultraLabel14.TabIndex = 14;
            this.ultraLabel14.Text = "Chi phí mua hàng";
            // 
            // ultraLabel9
            // 
            this.ultraLabel9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance121.TextHAlignAsString = "Right";
            appearance121.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance121;
            this.ultraLabel9.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel9.Location = new System.Drawing.Point(336, 80);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(120, 17);
            this.ultraLabel9.TabIndex = 11;
            this.ultraLabel9.Text = "0";
            // 
            // ultraLabel15
            // 
            this.ultraLabel15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance122.TextHAlignAsString = "Left";
            appearance122.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance122;
            this.ultraLabel15.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel15.Location = new System.Drawing.Point(472, 34);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(129, 17);
            this.ultraLabel15.TabIndex = 13;
            this.ultraLabel15.Text = "Tiền thuế TTĐB";
            // 
            // ultraLabel16
            // 
            this.ultraLabel16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance123.TextHAlignAsString = "Left";
            appearance123.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance123;
            this.ultraLabel16.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel16.Location = new System.Drawing.Point(472, 11);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(129, 17);
            this.ultraLabel16.TabIndex = 12;
            this.ultraLabel16.Text = "Tiền thuế nhập khẩu";
            // 
            // palBot
            // 
            this.palBot.AutoSize = true;
            this.palBot.Controls.Add(this.uGridControl);
            this.palBot.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBot.Location = new System.Drawing.Point(0, 107);
            this.palBot.Name = "palBot";
            this.palBot.Size = new System.Drawing.Size(856, 57);
            this.palBot.TabIndex = 28;
            // 
            // uGridControl
            // 
            this.uGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridControl.Location = new System.Drawing.Point(500, 0);
            this.uGridControl.Name = "uGridControl";
            this.uGridControl.Size = new System.Drawing.Size(356, 54);
            this.uGridControl.TabIndex = 1;
            this.uGridControl.Text = "uGridControl";
            // 
            // cbbAccountingObjectBankAccount
            // 
            appearance124.TextHAlignAsString = "Left";
            this.cbbAccountingObjectBankAccount.Appearance = appearance124;
            appearance125.BackColor = System.Drawing.SystemColors.Window;
            appearance125.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Appearance = appearance125;
            this.cbbAccountingObjectBankAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountingObjectBankAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance126.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance126.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance126.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance126.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectBankAccount.DisplayLayout.GroupByBox.Appearance = appearance126;
            appearance127.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance127;
            this.cbbAccountingObjectBankAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance128.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance128.BackColor2 = System.Drawing.SystemColors.Control;
            appearance128.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance128.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance128;
            this.cbbAccountingObjectBankAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountingObjectBankAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance129.BackColor = System.Drawing.SystemColors.Window;
            appearance129.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.ActiveCellAppearance = appearance129;
            appearance130.BackColor = System.Drawing.SystemColors.Highlight;
            appearance130.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.ActiveRowAppearance = appearance130;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance131.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.CardAreaAppearance = appearance131;
            appearance132.BorderColor = System.Drawing.Color.Silver;
            appearance132.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.CellAppearance = appearance132;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.CellPadding = 0;
            appearance133.BackColor = System.Drawing.SystemColors.Control;
            appearance133.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance133.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance133.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance133.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.GroupByRowAppearance = appearance133;
            appearance134.TextHAlignAsString = "Left";
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.HeaderAppearance = appearance134;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance135.BackColor = System.Drawing.SystemColors.Window;
            appearance135.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.RowAppearance = appearance135;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance136.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance136;
            this.cbbAccountingObjectBankAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectBankAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountingObjectBankAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountingObjectBankAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectBankAccount.Location = new System.Drawing.Point(131, 66);
            this.cbbAccountingObjectBankAccount.Name = "cbbAccountingObjectBankAccount";
            this.cbbAccountingObjectBankAccount.NullText = "<Chọn đối tượng cha>";
            this.cbbAccountingObjectBankAccount.Size = new System.Drawing.Size(157, 22);
            this.cbbAccountingObjectBankAccount.TabIndex = 80;
            // 
            // cbbBankAccountDetailID
            // 
            appearance137.TextHAlignAsString = "Left";
            this.cbbBankAccountDetailID.Appearance = appearance137;
            appearance138.BackColor = System.Drawing.SystemColors.Window;
            appearance138.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbBankAccountDetailID.DisplayLayout.Appearance = appearance138;
            this.cbbBankAccountDetailID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbBankAccountDetailID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance139.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance139.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance139.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance139.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccountDetailID.DisplayLayout.GroupByBox.Appearance = appearance139;
            appearance140.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBankAccountDetailID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance140;
            this.cbbBankAccountDetailID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance141.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance141.BackColor2 = System.Drawing.SystemColors.Control;
            appearance141.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance141.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBankAccountDetailID.DisplayLayout.GroupByBox.PromptAppearance = appearance141;
            this.cbbBankAccountDetailID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbBankAccountDetailID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance142.BackColor = System.Drawing.SystemColors.Window;
            appearance142.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbBankAccountDetailID.DisplayLayout.Override.ActiveCellAppearance = appearance142;
            appearance143.BackColor = System.Drawing.SystemColors.Highlight;
            appearance143.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbBankAccountDetailID.DisplayLayout.Override.ActiveRowAppearance = appearance143;
            this.cbbBankAccountDetailID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbBankAccountDetailID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance144.BackColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccountDetailID.DisplayLayout.Override.CardAreaAppearance = appearance144;
            appearance145.BorderColor = System.Drawing.Color.Silver;
            appearance145.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbBankAccountDetailID.DisplayLayout.Override.CellAppearance = appearance145;
            this.cbbBankAccountDetailID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbBankAccountDetailID.DisplayLayout.Override.CellPadding = 0;
            appearance146.BackColor = System.Drawing.SystemColors.Control;
            appearance146.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance146.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance146.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance146.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccountDetailID.DisplayLayout.Override.GroupByRowAppearance = appearance146;
            appearance147.TextHAlignAsString = "Left";
            this.cbbBankAccountDetailID.DisplayLayout.Override.HeaderAppearance = appearance147;
            this.cbbBankAccountDetailID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbBankAccountDetailID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance148.BackColor = System.Drawing.SystemColors.Window;
            appearance148.BorderColor = System.Drawing.Color.Silver;
            this.cbbBankAccountDetailID.DisplayLayout.Override.RowAppearance = appearance148;
            this.cbbBankAccountDetailID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance149.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbBankAccountDetailID.DisplayLayout.Override.TemplateAddRowAppearance = appearance149;
            this.cbbBankAccountDetailID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbBankAccountDetailID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbBankAccountDetailID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbBankAccountDetailID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbBankAccountDetailID.Location = new System.Drawing.Point(131, 21);
            this.cbbBankAccountDetailID.Name = "cbbBankAccountDetailID";
            this.cbbBankAccountDetailID.NullText = "<Chọn đối tượng cha>";
            this.cbbBankAccountDetailID.Size = new System.Drawing.Size(160, 22);
            this.cbbBankAccountDetailID.TabIndex = 38;
            // 
            // cbbCreditCardNumber
            // 
            editorButton7.Appearance = appearance33;
            editorButton7.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbCreditCardNumber.ButtonsRight.Add(editorButton7);
            this.cbbCreditCardNumber.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCreditCardNumber.Location = new System.Drawing.Point(131, 22);
            this.cbbCreditCardNumber.Name = "cbbCreditCardNumber";
            this.cbbCreditCardNumber.NullText = "<chọn dữ liệu>";
            this.cbbCreditCardNumber.Size = new System.Drawing.Size(157, 22);
            this.cbbCreditCardNumber.TabIndex = 85;
            // 
            // FFAIncrementBuyDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 572);
            this.Controls.Add(this.ultraPanel4);
            this.Controls.Add(this.ultraPanel_KhungTren);
            this.Name = "FFAIncrementBuyDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FFAIncrementDetail";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ultraPanel_KhungTren.ClientArea.ResumeLayout(false);
            this.ultraPanel_KhungTren.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ultraGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChonThanhToan)).EndInit();
            this.palTopVouchers.ResumeLayout(false);
            this.palTopVouchers.PerformLayout();
            this.pl_ThongTinTheoHinhThucTT.ClientArea.ResumeLayout(false);
            this.pl_ThongTinTheoHinhThucTT.ResumeLayout(false);
            this.ultraPanel10.ClientArea.ResumeLayout(false);
            this.ultraPanel10.ResumeLayout(false);
            this.pnSCK.ClientArea.ResumeLayout(false);
            this.pnSCK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxDonViTraTienSCK)).EndInit();
            this.ultraGroupBoxDonViTraTienSCK.ResumeLayout(false);
            this.ultraGroupBoxDonViTraTienSCK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetailIDSCK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankNameSCK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonSCK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            this.ultraPanel_SCK.ClientArea.ResumeLayout(false);
            this.ultraPanel_SCK.ResumeLayout(false);
            this.ultraPanel8.ClientArea.ResumeLayout(false);
            this.ultraPanel8.ClientArea.PerformLayout();
            this.ultraPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameSCK)).EndInit();
            this.ultraPanel9.ClientArea.ResumeLayout(false);
            this.ultraPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccountSCK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDSCK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectBankNameSCK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressSCK)).EndInit();
            this.pnTTD.ClientArea.ResumeLayout(false);
            this.pnTTD.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxHoaDonTTD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1TTD)).EndInit();
            this.ultraGroupBox1TTD.ResumeLayout(false);
            this.ultraGroupBox1TTD.PerformLayout();
            this.ultraPanel11.ClientArea.ResumeLayout(false);
            this.ultraPanel11.ResumeLayout(false);
            this.ultraPanel12.ClientArea.ResumeLayout(false);
            this.ultraPanel12.ClientArea.PerformLayout();
            this.ultraPanel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameTTD)).EndInit();
            this.ultraPanel13.ClientArea.ResumeLayout(false);
            this.ultraPanel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccountTTD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDTTD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectBankNameTTD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressTTD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxDonViTraTienTTD)).EndInit();
            this.ultraGroupBoxDonViTraTienTTD.ResumeLayout(false);
            this.ultraGroupBoxDonViTraTienTTD.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardNumberTTD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonTTD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grCTT)).EndInit();
            this.grCTT.ResumeLayout(false);
            this.grCTT.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).EndInit();
            this.ultraPanel5.ClientArea.ResumeLayout(false);
            this.ultraPanel5.ResumeLayout(false);
            this.ultraPanel6.ClientArea.ResumeLayout(false);
            this.ultraPanel6.ClientArea.PerformLayout();
            this.ultraPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            this.ultraPanel7.ClientArea.ResumeLayout(false);
            this.ultraPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).EndInit();
            this.pnSTM.ClientArea.ResumeLayout(false);
            this.pnSTM.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxHoaDonSTM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1STM)).EndInit();
            this.ultraGroupBox1STM.ResumeLayout(false);
            this.ultraGroupBox1STM.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteIssueDate)).EndInit();
            this.ultraPanel1STM.ClientArea.ResumeLayout(false);
            this.ultraPanel1STM.ResumeLayout(false);
            this.ultraPanel3STM.ClientArea.ResumeLayout(false);
            this.ultraPanel3STM.ClientArea.PerformLayout();
            this.ultraPanel3STM.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameSTM)).EndInit();
            this.ultraPanelSTM2.ClientArea.ResumeLayout(false);
            this.ultraPanelSTM2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDSTM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactNameSTM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxDonViTraTienSTM)).EndInit();
            this.ultraGroupBoxDonViTraTienSTM.ResumeLayout(false);
            this.ultraGroupBoxDonViTraTienSTM.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetailIDSTM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankNameSTM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonSTM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grTM)).EndInit();
            this.grTM.ResumeLayout(false);
            this.grTM.PerformLayout();
            this.ultraPanel2TM.ClientArea.ResumeLayout(false);
            this.ultraPanel2TM.ResumeLayout(false);
            this.ultraPanel3TM.ClientArea.ResumeLayout(false);
            this.ultraPanel3TM.ClientArea.PerformLayout();
            this.ultraPanel3TM.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameTM)).EndInit();
            this.ultraPanel4TM.ClientArea.ResumeLayout(false);
            this.ultraPanel4TM.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDTM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodeTM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonTM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactNameTM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressTM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberAttachTM)).EndInit();
            this.ultraPanel4.ClientArea.ResumeLayout(false);
            this.ultraPanel4.ResumeLayout(false);
            this.pltab_chitiet.ClientArea.ResumeLayout(false);
            this.pltab_chitiet.ResumeLayout(false);
            this.pnlGrid.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ClientArea.PerformLayout();
            this.ultraPanel1.ResumeLayout(false);
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            this.palBot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetailID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardNumber)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanel_KhungTren;
        private Infragistics.Win.Misc.UltraPanel pl_ThongTinTheoHinhThucTT;
        private Infragistics.Win.Misc.UltraPanel ultraPanel4;
        private Infragistics.Win.Misc.UltraPanel pltab_chitiet;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel lblTotalVATAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalDiscountAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalAmount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel lblTotalVATAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel lblTotalDiscountAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel lblTotalAmountOriginal;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        public Infragistics.Win.Misc.UltraGroupBox grCTT;
        public Infragistics.Win.Misc.UltraPanel ultraPanel5;
        public Infragistics.Win.Misc.UltraPanel ultraPanel6;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        public Infragistics.Win.Misc.UltraPanel ultraPanel7;
        public Infragistics.Win.Misc.UltraButton uButtonCongNo;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxCode;
        public Infragistics.Win.Misc.UltraLabel lblCompanyTaxCode;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        public Infragistics.Win.Misc.UltraLabel lblReason;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress;
        public Infragistics.Win.Misc.UltraLabel lblAccountingObjectAddress;
        public Infragistics.Win.Misc.UltraLabel lblAccountingObjectID;
        public Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectBankAccount;
        public Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccountDetailID;
        public Infragistics.Win.UltraWinGrid.UltraCombo cbbCreditCardNumber;
        private Infragistics.Win.Misc.UltraPanel ultraPanel10;
        private Infragistics.Win.Misc.UltraPanel pnSCK;
        public Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxDonViTraTienSCK;
        public Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccountDetailIDSCK;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankNameSCK;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtReasonSCK;
        public Infragistics.Win.Misc.UltraLabel lblReasonSCK;
        public Infragistics.Win.Misc.UltraLabel lblBankAccountDetailIDSCK;
        public Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        public Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        public Infragistics.Win.Misc.UltraPanel ultraPanel_SCK;
        public Infragistics.Win.Misc.UltraPanel ultraPanel8;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameSCK;
        public Infragistics.Win.Misc.UltraPanel ultraPanel9;
        public Infragistics.Win.Misc.UltraButton uButtonCongNoSCK;
        public Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectBankAccountSCK;
        public Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDSCK;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectBankNameSCK;
        public Infragistics.Win.Misc.UltraLabel lblAccountingObjectBankAccountSCK;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressSCK;
        public Infragistics.Win.Misc.UltraLabel lblAccountingObjectAddressSCK;
        public Infragistics.Win.Misc.UltraLabel lblAccountingObjectIDSCK;
        private Infragistics.Win.Misc.UltraPanel pnSTM;
        public Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxHoaDonSTM;
        public Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1STM;
        public Infragistics.Win.Misc.UltraPanel ultraPanel1STM;
        public Infragistics.Win.Misc.UltraPanel ultraPanel3STM;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameSTM;
        public Infragistics.Win.Misc.UltraPanel ultraPanelSTM2;
        public Infragistics.Win.Misc.UltraButton uButtonCongNoSTM;
        public Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit mkeIdentificationNoSTM;
        public Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDSTM;
        public Infragistics.Win.Misc.UltraLabel lblIdentificationNoSTM;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactNameSTM;
        public Infragistics.Win.Misc.UltraLabel lblContactNameSTM;
        public Infragistics.Win.Misc.UltraLabel lblAccountingObjectIDSTM;
        public Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxDonViTraTienSTM;
        public Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccountDetailIDSTM;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankNameSTM;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtReasonSTM;
        public Infragistics.Win.Misc.UltraLabel lblReasonSTM;
        public Infragistics.Win.Misc.UltraLabel lblBankAccountDetailIDSTM;
        private Infragistics.Win.Misc.UltraPanel pnTTD;
        public Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxHoaDonTTD;
        public Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1TTD;
        public Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectBankAccountTTD;
        public Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDTTD;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectBankNameTTD;
        public Infragistics.Win.Misc.UltraLabel lblAccountingObjectBankAccountTTD;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressTTD;
        public Infragistics.Win.Misc.UltraLabel lblAccountingObjectAddressTTD;
        public Infragistics.Win.Misc.UltraLabel lblAccountingObjectIDTTD;
        public Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxDonViTraTienTTD;
        public Infragistics.Win.Misc.UltraLabel lblOwnerCardTTD;
        public Infragistics.Win.Misc.UltraLabel lblCreditCardTypeTTD;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtReasonTTD;
        public Infragistics.Win.Misc.UltraLabel lblReasonTTD;
        public Infragistics.Win.Misc.UltraLabel lblBankAccountDetailIDTTD;
        public Infragistics.Win.Misc.UltraGroupBox grTM;
        public Infragistics.Win.Misc.UltraPanel ultraPanel2TM;
        public Infragistics.Win.Misc.UltraPanel ultraPanel3TM;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameTM;
        public Infragistics.Win.Misc.UltraPanel ultraPanel4TM;
        public Infragistics.Win.Misc.UltraButton uButtonCongNoTM;
        public Infragistics.Win.Misc.UltraLabel lblChungTuGocTM;
        public Infragistics.Win.Misc.UltraLabel lblNumberAttachTM;
        public Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDTM;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxCodeTM;
        public Infragistics.Win.Misc.UltraLabel lblCompanyTaxCodeTM;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtReasonTM;
        public Infragistics.Win.Misc.UltraLabel lblReasonTM;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactNameTM;
        public Infragistics.Win.Misc.UltraLabel lblContactNameTM;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressTM;
        public Infragistics.Win.Misc.UltraLabel lblAccountingObjectAddressTM;
        public Infragistics.Win.Misc.UltraLabel lblAccountingObjectIDTM;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtNumberAttachTM;
        public Infragistics.Win.Misc.UltraPanel ultraPanel11;
        public Infragistics.Win.Misc.UltraPanel ultraPanel12;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameTTD;
        public Infragistics.Win.Misc.UltraPanel ultraPanel13;
        public Infragistics.Win.Misc.UltraButton ultraButton1;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtIssueBy;
        public Infragistics.Win.Misc.UltraLabel lblIssueBy;
        public Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteIssueDate;
        public Infragistics.Win.Misc.UltraLabel lblIssueDate;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID;
        public Infragistics.Win.UltraWinGrid.UltraCombo cbbCreditCardNumberTTD;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraPanel palTopVouchers;
        public System.Windows.Forms.RadioButton rbt_Thanhtoanngay;
        public System.Windows.Forms.RadioButton rbt_Chuathanhtoan;
        public Infragistics.Win.UltraWinEditors.UltraComboEditor cbbChonThanhToan;
        private Infragistics.Win.Misc.UltraPanel pnlGrid;
        private System.Windows.Forms.Panel palBot;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridControl;
    }
}