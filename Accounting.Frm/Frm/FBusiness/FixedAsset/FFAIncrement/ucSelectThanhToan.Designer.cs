﻿namespace Accounting.FBusiness.FFixedAsset.FFAIncrement
{
    partial class ucSelectThanhToan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.cbbChonThanhToan = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.palChung = new System.Windows.Forms.Panel();
            this.rbt_Thanhtoanngay = new System.Windows.Forms.RadioButton();
            this.rbt_Chuathanhtoan = new System.Windows.Forms.RadioButton();
            this.ultraGroupBoxHoaDon = new Infragistics.Win.Misc.UltraGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChonThanhToan)).BeginInit();
            this.palChung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxHoaDon)).BeginInit();
            this.ultraGroupBoxHoaDon.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbbChonThanhToan
            // 
            this.cbbChonThanhToan.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbChonThanhToan.Location = new System.Drawing.Point(250, 3);
            this.cbbChonThanhToan.Name = "cbbChonThanhToan";
            this.cbbChonThanhToan.Size = new System.Drawing.Size(165, 21);
            this.cbbChonThanhToan.TabIndex = 36;
            // 
            // palChung
            // 
            this.palChung.BackColor = System.Drawing.Color.Transparent;
            this.palChung.Controls.Add(this.rbt_Thanhtoanngay);
            this.palChung.Controls.Add(this.rbt_Chuathanhtoan);
            this.palChung.Controls.Add(this.cbbChonThanhToan);
            this.palChung.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palChung.Location = new System.Drawing.Point(3, 19);
            this.palChung.Name = "palChung";
            this.palChung.Size = new System.Drawing.Size(418, 26);
            this.palChung.TabIndex = 40;
            // 
            // rbt_Thanhtoanngay
            // 
            this.rbt_Thanhtoanngay.AutoSize = true;
            this.rbt_Thanhtoanngay.Location = new System.Drawing.Point(138, 5);
            this.rbt_Thanhtoanngay.Name = "rbt_Thanhtoanngay";
            this.rbt_Thanhtoanngay.Size = new System.Drawing.Size(106, 17);
            this.rbt_Thanhtoanngay.TabIndex = 38;
            this.rbt_Thanhtoanngay.TabStop = true;
            this.rbt_Thanhtoanngay.Text = "Thanh toán ngay";
            this.rbt_Thanhtoanngay.UseVisualStyleBackColor = true;
            // 
            // rbt_Chuathanhtoan
            // 
            this.rbt_Chuathanhtoan.AutoSize = true;
            this.rbt_Chuathanhtoan.Location = new System.Drawing.Point(3, 5);
            this.rbt_Chuathanhtoan.Name = "rbt_Chuathanhtoan";
            this.rbt_Chuathanhtoan.Size = new System.Drawing.Size(104, 17);
            this.rbt_Chuathanhtoan.TabIndex = 37;
            this.rbt_Chuathanhtoan.TabStop = true;
            this.rbt_Chuathanhtoan.Text = "Chưa thanh toán";
            this.rbt_Chuathanhtoan.UseVisualStyleBackColor = true;
            // 
            // ultraGroupBoxHoaDon
            // 
            this.ultraGroupBoxHoaDon.Controls.Add(this.palChung);
            this.ultraGroupBoxHoaDon.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance1.FontData.BoldAsString = "True";
            appearance1.FontData.SizeInPoints = 10F;
            this.ultraGroupBoxHoaDon.HeaderAppearance = appearance1;
            this.ultraGroupBoxHoaDon.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxHoaDon.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxHoaDon.Name = "ultraGroupBoxHoaDon";
            this.ultraGroupBoxHoaDon.Size = new System.Drawing.Size(424, 48);
            this.ultraGroupBoxHoaDon.TabIndex = 45;
            this.ultraGroupBoxHoaDon.Text = "Hình thức thanh toán";
            this.ultraGroupBoxHoaDon.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ucSelectThanhToan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ultraGroupBoxHoaDon);
            this.Name = "ucSelectThanhToan";
            this.Size = new System.Drawing.Size(424, 48);
            ((System.ComponentModel.ISupportInitialize)(this.cbbChonThanhToan)).EndInit();
            this.palChung.ResumeLayout(false);
            this.palChung.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxHoaDon)).EndInit();
            this.ultraGroupBoxHoaDon.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public Infragistics.Win.UltraWinEditors.UltraComboEditor cbbChonThanhToan;
        private System.Windows.Forms.Panel palChung;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxHoaDon;
        public System.Windows.Forms.RadioButton rbt_Thanhtoanngay;
        public System.Windows.Forms.RadioButton rbt_Chuathanhtoan;

    }
}
