﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Accounting.Core.IService;
using FX.Core;

namespace Accounting
{
    public partial class FFASelectPPServices : CustormForm
    {
        private IPPInvoiceDetailCostService _IPPInvoiceDetailCostService { get { return IoC.Resolve<IPPInvoiceDetailCostService>(); } }
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        private DateTime _startDate;
        int statusForm;
        bool type;
        public List<PPServices> SelectPPServices { get; private set; }
        private List<PPInvoiceDetailCost> _ppInvoiceDetailCosts = new List<PPInvoiceDetailCost>();
        public FFASelectPPServices(FAIncrement ppInvoice, int _statusForm, bool _type)
        {
            InitializeComponent();
            statusForm = _statusForm;
            type = _type;
            _ppInvoiceDetailCosts = ppInvoice.PPInvoiceDetailCosts.ToList();
            btnGetData.Click += (s, e) => btnGetData_Click(s, e, ppInvoice);
            ConfigControl(ppInvoice);
        }

        private void ConfigControl(FAIncrement ppInvoice)
        {

            var stringToDateTime = Utils.StringToDateTime(ConstFrm.DbStartDate);
            if (stringToDateTime != null)
                _startDate = (DateTime)stringToDateTime;
            else _startDate = DateTime.Now;
            cbbAboutTime.ConfigComboSelectTime(_lstItems, "Name");
            cbbAboutTime.SelectedRow = cbbAboutTime.Rows[4];

            var result = Utils.IPPServiceService.Query.Where(x => x.Date >= dteDateFrom.DateTime && x.Date <= dteDateTo.DateTime /*&& x.PPInvoiceID == null*/ && x.IsFeightService && x.Recorded).OrderByDescending(s => s.PostedDate).ThenByDescending(s => s.No).ToList();
            Utils.IPPServiceService.UnbindSession(result);
            result = Utils.IPPServiceService.Query.Where(x => x.Date >= dteDateFrom.DateTime && x.Date <= dteDateTo.DateTime /*&& x.PPInvoiceID == null*/ && x.IsFeightService && x.Recorded).OrderByDescending(s => s.PostedDate).ThenByDescending(s => s.No).ToList();
            List<PPServices> lstPPServices = new List<PPServices>();
            foreach (PPService pp in result)
            {
                PPServices pps = new PPServices();
                pps.ID = pp.ID;
                pps.AccountingObjectID = pp.AccountingObjectID;
                pps.AccountingObjectName = pp.AccountingObjectName;
                pps.No = pp.No;
                pps.Reason = pp.Reason;
                pps.PostedDate = pp.PostedDate;
                pps.Date = pp.Date;
                pps.TotalAmount = pp.TotalAmount;
                pps.TotalAmountOriginal = pp.TotalAmountOriginal;
                pps.FreightAmount = (pps.TotalAmount - (_ppInvoiceDetailCosts.Any(x => x.PPServiceID == pps.ID && x.CostType != type) ? (_ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID && x.CostType != type)].AmountPB ?? 0) : 0) - (statusForm == 3 ? _IPPInvoiceDetailCostService.GetAmountByPPInvoice(pps.ID, ppInvoice.ID, type) : _IPPInvoiceDetailCostService.GetAmountByPPService(pps.ID)));
                pps.Amount = pps.FreightAmount;
                pps.AccumulatedAllocateAmount = ((_ppInvoiceDetailCosts.Any(x => x.PPServiceID == pps.ID && x.CostType != type) ? (_ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID && x.CostType != type)].AmountPB ?? 0) : 0) + (statusForm == 3 ? _IPPInvoiceDetailCostService.GetAmountByPPInvoice(pps.ID, ppInvoice.ID, type) : _IPPInvoiceDetailCostService.GetAmountByPPService(pps.ID)));
                pps.AccumulatedAllocateAmountOriginal = ((_ppInvoiceDetailCosts.Any(x => x.PPServiceID == pps.ID && x.CostType != type) ? (_ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID && x.CostType != type)].AmountPB ?? 0) : 0) + (statusForm == 3 ? _IPPInvoiceDetailCostService.GetAmountByPPInvoice(pps.ID, ppInvoice.ID, type) : _IPPInvoiceDetailCostService.GetAmountByPPService(pps.ID))) / ppInvoice.ExchangeRate;
                if (Convert.ToDecimal(pps.Amount) != 0M)
                    lstPPServices.Add(pps);
            }            
            ConfigGrid(lstPPServices);
            foreach (var row in uGrid.Rows)
            {
                row.Cells["CheckColumn"].Value = _ppInvoiceDetailCosts.Any(x => x.PPServiceID == (Guid)row.Cells["ID"].Value && x.CostType == type);
                if ((bool)row.Cells["CheckColumn"].Value == true)
                {
                    row.Cells["FreightAmount"].Value = _ppInvoiceDetailCosts.Find(x => x.PPServiceID == (Guid)row.Cells["ID"].Value && x.CostType == type).AmountPB;
                }
            }
        }

        private void ConfigGrid(List<PPServices> input)
        {
            uGrid.AfterExitEditMode += uGrid_AfterExitEditMode;
            uGrid.DataSource = input.Where(n => n.FreightAmount > 0).ToList();
            Utils.ConfigGrid(uGrid, ConstDatabase.SelectPPService_TableName, false);
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.UseFixedHeaders = false;
            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid);
                if (new string[] { "TypeID", "Date", "PostedDate", "No", "Amount", "TotalAmount", "AccountingObjectID", "AccountingObjectName", "AccountingObjectAddress", "Reason", "InvoiceNo" }.Contains(column.Key))
                {
                    column.CellActivation = Activation.NoEdit;
                }
            }
            uGrid.DisplayLayout.Bands[0].Columns["CheckColumn"].Header.VisiblePosition = 0;
            UltraGridColumn ugc = uGrid.DisplayLayout.Bands[0].Columns["CheckColumn"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnGetData_Click(object sender, EventArgs e, FAIncrement ppInvoice)
        {
            var result = Utils.IPPServiceService.Query.Where(x => x.Date >= dteDateFrom.DateTime && x.Date <= dteDateTo.DateTime /*&& x.PPInvoiceID == null*/ && x.IsFeightService && x.Recorded).OrderByDescending(s => s.PostedDate).ThenByDescending(s => s.No).ToList();
            Utils.IPPServiceService.UnbindSession(result);
            result = Utils.IPPServiceService.Query.Where(x => x.Date >= dteDateFrom.DateTime && x.Date <= dteDateTo.DateTime /*&& x.PPInvoiceID == null*/ && x.IsFeightService && x.Recorded).OrderByDescending(s => s.PostedDate).ThenByDescending(s => s.No).ToList();
            List<PPServices> lstPPServices = new List<PPServices>();
            foreach (PPService pp in result)
            {
                PPServices pps = new PPServices();
                pps.ID = pp.ID;
                pps.AccountingObjectID = pp.AccountingObjectID;
                pps.AccountingObjectName = pp.AccountingObjectName;
                pps.No = pp.No;
                pps.Reason = pp.Reason;
                pps.PostedDate = pp.PostedDate;
                pps.Date = pp.Date;
                pps.TotalAmount = pp.TotalAmount;
                pps.TotalAmountOriginal = pp.TotalAmountOriginal;
                pps.FreightAmount = (pps.TotalAmount - (_ppInvoiceDetailCosts.Any(x => x.PPServiceID == pps.ID && x.CostType != type) ? (_ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID && x.CostType != type)].AmountPB ?? 0) : 0) - (statusForm == 3 ? _IPPInvoiceDetailCostService.GetAmountByPPInvoice(pps.ID, ppInvoice.ID, type) : _IPPInvoiceDetailCostService.GetAmountByPPService(pps.ID)));
                pps.Amount = pps.FreightAmount;
                pps.AccumulatedAllocateAmount = ((_ppInvoiceDetailCosts.Any(x => x.PPServiceID == pps.ID && x.CostType != type) ? (_ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID && x.CostType != type)].AmountPB ?? 0) : 0) + (statusForm == 3 ? _IPPInvoiceDetailCostService.GetAmountByPPInvoice(pps.ID, ppInvoice.ID, type) : _IPPInvoiceDetailCostService.GetAmountByPPService(pps.ID)));
                pps.AccumulatedAllocateAmountOriginal = ((_ppInvoiceDetailCosts.Any(x => x.PPServiceID == pps.ID && x.CostType != type) ? (_ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID && x.CostType != type)].AmountPB ?? 0) : 0) + (statusForm == 3 ? _IPPInvoiceDetailCostService.GetAmountByPPInvoice(pps.ID, ppInvoice.ID, type) : _IPPInvoiceDetailCostService.GetAmountByPPService(pps.ID))) / ppInvoice.ExchangeRate;
                if (Convert.ToDecimal(pps.Amount) != 0M)
                    lstPPServices.Add(pps);
            }
            
            ConfigGrid(lstPPServices.ToList());
            foreach (var row in uGrid.Rows)
            {
                row.Cells["CheckColumn"].Value = _ppInvoiceDetailCosts.Any(x => x.PPServiceID == (Guid)row.Cells["ID"].Value && x.CostType == type);
                if ((bool)row.Cells["CheckColumn"].Value == true)
                {
                    row.Cells["FreightAmount"].Value = _ppInvoiceDetailCosts.Find(x => x.PPServiceID == (Guid)row.Cells["ID"].Value && x.CostType == type).AmountPB;
                }
            }
        }

        private void cbbAboutTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAboutTime.SelectedRow != null)
            {
                var model = cbbAboutTime.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(_startDate.Year,_startDate, model, out dtBegin, out dtEnd);
                dteDateFrom.DateTime = dtBegin;
                dteDateTo.DateTime = dtEnd;
            }
        }

        private void dteDateFrom_ValueChanged(object sender, EventArgs e)
        {
            if (ActiveControl != null && ActiveControl.Name == ((UltraDateTimeEditor)sender).Name)
                cbbAboutTime.SelectedRow = cbbAboutTime.Rows[34];
        }

        private void dteDateTo_ValueChanged(object sender, EventArgs e)
        {
            if (ActiveControl != null && ActiveControl.Name == ((UltraDateTimeEditor)sender).Name)
                cbbAboutTime.SelectedRow = cbbAboutTime.Rows[34];
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            var lstPPServices = from u in (List<PPServices>)uGrid.DataSource where u.CheckColumn select u;
            foreach (PPServices pps in lstPPServices)
            {
                if (pps.FreightAmount > pps.Amount)
                {
                    MSG.Warning("Số tiền phân bổ lần này không được vượt quá số tiền chưa phân bổ. Vui lòng kiểm tra lại!");
                    return;
                }
            }
            SelectPPServices = lstPPServices.ToList();
            DialogResult = DialogResult.OK;
            Close();
        }
        private void uGrid_AfterExitEditMode(object sender, EventArgs e)
        {
            AfterExitEditModeBill();
        }
        public void AfterExitEditModeBill()
        {
            UltraGridCell cell = uGrid.ActiveCell;
            switch (cell.Column.Key)
            {
                case "FreightAmount":
                    if (!cell.Row.Cells["FreightAmount"].IsFilterRowCell)
                    {
                        if (cell.Row.Cells["FreightAmount"].Value != null && cell.Row.Cells["Amount"].Value != null)
                        {
                            if ((decimal)cell.Row.Cells["FreightAmount"].Value > (decimal)cell.Row.Cells["Amount"].Value)
                                Utils.NotificationCell(uGrid, cell, "Số tiền phân bổ lần này không được vượt quá số tiền chưa phân bổ");
                            else
                                Utils.RemoveNotificationCell(uGrid, cell);
                        }
                    }
                    break;
            }
        }
    }
}