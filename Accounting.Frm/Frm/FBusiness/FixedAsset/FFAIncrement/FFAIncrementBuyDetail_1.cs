﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Frm.TextMessage;
using Castle.Core.Resource;
using FX.Core;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win;
using AutoCompleteMode = Infragistics.Win.AutoCompleteMode;

namespace Accounting.Frm
{
    public partial class FFAIncrementBuyDetail : FSFAIncrementBuyDetailBase
    {
        #region Khai báo
        #region Server khởi tạo

        private IFixedAssetService FixedAssetService
        {
            get { return IoC.Resolve<IFixedAssetService>(); }
        }

        private IGoodsServicePurchaseService GoodsServicePurchaseService
        {
            get { return IoC.Resolve<IGoodsServicePurchaseService>(); }
        }
        private IInvoiceTypeService invoiceTypeService
        {
            get { return IoC.Resolve<IInvoiceTypeService>(); }
        }
        #endregion
        #region teamplate
        Template template = new Template();
        #endregion
        readonly FAIncrement _faincrement = new FAIncrement();
        readonly FAIncrementDetail _faicremendetail = new FAIncrementDetail();
        List<FAIncrementDetail> _lstFaIncrementDetail = new List<FAIncrementDetail>();
        BindingList<FAIncrementDetail> _bdlFaIncrementDetails = new BindingList<FAIncrementDetail>();
        int startFrom;

        bool _kiemtraHinhthucThanhToan;

        readonly List<string> _lstFaIncrementDetailAmount = new List<string>() { "Amount",
            "CustomUnitPrice", "DiscountAmount", "FreightAmount", "ImportTaxAmount", 
            "ImportTaxExpenseAmount", "OrgPrice", "SpecialConsumeTaxAmount", "VATAmount" };
        readonly List<string> _lstFaIncrementDetailAmountOriginal = new List<string>() { "AmountOriginal",
            "CustomUnitPriceOriginal", "DiscountAmountOriginal", "FreightAmountOriginal","ImportTaxAmountOriginal", 
            "ImportTaxExpenseAmountOriginal", "OrgPriceOriginal", "SpecialConsumeTaxAmountOriginal", "VATAmountOriginal" };
        List<string> dscotduochienthi0 = new List<string>();
        List<string> dscotduochienthi1 = new List<string>();
        List<string> dscotduochienthi2 = new List<string>();
        #region vùng thông tin chung
        #region đối tượng
        readonly AccountingObject _accountingObject = new AccountingObject();
        readonly BankAccountDetail _bankAccountDetail = new BankAccountDetail();
        readonly AccountingObjectBankAccount _accountingObjectBankAccount = new AccountingObjectBankAccount();
        readonly CreditCard creditCard = new CreditCard();
        #endregion
        #region list đối tượng

        List<AccountingObject> _lstAccountingObject = new List<AccountingObject>();
        List<BankAccountDetail> _lstBankAccountDetail = new List<BankAccountDetail>();
        readonly List<AccountingObjectBankAccount> _lstAccountingObjectBankAccount = new List<AccountingObjectBankAccount>();
        List<CreditCard> _lstCreditCard = new List<CreditCard>();
        #endregion
        #region kiểm tra
        bool _isRunAccountingObject = false;
        bool _isRunBankAccountDetail = false;
        bool _isRunAccountingObjectBankAccount = false;
        bool _isRunCreditCrad = false;
        #endregion
        #endregion

        #region vùng tiền
        #region combobox vùng tiền
        readonly UltraCombo cbbAccountingObject_NhanVien = new UltraCombo();
        readonly UltraComboEditor _cbbIsImportPurchaseFixCung = new UltraComboEditor();
        readonly UltraCombo _cbbCurrency = new UltraCombo();

        readonly UltraCombo _cbbPaymentClause = new UltraCombo();
        readonly UltraCombo _cbbTransportMethod = new UltraCombo();
        #endregion
        #region list vùng tiền
        List<AccountingObject> _lstAccountingObjectNhanVien = new List<AccountingObject>();
        List<Currency> _lstCurrency = new List<Currency>();
        List<PaymentClause> _lstPaymentClause = new List<PaymentClause>();
        List<TransportMethod> _lstTransportMethod = new List<TransportMethod>();

        #endregion
        #region biến check
        private bool _checkCbbCurrencyOpen;
        #endregion

        #endregion

        #region vùng hàng tiền
        #region combobox

        readonly UltraCombo _cbbFixedAsset = new UltraCombo();
        readonly UltraCombo _cbbDepartment = new UltraCombo();
        readonly UltraCombo _cbbAccountNo = new UltraCombo();
        readonly UltraCombo _cbbAccountCo = new UltraCombo();
        readonly UltraCombo _cbbAccountingObjectHangtien = new UltraCombo();
        #endregion

        #region list đối tượng
        List<FixedAsset> _lstFixedAsset = new List<FixedAsset>();
        List<Department> _lstDepartment = new List<Department>();
        List<Account> _lstAccountDefaultNo = new List<Account>();
        List<Account> _lstAccountDefaultCo = new List<Account>();
        #endregion

        #region biến check

        #endregion
        #endregion

        #region vùng thuế
        #region combobox
        UltraComboEditor _cbbThueSuatGtgtVatRate = new UltraComboEditor();
        UltraCombo _cbbAccountsVatAccountThueGtgt = new UltraCombo();
        UltraCombo _cbbLoaiHdInvoceTypeId = new UltraCombo();
        UltraCombo _cbbGoodsServicePurchases = new UltraCombo();
        UltraCombo _cbbAccountsImportTaxAccountThueNk = new UltraCombo();
        UltraCombo _cbbAccountsSpecialConsumeTaxAccountThueTtdb = new UltraCombo();
        UltraCombo _cbbAccountsDeductionDebitAccount = new UltraCombo();
        UltraCombo _cbbAccountingObject = new UltraCombo();
        UltraComboEditor _cbbInvoiceTypeLoaiHD = new UltraComboEditor();
        #endregion

        #region list đối tượng
        List<Account> _lstAccountsVatAccountThueGtgt = new List<Account>();
        List<GoodsServicePurchase> _lstGoodsServicePurchases = new List<GoodsServicePurchase>();
        List<Account> _lstAccountsImportTaxAccountThueNk = new List<Account>();
        List<Account> _lstAccountsSpecialConsumeTaxAccountThueTtdb = new List<Account>();
        List<Account> _lstAccountsDeductionDebitAccount = new List<Account>();
        List<InvoiceType> _lstInvoiceTypeLoaiHD = new List<InvoiceType>(); 
        #endregion

        #region biến check combobox

        #endregion

        #endregion

        #region vùng thống kê
        #region combobox
        readonly UltraCombo _cbbBudgetItemsThuChi = new UltraCombo();
        readonly UltraCombo _cbbExpenseItemsChiPhi = new UltraCombo();
        readonly UltraCombo _cbbCostSetDtTapHopChiPhi = new UltraCombo();
        readonly UltraCombo _cbbContractStateHopDong = new UltraCombo();
        readonly UltraCombo _cbbStatisticsCodeThongKe = new UltraCombo();
        #endregion
        #region list đối tượng sử dụng
        List<BudgetItem> _lstBudgetItemsThuChi = new List<BudgetItem>();
        List<ExpenseItem> _lstExpenseItemsChiPhi = new List<ExpenseItem>();
        List<CostSet> _lstCostSetsDtTapHopChiPhi = new List<CostSet>();
        List<EMContract> _lstContractStatesHopDong = new List<EMContract>();
        List<StatisticsCode> _lstStatisticsCodesThongKe = new List<StatisticsCode>();
        #endregion
        #endregion

        #endregion

        #region Khởi tạo
        public FFAIncrementBuyDetail(FAIncrement faIncrement, IEnumerable<FAIncrement> dsFaIncrement, int status)
        {
            LoadChung();
            startFrom = status;
            if (startFrom == 2) //thêm
            {

            }
            else if (startFrom == 1) //xem
            {
                _faincrement = faIncrement;
            }
        }
        #endregion

        #region Overide

        private void LoadChung()
        {
            InitializeComponent();
            base.InitializeComponent();
            #region khai báo các list sử dụng
            _lstAccountingObject = IAccountingObjectService.GetAccountingObjects(1, true);
            _lstBankAccountDetail = IBankAccountDetailService.GetIsActive(true);
            //lstAccountingObjectBankAccount = IAccountingObjectBankAccountService.GetAll();
            _lstCreditCard = ICreditCardService.GetAll_IsActive(true);
            _lstAccountingObjectNhanVien = IAccountingObjectService.GetAll_ByIsEmployee(true);

            _lstCurrency = ICurrencyService.GetAll();//sai
            _lstPaymentClause = IPaymentClauseService.GetAll();//sai
            _lstTransportMethod = ITransportMethodService.GetAll();//sai

            _lstAccountDefaultCo = IAccountDefaultService.GetAccountDefaultByTypeId(500, "DebitAccount");
            _lstAccountDefaultNo = IAccountDefaultService.GetAccountDefaultByTypeId(500, "CreditAccount");
            _lstFixedAsset = FixedAssetService.GetAll();//sai
            _lstDepartment = IDepartmentService.GetAll(); //sai.

            _lstAccountsVatAccountThueGtgt = IAccountDefaultService.GetAccountDefaultByTypeId(500, "VATAccount");
            _lstGoodsServicePurchases = GoodsServicePurchaseService.GetAll();//sai
            _lstAccountsImportTaxAccountThueNk = IAccountDefaultService.GetAccountDefaultByTypeId(500, "ImportTaxAccount");
            _lstAccountsSpecialConsumeTaxAccountThueTtdb = IAccountDefaultService.GetAccountDefaultByTypeId(500, "SpecialConsumeTaxAccount");
            _lstAccountsDeductionDebitAccount = IAccountDefaultService.GetAccountDefaultByTypeId(500, "DeductionDebitAccount");

            _lstBudgetItemsThuChi = IBudgetItemService.GetByActive_OrderByTreeIsParentNode(true);
            _lstExpenseItemsChiPhi = IExpenseItemService.GetByActive_OrderByTreeIsParentNode(true);
            _lstCostSetsDtTapHopChiPhi = ICostSetService.GetByActive_OrderByTreeIsParentNode(true);
            _lstContractStatesHopDong = IEMContractService.GetAll_IsActive(true);
            _lstStatisticsCodesThongKe = IStatisticsCodeService.GetAll();//IStatisticsCodeService.GetByActive_OrderByTreeIsParentNode(true);
            _lstInvoiceTypeLoaiHD = invoiceTypeService.GetIsActive(true);
            #region lấy teamplate
            template = Utils.GetTemplateInDatabase(null, 500);
            #endregion
            #endregion

            #region sự kiện vùng Header
            ucSelectThanhToan1.rbt_Chuathanhtoan.CheckedChanged += new System.EventHandler(this.rbt_Chuathanhtoan_CheckedChanged);
            ucSelectThanhToan1.rbt_Thanhtoanngay.CheckedChanged += new System.EventHandler(this.rbt_Thanhtoanngay_CheckedChanged);
            ucSelectThanhToan1.cbbChonThanhToan.SelectionChanged += new System.EventHandler(this.cbbChonThanhToan_SelectionChanged);
            #endregion

            #region sự kiện vùng thông tin chung
            #region Sự kiện của các combobox Accounting Object
            this.cbbAccountingObjectID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccountingObjectID_RowSelected);
            this.cbbAccountingObjectIDSCK.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccountingObjectID_RowSelected);
            this.cbbAccountingObjectIDSTM.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccountingObjectID_RowSelected);
            this.cbbAccountingObjectIDTM.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccountingObjectID_RowSelected);
            this.cbbAccountingObjectIDTTD.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccountingObjectID_RowSelected);

            this.cbbAccountingObjectID.TextChanged += new System.EventHandler(this.cbbAccountingObjectID_TextChanged);
            this.cbbAccountingObjectIDSCK.TextChanged += new System.EventHandler(this.cbbAccountingObjectID_TextChanged);
            this.cbbAccountingObjectIDSTM.TextChanged += new System.EventHandler(this.cbbAccountingObjectID_TextChanged);
            this.cbbAccountingObjectIDTM.TextChanged += new System.EventHandler(this.cbbAccountingObjectID_TextChanged);
            this.cbbAccountingObjectIDTTD.TextChanged += new System.EventHandler(this.cbbAccountingObjectID_TextChanged);

            this.cbbAccountingObjectID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbAccountingObjectID_KeyPress);
            this.cbbAccountingObjectIDSCK.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbAccountingObjectID_KeyPress);
            this.cbbAccountingObjectIDSTM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbAccountingObjectID_KeyPress);
            this.cbbAccountingObjectIDTM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbAccountingObjectID_KeyPress);
            this.cbbAccountingObjectIDTTD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbAccountingObjectID_KeyPress);

            #region Sự kiện của nút thêm trong combobox Accounting Object
            this.cbbAccountingObjectID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbAccountingObjectID_EditorButtonClick);
            this.cbbAccountingObjectIDSCK.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbAccountingObjectID_EditorButtonClick);
            this.cbbAccountingObjectIDSTM.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbAccountingObjectID_EditorButtonClick);
            this.cbbAccountingObjectIDTM.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbAccountingObjectID_EditorButtonClick);
            this.cbbAccountingObjectIDTTD.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbAccountingObjectID_EditorButtonClick);
            #endregion

            #endregion

            #region Sự kiện của các combobox Bank Account
            this.cbbBankAccountDetailID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbBankAccountDetailID_RowSelected);
            this.cbbBankAccountDetailIDSCK.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbBankAccountDetailID_RowSelected);
            this.cbbBankAccountDetailIDSTM.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbBankAccountDetailID_RowSelected);


            this.cbbBankAccountDetailID.TextChanged += new System.EventHandler(this.cbbBankAccountDetailID_TextChanged);
            this.cbbBankAccountDetailIDSCK.TextChanged += new System.EventHandler(this.cbbBankAccountDetailID_TextChanged);
            this.cbbBankAccountDetailIDSTM.TextChanged += new System.EventHandler(this.cbbBankAccountDetailID_TextChanged);

            this.cbbBankAccountDetailID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbBankAccountDetailID_KeyPress);
            this.cbbBankAccountDetailIDSCK.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbBankAccountDetailID_KeyPress);
            this.cbbBankAccountDetailIDSTM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbBankAccountDetailID_KeyPress);
            #endregion

            #region Sự kiện của các combobox Accounting Object Bank Account
            this.cbbAccountingObjectBankAccount.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccountingObjectBankAccount_RowSelected);
            this.cbbAccountingObjectBankAccountSCK.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccountingObjectBankAccount_RowSelected);
            this.cbbAccountingObjectBankAccountTTD.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccountingObjectBankAccount_RowSelected);

            this.cbbAccountingObjectBankAccount.TextChanged += new System.EventHandler(this.cbbAccountingObjectBankAccount_TextChanged);
            this.cbbAccountingObjectBankAccountSCK.TextChanged += new System.EventHandler(this.cbbAccountingObjectBankAccount_TextChanged);
            this.cbbAccountingObjectBankAccountTTD.TextChanged += new System.EventHandler(this.cbbAccountingObjectBankAccount_TextChanged);

            this.cbbAccountingObjectBankAccount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbAccountingObjectBankAccount_KeyPress);
            this.cbbAccountingObjectBankAccountSCK.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbAccountingObjectBankAccount_KeyPress);
            this.cbbAccountingObjectBankAccountTTD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbAccountingObjectBankAccount_KeyPress);
            #endregion

            #region Sự kiện của các combobox Credit Card
            this.cbbCreditCardNumberTTD.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbCreditCardNumberTTD_RowSelected);

            this.cbbCreditCardNumberTTD.TextChanged += new System.EventHandler(this.cbbCreditCardNumberTTD_TextChanged);

            this.cbbCreditCardNumberTTD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbCreditCardNumberTTD_KeyPress);
            #region Sự kiện của nút thêm trong combobox Accounting Object
            this.cbbCreditCardNumberTTD.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbCreditCardNumberTTD_EditorButtonClick);
            #endregion
            #endregion
            #endregion

            #region Sự kiện của vùng quy đổi tiền
            this.ugvCurrency.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.uGridCurrency_InitializeRow);
            this.ugvCurrency.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridCurrency_CellChange);
            this.ugvCurrency.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.uGridCurrency_InitializeLayout);
            #endregion

            #region sự kiện tab hàng tiền
            this.ugvPosted.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ugvPosted_InitializeRow);
            this.ugvPosted.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.ugvPosted_CellChange);
            this.ugvPosted.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ugvPosted_InitializeLayout);
            #endregion

            #region sự kiện tab thuế
            this.ugvTax.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ugvTax_InitializeRow);
            this.ugvTax.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.ugvTax_CellChange);
            this.ugvTax.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ugvTax_InitializeLayout);
            #endregion

            #region sự kiện tab thống kê
            this.ugvStatistics.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ugvStatistics_InitializeRow);
            this.ugvStatistics.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.ugvStatistics_CellChange);
            this.ugvStatistics.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ugvStatistics_InitializeLayout);
            #endregion

            #region thiết lập mặc định vùng header
            ucSelectThanhToan1.rbt_Chuathanhtoan.Checked = true;
            #endregion

            #region thiết lập mặc định vùng thông tin chung
            #region Load combobox Accounting Object
            //AccountingObject accountingObject1 = new AccountingObject();
            LoadCombobox_AccountingObject(_lstAccountingObject, cbbAccountingObjectID, _accountingObject, true);
            LoadCombobox_AccountingObject(_lstAccountingObject, cbbAccountingObjectIDSCK, _accountingObject, true);
            LoadCombobox_AccountingObject(_lstAccountingObject, cbbAccountingObjectIDSTM, _accountingObject, true);
            LoadCombobox_AccountingObject(_lstAccountingObject, cbbAccountingObjectIDTM, _accountingObject, true);
            LoadCombobox_AccountingObject(_lstAccountingObject, cbbAccountingObjectIDTTD, _accountingObject, true);
            #endregion

            #region Load combobox Bank Account
            //BankAccountDetail bankAccountDetail1 = new BankAccountDetail();
            LoadCombobox_BankAccountDetail(_lstBankAccountDetail, cbbBankAccountDetailID, _bankAccountDetail, true);
            LoadCombobox_BankAccountDetail(_lstBankAccountDetail, cbbBankAccountDetailIDSCK, _bankAccountDetail, true);
            LoadCombobox_BankAccountDetail(_lstBankAccountDetail, cbbBankAccountDetailIDSTM, _bankAccountDetail, true);
            #endregion

            #region Load combobox Accounting Object Bank Account
            LoadCombobox_AccountingObjectBankAccount(_lstAccountingObjectBankAccount, cbbAccountingObjectBankAccountTTD, _accountingObjectBankAccount, true);
            LoadCombobox_AccountingObjectBankAccount(_lstAccountingObjectBankAccount, cbbAccountingObjectBankAccountSCK, _accountingObjectBankAccount, true);
            #endregion

            #region Load combobox Creadit Card
            LoadCombobox_CreditCard(_lstCreditCard, cbbCreditCardNumberTTD);
            #endregion

            _isRunAccountingObject = true;
            _isRunBankAccountDetail = true;
            _isRunAccountingObjectBankAccount = true;
            _isRunCreditCrad = true;
            #endregion

            #region thiết lập mặc định vùng quy đổi tiền
            CauHinhVungQuyDoiTien(_faincrement);
            #endregion

            #region thiết lập mặc định vùng hàng tiền - thuế - thống kê
            _cbbThueSuatGtgtVatRate.Items.Add(0, "0%");
            _cbbThueSuatGtgtVatRate.Items.Add(0.05, "5%");
            _cbbThueSuatGtgtVatRate.Items.Add(0.1, "10%");
            _cbbThueSuatGtgtVatRate.Items.Add(1, "KCT");
            _cbbInvoiceTypeLoaiHD.Items.Add(0, "Không có hóa đơn");
            _cbbInvoiceTypeLoaiHD.Items.Add(1, "Hóa đơn thông thường");
            _cbbInvoiceTypeLoaiHD.Items.Add(2, "Hóa đơn GTGT");
            _bdlFaIncrementDetails = new BindingList<FAIncrementDetail>(startFrom == ConstFrm.optStatusForm.Add
                                                                     ? new List<FAIncrementDetail>()
                                                                     : _faincrement.FAIncrementDetails);
            CauHinh(_bdlFaIncrementDetails, ugvPosted, 0);
            CauHinh(_bdlFaIncrementDetails, ugvTax, 1);
            CauHinh(_bdlFaIncrementDetails, ugvStatistics, 2);
            #endregion

            #region lấy danh sách các cột đang được hiển thị

            dscotduochienthi0 = Laydscot(0);
            dscotduochienthi1 = Laydscot(1);
            dscotduochienthi2 = Laydscot(2);
            foreach (string cotduochienthitinhquydoi in dscotduochienthi0)
            {
                string s = cotduochienthitinhquydoi.Replace("Original", "");
            }
            #endregion
        }
        #endregion

        #region Utils
        #region vùng Header
        private void ChonHinhThucThanhToan()
        {
            if (ucSelectThanhToan1.rbt_Chuathanhtoan.Checked)
            {
                _kiemtraHinhthucThanhToan = true;
                grCTT.Visible = true;
                ucSelectThanhToan1.cbbChonThanhToan.Text = "";
                ucSelectThanhToan1.cbbChonThanhToan.Enabled = false;
                uTopSctNhtNCT1.txtNo.Text = Utils.TaoMaChungTu(IGenCodeService.getGenCode(ConstFrm.TypeGroup_PPInvoice));
            }
            else if (ucSelectThanhToan1.rbt_Thanhtoanngay.Checked)
            {
                _kiemtraHinhthucThanhToan = false;
                grCTT.Visible = false;
                ucSelectThanhToan1.cbbChonThanhToan.Enabled = true;
                List<string> lst = new List<string> { "Tiền mặt", "Ủy nhiện chi", "Séc chuyển khoản", "Séc tiền mặt", "Thẻ tín dụng" };
                ucSelectThanhToan1.cbbChonThanhToan.DataSource = lst.ToArray();
                ucSelectThanhToan1.cbbChonThanhToan.SelectedIndex = 0;

            }
        }
        private void LoadVungThongTin()
        {
            #region ẩn tất cả các control
            grTM.Visible = false;
            pnSCK.Visible = false;
            pnSTM.Visible = false;
            pnTTD.Visible = false;
            #endregion
            if (_kiemtraHinhthucThanhToan == false)
            {
                switch (ucSelectThanhToan1.cbbChonThanhToan.Text)
                {
                    case "Tiền mặt":
                        grTM.Visible = true;
                        uTopSctNhtNCT1.txtNo.Text = Utils.TaoMaChungTu(IGenCodeService.getGenCode(ConstFrm.TypeGroupMCPayment));
                        break;
                    case "Ủy nhiện chi":
                        pnSCK.Visible = true;
                        uTopSctNhtNCT1.txtNo.Text = Utils.TaoMaChungTu(IGenCodeService.getGenCode(ConstFrm.TypeGruop_MBTellerPaper));
                        break;
                    case "Séc chuyển khoản":
                        pnSCK.Visible = true;
                        uTopSctNhtNCT1.txtNo.Text = Utils.TaoMaChungTu(IGenCodeService.getGenCode(13));
                        break;
                    case "Séc tiền mặt":
                        pnSTM.Visible = true;
                        uTopSctNhtNCT1.txtNo.Text = Utils.TaoMaChungTu(IGenCodeService.getGenCode(14));
                        break;
                    case "Thẻ tín dụng":
                        pnTTD.Visible = true;
                        uTopSctNhtNCT1.txtNo.Text = Utils.TaoMaChungTu(IGenCodeService.getGenCode(ConstFrm.TypeGroup_MBCreditCard));
                        break;
                    default:
                        {
                            break;
                        }
                }
            }
        }
        #endregion

        #region vùng thông tin chung
        #region Cấu hình combobox

        private void LoadCombobox_AccountingObject(List<AccountingObject> LstAccountingObject, UltraCombo cbbAccountingObject, AccountingObject accountingobject, bool databindings)
        {
            cbbAccountingObject.DataSource = LstAccountingObject;
            cbbAccountingObject.DisplayMember = "AccountingObjectCode";
            cbbAccountingObject.ValueMember = "ID";
            Utils.ConfigGrid(cbbAccountingObject, ConstDatabase.AccountingObject_TableName);
            if (databindings)
                cbbAccountingObject.DataBindings.Add("Value", accountingobject, "AccountingObjectCode", true, DataSourceUpdateMode.OnPropertyChanged);

        }

        private void LoadCombobox_BankAccountDetail(List<BankAccountDetail> LstBankAccountDetail, UltraCombo cbbBankAccountDetail, BankAccountDetail bankaccountdetail, bool databindings)
        {
            cbbBankAccountDetail.DataSource = LstBankAccountDetail;
            cbbBankAccountDetail.DisplayMember = "BankAccount";
            Utils.ConfigGrid(cbbBankAccountDetail, ConstDatabase.BankAccountDetail_TableName);
            if (databindings)
                cbbBankAccountDetail.DataBindings.Add("Value", bankaccountdetail, "BankAccount", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void LoadCombobox_AccountingObjectBankAccount(List<AccountingObjectBankAccount> LstAccountingObjectBankAccount, UltraCombo cbbAccountingObjectBankAccount, AccountingObjectBankAccount accountingobjectbankaccount, bool databindings)
        {
            cbbAccountingObjectBankAccount.DataSource = LstAccountingObjectBankAccount;
            cbbAccountingObjectBankAccount.DisplayMember = "BankAccount";
            Utils.ConfigGrid(cbbAccountingObjectBankAccount, ConstDatabase.AccountingObjectBankAccount_TableName);
            if (databindings)
                cbbAccountingObjectBankAccount.DataBindings.Add("Value", accountingobjectbankaccount, "BankAccount", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void LoadCombobox_CreditCard(List<CreditCard> LstCreditCard, UltraCombo cbb)
        {
            cbb.DataSource = LstCreditCard;
            cbb.DisplayMember = "CreditCardNumber";
            Utils.ConfigGrid(cbb, ConstDatabase.CreditCard_TableName);
        }
        #endregion
        #endregion

        #region vùng quy đổi tiền
        private void CauHinhVungQuyDoiTien(FAIncrement fAIncrement)
        {
            ugvCurrency.DataSource = new List<FAIncrement>() { fAIncrement };
            Template template = Utils.GetTemplateInDatabase(null, 500);
            Utils.ConfigGrid(ugvCurrency, "", template.TemplateDetails.Single(x => x.TabIndex == 100).TemplateColumns);
            ugvCurrency.DisplayLayout.Bands[0].Summaries.Clear();
            ugvCurrency.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            #region loại tiền
            //ugvCurrency.DisplayLayout.Bands[0].Columns["CurrencyID"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            _cbbCurrency.DataSource = _lstCurrency;
            _cbbCurrency.DisplayMember = "ID";
            Utils.ConfigGrid(_cbbCurrency, ConstDatabase.Currency_TableName);
            ugvCurrency.DisplayLayout.Bands[0].Columns["CurrencyID"].ValueList = _cbbCurrency;
            _cbbCurrency.RowSelected += cbbCurrency_RowSelected;
            #endregion

            #region điều khoản thanh toán
            _cbbPaymentClause.DataSource = _lstPaymentClause;
            _cbbPaymentClause.DisplayMember = "PaymentClauseCode";
            Utils.ConfigGrid(_cbbPaymentClause, ConstDatabase.PaymentClause_TableName);
            ugvCurrency.DisplayLayout.Bands[0].Columns["PaymentClauseID"].ValueList = _cbbPaymentClause;
            ugvCurrency.DisplayLayout.Bands[0].Columns["PaymentClauseID"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            _cbbPaymentClause.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(cbbPaymentClause_RowSelected);
            #endregion
            
            #region quy đổi
            Utils.FormatNumberic(ugvCurrency.DisplayLayout.Bands[0].Columns["ExchangeRate"], ConstDatabase.Format_Rate);
            ugvCurrency.DisplayLayout.Bands[0].Columns["ExchangeRate"].Hidden = true;
            ugvCurrency.DisplayLayout.Bands[0].Columns["ExchangeRate"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DoubleNonNegative;
            #endregion

            #region nhân viên mua hàng
            cbbAccountingObject_NhanVien.DataSource = _lstAccountingObjectNhanVien;
            cbbAccountingObject_NhanVien.DisplayMember = "AccountingObjectCode";
            Utils.ConfigGrid(cbbAccountingObject_NhanVien, ConstDatabase.AccountingObject_TableName);
            ugvCurrency.DisplayLayout.Bands[0].Columns["EmployeeID"].ValueList = cbbAccountingObject_NhanVien;
            cbbAccountingObject_NhanVien.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(cbbAccountingObject_NhanVien_RowSelected);
            #endregion

            #region phương thức vận chuyển
            _cbbTransportMethod.DataSource = _lstTransportMethod;
            _cbbTransportMethod.DisplayMember = "TransportMethodCode";
            Utils.ConfigGrid(_cbbTransportMethod, ConstDatabase.TransportMethod_TableName);
            ugvCurrency.DisplayLayout.Bands[0].Columns["TransportMethodID"].ValueList = _cbbTransportMethod;
            _cbbTransportMethod.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(cbbTransportMethod_RowSelected);
            #endregion

            #region mua tại
            _cbbIsImportPurchaseFixCung.Items.Add(false, "Trong nước");
            _cbbIsImportPurchaseFixCung.Items.Add(true, "Nhập khẩu");
            ugvCurrency.DisplayLayout.Bands[0].Columns["IsImportPurchase"].EditorComponent = _cbbIsImportPurchaseFixCung;
            ugvCurrency.DisplayLayout.Bands[0].Columns["IsImportPurchase"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            #endregion

        }

        #endregion

        #region cấu hình theo template(hàng tiền - thuế - thống kê)

        /// <summary>
        /// Hàm để cấu hình các grid view theo teamplate.
        /// </summary>
        /// <param name="bdlFaIncrementDetailsIn">tham số</param>
        /// <param name="ugvGrid">grid cần cấu hình</param>
        /// <param name="thamso">0-hàng tiền, 1-thuế , 2-thốnge kê</param>
        private void CauHinh(BindingList<FAIncrementDetail> bdlFaIncrementDetailsIn, UltraGrid ugvGrid, int thamso)
        {
            ugvGrid.DataSource = bdlFaIncrementDetailsIn;

            Utils.ConfigGrid(ugvGrid, "", template.TemplateDetails.Single(x => x.TabIndex == thamso).TemplateColumns);
            Utils.SetConfigNormalUltraGrid(ugvGrid);
            #region tài sản cố định
            _cbbFixedAsset.DataSource = _lstFixedAsset;
            _cbbFixedAsset.DisplayMember = "FixedAssetCode";
            _cbbFixedAsset.ValueMember = "ID";
            Utils.ConfigGrid(_cbbFixedAsset, ConstDatabase.FixedAsset_TableName);
            ugvGrid.DisplayLayout.Bands[0].Columns["FixAssetID"].ValueList = _cbbFixedAsset;
            _cbbFixedAsset.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(cbbFixedAsset_RowSelected);
            #endregion

            #region phòng ban
            _cbbDepartment.DataSource = _lstDepartment;
            _cbbDepartment.DisplayMember = "DepartmentCode";
            _cbbDepartment.ValueMember = "ID";
            Utils.ConfigGrid(_cbbDepartment, ConstDatabase.Department_TableName);
            ugvGrid.DisplayLayout.Bands[0].Columns["DepartmentID"].ValueList = _cbbDepartment;
            _cbbDepartment.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(cbbDepartment_RowSelected);
            #endregion

            #region tài khoản nợ
            LoadCbbAccountDefaut(_cbbAccountNo, _lstAccountDefaultNo);
            ugvGrid.DisplayLayout.Bands[0].Columns["DebitAccount"].ValueList = _cbbAccountNo;
            _cbbDepartment.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(cbbAccount_no_RowSelected);
            #endregion

            #region tài khoản có
            LoadCbbAccountDefaut(_cbbAccountCo, _lstAccountDefaultCo);
            ugvGrid.DisplayLayout.Bands[0].Columns["CreditAccount"].ValueList = _cbbAccountCo;
            _cbbDepartment.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(cbbAccount_co_RowSelected);
            #endregion

            #region nhà cung cấp
            _cbbAccountingObjectHangtien.DataSource = _lstAccountingObject;
            _cbbAccountingObjectHangtien.DisplayMember = "AccountingObjectCode";
            _cbbAccountingObject.ValueMember = "ID";
            Utils.ConfigGrid(_cbbAccountingObjectHangtien, ConstDatabase.AccountingObject_TableName);
            ugvGrid.DisplayLayout.Bands[0].Columns["AccountingObjectID"].ValueList = _cbbAccountingObjectHangtien;
            _cbbAccountingObjectHangtien.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(cbbAccountingObject_hangtien_RowSelected);
            #endregion

            #region quy đổi
            //Utils.FormatNumberic(ugvCurrency.DisplayLayout.Bands[0].Columns["Amount"], ConstDatabase.Format_CashEquivalents);
            //Utils.FormatNumberic(ugvCurrency.DisplayLayout.Bands[0].Columns["AmountOriginal"], ConstDatabase.Format_CashEquivalents);
            //ugvCurrency.DisplayLayout.Bands[0].Columns["AmountOriginal"].Hidden = true;
            #endregion

            #region thuế suất GTGT không load từ CSDL
            ugvGrid.DisplayLayout.Bands[0].Columns["VATRate"].EditorComponent = _cbbThueSuatGtgtVatRate;
            ugvGrid.DisplayLayout.Bands[0].Columns["VATRate"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            #endregion

            #region loại hóa đơn không load từ CSDL
            ugvGrid.DisplayLayout.Bands[0].Columns["InvoiceType"].EditorComponent = _cbbInvoiceTypeLoaiHD;
            ugvGrid.DisplayLayout.Bands[0].Columns["InvoiceType"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            #endregion

            #region loại hóa đơn
            ugvGrid.DisplayLayout.Bands[0].Columns["InvoceTypeID"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            _cbbLoaiHdInvoceTypeId.DataSource = _lstInvoiceTypeLoaiHD;
            _cbbLoaiHdInvoceTypeId.DisplayMember = "InvoiceTypeCode";
            _cbbLoaiHdInvoceTypeId.ValueMember = "ID";
            Utils.ConfigGrid(_cbbLoaiHdInvoceTypeId, ConstDatabase.InvoiceType_TableName);
            ugvGrid.DisplayLayout.Bands[0].Columns["InvoceTypeID"].ValueList = _cbbLoaiHdInvoceTypeId;
            #endregion

            #region tài khoản thuế GTGT
            LoadCbbAccountDefaut(_cbbAccountsVatAccountThueGtgt, _lstAccountsVatAccountThueGtgt);
            ugvGrid.DisplayLayout.Bands[0].Columns["VATAccount"].ValueList = _cbbAccountsVatAccountThueGtgt;
            #endregion

            #region load đối tượng kế toán
            LoadCombobox_AccountingObject(_lstAccountingObject, _cbbAccountingObject, _accountingObject, false);
            ugvGrid.DisplayLayout.Bands[0].Columns["AccountingObjectID"].ValueList = _cbbAccountingObject;
            #endregion

            #region nhóm hàng hóa dịch vụ
            LoadCbb(_cbbGoodsServicePurchases, _lstGoodsServicePurchases, "GoodsServicePurchaseCode", ConstDatabase.GoodsServicePurchase_TableName);
            ugvGrid.DisplayLayout.Bands[0].Columns["GoodsServicePurchaseID"].ValueList = _cbbGoodsServicePurchases;
            #endregion

            #region tài khoản thuế nhập khẩu
            LoadCbbAccountDefaut(_cbbAccountsImportTaxAccountThueNk, _lstAccountsImportTaxAccountThueNk);
            ugvGrid.DisplayLayout.Bands[0].Columns["ImportTaxAccount"].ValueList = _cbbAccountsImportTaxAccountThueNk;
            #endregion

            #region tài khoản thuế tiêu thụ đặc biệt
            LoadCbbAccountDefaut(_cbbAccountsSpecialConsumeTaxAccountThueTtdb, _lstAccountsSpecialConsumeTaxAccountThueTtdb);
            ugvGrid.DisplayLayout.Bands[0].Columns["SpecialConsumeTaxAccount"].ValueList = _cbbAccountsSpecialConsumeTaxAccountThueTtdb;
            #endregion

            #region tài khoản thuế khấu trừ/treo nợ (thuế đối ứng GTGT)
            LoadCbbAccountDefaut(_cbbAccountsDeductionDebitAccount, _lstAccountsDeductionDebitAccount);
            ugvGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"].ValueList = _cbbAccountsDeductionDebitAccount;
            #endregion

            #region mục thu / chi
            _cbbBudgetItemsThuChi.DataSource = _lstBudgetItemsThuChi;
            _cbbBudgetItemsThuChi.DisplayMember = "BudgetItemCode";
            _cbbBudgetItemsThuChi.ValueMember = "ID";
            Utils.ConfigGrid(_cbbBudgetItemsThuChi, ConstDatabase.BudgetItem_TableName);
            ugvGrid.DisplayLayout.Bands[0].Columns["BudgetItemID"].ValueList = _cbbBudgetItemsThuChi;
            #endregion

            #region khoản mục chi phí
            _cbbExpenseItemsChiPhi.DataSource = _lstExpenseItemsChiPhi;
            _cbbExpenseItemsChiPhi.DisplayMember = "ExpenseItemCode";
            _cbbExpenseItemsChiPhi.ValueMember = "ID";
            Utils.ConfigGrid(_cbbExpenseItemsChiPhi, ConstDatabase.ExpenseItem_TableName);
            ugvGrid.DisplayLayout.Bands[0].Columns["ExpenseItemID"].ValueList = _cbbExpenseItemsChiPhi;
            #endregion

            #region đối tượng tập hợp chi phí
            _cbbCostSetDtTapHopChiPhi.DataSource = _lstCostSetsDtTapHopChiPhi;
            _cbbCostSetDtTapHopChiPhi.DisplayMember = "CostSetCode";
            _cbbCostSetDtTapHopChiPhi.ValueMember = "ID";
            Utils.ConfigGrid(_cbbCostSetDtTapHopChiPhi, ConstDatabase.CostSet_TableName);
            ugvGrid.DisplayLayout.Bands[0].Columns["CostSetID"].ValueList = _cbbCostSetDtTapHopChiPhi;
            #endregion

            #region hợp đồng
            _cbbContractStateHopDong.DataSource = _lstContractStatesHopDong;
            _cbbContractStateHopDong.DisplayMember = "ContractStateCode";
            _cbbContractStateHopDong.ValueMember = "ID";
            Utils.ConfigGrid(_cbbContractStateHopDong, ConstDatabase.EMContract_TableName);
            ugvGrid.DisplayLayout.Bands[0].Columns["ContractID"].ValueList = _cbbContractStateHopDong;
            #endregion

            #region mã thống kê
            _cbbStatisticsCodeThongKe.DataSource = _lstStatisticsCodesThongKe;
            _cbbStatisticsCodeThongKe.DisplayMember = "StatisticsCode_";
            _cbbStatisticsCodeThongKe.ValueMember = "ID";
            Utils.ConfigGrid(_cbbStatisticsCodeThongKe, ConstDatabase.StatisticsCode_TableName);
            ugvGrid.DisplayLayout.Bands[0].Columns["StatisticsCodeID"].ValueList = _cbbStatisticsCodeThongKe;
            #endregion

            #region Colums Style
            //foreach (var item in ugvPosted.DisplayLayout.Bands[0].Columns)
            //{
            //    //phần chung
            //    //if (item.Key.Equals("MaterialGoodsID"))
            //    //    item.Editor = this.GetEmbeddableEditorBase(_dsMaterialGoods, "ID", "MaterialGoodsCode", ConstDatabase.MaterialGoods_TableName);
            //}
            #endregion

            #region cấu hình co giãn, hoặc kéo co data grid view dựa vào độ rộng
            int dodaiGrid = template.TemplateDetails.Single(x => x.TabIndex == thamso).TemplateColumns.Where(x => x.IsVisible == true).Sum(x => x.ColumnWidth);
            if (dodaiGrid <= Screen.PrimaryScreen.WorkingArea.Width) ugvGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            else ugvStatistics.DisplayLayout.AutoFitStyle = AutoFitStyle.None;
            #endregion
            #region thêm các tổng cho cột

            foreach (string aki in _lstFaIncrementDetailAmount)
            {
                string ai = "Sum";
                ai += aki;
                AddSumChoCot(ugvGrid, ai, aki);
            }
            foreach (string aik in _lstFaIncrementDetailAmountOriginal)
            {
                string ai = "Sum";
                ai += aik;
                AddSumChoCot(ugvGrid, ai, aik);
            }
            #endregion
        }
        #endregion
        #region AddSumChoCot thêm tổng cho cột
        /// <summary>
        /// Add cột tính tổng cho cột
        /// </summary>
        /// <param name="ugvGrid">tên data grid cần add</param>
        /// <param name="keySum">key của cột sum đó (tự đặt)</param>
        /// <param name="keyCot">key của cột tương ứng cần add</param>
        private void AddSumChoCot(UltraGrid ugvGrid, string keySum, string keyCot)
        {
            SummarySettings summary = ugvGrid.DisplayLayout.Bands[0].Summaries.Add(keySum, SummaryType.Sum, ugvGrid.DisplayLayout.Bands[0].Columns[keyCot]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
        }
        #endregion
        #region cấu hình các combobox tài khoản thuế được lấy trong bảng AccountDefaut

        private void LoadCbbAccountDefaut<T>(UltraCombo cbb, List<T> t)
        {
            cbb.DataSource = t;
            cbb.DisplayMember = "AccountNumber";
            cbb.ValueMember = "ID";
            Utils.ConfigGrid(cbb, ConstDatabase.Account_TableName);
            cbb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cbb.AutoSuggestFilterMode = AutoSuggestFilterMode.Contains;
        }
        #endregion
        #region cấu hình các cobox khác
        /// <summary>
        /// Load một combobox bất kỳ
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cbb">tên ultracombo cần gán</param>
        /// <param name="listGans">một list cần gán vào combobox</param>
        /// <param name="displayMember">thuộc tính hiển thị của cobobox</param>
        /// <param name="constDatabase">tên cấu hình trong bảng</param>
        private void LoadCbb<T>(UltraCombo cbb, List<T> listGans, string displayMember, string constDatabase)
        {
            cbb.DataSource = _lstFixedAsset;
            cbb.DisplayMember = displayMember;
            cbb.ValueMember = "ID";
            Utils.ConfigGrid(cbb, constDatabase);
        }
        #endregion

        private List<string> Laydscot(int tab)
        {
            List<string> dstencot = template.TemplateDetails.Single(x => x.TabIndex == tab).TemplateColumns.Where(x => x.IsVisible == true).Select(x => x.ColumnName).ToList();
            return (from tienquydoi in _lstFaIncrementDetailAmountOriginal from tencot in dstencot where tencot == tienquydoi select tienquydoi.Replace("Original", "")).ToList();
        }

        #endregion

        #region Event

        #region vùng Header
        private void rbt_Thanhtoanngay_CheckedChanged(object sender, EventArgs e)
        {
            ChonHinhThucThanhToan();
        }
        private void rbt_Chuathanhtoan_CheckedChanged(object sender, EventArgs e)
        {
            ChonHinhThucThanhToan();
        }
        private void cbbChonThanhToan_SelectionChanged(object sender, EventArgs e)
        {
            LoadVungThongTin();
        }
        #endregion

        #region vùng thông tin chung
        #region vùng combobox accounting object
        //--------------------------------------------------------------------------vùng combobox accounting object----------------------------------------------
        private void cbbAccountingObjectID_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row != null)
            {
                if (e.Row.Selected) _isRunAccountingObject = false;
                if (!_isRunAccountingObject)
                {
                    AccountingObject CBBAccountingObject = new AccountingObject();
                    CBBAccountingObject = (AccountingObject)e.Row.ListObject;

                    txtAccountingObjectName.Text = txtAccountingObjectNameTTD.Text
                        = txtAccountingObjectNameSCK.Text = txtAccountingObjectNameSTM.Text
                        = txtAccountingObjectNameTM.Text = CBBAccountingObject.AccountingObjectName;
                    txtAccountingObjectAddress.Text = txtAccountingObjectAddressSCK.Text
                        = txtAccountingObjectAddressTM.Text = txtAccountingObjectAddressTTD.Text = CBBAccountingObject.Address;
                    //lstBankAccountDetail = lstBankAccountDetail.Where(c => c.BankID = CBBAccountingObject.BankAccounts)
                    //lstAccountingObjectBankAccount = CBBAccountingObject.BankAccounts.ToList().Where(c => c.IsSelect == true).ToList();
                    //lstAccountingObjectBankAccount = lstAccountingObjectBankAccount.Where(c => c.AccountingObjectID == CBBAccountingObject.ID).ToList();
                    LoadCombobox_AccountingObjectBankAccount(CBBAccountingObject.BankAccounts.ToList(), cbbAccountingObjectBankAccountSCK, _accountingObjectBankAccount, false);
                    LoadCombobox_AccountingObjectBankAccount(CBBAccountingObject.BankAccounts.ToList(), cbbAccountingObjectBankAccountTTD, _accountingObjectBankAccount, false);

                    //faincrement.AccountingObjectID = CBBAccountingObject.ID;
                    //faincrement.AccountingObjectAddress = CBBAccountingObject.Address;
                    //faincrement.AccountingObjectName = CBBAccountingObject.AccountingObjectName;
                }
            }

        }
        private void cbbAccountingObjectID_TextChanged(object sender, EventArgs e)
        {
            if (_isRunAccountingObject)
            {
                UltraCombo cbbTemp = (UltraCombo)sender;
                Utils.AutoComplateUltraCombo(cbbTemp, _lstAccountingObject.Where(x => x.AccountingObjectCode.ToLower().Contains(cbbTemp.Text.ToLower()) || x.AccountingObjectName.ToLower().Contains(cbbTemp.Text.ToLower())).ToList());
            }
            else _isRunAccountingObject = false;
        }

        private void cbbAccountingObjectID_KeyPress(object sender, KeyPressEventArgs e)
        {
            _isRunAccountingObject = true;
        }

        private void cbbAccountingObjectID_EditorButtonClick(object sender, EditorButtonEventArgs e)
        {
            UltraCombo cbbTemp = (UltraCombo)sender;
            FAccountingObjectCustomersDetail frm = new FAccountingObjectCustomersDetail();
            frm.ShowDialog();
            _lstAccountingObject = IAccountingObjectService.GetAccountingObjects(1, true);
            if (!FAccountingObjectCustomersDetail.isClose) LoadCombobox_AccountingObject(_lstAccountingObject, cbbTemp, _accountingObject, false);
        }
        #endregion
        #region vùng combobox bank account detail
        //--------------------------------------------------------------------------vùng combobox bank account detail----------------------------------------------
        private void cbbBankAccountDetailID_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row != null)
            {
                if (e.Row.Selected) _isRunBankAccountDetail = false;
                if (!_isRunBankAccountDetail)
                {
                    BankAccountDetail CBBBankAccountDetail = new BankAccountDetail();
                    CBBBankAccountDetail = (BankAccountDetail)e.Row.ListObject;
                    txtBankNameSTM.Text = txtBankNameSCK.Text = CBBBankAccountDetail.BankName;
                }
            }
        }

        private void cbbBankAccountDetailID_TextChanged(object sender, EventArgs e)
        {
            if (_isRunBankAccountDetail)
            {
                UltraCombo cbbTemp = (UltraCombo)sender;
                Utils.AutoComplateUltraCombo(cbbTemp, _lstBankAccountDetail.Where(x => x.BankAccount.ToLower().Contains(cbbTemp.Text.ToLower()) || x.BankName.ToLower().Contains(cbbTemp.Text.ToLower())).ToList());
            }
            else _isRunBankAccountDetail = false;
        }
        private void cbbBankAccountDetailID_KeyPress(object sender, KeyPressEventArgs e)
        {
            _isRunBankAccountDetail = true;
        }
        #endregion
        #region vùng combobox Accounting Object Bank Account
        //--------------------------------------------------------------------------vùng combobox Accounting Object Bank Account----------------------------------------------
        private void cbbAccountingObjectBankAccount_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row != null)
            {
                if (e.Row.Selected) _isRunAccountingObjectBankAccount = false;
                if (!_isRunAccountingObjectBankAccount)
                {
                    AccountingObjectBankAccount CBBAccountingObjectBankAccount = new AccountingObjectBankAccount();
                    CBBAccountingObjectBankAccount = (AccountingObjectBankAccount)e.Row.ListObject;
                    txtAccountingObjectBankNameSCK.Text = txtAccountingObjectBankNameTTD.Text = CBBAccountingObjectBankAccount.BankName;
                }
            }
        }
        private void cbbAccountingObjectBankAccount_TextChanged(object sender, EventArgs e)
        {
            if (_isRunAccountingObjectBankAccount)
            {
                UltraCombo cbbTemp = (UltraCombo)sender;
                Utils.AutoComplateUltraCombo(cbbTemp, _lstAccountingObjectBankAccount.Where(x => x.BankAccount.ToLower().Contains(cbbTemp.Text.ToLower()) || x.BankName.ToLower().Contains(cbbTemp.Text.ToLower())).ToList());
            }
            else _isRunAccountingObjectBankAccount = false;
        }
        private void cbbAccountingObjectBankAccount_KeyPress(object sender, KeyPressEventArgs e)
        {
            _isRunAccountingObjectBankAccount = true;
        }
        #endregion
        #region vùng combobox credit card
        //--------------------------------------------------------------------------vùng combobox credit card----------------------------------------------
        private void cbbCreditCardNumberTTD_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row != null)
            {
                if (e.Row.Selected) _isRunCreditCrad = false;
                if (!_isRunCreditCrad)
                {
                    CreditCard CBBCreditCard = new CreditCard();
                    CBBCreditCard = (CreditCard)e.Row.ListObject;
                    lblCreditCardTypeTTD.Text = CBBCreditCard.CreditCardType;
                    lblOwnerCardTTD.Text = CBBCreditCard.OwnerCard;
                }
            }
        }

        private void cbbCreditCardNumberTTD_TextChanged(object sender, EventArgs e)
        {
            if (_isRunCreditCrad)
            {
                UltraCombo cbbTemp = (UltraCombo)sender;
                Utils.AutoComplateUltraCombo(cbbTemp, _lstCreditCard.Where(x => x.OwnerCard.ToLower().Contains(cbbTemp.Text.ToLower()) || x.CreditCardType.ToLower().Contains(cbbTemp.Text.ToLower())).ToList());
            }
            else _isRunCreditCrad = false;
        }

        private void cbbCreditCardNumberTTD_KeyPress(object sender, KeyPressEventArgs e)
        {
            _isRunCreditCrad = true;
        }
        private void cbbCreditCardNumberTTD_EditorButtonClick(object sender, EditorButtonEventArgs e)
        {
            UltraCombo cbbTemp = (UltraCombo)sender;
            FCreditCardDetail frm = new FCreditCardDetail();
            frm.ShowDialog();
            _lstCreditCard = ICreditCardService.GetAll();
            if (!FCreditCardDetail.IsClose) LoadCombobox_CreditCard(_lstCreditCard, cbbTemp);
        }
        #endregion
        #endregion

        #region vùng quy đổi tiền
        private void uGridCurrency_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
        }
        private void uGridCurrency_InitializeRow(object sender, InitializeRowEventArgs e)
        {
        }
        private void uGridCurrency_CellChange(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key == "CurrencyID")
            {
                UltraGridBand uGridCurrencyBand = ugvCurrency.DisplayLayout.Bands[0];
            }
        }
        #region các combo vùng quy đổi
        private void cbbCurrency_RowSelected(object sender, RowSelectedEventArgs e)
        {
            try
            {
                bool kt = true;
                _checkCbbCurrencyOpen = true;
                if (e.Row != null)
                {
                    var model = (Currency)e.Row.ListObject;
                    if (model.ID != "VND")
                    {
                        ugvCurrency.Rows[0].Cells["ExchangeRate"].Value = model.ExchangeRate;
                        kt = false;
                    }
                    ugvCurrency.DisplayLayout.Bands[0].Columns["ExchangeRate"].Hidden = kt;
                    string s = "Original";
                    foreach (string cotduochienthitinhquydoi in dscotduochienthi0)
                    {
                        ugvPosted.DisplayLayout.Bands[0].Columns[cotduochienthitinhquydoi].Hidden = kt;
                        foreach (UltraGridRow ugr in ugvPosted.Rows)
                        {
                            s = cotduochienthitinhquydoi + s;
                            ugr.Cells[cotduochienthitinhquydoi].Value = decimal.Parse(ugr.Cells[s].Value.ToString()) * model.ExchangeRate;
                        }
                    }
                    foreach (string cotduochienthitinhquydoi in dscotduochienthi1)
                    {
                        ugvTax.DisplayLayout.Bands[0].Columns[cotduochienthitinhquydoi].Hidden = kt;
                        foreach (UltraGridRow ugr in ugvPosted.Rows)
                        {
                            s = cotduochienthitinhquydoi + s;
                            ugr.Cells[cotduochienthitinhquydoi].Value = decimal.Parse(ugr.Cells[s].Value.ToString()) * model.ExchangeRate;
                        }
                    }
                    foreach (string cotduochienthitinhquydoi in dscotduochienthi2)
                    {
                        ugvStatistics.DisplayLayout.Bands[0].Columns[cotduochienthitinhquydoi].Hidden = kt;
                        foreach (UltraGridRow ugr in ugvPosted.Rows)
                        {
                            s = cotduochienthitinhquydoi + s;
                            ugr.Cells[cotduochienthitinhquydoi].Value = decimal.Parse(ugr.Cells[s].Value.ToString()) * model.ExchangeRate;
                        }
                    }
                }
            }
            catch (Exception)
            {
                string s = "";
            }
            
        }

        private void cbbPaymentClause_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row != null)
            {
                var paymentClause = (PaymentClause)e.Row.ListObject;
                _faincrement.PaymentClauseID = paymentClause.ID;
            }
        }
        private void cbbTransportMethod_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row != null)
            {
                var transportMethod = (TransportMethod)e.Row.ListObject;
                _faincrement.TransportMethodID = transportMethod.ID;
            }
        }
        private void cbbAccountingObject_NhanVien_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row != null)
            {
                //AccountingObject accountingObject = (AccountingObject)e.Row.ListObject;
                //faincrement.EmployeeID = AccountingObject.ID;
            }
        }
        #endregion
        #endregion

        #region tab hàng tiền
        private void ugvPosted_InitializeRow(object sender, InitializeRowEventArgs e)
        {

        }
        private void ugvPosted_CellChange(object sender, CellEventArgs e)
        {
            if (_checkCbbCurrencyOpen)
            {
                int i = 0;
                ugvPosted.UpdateData();
                bool kiemtradangquydoiloainao = true;
                foreach (var tiennguyente in _lstFaIncrementDetailAmountOriginal)
                {
                    if (e.Cell.Column.Key == tiennguyente)
                    {
                        decimal tygia = decimal.Parse(ugvCurrency.Rows[0].Cells["ExchangeRate"].Value.ToString());
                        decimal sotiennguyente = decimal.Parse(e.Cell.Row.Cells[tiennguyente].Value.ToString());
                        //từ tiền nguyên tệ tính ra quy đổi
                        if (sotiennguyente > 0)
                        {
                            e.Cell.Row.Cells[_lstFaIncrementDetailAmount[i]].Value = (decimal)tygia * sotiennguyente;
                            kiemtradangquydoiloainao = false;
                        }
                        e.Cell.Row.Cells[_lstFaIncrementDetailAmount[i]].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                    }
                    i++;
                }
                if (kiemtradangquydoiloainao)
                {
                    i = 0;
                    foreach (var tienquydoi in _lstFaIncrementDetailAmount)
                    {
                        if (e.Cell.Column.Key == tienquydoi)
                        {
                            decimal tygia = decimal.Parse(ugvCurrency.Rows[0].Cells["ExchangeRate"].Value.ToString());
                            decimal sotienquydoi = decimal.Parse(e.Cell.Row.Cells[tienquydoi].Value.ToString());

                            //từ tiền quy đổi tính ra nguyên tệ
                            if (sotienquydoi > 0)
                            {
                                e.Cell.Row.Cells[_lstFaIncrementDetailAmountOriginal[i]].Value = (decimal)sotienquydoi/tygia;
                            }
                            e.Cell.Row.Cells[_lstFaIncrementDetailAmountOriginal[i]].Style =
                                Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                        }
                        i++;
                    }
                }
            }

        }
        private void ugvPosted_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
        }
        private void cbbFixedAsset_RowSelected(object sender, RowSelectedEventArgs e)
        {

        }
        private void cbbDepartment_RowSelected(object sender, RowSelectedEventArgs e)
        {

        }
        private void cbbAccount_no_RowSelected(object sender, RowSelectedEventArgs e)
        {

        }
        private void cbbAccount_co_RowSelected(object sender, RowSelectedEventArgs e)
        {

        }
        private void cbbAccountingObject_hangtien_RowSelected(object sender, RowSelectedEventArgs e)
        {

        }

        #endregion

        #region tab thuế
        private void ugvTax_InitializeRow(object sender, InitializeRowEventArgs e)
        {

        }

        private void ugvTax_CellChange(object sender, CellEventArgs e)
        {

        }

        private void ugvTax_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {

        }
        #endregion

        #region tab thống kê

        private void ugvStatistics_InitializeRow(object sender, InitializeRowEventArgs e)
        {

        }

        private void ugvStatistics_CellChange(object sender, CellEventArgs e)
        {

        }

        private void ugvStatistics_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {

        }
        #endregion

        #endregion
        private void ugvStatistics_BeforeCellUpdate(object sender, BeforeCellUpdateEventArgs e)
        {
            //e.Cell.Text
        }
        private void ugvStatistics_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {

        }
    }
    public class FSFAIncrementBuyDetailBase : DetailBase<FAIncrement>
    {

    }
}
