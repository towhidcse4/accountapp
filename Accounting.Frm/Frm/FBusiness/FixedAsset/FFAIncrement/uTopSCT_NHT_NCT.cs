﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting.FBusiness.FFixedAsset.FFAIncrement
{
    public partial class UTopSctNhtNCT : UserControl
    {
        public UTopSctNhtNCT()
        {
            InitializeComponent();
        }
        public string No
        {
            get { return txtNo.Text; }
            set { txtNo.Text = value; }
        }
        public DateTime PostedDate
        {
            get { return dtePostedDate.DateTime; }
            set { dtePostedDate.DateTime = value; }
        }
        public DateTime Date
        {
            get { return dteDate.DateTime; }
            set { dteDate.DateTime = value; }
        }

        private void txtNo_TextChanged(object sender, EventArgs e)
        {
            lblNo.Text = txtNo.Text;
        }
    }
}
