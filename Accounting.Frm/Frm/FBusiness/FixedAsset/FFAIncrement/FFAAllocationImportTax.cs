﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FFAAllocationImportTax : CustormForm
    {
        public FAIncrement FAIncrement { get; private set; }
        //private PPInvoice PPInvoice = new PPInvoice();
        private List<PPServices> _ppServices = new List<PPServices>();
        private List<PPInvoiceDetailCost> _ppInvoiceDetailCosts = new List<PPInvoiceDetailCost>();
        int statusForm;
        public FFAAllocationImportTax(FAIncrement input, int _statusForm)
        {
            InitializeComponent();
            statusForm = _statusForm;
            FAIncrement = input.CloneObject();
            _ppInvoiceDetailCosts = input.PPInvoiceDetailCosts.ToList();
            if (_ppInvoiceDetailCosts.Count > 0)
            {
                txtTotalFreightAmount.Value = _ppInvoiceDetailCosts.Where(x => x.CostType == true).Sum(x => x.TotalFreightAmount);
                btnAllocation.PerformClick();
            }
            
            var sum = input.FAIncrementDetails.Sum(x => x.ImportTaxExpenseAmount);
            List<FAAllocationImportTax> listInput = input.FAIncrementDetails.Select(detail =>
                new FAAllocationImportTax
                {
                    FixedAssetID = detail.FixedAssetID ?? Guid.Empty,
                    //FixedAssetCode = detail.FixedAssetID,
                    Description = detail.Description,
                    Quantity = detail.Quantity ?? 0,
                    Amount = detail.AmountOriginal,
                    fAIncrementDetail = detail,
                    ImportTaxExpenseRate = sum > 0 ? detail.ImportTaxExpenseAmount / sum * 100 : 0,
                    ImportTaxExpenseAmount = detail.ImportTaxExpenseAmount,
                }).ToList();
            uGrid.DataSource = listInput;
            uGrid.ConfigGrid(ConstDatabase.FAAllocationImportTax_KeyName, null, true, false);
            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid);
            }
            uGrid.ConfigSummaryOfGrid();
            uGrid.DisplayLayout.Override.HeaderAppearance.TextHAlign = HAlign.Center;
            txtTotalFreightAmount.FormatNumberic(input.CurrencyID.Equals("VND") ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAllocation_Click(object sender, EventArgs e)
        {
            var p = (decimal?)uGrid.Rows.SummaryValues["SumAmount"].Value ?? 0;
            foreach (var row in uGrid.Rows)
            {
                var a = _ppServices.Sum(x => x.FreightAmount);
                var b = (decimal)(row.Cells["Amount"].Value ?? 0);
                var c = (a == 0 || b == 0) ? 0 : (b / p * 100);
                var d = (a == 0 || b == 0) ? 0 : (a * c / 100);
                row.Cells["ImportTaxExpenseRate"].Value = c;
                row.Cells["ImportTaxExpenseAmount"].Value = d;
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            FAIncrement.PPInvoiceDetailCosts = _ppInvoiceDetailCosts;
            SaveAndClose();
        }

        void SaveAndClose()
        {
            var listInput = (List<FAAllocationImportTax>)uGrid.DataSource;
            foreach (var FAIncrementDetail in FAIncrement.FAIncrementDetails)
            {
                var allocationImportTax = listInput.FirstOrDefault(x => x.fAIncrementDetail.FixedAssetID == FAIncrementDetail.FixedAssetID);
                if (allocationImportTax != null)
                {
                    FAIncrementDetail.ImportTaxExpenseAmountOriginal = allocationImportTax.ImportTaxExpenseAmount / (FAIncrement.ExchangeRate ?? 1);
                    FAIncrementDetail.ImportTaxExpenseAmount = allocationImportTax.ImportTaxExpenseAmount ;
                }
            }
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnSelectPPService_Click(object sender, EventArgs e)
        {
            try
            {
                var f = new FFASelectPPServices(FAIncrement, statusForm, true);
                f.FormClosed += new FormClosedEventHandler(fSelectPPServices_FormClosed);
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                return;
            }
        }
        private void fSelectPPServices_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason != CloseReason.UserClosing) return;
            var f = (FFASelectPPServices)sender;
            if (f.DialogResult == DialogResult.OK)
            {
                _ppServices = f.SelectPPServices;
                txtTotalFreightAmount.Value = _ppServices.Sum(x => x.FreightAmount);
                txtTotalFreightAmount.ReadOnly = _ppServices.Count > 1;
                for (int i = 0; i < _ppInvoiceDetailCosts.Count; i++)
                {
                    if (_ppServices.Any(x => x.ID != _ppInvoiceDetailCosts[i].PPServiceID && _ppInvoiceDetailCosts[i].CostType == true)) _ppInvoiceDetailCosts.RemoveAt(i);
                }
                foreach (var pps in _ppServices)
                {
                    if (_ppInvoiceDetailCosts.Any(x => x.PPServiceID == pps.ID && x.CostType == true))
                    {
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].TotalFreightAmount = pps.TotalAmount;
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].TotalFreightAmountOriginal = pps.TotalAmountOriginal;
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AmountPB = (pps.FreightAmount);
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AmountPBOriginal = pps.FreightAmount / FAIncrement.ExchangeRate;
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AccumulatedAllocateAmount = (_ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AmountPB + (pps.AccumulatedAllocateAmount));
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AccumulatedAllocateAmountOriginal = (_ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AmountPBOriginal + (pps.AccumulatedAllocateAmountOriginal));
                    }
                    else
                    {
                        PPInvoiceDetailCost ppi = new PPInvoiceDetailCost();
                        ppi.AccountingObjectID = pps.AccountingObjectID;
                        ppi.AccountingObjectName = pps.AccountingObjectName;
                        ppi.Date = pps.Date;
                        ppi.PostedDate = pps.PostedDate;
                        ppi.No = pps.No;
                        ppi.CostType = true;
                        ppi.TypeID = 210;
                        ppi.PPServiceID = pps.ID;
                        ppi.TotalFreightAmount = pps.TotalAmount;
                        ppi.TotalFreightAmountOriginal = pps.TotalAmountOriginal;
                        ppi.AmountPB = pps.FreightAmount;
                        ppi.AmountPBOriginal = pps.FreightAmount / FAIncrement.ExchangeRate;
                        ppi.AccumulatedAllocateAmount = (ppi.AmountPB + (pps.AccumulatedAllocateAmount));
                        ppi.AccumulatedAllocateAmountOriginal = (ppi.AmountPBOriginal + (pps.AccumulatedAllocateAmountOriginal));
                        ppi.ID = Guid.NewGuid();
                        _ppInvoiceDetailCosts.Add(ppi);
                    }
                }
            }
        }
    }
}
