﻿namespace Accounting.FBusiness.FFixedAsset.FFAIncrement
{
    partial class UTopSctNhtNCT
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            this.palBH = new System.Windows.Forms.Panel();
            this.lblPostedDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblNo = new Infragistics.Win.Misc.UltraLabel();
            this.txtNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDate = new Infragistics.Win.Misc.UltraLabel();
            this.dteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtePostedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.palBH.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtePostedDate)).BeginInit();
            this.SuspendLayout();
            // 
            // palBH
            // 
            this.palBH.BackColor = System.Drawing.Color.Transparent;
            this.palBH.Controls.Add(this.lblPostedDate);
            this.palBH.Controls.Add(this.lblNo);
            this.palBH.Controls.Add(this.txtNo);
            this.palBH.Controls.Add(this.lblDate);
            this.palBH.Controls.Add(this.dteDate);
            this.palBH.Controls.Add(this.dtePostedDate);
            this.palBH.Location = new System.Drawing.Point(0, 0);
            this.palBH.Name = "palBH";
            this.palBH.Size = new System.Drawing.Size(301, 48);
            this.palBH.TabIndex = 38;
            // 
            // lblPostedDate
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextHAlignAsString = "Center";
            appearance7.TextVAlignAsString = "Middle";
            this.lblPostedDate.Appearance = appearance7;
            this.lblPostedDate.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded4Thick;
            this.lblPostedDate.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Etched;
            this.lblPostedDate.Location = new System.Drawing.Point(101, 2);
            this.lblPostedDate.Name = "lblPostedDate";
            this.lblPostedDate.Size = new System.Drawing.Size(100, 24);
            this.lblPostedDate.TabIndex = 28;
            this.lblPostedDate.Text = "Ngày hoạch toán";
            // 
            // lblNo
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextHAlignAsString = "Center";
            appearance8.TextVAlignAsString = "Middle";
            this.lblNo.Appearance = appearance8;
            this.lblNo.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded4Thick;
            this.lblNo.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Etched;
            this.lblNo.Location = new System.Drawing.Point(2, 2);
            this.lblNo.Name = "lblNo";
            this.lblNo.Size = new System.Drawing.Size(100, 24);
            this.lblNo.TabIndex = 0;
            this.lblNo.Text = "Số chứng từ";
            // 
            // txtNo
            // 
            appearance3.TextHAlignAsString = "Center";
            this.txtNo.Appearance = appearance3;
            this.txtNo.Location = new System.Drawing.Point(2, 25);
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(99, 21);
            this.txtNo.TabIndex = 5;
            // 
            // lblDate
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextHAlignAsString = "Center";
            appearance9.TextVAlignAsString = "Middle";
            this.lblDate.Appearance = appearance9;
            this.lblDate.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded4Thick;
            this.lblDate.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Etched;
            this.lblDate.Location = new System.Drawing.Point(200, 2);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(100, 24);
            this.lblDate.TabIndex = 24;
            this.lblDate.Text = "Ngày chứng từ";
            // 
            // dteDate
            // 
            appearance10.TextHAlignAsString = "Center";
            appearance10.TextVAlignAsString = "Middle";
            this.dteDate.Appearance = appearance10;
            this.dteDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteDate.Location = new System.Drawing.Point(200, 25);
            this.dteDate.MaskInput = "";
            this.dteDate.Name = "dteDate";
            this.dteDate.Size = new System.Drawing.Size(99, 21);
            this.dteDate.TabIndex = 27;
            this.dteDate.Value = null;
            // 
            // dtePostedDate
            // 
            appearance11.TextHAlignAsString = "Center";
            appearance11.TextVAlignAsString = "Middle";
            this.dtePostedDate.Appearance = appearance11;
            this.dtePostedDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtePostedDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtePostedDate.Location = new System.Drawing.Point(101, 25);
            this.dtePostedDate.MaskInput = "";
            this.dtePostedDate.Name = "dtePostedDate";
            this.dtePostedDate.Size = new System.Drawing.Size(99, 21);
            this.dtePostedDate.TabIndex = 29;
            this.dtePostedDate.Value = null;
            // 
            // UTopSctNhtNct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.palBH);
            this.Name = "UTopSctNhtNct";
            this.Size = new System.Drawing.Size(301, 48);
            this.palBH.ResumeLayout(false);
            this.palBH.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtePostedDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel palBH;
        private Infragistics.Win.Misc.UltraLabel lblPostedDate;
        private Infragistics.Win.Misc.UltraLabel lblNo;
        public Infragistics.Win.UltraWinEditors.UltraTextEditor txtNo;
        private Infragistics.Win.Misc.UltraLabel lblDate;
        public Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDate;
        public Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtePostedDate;
    }
}
