﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Accounting.Core.IService;
using FX.Core;

namespace Accounting
{
    public partial class FFACostAlLocation : CustormForm
    {
        private IPPInvoiceDetailCostService _IPPInvoiceDetailCostService { get { return IoC.Resolve<IPPInvoiceDetailCostService>(); } }
        public FAIncrement FAIncrement { get; private set; }
        private List<PPServices> _ppServices = new List<PPServices>();
        private List<PPInvoiceDetailCost> _ppInvoiceDetailCosts = new List<PPInvoiceDetailCost>();
        int statusForm;
        public FFACostAlLocation(FAIncrement input, int _statusForm)
        {
            InitializeComponent();
            statusForm = _statusForm;
            FAIncrement = input;
            var lstFAIncrementDetail = input.FAIncrementDetails.Where(x => x.FixedAssetID != null).ToList();
            decimal sumFreightAmount = lstFAIncrementDetail.Sum(x => x.FreightAmount);
            _ppInvoiceDetailCosts = input.PPInvoiceDetailCosts.ToList();
            if (_ppInvoiceDetailCosts.Count > 0)
            {
                txtTotalFreightAmount.Value = _ppInvoiceDetailCosts.Where(x => x.CostType == false).Sum(x => x.TotalFreightAmount);
                btnAllocation.PerformClick();
            }
            List<FACostAllocation> listInput = lstFAIncrementDetail.Select(
                detail => new FACostAllocation
                {
                    FixedAssetID = 
                    (Guid)detail.FixedAssetID,
                    Description = detail.Description,
                    Quantity = detail.Quantity ?? 0,
                    Amount = detail.AmountOriginal,
                    FreightAmount = detail.FreightAmount,
                    FreightRate = (decimal)sumFreightAmount == 0 ? (decimal)0 : ((detail.FreightAmount / (decimal)sumFreightAmount) * 100),
                    FAIncrementDetail = detail
                }).ToList();
            uGrid.DataSource = listInput;
            uGrid.ConfigGrid(ConstDatabase.FACostAlLocation_KeyName, null, true, false);
            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid);
            }
            uGrid.ConfigSummaryOfGrid();
            uGrid.DisplayLayout.Override.HeaderAppearance.TextHAlign = HAlign.Center;
            txtTotalFreightAmount.FormatNumberic(input.CurrencyID.Equals("VND") ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            btnAllocation.Enabled = uGrid.Rows.Count > 0;
            btnApply.Enabled = uGrid.Rows.Count > 0;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAllocation_Click(object sender, EventArgs e)
        {
            CalculateFreight();
        }

        private void CalculateFreight()
        {
            var p = (decimal?)uGrid.Rows.SummaryValues["SumAmount"].Value ?? 0;
            foreach (var row in uGrid.Rows)
            {
                //var a = txtTotalFreightAmount.Text == "" ? (decimal)0 : Convert.ToDecimal(txtTotalFreightAmount.Value ?? 0);
                var a = _ppServices.Sum(x => x.FreightAmount);
                var b = (decimal)(row.Cells["Amount"].Value ?? 0);
                var c = (a == 0 || b == 0) ? 0 : (b / p * 100);
                var d = (a == 0 || b == 0) ? 0 : (a * c / 100);
                row.Cells["FreightRate"].Value = c;
                row.Cells["FreightAmount"].Value = d;
            }
        }

        private void btnSelectPPService_Click(object sender, EventArgs e)
        {
            try
            {
                var f = new FFASelectPPServices(FAIncrement, statusForm, false);
                f.FormClosed += new FormClosedEventHandler(fSelectPPServices_FormClosed);
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                return;
            }
        }

        private void fSelectPPServices_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason != CloseReason.UserClosing) return;
            var f = (FFASelectPPServices)sender;
            if (f.DialogResult == DialogResult.OK)
            {
                _ppServices = f.SelectPPServices;
                txtTotalFreightAmount.Value = _ppServices.Sum(x => x.FreightAmount);
                txtTotalFreightAmount.ReadOnly = _ppServices.Count > 1;
                for(int i = 0; i < _ppInvoiceDetailCosts.Count; i++)
                {
                    if (_ppServices.Any(x => x.ID != _ppInvoiceDetailCosts[i].PPServiceID && _ppInvoiceDetailCosts[i].CostType == false)) _ppInvoiceDetailCosts.RemoveAt(i);
                }
                foreach (var pps in _ppServices)
                {
                    if (_ppInvoiceDetailCosts.Any(x => x.PPServiceID == pps.ID && x.CostType == false))
                    {
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].TotalFreightAmount = pps.TotalAmount;
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].TotalFreightAmountOriginal = pps.TotalAmountOriginal;
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AmountPB = (pps.FreightAmount);
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AmountPBOriginal = pps.FreightAmount / FAIncrement.ExchangeRate;
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AccumulatedAllocateAmount = (_ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AmountPB + (pps.AccumulatedAllocateAmount));
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AccumulatedAllocateAmountOriginal = (_ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AmountPBOriginal + (pps.AccumulatedAllocateAmount));
                    }
                    else
                    {
                        PPInvoiceDetailCost ppi = new PPInvoiceDetailCost();
                        ppi.AccountingObjectID = pps.AccountingObjectID;
                        ppi.AccountingObjectName = pps.AccountingObjectName;
                        ppi.Date = pps.Date;
                        ppi.PostedDate = pps.PostedDate;
                        ppi.No = pps.No;
                        ppi.CostType = false;
                        ppi.TypeID = 210;
                        ppi.PPServiceID = pps.ID;
                        ppi.TotalFreightAmount = pps.TotalAmount;
                        ppi.TotalFreightAmountOriginal = pps.TotalAmountOriginal;
                        ppi.AmountPB = pps.FreightAmount;
                        ppi.AmountPBOriginal = pps.FreightAmount / FAIncrement.ExchangeRate;
                        ppi.AccumulatedAllocateAmount = (ppi.AmountPB + (pps.AccumulatedAllocateAmount));
                        ppi.AccumulatedAllocateAmountOriginal = (ppi.AmountPBOriginal + (pps.AccumulatedAllocateAmount));
                        ppi.ID = Guid.NewGuid();
                        _ppInvoiceDetailCosts.Add(ppi);
                    }
                }               
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                FAIncrement.PPInvoiceDetailCosts = _ppInvoiceDetailCosts;
                SaveAndClose();
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                return;
            }
        }

        void SaveAndClose()
        {
            var listInput = (List<FACostAllocation>)uGrid.DataSource;
            foreach (var FAIncrementDetail in FAIncrement.FAIncrementDetails)
            {
                var costAllocation = listInput.FirstOrDefault(x => x.FAIncrementDetail.FixedAssetID == FAIncrementDetail.FixedAssetID);
                if (costAllocation != null)
                {
                    FAIncrementDetail.FreightAmount = costAllocation.FreightAmount;
                    FAIncrementDetail.FreightAmountOriginal = costAllocation.FreightAmount / (FAIncrement.ExchangeRate ?? 1);
                }
                    
            }
            DialogResult = DialogResult.OK;
            Close();
        }

        private void uGrid_AfterCellUpdate(object sender, CellEventArgs e)
        {
        }
    }
}
