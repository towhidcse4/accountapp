﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Castle.Components.DictionaryAdapter;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FFATransferDetail : FFATransferDetailBase
    {
        #region khai báo


        private UltraGrid ugridDS;
        public BindingList<FATransferDetail> LstFATransferDetails = new BindingList<FATransferDetail>();
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        UltraGrid ugrid2 = null;
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        #endregion

        public FFATransferDetail(FATransfer temp, List<FATransfer> dsFATransfers, int statusForm)
        {

            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();

            #endregion

            #region Thiết lập ban đầu cho Form
            _statusForm = statusForm;
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.TypeID = 550;

            }
            else
            {
                _select = temp;
            }

            _listSelects.AddRange(dsFATransfers);
            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, dsFATransfers, statusForm);

            #endregion
            #region view

            Viewgrid();

            #endregion
        }

        void Viewgrid()
        {

            UltraGridBand band = ugridDS.DisplayLayout.Bands[0];
            List<Account> lst = IAccountService.Query.Where(
                        k =>
                            k.AccountNumber.StartsWith("154") || k.AccountNumber.StartsWith("241") ||
                            k.AccountNumber.StartsWith("641") || k.AccountNumber.StartsWith("642") ||
                            k.AccountNumber.StartsWith("631") || k.AccountNumber.StartsWith("811") ||
                            k.AccountNumber.StartsWith("632") || k.AccountNumber.StartsWith("635")).ToList();
            var cbbAccoutingOject = new UltraCombo
            {
                DataSource = lst,
                DisplayMember = "AccountNumber",
                ValueMember = "AccountNumber"
            };
            Utils.ConfigGrid(cbbAccoutingOject, ConstDatabase.Account_TableName);
            band.Columns["CostAccount"].ValueList = cbbAccoutingOject;

            ugridDS.DisplayLayout.Bands[0].Columns["FixedAssetID"].AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            ugridDS.DisplayLayout.Bands[0].Columns["CostAccount"].AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            ugridDS.DisplayLayout.Bands[0].Columns["CostSetID"].AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            ugridDS.DisplayLayout.Bands[0].Columns["StatisticsCodeID"].AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            ugridDS.DisplayLayout.Bands[0].Columns["ExpenseItemID"].AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            ugridDS.DisplayLayout.Bands[0].Columns["ToDepartment"].AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;

        }

        public override void InitializeGUI(FATransfer input)
        {
            if (input.FATransferDetails.Count == 0) input.FATransferDetails = new List<FATransferDetail>();

            Template mauGiaoDien = FFATransfer.GetMauGiaoDien(input.TypeID, input.TemplateID, FFATransfer.DsTemplate);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;
            BindingList<FATransferDetail> dsFATransferDetails = new BindingList<FATransferDetail>();
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            if (_statusForm != ConstFrm.optStatusForm.Add)
                bdlRefVoucher = new BindingList<RefVoucher>(input.RefVouchers);
            
            dsFATransferDetails = new BindingList<FATransferDetail>(_statusForm == ConstFrm.optStatusForm.Add ? new BindingList<FATransferDetail>() : input.FATransferDetails);

            _listObjectInput = new BindingList<System.Collections.IList> { dsFATransferDetails };
            _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
            this.ConfigGridByTemplete_General<FATransfer>(pnlUgrid, mauGiaoDien);
            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { true, false };
            this.ConfigGridByManyTemplete<FATransfer>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
            ugrid2 = Controls.Find("uGrid1", true).FirstOrDefault() as UltraGrid;
            ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            ugridDS = (UltraGrid)this.Controls.Find("uGrid0", true).FirstOrDefault();

            this.ConfigTopVouchersNo<FATransfer>(pnlDateNo, "No", "", "Date", null, false);
            txtDescription.DataBindings.Clear();
            //txtDescription.DataBindings.Add("Text", input, "Reason", true);
            DataBinding(new List<System.Windows.Forms.Control> { txtDescription, txtReciever, txtTransferor }, "Reason,Reciever,Transferor");
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
            }
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 550)//trungnq thêm vào để  làm task 6234
                {
                    txtDescription.Value = "Điều chuyển TSCĐ";
                    txtDescription.Text = "Điều chuyển TSCĐ";
                    _select.Reason = "Điều chuyển TSCĐ";
                };
            }
        }

        #region event
        private void combo1_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row != null)
            {

                if (e.Row.Selected)
                {
                    ugridDS.UpdateData();
                    var item = ugridDS.DataSource as BindingList<FATransferDetail>;
                    int index = ugridDS.ActiveCell.Row.Index;
                    var model = e.Row.ListObject as FixedAsset;
                    if (e.Row.ListObject == null) return;
                    if (model != null)
                    {
                        item[index].Description = model.FixedAssetName;
                        item[index].FromDepartment = model.DepartmentID;
                        item[index].Amount = model.OriginalPrice;
                        item[index].CostAccount = model.ExpenditureAccount;

                    }

                }

            }

            ugridDS.DataBind();
        }
        #endregion

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucher>();
                    var f = new FViewVoucherOriginal("VND", datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
               
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }
        private void FFATransferDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }

    public class FFATransferDetailBase : DetailBase<FATransfer>
    {
        public IGenCodeService IgenCodeService
        {
            get { return IoC.Resolve<IGenCodeService>(); }
        }
    }
}
