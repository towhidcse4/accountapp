﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FFAACostApplicationDetail : CustormForm
    {
        public readonly IFixedAssetService _IFixedAssetService;
        public readonly IExpenseItemService _IExpenseItemService;
        public readonly IBudgetItemService _IBudgetItemService;
        public readonly IAccountService _IAccountService;
        public readonly IDepartmentService _IDepartmentService;
        public readonly IContractStateService _IContractStateService;
        public readonly ICostSetService _ICostSetService;

        public FFAACostApplicationDetail(List<FixedAsset> lstChoseAssets)
        {
            InitializeComponent();

            _IFixedAssetService = IoC.Resolve<IFixedAssetService>();
            _IAccountService = IoC.Resolve<IAccountService>();
            _IDepartmentService = IoC.Resolve<IDepartmentService>();
            _IExpenseItemService = IoC.Resolve<IExpenseItemService>();
            _IContractStateService = IoC.Resolve<IContractStateService>();
            _ICostSetService = IoC.Resolve<ICostSetService>();

            uGridDS.DisplayLayout.ViewStyle = ViewStyle.SingleBand;

            foreach (FixedAsset lstChoseAsset in lstChoseAssets) lstChoseAsset.IsSecondHand = false;
            uGridDS.DataSource = lstChoseAssets;
            ViewGridDS();
            uGridCT.DataSource = new BindingList<FAACostApplicationDetail>();
            ViewGridCT();
        }

        void ViewGridDS()
        {
            Utils.ConfigGrid(uGridDS, "", mauGiaoDien());

            uGridDS.DisplayLayout.Bands[0].Columns["IsSecondHand"].Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            uGridDS.DisplayLayout.Bands[0].Columns["IsSecondHand"].Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;

            UltraGridBand band = uGridDS.DisplayLayout.Bands[0];

            UltraGridColumn ultraGridColumn = band.Columns["IsIrrationalCost"];
            ultraGridColumn.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ultraGridColumn.Header.Fixed = true;
            ultraGridColumn.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.Band;

            uGridDS.DisplayLayout.Bands[0].Columns["OriginalPrice"].Format = "#,###,###,##đ";
            uGridDS.DisplayLayout.Bands[0].Columns["OriginalPrice"].MaskInput = "nn,nnn,nnn,nnn,nnn,nnn,nnn,nnn";

            ugridDisColums(band.Columns["FixedAssetCode"]);
            ugridDisColums(band.Columns["FixedAssetName"]);
            ugridDisColums(band.Columns["OriginalPrice"]);

            band.Override.SelectedRowAppearance.BackColor = System.Drawing.Color.FromArgb(255, 192, 128);
            band.Override.HotTrackRowAppearance.ForeColor = Color.BlueViolet;
            band.Override.HotTrackRowSelectorAppearance.BackColor = Color.FromArgb(51, 153, 255);
            band.Override.HotTrackRowCellAppearance.BackColor = Color.FromArgb(51, 153, 255);
            band.Override.BorderStyleCell = UIElementBorderStyle.None;
            band.Override.BorderStyleRow = UIElementBorderStyle.None;
        }
        void ViewGridCT()
        {
            Utils.ConfigGrid(uGridCT, "", mauGiaoDienCT());
            Utils.SetConfigNormalUltraGrid(uGridCT);

            UltraCombo cbbAccoutingOject = new UltraCombo();
            cbbAccoutingOject.DataSource = _IAccountService.Query.Where(k => k.AccountNumber.StartsWith("154") || k.AccountNumber.StartsWith("241") || k.AccountNumber.StartsWith("642") || k.AccountNumber.StartsWith("642") || k.AccountNumber.StartsWith("631") || k.AccountNumber.StartsWith("811") || k.AccountNumber.StartsWith("632") || k.AccountNumber.StartsWith("635"));
            cbbAccoutingOject.DisplayMember = "AccountNumber";
            cbbAccoutingOject.ValueMember = "ID";
            Utils.ConfigGrid(cbbAccoutingOject, ConstDatabase.Account_TableName);
            uGridCT.DisplayLayout.Bands[0].Columns["AccountingObjectName"].ValueList = cbbAccoutingOject;

            UltraCombo cbbDepartmentCode = new UltraCombo();
            cbbDepartmentCode.DataSource = _IDepartmentService.GetAll_ByIsActive_OrderByDepartmentCode(true);
            cbbDepartmentCode.DisplayMember = "DepartmentCode";
            cbbDepartmentCode.ValueMember = "ID";
            Utils.ConfigGrid(cbbDepartmentCode, ConstDatabase.Department_TableName);
            uGridCT.DisplayLayout.Bands[0].Columns["DepartmentCode"].ValueList = cbbDepartmentCode;

            UltraCombo cbbExpenseItemId = new UltraCombo
            {
                DataSource = _IExpenseItemService.GetByActive_OrderByTreeIsParentNode(true),
                DisplayMember = "ExpenseItemCode",
                ValueMember = "ID"
            };
            Utils.ConfigGrid(cbbExpenseItemId, ConstDatabase.ExpenseItem_TableName);
            uGridCT.DisplayLayout.Bands[0].Columns["ExpenseItemCode"].ValueList = cbbExpenseItemId;

            UltraCombo cbbContractCode = new UltraCombo();
            cbbContractCode.DataSource = _IContractStateService.GetListContractStateOrder();
            cbbContractCode.DisplayMember = "ContractStateCode";
            cbbContractCode.ValueMember = "ID";
            Utils.ConfigGrid(cbbContractCode, ConstDatabase.ContractState_TableName);
            uGridCT.DisplayLayout.Bands[0].Columns["ContractCode"].ValueList = cbbContractCode;

            UltraCombo cbbCostSetCode = new UltraCombo();
            cbbCostSetCode.DataSource = _ICostSetService.GetCostSetCodeBool(true);
            cbbCostSetCode.DisplayMember = "CostSetCode";
            cbbCostSetCode.ValueMember = "ID";
            Utils.ConfigGrid(cbbCostSetCode, ConstDatabase.CostSet_TableName);
            uGridCT.DisplayLayout.Bands[0].Columns["CostSetID"].ValueList = cbbCostSetCode;
        }

        #region template
        private List<TemplateColumn> mauGiaoDien()
        {
            return new List<TemplateColumn> 
            {
                new TemplateColumn()
                {
                   ColumnName = "IsSecondHand",
                    ColumnCaption = "",
                    //ColumnWidth = 20,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 0
                },
                new TemplateColumn()
                {
                    ColumnName = "FixedAssetCode",
                    ColumnCaption = "Mã TSCĐ",
                    //ColumnWidth = 20,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 1
                },
                new TemplateColumn()
                {
                    ColumnName = "FixedAssetName",
                    ColumnCaption = "Tên TSCĐ",
                  
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 2
                },
                new TemplateColumn()
                {
                    ColumnName = "OriginalPrice",
                    ColumnCaption = "Giá trị khấu hao",
                    
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 3
                },

             };
        }
        private List<TemplateColumn> mauGiaoDienCT()
        {
            return new List<TemplateColumn> 
            {
                new TemplateColumn()
                {
                   ColumnName = "AccountingObjectName",
                    ColumnCaption = "Số hiệu TK",
                    //ColumnWidth = 20,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 1
                },
                new TemplateColumn()
                {
                    ColumnName = "DepartmentCode",
                    ColumnCaption = "Phòng ban",
                    //ColumnWidth = 20,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 2
                },
                new TemplateColumn()
                {
                    ColumnName = "ExpenseItemCode",
                    ColumnCaption = "Khoản mục CP",
                  
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 3
                },
                new TemplateColumn()
                {
                    ColumnName = "ContractCode",
                    ColumnCaption = "Hợp đồng",
                    
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 4
                },
                 new TemplateColumn()
                {
                    ColumnName = "CostSetID",
                    ColumnCaption = "ĐT tập hợp CP",
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 5
                },
                  new TemplateColumn()
                {
                    ColumnName = "Rate",
                    ColumnCaption = "Tỷ kệ %",
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 6
                },
                 new TemplateColumn()
                {
                    ColumnName = "IsIrrationalCost",
                    ColumnCaption = "CP không hợp lý",
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 7
                }

             };
        }
        #endregion

        private void uGridDS_AfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {

        }

        #region button
        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            List<FAACostApplicationDetail> tonghop = DsAddCostApplicationDetails((List<FixedAsset>)uGridDS.DataSource, (BindingList<FAACostApplicationDetail>)uGridCT.DataSource);
            FFAACostApplication.LstFaaCostApplications.AddRange(tonghop);
            Close();
        
        }
        #endregion

        #region check error
            
        void Error()
        {
            
            if (uGridCT.Rows[0].Cells["AccountingObjectName"].Value == null) MSG.Error("chua chon tai khoan");
            if (uGridCT.Rows[0].Cells["CostSetName"].Value == null) MSG.Error("chua chon DT chi phi");
          

        }
        #endregion

        #region Utils
        void ugridDisColums(UltraGridColumn ultraGrid)
        {
            ultraGrid.CellActivation = Activation.NoEdit;
            ultraGrid.CellClickAction = CellClickAction.RowSelect;
        }

        public List<FAACostApplicationDetail> DsAddCostApplicationDetails(List<FixedAsset> lstChoseAssets,
            BindingList<FAACostApplicationDetail> lstFaaCostApplicationDetails)
        {
            List<FAACostApplicationDetail> ketqua = new List<FAACostApplicationDetail>();

            foreach (FixedAsset fixedAsset in lstChoseAssets.Where(k => k.IsSecondHand))
            {
               
                foreach (FAACostApplicationDetail faaCostApplicationDetail in lstFaaCostApplicationDetails.ToList())
                {
                    FAACostApplicationDetail temp = Utils.CloneObject(faaCostApplicationDetail);
                    temp.FixedAssetCode = fixedAsset.FixedAssetCode;
                    temp.FixedAssetName = fixedAsset.FixedAssetName;
                    temp.DepreciationAmount = fixedAsset.OriginalPrice;
                    temp.AccountingObjectName = faaCostApplicationDetail.AccountingObjectName;
                    temp.ExpenseItemCode = faaCostApplicationDetail.ExpenseItemCode;
                    temp.DepartmentCode = faaCostApplicationDetail.DepartmentCode;
                    temp.CostSetID = faaCostApplicationDetail.CostSetID;
                    temp.ContractCode = faaCostApplicationDetail.ContractCode;
                    temp.Rate = faaCostApplicationDetail.Rate;
                    ketqua.Add(temp);
                }
              
            }

            return ketqua;
        }

        #endregion
    }
}
