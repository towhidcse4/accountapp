﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FFAACostApplication : CustormForm
    {
        public readonly IFixedAssetService _IFixedAssetService;
        public readonly IExpenseItemService _IExpenseItemService;
        public readonly IBudgetItemService _IBudgetItemService;
        public readonly IAccountDefaultService _IAccountDefaultService;
        public readonly IDepartmentService _IDepartmentService;
        public readonly IContractStateService _IContractStateService;
        public readonly ICostSetService _ICostSetService;
        public readonly IAccountService _IAccountService;
        public readonly IFADepreciatioAllocationService _IFADepreciatioAllocationService;
        public static List<FAACostApplicationDetail> LstFaaCostApplications;
        List<FixedAsset> LstFixedAssets;
        bool Them = true;
        FADepreciationAllocation _Select = new FADepreciationAllocation();
        public FFAACostApplication()
        {
            InitializeComponent();

            _IFixedAssetService = IoC.Resolve<IFixedAssetService>();
            _IAccountDefaultService = IoC.Resolve<IAccountDefaultService>();
            _IDepartmentService = IoC.Resolve<IDepartmentService>();
            _IExpenseItemService = IoC.Resolve<IExpenseItemService>();
            _IContractStateService = IoC.Resolve<IContractStateService>();
            _ICostSetService = IoC.Resolve<ICostSetService>();
            _IAccountService = IoC.Resolve<IAccountService>();
            _IFADepreciatioAllocationService = IoC.Resolve<IFADepreciatioAllocationService>();
            //tao du lieu test
            LstFaaCostApplications = new List<FAACostApplicationDetail>();  //lay trong csdl
            LstFixedAssets = _IFixedAssetService.GetAll();  //lay trong csdl

            if (uGridDS.Rows.Count > 0) uGridDS.Rows[0].Selected = true;// tu chon dong dau tien 
            uGridDS.DisplayLayout.ViewStyle = ViewStyle.SingleBand;

            uGridDS.DataSource = Utils.CloneObject(LstFaaCostApplications);
            viewGrid();
        }

        void viewGrid()
        {
            if (uGridDS.Rows.Count > 0) uGridDS.Rows[0].Selected = true;
            Utils.ConfigGrid(uGridDS, "", MauGiaoDien());
            uGridDS.DisplayLayout.Bands[0].Summaries.Clear();

            UltraCombo cbbAccoutingOject = new UltraCombo();
            cbbAccoutingOject.DataSource = _IAccountService.Query.Where(k => k.AccountNumber.StartsWith("154") || k.AccountNumber.StartsWith("241") || k.AccountNumber.StartsWith("642") || k.AccountNumber.StartsWith("642") || k.AccountNumber.StartsWith("631") || k.AccountNumber.StartsWith("811") || k.AccountNumber.StartsWith("632") || k.AccountNumber.StartsWith("635"));
            cbbAccoutingOject.DisplayMember = "AccountNumber";
            cbbAccoutingOject.ValueMember = "ID";
            Utils.ConfigGrid(cbbAccoutingOject, ConstDatabase.Account_TableName);
            uGridDS.DisplayLayout.Bands[0].Columns["AccountingObjectName"].ValueList = cbbAccoutingOject;


            UltraCombo cbbDepartmentCode = new UltraCombo();
            cbbDepartmentCode.DataSource = _IDepartmentService.GetAll_ByIsActive_OrderByDepartmentCode(true);
            cbbDepartmentCode.DisplayMember = "DepartmentCode";
            cbbDepartmentCode.ValueMember = "ID";
            Utils.ConfigGrid(cbbDepartmentCode, ConstDatabase.Department_TableName);
            uGridDS.DisplayLayout.Bands[0].Columns["DepartmentCode"].ValueList = cbbDepartmentCode;

            UltraCombo cbbExpenseItemCode = new UltraCombo();
            cbbExpenseItemCode.DataSource = _IExpenseItemService.GetByActive_OrderByTreeIsParentNode(true);
            cbbExpenseItemCode.DisplayMember = "ExpenseItemCode";
            cbbExpenseItemCode.ValueMember = "ID";
            Utils.ConfigGrid(cbbExpenseItemCode, ConstDatabase.ExpenseItem_TableName);
            uGridDS.DisplayLayout.Bands[0].Columns["ExpenseItemCode"].ValueList = cbbExpenseItemCode;

            UltraCombo cbbContractCode = new UltraCombo();
            cbbContractCode.DataSource = _IContractStateService.GetListContractStateOrder();
            cbbContractCode.DisplayMember = "ContractStateCode";
            cbbContractCode.ValueMember = "ID";
            Utils.ConfigGrid(cbbContractCode, ConstDatabase.ContractState_TableName);
            uGridDS.DisplayLayout.Bands[0].Columns["ContractCode"].ValueList = cbbContractCode;

            UltraCombo cbbCostSetCode = new UltraCombo();
            cbbCostSetCode.DataSource = _ICostSetService.GetCostSetCodeBool(true);
            cbbCostSetCode.DisplayMember = "CostSetCode";
            cbbCostSetCode.ValueMember = "ID";
            Utils.ConfigGrid(cbbCostSetCode, ConstDatabase.CostSet_TableName);
            uGridDS.DisplayLayout.Bands[0].Columns["CostSetID"].ValueList = cbbCostSetCode;

            uGridDS.DisplayLayout.Bands[0].Columns["DepreciationAmount"].Format = "#,###,###,##đ";
            uGridDS.DisplayLayout.Bands[0].Columns["DepreciationAmount"].MaskInput = "nn,nnn,nnn,nnn,nnn,nnn,nnn,nnn";

            UltraGridBand band = uGridDS.DisplayLayout.Bands[0];
            ugridDisColums(band.Columns["FixedAssetCode"]);
            ugridDisColums(band.Columns["FixedAssetName"]);
            ugridDisColums(band.Columns["AccountingObjectName"]);
        }
        #region button
        private void btnSelected_Click(object sender, EventArgs e)
        {
            Choosen();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {

            Deleted();
        }

        private void btnDeleteAll_Click(object sender, EventArgs e)
        {
            DeleteAll();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion

        void Choosen()
        {

            var lstChoseAssets = LstFixedAssets.Where(item => LstFaaCostApplications.Count(k => k.FixedAssetCode.Equals(item.FixedAssetCode)) <= 0).ToList();
            new FFAACostApplicationDetail(lstChoseAssets).ShowDialog(this);
            var tempmp = _ICostSetService.Query.FirstOrDefault(k => k.ID == LstFaaCostApplications[0].CostSetID);
            uGridDS.DataSource = Utils.CloneObject(LstFaaCostApplications);
        }

        private static List<TemplateColumn> MauGiaoDien()
        {
            return new List<TemplateColumn> 
            {
                new TemplateColumn()
                {
                    ColumnName = "FixedAssetCode",
                    ColumnCaption = "Mã TSCĐ",
                    //ColumnWidth = 20,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 0
                },
                new TemplateColumn()
                {
                    ColumnName = "FixedAssetName",
                    ColumnCaption = "Tên TSCĐ",
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 1
                },
                  new TemplateColumn()
                {
                   ColumnName = "AccountingObjectName",
                    ColumnCaption = "Số TK",
                    //ColumnWidth = 20,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 2
                },
                new TemplateColumn()
                {
                    ColumnName = "DepreciationAmount",
                    ColumnCaption = "Giá trị khấu hao",
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 3
                },
                   new TemplateColumn()
                {
                    ColumnName = "DepartmentCode",
                    ColumnCaption = "Phòng ban",
                    //ColumnWidth = 20,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 4
                },
                new TemplateColumn()
                {
                    ColumnName = "ExpenseItemCode",
                    ColumnCaption = "Khoản mục CP",
                  
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 5
                },
                new TemplateColumn()
                {
                    ColumnName = "ContractCode",
                    ColumnCaption = "Hợp đồng",
                    
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition =6
                },
                 new TemplateColumn()
                {
                    ColumnName = "CostSetID",
                    ColumnCaption = "ĐT tập hợp CP",
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 7
                },
                  new TemplateColumn()
                {
                    ColumnName = "Rate",
                    ColumnCaption = "Tỷ kệ %",
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 8
                },
                 new TemplateColumn()
                {
                    ColumnName = "IsIrrationalCost",
                    ColumnCaption = "CP không hợp lý",
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 9
                }
             };
        }

        void DeleteAll()
        {
            LstFaaCostApplications.Clear();
            var newList = new List<FAACostApplicationDetail>();
            uGridDS.DataSource = newList;
            Utils.ConfigGrid(uGridDS, "", MauGiaoDien());
        }

        void Deleted()
        {


            if (uGridDS.Selected.Rows.Count > 0)
            {
                FAACostApplicationDetail temp = uGridDS.Selected.Rows[0].ListObject as FAACostApplicationDetail;
                //int temp = (int) uGridDS.Selected.Rows[0].ListObject;
                uGridDS.DataSource = LstFaaCostApplications.Remove(temp);
                Utils.ConfigGrid(uGridDS, "", MauGiaoDien());
            }
        }

        void Save()
        {
            #region Check Lỗi
            //   if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj


            FADepreciationAllocation temp = Them ? new FADepreciationAllocation() : _IFADepreciatioAllocationService.Getbykey(_Select.ID);
            List<FAACostApplicationDetail> lstGrid = ((List<FAACostApplicationDetail>)uGridDS.DataSource).ToList();
            foreach (FAACostApplicationDetail faaCostApplicationDetail in lstGrid)
            {

            }
            temp = ObjandGUI(temp);
            #endregion

            #region Thao tác CSDL
            _IFADepreciatioAllocationService.BeginTran();
            if (Them) _IFADepreciatioAllocationService.CreateNew(temp);
            else _IFADepreciatioAllocationService.Update(temp);
            _IFADepreciatioAllocationService.CommitTran();
            #endregion


        }
        #region utils

        FADepreciationAllocation ObjandGUI(FADepreciationAllocation input)
        {
            if (uGridDS.Rows.Count > 0) uGridDS.Rows[0].Selected = true;
           
            Close();
            return input;
        }
        void ugridDisColums(UltraGridColumn ultraGrid)
        {
            ultraGrid.CellActivation = Activation.NoEdit;
            ultraGrid.CellClickAction = CellClickAction.RowSelect;
        }
        #endregion
    }
}
