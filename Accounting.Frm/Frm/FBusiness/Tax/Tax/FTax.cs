﻿using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FTax : CatalogBase
    {
        public int check;
        public FTax(int check_)
        {
            check = check_;
            InitializeComponent();


            LoaduGrid();
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            System.Windows.Forms.ToolTip ToolTip2 = new System.Windows.Forms.ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            System.Windows.Forms.ToolTip ToolTip3 = new System.Windows.Forms.ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
        }

        public void LoaduGrid()
        {
            Utils.ClearCacheByType<TM02GTGT>();
            Utils.ClearCacheByType<TM02GTGTDetail>();
            Utils.ClearCacheByType<TM01GTGT>();
            Utils.ClearCacheByType<TM01GTGTDetail>();
            Utils.ClearCacheByType<TM03TNDN>();
            Utils.ClearCacheByType<TM03TNDNDetail>();
            Utils.ClearCacheByType<TM01TAIN>();
            Utils.ClearCacheByType<TM01TAINDetail>();
            Utils.ClearCacheByType<TM02TAIN>();
            Utils.ClearCacheByType<TM02TAINDetail>();
            Utils.ClearCacheByType<GOtherVoucher>();
            Utils.ClearCacheByType<GOtherVoucherDetail>();
            Utils.ClearCacheByType<TM01GTGT>();
            Utils.ClearCacheByType<TM01GTGTDetail>();
            Utils.ClearCacheByType<TM04GTGT>();
            Utils.ClearCacheByType<TM04GTGTDetail>();
            List<Tax> lstT = new List<Tax>();
            if (check == 0)
            {
                foreach (var item in Utils.ListTM01GTGT)
                {
                    lstT.Add(new Tax
                    {
                        ID = item.ID,
                        TypeID = item.TypeID,
                        DeclarationName = item.DeclarationName,
                        DeclarationTerm = item.DeclarationTerm,
                        IsFirstDeclaration = item.IsFirstDeclaration,
                        AdditionTime = item.AdditionTime,
                        DateTo = item.ToDate
                    });
                }
            }
            else if (check == 1)
            {
                foreach (var item in Utils.ListTM02GTGT)
                {
                    lstT.Add(new Tax
                    {
                        ID = item.ID,
                        TypeID = item.TypeID,
                        DeclarationName = item.DeclarationName,
                        DeclarationTerm = item.DeclarationTerm,
                        IsFirstDeclaration = item.IsFirstDeclaration,
                        AdditionTime = item.AdditionTime,
                        DateTo = item.ToDate
                    });
                }
            }
            else if (check == 2)
            {
                foreach (var item in Utils.ListTM03TNDN)
                {
                    lstT.Add(new Tax
                    {
                        ID = item.ID,
                        TypeID = item.TypeID,
                        DeclarationName = item.DeclarationName,
                        DeclarationTerm = item.DeclarationTerm,
                        IsFirstDeclaration = item.IsFirstDeclaration,
                        AdditionTime = item.AdditionTime,
                        DateTo = item.ToDate
                    });
                }
            }
            else if (check == 4)
            {
                foreach (var item in Utils.ListTM01TAIN)
                {
                    lstT.Add(new Tax
                    {
                        ID = item.ID,
                        TypeID = item.TypeID,
                        DeclarationName = item.DeclarationName,
                        DeclarationTerm = item.DeclarationTerm,
                        IsFirstDeclaration = item.IsFirstDeclaration,
                        AdditionTime = item.AdditionTime,
                        DateTo = item.ToDate
                    });
                }
            }
            else if (check == 5)
            {
                foreach (var item in Utils.ListTM02TAIN)
                {
                    lstT.Add(new Tax
                    {
                        ID = item.ID,
                        TypeID = item.TypeID,
                        DeclarationName = item.DeclarationName,
                        DeclarationTerm = item.DeclarationTerm,
                        IsFirstDeclaration = item.IsFirstDeclaration,
                        AdditionTime = item.AdditionTime,
                        DateTo = item.ToDate
                    });
                }
            }
            else if (check == 6)
            {
                foreach (var item in Utils.ListTM04GTGT)
                {
                    lstT.Add(new Tax
                    {
                        ID = item.ID,
                        TypeID = item.TypeID,
                        DeclarationName = item.DeclarationName,
                        DeclarationTerm = item.DeclarationTerm,
                        IsFirstDeclaration = item.IsFirstDeclaration,
                        AdditionTime = item.AdditionTime,
                        DateTo = item.ToDate
                    });
                }
            }
            uGrid.DataSource = lstT.OrderByDescending(n => n.DateTo).ToList();
            Utils.ConfigGrid(uGrid, ConstDatabase.Tax_TableName);
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFuntion();
        }
        public void editFuntion(Tax tax)
        {
            if (tax.TypeID == 921)
            {
                try
                {
                    TM02GTGT tM02GTGT = Utils.ListTM02GTGT.FirstOrDefault(n => n.ID == tax.ID);
                    Declaration_02_GTGT declaration_02_GTGT = new Declaration_02_GTGT(tM02GTGT, true);
                    //declaration_02_GTGT.ShowDialog(this);
                    //LoaduGrid();
                    //HUYPD Edit Show Form
                    declaration_02_GTGT.FormClosed += new FormClosedEventHandler(FTaxDetail_FormClosed);
                    declaration_02_GTGT.Show(this);
                }
                catch (Exception ex)
                {
                    MSG.Error(ex.Message);
                }

            }
            else if (tax.TypeID == 920)
            {
                try
                {
                    TM01GTGT tM01GTGT = Utils.ListTM01GTGT.FirstOrDefault(n => n.ID == tax.ID);
                    _01_GTGT_Detail _01_GTGT_Detail = new _01_GTGT_Detail(tM01GTGT, true);
                    //_01_GTGT_Detail.ShowDialog(this);
                    //LoaduGrid();
                    //HUYPD Edit Show Form
                    _01_GTGT_Detail.FormClosed += new FormClosedEventHandler(FTaxDetail_FormClosed);
                    _01_GTGT_Detail.Show(this);
                }
                catch (Exception ex)
                {
                    MSG.Error(ex.Message);
                }
            }
            else if (tax.TypeID == 922)
            {
                try
                {
                    TM03TNDN tM03TNDN = Utils.ListTM03TNDN.FirstOrDefault(n => n.ID == tax.ID);
                    _03_TNDN_Detail _03_TNDN_Detail = new _03_TNDN_Detail(tM03TNDN, true);
                    //_03_TNDN_Detail.ShowDialog(this);
                    //LoaduGrid();
                    //HUYPD Edit Show Form
                    _03_TNDN_Detail.FormClosed += new FormClosedEventHandler(FTaxDetail_FormClosed);
                    _03_TNDN_Detail.Show(this);
                }
                catch (Exception ex)
                {
                    MSG.Error(ex.Message);
                }
            }
            else if (tax.TypeID == 924)
            {
                try
                {
                    TM01TAIN tM01TAIN = Utils.ListTM01TAIN.FirstOrDefault(n => n.ID == tax.ID);
                    _01_TAIN_Detail _01_TAIN_Detail = new _01_TAIN_Detail(tM01TAIN, true);
                    //_01_TAIN_Detail.ShowDialog(this);
                    //LoaduGrid();
                    //HUYPD Edit Show Form
                    _01_TAIN_Detail.FormClosed += new FormClosedEventHandler(FTaxDetail_FormClosed);
                    _01_TAIN_Detail.Show(this);
                }
                catch (Exception ex)
                {
                    MSG.Error(ex.Message);
                }
            }
            else if (tax.TypeID == 925)
            {
                try
                {
                    TM02TAIN tM02TAIN = Utils.ListTM02TAIN.FirstOrDefault(n => n.ID == tax.ID);
                    _02_TAIN_Detail _02_TAIN_Detail = new _02_TAIN_Detail(tM02TAIN, true);
                    //_02_TAIN_Detail.ShowDialog(this);
                    //LoaduGrid();
                    //HUYPD Edit Show Form
                    _02_TAIN_Detail.FormClosed += new FormClosedEventHandler(FTaxDetail_FormClosed);
                    _02_TAIN_Detail.Show(this);
                }
                catch (Exception ex)
                {
                    MSG.Error(ex.Message);
                }
            }
            else if (tax.TypeID == 926)
            {
                try
                {
                    TM04GTGT tM04GTGT = Utils.ListTM04GTGT.FirstOrDefault(n => n.ID == tax.ID);
                    _04_GTGT_Detail _04_GTGT_Detail = new _04_GTGT_Detail(tM04GTGT, true);
                    //_04_GTGT_Detail.ShowDialog(this);
                    //LoaduGrid();
                    //HUYPD Edit Show Form
                    _04_GTGT_Detail.FormClosed += new FormClosedEventHandler(FTaxDetail_FormClosed);
                    _04_GTGT_Detail.Show(this);
                }
                catch (Exception ex)
                {
                    MSG.Error(ex.Message);
                }
            }
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFuntion();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFuntion();
            //T 
        }
        private void FTaxDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            LoaduGrid();
        }
        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            bool a = uGrid.Selected.Rows.Count > 0;
            bool b = uGrid.ActiveRow == null;
            if (b) return;
            if (a)
            {
                SelectedRowsCollection lst = uGrid.Selected.Rows;
                Tax temp = (Tax)uGrid.ActiveRow.ListObject;
                editFuntion(temp);

            }
        }
        public void EditFuntion()
        {
            bool a = uGrid.Selected.Rows.Count > 0;
            bool b = uGrid.ActiveRow != null;
            if (a || b)
            {
                SelectedRowsCollection lst = uGrid.Selected.Rows;
                Tax temp = (Tax)uGrid.ActiveRow.ListObject;
                editFuntion(temp);

            }
        }
        public void AddFuntion()
        {
            switch (check)
            {
                case 0:
                    TaxPeriod_01_GTGT taxPeriod_01_GTGT = new TaxPeriod_01_GTGT();
                    //taxPeriod_01_GTGT.ShowDialog(this);
                    //LoaduGrid();
                    //HUYPD Edit Show Form
                    taxPeriod_01_GTGT.FormClosed += new FormClosedEventHandler(FTaxDetail_FormClosed);
                    taxPeriod_01_GTGT.Show(this);
                    break;
                case 1:
                    TaxPeriod_02_GTGT taxPeriod_02_GTGT = new TaxPeriod_02_GTGT();
                    //taxPeriod_02_GTGT.ShowDialog(this);
                    //LoaduGrid();
                    //HUYPD Edit Show Form
                    taxPeriod_02_GTGT.FormClosed += new FormClosedEventHandler(FTaxDetail_FormClosed);
                    taxPeriod_02_GTGT.Show(this);
                    break;
                case 2:
                    TaxPeriod_03_TNDN taxPeriod_03_TNDN = new TaxPeriod_03_TNDN();
                    //taxPeriod_03_TNDN.ShowDialog(this);
                    //LoaduGrid();
                    //HUYPD Edit Show Form
                    taxPeriod_03_TNDN.FormClosed += new FormClosedEventHandler(FTaxDetail_FormClosed);
                    taxPeriod_03_TNDN.Show(this);
                    break;
                case 3:
                    TaxPeriod_01_TTDB taxPeriod_01_TTDB = new TaxPeriod_01_TTDB();
                    //taxPeriod_01_TTDB.ShowDialog(this);
                    //LoaduGrid();
                    //HUYPD Edit Show Form
                    taxPeriod_01_TTDB.FormClosed += new FormClosedEventHandler(FTaxDetail_FormClosed);
                    taxPeriod_01_TTDB.Show(this);
                    break;
                case 4:
                    TaxPeriod_01_TAIN taxPeriod_01_TAIN = new TaxPeriod_01_TAIN();
                    //taxPeriod_01_TAIN.ShowDialog(this);
                    //LoaduGrid();
                    //HUYPD Edit Show Form
                    taxPeriod_01_TAIN.FormClosed += new FormClosedEventHandler(FTaxDetail_FormClosed);
                    taxPeriod_01_TAIN.Show(this);
                    break;
                case 5:
                    TaxPeriod_02_TAIN period_02_TAIN = new TaxPeriod_02_TAIN();
                    //period_02_TAIN.ShowDialog(this);
                    //LoaduGrid();
                    //HUYPD Edit Show Form
                    period_02_TAIN.FormClosed += new FormClosedEventHandler(FTaxDetail_FormClosed);
                    period_02_TAIN.Show(this);
                    break;
                case 6:
                    TaxPeriod_04_GTGT period_04_GTGT = new TaxPeriod_04_GTGT();
                    //period_04_GTGT.ShowDialog(this);
                    //LoaduGrid();
                    //HUYPD Edit Show Form
                    period_04_GTGT.FormClosed += new FormClosedEventHandler(FTaxDetail_FormClosed);
                    period_04_GTGT.Show(this);
                    break;
            }
        }
        public void DeleteFuntion()
        {
            bool a = uGrid.Selected.Rows.Count > 0;
            bool b = uGrid.ActiveRow != null;
            if (a || b)
            {
                if (!(DialogResult.Yes == MessageBox.Show("Bạn có muốn xóa tờ khai này không ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question)))
                    return;
                Tax temp = (Tax)uGrid.ActiveRow.ListObject;
                if (temp.TypeID == 921)
                {
                    try
                    {
                        TM02GTGT tM02GTGT = Utils.ListTM02GTGT.FirstOrDefault(n => n.ID == temp.ID);
                        Utils.ITM02GTGTService.BeginTran();
                        Utils.ITM02GTGTService.Delete(tM02GTGT);
                        Utils.ITM02GTGTService.CommitTran();
                        LoaduGrid();

                    }
                    catch
                    {
                        Utils.ITM02GTGTService.RolbackTran();
                        MSG.Error("Xóa chứng từ thất bại");
                    }

                }
                else if (temp.TypeID == 920)
                {
                    try
                    {
                        TM01GTGT tM01GTGT = Utils.ListTM01GTGT.FirstOrDefault(n => n.ID == temp.ID);
                        DateTime _datecheck = new DateTime(temp.DateTo.Year, temp.DateTo.Month, temp.DateTo.Day);
                        var _lstCheckCreate = Utils.ListGOtherVoucher.FirstOrDefault(g => g.TypeID == 602 && g.PostedDate == _datecheck);
                        var hasChild = Utils.ListTM01GTGT.Where(n => n.BranchID == temp.ID).FirstOrDefault();
                        if (_lstCheckCreate != null && tM01GTGT.IsFirstDeclaration)
                        {
                            MSG.Warning("Tờ khai này đã phát sinh chứng từ nghiệp vụ khấu trừ thuế. Vui lòng xóa chứng từ khấu trừ thuế trước khi thực hiện xóa tờ khai này!");
                            return;
                        }
                        if (hasChild != null)
                        {
                            MSG.Warning("Bạn phải xóa tờ khai bổ sung trước khi xóa tờ khai lần đầu của " + tM01GTGT.DeclarationTerm);
                            return;
                        }
                        Utils.ITM01GTGTService.BeginTran();
                        Utils.ITM01GTGTService.Delete(tM01GTGT);
                        Utils.ITM01GTGTService.CommitTran();
                        LoaduGrid();

                    }
                    catch (Exception ex)
                    {
                        Utils.ITM01GTGTService.RolbackTran();
                        MSG.Error("Xóa chứng từ thất bại");

                    }

                }
                else if (temp.TypeID == 922)
                {
                    try
                    {
                        TM03TNDN tM03TNDN = Utils.ListTM03TNDN.FirstOrDefault(n => n.ID == temp.ID);
                        Utils.ITM03TNDNService.BeginTran();
                        Utils.ITM03TNDNService.Delete(tM03TNDN);
                        Utils.ITM03TNDNService.CommitTran();
                        LoaduGrid();
                    }
                    catch (Exception ex)
                    {
                        Utils.ITM03TNDNService.RolbackTran();
                        MSG.Error("Xóa chứng từ thất bại");

                    }

                }
                else if (temp.TypeID == 924)
                {
                    try
                    {
                        TM01TAIN tM01TAIN = Utils.ListTM01TAIN.FirstOrDefault(n => n.ID == temp.ID);
                        Utils.ITM01TAINService.BeginTran();
                        Utils.ITM01TAINService.Delete(tM01TAIN);
                        Utils.ITM01TAINService.CommitTran();
                        LoaduGrid();
                    }
                    catch (Exception ex)
                    {
                        Utils.ITM01TAINService.RolbackTran();
                        MSG.Error("Xóa chứng từ thất bại");

                    }

                }
                else if (temp.TypeID == 925)
                {
                    try
                    {
                        TM02TAIN tM02TAIN = Utils.ListTM02TAIN.FirstOrDefault(n => n.ID == temp.ID);
                        Utils.ITM02TAINService.BeginTran();
                        Utils.ITM02TAINService.Delete(tM02TAIN);
                        Utils.ITM02TAINService.CommitTran();
                        LoaduGrid();
                    }
                    catch (Exception ex)
                    {
                        Utils.ITM02TAINService.RolbackTran();
                        MSG.Error("Xóa chứng từ thất bại");

                    }

                }
                else if (temp.TypeID == 926)
                {
                    try
                    {
                        TM04GTGT tM04GTGT = Utils.ListTM04GTGT.FirstOrDefault(n => n.ID == temp.ID);
                        Utils.ITM04GTGTService.BeginTran();
                        Utils.ITM04GTGTService.Delete(tM04GTGT);
                        Utils.ITM04GTGTService.CommitTran();
                        LoaduGrid();
                    }
                    catch (Exception ex)
                    {
                        Utils.ITM04GTGTService.RolbackTran();
                        MSG.Error("Xóa chứng từ thất bại");

                    }

                }


            }
            else
            {
                MSG.Error("Chưa chọn tờ khai");
                return;
            }
        }
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFuntion();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFuntion();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFuntion();
        }
        protected override void AddFunction()
        {
            AddFuntion();
        }
        protected override void EditFunction()
        {
            EditFuntion();
        }

        protected override void DeleteFunction()
        {
            DeleteFuntion();
        }
        protected override void ResetFunction()
        {
            //ResetFunction();
        }

        private void FTax_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }

        private void FTax_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }
    }
}
