﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using Accounting.Core.IService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using Accounting.Core.Domain.obj.Report;
using Infragistics.Win.UltraWinMaskedEdit;
using Accounting.Core.DAO;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using System.Xml;
using Accounting.Core.ServiceImp;

namespace Accounting
{

    public partial class _01_GTGT_Detail : Form
    {
        #region Khai báo
        public string DeclarationTerm = "";
        public bool IsFirst = false;
        public string AdditionalTime = "";
        // public ISystemOptionService _ISystemOptionService { get { return IoC.Resolve<ISystemOptionService>(); } }
        public decimal item22 = 0;
        public decimal item23 = 0;
        public decimal item24 = 0;
        public decimal item25 = 0;
        public decimal item26 = 0;
        public decimal item27 = 0;
        public decimal item28 = 0;
        public decimal item29 = 0;
        public decimal item30 = 0;
        public decimal item31 = 0;
        public decimal item32 = 0;
        public decimal item32a = 0;
        public decimal item33 = 0;
        public decimal item34 = 0;
        public decimal item35 = 0;
        public decimal item36 = 0;
        public decimal item37 = 0;
        public decimal item38 = 0;
        public decimal item39 = 0;
        public decimal item40 = 0;
        public decimal item40a = 0;
        public decimal item40b = 0;
        public decimal item41 = 0;
        public decimal item42 = 0;
        public decimal item43 = 0;
        public int MaxAdditionTime = 0;
        public DateTime FromDate = DateTime.Now;
        public DateTime ToDate = DateTime.Now;
        public DateTime KHBS = DateTime.Now;
        public readonly ITMIndustryTypeService _ITMIndustryTypeService;
        private TM01GTGT _select;
        private bool _isEdit;
        public bool checkSave = false;
        public bool isThang = false;
        public bool isQuy = false;
        public List<TMAppendixList> _lstTMAppendixList = new List<TMAppendixList>();
        private TM01GTGT _tM01GTGTBS;
        public bool _IsAppendix011GTGT;
        public bool _IsAppendix012GTGT;
        public bool CheckBS;
        List<TM01GTGTAdjust> lst1 = new List<TM01GTGTAdjust>();
        List<TM01GTGTAdjust> lst2 = new List<TM01GTGTAdjust>();
        public List<BangKeMuaVao_Model> lstTaxPL2 = new List<BangKeMuaVao_Model>();
        public List<BangKeBanRa_Model> lstTaxPL1 = new List<BangKeBanRa_Model>();
        List<string> lstReadOnly = new List<string>();
        Guid ParentID;
        Guid VoucherID;
        Dictionary<string, string> listTMIndustryType;
        string CheckClick;// biến sinh ra để check xem người dùng đang click vào datagridview nào, từ đó check để khi ấn phím tắt Ctr+f5, f6 biết mà thêm vào datagridview đó

        #endregion

        #region khởi tạo form
        public _01_GTGT_Detail(TM01GTGT temp, bool isEdit)
        {
            CheckClick = "";
            _ITMIndustryTypeService = IoC.Resolve<ITMIndustryTypeService>();
            Utils.ClearCacheByType<GOtherVoucher>();
            Utils.ClearCacheByType<GOtherVoucherDetail>();
            _select = temp;
            _isEdit = isEdit;

            InitializeComponent();
            TinhTongCot5va6();
            if (isEdit)
            {
                ObjandGUI(temp, false);
                LoadReadOnly(true);
                utmDetailBaseToolBar.Tools["mnbtnSave"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = true;
                utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Enabled = true;
                LoadMaskInp();

            }
            else
            {
                LoadReadOnly(false);
                utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["mnbtnDelete"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Enabled = false;
                LoadMaskInp();

            }
            //SystemOptionService _ISystemOptionService;
            Utils.ClearCacheByType<SystemOption>();

            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
                this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
            }


            LoadCbb();
            ConfigVND();
            ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[0];
            utmDetailBaseToolBar.Ribbon.NonInheritedRibbonTabs[0].Caption = @"Chức năng";


        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams handleParam = base.CreateParams;
                handleParam.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED       
                return handleParam;
            }
        }
        #endregion

        #region Định dạng tiền vnđ
        private void ConfigVND()
        {
            lblDeclaredTaxAmount.FormatNumberic(lblDeclaredTaxAmount.Text, ConstDatabase.Format_TienVND);
            lblAdjustTaxAmount.FormatNumberic(lblAdjustTaxAmount.Text, ConstDatabase.Format_TienVND);
            lblDifTaxAmount.FormatNumberic(lblDifTaxAmount.Text, ConstDatabase.Format_TienVND);
            lblDeclaredDiscountAmount.FormatNumberic(lblDeclaredDiscountAmount.Text, ConstDatabase.Format_TienVND);
            lblAdjustDiscountAmount.FormatNumberic(lblAdjustDiscountAmount.Text, ConstDatabase.Format_TienVND);
            lblDifDiscountAmount.FormatNumberic(lblDifDiscountAmount.Text, ConstDatabase.Format_TienVND);
            //txtItem26.FormatNumberic(txtItem26.Text, ConstDatabase.Format_TienVND);
            txtItem27.FormatNumberic(txtItem27.Text, ConstDatabase.Format_TienVND);
            txtItem28.FormatNumberic(txtItem28.Text, ConstDatabase.Format_TienVND);
            txtItem34.FormatNumberic(txtItem34.Text, ConstDatabase.Format_TienVND);
            txtItem35.FormatNumberic(txtItem35.Text, ConstDatabase.Format_TienVND);
            txtItem36.FormatNumberic(txtItem36.Text, ConstDatabase.Format_TienVND);
            txtItem40a.FormatNumberic(txtItem40a.Text, ConstDatabase.Format_TienVND);
            txtItem40.FormatNumberic(txtItem40.Text, ConstDatabase.Format_TienVND);
            txtItem41.FormatNumberic(txtItem41.Text, ConstDatabase.Format_TienVND);
            txtItem43.FormatNumberic(txtItem43.Text, ConstDatabase.Format_TienVND);
        }
        #endregion

        #region lấy giá trị ban đầu của chỉ tiêu
        private void FillDefaut()
        {
            item23 = getValueMaskedEdit(txtItem23);
            item24 = getValueMaskedEdit(txtItem24);
            item25 = getValueMaskedEdit(txtItem25);
            item26 = getValueMaskedEdit(txtItem26);
            item27 = getValueLabel(txtItem27);
            item28 = getValueLabel(txtItem28);
            item29 = getValueMaskedEdit(txtItem29);
            item30 = getValueMaskedEdit(txtItem30);
            item31 = getValueMaskedEdit(txtItem31);
            item32 = getValueMaskedEdit(txtItem32);
            item32a = getValueMaskedEdit(txtItem32a);
            item33 = getValueMaskedEdit(txtItem33);
            item34 = getValueLabel(txtItem34);
            item35 = getValueLabel(txtItem35);
            item36 = getValueLabel(txtItem36);
            item37 = getValueMaskedEdit(txtItem37);
            item38 = getValueMaskedEdit(txtItem38);
            item39 = getValueMaskedEdit(txtItem39);
            item40 = getValueLabel(txtItem40);
            item40a = getValueLabel(txtItem40a);
            item40b = getValueMaskedEdit(txtItem40b);
            item41 = getValueLabel(txtItem41);
            item42 = getValueMaskedEdit(txtItem42);
            item43 = getValueLabel(txtItem43);
        }
        #endregion

        #region Load cbb
        public void LoadCbb()
        {
            listTMIndustryType = new Dictionary<string, string>();
            listTMIndustryType.Add("0", " ");
            listTMIndustryType.Add("1", "Doanh nghiệp có quy mô nhỏ và vừa");
            listTMIndustryType.Add("2", "Doanh nghiệp sử dụng nhiều lao động");
            listTMIndustryType.Add("3", "Doanh nghiệp đầu tư – kinh doanh (bán, cho thuê, cho thuê mua) nhà ở");
            listTMIndustryType.Add("4", "Lý do khác");//trungnq sửa combo thuế sai datasource và sai readonly
            //cbbExtensionCase.DataSource = Utils.ListTMIndustryType.OrderBy(n => n.IndustryTypeCode).ToList();
            //this.ConfigCombo(Utils.ListTMIndustryType, cbbExtendCase, "IndustryTypeName", "ID");
            cbbExtensionCase.DataSource = listTMIndustryType;

            Utils.ConfigGrid(cbbExtensionCase, ConstDatabase.TMIndustryTypeGH_TableName);
            cbbExtensionCase.DisplayMember = "Value";
            cbbExtensionCase.ValueMember = "Key";
        }
        #endregion

        #region GetNameGroup
        private string GetNameGroup(decimal ma)
        {
            string ten = "";
            switch (ma)
            {
                case -1:
                    ten = "Hàng hoá, dịch vụ không chịu thuế GTGT";
                    break;
                case 0:
                    ten = "Hàng hoá, dịch vụ chịu thuế GTGT 0%";
                    break;
                case 5:
                    ten = "Hàng hoá, dịch vụ chịu thuế GTGT 5%";
                    break;
                case 10:
                    ten = "Hàng hoá, dịch vụ chịu thuế GTGT 10%";
                    break;
                default:
                    break;
            }
            return ten;
        }
        #endregion

        #region Fill chỉ tiêu vào báo cáo

        #endregion

        #region Config MaskedEdit
        private void ConfigMaskedEdit(UltraMaskedEdit txt)
        {
            txt.AutoSize = false;
            txt.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            txt.Dock = DockStyle.Fill;
            txt.Margin = new Padding(0);
            txt.PromptChar = ' ';
            txt.DisplayMode = MaskMode.IncludeLiterals;
            txt.EditAs = EditAsType.Double;
            txt.InputMask = "n,nnn,nnn,nnn,nnn,nnn";

        }
        #endregion

        #region LoadInitialize
        public bool LoadInitialize(bool isGet)
        {
            if (isGet)
            { }
            else
            {
                if (item22 != 0)
                    BindData(txtItem22, item22);
                lblDeclarationTerm.Text = DeclarationTerm;
                chkFirstDeclared.Checked = IsFirst;
                txtAdditionTime.Value = AdditionalTime;
                lblCompanyTaxCode.Text = Utils.GetCompanyTaxCode();
                lblCompanyName.Text = Utils.GetCompanyName();
                lblAgencyTaxCode.Text = Utils.GetMSTDLT();
                lblTaxAgencyName.Text = Utils.GetTenDLT();
                txtName.Text = Utils.GetHVTNhanVienDLT();
                txtCertificationNo.Text = Utils.GetCCHNDLT();
                txtSignDate.Value = DateTime.Now;
                txtSignName.Text = Utils.GetCompanyDirector();
                if (_lstTMAppendixList.Exists(n => n.TypeID == 101))
                {
                    ultraTabControl1.Tabs[1].Visible = true;
                    #region Fill dữ liệu vào PL1
                    ReportProcedureSDS sp = new ReportProcedureSDS();
                    List<BangKeBanRa_Model> data1 = new List<BangKeBanRa_Model>();
                    data1 = sp.GetProc_GetBkeInv_Sale(FromDate, ToDate, 1, 0, "");
                    data1.ForEach(
                        t =>
                        {
                            t.VATRATENAME = GetNameGroup(t.VATRATE);
                            if (t.FLAG == -1)
                            {
                                t.DOANH_SO = -t.DOANH_SO;
                                t.THUE_GTGT = -t.THUE_GTGT;
                            }
                        });
                    lstTaxPL1 = data1;
                    #endregion
                    #region Load Ugrid 1
                    LoadUgrid1(lstTaxPL1.Where(n => n.VATRATE == -1M).OrderBy(n => n.NGAY_HD).ToList());
                    #endregion
                    #region Load Ugrid 2
                    LoadUgrid2(lstTaxPL1.Where(n => n.VATRATE == 0M).OrderBy(n => n.NGAY_HD).ToList());
                    #endregion
                    #region Load Ugrid 3
                    LoadUgrid3(lstTaxPL1.Where(n => n.VATRATE == 5M).OrderBy(n => n.NGAY_HD).ToList());
                    #endregion
                    #region Load Ugrid 4
                    LoadUgrid4(lstTaxPL1.Where(n => n.VATRATE == 10M).OrderBy(n => n.NGAY_HD).ToList());
                    #endregion
                    _IsAppendix011GTGT = true;
                    BindData(txtItem31, getValueLabel(txtTaxAmount5));
                    BindData(txtItem33, getValueLabel(txtTaxAmount10));

                }
                else
                {
                    ultraTabControl1.Tabs[1].Visible = false;
                    #region Fill dữ liệu vào PL1
                    ReportProcedureSDS sp = new ReportProcedureSDS();
                    List<BangKeBanRa_Model> data1 = new List<BangKeBanRa_Model>();
                    data1 = sp.GetProc_GetBkeInv_Sale(FromDate, ToDate, 1, 0, "");
                    data1.ForEach(
                        t =>
                        {
                            t.VATRATENAME = GetNameGroup(t.VATRATE);
                            if (t.FLAG == -1)
                            {
                                t.DOANH_SO = -t.DOANH_SO;
                                t.THUE_GTGT = -t.THUE_GTGT;
                            }
                        });
                    lstTaxPL1 = data1;
                    #endregion
                    #region Load Ugrid 1
                    LoadUgrid1(lstTaxPL1.Where(n => n.VATRATE == -1M).OrderBy(n => n.NGAY_HD).ToList());
                    #endregion
                    #region Load Ugrid 2
                    LoadUgrid2(lstTaxPL1.Where(n => n.VATRATE == 0M).OrderBy(n => n.NGAY_HD).ToList());
                    #endregion
                    #region Load Ugrid 3
                    LoadUgrid3(lstTaxPL1.Where(n => n.VATRATE == 5M).OrderBy(n => n.NGAY_HD).ToList());
                    #endregion
                    #region Load Ugrid 4
                    LoadUgrid4(lstTaxPL1.Where(n => n.VATRATE == 10M).OrderBy(n => n.NGAY_HD).ToList());
                    #endregion
                    BindData(txtItem31, getValueLabel(txtTaxAmount5));
                    BindData(txtItem33, getValueLabel(txtTaxAmount10));
                    _IsAppendix011GTGT = false;
                }
                if (_lstTMAppendixList.Exists(n => n.TypeID == 102))
                {
                    #region Fill dữ liệu vào PL2
                    ReportProcedureSDS sp = new ReportProcedureSDS();
                    List<BangKeMuaVao_Model> data = new List<BangKeMuaVao_Model>();
                    data = sp.GetProc_GetBkeInv_BuyTax(FromDate, ToDate, 1, 0, "", 0);//add by cuongpv: them dk check khau tru thue hay ko
                    data.ForEach(t =>
                    {
                        t.THUE_SUAT_DETAIL = Convert.ToInt32(t.THUE_SUAT);
                        if (t.FLAG == -1)
                        {
                            t.GT_CHUATHUE = -t.GT_CHUATHUE;
                            t.THUE_GTGT = -t.THUE_GTGT;
                        }
                    });
                    if (data.Count != 0)
                    {
                        List<BangKeMuaVao_Model> listGroup = (from a in data
                                                              group a by a.GOODSSERVICEPURCHASECODE into g
                                                              select new BangKeMuaVao_Model
                                                              {
                                                                  GOODSSERVICEPURCHASECODE = g.Key,
                                                                  GT_CHUATHUE = g.Sum(t => t.GT_CHUATHUE),
                                                                  THUE_GTGT = g.Sum(t => t.THUE_GTGT)
                                                              }).ToList();
                    }
                    lstTaxPL2 = data;
                    #endregion
                    _IsAppendix012GTGT = true;
                    ultraTabControl1.Tabs[2].Visible = true;
                    #region Load Ugrid5
                    LoadUgrid5(lstTaxPL2.Where(n => n.GOODSSERVICEPURCHASECODE == "1").OrderBy(n => n.NGAY_HD).ToList());
                    #endregion
                    #region Load Ugrid6
                    LoadUgrid6(lstTaxPL2.Where(n => n.GOODSSERVICEPURCHASECODE == "2").OrderBy(n => n.NGAY_HD).ToList());
                    #endregion
                    #region Load Ugrid7
                    LoadUgrid7(lstTaxPL2.Where(n => n.GOODSSERVICEPURCHASECODE == "3").OrderBy(n => n.NGAY_HD).ToList());
                    #endregion
                }
                else
                {
                    #region Fill dữ liệu vào PL2
                    ReportProcedureSDS sp = new ReportProcedureSDS();
                    List<BangKeMuaVao_Model> data = new List<BangKeMuaVao_Model>();
                    data = sp.GetProc_GetBkeInv_BuyTax(FromDate, ToDate, 1, 0, "", 0);//add by cuongpv: them dk check khau tru thue hay ko
                    data.ForEach(t =>
                    {
                        t.THUE_SUAT_DETAIL = Convert.ToInt32(t.THUE_SUAT);
                        if (t.FLAG == -1)
                        {
                            t.GT_CHUATHUE = -t.GT_CHUATHUE;
                            t.THUE_GTGT = -t.THUE_GTGT;
                        }
                    });
                    if (data.Count != 0)
                    {
                        List<BangKeMuaVao_Model> listGroup = (from a in data
                                                              group a by a.GOODSSERVICEPURCHASECODE into g
                                                              select new BangKeMuaVao_Model
                                                              {
                                                                  GOODSSERVICEPURCHASECODE = g.Key,
                                                                  GT_CHUATHUE = g.Sum(t => t.GT_CHUATHUE),
                                                                  THUE_GTGT = g.Sum(t => t.THUE_GTGT)
                                                              }).ToList();
                    }
                    lstTaxPL2 = data;
                    #endregion
                    #region Load Ugrid5
                    LoadUgrid5(lstTaxPL2.Where(n => n.GOODSSERVICEPURCHASECODE == "1").OrderBy(n => n.NGAY_HD).ToList());
                    #endregion
                    #region Load Ugrid6
                    LoadUgrid6(lstTaxPL2.Where(n => n.GOODSSERVICEPURCHASECODE == "2").OrderBy(n => n.NGAY_HD).ToList());
                    #endregion
                    #region Load Ugrid7
                    LoadUgrid7(lstTaxPL2.Where(n => n.GOODSSERVICEPURCHASECODE == "3").OrderBy(n => n.NGAY_HD).ToList());
                    #endregion
                    ultraTabControl1.Tabs[2].Visible = false;
                    _IsAppendix012GTGT = false;
                }
                if (!IsFirst)
                {
                    ultraTabControl1.Tabs[3].Visible = !IsFirst;
                    ultraLabel154.Text = DeclarationTerm + " ngày " + KHBS.Day + " tháng " + KHBS.Month + " năm " + KHBS.Year + ")";
                    if (isThang)
                    {
                        DateTime dt = ToDate.AddDays(20);
                        txtDateDelay.Value = KHBS.Subtract(dt).Days;
                    }
                    if (isQuy)
                    {
                        DateTime dt = ToDate.AddDays(30);
                        txtDateDelay.Value = KHBS.Subtract(dt).Days;
                    }

                    if (Utils.ListTM01GTGT.Where(n => n.FromDate == FromDate && n.ToDate == ToDate).Count() > 0)
                    {
                        if (MaxAdditionTime == 0)
                        {
                            TM01GTGT tm01GTGTBS = Utils.ListTM01GTGT.Where(n => n.FromDate == FromDate && n.AdditionTime == null).FirstOrDefault();
                            _tM01GTGTBS = tm01GTGTBS;
                            ParentID = tm01GTGTBS.ID;
                            ObjandGUIKHBS(tm01GTGTBS, false);
                            ultraTabControl1.Tabs[2].Visible = false;
                            ultraTabControl1.Tabs[1].Visible = false;
                        }
                        else
                        {
                            TM01GTGT tm01GTGTBS = Utils.ListTM01GTGT.Where(n => n.FromDate == FromDate && n.AdditionTime == MaxAdditionTime).FirstOrDefault();
                            _tM01GTGTBS = tm01GTGTBS;
                            ParentID = tm01GTGTBS.ID;
                            ObjandGUIKHBS(tm01GTGTBS, false);
                            ultraTabControl1.Tabs[2].Visible = false;
                            ultraTabControl1.Tabs[1].Visible = false;
                        }

                    }
                    else
                    {
                        MSG.Error("Không tồn tại tờ khai cùng kỳ");
                        return false;
                    }

                }
                else
                {
                    ultraTabControl1.Tabs[3].Visible = false;
                    utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Visible = false;
                }
                FillDefaut();
            }
            return true;
        }
        #endregion

        #region Load Grid tab KBS
        #region Grid 1
        public void LoadUgridKBS1(List<TM01GTGTAdjust> lstTaxP)
        {
            if (lstTaxP.Count > 0)
            {
                dataGridView8.Size = new Size(991, lstTaxP.Count * 22 + 1);
                ultraLabel164.Location = new Point(ultraLabel164.Location.X, dataGridView8.Location.Y + dataGridView8.Height);
                dataGridView9.Location = new Point(dataGridView9.Location.X, ultraLabel164.Location.Y + ultraLabel164.Height);
                tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, dataGridView9.Location.Y + dataGridView9.Height);
                ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);
                foreach (var item in lstTaxP)
                {
                    int index = lstTaxP.IndexOf(item);
                    if (lstTaxP.IndexOf(item) == 0)
                    {
                        dataGridView8.Rows.Clear();
                        dataGridView8.Rows.Add();
                    }
                    else
                    {
                        dataGridView8.Rows.Add();
                    }
                    dataGridView8.Rows[index].Cells[0].Value = item.OrderPriority;
                    dataGridView8.Rows[index].Cells[1].Value = item.Name;
                    dataGridView8.Rows[index].Cells[2].Value = item.Code;
                    dataGridView8.Rows[index].Cells[3].Value = Utils.FormatNumberic(item.DeclaredAmount, ConstDatabase.Format_TienVND);
                    dataGridView8.Rows[index].Cells[4].Value = Utils.FormatNumberic(item.AdjustAmount, ConstDatabase.Format_TienVND);
                    dataGridView8.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.DifferAmount, ConstDatabase.Format_TienVND);


                }
            }
            else
            {
                ultraLabel164.Location = new Point(ultraLabel164.Location.X, dataGridView8.Location.Y + dataGridView8.Height);
                dataGridView9.Location = new Point(dataGridView9.Location.X, ultraLabel164.Location.Y + ultraLabel164.Height);
                tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, dataGridView9.Location.Y + dataGridView9.Height);
                ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);

            }
            for (int i = 0; i < dataGridView8.RowCount; i++)
            {
                dataGridView8.Rows[i].Cells[0].Value = i + 1;
            }
        }
        #endregion

        #region Grid 2
        public void LoadUgridKBS2(List<TM01GTGTAdjust> lstTaxP)
        {
            if (lstTaxP.Count > 0)
            {
                dataGridView9.Size = new Size(991, lstTaxP.Count * 22 + 1);

                tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, dataGridView9.Location.Y + dataGridView9.Height);
                ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);
                foreach (var item in lstTaxP)
                {
                    int index = lstTaxP.IndexOf(item);
                    if (lstTaxP.IndexOf(item) == 0)
                    {
                        dataGridView9.Rows.Clear();
                        dataGridView9.Rows.Add();
                    }
                    else
                    {
                        dataGridView9.Rows.Add();
                    }
                    dataGridView9.Rows[index].Cells[0].Value = item.OrderPriority;
                    dataGridView9.Rows[index].Cells[1].Value = item.Name;
                    dataGridView9.Rows[index].Cells[2].Value = item.Code;
                    dataGridView9.Rows[index].Cells[3].Value = Utils.FormatNumberic(item.DeclaredAmount, ConstDatabase.Format_TienVND);
                    dataGridView9.Rows[index].Cells[4].Value = Utils.FormatNumberic(item.AdjustAmount, ConstDatabase.Format_TienVND);
                    dataGridView9.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.DifferAmount, ConstDatabase.Format_TienVND);


                }
            }
            else
            {
                tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, dataGridView9.Location.Y + dataGridView9.Height);
                ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);

            }
            for (int i = 0; i < dataGridView9.RowCount; i++)
            {
                dataGridView9.Rows[i].Cells[0].Value = i + 1;
            }
        }
        #endregion
        #endregion

        #region Load Grid Tab PL 
        #region PL 1
        #region Grid 1
        public void LoadUgrid1(List<BangKeBanRa_Model> lstTaxP)
        {
            if (lstTaxP.Count != 0)
            {
                dataGridView1.Size = new Size(1232, lstTaxP.Count * 22 + 1);

                ultraPanel51.Location = new Point(ultraPanel51.Location.X, dataGridView1.Location.Y + dataGridView1.Height);
                ultraLabel172.Location = new Point(ultraLabel172.Location.X, ultraPanel51.Location.Y + ultraPanel51.Height);
                dataGridView2.Location = new Point(dataGridView2.Location.X, ultraLabel172.Location.Y + ultraLabel172.Height);
                ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);

                foreach (var item in lstTaxP)
                {
                    int index = lstTaxP.IndexOf(item);
                    if (lstTaxP.IndexOf(item) == 0)
                    {
                        dataGridView1.Rows.Clear();
                        dataGridView1.Rows.Add();


                    }
                    else
                    {
                        dataGridView1.Rows.Add();
                    }
                    dataGridView1.Rows[index].Cells[0].Value = index + 1;
                    if (item.SO_HD == "")
                        dataGridView1.Rows[index].Cells[1].Value = null;
                    else
                        dataGridView1.Rows[index].Cells[1].Value = item.SO_HD;
                    dataGridView1.Rows[index].Cells[2].Value = item.NGAY_HD;
                    dataGridView1.Rows[index].Cells[3].Value = item.NGUOI_MUA;
                    dataGridView1.Rows[index].Cells[4].Value = item.MST;
                    dataGridView1.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.DOANH_SO, ConstDatabase.Format_TienVND);
                    dataGridView1.Rows[index].Cells[6].Value = Utils.FormatNumberic(item.THUE_GTGT, ConstDatabase.Format_TienVND);
                    dataGridView1.Rows[index].Cells[7].Value = item.Note;

                }
                dataGridView1.AllowUserToAddRows = false;
            }
            else
            {
                dataGridView1.Rows.Add();
                dataGridView1.AllowUserToAddRows = false;
                ultraPanel51.Location = new Point(ultraPanel51.Location.X, dataGridView1.Location.Y + dataGridView1.Height);
                ultraLabel172.Location = new Point(ultraLabel172.Location.X, ultraPanel51.Location.Y + ultraPanel51.Height);
                dataGridView2.Location = new Point(dataGridView2.Location.X, ultraLabel172.Location.Y + ultraLabel172.Height);
                ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);

            }
        }
        #endregion

        #region Grid 2
        public void LoadUgrid2(List<BangKeBanRa_Model> lstTaxP)
        {
            if (lstTaxP.Count != 0)
            {
                dataGridView2.Size = new Size(1232, lstTaxP.Count * 22 + 1);

                ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);

                foreach (var item in lstTaxP)
                {
                    int index = lstTaxP.IndexOf(item);
                    if (lstTaxP.IndexOf(item) == 0)
                    {
                        dataGridView2.Rows.Clear();
                        dataGridView2.Rows.Add();

                    }
                    else
                    {
                        dataGridView2.Rows.Add();
                    }
                    dataGridView2.Rows[index].Cells[0].Value = index + 1;
                    if (item.SO_HD == "")
                        dataGridView2.Rows[index].Cells[1].Value = null;
                    else
                        dataGridView2.Rows[index].Cells[1].Value = item.SO_HD;
                    dataGridView2.Rows[index].Cells[2].Value = item.NGAY_HD;
                    dataGridView2.Rows[index].Cells[3].Value = item.NGUOI_MUA;
                    dataGridView2.Rows[index].Cells[4].Value = item.MST;
                    dataGridView2.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.DOANH_SO, ConstDatabase.Format_TienVND);
                    dataGridView2.Rows[index].Cells[6].Value = Utils.FormatNumberic(item.THUE_GTGT, ConstDatabase.Format_TienVND);
                    dataGridView2.Rows[index].Cells[7].Value = item.Note;

                }
                dataGridView2.AllowUserToAddRows = false;
            }
            else
            {
                dataGridView2.Rows.Add();
                dataGridView2.AllowUserToAddRows = false;
                ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);

            }
        }
        #endregion

        #region Grid 3
        public void LoadUgrid3(List<BangKeBanRa_Model> lstTaxP)
        {
            if (lstTaxP.Count != 0)
            {
                dataGridView3.Size = new Size(1232, lstTaxP.Count * 22 + 1);

                ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);

                foreach (var item in lstTaxP)
                {
                    int index = lstTaxP.IndexOf(item);
                    if (lstTaxP.IndexOf(item) == 0)
                    {
                        dataGridView3.Rows.Clear();
                        dataGridView3.Rows.Add();

                    }
                    else
                    {
                        dataGridView3.Rows.Add();


                    }
                    dataGridView3.Rows[index].Cells[0].Value = index + 1;
                    if (item.SO_HD == "")
                        dataGridView3.Rows[index].Cells[1].Value = null;
                    else
                        dataGridView3.Rows[index].Cells[1].Value = item.SO_HD;
                    dataGridView3.Rows[index].Cells[2].Value = item.NGAY_HD;
                    dataGridView3.Rows[index].Cells[3].Value = item.NGUOI_MUA;
                    dataGridView3.Rows[index].Cells[4].Value = item.MST;
                    dataGridView3.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.DOANH_SO, ConstDatabase.Format_TienVND);
                    dataGridView3.Rows[index].Cells[6].Value = Utils.FormatNumberic(item.THUE_GTGT, ConstDatabase.Format_TienVND);
                    dataGridView3.Rows[index].Cells[7].Value = item.Note;
                }
                dataGridView3.AllowUserToAddRows = false;
            }
            else
            {
                dataGridView3.Rows.Add();
                dataGridView3.AllowUserToAddRows = false;
                ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);

            }
        }
        #endregion

        #region Grid 4
        public void LoadUgrid4(List<BangKeBanRa_Model> lstTaxP)
        {
            if (lstTaxP.Count != 0)
            {
                dataGridView4.Size = new Size(1232, lstTaxP.Count * 22 + 1);
                ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                foreach (var item in lstTaxP)
                {
                    int index = lstTaxP.IndexOf(item);
                    if (lstTaxP.IndexOf(item) == 0)
                    {
                        dataGridView4.Rows.Clear();
                        dataGridView4.Rows.Add();

                    }
                    else
                    {
                        dataGridView4.Rows.Add();
                    }
                    dataGridView4.Rows[index].Cells[0].Value = index + 1;
                    if (item.SO_HD == "")
                        dataGridView4.Rows[index].Cells[1].Value = null;
                    else
                        dataGridView4.Rows[index].Cells[1].Value = item.SO_HD;
                    dataGridView4.Rows[index].Cells[2].Value = item.NGAY_HD;
                    dataGridView4.Rows[index].Cells[3].Value = item.NGUOI_MUA;
                    dataGridView4.Rows[index].Cells[4].Value = item.MST;
                    dataGridView4.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.DOANH_SO, ConstDatabase.Format_TienVND);
                    dataGridView4.Rows[index].Cells[6].Value = Utils.FormatNumberic(item.THUE_GTGT, ConstDatabase.Format_TienVND);
                    dataGridView4.Rows[index].Cells[7].Value = item.Note;
                }
                dataGridView4.AllowUserToAddRows = false;
            }
            else
            {
                dataGridView4.Rows.Add();
                dataGridView4.AllowUserToAddRows = false;
                ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);

            }
        }
        #endregion
        #endregion

        #region PL 2
        #region Grid 5
        public void LoadUgrid5(List<BangKeMuaVao_Model> lstTaxP)
        {
            if (lstTaxP.Count != 0)
            {
                dataGridView5.Size = new Size(1232, lstTaxP.Count * 22 + 1);
                ultraPanel55.Location = new Point(ultraPanel55.Location.X, dataGridView5.Location.Y + dataGridView5.Height);
                ultraLabel150.Location = new Point(ultraLabel150.Location.X, ultraPanel55.Location.Y + ultraPanel55.Height);
                dataGridView6.Location = new Point(dataGridView6.Location.X, ultraLabel150.Location.Y + ultraLabel150.Height);
                ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);


                foreach (var item in lstTaxP)
                {
                    int index = lstTaxP.IndexOf(item);
                    if (lstTaxP.IndexOf(item) == 0)
                    {
                        dataGridView5.Rows.Clear();
                        dataGridView5.Rows.Add();


                    }
                    else
                    {
                        dataGridView5.Rows.Add();
                    }

                    dataGridView5.Rows[index].Cells[0].Value = index + 1;
                    if (item.SO_HD == "")
                        dataGridView5.Rows[index].Cells[1].Value = null;
                    else
                        dataGridView5.Rows[index].Cells[1].Value = item.SO_HD;
                    dataGridView5.Rows[index].Cells[2].Value = item.NGAY_HD;
                    dataGridView5.Rows[index].Cells[3].Value = item.TEN_NBAN;
                    dataGridView5.Rows[index].Cells[4].Value = item.MST;
                    dataGridView5.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.GT_CHUATHUE, ConstDatabase.Format_TienVND);
                    dataGridView5.Rows[index].Cells[6].Value = Utils.FormatNumberic(item.THUE_GTGT, ConstDatabase.Format_TienVND);
                    dataGridView5.Rows[index].Cells[7].Value = item.Note;
                    dataGridView5.Rows[index].Cells[8].Value = item.VoucherID;
                }
                dataGridView5.AllowUserToAddRows = false;
            }
            else
            {
                dataGridView5.Rows.Add();
                dataGridView5.AllowUserToAddRows = false;
                ultraPanel55.Location = new Point(ultraPanel55.Location.X, dataGridView5.Location.Y + dataGridView5.Height);
                ultraLabel150.Location = new Point(ultraLabel150.Location.X, ultraPanel55.Location.Y + ultraPanel55.Height);
                dataGridView6.Location = new Point(dataGridView6.Location.X, ultraLabel150.Location.Y + ultraLabel150.Height);
                ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);

            }
        }
        #endregion

        #region Grid 6
        public void LoadUgrid6(List<BangKeMuaVao_Model> lstTaxP)
        {
            if (lstTaxP.Count != 0)
            {
                dataGridView6.Size = new Size(1232, lstTaxP.Count * 22 + 1);
                ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);
                foreach (var item in lstTaxP)
                {
                    int index = lstTaxP.IndexOf(item);
                    if (lstTaxP.IndexOf(item) == 0)
                    {
                        dataGridView6.Rows.Clear();
                        dataGridView6.Rows.Add();

                    }
                    else
                    {
                        dataGridView6.Rows.Add();
                    }
                    dataGridView6.Rows[index].Cells[0].Value = index + 1;
                    if (item.SO_HD == "")
                        dataGridView6.Rows[index].Cells[1].Value = null;
                    else
                        dataGridView6.Rows[index].Cells[1].Value = item.SO_HD;
                    dataGridView6.Rows[index].Cells[2].Value = item.NGAY_HD;
                    dataGridView6.Rows[index].Cells[3].Value = item.TEN_NBAN;
                    dataGridView6.Rows[index].Cells[4].Value = item.MST;
                    dataGridView6.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.GT_CHUATHUE, ConstDatabase.Format_TienVND);
                    dataGridView6.Rows[index].Cells[6].Value = Utils.FormatNumberic(item.THUE_GTGT, ConstDatabase.Format_TienVND);
                    dataGridView6.Rows[index].Cells[7].Value = item.Note;
                    dataGridView6.Rows[index].Cells[8].Value = item.VoucherID; ;
                }
                dataGridView6.AllowUserToAddRows = false;
            }
            else

            {
                dataGridView6.Rows.Add();
                dataGridView6.AllowUserToAddRows = false;
                ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);

            }
        }
        #endregion

        #region Grid 7
        public void LoadUgrid7(List<BangKeMuaVao_Model> lstTaxP)
        {
            if (lstTaxP.Count != 0)
            {
                dataGridView7.Size = new Size(1232, lstTaxP.Count * 22 + 1);
                ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);
                foreach (var item in lstTaxP)
                {
                    int index = lstTaxP.IndexOf(item);
                    if (lstTaxP.IndexOf(item) == 0)
                    {
                        dataGridView7.Rows.Clear();
                        dataGridView7.Rows.Add();


                    }
                    else
                    {
                        dataGridView7.Rows.Add();
                    }
                    dataGridView7.Rows[index].Cells[0].Value = index + 1;
                    if (item.SO_HD == "")
                        dataGridView7.Rows[index].Cells[1].Value = null;
                    else
                        dataGridView7.Rows[index].Cells[1].Value = item.SO_HD;
                    dataGridView7.Rows[index].Cells[2].Value = item.NGAY_HD;
                    dataGridView7.Rows[index].Cells[3].Value = item.TEN_NBAN;
                    dataGridView7.Rows[index].Cells[4].Value = item.MST;
                    dataGridView7.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.GT_CHUATHUE, ConstDatabase.Format_TienVND);
                    dataGridView7.Rows[index].Cells[6].Value = Utils.FormatNumberic(item.THUE_GTGT, ConstDatabase.Format_TienVND);
                    dataGridView7.Rows[index].Cells[7].Value = item.Note;
                    dataGridView7.Rows[index].Cells[8].Value = item.VoucherID;

                }
                dataGridView7.AllowUserToAddRows = false;
            }
            else
            {
                dataGridView7.Rows.Add();
                dataGridView7.AllowUserToAddRows = false;
                ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);

            }
        }
        #endregion
        #endregion
        #endregion

        #region ObjectGUI
        public TM01GTGT ObjandGUI(TM01GTGT input, bool isGet)
        {
            #region Lưu vào DB
            if (isGet)
            {
                if (input.ID == null || input.ID == Guid.Empty)
                {
                    input.ID = Guid.NewGuid();
                }
                input.TypeID = 920;
                input.DeclarationName = ultraLabel1.Text;
                input.DeclarationTerm = lblDeclarationTerm.Text;
                input.AdditionTerm = ultraLabel154.Text;
                input.IsFirstDeclaration = chkFirstDeclared.Checked;
                input.IsExtend = chkExtend.Checked;
                input.FromDate = FromDate;
                input.ToDate = ToDate;
                try
                {
                    input.AdditionTime = int.Parse(txtAdditionTime.Text);
                }
                catch { }
                if (!IsFirst)
                    input.BranchID = ParentID;
                input.CompanyName = lblCompanyName.Text;
                input.CompanyTaxCode = lblCompanyTaxCode.Text;
                input.TaxAgencyName = lblTaxAgencyName.Text;
                input.TaxAgencyTaxCode = lblAgencyTaxCode.Text;
                input.TaxAgencyEmployeeName = txtName.Text;
                input.CertificationNo = txtCertificationNo.Text;
                if (txtSignDate.Value != null)
                    input.SignDate = txtSignDate.DateTime;
                else
                    input.SignDate = null;
                input.SignName = txtSignName.Text;
                if (ultraTabControl1.Tabs[1].Visible == true)
                    _IsAppendix011GTGT = true;
                else
                    _IsAppendix011GTGT = false;
                if (ultraTabControl1.Tabs[2].Visible == true)
                    _IsAppendix012GTGT = true;
                else
                    _IsAppendix012GTGT = false;
                input.IsAppendix011GTGT = _IsAppendix011GTGT;
                input.IsAppendix012GTGT = _IsAppendix012GTGT;
                if (chkExtend.Checked)
                {
                    if (!string.IsNullOrEmpty(cbbExtensionCase.Text))
                    {
                        input.ExtensionCase = cbbExtensionCase.Value.ToString();
                    }
                }
                else { input.ExtensionCase = ""; }
                #region bảng chỉ tiêu
                int i = 0;
                if (_isEdit)
                {
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item21" && n.TM01GTGTID == input.ID).Data = chkItem21.Checked == false ? "0" : "1";
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item22" && n.TM01GTGTID == input.ID).Data = getValueMaskedEdit(txtItem22).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item23" && n.TM01GTGTID == input.ID).Data = getValueMaskedEdit(txtItem23).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item24" && n.TM01GTGTID == input.ID).Data = getValueMaskedEdit(txtItem24).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item25" && n.TM01GTGTID == input.ID).Data = getValueMaskedEdit(txtItem25).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item26" && n.TM01GTGTID == input.ID).Data = getValueMaskedEdit(txtItem26).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item27" && n.TM01GTGTID == input.ID).Data = getValueLabel(txtItem27).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item28" && n.TM01GTGTID == input.ID).Data = getValueLabel(txtItem28).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item29" && n.TM01GTGTID == input.ID).Data = getValueMaskedEdit(txtItem29).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item30" && n.TM01GTGTID == input.ID).Data = getValueMaskedEdit(txtItem30).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item31" && n.TM01GTGTID == input.ID).Data = getValueMaskedEdit(txtItem31).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item32" && n.TM01GTGTID == input.ID).Data = getValueMaskedEdit(txtItem32).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item32a" && n.TM01GTGTID == input.ID).Data = getValueMaskedEdit(txtItem32a).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item33" && n.TM01GTGTID == input.ID).Data = getValueMaskedEdit(txtItem33).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item34" && n.TM01GTGTID == input.ID).Data = getValueLabel(txtItem34).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item35" && n.TM01GTGTID == input.ID).Data = getValueLabel(txtItem35).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item36" && n.TM01GTGTID == input.ID).Data = getValueLabel(txtItem36).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item37" && n.TM01GTGTID == input.ID).Data = getValueMaskedEdit(txtItem37).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item38" && n.TM01GTGTID == input.ID).Data = getValueMaskedEdit(txtItem38).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item39" && n.TM01GTGTID == input.ID).Data = getValueMaskedEdit(txtItem39).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40a" && n.TM01GTGTID == input.ID).Data = getValueLabel(txtItem40a).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40b" && n.TM01GTGTID == input.ID).Data = getValueMaskedEdit(txtItem40b).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40" && n.TM01GTGTID == input.ID).Data = getValueLabel(txtItem40).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item41" && n.TM01GTGTID == input.ID).Data = getValueLabel(txtItem41).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item42" && n.TM01GTGTID == input.ID).Data = getValueMaskedEdit(txtItem42).ToString();
                    input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item43" && n.TM01GTGTID == input.ID).Data = getValueLabel(txtItem43).ToString();
                }
                else
                {
                    input.FromDate = FromDate;
                    input.ToDate = ToDate;
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item21",
                        Name = ultraLabel20.Text,
                        Data = chkItem21.Checked == false ? "0" : "1",
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {

                        TM01GTGTID = input.ID,
                        Code = "Item22",
                        Name = ultraLabel23.Text,
                        Data = getValueMaskedEdit(txtItem22).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {

                        TM01GTGTID = input.ID,
                        Code = "Item23",
                        Name = ultraLabel29.Text,
                        Data = getValueMaskedEdit(txtItem23).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {

                        TM01GTGTID = input.ID,
                        Code = "Item24",
                        Name = ultraLabel29.Text,
                        Data = getValueMaskedEdit(txtItem24).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {

                        TM01GTGTID = input.ID,
                        Code = "Item25",
                        Name = ultraLabel33.Text,
                        Data = getValueMaskedEdit(txtItem25).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item26",
                        Name = ultraLabel50.Text,
                        Data = getValueMaskedEdit(txtItem26).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item27",
                        Name = ultraLabel47.Text,
                        Data = getValueLabel(txtItem27).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item28",
                        Name = ultraLabel47.Text,
                        Data = getValueLabel(txtItem28).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item29",
                        Name = ultraLabel41.Text,
                        Data = getValueMaskedEdit(txtItem29).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item30",
                        Name = ultraLabel53.Text,
                        Data = getValueMaskedEdit(txtItem30).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item31",
                        Name = ultraLabel53.Text,
                        Data = getValueMaskedEdit(txtItem31).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item32",
                        Name = ultraLabel54.Text,
                        Data = getValueMaskedEdit(txtItem32).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item32a",
                        Name = ultraLabel52.Text,
                        Data = getValueMaskedEdit(txtItem32a).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item33",
                        Name = ultraLabel54.Text,
                        Data = getValueMaskedEdit(txtItem33).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item34",
                        Name = ultraLabel60.Text,
                        Data = getValueLabel(txtItem34).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item35",
                        Name = ultraLabel60.Text,
                        Data = getValueLabel(txtItem35).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item36",
                        Name = ultraLabel63.Text,
                        Data = getValueLabel(txtItem36).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item37",
                        Name = ultraLabel68.Text,
                        Data = getValueMaskedEdit(txtItem37).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item38",
                        Name = ultraLabel70.Text,
                        Data = getValueMaskedEdit(txtItem38).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item39",
                        Name = ultraLabel75.Text,
                        Data = getValueMaskedEdit(txtItem39).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item40a",
                        Name = ultraLabel79.Text,
                        Data = getValueLabel(txtItem40a).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item40b",
                        Name = ultraLabel81.Text,
                        Data = getValueMaskedEdit(txtItem40b).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item40",
                        Name = ultraLabel83.Text,
                        Data = getValueLabel(txtItem40).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item41",
                        Name = ultraLabel87.Text,
                        Data = getValueLabel(txtItem41).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item42",
                        Name = ultraLabel85.Text,
                        Data = getValueMaskedEdit(txtItem42).ToString(),
                        OrderPriority = i++
                    });
                    input.TM01GTGTDetails.Add(new TM01GTGTDetail
                    {
                        TM01GTGTID = input.ID,
                        Code = "Item43",
                        Name = ultraLabel89.Text,
                        Data = getValueLabel(txtItem43).ToString(),
                        OrderPriority = i++
                    });
                }
                #endregion

                #region PL 1

                if (ultraTabControl1.Tabs[1].Visible == true)
                {
                    input.TM011GTGTs.Clear();
                    #region table 1
                    for (int t = 0; t < dataGridView1.RowCount; t++)
                    {
                        if (!dataGridView1.Rows[t].Cells[1].Value.IsNullOrEmpty() && !dataGridView1.Rows[t].Cells[2].Value.IsNullOrEmpty() && !dataGridView1.Rows[t].Cells[3].Value.IsNullOrEmpty())
                        {
                            DateTime u = Convert.ToDateTime(getDate(dataGridView1.Rows[t].Cells[2]));
                            input.TM011GTGTs.Add(new TM011GTGT
                            {
                                TM01GTGTID = input.ID,
                                OrderPriority = dataGridView1.Rows[t].Cells[0].Value.ToInt(),
                                InvoiceNo = configInvoiceNo(getString(dataGridView1.Rows[t].Cells[1])),

                                InvoiceDate = u.Date,
                                AccountingObjectName = getString(dataGridView1.Rows[t].Cells[3]),
                                TaxCode = getString(dataGridView1.Rows[t].Cells[4]),
                                PretaxAmount = getValueMaskedEdit1(dataGridView1.Rows[t].Cells[5]),
                                TaxAmount = getValueMaskedEdit1(dataGridView1.Rows[t].Cells[6]),
                                VATRate = -1M,
                                Note = getString(dataGridView1.Rows[t].Cells[7])
                            });
                        }
                    }

                    #endregion

                    #region table 2
                    for (int t = 0; t < dataGridView2.RowCount; t++)
                    {
                        if (!dataGridView2.Rows[t].Cells[1].Value.IsNullOrEmpty() && !dataGridView2.Rows[t].Cells[2].Value.IsNullOrEmpty() && !dataGridView2.Rows[t].Cells[3].Value.IsNullOrEmpty())
                        {
                            DateTime u = Convert.ToDateTime(getDate(dataGridView2.Rows[t].Cells[2]));
                            input.TM011GTGTs.Add(new TM011GTGT
                            {
                                TM01GTGTID = input.ID,
                                OrderPriority = dataGridView2.Rows[t].Cells[0].Value.ToInt(),
                                InvoiceNo = configInvoiceNo(getString(dataGridView2.Rows[t].Cells[1])),
                                InvoiceDate = u.Date,
                                AccountingObjectName = getString(dataGridView2.Rows[t].Cells[3]),
                                TaxCode = getString(dataGridView2.Rows[t].Cells[4]),
                                PretaxAmount = getValueMaskedEdit1(dataGridView2.Rows[t].Cells[5]),
                                TaxAmount = getValueMaskedEdit1(dataGridView2.Rows[t].Cells[6]),
                                VATRate = 0M,
                                Note = getString(dataGridView2.Rows[t].Cells[7])
                            });
                        }
                    }

                    #endregion

                    #region table 3
                    for (int t = 0; t < dataGridView3.RowCount; t++)
                    {
                        if (!dataGridView3.Rows[t].Cells[1].Value.IsNullOrEmpty() && !dataGridView3.Rows[t].Cells[2].Value.IsNullOrEmpty() && !dataGridView3.Rows[t].Cells[3].Value.IsNullOrEmpty())
                        {
                            DateTime u = Convert.ToDateTime(getDate(dataGridView3.Rows[t].Cells[2]));
                            input.TM011GTGTs.Add(new TM011GTGT
                            {
                                TM01GTGTID = input.ID,
                                OrderPriority = dataGridView3.Rows[t].Cells[0].Value.ToInt(),
                                InvoiceNo = configInvoiceNo(getString(dataGridView3.Rows[t].Cells[1])),
                                InvoiceDate = u.Date,
                                AccountingObjectName = getString(dataGridView3.Rows[t].Cells[3]),
                                TaxCode = getString(dataGridView3.Rows[t].Cells[4]),
                                PretaxAmount = getValueMaskedEdit1(dataGridView3.Rows[t].Cells[5]),
                                TaxAmount = getValueMaskedEdit1(dataGridView3.Rows[t].Cells[6]),
                                VATRate = 5M,
                                Note = getString(dataGridView3.Rows[t].Cells[7])
                            });
                        }
                    }

                    #endregion

                    #region table 4
                    for (int t = 0; t < dataGridView4.RowCount; t++)
                    {
                        if (!dataGridView4.Rows[t].Cells[1].Value.IsNullOrEmpty() && !dataGridView4.Rows[t].Cells[2].Value.IsNullOrEmpty() && !dataGridView4.Rows[t].Cells[3].Value.IsNullOrEmpty())
                        {
                            DateTime u = Convert.ToDateTime(getDate(dataGridView4.Rows[t].Cells[2]));
                            input.TM011GTGTs.Add(new TM011GTGT
                            {
                                TM01GTGTID = input.ID,
                                OrderPriority = dataGridView4.Rows[t].Cells[0].Value.ToInt(),
                                InvoiceNo = configInvoiceNo(getString(dataGridView4.Rows[t].Cells[1])),
                                InvoiceDate = u.Date,
                                AccountingObjectName = getString(dataGridView4.Rows[t].Cells[3]),
                                TaxCode = getString(dataGridView4.Rows[t].Cells[4]),
                                PretaxAmount = getValueMaskedEdit1(dataGridView4.Rows[t].Cells[5]),
                                TaxAmount = getValueMaskedEdit1(dataGridView4.Rows[t].Cells[6]),
                                VATRate = 10M,
                                Note = getString(dataGridView4.Rows[t].Cells[7])
                            });
                        }
                    }

                    #endregion
                }

                #endregion

                #region PL 2
                if (ultraTabControl1.Tabs[2].Visible == true)
                {
                    input.TM012GTGTs.Clear();
                    #region table 5
                    for (int t = 0; t < dataGridView5.RowCount; t++)
                    {
                        if (!dataGridView5.Rows[t].Cells[1].Value.IsNullOrEmpty() && !dataGridView5.Rows[t].Cells[2].Value.IsNullOrEmpty() && !dataGridView5.Rows[t].Cells[3].Value.IsNullOrEmpty())
                        {
                            DateTime u = Convert.ToDateTime(getDate(dataGridView5.Rows[t].Cells[2]));
                            input.TM012GTGTs.Add(new TM012GTGT
                            {
                                TM01GTGTID = input.ID,
                                OrderPriority = dataGridView5.Rows[t].Cells[0].Value.ToInt(),
                                InvoiceNo = getString(dataGridView5.Rows[t].Cells[1]),
                                InvoiceDate = u.Date,
                                AccountingObjectName = getString(dataGridView5.Rows[t].Cells[3]),
                                TaxCode = getString(dataGridView5.Rows[t].Cells[4]),
                                PretaxAmount = getValueMaskedEdit1(dataGridView5.Rows[t].Cells[5]),
                                TaxAmount = getValueMaskedEdit1(dataGridView5.Rows[t].Cells[6]),
                                Type = 1,
                                Note = getString(dataGridView5.Rows[t].Cells[7]),
                                VoucherID = getGuid(dataGridView5.Rows[t].Cells[8])
                            });
                        }
                    }

                    #endregion

                    #region table 6
                    for (int t = 0; t < dataGridView6.RowCount; t++)
                    {
                        if (!dataGridView6.Rows[t].Cells[1].Value.IsNullOrEmpty() && !dataGridView6.Rows[t].Cells[2].Value.IsNullOrEmpty() && !dataGridView6.Rows[t].Cells[3].Value.IsNullOrEmpty())
                        {
                            DateTime u = Convert.ToDateTime(getDate(dataGridView5.Rows[t].Cells[2]));
                            input.TM012GTGTs.Add(new TM012GTGT
                            {
                                TM01GTGTID = input.ID,
                                OrderPriority = dataGridView6.Rows[t].Cells[0].Value.ToInt(),
                                InvoiceNo = getString(dataGridView6.Rows[t].Cells[1]),
                                InvoiceDate = u.Date,
                                AccountingObjectName = getString(dataGridView6.Rows[t].Cells[3]),
                                TaxCode = getString(dataGridView6.Rows[t].Cells[4]),
                                PretaxAmount = getValueMaskedEdit1(dataGridView6.Rows[t].Cells[5]),
                                TaxAmount = getValueMaskedEdit1(dataGridView6.Rows[t].Cells[6]),
                                Type = 2,
                                Note = getString(dataGridView6.Rows[t].Cells[7]),
                                VoucherID = getGuid(dataGridView6.Rows[t].Cells[8])
                            });
                        }
                    }

                    #endregion

                    #region table 7
                    for (int t = 0; t < dataGridView7.RowCount; t++)
                    {
                        if (!dataGridView7.Rows[t].Cells[1].Value.IsNullOrEmpty() && !dataGridView7.Rows[t].Cells[2].Value.IsNullOrEmpty() && !dataGridView7.Rows[t].Cells[3].Value.IsNullOrEmpty())
                        {
                            DateTime u = Convert.ToDateTime(getDate(dataGridView5.Rows[t].Cells[2]));
                            input.TM012GTGTs.Add(new TM012GTGT
                            {
                                TM01GTGTID = input.ID,
                                OrderPriority = dataGridView7.Rows[t].Cells[0].Value.ToInt(),
                                InvoiceNo = getString(dataGridView7.Rows[t].Cells[1]),
                                InvoiceDate = u.Date,
                                AccountingObjectName = getString(dataGridView7.Rows[t].Cells[3]),
                                TaxCode = getString(dataGridView7.Rows[t].Cells[4]),
                                PretaxAmount = getValueMaskedEdit1(dataGridView7.Rows[t].Cells[5]),
                                TaxAmount = getValueMaskedEdit1(dataGridView7.Rows[t].Cells[6]),
                                Type = 3,
                                Note = getString(dataGridView7.Rows[t].Cells[7]),
                                VoucherID = getGuid(dataGridView7.Rows[t].Cells[8])
                            });
                        }
                    }

                    #endregion
                }
                #endregion

                #region KHBS

                if (ultraTabControl1.Tabs[3].Visible == true)
                {
                    input.TM01GTGTAdjusts.Clear();
                    #region table 8
                    if (lst1.Count != 0)
                    {
                        for (int t = 0; t < dataGridView8.RowCount; t++)
                        {
                            if (!dataGridView8.Rows[t].Cells[1].Value.IsNullOrEmpty() && !dataGridView8.Rows[t].Cells[2].Value.IsNullOrEmpty() && !dataGridView8.Rows[t].Cells[3].Value.IsNullOrEmpty())
                            {
                                input.TM01GTGTAdjusts.Add(new TM01GTGTAdjust
                                {
                                    TM01GTGTID = input.ID,
                                    OrderPriority = dataGridView8.Rows[t].Cells[0].Value.ToInt(),
                                    Name = getString(dataGridView8.Rows[t].Cells[1]),
                                    Code = getString(dataGridView8.Rows[t].Cells[2]),
                                    DeclaredAmount = getValueMaskedEdit1(dataGridView8.Rows[t].Cells[3]),
                                    AdjustAmount = getValueMaskedEdit1(dataGridView8.Rows[t].Cells[4]),
                                    DifferAmount = getValueMaskedEdit1(dataGridView8.Rows[t].Cells[5]),
                                    Type = 1



                                });
                            }
                        }
                        //for (int t = 1; t < tableLayoutPanel8.RowCount; t++)
                        //{
                        //    var control0 = tableLayoutPanel8.GetControlFromPosition(0, t);
                        //    var control1 = tableLayoutPanel8.GetControlFromPosition(1, t);
                        //    var control2 = tableLayoutPanel8.GetControlFromPosition(2, t);
                        //    var control3 = tableLayoutPanel8.GetControlFromPosition(3, t);
                        //    var control4 = tableLayoutPanel8.GetControlFromPosition(4, t);
                        //    var control5 = tableLayoutPanel8.GetControlFromPosition(5, t);

                        //    UltraLabel STT = control0 as UltraLabel;
                        //    UltraTextEditor Name = control1 as UltraTextEditor;
                        //    UltraTextEditor Code = control2 as UltraTextEditor;
                        //    UltraTextEditor DecAmount = control3 as UltraTextEditor;
                        //    UltraTextEditor AdjAmount = control4 as UltraTextEditor;
                        //    UltraTextEditor DiffAmount = control5 as UltraTextEditor;

                        //    input.TM01GTGTAdjusts.Add(new TM01GTGTAdjust
                        //    {
                        //        TM01GTGTID = input.ID,
                        //        OrderPriority = int.Parse(STT.Text),
                        //        Name = Name.Text,
                        //        Code = Code.Text,
                        //        DeclaredAmount = getValueTextEdit(DecAmount),
                        //        AdjustAmount = getValueTextEdit(AdjAmount),
                        //        DifferAmount = getValueTextEdit(DiffAmount),
                        //        Type = 1

                        //    });
                        //}
                    }
                    #endregion

                    #region table 9
                    if (lst2.Count != 0)
                    {
                        for (int t = 0; t < dataGridView9.RowCount; t++)
                        {
                            if (!dataGridView9.Rows[t].Cells[1].Value.IsNullOrEmpty() && !dataGridView9.Rows[t].Cells[2].Value.IsNullOrEmpty() && !dataGridView9.Rows[t].Cells[3].Value.IsNullOrEmpty())
                            {
                                input.TM01GTGTAdjusts.Add(new TM01GTGTAdjust
                                {
                                    TM01GTGTID = input.ID,
                                    OrderPriority = dataGridView9.Rows[t].Cells[0].Value.ToInt(),
                                    Name = getString(dataGridView9.Rows[t].Cells[1]),
                                    Code = getString(dataGridView9.Rows[t].Cells[2]),
                                    DeclaredAmount = getValueMaskedEdit1(dataGridView9.Rows[t].Cells[3]),
                                    AdjustAmount = getValueMaskedEdit1(dataGridView9.Rows[t].Cells[4]),
                                    DifferAmount = getValueMaskedEdit1(dataGridView9.Rows[t].Cells[5]),
                                    Type = 2



                                });
                            }
                        }
                        //for (int t = 1; t < tableLayoutPanel9.RowCount; t++)
                        //{
                        //    var control0 = tableLayoutPanel9.GetControlFromPosition(0, t);
                        //    var control1 = tableLayoutPanel9.GetControlFromPosition(1, t);
                        //    var control2 = tableLayoutPanel9.GetControlFromPosition(2, t);
                        //    var control3 = tableLayoutPanel9.GetControlFromPosition(3, t);
                        //    var control4 = tableLayoutPanel9.GetControlFromPosition(4, t);
                        //    var control5 = tableLayoutPanel9.GetControlFromPosition(5, t);

                        //    UltraLabel STT = control0 as UltraLabel;
                        //    UltraTextEditor Name = control1 as UltraTextEditor;
                        //    UltraTextEditor Code = control2 as UltraTextEditor;
                        //    UltraTextEditor DecAmount = control3 as UltraTextEditor;
                        //    UltraTextEditor AdjAmount = control4 as UltraTextEditor;
                        //    UltraTextEditor DiffAmount = control5 as UltraTextEditor;

                        //    input.TM01GTGTAdjusts.Add(new TM01GTGTAdjust
                        //    {
                        //        TM01GTGTID = input.ID,
                        //        OrderPriority = int.Parse(STT.Text),
                        //        Name = Name.Text,
                        //        Code = Code.Text,
                        //        DeclaredAmount = getValueTextEdit(DecAmount),
                        //        AdjustAmount = getValueTextEdit(AdjAmount),
                        //        DifferAmount = getValueTextEdit(DiffAmount),
                        //        Type = 2
                        //    });
                        //}
                    }
                    #endregion
                    if (lst1.Count != 0 || lst2.Count != 0)
                    {
                        input.TM01GTGTAdjusts.Add(new TM01GTGTAdjust
                        {
                            TM01GTGTID = input.ID,
                            Name = "Thuế GTGT còn phải nộp trong kỳ",
                            Code = "40",
                            DeclaredAmount = getValueLabel(lblDeclaredTaxAmount),
                            AdjustAmount = getValueLabel(lblAdjustTaxAmount),
                            DifferAmount = getValueLabel(lblDifTaxAmount),
                            LateDays = int.Parse(txtDateDelay.Value.ToString()),
                            LateAmount = getValueMaskedEdit(txtLateAmount),
                            ExplainAmount = getValueMaskedEdit(txtExplainAmount),
                            CommandNo = txtCommandNo.Text,
                            CommandDate = dteDate.DateTime,
                            TaxCompanyName = txtTaxCompanyName.Text,
                            TaxCompanyDecisionName = txtTaxCompanyDecisionName.Text,
                            ReceiveDays = int.Parse(txtReceiveDays.Value.ToString()),
                            ExplainLateAmount = getValueMaskedEdit(txtExplainLateAmount),
                            DifferReason = txtReason.Text,
                            OrderPriority = 1,
                            Type = 3
                        });
                        input.TM01GTGTAdjusts.Add(new TM01GTGTAdjust
                        {
                            TM01GTGTID = input.ID,
                            Name = "Thuế GTGT còn được khấu trừ chuyển kỳ sau",
                            Code = "43",
                            DeclaredAmount = getValueLabel(lblDeclaredDiscountAmount),
                            AdjustAmount = getValueLabel(lblAdjustDiscountAmount),
                            DifferAmount = getValueLabel(lblDifDiscountAmount),
                            LateDays = int.Parse(txtDateDelay.Value.ToString()),
                            LateAmount = getValueMaskedEdit(txtLateAmount),
                            ExplainAmount = getValueMaskedEdit(txtExplainAmount),
                            CommandNo = txtCommandNo.Text,
                            CommandDate = dteDate.DateTime,
                            TaxCompanyName = txtTaxCompanyName.Text,
                            TaxCompanyDecisionName = txtTaxCompanyDecisionName.Text,
                            ReceiveDays = int.Parse(txtReceiveDays.Value.ToString()),
                            ExplainLateAmount = getValueMaskedEdit(txtExplainLateAmount),
                            DifferReason = txtReason.Text,
                            OrderPriority = 2,
                            Type = 3
                        });
                    }

                }
                #endregion



            }
            #endregion

            #region Lấy từ DB lên
            else
            {
                cbbExtensionCase.Value = input.ExtensionCase;
                ultraTabControl1.Tabs[1].Visible = input.IsAppendix011GTGT;
                ultraTabControl1.Tabs[2].Visible = input.IsAppendix012GTGT;
                if (input.IsFirstDeclaration)
                {
                    ultraTabControl1.Tabs[3].Visible = false;
                    utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Visible = false;
                }
                else
                {
                    ultraTabControl1.Tabs[3].Visible = true;

                }
                chkFirstDeclared.Checked = input.IsFirstDeclaration;
                FromDate = input.FromDate;
                ToDate = input.ToDate;
                chkExtend.Checked = input.IsExtend;
                lblDeclarationTerm.Text = input.DeclarationTerm;
                lblCompanyName.Text = input.CompanyName;
                lblCompanyTaxCode.Text = input.CompanyTaxCode;
                lblTaxAgencyName.Text = input.TaxAgencyName;
                lblAgencyTaxCode.Text = input.TaxAgencyTaxCode;
                txtName.Text = input.TaxAgencyEmployeeName;
                txtCertificationNo.Text = input.CertificationNo;
                ultraLabel154.Text = input.AdditionTerm;
                txtSignDate.Value = input.SignDate;
                txtSignName.Text = input.SignName;
                if (input.AdditionTime != null && input.AdditionTime != 0)
                    txtAdditionTime.Value = input.AdditionTime;

                #region chỉ tiêu
                if (input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item21" && n.TM01GTGTID == input.ID).Data == "0")
                    chkItem21.Checked = false;
                else
                    chkItem21.Checked = true;
                BindData(txtItem22, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item22" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem23, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item23" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem24, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item24" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem25, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item25" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem26, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item26" && n.TM01GTGTID == input.ID).Data));
                txtItem27.FormatNumberic(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item27" && n.TM01GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                txtItem28.FormatNumberic(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item28" && n.TM01GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                BindData(txtItem29, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item29" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem30, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item30" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem31, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item31" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem32, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item32" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem32a, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item32a" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem33, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item33" && n.TM01GTGTID == input.ID).Data));
                txtItem34.FormatNumberic(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item34" && n.TM01GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                txtItem35.FormatNumberic(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item35" && n.TM01GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                txtItem36.FormatNumberic(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item36" && n.TM01GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                BindData(txtItem37, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item37" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem38, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item38" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem39, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item39" && n.TM01GTGTID == input.ID).Data));
                txtItem40a.FormatNumberic(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40a" && n.TM01GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                BindData(txtItem40b, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40b" && n.TM01GTGTID == input.ID).Data));
                txtItem40.FormatNumberic(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40" && n.TM01GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                txtItem41.FormatNumberic(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item41" && n.TM01GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                BindData(txtItem42, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item42" && n.TM01GTGTID == input.ID).Data));
                txtItem43.FormatNumberic(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item43" && n.TM01GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                #endregion

                #region PL 1
                if (ultraTabControl1.Tabs[1].Visible == true)
                {
                    #region table 1
                    List<TM011GTGT> lstTM011GTGT1 = new List<TM011GTGT>();
                    lstTM011GTGT1 = input.TM011GTGTs.Where(n => n.VATRate == -1M && n.TM01GTGTID == input.ID).OrderBy(n => n.OrderPriority).ToList();
                    if (lstTM011GTGT1.Count > 0)
                    {
                        dataGridView1.Size = new Size(1232, lstTM011GTGT1.Count * 22 + 1);

                        ultraPanel51.Location = new Point(ultraPanel51.Location.X, dataGridView1.Location.Y + dataGridView1.Height);
                        ultraLabel172.Location = new Point(ultraLabel172.Location.X, ultraPanel51.Location.Y + ultraPanel51.Height);
                        dataGridView2.Location = new Point(dataGridView2.Location.X, ultraLabel172.Location.Y + ultraLabel172.Height);
                        ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                        ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                        dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                        ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                        ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                        dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                        ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                        ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);

                        foreach (var item in lstTM011GTGT1)
                        {
                            int index = lstTM011GTGT1.IndexOf(item);
                            if (index == 0)
                            {
                                dataGridView1.Rows.Clear();
                                dataGridView1.Rows.Add();


                            }
                            else
                            {
                                dataGridView1.Rows.Add();
                            }
                            dataGridView1.Rows[index].Cells[0].Value = item.OrderPriority;
                            dataGridView1.Rows[index].Cells[1].Value = item.InvoiceNo;
                            dataGridView1.Rows[index].Cells[2].Value = item.InvoiceDate;
                            dataGridView1.Rows[index].Cells[3].Value = item.AccountingObjectName;
                            dataGridView1.Rows[index].Cells[4].Value = item.TaxCode;
                            dataGridView1.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.PretaxAmount, ConstDatabase.Format_TienVND);
                            dataGridView1.Rows[index].Cells[6].Value = Utils.FormatNumberic(item.TaxAmount, ConstDatabase.Format_TienVND);
                            dataGridView1.Rows[index].Cells[7].Value = item.Note;

                        }
                        dataGridView1.AllowUserToAddRows = false;
                    }
                    else
                    {
                        dataGridView1.Rows.Add();
                        dataGridView1.AllowUserToAddRows = false;
                        ultraPanel51.Location = new Point(ultraPanel51.Location.X, dataGridView1.Location.Y + dataGridView1.Height);
                        ultraLabel172.Location = new Point(ultraLabel172.Location.X, ultraPanel51.Location.Y + ultraPanel51.Height);
                        dataGridView2.Location = new Point(dataGridView2.Location.X, ultraLabel172.Location.Y + ultraLabel172.Height);
                        ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                        ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                        dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                        ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                        ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                        dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                        ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                        ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                        //dataGridView1.AllowUserToAddRows = false;
                    }

                    #endregion

                    #region table 2
                    List<TM011GTGT> lstTM011GTGT2 = new List<TM011GTGT>();
                    lstTM011GTGT2 = input.TM011GTGTs.Where(n => n.VATRate == 0M && n.TM01GTGTID == input.ID).OrderBy(n => n.OrderPriority).ToList();
                    if (lstTM011GTGT2.Count > 0)
                    {
                        dataGridView2.Size = new Size(1232, lstTM011GTGT2.Count * 22 + 1);

                        ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                        ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                        dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                        ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                        ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                        dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                        ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                        ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);


                        foreach (var item in lstTM011GTGT2)
                        {
                            int index = lstTM011GTGT2.IndexOf(item);
                            if (lstTM011GTGT2.IndexOf(item) == 0)
                            {
                                dataGridView2.Rows.Clear();
                                dataGridView2.Rows.Add();

                            }
                            else
                            {
                                dataGridView2.Rows.Add();
                            }
                            dataGridView2.Rows[index].Cells[0].Value = item.OrderPriority;
                            dataGridView2.Rows[index].Cells[1].Value = item.InvoiceNo;
                            dataGridView2.Rows[index].Cells[2].Value = item.InvoiceDate;
                            dataGridView2.Rows[index].Cells[3].Value = item.AccountingObjectName;
                            dataGridView2.Rows[index].Cells[4].Value = item.TaxCode;
                            dataGridView2.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.PretaxAmount, ConstDatabase.Format_TienVND);
                            dataGridView2.Rows[index].Cells[6].Value = Utils.FormatNumberic(item.TaxAmount, ConstDatabase.Format_TienVND);
                            dataGridView2.Rows[index].Cells[7].Value = item.Note;

                        }

                        dataGridView2.AllowUserToAddRows = false;
                    }
                    else
                    {
                        dataGridView2.Rows.Add();
                        dataGridView2.AllowUserToAddRows = false;
                        ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                        ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                        dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                        ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                        ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                        dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                        ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                        ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);

                    }

                    #endregion

                    #region table 3
                    List<TM011GTGT> lstTM011GTGT3 = new List<TM011GTGT>();
                    lstTM011GTGT3 = input.TM011GTGTs.Where(n => n.VATRate == 5M && n.TM01GTGTID == input.ID).OrderBy(n => n.OrderPriority).ToList();
                    if (lstTM011GTGT3.Count > 0)
                    {
                        dataGridView3.Size = new Size(1232, lstTM011GTGT3.Count * 22 + 1);

                        ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                        ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                        dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                        ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                        ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);

                        foreach (var item in lstTM011GTGT3)
                        {
                            int index = lstTM011GTGT3.IndexOf(item);
                            if (lstTM011GTGT3.IndexOf(item) == 0)
                            {
                                dataGridView3.Rows.Clear();
                                dataGridView3.Rows.Add();

                            }
                            else
                            {
                                dataGridView3.Rows.Add();


                            }
                            dataGridView3.Rows[index].Cells[0].Value = item.OrderPriority;
                            dataGridView3.Rows[index].Cells[1].Value = item.InvoiceNo;
                            dataGridView3.Rows[index].Cells[2].Value = item.InvoiceDate;
                            dataGridView3.Rows[index].Cells[3].Value = item.AccountingObjectName;
                            dataGridView3.Rows[index].Cells[4].Value = item.TaxCode;
                            dataGridView3.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.PretaxAmount, ConstDatabase.Format_TienVND);
                            dataGridView3.Rows[index].Cells[6].Value = Utils.FormatNumberic(item.TaxAmount, ConstDatabase.Format_TienVND);
                            dataGridView3.Rows[index].Cells[7].Value = item.Note;
                        }
                        dataGridView3.AllowUserToAddRows = false;
                    }
                    else
                    {
                        dataGridView3.Rows.Add();
                        dataGridView3.AllowUserToAddRows = false;
                        ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                        ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                        dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                        ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                        ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);

                    }

                    #endregion

                    #region table 4
                    List<TM011GTGT> lstTM011GTGT4 = new List<TM011GTGT>();
                    lstTM011GTGT4 = input.TM011GTGTs.Where(n => n.VATRate == 10M && n.TM01GTGTID == input.ID).OrderBy(n => n.OrderPriority).ToList();
                    if (lstTM011GTGT4.Count > 0)
                    {
                        dataGridView4.Size = new Size(1232, lstTM011GTGT4.Count * 22 + 1);
                        ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                        ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                        foreach (var item in lstTM011GTGT4)
                        {
                            int index = lstTM011GTGT4.IndexOf(item);
                            if (lstTM011GTGT4.IndexOf(item) == 0)
                            {
                                dataGridView4.Rows.Clear();
                                dataGridView4.Rows.Add();

                            }
                            else
                            {
                                dataGridView4.Rows.Add();
                            }
                            dataGridView4.Rows[index].Cells[0].Value = item.OrderPriority;
                            dataGridView4.Rows[index].Cells[1].Value = item.InvoiceNo;
                            dataGridView4.Rows[index].Cells[2].Value = item.InvoiceDate;
                            dataGridView4.Rows[index].Cells[3].Value = item.AccountingObjectName;
                            dataGridView4.Rows[index].Cells[4].Value = item.TaxCode;
                            dataGridView4.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.PretaxAmount, ConstDatabase.Format_TienVND);
                            dataGridView4.Rows[index].Cells[6].Value = Utils.FormatNumberic(item.TaxAmount, ConstDatabase.Format_TienVND);
                            dataGridView4.Rows[index].Cells[7].Value = item.Note;
                        }
                        dataGridView4.AllowUserToAddRows = false;
                    }
                    else
                    {
                        dataGridView4.Rows.Add();
                        dataGridView4.AllowUserToAddRows = false;
                        ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                        ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);

                    }

                    #endregion
                }
                #endregion

                #region PL 2
                if (ultraTabControl1.Tabs[2].Visible == true)
                {
                    #region table 5
                    var lstTM012GTGT1 = new List<TM012GTGT>();
                    lstTM012GTGT1 = input.TM012GTGTs.Where(n => n.Type == 1 && n.TM01GTGTID == input.ID).OrderBy(n => n.OrderPriority).ToList();
                    if (lstTM012GTGT1.Count > 0)
                    {
                        dataGridView5.Size = new Size(1232, lstTM012GTGT1.Count * 22 + 1);
                        ultraPanel55.Location = new Point(ultraPanel55.Location.X, dataGridView5.Location.Y + dataGridView5.Height);
                        ultraLabel150.Location = new Point(ultraLabel150.Location.X, ultraPanel55.Location.Y + ultraPanel55.Height);
                        dataGridView6.Location = new Point(dataGridView6.Location.X, ultraLabel150.Location.Y + ultraLabel150.Height);
                        ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                        ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                        dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                        ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                        ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);

                        foreach (var item in lstTM012GTGT1)
                        {
                            int index = lstTM012GTGT1.IndexOf(item);
                            if (lstTM012GTGT1.IndexOf(item) == 0)
                            {
                                dataGridView5.Rows.Clear();
                                dataGridView5.Rows.Add();


                            }
                            else
                            {
                                dataGridView5.Rows.Add();
                            }

                            dataGridView5.Rows[index].Cells[0].Value = item.OrderPriority;
                            dataGridView5.Rows[index].Cells[1].Value = item.InvoiceNo;
                            dataGridView5.Rows[index].Cells[2].Value = item.InvoiceDate;
                            dataGridView5.Rows[index].Cells[3].Value = item.AccountingObjectName;
                            dataGridView5.Rows[index].Cells[4].Value = item.TaxCode;
                            dataGridView5.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.PretaxAmount, ConstDatabase.Format_TienVND);
                            dataGridView5.Rows[index].Cells[6].Value = Utils.FormatNumberic(item.TaxAmount, ConstDatabase.Format_TienVND);
                            dataGridView5.Rows[index].Cells[7].Value = item.Note;
                            dataGridView5.Rows[index].Cells[8].Value = item.VoucherID;
                        }
                        dataGridView5.AllowUserToAddRows = false;
                    }
                    else
                    {
                        dataGridView5.Rows.Add();
                        dataGridView5.AllowUserToAddRows = false;
                        ultraPanel55.Location = new Point(ultraPanel55.Location.X, dataGridView5.Location.Y + dataGridView5.Height);
                        ultraLabel150.Location = new Point(ultraLabel150.Location.X, ultraPanel55.Location.Y + ultraPanel55.Height);
                        dataGridView6.Location = new Point(dataGridView6.Location.X, ultraLabel150.Location.Y + ultraLabel150.Height);
                        ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                        ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                        dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                        ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                        ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);

                    }

                    #endregion

                    #region table 6
                    var lstTM012GTGT2 = new List<TM012GTGT>();
                    lstTM012GTGT2 = input.TM012GTGTs.Where(n => n.Type == 2 && n.TM01GTGTID == input.ID).OrderBy(n => n.OrderPriority).ToList();
                    if (lstTM012GTGT2.Count > 0)
                    {
                        dataGridView6.Size = new Size(1232, lstTM012GTGT2.Count * 22 + 1);
                        ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                        ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                        dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                        ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                        ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);

                        foreach (var item in lstTM012GTGT2)
                        {
                            int index = lstTM012GTGT2.IndexOf(item);
                            if (lstTM012GTGT2.IndexOf(item) == 0)
                            {
                                dataGridView6.Rows.Clear();
                                dataGridView6.Rows.Add();

                            }
                            else
                            {
                                dataGridView6.Rows.Add();
                            }
                            dataGridView6.Rows[index].Cells[0].Value = item.OrderPriority;
                            dataGridView6.Rows[index].Cells[1].Value = item.InvoiceNo;
                            dataGridView6.Rows[index].Cells[2].Value = item.InvoiceDate;
                            dataGridView6.Rows[index].Cells[3].Value = item.AccountingObjectName;
                            dataGridView6.Rows[index].Cells[4].Value = item.TaxCode;
                            dataGridView6.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.PretaxAmount, ConstDatabase.Format_TienVND);
                            dataGridView6.Rows[index].Cells[6].Value = Utils.FormatNumberic(item.TaxAmount, ConstDatabase.Format_TienVND);
                            dataGridView6.Rows[index].Cells[7].Value = item.Note;
                            dataGridView6.Rows[index].Cells[8].Value = item.VoucherID;
                        }
                        dataGridView6.AllowUserToAddRows = false;
                    }
                    else
                    {
                        dataGridView6.Rows.Add();
                        dataGridView6.AllowUserToAddRows = false;
                        ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                        ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                        dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                        ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                        ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);

                    }

                    #endregion

                    #region table 7
                    var lstTM012GTGT3 = new List<TM012GTGT>();
                    lstTM012GTGT3 = input.TM012GTGTs.Where(n => n.Type == 3 && n.TM01GTGTID == input.ID).OrderBy(n => n.OrderPriority).ToList();
                    if (lstTM012GTGT3.Count > 0)
                    {
                        dataGridView7.Size = new Size(1232, lstTM012GTGT3.Count * 22 + 1);
                        ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                        ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);
                        foreach (var item in lstTM012GTGT3)
                        {
                            int index = lstTM012GTGT3.IndexOf(item);
                            if (lstTM012GTGT3.IndexOf(item) == 0)
                            {
                                dataGridView7.Rows.Clear();
                                dataGridView7.Rows.Add();


                            }
                            else
                            {
                                dataGridView7.Rows.Add();
                            }
                            dataGridView7.Rows[index].Cells[0].Value = item.OrderPriority;
                            dataGridView7.Rows[index].Cells[1].Value = item.InvoiceNo;
                            dataGridView7.Rows[index].Cells[2].Value = item.InvoiceDate;
                            dataGridView7.Rows[index].Cells[3].Value = item.AccountingObjectName;
                            dataGridView7.Rows[index].Cells[4].Value = item.TaxCode;
                            dataGridView7.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.PretaxAmount, ConstDatabase.Format_TienVND);
                            dataGridView7.Rows[index].Cells[6].Value = Utils.FormatNumberic(item.TaxAmount, ConstDatabase.Format_TienVND);
                            dataGridView7.Rows[index].Cells[7].Value = item.Note;
                            dataGridView7.Rows[index].Cells[8].Value = item.VoucherID;
                        }
                        dataGridView7.AllowUserToAddRows = false;
                    }
                    else
                    {
                        dataGridView7.Rows.Add();
                        dataGridView7.AllowUserToAddRows = false;
                        ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                        ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);

                    }

                    #endregion
                }
                #endregion

                #region KHBS
                if (ultraTabControl1.Tabs[3].Visible == true)
                {
                    var lstTM01GTGTAdjust1 = new List<TM01GTGTAdjust>();
                    lstTM01GTGTAdjust1 = input.TM01GTGTAdjusts.Where(n => n.Type == 1 && n.TM01GTGTID == input.ID).OrderBy(n => n.OrderPriority).ToList();
                    if (lstTM01GTGTAdjust1.Count > 0)
                    {
                        dataGridView8.Size = new Size(991, lstTM01GTGTAdjust1.Count * 22 + 1);
                        ultraLabel164.Location = new Point(ultraLabel164.Location.X, dataGridView8.Location.Y + dataGridView8.Height);
                        dataGridView9.Location = new Point(dataGridView9.Location.X, ultraLabel164.Location.Y + ultraLabel164.Height);
                        tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, dataGridView9.Location.Y + dataGridView9.Height);
                        ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);

                        foreach (var item in lstTM01GTGTAdjust1)
                        {
                            int index = lstTM01GTGTAdjust1.IndexOf(item);
                            if (lstTM01GTGTAdjust1.IndexOf(item) == 0)
                            {
                                dataGridView8.Rows.Clear();
                                dataGridView8.Rows.Add();
                            }
                            else
                            {
                                dataGridView8.Rows.Add();
                            }
                            dataGridView8.Rows[index].Cells[0].Value = item.OrderPriority;
                            dataGridView8.Rows[index].Cells[1].Value = item.Name;
                            dataGridView8.Rows[index].Cells[2].Value = item.Code;
                            dataGridView8.Rows[index].Cells[3].Value = Utils.FormatNumberic(item.DeclaredAmount, ConstDatabase.Format_TienVND);
                            dataGridView8.Rows[index].Cells[4].Value = Utils.FormatNumberic(item.AdjustAmount, ConstDatabase.Format_TienVND);
                            dataGridView8.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.DifferAmount, ConstDatabase.Format_TienVND);


                        }
                        dataGridView8.AllowUserToAddRows = false;
                    }
                    else
                    {
                        dataGridView8.Rows.Add();
                        dataGridView8.AllowUserToAddRows = false;
                        ultraLabel164.Location = new Point(ultraLabel164.Location.X, dataGridView8.Location.Y + dataGridView8.Height);
                        dataGridView9.Location = new Point(dataGridView9.Location.X, ultraLabel164.Location.Y + ultraLabel164.Height);
                        tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, dataGridView9.Location.Y + dataGridView9.Height);
                        ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);

                    }

                    for (int i = 0; i < dataGridView8.RowCount; i++)
                    {
                        dataGridView8.Rows[i].Cells[0].Value = i + 1;
                    }

                    var lstTM01GTGTAdjust2 = new List<TM01GTGTAdjust>();
                    lstTM01GTGTAdjust2 = input.TM01GTGTAdjusts.Where(n => n.Type == 2 && n.TM01GTGTID == input.ID).OrderBy(n => n.OrderPriority).ToList();
                    if (lstTM01GTGTAdjust2.Count > 0)
                    {
                        dataGridView9.Size = new Size(991, lstTM01GTGTAdjust2.Count * 22 + 1);
                        tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, dataGridView9.Location.Y + dataGridView9.Height);
                        ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);
                        foreach (var item in lstTM01GTGTAdjust2)
                        {
                            int index = lstTM01GTGTAdjust2.IndexOf(item);
                            if (lstTM01GTGTAdjust2.IndexOf(item) == 0)
                            {
                                dataGridView9.Rows.Clear();
                                dataGridView9.Rows.Add();

                            }
                            else
                            {
                                dataGridView9.Rows.Add();
                            }
                            dataGridView9.Rows[index].Cells[0].Value = item.OrderPriority;
                            dataGridView9.Rows[index].Cells[1].Value = item.Name;
                            dataGridView9.Rows[index].Cells[2].Value = item.Code;
                            dataGridView9.Rows[index].Cells[3].Value = Utils.FormatNumberic(item.DeclaredAmount, ConstDatabase.Format_TienVND);
                            dataGridView9.Rows[index].Cells[4].Value = Utils.FormatNumberic(item.AdjustAmount, ConstDatabase.Format_TienVND);
                            dataGridView9.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.DifferAmount, ConstDatabase.Format_TienVND);
                        }
                        dataGridView9.AllowUserToAddRows = false;
                    }
                    else
                    {
                        dataGridView9.Rows.Add();
                        dataGridView9.AllowUserToAddRows = false;
                        tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, dataGridView9.Location.Y + dataGridView9.Height);
                        ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);

                    }

                    for (int i = 0; i < dataGridView9.RowCount; i++)
                    {
                        dataGridView9.Rows[i].Cells[0].Value = i + 1;
                    }

                    var _tM01GTGTAdjust40 = new TM01GTGTAdjust();
                    _tM01GTGTAdjust40 = input.TM01GTGTAdjusts.Where(n => n.Code == "40" && n.TM01GTGTID == input.ID).FirstOrDefault();
                    if (_tM01GTGTAdjust40 != null)
                    {
                        lblCodeItem40.Text = _tM01GTGTAdjust40.Code ?? "";
                        lblDeclaredTaxAmount.FormatNumberic(_tM01GTGTAdjust40.DeclaredAmount.ToString() ?? "0", ConstDatabase.Format_TienVND);
                        lblAdjustTaxAmount.FormatNumberic(_tM01GTGTAdjust40.AdjustAmount.ToString() ?? "0", ConstDatabase.Format_TienVND);
                        lblDifTaxAmount.FormatNumberic(_tM01GTGTAdjust40.DifferAmount.ToString() ?? "0", ConstDatabase.Format_TienVND);
                        txtDateDelay.Value = _tM01GTGTAdjust40.LateDays ?? 0;
                        BindData(txtLateAmount, _tM01GTGTAdjust40.LateAmount);
                        BindData(txtExplainAmount, _tM01GTGTAdjust40.ExplainAmount);
                        txtCommandNo.Text = _tM01GTGTAdjust40.CommandNo ?? "0";
                        dteDate.DateTime = _tM01GTGTAdjust40.CommandDate ?? DateTime.Now;
                        txtTaxCompanyName.Text = _tM01GTGTAdjust40.TaxCompanyName ?? "";
                        txtTaxCompanyDecisionName.Text = _tM01GTGTAdjust40.TaxCompanyDecisionName ?? "";
                        txtReceiveDays.Value = _tM01GTGTAdjust40.ReceiveDays ?? 0;
                        BindData(txtExplainLateAmount, _tM01GTGTAdjust40.ExplainLateAmount);
                        txtReason.Text = _tM01GTGTAdjust40.DifferReason ?? "";
                    }
                    var _tM01GTGTAdjust43 = new TM01GTGTAdjust();
                    _tM01GTGTAdjust43 = input.TM01GTGTAdjusts.Where(n => n.Code == "43" && n.TM01GTGTID == input.ID).FirstOrDefault();
                    if (_tM01GTGTAdjust43 != null)
                    {
                        lblCodeItem43.Text = _tM01GTGTAdjust43.Code ?? "";
                        lblDeclaredDiscountAmount.FormatNumberic(_tM01GTGTAdjust43.DeclaredAmount.ToString() ?? "0", ConstDatabase.Format_TienVND);
                        lblAdjustDiscountAmount.FormatNumberic(_tM01GTGTAdjust43.AdjustAmount.ToString() ?? "0", ConstDatabase.Format_TienVND);
                        lblDifDiscountAmount.FormatNumberic(_tM01GTGTAdjust43.DifferAmount.ToString() ?? "0", ConstDatabase.Format_TienVND);
                    }

                }
                #endregion
            }
            #endregion

            return input;
        }
        #endregion

        #region ObjectGUIKHBS
        public TM01GTGT ObjandGUIKHBS(TM01GTGT input, bool isGet)
        {
            #region Lưu vào DB
            if (isGet)
            {
            }
            #endregion

            #region Lấy từ DB lên
            else
            {

                cbbExtensionCase.Value = input.ExtensionCase;
                ultraTabControl1.Tabs[1].Visible = false;
                ultraTabControl1.Tabs[2].Visible = false;
                ultraTabControl1.Tabs[3].Visible = true;
                utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Visible = true;
                chkFirstDeclared.Checked = false;
                chkExtend.Checked = input.IsExtend;
                lblDeclarationTerm.Text = input.DeclarationTerm;
                lblCompanyName.Text = input.CompanyName;
                lblCompanyTaxCode.Text = input.CompanyTaxCode;
                lblTaxAgencyName.Text = input.TaxAgencyName;
                lblAgencyTaxCode.Text = input.TaxAgencyTaxCode;
                txtName.Text = input.TaxAgencyEmployeeName;
                txtCertificationNo.Text = input.CertificationNo;
                //ultraLabel154.Text = input.AdditionTerm;
                txtSignDate.Value = input.SignDate;
                txtSignName.Text = input.SignName;
                //if (input.AdditionTime != null && input.AdditionTime != 0)
                //    txtAdditionTime.Value = input.AdditionTime;

                #region chỉ tiêu
                if (input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item21" && n.TM01GTGTID == input.ID).Data == "0")
                    chkItem21.Checked = false;
                else
                    chkItem21.Checked = true;
                BindData(txtItem22, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item22" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem23, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item23" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem24, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item24" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem25, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item25" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem26, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item26" && n.TM01GTGTID == input.ID).Data));
                txtItem27.FormatNumberic(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item27" && n.TM01GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                txtItem28.FormatNumberic(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item28" && n.TM01GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                BindData(txtItem29, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item29" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem30, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item30" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem31, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item31" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem32, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item32" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem32a, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item32a" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem33, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item33" && n.TM01GTGTID == input.ID).Data));
                txtItem34.FormatNumberic(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item34" && n.TM01GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                txtItem35.FormatNumberic(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item35" && n.TM01GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                txtItem36.FormatNumberic(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item36" && n.TM01GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                BindData(txtItem37, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item37" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem38, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item38" && n.TM01GTGTID == input.ID).Data));
                BindData(txtItem39, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item39" && n.TM01GTGTID == input.ID).Data));
                txtItem40a.FormatNumberic(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40a" && n.TM01GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                BindData(txtItem40b, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40b" && n.TM01GTGTID == input.ID).Data));
                txtItem40.FormatNumberic(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40" && n.TM01GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                txtItem41.FormatNumberic(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item41" && n.TM01GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                BindData(txtItem42, decimal.Parse(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item42" && n.TM01GTGTID == input.ID).Data));
                txtItem43.FormatNumberic(input.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item43" && n.TM01GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                #endregion

                #region KHBS
                if (ultraTabControl1.Tabs[3].Visible == true)
                {
                    var lstTM01GTGTAdjust1 = new List<TM01GTGTAdjust>();
                    lstTM01GTGTAdjust1 = input.TM01GTGTAdjusts.Where(n => n.Type == 1 && n.TM01GTGTID == input.ID).OrderBy(n => n.OrderPriority).ToList();
                    if (lstTM01GTGTAdjust1.Count > 0)
                    {
                        dataGridView8.Size = new Size(991, lstTM01GTGTAdjust1.Count * 22 + 1);
                        ultraLabel164.Location = new Point(ultraLabel164.Location.X, dataGridView8.Location.Y + dataGridView8.Height);
                        dataGridView9.Location = new Point(dataGridView9.Location.X, ultraLabel164.Location.Y + ultraLabel164.Height);
                        tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, dataGridView9.Location.Y + dataGridView9.Height);
                        ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);

                        foreach (var item in lstTM01GTGTAdjust1)
                        {
                            int index = lstTM01GTGTAdjust1.IndexOf(item);
                            if (lstTM01GTGTAdjust1.IndexOf(item) == 0)
                            {
                                dataGridView8.Rows.Clear();
                                dataGridView8.Rows.Add();
                            }
                            else
                            {
                                dataGridView8.Rows.Add();
                            }
                            dataGridView8.Rows[index].Cells[0].Value = item.OrderPriority;
                            dataGridView8.Rows[index].Cells[1].Value = item.Name;
                            dataGridView8.Rows[index].Cells[2].Value = item.Code;
                            dataGridView8.Rows[index].Cells[3].Value = Utils.FormatNumberic(item.DeclaredAmount, ConstDatabase.Format_TienVND);
                            dataGridView8.Rows[index].Cells[4].Value = Utils.FormatNumberic(item.AdjustAmount, ConstDatabase.Format_TienVND);
                            dataGridView8.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.DifferAmount, ConstDatabase.Format_TienVND);

                        }
                        dataGridView8.AllowUserToAddRows = false;
                    }
                    else
                    {
                        dataGridView8.Rows.Add();
                        dataGridView8.AllowUserToAddRows = false;
                        ultraLabel164.Location = new Point(ultraLabel164.Location.X, dataGridView8.Location.Y + dataGridView8.Height);
                        dataGridView9.Location = new Point(dataGridView9.Location.X, ultraLabel164.Location.Y + ultraLabel164.Height);
                        tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, dataGridView9.Location.Y + dataGridView9.Height);
                        ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);

                    }
                    for (int i = 0; i < dataGridView8.RowCount; i++)
                    {
                        dataGridView8.Rows[i].Cells[0].Value = i + 1;
                    }
                    var lstTM01GTGTAdjust2 = new List<TM01GTGTAdjust>();
                    lstTM01GTGTAdjust2 = input.TM01GTGTAdjusts.Where(n => n.Type == 2 && n.TM01GTGTID == input.ID).OrderBy(n => n.OrderPriority).ToList();
                    if (lstTM01GTGTAdjust2.Count > 0)
                    {
                        dataGridView9.Size = new Size(991, lstTM01GTGTAdjust2.Count * 22 + 1);
                        tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, dataGridView9.Location.Y + dataGridView9.Height);
                        ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);


                        foreach (var item in lstTM01GTGTAdjust2)
                        {
                            int index = lstTM01GTGTAdjust2.IndexOf(item);
                            if (lstTM01GTGTAdjust2.IndexOf(item) == 0)
                            {
                                dataGridView9.Rows.Clear();
                                dataGridView9.Rows.Add();

                            }
                            else
                            {
                                dataGridView9.Rows.Add();
                            }
                            dataGridView9.Rows[index].Cells[0].Value = item.OrderPriority;
                            dataGridView9.Rows[index].Cells[1].Value = item.Name;
                            dataGridView9.Rows[index].Cells[2].Value = item.Code;
                            dataGridView9.Rows[index].Cells[3].Value = Utils.FormatNumberic(item.DeclaredAmount, ConstDatabase.Format_TienVND);
                            dataGridView9.Rows[index].Cells[4].Value = Utils.FormatNumberic(item.AdjustAmount, ConstDatabase.Format_TienVND);
                            dataGridView9.Rows[index].Cells[5].Value = Utils.FormatNumberic(item.DifferAmount, ConstDatabase.Format_TienVND);

                        }
                        dataGridView9.AllowUserToAddRows = false;
                    }
                    else
                    {
                        dataGridView9.Rows.Add();
                        dataGridView9.AllowUserToAddRows = false;
                        tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, dataGridView9.Location.Y + dataGridView9.Height);
                        ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);

                    }
                    for (int i = 0; i < dataGridView9.RowCount; i++)
                    {
                        dataGridView9.Rows[i].Cells[0].Value = i + 1;
                    }
                    var _tM01GTGTAdjust40 = new TM01GTGTAdjust();
                    _tM01GTGTAdjust40 = input.TM01GTGTAdjusts.Where(n => n.Code == "40" && n.TM01GTGTID == input.ID).FirstOrDefault();
                    if (_tM01GTGTAdjust40 != null)
                    {
                        lblCodeItem40.Text = _tM01GTGTAdjust40.Code ?? "";
                        lblDeclaredTaxAmount.FormatNumberic(_tM01GTGTAdjust40.DeclaredAmount.ToString() ?? "0", ConstDatabase.Format_TienVND);
                        lblAdjustTaxAmount.FormatNumberic(_tM01GTGTAdjust40.AdjustAmount.ToString() ?? "0", ConstDatabase.Format_TienVND);
                        lblDifTaxAmount.FormatNumberic(_tM01GTGTAdjust40.DifferAmount.ToString() ?? "0", ConstDatabase.Format_TienVND);
                        txtDateDelay.Value = _tM01GTGTAdjust40.LateDays ?? 0;
                        BindData(txtLateAmount, _tM01GTGTAdjust40.LateAmount);
                        BindData(txtExplainAmount, _tM01GTGTAdjust40.ExplainAmount);
                        txtCommandNo.Text = _tM01GTGTAdjust40.CommandNo ?? "0";
                        dteDate.DateTime = _tM01GTGTAdjust40.CommandDate ?? DateTime.Now;
                        txtTaxCompanyName.Text = _tM01GTGTAdjust40.TaxCompanyName ?? "";
                        txtTaxCompanyDecisionName.Text = _tM01GTGTAdjust40.TaxCompanyDecisionName ?? "";
                        txtReceiveDays.Value = _tM01GTGTAdjust40.ReceiveDays ?? 0;
                        BindData(txtExplainLateAmount, _tM01GTGTAdjust40.ExplainLateAmount);
                        txtReason.Text = _tM01GTGTAdjust40.DifferReason ?? "";
                    }
                    var _tM01GTGTAdjust43 = new TM01GTGTAdjust();
                    _tM01GTGTAdjust43 = input.TM01GTGTAdjusts.Where(n => n.Code == "43" && n.TM01GTGTID == input.ID).FirstOrDefault();
                    if (_tM01GTGTAdjust43 != null)
                    {
                        lblCodeItem43.Text = _tM01GTGTAdjust43.Code ?? "";
                        lblDeclaredDiscountAmount.FormatNumberic(_tM01GTGTAdjust43.DeclaredAmount.ToString() ?? "0", ConstDatabase.Format_TienVND);
                        lblAdjustDiscountAmount.FormatNumberic(_tM01GTGTAdjust43.AdjustAmount.ToString() ?? "0", ConstDatabase.Format_TienVND);
                        lblDifDiscountAmount.FormatNumberic(_tM01GTGTAdjust43.DifferAmount.ToString() ?? "0", ConstDatabase.Format_TienVND);
                    }

                }
                #endregion
            }
            #endregion

            return input;
        }
        #endregion

        #region Check lỗi

        private bool CheckError1(DataGridView dataGridView)
        {
            dataGridView.EndEdit();
            for (int t = 0; t < dataGridView.RowCount - 1; t++)
            {

                if (dataGridView.Rows[t].Cells[1].Value.IsNullOrEmpty() && dataGridView.Rows[t].Cells[2].Value.IsNullOrEmpty() && dataGridView.Rows[t].Cells[3].Value.IsNullOrEmpty() && dataGridView.Rows[t].Cells[4].Value.IsNullOrEmpty()
                                                && dataGridView.Rows[t].Cells[5].Value.IsNullOrEmpty() && dataGridView.Rows[t].Cells[6].Value.IsNullOrEmpty() && dataGridView.Rows[t].Cells[7].Value.IsNullOrEmpty())
                {

                    return true;

                }
                else
                {
                    if (dataGridView.Rows[t].Cells[1].Value.IsNullOrEmpty())
                    {
                        MSG.Error("Số hóa đơn không được bỏ trống");
                        dataGridView.Focus();
                        return false;
                    }
                    else if (dataGridView.Rows[t].Cells[2].Value.IsNullOrEmpty())
                    {
                        MSG.Error("Ngày hóa đơn không được bỏ trống");
                        dataGridView.Focus();
                        return false;
                    }
                    else if (Convert.ToDateTime(getDate(dataGridView.Rows[t].Cells[2])) > ToDate)
                    {
                        MSG.Error("Ngày hóa đơn phải nhỏ hơn hoặc bằng ngày cuối cùng của kỳ tính thuế");
                        dataGridView.Focus();
                        return false;
                    }
                    else if (dataGridView.Rows[t].Cells[3].Value.IsNullOrEmpty())
                    {
                        MSG.Error("Tên người bán không được bỏ trống");
                        dataGridView.Focus();
                        return false;
                    }
                }
            }


            return true;
        }
        private bool CheckError2(TableLayoutPanel tableLayoutPanel)
        {
            for (int t = 1; t < tableLayoutPanel.RowCount; t++)
            {
                var control1 = tableLayoutPanel.GetControlFromPosition(1, t);
                var control2 = tableLayoutPanel.GetControlFromPosition(2, t);
                var control3 = tableLayoutPanel.GetControlFromPosition(3, t);
                var control4 = tableLayoutPanel.GetControlFromPosition(4, t);
                var control5 = tableLayoutPanel.GetControlFromPosition(5, t);
                var control6 = tableLayoutPanel.GetControlFromPosition(6, t);
                var control7 = tableLayoutPanel.GetControlFromPosition(7, t);
                if (control1 is UltraTextEditor)
                {
                    if (control2 is UltraDateTimeEditor)
                    {
                        if (control3 is UltraTextEditor)
                        {
                            if (control4 is UltraTextEditor)
                            {
                                if (control5 is UltraMaskedEdit)
                                {
                                    if (control6 is UltraMaskedEdit)
                                    {
                                        if (control7 is UltraTextEditor)
                                        {
                                            UltraTextEditor ivNo = control1 as UltraTextEditor;
                                            UltraDateTimeEditor ivDate = control2 as UltraDateTimeEditor;
                                            UltraTextEditor AccName = control3 as UltraTextEditor;
                                            UltraTextEditor AccTax = control4 as UltraTextEditor;
                                            UltraMaskedEdit PreTax = control5 as UltraMaskedEdit;
                                            UltraMaskedEdit Tax = control6 as UltraMaskedEdit;
                                            UltraTextEditor Note = control7 as UltraTextEditor;
                                            int row = tableLayoutPanel.RowCount - 1;
                                            //if (row == 1)
                                            if (ivNo.Text == "" && ivDate.Value == null && AccName.Text == "" && AccTax.Text == ""
                                                && PreTax.Text.IsNullOrEmpty() && Tax.Text.IsNullOrEmpty() && Note.Text == "" && row == 1)
                                            {
                                                //if (ivNo.Text == "" && ivDate.Value == null && AccName.Text == "")
                                                return true;
                                                //else
                                                //{
                                                //    if (ivNo.Text == "")
                                                //    {
                                                //        MSG.Error("Số hóa đơn không được bỏ trống");
                                                //        ivNo.Focus();
                                                //        return false;
                                                //    }
                                                //    else if (ivDate.Value == null)
                                                //    {
                                                //        MSG.Error("Ngày hóa đơn không được bỏ trống");
                                                //        ivDate.Focus();
                                                //        return false;
                                                //    }
                                                //    else if (ivDate.DateTime > ToDate)
                                                //    {
                                                //        MSG.Error("Ngày hóa đơn phải nhỏ hơn hoặc bằng ngày cuối cùng của kỳ tính thuế");
                                                //        ivDate.Focus();
                                                //        return false;
                                                //    }
                                                //    else if (AccName.Text == "")
                                                //    {
                                                //        MSG.Error("Tên người bán không được bỏ trống");
                                                //        AccName.Focus();
                                                //        return false;
                                                //    }
                                                //}
                                            }
                                            else
                                            {
                                                if (ivNo.Text == "")
                                                {
                                                    MSG.Error("Số hóa đơn không được bỏ trống");
                                                    ivNo.Focus();
                                                    return false;
                                                }
                                                else if (ivDate.Value == null)
                                                {
                                                    MSG.Error("Ngày hóa đơn không được bỏ trống");
                                                    ivDate.Focus();
                                                    return false;
                                                }
                                                else if (ivDate.DateTime > ToDate)
                                                {
                                                    MSG.Error("Ngày hóa đơn phải nhỏ hơn hoặc bằng ngày cuối cùng của kỳ tính thuế");
                                                    ivDate.Focus();
                                                    return false;
                                                }
                                                else if (AccName.Text == "")
                                                {
                                                    MSG.Error("Tên người mua không được bỏ trống");
                                                    AccName.Focus();
                                                    return false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }

                }
            }
            return true;
        }
        #endregion

        #region Lưu thì xóa dòng null
        private void DeleteRowNull()
        {
            #region table 1


            for (int t = 0; t < dataGridView1.RowCount; t++)
            {
                if (dataGridView1.Rows[t].Cells[1].Value.IsNullOrEmpty() && dataGridView1.Rows[t].Cells[2].Value.IsNullOrEmpty() && dataGridView1.Rows[t].Cells[3].Value.IsNullOrEmpty())
                {
                    dataGridView1.Rows.Remove(dataGridView1.Rows[t]);
                    t--;
                }

            }
            if (dataGridView1.RowCount == 0)
                dataGridView1.Rows.Add();
            dataGridView1.Size = new Size(dataGridView1.Width, (dataGridView1.RowCount) * 22);
            ultraPanel51.Location = new Point(ultraPanel51.Location.X, dataGridView1.Location.Y + dataGridView1.Height);
            ultraLabel172.Location = new Point(ultraLabel172.Location.X, ultraPanel51.Location.Y + ultraPanel51.Height);
            dataGridView2.Location = new Point(dataGridView2.Location.X, ultraLabel172.Location.Y + ultraLabel172.Height);
            ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
            ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
            dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
            ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
            ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
            dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                dataGridView1.Rows[i].Cells[0].Value = i + 1;
            }

            #endregion
            #region table 2
            for (int t = 0; t < dataGridView2.RowCount; t++)
            {
                if (dataGridView2.Rows[t].Cells[1].Value.IsNullOrEmpty() && dataGridView2.Rows[t].Cells[2].Value.IsNullOrEmpty() && dataGridView2.Rows[t].Cells[3].Value.IsNullOrEmpty())
                {
                    dataGridView2.Rows.Remove(dataGridView2.Rows[t]);
                    t--;
                }

            }
            if (dataGridView2.RowCount == 0)
                dataGridView2.Rows.Add();
            dataGridView2.Size = new Size(dataGridView2.Width, (dataGridView2.RowCount) * 22);
            ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
            ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
            dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
            ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
            ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
            dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
            for (int i = 0; i < dataGridView2.RowCount; i++)
            {
                dataGridView2.Rows[i].Cells[0].Value = i + 1;
            }
            #endregion
            #region table 3
            for (int t = 0; t < dataGridView3.RowCount; t++)
            {
                if (dataGridView3.Rows[t].Cells[1].Value.IsNullOrEmpty() && dataGridView3.Rows[t].Cells[2].Value.IsNullOrEmpty() && dataGridView3.Rows[t].Cells[3].Value.IsNullOrEmpty())
                {
                    dataGridView3.Rows.Remove(dataGridView3.Rows[t]);
                    t--;
                }

            }
            if (dataGridView3.RowCount == 0)
                dataGridView3.Rows.Add();
            dataGridView3.Size = new Size(dataGridView3.Width, (dataGridView3.RowCount) * 22);
            ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
            ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
            dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
            for (int i = 0; i < dataGridView3.RowCount; i++)
            {
                dataGridView3.Rows[i].Cells[0].Value = i + 1;
            }
            #endregion
            #region table 4
            for (int t = 0; t < dataGridView4.RowCount; t++)
            {
                if (dataGridView4.Rows[t].Cells[1].Value.IsNullOrEmpty() && dataGridView4.Rows[t].Cells[2].Value.IsNullOrEmpty() && dataGridView4.Rows[t].Cells[3].Value.IsNullOrEmpty())
                {
                    dataGridView4.Rows.Remove(dataGridView4.Rows[t]);
                    t--;
                }

            }
            if (dataGridView4.RowCount == 0)
                dataGridView4.Rows.Add();
            dataGridView4.Size = new Size(dataGridView4.Width, (dataGridView4.RowCount) * 22);

            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
            for (int i = 0; i < dataGridView4.RowCount; i++)
            {
                dataGridView4.Rows[i].Cells[0].Value = i + 1;
            }
            #endregion
            #region table 5
            for (int t = 0; t < dataGridView5.RowCount; t++)
            {
                if (dataGridView5.Rows[t].Cells[1].Value.IsNullOrEmpty() && dataGridView5.Rows[t].Cells[2].Value.IsNullOrEmpty() && dataGridView5.Rows[t].Cells[3].Value.IsNullOrEmpty())
                {
                    dataGridView5.Rows.Remove(dataGridView5.Rows[t]);
                    t--;
                }

            }
            if (dataGridView5.RowCount == 0)
                dataGridView5.Rows.Add();
            dataGridView5.Size = new Size(dataGridView5.Width, (dataGridView5.RowCount) * 22);

            ultraPanel55.Location = new Point(ultraPanel55.Location.X, dataGridView5.Location.Y + dataGridView5.Height);
            ultraLabel150.Location = new Point(ultraLabel150.Location.X, ultraPanel55.Location.Y + ultraPanel55.Height);
            dataGridView6.Location = new Point(dataGridView6.Location.X, ultraLabel150.Location.Y + ultraLabel150.Height);
            ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
            ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
            dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
            ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
            ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);
            for (int i = 0; i < dataGridView5.RowCount; i++)
            {
                dataGridView5.Rows[i].Cells[0].Value = i + 1;
            }


            #endregion
            #region table 6
            for (int t = 0; t < dataGridView6.RowCount; t++)
            {
                if (dataGridView6.Rows[t].Cells[1].Value.IsNullOrEmpty() && dataGridView6.Rows[t].Cells[2].Value.IsNullOrEmpty() && dataGridView6.Rows[t].Cells[3].Value.IsNullOrEmpty())
                {
                    dataGridView6.Rows.Remove(dataGridView6.Rows[t]);
                    t--;
                }

            }
            if (dataGridView6.RowCount == 0)
                dataGridView6.Rows.Add();
            dataGridView6.Size = new Size(dataGridView6.Width, (dataGridView6.RowCount) * 22);


            ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
            ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
            dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
            ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
            ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);
            for (int i = 0; i < dataGridView6.RowCount; i++)
            {
                dataGridView6.Rows[i].Cells[0].Value = i + 1;
            }

            #endregion
            #region table 7
            for (int t = 0; t < dataGridView7.RowCount; t++)
            {
                if (dataGridView7.Rows[t].Cells[1].Value.IsNullOrEmpty() && dataGridView7.Rows[t].Cells[2].Value.IsNullOrEmpty() && dataGridView7.Rows[t].Cells[3].Value.IsNullOrEmpty())
                {
                    dataGridView7.Rows.Remove(dataGridView7.Rows[t]);
                    t--;
                }

            }
            if (dataGridView7.RowCount == 0)
                dataGridView7.Rows.Add();
            dataGridView7.Size = new Size(dataGridView7.Width, (dataGridView7.RowCount) * 22);



            ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
            ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);
            for (int i = 0; i < dataGridView7.RowCount; i++)
            {
                dataGridView7.Rows[i].Cells[0].Value = i + 1;
            }

            #endregion
        }
        #endregion

        #region Load lại dữ liệu khi khai bổ sung
        private void LoadDataKHBS1(List<TM01GTGTAdjust> lstBS1)
        {
            #region table 8
            if (lstBS1 != null)
            {

                //for (int t = 1; t < tableLayoutPanel8.RowCount; t++)
                //{
                //    var control0 = tableLayoutPanel8.GetControlFromPosition(0, t);
                //    var control1 = tableLayoutPanel8.GetControlFromPosition(1, t);
                //    var control2 = tableLayoutPanel8.GetControlFromPosition(2, t);
                //    var control3 = tableLayoutPanel8.GetControlFromPosition(3, t);
                //    var control4 = tableLayoutPanel8.GetControlFromPosition(4, t);
                //    var control5 = tableLayoutPanel8.GetControlFromPosition(5, t);
                //    if (control0 is UltraLabel)
                //    {
                //        if (control1 is UltraTextEditor)
                //        {
                //            if (control2 is UltraTextEditor)
                //            {
                //                if (control3 is UltraTextEditor)
                //                {
                //                    if (control4 is UltraTextEditor)
                //                    {
                //                        if (control5 is UltraTextEditor)
                //                        {
                //                            UltraLabel STT = control0 as UltraLabel;
                //                            UltraTextEditor DeclareName = control1 as UltraTextEditor;
                //                            UltraTextEditor DeclareCode = control2 as UltraTextEditor;
                //                            UltraTextEditor DeclareAmount = control3 as UltraTextEditor;
                //                            UltraTextEditor AdjustAmount = control4 as UltraTextEditor;
                //                            UltraTextEditor DiffAmount = control5 as UltraTextEditor;
                //                            if (DeclareName.Text != "")
                //                            {
                //                                int r = tableLayoutPanel8.RowCount - 1;
                //                                if (r == 1)
                //                                {
                //                                    DeclareName.Text = "";
                //                                    DeclareCode.Text = "";
                //                                    DeclareAmount.Text = "0";
                //                                    AdjustAmount.Text = "0";
                //                                    DiffAmount.Text = "0";
                //                                    return;
                //                                }
                //                                DeclareName.Focus();
                //                                int row = GetRow(tableLayoutPanel8);
                //                                remove_row(tableLayoutPanel8, row);
                //                                tableLayoutPanel9.Location = new Point(tableLayoutPanel9.Location.X, tableLayoutPanel8.Location.Y + tableLayoutPanel8.Height);
                //                                tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, tableLayoutPanel9.Location.Y + tableLayoutPanel9.Height);
                //                                ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);
                //                                t = 1;
                //                                for (int i = 1; i < tableLayoutPanel8.RowCount; i++)
                //                                {
                //                                    var control = tableLayoutPanel8.GetControlFromPosition(0, i);
                //                                    if (control is UltraLabel)
                //                                    {
                //                                        UltraLabel ultraTextEditor = control as UltraLabel;
                //                                        ultraTextEditor.Text = i.ToString();
                //                                    }
                //                                }

                //                            }
                //                        }
                //                    }
                //                }
                //            }

                //        }
                //    }
                //}
            }
            #endregion
        }
        private void LoadDataKHBS2(List<TM01GTGTAdjust> lstBS2)
        {
            #region table 9
            if (lstBS2 != null)
            {
                //for (int t = 1; t < tableLayoutPanel9.RowCount; t++)
                //{
                //    var control0 = tableLayoutPanel9.GetControlFromPosition(0, t);
                //    var control1 = tableLayoutPanel9.GetControlFromPosition(1, t);
                //    var control2 = tableLayoutPanel9.GetControlFromPosition(2, t);
                //    var control3 = tableLayoutPanel9.GetControlFromPosition(3, t);
                //    var control4 = tableLayoutPanel9.GetControlFromPosition(4, t);
                //    var control5 = tableLayoutPanel9.GetControlFromPosition(5, t);
                //    if (control0 is UltraLabel)
                //    {
                //        if (control1 is UltraTextEditor)
                //        {
                //            if (control2 is UltraTextEditor)
                //            {
                //                if (control3 is UltraTextEditor)
                //                {
                //                    if (control4 is UltraTextEditor)
                //                    {
                //                        if (control5 is UltraTextEditor)
                //                        {
                //                            UltraLabel STT = control0 as UltraLabel;
                //                            UltraTextEditor DeclareName = control1 as UltraTextEditor;
                //                            UltraTextEditor DeclareCode = control2 as UltraTextEditor;
                //                            UltraTextEditor DeclareAmount = control3 as UltraTextEditor;
                //                            UltraTextEditor AdjustAmount = control4 as UltraTextEditor;
                //                            UltraTextEditor DiffAmount = control5 as UltraTextEditor;
                //                            if (DeclareName.Text != "")
                //                            {
                //                                int r = tableLayoutPanel9.RowCount - 1;
                //                                if (r == 1)
                //                                {
                //                                    DeclareName.Text = "";
                //                                    DeclareCode.Text = "";
                //                                    DeclareAmount.Text = "0";
                //                                    AdjustAmount.Text = "0";
                //                                    DiffAmount.Text = "0";
                //                                    return;
                //                                }
                //                                DeclareName.Focus();
                //                                int row = GetRow(tableLayoutPanel9);
                //                                remove_row(tableLayoutPanel9, row);
                //                                tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, tableLayoutPanel9.Location.Y + tableLayoutPanel9.Height);
                //                                ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);
                //                                t = 1;
                //                                for (int i = 1; i < tableLayoutPanel9.RowCount; i++)
                //                                {
                //                                    var control = tableLayoutPanel9.GetControlFromPosition(0, i);
                //                                    if (control is UltraLabel)
                //                                    {
                //                                        UltraLabel ultraTextEditor = control as UltraLabel;
                //                                        ultraTextEditor.Text = i.ToString();
                //                                    }
                //                                }

                //                            }
                //                        }
                //                    }
                //                }
                //            }

                //        }
                //    }
                //}
            }
            #endregion
        }
        #endregion

        #region Tổng hợp KHBS
        private void THKBS()
        {
            CheckChangeBS();
            LoadDataKHBS1(lst1);
            LoadDataKHBS2(lst2);
            LoadUgridKBS1(lst1);
            LoadUgridKBS2(lst2);

            if (lst1.Count == 0 && lst2.Count == 0)
            {
                lblDeclaredTaxAmount.Text = "0";
                lblAdjustTaxAmount.Text = "0";
                lblDifTaxAmount.Text = "0";
                lblDeclaredDiscountAmount.Text = "0";
                lblAdjustDiscountAmount.Text = "0";
                lblDifDiscountAmount.Text = "0";

            }
            else
            {
                if (AdditionalTime == "1")
                {
                    TM01GTGT temp = Utils.ListTM01GTGT.Where(n => n.FromDate == FromDate && n.ToDate == ToDate && n.AdditionTime == null).FirstOrDefault();
                    lblCodeItem40.Text = "40";
                    lblDeclaredTaxAmount.FormatNumberic(temp.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40").Data, ConstDatabase.Format_TienVND);
                    lblAdjustTaxAmount.FormatNumberic(txtItem40.Text, ConstDatabase.Format_TienVND);
                    lblDifTaxAmount.FormatNumberic((getValueLabel(txtItem40) - getValueLabel(lblDeclaredTaxAmount)), ConstDatabase.Format_TienVND);
                    lblCodeItem43.Text = "43";
                    lblDeclaredDiscountAmount.FormatNumberic(temp.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item43").Data, ConstDatabase.Format_TienVND);
                    lblAdjustDiscountAmount.FormatNumberic(txtItem43.Text, ConstDatabase.Format_TienVND);
                    lblDifDiscountAmount.FormatNumberic((getValueLabel(txtItem43) - getValueLabel(lblDeclaredDiscountAmount)), ConstDatabase.Format_TienVND);
                }
                if (MaxAdditionTime != 0)
                {
                    TM01GTGT temp = Utils.ListTM01GTGT.Where(n => n.FromDate == FromDate && n.ToDate == ToDate && n.AdditionTime == MaxAdditionTime).FirstOrDefault();
                    lblCodeItem40.Text = "40";
                    lblDeclaredTaxAmount.FormatNumberic(temp.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40").Data, ConstDatabase.Format_TienVND);
                    lblAdjustTaxAmount.FormatNumberic(txtItem40.Text, ConstDatabase.Format_TienVND);
                    lblDifTaxAmount.FormatNumberic((getValueLabel(lblAdjustTaxAmount) - getValueLabel(lblDeclaredTaxAmount)), ConstDatabase.Format_TienVND);
                    lblCodeItem43.Text = "43";
                    lblDeclaredDiscountAmount.FormatNumberic(temp.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item43").Data, ConstDatabase.Format_TienVND);
                    lblAdjustDiscountAmount.FormatNumberic(txtItem43.Text, ConstDatabase.Format_TienVND);
                    lblDifDiscountAmount.FormatNumberic((getValueLabel(lblAdjustDiscountAmount) - getValueLabel(lblDeclaredDiscountAmount)), ConstDatabase.Format_TienVND);
                }
            }
            if (getValueLabel(lblDifTaxAmount) > 0)
                BindData(txtLateAmount, (getValueLabel(lblDifTaxAmount) * decimal.Parse(txtDateDelay.Value.ToString()) * decimal.Parse("0,03")) / 100);
        }
        #endregion

        #region  event
        private void txtInvoiceNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        private void txtInvoiceNo_Validated(object sender, EventArgs e)
        {
            var txtInvoiceNo = (UltraTextEditor)sender;
            if (txtInvoiceNo.Value != null && txtInvoiceNo.Text != "")
            {
                string t = txtInvoiceNo.Value.ToString();
                txtInvoiceNo.Value = t.PadLeft(7, '0');
            }
        }
        private void txtTax_Validated(object sender, EventArgs e)
        {
            var txtTaxCode = (UltraTextEditor)sender;
            if (txtTaxCode.Text != "")
            {
                if (!Core.Utils.CheckMST(txtTaxCode.Text))
                {
                    MSG.Error(resSystem.MSG_System_44);
                    txtTaxCode.Focus();
                    txtTaxCode.Text = "";
                    return;
                }
            }
        }

        private void txtItem29_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";

            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "(n,nnn,nnn,nnn,nnn,nnn)";
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            else
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
                ((UltraMaskedEdit)sender).FormatString = "###,###,###,###,###,##0";
            }

            txtItem27.FormatNumberic((getValueMaskedEdit(txtItem29) + getValueMaskedEdit(txtItem30) + getValueMaskedEdit(txtItem32) + getValueMaskedEdit(txtItem32a)), ConstDatabase.Format_TienVND);
        }
        private void txtItem30_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            else
            {
                ((UltraMaskedEdit)sender).FormatString = "###,###,###,###,###,##0";
            }

            txtItem27.FormatNumberic((getValueMaskedEdit(txtItem29) + getValueMaskedEdit(txtItem30) + getValueMaskedEdit(txtItem32) + getValueMaskedEdit(txtItem32a)), ConstDatabase.Format_TienVND);
            if (ultraTabControl1.Tabs[1].Visible == false)
                BindData(txtItem31, Math.Round(getValueMaskedEdit(txtItem30) * 5 / 100));
        }
        private void txtItem32_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            else
            {
                ((UltraMaskedEdit)sender).FormatString = "###,###,###,###,###,##0";
            }

            txtItem27.FormatNumberic((getValueMaskedEdit(txtItem29) + getValueMaskedEdit(txtItem30) + getValueMaskedEdit(txtItem32) + getValueMaskedEdit(txtItem32a)), ConstDatabase.Format_TienVND);
            if (ultraTabControl1.Tabs[1].Visible == false)
                BindData(txtItem33, Math.Round(getValueMaskedEdit(txtItem32) * 10 / 100));
        }

        private void chkExtend_CheckedChanged_1(object sender, EventArgs e)
        {
            if (chkExtend.Checked == true)
                cbbExtensionCase.ReadOnly = false;
            else
            { cbbExtensionCase.ReadOnly = true;
                cbbExtensionCase.Value = null;//trungnq sửa combo thuế sai datasource và sai readonly
            }
        }

        private void txtItem31_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            else
            {
                ((UltraMaskedEdit)sender).FormatString = "###,###,###,###,###,##0";
            }

            txtItem28.FormatNumberic((getValueMaskedEdit(txtItem31) + getValueMaskedEdit(txtItem33)), ConstDatabase.Format_TienVND);
        }


        private void txtItem26_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            else
            {
                ((UltraMaskedEdit)sender).FormatString = "###,###,###,###,###,##0";
            }

            if (txtItem27.Text.IsNullOrEmpty())
            {
                txtItem27.Text = "0";
            }
            txtItem34.FormatNumberic((getValueMaskedEdit(txtItem26) + getValueLabel(txtItem27)), ConstDatabase.Format_TienVND);
        }
        private void txtItem27_ValueChanged(object sender, EventArgs e)
        {

            if (txtItem27.Text.IsNullOrEmpty())
            {
                txtItem27.Text = "0";
            }
            txtItem34.FormatNumberic((getValueMaskedEdit(txtItem26) + getValueLabel(txtItem27)), ConstDatabase.Format_TienVND);
        }


        private void txtItem28_ValueChanged(object sender, EventArgs e)
        {
            if (txtItem28.Text.IsNullOrEmpty())
            {
                txtItem28.Text = "0";
            }
            txtItem35.FormatNumberic(getValueLabel(txtItem28), ConstDatabase.Format_TienVND);
        }
        private void txtItem24_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            else
            {
                ((UltraMaskedEdit)sender).FormatString = "###,###,###,###,###,##0";
            }

            BindData(txtItem25, getValueMaskedEdit(txtItem24));
        }
        private void txtItem25_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            else
            {
                ((UltraMaskedEdit)sender).FormatString = "###,###,###,###,###,##0";
            }

            if (txtItem35.Text.IsNullOrEmpty())
            {
                txtItem35.Text = "0";
            }
            txtItem36.FormatNumberic((getValueLabel(txtItem35) - getValueMaskedEdit(txtItem25)), ConstDatabase.Format_TienVND);
        }
        private void txtItem35_ValueChanged(object sender, EventArgs e)
        {

            if (txtItem35.Text.IsNullOrEmpty())
            {
                txtItem35.Text = "0";
            }
            txtItem36.FormatNumberic((getValueLabel(txtItem35) - getValueMaskedEdit(txtItem25)), ConstDatabase.Format_TienVND);
        }

        private void txtItem22_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";

            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            else
            {
                ((UltraMaskedEdit)sender).FormatString = "###,###,###,###,###,##0";
            }

            decimal s = getValueLabel(txtItem36) - getValueMaskedEdit(txtItem22) + getValueMaskedEdit(txtItem37) - getValueMaskedEdit(txtItem38) - getValueMaskedEdit(txtItem39);
            s.FormatNumberic(ConstDatabase.Format_TienVND);
            if (s >= 0)
                txtItem40a.FormatNumberic(s, ConstDatabase.Format_TienVND);
            else
            {
                txtItem41.FormatNumberic(s, ConstDatabase.Format_TienVND);
                txtItem41.Text = txtItem41.Text.Trim('(', ')');
            }
        }
        private void txtItem36_ValueChanged(object sender, EventArgs e)
        {
            if (txtItem36.Text.IsNullOrEmpty())
            {
                txtItem36.Text = "0";
            }

            decimal s = getValueLabel(txtItem36) - getValueMaskedEdit(txtItem22) + getValueMaskedEdit(txtItem37) - getValueMaskedEdit(txtItem38) - getValueMaskedEdit(txtItem39);
            s.FormatNumberic(ConstDatabase.Format_TienVND);
            if (s >= 0)
                txtItem40a.FormatNumberic(s, ConstDatabase.Format_TienVND);
            else
            {
                txtItem41.FormatNumberic(s, ConstDatabase.Format_TienVND);
                txtItem41.Text = txtItem41.Text.Trim('(', ')');
            }
        }

        private void txtItem40a_ValueChanged(object sender, EventArgs e)
        {

            if (txtItem40a.Text.IsNullOrEmpty())
            {
                txtItem40a.Text = "0";
            }

            txtItem40.FormatNumberic((getValueLabel(txtItem40a) - getValueMaskedEdit(txtItem40b)), ConstDatabase.Format_TienVND);
            if (!txtItem40a.Text.IsNullOrEmpty() && txtItem40a.Text != "0")
            {
                txtItem41.Text = "0";
            }
        }
        private void txtItem40b_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            else
            {
                ((UltraMaskedEdit)sender).FormatString = "###,###,###,###,###,##0";
            }
            if (txtItem40a.Text.IsNullOrEmpty())
            {
                txtItem40a.Text = "0";
            }

            txtItem40.FormatNumberic((getValueLabel(txtItem40a) - getValueMaskedEdit(txtItem40b)), ConstDatabase.Format_TienVND);
            if (!txtItem40a.Text.IsNullOrEmpty() && txtItem40a.Text != "0")
            {
                txtItem41.Text = "0";
            }
        }
        private void txtItem23_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            else
            {
                ((UltraMaskedEdit)sender).FormatString = "###,###,###,###,###,##0";
            }

        }
        private void txtItem41_ValueChanged(object sender, EventArgs e)
        {
            if (txtItem41.Text.IsNullOrEmpty())
            {
                txtItem41.Text = "0";
            }

            txtItem43.FormatNumberic((getValueLabel(txtItem41) - getValueMaskedEdit(txtItem42)), ConstDatabase.Format_TienVND);
            if (!txtItem41.Text.IsNullOrEmpty() && txtItem41.Text != "0")
            {
                txtItem40a.Text = "0";
            }
        }
        private void txtItem42_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            else
            {
                ((UltraMaskedEdit)sender).FormatString = "###,###,###,###,###,##0";
            }
            if (txtItem41.Text.IsNullOrEmpty())
            {
                txtItem41.Text = "0";
            }

            txtItem43.FormatNumberic((getValueLabel(txtItem41) - getValueMaskedEdit(txtItem42)), ConstDatabase.Format_TienVND);
            if (!txtItem41.Text.IsNullOrEmpty() && txtItem41.Text != "0")
            {
                txtItem40a.Text = "0";
            }
        }
        private void utmDetailBaseToolBar_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
        {
            switch (e.Tool.Key)
            {
                case "mnbtnBack":
                    break;

                case "mnbtnForward":
                    break;

                case "mnbtnAdd":

                    break;

                case "mnbtnEdit":
                    utmDetailBaseToolBar.Tools["mnbtnSave"].SharedProps.Enabled = true;
                    utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = false;
                    utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Enabled = true;
                    utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = false;
                    utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Enabled = false;
                    LoadReadOnly(false);
                    dataGridView1.AllowUserToResizeColumns = false;
                    dataGridView1.AllowUserToResizeRows = false;
                    dataGridView2.AllowUserToResizeColumns = false;
                    dataGridView2.AllowUserToResizeRows = false;
                    dataGridView3.AllowUserToResizeColumns = false;
                    dataGridView3.AllowUserToResizeRows = false;
                    dataGridView4.AllowUserToResizeColumns = false;
                    dataGridView4.AllowUserToResizeRows = false;
                    dataGridView5.AllowUserToResizeColumns = false;
                    dataGridView5.AllowUserToResizeRows = false;
                    dataGridView6.AllowUserToResizeColumns = false;
                    dataGridView6.AllowUserToResizeRows = false;
                    dataGridView7.AllowUserToResizeColumns = false;
                    dataGridView7.AllowUserToResizeRows = false;
                    dataGridView8.AllowUserToResizeColumns = false;
                    dataGridView8.AllowUserToResizeRows = false;
                    dataGridView9.AllowUserToResizeColumns = false;
                    dataGridView9.AllowUserToResizeRows = false;
                    if (ultraTabControl1.Tabs[1].Visible == true)
                    {
                        for (int i = 0; i < dataGridView1.RowCount; i++)
                        {
                            dataGridView1.Rows[i].Cells[0].Value = i + 1;
                        }
                        for (int i = 0; i < dataGridView2.RowCount; i++)
                        {
                            dataGridView2.Rows[i].Cells[0].Value = i + 1;
                        }
                        for (int i = 0; i < dataGridView3.RowCount; i++)
                        {
                            dataGridView3.Rows[i].Cells[0].Value = i + 1;
                        }
                        for (int i = 0; i < dataGridView4.RowCount; i++)
                        {
                            dataGridView4.Rows[i].Cells[0].Value = i + 1;
                        }

                    }
                    if (ultraTabControl1.Tabs[2].Visible == true)
                    {
                        for (int i = 0; i < dataGridView5.RowCount; i++)
                        {
                            dataGridView5.Rows[i].Cells[0].Value = i + 1;
                        }
                        for (int i = 0; i < dataGridView6.RowCount; i++)
                        {
                            dataGridView6.Rows[i].Cells[0].Value = i + 1;
                        }
                        for (int i = 0; i < dataGridView7.RowCount; i++)
                        {
                            dataGridView7.Rows[i].Cells[0].Value = i + 1;
                        }

                    }
                    if (ultraTabControl1.Tabs[3].Visible == true)
                    {
                        for (int i = 0; i < dataGridView8.RowCount; i++)
                        {
                            dataGridView8.Rows[i].Cells[0].Value = i + 1;
                        }
                        for (int i = 0; i < dataGridView9.RowCount; i++)
                        {
                            dataGridView9.Rows[i].Cells[0].Value = i + 1;
                        }

                    }
                    _isEdit = true;
                    break;

                case "mnbtnSave":
                    try
                    {
                        if (txtSignDate.Value == null)
                        {
                            MSG.Error("Bạn chưa chọn ngày ký");
                            return;
                        }
                        if (ultraTabControl1.Tabs[1].Visible == true)
                        {
                            if (!CheckError1(dataGridView1)) return;
                            string y = txtPreTaxAmountKCT.Text;
                            string z = txtItem26.Text;
                            if (!CheckError1(dataGridView2)) return;
                            if (!CheckError1(dataGridView3)) return;
                            if (!CheckError1(dataGridView4)) return;
                        }
                        if (ultraTabControl1.Tabs[2].Visible == true)
                        {
                            if (!CheckError1(dataGridView5)) return;
                            if (!CheckError1(dataGridView6)) return;
                            if (!CheckError1(dataGridView7)) return;
                        }
                        if (ultraTabControl1.Tabs[3].Visible == true)
                            if (CheckBS == false)
                                THKBS();


                        DeleteRowNull();

                        Utils.ITM01GTGTService.BeginTran();
                        if (_isEdit)
                        {
                            TM01GTGT input = new TM01GTGT();
                            input = ObjandGUI(_select, true);
                            Utils.ITM01GTGTService.Update(input);

                        }
                        else
                        {
                            TM01GTGT input = new TM01GTGT();
                            input = ObjandGUI(input, true);
                            Utils.ITM01GTGTService.CreateNew(input);
                            _select = input;
                        }

                        Utils.ITM01GTGTService.CommitTran();
                        MSG.Information("Lưu thành công");
                        Utils.ClearCacheByType<TM01GTGT>();
                        Utils.ClearCacheByType<TM01GTGTDetail>();
                        _select = Utils.ITM01GTGTService.Getbykey(_select.ID);
                        ClearData();
                        ObjandGUI(_select, false);
                        //_01_GTGT_Detail(_select, false);
                        utmDetailBaseToolBar.Tools["mnbtnSave"].SharedProps.Enabled = false;
                        utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = true;
                        utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = true;
                        utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Enabled = false;
                        utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Enabled = true;
                        //if (ultraTabControl1.Tabs[1].Visible == true)
                        //{
                        //               setAllowUserToAddRow(dataGridView1);
                        //    setAllowUserToAddRow(dataGridView2);
                        //    setAllowUserToAddRow(dataGridView3);
                        //    setAllowUserToAddRow(dataGridView4);
                        //}
                        //if (ultraTabControl1.Tabs[2].Visible == true)
                        //{
                        //    setAllowUserToAddRow(dataGridView5);
                        //    setAllowUserToAddRow(dataGridView6);
                        //    setAllowUserToAddRow(dataGridView7);
                        //}
                        //if (ultraTabControl1.Tabs[3].Visible == true)
                        //{
                        //    setAllowUserToAddRow(dataGridView8);
                        //    setAllowUserToAddRow(dataGridView9);

                        //}
                        ////DeleteRowNull();
                        // TinhTongCot5va6();
                        LoadReadOnly(true);
                        checkSave = true;
                    }
                    catch (Exception ex)
                    {
                        MSG.Error(ex.Message);
                        Utils.ITM01GTGTService.RolbackTran();
                    }

                    break;

                case "mnbtnDelete":
                    if (!(DialogResult.Yes == MessageBox.Show("Bạn có muốn xóa tờ khai này không ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question)))
                        return;
                    DateTime _datecheck = new DateTime(_select.ToDate.Year, _select.ToDate.Month, _select.ToDate.Day);
                    var _lstCheckCreate = Utils.ListGOtherVoucher.FirstOrDefault(g => g.TypeID == 602 && g.PostedDate == _datecheck);
                    var hasChild = Utils.ListTM01GTGT.Where(n => n.BranchID == _select.ID).FirstOrDefault();
                    if (_lstCheckCreate != null && _select.IsFirstDeclaration)
                    {
                        MSG.Warning("Tờ khai này đã phát sinh chứng từ nghiệp vụ khấu trừ thuế. Vui lòng xóa chứng từ khấu trừ thuế trước khi thực hiện xóa tờ khai này!");
                        return;
                    }
                    if (hasChild != null)
                    {
                        MSG.Warning("Bạn phải xóa tờ khai bổ sung trước khi xóa tờ khai này.");
                        return;
                    }
                    TM01GTGT tM01GTGT = Utils.ListTM01GTGT.FirstOrDefault(n => n.ID == _select.ID);
                    Utils.ITM01GTGTService.BeginTran();
                    Utils.ITM01GTGTService.Delete(tM01GTGT);
                    Utils.ITM01GTGTService.CommitTran();
                    MSG.Information("Xóa thành công");
                    this.Close();
                    break;

                case "mnbtnComeBack":
                    this.Close();//add by cuongpv
                    break;

                case "mnbtnPrint":
                    break;

                case "btnKBS":
                    THKBS();
                    CheckBS = true;
                    break;
                case "mnbtnClose":
                    this.Close();
                    break;
                case "btnPrint":
                    //utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = false;
                    //  FillReport();
                    #region fill dữ liệu
                    GTGT01DetailReport gTGT01DetailReport = new GTGT01DetailReport();
                    gTGT01DetailReport.Item21 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item21").Data;
                    gTGT01DetailReport.Item22 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item22").Data;
                    gTGT01DetailReport.Item23 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item23").Data;
                    gTGT01DetailReport.Item24 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item24").Data;
                    gTGT01DetailReport.Item25 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item25").Data;
                    gTGT01DetailReport.Item26 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item26").Data;
                    gTGT01DetailReport.Item27 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item27").Data;
                    gTGT01DetailReport.Item28 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item28").Data;
                    gTGT01DetailReport.Item29 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item29").Data;
                    gTGT01DetailReport.Item30 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item30").Data;
                    gTGT01DetailReport.Item31 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item31").Data;
                    gTGT01DetailReport.Item32 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item32").Data;
                    gTGT01DetailReport.Item32a = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item32a").Data;
                    gTGT01DetailReport.Item33 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item33").Data;
                    gTGT01DetailReport.Item34 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item34").Data;
                    gTGT01DetailReport.Item35 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item35").Data;
                    gTGT01DetailReport.Item36 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item36").Data;
                    gTGT01DetailReport.Item37 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item37").Data;
                    gTGT01DetailReport.Item38 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item38").Data;
                    gTGT01DetailReport.Item39 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item39").Data;
                    gTGT01DetailReport.Item40 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40").Data;
                    gTGT01DetailReport.Item40a = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40a").Data;
                    gTGT01DetailReport.Item40b = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40b").Data;
                    gTGT01DetailReport.Item41 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item41").Data;
                    gTGT01DetailReport.Item42 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item42").Data;
                    gTGT01DetailReport.Item43 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item43").Data;

                    gTGT01DetailReport.ID = _select.ID;
                    gTGT01DetailReport.BranchID = _select.BranchID;
                    gTGT01DetailReport.TypeID = _select.TypeID;
                    gTGT01DetailReport.DeclarationName = _select.DeclarationName;
                    gTGT01DetailReport.DeclarationTerm = _select.DeclarationTerm;
                    gTGT01DetailReport.FromDate = _select.FromDate;
                    gTGT01DetailReport.ToDate = _select.ToDate;
                    gTGT01DetailReport.IsFirstDeclaration = _select.IsFirstDeclaration;
                    gTGT01DetailReport.AdditionTime = _select.AdditionTime;
                    gTGT01DetailReport.AdditionDate = _select.AdditionDate;
                    gTGT01DetailReport.Career = _select.Career;
                    gTGT01DetailReport.IsExtend = _select.IsExtend;
                    gTGT01DetailReport.ExtensionCase = _select.ExtensionCase;
                    gTGT01DetailReport.IsAppendix011GTGT = _select.IsAppendix011GTGT;
                    gTGT01DetailReport.IsAppendix012GTGT = _select.IsAppendix012GTGT;
                    gTGT01DetailReport.CompanyName = _select.CompanyName;
                    gTGT01DetailReport.CompanyTaxCode = _select.CompanyTaxCode;
                    gTGT01DetailReport.TaxAgencyTaxCode = _select.CompanyTaxCode;
                    gTGT01DetailReport.TaxAgencyName = _select.TaxAgencyName;
                    gTGT01DetailReport.TaxAgencyEmployeeName = _select.TaxAgencyEmployeeName;
                    gTGT01DetailReport.CertificationNo = _select.CertificationNo;
                    gTGT01DetailReport.SignName = _select.SignName;
                    gTGT01DetailReport.SignDate = _select.SignDate;
                    gTGT01DetailReport.TM01GTGTDetails = _select.TM01GTGTDetails;

                    // var lst = Utils.ListTM01GTGTAdjust.CloneObject();

                    gTGT01DetailReport.TM01GTGTAdjusts = _select.TM01GTGTAdjusts;

                    if (gTGT01DetailReport.AdditionTime != null)
                    {
                        //gTGT01DetailReport.TM01GTGTAdjusts = lst;
                        gTGT01DetailReport.TM01GTGTAdjusts1 = gTGT01DetailReport.TM01GTGTAdjusts.Where(n => n.Type == 1).OrderBy(n => n.OrderPriority).ToList();
                        gTGT01DetailReport.TM01GTGTAdjusts2 = gTGT01DetailReport.TM01GTGTAdjusts.Where(n => n.Type == 2).OrderBy(n => n.OrderPriority).ToList();
                        gTGT01DetailReport.TM01GTGTAdjusts3 = gTGT01DetailReport.TM01GTGTAdjusts.Where(n => n.Type == 3).OrderBy(n => n.OrderPriority).ToList();
                        if (gTGT01DetailReport.TM01GTGTAdjusts1.Count == 0)
                        {
                            TM01GTGTAdjust adjust = new TM01GTGTAdjust();
                            adjust.ID = Guid.Empty;
                            adjust.TM01GTGTID = _select.ID;
                            adjust.Name = "";
                            adjust.Code = "";
                            adjust.DeclaredAmount = 0;
                            adjust.AdjustAmount = 0;
                            adjust.DifferAmount = 0;
                            adjust.Type = 1;
                            gTGT01DetailReport.TM01GTGTAdjusts.Add(adjust);
                        }
                        if (gTGT01DetailReport.TM01GTGTAdjusts3.Count == 0)
                        {
                            TM01GTGTAdjust adjust3 = new TM01GTGTAdjust();
                            adjust3.ID = Guid.Empty;
                            adjust3.TM01GTGTID = _select.ID;
                            adjust3.Name = "";
                            adjust3.Code = "";
                            adjust3.DeclaredAmount = 0;
                            adjust3.AdjustAmount = 0;
                            adjust3.DifferAmount = 0;
                            adjust3.Type = 3;
                            gTGT01DetailReport.TM01GTGTAdjusts.Add(adjust3);
                        }
                        if (gTGT01DetailReport.TM01GTGTAdjusts2.Count == 0)
                        {
                            TM01GTGTAdjust adjust2 = new TM01GTGTAdjust();
                            adjust2.ID = Guid.Empty;
                            adjust2.TM01GTGTID = _select.ID;
                            adjust2.Name = "";
                            adjust2.Code = "";
                            adjust2.DeclaredAmount = 0;
                            adjust2.AdjustAmount = 0;
                            adjust2.DifferAmount = 0;
                            adjust2.Type = 2;
                            gTGT01DetailReport.TM01GTGTAdjusts.Add(adjust2);
                        }
                    }

                    gTGT01DetailReport.TM011GTGTs = _select.TM011GTGTs;
                    gTGT01DetailReport.TM012GTGTs = _select.TM012GTGTs;



                    if (gTGT01DetailReport.IsAppendix011GTGT)
                    {
                        if (gTGT01DetailReport.TM011GTGTs != null)
                        {
                            gTGT01DetailReport.lstKCT = gTGT01DetailReport.TM011GTGTs.Where(n => n.VATRate == -1).OrderBy(n => n.OrderPriority).ToList();
                            gTGT01DetailReport.lstRate0 = gTGT01DetailReport.TM011GTGTs.Where(n => n.VATRate == 0).OrderBy(n => n.OrderPriority).ToList();
                            gTGT01DetailReport.lstRate5 = gTGT01DetailReport.TM011GTGTs.Where(n => n.VATRate == 5).OrderBy(n => n.OrderPriority).ToList();
                            gTGT01DetailReport.lstRate10 = gTGT01DetailReport.TM011GTGTs.Where(n => n.VATRate == 10).OrderBy(n => n.OrderPriority).ToList();
                        }

                        TM011GTGT item = new TM011GTGT();
                        item.ID = Guid.Empty;
                        item.TM01GTGTID = _select.ID;
                        item.VoucherID = Guid.Empty;
                        item.VoucherDetailID = Guid.Empty;
                        item.InvoiceDate = DateTime.MinValue;
                        item.InvoiceNo = "";
                        item.InvoiceSeries = "";
                        item.AccountingObjectID = Guid.Empty;
                        item.AccountingObjectName = "";
                        item.TaxCode = "";
                        item.PretaxAmount = 0;
                        item.TaxAmount = 0;
                        item.Note = "";
                        item.OrderPriority = 0;
                        item.STT = 1;
                        item.GroupName = "";

                        if (gTGT01DetailReport.lstKCT.Count == 0)
                        {
                            item.VATRate = -1;
                            gTGT01DetailReport.lstKCT.Add(item);
                        }
                        if (gTGT01DetailReport.lstRate0.Count == 0)
                        {
                            item.VATRate = 0;
                            gTGT01DetailReport.lstRate0.Add(item);
                        }
                        if (gTGT01DetailReport.lstRate5.Count == 0)
                        {
                            item.VATRate = 5;
                            gTGT01DetailReport.lstRate5.Add(item);

                        }
                        if (gTGT01DetailReport.lstRate10.Count == 0)
                        {
                            item.VATRate = 10;
                            gTGT01DetailReport.lstRate10.Add(item);
                        }
                    }

                    if (gTGT01DetailReport.IsAppendix012GTGT)
                    {
                        gTGT01DetailReport.TM012GTGTs1 = gTGT01DetailReport.TM012GTGTs.Where(n => n.Type == 1).ToList();
                        gTGT01DetailReport.TM012GTGTs2 = gTGT01DetailReport.TM012GTGTs.Where(n => n.Type == 2).ToList();
                        gTGT01DetailReport.TM012GTGTs3 = gTGT01DetailReport.TM012GTGTs.Where(n => n.Type == 3).ToList();

                        if (gTGT01DetailReport.TM012GTGTs1.Count == 0)
                        {

                            TM012GTGT item1 = new TM012GTGT();
                            item1.ID = Guid.Empty;
                            item1.TM01GTGTID = _select.ID;
                            item1.VoucherID = Guid.Empty;
                            item1.VoucherDetailID = Guid.Empty;
                            item1.InvoiceNo = "";
                            item1.InvoiceDate = DateTime.MinValue;
                            item1.InvoiceSeries = "";
                            item1.AccountingObjectID = Guid.Empty;
                            item1.AccountingObjectName = "";
                            item1.TaxCode = "";
                            item1.PretaxAmount = 0;
                            item1.TaxAmount = 0;
                            item1.Note = "";
                            item1.OrderPriority = 0;
                            item1.STT = 1;
                            item1.GroupName = "";
                            item1.Type = 1;
                            gTGT01DetailReport.TM012GTGTs1.Add(item1);
                        }
                        if (gTGT01DetailReport.TM012GTGTs2.Count == 0)
                        {
                            TM012GTGT item2 = new TM012GTGT();
                            item2.ID = Guid.Empty;
                            item2.TM01GTGTID = _select.ID;
                            item2.VoucherID = Guid.Empty;
                            item2.VoucherDetailID = Guid.Empty;
                            item2.InvoiceSeries = "";
                            item2.InvoiceNo = "";
                            item2.InvoiceDate = DateTime.MinValue;
                            item2.AccountingObjectID = Guid.Empty;
                            item2.AccountingObjectName = "";
                            item2.TaxCode = "";
                            item2.PretaxAmount = 0;
                            item2.TaxAmount = 0;
                            item2.Note = "";
                            item2.OrderPriority = 0;
                            item2.STT = 1;
                            item2.GroupName = "";
                            item2.Type = 2;
                            gTGT01DetailReport.TM012GTGTs2.Add(item2);
                        }
                        if (gTGT01DetailReport.TM012GTGTs3.Count == 0)
                        {
                            TM012GTGT item3 = new TM012GTGT();
                            item3.ID = Guid.Empty;
                            item3.TM01GTGTID = _select.ID;
                            item3.VoucherID = Guid.Empty;
                            item3.VoucherDetailID = Guid.Empty;
                            item3.InvoiceNo = "";
                            item3.InvoiceDate = DateTime.MinValue;
                            item3.InvoiceSeries = "";
                            item3.AccountingObjectID = Guid.Empty;
                            item3.AccountingObjectName = "";
                            item3.TaxCode = "";
                            item3.PretaxAmount = 0;
                            item3.TaxAmount = 0;
                            item3.Note = "";
                            item3.OrderPriority = 0;
                            item3.STT = 1;
                            item3.GroupName = "";
                            item3.Type = 3;
                            gTGT01DetailReport.TM012GTGTs3.Add(item3);
                        }
                    }


                    gTGT01DetailReport.TM012GTGTs = gTGT01DetailReport.TM012GTGTs.OrderBy(n => n.Type).ToList();
                    gTGT01DetailReport.TM01GTGTAdjusts = gTGT01DetailReport.TM01GTGTAdjusts.OrderBy(n => n.Type).ToList();
                    #endregion
                    Utils.Print("01-GTGT", gTGT01DetailReport, this);
                    break;
                case "btnExportXml":
                    ExportXml(_select);
                    break;
            }
        }
        private void chkItem21_CheckedChanged(object sender, EventArgs e)
        {
            if (chkItem21.Checked == true)
            {
                if (
                    txtItem23.Text != "" ||
                    txtItem24.Text != "" ||
                    txtItem25.Text != "" ||
                    txtItem26.Text != "" ||
                    txtItem27.Text != "0" ||
                    txtItem28.Text != "0" ||
                    txtItem29.Text != "" ||
                    txtItem30.Text != "" ||
                    txtItem31.Text != "" ||
                    txtItem32.Text != "" ||
                    txtItem32a.Text != "" ||
                    txtItem33.Text != "" ||
                    txtItem34.Text != "0" ||
                    txtItem35.Text != "0" ||
                    txtItem36.Text != "0" ||
                    txtItem37.Text != "" ||
                    txtItem38.Text != "" ||
                    txtItem39.Text != "" ||
                    txtItem40.Text != "0" ||
                    txtItem40a.Text != "0" ||
                    txtItem40b.Text != "" ||
                    txtItem41.Text != "0" ||
                    txtItem42.Text != "" ||
                    txtItem43.Text != "0"
                    )
                {
                    if (DialogResult.Yes == MessageBox.Show("Các số liệu có trong các chỉ tiêu phát sinh sẽ bị xóa bằng 0. Bạn có đồng ý?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        txtItem23.Text = "0";
                        txtItem24.Text = "0";
                        txtItem25.Text = "0";
                        txtItem26.Text = "0";
                        txtItem27.Text = "0";
                        txtItem28.Text = "0";
                        txtItem29.Text = "0";
                        txtItem30.Text = "0";
                        txtItem31.Text = "0";
                        txtItem32.Text = "0";
                        txtItem32a.Text = "0";
                        txtItem33.Text = "0";
                        txtItem34.Text = "0";
                        txtItem35.Text = "0";
                        txtItem36.Text = "0";
                        txtItem37.Text = "0";
                        txtItem38.Text = "0";
                        txtItem39.Text = "0";
                        txtItem40.Text = "0";
                        txtItem40a.Text = "0";
                        txtItem40b.Text = "0";
                        if(txtItem22.Text == "0")//add by cuongpv: fix bug SDSACC-6291
                        {
                            txtItem41.Text = "0";
                            txtItem42.Text = "0";
                            txtItem43.Text = "0";
                        }
                        LoadReadOnly1(true);
                    }
                    else
                    {
                        chkItem21.Checked = false;
                        LoadReadOnly1(false);

                    }
                }

            }
            else
            {
                if (_isEdit)
                {
                    BindData(txtItem23, decimal.Parse(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item23" && n.TM01GTGTID == _select.ID).Data));
                    BindData(txtItem24, decimal.Parse(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item24" && n.TM01GTGTID == _select.ID).Data));
                    BindData(txtItem25, decimal.Parse(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item25" && n.TM01GTGTID == _select.ID).Data));
                    BindData(txtItem26, decimal.Parse(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item26" && n.TM01GTGTID == _select.ID).Data));
                    txtItem27.FormatNumberic(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item27" && n.TM01GTGTID == _select.ID).Data, ConstDatabase.Format_TienVND);
                    txtItem28.FormatNumberic(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item28" && n.TM01GTGTID == _select.ID).Data, ConstDatabase.Format_TienVND);
                    BindData(txtItem29, decimal.Parse(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item29" && n.TM01GTGTID == _select.ID).Data));
                    BindData(txtItem30, decimal.Parse(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item30" && n.TM01GTGTID == _select.ID).Data));
                    BindData(txtItem31, decimal.Parse(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item31" && n.TM01GTGTID == _select.ID).Data));
                    BindData(txtItem32, decimal.Parse(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item32" && n.TM01GTGTID == _select.ID).Data));
                    BindData(txtItem32a, decimal.Parse(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item32a" && n.TM01GTGTID == _select.ID).Data));
                    BindData(txtItem33, decimal.Parse(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item33" && n.TM01GTGTID == _select.ID).Data));
                    txtItem34.FormatNumberic(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item34" && n.TM01GTGTID == _select.ID).Data, ConstDatabase.Format_TienVND);
                    txtItem35.FormatNumberic(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item35" && n.TM01GTGTID == _select.ID).Data, ConstDatabase.Format_TienVND);
                    txtItem36.FormatNumberic(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item36" && n.TM01GTGTID == _select.ID).Data, ConstDatabase.Format_TienVND);
                    BindData(txtItem37, decimal.Parse(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item37" && n.TM01GTGTID == _select.ID).Data));
                    BindData(txtItem38, decimal.Parse(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item38" && n.TM01GTGTID == _select.ID).Data));
                    BindData(txtItem39, decimal.Parse(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item39" && n.TM01GTGTID == _select.ID).Data));
                    txtItem40a.FormatNumberic(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40a" && n.TM01GTGTID == _select.ID).Data, ConstDatabase.Format_TienVND);
                    BindData(txtItem40b, decimal.Parse(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40b" && n.TM01GTGTID == _select.ID).Data));
                    txtItem40.FormatNumberic(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40" && n.TM01GTGTID == _select.ID).Data, ConstDatabase.Format_TienVND);
                    txtItem41.FormatNumberic(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item41" && n.TM01GTGTID == _select.ID).Data, ConstDatabase.Format_TienVND);
                    BindData(txtItem42, decimal.Parse(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item42" && n.TM01GTGTID == _select.ID).Data));
                    txtItem43.FormatNumberic(_select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item43" && n.TM01GTGTID == _select.ID).Data, ConstDatabase.Format_TienVND);
                }
                else
                {
                    BindData(txtItem23, item23);
                    BindData(txtItem24, item24);
                    BindData(txtItem25, item25);
                    txtItem26.FormatNumberic(item26, ConstDatabase.Format_TienVND);
                    txtItem27.FormatNumberic(item27, ConstDatabase.Format_TienVND);
                    txtItem28.FormatNumberic(item28, ConstDatabase.Format_TienVND);
                    BindData(txtItem29, item29);
                    BindData(txtItem30, item30);
                    BindData(txtItem31, item31);
                    BindData(txtItem32, item32);
                    BindData(txtItem32a, item32a);
                    BindData(txtItem33, item33);
                    txtItem34.FormatNumberic(item34, ConstDatabase.Format_TienVND);
                    txtItem35.FormatNumberic(item35, ConstDatabase.Format_TienVND);
                    txtItem36.FormatNumberic(item36, ConstDatabase.Format_TienVND);
                    BindData(txtItem37, item37);
                    BindData(txtItem38, item38);
                    BindData(txtItem39, item39);
                    txtItem40.FormatNumberic(item40, ConstDatabase.Format_TienVND);
                    txtItem40a.FormatNumberic(item40a, ConstDatabase.Format_TienVND);
                    BindData(txtItem40b, item40b);
                    txtItem41.FormatNumberic(item41, ConstDatabase.Format_TienVND);
                    BindData(txtItem42, item42);
                    txtItem43.FormatNumberic(item43, ConstDatabase.Format_TienVND);
                }
                LoadReadOnly1(false);
            }
        }
        #endregion
        private void setAllowUserToAddRow(DataGridView dataGridView)
        {
            if (dataGridView.RowCount > 0)
                dataGridView.AllowUserToAddRows = false;
        }
        #region hàm xóa dòng
        public void remove_row(TableLayoutPanel panel, int row_index_to_remove)
        {
            if (row_index_to_remove >= panel.RowCount)
            {
                return;
            }

            // delete all controls of row that we want to delete
            for (int i = 0; i < panel.ColumnCount; i++)
            {
                var control = panel.GetControlFromPosition(i, row_index_to_remove);
                panel.Controls.Remove(control);
            }

            // move up row controls that comes after row we want to remove
            for (int i = row_index_to_remove + 1; i < panel.RowCount; i++)
            {
                for (int j = 0; j < panel.ColumnCount; j++)
                {
                    var control = panel.GetControlFromPosition(j, i);
                    if (control != null)
                    {
                        panel.SetRow(control, i - 1);
                    }
                }
            }

            // remove last row
            //panel.RowStyles.RemoveAt(panel.RowCount - 1);
            panel.RowCount--;
        }
        #endregion

        public int GetRow(TableLayoutPanel tableLayoutPanel)
        {
            for (int t = 1; t < tableLayoutPanel.RowCount; t++)
            {
                for (int i = 0; i < tableLayoutPanel.ColumnCount; i++)
                {
                    var control = tableLayoutPanel.GetControlFromPosition(i, t);
                    if (control != null)
                        if (control.ContainsFocus)
                        {
                            return t;

                        }
                }
            }
            return 0;
        }
        private void FillNull(TableLayoutPanel tableLayoutPanel)
        {
            var control1 = tableLayoutPanel.GetControlFromPosition(1, 1);
            var control2 = tableLayoutPanel.GetControlFromPosition(2, 1);
            var control3 = tableLayoutPanel.GetControlFromPosition(3, 1);
            var control4 = tableLayoutPanel.GetControlFromPosition(4, 1);
            var control5 = tableLayoutPanel.GetControlFromPosition(5, 1);
            var control6 = tableLayoutPanel.GetControlFromPosition(6, 1);
            var control7 = tableLayoutPanel.GetControlFromPosition(7, 1);
            if (control1 is UltraTextEditor)
                control1.Text = "";
            if (control2 is UltraDateTimeEditor)
                control2.Text = null;
            if (control3 is UltraTextEditor)
                control3.Text = "";
            if (control4 is UltraTextEditor)
                control4.Text = "";
            if (control5 is UltraMaskedEdit)
            {
                UltraMaskedEdit uMask = control5 as UltraMaskedEdit;
                uMask.InputMask = "n,nnn,nnn,nnn,nnn,nnn";
                uMask.Text = "";
                uMask.NullText = "0";
            }
            if (control6 is UltraMaskedEdit)
            {
                UltraMaskedEdit uMask = control6 as UltraMaskedEdit;
                uMask.InputMask = "n,nnn,nnn,nnn,nnn,nnn";
                uMask.Text = "";
                uMask.NullText = "0";
            }
            if (control7 is UltraTextEditor)
                control7.Text = "";
        }
        #region Add control table
        private void AddControl(TableLayoutPanel tableLayoutPanel, ContextMenuStrip contextMenuStrip, UltraLabel txtPreTaxAmout, UltraLabel txtTaxAmount, int percent = 0)
        {
            int row = GetRow(tableLayoutPanel);
            if (row == 0) return;

            RowStyle temp = tableLayoutPanel.RowStyles[tableLayoutPanel.RowCount - 1];
            RowStyle rowStyle = new RowStyle(temp.SizeType, temp.Height);
            tableLayoutPanel.RowStyles.Insert(GetRow(tableLayoutPanel), rowStyle);
            tableLayoutPanel.RowCount++;

            foreach (Control ExistControl in tableLayoutPanel.Controls)
            {
                if (tableLayoutPanel.GetRow(ExistControl) > row)
                    tableLayoutPanel.SetRow(ExistControl, tableLayoutPanel.GetRow(ExistControl) + 1);
            }

            UltraTextEditor txtSTT = new UltraTextEditor();
            txtSTT.ReadOnly = true;
            txtSTT.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
            txtSTT.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            txtSTT.AutoSize = false;
            txtSTT.Dock = DockStyle.Fill;
            txtSTT.Margin = new Padding(0);

            UltraTextEditor txtInvoiceNo = new UltraTextEditor();
            txtInvoiceNo.MaxLength = 7;
            txtInvoiceNo.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
            txtInvoiceNo.AutoSize = false;
            txtInvoiceNo.Dock = DockStyle.Fill;
            txtInvoiceNo.Margin = new Padding(0);

            UltraDateTimeEditor txtInvoiceDate = new UltraDateTimeEditor();
            txtInvoiceDate.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
            txtInvoiceDate.MaskInput = "{LOC}dd/mm/yyyy";
            txtInvoiceDate.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            txtInvoiceDate.AutoSize = false;
            txtInvoiceDate.Dock = DockStyle.Fill;
            txtInvoiceDate.Margin = new Padding(0);
            txtInvoiceDate.Value = null;

            UltraTextEditor txtAccName = new UltraTextEditor();
            txtAccName.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
            txtAccName.AutoSize = false;
            txtAccName.Dock = DockStyle.Fill;
            txtAccName.Margin = new Padding(0);

            UltraTextEditor txtAccTax = new UltraTextEditor();
            txtAccTax.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
            txtAccTax.AutoSize = false;
            txtAccTax.Dock = DockStyle.Fill;
            txtAccTax.Margin = new Padding(0);

            UltraMaskedEdit txtPreTax = new UltraMaskedEdit();
            txtPreTax.NullText = "0";
            ConfigMaskedEdit(txtPreTax);

            UltraMaskedEdit txtTax = new UltraMaskedEdit();
            txtTax.NullText = "0";
            ConfigMaskedEdit(txtTax);

            UltraTextEditor txtNote = new UltraTextEditor();
            txtNote.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
            txtNote.AutoSize = false;
            txtNote.Dock = DockStyle.Fill;
            txtNote.Margin = new Padding(0);

            txtSTT.ContextMenuStrip = contextMenuStrip;
            txtInvoiceNo.ContextMenuStrip = contextMenuStrip;
            txtInvoiceDate.ContextMenuStrip = contextMenuStrip;
            txtAccName.ContextMenuStrip = contextMenuStrip;
            txtAccTax.ContextMenuStrip = contextMenuStrip;
            txtPreTax.ContextMenuStrip = contextMenuStrip;
            txtTax.ContextMenuStrip = contextMenuStrip;
            txtNote.ContextMenuStrip = contextMenuStrip;

            #region add event control
            txtTax.KeyPress += new KeyPressEventHandler(uMaskedEdit_KeyPress);
            txtPreTax.KeyPress += new KeyPressEventHandler(uMaskedEdit_KeyPress);
            txtPreTax.ValueChanged += new EventHandler((s, a) => txtPreTax_ValueChanged(s, a, tableLayoutPanel, txtPreTaxAmout, txtTax, percent));
            txtTax.ValueChanged += new EventHandler((s, a) => txtTax_ValueChanged(s, a, tableLayoutPanel, txtTaxAmount));
            txtInvoiceNo.Validated += new System.EventHandler(this.txtInvoiceNo_Validated);
            txtAccTax.Validated += new System.EventHandler(this.txtTax_Validated);
            txtInvoiceNo.KeyPress += new KeyPressEventHandler(txtInvoiceNo_KeyPress);
            #endregion

            tableLayoutPanel.Controls.Add(txtSTT, 0, row + 1);
            tableLayoutPanel.Controls.Add(txtInvoiceNo, 1, row + 1);
            tableLayoutPanel.Controls.Add(txtInvoiceDate, 2, row + 1);
            tableLayoutPanel.Controls.Add(txtAccName, 3, row + 1);
            tableLayoutPanel.Controls.Add(txtAccTax, 4, row + 1);
            tableLayoutPanel.Controls.Add(txtPreTax, 5, row + 1);
            tableLayoutPanel.Controls.Add(txtTax, 6, row + 1);
            tableLayoutPanel.Controls.Add(txtNote, 7, row + 1);
        }
        #endregion

        #region tsm grid 1
        #region Add
        private void tsmAdd_Click(object sender, EventArgs e) //thêm một dòng
        {

            try
            {
                ToolStripMenuItem ts = (ToolStripMenuItem)sender;
                ContextMenuStrip cs = (ContextMenuStrip)ts.Owner;
                if (cs.SourceControl.Name.Equals("dataGridView1") || cs.SourceControl.Name.Equals("ultraLabel171"))
                {
                    dataGridView1.Rows.Add();
                    dataGridView1.Size = new Size(dataGridView1.Width, dataGridView1.Height + 22);
                    ultraPanel51.Location = new Point(ultraPanel51.Location.X, dataGridView1.Location.Y + dataGridView1.Height);
                    ultraLabel172.Location = new Point(ultraLabel172.Location.X, ultraPanel51.Location.Y + ultraPanel51.Height);
                    dataGridView2.Location = new Point(dataGridView2.Location.X, ultraLabel172.Location.Y + ultraLabel172.Height);
                    ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                    ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                    dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                    ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                    ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                    dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                    ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                    ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                    for (int i = 0; i < dataGridView1.RowCount; i++)
                    {
                        dataGridView1.Rows[i].Cells[0].Value = i + 1;
                    }
                }
                if (cs.SourceControl.Name.Equals("dataGridView2") || cs.SourceControl.Name.Equals("ultraLabel172"))
                {
                    dataGridView2.Rows.Add();
                    dataGridView2.Size = new Size(dataGridView2.Width, dataGridView2.Height + 22);
                    ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                    ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                    dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                    ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                    ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                    dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                    ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                    ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                    for (int i = 0; i < dataGridView2.RowCount; i++)
                    {
                        dataGridView2.Rows[i].Cells[0].Value = i + 1;
                    }
                }
                if (cs.SourceControl.Name.Equals("dataGridView3") || cs.SourceControl.Name.Equals("ultraLabel173"))
                {
                    dataGridView3.Rows.Add();
                    dataGridView3.Size = new Size(dataGridView3.Width, dataGridView3.Height + 22);
                    ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                    ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                    dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                    ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                    ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                    for (int i = 0; i < dataGridView3.RowCount; i++)
                    {
                        dataGridView3.Rows[i].Cells[0].Value = i + 1;
                    }
                }
                if (cs.SourceControl.Name.Equals("dataGridView4") || cs.SourceControl.Name.Equals("ultraLabel174"))
                {
                    dataGridView4.Rows.Add();
                    dataGridView4.Size = new Size(dataGridView4.Width, dataGridView4.Height + 22);
                    ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                    ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                    for (int i = 0; i < dataGridView4.RowCount; i++)
                    {
                        dataGridView4.Rows[i].Cells[0].Value = i + 1;
                    }
                }
                if (cs.SourceControl.Name.Equals("dataGridView5") || cs.SourceControl.Name.Equals("ultraLabel149"))
                {
                    dataGridView5.Rows.Add();
                    dataGridView5.Size = new Size(dataGridView5.Width, dataGridView5.Height + 22);
                    ultraPanel55.Location = new Point(ultraPanel55.Location.X, dataGridView5.Location.Y + dataGridView5.Height);
                    ultraLabel150.Location = new Point(ultraLabel150.Location.X, ultraPanel55.Location.Y + ultraPanel55.Height);
                    dataGridView6.Location = new Point(dataGridView6.Location.X, ultraLabel150.Location.Y + ultraLabel150.Height);
                    ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                    ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                    dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                    ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                    ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);
                    for (int i = 0; i < dataGridView5.RowCount; i++)
                    {
                        dataGridView5.Rows[i].Cells[0].Value = i + 1;
                    }
                }
                if (cs.SourceControl.Name.Equals("dataGridView6") || cs.SourceControl.Name.Equals("ultraLabel150"))
                {
                    dataGridView6.Rows.Add();
                    dataGridView6.Size = new Size(dataGridView6.Width, dataGridView6.Height + 22);
                    ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                    ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                    dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                    ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                    ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);

                    for (int i = 0; i < dataGridView6.RowCount; i++)
                    {
                        dataGridView6.Rows[i].Cells[0].Value = i + 1;
                    }
                }
                if (cs.SourceControl.Name.Equals("dataGridView7") || cs.SourceControl.Name.Equals("ultraLabel175"))
                {
                    dataGridView7.Rows.Add();
                    dataGridView7.Size = new Size(dataGridView7.Width, dataGridView7.Height + 22);
                    ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                    ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);

                    for (int i = 0; i < dataGridView7.RowCount; i++)
                    {
                        dataGridView7.Rows[i].Cells[0].Value = i + 1;
                    }
                }


            }
            catch (Exception ex)
            {
                if (CheckClick == "dataGridView1")
                {
                    dataGridView1.Rows.Add();
                    dataGridView1.Size = new Size(dataGridView1.Width, dataGridView1.Height + 22);
                    ultraPanel51.Location = new Point(ultraPanel51.Location.X, dataGridView1.Location.Y + dataGridView1.Height);
                    ultraLabel172.Location = new Point(ultraLabel172.Location.X, ultraPanel51.Location.Y + ultraPanel51.Height);
                    dataGridView2.Location = new Point(dataGridView2.Location.X, ultraLabel172.Location.Y + ultraLabel172.Height);
                    ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                    ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                    dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                    ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                    ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                    dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                    ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                    ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                    for (int i = 0; i < dataGridView1.RowCount; i++)
                    {
                        dataGridView1.Rows[i].Cells[0].Value = i + 1;
                    }
                    //CheckClick = "";
                }
                else if (CheckClick == "dataGridView2")
                {
                    dataGridView2.Rows.Add();
                    dataGridView2.Size = new Size(dataGridView2.Width, dataGridView2.Height + 22);
                    ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                    ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                    dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                    ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                    ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                    dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                    ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                    ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                    for (int i = 0; i < dataGridView2.RowCount; i++)
                    {
                        dataGridView2.Rows[i].Cells[0].Value = i + 1;
                    }
                }
                else if (CheckClick == "dataGridView3")
                {
                    dataGridView3.Rows.Add();
                    dataGridView3.Size = new Size(dataGridView3.Width, dataGridView3.Height + 22);
                    ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                    ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                    dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                    ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                    ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                    for (int i = 0; i < dataGridView3.RowCount; i++)
                    {
                        dataGridView3.Rows[i].Cells[0].Value = i + 1;
                    }
                }
                else if (CheckClick == "dataGridView4")
                {
                    dataGridView4.Rows.Add();
                    dataGridView4.Size = new Size(dataGridView4.Width, dataGridView4.Height + 22);
                    ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                    ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                    for (int i = 0; i < dataGridView4.RowCount; i++)
                    {
                        dataGridView4.Rows[i].Cells[0].Value = i + 1;
                    }
                }
                else if (CheckClick == "dataGridView5")
                {
                    dataGridView5.Rows.Add();
                    dataGridView5.Size = new Size(dataGridView5.Width, dataGridView5.Height + 22);
                    ultraPanel55.Location = new Point(ultraPanel55.Location.X, dataGridView5.Location.Y + dataGridView5.Height);
                    ultraLabel150.Location = new Point(ultraLabel150.Location.X, ultraPanel55.Location.Y + ultraPanel55.Height);
                    dataGridView6.Location = new Point(dataGridView6.Location.X, ultraLabel150.Location.Y + ultraLabel150.Height);
                    ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                    ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                    dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                    ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                    ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);
                    for (int i = 0; i < dataGridView5.RowCount; i++)
                    {
                        dataGridView5.Rows[i].Cells[0].Value = i + 1;
                    }
                }
                else if (CheckClick == "dataGridView6")
                {
                    dataGridView6.Rows.Add();
                    dataGridView6.Size = new Size(dataGridView6.Width, dataGridView6.Height + 22);
                    ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                    ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                    dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                    ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                    ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);

                    for (int i = 0; i < dataGridView6.RowCount; i++)
                    {
                        dataGridView6.Rows[i].Cells[0].Value = i + 1;
                    }
                }
                else if (CheckClick == "dataGridView7")
                {
                    dataGridView7.Rows.Add();
                    dataGridView7.Size = new Size(dataGridView7.Width, dataGridView7.Height + 22);
                    ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                    ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);

                    for (int i = 0; i < dataGridView7.RowCount; i++)
                    {
                        dataGridView7.Rows[i].Cells[0].Value = i + 1;
                    }
                }

            }


        }

        #endregion
        #region Delete
        private void tsmDelete_Click(object sender, EventArgs e)//xóa 1 dòng
        {

            //string a = s.SourceControl.Name;
            try
            {
                ToolStripMenuItem t = (ToolStripMenuItem)sender;
                ContextMenuStrip s = (ContextMenuStrip)t.Owner;
                if (s.SourceControl.Name.Equals("dataGridView1"))
                {
                    if (dataGridView1.RowCount == 1)

                    {
                        if (dataGridView1.Rows[0].Cells[1].Value.IsNullOrEmpty() && dataGridView1.Rows[0].Cells[2].Value.IsNullOrEmpty() && dataGridView1.Rows[0].Cells[3].Value.IsNullOrEmpty())
                        {
                            MSG.Error("Không thể xóa");
                            return;
                        }
                    }
                    if (dataGridViewRow1_ != null)
                    {
                        dataGridView1.Rows.RemoveAt(dataGridViewRow1_.Index);
                        if (dataGridView1.RowCount == 0)
                            dataGridView1.Rows.Add();

                        if (dataGridView1.Size.Height > 22)
                        {
                            dataGridView1.Size = new Size(dataGridView1.Width, dataGridView1.Height - 22);
                            ultraPanel51.Location = new Point(ultraPanel51.Location.X, dataGridView1.Location.Y + dataGridView1.Height);
                            ultraLabel172.Location = new Point(ultraLabel172.Location.X, ultraPanel51.Location.Y + ultraPanel51.Height);
                            dataGridView2.Location = new Point(dataGridView2.Location.X, ultraLabel172.Location.Y + ultraLabel172.Height);
                            ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                            ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                            dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                            ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                            ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                            dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                        }
                        dataGridViewRow1_ = null;
                        for (int i = 0; i < dataGridView1.RowCount; i++)
                        {
                            dataGridView1.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    else
                    {
                        dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.RowCount - 1]);
                        if (dataGridView1.RowCount == 0)
                            dataGridView1.Rows.Add();

                        if (dataGridView1.Size.Height > 22)
                        {
                            dataGridView1.Size = new Size(dataGridView1.Width, dataGridView1.Height - 22);
                            ultraPanel51.Location = new Point(ultraPanel51.Location.X, dataGridView1.Location.Y + dataGridView1.Height);
                            ultraLabel172.Location = new Point(ultraLabel172.Location.X, ultraPanel51.Location.Y + ultraPanel51.Height);
                            dataGridView2.Location = new Point(dataGridView2.Location.X, ultraLabel172.Location.Y + ultraLabel172.Height);
                            ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                            ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                            dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                            ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                            ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                            dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                        }
                        for (int i = 0; i < dataGridView1.RowCount; i++)
                        {
                            dataGridView1.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    Decimal sumPreTax1 = 0;
                    decimal sumTax1 = 0;

                    for (int i = 0; i < dataGridView1.RowCount; i++)
                    {
                        if (!dataGridView1.Rows[i].Cells[5].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try
                            {
                                u = dataGridView1.Rows[i].Cells[5].Value.ToString();
                            }
                            catch
                            { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumPreTax1 += h;
                            }
                        }
                    }
                    for (int i = 0; i < dataGridView1.RowCount; i++)
                    {
                        if (!dataGridView1.Rows[i].Cells[6].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try
                            {
                                u = dataGridView1.Rows[i].Cells[6].Value.ToString();
                            }
                            catch { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumTax1 += h;
                            }
                        }
                    }


                    txtPreTaxAmountKCT.Text = Utils.FormatNumberic(sumPreTax1, ConstDatabase.Format_TienVND);
                    txtTaxAmountKCT.Text = Utils.FormatNumberic(sumTax1, ConstDatabase.Format_TienVND);

                }
                else if (s.SourceControl.Name.Equals("dataGridView2"))
                {
                    if (dataGridView2.RowCount == 1)
                    {
                        if (dataGridView2.Rows[0].Cells[1].Value.IsNullOrEmpty() && dataGridView2.Rows[0].Cells[2].Value.IsNullOrEmpty() && dataGridView2.Rows[0].Cells[3].Value.IsNullOrEmpty())
                        {
                            MSG.Error("Không thể xóa");
                            return;
                        }
                    }
                    if (dataGridViewRow2_ != null)
                    {
                        dataGridView2.Rows.Remove(dataGridViewRow2_);
                        if (dataGridView2.RowCount == 0)
                            dataGridView2.Rows.Add();

                        if (dataGridView2.Size.Height > 22)
                        {
                            dataGridView2.Size = new Size(dataGridView2.Size.Width, dataGridView2.Height - 22);
                            ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                            ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                            dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                            ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                            ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                            dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                        }
                        dataGridViewRow2_ = null;
                        for (int i = 0; i < dataGridView2.RowCount; i++)
                        {
                            dataGridView2.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    else
                    {
                        dataGridView2.Rows.Remove(dataGridView2.Rows[dataGridView2.RowCount - 1]);
                        if (dataGridView2.RowCount == 0)
                            dataGridView2.Rows.Add();

                        if (dataGridView2.Size.Height > 22)
                        {
                            dataGridView2.Size = new Size(dataGridView2.Size.Width, dataGridView2.Height - 22);
                            ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                            ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                            dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                            ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                            ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                            dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                        }
                        for (int i = 0; i < dataGridView2.RowCount; i++)
                        {
                            dataGridView2.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    Decimal sumPreTax2 = 0;
                    decimal sumTax2 = 0;

                    for (int i = 0; i < dataGridView2.RowCount; i++)
                    {
                        if (!dataGridView2.Rows[i].Cells[5].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try
                            {
                                u = dataGridView2.Rows[i].Cells[5].Value.ToString();
                            }
                            catch
                            { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumPreTax2 += h;
                            }
                        }
                    }
                    for (int i = 0; i < dataGridView2.RowCount; i++)
                    {
                        if (!dataGridView2.Rows[i].Cells[6].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try
                            {
                                u = dataGridView2.Rows[i].Cells[6].Value.ToString();
                            }
                            catch
                            { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumTax2 += h;
                            }
                        }
                    }


                    txtPreTaxAmount0.Text = Utils.FormatNumberic(sumPreTax2, ConstDatabase.Format_TienVND);
                    txtTaxAmount0.Text = Utils.FormatNumberic(sumTax2, ConstDatabase.Format_TienVND);

                }
                else if (s.SourceControl.Name.Equals("dataGridView3"))
                {
                    if (dataGridView3.RowCount == 1)
                    {
                        if (dataGridView3.Rows[0].Cells[1].Value.IsNullOrEmpty() && dataGridView3.Rows[0].Cells[2].Value.IsNullOrEmpty() && dataGridView3.Rows[0].Cells[3].Value.IsNullOrEmpty())
                        {
                            MSG.Error("Không thể xóa");
                            return;
                        }
                    }
                    if (dataGridViewRow3_ != null)
                    {
                        dataGridView3.Rows.Remove(dataGridViewRow3_);
                        if (dataGridView3.RowCount == 0)
                            dataGridView3.Rows.Add();

                        if (dataGridView3.Size.Height > 22)
                        {
                            dataGridView3.Size = new Size(dataGridView3.Size.Width, dataGridView3.Height - 22);

                            ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                            ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                            dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);

                        }
                        dataGridViewRow3_ = null;
                        for (int i = 0; i < dataGridView3.RowCount; i++)
                        {
                            dataGridView3.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    else
                    {
                        dataGridView3.Rows.Remove(dataGridView3.Rows[dataGridView3.RowCount - 1]);
                        if (dataGridView3.RowCount == 0)
                            dataGridView3.Rows.Add();

                        if (dataGridView3.Size.Height > 22)
                        {
                            dataGridView3.Size = new Size(dataGridView3.Size.Width, dataGridView3.Height - 22);

                            ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                            ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                            dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                        }
                        for (int i = 0; i < dataGridView3.RowCount; i++)
                        {
                            dataGridView3.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    Decimal sumPreTax3 = 0;
                    decimal sumTax3 = 0;

                    for (int i = 0; i < dataGridView3.RowCount; i++)
                    {
                        if (!dataGridView3.Rows[i].Cells[5].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try
                            {
                                u = dataGridView3.Rows[i].Cells[5].Value.ToString();
                            }
                            catch
                            { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumPreTax3 += h;
                            }
                        }
                    }
                    for (int i = 0; i < dataGridView3.RowCount; i++)
                    {
                        if (!dataGridView3.Rows[i].Cells[6].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try
                            {
                                u = dataGridView3.Rows[i].Cells[6].Value.ToString();
                            }
                            catch
                            { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumTax3 += h;
                            }
                        }
                    }


                    txtPreTaxAmount5.Text = Utils.FormatNumberic(sumPreTax3, ConstDatabase.Format_TienVND);
                    txtTaxAmount5.Text = Utils.FormatNumberic(sumTax3, ConstDatabase.Format_TienVND);

                }
                else if (s.SourceControl.Name.Equals("dataGridView4"))
                {
                    if (dataGridView4.RowCount == 1)
                    {
                        if (dataGridView4.Rows[0].Cells[1].Value.IsNullOrEmpty() && dataGridView4.Rows[0].Cells[2].Value.IsNullOrEmpty() && dataGridView4.Rows[0].Cells[3].Value.IsNullOrEmpty())
                        {
                            MSG.Error("Không thể xóa");
                            return;
                        }
                    }
                    if (dataGridViewRow4_ != null)
                    {
                        dataGridView4.Rows.Remove(dataGridViewRow4_);
                        if (dataGridView4.RowCount == 0)
                            dataGridView4.Rows.Add();

                        if (dataGridView4.Size.Height > 22)
                        {
                            dataGridView4.Size = new Size(dataGridView4.Size.Width, dataGridView4.Height - 22);

                            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                        }
                        dataGridViewRow4_ = null;
                        for (int i = 0; i < dataGridView4.RowCount; i++)
                        {
                            dataGridView4.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    else
                    {
                        dataGridView4.Rows.Remove(dataGridView4.Rows[dataGridView4.RowCount - 1]);
                        if (dataGridView4.RowCount == 0)
                            dataGridView4.Rows.Add();

                        if (dataGridView4.Size.Height > 22)
                        {
                            dataGridView4.Size = new Size(dataGridView4.Size.Width, dataGridView4.Height - 22);

                            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                        }
                        for (int i = 0; i < dataGridView4.RowCount; i++)
                        {
                            dataGridView4.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    Decimal sumPreTax4 = 0;
                    decimal sumTax4 = 0;

                    for (int i = 0; i < dataGridView4.RowCount; i++)
                    {
                        if (!dataGridView4.Rows[i].Cells[5].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try
                            { u = dataGridView4.Rows[i].Cells[5].Value.ToString(); }
                            catch { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumPreTax4 += h;
                            }
                        }
                    }
                    for (int i = 0; i < dataGridView4.RowCount; i++)
                    {
                        if (!dataGridView4.Rows[i].Cells[6].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try { u = dataGridView4.Rows[i].Cells[6].Value.ToString(); }
                            catch { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumTax4 += h;
                            }
                        }
                    }


                    txtPreTaxAmount10.Text = Utils.FormatNumberic(sumPreTax4, ConstDatabase.Format_TienVND);
                    txtTaxAmount10.Text = Utils.FormatNumberic(sumTax4, ConstDatabase.Format_TienVND);

                }
                else if (s.SourceControl.Name.Equals("dataGridView5"))
                {
                    if (dataGridView5.RowCount == 1)
                    {
                        if (dataGridView5.Rows[0].Cells[1].Value.IsNullOrEmpty() && dataGridView5.Rows[0].Cells[2].Value.IsNullOrEmpty() && dataGridView5.Rows[0].Cells[3].Value.IsNullOrEmpty())
                        {
                            MSG.Error("Không thể xóa");
                            return;
                        }
                    }
                    if (dataGridViewRow5_ != null)
                    {
                        dataGridView5.Rows.Remove(dataGridViewRow5_);
                        if (dataGridView5.RowCount == 0)
                            dataGridView5.Rows.Add();

                        if (dataGridView5.Size.Height > 22)
                        {
                            dataGridView5.Size = new Size(dataGridView5.Size.Width, dataGridView5.Height - 22);
                            ultraPanel55.Location = new Point(ultraPanel55.Location.X, dataGridView5.Location.Y + dataGridView5.Height);
                            ultraLabel150.Location = new Point(ultraLabel150.Location.X, ultraPanel55.Location.Y + ultraPanel55.Height);
                            dataGridView6.Location = new Point(dataGridView6.Location.X, ultraLabel150.Location.Y + ultraLabel150.Height);
                            ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                            ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                            dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                            ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                            ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);
                        }
                        dataGridViewRow5_ = null;
                        for (int i = 0; i < dataGridView5.RowCount; i++)
                        {
                            dataGridView5.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    else
                    {
                        dataGridView5.Rows.Remove(dataGridView5.Rows[dataGridView5.RowCount - 1]);
                        if (dataGridView5.RowCount == 0)
                            dataGridView5.Rows.Add();

                        if (dataGridView5.Size.Height > 22)
                        {
                            dataGridView5.Size = new Size(dataGridView5.Size.Width, dataGridView5.Height - 22);
                            ultraPanel55.Location = new Point(ultraPanel55.Location.X, dataGridView5.Location.Y + dataGridView5.Height);
                            ultraLabel150.Location = new Point(ultraLabel150.Location.X, ultraPanel55.Location.Y + ultraPanel55.Height);
                            dataGridView6.Location = new Point(dataGridView6.Location.X, ultraLabel150.Location.Y + ultraLabel150.Height);
                            ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                            ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                            dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                            ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                            ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);
                        }
                        for (int i = 0; i < dataGridView5.RowCount; i++)
                        {
                            dataGridView5.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    Decimal sumPreTax5 = 0;
                    decimal sumTax5 = 0;

                    for (int i = 0; i < dataGridView5.RowCount; i++)
                    {
                        if (!dataGridView5.Rows[i].Cells[5].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try { u = dataGridView5.Rows[i].Cells[5].Value.ToString(); }
                            catch { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumPreTax5 += h;
                            }
                        }
                    }
                    for (int i = 0; i < dataGridView5.RowCount; i++)
                    {
                        if (!dataGridView5.Rows[i].Cells[6].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try { u = dataGridView5.Rows[i].Cells[6].Value.ToString(); }
                            catch { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumTax5 += h;
                            }
                        }
                    }


                    txtPreTaxAmount1.Text = Utils.FormatNumberic(sumPreTax5, ConstDatabase.Format_TienVND);
                    txtTaxAmount1.Text = Utils.FormatNumberic(sumTax5, ConstDatabase.Format_TienVND);

                }
                else if (s.SourceControl.Name.Equals("dataGridView6"))
                {
                    if (dataGridView6.RowCount == 1)
                    {
                        if (dataGridView6.Rows[0].Cells[1].Value.IsNullOrEmpty() && dataGridView6.Rows[0].Cells[2].Value.IsNullOrEmpty() && dataGridView6.Rows[0].Cells[3].Value.IsNullOrEmpty())
                        {
                            MSG.Error("Không thể xóa");
                            return;
                        }
                    }
                    if (dataGridViewRow6_ != null)
                    {
                        dataGridView6.Rows.Remove(dataGridViewRow6_);
                        if (dataGridView6.RowCount == 0)
                            dataGridView6.Rows.Add();

                        if (dataGridView6.Size.Height > 22)
                        {
                            dataGridView6.Size = new Size(dataGridView6.Size.Width, dataGridView6.Height - 22);
                            ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                            ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                            dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                            ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                            ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);
                        }
                        dataGridViewRow6_ = null;
                        for (int i = 0; i < dataGridView6.RowCount; i++)
                        {
                            dataGridView6.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    else
                    {
                        dataGridView6.Rows.Remove(dataGridView6.Rows[dataGridView6.RowCount - 1]);
                        if (dataGridView6.RowCount == 0)
                            dataGridView6.Rows.Add();

                        if (dataGridView6.Size.Height > 22)
                        {
                            dataGridView6.Size = new Size(dataGridView6.Size.Width, dataGridView6.Height - 22);
                            ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                            ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                            dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                            ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                            ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);
                        }
                        for (int i = 0; i < dataGridView6.RowCount; i++)
                        {
                            dataGridView6.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    Decimal sumPreTax6 = 0;
                    decimal sumTax6 = 0;

                    for (int i = 0; i < dataGridView6.RowCount; i++)
                    {
                        if (!dataGridView6.Rows[i].Cells[5].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try { u = dataGridView6.Rows[i].Cells[5].Value.ToString(); }
                            catch { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumPreTax6 += h;
                            }
                        }
                    }
                    for (int i = 0; i < dataGridView6.RowCount; i++)
                    {
                        if (!dataGridView6.Rows[i].Cells[6].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try { u = dataGridView6.Rows[i].Cells[6].Value.ToString(); }
                            catch { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumTax6 += h;
                            }
                        }
                    }


                    txtPreTaxAmount2.Text = Utils.FormatNumberic(sumPreTax6, ConstDatabase.Format_TienVND);
                    txtTaxAmount2.Text = Utils.FormatNumberic(sumTax6, ConstDatabase.Format_TienVND);
                }
                else if (s.SourceControl.Name.Equals("dataGridView7"))
                {
                    if (dataGridView7.RowCount == 1)
                    {
                        if (dataGridView7.Rows[0].Cells[1].Value.IsNullOrEmpty() && dataGridView7.Rows[0].Cells[2].Value.IsNullOrEmpty() && dataGridView7.Rows[0].Cells[3].Value.IsNullOrEmpty())
                        {
                            MSG.Error("Không thể xóa");
                            return;
                        }
                    }
                    if (dataGridViewRow7_ != null)
                    {
                        dataGridView7.Rows.Remove(dataGridViewRow7_);
                        if (dataGridView7.RowCount == 0)
                            dataGridView7.Rows.Add();

                        if (dataGridView7.Size.Height > 22)
                        {
                            dataGridView7.Size = new Size(dataGridView7.Size.Width, dataGridView7.Height - 22);
                            ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                            ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);

                        }
                        dataGridViewRow7_ = null;
                        for (int i = 0; i < dataGridView7.RowCount; i++)
                        {
                            dataGridView7.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    else
                    {
                        dataGridView7.Rows.Remove(dataGridView7.Rows[dataGridView7.RowCount - 1]);
                        if (dataGridView7.RowCount == 0)
                            dataGridView7.Rows.Add();

                        if (dataGridView7.Size.Height > 22)
                        {
                            dataGridView7.Size = new Size(dataGridView7.Size.Width, dataGridView7.Height - 22);
                            ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                            ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);
                        }
                        for (int i = 0; i < dataGridView7.RowCount; i++)
                        {
                            dataGridView7.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    Decimal sumPreTax7 = 0;
                    decimal sumTax7 = 0;

                    for (int i = 0; i < dataGridView7.RowCount; i++)
                    {
                        if (!dataGridView7.Rows[i].Cells[5].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try { u = dataGridView7.Rows[i].Cells[5].Value.ToString(); }
                            catch { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumPreTax7 += h;
                            }
                        }
                    }
                    for (int i = 0; i < dataGridView7.RowCount; i++)
                    {
                        if (!dataGridView7.Rows[i].Cells[6].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try { u = dataGridView7.Rows[i].Cells[6].Value.ToString(); }
                            catch { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumTax7 += h;
                            }
                        }
                    }


                    txtPreTaxAmount3.Text = Utils.FormatNumberic(sumPreTax7, ConstDatabase.Format_TienVND);
                    txtTaxAmount3.Text = Utils.FormatNumberic(sumTax7, ConstDatabase.Format_TienVND);
                }


                //int r = tableLayoutPanel1.RowCount - 1;
                //if (r == 1)
                //{
                //    FillNull(tableLayoutPanel1);
                //    return;
                //}
                //int row = GetRow(tableLayoutPanel1);
                //if (row == 0) return;

                //remove_row(tableLayoutPanel1, row);
                //ultraPanel51.Location = new Point(ultraPanel51.Location.X, tableLayoutPanel1.Location.Y + tableLayoutPanel1.Height);
                //tableLayoutPanel2.Location = new Point(tableLayoutPanel2.Location.X, ultraPanel51.Location.Y + ultraPanel51.Height);
                //ultraPanel52.Location = new Point(ultraPanel52.Location.X, tableLayoutPanel2.Location.Y + tableLayoutPanel2.Height);
                //tableLayoutPanel3.Location = new Point(tableLayoutPanel3.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                //ultraPanel53.Location = new Point(ultraPanel53.Location.X, tableLayoutPanel3.Location.Y + tableLayoutPanel3.Height);
                //tableLayoutPanel4.Location = new Point(tableLayoutPanel4.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                //ultraPanel54.Location = new Point(ultraPanel54.Location.X, tableLayoutPanel4.Location.Y + tableLayoutPanel4.Height);
                //ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);

                //for (int i = 0; i < tableLayoutPanel1.RowCount; i++)
                //{
                //    var control = tableLayoutPanel1.GetControlFromPosition(0, i);
                //    if (control is UltraTextEditor)
                //    {
                //        UltraTextEditor ultraTextEditor = control as UltraTextEditor;
                //        ultraTextEditor.Value = i;
                //    }
                //}
                //if (row == tableLayoutPanel1.RowCount)
                //{
                //    var control1 = tableLayoutPanel1.GetControlFromPosition(1, row - 1);
                //    if (control1 is UltraTextEditor)
                //        control1.Focus();
                //}
                //else
                //{
                //    var control1 = tableLayoutPanel1.GetControlFromPosition(1, row);
                //    if (control1 is UltraTextEditor)
                //        control1.Focus();
                //}
            }
            catch
            {
                if (CheckClick == "dataGridView1")
                {
                    if (dataGridView1.RowCount == 1)

                    {
                        if (dataGridView1.Rows[0].Cells[1].Value.IsNullOrEmpty() && dataGridView1.Rows[0].Cells[2].Value.IsNullOrEmpty() && dataGridView1.Rows[0].Cells[3].Value.IsNullOrEmpty())
                        {
                            MSG.Error("Không thể xóa");
                            return;
                        }
                    }
                    if (dataGridViewRow1_ != null)
                    {
                        dataGridView1.Rows.RemoveAt(dataGridViewRow1_.Index);
                        if (dataGridView1.RowCount == 0)
                            dataGridView1.Rows.Add();

                        if (dataGridView1.Size.Height > 22)
                        {
                            dataGridView1.Size = new Size(dataGridView1.Width, dataGridView1.Height - 22);
                            ultraPanel51.Location = new Point(ultraPanel51.Location.X, dataGridView1.Location.Y + dataGridView1.Height);
                            ultraLabel172.Location = new Point(ultraLabel172.Location.X, ultraPanel51.Location.Y + ultraPanel51.Height);
                            dataGridView2.Location = new Point(dataGridView2.Location.X, ultraLabel172.Location.Y + ultraLabel172.Height);
                            ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                            ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                            dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                            ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                            ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                            dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                        }
                        dataGridViewRow1_ = null;
                        for (int i = 0; i < dataGridView1.RowCount; i++)
                        {
                            dataGridView1.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    else
                    {
                        dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.RowCount - 1]);
                        if (dataGridView1.RowCount == 0)
                            dataGridView1.Rows.Add();

                        if (dataGridView1.Size.Height > 22)
                        {
                            dataGridView1.Size = new Size(dataGridView1.Width, dataGridView1.Height - 22);
                            ultraPanel51.Location = new Point(ultraPanel51.Location.X, dataGridView1.Location.Y + dataGridView1.Height);
                            ultraLabel172.Location = new Point(ultraLabel172.Location.X, ultraPanel51.Location.Y + ultraPanel51.Height);
                            dataGridView2.Location = new Point(dataGridView2.Location.X, ultraLabel172.Location.Y + ultraLabel172.Height);
                            ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                            ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                            dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                            ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                            ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                            dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                        }
                        for (int i = 0; i < dataGridView1.RowCount; i++)
                        {
                            dataGridView1.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    Decimal sumPreTax1 = 0;
                    decimal sumTax1 = 0;

                    for (int i = 0; i < dataGridView1.RowCount; i++)
                    {
                        if (!dataGridView1.Rows[i].Cells[5].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try
                            {
                                u = dataGridView1.Rows[i].Cells[5].Value.ToString();
                            }
                            catch
                            { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumPreTax1 += h;
                            }
                        }
                    }
                    for (int i = 0; i < dataGridView1.RowCount; i++)
                    {
                        if (!dataGridView1.Rows[i].Cells[6].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try
                            {
                                u = dataGridView1.Rows[i].Cells[6].Value.ToString();
                            }
                            catch { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumTax1 += h;
                            }
                        }
                    }


                    txtPreTaxAmountKCT.Text = Utils.FormatNumberic(sumPreTax1, ConstDatabase.Format_TienVND);
                    txtTaxAmountKCT.Text = Utils.FormatNumberic(sumTax1, ConstDatabase.Format_TienVND);
                }
                else if (CheckClick == "dataGridView2")
                {
                    if (dataGridView2.RowCount == 1)
                    {
                        if (dataGridView2.Rows[0].Cells[1].Value.IsNullOrEmpty() && dataGridView2.Rows[0].Cells[2].Value.IsNullOrEmpty() && dataGridView2.Rows[0].Cells[3].Value.IsNullOrEmpty())
                        {
                            MSG.Error("Không thể xóa");
                            return;
                        }
                    }
                    if (dataGridViewRow2_ != null)
                    {
                        dataGridView2.Rows.Remove(dataGridViewRow2_);
                        if (dataGridView2.RowCount == 0)
                            dataGridView2.Rows.Add();

                        if (dataGridView2.Size.Height > 22)
                        {
                            dataGridView2.Size = new Size(dataGridView2.Size.Width, dataGridView2.Height - 22);
                            ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                            ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                            dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                            ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                            ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                            dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                        }
                        dataGridViewRow2_ = null;
                        for (int i = 0; i < dataGridView2.RowCount; i++)
                        {
                            dataGridView2.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    else
                    {
                        dataGridView2.Rows.Remove(dataGridView2.Rows[dataGridView2.RowCount - 1]);
                        if (dataGridView2.RowCount == 0)
                            dataGridView2.Rows.Add();

                        if (dataGridView2.Size.Height > 22)
                        {
                            dataGridView2.Size = new Size(dataGridView2.Size.Width, dataGridView2.Height - 22);
                            ultraPanel52.Location = new Point(ultraPanel52.Location.X, dataGridView2.Location.Y + dataGridView2.Height);
                            ultraLabel173.Location = new Point(ultraLabel173.Location.X, ultraPanel52.Location.Y + ultraPanel52.Height);
                            dataGridView3.Location = new Point(dataGridView3.Location.X, ultraLabel173.Location.Y + ultraLabel173.Height);
                            ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                            ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                            dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                        }
                        for (int i = 0; i < dataGridView2.RowCount; i++)
                        {
                            dataGridView2.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    Decimal sumPreTax2 = 0;
                    decimal sumTax2 = 0;

                    for (int i = 0; i < dataGridView2.RowCount; i++)
                    {
                        if (!dataGridView2.Rows[i].Cells[5].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try
                            {
                                u = dataGridView2.Rows[i].Cells[5].Value.ToString();
                            }
                            catch
                            { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumPreTax2 += h;
                            }
                        }
                    }
                    for (int i = 0; i < dataGridView2.RowCount; i++)
                    {
                        if (!dataGridView2.Rows[i].Cells[6].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try
                            {
                                u = dataGridView2.Rows[i].Cells[6].Value.ToString();
                            }
                            catch
                            { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumTax2 += h;
                            }
                        }
                    }


                    txtPreTaxAmount0.Text = Utils.FormatNumberic(sumPreTax2, ConstDatabase.Format_TienVND);
                    txtTaxAmount0.Text = Utils.FormatNumberic(sumTax2, ConstDatabase.Format_TienVND);
                }
                else if (CheckClick == "dataGridView3")
                {
                    if (dataGridView3.RowCount == 1)
                    {
                        if (dataGridView3.Rows[0].Cells[1].Value.IsNullOrEmpty() && dataGridView3.Rows[0].Cells[2].Value.IsNullOrEmpty() && dataGridView3.Rows[0].Cells[3].Value.IsNullOrEmpty())
                        {
                            MSG.Error("Không thể xóa");
                            return;
                        }
                    }
                    if (dataGridViewRow3_ != null)
                    {
                        dataGridView3.Rows.Remove(dataGridViewRow3_);
                        if (dataGridView3.RowCount == 0)
                            dataGridView3.Rows.Add();

                        if (dataGridView3.Size.Height > 22)
                        {
                            dataGridView3.Size = new Size(dataGridView3.Size.Width, dataGridView3.Height - 22);

                            ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                            ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                            dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);

                        }
                        dataGridViewRow3_ = null;
                        for (int i = 0; i < dataGridView3.RowCount; i++)
                        {
                            dataGridView3.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    else
                    {
                        dataGridView3.Rows.Remove(dataGridView3.Rows[dataGridView3.RowCount - 1]);
                        if (dataGridView3.RowCount == 0)
                            dataGridView3.Rows.Add();

                        if (dataGridView3.Size.Height > 22)
                        {
                            dataGridView3.Size = new Size(dataGridView3.Size.Width, dataGridView3.Height - 22);

                            ultraPanel53.Location = new Point(ultraPanel53.Location.X, dataGridView3.Location.Y + dataGridView3.Height);
                            ultraLabel174.Location = new Point(ultraLabel174.Location.X, ultraPanel53.Location.Y + ultraPanel53.Height);
                            dataGridView4.Location = new Point(dataGridView4.Location.X, ultraLabel174.Location.Y + ultraLabel174.Height);
                            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                        }
                        for (int i = 0; i < dataGridView3.RowCount; i++)
                        {
                            dataGridView3.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    Decimal sumPreTax3 = 0;
                    decimal sumTax3 = 0;

                    for (int i = 0; i < dataGridView3.RowCount; i++)
                    {
                        if (!dataGridView3.Rows[i].Cells[5].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try
                            {
                                u = dataGridView3.Rows[i].Cells[5].Value.ToString();
                            }
                            catch
                            { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumPreTax3 += h;
                            }
                        }
                    }
                    for (int i = 0; i < dataGridView3.RowCount; i++)
                    {
                        if (!dataGridView3.Rows[i].Cells[6].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try
                            {
                                u = dataGridView3.Rows[i].Cells[6].Value.ToString();
                            }
                            catch
                            { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumTax3 += h;
                            }
                        }
                    }


                    txtPreTaxAmount5.Text = Utils.FormatNumberic(sumPreTax3, ConstDatabase.Format_TienVND);
                    txtTaxAmount5.Text = Utils.FormatNumberic(sumTax3, ConstDatabase.Format_TienVND);
                }
                else if (CheckClick == "dataGridView4")
                {
                    if (dataGridView4.RowCount == 1)
                    {
                        if (dataGridView4.Rows[0].Cells[1].Value.IsNullOrEmpty() && dataGridView4.Rows[0].Cells[2].Value.IsNullOrEmpty() && dataGridView4.Rows[0].Cells[3].Value.IsNullOrEmpty())
                        {
                            MSG.Error("Không thể xóa");
                            return;
                        }
                    }
                    if (dataGridViewRow4_ != null)
                    {
                        dataGridView4.Rows.Remove(dataGridViewRow4_);
                        if (dataGridView4.RowCount == 0)
                            dataGridView4.Rows.Add();

                        if (dataGridView4.Size.Height > 22)
                        {
                            dataGridView4.Size = new Size(dataGridView4.Size.Width, dataGridView4.Height - 22);

                            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                        }
                        dataGridViewRow4_ = null;
                        for (int i = 0; i < dataGridView4.RowCount; i++)
                        {
                            dataGridView4.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    else
                    {
                        dataGridView4.Rows.Remove(dataGridView4.Rows[dataGridView4.RowCount - 1]);
                        if (dataGridView4.RowCount == 0)
                            dataGridView4.Rows.Add();

                        if (dataGridView4.Size.Height > 22)
                        {
                            dataGridView4.Size = new Size(dataGridView4.Size.Width, dataGridView4.Height - 22);

                            ultraPanel54.Location = new Point(ultraPanel54.Location.X, dataGridView4.Location.Y + dataGridView4.Height);
                            ultraPanel43.Location = new Point(ultraPanel43.Location.X, ultraPanel54.Location.Y + ultraPanel54.Height);
                        }
                        for (int i = 0; i < dataGridView4.RowCount; i++)
                        {
                            dataGridView4.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    Decimal sumPreTax4 = 0;
                    decimal sumTax4 = 0;

                    for (int i = 0; i < dataGridView4.RowCount; i++)
                    {
                        if (!dataGridView4.Rows[i].Cells[5].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try
                            { u = dataGridView4.Rows[i].Cells[5].Value.ToString(); }
                            catch { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumPreTax4 += h;
                            }
                        }
                    }
                    for (int i = 0; i < dataGridView4.RowCount; i++)
                    {
                        if (!dataGridView4.Rows[i].Cells[6].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try { u = dataGridView4.Rows[i].Cells[6].Value.ToString(); }
                            catch { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumTax4 += h;
                            }
                        }
                    }


                    txtPreTaxAmount10.Text = Utils.FormatNumberic(sumPreTax4, ConstDatabase.Format_TienVND);
                    txtTaxAmount10.Text = Utils.FormatNumberic(sumTax4, ConstDatabase.Format_TienVND);
                }
                else if (CheckClick == "dataGridView5")
                {
                    if (dataGridView5.RowCount == 1)
                    {
                        if (dataGridView5.Rows[0].Cells[1].Value.IsNullOrEmpty() && dataGridView5.Rows[0].Cells[2].Value.IsNullOrEmpty() && dataGridView5.Rows[0].Cells[3].Value.IsNullOrEmpty())
                        {
                            MSG.Error("Không thể xóa");
                            return;
                        }
                    }
                    if (dataGridViewRow5_ != null)
                    {
                        dataGridView5.Rows.Remove(dataGridViewRow5_);
                        if (dataGridView5.RowCount == 0)
                            dataGridView5.Rows.Add();

                        if (dataGridView5.Size.Height > 22)
                        {
                            dataGridView5.Size = new Size(dataGridView5.Size.Width, dataGridView5.Height - 22);
                            ultraPanel55.Location = new Point(ultraPanel55.Location.X, dataGridView5.Location.Y + dataGridView5.Height);
                            ultraLabel150.Location = new Point(ultraLabel150.Location.X, ultraPanel55.Location.Y + ultraPanel55.Height);
                            dataGridView6.Location = new Point(dataGridView6.Location.X, ultraLabel150.Location.Y + ultraLabel150.Height);
                            ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                            ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                            dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                            ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                            ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);
                        }
                        dataGridViewRow5_ = null;
                        for (int i = 0; i < dataGridView5.RowCount; i++)
                        {
                            dataGridView5.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    else
                    {
                        dataGridView5.Rows.Remove(dataGridView5.Rows[dataGridView5.RowCount - 1]);
                        if (dataGridView5.RowCount == 0)
                            dataGridView5.Rows.Add();

                        if (dataGridView5.Size.Height > 22)
                        {
                            dataGridView5.Size = new Size(dataGridView5.Size.Width, dataGridView5.Height - 22);
                            ultraPanel55.Location = new Point(ultraPanel55.Location.X, dataGridView5.Location.Y + dataGridView5.Height);
                            ultraLabel150.Location = new Point(ultraLabel150.Location.X, ultraPanel55.Location.Y + ultraPanel55.Height);
                            dataGridView6.Location = new Point(dataGridView6.Location.X, ultraLabel150.Location.Y + ultraLabel150.Height);
                            ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                            ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                            dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                            ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                            ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);
                        }
                        for (int i = 0; i < dataGridView5.RowCount; i++)
                        {
                            dataGridView5.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    Decimal sumPreTax5 = 0;
                    decimal sumTax5 = 0;

                    for (int i = 0; i < dataGridView5.RowCount; i++)
                    {
                        if (!dataGridView5.Rows[i].Cells[5].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try { u = dataGridView5.Rows[i].Cells[5].Value.ToString(); }
                            catch { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumPreTax5 += h;
                            }
                        }
                    }
                    for (int i = 0; i < dataGridView5.RowCount; i++)
                    {
                        if (!dataGridView5.Rows[i].Cells[6].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try { u = dataGridView5.Rows[i].Cells[6].Value.ToString(); }
                            catch { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumTax5 += h;
                            }
                        }
                    }


                    txtPreTaxAmount1.Text = Utils.FormatNumberic(sumPreTax5, ConstDatabase.Format_TienVND);
                    txtTaxAmount1.Text = Utils.FormatNumberic(sumTax5, ConstDatabase.Format_TienVND);
                }
                else if (CheckClick == "dataGridView6")
                {
                    if (dataGridView6.RowCount == 1)
                    {
                        if (dataGridView6.Rows[0].Cells[1].Value.IsNullOrEmpty() && dataGridView6.Rows[0].Cells[2].Value.IsNullOrEmpty() && dataGridView6.Rows[0].Cells[3].Value.IsNullOrEmpty())
                        {
                            MSG.Error("Không thể xóa");
                            return;
                        }
                    }
                    if (dataGridViewRow6_ != null)
                    {
                        dataGridView6.Rows.Remove(dataGridViewRow6_);
                        if (dataGridView6.RowCount == 0)
                            dataGridView6.Rows.Add();

                        if (dataGridView6.Size.Height > 22)
                        {
                            dataGridView6.Size = new Size(dataGridView6.Size.Width, dataGridView6.Height - 22);
                            ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                            ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                            dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                            ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                            ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);
                        }
                        dataGridViewRow6_ = null;
                        for (int i = 0; i < dataGridView6.RowCount; i++)
                        {
                            dataGridView6.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    else
                    {
                        dataGridView6.Rows.Remove(dataGridView6.Rows[dataGridView6.RowCount - 1]);
                        if (dataGridView6.RowCount == 0)
                            dataGridView6.Rows.Add();

                        if (dataGridView6.Size.Height > 22)
                        {
                            dataGridView6.Size = new Size(dataGridView6.Size.Width, dataGridView6.Height - 22);
                            ultraPanel56.Location = new Point(ultraPanel56.Location.X, dataGridView6.Location.Y + dataGridView6.Height);
                            ultraLabel175.Location = new Point(ultraLabel175.Location.X, ultraPanel56.Location.Y + ultraPanel56.Height);
                            dataGridView7.Location = new Point(dataGridView7.Location.X, ultraLabel175.Location.Y + ultraLabel175.Height);
                            ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                            ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);
                        }
                        for (int i = 0; i < dataGridView6.RowCount; i++)
                        {
                            dataGridView6.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    Decimal sumPreTax6 = 0;
                    decimal sumTax6 = 0;

                    for (int i = 0; i < dataGridView6.RowCount; i++)
                    {
                        if (!dataGridView6.Rows[i].Cells[5].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try { u = dataGridView6.Rows[i].Cells[5].Value.ToString(); }
                            catch { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumPreTax6 += h;
                            }
                        }
                    }
                    for (int i = 0; i < dataGridView6.RowCount; i++)
                    {
                        if (!dataGridView6.Rows[i].Cells[6].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try { u = dataGridView6.Rows[i].Cells[6].Value.ToString(); }
                            catch { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumTax6 += h;
                            }
                        }
                    }


                    txtPreTaxAmount2.Text = Utils.FormatNumberic(sumPreTax6, ConstDatabase.Format_TienVND);
                    txtTaxAmount2.Text = Utils.FormatNumberic(sumTax6, ConstDatabase.Format_TienVND);
                }
                else if (CheckClick == "dataGridView7")
                {
                    if (dataGridView7.RowCount == 1)
                    {
                        if (dataGridView7.Rows[0].Cells[1].Value.IsNullOrEmpty() && dataGridView7.Rows[0].Cells[2].Value.IsNullOrEmpty() && dataGridView7.Rows[0].Cells[3].Value.IsNullOrEmpty())
                        {
                            MSG.Error("Không thể xóa");
                            return;
                        }
                    }
                    if (dataGridViewRow7_ != null)
                    {
                        dataGridView7.Rows.Remove(dataGridViewRow7_);
                        if (dataGridView7.RowCount == 0)
                            dataGridView7.Rows.Add();

                        if (dataGridView7.Size.Height > 22)
                        {
                            dataGridView7.Size = new Size(dataGridView7.Size.Width, dataGridView7.Height - 22);
                            ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                            ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);

                        }
                        dataGridViewRow7_ = null;
                        for (int i = 0; i < dataGridView7.RowCount; i++)
                        {
                            dataGridView7.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    else
                    {
                        dataGridView7.Rows.Remove(dataGridView7.Rows[dataGridView7.RowCount - 1]);
                        if (dataGridView7.RowCount == 0)
                            dataGridView7.Rows.Add();

                        if (dataGridView7.Size.Height > 22)
                        {
                            dataGridView7.Size = new Size(dataGridView7.Size.Width, dataGridView7.Height - 22);
                            ultraPanel57.Location = new Point(ultraPanel57.Location.X, dataGridView7.Location.Y + dataGridView7.Height);
                            ultraPanel44.Location = new Point(ultraPanel44.Location.X, ultraPanel57.Location.Y + ultraPanel57.Height);
                        }
                        for (int i = 0; i < dataGridView7.RowCount; i++)
                        {
                            dataGridView7.Rows[i].Cells[0].Value = i + 1;
                        }
                    }
                    Decimal sumPreTax7 = 0;
                    decimal sumTax7 = 0;

                    for (int i = 0; i < dataGridView7.RowCount; i++)
                    {
                        if (!dataGridView7.Rows[i].Cells[5].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try { u = dataGridView7.Rows[i].Cells[5].Value.ToString(); }
                            catch { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumPreTax7 += h;
                            }
                        }
                    }
                    for (int i = 0; i < dataGridView7.RowCount; i++)
                    {
                        if (!dataGridView7.Rows[i].Cells[6].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try { u = dataGridView7.Rows[i].Cells[6].Value.ToString(); }
                            catch { }
                            if (!u.IsNullOrEmpty())
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumTax7 += h;
                            }
                        }
                    }


                    txtPreTaxAmount3.Text = Utils.FormatNumberic(sumPreTax7, ConstDatabase.Format_TienVND);
                    txtTaxAmount3.Text = Utils.FormatNumberic(sumTax7, ConstDatabase.Format_TienVND);
                }
            }

        }
        #endregion
        #endregion



        #region set read only sau khi save
        private void LoadReadOnly(bool isReadOnly)
        {
            utmDetailBaseToolBar.Tools["mnbtnDelete"].SharedProps.Enabled = isReadOnly;
            chkExtend.Enabled = !isReadOnly;
            chkItem21.Enabled = !isReadOnly;
            txtItem22.ReadOnly = isReadOnly;
            txtItem23.ReadOnly = isReadOnly;
            txtItem24.ReadOnly = isReadOnly;
            txtItem25.ReadOnly = isReadOnly;
            txtItem29.ReadOnly = isReadOnly;
            txtItem30.ReadOnly = isReadOnly;
            txtItem31.ReadOnly = isReadOnly;
            txtItem32.ReadOnly = isReadOnly;
            txtItem32a.ReadOnly = isReadOnly;
            txtItem33.ReadOnly = isReadOnly;
            txtItem37.ReadOnly = isReadOnly;
            txtItem38.ReadOnly = isReadOnly;
            txtItem39.ReadOnly = isReadOnly;
            txtItem40b.ReadOnly = isReadOnly;
            txtItem42.ReadOnly = isReadOnly;
            txtName.ReadOnly = isReadOnly;
            txtCertificationNo.ReadOnly = isReadOnly;
            txtSignName.ReadOnly = isReadOnly;
            txtSignDate.ReadOnly = isReadOnly;
            tsmAdd.Visible = !isReadOnly;
            tsmDelete.Visible = !isReadOnly;
            cbbExtensionCase.ReadOnly = isReadOnly;//trungnq sửa combo thuế sai datasource và sai readonly
            if (!chkExtend.Checked)
            { cbbExtensionCase.ReadOnly = true; }
            

            foreach (var item in GetAll(this, typeof(UltraMaskedEdit)))
            {
                try
                {
                    if (item is UltraMaskedEdit)
                    {
                        (item as UltraMaskedEdit).ReadOnly = isReadOnly;

                    }
                }
                catch (Exception ex)
                {

                }


            }
            foreach (var item in GetAll(this, typeof(UltraDateTimeEditor)))
            {
                try
                {
                    if (item is UltraDateTimeEditor)
                    {
                        (item as UltraDateTimeEditor).ReadOnly = isReadOnly;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            foreach (var item in GetAll(this, typeof(UltraTextEditor)))
            {
                try
                {
                    if (item is UltraTextEditor)
                    {
                        (item as UltraTextEditor).ReadOnly = isReadOnly;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            dataGridView1.ReadOnly = isReadOnly;
            dataGridView2.ReadOnly = isReadOnly;
            dataGridView3.ReadOnly = isReadOnly;
            dataGridView4.ReadOnly = isReadOnly;
            dataGridView5.ReadOnly = isReadOnly;
            dataGridView6.ReadOnly = isReadOnly;
            dataGridView7.ReadOnly = isReadOnly;


            //for (int i = 0; i < tableLayoutPanel1.RowCount; i++)
            //{
            //    var control = tableLayoutPanel1.GetControlFromPosition(0, i);
            //    if (control is UltraTextEditor)
            //    {
            //        UltraTextEditor ultraTextEditor = control as UltraTextEditor;
            //        ultraTextEditor.ReadOnly = true;
            //    }
            //}
            //for (int i = 0; i < tableLayoutPanel2.RowCount; i++)
            //{
            //    var control = tableLayoutPanel2.GetControlFromPosition(0, i);
            //    if (control is UltraTextEditor)
            //    {
            //        UltraTextEditor ultraTextEditor = control as UltraTextEditor;
            //        ultraTextEditor.ReadOnly = true;
            //    }
            //}
            //for (int i = 0; i < tableLayoutPanel3.RowCount; i++)
            //{
            //    var control = tableLayoutPanel3.GetControlFromPosition(0, i);
            //    if (control is UltraTextEditor)
            //    {
            //        UltraTextEditor ultraTextEditor = control as UltraTextEditor;
            //        ultraTextEditor.ReadOnly = true;
            //    }
            //}
            //for (int i = 0; i < tableLayoutPanel4.RowCount; i++)
            //{
            //    var control = tableLayoutPanel4.GetControlFromPosition(0, i);
            //    if (control is UltraTextEditor)
            //    {
            //        UltraTextEditor ultraTextEditor = control as UltraTextEditor;
            //        ultraTextEditor.ReadOnly = true;
            //    }
            //}
            //for (int i = 0; i < tableLayoutPanel5.RowCount; i++)
            //{
            //    var control = tableLayoutPanel5.GetControlFromPosition(0, i);
            //    if (control is UltraTextEditor)
            //    {
            //        UltraTextEditor ultraTextEditor = control as UltraTextEditor;
            //        ultraTextEditor.ReadOnly = true;
            //    }
            //}
            //for (int i = 0; i < tableLayoutPanel6.RowCount; i++)
            //{
            //    var control = tableLayoutPanel6.GetControlFromPosition(0, i);
            //    if (control is UltraTextEditor)
            //    {
            //        UltraTextEditor ultraTextEditor = control as UltraTextEditor;
            //        ultraTextEditor.ReadOnly = true;
            //    }
            //}
            //for (int i = 0; i < tableLayoutPanel7.RowCount; i++)
            //{
            //    var control = tableLayoutPanel7.GetControlFromPosition(0, i);
            //    if (control is UltraTextEditor)
            //    {
            //        UltraTextEditor ultraTextEditor = control as UltraTextEditor;
            //        ultraTextEditor.ReadOnly = true;
            //    }
            //}

        }
        #endregion

        #region set read only sau khi chọn chỉ tiêu đầu tiên
        private void LoadReadOnly1(bool isReadOnly)
        {
            txtItem22.ReadOnly = isReadOnly;
            txtItem23.ReadOnly = isReadOnly;
            txtItem24.ReadOnly = isReadOnly;
            txtItem25.ReadOnly = isReadOnly;
            txtItem29.ReadOnly = isReadOnly;
            txtItem30.ReadOnly = isReadOnly;
            txtItem31.ReadOnly = isReadOnly;
            txtItem32.ReadOnly = isReadOnly;
            txtItem32a.ReadOnly = isReadOnly;
            txtItem33.ReadOnly = isReadOnly;
            txtItem37.ReadOnly = isReadOnly;
            txtItem38.ReadOnly = isReadOnly;
            txtItem39.ReadOnly = isReadOnly;
            txtItem40b.ReadOnly = isReadOnly;
            txtItem42.ReadOnly = isReadOnly;

        }

        #endregion

        #region chỉ tiêu nào thay đổi thì fill sang tab bổ sung
        public List<TM01GTGTAdjust> CheckChangeBS()
        {
            lst1 = new List<TM01GTGTAdjust>();
            lst2 = new List<TM01GTGTAdjust>();
            if (txtAdditionTime.Text == "1")
            {

                {
                    TM01GTGT _tm01GTGT = Utils.ListTM01GTGT.Where(n => n.FromDate == FromDate && n.AdditionTime == null).FirstOrDefault();
                    if (_tm01GTGT.TM01GTGTDetails != null)
                    {
                        foreach (var item in _tm01GTGT.TM01GTGTDetails)
                        {
                            if (item.Code == "Item25")
                            {
                                if (txtItem25.Value.ToString() != item.Data)
                                {
                                    var s1 = getValueMaskedEdit(txtItem25) - decimal.Parse(item.Data);
                                    if (s1 != 0M)
                                    {
                                        if (s1 < 0M)
                                        {
                                            lst1.Add(new TM01GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "25",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem25),
                                                DifferAmount = s1,
                                                Type = 1

                                            });


                                        }
                                        else
                                        {
                                            lst2.Add(new TM01GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "25",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem25),
                                                DifferAmount = s1,
                                                Type = 2
                                            });
                                        }
                                    }
                                }
                            }
                            if (item.Code == "Item22")
                            {
                                if (txtItem22.Value.ToString() != item.Data)
                                {
                                    var s1 = getValueMaskedEdit(txtItem22) - decimal.Parse(item.Data);
                                    if (s1 != 0M)
                                    {
                                        if (s1 < 0M)
                                        {
                                            lst1.Add(new TM01GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "22",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem22),
                                                DifferAmount = s1,
                                                Type = 1
                                            });

                                        }
                                        else
                                        {
                                            lst2.Add(new TM01GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "22",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem22),
                                                DifferAmount = s1,
                                                Type = 2
                                            });
                                        }
                                    }
                                }
                            }
                            if (item.Code == "Item33")
                            {
                                if (txtItem33.Value.ToString() != item.Data)
                                {
                                    var s1 = getValueMaskedEdit(txtItem33) - decimal.Parse(item.Data);
                                    if (s1 != 0M)
                                    {
                                        if (s1 < 0M)
                                        {
                                            lst2.Add(new TM01GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "33",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem33),
                                                DifferAmount = s1,
                                                Type = 2
                                            });

                                        }
                                        else
                                        {
                                            lst1.Add(new TM01GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "33",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem33),
                                                DifferAmount = s1,
                                                Type = 1
                                            });

                                        }
                                    }
                                }
                            }
                            if (item.Code == "Item31")
                            {
                                if (txtItem31.Value.ToString() != item.Data)
                                {
                                    var s1 = getValueMaskedEdit(txtItem31) - decimal.Parse(item.Data);
                                    if (s1 != 0M)
                                    {
                                        if (s1 < 0M)
                                        {
                                            lst2.Add(new TM01GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "31",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem31),
                                                DifferAmount = s1,
                                                Type = 2
                                            });

                                        }
                                        else
                                        {
                                            lst1.Add(new TM01GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "31",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem31),
                                                DifferAmount = s1,
                                                Type = 1
                                            });

                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }

            else
            {
                {
                    TM01GTGT _tm01GTGT = Utils.ListTM01GTGT.Where(n => n.FromDate == FromDate && n.AdditionTime == txtAdditionTime.Text.ToInt() - 1).FirstOrDefault();
                    if (_tm01GTGT.TM01GTGTDetails != null)
                    {
                        foreach (var item in _tm01GTGT.TM01GTGTDetails)
                        {
                            if (item.Code == "Item25")
                            {
                                if (txtItem25.Value.ToString() != item.Data)
                                {
                                    var s1 = getValueMaskedEdit(txtItem25) - decimal.Parse(item.Data);
                                    if (s1 != 0M)
                                    {
                                        if (s1 < 0M)
                                        {
                                            lst1.Add(new TM01GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "25",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem25),
                                                DifferAmount = s1,
                                                Type = 1

                                            });


                                        }
                                        else
                                        {
                                            lst2.Add(new TM01GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "25",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem25),
                                                DifferAmount = s1,
                                                Type = 2
                                            });
                                        }
                                    }
                                }
                            }
                            if (item.Code == "Item22")
                            {
                                if (txtItem22.Value.ToString() != item.Data)
                                {
                                    var s1 = getValueMaskedEdit(txtItem22) - decimal.Parse(item.Data);
                                    if (s1 != 0M)
                                    {
                                        if (s1 < 0M)
                                        {
                                            lst1.Add(new TM01GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "22",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem22),
                                                DifferAmount = s1,
                                                Type = 1
                                            });

                                        }
                                        else
                                        {
                                            lst2.Add(new TM01GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "22",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem22),
                                                DifferAmount = s1,
                                                Type = 2
                                            });
                                        }
                                    }
                                }
                            }
                            if (item.Code == "Item33")
                            {
                                if (txtItem33.Value.ToString() != item.Data)
                                {
                                    var s1 = getValueMaskedEdit(txtItem33) - decimal.Parse(item.Data);
                                    if (s1 != 0M)
                                    {
                                        if (s1 < 0M)
                                        {
                                            lst2.Add(new TM01GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "33",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem33),
                                                DifferAmount = s1,
                                                Type = 2
                                            });

                                        }
                                        else
                                        {
                                            lst1.Add(new TM01GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "33",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem33),
                                                DifferAmount = s1,
                                                Type = 1
                                            });

                                        }
                                    }
                                }
                            }
                            if (item.Code == "Item31")
                            {
                                if (txtItem31.Value.ToString() != item.Data)
                                {
                                    var s1 = getValueMaskedEdit(txtItem31) - decimal.Parse(item.Data);
                                    if (s1 != 0M)
                                    {
                                        if (s1 < 0M)
                                        {
                                            lst2.Add(new TM01GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "31",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem31),
                                                DifferAmount = s1,
                                                Type = 2
                                            });

                                        }
                                        else
                                        {
                                            lst1.Add(new TM01GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "31",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem31),
                                                DifferAmount = s1,
                                                Type = 1
                                            });

                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
            return lst1;
            return lst2;

        }

        #endregion

        #region event cho control được sinh bằng csm
        private void txtPreTax_ValueChanged(object sender, EventArgs e, TableLayoutPanel tableLayoutPanel, UltraLabel txtPreTaxAmount, UltraMaskedEdit txtTax = null, int percent = 0)
        {
            var txtPreTax = (UltraMaskedEdit)sender;
            if (getValueMaskedEdit(txtPreTax) == 0)
            {
                txtPreTax.InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit(txtPreTax) < 0)
            {
                txtPreTax.FormatString = "(###,###,###,###,###,##0)";
            }
            else
            {
                ((UltraMaskedEdit)sender).FormatString = "###,###,###,###,###,##0";
            }
            decimal sum1 = 0;
            for (int t = 1; t < tableLayoutPanel.RowCount; t++)
            {
                var control = tableLayoutPanel.GetControlFromPosition(5, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    if (ultraTextEditor.Text != "")
                    {
                        sum1 += getValueMaskedEdit(ultraTextEditor);
                    }
                }
            }
            txtPreTaxAmount.FormatNumberic(sum1, ConstDatabase.Format_TienVND);
            BindData(txtTax, Math.Round((getValueMaskedEdit(txtPreTax) * percent) / 100));
        }
        private void txtTax_ValueChanged(object sender, EventArgs e, TableLayoutPanel tableLayoutPanel, UltraLabel txtTaxAmount)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            else
            {
                ((UltraMaskedEdit)sender).FormatString = "###,###,###,###,###,##0";
            }
            decimal sum1 = 0;
            for (int t = 1; t < tableLayoutPanel.RowCount; t++)
            {
                var control = tableLayoutPanel.GetControlFromPosition(6, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    if (ultraTextEditor.Text != "")
                    {
                        sum1 += getValueMaskedEdit(ultraTextEditor);
                    }
                }
            }
            txtTaxAmount.FormatNumberic(sum1, ConstDatabase.Format_TienVND);
        }
        #endregion
        #region Clear Data
        private void ClearData()
        {
            //txtItem22.Text = "0";
            //txtItem23.Text = "0";
            //txtItem24.Text = "0";
            //txtItem25.Text = "0";
            //txtItem26.Text = "0";

            //txtItem27.Text = "0";
            //txtItem28.Text = "0";
            //txtItem29.Text = "0";
            //txtItem30.Text = "0";
            //txtItem31.Text = "0";
            //txtItem32.Text = "0";
            //txtItem32a.Text = "0";
            //txtItem33.Text = "0";
            //txtItem34.Text = "0";
            //txtItem36.Text = "0";
            //txtItem37.Text = "0";
            //txtItem38.Text = "0";
            //txtItem39.Text = "0";
            //txtItem40.Text = "0";
            //txtItem40a.Text = "0";
            //txtItem40b.Text = "0";
            //txtItem41.Text = "0";
            //txtItem42.Text = "0";
            //txtItem43.Text = "0";
            if (ultraTabControl1.Tabs[1].Visible == true)
            {
                dataGridView1.Rows.Clear();
                dataGridView2.Rows.Clear();
                dataGridView3.Rows.Clear();
                dataGridView4.Rows.Clear();
                txtPreTaxAmountKCT.Text = "0";
                txtPreTaxAmount0.Text = "0";
                txtPreTaxAmount5.Text = "0";
                txtPreTaxAmount10.Text = "0";
                txtTaxAmountKCT.Text = "0";
                txtTaxAmount0.Text = "0";
                txtTaxAmount5.Text = "0";
                txtTaxAmount10.Text = "0";
                txtTotalRevenueSale1.Text = "0";
                txtTotalVATAmountSale.Text = "0";
            }
            if (ultraTabControl1.Tabs[2].Visible == true)
            {
                dataGridView5.Rows.Clear();
                dataGridView6.Rows.Clear();
                dataGridView7.Rows.Clear();
                txtPreTaxAmount1.Text = "0";
                txtTaxAmount1.Text = "0";
                txtPreTaxAmount2.Text = "0";
                txtTaxAmount2.Text = "0";
                txtPreTaxAmount3.Text = "0";
                txtTaxAmount3.Text = "0";
                txtDiscountPurchase.Text = "0";
                txtDiscountTaxAmount1.Text = "0";

            }
            if (ultraTabControl1.Tabs[3].Visible == true)
            {
                dataGridView8.Rows.Clear();
                dataGridView9.Rows.Clear();
                lblDeclaredTaxAmount.Text = "0";
                lblAdjustTaxAmount.Text = "0";
                lblDifTaxAmount.Text = "0";
                lblDeclaredDiscountAmount.Text = "0";
                lblAdjustDiscountAmount.Text = "0";
                lblDifDiscountAmount.Text = "0";
                txtDateDelay.Value = 0;
                txtLateAmount.Text = "0";
                txtExplainAmount.Text = "0";
                txtCommandNo.Text = "";
                dteDate.Value = null;
                txtTaxCompanyName.Text = "";
                txtTaxCompanyDecisionName.Text = "";
                txtReceiveDays.Value = 0;
                txtExplainLateAmount.Text = "0";
                txtReason.Text = "";

            }

        }
        #endregion

        #region event PL 1
        #region Tổng giá trị chưa thuế PL 1

        private void txtPreTaxAmount0_TextChanged(object sender, EventArgs e)
        {
            txtTotalRevenueSale1.FormatNumberic((getValueLabel(txtPreTaxAmountKCT) + getValueLabel(txtPreTaxAmount0) + getValueLabel(txtPreTaxAmount5) + getValueLabel(txtPreTaxAmount10)), ConstDatabase.Format_TienVND);
            BindData(txtItem29, getValueLabel(txtPreTaxAmount0));
        }

        private void txtPreTaxAmount5_TextChanged(object sender, EventArgs e)
        {
            txtTotalRevenueSale1.FormatNumberic((getValueLabel(txtPreTaxAmountKCT) + getValueLabel(txtPreTaxAmount0) + getValueLabel(txtPreTaxAmount5) + getValueLabel(txtPreTaxAmount10)), ConstDatabase.Format_TienVND);
            BindData(txtItem30, getValueLabel(txtPreTaxAmount5));
        }

        private void txtPreTaxAmount10_TextChanged(object sender, EventArgs e)
        {
            txtTotalRevenueSale1.FormatNumberic((getValueLabel(txtPreTaxAmountKCT) + getValueLabel(txtPreTaxAmount0) + getValueLabel(txtPreTaxAmount5) + getValueLabel(txtPreTaxAmount10)), ConstDatabase.Format_TienVND);
            BindData(txtItem32, getValueLabel(txtPreTaxAmount10));
        }
        #endregion

        #region Tổng giá trị thuế GTGT PL 1
        private void txtTaxAmount5_TextChanged(object sender, EventArgs e)
        {
            txtTotalVATAmountSale.FormatNumberic((getValueLabel(txtTaxAmountKCT) + getValueLabel(txtTaxAmount0) + getValueLabel(txtTaxAmount5) + getValueLabel(txtTaxAmount10)), ConstDatabase.Format_TienVND);
            if (ultraTabControl1.Tabs[1].Visible == true)
                BindData(txtItem31, getValueLabel(txtTaxAmount5));

        }

        private void txtTaxAmount10_TextChanged(object sender, EventArgs e)
        {
            txtTotalVATAmountSale.FormatNumberic((getValueLabel(txtTaxAmountKCT) + getValueLabel(txtTaxAmount0) + getValueLabel(txtTaxAmount5) + getValueLabel(txtTaxAmount10)), ConstDatabase.Format_TienVND);
            if (ultraTabControl1.Tabs[1].Visible == true)
                BindData(txtItem33, getValueLabel(txtTaxAmount10));

        }
        #endregion

        #region event cột 6 PL 1
        private void txtPreTax1_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            else
            {
                ((UltraMaskedEdit)sender).FormatString = "###,###,###,###,###,##0";
            }
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel1.RowCount; t++)
            //{
            //    var control = tableLayoutPanel1.GetControlFromPosition(5, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtPreTaxAmountKCT.FormatNumberic(sum1, ConstDatabase.Format_TienVND);

        }
        private void txtPreTax2_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel2.RowCount; t++)
            //{
            //    var control = tableLayoutPanel2.GetControlFromPosition(5, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtPreTaxAmount0.FormatNumberic(sum1, ConstDatabase.Format_TienVND);
        }

        private void txtPreTax3_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel3.RowCount; t++)
            //{
            //    var control = tableLayoutPanel3.GetControlFromPosition(5, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtPreTaxAmount5.FormatNumberic(sum1, ConstDatabase.Format_TienVND);
            // BindData(txtTax3, Math.Round((getValueMaskedEdit(txtPreTax3) * 5) / 100));
        }

        private void txtPreTax4_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel4.RowCount; t++)
            //{
            //    var control = tableLayoutPanel4.GetControlFromPosition(5, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtPreTaxAmount10.FormatNumberic(sum1, ConstDatabase.Format_TienVND);
            // BindData(txtTax4, Math.Round((getValueMaskedEdit(txtPreTax4) * 10) / 100));
        }
        #endregion

        #region event cột 7 PL 1
        private void txtTax1_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            else
            {
                ((UltraMaskedEdit)sender).FormatString = "###,###,###,###,###,##0";
            }
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel1.RowCount; t++)
            //{
            //    var control = tableLayoutPanel1.GetControlFromPosition(6, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtTaxAmountKCT.FormatNumberic(sum1, ConstDatabase.Format_TienVND);
        }

        private void txtTax2_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel2.RowCount; t++)
            //{
            //    var control = tableLayoutPanel2.GetControlFromPosition(6, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtTaxAmount0.FormatNumberic(sum1, ConstDatabase.Format_TienVND);
        }

        private void txtTax3_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel3.RowCount; t++)
            //{
            //    var control = tableLayoutPanel3.GetControlFromPosition(6, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtTaxAmount5.FormatNumberic(sum1, ConstDatabase.Format_TienVND);
        }

        private void txtTax4_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel4.RowCount; t++)
            //{
            //    var control = tableLayoutPanel4.GetControlFromPosition(6, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtTaxAmount10.FormatNumberic(sum1, ConstDatabase.Format_TienVND);
        }
        #endregion
        #endregion

        #region event PL 2
        #region event cột 6 PL 2
        private void txtPreTax5_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            else
            {
                ((UltraMaskedEdit)sender).FormatString = "###,###,###,###,###,##0";
            }
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel5.RowCount; t++)
            //{
            //    var control = tableLayoutPanel5.GetControlFromPosition(5, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtPreTaxAmount1.FormatNumberic(sum1, ConstDatabase.Format_TienVND);
        }

        private void txtPreTax6_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel6.RowCount; t++)
            //{
            //    var control = tableLayoutPanel6.GetControlFromPosition(5, t);

            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtPreTaxAmount2.FormatNumberic(sum1, ConstDatabase.Format_TienVND);
        }

        private void txtPreTax7_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel7.RowCount; t++)
            //{
            //    var control = tableLayoutPanel7.GetControlFromPosition(5, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtPreTaxAmount3.FormatNumberic(sum1, ConstDatabase.Format_TienVND);
        }
        #endregion

        #region event cột 7 PL 2
        private void txtTax5_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            else
            {
                ((UltraMaskedEdit)sender).FormatString = "###,###,###,###,###,##0";
            }
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel5.RowCount; t++)
            //{
            //    var control = tableLayoutPanel5.GetControlFromPosition(6, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtTaxAmount1.FormatNumberic(sum1, ConstDatabase.Format_TienVND);
        }

        private void txtTax6_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel6.RowCount; t++)
            //{
            //    var control = tableLayoutPanel6.GetControlFromPosition(6, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtTaxAmount2.FormatNumberic(sum1, ConstDatabase.Format_TienVND);
        }

        private void txtTax7_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            else if (getValueMaskedEdit((UltraMaskedEdit)sender) < 0)
            {
                ((UltraMaskedEdit)sender).FormatString = "(###,###,###,###,###,##0)";
            }
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel7.RowCount; t++)
            //{
            //    var control = tableLayoutPanel7.GetControlFromPosition(6, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtTaxAmount3.FormatNumberic(sum1, ConstDatabase.Format_TienVND);
        }
        #endregion

        #region tổng giá trị chưa thế PL 2
        private void txtPreTaxAmount1_TextChanged(object sender, EventArgs e)
        {
            txtDiscountPurchase.FormatNumberic((getValueLabel(txtPreTaxAmount1) + getValueLabel(txtPreTaxAmount2)), ConstDatabase.Format_TienVND);
            BindData(txtItem23, (getValueLabel(txtPreTaxAmount1) + getValueLabel(txtPreTaxAmount2) + getValueLabel(txtPreTaxAmount3)));
        }

        private void txtPreTaxAmount2_TextChanged(object sender, EventArgs e)
        {
            txtDiscountPurchase.FormatNumberic((getValueLabel(txtPreTaxAmount1) + getValueLabel(txtPreTaxAmount2)), ConstDatabase.Format_TienVND);
            BindData(txtItem23, (getValueLabel(txtPreTaxAmount1) + getValueLabel(txtPreTaxAmount2) + getValueLabel(txtPreTaxAmount3)));
        }

        private void txtPreTaxAmount3_TextChanged(object sender, EventArgs e)
        {
            txtDiscountPurchase.FormatNumberic((getValueLabel(txtPreTaxAmount1) + getValueLabel(txtPreTaxAmount2)), ConstDatabase.Format_TienVND);
            BindData(txtItem23, (getValueLabel(txtPreTaxAmount1) + getValueLabel(txtPreTaxAmount2) + getValueLabel(txtPreTaxAmount3)));
        }
        #endregion

        #region Tổng giá trị sau thuế PL 2
        private void txtTaxAmount1_TextChanged(object sender, EventArgs e)
        {
            txtDiscountTaxAmount1.FormatNumberic((getValueLabel(txtTaxAmount1) + getValueLabel(txtTaxAmount2)), ConstDatabase.Format_TienVND);
            BindData(txtItem24, (getValueLabel(txtTaxAmount1) + getValueLabel(txtTaxAmount2) + getValueLabel(txtTaxAmount3)));
        }

        private void txtTaxAmount2_TextChanged(object sender, EventArgs e)
        {
            txtDiscountTaxAmount1.FormatNumberic((getValueLabel(txtTaxAmount1) + getValueLabel(txtTaxAmount2)), ConstDatabase.Format_TienVND);
            BindData(txtItem24, (getValueLabel(txtTaxAmount1) + getValueLabel(txtTaxAmount2) + getValueLabel(txtTaxAmount3)));
        }

        private void txtTaxAmount3_TextChanged(object sender, EventArgs e)
        {
            // txtDiscountTaxAmount1.FormatNumberic((getValueLabel(txtTaxAmount1) + getValueLabel(txtTaxAmount2)), ConstDatabase.Format_TienVND);
            BindData(txtItem24, (getValueLabel(txtTaxAmount1) + getValueLabel(txtTaxAmount2) + getValueLabel(txtTaxAmount3)));
        }
        #endregion 
        #endregion

        #region format dấu
        public bool LoadMaskInp()
        {
            foreach (var item in GetAll(this, typeof(UltraMaskedEdit)))
            {
                UltraMaskedEdit ultraMaskedEdit = item as UltraMaskedEdit;
                if (item is UltraMaskedEdit)
                {

                    decimal r = getValueMaskedEdit(item as UltraMaskedEdit);
                    if (r < 0)
                    {
                        (item as UltraMaskedEdit).InputMask = "(n,nnn,nnn,nnn,nnn,nnn)";
                        (item as UltraMaskedEdit).FormatString = "(###,###,###,###,###,##0)";
                        (item as UltraMaskedEdit).Value = -r;

                    }
                    if (ultraMaskedEdit.InputMask == "-n,nnn,nnn,nnn,nnn,nnn")
                    {
                        (item as UltraMaskedEdit).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
                        (item as UltraMaskedEdit).FormatString = "###,###,###,###,###,##0";
                    }
                }
            }
            return true;
        }

        public IEnumerable<Control> GetAll(Control control, System.Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }
        public bool BindData(UltraMaskedEdit ultraMaskedEdit, decimal r)
        {
            try
            {
                if (r < 0)
                {
                    ultraMaskedEdit.InputMask = "(n,nnn,nnn,nnn,nnn,nnn)";
                    ultraMaskedEdit.Value = -r;
                }
                else
                {
                    ultraMaskedEdit.InputMask = "n,nnn,nnn,nnn,nnn,nnn";
                    ultraMaskedEdit.Value = r;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private void uMaskedEdit_KeyPress(object sender, KeyPressEventArgs e)
        {
            UltraMaskedEdit ultraMaskedEdit = (UltraMaskedEdit)sender;
            //ultraMaskedEdit.Text = ultraMaskedEdit.Value.ToString().Trim(' ', '_');
            if (e.KeyChar == '-')
            {
                if (ultraMaskedEdit.InputMask == "n,nnn,nnn,nnn,nnn,nnn")
                {
                    ultraMaskedEdit.InputMask = "(n,nnn,nnn,nnn,nnn,nnn)";
                }
                else
                {
                    ultraMaskedEdit.InputMask = "n,nnn,nnn,nnn,nnn,nnn";
                }
            }

        }
        //config ngày tháng khi ấy trong datagridview
        private string getDate(DataGridViewCell dataGridViewCell)
        {
            string u = dataGridViewCell.Value.ToString();
            if (!u.IsNullOrEmpty())
            {
                if (u.Contains("/"))
                {
                    string y = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}", u[0], u[1], u[2], u[3], u[4], u[5], u[6], u[7], u[8], u[9], u[10]);
                    return y;
                }
                else
                {
                    string u1 = string.Format("{0}{1}", u[0], u[1]);
                    string u2 = string.Format("{0}{1}", u[2], u[3]);
                    string u3 = string.Format("{0}{1}{2}{3}", u[4], u[5], u[6], u[7]);
                    string u4 = string.Format("{0}/{1}/{2}", u2, u1, u3);
                    return u4;
                }
            }
            else
            {
                return null;
            }

        }
        private string getString(DataGridViewCell dataGridViewCell)
        {
            if (!dataGridViewCell.Value.IsNullOrEmpty())
            {
                string r = dataGridViewCell.Value.ToString();

                return r;

            }
            else
            {
                return "";
            }

        }
        private Guid getGuid(DataGridViewCell dataGridViewCell)
        {
            if (!dataGridViewCell.Value.IsNullOrEmpty())
            {
                Guid r = (Guid)dataGridViewCell.Value;

                return r;

            }
            else
            {
                return Guid.Empty;
            }

        }
        private decimal getValueMaskedEdit1(DataGridViewCell ultraMaskedEdit)
        {
            string sotunhien = "";
            try
            {
                sotunhien = ultraMaskedEdit.Value.ToString();
            }
            catch
            { }
            if (sotunhien != "")
            {
                if (sotunhien.Contains("("))
                {
                    return -decimal.Parse(sotunhien.Trim('(', ')'));
                }
                else
                {
                    return decimal.Parse(sotunhien);
                }
            }
            else
            {
                return 0;
            }

        }
        private decimal getValueMaskedEdit(UltraMaskedEdit ultraMaskedEdit)
        {
            if (!ultraMaskedEdit.Value.IsNullOrEmpty())
            {
                if (ultraMaskedEdit.Text.Contains("("))
                {
                    return -decimal.Parse(ultraMaskedEdit.Value.ToString());
                }
                else
                {
                    return decimal.Parse(ultraMaskedEdit.Value.ToString());
                }
            }
            return 0;
        }
        private decimal getValueTextEdit(UltraTextEditor ultraMaskedEdit)
        {
            if (!ultraMaskedEdit.Value.IsNullOrEmpty())
            {
                if (!ultraMaskedEdit.Text.IsNullOrEmpty())
                {
                    if (ultraMaskedEdit.Text.Contains("("))
                    {
                        return -decimal.Parse(ultraMaskedEdit.Value.ToString().Trim('(', ')'));
                    }
                    else
                    {
                        return decimal.Parse(ultraMaskedEdit.Value.ToString());
                    }
                }
            }
            return 0;
        }
        private decimal getValueLabel(UltraLabel ultraMaskedEdit)
        {
            if (!ultraMaskedEdit.Text.IsNullOrEmpty())
            {
                if (ultraMaskedEdit.Text.Contains("("))
                {
                    return -decimal.Parse(ultraMaskedEdit.Text.Trim('(', ')'));
                }
                else
                {
                    return decimal.Parse(ultraMaskedEdit.Text);
                }
            }
            return 0;
        }





        #endregion

        #region tính lại tổng khi xóa dòng PL 2
        private void tableLayoutPanel5_ControlRemoved(object sender, ControlEventArgs e)
        {
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel5.RowCount; t++)
            //{
            //    var control = tableLayoutPanel5.GetControlFromPosition(5, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtPreTaxAmount1.FormatNumberic(sum1, ConstDatabase.Format_TienVND);

            decimal sum2 = 0;
            //for (int t = 1; t < tableLayoutPanel5.RowCount; t++)
            //{
            //    var control = tableLayoutPanel5.GetControlFromPosition(6, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum2 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtTaxAmount1.FormatNumberic(sum2, ConstDatabase.Format_TienVND);
        }

        private void tableLayoutPanel6_ControlRemoved(object sender, ControlEventArgs e)
        {
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel6.RowCount; t++)
            //{
            //    var control = tableLayoutPanel6.GetControlFromPosition(5, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtPreTaxAmount2.FormatNumberic(sum1, ConstDatabase.Format_TienVND);

            decimal sum2 = 0;
            //for (int t = 1; t < tableLayoutPanel6.RowCount; t++)
            //{
            //    var control = tableLayoutPanel6.GetControlFromPosition(6, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum2 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtTaxAmount2.FormatNumberic(sum2, ConstDatabase.Format_TienVND);
        }

        private void tableLayoutPanel7_ControlRemoved(object sender, ControlEventArgs e)
        {
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel7.RowCount; t++)
            //{
            //    var control = tableLayoutPanel7.GetControlFromPosition(5, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtPreTaxAmount3.FormatNumberic(sum1, ConstDatabase.Format_TienVND);

            decimal sum2 = 0;
            //for (int t = 1; t < tableLayoutPanel7.RowCount; t++)
            //{
            //    var control = tableLayoutPanel7.GetControlFromPosition(6, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum2 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtTaxAmount3.FormatNumberic(sum2, ConstDatabase.Format_TienVND);
        }
        #endregion

        #region tính lại tổng khi xóa dòng PL 1
        private void tableLayoutPanel1_ControlRemoved(object sender, ControlEventArgs e)
        {
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel1.RowCount; t++)
            //{
            //    var control = tableLayoutPanel1.GetControlFromPosition(5, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtPreTaxAmountKCT.FormatNumberic(sum1, ConstDatabase.Format_TienVND);

            decimal sum2 = 0;
            //for (int t = 1; t < tableLayoutPanel1.RowCount; t++)
            //{
            //    var control = tableLayoutPanel1.GetControlFromPosition(6, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum2 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtTaxAmountKCT.FormatNumberic(sum2, ConstDatabase.Format_TienVND);
        }

        private void tableLayoutPanel2_ControlRemoved(object sender, ControlEventArgs e)
        {
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel2.RowCount; t++)
            //{
            //    var control = tableLayoutPanel2.GetControlFromPosition(5, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtPreTaxAmount0.FormatNumberic(sum1, ConstDatabase.Format_TienVND);

            decimal sum2 = 0;
            //for (int t = 1; t < tableLayoutPanel2.RowCount; t++)
            //{
            //    var control = tableLayoutPanel2.GetControlFromPosition(6, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum2 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtTaxAmount0.FormatNumberic(sum2, ConstDatabase.Format_TienVND);
        }

        private void tableLayoutPanel3_ControlRemoved(object sender, ControlEventArgs e)
        {
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel3.RowCount; t++)
            //{
            //    var control = tableLayoutPanel3.GetControlFromPosition(5, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtPreTaxAmount5.FormatNumberic(sum1, ConstDatabase.Format_TienVND);

            decimal sum2 = 0;
            //for (int t = 1; t < tableLayoutPanel3.RowCount; t++)
            //{
            //    var control = tableLayoutPanel3.GetControlFromPosition(6, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum2 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtTaxAmount5.FormatNumberic(sum2, ConstDatabase.Format_TienVND);
        }

        private void tableLayoutPanel4_ControlRemoved(object sender, ControlEventArgs e)
        {
            decimal sum1 = 0;
            //for (int t = 1; t < tableLayoutPanel4.RowCount; t++)
            //{
            //    var control = tableLayoutPanel4.GetControlFromPosition(5, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum1 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtPreTaxAmount10.FormatNumberic(sum1, ConstDatabase.Format_TienVND);

            decimal sum2 = 0;
            //for (int t = 1; t < tableLayoutPanel4.RowCount; t++)
            //{
            //    var control = tableLayoutPanel4.GetControlFromPosition(6, t);
            //    if (control is UltraMaskedEdit)
            //    {
            //        UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
            //        if (ultraTextEditor.Text != "")
            //        {
            //            sum2 += getValueMaskedEdit(ultraTextEditor);
            //        }
            //    }
            //}
            txtTaxAmount10.FormatNumberic(sum2, ConstDatabase.Format_TienVND);
        }




        #endregion

        private void txtItem_Validated(object sender, EventArgs e)
        {
            UltraMaskedEdit txt = (UltraMaskedEdit)sender;
            txt.Text = txt.Text.TrimStart('0');
        }
        private bool ExportXml(TM01GTGT tM01GTGT)
        {
            #region Tờ khai chính
            SaveFileDialog sf = new SaveFileDialog
            {
                FileName = "TM01GTGT.xml",
                AddExtension = true,
                Filter = "Excel Document(*.xml)|*.xml"
            };
            if (sf.ShowDialog() != DialogResult.OK)
            {
                return true;
            }
            string path = System.IO.Path.GetDirectoryName(
                        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            string filePath = string.Format("{0}\\TemplateResource\\xml\\01_GTGT_xml.xml", path);
            XmlDocument doc = new XmlDocument();
            doc.Load(filePath);
            string tt = "HSoThueDTu/HSoKhaiThue/TTinChung/";
            doc.DocumentElement.SetAttribute("xmlns", "http://kekhaithue.gdt.gov.vn/TKhaiThue");
            doc.DocumentElement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/maTKhai").InnerText = "01";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/tenTKhai").InnerText = tM01GTGT.DeclarationName;
            if (tM01GTGT.IsFirstDeclaration)
            {
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/loaiTKhai").InnerText = "C";
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/soLan").InnerText = "0";
            }
            else
            {
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/loaiTKhai").InnerText = "B";
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/soLan").InnerText = (tM01GTGT.AdditionTime ?? 0).ToString();
            }

            if (tM01GTGT.DeclarationTerm.Contains("Quý"))
            {
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kieuKy").InnerText = "Q";
                if (tM01GTGT.DeclarationTerm.Split(' ')[1].Equals("I"))
                {
                    doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = "1/" + tM01GTGT.DeclarationTerm.Split(' ')[3];
                }
                else if (tM01GTGT.DeclarationTerm.Split(' ')[1].Equals("II"))
                {
                    doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = "2/" + tM01GTGT.DeclarationTerm.Split(' ')[3];
                }
                else if (tM01GTGT.DeclarationTerm.Split(' ')[1].Equals("III"))
                {
                    doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = "3/" + tM01GTGT.DeclarationTerm.Split(' ')[3];
                }
                else
                {
                    doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = "4/" + tM01GTGT.DeclarationTerm.Split(' ')[3];
                }
            }
            else
            {
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kieuKy").InnerText = "M";
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = tM01GTGT.DeclarationTerm.Split(' ')[1] + "/" + tM01GTGT.DeclarationTerm.Split(' ')[3];
            }

            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhaiTuNgay").InnerText = tM01GTGT.FromDate.ToString("dd/MM/yyyy");
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhaiDenNgay").InnerText = tM01GTGT.ToDate.ToString("dd/MM/yyyy");

            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/maCQTNoiNop").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/tenCQTNoiNop").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/ngayLapTKhai").InnerText = "";

            if (tM01GTGT.IsExtend)
            {
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/GiaHan/maLyDoGiaHan").InnerText = int.Parse(tM01GTGT.ExtensionCase) != 3 ? (int.Parse(tM01GTGT.ExtensionCase) + 1).ToString().PadLeft(2, '0') : "99";
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/GiaHan/lyDoGiaHan").InnerText = listTMIndustryType[tM01GTGT.ExtensionCase];
            }
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/nguoiKy").InnerText = tM01GTGT.SignName;
            if (tM01GTGT.SignDate != null)
            {
                DateTime dt = (DateTime)tM01GTGT.SignDate;
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/ngayKy").InnerText = dt.ToString("yyyy-MM-dd");
            }

            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/nganhNgheKD").InnerText = tM01GTGT.CertificationNo;

            if (!Utils.GetDLTNgay().IsNullOrEmpty())
            {
                DateTime ngdlt = DateTime.ParseExact(Utils.GetDLTNgay(), "dd/MM/yyyy", null);
                doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/ngayKyHDDLyThue").InnerText = ngdlt.ToString("yyyy-MM-dd");
            }
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/mst").InnerText = Utils.GetCompanyTaxCodeForInvoice();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/tenNNT").InnerText = tM01GTGT.CompanyName;
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/dchiNNT").InnerText = Utils.GetCompanyAddress();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/phuongXa").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/maHuyenNNT").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/tenHuyenNNT").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/maTinhNNT").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/tenTinhNNT").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/dthoaiNNT").InnerText = Utils.GetCompanyPhoneNumber();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/faxNNT").InnerText = tM01GTGT.CompanyTaxCode;
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/emailNNT").InnerText = Utils.GetCompanyEmail();

            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/mstDLyThue").InnerText = Utils.GetMSTDLT();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/tenDLyThue").InnerText = tM01GTGT.TaxAgencyName;
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/dchiDLyThue").InnerText = Utils.GetDLTAddress();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/maHuyenDLyThue").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/tenHuyenDLyThue").InnerText = Utils.GetDLTDistrict();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/maTinhDLyThue").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/tenTinhDLyThue").InnerText = Utils.GetDLTCity();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/dthoaiDLyThue").InnerText = Utils.GetDLTPhoneNumber();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/faxDLyThue").InnerText = Utils.GetDLTFax();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/emailDLyThue").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/soHDongDLyThue").InnerText = Utils.GetDLTSoHD();

            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/NVienDLy/tenNVienDLyThue").InnerText = Utils.GetHVTNhanVienDLT();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/NVienDLy/cchiHNghe").InnerText = Utils.GetCCHNDLT();

            if (!Utils.GetTTDLT())
            {
                doc.SelectSingleNode(tt + "TTinTKhaiThue").RemoveChild(doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue"));
            }

            tt = "HSoThueDTu/HSoKhaiThue/CTieuTKhaiChinh/";

            doc.SelectSingleNode(tt + "tieuMucHachToan").InnerText = "1701";
            doc.SelectSingleNode(tt + "ct21").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item21").Data;
            doc.SelectSingleNode(tt + "ct22").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item22").Data;
            doc.SelectSingleNode(tt + "GiaTriVaThueGTGTHHDVMuaVao/ct23").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item23").Data;
            doc.SelectSingleNode(tt + "GiaTriVaThueGTGTHHDVMuaVao/ct24").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item24").Data;
            doc.SelectSingleNode(tt + "ct25").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item25").Data;
            doc.SelectSingleNode(tt + "ct26").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item26").Data;
            doc.SelectSingleNode(tt + "HHDVBRaChiuThueGTGT/ct27").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item27").Data;
            doc.SelectSingleNode(tt + "HHDVBRaChiuThueGTGT/ct28").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item28").Data;
            doc.SelectSingleNode(tt + "ct29").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item29").Data;
            doc.SelectSingleNode(tt + "HHDVBRaChiuTSuat5/ct30").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item30").Data;
            doc.SelectSingleNode(tt + "HHDVBRaChiuTSuat5/ct31").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item31").Data;
            doc.SelectSingleNode(tt + "HHDVBRaChiuTSuat10/ct32").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item32").Data;
            doc.SelectSingleNode(tt + "HHDVBRaChiuTSuat10/ct33").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item33").Data;
            doc.SelectSingleNode(tt + "HHDVBRaKhongTinhThue/ct32a").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item32a").Data;
            doc.SelectSingleNode(tt + "TongDThuVaThueGTGTHHDVBRa/ct34").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item34").Data;
            doc.SelectSingleNode(tt + "TongDThuVaThueGTGTHHDVBRa/ct35").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item35").Data;
            doc.SelectSingleNode(tt + "ct36").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item36").Data;
            doc.SelectSingleNode(tt + "ct37").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item37").Data;
            doc.SelectSingleNode(tt + "ct38").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item38").Data;
            doc.SelectSingleNode(tt + "ct39").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item39").Data;
            doc.SelectSingleNode(tt + "ct40a").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40a").Data;
            doc.SelectSingleNode(tt + "ct40b").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40b").Data;
            doc.SelectSingleNode(tt + "ct40").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40").Data;
            doc.SelectSingleNode(tt + "ct41").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item41").Data;
            doc.SelectSingleNode(tt + "ct42").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item42").Data;
            doc.SelectSingleNode(tt + "ct43").InnerText = tM01GTGT.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item43").Data;

            if (tM01GTGT.IsFirstDeclaration)
            {
                doc.SelectSingleNode("HSoThueDTu/HSoKhaiThue/PLuc").InnerText = "";
            }
            else
            {
                string pathBS = System.IO.Path.GetDirectoryName(
                        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                string filePathBS = string.Format("{0}\\TemplateResource\\xml\\PL01_KHBS.xml", path);
                XmlDocument docBS = new XmlDocument();
                docBS.Load(filePathBS);
                //string ttbs = "PL01_KHBS/NDUNGKHAIBOSUNGDIEUCHINH/";
                //List<TM01GTGTAdjust> lst1 = tM01GTGT.TM01GTGTAdjusts.Where(n => n.Type == 1).ToList();
                //List<TM01GTGTAdjust> lst2 = tM01GTGT.TM01GTGTAdjusts.Where(n => n.Type == 2).ToList();
                //List<TM01GTGTAdjust> lst3 = tM01GTGT.TM01GTGTAdjusts.Where(n => n.Type == 3).ToList();
                //for (int i = 0; i < lst1.Count(); i++)
                //{
                //    if (i == 0)
                //    {
                //        docBS.SelectSingleNode(ttbs + "DieuChinhTang/KHBSDieuChinhTang/ct2").InnerText = lst1[i].Name;
                //        docBS.SelectSingleNode(ttbs + "DieuChinhTang/KHBSDieuChinhTang/ct3").InnerText = lst1[i].Code;
                //        docBS.SelectSingleNode(ttbs + "DieuChinhTang/KHBSDieuChinhTang/ct4").InnerText = lst1[i].DeclaredAmount.ToString("0").Replace(',', '.');
                //        docBS.SelectSingleNode(ttbs + "DieuChinhTang/KHBSDieuChinhTang/ct5").InnerText = lst1[i].AdjustAmount.ToString("0").Replace(',', '.');
                //        docBS.SelectSingleNode(ttbs + "DieuChinhTang/KHBSDieuChinhTang/ct6").InnerText = lst1[i].DifferAmount.ToString("0").Replace(',', '.');
                //    }
                //    else
                //    {
                //        XmlNode xmlNode1 = doc.SelectSingleNode(ttbs + "DieuChinhTang/KHBSDieuChinhTang").CloneNode(true);
                //        xmlNode1.Attributes["id"].Value = "ID_" + (i + 1).ToString();
                //        xmlNode1.SelectSingleNode("ct2").InnerText = lst1[i].Name;
                //        xmlNode1.SelectSingleNode("ct3").InnerText = lst1[i].Code;
                //        xmlNode1.SelectSingleNode("ct4").InnerText = lst1[i].DeclaredAmount.ToString("0").Replace(',', '.');
                //        xmlNode1.SelectSingleNode("ct5").InnerText = lst1[i].AdjustAmount.ToString("0").Replace(',', '.');
                //        xmlNode1.SelectSingleNode("ct6").InnerText = lst1[i].DifferAmount.ToString("0").Replace(',', '.');
                //        docBS.SelectSingleNode(ttbs + "DieuChinhTang").AppendChild(xmlNode1);
                //    }
                //}
                //for (int i = 0; i < lst2.Count(); i++)
                //{
                //    if (i == 0)
                //    {
                //        docBS.SelectSingleNode(ttbs + "DieuChinhGiam/KHBSDieuChinhGiam/ct2").InnerText = lst2[i].Name;
                //        docBS.SelectSingleNode(ttbs + "DieuChinhGiam/KHBSDieuChinhGiam/ct3").InnerText = lst2[i].Code;
                //        docBS.SelectSingleNode(ttbs + "DieuChinhGiam/KHBSDieuChinhGiam/ct4").InnerText = lst2[i].DeclaredAmount.ToString("0").Replace(',', '.');
                //        docBS.SelectSingleNode(ttbs + "DieuChinhGiam/KHBSDieuChinhGiam/ct5").InnerText = lst2[i].AdjustAmount.ToString("0").Replace(',', '.');
                //        docBS.SelectSingleNode(ttbs + "DieuChinhGiam/KHBSDieuChinhGiam/ct6").InnerText = lst2[i].DifferAmount.ToString("0").Replace(',', '.');
                //    }
                //    else
                //    {
                //        XmlNode xmlNode1 = doc.SelectSingleNode(ttbs + "DieuChinhGiam/KHBSDieuChinhGiam").CloneNode(true);
                //        xmlNode1.Attributes["id"].Value = "ID_" + (i + 1).ToString();
                //        xmlNode1.SelectSingleNode("ct2").InnerText = lst2[i].Name;
                //        xmlNode1.SelectSingleNode("ct3").InnerText = lst2[i].Code;
                //        xmlNode1.SelectSingleNode("ct4").InnerText = lst2[i].DeclaredAmount.ToString("0").Replace(',', '.');
                //        xmlNode1.SelectSingleNode("ct5").InnerText = lst2[i].AdjustAmount.ToString("0").Replace(',', '.');
                //        xmlNode1.SelectSingleNode("ct6").InnerText = lst2[i].DifferAmount.ToString("0").Replace(',', '.');
                //        docBS.SelectSingleNode(ttbs + "DieuChinhGiam").AppendChild(xmlNode1);
                //    }
                //}
                //docBS.SelectSingleNode(ttbs + "TongHopDCTangGiamPNop/ct40/ct2").InnerText = lst3.FirstOrDefault(n => n.Code == "40").Name;
                //docBS.SelectSingleNode(ttbs + "TongHopDCTangGiamPNop/ct40/ct3").InnerText = lst3.FirstOrDefault(n => n.Code == "40").Code;
                //docBS.SelectSingleNode(ttbs + "TongHopDCTangGiamPNop/ct40/ct4").InnerText = lst3.FirstOrDefault(n => n.Code == "40").DeclaredAmount.ToString("0").Replace(',', '.');
                //docBS.SelectSingleNode(ttbs + "TongHopDCTangGiamPNop/ct40/ct5").InnerText = lst3.FirstOrDefault(n => n.Code == "40").AdjustAmount.ToString("0").Replace(',', '.');
                //docBS.SelectSingleNode(ttbs + "TongHopDCTangGiamPNop/ct40/ct6").InnerText = lst3.FirstOrDefault(n => n.Code == "40").DifferAmount.ToString("0").Replace(',', '.');

                //docBS.SelectSingleNode(ttbs + "TongHopDCTangGiamPNop/ct43/ct2").InnerText = lst3.FirstOrDefault(n => n.Code == "43").Name;
                //docBS.SelectSingleNode(ttbs + "TongHopDCTangGiamPNop/ct43/ct3").InnerText = lst3.FirstOrDefault(n => n.Code == "43").Code;
                //docBS.SelectSingleNode(ttbs + "TongHopDCTangGiamPNop/ct43/ct4").InnerText = lst3.FirstOrDefault(n => n.Code == "43").DeclaredAmount.ToString("0").Replace(',', '.');
                //docBS.SelectSingleNode(ttbs + "TongHopDCTangGiamPNop/ct43/ct5").InnerText = lst3.FirstOrDefault(n => n.Code == "43").AdjustAmount.ToString("0").Replace(',', '.');
                //docBS.SelectSingleNode(ttbs + "TongHopDCTangGiamPNop/ct43/ct6").InnerText = lst3.FirstOrDefault(n => n.Code == "43").DifferAmount.ToString("0").Replace(',', '.');

                //ttbs = "PL01_KHBS/PhatChamNop/";
                //docBS.SelectSingleNode("PL01_KHBS/PhatChamNop/soNgayNopCham").InnerText = lst3.FirstOrDefault(n => n.Code == "40").LateDays == null ? "" : lst3.FirstOrDefault(n => n.Code == "40").LateDays.ToString();
                //docBS.SelectSingleNode("PL01_KHBS/PhatChamNop/soTienPhatNopCham").InnerText = lst3.FirstOrDefault(n => n.Code == "40").LateAmount.ToString("0").Replace(',', '.');

                //docBS.SelectSingleNode("PL01_KHBS/NOIDUNGGIAITHICH/soTienTraNSNN").InnerText = lst3.FirstOrDefault(n => n.Code == "40").ExplainAmount.ToString("0").Replace(',', '.');
                //docBS.SelectSingleNode("PL01_KHBS/NOIDUNGGIAITHICH/quyetDinhHoanThueSo").InnerText = lst3.FirstOrDefault(n => n.Code == "40").CommandNo;
                //if(lst3.FirstOrDefault(n => n.Code == "40").CommandDate != null)
                //{
                //    docBS.SelectSingleNode("PL01_KHBS/NOIDUNGGIAITHICH/ngay").InnerText = (lst3.FirstOrDefault(n => n.Code == "40").CommandDate ?? DateTime.Now).ToString("yyyy-MM-dd");
                //}
                //XmlAttribute attr = docBS.SelectSingleNode("PL01_KHBS/NOIDUNGGIAITHICH/ngay").OwnerDocument.CreateAttribute("xsi:nil");
                //attr.Value = "true";
                //docBS.SelectSingleNode("PL01_KHBS/NOIDUNGGIAITHICH/ngay").Attributes.Append(attr);
                //docBS.SelectSingleNode("PL01_KHBS/NOIDUNGGIAITHICH/coQuanThue").InnerText = lst3.FirstOrDefault(n => n.Code == "40").TaxCompanyName;
                //docBS.SelectSingleNode("PL01_KHBS/NOIDUNGGIAITHICH/soNgayNhanDuocTienHoan").InnerText = lst3.FirstOrDefault(n => n.Code == "40").ReceiveDays == null ? "" : lst3.FirstOrDefault(n => n.Code == "40").ReceiveDays.ToString();
                //docBS.SelectSingleNode("PL01_KHBS/NOIDUNGGIAITHICH/soTienChamNop").InnerText = lst3.FirstOrDefault(n => n.Code == "40").ExplainLateAmount.ToString("0").Replace(',', '.');
                //docBS.SelectSingleNode("PL01_KHBS/NOIDUNGGIAITHICH/lyDoKhac").InnerText = lst3.FirstOrDefault(n => n.Code == "40").DifferReason;

                doc.SelectSingleNode("HSoThueDTu/HSoKhaiThue/PLuc").InnerXml = docBS.InnerXml;
            }


            doc.Save(sf.FileName);
            if (MSG.Question("Xuất xml thành công, Bạn có muốn mở file vừa tải") == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    System.Diagnostics.Process.Start(sf.FileName);
                }
                catch
                {
                    MSG.Error("Lỗi mở file");
                }

            }

            return true;
            #endregion
            #region PL01
            //else if (ultraTabControl1.SelectedTab.Key == "PL01-1/GTGT")
            //{
            //    SaveFileDialog sf = new SaveFileDialog
            //    {
            //        FileName = "01_1_GTGT_xml.xml",
            //        AddExtension = true,
            //        Filter = "Excel Document(*.xml)|*.xml"
            //    };
            //    if (sf.ShowDialog() != DialogResult.OK)
            //    {
            //        return true;
            //    }
            //    string path = System.IO.Path.GetDirectoryName(
            //                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            //    string filePath = string.Format("{0}\\TemplateResource\\xml\\01_1_GTGT_xml.xml", path);
            //    XmlDocument doc = new XmlDocument();
            //    doc.Load(filePath);

            //    string tt = "PL01_1_GTGT/HHDVKChiuThue";
            //    for (int i = 0; i < tM01GTGT.TM011GTGTs.Count; i++)
            //    {
            //        if (i == 0)
            //        {
            //            doc.SelectSingleNode(tt + "ChiTietHHDVKChiuThue/HDonBRa/kyHieuMauHDon").InnerText = tM01GTGT.TM011GTGTs[i].OrderPriority.ToString();
            //            doc.SelectSingleNode(tt + "ChiTietHHDVKChiuThue/HDonBRa/kyHieuHDon").InnerText = "";
            //            doc.SelectSingleNode(tt + "ChiTietHHDVKChiuThue/HDonBRa/soHDon").InnerText = "";
            //            doc.SelectSingleNode(tt + "ChiTietHHDVKChiuThue/HDonBRa/ngayPHanh").InnerText = "";
            //            doc.SelectSingleNode(tt + "ChiTietHHDVKChiuThue/HDonBRa/tenNMUA").InnerText = "";
            //            doc.SelectSingleNode(tt + "ChiTietHHDVKChiuThue/HDonBRa/mstNMUA").InnerText = "";
            //            doc.SelectSingleNode(tt + "ChiTietHHDVKChiuThue/HDonBRa/matHang").InnerText = "";
            //            doc.SelectSingleNode(tt + "ChiTietHHDVKChiuThue/HDonBRa/dsoBanChuaThue").InnerText = "";
            //            doc.SelectSingleNode(tt + "ChiTietHHDVKChiuThue/HDonBRa/thueGTGT").InnerText = "";
            //            doc.SelectSingleNode(tt + "ChiTietHHDVKChiuThue/HDonBRa/ghiChu").InnerText = "";
            //        }
            //        else
            //        {
            //            XmlNode xmlNode = doc.SelectSingleNode(tt + "ChiTietHHDVKChiuThue/HDonBRa").CloneNode(true);
            //            xmlNode.Attributes["id"].Value = "ID_" + (i + 1).ToString();
            //            xmlNode.SelectSingleNode("kyHieuMauHDon").InnerText = tM01GTGT.TM011GTGTs[i].OrderPriority.ToString();
            //            xmlNode.SelectSingleNode("kyHieuHDon").InnerText = tM01GTGT.TM011GTGTs[i].OrderPriority.ToString();
            //            xmlNode.SelectSingleNode("soHDon").InnerText = tM01GTGT.TM011GTGTs[i].OrderPriority.ToString();
            //            xmlNode.SelectSingleNode("ngayPHanh").InnerText = tM01GTGT.TM011GTGTs[i].OrderPriority.ToString();
            //            xmlNode.SelectSingleNode("tenNMUA").InnerText = tM01GTGT.TM011GTGTs[i].OrderPriority.ToString();
            //            xmlNode.SelectSingleNode("mstNMUA").InnerText = tM01GTGT.TM011GTGTs[i].OrderPriority.ToString();
            //            xmlNode.SelectSingleNode("matHang").InnerText = tM01GTGT.TM011GTGTs[i].OrderPriority.ToString();
            //            xmlNode.SelectSingleNode("dsoBanChuaThue").InnerText = tM01GTGT.TM011GTGTs[i].OrderPriority.ToString();
            //            xmlNode.SelectSingleNode("thueGTGT").InnerText = tM01GTGT.TM011GTGTs[i].OrderPriority.ToString();
            //            xmlNode.SelectSingleNode("ghiChu").InnerText = tM01GTGT.TM011GTGTs[i].OrderPriority.ToString();
            //        }
            //    }
            //}
            #endregion
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        DataGridViewRow dataGridViewRow1_;
        DataGridViewRow dataGridViewRow2_;
        DataGridViewRow dataGridViewRow3_;
        DataGridViewRow dataGridViewRow4_;
        DataGridViewRow dataGridViewRow5_;
        DataGridViewRow dataGridViewRow6_;
        DataGridViewRow dataGridViewRow7_;

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                CheckClick = "dataGridView1";
                DataGridView ultraMaskedEdit = (DataGridView)sender;
                dataGridViewRow1_ = ultraMaskedEdit.Rows[e.RowIndex];
            }
            catch (Exception ex)
            {

            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                CheckClick = "dataGridView2";
                DataGridView ultraMaskedEdit = (DataGridView)sender;
                dataGridViewRow2_ = ultraMaskedEdit.Rows[e.RowIndex];
            }
            catch (Exception ex)
            {

            }
        }

        private void dataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                CheckClick = "dataGridView3";
                DataGridView ultraMaskedEdit = (DataGridView)sender;
                dataGridViewRow3_ = ultraMaskedEdit.Rows[e.RowIndex];
            }
            catch (Exception ex)
            {

            }
        }

        private void dataGridView4_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                CheckClick = "dataGridView4";
                DataGridView ultraMaskedEdit = (DataGridView)sender;
                dataGridViewRow4_ = ultraMaskedEdit.Rows[e.RowIndex];
            }
            catch (Exception ex)
            {

            }
        }

        private void dataGridView5_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                CheckClick = "dataGridView5";
                DataGridView ultraMaskedEdit = (DataGridView)sender;
                dataGridViewRow5_ = ultraMaskedEdit.Rows[e.RowIndex];
            }
            catch (Exception ex)
            {

            }
        }

        private void dataGridView6_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                CheckClick = "dataGridView6";
                DataGridView ultraMaskedEdit = (DataGridView)sender;
                dataGridViewRow6_ = ultraMaskedEdit.Rows[e.RowIndex];
            }
            catch (Exception ex)
            {

            }
        }

        private void dataGridView7_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                CheckClick = "dataGridView7";
                DataGridView ultraMaskedEdit = (DataGridView)sender;
                dataGridViewRow7_ = ultraMaskedEdit.Rows[e.RowIndex];
            }
            catch (Exception ex)
            {

            }
        }
        private void TinhTongCot5va6datagridview(DataGridView dataGridView)
        {
            decimal sumPreTax1 = 0;
            decimal sumTax1 = 0;
            for (int i = 0; i < dataGridView.RowCount; i++)
            {
                if (!dataGridView.Rows[i].Cells[5].Value.IsNullOrEmpty())
                {
                    string u = dataGridView.Rows[i].Cells[5].Value.ToString();
                    if (!u.IsNullOrEmpty())
                    {
                        decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                        sumPreTax1 += h;
                    }
                }
            }
            for (int i = 0; i < dataGridView.RowCount; i++)
            {
                if (!dataGridView.Rows[i].Cells[6].Value.IsNullOrEmpty())
                {
                    string u = dataGridView.Rows[i].Cells[6].Value.ToString();
                    if (!u.IsNullOrEmpty())
                    {
                        decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                        sumTax1 += h;
                    }
                }
            }
        }
        private string configInvoiceNo(string str)
        {
            string a = str;
            int t = str.Length;
            for (int i = 1; i <= (7 - t); i++)
            {
                a = string.Format("{0}{1}", 0, a);
            }
            return a;
        }
        private void TinhTongCot5va6()
        {
            DataGridView d1 = dataGridView1;
            DataGridView d2 = dataGridView2;
            DataGridView d3 = dataGridView3;
            DataGridView d4 = dataGridView4;
            DataGridView d5 = dataGridView5;
            DataGridView d6 = dataGridView6;
            DataGridView d7 = dataGridView7;
            decimal sumPreTax1 = 0;
            decimal sumTax1 = 0;
            decimal sumPreTax2 = 0;
            decimal sumTax2 = 0;
            decimal sumPreTax3 = 0;
            decimal sumTax3 = 0;
            decimal sumPreTax4 = 0;
            decimal sumTax4 = 0;
            decimal sumPreTax5 = 0;
            decimal sumTax5 = 0;
            decimal sumPreTax6 = 0;
            decimal sumTax6 = 0;
            decimal sumPreTax7 = 0;
            decimal sumTax7 = 0;

            for (int i = 0; i < d1.RowCount; i++)
            {
                if (!d1.Rows[i].Cells[5].Value.IsNullOrEmpty())
                {
                    string u = "";
                    try { u = d1.Rows[i].Cells[5].Value.ToString(); }
                    catch { }
                    if (!u.IsNullOrEmpty())
                    {
                        decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                        sumPreTax1 += h;
                    }
                }
            }
            for (int i = 0; i < d1.RowCount; i++)
            {
                if (!d1.Rows[i].Cells[6].Value.IsNullOrEmpty())
                {
                    string u = "";
                    try { u = d1.Rows[i].Cells[6].Value.ToString(); }
                    catch { }
                    if (!u.IsNullOrEmpty())
                    {
                        decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                        sumTax1 += h;
                    }
                }
            }


            txtPreTaxAmountKCT.Text = Utils.FormatNumberic(sumPreTax1, ConstDatabase.Format_TienVND);
            txtTaxAmountKCT.Text = Utils.FormatNumberic(sumTax1, ConstDatabase.Format_TienVND);
            for (int i = 0; i < d2.RowCount; i++)
            {
                if (!d2.Rows[i].Cells[5].Value.IsNullOrEmpty())
                {
                    string u = "";
                    try { u = d2.Rows[i].Cells[5].Value.ToString(); }
                    catch { }
                    if (!u.IsNullOrEmpty())
                    {
                        decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                        sumPreTax2 += h;
                    }
                }
            }
            for (int i = 0; i < d2.RowCount; i++)
            {
                if (!d2.Rows[i].Cells[6].Value.IsNullOrEmpty())
                {
                    string u = "";
                    try { u = d2.Rows[i].Cells[6].Value.ToString(); }
                    catch { }
                    if (!u.IsNullOrEmpty())
                    {
                        decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                        sumTax2 += h;
                    }
                }

            }
            txtPreTaxAmount0.Text = Utils.FormatNumberic(sumPreTax2, ConstDatabase.Format_TienVND);
            txtTaxAmount0.Text = Utils.FormatNumberic(sumTax2, ConstDatabase.Format_TienVND);
            for (int i = 0; i < d3.RowCount; i++)
            {
                if (!d3.Rows[i].Cells[5].Value.IsNullOrEmpty())
                {
                    string u = "";
                    try { u = d3.Rows[i].Cells[5].Value.ToString(); }
                    catch { }
                    if (!u.IsNullOrEmpty())
                    {
                        decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                        sumPreTax3 += h;
                    }
                }
            }
            for (int i = 0; i < d3.RowCount; i++)
            {
                if (!d3.Rows[i].Cells[6].Value.IsNullOrEmpty())
                {
                    string u = "";
                    try { u = d3.Rows[i].Cells[6].Value.ToString(); }
                    catch { }
                    if (!u.IsNullOrEmpty())
                    {
                        decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                        sumTax3 += h;
                    }
                }
            }


            txtPreTaxAmount5.Text = Utils.FormatNumberic(sumPreTax3, ConstDatabase.Format_TienVND);
            txtTaxAmount5.Text = Utils.FormatNumberic(sumTax3, ConstDatabase.Format_TienVND);
            for (int i = 0; i < d4.RowCount; i++)
            {
                if (!d4.Rows[i].Cells[5].Value.IsNullOrEmpty())
                {
                    string u = "";
                    try { u = d4.Rows[i].Cells[5].Value.ToString(); }
                    catch { }
                    if (!u.IsNullOrEmpty())
                    {
                        decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                        sumPreTax4 += h;
                    }
                }
            }
            for (int i = 0; i < d4.RowCount; i++)
            {
                if (!d4.Rows[i].Cells[6].Value.IsNullOrEmpty())
                {
                    string u = "";
                    try { u = d4.Rows[i].Cells[6].Value.ToString(); }
                    catch { }
                    if (!u.IsNullOrEmpty())
                    {
                        decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                        sumTax4 += h;
                    }
                }
            }


            txtPreTaxAmount10.Text = Utils.FormatNumberic(sumPreTax4, ConstDatabase.Format_TienVND);
            txtTaxAmount10.Text = Utils.FormatNumberic(sumTax4, ConstDatabase.Format_TienVND);
            for (int i = 0; i < d5.RowCount; i++)
            {
                if (!d5.Rows[i].Cells[5].Value.IsNullOrEmpty())
                {
                    string u = "";
                    try { u = d5.Rows[i].Cells[5].Value.ToString(); }
                    catch { }
                    if (!u.IsNullOrEmpty())
                    {
                        decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                        sumPreTax5 += h;
                    }
                }
            }
            for (int i = 0; i < d5.RowCount; i++)
            {
                if (!d5.Rows[i].Cells[6].Value.IsNullOrEmpty())
                {
                    string u = "";
                    try { u = d5.Rows[i].Cells[6].Value.ToString(); }
                    catch { }
                    if (!u.IsNullOrEmpty())
                    {
                        decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                        sumTax5 += h;
                    }
                }
            }


            txtPreTaxAmount1.Text = Utils.FormatNumberic(sumPreTax5, ConstDatabase.Format_TienVND);
            txtTaxAmount1.Text = Utils.FormatNumberic(sumTax5, ConstDatabase.Format_TienVND);
            for (int i = 0; i < d6.RowCount; i++)
            {
                if (!d6.Rows[i].Cells[5].Value.IsNullOrEmpty())
                {
                    string u = "";
                    try { u = d6.Rows[i].Cells[5].Value.ToString(); }
                    catch { }
                    if (!u.IsNullOrEmpty())
                    {
                        decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                        sumPreTax6 += h;
                    }
                }
            }
            for (int i = 0; i < d6.RowCount; i++)
            {
                if (!d6.Rows[i].Cells[6].Value.IsNullOrEmpty())
                {
                    string u = "";
                    try { u = d6.Rows[i].Cells[6].Value.ToString(); }
                    catch { }
                    if (!u.IsNullOrEmpty())
                    {
                        decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                        sumTax6 += h;
                    }
                }
            }


            txtPreTaxAmount2.Text = Utils.FormatNumberic(sumPreTax6, ConstDatabase.Format_TienVND);
            txtTaxAmount2.Text = Utils.FormatNumberic(sumTax6, ConstDatabase.Format_TienVND);
            for (int i = 0; i < d7.RowCount; i++)
            {
                if (!d7.Rows[i].Cells[5].Value.IsNullOrEmpty())
                {
                    string u = "";
                    try { u = d7.Rows[i].Cells[5].Value.ToString(); }
                    catch { }
                    if (!u.IsNullOrEmpty())
                    {
                        decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                        sumPreTax7 += h;
                    }
                }
            }
            for (int i = 0; i < d7.RowCount; i++)
            {
                if (!d7.Rows[i].Cells[6].Value.IsNullOrEmpty())
                {
                    string u = "";
                    try { u = d7.Rows[i].Cells[6].Value.ToString(); }
                    catch { }
                    if (!u.IsNullOrEmpty())
                    {
                        decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                        sumTax7 += h;
                    }
                }

            }


            txtPreTaxAmount3.Text = Utils.FormatNumberic(sumPreTax7, ConstDatabase.Format_TienVND);
            txtTaxAmount3.Text = Utils.FormatNumberic(sumTax7, ConstDatabase.Format_TienVND);
        }


        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            if ((e.ColumnIndex == 1) && (e.RowIndex >= 0))
            {
                DataGridView ultraMaskedEdit = (DataGridView)sender;
                string u = "";
                try { u = ultraMaskedEdit.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(); }
                catch { }
                if ((u == "0000000") || (u == "000000") || (u == "00000") || (u == "0000") || (u == "000") || (u == "00") || (u == "0") || (u == ""))
                { ultraMaskedEdit.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = null; }
            }
            if (((e.ColumnIndex == 5) || (e.ColumnIndex == 6)) && (e.RowIndex >= 0))
            {


                DataGridView ultraMaskedEdit = (DataGridView)sender;
                if (!ultraMaskedEdit.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.IsNullOrEmpty())
                {
                    string d = ultraMaskedEdit.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                    decimal result = 0;

                    try
                    {
                        if (d.Contains("("))
                        {
                            result = -decimal.Parse(d.Trim('(', ')', '.'));
                        }
                        else
                        {
                            if (d.Contains("-") && d.Length == 1)
                            { }
                            else
                            {
                                result = decimal.Parse(d.Trim('(', ')', '.'));
                            }
                        }

                        ultraMaskedEdit.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = Utils.FormatNumberic(result, ConstDatabase.Format_TienVND);

                    }
                    catch (Exception)
                    {
                        throw;

                    }

                }


            }
            //if (((e.ColumnIndex == 2) ) && (e.RowIndex >= 0))
            //{


            //    DataGridView ultraMaskedEdit = (DataGridView)sender;
            //    if (!ultraMaskedEdit.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.IsNullOrEmpty())
            //    {
            //        string d = ultraMaskedEdit.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            //        if (d.Contains("/"))
            //        {


            //                //string d1 = string.Format("{0}{1}", d[0], d[1]);
            //                //string d2 = string.Format("{0}{1}", d[3], d[4]);
            //                //string d3 = string.Format("{0}{1}{2}{3}", d[6], d[7], d[8], d[9]);
            //                //string d4 = string.Format("{0}/{1}/{2}", d2, d1, d3);
            //                ultraMaskedEdit.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = Utils.StringToDateTime(d);
            //            string u= ultraMaskedEdit.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();


            //        }
            //        else
            //        {

            //                string d1 = string.Format("{0}{1}", d[0], d[1]);
            //                string d2 = string.Format("{0}{1}", d[2], d[3]);
            //                string d3 = string.Format("{0}{1}{2}{3}", d[4], d[5], d[6], d[7]);
            //                string d4 = string.Format("{0}/{1}/{2}", d2, d1, d3);
            //                ultraMaskedEdit.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = Utils.StringToDateTime(d);

            //        }


            //    }


            //}


            if ((e.ColumnIndex == 5) || (e.ColumnIndex == 6))
            {

                DataGridView tu = (DataGridView)sender;
                if (tu.Name == "dataGridView1")

                {
                    decimal sumdgv1 = 0;
                    for (int i = 0; i < tu.RowCount; i++)
                    {
                        if (!tu.Rows[i].Cells[e.ColumnIndex].Value.IsNullOrEmpty())
                        {
                            string u = "";
                            try
                            {
                                u = tu.Rows[i].Cells[e.ColumnIndex].Value.ToString();
                            }
                            catch
                            {

                            }
                            if (!u.IsNullOrEmpty())
                            {
                                if (u.Contains("("))
                                {
                                    decimal h = -decimal.Parse(u.Trim('(', ')', '.'));
                                    sumdgv1 += h;
                                }
                                else
                                {
                                    decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                    sumdgv1 += h;
                                }


                            }
                        }
                    }

                    if (e.ColumnIndex == 5)
                        txtPreTaxAmountKCT.Text = Utils.FormatNumberic(sumdgv1, ConstDatabase.Format_TienVND);
                    if (e.ColumnIndex == 6)
                        txtTaxAmountKCT.Text = Utils.FormatNumberic(sumdgv1, ConstDatabase.Format_TienVND);
                }
                if (tu.Name == "dataGridView2")

                {
                    decimal sumdgv1 = 0;
                    for (int i = 0; i < tu.RowCount; i++)
                    {
                        string u = "";
                        try
                        {
                            u = tu.Rows[i].Cells[e.ColumnIndex].Value.ToString();
                        }
                        catch
                        {

                        }
                        if (!u.IsNullOrEmpty())
                        {
                            if (u.Contains("("))
                            {
                                decimal h = -decimal.Parse(u.Trim('(', ')', '.'));
                                sumdgv1 += h;
                            }
                            else
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumdgv1 += h;
                            }


                        }
                    }

                    if (e.ColumnIndex == 5)
                        txtPreTaxAmount0.Text = Utils.FormatNumberic(sumdgv1, ConstDatabase.Format_TienVND);
                    if (e.ColumnIndex == 6)
                        txtTaxAmount0.Text = Utils.FormatNumberic(sumdgv1, ConstDatabase.Format_TienVND);
                }
                if (tu.Name == "dataGridView3")

                {
                    decimal sumdgv1 = 0;
                    for (int i = 0; i < tu.RowCount; i++)
                    {
                        string u = "";
                        try
                        {
                            u = tu.Rows[i].Cells[e.ColumnIndex].Value.ToString();
                        }
                        catch
                        {

                        }
                        if (!u.IsNullOrEmpty())
                        {
                            if (u.Contains("("))
                            {
                                decimal h = -decimal.Parse(u.Trim('(', ')', '.'));
                                sumdgv1 += h;
                            }
                            else
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumdgv1 += h;
                            }


                        }
                    }

                    if (e.ColumnIndex == 5)
                        txtPreTaxAmount5.Text = Utils.FormatNumberic(sumdgv1, ConstDatabase.Format_TienVND);
                    if (e.ColumnIndex == 6)
                        txtTaxAmount5.Text = Utils.FormatNumberic(sumdgv1, ConstDatabase.Format_TienVND);
                }
                if (tu.Name == "dataGridView4")

                {
                    decimal sumdgv1 = 0;
                    for (int i = 0; i < tu.RowCount; i++)
                    {
                        string u = "";
                        try
                        {
                            u = tu.Rows[i].Cells[e.ColumnIndex].Value.ToString();
                        }
                        catch
                        {

                        }
                        if (!u.IsNullOrEmpty())
                        {
                            if (u.Contains("("))
                            {
                                decimal h = -decimal.Parse(u.Trim('(', ')', '.'));
                                sumdgv1 += h;
                            }
                            else
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumdgv1 += h;
                            }


                        }
                    }

                    if (e.ColumnIndex == 5)
                        txtPreTaxAmount10.Text = Utils.FormatNumberic(sumdgv1, ConstDatabase.Format_TienVND);
                    if (e.ColumnIndex == 6)
                        txtTaxAmount10.Text = Utils.FormatNumberic(sumdgv1, ConstDatabase.Format_TienVND);
                }
                if (tu.Name == "dataGridView5")

                {
                    decimal sumdgv1 = 0;
                    for (int i = 0; i < tu.RowCount; i++)
                    {
                        string u = "";
                        try
                        {
                            u = tu.Rows[i].Cells[e.ColumnIndex].Value.ToString();
                        }
                        catch
                        {

                        }
                        if (!u.IsNullOrEmpty())
                        {
                            if (u.Contains("("))
                            {
                                decimal h = -decimal.Parse(u.Trim('(', ')', '.'));
                                sumdgv1 += h;
                            }
                            else
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumdgv1 += h;
                            }


                        }
                    }

                    if (e.ColumnIndex == 5)
                        txtPreTaxAmount1.Text = Utils.FormatNumberic(sumdgv1, ConstDatabase.Format_TienVND);
                    if (e.ColumnIndex == 6)
                        txtTaxAmount1.Text = Utils.FormatNumberic(sumdgv1, ConstDatabase.Format_TienVND);
                }
                if (tu.Name == "dataGridView6")

                {
                    decimal sumdgv1 = 0;
                    for (int i = 0; i < tu.RowCount; i++)
                    {
                        string u = "";
                        try
                        {
                            u = tu.Rows[i].Cells[e.ColumnIndex].Value.ToString();
                        }
                        catch
                        {

                        }
                        if (!u.IsNullOrEmpty())
                        {
                            if (u.Contains("("))
                            {
                                decimal h = -decimal.Parse(u.Trim('(', ')', '.'));
                                sumdgv1 += h;
                            }
                            else
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumdgv1 += h;
                            }


                        }
                    }

                    if (e.ColumnIndex == 5)
                        txtPreTaxAmount2.Text = Utils.FormatNumberic(sumdgv1, ConstDatabase.Format_TienVND);
                    if (e.ColumnIndex == 6)
                        txtTaxAmount2.Text = Utils.FormatNumberic(sumdgv1, ConstDatabase.Format_TienVND);
                }
                if (tu.Name == "dataGridView7")

                {
                    decimal sumdgv1 = 0;
                    for (int i = 0; i < tu.RowCount; i++)
                    {
                        string u = "";
                        try
                        {
                            u = tu.Rows[i].Cells[e.ColumnIndex].Value.ToString();
                        }
                        catch
                        {

                        }
                        if (!u.IsNullOrEmpty())
                        {
                            if (u.Contains("("))
                            {
                                decimal h = -decimal.Parse(u.Trim('(', ')', '.'));
                                sumdgv1 += h;
                            }
                            else
                            {
                                decimal h = decimal.Parse(u.Trim('(', ')', '.'));
                                sumdgv1 += h;
                            }


                        }
                    }

                    if (e.ColumnIndex == 5)
                        txtPreTaxAmount3.Text = Utils.FormatNumberic(sumdgv1, ConstDatabase.Format_TienVND);
                    if (e.ColumnIndex == 6)
                        txtTaxAmount3.Text = Utils.FormatNumberic(sumdgv1, ConstDatabase.Format_TienVND);
                }

            }



        }

        private void dataGridView1_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            int a = e.ColumnIndex;
            int b = e.RowIndex;
        }

        private void dataGridView1_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            int a = e.ColumnIndex;
            int b = e.RowIndex;
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            int a = e.ColumnIndex;
            int b = e.RowIndex;
        }

        private void dataGridView1_CellLeave(object sender, DataGridViewCellEventArgs e)
        {

            //if ((e.ColumnIndex == 5) || (e.ColumnIndex == 6))
            //{


            //    DataGridView ultraMaskedEdit = (DataGridView)sender;
            //    if (!ultraMaskedEdit.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.IsNullOrEmpty())
            //    {
            //        string d = ultraMaskedEdit.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            //        decimal result = 0;

            //        try
            //        {

            //            result = decimal.Parse(d.Trim('(', ')', '.'));
            //            ultraMaskedEdit.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = Utils.FormatNumberic(result, ConstDatabase.Format_TienVND);

            //        }
            //        catch (Exception)
            //        {
            //            throw;

            //        }

            //    }


            //}
        }

        private void txtPreTaxAmountKCT_TextChanged(object sender, EventArgs e)
        {
            txtItem26.Text = txtPreTaxAmountKCT.Text;
        }

        private void _01_GTGT_Detail_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (Form)sender;
            frm.Dispose();

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                contextMenuStrip1.Show();
                CheckClick = "dataGridView1";
                MouseEventArgs mouseEventArgs = new MouseEventArgs(MouseButtons.Right, 1, 3, 3, 0);
                dataGridView1_MouseClick(dataGridView1, mouseEventArgs);
                contextMenuStrip1.Visible = false;
                //contextMenuStrip1.Hide();
            }
            //int currentMouseOverRow = dataGridView1.HitTest(e.X, e.Y).RowIndex;
            //ContextMenuStrip m = contextMenuStrip1;


            //m.Show(dataGridView1,e.X, e.Y);
            //m.Show();
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void dataGridView1_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            //int currentMouseOverRow = dataGridView1.HitTest(e.X, e.Y).RowIndex;
            try
            {
                DataGridView ultraMaskedEdit = (DataGridView)sender;
                dataGridViewRow1_ = ultraMaskedEdit.Rows[e.RowIndex];
                ultraMaskedEdit.Rows[e.RowIndex].Selected = true;
            }
            catch (Exception ex)
            {

            }
        }

        private void dataGridView2_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                DataGridView ultraMaskedEdit = (DataGridView)sender;
                dataGridViewRow2_ = ultraMaskedEdit.Rows[e.RowIndex];
                ultraMaskedEdit.Rows[e.RowIndex].Selected = true;
            }
            catch (Exception ex)
            {

            }
        }

        private void dataGridView3_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                DataGridView ultraMaskedEdit = (DataGridView)sender;
                dataGridViewRow3_ = ultraMaskedEdit.Rows[e.RowIndex];
                ultraMaskedEdit.Rows[e.RowIndex].Selected = true;
            }
            catch (Exception ex)
            {

            }
        }

        private void dataGridView4_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                DataGridView ultraMaskedEdit = (DataGridView)sender;
                dataGridViewRow4_ = ultraMaskedEdit.Rows[e.RowIndex];
                ultraMaskedEdit.Rows[e.RowIndex].Selected = true;
            }
            catch (Exception ex)
            {

            }
        }

        private void dataGridView5_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                DataGridView ultraMaskedEdit = (DataGridView)sender;
                dataGridViewRow5_ = ultraMaskedEdit.Rows[e.RowIndex];
                ultraMaskedEdit.Rows[e.RowIndex].Selected = true;
            }
            catch (Exception ex)
            {

            }
        }

        private void dataGridView6_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                DataGridView ultraMaskedEdit = (DataGridView)sender;
                dataGridViewRow6_ = ultraMaskedEdit.Rows[e.RowIndex];
                ultraMaskedEdit.Rows[e.RowIndex].Selected = true;
            }
            catch (Exception ex)
            {

            }
        }

        private void dataGridView7_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                DataGridView ultraMaskedEdit = (DataGridView)sender;
                dataGridViewRow7_ = ultraMaskedEdit.Rows[e.RowIndex];
                ultraMaskedEdit.Rows[e.RowIndex].Selected = true;
            }
            catch (Exception ex)
            {

            }
        }

        private void dataGridView5_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void dataGridView1_Click(object sender, EventArgs e)
        {

            //DataGridView ts = (DataGridView)sender;
            //ContextMenuStrip cs = (ContextMenuStrip)ts.ContextMenuStrip;
            //cs.clo
            //cs.SourceControl.Parent = dataGridView1;
        }

        private void dataGridView1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if(e.==)
        }

        private void dataGridView2_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                contextMenuStrip1.Show();
                CheckClick = "dataGridView2";
                MouseEventArgs mouseEventArgs = new MouseEventArgs(MouseButtons.Right, 1, 3, 3, 0);
                dataGridView2_MouseClick(dataGridView2, mouseEventArgs);
                contextMenuStrip1.Visible = false;
                //contextMenuStrip1.Hide();
            }
        }

        private void dataGridView3_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                contextMenuStrip1.Show();
                CheckClick = "dataGridView3";
                MouseEventArgs mouseEventArgs = new MouseEventArgs(MouseButtons.Right, 1, 3, 3, 0);
                dataGridView3_MouseClick(dataGridView3, mouseEventArgs);
                contextMenuStrip1.Visible = false;
                //contextMenuStrip1.Hide();
            }
        }

        private void dataGridView4_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                contextMenuStrip1.Show();
                CheckClick = "dataGridView4";
                MouseEventArgs mouseEventArgs = new MouseEventArgs(MouseButtons.Right, 1, 3, 3, 0);
                dataGridView4_MouseClick(dataGridView4, mouseEventArgs);
                contextMenuStrip1.Visible = false;
                //contextMenuStrip1.Hide();
            }
        }

        private void dataGridView5_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                contextMenuStrip1.Show();
                CheckClick = "dataGridView5";
                MouseEventArgs mouseEventArgs = new MouseEventArgs(MouseButtons.Right, 1, 3, 3, 0);
                dataGridView5_MouseClick(dataGridView5, mouseEventArgs);
                contextMenuStrip1.Visible = false;
                //contextMenuStrip1.Hide();
            }
        }

        private void dataGridView6_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                contextMenuStrip1.Show();
                CheckClick = "dataGridView6";
                MouseEventArgs mouseEventArgs = new MouseEventArgs(MouseButtons.Right, 1, 3, 3, 0);
                dataGridView6_MouseClick(dataGridView6, mouseEventArgs);
                contextMenuStrip1.Visible = false;
                //contextMenuStrip1.Hide();
            }
        }

        private void dataGridView7_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                contextMenuStrip1.Show();
                CheckClick = "dataGridView7";
                MouseEventArgs mouseEventArgs = new MouseEventArgs(MouseButtons.Right, 1, 3, 3, 0);
                dataGridView7_MouseClick(dataGridView7, mouseEventArgs);
                contextMenuStrip1.Visible = false;
                //contextMenuStrip1.Hide();
            }
        }
    }
}

