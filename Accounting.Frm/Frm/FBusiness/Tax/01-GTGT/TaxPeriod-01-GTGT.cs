﻿using Accounting.Core;
using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting
{
    public partial class TaxPeriod_01_GTGT : Form
    {
        #region khai báo
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        List<TMAppendixList> lstTMAppendixList;
        public readonly ITMAppendixListService _ITMAppendixListService;
        public readonly ITM01GTGTService _ITM01GTGTService;
        public Tax Tax;
        #endregion
        public TaxPeriod_01_GTGT()
        {
            _ITMAppendixListService = IoC.Resolve<ITMAppendixListService>();
            _ITM01GTGTService = IoC.Resolve<ITM01GTGTService>();
            lstTMAppendixList = new List<TMAppendixList>();
            InitializeComponent();
            txtYear.Value = DateTime.Now.Year;
            InitializeGUI();


        }
        private void InitializeGUI()
        {
            #region Config cbbMonth
            cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 21 && int.Parse(n.Value) > 8).ToList(), "Name");
            cbbMonth.DropDownStyle = UltraComboStyle.DropDownList;
            try
            {
                foreach (var item in cbbMonth.Rows)
                {
                    if (int.Parse((item.ListObject as Item).Value) == DateTime.Now.Month + 8) cbbMonth.SelectedRow = item;
                }
            }
            catch (Exception e)
            {

            }
            #endregion
            #region Config cbbJob
            cbbJob.Items.Add(0, "Ngành hàng sản xuất, kinh doanh thông thường");
            cbbJob.Items.Add(1, "Từ hoạt động thăm dò, phát triển mỏ và khai thác dầu, khí thiên nhiên");
            cbbJob.Items.Add(2, "Từ hoạt động xổ số kiến thiết của các công ty xổ số kiến thiết");
            cbbJob.NullText = "Ngành hàng sản xuất, kinh doanh thông thường";
            #endregion
            #region Config grid
            lstTMAppendixList = _ITMAppendixListService.GetAppendixListByTypeGroup(100);
            uGrid.SetDataBinding(lstTMAppendixList, "");
            Utils.ConfigGrid(uGrid, ConstDatabase.TMAppendixList_TableName);
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;
            uGrid.DisplayLayout.Bands[0].Columns["Status"].CellClickAction = CellClickAction.EditAndSelectText;
            #endregion

        }

        private void ultraOptionSet1_ValueChanged(object sender, EventArgs e)
        {
            if (ultraOptionSet1.CheckedIndex == 0)
            {
                ultraLabel1.Text = "Tháng";
                cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 21 && int.Parse(n.Value) > 8).ToList(), "Name");
                foreach (var item in cbbMonth.Rows)
                {
                    if (int.Parse((item.ListObject as Item).Value) == DateTime.Now.Month + 8) cbbMonth.SelectedRow = item;
                }
            }
            else
            {
                ultraLabel1.Text = "Quý";
                cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 25 && int.Parse(n.Value) > 20).ToList(), "Name");
                float now = float.Parse(DateTime.Now.Month.ToString()) / 3;
                foreach (var item in cbbMonth.Rows)
                {
                    if (now <= 1)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 21) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 2 && now > 1)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 22) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 3 && now > 2)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 23) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 4 && now > 3)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 24) cbbMonth.SelectedRow = item;
                    }

                }

            }
        }

        private void cbbMonth_ValueChanged(object sender, EventArgs e)
        {
            if (cbbMonth.SelectedRow != null)
            {
                var model = cbbMonth.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd((int)txtYear.Value, DateTime.Now, model, out dtBegin, out dtEnd);
                dteDateFrom.Value = new DateTime((int)txtYear.Value, dtBegin.Month, dtBegin.Day);
                dteDateTo.Value = new DateTime((int)txtYear.Value, dtEnd.Month, dtEnd.Day);
                var temp = Utils.ListTM01GTGT.FirstOrDefault(p => p.FromDate.Month == dteDateFrom.DateTime.Month && p.FromDate.Year==dteDateFrom.DateTime.Year && p.ToDate.Month == dteDateTo.DateTime.Month && p.ToDate.Year==dteDateTo.DateTime.Year && p.IsFirstDeclaration == true);
                if (temp != null)
                {
                    uOptionSetIsFirst.CheckedIndex = 1;
                }
                else
                {
                    uOptionSetIsFirst.CheckedIndex = 0;
                }
                var temp1 = Utils.ListTM01GTGT.Where(n => dteDateFrom.DateTime <= n.FromDate && dteDateTo.DateTime >= n.ToDate || dteDateFrom.DateTime >= n.FromDate && dteDateTo.DateTime <= n.ToDate && n.IsFirstDeclaration == false).ToList();
                int maxAdditionalTime = temp1.Max(n => n.AdditionTime) ?? 0;
                txtAdditionalTime.Value = maxAdditionalTime + 1;
            }
        }

        private void txtYear_ValueChanged(object sender, EventArgs e)
        {
            if (txtYear.Value.ToInt() >= dteDateFrom.MinDate.Year && txtYear.Value.ToInt() <= dteDateFrom.MaxDate.Year)
            {
                DateTime t1 = dteDateFrom.DateTime;
                DateTime t2 = dteDateTo.DateTime;
                if(cbbMonth.Text=="Tháng 2")
                {
                    var model = cbbMonth.SelectedRow.ListObject as Item;
                    DateTime dtBegin;
                    DateTime dtEnd;
                    Utils.GetDateBeginDateEnd((int)txtYear.Value, DateTime.Now, model, out dtBegin, out dtEnd);
                    dteDateFrom.Value = new DateTime((int)txtYear.Value, t1.Month, t1.Day);
                    dteDateTo.Value = new DateTime((int)txtYear.Value, t2.Month, dtEnd.Day);
                }
                else
                {
                    dteDateFrom.Value = new DateTime((int)txtYear.Value, t1.Month, t1.Day);
                    dteDateTo.Value = new DateTime((int)txtYear.Value, t2.Month, t2.Day);
                }
                if (cbbMonth.SelectedRow != null)
                {
                    var model = cbbMonth.SelectedRow.ListObject as Item;
                    DateTime dtBegin;
                    DateTime dtEnd;
                    Utils.GetDateBeginDateEnd((int)txtYear.Value, DateTime.Now, model, out dtBegin, out dtEnd);
                    dteDateFrom.Value = new DateTime((int)txtYear.Value, dtBegin.Month, dtBegin.Day);
                    dteDateTo.Value = new DateTime((int)txtYear.Value, dtEnd.Month, dtEnd.Day);
                    var temp = Utils.ListTM01GTGT.FirstOrDefault(p => p.FromDate.Month == dteDateFrom.DateTime.Month && p.FromDate.Year == dteDateFrom.DateTime.Year && p.ToDate.Month == dteDateTo.DateTime.Month && p.ToDate.Year == dteDateTo.DateTime.Year && p.IsFirstDeclaration == true);
                    if (temp != null)
                    {
                        uOptionSetIsFirst.CheckedIndex = 1;
                    }
                    else
                    {
                        uOptionSetIsFirst.CheckedIndex = 0;
                    }
                    var lstAdded = Utils.ListTM01GTGT.Where(n => dteDateFrom.DateTime <= n.FromDate && dteDateTo.DateTime >= n.ToDate || dteDateFrom.DateTime >= n.FromDate && dteDateTo.DateTime <= n.ToDate && n.IsFirstDeclaration == false).ToList();
                    int maxAdditionalTime = lstAdded.Max(n => n.AdditionTime) ?? 0;
                    txtAdditionalTime.Value = maxAdditionalTime + 1;
                }
            }

        }

        private void ultraOptionSet2_ValueChanged(object sender, EventArgs e)
        {
            if (uOptionSetIsFirst.CheckedIndex == 1)
            {
                ultraLabel4.Visible = true;
                txtAdditionalTime.Visible = true;
                ultraLabel5.Visible = true;
                dteKHBS.Visible = true;
                ultraGroupBox1.Visible = false;
                this.MaximumSize = new Size(682, 263);
                this.MinimumSize = new Size(682, 263);
                var temp = Utils.ListTM01GTGT.Where(n => dteDateFrom.DateTime <= n.FromDate && dteDateTo.DateTime >= n.ToDate || dteDateFrom.DateTime >= n.FromDate && dteDateTo.DateTime <= n.ToDate && n.IsFirstDeclaration == false).ToList();
                int maxAdditionalTime = temp.Max(n => n.AdditionTime) ?? 0;
                txtAdditionalTime.Value = maxAdditionalTime + 1;
            }
            else
            {
                ultraLabel4.Visible = false;
                txtAdditionalTime.Visible = false;
                ultraLabel5.Visible = false;
                dteKHBS.Visible = false;
                ultraGroupBox1.Visible = true;
                this.MaximumSize = new Size(682, 501);
                this.MinimumSize = new Size(682, 501);
                UltraGridBand band = uGrid.DisplayLayout.Bands[0];
                UltraGridCell cell = uGrid.ActiveCell;
                foreach (var item in band.Columns)
                {
                    if (item.Key.Contains("STT"))
                    {
                        cell.Row.Cells["STT"].Value = false;
                    }
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtYear.Value.ToInt() < dteDateFrom.MinDate.Year || txtYear.Value.ToInt() > dteDateFrom.MaxDate.Year)
            {
                txtYear.Value = DateTime.Now.Year;
                txtYear.Focus();
                return;
            }
            
            string DeclarationTerm = cbbMonth.Text.ToString() + " năm " + txtYear.Value.ToString();
            DateTime FromDate = dteDateFrom.DateTime;
            DateTime ToDate = dteDateTo.DateTime;
            decimal item22=0;
            bool IsFirst = true;
            int MaxAdditionTime=1;
            DateTime KHBS = DateTime.Now.Date;
            string AdditionalTime="";

            if (Utils.ListTM01GTGT.Count > 0)
            {
                DateTime maxPeriod = Utils.ListTM01GTGT.Max(n => n.FromDate);
                var temp1 = Utils.ListTM01GTGT.Where(n => n.FromDate == maxPeriod && n.IsFirstDeclaration == true).FirstOrDefault();
                if (temp1 != null)
                {
                    if (dteDateFrom.DateTime.Date > temp1.ToDate)
                        item22 = decimal.Parse((temp1.TM01GTGTDetails.FirstOrDefault(p => p.Code == "Item43").Data).ToString());
                    else
                        item22 = 0;
                }
                else
                    item22 = 0;
            }
            else
                item22 = 0;
            if (uOptionSetIsFirst.Value.ToString() == "0")
            {
                if (ultraOptionSet1.Value.ToString() == "0")
                {
                    if (txtYear.Value.ToInt() > DateTime.Now.Year)
                    {
                        MSG.Warning("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                        return;
                    }
                    else if (txtYear.Value.ToInt() == DateTime.Now.Year)
                    {
                        if (dteDateFrom.DateTime.Month > DateTime.Now.Month)
                        {
                            MSG.Warning("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                            return;
                        }
                    }
                    if (txtYear.Value.ToInt() < dteDateFrom.MinDate.Year)
                    {
                        MSG.Warning("Năm không tồn tại");
                        return;
                    }

                }
                else
                {
                    float now = float.Parse(DateTime.Now.Month.ToString()) / 3;
                    float diff = float.Parse(dteDateFrom.DateTime.Month.ToString()) / 3;
                    if (txtYear.Value.ToInt() > DateTime.Now.Year)
                    {
                        MSG.Warning("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                        return;
                    }
                    else if (txtYear.Value.ToInt() == DateTime.Now.Year)
                    {
                        if (now < diff)
                        {
                            MSG.Warning("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                            return;
                        }
                    }
                    if (txtYear.Value.ToInt() < dteDateFrom.MinDate.Year)
                    {
                        MSG.Warning("Năm không tồn tại");
                        return;
                    }
                }
                
                List<TM01GTGT> lstTm = Utils.ListTM01GTGT.Where(n => dteDateFrom.DateTime <= n.FromDate && dteDateTo.DateTime >= n.ToDate || dteDateFrom.DateTime >= n.FromDate && dteDateTo.DateTime <= n.ToDate).ToList();
                if (lstTm.Count > 0)
                {
                    if (DialogResult.Yes == MessageBox.Show("Đã tồn tại tờ khai này trong kỳ trên. Bạn có muốn mở không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        TM01GTGT tM01GTGT = lstTm.FirstOrDefault();
                        _01_GTGT_Detail temp = new _01_GTGT_Detail(tM01GTGT, true);
                        this.Close();
                        this.Dispose();
                        temp.Show();
                       
                    }
                    return;
                }
                else
                {
                }
            }
            else
            {

                var temp = Utils.ListTM01GTGT.Where(n => dteDateFrom.DateTime <= n.FromDate && dteDateTo.DateTime >= n.ToDate || dteDateFrom.DateTime >= n.FromDate && dteDateTo.DateTime <= n.ToDate && n.IsFirstDeclaration == false).ToList();
                if (temp.Count > 0)
                {
                    int maxAdditionalTime = temp.Max(n => n.AdditionTime) ?? 0;
                    MaxAdditionTime = maxAdditionalTime;
                    if (int.Parse(txtAdditionalTime.Value.ToString()) != (maxAdditionalTime + 1))
                    {
                        MSG.Warning("Chưa tồn tại kỳ khai lần " + (int.Parse(txtAdditionalTime.Value.ToString()) - 1));
                        return;
                    }
                    else
                    {
                        if(ultraOptionSet1.Value.ToString() == "0")
                        {
                            if (dteKHBS.DateTime.Date < (dteDateTo.DateTime.Date.AddDays(20)))
                            {
                                MSG.Warning("Ngày nộp tờ khai bổ sung phải sau hạn nộp tờ khai");
                                return;
                            }

                        }
                        else
                        {
                            if (dteKHBS.DateTime.Date < (dteDateTo.DateTime.Date.AddDays(30)))
                            {
                                MSG.Warning("Ngày nộp tờ khai bổ sung phải sau hạn nộp tờ khai");
                                return;
                            }
                        }
                        AdditionalTime = txtAdditionalTime.Value.ToString();
                        IsFirst = false;
                        KHBS = dteKHBS.DateTime;
                    }
                }
                else
                {
                    MSG.Warning("Chưa tồn tại tờ khai lần đầu " );
                    return;
                }

            }
            _01_GTGT_Detail _01_GTGT_Detail = new _01_GTGT_Detail(new TM01GTGT(), false);
            _01_GTGT_Detail.DeclarationTerm = DeclarationTerm;
            _01_GTGT_Detail.FromDate = FromDate;
            _01_GTGT_Detail.ToDate = ToDate;
            _01_GTGT_Detail.item22 = item22;
            _01_GTGT_Detail.IsFirst = IsFirst;
            _01_GTGT_Detail.MaxAdditionTime = MaxAdditionTime;
            _01_GTGT_Detail.KHBS = KHBS;
            _01_GTGT_Detail.AdditionalTime = AdditionalTime;

            if (ultraOptionSet1.Value.ToString() == "0")
                _01_GTGT_Detail.isThang = true;
            else
                _01_GTGT_Detail.isQuy = true;
            _01_GTGT_Detail._lstTMAppendixList = lstTMAppendixList.Where(n => n.Status == true).ToList();
            this.Close();
            this.Dispose();
            if (_01_GTGT_Detail.LoadInitialize(false))
                _01_GTGT_Detail.Show();

        }

        private void TaxPeriod_01_GTGT_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (Form)sender;
            frm.Dispose();

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}
