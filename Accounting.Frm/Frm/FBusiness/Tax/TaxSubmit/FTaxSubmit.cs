﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.DAO;
using Accounting.TextMessage;
using System.IO;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using Accounting.Core.IService;

namespace Accounting
{
    public partial class FTaxSubmit : Form
    {
        List<TaxSubmit> lstTax = new List<TaxSubmit>();
        ReportProcedureSDS sp = new ReportProcedureSDS();
        private readonly IAccountingObjectBankAccountService _IAccountingObjectBankAccountService = Utils.IAccountingObjectBankAccountService;
        public FTaxSubmit()
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            var dte = Utils.GetDbStartDate().StringToDateTime();
            dtBeginDate.Value = dte.HasValue ? dte.Value : DateTime.Now;
            cbbCurrentState.SelectedIndex = 0;

            lstTax = sp.GetListTaxSubmit((DateTime)dtBeginDate.Value);

            lstTax = formatLstTax(lstTax);//format tien VND

            ugridTaxSubmit.DataSource = lstTax;
            Utils.ConfigGrid(ugridTaxSubmit, ConstDatabase.TaxSubmit_TableName);
            ugridTaxSubmit.DisplayLayout.Bands[0].Summaries.Clear();
            ugridTaxSubmit.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            ugridTaxSubmit.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
            ugridTaxSubmit.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            ugridTaxSubmit.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            UltraGridBand band = ugridTaxSubmit.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["icheck"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugridTaxSubmit.DisplayLayout.Bands[0].Columns["icheck"].CellActivation = Activation.AllowEdit;
            ugridTaxSubmit.DisplayLayout.Bands[0].Columns["soTienNopLanNay"].CellActivation = Activation.AllowEdit;
            ugridTaxSubmit.DisplayLayout.Bands[0].Columns["soTienNopLanNay"].FormatNumberic(ConstDatabase.Format_TienVND);
            ugridTaxSubmit.DisplayLayout.Bands[0].Columns["soTienPhaiNop"].FormatNumberic(ConstDatabase.Format_TienVND);
            band.Columns["icheck"].Header.Fixed = true;
            Fillcbb();
            foreach (var c in band.Columns)
            {
                if (c.Key != "icheck" && c.Key != "soTienNopLanNay") c.CellActivation = Activation.NoEdit;
            }
            foreach (var column in ugridTaxSubmit.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, ugridTaxSubmit);
            }
            WaitingFrm.StopWaiting();
        }

        public void Fillcbb()
        {
            this.ConfigCombo(Utils.ListBankAccountDetail, cbbCompanyAccountBank, "bankAccount", "ID");
        }

        private void cbbCurrentState_ValueChanged(object sender, EventArgs e)
        {
            if (cbbCurrentState.Value.ToInt() == 122)
                cbbCompanyAccountBank.Enabled = true;
            else
            {
                cbbCompanyAccountBank.Value = "";
                cbbCompanyAccountBank.Enabled = false;
            }
        }

        private List<TaxSubmit> formatLstTax(List<TaxSubmit> _lst)
        {
            if (_lst.Count > 0)
            {
                foreach (var item_lst in _lst)
                {
                    item_lst.soTienPhaiNop = Convert.ToDecimal(item_lst.soTienPhaiNop.FormatNumberic(ConstDatabase.Format_TienVND));
                    item_lst.soTienNopLanNay = Convert.ToDecimal(item_lst.soTienNopLanNay.FormatNumberic(ConstDatabase.Format_TienVND));
                }
            }
            return _lst;
        }

        private void dtBeginDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime date = (DateTime)dtBeginDate.Value;
                lstTax = sp.GetListTaxSubmit(date);
                lstTax = formatLstTax(lstTax);//format tien VND
                ugridTaxSubmit.DataSource = lstTax;
            }
            catch
            {
                ugridTaxSubmit.DataSource = lstTax = new List<TaxSubmit>();
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            List<TaxSubmit> lstChecks = lstTax.Where(x => x.icheck).ToList();
            if (lstChecks.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn loại thuế nào để nộp.");
                return;
            }

            if (cbbCurrentState.Value.ToInt() == 122)//tien gui
            {
                if (cbbCompanyAccountBank.Value == null)
                {
                    MSG.Warning("Bạn chưa chọn tài khoản chi");
                    return;
                }
            }

            string _strMes = string.Empty;

            foreach (var itemCheck in lstChecks)
            {
                if (itemCheck.soTienNopLanNay > itemCheck.soTienPhaiNop)
                {
                    if (string.IsNullOrEmpty(_strMes))
                        _strMes = string.Format("Số tiền nộp lần này của {0} phải nhỏ hơn hoặc bằng số tiền phải nộp.", itemCheck.dienGiai);
                    else
                        _strMes = _strMes + "\n\n" + string.Format("Số tiền nộp lần này của {0} phải nhỏ hơn hoặc bằng số tiền phải nộp.", itemCheck.dienGiai);
                }

                if (itemCheck.soTienNopLanNay == 0)
                {
                    if (string.IsNullOrEmpty(_strMes))
                        _strMes = string.Format("Số tiền nộp lần này của {0} đang bằng 0.", itemCheck.dienGiai);
                    else
                        _strMes = _strMes + "\n\n" + string.Format("Số tiền nộp lần này của {0} đang bằng 0.", itemCheck.dienGiai);
                }
            }
            if (!string.IsNullOrEmpty(_strMes))
            {
                MSG.Warning(_strMes);
                return;
            }

            #region lay so lieu sinh chung tu hoach toan
            DateTime date = (DateTime)dtBeginDate.Value;
            int idCurrentState = cbbCurrentState.Value.ToInt();
            if (idCurrentState==112)//nop thue tien mat
            {
                MCPayment mcPayMent = new MCPayment
                {
                    ID = Guid.NewGuid(),
                    TypeID = idCurrentState,
                    Date = new DateTime(date.Year,date.Month,date.Day),
                    PostedDate = new DateTime(date.Year, date.Month, date.Day),
                    No = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(11)),
                    Reason = "Nộp thuế",
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    TotalAmount = lstChecks.Sum(m => m.soTienNopLanNay),
                    TotalAmountOriginal = lstChecks.Sum(m => m.soTienNopLanNay),
                    TemplateID = new Guid("85DB2DD8-8F4C-4FD7-AE98-F4E851A226EE"),
                    TotalAll = lstChecks.Sum(m => m.soTienNopLanNay),
                    TotalAllOriginal = lstChecks.Sum(m => m.soTienNopLanNay),
                    AccountingObjectType = 3,
                    Recorded = true,
                    Exported = false
                };

                MCPaymentDetail mcPayMentDetail = new MCPaymentDetail();

                foreach (var itemCheck in lstChecks)
                {
                    mcPayMentDetail = new MCPaymentDetail
                    {
                        MCPaymentID = mcPayMent.ID,
                        Description = itemCheck.dienGiai,
                        DebitAccount = itemCheck.tkThue,
                        CreditAccount = "1111",
                        Amount = itemCheck.soTienNopLanNay,
                        AmountOriginal = itemCheck.soTienNopLanNay,
                        TaxAmount = itemCheck.soTienPhaiNop
                    };
                    mcPayMent.MCPaymentDetails.Add(mcPayMentDetail);
                }

                try
                {
                    #region code cu sinh chung tu xong luu
                    ////luu chung tu vua sinh
                    //Utils.IMCPaymentService.BeginTran();
                    //Utils.IMCPaymentService.CreateNew(mcPayMent);

                    ////luu GL
                    //List<GeneralLedger> generalLedgers = GenGeneralLedgers(mcPayMent);
                    //foreach (GeneralLedger detiail in generalLedgers)
                    //{
                    //    Utils.IGeneralLedgerService.Save(detiail);
                    //}
                    //bool status = Utils.IGenCodeService.UpdateGenCodeForm(11, mcPayMent.No, mcPayMent.ID);
                    //if (!status)
                    //{
                    //    MSG.Warning(resSystem.MSG_System_52);
                    //    Utils.IMCPaymentService.RolbackTran();
                    //    return;
                    //}
                    //Utils.IMCPaymentService.CommitTran();
                    //if (MSG.Question(string.Format("Hệ thống đã sinh chứng từ phiếu chi nộp thuế số {0}. Bạn có muốn xem chứng từ vừa tạo không", mcPayMent.No)) == DialogResult.Yes)
                    //{
                    //    new FMCPaymentDetail(mcPayMent, new List<MCPayment> { mcPayMent }, ConstFrm.optStatusForm.View).ShowDialog(this);
                    //}
                    #endregion
                    
                    #region code moi tao doi tuong roi view de trang thai sua thong tin va chua ghi so
                    bool status = Utils.IGenCodeService.UpdateGenCodeForm(11, mcPayMent.No, mcPayMent.ID);
                    if (!status)
                    {
                        MSG.Warning(resSystem.MSG_System_52);
                        Utils.IMCPaymentService.RolbackTran();
                        return;
                    }
                    new FMCPaymentDetail(mcPayMent, new List<MCPayment> { mcPayMent }, ConstFrm.optStatusForm.Edit).ShowDialog(this);

                    #endregion
                }
                catch (Exception ex)
                {
                    Utils.IMCPaymentService.RolbackTran();
                    MSG.Warning("Có lỗi xảy ra \n\r" + ex.Message);
                }

            }
            else// 122 nop thue tien gui
            {
                MBTellerPaper mbTellerPaper = new MBTellerPaper
                {
                    ID = Guid.NewGuid(),
                    TypeID = idCurrentState,
                    Date = new DateTime(date.Year, date.Month, date.Day),
                    PostedDate = new DateTime(date.Year, date.Month, date.Day),
                    No = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(12)),
                    Reason = "Nộp thuế",
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    TotalAmount = lstChecks.Sum(m => m.soTienNopLanNay),
                    TotalAmountOriginal = lstChecks.Sum(m => m.soTienNopLanNay),
                    TemplateID = new Guid("A9796CA4-59D4-4A17-87A9-7F1064AE7A66"),
                    TotalAll = lstChecks.Sum(m => m.soTienNopLanNay),
                    TotalAllOriginal = lstChecks.Sum(m => m.soTienNopLanNay),
                    BankAccountDetailID = (Guid)cbbCompanyAccountBank.Value,
                    BankName = Utils.ListBankAccountDetail.FirstOrDefault(n => n.ID == (Guid)cbbCompanyAccountBank.Value).BankName,
                    AccountingObjectType = 3,
                    Recorded = true,
                    Exported = false
                };

                MBTellerPaperDetail mbTellerPaperDetail = new MBTellerPaperDetail();
                foreach (var itemCheck in lstChecks)
                {
                    mbTellerPaperDetail = new MBTellerPaperDetail
                    {
                        MBTellerPaperID = mbTellerPaper.ID,
                        Description = itemCheck.dienGiai,
                        DebitAccount = itemCheck.tkThue,
                        CreditAccount = "1121",
                        Amount = itemCheck.soTienNopLanNay,
                        AmountOriginal = itemCheck.soTienNopLanNay,
                        BankAccountDetailID = mbTellerPaper.BankAccountDetailID,
                        TaxAmount = itemCheck.soTienPhaiNop
                    };
                    mbTellerPaper.MBTellerPaperDetails.Add(mbTellerPaperDetail);
                }

                try
                {
                    #region Code cu sinh chung tu xong luu luon sau do view cho xem
                    ////luu chung tu vua sinh
                    //Utils.IMBTellerPaperService.BeginTran();
                    //Utils.IMBTellerPaperService.CreateNew(mbTellerPaper);

                    ////luu GL
                    //List<GeneralLedger> generalLedgers = GenGeneralLedgers(mbTellerPaper);
                    //foreach (GeneralLedger detiail in generalLedgers)
                    //{
                    //    Utils.IGeneralLedgerService.Save(detiail);
                    //}
                    //bool status = Utils.IGenCodeService.UpdateGenCodeForm(12, mbTellerPaper.No, mbTellerPaper.ID);
                    //if (!status)
                    //{
                    //    MSG.Warning(resSystem.MSG_System_52);
                    //    Utils.IMBTellerPaperService.RolbackTran();
                    //    return;
                    //}
                    //Utils.IMBTellerPaperService.CommitTran();
                    //if (MSG.Question(string.Format("Hệ thống đã sinh chứng từ nộp thuế bằng ủy nhiệm chi số {0}. Bạn có muốn xem chứng từ vừa tạo không", mbTellerPaper.No)) == DialogResult.Yes)
                    //{
                    //    new FMBTellerPaperDetail(mbTellerPaper, new List<MBTellerPaper> { mbTellerPaper }, ConstFrm.optStatusForm.View).ShowDialog(this);
                    //}
                    #endregion

                    #region code moi tao doi tuong roi view de trang thai sua thong tin va chua ghi so
                    bool status = Utils.IGenCodeService.UpdateGenCodeForm(12, mbTellerPaper.No, mbTellerPaper.ID);
                    if (!status)
                    {
                        MSG.Warning(resSystem.MSG_System_52);
                        Utils.IMBTellerPaperService.RolbackTran();
                        return;
                    }
                    new FMBTellerPaperDetail(mbTellerPaper, new List<MBTellerPaper> { mbTellerPaper }, ConstFrm.optStatusForm.Edit).ShowDialog(this);
                    #endregion
                }
                catch (Exception ex)
                {
                    Utils.IMBTellerPaperService.RolbackTran();
                    MSG.Warning("Có lỗi xảy ra \n\r" + ex.Message);
                }

            }
            #endregion
            //load lai du lieu thue con lai khi vua nop thue xong
            lstTax = sp.GetListTaxSubmit(date);
            lstTax = formatLstTax(lstTax);//format tien VND
            ugridTaxSubmit.DataSource = lstTax;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        List<GeneralLedger> GenGeneralLedgers(MBTellerPaper mbTellerPaper)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

            #region Cặp Nợ-Có

            for (int i = 0; i < mbTellerPaper.MBTellerPaperDetails.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mbTellerPaper.ID,
                    TypeID = mbTellerPaper.TypeID,
                    Date = mbTellerPaper.Date,
                    PostedDate = mbTellerPaper.PostedDate,
                    No = mbTellerPaper.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = mbTellerPaper.MBTellerPaperDetails[i].DebitAccount,
                    AccountCorresponding = mbTellerPaper.MBTellerPaperDetails[i].CreditAccount,
                    BankAccountDetailID = mbTellerPaper.BankAccountDetailID,
                    CurrencyID = mbTellerPaper.CurrencyID,
                    ExchangeRate = mbTellerPaper.ExchangeRate,
                    DebitAmount = mbTellerPaper.MBTellerPaperDetails[i].Amount,
                    DebitAmountOriginal = mbTellerPaper.MBTellerPaperDetails[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = mbTellerPaper.Reason,
                    Description = mbTellerPaper.MBTellerPaperDetails[i].Description,
                    AccountingObjectID = mbTellerPaper.AccountingObjectID,
                    EmployeeID = mbTellerPaper.EmployeeID,
                    BudgetItemID = mbTellerPaper.MBTellerPaperDetails[i].BudgetItemID,
                    CostSetID = mbTellerPaper.MBTellerPaperDetails[i].CostSetID,
                    ContractID = mbTellerPaper.MBTellerPaperDetails[i].ContractID,
                    StatisticsCodeID = mbTellerPaper.MBTellerPaperDetails[i].StatisticsCodeID,
                    ExpenseItemID = mbTellerPaper.MBTellerPaperDetails[i].ExpenseItemID,
                    InvoiceSeries = "",
                    ContactName = mbTellerPaper.Receiver,
                    DetailID = mbTellerPaper.MBTellerPaperDetails[i].ID,
                    RefNo = mbTellerPaper.No,
                    RefDate = mbTellerPaper.Date,
                    DepartmentID = mbTellerPaper.MBTellerPaperDetails[i].DepartmentID,

                };

                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mbTellerPaper.ID,
                    TypeID = mbTellerPaper.TypeID,
                    Date = mbTellerPaper.Date,
                    PostedDate = mbTellerPaper.PostedDate,
                    No = mbTellerPaper.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = mbTellerPaper.MBTellerPaperDetails[i].CreditAccount,
                    AccountCorresponding = mbTellerPaper.MBTellerPaperDetails[i].DebitAccount,
                    BankAccountDetailID = mbTellerPaper.BankAccountDetailID,
                    CurrencyID = mbTellerPaper.CurrencyID,
                    ExchangeRate = mbTellerPaper.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = mbTellerPaper.MBTellerPaperDetails[i].Amount,
                    CreditAmountOriginal = mbTellerPaper.MBTellerPaperDetails[i].AmountOriginal,
                    Reason = mbTellerPaper.Reason,
                    Description = mbTellerPaper.MBTellerPaperDetails[i].Description,
                    AccountingObjectID = mbTellerPaper.AccountingObjectID,
                    EmployeeID = mbTellerPaper.EmployeeID,
                    BudgetItemID = mbTellerPaper.MBTellerPaperDetails[i].BudgetItemID,
                    CostSetID = mbTellerPaper.MBTellerPaperDetails[i].CostSetID,
                    ContractID = mbTellerPaper.MBTellerPaperDetails[i].ContractID,
                    StatisticsCodeID = mbTellerPaper.MBTellerPaperDetails[i].StatisticsCodeID,
                    ExpenseItemID = mbTellerPaper.MBTellerPaperDetails[i].ExpenseItemID,
                    InvoiceSeries = "",
                    ContactName = mbTellerPaper.Receiver,
                    DetailID = mbTellerPaper.MBTellerPaperDetails[i].ID,
                    RefNo = mbTellerPaper.No,
                    RefDate = mbTellerPaper.Date,
                    DepartmentID = mbTellerPaper.MBTellerPaperDetails[i].DepartmentID,

                };
                listGenTemp.Add(genTempCorresponding);
            }

            #endregion

            return listGenTemp;
        }

        List<GeneralLedger> GenGeneralLedgers(MCPayment mcPayment)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

            #region Cặp Nợ-Có

            for (int i = 0; i < mcPayment.MCPaymentDetails.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mcPayment.ID,
                    TypeID = mcPayment.TypeID,
                    Date = mcPayment.Date,
                    PostedDate = mcPayment.PostedDate,
                    No = mcPayment.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = mcPayment.MCPaymentDetails[i].DebitAccount,
                    AccountCorresponding = mcPayment.MCPaymentDetails[i].CreditAccount,
                    CurrencyID = mcPayment.CurrencyID,
                    ExchangeRate = mcPayment.ExchangeRate,
                    DebitAmount = mcPayment.MCPaymentDetails[i].Amount,
                    DebitAmountOriginal = mcPayment.MCPaymentDetails[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = mcPayment.Reason,
                    Description = mcPayment.MCPaymentDetails[i].Description,
                    AccountingObjectID = mcPayment.AccountingObjectID,
                    EmployeeID = mcPayment.EmployeeID,
                    BudgetItemID = mcPayment.MCPaymentDetails[i].BudgetItemID,
                    CostSetID = mcPayment.MCPaymentDetails[i].CostSetID,
                    ContractID = mcPayment.MCPaymentDetails[i].ContractID,
                    StatisticsCodeID = mcPayment.MCPaymentDetails[i].StatisticsCodeID,
                    ExpenseItemID = mcPayment.MCPaymentDetails[i].ExpenseItemID,
                    InvoiceSeries = "",
                    ContactName = mcPayment.Receiver,
                    DetailID = mcPayment.MCPaymentDetails[i].ID,
                    RefNo = mcPayment.No,
                    RefDate = mcPayment.Date,
                    DepartmentID = mcPayment.MCPaymentDetails[i].DepartmentID,

                };
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mcPayment.ID,
                    TypeID = mcPayment.TypeID,
                    Date = mcPayment.Date,
                    PostedDate = mcPayment.PostedDate,
                    No = mcPayment.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = mcPayment.MCPaymentDetails[i].CreditAccount,
                    AccountCorresponding = mcPayment.MCPaymentDetails[i].DebitAccount,
                    CurrencyID = mcPayment.CurrencyID,
                    ExchangeRate = mcPayment.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = mcPayment.MCPaymentDetails[i].Amount,
                    CreditAmountOriginal = mcPayment.MCPaymentDetails[i].AmountOriginal,
                    Reason = mcPayment.Reason,
                    Description = mcPayment.MCPaymentDetails[i].Description,
                    AccountingObjectID = mcPayment.AccountingObjectID,
                    EmployeeID = mcPayment.EmployeeID,
                    BudgetItemID = mcPayment.MCPaymentDetails[i].BudgetItemID,
                    CostSetID = mcPayment.MCPaymentDetails[i].CostSetID,
                    ContractID = mcPayment.MCPaymentDetails[i].ContractID,
                    StatisticsCodeID = mcPayment.MCPaymentDetails[i].StatisticsCodeID,

                    ExpenseItemID = mcPayment.MCPaymentDetails[i].ExpenseItemID,
                    InvoiceSeries = "",
                    ContactName = mcPayment.Receiver,
                    DetailID = mcPayment.MCPaymentDetails[i].ID,
                    RefNo = mcPayment.No,
                    RefDate = mcPayment.Date,
                    DepartmentID = mcPayment.MCPaymentDetails[i].DepartmentID,
                };

                listGenTemp.Add(genTempCorresponding);
            }

            #endregion

            return listGenTemp;
        }

        private void FTaxSubmit_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (Form)sender;
            frm.Dispose();

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}
