﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinTabControl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Accounting
{
    public partial class _03_TNDN_Detail : Form
    {
        public string item01 = "";
        public int year = 0;
        public bool item02 = false;
        public bool checkSave = false;
        //public ISystemOptionService _ISystemOptionService { get { return IoC.Resolve<ISystemOptionService>(); } }
        public DateTime FromDate;
        public DateTime ToDate;
        private bool _isEdit;
        public int Career;
        private TM03TNDN _select;
        bool editKeypress = false;
        bool change = true;
        public List<TMAppendixList> lstTMAppendixList_ = new List<TMAppendixList>();
        List<string> lstReadOnly = new List<string>();
        public _03_TNDN_Detail(TM03TNDN tM03TNDN, bool isEdit)
        {
            _select = tM03TNDN;
            _isEdit = isEdit;
            InitializeComponent();
            LoadCbb();
            lstReadOnly = lstReadOnlyDefaut();
            if (isEdit)
            {
                change = false;
                LoadReadOnly(true);
                utmDetailBaseToolBar.Tools["mnbtnSave"].SharedProps.Enabled = false;
                ObjandGUI(tM03TNDN, false);
                LoadMaskInp();
                change = true;
            }
            else
            {
                LoadReadOnly(false);
                utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Visible = false;
                LoadMaskInp();
            }
            Utils.ClearCacheByType<SystemOption>();

            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
                this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
            }
            //utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Visible = false;
        }
        public void LoadPL1(bool IsGet)
        {
            if (IsGet)
            {
                try
                {
                    List<GeneralLedger> lstPL = Utils.ListGeneralLedger.Where(n => n.PostedDate >= FromDate && n.PostedDate <= ToDate && n.Account != null && n.AccountCorresponding != null).ToList();
                    txtItemPL1.Value = lstPL.Where(n => n.Account.StartsWith("511") && !n.AccountCorresponding.StartsWith("911") && n.TypeID != 330 && n.TypeID != 340).Select(n => n.CreditAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                    txtItemPL2.Value = (from a in lstPL
                                        join b in Utils.ListSAInvoice on a.ReferenceID equals b.ID
                                        where b.Exported && a.Account.StartsWith("511") && !a.AccountCorresponding.StartsWith("911")
                                        select a.CreditAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                    txtItemPL4.Value = lstPL.Where(n => n.Account.StartsWith("511") && n.DebitAmount > 0 && new List<int> { 320, 321, 322, 323, 324, 325 }.Any(m => m == n.TypeID)).Select(n => n.DebitAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                    var lstPL5 = lstPL.Where(n => n.Account.StartsWith("511") && n.TypeID == 340).ToList();
                    txtItemPL5.Value = lstPL5.Select(n => n.DebitAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero)) - lstPL5.Select(n => n.CreditAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                    var lstPL6 = lstPL.Where(n => n.Account.StartsWith("511") && n.TypeID == 330).ToList();
                    txtItemPL6.Value = lstPL6.Select(n => n.DebitAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero)) - lstPL6.Select(n => n.CreditAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                    txtItemPL8.Value = lstPL.Where(n => n.Account.StartsWith("515") && n.DebitAmount > 0 && n.AccountCorresponding.StartsWith("911")).Select(n => n.DebitAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                    txtItemPL10.Value = lstPL.Where(n => n.Account.StartsWith("632") && n.CreditAmount > 0 && n.AccountCorresponding.StartsWith("911")).Select(n => n.CreditAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                    txtItemPL11.Value = lstPL.Where(n => n.Account.StartsWith("6421") && n.CreditAmount > 0 && n.AccountCorresponding.StartsWith("911")).Select(n => n.CreditAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                    txtItemPL12.Value = lstPL.Where(n => n.Account.StartsWith("6422") && n.CreditAmount > 0 && n.AccountCorresponding.StartsWith("911")).Select(n => n.CreditAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                    txtItemPL13.Value = lstPL.Where(n => n.Account.StartsWith("635") && n.CreditAmount > 0 && n.AccountCorresponding.StartsWith("911")).Select(n => n.CreditAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                    txtItemPL16.Value = lstPL.Where(n => n.Account.StartsWith("711") && n.DebitAmount > 0 && n.AccountCorresponding.StartsWith("911")).Select(n => n.DebitAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                    txtItemPL17.Value = lstPL.Where(n => n.Account.StartsWith("811") && n.CreditAmount > 0 && n.AccountCorresponding.StartsWith("911")).Select(n => n.CreditAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));

                }
                catch (Exception ex)
                {

                }

            }
        }
        public void LoadPL2(bool IsGet)
        {
            txtItemPL2_20.Value = year - 5;
            txtItemPL2_21.Value = year - 4;
            txtItemPL2_22.Value = year - 3;
            txtItemPL2_23.Value = year - 2;
            txtItemPL2_24.Value = year - 1;
            TM03TNDN tM03TNDN = Utils.ListTM03TNDN.FirstOrDefault(n => n.Year == year - 5 && n.IsFirstDeclaration);
            TM03TNDN tM03TNDN1 = Utils.ListTM03TNDN.FirstOrDefault(n => n.Year == year - 4 && n.IsFirstDeclaration);
            TM03TNDN tM03TNDN2 = Utils.ListTM03TNDN.FirstOrDefault(n => n.Year == year - 3 && n.IsFirstDeclaration);
            TM03TNDN tM03TNDN3 = Utils.ListTM03TNDN.FirstOrDefault(n => n.Year == year - 2 && n.IsFirstDeclaration);
            TM03TNDN tM03TNDN4 = Utils.ListTM03TNDN.FirstOrDefault(n => n.Year == year - 2 && n.IsFirstDeclaration);
            if (tM03TNDN != null)
            {
                if (tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC4").Amount < 0)
                {
                    txtItemPL2_30.Value = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC4").Amount;

                }
                else
                {
                    txtItemPL2_50.Value = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC4").Amount;
                }
                if (tM03TNDN.TM032ATNDNs.Count > 0)
                {
                    txtItemPL2_40.Value = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 5).LostAmountRemain;
                }
            }

            if (tM03TNDN1 != null)
            {
                if (tM03TNDN1.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC4").Amount < 0)
                {
                    txtItemPL2_31.Value = tM03TNDN1.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC4").Amount;

                }
                else
                {
                    txtItemPL2_51.Value = tM03TNDN1.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC4").Amount;
                }
                if (tM03TNDN1.TM032ATNDNs.Count > 0)
                {
                    txtItemPL2_41.Value = tM03TNDN1.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN1.Year - 5).LostAmountRemain;
                }
            }

            if (tM03TNDN2 != null)
            {
                if (tM03TNDN2.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC4").Amount < 0)
                {
                    txtItemPL2_32.Value = tM03TNDN2.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC4").Amount;

                }
                else
                {
                    txtItemPL2_52.Value = tM03TNDN2.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC4").Amount;
                }
                if (tM03TNDN2.TM032ATNDNs.Count > 0)
                {
                    txtItemPL2_42.Value = tM03TNDN2.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN2.Year - 5).LostAmountRemain;
                }
            }

            if (tM03TNDN3 != null)
            {
                if (tM03TNDN3.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC4").Amount < 0)
                {
                    txtItemPL2_33.Value = tM03TNDN3.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC4").Amount;
                }
                else
                {
                    txtItemPL2_53.Value = tM03TNDN3.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC4").Amount;
                }
                if (tM03TNDN3.TM032ATNDNs.Count > 0)
                {
                    txtItemPL2_43.Value = tM03TNDN3.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN3.Year - 5).LostAmountRemain;
                }
            }

            if (tM03TNDN4 != null)
            {
                if (tM03TNDN4.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC4").Amount < 0)
                {
                    txtItemPL2_34.Value = tM03TNDN4.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC4").Amount;
                }
                else
                {
                    txtItemPL2_54.Value = tM03TNDN4.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC4").Amount;
                }
                if (tM03TNDN4.TM032ATNDNs.Count > 0)
                {
                    txtItemPL2_44.Value = tM03TNDN4.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN4.Year - 5).LostAmountRemain;
                }
            }


        }
        public void LoadCbb()
        {
            cbbTMIndustryType.DataSource = Utils.ITMIndustryTypeService.GetAll().OrderBy(n => n.IndustryTypeCode).ToList();
            //this.ConfigCombo(Utils.ListTMIndustryType, cbbExtendCase, "IndustryTypeName", "ID");
            cbbTMIndustryType.DisplayMember = "IndustryTypeName";
            cbbTMIndustryType.ValueMember = "ID";
            Utils.ConfigGrid(cbbTMIndustryType, ConstDatabase.TMIndustryType_TableName);

            #region ccb ngành nghề
            cbbExtensionCase.Items.Add(-1, " ");
            cbbExtensionCase.Items.Add(0, "Doanh nghiệp có quy mô nhỏ và vừa");
            cbbExtensionCase.Items.Add(1, "Doanh nghiệp sử dụng nhiều lao động");
            cbbExtensionCase.Items.Add(2, "Doanh nghiệp đầu tư - kinh doanh(bán, cho thuê, cho thuê mua) nhà ở");
            cbbExtensionCase.Items.Add(3, "Lý do khác");
            #endregion
        }
        public List<string> lstReadOnlyDefaut()
        {
            List<string> lst = new List<string>();
            foreach (var item in GetAll(this, typeof(UltraMaskedEdit)))
            {
                try
                {
                    if (item is UltraMaskedEdit)
                    {
                        if ((item as UltraMaskedEdit).ReadOnly)
                        {
                            lst.Add((item as UltraMaskedEdit).Name);
                            (item as UltraMaskedEdit).SelectedTextForeColor = Color.Blue;
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }
            foreach (var item in GetAll(this, typeof(UltraTextEditor)))
            {
                try
                {
                    if ((item as UltraTextEditor).ReadOnly)
                    {
                        lst.Add((item as UltraTextEditor).Name);
                    }
                }
                catch (Exception ex)
                {

                }
            }
            foreach (var item in GetAll(this, typeof(UltraCombo)))
            {
                try
                {
                    if ((item as UltraCombo).ReadOnly)
                    {
                        lst.Add((item as UltraCombo).Name);
                    }
                }
                catch (Exception ex)
                {

                }
            }
            foreach (var item in GetAll(this, typeof(UltraDateTimeEditor)))
            {
                try
                {
                    if ((item as UltraDateTimeEditor).ReadOnly)
                    {
                        lst.Add((item as UltraDateTimeEditor).Name);
                    }
                }
                catch (Exception ex)
                {

                }
            }
            return lst;
        }
        #region check lỗi

        #endregion
        public void LoadReadOnly(bool isReadOnly)
        {
            txtIsExtend.Enabled = !isReadOnly;
            foreach (var item in GetAll(this, typeof(UltraMaskedEdit)))
            {
                try
                {
                    if (item is UltraMaskedEdit)
                    {
                        if (!lstReadOnly.Contains((item as UltraMaskedEdit).Name))
                        {
                            (item as UltraMaskedEdit).ReadOnly = isReadOnly;
                        }

                    }
                }
                catch (Exception ex)
                {

                }


            }
            foreach (var item in GetAll(this, typeof(UltraDateTimeEditor)))
            {
                try
                {
                    if (!lstReadOnly.Contains((item as UltraDateTimeEditor).Name))
                    {
                        (item as UltraDateTimeEditor).ReadOnly = isReadOnly;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            foreach (var item in GetAll(this, typeof(UltraTextEditor)))
            {
                try
                {
                    if (!lstReadOnly.Contains((item as UltraTextEditor).Name))
                    {
                        (item as UltraTextEditor).ReadOnly = isReadOnly;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            foreach (var item in GetAll(this, typeof(UltraCombo)))
            {
                try
                {
                    if (!lstReadOnly.Contains((item as UltraCombo).Name))
                    {
                        (item as UltraCombo).ReadOnly = isReadOnly;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            foreach (var item in GetAll(this, typeof(UltraComboEditor)))
            {
                try
                {
                    if (!lstReadOnly.Contains((item as UltraComboEditor).Name))
                    {
                        (item as UltraComboEditor).ReadOnly = isReadOnly;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            foreach (var item in GetAll(this, typeof(UltraCheckEditor)))
            {
                try
                {
                    if (!lstReadOnly.Contains((item as UltraCheckEditor).Name))
                    {
                        (item as UltraCheckEditor).Enabled = !isReadOnly;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            contextMenuStrip1.Enabled = !isReadOnly;
        }
        public bool LoadInitialize()
        {
            utmDetailBaseToolBar.Tools["mnbtnDelete"].SharedProps.Enabled = false;
            dteExtendTime.Value = null;
            dteExtendTime.Enabled = false;
            cbbExtensionCase.Value = null;
            cbbExtensionCase.Enabled = false;
            txtExtendTaxAmount.Enabled = false;
            txtNotExtendTaxAmount.Enabled = false;
            txtItem1.Text = item01;
            txtItem2.Checked = item02;
            txtItem6.Text = Utils.GetCompanyName();
            txtItem7.Text = Utils.GetCompanyTaxCode();
            txtItem14.Text = Utils.GetTenDLT();
            txtItem15.Text = Utils.GetMSTDLT();
            txtName.Value = Utils.GetHVTNhanVienDLT();
            txtCCHN.Value = Utils.GetCCHNDLT();
            txtNameSign.Value = Utils.GetCompanyDirector();
            txtSignDate.Value = DateTime.Now;
            dteFromDateLate.DateTime = ToDate;
            dteFromDateLate.DateTime = ToDate.AddDays(91);
            dteToDateLate.DateTime = ToDate.AddDays(91);
            if (!item02)
            {

            }
            else
            {
                ultraTabControl1.Tabs[3].Visible = false;
                utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Visible = false;
            }
            LoadPL1(true);
            LoadPL2(true);
            txtItemA1.Value = txtItemPL19.Value;
            txtItemA1.FormatString = txtItemPL19.FormatString;
            txtItemA1.InputMask = txtItemPL19.InputMask;
            if (lstTMAppendixList_.FirstOrDefault(n => n.TypeID == 121).Status)
            {
                ultraTabControl1.Tabs[1].Visible = true;

            }
            else
            {
                ultraTabControl1.Tabs[1].Visible = false;
            }
            if (lstTMAppendixList_.FirstOrDefault(n => n.TypeID == 122).Status)
            {
                ultraTabControl1.Tabs[2].Visible = true;
            }
            else
            {
                ultraTabControl1.Tabs[2].Visible = false;
            }

            txtItemC3a.Value = txtItemPL2_55.Value;
            txtItemC3a.InputMask = txtItemPL2_55.InputMask;
            txtItemB13.Value = txtItemB12.Value;
            txtItemB13.InputMask = txtItemB12.InputMask;
            txtItemC3.Value = txtItemC3.Value;
            txtItemC3.InputMask = txtItemC3a.InputMask;
            return true;
        }
        public List<TM03TNDN> CheckChangeBS()
        {
            return new List<TM03TNDN>();
        }
        public TM03TNDN ObjandGUI(TM03TNDN tM03TNDN, bool isGet)
        {
            if (isGet)
            {
                if (tM03TNDN.ID == null || tM03TNDN.ID == Guid.Empty)
                {
                    tM03TNDN.ID = Guid.NewGuid();
                }
                tM03TNDN.TypeID = 922;
                tM03TNDN.DeclarationName = txtDeclarationName.Text;
                tM03TNDN.DeclarationTerm = txtItem1.Text;

                tM03TNDN.IsFirstDeclaration = txtItem2.Checked;
                try
                {
                    tM03TNDN.AdditionTime = int.Parse(txtItem3.Text);
                }
                catch { }
                tM03TNDN.IsAppendix031ATNDN = ultraTabControl1.Tabs[1].Visible;
                tM03TNDN.IsAppendix032ATNDN = ultraTabControl1.Tabs[2].Visible;
                //tM03TNDN.AdditionDate 
                tM03TNDN.CompanyName = txtItem6.Text;
                tM03TNDN.CompanyTaxCode = txtItem7.Text;
                tM03TNDN.TaxAgencyTaxCode = txtItem15.Text;
                tM03TNDN.TaxAgencyName = txtItem14.Text;
                tM03TNDN.TaxAgencyEmployeeName = txtName.Text;
                tM03TNDN.CertificationNo = txtCCHN.Text;
                tM03TNDN.SignName = txtNameSign.Text;
                if (txtSignDate.Value != null)
                    tM03TNDN.SignDate = txtSignDate.DateTime;
                else
                    tM03TNDN.SignDate = null;
                if (year != 0)
                    tM03TNDN.Year = year;
                if (!txtItem5.Text.IsNullOrEmpty())
                {
                    tM03TNDN.Rate = getValueMaskedEdit(txtItem5);
                }
                tM03TNDN.Career = Career;
                tM03TNDN.IsSMEEnterprise = txtIsSMEEnterprise.Checked;
                tM03TNDN.IsRelatedTransactionEnterprise = txtIsRelatedTransactionEnterp.Checked;
                tM03TNDN.IsDependentEnterprise = txtIsDependentEnterprise.Checked;
                tM03TNDN.IsExtend = txtIsExtend.Checked;
                if (dteExtendTime.Value == null)
                {
                    tM03TNDN.ExtendTime = null;
                }
                else
                {
                    tM03TNDN.ExtendTime = dteExtendTime.DateTime;
                }

                if (cbbExtensionCase.Value != null)
                {
                    if (cbbExtensionCase.Value.ToString() != "-1")
                    {
                        tM03TNDN.ExtensionCase = int.Parse(cbbExtensionCase.Value.ToString());
                    }
                }
                tM03TNDN.ExtendTaxAmount = getValueMaskedEdit(txtExtendTaxAmount);
                tM03TNDN.NotExtendTaxAmount = getValueMaskedEdit(txtNotExtendTaxAmount);
                tM03TNDN.LateDays = int.Parse(getValueMaskedEdit(txtLateDays).ToString());
                tM03TNDN.FromDateLate = dteFromDateLate.DateTime;
                tM03TNDN.ToDateLate = dteToDateLate.DateTime;
                if (!txtLateAmount.Text.StrIsNullOrEmpty())
                {
                    tM03TNDN.LateAmount = getValueMaskedEdit(txtLateAmount);
                }
                try
                {
                    tM03TNDN.TMIndustryTypeID = Guid.Parse(cbbTMIndustryType.Value.ToString());
                }
                catch
                {

                }
                #region  Details
                int i = 0;
                if (_isEdit)
                {
                    #region TKC
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemA1").Amount = getValueMaskedEdit(txtItemA1);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB1").Amount = getValueMaskedEdit(txtItemB1);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB2").Amount = getValueMaskedEdit(txtItemB2);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB3").Amount = getValueMaskedEdit(txtItemB3);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB4").Amount = getValueMaskedEdit(txtItemB4);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB5").Amount = getValueMaskedEdit(txtItemB5);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB6").Amount = getValueMaskedEdit(txtItemB6);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB7").Amount = getValueMaskedEdit(txtItemB7);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB8").Amount = getValueMaskedEdit(txtItemB8);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB9").Amount = getValueMaskedEdit(txtItemB9);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB10").Amount = getValueMaskedEdit(txtItemB10);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB11").Amount = getValueMaskedEdit(txtItemB11);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB12").Amount = getValueMaskedEdit(txtItemB12);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB13").Amount = getValueMaskedEdit(txtItemB13);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB14").Amount = getValueMaskedEdit(txtItemB14);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC1").Amount = getValueMaskedEdit(txtItemC1);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC2").Amount = getValueMaskedEdit(txtItemC2);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC3").Amount = getValueMaskedEdit(txtItemC3);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC3a").Amount = getValueMaskedEdit(txtItemC3a);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC3b").Amount = getValueMaskedEdit(txtItemC3b);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC4").Amount = getValueMaskedEdit(txtItemC4);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC5").Amount = getValueMaskedEdit(txtItemC5);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC6").Amount = getValueMaskedEdit(txtItemC6);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC7").Amount = getValueMaskedEdit(txtItemC7);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC8").Amount = getValueMaskedEdit(txtItemC8);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC9").Amount = getValueMaskedEdit(txtItemC9);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC9a").Amount = getValueMaskedEdit(txtItemC9a);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC10").Amount = getValueMaskedEdit(txtItemC10);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC11").Amount = getValueMaskedEdit(txtItemC11);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC12").Amount = getValueMaskedEdit(txtItemC12);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC13").Amount = getValueMaskedEdit(txtItemC13);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC14").Amount = getValueMaskedEdit(txtItemC14);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC15").Amount = getValueMaskedEdit(txtItemC15);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC16").Amount = getValueMaskedEdit(txtItemC16);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemD").Amount = getValueMaskedEdit(txtItemD);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemD1").Amount = getValueMaskedEdit(txtItemD1);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemD2").Amount = getValueMaskedEdit(txtItemD2);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemD3").Amount = getValueMaskedEdit(txtItemD3);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemE").Amount = getValueMaskedEdit(txtItemE);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemE1").Amount = getValueMaskedEdit(txtItemE1);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemE2").Amount = getValueMaskedEdit(txtItemE2);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemE3").Amount = getValueMaskedEdit(txtItemE3);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemG").Amount = getValueMaskedEdit(txtItemG);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemG1").Amount = getValueMaskedEdit(txtItemG1);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemG2").Amount = getValueMaskedEdit(txtItemG2);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemG3").Amount = getValueMaskedEdit(txtItemG3);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemH").Amount = getValueMaskedEdit(txtItemH);
                    tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemI").Amount = getValueMaskedEdit(txtItemI);
                    #endregion
                    #region PL1
                    if (tM03TNDN.IsAppendix031ATNDN)
                    {
                        tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item1").Data = getValueMaskedEdit(txtItemPL1).ToString();
                        tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item2").Data = getValueMaskedEdit(txtItemPL2).ToString();
                        tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item3").Data = getValueMaskedEdit(txtItemPL3).ToString();
                        tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item4").Data = getValueMaskedEdit(txtItemPL4).ToString();
                        tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item5").Data = getValueMaskedEdit(txtItemPL5).ToString();
                        tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item6").Data = getValueMaskedEdit(txtItemPL6).ToString();
                        tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item7").Data = getValueMaskedEdit(txtItemPL7).ToString();
                        tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item8").Data = getValueMaskedEdit(txtItemPL8).ToString();
                        tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item9").Data = getValueMaskedEdit(txtItemPL9).ToString();
                        tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item10").Data = getValueMaskedEdit(txtItemPL10).ToString();
                        tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item11").Data = getValueMaskedEdit(txtItemPL11).ToString();
                        tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item12").Data = getValueMaskedEdit(txtItemPL12).ToString();
                        tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item13").Data = getValueMaskedEdit(txtItemPL13).ToString();
                        tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item14").Data = getValueMaskedEdit(txtItemPL14).ToString();
                        tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item15").Data = getValueMaskedEdit(txtItemPL15).ToString();
                        tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item16").Data = getValueMaskedEdit(txtItemPL16).ToString();
                        tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item17").Data = getValueMaskedEdit(txtItemPL17).ToString();
                        tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item18").Data = getValueMaskedEdit(txtItemPL18).ToString();
                        tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item19").Data = getValueMaskedEdit(txtItemPL19).ToString();
                    }
                    #endregion
                    #region PL2
                    if (tM03TNDN.IsAppendix032ATNDN)
                    {
                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 5).LostAmount = getValueMaskedEdit(txtItemPL2_30);
                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 5).LostAmountTranferPreviousPeriods = getValueMaskedEdit(txtItemPL2_40);
                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 5).LostAmountTranferThisPeriod = getValueMaskedEdit(txtItemPL2_50);
                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 5).LostAmountRemain = getValueMaskedEdit(txtItemPL2_60);

                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 4).LostAmount = getValueMaskedEdit(txtItemPL2_31);
                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 4).LostAmountTranferPreviousPeriods = getValueMaskedEdit(txtItemPL2_41);
                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 4).LostAmountTranferThisPeriod = getValueMaskedEdit(txtItemPL2_51);
                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 4).LostAmountRemain = getValueMaskedEdit(txtItemPL2_61);

                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 3).LostAmount = getValueMaskedEdit(txtItemPL2_32);
                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 3).LostAmountTranferPreviousPeriods = getValueMaskedEdit(txtItemPL2_42);
                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 3).LostAmountTranferThisPeriod = getValueMaskedEdit(txtItemPL2_52);
                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 3).LostAmountRemain = getValueMaskedEdit(txtItemPL2_62);

                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 2).LostAmount = getValueMaskedEdit(txtItemPL2_33);
                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 2).LostAmountTranferPreviousPeriods = getValueMaskedEdit(txtItemPL2_43);
                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 2).LostAmountTranferThisPeriod = getValueMaskedEdit(txtItemPL2_53);
                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 2).LostAmountRemain = getValueMaskedEdit(txtItemPL2_63);

                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 1).LostAmount = getValueMaskedEdit(txtItemPL2_34);
                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 1).LostAmountTranferPreviousPeriods = getValueMaskedEdit(txtItemPL2_44);
                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 1).LostAmountTranferThisPeriod = getValueMaskedEdit(txtItemPL2_54);
                        tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 1).LostAmountRemain = getValueMaskedEdit(txtItemPL2_64);

                    }
                    #endregion
                    #region TLKT
                    tM03TNDN.TM03TNDNDocuments.Clear();
                    int OrderPriority = 1;
                    for (int t = 1; t < tableLayoutPanel1.RowCount; t++)
                    {
                        //for (int t_ = 0; t_ < tableLayoutPanel1.ColumnCount; t_++)
                        //{
                        var control = tableLayoutPanel1.GetControlFromPosition(1, t);
                        if (control is UltraTextEditor)
                        {
                            UltraTextEditor ultraTextEditor = control as UltraTextEditor;
                            if (!ultraTextEditor.Text.IsNullOrEmpty())
                            {
                                tM03TNDN.TM03TNDNDocuments.Add(new TM03TNDNDocument
                                {
                                    TM03TNDNID = tM03TNDN.ID,
                                    DocumentName = ultraTextEditor.Text,
                                    OrderPriority = OrderPriority++
                                });
                            }
                        }
                        //}
                    }
                    #endregion
                }
                else
                {
                    #region TKC
                    tM03TNDN.FromDate = FromDate;
                    tM03TNDN.ToDate = ToDate;

                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemA1",
                        Name = NameA1.Text,
                        Amount = getValueMaskedEdit(txtItemA1),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemB1",
                        Name = NameB1.Text,
                        Amount = getValueMaskedEdit(txtItemB1),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemB2",
                        Name = NameB2.Text,
                        Amount = getValueMaskedEdit(txtItemB2),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemB3",
                        Name = NameB3.Text,
                        Amount = getValueMaskedEdit(txtItemB3),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemB4",
                        Name = NameB4.Text,
                        Amount = getValueMaskedEdit(txtItemB4),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemB5",
                        Name = NameB5.Text,
                        Amount = getValueMaskedEdit(txtItemB5),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemB6",
                        Name = NameB6.Text,
                        Amount = getValueMaskedEdit(txtItemB6),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemB7",
                        Name = NameB7.Text,
                        Amount = getValueMaskedEdit(txtItemB7),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemB8",
                        Name = NameB8.Text,
                        Amount = getValueMaskedEdit(txtItemB8),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemB9",
                        Name = NameB9.Text,
                        Amount = getValueMaskedEdit(txtItemB9),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemB10",
                        Name = NameB10.Text,
                        Amount = getValueMaskedEdit(txtItemB10),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemB11",
                        Name = NameB11.Text,
                        Amount = getValueMaskedEdit(txtItemB11),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemB12",
                        Name = NameB12.Text,
                        Amount = getValueMaskedEdit(txtItemB12),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemB13",
                        Name = NameB13.Text,
                        Amount = getValueMaskedEdit(txtItemB13),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemB14",
                        Name = NameB14.Text,
                        Amount = getValueMaskedEdit(txtItemB14),
                        OrderPriority = i++
                    });

                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemC1",
                        Name = NameC1.Text,
                        Amount = getValueMaskedEdit(txtItemC1),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemC2",
                        Name = NameC2.Text,
                        Amount = getValueMaskedEdit(txtItemC2),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemC3",
                        Name = NameC3.Text,
                        Amount = getValueMaskedEdit(txtItemC3),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemC3a",
                        Name = NameC3a.Text,
                        Amount = getValueMaskedEdit(txtItemC3a),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemC3b",
                        Name = NameC3b.Text,
                        Amount = getValueMaskedEdit(txtItemC3b),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemC4",
                        Name = NameC4.Text,
                        Amount = getValueMaskedEdit(txtItemC4),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemC5",
                        Name = NameC5.Text,
                        Amount = getValueMaskedEdit(txtItemC5),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemC6",
                        Name = NameC6.Text,
                        Amount = getValueMaskedEdit(txtItemC6),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemC7",
                        Name = NameC7.Text,
                        Amount = getValueMaskedEdit(txtItemC7),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemC8",
                        Name = NameC8.Text,
                        Amount = getValueMaskedEdit(txtItemC8),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemC9",
                        Name = NameC9.Text,
                        Amount = getValueMaskedEdit(txtItemC9),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemC9a",
                        Name = NameC9a.Text,
                        Amount = getValueMaskedEdit(txtItemC9a),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemC10",
                        Name = NameC10.Text,
                        Amount = getValueMaskedEdit(txtItemC10),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemC11",
                        Name = NameC11.Text,
                        Amount = getValueMaskedEdit(txtItemC11),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemC12",
                        Name = NameC12.Text,
                        Amount = getValueMaskedEdit(txtItemC12),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemC13",
                        Name = NameC13.Text,
                        Amount = getValueMaskedEdit(txtItemC13),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemC14",
                        Name = NameC14.Text,
                        Amount = getValueMaskedEdit(txtItemC14),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemC15",
                        Name = NameC15.Text,
                        Amount = getValueMaskedEdit(txtItemC15),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemC16",
                        Name = NameC16.Text,
                        Amount = getValueMaskedEdit(txtItemC16),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemD",
                        Name = NameD.Text,
                        Amount = getValueMaskedEdit(txtItemD),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemD1",
                        Name = NameD1.Text,
                        Amount = getValueMaskedEdit(txtItemD1),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemD2",
                        Name = NameD2.Text,
                        Amount = getValueMaskedEdit(txtItemD2),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemD3",
                        Name = NameD3.Text,
                        Amount = getValueMaskedEdit(txtItemD3),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemE",
                        Name = NameE.Text,
                        Amount = getValueMaskedEdit(txtItemE),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemE1",
                        Name = NameE1.Text,
                        Amount = getValueMaskedEdit(txtItemE1),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemE2",
                        Name = NameE2.Text,
                        Amount = getValueMaskedEdit(txtItemE2),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemE3",
                        Name = NameE3.Text,
                        Amount = getValueMaskedEdit(txtItemE3),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemG",
                        Name = NameG.Text,
                        Amount = getValueMaskedEdit(txtItemG),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemG1",
                        Name = NameG1.Text,
                        Amount = getValueMaskedEdit(txtItemG1),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemG2",
                        Name = NameG2.Text,
                        Amount = getValueMaskedEdit(txtItemG2),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemG3",
                        Name = NameG3.Text,
                        Amount = getValueMaskedEdit(txtItemG3),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemH",
                        Name = NameH.Text,
                        Amount = getValueMaskedEdit(txtItemH),
                        OrderPriority = i++
                    });
                    tM03TNDN.TM03TNDNDetails.Add(new TM03TNDNDetail
                    {
                        TM03TNDNID = tM03TNDN.ID,
                        Code = "ItemI",
                        Name = NameI.Text,
                        Amount = getValueMaskedEdit(txtItemI),
                        OrderPriority = i++
                    });
                    #endregion
                    #region PL1
                    if (tM03TNDN.IsAppendix031ATNDN)
                    {
                        int i_ = 0;
                        tM03TNDN.TM031ATNDNs.Add(new TM031ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Code = "Item1",
                            Name = txtNamePL1.Text,
                            Data = getValueMaskedEdit(txtItemPL1).ToString(),
                            OrderPriority = i_++
                        });
                        tM03TNDN.TM031ATNDNs.Add(new TM031ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Code = "Item2",
                            Name = txtNamePL2.Text,
                            Data = getValueMaskedEdit(txtItemPL2).ToString(),
                            OrderPriority = i_++
                        });
                        tM03TNDN.TM031ATNDNs.Add(new TM031ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Code = "Item3",
                            Name = txtNamePL3.Text,
                            Data = getValueMaskedEdit(txtItemPL3).ToString(),
                            OrderPriority = i_++
                        });
                        tM03TNDN.TM031ATNDNs.Add(new TM031ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Code = "Item4",
                            Name = txtNamePL4.Text,
                            Data = getValueMaskedEdit(txtItemPL4).ToString(),
                            OrderPriority = i_++
                        });
                        tM03TNDN.TM031ATNDNs.Add(new TM031ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Code = "Item5",
                            Name = txtNamePL5.Text,
                            Data = getValueMaskedEdit(txtItemPL5).ToString(),
                            OrderPriority = i_++
                        });
                        tM03TNDN.TM031ATNDNs.Add(new TM031ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Code = "Item6",
                            Name = txtNamePL6.Text,
                            Data = getValueMaskedEdit(txtItemPL6).ToString(),
                            OrderPriority = i_++
                        });
                        tM03TNDN.TM031ATNDNs.Add(new TM031ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Code = "Item7",
                            Name = txtNamePL7.Text,
                            Data = getValueMaskedEdit(txtItemPL7).ToString(),
                            OrderPriority = i_++
                        });
                        tM03TNDN.TM031ATNDNs.Add(new TM031ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Code = "Item8",
                            Name = txtNamePL8.Text,
                            Data = getValueMaskedEdit(txtItemPL8).ToString(),
                            OrderPriority = i_++
                        });
                        tM03TNDN.TM031ATNDNs.Add(new TM031ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Code = "Item9",
                            Name = txtNamePL9.Text,
                            Data = getValueMaskedEdit(txtItemPL9).ToString(),
                            OrderPriority = i_++
                        });
                        tM03TNDN.TM031ATNDNs.Add(new TM031ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Code = "Item10",
                            Name = txtNamePL10.Text,
                            Data = getValueMaskedEdit(txtItemPL10).ToString(),
                            OrderPriority = i_++
                        });
                        tM03TNDN.TM031ATNDNs.Add(new TM031ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Code = "Item11",
                            Name = txtNamePL11.Text,
                            Data = getValueMaskedEdit(txtItemPL11).ToString(),
                            OrderPriority = i_++
                        });
                        tM03TNDN.TM031ATNDNs.Add(new TM031ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Code = "Item12",
                            Name = txtNamePL12.Text,
                            Data = getValueMaskedEdit(txtItemPL12).ToString(),
                            OrderPriority = i_++
                        });
                        tM03TNDN.TM031ATNDNs.Add(new TM031ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Code = "Item13",
                            Name = txtNamePL13.Text,
                            Data = getValueMaskedEdit(txtItemPL13).ToString(),
                            OrderPriority = i_++
                        });
                        tM03TNDN.TM031ATNDNs.Add(new TM031ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Code = "Item14",
                            Name = txtNamePL14.Text,
                            Data = getValueMaskedEdit(txtItemPL14).ToString(),
                            OrderPriority = i_++
                        });
                        tM03TNDN.TM031ATNDNs.Add(new TM031ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Code = "Item15",
                            Name = txtNamePL15.Text,
                            Data = getValueMaskedEdit(txtItemPL15).ToString(),
                            OrderPriority = i_++
                        });
                        tM03TNDN.TM031ATNDNs.Add(new TM031ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Code = "Item16",
                            Name = txtNamePL16.Text,
                            Data = getValueMaskedEdit(txtItemPL16).ToString(),
                            OrderPriority = i_++
                        });
                        tM03TNDN.TM031ATNDNs.Add(new TM031ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Code = "Item17",
                            Name = txtNamePL17.Text,
                            Data = getValueMaskedEdit(txtItemPL17).ToString(),
                            OrderPriority = i_++
                        });
                        tM03TNDN.TM031ATNDNs.Add(new TM031ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Code = "Item18",
                            Name = txtNamePL18.Text,
                            Data = getValueMaskedEdit(txtItemPL18).ToString(),
                            OrderPriority = i_++
                        });
                        tM03TNDN.TM031ATNDNs.Add(new TM031ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Code = "Item19",
                            Name = txtNamePL19.Text,
                            Data = getValueMaskedEdit(txtItemPL19).ToString(),
                            OrderPriority = i_++
                        });
                    }

                    #endregion
                    #region PL2
                    if (tM03TNDN.IsAppendix032ATNDN)
                    {
                        tM03TNDN.TM032ATNDNs.Add(new TM032ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Year = int.Parse(txtItemPL2_20.Value.ToString()),
                            LostAmount = getValueMaskedEdit(txtItemPL2_30),
                            LostAmountTranferPreviousPeriods = getValueMaskedEdit(txtItemPL2_40),
                            LostAmountTranferThisPeriod = getValueMaskedEdit(txtItemPL2_50),
                            LostAmountRemain = getValueMaskedEdit(txtItemPL2_60),
                            OrderPriority = int.Parse(txtItemPL2_10.Value.ToString())
                        });
                        tM03TNDN.TM032ATNDNs.Add(new TM032ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Year = int.Parse(txtItemPL2_21.Value.ToString()),
                            LostAmount = getValueMaskedEdit(txtItemPL2_31),
                            LostAmountTranferPreviousPeriods = getValueMaskedEdit(txtItemPL2_41),
                            LostAmountTranferThisPeriod = getValueMaskedEdit(txtItemPL2_51),
                            LostAmountRemain = getValueMaskedEdit(txtItemPL2_61),
                            OrderPriority = int.Parse(txtItemPL2_11.Value.ToString())
                        });
                        tM03TNDN.TM032ATNDNs.Add(new TM032ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Year = int.Parse(txtItemPL2_22.Value.ToString()),
                            LostAmount = getValueMaskedEdit(txtItemPL2_32),
                            LostAmountTranferPreviousPeriods = getValueMaskedEdit(txtItemPL2_42),
                            LostAmountTranferThisPeriod = getValueMaskedEdit(txtItemPL2_52),
                            LostAmountRemain = getValueMaskedEdit(txtItemPL2_62),
                            OrderPriority = int.Parse(txtItemPL2_12.Value.ToString())
                        });
                        tM03TNDN.TM032ATNDNs.Add(new TM032ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Year = int.Parse(txtItemPL2_23.Value.ToString()),
                            LostAmount = getValueMaskedEdit(txtItemPL2_33),
                            LostAmountTranferPreviousPeriods = getValueMaskedEdit(txtItemPL2_43),
                            LostAmountTranferThisPeriod = getValueMaskedEdit(txtItemPL2_53),
                            LostAmountRemain = getValueMaskedEdit(txtItemPL2_63),
                            OrderPriority = int.Parse(txtItemPL2_13.Value.ToString())
                        });
                        tM03TNDN.TM032ATNDNs.Add(new TM032ATNDN
                        {
                            TM03TNDNID = tM03TNDN.ID,
                            Year = int.Parse(txtItemPL2_24.Value.ToString()),
                            LostAmount = getValueMaskedEdit(txtItemPL2_34),
                            LostAmountTranferPreviousPeriods = getValueMaskedEdit(txtItemPL2_44),
                            LostAmountTranferThisPeriod = getValueMaskedEdit(txtItemPL2_54),
                            LostAmountRemain = getValueMaskedEdit(txtItemPL2_64),
                            OrderPriority = int.Parse(txtItemPL2_14.Value.ToString())
                        });
                    }
                    #endregion
                    #region TLKT

                    int OrderPriority = 1;
                    for (int t = 1; t < tableLayoutPanel1.RowCount; t++)
                    {
                        //for (int t_ = 0; t_ < tableLayoutPanel1.ColumnCount; t_++)
                        //{
                        var control = tableLayoutPanel1.GetControlFromPosition(1, t);
                        if (control is UltraTextEditor)
                        {
                            UltraTextEditor ultraTextEditor = control as UltraTextEditor;
                            if (!ultraTextEditor.Text.IsNullOrEmpty())
                            {
                                tM03TNDN.TM03TNDNDocuments.Add(new TM03TNDNDocument
                                {
                                    TM03TNDNID = tM03TNDN.ID,
                                    DocumentName = ultraTextEditor.Text,
                                    OrderPriority = OrderPriority++
                                });
                            }
                        }
                        //}
                    }
                    #endregion
                }
                #endregion
            }
            else
            {
                txtItem1.Text = tM03TNDN.DeclarationTerm;
                txtItem2.Checked = tM03TNDN.IsFirstDeclaration;
                txtItem5.Value = tM03TNDN.Rate;
                if (tM03TNDN.AdditionTime != null)
                {
                    txtItem3.Value = tM03TNDN.AdditionTime;
                }
                if (tM03TNDN.IsFirstDeclaration)
                {
                    ultraTabControl1.Tabs[3].Visible = false;
                    utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Visible = false;
                }
                if (tM03TNDN.IsAppendix031ATNDN)
                {
                    ultraTabControl1.Tabs[1].Visible = true;
                }
                else
                {
                    ultraTabControl1.Tabs[1].Visible = false;
                }
                if (tM03TNDN.IsAppendix032ATNDN)
                {
                    ultraTabControl1.Tabs[2].Visible = true;
                }
                else
                {
                    ultraTabControl1.Tabs[2].Visible = false;
                }
                try
                {
                    cbbTMIndustryType.Value = tM03TNDN.TMIndustryTypeID;
                }
                catch (Exception ex)
                {

                }
                if (tM03TNDN.ExtensionCase != null)
                {
                    cbbExtensionCase.Value = tM03TNDN.ExtensionCase;
                }
                txtExtendTaxAmount.Value = tM03TNDN.ExtendTaxAmount;
                txtNotExtendTaxAmount.Value = tM03TNDN.NotExtendTaxAmount;
                txtLateDays.Value = tM03TNDN.LateDays;
                txtLateAmount.Value = tM03TNDN.LateAmount;

                txtIsRelatedTransactionEnterp.Checked = tM03TNDN.IsRelatedTransactionEnterprise;
                txtIsDependentEnterprise.Checked = tM03TNDN.IsDependentEnterprise;
                txtIsSMEEnterprise.Checked = tM03TNDN.IsSMEEnterprise;
                txtIsExtend.Checked = tM03TNDN.IsExtend;
                if (!tM03TNDN.IsExtend)
                {
                    dteExtendTime.Enabled = false;
                    dteExtendTime.Value = null;
                    cbbExtensionCase.Value = null;
                    cbbExtensionCase.Enabled = false;
                    txtExtendTaxAmount.Enabled = false;
                    txtNotExtendTaxAmount.Enabled = false;
                }
                txtItem6.Text = tM03TNDN.CompanyName;
                txtItem7.Text = tM03TNDN.CompanyTaxCode;
                txtItem14.Text = tM03TNDN.TaxAgencyName;
                txtItem15.Text = tM03TNDN.TaxAgencyTaxCode;
                txtName.Value = tM03TNDN.TaxAgencyEmployeeName;
                txtCCHN.Value = tM03TNDN.CertificationNo;
                txtNameSign.Value = tM03TNDN.SignName;
                txtSignDate.Value = tM03TNDN.SignDate;
                dteFromDateLate.Value = tM03TNDN.FromDateLate;
                dteToDateLate.Value = tM03TNDN.ToDateLate;
                #region Details
                BindData(txtItemA1, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemA1").Amount);
                BindData(txtItemB1, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB1").Amount);
                BindData(txtItemB2, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB2").Amount);
                BindData(txtItemB3, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB3").Amount);
                BindData(txtItemB4, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB4").Amount);
                BindData(txtItemB5, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB5").Amount);
                BindData(txtItemB6, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB6").Amount);
                BindData(txtItemB7, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB7").Amount);
                BindData(txtItemB8, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB8").Amount);
                BindData(txtItemB9, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB9").Amount);
                BindData(txtItemB10, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB10").Amount);
                BindData(txtItemB11, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB11").Amount);
                BindData(txtItemB12, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB12").Amount);
                BindData(txtItemB13, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB13").Amount);
                BindData(txtItemB14, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB14").Amount);
                BindData(txtItemC1, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC1").Amount);
                BindData(txtItemC2, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC2").Amount);
                BindData(txtItemC3, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC3").Amount);
                BindData(txtItemC3a, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC3a").Amount);
                BindData(txtItemC3b, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC3b").Amount);
                BindData(txtItemC4, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC4").Amount);
                BindData(txtItemC5, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC5").Amount);
                BindData(txtItemC6, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC6").Amount);
                BindData(txtItemC7, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC7").Amount);
                BindData(txtItemC8, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC8").Amount);
                BindData(txtItemC9, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC9").Amount);
                BindData(txtItemC9a, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC9a").Amount);
                BindData(txtItemC10, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC10").Amount);
                BindData(txtItemC11, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC11").Amount);
                BindData(txtItemC12, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC12").Amount);
                BindData(txtItemC13, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC13").Amount);
                BindData(txtItemC14, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC14").Amount);
                BindData(txtItemC15, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC15").Amount);
                BindData(txtItemC16, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC16").Amount);
                BindData(txtItemD, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemD").Amount);
                BindData(txtItemD1, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemD1").Amount);
                BindData(txtItemD2, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemD2").Amount);
                BindData(txtItemD3, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemD3").Amount);
                BindData(txtItemE, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemE").Amount);
                BindData(txtItemE1, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemE1").Amount);
                BindData(txtItemE2, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemE2").Amount);
                BindData(txtItemE3, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemE3").Amount);
                BindData(txtItemG, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemG").Amount);
                BindData(txtItemG1, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemG1").Amount);
                BindData(txtItemG2, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemG2").Amount);
                BindData(txtItemG3, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemG3").Amount);
                BindData(txtItemH, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemH").Amount);
                BindData(txtItemI, tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemI").Amount);
                #endregion
                if (tM03TNDN.IsAppendix031ATNDN)
                {
                    BindData(txtItemPL1, decimal.Parse(tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item1").Data));
                    BindData(txtItemPL2, decimal.Parse(tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item2").Data));
                    BindData(txtItemPL3, decimal.Parse(tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item3").Data));
                    BindData(txtItemPL4, decimal.Parse(tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item4").Data));
                    BindData(txtItemPL5, decimal.Parse(tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item5").Data));
                    BindData(txtItemPL6, decimal.Parse(tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item6").Data));
                    BindData(txtItemPL7, decimal.Parse(tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item7").Data));
                    BindData(txtItemPL8, decimal.Parse(tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item8").Data));
                    BindData(txtItemPL9, decimal.Parse(tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item9").Data));
                    BindData(txtItemPL10, decimal.Parse(tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item10").Data));
                    BindData(txtItemPL11, decimal.Parse(tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item11").Data));
                    BindData(txtItemPL12, decimal.Parse(tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item12").Data));
                    BindData(txtItemPL13, decimal.Parse(tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item13").Data));
                    BindData(txtItemPL14, decimal.Parse(tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item14").Data));
                    BindData(txtItemPL15, decimal.Parse(tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item15").Data));
                    BindData(txtItemPL16, decimal.Parse(tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item16").Data));
                    BindData(txtItemPL17, decimal.Parse(tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item17").Data));
                    BindData(txtItemPL18, decimal.Parse(tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item18").Data));
                    BindData(txtItemPL19, decimal.Parse(tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item19").Data));
                }
                if (tM03TNDN.IsAppendix032ATNDN)
                {
                    //txtItemPL2_10.Value = 1;
                    txtItemPL2_20.Value = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 5).Year;
                    BindData(txtItemPL2_30, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 5).LostAmount);
                    BindData(txtItemPL2_40, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 5).LostAmountTranferPreviousPeriods);
                    BindData(txtItemPL2_50, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 5).LostAmountTranferThisPeriod);
                    BindData(txtItemPL2_60, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 5).LostAmountRemain);

                    //txtItemPL2_11.Value = 1;
                    txtItemPL2_21.Value = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 4).Year;
                    var t = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 4);
                    BindData(txtItemPL2_31, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 4).LostAmount);
                    BindData(txtItemPL2_41, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 4).LostAmountTranferPreviousPeriods);
                    BindData(txtItemPL2_51, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 4).LostAmountTranferThisPeriod);
                    BindData(txtItemPL2_61, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 4).LostAmountRemain);

                    //txtItemPL2_12.Value = 1;
                    txtItemPL2_22.Value = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 3).Year;
                    BindData(txtItemPL2_32, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 3).LostAmount);
                    BindData(txtItemPL2_42, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 3).LostAmountTranferPreviousPeriods);
                    BindData(txtItemPL2_52, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 3).LostAmountTranferThisPeriod);
                    BindData(txtItemPL2_62, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 3).LostAmountRemain);

                    //txtItemPL2_13.Value = 1;
                    txtItemPL2_23.Value = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 2).Year;
                    BindData(txtItemPL2_33, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 2).LostAmount);
                    BindData(txtItemPL2_43, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 2).LostAmountTranferPreviousPeriods);
                    BindData(txtItemPL2_53, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 2).LostAmountTranferThisPeriod);
                    BindData(txtItemPL2_63, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 2).LostAmountRemain);

                    //txtItemPL2_14.Value = 1;
                    txtItemPL2_24.Value = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 1).Year;
                    BindData(txtItemPL2_34, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 1).LostAmount);
                    BindData(txtItemPL2_44, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 1).LostAmountTranferPreviousPeriods);
                    BindData(txtItemPL2_54, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 1).LostAmountTranferThisPeriod);
                    BindData(txtItemPL2_64, tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.Year == tM03TNDN.Year - 1).LostAmountRemain);
                }
                #region TLKT

                foreach (var item in tM03TNDN.TM03TNDNDocuments.OrderBy(n => n.OrderPriority))
                {
                    if (item.OrderPriority == 1)
                    {
                        var control = tableLayoutPanel1.GetControlFromPosition(1, 1);
                        (control as UltraTextEditor).Text = item.DocumentName;
                    }
                    else
                    {
                        RowStyle temp = tableLayoutPanel1.RowStyles[tableLayoutPanel1.RowCount - 1];
                        //increase panel rows count by one
                        tableLayoutPanel1.RowCount++;
                        int h = tableLayoutPanel1.Height;
                        //tableLayoutPanel1.Height = h + (int)temp.Height;
                        RowStyle rowStyle = new RowStyle(temp.SizeType, temp.Height);

                        //add a new RowStyle as a copy of the previous one
                        tableLayoutPanel1.RowStyles.Add(rowStyle);


                        UltraLabel txtSTT = new UltraLabel();
                        txtSTT.Text = (item.OrderPriority ?? 0).ToString();
                        txtSTT.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        txtSTT.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                        txtSTT.AutoSize = false;
                        txtSTT.Dock = DockStyle.Fill;
                        txtSTT.Margin = new Padding(0);

                        UltraTextEditor ultraTextEditor1 = new UltraTextEditor();
                        ultraTextEditor1.Dock = DockStyle.Fill;
                        ultraTextEditor1.AutoSize = false;
                        ultraTextEditor1.Text = item.DocumentName;
                        ultraTextEditor1.Margin = new Padding(0);
                        ultraTextEditor1.ContextMenuStrip = contextMenuStrip1;
                        tableLayoutPanel1.Controls.Add(txtSTT, 0, tableLayoutPanel1.RowCount - 1);
                        tableLayoutPanel1.Controls.Add(ultraTextEditor1, 1, tableLayoutPanel1.RowCount - 1);
                        ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel1.Location.Y + tableLayoutPanel1.Height + 10);
                    }

                }
                #endregion
            }
            return tM03TNDN;
        }
        private void utmDetailBaseToolBar_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
        {
            switch (e.Tool.Key)
            {
                case "mnbtnBack":
                    break;

                case "mnbtnForward":
                    break;

                case "mnbtnAdd":

                    break;

                case "mnbtnEdit":
                    editKeypress = true;
                    utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Visible = false;
                    utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = false;
                    utmDetailBaseToolBar.Tools["mnbtnSave"].SharedProps.Enabled = true;
                    utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = false;
                    utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Enabled = true;
                    utmDetailBaseToolBar.Tools["mnbtnDelete"].SharedProps.Enabled = false;
                    LoadReadOnly(false);
                    break;

                case "mnbtnSave":
                    try
                    {

                        if (!CheckError()) return;
                        editKeypress = false;
                        if (txtSignDate.Value == null)
                        {
                            MSG.Error("Bạn chưa chọn ngày ký");
                            return;
                        }
                        if (_isEdit)
                        {
                            TM03TNDN tM03TNDN = new TM03TNDN();
                            tM03TNDN = ObjandGUI(_select, true);
                            Utils.ITM03TNDNService.BeginTran();
                            Utils.ITM03TNDNService.Update(tM03TNDN);
                        }
                        else
                        {
                            TM03TNDN tM03TNDN = new TM03TNDN();
                            tM03TNDN = ObjandGUI(tM03TNDN, true);
                            Utils.ITM03TNDNService.BeginTran();
                            Utils.ITM03TNDNService.CreateNew(tM03TNDN);
                            _isEdit = true;
                            _select = tM03TNDN;
                        }

                        Utils.ITM03TNDNService.CommitTran();
                        MSG.Information("Lưu thành công");
                        Utils.ClearCacheByType<TM03TNDN>();
                        Utils.ClearCacheByType<TM03TNDNDocument>();
                        Utils.ClearCacheByType<TM03TNDNDetail>();
                        Utils.ClearCacheByType<TM031ATNDN>();
                        Utils.ClearCacheByType<TM032ATNDN>();
                        _select = Utils.ITM03TNDNService.Getbykey(_select.ID);
                        utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Visible = true;
                        utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = true;
                        utmDetailBaseToolBar.Tools["mnbtnSave"].SharedProps.Enabled = false;
                        utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = true;
                        utmDetailBaseToolBar.Tools["mnbtnDelete"].SharedProps.Enabled = true;
                        LoadReadOnly(true);
                        checkSave = true;
                    }
                    catch (Exception ex)
                    {
                        MSG.Error(ex.Message);
                        Utils.ITM03TNDNService.RolbackTran();
                    }

                    break;

                case "mnbtnDelete":
                    if (!(DialogResult.Yes == MessageBox.Show("Bạn có muốn xóa tờ khai này không ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question)))
                        return;
                    TM03TNDN tM03TNDN_D = Utils.ListTM03TNDN.FirstOrDefault(n => n.ID == _select.ID);
                    Utils.ITM03TNDNService.BeginTran();
                    Utils.ITM03TNDNService.Delete(tM03TNDN_D);
                    Utils.ITM03TNDNService.CommitTran();
                    MSG.Information("Xóa thành công");
                    this.Close();
                    break;

                case "mnbtnComeBack":
                    break;

                case "mnbtnPrint":
                    break;
                case "btnKBS":
                    break;
                case "mnbtnClose":
                    this.Close();
                    break;
                case "PrintTNDN-03":
                    #region fill dữ liệu
                    TNDN03Report tNDN03Report = new TNDN03Report();
                    tNDN03Report.BranchID = _select.BranchID;
                    tNDN03Report.TypeID = _select.TypeID;
                    tNDN03Report.DeclarationName = _select.DeclarationName;
                    tNDN03Report.DeclarationTerm = _select.DeclarationTerm;
                    tNDN03Report.FromDate = _select.FromDate;
                    tNDN03Report.ToDate = _select.ToDate;
                    tNDN03Report.IsFirstDeclaration = _select.IsFirstDeclaration;
                    tNDN03Report.AdditionTime = _select.AdditionTime;
                    tNDN03Report.Career = _select.Career;
                    tNDN03Report.IsAppendix031ATNDN = _select.IsAppendix031ATNDN;
                    tNDN03Report.IsAppendix032ATNDN = _select.IsAppendix032ATNDN;
                    tNDN03Report.IsSMEEnterprise = _select.IsSMEEnterprise;
                    tNDN03Report.IsDependentEnterprise = _select.IsDependentEnterprise;
                    tNDN03Report.IsRelatedTransactionEnterprise = _select.IsRelatedTransactionEnterprise;
                    tNDN03Report.TMIndustryTypeID = _select.TMIndustryTypeID;
                    tNDN03Report.Rate = _select.Rate;
                    tNDN03Report.CompanyName = _select.CompanyName;
                    tNDN03Report.CompanyTaxCode = _select.CompanyTaxCode;
                    tNDN03Report.TaxAgencyTaxCode = _select.TaxAgencyTaxCode;
                    tNDN03Report.TaxAgencyName = _select.TaxAgencyName;
                    tNDN03Report.TaxAgencyEmployeeName = _select.TaxAgencyEmployeeName;
                    tNDN03Report.CertificationNo = _select.CertificationNo;
                    tNDN03Report.SignName = _select.SignName;
                    tNDN03Report.SignDate = _select.SignDate;
                    tNDN03Report.IsExtend = _select.IsExtend;
                    tNDN03Report.ExtensionCase = _select.ExtensionCase;
                    tNDN03Report.ExtendTime = _select.ExtendTime;
                    tNDN03Report.ExtendTaxAmount = _select.ExtendTaxAmount;
                    tNDN03Report.NotExtendTaxAmount = _select.NotExtendTaxAmount;
                    tNDN03Report.LateDays = _select.LateDays;
                    tNDN03Report.FromDateLate = _select.FromDateLate;
                    tNDN03Report.ToDateLate = _select.ToDateLate;
                    tNDN03Report.LateAmount = _select.LateAmount;
                    tNDN03Report.Year = _select.Year;

                    if (_select.TMIndustryTypeID == null || _select.TMIndustryTypeID == Guid.Empty)
                    {
                        tNDN03Report.ExtensionCaseName = "";
                        tNDN03Report.IndustryTypeName1 = "";
                    }
                    else
                    {
                        tNDN03Report.IndustryTypeName1 = Utils.ITMIndustryTypeService.Getbykey(_select.TMIndustryTypeID).IndustryTypeName;



                        if (_select.ExtensionCase == -1)
                        {
                            tNDN03Report.ExtensionCaseName = "";
                        }
                        else if (_select.ExtensionCase == 0)
                        {
                            tNDN03Report.ExtensionCaseName = "Doanh nghiệp có quy mô nhỏ và vừa";
                        }
                        else if (_select.ExtensionCase == 1)
                        {
                            tNDN03Report.ExtensionCaseName = " Doanh nghiệp sử dụng nhiêu lao động";
                        }
                        else if (_select.ExtensionCase == 1)
                        {
                            tNDN03Report.ExtensionCaseName = "Doanh nghiệp đầu tư -kinh doanh(bán, cho thuê, cho thuê mua) nhà ở";
                        }
                        else if (_select.ExtensionCase == 1)
                        {
                            tNDN03Report.ExtensionCaseName = " Lý do khác";
                        }
                    }
                    tNDN03Report.Itema1 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemA1").Amount;
                    tNDN03Report.Itemb1 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB1").Amount;
                    tNDN03Report.Itemb2 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB2").Amount;
                    tNDN03Report.Itemb3 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB3").Amount;
                    tNDN03Report.Itemb4 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB4").Amount;
                    tNDN03Report.Itemb5 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB5").Amount;
                    tNDN03Report.Itemb6 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB6").Amount;
                    tNDN03Report.Itemb7 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB7").Amount;
                    tNDN03Report.Itemb8 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB8").Amount;
                    tNDN03Report.Itemb9 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB9").Amount;
                    tNDN03Report.Itemb10 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB10").Amount;
                    tNDN03Report.Itemb11 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB11").Amount;
                    tNDN03Report.Itemb12 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB12").Amount;
                    tNDN03Report.Itemb13 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB13").Amount;
                    tNDN03Report.Itemb14 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB14").Amount;
                    tNDN03Report.Itemc1 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC1").Amount;
                    tNDN03Report.Itemc2 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC2").Amount;
                    tNDN03Report.Itemc3 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC3").Amount;
                    tNDN03Report.Itemc3a = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC3a").Amount;
                    tNDN03Report.Itemc3b = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC3b").Amount;
                    tNDN03Report.Itemc4 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC4").Amount;
                    tNDN03Report.Itemc5 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC5").Amount;
                    tNDN03Report.Itemc6 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC6").Amount;
                    tNDN03Report.Itemc7 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC7").Amount;
                    tNDN03Report.Itemc8 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC8").Amount;
                    tNDN03Report.Itemc9 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC9").Amount;
                    tNDN03Report.Itemc9a = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC9a").Amount;
                    tNDN03Report.Itemc10 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC10").Amount;
                    tNDN03Report.Itemc11 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC11").Amount;
                    tNDN03Report.Itemc12 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC12").Amount;
                    tNDN03Report.Itemc13 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC13").Amount;
                    tNDN03Report.Itemc14 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC14").Amount;
                    tNDN03Report.Itemc15 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC15").Amount;
                    tNDN03Report.Itemc16 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC16").Amount;
                    tNDN03Report.Itemd = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemD").Amount;
                    tNDN03Report.Itemd1 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemD1").Amount;
                    tNDN03Report.Itemd2 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemD2").Amount;
                    tNDN03Report.Itemd3 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemD3").Amount;
                    tNDN03Report.Iteme = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemE").Amount;
                    tNDN03Report.Iteme1 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemE1").Amount;
                    tNDN03Report.Iteme2 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemE2").Amount;
                    tNDN03Report.Iteme3 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemE3").Amount;
                    tNDN03Report.Itemg = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemG").Amount;
                    tNDN03Report.Itemg1 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemG1").Amount;
                    tNDN03Report.Itemg2 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemG2").Amount;
                    tNDN03Report.Itemg3 = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemG3").Amount;
                    tNDN03Report.Itemh = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemH").Amount;
                    tNDN03Report.Itemi = _select.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemI").Amount;

                    if (tNDN03Report.IsAppendix031ATNDN)
                    {
                        tNDN03Report.Item1 = _select.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item1").Data;
                        tNDN03Report.Item2 = _select.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item2").Data;
                        tNDN03Report.Item3 = _select.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item3").Data;
                        tNDN03Report.Item4 = _select.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item4").Data;
                        tNDN03Report.Item5 = _select.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item5").Data;
                        tNDN03Report.Item6 = _select.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item6").Data;
                        tNDN03Report.Item7 = _select.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item7").Data;
                        tNDN03Report.Item8 = _select.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item8").Data;
                        tNDN03Report.Item9 = _select.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item9").Data;
                        tNDN03Report.Item10 = _select.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item10").Data;
                        tNDN03Report.Item11 = _select.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item11").Data;
                        tNDN03Report.Item12 = _select.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item12").Data;
                        tNDN03Report.Item13 = _select.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item13").Data;
                        tNDN03Report.Item14 = _select.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item14").Data;
                        tNDN03Report.Item15 = _select.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item15").Data;
                        tNDN03Report.Item16 = _select.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item16").Data;
                        tNDN03Report.Item17 = _select.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item17").Data;
                        tNDN03Report.Item18 = _select.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item18").Data;
                        tNDN03Report.Item19 = _select.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item19").Data;

                    }
                    tNDN03Report.TM032ATNDNs = _select.TM032ATNDNs.OrderBy(n => n.OrderPriority).ToList();

                    //  tNDN03Report.IndustryTypeName= Utils.ITMIndustryTypeService.GetAll().OrderBy(n => n.IndustryTypeCode).ToList();
                    //tNDN03Report.ExtensionCaseName = cbbExtensionCase.GetItemText(tNDN03Report.ExtensionCase).ToString();
                    TM03TNDNDocument item = new TM03TNDNDocument();
                    item.DocumentName = "";
                    item.ID = Guid.Empty;
                    item.OrderPriority = 1;
                    item.TM03TNDNID = _select.ID;
                    tNDN03Report.TM03TNDNDocuments1 = _select.TM03TNDNDocuments.OrderBy(n => n.OrderPriority).ToList();
                    if (tNDN03Report.TM03TNDNDocuments1.Count == 0)
                    {
                        tNDN03Report.TM03TNDNDocuments1.Add(item);
                    }
                    #endregion

                    Utils.Print("TNDN-03", tNDN03Report, this);
                    break;
                case "btnExportXml":
                    ExportXml(_select);
                    break;
            }
        }

        private void txtItemB2_ValueChanged(object sender, EventArgs e)
        {
            BindData(txtItemB1, getValueMaskedEdit(txtItemB2) + getValueMaskedEdit(txtItemB3) + getValueMaskedEdit(txtItemB4) + getValueMaskedEdit(txtItemB5) + getValueMaskedEdit(txtItemB6) + getValueMaskedEdit(txtItemB7));
        }

        private void txtItemB9_ValueChanged(object sender, EventArgs e)
        {
            BindData(txtItemB8, getValueMaskedEdit(txtItemB9) + getValueMaskedEdit(txtItemB10) + getValueMaskedEdit(txtItemB11));
        }

        private void txtItemA1_ValueChanged(object sender, EventArgs e)
        {
            BindData(txtItemB12, getValueMaskedEdit(txtItemA1) + getValueMaskedEdit(txtItemB1) - getValueMaskedEdit(txtItemB8));
        }

        private void txtItemB12_ValueChanged(object sender, EventArgs e)
        {
            if (change)
            {
                if (((UltraMaskedEdit)sender).Name == "txtItemB12")
                {
                    BindData(txtItemB13, getValueMaskedEdit(txtItemB12));
                }
            }
            BindData(txtItemC1, getValueMaskedEdit(txtItemB13));
            BindData(txtItemB14, getValueMaskedEdit(txtItemB12) - getValueMaskedEdit(txtItemB13));
        }

        private void txtItemC1_ValueChanged(object sender, EventArgs e)
        {
            BindData(txtItemC4, getValueMaskedEdit(txtItemC1) - getValueMaskedEdit(txtItemC2) - getValueMaskedEdit(txtItemC3a) - getValueMaskedEdit(txtItemC3b));
        }

        private void txtItemC4_ValueChanged(object sender, EventArgs e)
        {
            if (txtItemC4.Text != null)//trungnq thêm để sửa task 6202
            {
                if (txtItemC4.Text != "")
                {
                    if (txtItemC4.Text.Contains("(") || txtItemC4.Text.Contains(")"))
                    {
                        txtItemC5.ReadOnly = true;
                        
                        txtItemC5.Text = "0";
                        
                        txtItemC5.Value = 0;
                       
                    }
                    else
                    {
                        txtItemC5.ReadOnly = false;
                        
                        txtItemC5.MinValue = decimal.Zero;
                    }
                }
            }
            if (txtItemC5.Text.Contains("(") || txtItemC5.Text.Contains(")"))
            {
                txtItemC5.Value = 0;
                txtItemC5.Text = "0";
            }//trungnq thêm để sửa task 6202
            //if (getValueMaskedEdit(txtItemC5) > getValueMaskedEdit(txtItemC4) * 10 / 100)
            //{
            //    MSG.Error("Chỉ tiêu C5 phải nhỏ hơn hoặc bằng C4 *10%");
            //    return;
            //}
            BindData(txtItemC6, getValueMaskedEdit(txtItemC4) - getValueMaskedEdit(txtItemC5));
        }

        private void txtItemC7_ValueChanged(object sender, EventArgs e)
        {
            if(txtItemC7.Text.Contains("(")|| txtItemC7.Text.Contains(")"))//trungnq thêm sửa task 6202
            {
                txtItemC7.Value = 0;
                txtItemC7.Text = "0";
            }
            if (txtItemC8.Text.Contains("(") || txtItemC8.Text.Contains(")"))
            {
                txtItemC8.Value = 0;
                txtItemC8.Text = "0";
            }
            if (txtItemC9.Text.Contains("(") || txtItemC9.Text.Contains(")"))
            {
                txtItemC9.Value = 0;
                txtItemC9.Text = "0";
            }//trungnq thêm để sửa task 6202
            //if (txtItemC7.Value.IsNullOrEmpty())
            //{
            //    txtItemC7.Value = 0;
            //}
            //if (txtItemC8.Value.IsNullOrEmpty())
            //{
            //    txtItemC8.Value = 0;
            //}
            //if (txtItemC9.Value.IsNullOrEmpty())
            //{
            //    txtItemC9.Value = 0;
            //}
            //if (txtItemC9a.Value.IsNullOrEmpty())
            //{
            //    txtItemC9a.Value = 0;
            //}
            //BindData(txtItemC6, getValueMaskedEdit(txtItemC7) + getValueMaskedEdit(txtItemC8) + getValueMaskedEdit(txtItemC9));
            if (getValueMaskedEdit(txtItemC6) > 0)//trungnq thêm sửa task 6202
            {
                if (getValueMaskedEdit(txtItemC7) > getValueMaskedEdit(txtItemC6) || getValueMaskedEdit(txtItemC8) > getValueMaskedEdit(txtItemC6) || getValueMaskedEdit(txtItemC9) > getValueMaskedEdit(txtItemC6))
                {
                    MSG.Error("Không được nhập chỉ tiêu C7, C8, C9 lớn hơn chỉ tiêu C6");
                    return;
                }
            }
            BindData(txtItemC10, (getValueMaskedEdit(txtItemC7) * 22 / 100) + (getValueMaskedEdit(txtItemC8) * 20 / 100) + (getValueMaskedEdit(txtItemC9) * getValueMaskedEdit(txtItemC9a) / 100));

        }

        private void txtItemC10_ValueChanged(object sender, EventArgs e)
        {
            BindData(txtItemC16, getValueMaskedEdit(txtItemC10) - getValueMaskedEdit(txtItemC11) - getValueMaskedEdit(txtItemC12) - getValueMaskedEdit(txtItemC15));
        }

        private void txtItemD1_ValueChanged(object sender, EventArgs e)
        {
            BindData(txtItemD, getValueMaskedEdit(txtItemD1) + getValueMaskedEdit(txtItemD2) + getValueMaskedEdit(txtItemD3));
            BindData(txtItemG1, getValueMaskedEdit(txtItemD1) - getValueMaskedEdit(txtItemE1));
            BindData(txtItemG2, getValueMaskedEdit(txtItemD2) - getValueMaskedEdit(txtItemE2));
            BindData(txtItemG3, getValueMaskedEdit(txtItemD3) - getValueMaskedEdit(txtItemE3));
        }

        private void txtItemE1_ValueChanged(object sender, EventArgs e)
        {
            BindData(txtItemE, getValueMaskedEdit(txtItemE1) + getValueMaskedEdit(txtItemE2) + getValueMaskedEdit(txtItemE3));
            BindData(txtItemG1, getValueMaskedEdit(txtItemD1) - getValueMaskedEdit(txtItemE1));
            BindData(txtItemG2, getValueMaskedEdit(txtItemD2) - getValueMaskedEdit(txtItemE2));
            BindData(txtItemG3, getValueMaskedEdit(txtItemD3) - getValueMaskedEdit(txtItemE3));
        }

        private void txtItemG1_ValueChanged(object sender, EventArgs e)
        {
            BindData(txtItemG, getValueMaskedEdit(txtItemG1) + getValueMaskedEdit(txtItemG2) + getValueMaskedEdit(txtItemG3));
        }

        private void txtItemC16_ValueChanged(object sender, EventArgs e)
        {
            BindData(txtItemD1, getValueMaskedEdit(txtItemC16));
        }

        private void txtItemD_ValueChanged(object sender, EventArgs e)
        {
            BindData(txtItemH, getValueMaskedEdit(txtItemD) * 20 / 100);
        }

        private void txtItemG_ValueChanged(object sender, EventArgs e)
        {
            BindData(txtItemI, getValueMaskedEdit(txtItemG) - getValueMaskedEdit(txtItemH));
            if (txtIsExtend.Checked)
                BindData(txtNotExtendTaxAmount, getValueMaskedEdit(txtItemG) - getValueMaskedEdit(txtExtendTaxAmount));
        }


        private void txtItemPL4_ValueChanged(object sender, EventArgs e)
        {
            BindData(txtItemPL3, getValueMaskedEdit(txtItemPL4) + getValueMaskedEdit(txtItemPL5) + getValueMaskedEdit(txtItemPL6) + getValueMaskedEdit(txtItemPL7));
        }

        private void txtItemPL10_ValueChanged(object sender, EventArgs e)
        {
            BindData(txtItemPL9, getValueMaskedEdit(txtItemPL10) + getValueMaskedEdit(txtItemPL11) + getValueMaskedEdit(txtItemPL12));
        }

        private void txtItemPL1_ValueChanged(object sender, EventArgs e)
        {
            BindData(txtItemPL15, getValueMaskedEdit(txtItemPL1) - getValueMaskedEdit(txtItemPL3) + getValueMaskedEdit(txtItemPL8) - getValueMaskedEdit(txtItemPL9) - getValueMaskedEdit(txtItemPL13));
        }

        private void txtItemPL16_ValueChanged(object sender, EventArgs e)
        {
            BindData(txtItemPL18, getValueMaskedEdit(txtItemPL16) - getValueMaskedEdit(txtItemPL17));
        }

        private void txtItemPL15_ValueChanged(object sender, EventArgs e)
        {
            BindData(txtItemPL19, getValueMaskedEdit(txtItemPL15) + getValueMaskedEdit(txtItemPL18));
        }

        private void ultraTabPageControl3_Paint(object sender, PaintEventArgs e)
        {
            BindData(txtItemPL19, getValueMaskedEdit(txtItemPL15) + getValueMaskedEdit(txtItemPL18));
        }

        private void txtItemPL2_30_ValueChanged(object sender, EventArgs e)
        {
            BindData(txtItemPL2_35, getValueMaskedEdit(txtItemPL2_30) + getValueMaskedEdit(txtItemPL2_31) + getValueMaskedEdit(txtItemPL2_32) + getValueMaskedEdit(txtItemPL2_33) + getValueMaskedEdit(txtItemPL2_34));


            BindData(txtItemPL2_60, getValueMaskedEdit(txtItemPL2_30) - getValueMaskedEdit(txtItemPL2_40) - getValueMaskedEdit(txtItemPL2_50));

            BindData(txtItemPL2_61, getValueMaskedEdit(txtItemPL2_31) - getValueMaskedEdit(txtItemPL2_41) - getValueMaskedEdit(txtItemPL2_51));

            BindData(txtItemPL2_62, getValueMaskedEdit(txtItemPL2_32) - getValueMaskedEdit(txtItemPL2_42) - getValueMaskedEdit(txtItemPL2_52));

            BindData(txtItemPL2_63, getValueMaskedEdit(txtItemPL2_33) - getValueMaskedEdit(txtItemPL2_43) - getValueMaskedEdit(txtItemPL2_53));

            BindData(txtItemPL2_64, getValueMaskedEdit(txtItemPL2_34) - getValueMaskedEdit(txtItemPL2_44) - getValueMaskedEdit(txtItemPL2_54));
        }

        private void txtItemPL2_40_ValueChanged(object sender, EventArgs e)
        {

            BindData(txtItemPL2_45, getValueMaskedEdit(txtItemPL2_40) + getValueMaskedEdit(txtItemPL2_41) + getValueMaskedEdit(txtItemPL2_42) + getValueMaskedEdit(txtItemPL2_43) + getValueMaskedEdit(txtItemPL2_44));


            BindData(txtItemPL2_60, getValueMaskedEdit(txtItemPL2_30) - getValueMaskedEdit(txtItemPL2_40) - getValueMaskedEdit(txtItemPL2_50));

            BindData(txtItemPL2_61, getValueMaskedEdit(txtItemPL2_31) - getValueMaskedEdit(txtItemPL2_41) - getValueMaskedEdit(txtItemPL2_51));


            BindData(txtItemPL2_62, getValueMaskedEdit(txtItemPL2_32) - getValueMaskedEdit(txtItemPL2_42) - getValueMaskedEdit(txtItemPL2_52));

            BindData(txtItemPL2_63, getValueMaskedEdit(txtItemPL2_33) - getValueMaskedEdit(txtItemPL2_43) - getValueMaskedEdit(txtItemPL2_53));

            BindData(txtItemPL2_64, getValueMaskedEdit(txtItemPL2_34) - getValueMaskedEdit(txtItemPL2_44) - getValueMaskedEdit(txtItemPL2_54));

        }

        private void txtItemPL2_50_ValueChanged(object sender, EventArgs e)
        {

            BindData(txtItemPL2_55, getValueMaskedEdit(txtItemPL2_50) + getValueMaskedEdit(txtItemPL2_51) + getValueMaskedEdit(txtItemPL2_52) + getValueMaskedEdit(txtItemPL2_53) + getValueMaskedEdit(txtItemPL2_54));

            BindData(txtItemPL2_60, getValueMaskedEdit(txtItemPL2_30) - getValueMaskedEdit(txtItemPL2_40) - getValueMaskedEdit(txtItemPL2_50));

            BindData(txtItemPL2_61, getValueMaskedEdit(txtItemPL2_31) - getValueMaskedEdit(txtItemPL2_41) - getValueMaskedEdit(txtItemPL2_51));

            BindData(txtItemPL2_62, getValueMaskedEdit(txtItemPL2_32) - getValueMaskedEdit(txtItemPL2_42) - getValueMaskedEdit(txtItemPL2_52));

            BindData(txtItemPL2_63, getValueMaskedEdit(txtItemPL2_33) - getValueMaskedEdit(txtItemPL2_43) - getValueMaskedEdit(txtItemPL2_53));

            BindData(txtItemPL2_64, getValueMaskedEdit(txtItemPL2_34) - getValueMaskedEdit(txtItemPL2_44) - getValueMaskedEdit(txtItemPL2_54));
        }

        private void txtItemPL2_60_ValueChanged(object sender, EventArgs e)
        {

            //if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            //{
            //    ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            //    if (((UltraMaskedEdit)sender).Appearance.FontData.Bold == DefaultableBoolean.True)
            //        ((UltraMaskedEdit)sender).FormatString = "###,###,###,###,###,##0";
            //}

            decimal r = getValueMaskedEdit(txtItemPL2_60) + getValueMaskedEdit(txtItemPL2_61) + getValueMaskedEdit(txtItemPL2_62) + getValueMaskedEdit(txtItemPL2_63) + getValueMaskedEdit(txtItemPL2_64);
            BindData(txtItemPL2_65, r);
        }
        public bool BindData(UltraMaskedEdit ultraMaskedEdit, decimal r)
        {
            try
            {
                if (r < 0)
                {
                    ultraMaskedEdit.InputMask = "(n,nnn,nnn,nnn,nnn,nnn)";
                    if (ultraMaskedEdit.Appearance.FontData.Bold == DefaultableBoolean.True)
                        ultraMaskedEdit.FormatString = "(###,###,###,###,###,##0)";
                    ultraMaskedEdit.Value = -Math.Round(r, MidpointRounding.AwayFromZero);
                }
                else
                {
                    if (ultraMaskedEdit.Appearance.FontData.Bold == DefaultableBoolean.True)
                        ultraMaskedEdit.FormatString = "###,###,###,###,###,##0";

                    ultraMaskedEdit.InputMask = "n,nnn,nnn,nnn,nnn,nnn";
                    ultraMaskedEdit.Value = Math.Round(r, MidpointRounding.AwayFromZero);
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int row = GetRow(tableLayoutPanel1);
                if (row == 0) return;
                RowStyle temp = tableLayoutPanel1.RowStyles[tableLayoutPanel1.RowCount - 1];

                int h = tableLayoutPanel1.Height;
                //tableLayoutPanel1.Height = h + (int)temp.Height;
                RowStyle rowStyle = new RowStyle(temp.SizeType, temp.Height);
                tableLayoutPanel1.RowStyles.Insert(GetRow(tableLayoutPanel1), rowStyle);
                //increase panel rows count by one
                tableLayoutPanel1.RowCount++;

                foreach (Control ExistControl in tableLayoutPanel1.Controls)
                {
                    if (tableLayoutPanel1.GetRow(ExistControl) > row)
                        tableLayoutPanel1.SetRow(ExistControl, tableLayoutPanel1.GetRow(ExistControl) + 1);
                }
                //add a new RowStyle as a copy of the previous one
                //tableLayoutPanel1.RowStyles.Add(rowStyle);

                UltraLabel txtSTT = new UltraLabel();
                txtSTT.Text = (tableLayoutPanel1.RowCount - 1).ToString();
                txtSTT.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                txtSTT.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                txtSTT.AutoSize = false;
                txtSTT.Dock = DockStyle.Fill;
                txtSTT.Margin = new Padding(0);

                UltraTextEditor ultraTextEditor1 = new UltraTextEditor();
                ultraTextEditor1.Dock = DockStyle.Fill;
                ultraTextEditor1.AutoSize = false;
                ultraTextEditor1.Margin = new Padding(0);
                ultraTextEditor1.KeyPress += new KeyPressEventHandler((s, a) => ultraTextEditor1_KeyPress(s, a));
                ultraTextEditor1.ContextMenuStrip = contextMenuStrip1;
                tableLayoutPanel1.Controls.Add(txtSTT, 0, row + 1);
                tableLayoutPanel1.Controls.Add(ultraTextEditor1, 1, row + 1);
                ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel1.Location.Y + tableLayoutPanel1.Height + 10);
                //STT
                for (int i = 1; i < tableLayoutPanel1.RowCount; i++)
                {
                    var control = tableLayoutPanel1.GetControlFromPosition(0, i);
                    if (control is UltraLabel)
                    {
                        UltraLabel ultraTextEditorSTT = control as UltraLabel;
                        ultraTextEditorSTT.Text = i.ToString();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void Add()
        {

        }
        private void ultraTextEditor1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == 'F5')
            //{

            //}
            //else if(e.KeyChar == 'F5')
            //{

            //}
        }
        private void tsmDelete_Click(object sender, EventArgs e)
        {

            int r = tableLayoutPanel1.RowCount - 1;
            if (r == 1)
            {
                var control1 = tableLayoutPanel1.GetControlFromPosition(1, 1);
                if (control1 is UltraTextEditor)
                    control1.Text = "";
                return;
            }

            int row = GetRow(tableLayoutPanel1);
            if (row == 0) return;

            remove_row(tableLayoutPanel1, row);
            ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel1.Location.Y + tableLayoutPanel1.Height + 10);

            for (int i = 1; i < tableLayoutPanel1.RowCount; i++)
            {
                var control = tableLayoutPanel1.GetControlFromPosition(0, i);
                if (control is UltraLabel)
                {
                    UltraLabel ultraTextEditorSTT = control as UltraLabel;
                    ultraTextEditorSTT.Text = i.ToString();
                }
            }
            if (row == tableLayoutPanel1.RowCount)
            {
                var control1 = tableLayoutPanel1.GetControlFromPosition(1, row - 1);
                if (control1 is UltraTextEditor)
                    control1.Focus();
            }
            else
            {
                var control1 = tableLayoutPanel1.GetControlFromPosition(1, row);
                if (control1 is UltraTextEditor)
                    control1.Focus();
            }

        }
        public void remove_row(TableLayoutPanel panel, int row_index_to_remove)
        {
            if (row_index_to_remove >= panel.RowCount)
            {
                return;
            }

            // delete all controls of row that we want to delete
            for (int i = 0; i < panel.ColumnCount; i++)
            {
                var control = panel.GetControlFromPosition(i, row_index_to_remove);
                panel.Controls.Remove(control);
            }

            // move up row controls that comes after row we want to remove
            for (int i = row_index_to_remove + 1; i < panel.RowCount; i++)
            {
                for (int j = 0; j < panel.ColumnCount; j++)
                {
                    var control = panel.GetControlFromPosition(j, i);
                    if (control != null)
                    {
                        panel.SetRow(control, i - 1);
                    }
                }
            }

            // remove last row
            //panel.RowStyles.RemoveAt(panel.RowCount - 1);
            panel.RowCount--;
        }
        public void RemoveAll<T>(T item, List<T> list)
        {
            while (list.Contains(item)) list.Remove(item);
        }
        private void ultraMaskedEdit3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(sender is UltraMaskedEdit)) return;
            if (editKeypress || _select.ID == null || _select.ID == Guid.Empty)
            {
                UltraMaskedEdit ultraMaskedEdit = (UltraMaskedEdit)sender;
                //if (getValueMaskedEdit(ultraMaskedEdit) == 0) return;
                if (e.KeyChar == '-')
                {
                    if (ultraMaskedEdit.InputMask == "-n,nnn,nnn,nnn,nnn,nnn" || ultraMaskedEdit.InputMask == "n,nnn,nnn,nnn,nnn,nnn")
                    {
                        ultraMaskedEdit.InputMask = "(n,nnn,nnn,nnn,nnn,nnn)";
                        if (ultraMaskedEdit.Appearance.FontData.Bold == DefaultableBoolean.True)
                            ultraMaskedEdit.FormatString = "(###,###,###,###,###,##0)";
                    }
                    else
                    {
                        ultraMaskedEdit.InputMask = "n,nnn,nnn,nnn,nnn,nnn";
                        if (ultraMaskedEdit.Appearance.FontData.Bold == DefaultableBoolean.True)
                            ultraMaskedEdit.FormatString = "###,###,###,###,###,##0";
                    }
                }
            }
            else
            {

            }

        }
        private decimal getValueMaskedEdit(UltraMaskedEdit ultraMaskedEdit)
        {
            if (!ultraMaskedEdit.Value.IsNullOrEmpty())
            {
                if (ultraMaskedEdit.Text.Contains("("))
                {
                    if (decimal.Parse(ultraMaskedEdit.Value.ToString()) == 0)
                    {
                        return 0;
                    }
                    else
                        return -decimal.Parse(ultraMaskedEdit.Value.ToString());
                }
                else
                {
                    return decimal.Parse(ultraMaskedEdit.Value.ToString());
                }
            }
            return 0;
        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams handleParam = base.CreateParams;
                handleParam.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED       
                return handleParam;
            }
        }
        public bool LoadMaskInp()
        {

            foreach (var item in GetAll(this, typeof(UltraMaskedEdit)))
            {
                UltraMaskedEdit ultraMaskedEdit = item as UltraMaskedEdit;
                if (item is UltraMaskedEdit)
                {
                    if ((item as UltraMaskedEdit).Appearance.FontData.Bold == DefaultableBoolean.True)
                        (item as UltraMaskedEdit).FormatString = "###,###,###,###,###,##0";
                    decimal r = getValueMaskedEdit(item as UltraMaskedEdit);
                    if (r < 0)
                    {
                        (item as UltraMaskedEdit).InputMask = "(n,nnn,nnn,nnn,nnn,nnn)";
                        if ((item as UltraMaskedEdit).Appearance.FontData.Bold == DefaultableBoolean.True)
                            (item as UltraMaskedEdit).FormatString = "(###,###,###,###,###,##0)";
                        (item as UltraMaskedEdit).Value = -r;

                    }
                    if (ultraMaskedEdit.InputMask == "-n,nnn,nnn,nnn,nnn,nnn")
                    {
                        (item as UltraMaskedEdit).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
                        if ((item as UltraMaskedEdit).Appearance.FontData.Bold == DefaultableBoolean.True)
                            (item as UltraMaskedEdit).FormatString = "###,###,###,###,###,##0";
                    }
                }
            }
            return true;
        }
        public IEnumerable<Control> GetAll(Control control, System.Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }
        private void txtItemPL1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(sender is UltraMaskedEdit)) return;
            if (editKeypress || _select.ID == null || _select.ID == Guid.Empty)
            {
                UltraMaskedEdit ultraMaskedEdit = (UltraMaskedEdit)sender;
                //if (getValueMaskedEdit(ultraMaskedEdit) == 0) return;
                if (e.KeyChar == '-')
                {
                    if (ultraMaskedEdit.InputMask == "-n,nnn,nnn,nnn,nnn,nnn" || ultraMaskedEdit.InputMask == "n,nnn,nnn,nnn,nnn,nnn")
                    {
                        ultraMaskedEdit.InputMask = "(n,nnn,nnn,nnn,nnn,nnn)";
                        if (ultraMaskedEdit.Appearance.FontData.Bold == DefaultableBoolean.True)
                            ultraMaskedEdit.FormatString = "(###,###,###,###,###,##0)";
                    }
                    else
                    {
                        ultraMaskedEdit.InputMask = "n,nnn,nnn,nnn,nnn,nnn";
                        if (ultraMaskedEdit.Appearance.FontData.Bold == DefaultableBoolean.True)
                            ultraMaskedEdit.FormatString = "###,###,###,###,###,##0";
                    }
                }
            }
        }

        private void txtIsExtend_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtIsExtend_CheckedValueChanged(object sender, EventArgs e)
        {
            if (txtIsExtend.Checked)
            {
                cbbExtensionCase.Value = null;
                cbbExtensionCase.Enabled = true;
                dteExtendTime.Enabled = true;
                txtExtendTaxAmount.Enabled = true;
                txtNotExtendTaxAmount.Enabled = true;
                dteExtendTime.DateTime = DateTime.Now;
                BindData(txtNotExtendTaxAmount, getValueMaskedEdit(txtItemG) - getValueMaskedEdit(txtExtendTaxAmount));
            }
            else
            {
                dteExtendTime.Enabled = false;
                cbbExtensionCase.Value = null;
                cbbExtensionCase.Enabled = false;
                txtExtendTaxAmount.Enabled = false;
                txtNotExtendTaxAmount.Enabled = false;
                txtExtendTaxAmount.Value = 0;
                txtNotExtendTaxAmount.Value = 0;
                dteExtendTime.Value = null;
            }
        }

        private void txtExtendTaxAmount_ValueChanged(object sender, EventArgs e)
        {
            if (txtIsExtend.Checked)
                BindData(txtNotExtendTaxAmount, getValueMaskedEdit(txtItemG) - getValueMaskedEdit(txtExtendTaxAmount));
        }

        private void dteToDateLate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                txtLateDays.Value = dteToDateLate.DateTime.Subtract(dteFromDateLate.DateTime).Days;
            }
            catch (Exception)
            {
            }

        }
        public bool CheckError()
        {
            if (getValueMaskedEdit(txtItemC6) > 0)//trungnq thêm để sửa task 6202
            {
                if (getValueMaskedEdit(txtItemC6) != getValueMaskedEdit(txtItemC7) + getValueMaskedEdit(txtItemC8) + getValueMaskedEdit(txtItemC9))
                {
                    MSG.Error("Lỗi khi nhập liệu : \nTổng chỉ tiêu C7 + C8 + C9 phải bằng C4 - C5 = C6");
                    return false;
                }
            }
            if (getValueMaskedEdit(txtItemC4) > 0)//trungnq thêm để sửa task 6202
            {
                if (getValueMaskedEdit(txtItemC5) > getValueMaskedEdit(txtItemC4) * 10 / 100)
                {
                    MSG.Error("Chỉ tiêu C5 phải nhỏ hơn hoặc bằng C4 *10%");
                    return false;
                }
            }
            return true;
        }

        private void txtItemI_ValueChanged(object sender, EventArgs e)
        {
            if (change)
                BindData(txtLateAmount, getValueMaskedEdit(txtItemI) * getValueMaskedEdit(txtLateDays) * (decimal)0.03 / 100);
        }
        public int GetRow(TableLayoutPanel tableLayoutPanel)
        {
            for (int t = 1; t < tableLayoutPanel.RowCount; t++)
            {
                for (int i = 0; i < tableLayoutPanel.ColumnCount; i++)
                {
                    var control = tableLayoutPanel.GetControlFromPosition(i, t);
                    if (control != null)
                        if (control.ContainsFocus)
                        {
                            return t;

                        }
                }
            }
            return 0;
        }

        private void txtItemPL2_55_ValueChanged(object sender, EventArgs e)
        {
            if (change)
                BindData(txtItemC3a, getValueMaskedEdit(txtItemPL2_55));
        }

        private void txtItemA1_Validated(object sender, EventArgs e)
        {
            if (!(sender as UltraMaskedEdit).Value.IsNullOrEmpty())
                (sender as UltraMaskedEdit).Value = decimal.Parse((sender as UltraMaskedEdit).Value.ToString());
        }

        private bool ExportXml(TM03TNDN tM03TNDN)
        {
            #region Tờ Chính
            SaveFileDialog sf = new SaveFileDialog
            {
                FileName = "03TNDN.xml",
                AddExtension = true,
                Filter = "Excel Document(*.xml)|*.xml"
            };
            if (sf.ShowDialog() != DialogResult.OK)
            {
                return true;
            }
            String phuLuc = "";
            string path = System.IO.Path.GetDirectoryName(
                        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            string filePath = string.Format("{0}\\TemplateResource\\xml\\03_TNDN_xml.xml", path);
            XmlDocument doc = new XmlDocument();
            doc.Load(filePath);

            #region TTChung
            string tt = "HSoThueDTu/HSoKhaiThue/TTinChung/";
            //doc.SelectSingleNode(tt + "TTinDVu/maDVu").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinDVu/tenDVu").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinDVu/pbanDVu").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinDVu/ttinNhaCCapDVu").InnerText = "";

            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/maTKhai").InnerText = "03";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/tenTKhai").InnerText = tM03TNDN.DeclarationName;
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/moTaBMau").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/pbanTKhaiXML").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/loaiTKhai").InnerText = "C";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/soLan").InnerText = "0";

            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kieuKy").InnerText = "Y";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = tM03TNDN.DeclarationTerm.Split(' ')[0];
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhaiTuNgay").InnerText = tM03TNDN.FromDate.ToString("dd/MM/yyyy");
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhaiDenNgay").InnerText = tM03TNDN.ToDate.ToString("dd/MM/yyyy");

            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/maCQTNoiNop").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/tenCQTNoiNop").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/ngayLapTKhai").InnerText = "";

            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/GiaHan/maLyDoGiaHan").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/GiaHan/lyDoGiaHan").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/GiaHan/lyDoGiaHan").InnerText = "";

            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/nguoiKy").InnerText = tM03TNDN.SignName;
            if (tM03TNDN.SignDate != null)
            {
                DateTime dt = (DateTime)tM03TNDN.SignDate;
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/ngayKy").InnerText = dt.ToString("yyyy-MM-dd");
            }
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/nganhNgheKD").InnerText = tM03TNDN.CertificationNo;

            if (!Utils.GetDLTNgay().IsNullOrEmpty())
            {
                DateTime ngdlt = DateTime.ParseExact(Utils.GetDLTNgay(), "dd/MM/yyyy", null);
                doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/ngayKyHDDLyThue").InnerText = ngdlt.ToString("yyyy-MM-dd");
            }
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/mst").InnerText = Utils.GetCompanyTaxCodeForInvoice();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/tenNNT").InnerText = tM01GTGT.CompanyName;
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/dchiNNT").InnerText = Utils.GetCompanyAddress();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/phuongXa").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/maHuyenNNT").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/tenHuyenNNT").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/maTinhNNT").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/tenTinhNNT").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/dthoaiNNT").InnerText = Utils.GetCompanyPhoneNumber();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/faxNNT").InnerText = tM01GTGT.CompanyTaxCode;
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/emailNNT").InnerText = Utils.GetCompanyEmail();

            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/mstDLyThue").InnerText = Utils.GetMSTDLT();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/tenDLyThue").InnerText = tM01GTGT.TaxAgencyName;
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/dchiDLyThue").InnerText = Utils.GetDLTAddress();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/maHuyenDLyThue").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/tenHuyenDLyThue").InnerText = Utils.GetDLTDistrict();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/maTinhDLyThue").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/tenTinhDLyThue").InnerText = Utils.GetDLTCity();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/dthoaiDLyThue").InnerText = Utils.GetDLTPhoneNumber();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/faxDLyThue").InnerText = Utils.GetDLTFax();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/emailDLyThue").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/soHDongDLyThue").InnerText = Utils.GetDLTSoHD();

            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/NVienDLy/tenNVienDLyThue").InnerText = Utils.GetHVTNhanVienDLT();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/NVienDLy/cchiHNghe").InnerText = Utils.GetCCHNDLT();
            if (!Utils.GetTTDLT())
            {
                doc.SelectSingleNode(tt + "TTinTKhaiThue").RemoveChild(doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue"));
            }
            #endregion
            tt = "HSoThueDTu/HSoKhaiThue/CTieuTKhaiChinh/";
            doc.DocumentElement.SetAttribute("xmlns", "http://kekhaithue.gdt.gov.vn/TKhaiThue");
            doc.DocumentElement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            doc.SelectSingleNode(tt + "tieuMucHachToan").InnerText = "1052";
            doc.SelectSingleNode(tt + "doanhNghiepCoQuyMoVuaVaNho").InnerText = tM03TNDN.IsSMEEnterprise ? "1" : "0";
            doc.SelectSingleNode(tt + "doanhNghiepCoCSoHToanPThuoc").InnerText = tM03TNDN.IsDependentEnterprise ? "1" : "0";
            doc.SelectSingleNode(tt + "doanhNghiepKeKhaiTTinLienKet").InnerText = tM03TNDN.IsRelatedTransactionEnterprise ? "1" : "0";
            //doc.SelectSingleNode(tt + "ct_04_ma").InnerText = "";
            //doc.SelectSingleNode(tt + "ct_04_ten").InnerText = "";
            //doc.SelectSingleNode(tt + "ct_05").InnerText = "";

            doc.SelectSingleNode(tt + "ctA1").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemA1").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctB1").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB1").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctB2").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB2").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctB3").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB3").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctB4").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB4").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctB5").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB5").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctB6").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB6").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctB7").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB7").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctB8").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB8").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctB9").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB9").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctB10").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB10").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctB11").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB11").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctB12").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB12").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctB13").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB13").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctB14").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemB14").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctC1").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC1").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctC2").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC2").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctC3").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC3").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctC3a").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC3a").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctC3b").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC3b").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctC4").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC4").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctC5").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC5").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctC6").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC6").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctC7").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC7").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctC8").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC8").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctC9").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC9").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctC9a").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC9a").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctC10").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC10").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctC11").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC11").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctC12").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC12").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctC13").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC13").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctC14").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC14").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctC15").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC15").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctC16").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemC16").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctD").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemD").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctD1").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemD1").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctD2").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemD2").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctD3").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemD3").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctE").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemE").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctE1").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemE1").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctE2").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemE2").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctE3").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemE3").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctG").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemG").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctG1").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemG1").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctG2").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemG2").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctG3").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemG3").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctH").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemH").Amount.ToString("0");
            doc.SelectSingleNode(tt + "ctI").InnerText = tM03TNDN.TM03TNDNDetails.FirstOrDefault(n => n.Code == "ItemI").Amount.ToString("0");
            if (tM03TNDN.IsExtend)
            {
                doc.SelectSingleNode(tt + "GiaHanNopThue/ctL1").InnerText = "1";
                if (tM03TNDN.ExtensionCase != null)
                {
                    if (tM03TNDN.ExtensionCase >= 0 && tM03TNDN.ExtensionCase <= 2)
                    {
                        doc.SelectSingleNode(tt + "GiaHanNopThue/ctL2_ma").InnerText = (tM03TNDN.ExtensionCase + 1).ToString().PadLeft(2, '0');
                    }
                    else if (tM03TNDN.ExtensionCase == 3)
                    {
                        doc.SelectSingleNode(tt + "GiaHanNopThue/ctL2_ma").InnerText = "99";
                    }
                }
                //doc.SelectSingleNode(tt + "GiaHanNopThue/ctL2_ten").InnerText = "";
                if (tM03TNDN.ExtendTime != null)
                {
                    doc.SelectSingleNode(tt + "GiaHanNopThue/ctL3").InnerText = (tM03TNDN.ExtendTime ?? DateTime.Now).ToString("yyyy-MM-dd");
                    doc.SelectSingleNode(tt + "GiaHanNopThue/ctL3").Attributes.RemoveAll();
                }
                doc.SelectSingleNode(tt + "GiaHanNopThue/ctL4").InnerText = tM03TNDN.ExtendTaxAmount.ToString("0");
                doc.SelectSingleNode(tt + "GiaHanNopThue/ctL5").InnerText = tM03TNDN.NotExtendTaxAmount.ToString("0");
            }

            doc.SelectSingleNode(tt + "TienChamNop/CTM1/soNgay").InnerText = tM03TNDN.LateDays.ToString();
            if (tM03TNDN.FromDateLate != null)
            {
                doc.SelectSingleNode(tt + "TienChamNop/CTM1/tuNgay").InnerText = (tM03TNDN.FromDateLate ?? DateTime.Now).ToString("yyyy-MM-dd");
                doc.SelectSingleNode(tt + "TienChamNop/CTM1/tuNgay").Attributes.RemoveAll();
            }
            if (tM03TNDN.ToDateLate != null)
            {
                doc.SelectSingleNode(tt + "TienChamNop/CTM1/denNgay").InnerText = (tM03TNDN.ToDateLate ?? DateTime.Now).ToString("yyyy-MM-dd");
                doc.SelectSingleNode(tt + "TienChamNop/CTM1/denNgay").Attributes.RemoveAll();
            }
            doc.SelectSingleNode(tt + "TienChamNop/ctM2").InnerText = tM03TNDN.LateAmount.ToString("0");
            for (int i = 0; i < tM03TNDN.TM03TNDNDocuments.Count(); i++)
            {
                if (i == 0)
                {
                    doc.SelectSingleNode(tt + "TaiLieu_Guikem/Tailieuguikem").Attributes["id"].Value = (i + 1).ToString();
                    doc.SelectSingleNode(tt + "TaiLieu_Guikem/Tailieuguikem/tenTaiLieu").InnerText = tM03TNDN.TM03TNDNDocuments[i].DocumentName;
                }
                else
                {
                    XmlNode xmlNode = doc.SelectSingleNode(tt + "TaiLieu_Guikem/Tailieuguikem").CloneNode(true);
                    xmlNode.Attributes["id"].Value = (i + 1).ToString();
                    xmlNode.SelectSingleNode("tenTaiLieu").InnerText = tM03TNDN.TM03TNDNDocuments[i].DocumentName;
                    doc.SelectSingleNode(tt + "TaiLieu_Guikem").AppendChild(xmlNode);
                }
            }


            #endregion
            #region PL01A
            if (ultraTabControl1.Tabs["03-1A"].Visible)
            {
                //SaveFileDialog sf = new SaveFileDialog
                //{
                //    FileName = "03_1A_TNDN_xml.xml",
                //    AddExtension = true,
                //    Filter = "Excel Document(*.xml)|*.xml"
                //};
                //if (sf.ShowDialog() != DialogResult.OK)
                //{
                //    return true;
                //}
                string pathPL1 = System.IO.Path.GetDirectoryName(
                            System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                string filePathPL1 = string.Format("{0}\\TemplateResource\\xml\\03_1A_TNDN_xml.xml", pathPL1);
                XmlDocument docPL1 = new XmlDocument();
                docPL1.Load(filePathPL1);
                string ttPL1 = "PL03_1A_TNDN/";
                docPL1.SelectSingleNode(ttPL1 + "ct01").InnerText = tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item1").Data;
                docPL1.SelectSingleNode(ttPL1 + "ct02").InnerText = tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item2").Data;
                docPL1.SelectSingleNode(ttPL1 + "ct03").InnerText = tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item3").Data;
                docPL1.SelectSingleNode(ttPL1 + "ct04").InnerText = tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item4").Data;
                docPL1.SelectSingleNode(ttPL1 + "ct05").InnerText = tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item5").Data;
                docPL1.SelectSingleNode(ttPL1 + "ct06").InnerText = tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item6").Data;
                docPL1.SelectSingleNode(ttPL1 + "ct07").InnerText = tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item7").Data;
                docPL1.SelectSingleNode(ttPL1 + "ct08").InnerText = tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item8").Data;
                docPL1.SelectSingleNode(ttPL1 + "ct09").InnerText = tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item9").Data;
                docPL1.SelectSingleNode(ttPL1 + "ct10").InnerText = tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item10").Data;
                docPL1.SelectSingleNode(ttPL1 + "ct11").InnerText = tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item11").Data;
                docPL1.SelectSingleNode(ttPL1 + "ct12").InnerText = tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item12").Data;
                docPL1.SelectSingleNode(ttPL1 + "ct13").InnerText = tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item13").Data;
                docPL1.SelectSingleNode(ttPL1 + "ct14").InnerText = tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item14").Data;
                docPL1.SelectSingleNode(ttPL1 + "ct15").InnerText = tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item15").Data;
                docPL1.SelectSingleNode(ttPL1 + "ct16").InnerText = tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item16").Data;
                docPL1.SelectSingleNode(ttPL1 + "ct17").InnerText = tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item17").Data;
                docPL1.SelectSingleNode(ttPL1 + "ct18").InnerText = tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item18").Data;
                docPL1.SelectSingleNode(ttPL1 + "ct19").InnerText = tM03TNDN.TM031ATNDNs.FirstOrDefault(n => n.Code == "Item19").Data;
                phuLuc += docPL1.InnerXml;
                //doc.Save(sf.FileName);
                //if (MSG.Question("Xuất xml thành công, Bạn có muốn mở file vừa tải") == System.Windows.Forms.DialogResult.Yes)
                //{
                //    try
                //    {
                //        System.Diagnostics.Process.Start(sf.FileName);
                //    }
                //    catch
                //    {
                //        MSG.Error("Lỗi mở file");
                //    }

                //}
            }
            #endregion
            #region PL02A
            if (ultraTabControl1.Tabs["03-2A"].Visible)
            {
                //SaveFileDialog sf = new SaveFileDialog
                //{
                //    FileName = "03_2A_TNDN_xml.xml",
                //    AddExtension = true,
                //    Filter = "Excel Document(*.xml)|*.xml"
                //};
                //if (sf.ShowDialog() != DialogResult.OK)
                //{
                //    return true;
                //}
                string pathPL2 = System.IO.Path.GetDirectoryName(
                            System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                string filePathPL2 = string.Format("{0}\\TemplateResource\\xml\\03_2A_TNDN_xml.xml", pathPL2);
                XmlDocument docPL2 = new XmlDocument();
                docPL2.Load(filePathPL2);
                string ttPL2 = "PL03_2A_TNDN/BangChiTietSoLo/";
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu1/namPSinhLo").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 1).Year.ToString().Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu1/soLoPSinh").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 1).LostAmount.ToString("0").Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu1/soLoDaChuyen").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 1).LostAmountTranferPreviousPeriods.ToString("0").Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu1/soLoKyNay").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 1).LostAmountTranferThisPeriod.ToString("0").Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu1/soLoChuyenSau").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 1).LostAmountRemain.ToString("0").Replace(',', '.');

                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu2/namPSinhLo").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 2).Year.ToString().Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu2/soLoPSinh").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 2).LostAmount.ToString("0").Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu2/soLoDaChuyen").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 2).LostAmountTranferPreviousPeriods.ToString("0").Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu2/soLoKyNay").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 2).LostAmountTranferThisPeriod.ToString("0").Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu2/soLoChuyenSau").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 2).LostAmountRemain.ToString("0").Replace(',', '.');

                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu3/namPSinhLo").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 3).Year.ToString().Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu3/soLoPSinh").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 3).LostAmount.ToString("0").Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu3/soLoDaChuyen").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 3).LostAmountTranferPreviousPeriods.ToString("0").Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu3/soLoKyNay").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 3).LostAmountTranferThisPeriod.ToString("0").Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu3/soLoChuyenSau").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 3).LostAmountRemain.ToString("0").Replace(',', '.');

                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu4/namPSinhLo").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 4).Year.ToString().Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu4/soLoPSinh").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 4).LostAmount.ToString("0").Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu4/soLoDaChuyen").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 4).LostAmountTranferPreviousPeriods.ToString("0").Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu4/soLoKyNay").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 4).LostAmountTranferThisPeriod.ToString("0").Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu4/soLoChuyenSau").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 4).LostAmountRemain.ToString("0").Replace(',', '.');

                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu5/namPSinhLo").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 5).Year.ToString().Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu5/soLoPSinh").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 5).LostAmount.ToString("0").Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu5/soLoDaChuyen").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 5).LostAmountTranferPreviousPeriods.ToString("0").Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu5/soLoKyNay").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 5).LostAmountTranferThisPeriod.ToString("0").Replace(',', '.');
                docPL2.SelectSingleNode(ttPL2 + "noiDungNamThu5/soLoChuyenSau").InnerText = tM03TNDN.TM032ATNDNs.FirstOrDefault(n => n.OrderPriority == 5).LostAmountRemain.ToString("0").Replace(',', '.');

                docPL2.SelectSingleNode("PL03_2A_TNDN/tongSoLoPSinh").InnerText = tM03TNDN.TM032ATNDNs.Sum(n => n.LostAmount).ToString("0").Replace(',', '.');
                docPL2.SelectSingleNode("PL03_2A_TNDN/tongSoLoDaChuyen").InnerText = tM03TNDN.TM032ATNDNs.Sum(n => n.LostAmountTranferPreviousPeriods).ToString("0").Replace(',', '.');
                docPL2.SelectSingleNode("PL03_2A_TNDN/tongSoLoKyNay").InnerText = tM03TNDN.TM032ATNDNs.Sum(n => n.LostAmountTranferThisPeriod).ToString("0").Replace(',', '.');
                docPL2.SelectSingleNode("PL03_2A_TNDN/tongSoLoChuyenSau").InnerText = tM03TNDN.TM032ATNDNs.Sum(n => n.LostAmountRemain).ToString("0").Replace(',', '.');
                phuLuc += docPL2.InnerXml;
                //doc.Save(sf.FileName);
                //if (MSG.Question("Xuất xml thành công, Bạn có muốn mở file vừa tải") == System.Windows.Forms.DialogResult.Yes)
                //{
                //    try
                //    {
                //        System.Diagnostics.Process.Start(sf.FileName);
                //    }
                //    catch
                //    {
                //        MSG.Error("Lỗi mở file");
                //    }

                //}
            }
            if (!string.IsNullOrEmpty(phuLuc))
            {
                doc.SelectSingleNode("HSoThueDTu/HSoKhaiThue/PLuc").InnerXml = phuLuc;
            }
            #endregion
            doc.Save(sf.FileName);
            if (MSG.Question("Xuất xml thành công, Bạn có muốn mở file vừa tải") == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    System.Diagnostics.Process.Start(sf.FileName);
                }
                catch
                {
                    MSG.Error("Lỗi mở file");
                }

            }

            return true;
        }

        private void _03_TNDN_Detail_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (Form)sender;
            frm.Dispose();

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }

        private void ultraTabControl1_SelectedTabChanged(object sender, SelectedTabChangedEventArgs e)
        {

        }

        private void txtItemC6_ValueChanged(object sender, EventArgs e)//trungnq thêm để sửa task 6202
        {
            if(txtItemC6.Text!=null)
            {
                if (txtItemC6.Text != "")
                {
                    if(txtItemC6.Text.Contains("(")|| txtItemC6.Text.Contains(")"))
                    {
                        txtItemC7.ReadOnly = true;
                        txtItemC8.ReadOnly = true;
                        txtItemC9.ReadOnly = true;
                        txtItemC7.Text = "0";
                        txtItemC8.Text = "0";
                        txtItemC9.Text = "0";
                        txtItemC7.Value = 0;
                        txtItemC8.Value = 0;
                        txtItemC9.Value = 0;
                    }
                    else
                    {
                        txtItemC7.ReadOnly = false;
                        txtItemC8.ReadOnly = false;
                        txtItemC9.ReadOnly = false;
                        txtItemC7.MinValue = decimal.Zero;
                        txtItemC8.MinValue = decimal.Zero;
                        txtItemC9.MinValue = decimal.Zero;
                    }
                }
            }
        }

        private void txtItemPL19_ValueChanged(object sender, EventArgs e)
        {
            txtItemA1.Value = txtItemPL19.Value;
            txtItemA1.FormatString = txtItemPL19.FormatString;
            txtItemA1.InputMask = txtItemPL19.InputMask;
        }
    }
}
