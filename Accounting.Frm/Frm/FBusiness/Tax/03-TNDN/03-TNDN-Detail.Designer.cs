﻿namespace Accounting
{
    partial class _03_TNDN_Detail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_03_TNDN_Detail));
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance116 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance123 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance127 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance129 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance130 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance131 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance132 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance133 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance134 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance135 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance136 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance137 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance138 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance139 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance140 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance141 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance142 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance143 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance144 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance145 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance146 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance147 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance148 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance149 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance150 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance151 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance152 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance153 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance154 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance155 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance156 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance157 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance158 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance159 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance160 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance161 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance162 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance163 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance164 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance165 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance166 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance167 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance168 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance169 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance170 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance171 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance172 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance173 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance174 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance175 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance176 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance177 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance178 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance179 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance180 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance181 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance182 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance183 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance184 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance185 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance186 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance187 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance188 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance189 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance190 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance191 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance192 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance193 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance194 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance195 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance196 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance197 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance198 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance199 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance200 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance201 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance202 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance203 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance204 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance205 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance206 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance207 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance208 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance209 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance210 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance211 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance212 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance213 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance214 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance215 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance216 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance217 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance218 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance219 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance220 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance221 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance222 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance223 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance224 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance225 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance226 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance227 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance228 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance229 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance230 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance231 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance232 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance233 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance234 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance235 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance236 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance237 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance238 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance239 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance240 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance241 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance242 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance243 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance244 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance245 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance246 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance247 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance248 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance249 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance250 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance251 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance252 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance253 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance254 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance255 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance256 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance257 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance258 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance259 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance260 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance261 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance262 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance263 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance264 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance265 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance266 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance267 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance268 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance269 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance270 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance271 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance272 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance273 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance274 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance275 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance276 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance277 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance278 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance279 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance280 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance281 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance282 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance283 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance284 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance285 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance286 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance287 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance288 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance289 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance290 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance291 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance292 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance293 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance294 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance295 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance296 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance297 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance298 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance299 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance300 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance301 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance302 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance303 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance304 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance305 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance306 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance307 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance308 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance309 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance310 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance311 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance312 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance313 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance314 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance315 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance316 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance317 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance318 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance319 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance320 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance321 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance322 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance323 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance324 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance325 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance326 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance327 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance328 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance329 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance330 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance331 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance332 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance333 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance334 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance335 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance336 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance337 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance338 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance339 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance340 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance341 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance342 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance343 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance344 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance345 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance346 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance347 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance348 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance349 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance350 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance351 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance352 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance353 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance354 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance355 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance356 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance357 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance358 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance359 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance360 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance361 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance362 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance363 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance364 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance365 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance366 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance367 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance368 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance369 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance370 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance371 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance372 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance373 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance374 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance375 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance376 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance377 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance378 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance379 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance380 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance381 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance382 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance383 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance384 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance385 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance386 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance387 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance388 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance389 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance390 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance391 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance392 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance393 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance394 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance395 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance396 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance397 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance398 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance399 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance400 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance401 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance402 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance403 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance404 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance405 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance406 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance407 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance408 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance409 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance410 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance411 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance412 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance413 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance414 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance415 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance416 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance417 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance418 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance419 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance420 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance421 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance422 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance423 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance424 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance425 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance426 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance427 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance428 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance429 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance430 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance431 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance432 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance433 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance434 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance435 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance436 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance437 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance438 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance439 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance440 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance441 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance442 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance443 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance444 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance445 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance446 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance447 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance448 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance449 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance450 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance451 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance452 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance453 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance454 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance455 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance456 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance457 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance458 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance459 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance460 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance461 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance462 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance463 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance464 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance465 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance466 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance467 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance468 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance469 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance470 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance471 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance472 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance473 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance474 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance475 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance476 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance477 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance478 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance479 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance480 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance481 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance482 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance483 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance484 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance485 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance486 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance487 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance488 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance489 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance490 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance491 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance492 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance493 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance494 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance495 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance496 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance497 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance498 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance499 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance500 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance501 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance502 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance503 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance504 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance505 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance506 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance507 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance508 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance509 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance510 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance511 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance512 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance513 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance514 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance515 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance516 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance517 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance518 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance519 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance520 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance521 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance522 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance523 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance524 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance525 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance526 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance527 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance528 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance529 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance530 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance531 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance532 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance533 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance534 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance535 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance536 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance537 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance538 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance539 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance540 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance541 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance542 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance543 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance544 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance545 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance546 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance547 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance548 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance549 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance550 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance551 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance552 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance553 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance554 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance555 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance556 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance557 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance558 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance559 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance560 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance561 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance562 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.RibbonTab ribbonTab1 = new Infragistics.Win.UltraWinToolbars.RibbonTab("ribbon1");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup1 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup2");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool34 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnEdit");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool35 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnSave");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool49 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnDelete");
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool1 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("mnbtnPrint");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool2 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnKBS");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool3 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnExportXml");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool11 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnClose");
            Infragistics.Win.Appearance appearance563 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance564 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance565 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance566 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance567 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance568 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance569 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance570 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance571 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance572 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.UltraToolbar ultraToolbar1 = new Infragistics.Win.UltraWinToolbars.UltraToolbar("UltraToolbar1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool39 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnBack");
            Infragistics.Win.Appearance appearance573 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool40 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnForward");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool41 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnAdd");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool42 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnEdit");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool43 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnSave");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool44 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnDelete");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool45 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnComeBack");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool46 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnPost");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool47 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnReset");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool48 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnViewList");
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool8 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("mnpopUtilities");
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool2 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("mnpopTemplate");
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool6 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("mnbtnPrint");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool52 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnHelp");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool53 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnClose");
            Infragistics.Win.UltraWinToolbars.UltraToolbar ultraToolbar2 = new Infragistics.Win.UltraWinToolbars.UltraToolbar("UltraToolbar2");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool37 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnComeBack");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool38 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnComeBack");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool54 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnComeBack");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool55 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnComeBack");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool56 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnComeBack");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool57 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnComeBack");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool58 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnComeBack");
            Infragistics.Win.Appearance appearance574 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance575 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance576 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance577 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance578 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance579 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance580 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance581 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool16 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnBack");
            Infragistics.Win.Appearance appearance582 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool17 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnForward");
            Infragistics.Win.Appearance appearance583 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool18 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnAdd");
            Infragistics.Win.Appearance appearance584 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance585 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool19 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnEdit");
            Infragistics.Win.Appearance appearance586 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance587 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool20 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnSave");
            Infragistics.Win.Appearance appearance588 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance589 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool21 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnDelete");
            Infragistics.Win.Appearance appearance590 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance591 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool22 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnComeBack");
            Infragistics.Win.Appearance appearance592 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool23 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnPost");
            Infragistics.Win.Appearance appearance593 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance594 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool24 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnReset");
            Infragistics.Win.Appearance appearance595 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool25 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnViewList");
            Infragistics.Win.Appearance appearance596 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance597 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool29 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnHelp");
            Infragistics.Win.Appearance appearance598 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool30 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnClose");
            Infragistics.Win.Appearance appearance599 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.TaskPaneTool taskPaneTool4 = new Infragistics.Win.UltraWinToolbars.TaskPaneTool("TaskPaneTool1");
            Infragistics.Win.UltraWinToolbars.TaskPaneTool taskPaneTool5 = new Infragistics.Win.UltraWinToolbars.TaskPaneTool("TaskPaneTool2");
            Infragistics.Win.UltraWinToolbars.TaskPaneTool taskPaneTool6 = new Infragistics.Win.UltraWinToolbars.TaskPaneTool("TaskPaneTool3");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool32 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnUnPost");
            Infragistics.Win.Appearance appearance600 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance601 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool36 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnTemplate0");
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool3 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("mnpopTemplate");
            Infragistics.Win.Appearance appearance602 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool9 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("mnpopUtilities");
            Infragistics.Win.Appearance appearance603 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool7 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("mnbtnPrint");
            Infragistics.Win.Appearance appearance604 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool6 = new Infragistics.Win.UltraWinToolbars.ButtonTool("PrintTNDN-03");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool4 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnExportXml");
            Infragistics.Win.Appearance appearance605 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance606 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.FontListTool fontListTool2 = new Infragistics.Win.UltraWinToolbars.FontListTool("FontListTool1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool1 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnKBS");
            Infragistics.Win.Appearance appearance607 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance608 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ComboBoxTool comboBoxTool2 = new Infragistics.Win.UltraWinToolbars.ComboBoxTool("PrintTNDN03");
            Infragistics.Win.ValueList valueList1 = new Infragistics.Win.ValueList(0);
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool7 = new Infragistics.Win.UltraWinToolbars.ButtonTool("PrintTNDN-03");
            Infragistics.Win.Appearance appearance609 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel330 = new Infragistics.Win.Misc.UltraLabel();
            this.txtSignDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel329 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNameSign = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtCCHN = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel328 = new Infragistics.Win.Misc.UltraLabel();
            this.txtName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel327 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel326 = new Infragistics.Win.Misc.UltraLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor9 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.txtItem5 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtLateAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtLateDays = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtNotExtendTaxAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtExtendTaxAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.cbbExtensionCase = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtItem1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel321 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel320 = new Infragistics.Win.Misc.UltraLabel();
            this.dteToDateLate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel319 = new Infragistics.Win.Misc.UltraLabel();
            this.dteFromDateLate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel318 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel317 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel316 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel315 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel314 = new Infragistics.Win.Misc.UltraLabel();
            this.dteExtendTime = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel313 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel312 = new Infragistics.Win.Misc.UltraLabel();
            this.txtIsExtend = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel311 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel310 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel74 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemI = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel307 = new Infragistics.Win.Misc.UltraLabel();
            this.NameI = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel309 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel73 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemH = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel304 = new Infragistics.Win.Misc.UltraLabel();
            this.NameH = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel306 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel69 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemG2 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel70 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemG3 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel71 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemG1 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel292 = new Infragistics.Win.Misc.UltraLabel();
            this.NameG3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel294 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel295 = new Infragistics.Win.Misc.UltraLabel();
            this.NameG2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel297 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel298 = new Infragistics.Win.Misc.UltraLabel();
            this.NameG1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel300 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel72 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemG = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel301 = new Infragistics.Win.Misc.UltraLabel();
            this.NameG = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel303 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel68 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemE2 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel67 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemE3 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel66 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemE1 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel283 = new Infragistics.Win.Misc.UltraLabel();
            this.NameE3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel285 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel286 = new Infragistics.Win.Misc.UltraLabel();
            this.NameE2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel288 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel289 = new Infragistics.Win.Misc.UltraLabel();
            this.NameE1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel291 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel64 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemD3 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel65 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemE = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel280 = new Infragistics.Win.Misc.UltraLabel();
            this.NameE = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel282 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel277 = new Infragistics.Win.Misc.UltraLabel();
            this.NameD3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel279 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel63 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemD2 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel274 = new Infragistics.Win.Misc.UltraLabel();
            this.NameD2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel276 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel62 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemD1 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel271 = new Infragistics.Win.Misc.UltraLabel();
            this.NameD1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel273 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel61 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemD = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel268 = new Infragistics.Win.Misc.UltraLabel();
            this.NameD = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel270 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel60 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemC16 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel265 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel267 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel59 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemC15 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel262 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel264 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel58 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemC14 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel259 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel261 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel57 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemC13 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel256 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel258 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel56 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemC12 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel253 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel255 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel55 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemC11 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel250 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel252 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel54 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemC10 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel247 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel249 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel53 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemC9a = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel245 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC9a = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel52 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemC9 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel242 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel244 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel51 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemC8 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel239 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel241 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel50 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemC7 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel236 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel238 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel49 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemC6 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel233 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel235 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel48 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemC5 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel230 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel232 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel47 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemC4 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel227 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel229 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel46 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemC3b = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel224 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC3b = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel226 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel45 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemC3a = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel221 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC3a = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel223 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel44 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemC3 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel218 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel220 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel43 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemC2 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel215 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel217 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel42 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemC1 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel212 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel214 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel41 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel209 = new Infragistics.Win.Misc.UltraLabel();
            this.NameC = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel211 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel39 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemB14 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel206 = new Infragistics.Win.Misc.UltraLabel();
            this.NameB14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel208 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel38 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemB13 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel203 = new Infragistics.Win.Misc.UltraLabel();
            this.NameB13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel205 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel37 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemB12 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel200 = new Infragistics.Win.Misc.UltraLabel();
            this.NameB12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel202 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel36 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemB11 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel196 = new Infragistics.Win.Misc.UltraLabel();
            this.NameB11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel198 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel35 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemB10 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel180 = new Infragistics.Win.Misc.UltraLabel();
            this.NameB10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel195 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel34 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemB9 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel177 = new Infragistics.Win.Misc.UltraLabel();
            this.NameB9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel179 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel33 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemB8 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel150 = new Infragistics.Win.Misc.UltraLabel();
            this.NameB8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel176 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel32 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemB7 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel147 = new Infragistics.Win.Misc.UltraLabel();
            this.NameB7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel149 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel31 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemB6 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel144 = new Infragistics.Win.Misc.UltraLabel();
            this.NameB6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel146 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel30 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemB5 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel141 = new Infragistics.Win.Misc.UltraLabel();
            this.NameB5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel143 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel29 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemB4 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel138 = new Infragistics.Win.Misc.UltraLabel();
            this.NameB4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel140 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel28 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemB3 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel135 = new Infragistics.Win.Misc.UltraLabel();
            this.NameB3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel137 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel27 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemB2 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel132 = new Infragistics.Win.Misc.UltraLabel();
            this.NameB2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel134 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel26 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemB1 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel129 = new Infragistics.Win.Misc.UltraLabel();
            this.NameB1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel131 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel199 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel25 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel24 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemA1 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel166 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel165 = new Infragistics.Win.Misc.UltraLabel();
            this.NameB = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel128 = new Infragistics.Win.Misc.UltraLabel();
            this.NameA1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel126 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel23 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel122 = new Infragistics.Win.Misc.UltraLabel();
            this.NameA = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel124 = new Infragistics.Win.Misc.UltraLabel();
            this.txtItem15 = new Infragistics.Win.Misc.UltraLabel();
            this.txtItem14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel119 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel118 = new Infragistics.Win.Misc.UltraLabel();
            this.txtItem7 = new Infragistics.Win.Misc.UltraLabel();
            this.txtItem6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel110 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel114 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel106 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel86 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel90 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel94 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel98 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel99 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel100 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel101 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel102 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbTMIndustryType = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel82 = new Infragistics.Win.Misc.UltraLabel();
            this.txtIsRelatedTransactionEnterp = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel78 = new Infragistics.Win.Misc.UltraLabel();
            this.txtIsDependentEnterprise = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel74 = new Infragistics.Win.Misc.UltraLabel();
            this.txtIsSMEEnterprise = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel70 = new Infragistics.Win.Misc.UltraLabel();
            this.txtItem3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel66 = new Infragistics.Win.Misc.UltraLabel();
            this.txtItem2 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel62 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel38 = new Infragistics.Win.Misc.UltraLabel();
            this.txtDeclarationName = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel22 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemPL19 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel21 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemPL18 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel20 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemPL17 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel19 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemPL16 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel18 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemPL15 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel17 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemPL14 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel16 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemPL13 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel15 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemPL12 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel14 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemPL11 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel13 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemPL10 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel12 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemPL9 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel11 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemPL8 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel10 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemPL7 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel9 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemPL6 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel8 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemPL5 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel7 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemPL4 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel6 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemPL3 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel5 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemPL2 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel4 = new Infragistics.Win.Misc.UltraPanel();
            this.txtItemPL1 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel115 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamePL19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel117 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel111 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamePL18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel113 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel107 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamePL17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel109 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel103 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamePL16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel105 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel95 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamePL15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel97 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel91 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamePL14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel93 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel87 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamePL13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel89 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel83 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamePL12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel85 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel79 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamePL11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel81 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel75 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamePL10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel77 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel71 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamePL9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel73 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel67 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamePL8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel69 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel63 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamePL7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel65 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel59 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamePL6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel61 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel55 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamePL5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel57 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel51 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamePL4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel53 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel47 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamePL3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel49 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel43 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamePL2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel45 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel39 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamePL1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel41 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel36 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel34 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel35 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel33 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel29 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel30 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel31 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel32 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tablePL2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtItemPL2_10 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.label13 = new System.Windows.Forms.Label();
            this.txtItemPL2_65 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_55 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_45 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_35 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_64 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_54 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_44 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_34 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_63 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_53 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_43 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_33 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_62 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_52 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_42 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_32 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_61 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_51 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_41 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_31 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_60 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_50 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_40 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtItemPL2_30 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtItemPL2_11 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtItemPL2_12 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtItemPL2_13 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtItemPL2_14 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtItemPL2_20 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtItemPL2_21 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtItemPL2_22 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtItemPL2_23 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtItemPL2_24 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraTextEditor7 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel194 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor6 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel193 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor5 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel192 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel191 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel190 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraDateTimeEditor1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel189 = new Infragistics.Win.Misc.UltraLabel();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel188 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel187 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel185 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel184 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel183 = new Infragistics.Win.Misc.UltraLabel();
            this.txtDateDelay = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel182 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel174 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel173 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel172 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel171 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel170 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel168 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel167 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel181 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel169 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel40 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraPanel76 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel163 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel164 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGrid5 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraLabel161 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel158 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel156 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel157 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel159 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel160 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel162 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel155 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel154 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel153 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel152 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel151 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.utmDetailBaseToolBar = new Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(this.components);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._03_TNDN_Detail_Fill_Panel = new Infragistics.Win.Misc.UltraPanel();
            this.ultraTabPageControl1.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameSign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCCHN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbExtensionCase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteToDateLate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFromDateLate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteExtendTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIsExtend)).BeginInit();
            this.ultraPanel74.ClientArea.SuspendLayout();
            this.ultraPanel74.SuspendLayout();
            this.ultraPanel73.ClientArea.SuspendLayout();
            this.ultraPanel73.SuspendLayout();
            this.ultraPanel69.ClientArea.SuspendLayout();
            this.ultraPanel69.SuspendLayout();
            this.ultraPanel70.ClientArea.SuspendLayout();
            this.ultraPanel70.SuspendLayout();
            this.ultraPanel71.ClientArea.SuspendLayout();
            this.ultraPanel71.SuspendLayout();
            this.ultraPanel72.ClientArea.SuspendLayout();
            this.ultraPanel72.SuspendLayout();
            this.ultraPanel68.ClientArea.SuspendLayout();
            this.ultraPanel68.SuspendLayout();
            this.ultraPanel67.ClientArea.SuspendLayout();
            this.ultraPanel67.SuspendLayout();
            this.ultraPanel66.ClientArea.SuspendLayout();
            this.ultraPanel66.SuspendLayout();
            this.ultraPanel64.ClientArea.SuspendLayout();
            this.ultraPanel64.SuspendLayout();
            this.ultraPanel65.ClientArea.SuspendLayout();
            this.ultraPanel65.SuspendLayout();
            this.ultraPanel63.ClientArea.SuspendLayout();
            this.ultraPanel63.SuspendLayout();
            this.ultraPanel62.ClientArea.SuspendLayout();
            this.ultraPanel62.SuspendLayout();
            this.ultraPanel61.ClientArea.SuspendLayout();
            this.ultraPanel61.SuspendLayout();
            this.ultraPanel60.ClientArea.SuspendLayout();
            this.ultraPanel60.SuspendLayout();
            this.ultraPanel59.ClientArea.SuspendLayout();
            this.ultraPanel59.SuspendLayout();
            this.ultraPanel58.ClientArea.SuspendLayout();
            this.ultraPanel58.SuspendLayout();
            this.ultraPanel57.ClientArea.SuspendLayout();
            this.ultraPanel57.SuspendLayout();
            this.ultraPanel56.ClientArea.SuspendLayout();
            this.ultraPanel56.SuspendLayout();
            this.ultraPanel55.ClientArea.SuspendLayout();
            this.ultraPanel55.SuspendLayout();
            this.ultraPanel54.ClientArea.SuspendLayout();
            this.ultraPanel54.SuspendLayout();
            this.ultraPanel53.ClientArea.SuspendLayout();
            this.ultraPanel53.SuspendLayout();
            this.ultraPanel52.ClientArea.SuspendLayout();
            this.ultraPanel52.SuspendLayout();
            this.ultraPanel51.ClientArea.SuspendLayout();
            this.ultraPanel51.SuspendLayout();
            this.ultraPanel50.ClientArea.SuspendLayout();
            this.ultraPanel50.SuspendLayout();
            this.ultraPanel49.ClientArea.SuspendLayout();
            this.ultraPanel49.SuspendLayout();
            this.ultraPanel48.ClientArea.SuspendLayout();
            this.ultraPanel48.SuspendLayout();
            this.ultraPanel47.ClientArea.SuspendLayout();
            this.ultraPanel47.SuspendLayout();
            this.ultraPanel46.ClientArea.SuspendLayout();
            this.ultraPanel46.SuspendLayout();
            this.ultraPanel45.ClientArea.SuspendLayout();
            this.ultraPanel45.SuspendLayout();
            this.ultraPanel44.ClientArea.SuspendLayout();
            this.ultraPanel44.SuspendLayout();
            this.ultraPanel43.ClientArea.SuspendLayout();
            this.ultraPanel43.SuspendLayout();
            this.ultraPanel42.ClientArea.SuspendLayout();
            this.ultraPanel42.SuspendLayout();
            this.ultraPanel41.SuspendLayout();
            this.ultraPanel39.ClientArea.SuspendLayout();
            this.ultraPanel39.SuspendLayout();
            this.ultraPanel38.ClientArea.SuspendLayout();
            this.ultraPanel38.SuspendLayout();
            this.ultraPanel37.ClientArea.SuspendLayout();
            this.ultraPanel37.SuspendLayout();
            this.ultraPanel36.ClientArea.SuspendLayout();
            this.ultraPanel36.SuspendLayout();
            this.ultraPanel35.ClientArea.SuspendLayout();
            this.ultraPanel35.SuspendLayout();
            this.ultraPanel34.ClientArea.SuspendLayout();
            this.ultraPanel34.SuspendLayout();
            this.ultraPanel33.ClientArea.SuspendLayout();
            this.ultraPanel33.SuspendLayout();
            this.ultraPanel32.ClientArea.SuspendLayout();
            this.ultraPanel32.SuspendLayout();
            this.ultraPanel31.ClientArea.SuspendLayout();
            this.ultraPanel31.SuspendLayout();
            this.ultraPanel30.ClientArea.SuspendLayout();
            this.ultraPanel30.SuspendLayout();
            this.ultraPanel29.ClientArea.SuspendLayout();
            this.ultraPanel29.SuspendLayout();
            this.ultraPanel28.ClientArea.SuspendLayout();
            this.ultraPanel28.SuspendLayout();
            this.ultraPanel27.ClientArea.SuspendLayout();
            this.ultraPanel27.SuspendLayout();
            this.ultraPanel26.ClientArea.SuspendLayout();
            this.ultraPanel26.SuspendLayout();
            this.ultraPanel25.SuspendLayout();
            this.ultraPanel24.ClientArea.SuspendLayout();
            this.ultraPanel24.SuspendLayout();
            this.ultraPanel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTMIndustryType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIsRelatedTransactionEnterp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIsDependentEnterprise)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIsSMEEnterprise)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItem2)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            this.ultraPanel22.ClientArea.SuspendLayout();
            this.ultraPanel22.SuspendLayout();
            this.ultraPanel21.ClientArea.SuspendLayout();
            this.ultraPanel21.SuspendLayout();
            this.ultraPanel20.ClientArea.SuspendLayout();
            this.ultraPanel20.SuspendLayout();
            this.ultraPanel19.ClientArea.SuspendLayout();
            this.ultraPanel19.SuspendLayout();
            this.ultraPanel18.ClientArea.SuspendLayout();
            this.ultraPanel18.SuspendLayout();
            this.ultraPanel17.ClientArea.SuspendLayout();
            this.ultraPanel17.SuspendLayout();
            this.ultraPanel16.ClientArea.SuspendLayout();
            this.ultraPanel16.SuspendLayout();
            this.ultraPanel15.ClientArea.SuspendLayout();
            this.ultraPanel15.SuspendLayout();
            this.ultraPanel14.ClientArea.SuspendLayout();
            this.ultraPanel14.SuspendLayout();
            this.ultraPanel13.ClientArea.SuspendLayout();
            this.ultraPanel13.SuspendLayout();
            this.ultraPanel12.ClientArea.SuspendLayout();
            this.ultraPanel12.SuspendLayout();
            this.ultraPanel11.ClientArea.SuspendLayout();
            this.ultraPanel11.SuspendLayout();
            this.ultraPanel10.ClientArea.SuspendLayout();
            this.ultraPanel10.SuspendLayout();
            this.ultraPanel9.ClientArea.SuspendLayout();
            this.ultraPanel9.SuspendLayout();
            this.ultraPanel8.ClientArea.SuspendLayout();
            this.ultraPanel8.SuspendLayout();
            this.ultraPanel7.ClientArea.SuspendLayout();
            this.ultraPanel7.SuspendLayout();
            this.ultraPanel6.ClientArea.SuspendLayout();
            this.ultraPanel6.SuspendLayout();
            this.ultraPanel5.ClientArea.SuspendLayout();
            this.ultraPanel5.SuspendLayout();
            this.ultraPanel4.ClientArea.SuspendLayout();
            this.ultraPanel4.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            this.ultraTabPageControl3.SuspendLayout();
            this.tablePL2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_24)).BeginInit();
            this.ultraTabPageControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateDelay)).BeginInit();
            this.ultraPanel40.ClientArea.SuspendLayout();
            this.ultraPanel40.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            this.ultraPanel76.ClientArea.SuspendLayout();
            this.ultraPanel76.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.utmDetailBaseToolBar)).BeginInit();
            this._03_TNDN_Detail_Fill_Panel.ClientArea.SuspendLayout();
            this._03_TNDN_Detail_Fill_Panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.AutoScroll = true;
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel10);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel1);
            this.ultraTabPageControl1.Controls.Add(this.tableLayoutPanel1);
            this.ultraTabPageControl1.Controls.Add(this.txtItem5);
            this.ultraTabPageControl1.Controls.Add(this.txtLateAmount);
            this.ultraTabPageControl1.Controls.Add(this.txtLateDays);
            this.ultraTabPageControl1.Controls.Add(this.txtNotExtendTaxAmount);
            this.ultraTabPageControl1.Controls.Add(this.txtExtendTaxAmount);
            this.ultraTabPageControl1.Controls.Add(this.cbbExtensionCase);
            this.ultraTabPageControl1.Controls.Add(this.txtItem1);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel321);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel320);
            this.ultraTabPageControl1.Controls.Add(this.dteToDateLate);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel319);
            this.ultraTabPageControl1.Controls.Add(this.dteFromDateLate);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel318);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel317);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel316);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel315);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel314);
            this.ultraTabPageControl1.Controls.Add(this.dteExtendTime);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel313);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel312);
            this.ultraTabPageControl1.Controls.Add(this.txtIsExtend);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel311);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel310);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel74);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel307);
            this.ultraTabPageControl1.Controls.Add(this.NameI);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel309);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel73);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel304);
            this.ultraTabPageControl1.Controls.Add(this.NameH);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel306);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel69);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel70);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel71);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel292);
            this.ultraTabPageControl1.Controls.Add(this.NameG3);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel294);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel295);
            this.ultraTabPageControl1.Controls.Add(this.NameG2);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel297);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel298);
            this.ultraTabPageControl1.Controls.Add(this.NameG1);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel300);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel72);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel301);
            this.ultraTabPageControl1.Controls.Add(this.NameG);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel303);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel68);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel67);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel66);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel283);
            this.ultraTabPageControl1.Controls.Add(this.NameE3);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel285);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel286);
            this.ultraTabPageControl1.Controls.Add(this.NameE2);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel288);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel289);
            this.ultraTabPageControl1.Controls.Add(this.NameE1);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel291);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel64);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel65);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel280);
            this.ultraTabPageControl1.Controls.Add(this.NameE);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel282);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel277);
            this.ultraTabPageControl1.Controls.Add(this.NameD3);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel279);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel63);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel274);
            this.ultraTabPageControl1.Controls.Add(this.NameD2);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel276);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel62);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel271);
            this.ultraTabPageControl1.Controls.Add(this.NameD1);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel273);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel61);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel268);
            this.ultraTabPageControl1.Controls.Add(this.NameD);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel270);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel60);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel265);
            this.ultraTabPageControl1.Controls.Add(this.NameC16);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel267);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel59);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel262);
            this.ultraTabPageControl1.Controls.Add(this.NameC15);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel264);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel58);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel259);
            this.ultraTabPageControl1.Controls.Add(this.NameC14);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel261);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel57);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel256);
            this.ultraTabPageControl1.Controls.Add(this.NameC13);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel258);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel56);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel253);
            this.ultraTabPageControl1.Controls.Add(this.NameC12);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel255);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel55);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel250);
            this.ultraTabPageControl1.Controls.Add(this.NameC11);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel252);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel54);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel247);
            this.ultraTabPageControl1.Controls.Add(this.NameC10);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel249);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel53);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel245);
            this.ultraTabPageControl1.Controls.Add(this.NameC9a);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel52);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel242);
            this.ultraTabPageControl1.Controls.Add(this.NameC9);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel244);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel51);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel239);
            this.ultraTabPageControl1.Controls.Add(this.NameC8);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel241);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel50);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel236);
            this.ultraTabPageControl1.Controls.Add(this.NameC7);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel238);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel49);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel233);
            this.ultraTabPageControl1.Controls.Add(this.NameC6);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel235);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel48);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel230);
            this.ultraTabPageControl1.Controls.Add(this.NameC5);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel232);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel47);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel227);
            this.ultraTabPageControl1.Controls.Add(this.NameC4);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel229);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel46);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel224);
            this.ultraTabPageControl1.Controls.Add(this.NameC3b);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel226);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel45);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel221);
            this.ultraTabPageControl1.Controls.Add(this.NameC3a);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel223);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel44);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel218);
            this.ultraTabPageControl1.Controls.Add(this.NameC3);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel220);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel43);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel215);
            this.ultraTabPageControl1.Controls.Add(this.NameC2);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel217);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel42);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel212);
            this.ultraTabPageControl1.Controls.Add(this.NameC1);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel214);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel41);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel209);
            this.ultraTabPageControl1.Controls.Add(this.NameC);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel211);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel39);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel206);
            this.ultraTabPageControl1.Controls.Add(this.NameB14);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel208);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel38);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel203);
            this.ultraTabPageControl1.Controls.Add(this.NameB13);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel205);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel37);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel200);
            this.ultraTabPageControl1.Controls.Add(this.NameB12);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel202);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel36);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel196);
            this.ultraTabPageControl1.Controls.Add(this.NameB11);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel198);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel35);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel180);
            this.ultraTabPageControl1.Controls.Add(this.NameB10);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel195);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel34);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel177);
            this.ultraTabPageControl1.Controls.Add(this.NameB9);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel179);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel33);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel150);
            this.ultraTabPageControl1.Controls.Add(this.NameB8);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel176);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel32);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel147);
            this.ultraTabPageControl1.Controls.Add(this.NameB7);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel149);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel31);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel144);
            this.ultraTabPageControl1.Controls.Add(this.NameB6);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel146);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel30);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel141);
            this.ultraTabPageControl1.Controls.Add(this.NameB5);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel143);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel29);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel138);
            this.ultraTabPageControl1.Controls.Add(this.NameB4);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel140);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel28);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel135);
            this.ultraTabPageControl1.Controls.Add(this.NameB3);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel137);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel27);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel132);
            this.ultraTabPageControl1.Controls.Add(this.NameB2);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel134);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel26);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel129);
            this.ultraTabPageControl1.Controls.Add(this.NameB1);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel131);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel199);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel25);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel24);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel166);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel165);
            this.ultraTabPageControl1.Controls.Add(this.NameB);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel128);
            this.ultraTabPageControl1.Controls.Add(this.NameA1);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel126);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel23);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel122);
            this.ultraTabPageControl1.Controls.Add(this.NameA);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel124);
            this.ultraTabPageControl1.Controls.Add(this.txtItem15);
            this.ultraTabPageControl1.Controls.Add(this.txtItem14);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel119);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel118);
            this.ultraTabPageControl1.Controls.Add(this.txtItem7);
            this.ultraTabPageControl1.Controls.Add(this.txtItem6);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel110);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel114);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel106);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel86);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel90);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel94);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel98);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel99);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel100);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel101);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel102);
            this.ultraTabPageControl1.Controls.Add(this.cbbTMIndustryType);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel82);
            this.ultraTabPageControl1.Controls.Add(this.txtIsRelatedTransactionEnterp);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel78);
            this.ultraTabPageControl1.Controls.Add(this.txtIsDependentEnterprise);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel74);
            this.ultraTabPageControl1.Controls.Add(this.txtIsSMEEnterprise);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel70);
            this.ultraTabPageControl1.Controls.Add(this.txtItem3);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel66);
            this.ultraTabPageControl1.Controls.Add(this.txtItem2);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel62);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel38);
            this.ultraTabPageControl1.Controls.Add(this.txtDeclarationName);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1128, 568);
            // 
            // ultraLabel10
            // 
            appearance1.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance1.TextHAlignAsString = "Left";
            this.ultraLabel10.Appearance = appearance1;
            this.ultraLabel10.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel10.Location = new System.Drawing.Point(444, 224);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(30, 21);
            this.ultraLabel10.TabIndex = 520;
            this.ultraLabel10.Text = "%";
            // 
            // ultraPanel1
            // 
            appearance2.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraPanel1.Appearance = appearance2;
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel330);
            this.ultraPanel1.ClientArea.Controls.Add(this.txtSignDate);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel329);
            this.ultraPanel1.ClientArea.Controls.Add(this.txtNameSign);
            this.ultraPanel1.ClientArea.Controls.Add(this.txtCCHN);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel328);
            this.ultraPanel1.ClientArea.Controls.Add(this.txtName);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel327);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel326);
            this.ultraPanel1.Location = new System.Drawing.Point(80, 2242);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(869, 139);
            this.ultraPanel1.TabIndex = 519;
            // 
            // ultraLabel330
            // 
            appearance3.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel330.Appearance = appearance3;
            this.ultraLabel330.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel330.Location = new System.Drawing.Point(549, 90);
            this.ultraLabel330.Name = "ultraLabel330";
            this.ultraLabel330.Size = new System.Drawing.Size(67, 16);
            this.ultraLabel330.TabIndex = 517;
            this.ultraLabel330.Text = "Ngày ký:";
            // 
            // txtSignDate
            // 
            this.txtSignDate.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSignDate.Location = new System.Drawing.Point(622, 88);
            this.txtSignDate.MaskInput = "dd/mm/yyyy";
            this.txtSignDate.Name = "txtSignDate";
            this.txtSignDate.Size = new System.Drawing.Size(226, 22);
            this.txtSignDate.TabIndex = 512;
            // 
            // ultraLabel329
            // 
            appearance4.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel329.Appearance = appearance4;
            this.ultraLabel329.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel329.Location = new System.Drawing.Point(549, 60);
            this.ultraLabel329.Name = "ultraLabel329";
            this.ultraLabel329.Size = new System.Drawing.Size(67, 16);
            this.ultraLabel329.TabIndex = 516;
            this.ultraLabel329.Text = "Người ký:";
            // 
            // txtNameSign
            // 
            this.txtNameSign.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNameSign.Location = new System.Drawing.Point(622, 56);
            this.txtNameSign.Name = "txtNameSign";
            this.txtNameSign.Size = new System.Drawing.Size(226, 22);
            this.txtNameSign.TabIndex = 511;
            // 
            // txtCCHN
            // 
            this.txtCCHN.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCCHN.Location = new System.Drawing.Point(206, 86);
            this.txtCCHN.Name = "txtCCHN";
            this.txtCCHN.Size = new System.Drawing.Size(213, 22);
            this.txtCCHN.TabIndex = 510;
            // 
            // ultraLabel328
            // 
            appearance5.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel328.Appearance = appearance5;
            this.ultraLabel328.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel328.Location = new System.Drawing.Point(8, 90);
            this.ultraLabel328.Name = "ultraLabel328";
            this.ultraLabel328.Size = new System.Drawing.Size(145, 16);
            this.ultraLabel328.TabIndex = 515;
            this.ultraLabel328.Text = "Chứng chỉ hành nghề số:";
            // 
            // txtName
            // 
            this.txtName.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.Location = new System.Drawing.Point(206, 56);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(213, 22);
            this.txtName.TabIndex = 509;
            // 
            // ultraLabel327
            // 
            appearance6.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel327.Appearance = appearance6;
            this.ultraLabel327.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel327.Location = new System.Drawing.Point(8, 60);
            this.ultraLabel327.Name = "ultraLabel327";
            this.ultraLabel327.Size = new System.Drawing.Size(145, 16);
            this.ultraLabel327.TabIndex = 514;
            this.ultraLabel327.Text = "Họ và tên:";
            // 
            // ultraLabel326
            // 
            appearance7.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance7.TextHAlignAsString = "Left";
            this.ultraLabel326.Appearance = appearance7;
            this.ultraLabel326.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel326.Location = new System.Drawing.Point(5, 29);
            this.ultraLabel326.Name = "ultraLabel326";
            this.ultraLabel326.Size = new System.Drawing.Size(170, 21);
            this.ultraLabel326.TabIndex = 513;
            this.ultraLabel326.Text = "NHÂN VIÊN ĐẠI LÝ THUẾ:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 859F));
            this.tableLayoutPanel1.Controls.Add(this.ultraLabel11, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.ultraTextEditor9, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.ultraLabel7, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.ultraLabel8, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(80, 2165);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(902, 69);
            this.tableLayoutPanel1.TabIndex = 518;
            // 
            // ultraLabel11
            // 
            this.ultraLabel11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.TextHAlignAsString = "Center";
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance8;
            this.ultraLabel11.Location = new System.Drawing.Point(1, 38);
            this.ultraLabel11.Margin = new System.Windows.Forms.Padding(0);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(40, 30);
            this.ultraLabel11.TabIndex = 521;
            this.ultraLabel11.Text = "1";
            // 
            // ultraTextEditor9
            // 
            this.ultraTextEditor9.AutoSize = false;
            this.ultraTextEditor9.ContextMenuStrip = this.contextMenuStrip1;
            this.ultraTextEditor9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTextEditor9.Location = new System.Drawing.Point(42, 38);
            this.ultraTextEditor9.Margin = new System.Windows.Forms.Padding(0);
            this.ultraTextEditor9.Name = "ultraTextEditor9";
            this.ultraTextEditor9.Size = new System.Drawing.Size(859, 30);
            this.ultraTextEditor9.TabIndex = 3;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmDelete});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(208, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F5)));
            this.tsmAdd.Size = new System.Drawing.Size(207, 22);
            this.tsmAdd.Text = "Thêm một dòng";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F6)));
            this.tsmDelete.Size = new System.Drawing.Size(207, 22);
            this.tsmDelete.Text = "Xóa một dòng";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // ultraLabel7
            // 
            appearance9.TextHAlignAsString = "Center";
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance9;
            this.ultraLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel7.Location = new System.Drawing.Point(45, 4);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(853, 30);
            this.ultraLabel7.TabIndex = 1;
            this.ultraLabel7.Text = "Tên tài liệu";
            // 
            // ultraLabel8
            // 
            appearance10.TextHAlignAsString = "Center";
            appearance10.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance10;
            this.ultraLabel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel8.Location = new System.Drawing.Point(4, 4);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(34, 30);
            this.ultraLabel8.TabIndex = 0;
            this.ultraLabel8.Text = "STT";
            // 
            // txtItem5
            // 
            appearance11.TextHAlignAsString = "Right";
            this.txtItem5.Appearance = appearance11;
            this.txtItem5.AutoSize = false;
            this.txtItem5.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItem5.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtItem5.InputMask = "{LOC}nnnnnnn.nn";
            this.txtItem5.Location = new System.Drawing.Point(397, 222);
            this.txtItem5.Name = "txtItem5";
            this.txtItem5.NullText = "";
            this.txtItem5.PromptChar = ' ';
            this.txtItem5.Size = new System.Drawing.Size(46, 23);
            this.txtItem5.TabIndex = 152;
            // 
            // txtLateAmount
            // 
            appearance12.TextHAlignAsString = "Right";
            this.txtLateAmount.Appearance = appearance12;
            this.txtLateAmount.AutoSize = false;
            this.txtLateAmount.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtLateAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtLateAmount.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtLateAmount.Location = new System.Drawing.Point(267, 2102);
            this.txtLateAmount.Name = "txtLateAmount";
            this.txtLateAmount.NullText = "";
            this.txtLateAmount.PromptChar = ' ';
            this.txtLateAmount.Size = new System.Drawing.Size(143, 23);
            this.txtLateAmount.TabIndex = 515;
            this.txtLateAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ultraMaskedEdit3_KeyPress);
            // 
            // txtLateDays
            // 
            appearance13.TextHAlignAsString = "Right";
            this.txtLateDays.Appearance = appearance13;
            this.txtLateDays.AutoSize = false;
            this.txtLateDays.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtLateDays.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtLateDays.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtLateDays.Location = new System.Drawing.Point(267, 2073);
            this.txtLateDays.Name = "txtLateDays";
            this.txtLateDays.NullText = "";
            this.txtLateDays.PromptChar = ' ';
            this.txtLateDays.ReadOnly = true;
            this.txtLateDays.Size = new System.Drawing.Size(77, 23);
            this.txtLateDays.TabIndex = 514;
            this.txtLateDays.ValueChanged += new System.EventHandler(this.txtItemI_ValueChanged);
            // 
            // txtNotExtendTaxAmount
            // 
            appearance14.TextHAlignAsString = "Right";
            this.txtNotExtendTaxAmount.Appearance = appearance14;
            this.txtNotExtendTaxAmount.AutoSize = false;
            this.txtNotExtendTaxAmount.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtNotExtendTaxAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtNotExtendTaxAmount.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtNotExtendTaxAmount.Location = new System.Drawing.Point(421, 2001);
            this.txtNotExtendTaxAmount.Name = "txtNotExtendTaxAmount";
            this.txtNotExtendTaxAmount.NullText = "";
            this.txtNotExtendTaxAmount.PromptChar = ' ';
            this.txtNotExtendTaxAmount.ReadOnly = true;
            this.txtNotExtendTaxAmount.Size = new System.Drawing.Size(276, 23);
            this.txtNotExtendTaxAmount.TabIndex = 513;
            this.txtNotExtendTaxAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ultraMaskedEdit3_KeyPress);
            // 
            // txtExtendTaxAmount
            // 
            appearance15.TextHAlignAsString = "Right";
            this.txtExtendTaxAmount.Appearance = appearance15;
            this.txtExtendTaxAmount.AutoSize = false;
            this.txtExtendTaxAmount.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtExtendTaxAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtExtendTaxAmount.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtExtendTaxAmount.Location = new System.Drawing.Point(421, 1972);
            this.txtExtendTaxAmount.Name = "txtExtendTaxAmount";
            this.txtExtendTaxAmount.NullText = "";
            this.txtExtendTaxAmount.PromptChar = ' ';
            this.txtExtendTaxAmount.Size = new System.Drawing.Size(276, 23);
            this.txtExtendTaxAmount.TabIndex = 512;
            this.txtExtendTaxAmount.ValueChanged += new System.EventHandler(this.txtExtendTaxAmount_ValueChanged);
            this.txtExtendTaxAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ultraMaskedEdit3_KeyPress);
            // 
            // cbbExtensionCase
            // 
            this.cbbExtensionCase.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbExtensionCase.Location = new System.Drawing.Point(421, 1913);
            this.cbbExtensionCase.Name = "cbbExtensionCase";
            this.cbbExtensionCase.Size = new System.Drawing.Size(527, 21);
            this.cbbExtensionCase.TabIndex = 511;
            // 
            // txtItem1
            // 
            appearance16.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance16.TextHAlignAsString = "Left";
            this.txtItem1.Appearance = appearance16;
            this.txtItem1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtItem1.Location = new System.Drawing.Point(411, 52);
            this.txtItem1.Name = "txtItem1";
            this.txtItem1.Size = new System.Drawing.Size(445, 21);
            this.txtItem1.TabIndex = 509;
            this.txtItem1.Text = "[01] ";
            // 
            // ultraLabel321
            // 
            appearance17.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance17.TextHAlignAsString = "Left";
            this.ultraLabel321.Appearance = appearance17;
            this.ultraLabel321.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel321.Location = new System.Drawing.Point(80, 2141);
            this.ultraLabel321.Name = "ultraLabel321";
            this.ultraLabel321.Size = new System.Drawing.Size(414, 21);
            this.ultraLabel321.TabIndex = 496;
            this.ultraLabel321.Text = "I. Ngoài các Phụ Lục của tờ khai này, chúng tôi gửi kèm theo các tài liệu sau:";
            // 
            // ultraLabel320
            // 
            appearance18.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel320.Appearance = appearance18;
            this.ultraLabel320.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel320.Location = new System.Drawing.Point(118, 2105);
            this.ultraLabel320.Name = "ultraLabel320";
            this.ultraLabel320.Size = new System.Drawing.Size(145, 16);
            this.ultraLabel320.TabIndex = 495;
            this.ultraLabel320.Text = "[M2] Số tiền chậm nộp:";
            // 
            // dteToDateLate
            // 
            this.dteToDateLate.AutoSize = false;
            this.dteToDateLate.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteToDateLate.Location = new System.Drawing.Point(818, 2075);
            this.dteToDateLate.MaskInput = "dd/mm/yyyy";
            this.dteToDateLate.Name = "dteToDateLate";
            this.dteToDateLate.Size = new System.Drawing.Size(130, 22);
            this.dteToDateLate.TabIndex = 60;
            this.dteToDateLate.ValueChanged += new System.EventHandler(this.dteToDateLate_ValueChanged);
            // 
            // ultraLabel319
            // 
            appearance19.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel319.Appearance = appearance19;
            this.ultraLabel319.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel319.Location = new System.Drawing.Point(723, 2079);
            this.ultraLabel319.Name = "ultraLabel319";
            this.ultraLabel319.Size = new System.Drawing.Size(62, 16);
            this.ultraLabel319.TabIndex = 492;
            this.ultraLabel319.Text = "đến ngày";
            // 
            // dteFromDateLate
            // 
            this.dteFromDateLate.AutoSize = false;
            this.dteFromDateLate.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteFromDateLate.Location = new System.Drawing.Point(567, 2073);
            this.dteFromDateLate.MaskInput = "dd/mm/yyyy";
            this.dteFromDateLate.Name = "dteFromDateLate";
            this.dteFromDateLate.Size = new System.Drawing.Size(130, 22);
            this.dteFromDateLate.TabIndex = 59;
            this.dteFromDateLate.ValueChanged += new System.EventHandler(this.dteToDateLate_ValueChanged);
            // 
            // ultraLabel318
            // 
            appearance20.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel318.Appearance = appearance20;
            this.ultraLabel318.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel318.Location = new System.Drawing.Point(449, 2077);
            this.ultraLabel318.Name = "ultraLabel318";
            this.ultraLabel318.Size = new System.Drawing.Size(83, 16);
            this.ultraLabel318.TabIndex = 490;
            this.ultraLabel318.Text = "ngày, từ ngày";
            // 
            // ultraLabel317
            // 
            appearance21.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel317.Appearance = appearance21;
            this.ultraLabel317.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel317.Location = new System.Drawing.Point(118, 2077);
            this.ultraLabel317.Name = "ultraLabel317";
            this.ultraLabel317.Size = new System.Drawing.Size(145, 16);
            this.ultraLabel317.TabIndex = 488;
            this.ultraLabel317.Text = "[M1] Số ngày chậm nộp";
            // 
            // ultraLabel316
            // 
            appearance22.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance22.TextHAlignAsString = "Left";
            this.ultraLabel316.Appearance = appearance22;
            this.ultraLabel316.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel316.Location = new System.Drawing.Point(80, 2036);
            this.ultraLabel316.Name = "ultraLabel316";
            this.ultraLabel316.Size = new System.Drawing.Size(586, 31);
            this.ultraLabel316.TabIndex = 487;
            this.ultraLabel316.Text = resources.GetString("ultraLabel316.Text");
            // 
            // ultraLabel315
            // 
            appearance23.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel315.Appearance = appearance23;
            this.ultraLabel315.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel315.Location = new System.Drawing.Point(118, 2008);
            this.ultraLabel315.Name = "ultraLabel315";
            this.ultraLabel315.Size = new System.Drawing.Size(297, 16);
            this.ultraLabel315.TabIndex = 486;
            this.ultraLabel315.Text = "[L5] Số thuế TNDN không được gia hạn:";
            // 
            // ultraLabel314
            // 
            appearance24.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel314.Appearance = appearance24;
            this.ultraLabel314.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel314.Location = new System.Drawing.Point(118, 1979);
            this.ultraLabel314.Name = "ultraLabel314";
            this.ultraLabel314.Size = new System.Drawing.Size(297, 16);
            this.ultraLabel314.TabIndex = 484;
            this.ultraLabel314.Text = "[L4] Số thuế TNDN được gia hạn:";
            // 
            // dteExtendTime
            // 
            this.dteExtendTime.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteExtendTime.Location = new System.Drawing.Point(421, 1944);
            this.dteExtendTime.MaskInput = "dd/mm/yyyy";
            this.dteExtendTime.Name = "dteExtendTime";
            this.dteExtendTime.Size = new System.Drawing.Size(111, 22);
            this.dteExtendTime.TabIndex = 55;
            // 
            // ultraLabel313
            // 
            appearance25.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel313.Appearance = appearance25;
            this.ultraLabel313.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel313.Location = new System.Drawing.Point(118, 1950);
            this.ultraLabel313.Name = "ultraLabel313";
            this.ultraLabel313.Size = new System.Drawing.Size(297, 16);
            this.ultraLabel313.TabIndex = 481;
            this.ultraLabel313.Text = "[L3] Thời hạn được gia hạn:";
            // 
            // ultraLabel312
            // 
            appearance26.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel312.Appearance = appearance26;
            this.ultraLabel312.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel312.Location = new System.Drawing.Point(118, 1918);
            this.ultraLabel312.Name = "ultraLabel312";
            this.ultraLabel312.Size = new System.Drawing.Size(297, 16);
            this.ultraLabel312.TabIndex = 479;
            this.ultraLabel312.Text = "[L2] Trường hợp được gia hạn nộp thuế TNDN theo:";
            // 
            // txtIsExtend
            // 
            this.txtIsExtend.BackColor = System.Drawing.Color.Transparent;
            this.txtIsExtend.BackColorInternal = System.Drawing.Color.Transparent;
            this.txtIsExtend.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txtIsExtend.Location = new System.Drawing.Point(421, 1886);
            this.txtIsExtend.Name = "txtIsExtend";
            this.txtIsExtend.Size = new System.Drawing.Size(14, 20);
            this.txtIsExtend.TabIndex = 478;
            this.txtIsExtend.CheckedValueChanged += new System.EventHandler(this.txtIsExtend_CheckedValueChanged);
            // 
            // ultraLabel311
            // 
            appearance27.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel311.Appearance = appearance27;
            this.ultraLabel311.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel311.Location = new System.Drawing.Point(118, 1890);
            this.ultraLabel311.Name = "ultraLabel311";
            this.ultraLabel311.Size = new System.Drawing.Size(167, 16);
            this.ultraLabel311.TabIndex = 477;
            this.ultraLabel311.Text = "[L1] Đối tượng được gia hạn";
            // 
            // ultraLabel310
            // 
            appearance28.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance28.TextHAlignAsString = "Left";
            this.ultraLabel310.Appearance = appearance28;
            this.ultraLabel310.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel310.Location = new System.Drawing.Point(80, 1861);
            this.ultraLabel310.Name = "ultraLabel310";
            this.ultraLabel310.Size = new System.Drawing.Size(288, 21);
            this.ultraLabel310.TabIndex = 476;
            this.ultraLabel310.Text = "L. Gia hạn nộp thuế (nếu có):";
            // 
            // ultraPanel74
            // 
            appearance29.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance29.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance29.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel74.Appearance = appearance29;
            this.ultraPanel74.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel74.ClientArea
            // 
            this.ultraPanel74.ClientArea.Controls.Add(this.txtItemI);
            this.ultraPanel74.Location = new System.Drawing.Point(723, 1817);
            this.ultraPanel74.Name = "ultraPanel74";
            this.ultraPanel74.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel74.TabIndex = 475;
            // 
            // txtItemI
            // 
            appearance30.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance30.FontData.BoldAsString = "True";
            appearance30.TextHAlignAsString = "Right";
            this.txtItemI.Appearance = appearance30;
            this.txtItemI.AutoSize = false;
            this.txtItemI.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemI.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemI.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemI.Location = new System.Drawing.Point(0, 0);
            this.txtItemI.Name = "txtItemI";
            this.txtItemI.NullText = "0";
            this.txtItemI.PromptChar = ' ';
            this.txtItemI.ReadOnly = true;
            this.txtItemI.Size = new System.Drawing.Size(225, 27);
            this.txtItemI.TabIndex = 6;
            this.txtItemI.ValueChanged += new System.EventHandler(this.txtItemI_ValueChanged);
            // 
            // ultraLabel307
            // 
            appearance31.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance31.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance31.BorderColor = System.Drawing.Color.Black;
            appearance31.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance31.TextHAlignAsString = "Center";
            appearance31.TextVAlignAsString = "Middle";
            this.ultraLabel307.Appearance = appearance31;
            this.ultraLabel307.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel307.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel307.Location = new System.Drawing.Point(567, 1817);
            this.ultraLabel307.Name = "ultraLabel307";
            this.ultraLabel307.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel307.TabIndex = 474;
            this.ultraLabel307.Text = "I";
            // 
            // NameI
            // 
            appearance32.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance32.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance32.BorderColor = System.Drawing.Color.Black;
            appearance32.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance32.TextHAlignAsString = "Left";
            appearance32.TextVAlignAsString = "Middle";
            this.NameI.Appearance = appearance32;
            this.NameI.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameI.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameI.Location = new System.Drawing.Point(118, 1817);
            this.NameI.Name = "NameI";
            this.NameI.Size = new System.Drawing.Size(450, 29);
            this.NameI.TabIndex = 473;
            this.NameI.Text = "Chênh lệch giữa số thuế TNDN còn phải nộp với 20% số thuế TNDN phải nộp (I = G - " +
    "H)";
            // 
            // ultraLabel309
            // 
            appearance33.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance33.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance33.BorderColor = System.Drawing.Color.Black;
            appearance33.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance33.TextHAlignAsString = "Center";
            appearance33.TextVAlignAsString = "Middle";
            this.ultraLabel309.Appearance = appearance33;
            this.ultraLabel309.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel309.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel309.Location = new System.Drawing.Point(80, 1817);
            this.ultraLabel309.Name = "ultraLabel309";
            this.ultraLabel309.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel309.TabIndex = 472;
            this.ultraLabel309.Text = "I";
            // 
            // ultraPanel73
            // 
            appearance34.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance34.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance34.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel73.Appearance = appearance34;
            this.ultraPanel73.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel73.ClientArea
            // 
            this.ultraPanel73.ClientArea.Controls.Add(this.txtItemH);
            this.ultraPanel73.Location = new System.Drawing.Point(723, 1789);
            this.ultraPanel73.Name = "ultraPanel73";
            this.ultraPanel73.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel73.TabIndex = 471;
            // 
            // txtItemH
            // 
            appearance35.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance35.FontData.BoldAsString = "True";
            appearance35.TextHAlignAsString = "Right";
            this.txtItemH.Appearance = appearance35;
            this.txtItemH.AutoSize = false;
            this.txtItemH.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemH.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemH.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemH.Location = new System.Drawing.Point(0, 0);
            this.txtItemH.Name = "txtItemH";
            this.txtItemH.NullText = "0";
            this.txtItemH.PromptChar = ' ';
            this.txtItemH.ReadOnly = true;
            this.txtItemH.Size = new System.Drawing.Size(225, 27);
            this.txtItemH.TabIndex = 6;
            this.txtItemH.ValueChanged += new System.EventHandler(this.txtItemG_ValueChanged);
            // 
            // ultraLabel304
            // 
            appearance36.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance36.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance36.BorderColor = System.Drawing.Color.Black;
            appearance36.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance36.TextHAlignAsString = "Center";
            appearance36.TextVAlignAsString = "Middle";
            this.ultraLabel304.Appearance = appearance36;
            this.ultraLabel304.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel304.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel304.Location = new System.Drawing.Point(567, 1789);
            this.ultraLabel304.Name = "ultraLabel304";
            this.ultraLabel304.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel304.TabIndex = 470;
            this.ultraLabel304.Text = "H";
            // 
            // NameH
            // 
            appearance37.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance37.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance37.BorderColor = System.Drawing.Color.Black;
            appearance37.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance37.TextHAlignAsString = "Left";
            appearance37.TextVAlignAsString = "Middle";
            this.NameH.Appearance = appearance37;
            this.NameH.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameH.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameH.Location = new System.Drawing.Point(118, 1789);
            this.NameH.Name = "NameH";
            this.NameH.Size = new System.Drawing.Size(450, 29);
            this.NameH.TabIndex = 469;
            this.NameH.Text = "20% số thuế TNDN phải nộp (H = D * 20%)";
            // 
            // ultraLabel306
            // 
            appearance38.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance38.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance38.BorderColor = System.Drawing.Color.Black;
            appearance38.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance38.TextHAlignAsString = "Center";
            appearance38.TextVAlignAsString = "Middle";
            this.ultraLabel306.Appearance = appearance38;
            this.ultraLabel306.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel306.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel306.Location = new System.Drawing.Point(80, 1789);
            this.ultraLabel306.Name = "ultraLabel306";
            this.ultraLabel306.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel306.TabIndex = 468;
            this.ultraLabel306.Text = "H";
            // 
            // ultraPanel69
            // 
            appearance39.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance39.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance39.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel69.Appearance = appearance39;
            this.ultraPanel69.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel69.ClientArea
            // 
            this.ultraPanel69.ClientArea.Controls.Add(this.txtItemG2);
            this.ultraPanel69.Location = new System.Drawing.Point(723, 1733);
            this.ultraPanel69.Name = "ultraPanel69";
            this.ultraPanel69.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel69.TabIndex = 467;
            // 
            // txtItemG2
            // 
            appearance40.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance40.TextHAlignAsString = "Right";
            this.txtItemG2.Appearance = appearance40;
            this.txtItemG2.AutoSize = false;
            this.txtItemG2.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemG2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemG2.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemG2.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemG2.Location = new System.Drawing.Point(0, 0);
            this.txtItemG2.Name = "txtItemG2";
            this.txtItemG2.NullText = "0";
            this.txtItemG2.PromptChar = ' ';
            this.txtItemG2.ReadOnly = true;
            this.txtItemG2.Size = new System.Drawing.Size(225, 27);
            this.txtItemG2.TabIndex = 6;
            this.txtItemG2.ValueChanged += new System.EventHandler(this.txtItemG1_ValueChanged);
            // 
            // ultraPanel70
            // 
            appearance41.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance41.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance41.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel70.Appearance = appearance41;
            this.ultraPanel70.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel70.ClientArea
            // 
            this.ultraPanel70.ClientArea.Controls.Add(this.txtItemG3);
            this.ultraPanel70.Location = new System.Drawing.Point(723, 1761);
            this.ultraPanel70.Name = "ultraPanel70";
            this.ultraPanel70.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel70.TabIndex = 466;
            // 
            // txtItemG3
            // 
            appearance42.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance42.TextHAlignAsString = "Right";
            this.txtItemG3.Appearance = appearance42;
            this.txtItemG3.AutoSize = false;
            this.txtItemG3.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemG3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemG3.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemG3.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemG3.Location = new System.Drawing.Point(0, 0);
            this.txtItemG3.Name = "txtItemG3";
            this.txtItemG3.NullText = "0";
            this.txtItemG3.PromptChar = ' ';
            this.txtItemG3.ReadOnly = true;
            this.txtItemG3.Size = new System.Drawing.Size(225, 27);
            this.txtItemG3.TabIndex = 6;
            this.txtItemG3.ValueChanged += new System.EventHandler(this.txtItemG1_ValueChanged);
            // 
            // ultraPanel71
            // 
            appearance43.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance43.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance43.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel71.Appearance = appearance43;
            this.ultraPanel71.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel71.ClientArea
            // 
            this.ultraPanel71.ClientArea.Controls.Add(this.txtItemG1);
            this.ultraPanel71.Location = new System.Drawing.Point(723, 1705);
            this.ultraPanel71.Name = "ultraPanel71";
            this.ultraPanel71.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel71.TabIndex = 465;
            // 
            // txtItemG1
            // 
            appearance44.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance44.TextHAlignAsString = "Right";
            this.txtItemG1.Appearance = appearance44;
            this.txtItemG1.AutoSize = false;
            this.txtItemG1.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemG1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemG1.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemG1.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemG1.Location = new System.Drawing.Point(0, 0);
            this.txtItemG1.Name = "txtItemG1";
            this.txtItemG1.NullText = "0";
            this.txtItemG1.PromptChar = ' ';
            this.txtItemG1.ReadOnly = true;
            this.txtItemG1.Size = new System.Drawing.Size(225, 27);
            this.txtItemG1.TabIndex = 6;
            this.txtItemG1.ValueChanged += new System.EventHandler(this.txtItemG1_ValueChanged);
            // 
            // ultraLabel292
            // 
            appearance45.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance45.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance45.BorderColor = System.Drawing.Color.Black;
            appearance45.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance45.TextHAlignAsString = "Center";
            appearance45.TextVAlignAsString = "Middle";
            this.ultraLabel292.Appearance = appearance45;
            this.ultraLabel292.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel292.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel292.Location = new System.Drawing.Point(567, 1761);
            this.ultraLabel292.Name = "ultraLabel292";
            this.ultraLabel292.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel292.TabIndex = 464;
            this.ultraLabel292.Text = "G3";
            // 
            // NameG3
            // 
            appearance46.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance46.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance46.BorderColor = System.Drawing.Color.Black;
            appearance46.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance46.TextHAlignAsString = "Left";
            appearance46.TextVAlignAsString = "Middle";
            this.NameG3.Appearance = appearance46;
            this.NameG3.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameG3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameG3.Location = new System.Drawing.Point(118, 1761);
            this.NameG3.Name = "NameG3";
            this.NameG3.Size = new System.Drawing.Size(450, 29);
            this.NameG3.TabIndex = 463;
            this.NameG3.Text = "Thuế TNDN phải nộp khác (nếu có) (G3 = D3 - E3)";
            // 
            // ultraLabel294
            // 
            appearance47.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance47.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance47.BorderColor = System.Drawing.Color.Black;
            appearance47.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance47.TextHAlignAsString = "Center";
            appearance47.TextVAlignAsString = "Middle";
            this.ultraLabel294.Appearance = appearance47;
            this.ultraLabel294.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel294.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel294.Location = new System.Drawing.Point(80, 1761);
            this.ultraLabel294.Name = "ultraLabel294";
            this.ultraLabel294.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel294.TabIndex = 462;
            this.ultraLabel294.Text = "3";
            // 
            // ultraLabel295
            // 
            appearance48.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance48.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance48.BorderColor = System.Drawing.Color.Black;
            appearance48.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance48.TextHAlignAsString = "Center";
            appearance48.TextVAlignAsString = "Middle";
            this.ultraLabel295.Appearance = appearance48;
            this.ultraLabel295.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel295.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel295.Location = new System.Drawing.Point(567, 1733);
            this.ultraLabel295.Name = "ultraLabel295";
            this.ultraLabel295.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel295.TabIndex = 461;
            this.ultraLabel295.Text = "G2";
            // 
            // NameG2
            // 
            appearance49.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance49.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance49.BorderColor = System.Drawing.Color.Black;
            appearance49.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance49.TextHAlignAsString = "Left";
            appearance49.TextVAlignAsString = "Middle";
            this.NameG2.Appearance = appearance49;
            this.NameG2.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameG2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameG2.Location = new System.Drawing.Point(118, 1733);
            this.NameG2.Name = "NameG2";
            this.NameG2.Size = new System.Drawing.Size(450, 29);
            this.NameG2.TabIndex = 460;
            this.NameG2.Text = "Thuế TNDN từ hoạt động chuyển nhượng bất động sản (G2 = D2 - E2)";
            // 
            // ultraLabel297
            // 
            appearance50.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance50.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance50.BorderColor = System.Drawing.Color.Black;
            appearance50.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance50.TextHAlignAsString = "Center";
            appearance50.TextVAlignAsString = "Middle";
            this.ultraLabel297.Appearance = appearance50;
            this.ultraLabel297.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel297.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel297.Location = new System.Drawing.Point(80, 1733);
            this.ultraLabel297.Name = "ultraLabel297";
            this.ultraLabel297.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel297.TabIndex = 459;
            this.ultraLabel297.Text = "2";
            // 
            // ultraLabel298
            // 
            appearance51.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance51.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance51.BorderColor = System.Drawing.Color.Black;
            appearance51.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance51.TextHAlignAsString = "Center";
            appearance51.TextVAlignAsString = "Middle";
            this.ultraLabel298.Appearance = appearance51;
            this.ultraLabel298.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel298.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel298.Location = new System.Drawing.Point(567, 1705);
            this.ultraLabel298.Name = "ultraLabel298";
            this.ultraLabel298.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel298.TabIndex = 458;
            this.ultraLabel298.Text = "G1";
            // 
            // NameG1
            // 
            appearance52.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance52.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance52.BorderColor = System.Drawing.Color.Black;
            appearance52.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance52.TextHAlignAsString = "Left";
            appearance52.TextVAlignAsString = "Middle";
            this.NameG1.Appearance = appearance52;
            this.NameG1.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameG1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameG1.Location = new System.Drawing.Point(118, 1705);
            this.NameG1.Name = "NameG1";
            this.NameG1.Size = new System.Drawing.Size(450, 29);
            this.NameG1.TabIndex = 457;
            this.NameG1.Text = "Thuế TNDN của hoạt động sản xuất kinh doanh (G1 = D1 - E1)";
            // 
            // ultraLabel300
            // 
            appearance53.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance53.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance53.BorderColor = System.Drawing.Color.Black;
            appearance53.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance53.TextHAlignAsString = "Center";
            appearance53.TextVAlignAsString = "Middle";
            this.ultraLabel300.Appearance = appearance53;
            this.ultraLabel300.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel300.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel300.Location = new System.Drawing.Point(80, 1705);
            this.ultraLabel300.Name = "ultraLabel300";
            this.ultraLabel300.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel300.TabIndex = 456;
            this.ultraLabel300.Text = "1";
            // 
            // ultraPanel72
            // 
            appearance54.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance54.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance54.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel72.Appearance = appearance54;
            this.ultraPanel72.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel72.ClientArea
            // 
            this.ultraPanel72.ClientArea.Controls.Add(this.txtItemG);
            this.ultraPanel72.Location = new System.Drawing.Point(723, 1677);
            this.ultraPanel72.Name = "ultraPanel72";
            this.ultraPanel72.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel72.TabIndex = 455;
            // 
            // txtItemG
            // 
            appearance55.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance55.FontData.BoldAsString = "True";
            appearance55.TextHAlignAsString = "Right";
            this.txtItemG.Appearance = appearance55;
            this.txtItemG.AutoSize = false;
            this.txtItemG.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemG.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemG.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemG.Location = new System.Drawing.Point(0, 0);
            this.txtItemG.Name = "txtItemG";
            this.txtItemG.NullText = "0";
            this.txtItemG.PromptChar = ' ';
            this.txtItemG.ReadOnly = true;
            this.txtItemG.Size = new System.Drawing.Size(225, 27);
            this.txtItemG.TabIndex = 6;
            this.txtItemG.ValueChanged += new System.EventHandler(this.txtItemG_ValueChanged);
            // 
            // ultraLabel301
            // 
            appearance56.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance56.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance56.BorderColor = System.Drawing.Color.Black;
            appearance56.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance56.TextHAlignAsString = "Center";
            appearance56.TextVAlignAsString = "Middle";
            this.ultraLabel301.Appearance = appearance56;
            this.ultraLabel301.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel301.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel301.Location = new System.Drawing.Point(567, 1677);
            this.ultraLabel301.Name = "ultraLabel301";
            this.ultraLabel301.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel301.TabIndex = 454;
            this.ultraLabel301.Text = "G";
            // 
            // NameG
            // 
            appearance57.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance57.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance57.BorderColor = System.Drawing.Color.Black;
            appearance57.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance57.TextHAlignAsString = "Left";
            appearance57.TextVAlignAsString = "Middle";
            this.NameG.Appearance = appearance57;
            this.NameG.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameG.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameG.Location = new System.Drawing.Point(118, 1677);
            this.NameG.Name = "NameG";
            this.NameG.Size = new System.Drawing.Size(450, 29);
            this.NameG.TabIndex = 453;
            this.NameG.Text = "Tổng số thuế TNDN còn phải nộp (G = G1 + G2 + G3)";
            // 
            // ultraLabel303
            // 
            appearance58.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance58.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance58.BorderColor = System.Drawing.Color.Black;
            appearance58.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance58.TextHAlignAsString = "Center";
            appearance58.TextVAlignAsString = "Middle";
            this.ultraLabel303.Appearance = appearance58;
            this.ultraLabel303.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel303.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel303.Location = new System.Drawing.Point(80, 1677);
            this.ultraLabel303.Name = "ultraLabel303";
            this.ultraLabel303.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel303.TabIndex = 452;
            this.ultraLabel303.Text = "G";
            // 
            // ultraPanel68
            // 
            appearance59.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance59.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance59.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel68.Appearance = appearance59;
            this.ultraPanel68.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel68.ClientArea
            // 
            this.ultraPanel68.ClientArea.Controls.Add(this.txtItemE2);
            this.ultraPanel68.Location = new System.Drawing.Point(723, 1621);
            this.ultraPanel68.Name = "ultraPanel68";
            this.ultraPanel68.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel68.TabIndex = 451;
            // 
            // txtItemE2
            // 
            appearance60.TextHAlignAsString = "Right";
            this.txtItemE2.Appearance = appearance60;
            this.txtItemE2.AutoSize = false;
            this.txtItemE2.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemE2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemE2.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemE2.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemE2.Location = new System.Drawing.Point(0, 0);
            this.txtItemE2.Name = "txtItemE2";
            this.txtItemE2.NullText = "0";
            this.txtItemE2.PromptChar = ' ';
            this.txtItemE2.Size = new System.Drawing.Size(225, 27);
            this.txtItemE2.TabIndex = 6;
            this.txtItemE2.ValueChanged += new System.EventHandler(this.txtItemE1_ValueChanged);
            this.txtItemE2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemE2.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraPanel67
            // 
            appearance61.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance61.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance61.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel67.Appearance = appearance61;
            this.ultraPanel67.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel67.ClientArea
            // 
            this.ultraPanel67.ClientArea.Controls.Add(this.txtItemE3);
            this.ultraPanel67.Location = new System.Drawing.Point(723, 1649);
            this.ultraPanel67.Name = "ultraPanel67";
            this.ultraPanel67.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel67.TabIndex = 450;
            // 
            // txtItemE3
            // 
            appearance62.TextHAlignAsString = "Right";
            this.txtItemE3.Appearance = appearance62;
            this.txtItemE3.AutoSize = false;
            this.txtItemE3.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemE3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemE3.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemE3.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemE3.Location = new System.Drawing.Point(0, 0);
            this.txtItemE3.Name = "txtItemE3";
            this.txtItemE3.NullText = "0";
            this.txtItemE3.PromptChar = ' ';
            this.txtItemE3.Size = new System.Drawing.Size(225, 27);
            this.txtItemE3.TabIndex = 6;
            this.txtItemE3.ValueChanged += new System.EventHandler(this.txtItemE1_ValueChanged);
            this.txtItemE3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemE3.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraPanel66
            // 
            appearance63.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance63.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance63.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel66.Appearance = appearance63;
            this.ultraPanel66.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel66.ClientArea
            // 
            this.ultraPanel66.ClientArea.Controls.Add(this.txtItemE1);
            this.ultraPanel66.Location = new System.Drawing.Point(723, 1593);
            this.ultraPanel66.Name = "ultraPanel66";
            this.ultraPanel66.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel66.TabIndex = 449;
            // 
            // txtItemE1
            // 
            appearance64.TextHAlignAsString = "Right";
            this.txtItemE1.Appearance = appearance64;
            this.txtItemE1.AutoSize = false;
            this.txtItemE1.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemE1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemE1.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemE1.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemE1.Location = new System.Drawing.Point(0, 0);
            this.txtItemE1.Name = "txtItemE1";
            this.txtItemE1.NullText = "0";
            this.txtItemE1.PromptChar = ' ';
            this.txtItemE1.Size = new System.Drawing.Size(225, 27);
            this.txtItemE1.TabIndex = 6;
            this.txtItemE1.ValueChanged += new System.EventHandler(this.txtItemE1_ValueChanged);
            this.txtItemE1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemE1.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel283
            // 
            appearance65.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance65.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance65.BorderColor = System.Drawing.Color.Black;
            appearance65.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance65.TextHAlignAsString = "Center";
            appearance65.TextVAlignAsString = "Middle";
            this.ultraLabel283.Appearance = appearance65;
            this.ultraLabel283.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel283.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel283.Location = new System.Drawing.Point(567, 1649);
            this.ultraLabel283.Name = "ultraLabel283";
            this.ultraLabel283.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel283.TabIndex = 448;
            this.ultraLabel283.Text = "E3";
            // 
            // NameE3
            // 
            appearance66.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance66.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance66.BorderColor = System.Drawing.Color.Black;
            appearance66.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance66.TextHAlignAsString = "Left";
            appearance66.TextVAlignAsString = "Middle";
            this.NameE3.Appearance = appearance66;
            this.NameE3.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameE3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameE3.Location = new System.Drawing.Point(118, 1649);
            this.NameE3.Name = "NameE3";
            this.NameE3.Size = new System.Drawing.Size(450, 29);
            this.NameE3.TabIndex = 447;
            this.NameE3.Text = "Thuế TNDN phải nộp khác (nếu có)";
            // 
            // ultraLabel285
            // 
            appearance67.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance67.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance67.BorderColor = System.Drawing.Color.Black;
            appearance67.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance67.TextHAlignAsString = "Center";
            appearance67.TextVAlignAsString = "Middle";
            this.ultraLabel285.Appearance = appearance67;
            this.ultraLabel285.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel285.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel285.Location = new System.Drawing.Point(80, 1649);
            this.ultraLabel285.Name = "ultraLabel285";
            this.ultraLabel285.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel285.TabIndex = 446;
            this.ultraLabel285.Text = "3";
            // 
            // ultraLabel286
            // 
            appearance68.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance68.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance68.BorderColor = System.Drawing.Color.Black;
            appearance68.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance68.TextHAlignAsString = "Center";
            appearance68.TextVAlignAsString = "Middle";
            this.ultraLabel286.Appearance = appearance68;
            this.ultraLabel286.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel286.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel286.Location = new System.Drawing.Point(567, 1621);
            this.ultraLabel286.Name = "ultraLabel286";
            this.ultraLabel286.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel286.TabIndex = 445;
            this.ultraLabel286.Text = "E2";
            // 
            // NameE2
            // 
            appearance69.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance69.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance69.BorderColor = System.Drawing.Color.Black;
            appearance69.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance69.TextHAlignAsString = "Left";
            appearance69.TextVAlignAsString = "Middle";
            this.NameE2.Appearance = appearance69;
            this.NameE2.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameE2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameE2.Location = new System.Drawing.Point(118, 1621);
            this.NameE2.Name = "NameE2";
            this.NameE2.Size = new System.Drawing.Size(450, 29);
            this.NameE2.TabIndex = 444;
            this.NameE2.Text = "Thuế TNDN từ hoạt động chuyển nhượng bất động sản";
            // 
            // ultraLabel288
            // 
            appearance70.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance70.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance70.BorderColor = System.Drawing.Color.Black;
            appearance70.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance70.TextHAlignAsString = "Center";
            appearance70.TextVAlignAsString = "Middle";
            this.ultraLabel288.Appearance = appearance70;
            this.ultraLabel288.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel288.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel288.Location = new System.Drawing.Point(80, 1621);
            this.ultraLabel288.Name = "ultraLabel288";
            this.ultraLabel288.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel288.TabIndex = 443;
            this.ultraLabel288.Text = "2";
            // 
            // ultraLabel289
            // 
            appearance71.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance71.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance71.BorderColor = System.Drawing.Color.Black;
            appearance71.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance71.TextHAlignAsString = "Center";
            appearance71.TextVAlignAsString = "Middle";
            this.ultraLabel289.Appearance = appearance71;
            this.ultraLabel289.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel289.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel289.Location = new System.Drawing.Point(567, 1593);
            this.ultraLabel289.Name = "ultraLabel289";
            this.ultraLabel289.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel289.TabIndex = 442;
            this.ultraLabel289.Text = "E1";
            // 
            // NameE1
            // 
            appearance72.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance72.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance72.BorderColor = System.Drawing.Color.Black;
            appearance72.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance72.TextHAlignAsString = "Left";
            appearance72.TextVAlignAsString = "Middle";
            this.NameE1.Appearance = appearance72;
            this.NameE1.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameE1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameE1.Location = new System.Drawing.Point(118, 1593);
            this.NameE1.Name = "NameE1";
            this.NameE1.Size = new System.Drawing.Size(450, 29);
            this.NameE1.TabIndex = 441;
            this.NameE1.Text = "Thuế TNDN của hoạt động sản xuất kinh doanh";
            // 
            // ultraLabel291
            // 
            appearance73.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance73.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance73.BorderColor = System.Drawing.Color.Black;
            appearance73.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance73.TextHAlignAsString = "Center";
            appearance73.TextVAlignAsString = "Middle";
            this.ultraLabel291.Appearance = appearance73;
            this.ultraLabel291.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel291.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel291.Location = new System.Drawing.Point(80, 1593);
            this.ultraLabel291.Name = "ultraLabel291";
            this.ultraLabel291.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel291.TabIndex = 440;
            this.ultraLabel291.Text = "1";
            // 
            // ultraPanel64
            // 
            appearance74.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance74.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance74.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel64.Appearance = appearance74;
            this.ultraPanel64.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel64.ClientArea
            // 
            this.ultraPanel64.ClientArea.Controls.Add(this.txtItemD3);
            this.ultraPanel64.Location = new System.Drawing.Point(723, 1537);
            this.ultraPanel64.Name = "ultraPanel64";
            this.ultraPanel64.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel64.TabIndex = 439;
            // 
            // txtItemD3
            // 
            appearance75.TextHAlignAsString = "Right";
            this.txtItemD3.Appearance = appearance75;
            this.txtItemD3.AutoSize = false;
            this.txtItemD3.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemD3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemD3.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemD3.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemD3.Location = new System.Drawing.Point(0, 0);
            this.txtItemD3.Name = "txtItemD3";
            this.txtItemD3.NullText = "0";
            this.txtItemD3.PromptChar = ' ';
            this.txtItemD3.Size = new System.Drawing.Size(225, 27);
            this.txtItemD3.TabIndex = 6;
            this.txtItemD3.ValueChanged += new System.EventHandler(this.txtItemD1_ValueChanged);
            this.txtItemD3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemD3.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraPanel65
            // 
            appearance76.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance76.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance76.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel65.Appearance = appearance76;
            this.ultraPanel65.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel65.ClientArea
            // 
            this.ultraPanel65.ClientArea.Controls.Add(this.txtItemE);
            this.ultraPanel65.Location = new System.Drawing.Point(723, 1565);
            this.ultraPanel65.Name = "ultraPanel65";
            this.ultraPanel65.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel65.TabIndex = 438;
            // 
            // txtItemE
            // 
            appearance77.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance77.FontData.BoldAsString = "True";
            appearance77.TextHAlignAsString = "Right";
            this.txtItemE.Appearance = appearance77;
            this.txtItemE.AutoSize = false;
            this.txtItemE.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemE.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemE.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemE.Location = new System.Drawing.Point(0, 0);
            this.txtItemE.Name = "txtItemE";
            this.txtItemE.NullText = "0";
            this.txtItemE.PromptChar = ' ';
            this.txtItemE.ReadOnly = true;
            this.txtItemE.Size = new System.Drawing.Size(225, 27);
            this.txtItemE.TabIndex = 6;
            // 
            // ultraLabel280
            // 
            appearance78.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance78.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance78.BorderColor = System.Drawing.Color.Black;
            appearance78.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance78.TextHAlignAsString = "Center";
            appearance78.TextVAlignAsString = "Middle";
            this.ultraLabel280.Appearance = appearance78;
            this.ultraLabel280.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel280.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel280.Location = new System.Drawing.Point(567, 1565);
            this.ultraLabel280.Name = "ultraLabel280";
            this.ultraLabel280.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel280.TabIndex = 437;
            this.ultraLabel280.Text = "E";
            // 
            // NameE
            // 
            appearance79.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance79.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance79.BorderColor = System.Drawing.Color.Black;
            appearance79.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance79.TextHAlignAsString = "Left";
            appearance79.TextVAlignAsString = "Middle";
            this.NameE.Appearance = appearance79;
            this.NameE.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameE.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameE.Location = new System.Drawing.Point(118, 1565);
            this.NameE.Name = "NameE";
            this.NameE.Size = new System.Drawing.Size(450, 29);
            this.NameE.TabIndex = 436;
            this.NameE.Text = "Số thuế TNDN đã tạm nộp trong năm (E = E1+ E2 + E3)";
            // 
            // ultraLabel282
            // 
            appearance80.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance80.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance80.BorderColor = System.Drawing.Color.Black;
            appearance80.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance80.TextHAlignAsString = "Center";
            appearance80.TextVAlignAsString = "Middle";
            this.ultraLabel282.Appearance = appearance80;
            this.ultraLabel282.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel282.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel282.Location = new System.Drawing.Point(80, 1565);
            this.ultraLabel282.Name = "ultraLabel282";
            this.ultraLabel282.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel282.TabIndex = 435;
            this.ultraLabel282.Text = "E";
            // 
            // ultraLabel277
            // 
            appearance81.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance81.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance81.BorderColor = System.Drawing.Color.Black;
            appearance81.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance81.TextHAlignAsString = "Center";
            appearance81.TextVAlignAsString = "Middle";
            this.ultraLabel277.Appearance = appearance81;
            this.ultraLabel277.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel277.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel277.Location = new System.Drawing.Point(567, 1537);
            this.ultraLabel277.Name = "ultraLabel277";
            this.ultraLabel277.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel277.TabIndex = 433;
            this.ultraLabel277.Text = "D3";
            // 
            // NameD3
            // 
            appearance82.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance82.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance82.BorderColor = System.Drawing.Color.Black;
            appearance82.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance82.TextHAlignAsString = "Left";
            appearance82.TextVAlignAsString = "Middle";
            this.NameD3.Appearance = appearance82;
            this.NameD3.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameD3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameD3.Location = new System.Drawing.Point(118, 1537);
            this.NameD3.Name = "NameD3";
            this.NameD3.Size = new System.Drawing.Size(450, 29);
            this.NameD3.TabIndex = 432;
            this.NameD3.Text = "Thuế TNDN phải nộp khác (nếu có)";
            // 
            // ultraLabel279
            // 
            appearance83.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance83.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance83.BorderColor = System.Drawing.Color.Black;
            appearance83.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance83.TextHAlignAsString = "Center";
            appearance83.TextVAlignAsString = "Middle";
            this.ultraLabel279.Appearance = appearance83;
            this.ultraLabel279.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel279.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel279.Location = new System.Drawing.Point(80, 1537);
            this.ultraLabel279.Name = "ultraLabel279";
            this.ultraLabel279.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel279.TabIndex = 431;
            this.ultraLabel279.Text = "3";
            // 
            // ultraPanel63
            // 
            appearance84.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance84.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance84.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel63.Appearance = appearance84;
            this.ultraPanel63.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel63.ClientArea
            // 
            this.ultraPanel63.ClientArea.Controls.Add(this.txtItemD2);
            this.ultraPanel63.Location = new System.Drawing.Point(723, 1509);
            this.ultraPanel63.Name = "ultraPanel63";
            this.ultraPanel63.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel63.TabIndex = 430;
            // 
            // txtItemD2
            // 
            appearance85.TextHAlignAsString = "Right";
            this.txtItemD2.Appearance = appearance85;
            this.txtItemD2.AutoSize = false;
            this.txtItemD2.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemD2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemD2.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemD2.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemD2.Location = new System.Drawing.Point(0, 0);
            this.txtItemD2.Name = "txtItemD2";
            this.txtItemD2.NullText = "0";
            this.txtItemD2.PromptChar = ' ';
            this.txtItemD2.Size = new System.Drawing.Size(225, 27);
            this.txtItemD2.TabIndex = 6;
            this.txtItemD2.ValueChanged += new System.EventHandler(this.txtItemD1_ValueChanged);
            this.txtItemD2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemD2.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel274
            // 
            appearance86.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance86.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance86.BorderColor = System.Drawing.Color.Black;
            appearance86.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance86.TextHAlignAsString = "Center";
            appearance86.TextVAlignAsString = "Middle";
            this.ultraLabel274.Appearance = appearance86;
            this.ultraLabel274.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel274.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel274.Location = new System.Drawing.Point(567, 1509);
            this.ultraLabel274.Name = "ultraLabel274";
            this.ultraLabel274.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel274.TabIndex = 429;
            this.ultraLabel274.Text = "D2";
            // 
            // NameD2
            // 
            appearance87.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance87.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance87.BorderColor = System.Drawing.Color.Black;
            appearance87.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance87.TextHAlignAsString = "Left";
            appearance87.TextVAlignAsString = "Middle";
            this.NameD2.Appearance = appearance87;
            this.NameD2.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameD2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameD2.Location = new System.Drawing.Point(118, 1509);
            this.NameD2.Name = "NameD2";
            this.NameD2.Size = new System.Drawing.Size(450, 29);
            this.NameD2.TabIndex = 428;
            this.NameD2.Text = "Thuế TNDN từ hoạt động chuyển nhượng bất động sản";
            // 
            // ultraLabel276
            // 
            appearance88.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance88.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance88.BorderColor = System.Drawing.Color.Black;
            appearance88.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance88.TextHAlignAsString = "Center";
            appearance88.TextVAlignAsString = "Middle";
            this.ultraLabel276.Appearance = appearance88;
            this.ultraLabel276.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel276.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel276.Location = new System.Drawing.Point(80, 1509);
            this.ultraLabel276.Name = "ultraLabel276";
            this.ultraLabel276.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel276.TabIndex = 427;
            this.ultraLabel276.Text = "2";
            // 
            // ultraPanel62
            // 
            appearance89.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance89.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance89.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel62.Appearance = appearance89;
            this.ultraPanel62.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel62.ClientArea
            // 
            this.ultraPanel62.ClientArea.Controls.Add(this.txtItemD1);
            this.ultraPanel62.Location = new System.Drawing.Point(723, 1481);
            this.ultraPanel62.Name = "ultraPanel62";
            this.ultraPanel62.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel62.TabIndex = 426;
            // 
            // txtItemD1
            // 
            appearance90.TextHAlignAsString = "Right";
            this.txtItemD1.Appearance = appearance90;
            this.txtItemD1.AutoSize = false;
            this.txtItemD1.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemD1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemD1.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemD1.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemD1.Location = new System.Drawing.Point(0, 0);
            this.txtItemD1.Name = "txtItemD1";
            this.txtItemD1.NullText = "0";
            this.txtItemD1.PromptChar = ' ';
            this.txtItemD1.Size = new System.Drawing.Size(225, 27);
            this.txtItemD1.TabIndex = 6;
            this.txtItemD1.ValueChanged += new System.EventHandler(this.txtItemD1_ValueChanged);
            // 
            // ultraLabel271
            // 
            appearance91.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance91.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance91.BorderColor = System.Drawing.Color.Black;
            appearance91.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance91.TextHAlignAsString = "Center";
            appearance91.TextVAlignAsString = "Middle";
            this.ultraLabel271.Appearance = appearance91;
            this.ultraLabel271.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel271.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel271.Location = new System.Drawing.Point(567, 1481);
            this.ultraLabel271.Name = "ultraLabel271";
            this.ultraLabel271.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel271.TabIndex = 425;
            this.ultraLabel271.Text = "D1";
            // 
            // NameD1
            // 
            appearance92.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance92.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance92.BorderColor = System.Drawing.Color.Black;
            appearance92.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance92.TextHAlignAsString = "Left";
            appearance92.TextVAlignAsString = "Middle";
            this.NameD1.Appearance = appearance92;
            this.NameD1.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameD1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameD1.Location = new System.Drawing.Point(118, 1481);
            this.NameD1.Name = "NameD1";
            this.NameD1.Size = new System.Drawing.Size(450, 29);
            this.NameD1.TabIndex = 424;
            this.NameD1.Text = "Thuế TNDN của hoạt động sản xuất kinh doanh (D1 = C16)";
            // 
            // ultraLabel273
            // 
            appearance93.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance93.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance93.BorderColor = System.Drawing.Color.Black;
            appearance93.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance93.TextHAlignAsString = "Center";
            appearance93.TextVAlignAsString = "Middle";
            this.ultraLabel273.Appearance = appearance93;
            this.ultraLabel273.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel273.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel273.Location = new System.Drawing.Point(80, 1481);
            this.ultraLabel273.Name = "ultraLabel273";
            this.ultraLabel273.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel273.TabIndex = 423;
            this.ultraLabel273.Text = "1";
            // 
            // ultraPanel61
            // 
            appearance94.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance94.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance94.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel61.Appearance = appearance94;
            this.ultraPanel61.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel61.ClientArea
            // 
            this.ultraPanel61.ClientArea.Controls.Add(this.txtItemD);
            this.ultraPanel61.Location = new System.Drawing.Point(723, 1453);
            this.ultraPanel61.Name = "ultraPanel61";
            this.ultraPanel61.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel61.TabIndex = 422;
            // 
            // txtItemD
            // 
            appearance95.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance95.FontData.BoldAsString = "True";
            appearance95.TextHAlignAsString = "Right";
            this.txtItemD.Appearance = appearance95;
            this.txtItemD.AutoSize = false;
            this.txtItemD.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemD.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemD.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemD.Location = new System.Drawing.Point(0, 0);
            this.txtItemD.Name = "txtItemD";
            this.txtItemD.NullText = "0";
            this.txtItemD.PromptChar = ' ';
            this.txtItemD.ReadOnly = true;
            this.txtItemD.Size = new System.Drawing.Size(225, 27);
            this.txtItemD.TabIndex = 6;
            this.txtItemD.ValueChanged += new System.EventHandler(this.txtItemD_ValueChanged);
            // 
            // ultraLabel268
            // 
            appearance96.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance96.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance96.BorderColor = System.Drawing.Color.Black;
            appearance96.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance96.TextHAlignAsString = "Center";
            appearance96.TextVAlignAsString = "Middle";
            this.ultraLabel268.Appearance = appearance96;
            this.ultraLabel268.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel268.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel268.Location = new System.Drawing.Point(567, 1453);
            this.ultraLabel268.Name = "ultraLabel268";
            this.ultraLabel268.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel268.TabIndex = 421;
            this.ultraLabel268.Text = "D";
            // 
            // NameD
            // 
            appearance97.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance97.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance97.BorderColor = System.Drawing.Color.Black;
            appearance97.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance97.TextHAlignAsString = "Left";
            appearance97.TextVAlignAsString = "Middle";
            this.NameD.Appearance = appearance97;
            this.NameD.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameD.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameD.Location = new System.Drawing.Point(118, 1453);
            this.NameD.Name = "NameD";
            this.NameD.Size = new System.Drawing.Size(450, 29);
            this.NameD.TabIndex = 420;
            this.NameD.Text = "Tổng số thuế TNDN phải nộp (D = D1 + D2 + D3)";
            // 
            // ultraLabel270
            // 
            appearance98.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance98.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance98.BorderColor = System.Drawing.Color.Black;
            appearance98.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance98.TextHAlignAsString = "Center";
            appearance98.TextVAlignAsString = "Middle";
            this.ultraLabel270.Appearance = appearance98;
            this.ultraLabel270.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel270.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel270.Location = new System.Drawing.Point(80, 1453);
            this.ultraLabel270.Name = "ultraLabel270";
            this.ultraLabel270.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel270.TabIndex = 419;
            this.ultraLabel270.Text = "D";
            // 
            // ultraPanel60
            // 
            appearance99.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance99.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance99.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel60.Appearance = appearance99;
            this.ultraPanel60.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel60.ClientArea
            // 
            this.ultraPanel60.ClientArea.Controls.Add(this.txtItemC16);
            this.ultraPanel60.Location = new System.Drawing.Point(723, 1425);
            this.ultraPanel60.Name = "ultraPanel60";
            this.ultraPanel60.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel60.TabIndex = 418;
            // 
            // txtItemC16
            // 
            appearance100.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance100.FontData.BoldAsString = "False";
            appearance100.TextHAlignAsString = "Right";
            this.txtItemC16.Appearance = appearance100;
            this.txtItemC16.AutoSize = false;
            this.txtItemC16.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemC16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemC16.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemC16.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemC16.Location = new System.Drawing.Point(0, 0);
            this.txtItemC16.Name = "txtItemC16";
            this.txtItemC16.NullText = "0";
            this.txtItemC16.PromptChar = ' ';
            this.txtItemC16.ReadOnly = true;
            this.txtItemC16.Size = new System.Drawing.Size(225, 27);
            this.txtItemC16.TabIndex = 6;
            this.txtItemC16.ValueChanged += new System.EventHandler(this.txtItemC16_ValueChanged);
            // 
            // ultraLabel265
            // 
            appearance101.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance101.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance101.BorderColor = System.Drawing.Color.Black;
            appearance101.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance101.TextHAlignAsString = "Center";
            appearance101.TextVAlignAsString = "Middle";
            this.ultraLabel265.Appearance = appearance101;
            this.ultraLabel265.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel265.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel265.Location = new System.Drawing.Point(567, 1425);
            this.ultraLabel265.Name = "ultraLabel265";
            this.ultraLabel265.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel265.TabIndex = 417;
            this.ultraLabel265.Text = "C16";
            // 
            // NameC16
            // 
            appearance102.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance102.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance102.BorderColor = System.Drawing.Color.Black;
            appearance102.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance102.TextHAlignAsString = "Left";
            appearance102.TextVAlignAsString = "Middle";
            this.NameC16.Appearance = appearance102;
            this.NameC16.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC16.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC16.Location = new System.Drawing.Point(118, 1425);
            this.NameC16.Name = "NameC16";
            this.NameC16.Size = new System.Drawing.Size(450, 29);
            this.NameC16.TabIndex = 416;
            this.NameC16.Text = "Thuế TNDN của hoạt động sản xuất kinh doanh (C16 = C10 - C11 - C12 - C15)";
            // 
            // ultraLabel267
            // 
            appearance103.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance103.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance103.BorderColor = System.Drawing.Color.Black;
            appearance103.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance103.TextHAlignAsString = "Center";
            appearance103.TextVAlignAsString = "Middle";
            this.ultraLabel267.Appearance = appearance103;
            this.ultraLabel267.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel267.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel267.Location = new System.Drawing.Point(80, 1425);
            this.ultraLabel267.Name = "ultraLabel267";
            this.ultraLabel267.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel267.TabIndex = 415;
            this.ultraLabel267.Text = "11";
            // 
            // ultraPanel59
            // 
            appearance104.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance104.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance104.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel59.Appearance = appearance104;
            this.ultraPanel59.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel59.ClientArea
            // 
            this.ultraPanel59.ClientArea.Controls.Add(this.txtItemC15);
            this.ultraPanel59.Location = new System.Drawing.Point(723, 1397);
            this.ultraPanel59.Name = "ultraPanel59";
            this.ultraPanel59.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel59.TabIndex = 414;
            // 
            // txtItemC15
            // 
            appearance105.FontData.BoldAsString = "False";
            appearance105.TextHAlignAsString = "Right";
            this.txtItemC15.Appearance = appearance105;
            this.txtItemC15.AutoSize = false;
            this.txtItemC15.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemC15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemC15.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemC15.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemC15.Location = new System.Drawing.Point(0, 0);
            this.txtItemC15.Name = "txtItemC15";
            this.txtItemC15.NullText = "0";
            this.txtItemC15.PromptChar = ' ';
            this.txtItemC15.Size = new System.Drawing.Size(225, 27);
            this.txtItemC15.TabIndex = 6;
            this.txtItemC15.ValueChanged += new System.EventHandler(this.txtItemC10_ValueChanged);
            this.txtItemC15.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemC15.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel262
            // 
            appearance106.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance106.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance106.BorderColor = System.Drawing.Color.Black;
            appearance106.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance106.TextHAlignAsString = "Center";
            appearance106.TextVAlignAsString = "Middle";
            this.ultraLabel262.Appearance = appearance106;
            this.ultraLabel262.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel262.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel262.Location = new System.Drawing.Point(567, 1397);
            this.ultraLabel262.Name = "ultraLabel262";
            this.ultraLabel262.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel262.TabIndex = 413;
            this.ultraLabel262.Text = "C15";
            // 
            // NameC15
            // 
            appearance107.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance107.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance107.BorderColor = System.Drawing.Color.Black;
            appearance107.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance107.TextHAlignAsString = "Left";
            appearance107.TextVAlignAsString = "Middle";
            this.NameC15.Appearance = appearance107;
            this.NameC15.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC15.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC15.Location = new System.Drawing.Point(118, 1397);
            this.NameC15.Name = "NameC15";
            this.NameC15.Size = new System.Drawing.Size(450, 29);
            this.NameC15.TabIndex = 412;
            this.NameC15.Text = "Số thuế thu nhập đã nộp ở nước ngoài được trừ trong kỳ tính thuế";
            // 
            // ultraLabel264
            // 
            appearance108.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance108.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance108.BorderColor = System.Drawing.Color.Black;
            appearance108.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance108.TextHAlignAsString = "Center";
            appearance108.TextVAlignAsString = "Middle";
            this.ultraLabel264.Appearance = appearance108;
            this.ultraLabel264.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel264.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel264.Location = new System.Drawing.Point(80, 1397);
            this.ultraLabel264.Name = "ultraLabel264";
            this.ultraLabel264.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel264.TabIndex = 411;
            this.ultraLabel264.Text = "10";
            // 
            // ultraPanel58
            // 
            appearance109.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance109.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance109.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel58.Appearance = appearance109;
            this.ultraPanel58.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel58.ClientArea
            // 
            this.ultraPanel58.ClientArea.Controls.Add(this.txtItemC14);
            this.ultraPanel58.Location = new System.Drawing.Point(723, 1369);
            this.ultraPanel58.Name = "ultraPanel58";
            this.ultraPanel58.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel58.TabIndex = 410;
            // 
            // txtItemC14
            // 
            appearance110.FontData.BoldAsString = "False";
            appearance110.TextHAlignAsString = "Right";
            this.txtItemC14.Appearance = appearance110;
            this.txtItemC14.AutoSize = false;
            this.txtItemC14.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemC14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemC14.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemC14.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemC14.Location = new System.Drawing.Point(0, 0);
            this.txtItemC14.Name = "txtItemC14";
            this.txtItemC14.NullText = "0";
            this.txtItemC14.PromptChar = ' ';
            this.txtItemC14.Size = new System.Drawing.Size(225, 27);
            this.txtItemC14.TabIndex = 6;
            this.txtItemC14.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemC14.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel259
            // 
            appearance111.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance111.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance111.BorderColor = System.Drawing.Color.Black;
            appearance111.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance111.TextHAlignAsString = "Center";
            appearance111.TextVAlignAsString = "Middle";
            this.ultraLabel259.Appearance = appearance111;
            this.ultraLabel259.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel259.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel259.Location = new System.Drawing.Point(567, 1369);
            this.ultraLabel259.Name = "ultraLabel259";
            this.ultraLabel259.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel259.TabIndex = 409;
            this.ultraLabel259.Text = "C14";
            // 
            // NameC14
            // 
            appearance112.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance112.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance112.BorderColor = System.Drawing.Color.Black;
            appearance112.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance112.TextHAlignAsString = "Left";
            appearance112.TextVAlignAsString = "Middle";
            this.NameC14.Appearance = appearance112;
            this.NameC14.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC14.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC14.Location = new System.Drawing.Point(118, 1369);
            this.NameC14.Name = "NameC14";
            this.NameC14.Size = new System.Drawing.Size(450, 29);
            this.NameC14.TabIndex = 408;
            this.NameC14.Text = "                 + Số thuế được miễn, giảm không theo Luật Thuế TNDN";
            // 
            // ultraLabel261
            // 
            appearance113.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance113.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance113.BorderColor = System.Drawing.Color.Black;
            appearance113.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance113.TextHAlignAsString = "Center";
            appearance113.TextVAlignAsString = "Middle";
            this.ultraLabel261.Appearance = appearance113;
            this.ultraLabel261.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel261.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel261.Location = new System.Drawing.Point(80, 1369);
            this.ultraLabel261.Name = "ultraLabel261";
            this.ultraLabel261.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel261.TabIndex = 407;
            this.ultraLabel261.Text = "9.2";
            // 
            // ultraPanel57
            // 
            appearance114.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance114.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance114.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel57.Appearance = appearance114;
            this.ultraPanel57.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel57.ClientArea
            // 
            this.ultraPanel57.ClientArea.Controls.Add(this.txtItemC13);
            this.ultraPanel57.Location = new System.Drawing.Point(723, 1341);
            this.ultraPanel57.Name = "ultraPanel57";
            this.ultraPanel57.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel57.TabIndex = 406;
            // 
            // txtItemC13
            // 
            appearance115.FontData.BoldAsString = "False";
            appearance115.TextHAlignAsString = "Right";
            this.txtItemC13.Appearance = appearance115;
            this.txtItemC13.AutoSize = false;
            this.txtItemC13.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemC13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemC13.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemC13.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemC13.Location = new System.Drawing.Point(0, 0);
            this.txtItemC13.Name = "txtItemC13";
            this.txtItemC13.NullText = "0";
            this.txtItemC13.PromptChar = ' ';
            this.txtItemC13.Size = new System.Drawing.Size(225, 27);
            this.txtItemC13.TabIndex = 6;
            this.txtItemC13.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemC13.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel256
            // 
            appearance116.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance116.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance116.BorderColor = System.Drawing.Color.Black;
            appearance116.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance116.TextHAlignAsString = "Center";
            appearance116.TextVAlignAsString = "Middle";
            this.ultraLabel256.Appearance = appearance116;
            this.ultraLabel256.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel256.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel256.Location = new System.Drawing.Point(567, 1341);
            this.ultraLabel256.Name = "ultraLabel256";
            this.ultraLabel256.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel256.TabIndex = 405;
            this.ultraLabel256.Text = "C13";
            // 
            // NameC13
            // 
            appearance117.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance117.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance117.BorderColor = System.Drawing.Color.Black;
            appearance117.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance117.TextHAlignAsString = "Left";
            appearance117.TextVAlignAsString = "Middle";
            this.NameC13.Appearance = appearance117;
            this.NameC13.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC13.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC13.Location = new System.Drawing.Point(118, 1341);
            this.NameC13.Name = "NameC13";
            this.NameC13.Size = new System.Drawing.Size(450, 29);
            this.NameC13.TabIndex = 404;
            this.NameC13.Text = "Trong đó: + Số thuế TNDN được miễn, giảm theo Hiệp định";
            // 
            // ultraLabel258
            // 
            appearance118.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance118.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance118.BorderColor = System.Drawing.Color.Black;
            appearance118.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance118.TextHAlignAsString = "Center";
            appearance118.TextVAlignAsString = "Middle";
            this.ultraLabel258.Appearance = appearance118;
            this.ultraLabel258.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel258.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel258.Location = new System.Drawing.Point(80, 1341);
            this.ultraLabel258.Name = "ultraLabel258";
            this.ultraLabel258.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel258.TabIndex = 403;
            this.ultraLabel258.Text = "9.1";
            // 
            // ultraPanel56
            // 
            appearance119.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance119.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance119.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel56.Appearance = appearance119;
            this.ultraPanel56.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel56.ClientArea
            // 
            this.ultraPanel56.ClientArea.Controls.Add(this.txtItemC12);
            this.ultraPanel56.Location = new System.Drawing.Point(723, 1313);
            this.ultraPanel56.Name = "ultraPanel56";
            this.ultraPanel56.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel56.TabIndex = 402;
            // 
            // txtItemC12
            // 
            appearance120.TextHAlignAsString = "Right";
            this.txtItemC12.Appearance = appearance120;
            this.txtItemC12.AutoSize = false;
            this.txtItemC12.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemC12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemC12.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemC12.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemC12.Location = new System.Drawing.Point(0, 0);
            this.txtItemC12.Name = "txtItemC12";
            this.txtItemC12.NullText = "0";
            this.txtItemC12.PromptChar = ' ';
            this.txtItemC12.Size = new System.Drawing.Size(225, 27);
            this.txtItemC12.TabIndex = 6;
            this.txtItemC12.ValueChanged += new System.EventHandler(this.txtItemC10_ValueChanged);
            this.txtItemC12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemC12.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel253
            // 
            appearance121.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance121.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance121.BorderColor = System.Drawing.Color.Black;
            appearance121.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance121.TextHAlignAsString = "Center";
            appearance121.TextVAlignAsString = "Middle";
            this.ultraLabel253.Appearance = appearance121;
            this.ultraLabel253.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel253.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel253.Location = new System.Drawing.Point(567, 1313);
            this.ultraLabel253.Name = "ultraLabel253";
            this.ultraLabel253.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel253.TabIndex = 401;
            this.ultraLabel253.Text = "C12";
            // 
            // NameC12
            // 
            appearance122.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance122.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance122.BorderColor = System.Drawing.Color.Black;
            appearance122.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance122.TextHAlignAsString = "Left";
            appearance122.TextVAlignAsString = "Middle";
            this.NameC12.Appearance = appearance122;
            this.NameC12.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC12.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC12.Location = new System.Drawing.Point(118, 1313);
            this.NameC12.Name = "NameC12";
            this.NameC12.Size = new System.Drawing.Size(450, 29);
            this.NameC12.TabIndex = 400;
            this.NameC12.Text = "Thuế TNDN được miễn, giảm trong kỳ";
            // 
            // ultraLabel255
            // 
            appearance123.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance123.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance123.BorderColor = System.Drawing.Color.Black;
            appearance123.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance123.TextHAlignAsString = "Center";
            appearance123.TextVAlignAsString = "Middle";
            this.ultraLabel255.Appearance = appearance123;
            this.ultraLabel255.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel255.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel255.Location = new System.Drawing.Point(80, 1313);
            this.ultraLabel255.Name = "ultraLabel255";
            this.ultraLabel255.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel255.TabIndex = 399;
            this.ultraLabel255.Text = "9";
            // 
            // ultraPanel55
            // 
            appearance124.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance124.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance124.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel55.Appearance = appearance124;
            this.ultraPanel55.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel55.ClientArea
            // 
            this.ultraPanel55.ClientArea.Controls.Add(this.txtItemC11);
            this.ultraPanel55.Location = new System.Drawing.Point(723, 1285);
            this.ultraPanel55.Name = "ultraPanel55";
            this.ultraPanel55.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel55.TabIndex = 398;
            // 
            // txtItemC11
            // 
            appearance125.FontData.BoldAsString = "False";
            appearance125.TextHAlignAsString = "Right";
            this.txtItemC11.Appearance = appearance125;
            this.txtItemC11.AutoSize = false;
            this.txtItemC11.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemC11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemC11.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemC11.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemC11.Location = new System.Drawing.Point(0, 0);
            this.txtItemC11.Name = "txtItemC11";
            this.txtItemC11.NullText = "0";
            this.txtItemC11.PromptChar = ' ';
            this.txtItemC11.Size = new System.Drawing.Size(225, 27);
            this.txtItemC11.TabIndex = 6;
            this.txtItemC11.ValueChanged += new System.EventHandler(this.txtItemC10_ValueChanged);
            this.txtItemC11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemC11.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel250
            // 
            appearance126.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance126.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance126.BorderColor = System.Drawing.Color.Black;
            appearance126.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance126.TextHAlignAsString = "Center";
            appearance126.TextVAlignAsString = "Middle";
            this.ultraLabel250.Appearance = appearance126;
            this.ultraLabel250.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel250.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel250.Location = new System.Drawing.Point(567, 1285);
            this.ultraLabel250.Name = "ultraLabel250";
            this.ultraLabel250.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel250.TabIndex = 397;
            this.ultraLabel250.Text = "C11";
            // 
            // NameC11
            // 
            appearance127.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance127.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance127.BorderColor = System.Drawing.Color.Black;
            appearance127.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance127.TextHAlignAsString = "Left";
            appearance127.TextVAlignAsString = "Middle";
            this.NameC11.Appearance = appearance127;
            this.NameC11.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC11.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC11.Location = new System.Drawing.Point(118, 1285);
            this.NameC11.Name = "NameC11";
            this.NameC11.Size = new System.Drawing.Size(450, 29);
            this.NameC11.TabIndex = 396;
            this.NameC11.Text = "Thuế TNDN chênh lệch do áp dụng mức thuế suất ưu đãi";
            // 
            // ultraLabel252
            // 
            appearance128.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance128.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance128.BorderColor = System.Drawing.Color.Black;
            appearance128.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance128.TextHAlignAsString = "Center";
            appearance128.TextVAlignAsString = "Middle";
            this.ultraLabel252.Appearance = appearance128;
            this.ultraLabel252.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel252.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel252.Location = new System.Drawing.Point(80, 1285);
            this.ultraLabel252.Name = "ultraLabel252";
            this.ultraLabel252.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel252.TabIndex = 395;
            this.ultraLabel252.Text = "8";
            // 
            // ultraPanel54
            // 
            appearance129.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance129.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance129.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel54.Appearance = appearance129;
            this.ultraPanel54.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel54.ClientArea
            // 
            this.ultraPanel54.ClientArea.Controls.Add(this.txtItemC10);
            this.ultraPanel54.Location = new System.Drawing.Point(723, 1257);
            this.ultraPanel54.Name = "ultraPanel54";
            this.ultraPanel54.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel54.TabIndex = 394;
            // 
            // txtItemC10
            // 
            appearance130.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance130.FontData.BoldAsString = "False";
            appearance130.TextHAlignAsString = "Right";
            this.txtItemC10.Appearance = appearance130;
            this.txtItemC10.AutoSize = false;
            this.txtItemC10.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemC10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemC10.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemC10.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemC10.Location = new System.Drawing.Point(0, 0);
            this.txtItemC10.Name = "txtItemC10";
            this.txtItemC10.NullText = "0";
            this.txtItemC10.PromptChar = ' ';
            this.txtItemC10.ReadOnly = true;
            this.txtItemC10.Size = new System.Drawing.Size(225, 27);
            this.txtItemC10.TabIndex = 6;
            this.txtItemC10.ValueChanged += new System.EventHandler(this.txtItemC10_ValueChanged);
            // 
            // ultraLabel247
            // 
            appearance131.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance131.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance131.BorderColor = System.Drawing.Color.Black;
            appearance131.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance131.TextHAlignAsString = "Center";
            appearance131.TextVAlignAsString = "Middle";
            this.ultraLabel247.Appearance = appearance131;
            this.ultraLabel247.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel247.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel247.Location = new System.Drawing.Point(567, 1257);
            this.ultraLabel247.Name = "ultraLabel247";
            this.ultraLabel247.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel247.TabIndex = 393;
            this.ultraLabel247.Text = "C10";
            // 
            // NameC10
            // 
            appearance132.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance132.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance132.BorderColor = System.Drawing.Color.Black;
            appearance132.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance132.TextHAlignAsString = "Left";
            appearance132.TextVAlignAsString = "Middle";
            this.NameC10.Appearance = appearance132;
            this.NameC10.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC10.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC10.Location = new System.Drawing.Point(118, 1257);
            this.NameC10.Name = "NameC10";
            this.NameC10.Size = new System.Drawing.Size(450, 29);
            this.NameC10.TabIndex = 392;
            this.NameC10.Text = "Thuế TNDN từ hoạt động SXKD tính theo thuế suất không ưu đãi (C10 = (C7 x 22%) + " +
    "(C8 x 20%) + (C9 x C9a))";
            // 
            // ultraLabel249
            // 
            appearance133.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance133.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance133.BorderColor = System.Drawing.Color.Black;
            appearance133.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance133.TextHAlignAsString = "Center";
            appearance133.TextVAlignAsString = "Middle";
            this.ultraLabel249.Appearance = appearance133;
            this.ultraLabel249.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel249.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel249.Location = new System.Drawing.Point(80, 1257);
            this.ultraLabel249.Name = "ultraLabel249";
            this.ultraLabel249.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel249.TabIndex = 391;
            this.ultraLabel249.Text = "7";
            // 
            // ultraPanel53
            // 
            appearance134.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance134.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance134.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel53.Appearance = appearance134;
            this.ultraPanel53.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel53.ClientArea
            // 
            this.ultraPanel53.ClientArea.Controls.Add(this.txtItemC9a);
            this.ultraPanel53.Location = new System.Drawing.Point(723, 1229);
            this.ultraPanel53.Name = "ultraPanel53";
            this.ultraPanel53.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel53.TabIndex = 390;
            // 
            // txtItemC9a
            // 
            appearance135.FontData.BoldAsString = "False";
            appearance135.TextHAlignAsString = "Right";
            this.txtItemC9a.Appearance = appearance135;
            this.txtItemC9a.AutoSize = false;
            this.txtItemC9a.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemC9a.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemC9a.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemC9a.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemC9a.Location = new System.Drawing.Point(0, 0);
            this.txtItemC9a.Name = "txtItemC9a";
            this.txtItemC9a.NullText = "0";
            this.txtItemC9a.PromptChar = ' ';
            this.txtItemC9a.Size = new System.Drawing.Size(225, 27);
            this.txtItemC9a.TabIndex = 6;
            this.txtItemC9a.ValueChanged += new System.EventHandler(this.txtItemC7_ValueChanged);
            this.txtItemC9a.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemC9a.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel245
            // 
            appearance136.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance136.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance136.BorderColor = System.Drawing.Color.Black;
            appearance136.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance136.TextHAlignAsString = "Center";
            appearance136.TextVAlignAsString = "Middle";
            this.ultraLabel245.Appearance = appearance136;
            this.ultraLabel245.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel245.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel245.Location = new System.Drawing.Point(567, 1229);
            this.ultraLabel245.Name = "ultraLabel245";
            this.ultraLabel245.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel245.TabIndex = 389;
            this.ultraLabel245.Text = "C9a";
            // 
            // NameC9a
            // 
            appearance137.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance137.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance137.BorderColor = System.Drawing.Color.Black;
            appearance137.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance137.TextHAlignAsString = "Left";
            appearance137.TextVAlignAsString = "Middle";
            this.NameC9a.Appearance = appearance137;
            this.NameC9a.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC9a.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC9a.Location = new System.Drawing.Point(118, 1229);
            this.NameC9a.Name = "NameC9a";
            this.NameC9a.Size = new System.Drawing.Size(450, 29);
            this.NameC9a.TabIndex = 388;
            this.NameC9a.Text = "                 + Thuế suất không ưu đãi khác (%)";
            // 
            // ultraPanel52
            // 
            appearance138.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance138.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance138.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel52.Appearance = appearance138;
            this.ultraPanel52.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel52.ClientArea
            // 
            this.ultraPanel52.ClientArea.Controls.Add(this.txtItemC9);
            this.ultraPanel52.Location = new System.Drawing.Point(723, 1201);
            this.ultraPanel52.Name = "ultraPanel52";
            this.ultraPanel52.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel52.TabIndex = 386;
            // 
            // txtItemC9
            // 
            appearance139.FontData.BoldAsString = "False";
            appearance139.TextHAlignAsString = "Right";
            this.txtItemC9.Appearance = appearance139;
            this.txtItemC9.AutoSize = false;
            this.txtItemC9.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemC9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemC9.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemC9.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemC9.Location = new System.Drawing.Point(0, 0);
            this.txtItemC9.Name = "txtItemC9";
            this.txtItemC9.NullText = "0";
            this.txtItemC9.PromptChar = ' ';
            this.txtItemC9.Size = new System.Drawing.Size(225, 27);
            this.txtItemC9.TabIndex = 6;
            this.txtItemC9.ValueChanged += new System.EventHandler(this.txtItemC7_ValueChanged);
            this.txtItemC9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemC9.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel242
            // 
            appearance140.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance140.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance140.BorderColor = System.Drawing.Color.Black;
            appearance140.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance140.TextHAlignAsString = "Center";
            appearance140.TextVAlignAsString = "Middle";
            this.ultraLabel242.Appearance = appearance140;
            this.ultraLabel242.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel242.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel242.Location = new System.Drawing.Point(567, 1201);
            this.ultraLabel242.Name = "ultraLabel242";
            this.ultraLabel242.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel242.TabIndex = 385;
            this.ultraLabel242.Text = "C9";
            // 
            // NameC9
            // 
            appearance141.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance141.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance141.BorderColor = System.Drawing.Color.Black;
            appearance141.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance141.TextHAlignAsString = "Left";
            appearance141.TextVAlignAsString = "Middle";
            this.NameC9.Appearance = appearance141;
            this.NameC9.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC9.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC9.Location = new System.Drawing.Point(118, 1201);
            this.NameC9.Name = "NameC9";
            this.NameC9.Size = new System.Drawing.Size(450, 29);
            this.NameC9.TabIndex = 384;
            this.NameC9.Text = "                 + Thu nhập tính thuế tính theo thuế suất không ưu đãi khác";
            // 
            // ultraLabel244
            // 
            appearance142.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance142.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance142.BorderColor = System.Drawing.Color.Black;
            appearance142.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance142.TextHAlignAsString = "Center";
            appearance142.TextVAlignAsString = "Middle";
            this.ultraLabel244.Appearance = appearance142;
            this.ultraLabel244.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel244.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel244.Location = new System.Drawing.Point(80, 1201);
            this.ultraLabel244.Name = "ultraLabel244";
            this.ultraLabel244.Size = new System.Drawing.Size(39, 57);
            this.ultraLabel244.TabIndex = 383;
            this.ultraLabel244.Text = "6.3";
            // 
            // ultraPanel51
            // 
            appearance143.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance143.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance143.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel51.Appearance = appearance143;
            this.ultraPanel51.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel51.ClientArea
            // 
            this.ultraPanel51.ClientArea.Controls.Add(this.txtItemC8);
            this.ultraPanel51.Location = new System.Drawing.Point(723, 1173);
            this.ultraPanel51.Name = "ultraPanel51";
            this.ultraPanel51.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel51.TabIndex = 382;
            // 
            // txtItemC8
            // 
            appearance144.FontData.BoldAsString = "False";
            appearance144.TextHAlignAsString = "Right";
            this.txtItemC8.Appearance = appearance144;
            this.txtItemC8.AutoSize = false;
            this.txtItemC8.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemC8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemC8.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemC8.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemC8.Location = new System.Drawing.Point(0, 0);
            this.txtItemC8.Name = "txtItemC8";
            this.txtItemC8.NullText = "0";
            this.txtItemC8.PromptChar = ' ';
            this.txtItemC8.Size = new System.Drawing.Size(225, 27);
            this.txtItemC8.TabIndex = 6;
            this.txtItemC8.ValueChanged += new System.EventHandler(this.txtItemC7_ValueChanged);
            this.txtItemC8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemC8.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel239
            // 
            appearance145.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance145.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance145.BorderColor = System.Drawing.Color.Black;
            appearance145.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance145.TextHAlignAsString = "Center";
            appearance145.TextVAlignAsString = "Middle";
            this.ultraLabel239.Appearance = appearance145;
            this.ultraLabel239.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel239.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel239.Location = new System.Drawing.Point(567, 1173);
            this.ultraLabel239.Name = "ultraLabel239";
            this.ultraLabel239.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel239.TabIndex = 381;
            this.ultraLabel239.Text = "C8";
            // 
            // NameC8
            // 
            appearance146.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance146.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance146.BorderColor = System.Drawing.Color.Black;
            appearance146.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance146.TextHAlignAsString = "Left";
            appearance146.TextVAlignAsString = "Middle";
            this.NameC8.Appearance = appearance146;
            this.NameC8.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC8.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC8.Location = new System.Drawing.Point(118, 1173);
            this.NameC8.Name = "NameC8";
            this.NameC8.Size = new System.Drawing.Size(450, 29);
            this.NameC8.TabIndex = 380;
            this.NameC8.Text = "                 + Thu nhập tính thuế tính theo thuế suất 20% (bao gồm cả thu nhậ" +
    "p được áp dụng thuế suất ưu đãi)";
            // 
            // ultraLabel241
            // 
            appearance147.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance147.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance147.BorderColor = System.Drawing.Color.Black;
            appearance147.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance147.TextHAlignAsString = "Center";
            appearance147.TextVAlignAsString = "Middle";
            this.ultraLabel241.Appearance = appearance147;
            this.ultraLabel241.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel241.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel241.Location = new System.Drawing.Point(80, 1173);
            this.ultraLabel241.Name = "ultraLabel241";
            this.ultraLabel241.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel241.TabIndex = 379;
            this.ultraLabel241.Text = "6.2";
            // 
            // ultraPanel50
            // 
            appearance148.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance148.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance148.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel50.Appearance = appearance148;
            this.ultraPanel50.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel50.ClientArea
            // 
            this.ultraPanel50.ClientArea.Controls.Add(this.txtItemC7);
            this.ultraPanel50.Location = new System.Drawing.Point(723, 1145);
            this.ultraPanel50.Name = "ultraPanel50";
            this.ultraPanel50.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel50.TabIndex = 378;
            // 
            // txtItemC7
            // 
            appearance149.FontData.BoldAsString = "False";
            appearance149.TextHAlignAsString = "Right";
            this.txtItemC7.Appearance = appearance149;
            this.txtItemC7.AutoSize = false;
            this.txtItemC7.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemC7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemC7.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemC7.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemC7.Location = new System.Drawing.Point(0, 0);
            this.txtItemC7.Name = "txtItemC7";
            this.txtItemC7.NullText = "0";
            this.txtItemC7.PromptChar = ' ';
            this.txtItemC7.Size = new System.Drawing.Size(225, 27);
            this.txtItemC7.TabIndex = 6;
            this.txtItemC7.ValueChanged += new System.EventHandler(this.txtItemC7_ValueChanged);
            this.txtItemC7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemC7.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel236
            // 
            appearance150.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance150.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance150.BorderColor = System.Drawing.Color.Black;
            appearance150.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance150.TextHAlignAsString = "Center";
            appearance150.TextVAlignAsString = "Middle";
            this.ultraLabel236.Appearance = appearance150;
            this.ultraLabel236.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel236.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel236.Location = new System.Drawing.Point(567, 1145);
            this.ultraLabel236.Name = "ultraLabel236";
            this.ultraLabel236.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel236.TabIndex = 377;
            this.ultraLabel236.Text = "C7";
            // 
            // NameC7
            // 
            appearance151.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance151.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance151.BorderColor = System.Drawing.Color.Black;
            appearance151.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance151.TextHAlignAsString = "Left";
            appearance151.TextVAlignAsString = "Middle";
            this.NameC7.Appearance = appearance151;
            this.NameC7.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC7.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC7.Location = new System.Drawing.Point(118, 1145);
            this.NameC7.Name = "NameC7";
            this.NameC7.Size = new System.Drawing.Size(450, 29);
            this.NameC7.TabIndex = 376;
            this.NameC7.Text = "Trong đó: + Thu nhập tính thuế tính theo thuế suất 22% (bao gồm cả thu nhập được " +
    "áp dụng thuế suất ưu đãi)";
            // 
            // ultraLabel238
            // 
            appearance152.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance152.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance152.BorderColor = System.Drawing.Color.Black;
            appearance152.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance152.TextHAlignAsString = "Center";
            appearance152.TextVAlignAsString = "Middle";
            this.ultraLabel238.Appearance = appearance152;
            this.ultraLabel238.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel238.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel238.Location = new System.Drawing.Point(80, 1145);
            this.ultraLabel238.Name = "ultraLabel238";
            this.ultraLabel238.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel238.TabIndex = 375;
            this.ultraLabel238.Text = "6.1";
            // 
            // ultraPanel49
            // 
            appearance153.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance153.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance153.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel49.Appearance = appearance153;
            this.ultraPanel49.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel49.ClientArea
            // 
            this.ultraPanel49.ClientArea.Controls.Add(this.txtItemC6);
            this.ultraPanel49.Location = new System.Drawing.Point(723, 1117);
            this.ultraPanel49.Name = "ultraPanel49";
            this.ultraPanel49.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel49.TabIndex = 374;
            // 
            // txtItemC6
            // 
            appearance154.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance154.FontData.BoldAsString = "False";
            appearance154.TextHAlignAsString = "Right";
            this.txtItemC6.Appearance = appearance154;
            this.txtItemC6.AutoSize = false;
            this.txtItemC6.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemC6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemC6.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemC6.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemC6.Location = new System.Drawing.Point(0, 0);
            this.txtItemC6.Name = "txtItemC6";
            this.txtItemC6.NullText = "0";
            this.txtItemC6.PromptChar = ' ';
            this.txtItemC6.ReadOnly = true;
            this.txtItemC6.Size = new System.Drawing.Size(225, 27);
            this.txtItemC6.TabIndex = 6;
            this.txtItemC6.ValueChanged += new System.EventHandler(this.txtItemC6_ValueChanged);
            // 
            // ultraLabel233
            // 
            appearance155.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance155.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance155.BorderColor = System.Drawing.Color.Black;
            appearance155.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance155.TextHAlignAsString = "Center";
            appearance155.TextVAlignAsString = "Middle";
            this.ultraLabel233.Appearance = appearance155;
            this.ultraLabel233.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel233.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel233.Location = new System.Drawing.Point(567, 1117);
            this.ultraLabel233.Name = "ultraLabel233";
            this.ultraLabel233.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel233.TabIndex = 373;
            this.ultraLabel233.Text = "C6";
            // 
            // NameC6
            // 
            appearance156.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance156.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance156.BorderColor = System.Drawing.Color.Black;
            appearance156.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance156.TextHAlignAsString = "Left";
            appearance156.TextVAlignAsString = "Middle";
            this.NameC6.Appearance = appearance156;
            this.NameC6.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC6.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC6.Location = new System.Drawing.Point(118, 1117);
            this.NameC6.Name = "NameC6";
            this.NameC6.Size = new System.Drawing.Size(450, 29);
            this.NameC6.TabIndex = 372;
            this.NameC6.Text = "TNTT sau khi đã trích lập quỹ khoa học công nghệ (C6 = C4 - C5 = C7 + C8 + C9)";
            // 
            // ultraLabel235
            // 
            appearance157.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance157.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance157.BorderColor = System.Drawing.Color.Black;
            appearance157.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance157.TextHAlignAsString = "Center";
            appearance157.TextVAlignAsString = "Middle";
            this.ultraLabel235.Appearance = appearance157;
            this.ultraLabel235.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel235.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel235.Location = new System.Drawing.Point(80, 1117);
            this.ultraLabel235.Name = "ultraLabel235";
            this.ultraLabel235.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel235.TabIndex = 371;
            this.ultraLabel235.Text = "6";
            // 
            // ultraPanel48
            // 
            appearance158.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance158.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance158.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel48.Appearance = appearance158;
            this.ultraPanel48.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel48.ClientArea
            // 
            this.ultraPanel48.ClientArea.Controls.Add(this.txtItemC5);
            this.ultraPanel48.Location = new System.Drawing.Point(723, 1089);
            this.ultraPanel48.Name = "ultraPanel48";
            this.ultraPanel48.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel48.TabIndex = 370;
            // 
            // txtItemC5
            // 
            appearance159.FontData.BoldAsString = "False";
            appearance159.TextHAlignAsString = "Right";
            this.txtItemC5.Appearance = appearance159;
            this.txtItemC5.AutoSize = false;
            this.txtItemC5.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemC5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemC5.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemC5.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemC5.Location = new System.Drawing.Point(0, 0);
            this.txtItemC5.Name = "txtItemC5";
            this.txtItemC5.NullText = "0";
            this.txtItemC5.PromptChar = ' ';
            this.txtItemC5.Size = new System.Drawing.Size(225, 27);
            this.txtItemC5.TabIndex = 6;
            this.txtItemC5.ValueChanged += new System.EventHandler(this.txtItemC4_ValueChanged);
            this.txtItemC5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemC5.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel230
            // 
            appearance160.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance160.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance160.BorderColor = System.Drawing.Color.Black;
            appearance160.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance160.TextHAlignAsString = "Center";
            appearance160.TextVAlignAsString = "Middle";
            this.ultraLabel230.Appearance = appearance160;
            this.ultraLabel230.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel230.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel230.Location = new System.Drawing.Point(567, 1089);
            this.ultraLabel230.Name = "ultraLabel230";
            this.ultraLabel230.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel230.TabIndex = 369;
            this.ultraLabel230.Text = "C5";
            // 
            // NameC5
            // 
            appearance161.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance161.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance161.BorderColor = System.Drawing.Color.Black;
            appearance161.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance161.TextHAlignAsString = "Left";
            appearance161.TextVAlignAsString = "Middle";
            this.NameC5.Appearance = appearance161;
            this.NameC5.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC5.Location = new System.Drawing.Point(118, 1089);
            this.NameC5.Name = "NameC5";
            this.NameC5.Size = new System.Drawing.Size(450, 29);
            this.NameC5.TabIndex = 368;
            this.NameC5.Text = "Trích lập quỹ khoa học công nghệ (nếu có)";
            // 
            // ultraLabel232
            // 
            appearance162.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance162.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance162.BorderColor = System.Drawing.Color.Black;
            appearance162.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance162.TextHAlignAsString = "Center";
            appearance162.TextVAlignAsString = "Middle";
            this.ultraLabel232.Appearance = appearance162;
            this.ultraLabel232.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel232.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel232.Location = new System.Drawing.Point(80, 1089);
            this.ultraLabel232.Name = "ultraLabel232";
            this.ultraLabel232.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel232.TabIndex = 367;
            this.ultraLabel232.Text = "5";
            // 
            // ultraPanel47
            // 
            appearance163.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance163.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance163.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel47.Appearance = appearance163;
            this.ultraPanel47.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel47.ClientArea
            // 
            this.ultraPanel47.ClientArea.Controls.Add(this.txtItemC4);
            this.ultraPanel47.Location = new System.Drawing.Point(723, 1061);
            this.ultraPanel47.Name = "ultraPanel47";
            this.ultraPanel47.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel47.TabIndex = 366;
            // 
            // txtItemC4
            // 
            appearance164.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance164.FontData.BoldAsString = "False";
            appearance164.TextHAlignAsString = "Right";
            this.txtItemC4.Appearance = appearance164;
            this.txtItemC4.AutoSize = false;
            this.txtItemC4.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemC4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemC4.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemC4.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemC4.Location = new System.Drawing.Point(0, 0);
            this.txtItemC4.Name = "txtItemC4";
            this.txtItemC4.NullText = "0";
            this.txtItemC4.PromptChar = ' ';
            this.txtItemC4.ReadOnly = true;
            this.txtItemC4.Size = new System.Drawing.Size(225, 27);
            this.txtItemC4.TabIndex = 6;
            this.txtItemC4.ValueChanged += new System.EventHandler(this.txtItemC4_ValueChanged);
            // 
            // ultraLabel227
            // 
            appearance165.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance165.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance165.BorderColor = System.Drawing.Color.Black;
            appearance165.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance165.TextHAlignAsString = "Center";
            appearance165.TextVAlignAsString = "Middle";
            this.ultraLabel227.Appearance = appearance165;
            this.ultraLabel227.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel227.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel227.Location = new System.Drawing.Point(567, 1061);
            this.ultraLabel227.Name = "ultraLabel227";
            this.ultraLabel227.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel227.TabIndex = 365;
            this.ultraLabel227.Text = "C4";
            // 
            // NameC4
            // 
            appearance166.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance166.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance166.BorderColor = System.Drawing.Color.Black;
            appearance166.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance166.TextHAlignAsString = "Left";
            appearance166.TextVAlignAsString = "Middle";
            this.NameC4.Appearance = appearance166;
            this.NameC4.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC4.Location = new System.Drawing.Point(118, 1061);
            this.NameC4.Name = "NameC4";
            this.NameC4.Size = new System.Drawing.Size(450, 29);
            this.NameC4.TabIndex = 364;
            this.NameC4.Text = "Thu nhập tính thuế (TNTT) (C4 = C1 - C2 - C3a - C3b)";
            // 
            // ultraLabel229
            // 
            appearance167.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance167.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance167.BorderColor = System.Drawing.Color.Black;
            appearance167.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance167.TextHAlignAsString = "Center";
            appearance167.TextVAlignAsString = "Middle";
            this.ultraLabel229.Appearance = appearance167;
            this.ultraLabel229.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel229.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel229.Location = new System.Drawing.Point(80, 1061);
            this.ultraLabel229.Name = "ultraLabel229";
            this.ultraLabel229.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel229.TabIndex = 363;
            this.ultraLabel229.Text = "4";
            // 
            // ultraPanel46
            // 
            appearance168.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance168.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance168.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel46.Appearance = appearance168;
            this.ultraPanel46.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel46.ClientArea
            // 
            this.ultraPanel46.ClientArea.Controls.Add(this.txtItemC3b);
            this.ultraPanel46.Location = new System.Drawing.Point(723, 1033);
            this.ultraPanel46.Name = "ultraPanel46";
            this.ultraPanel46.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel46.TabIndex = 362;
            // 
            // txtItemC3b
            // 
            appearance169.FontData.BoldAsString = "False";
            appearance169.TextHAlignAsString = "Right";
            this.txtItemC3b.Appearance = appearance169;
            this.txtItemC3b.AutoSize = false;
            this.txtItemC3b.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemC3b.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemC3b.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemC3b.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemC3b.Location = new System.Drawing.Point(0, 0);
            this.txtItemC3b.Name = "txtItemC3b";
            this.txtItemC3b.NullText = "0";
            this.txtItemC3b.PromptChar = ' ';
            this.txtItemC3b.Size = new System.Drawing.Size(225, 27);
            this.txtItemC3b.TabIndex = 6;
            this.txtItemC3b.ValueChanged += new System.EventHandler(this.txtItemC1_ValueChanged);
            this.txtItemC3b.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemC3b.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel224
            // 
            appearance170.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance170.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance170.BorderColor = System.Drawing.Color.Black;
            appearance170.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance170.TextHAlignAsString = "Center";
            appearance170.TextVAlignAsString = "Middle";
            this.ultraLabel224.Appearance = appearance170;
            this.ultraLabel224.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel224.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel224.Location = new System.Drawing.Point(567, 1033);
            this.ultraLabel224.Name = "ultraLabel224";
            this.ultraLabel224.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel224.TabIndex = 361;
            this.ultraLabel224.Text = "C3b";
            // 
            // NameC3b
            // 
            appearance171.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance171.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance171.BorderColor = System.Drawing.Color.Black;
            appearance171.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance171.TextHAlignAsString = "Left";
            appearance171.TextVAlignAsString = "Middle";
            this.NameC3b.Appearance = appearance171;
            this.NameC3b.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC3b.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC3b.Location = new System.Drawing.Point(118, 1033);
            this.NameC3b.Name = "NameC3b";
            this.NameC3b.Size = new System.Drawing.Size(450, 29);
            this.NameC3b.TabIndex = 360;
            this.NameC3b.Text = "Lỗ từ chuyển nhượng BĐS được bù trừ với lãi của hoạt động SXKD";
            // 
            // ultraLabel226
            // 
            appearance172.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance172.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance172.BorderColor = System.Drawing.Color.Black;
            appearance172.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance172.TextHAlignAsString = "Center";
            appearance172.TextVAlignAsString = "Middle";
            this.ultraLabel226.Appearance = appearance172;
            this.ultraLabel226.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel226.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel226.Location = new System.Drawing.Point(80, 1033);
            this.ultraLabel226.Name = "ultraLabel226";
            this.ultraLabel226.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel226.TabIndex = 359;
            this.ultraLabel226.Text = "3.2";
            // 
            // ultraPanel45
            // 
            appearance173.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance173.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance173.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel45.Appearance = appearance173;
            this.ultraPanel45.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel45.ClientArea
            // 
            this.ultraPanel45.ClientArea.Controls.Add(this.txtItemC3a);
            this.ultraPanel45.Location = new System.Drawing.Point(723, 1005);
            this.ultraPanel45.Name = "ultraPanel45";
            this.ultraPanel45.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel45.TabIndex = 358;
            // 
            // txtItemC3a
            // 
            appearance174.FontData.BoldAsString = "False";
            appearance174.TextHAlignAsString = "Right";
            this.txtItemC3a.Appearance = appearance174;
            this.txtItemC3a.AutoSize = false;
            this.txtItemC3a.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemC3a.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemC3a.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemC3a.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemC3a.Location = new System.Drawing.Point(0, 0);
            this.txtItemC3a.Name = "txtItemC3a";
            this.txtItemC3a.NullText = "0";
            this.txtItemC3a.PromptChar = ' ';
            this.txtItemC3a.Size = new System.Drawing.Size(225, 27);
            this.txtItemC3a.TabIndex = 6;
            this.txtItemC3a.ValueChanged += new System.EventHandler(this.txtItemC1_ValueChanged);
            this.txtItemC3a.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemC3a.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel221
            // 
            appearance175.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance175.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance175.BorderColor = System.Drawing.Color.Black;
            appearance175.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance175.TextHAlignAsString = "Center";
            appearance175.TextVAlignAsString = "Middle";
            this.ultraLabel221.Appearance = appearance175;
            this.ultraLabel221.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel221.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel221.Location = new System.Drawing.Point(567, 1005);
            this.ultraLabel221.Name = "ultraLabel221";
            this.ultraLabel221.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel221.TabIndex = 357;
            this.ultraLabel221.Text = "C3a";
            // 
            // NameC3a
            // 
            appearance176.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance176.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance176.BorderColor = System.Drawing.Color.Black;
            appearance176.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance176.TextHAlignAsString = "Left";
            appearance176.TextVAlignAsString = "Middle";
            this.NameC3a.Appearance = appearance176;
            this.NameC3a.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC3a.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC3a.Location = new System.Drawing.Point(118, 1005);
            this.NameC3a.Name = "NameC3a";
            this.NameC3a.Size = new System.Drawing.Size(450, 29);
            this.NameC3a.TabIndex = 356;
            this.NameC3a.Text = "Lỗ từ hoạt động SXKD được chuyển trong kỳ";
            // 
            // ultraLabel223
            // 
            appearance177.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance177.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance177.BorderColor = System.Drawing.Color.Black;
            appearance177.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance177.TextHAlignAsString = "Center";
            appearance177.TextVAlignAsString = "Middle";
            this.ultraLabel223.Appearance = appearance177;
            this.ultraLabel223.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel223.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel223.Location = new System.Drawing.Point(80, 1005);
            this.ultraLabel223.Name = "ultraLabel223";
            this.ultraLabel223.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel223.TabIndex = 355;
            this.ultraLabel223.Text = "3.1";
            // 
            // ultraPanel44
            // 
            appearance178.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance178.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance178.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel44.Appearance = appearance178;
            this.ultraPanel44.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel44.ClientArea
            // 
            this.ultraPanel44.ClientArea.Controls.Add(this.txtItemC3);
            this.ultraPanel44.Location = new System.Drawing.Point(723, 977);
            this.ultraPanel44.Name = "ultraPanel44";
            this.ultraPanel44.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel44.TabIndex = 354;
            // 
            // txtItemC3
            // 
            appearance179.TextHAlignAsString = "Right";
            this.txtItemC3.Appearance = appearance179;
            this.txtItemC3.AutoSize = false;
            this.txtItemC3.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemC3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemC3.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemC3.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemC3.Location = new System.Drawing.Point(0, 0);
            this.txtItemC3.Name = "txtItemC3";
            this.txtItemC3.NullText = "0";
            this.txtItemC3.PromptChar = ' ';
            this.txtItemC3.Size = new System.Drawing.Size(225, 27);
            this.txtItemC3.TabIndex = 6;
            this.txtItemC3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemC3.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel218
            // 
            appearance180.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance180.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance180.BorderColor = System.Drawing.Color.Black;
            appearance180.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance180.TextHAlignAsString = "Center";
            appearance180.TextVAlignAsString = "Middle";
            this.ultraLabel218.Appearance = appearance180;
            this.ultraLabel218.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel218.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel218.Location = new System.Drawing.Point(567, 977);
            this.ultraLabel218.Name = "ultraLabel218";
            this.ultraLabel218.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel218.TabIndex = 353;
            this.ultraLabel218.Text = "C3";
            // 
            // NameC3
            // 
            appearance181.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance181.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance181.BorderColor = System.Drawing.Color.Black;
            appearance181.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance181.TextHAlignAsString = "Left";
            appearance181.TextVAlignAsString = "Middle";
            this.NameC3.Appearance = appearance181;
            this.NameC3.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC3.Location = new System.Drawing.Point(118, 977);
            this.NameC3.Name = "NameC3";
            this.NameC3.Size = new System.Drawing.Size(450, 29);
            this.NameC3.TabIndex = 352;
            this.NameC3.Text = "Chuyển lỗ và bù trừ lãi, lỗ";
            // 
            // ultraLabel220
            // 
            appearance182.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance182.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance182.BorderColor = System.Drawing.Color.Black;
            appearance182.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance182.TextHAlignAsString = "Center";
            appearance182.TextVAlignAsString = "Middle";
            this.ultraLabel220.Appearance = appearance182;
            this.ultraLabel220.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel220.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel220.Location = new System.Drawing.Point(80, 977);
            this.ultraLabel220.Name = "ultraLabel220";
            this.ultraLabel220.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel220.TabIndex = 351;
            this.ultraLabel220.Text = "3";
            // 
            // ultraPanel43
            // 
            appearance183.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance183.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance183.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel43.Appearance = appearance183;
            this.ultraPanel43.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel43.ClientArea
            // 
            this.ultraPanel43.ClientArea.Controls.Add(this.txtItemC2);
            this.ultraPanel43.Location = new System.Drawing.Point(723, 949);
            this.ultraPanel43.Name = "ultraPanel43";
            this.ultraPanel43.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel43.TabIndex = 350;
            // 
            // txtItemC2
            // 
            appearance184.TextHAlignAsString = "Right";
            this.txtItemC2.Appearance = appearance184;
            this.txtItemC2.AutoSize = false;
            this.txtItemC2.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemC2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemC2.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemC2.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemC2.Location = new System.Drawing.Point(0, 0);
            this.txtItemC2.Name = "txtItemC2";
            this.txtItemC2.NullText = "0";
            this.txtItemC2.PromptChar = ' ';
            this.txtItemC2.Size = new System.Drawing.Size(225, 27);
            this.txtItemC2.TabIndex = 6;
            this.txtItemC2.ValueChanged += new System.EventHandler(this.txtItemC1_ValueChanged);
            this.txtItemC2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemC2.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel215
            // 
            appearance185.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance185.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance185.BorderColor = System.Drawing.Color.Black;
            appearance185.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance185.TextHAlignAsString = "Center";
            appearance185.TextVAlignAsString = "Middle";
            this.ultraLabel215.Appearance = appearance185;
            this.ultraLabel215.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel215.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel215.Location = new System.Drawing.Point(567, 949);
            this.ultraLabel215.Name = "ultraLabel215";
            this.ultraLabel215.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel215.TabIndex = 349;
            this.ultraLabel215.Text = "C2";
            // 
            // NameC2
            // 
            appearance186.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance186.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance186.BorderColor = System.Drawing.Color.Black;
            appearance186.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance186.TextHAlignAsString = "Left";
            appearance186.TextVAlignAsString = "Middle";
            this.NameC2.Appearance = appearance186;
            this.NameC2.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC2.Location = new System.Drawing.Point(118, 949);
            this.NameC2.Name = "NameC2";
            this.NameC2.Size = new System.Drawing.Size(450, 29);
            this.NameC2.TabIndex = 348;
            this.NameC2.Text = "Thu nhập miễn thuế";
            // 
            // ultraLabel217
            // 
            appearance187.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance187.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance187.BorderColor = System.Drawing.Color.Black;
            appearance187.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance187.TextHAlignAsString = "Center";
            appearance187.TextVAlignAsString = "Middle";
            this.ultraLabel217.Appearance = appearance187;
            this.ultraLabel217.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel217.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel217.Location = new System.Drawing.Point(80, 949);
            this.ultraLabel217.Name = "ultraLabel217";
            this.ultraLabel217.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel217.TabIndex = 347;
            this.ultraLabel217.Text = "2";
            // 
            // ultraPanel42
            // 
            appearance188.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance188.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance188.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel42.Appearance = appearance188;
            this.ultraPanel42.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel42.ClientArea
            // 
            this.ultraPanel42.ClientArea.Controls.Add(this.txtItemC1);
            this.ultraPanel42.Location = new System.Drawing.Point(723, 921);
            this.ultraPanel42.Name = "ultraPanel42";
            this.ultraPanel42.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel42.TabIndex = 346;
            // 
            // txtItemC1
            // 
            appearance189.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance189.TextHAlignAsString = "Right";
            this.txtItemC1.Appearance = appearance189;
            this.txtItemC1.AutoSize = false;
            this.txtItemC1.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemC1.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemC1.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemC1.Location = new System.Drawing.Point(0, 0);
            this.txtItemC1.Name = "txtItemC1";
            this.txtItemC1.NullText = "0";
            this.txtItemC1.PromptChar = ' ';
            this.txtItemC1.ReadOnly = true;
            this.txtItemC1.Size = new System.Drawing.Size(225, 27);
            this.txtItemC1.TabIndex = 6;
            this.txtItemC1.ValueChanged += new System.EventHandler(this.txtItemC1_ValueChanged);
            this.txtItemC1.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel212
            // 
            appearance190.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance190.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance190.BorderColor = System.Drawing.Color.Black;
            appearance190.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance190.TextHAlignAsString = "Center";
            appearance190.TextVAlignAsString = "Middle";
            this.ultraLabel212.Appearance = appearance190;
            this.ultraLabel212.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel212.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel212.Location = new System.Drawing.Point(567, 921);
            this.ultraLabel212.Name = "ultraLabel212";
            this.ultraLabel212.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel212.TabIndex = 345;
            this.ultraLabel212.Text = "C1";
            // 
            // NameC1
            // 
            appearance191.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance191.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance191.BorderColor = System.Drawing.Color.Black;
            appearance191.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance191.TextHAlignAsString = "Left";
            appearance191.TextVAlignAsString = "Middle";
            this.NameC1.Appearance = appearance191;
            this.NameC1.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC1.Location = new System.Drawing.Point(118, 921);
            this.NameC1.Name = "NameC1";
            this.NameC1.Size = new System.Drawing.Size(450, 29);
            this.NameC1.TabIndex = 344;
            this.NameC1.Text = "Thu nhập chịu thuế (C1 = B13)";
            // 
            // ultraLabel214
            // 
            appearance192.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance192.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance192.BorderColor = System.Drawing.Color.Black;
            appearance192.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance192.TextHAlignAsString = "Center";
            appearance192.TextVAlignAsString = "Middle";
            this.ultraLabel214.Appearance = appearance192;
            this.ultraLabel214.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel214.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel214.Location = new System.Drawing.Point(80, 921);
            this.ultraLabel214.Name = "ultraLabel214";
            this.ultraLabel214.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel214.TabIndex = 343;
            this.ultraLabel214.Text = "1";
            // 
            // ultraPanel41
            // 
            appearance193.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance193.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance193.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel41.Appearance = appearance193;
            this.ultraPanel41.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraPanel41.Location = new System.Drawing.Point(723, 893);
            this.ultraPanel41.Name = "ultraPanel41";
            this.ultraPanel41.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel41.TabIndex = 342;
            // 
            // ultraLabel209
            // 
            appearance194.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance194.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance194.BorderColor = System.Drawing.Color.Black;
            appearance194.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance194.TextHAlignAsString = "Center";
            appearance194.TextVAlignAsString = "Middle";
            this.ultraLabel209.Appearance = appearance194;
            this.ultraLabel209.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel209.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel209.Location = new System.Drawing.Point(567, 893);
            this.ultraLabel209.Name = "ultraLabel209";
            this.ultraLabel209.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel209.TabIndex = 341;
            this.ultraLabel209.Text = "C";
            // 
            // NameC
            // 
            appearance195.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance195.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance195.BorderColor = System.Drawing.Color.Black;
            appearance195.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance195.TextHAlignAsString = "Left";
            appearance195.TextVAlignAsString = "Middle";
            this.NameC.Appearance = appearance195;
            this.NameC.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameC.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameC.Location = new System.Drawing.Point(118, 893);
            this.NameC.Name = "NameC";
            this.NameC.Size = new System.Drawing.Size(450, 29);
            this.NameC.TabIndex = 340;
            this.NameC.Text = "Xác định thuế thu nhập doanh nghiệp (TNDN) phải nộp từ hoạt động sản xuất kinh do" +
    "anh";
            // 
            // ultraLabel211
            // 
            appearance196.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance196.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance196.BorderColor = System.Drawing.Color.Black;
            appearance196.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance196.TextHAlignAsString = "Center";
            appearance196.TextVAlignAsString = "Middle";
            this.ultraLabel211.Appearance = appearance196;
            this.ultraLabel211.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel211.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel211.Location = new System.Drawing.Point(80, 893);
            this.ultraLabel211.Name = "ultraLabel211";
            this.ultraLabel211.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel211.TabIndex = 339;
            this.ultraLabel211.Text = "C";
            // 
            // ultraPanel39
            // 
            appearance197.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance197.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance197.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel39.Appearance = appearance197;
            this.ultraPanel39.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel39.ClientArea
            // 
            this.ultraPanel39.ClientArea.Controls.Add(this.txtItemB14);
            this.ultraPanel39.Location = new System.Drawing.Point(723, 865);
            this.ultraPanel39.Name = "ultraPanel39";
            this.ultraPanel39.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel39.TabIndex = 338;
            // 
            // txtItemB14
            // 
            appearance198.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance198.TextHAlignAsString = "Right";
            this.txtItemB14.Appearance = appearance198;
            this.txtItemB14.AutoSize = false;
            this.txtItemB14.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemB14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemB14.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemB14.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemB14.Location = new System.Drawing.Point(0, 0);
            this.txtItemB14.Name = "txtItemB14";
            this.txtItemB14.NullText = "0";
            this.txtItemB14.PromptChar = ' ';
            this.txtItemB14.ReadOnly = true;
            this.txtItemB14.Size = new System.Drawing.Size(225, 27);
            this.txtItemB14.TabIndex = 6;
            // 
            // ultraLabel206
            // 
            appearance199.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance199.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance199.BorderColor = System.Drawing.Color.Black;
            appearance199.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance199.TextHAlignAsString = "Center";
            appearance199.TextVAlignAsString = "Middle";
            this.ultraLabel206.Appearance = appearance199;
            this.ultraLabel206.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel206.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel206.Location = new System.Drawing.Point(567, 865);
            this.ultraLabel206.Name = "ultraLabel206";
            this.ultraLabel206.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel206.TabIndex = 337;
            this.ultraLabel206.Text = "B14";
            // 
            // NameB14
            // 
            appearance200.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance200.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance200.BorderColor = System.Drawing.Color.Black;
            appearance200.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance200.TextHAlignAsString = "Left";
            appearance200.TextVAlignAsString = "Middle";
            this.NameB14.Appearance = appearance200;
            this.NameB14.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameB14.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameB14.Location = new System.Drawing.Point(118, 865);
            this.NameB14.Name = "NameB14";
            this.NameB14.Size = new System.Drawing.Size(450, 29);
            this.NameB14.TabIndex = 336;
            this.NameB14.Text = "Thu nhập chịu thuế từ hoạt động chuyển nhượng bất động sản (B14 = B12 - B13)";
            // 
            // ultraLabel208
            // 
            appearance201.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance201.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance201.BorderColor = System.Drawing.Color.Black;
            appearance201.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance201.TextHAlignAsString = "Center";
            appearance201.TextVAlignAsString = "Middle";
            this.ultraLabel208.Appearance = appearance201;
            this.ultraLabel208.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel208.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel208.Location = new System.Drawing.Point(80, 865);
            this.ultraLabel208.Name = "ultraLabel208";
            this.ultraLabel208.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel208.TabIndex = 335;
            this.ultraLabel208.Text = "3.2";
            // 
            // ultraPanel38
            // 
            appearance202.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance202.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance202.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel38.Appearance = appearance202;
            this.ultraPanel38.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel38.ClientArea
            // 
            this.ultraPanel38.ClientArea.Controls.Add(this.txtItemB13);
            this.ultraPanel38.Location = new System.Drawing.Point(723, 837);
            this.ultraPanel38.Name = "ultraPanel38";
            this.ultraPanel38.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel38.TabIndex = 334;
            // 
            // txtItemB13
            // 
            appearance203.TextHAlignAsString = "Right";
            this.txtItemB13.Appearance = appearance203;
            this.txtItemB13.AutoSize = false;
            this.txtItemB13.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemB13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemB13.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemB13.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemB13.Location = new System.Drawing.Point(0, 0);
            this.txtItemB13.Name = "txtItemB13";
            this.txtItemB13.NullText = "0";
            this.txtItemB13.PromptChar = ' ';
            this.txtItemB13.Size = new System.Drawing.Size(225, 27);
            this.txtItemB13.TabIndex = 6;
            this.txtItemB13.ValueChanged += new System.EventHandler(this.txtItemB12_ValueChanged);
            this.txtItemB13.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemB13.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel203
            // 
            appearance204.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance204.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance204.BorderColor = System.Drawing.Color.Black;
            appearance204.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance204.TextHAlignAsString = "Center";
            appearance204.TextVAlignAsString = "Middle";
            this.ultraLabel203.Appearance = appearance204;
            this.ultraLabel203.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel203.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel203.Location = new System.Drawing.Point(567, 837);
            this.ultraLabel203.Name = "ultraLabel203";
            this.ultraLabel203.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel203.TabIndex = 333;
            this.ultraLabel203.Text = "B13";
            // 
            // NameB13
            // 
            appearance205.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance205.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance205.BorderColor = System.Drawing.Color.Black;
            appearance205.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance205.TextHAlignAsString = "Left";
            appearance205.TextVAlignAsString = "Middle";
            this.NameB13.Appearance = appearance205;
            this.NameB13.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameB13.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameB13.Location = new System.Drawing.Point(118, 837);
            this.NameB13.Name = "NameB13";
            this.NameB13.Size = new System.Drawing.Size(450, 29);
            this.NameB13.TabIndex = 332;
            this.NameB13.Text = "Thu nhập chịu thuế từ hoạt động sản xuất kinh doanh";
            // 
            // ultraLabel205
            // 
            appearance206.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance206.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance206.BorderColor = System.Drawing.Color.Black;
            appearance206.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance206.TextHAlignAsString = "Center";
            appearance206.TextVAlignAsString = "Middle";
            this.ultraLabel205.Appearance = appearance206;
            this.ultraLabel205.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel205.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel205.Location = new System.Drawing.Point(80, 837);
            this.ultraLabel205.Name = "ultraLabel205";
            this.ultraLabel205.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel205.TabIndex = 331;
            this.ultraLabel205.Text = "3.1";
            // 
            // ultraPanel37
            // 
            appearance207.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance207.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance207.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel37.Appearance = appearance207;
            this.ultraPanel37.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel37.ClientArea
            // 
            this.ultraPanel37.ClientArea.Controls.Add(this.txtItemB12);
            this.ultraPanel37.Location = new System.Drawing.Point(723, 809);
            this.ultraPanel37.Name = "ultraPanel37";
            this.ultraPanel37.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel37.TabIndex = 330;
            // 
            // txtItemB12
            // 
            appearance208.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance208.TextHAlignAsString = "Right";
            this.txtItemB12.Appearance = appearance208;
            this.txtItemB12.AutoSize = false;
            this.txtItemB12.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemB12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemB12.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemB12.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemB12.Location = new System.Drawing.Point(0, 0);
            this.txtItemB12.Name = "txtItemB12";
            this.txtItemB12.NullText = "0";
            this.txtItemB12.PromptChar = ' ';
            this.txtItemB12.ReadOnly = true;
            this.txtItemB12.Size = new System.Drawing.Size(225, 27);
            this.txtItemB12.TabIndex = 6;
            this.txtItemB12.ValueChanged += new System.EventHandler(this.txtItemB12_ValueChanged);
            // 
            // ultraLabel200
            // 
            appearance209.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance209.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance209.BorderColor = System.Drawing.Color.Black;
            appearance209.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance209.TextHAlignAsString = "Center";
            appearance209.TextVAlignAsString = "Middle";
            this.ultraLabel200.Appearance = appearance209;
            this.ultraLabel200.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel200.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel200.Location = new System.Drawing.Point(567, 809);
            this.ultraLabel200.Name = "ultraLabel200";
            this.ultraLabel200.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel200.TabIndex = 329;
            this.ultraLabel200.Text = "B12";
            // 
            // NameB12
            // 
            appearance210.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance210.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance210.BorderColor = System.Drawing.Color.Black;
            appearance210.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance210.TextHAlignAsString = "Left";
            appearance210.TextVAlignAsString = "Middle";
            this.NameB12.Appearance = appearance210;
            this.NameB12.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameB12.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameB12.Location = new System.Drawing.Point(118, 809);
            this.NameB12.Name = "NameB12";
            this.NameB12.Size = new System.Drawing.Size(450, 29);
            this.NameB12.TabIndex = 328;
            this.NameB12.Text = "Tổng thu nhập chịu thuế (B12 = A1 + B1 - B8)";
            // 
            // ultraLabel202
            // 
            appearance211.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance211.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance211.BorderColor = System.Drawing.Color.Black;
            appearance211.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance211.TextHAlignAsString = "Center";
            appearance211.TextVAlignAsString = "Middle";
            this.ultraLabel202.Appearance = appearance211;
            this.ultraLabel202.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel202.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel202.Location = new System.Drawing.Point(80, 809);
            this.ultraLabel202.Name = "ultraLabel202";
            this.ultraLabel202.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel202.TabIndex = 327;
            this.ultraLabel202.Text = "3";
            // 
            // ultraPanel36
            // 
            appearance212.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance212.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance212.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel36.Appearance = appearance212;
            this.ultraPanel36.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel36.ClientArea
            // 
            this.ultraPanel36.ClientArea.Controls.Add(this.txtItemB11);
            this.ultraPanel36.Location = new System.Drawing.Point(723, 781);
            this.ultraPanel36.Name = "ultraPanel36";
            this.ultraPanel36.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel36.TabIndex = 326;
            // 
            // txtItemB11
            // 
            appearance213.TextHAlignAsString = "Right";
            this.txtItemB11.Appearance = appearance213;
            this.txtItemB11.AutoSize = false;
            this.txtItemB11.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemB11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemB11.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemB11.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemB11.Location = new System.Drawing.Point(0, 0);
            this.txtItemB11.Name = "txtItemB11";
            this.txtItemB11.NullText = "0";
            this.txtItemB11.PromptChar = ' ';
            this.txtItemB11.Size = new System.Drawing.Size(225, 27);
            this.txtItemB11.TabIndex = 6;
            this.txtItemB11.ValueChanged += new System.EventHandler(this.txtItemB9_ValueChanged);
            this.txtItemB11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemB11.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel196
            // 
            appearance214.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance214.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance214.BorderColor = System.Drawing.Color.Black;
            appearance214.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance214.TextHAlignAsString = "Center";
            appearance214.TextVAlignAsString = "Middle";
            this.ultraLabel196.Appearance = appearance214;
            this.ultraLabel196.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel196.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel196.Location = new System.Drawing.Point(567, 781);
            this.ultraLabel196.Name = "ultraLabel196";
            this.ultraLabel196.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel196.TabIndex = 325;
            this.ultraLabel196.Text = "B11";
            // 
            // NameB11
            // 
            appearance215.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance215.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance215.BorderColor = System.Drawing.Color.Black;
            appearance215.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance215.TextHAlignAsString = "Left";
            appearance215.TextVAlignAsString = "Middle";
            this.NameB11.Appearance = appearance215;
            this.NameB11.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameB11.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameB11.Location = new System.Drawing.Point(118, 781);
            this.NameB11.Name = "NameB11";
            this.NameB11.Size = new System.Drawing.Size(450, 29);
            this.NameB11.TabIndex = 324;
            this.NameB11.Text = "Các khoản điều chỉnh làm giảm lợi nhuận trước thuế khác";
            // 
            // ultraLabel198
            // 
            appearance216.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance216.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance216.BorderColor = System.Drawing.Color.Black;
            appearance216.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance216.TextHAlignAsString = "Center";
            appearance216.TextVAlignAsString = "Middle";
            this.ultraLabel198.Appearance = appearance216;
            this.ultraLabel198.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel198.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel198.Location = new System.Drawing.Point(80, 781);
            this.ultraLabel198.Name = "ultraLabel198";
            this.ultraLabel198.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel198.TabIndex = 323;
            this.ultraLabel198.Text = "2.3";
            // 
            // ultraPanel35
            // 
            appearance217.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance217.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance217.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel35.Appearance = appearance217;
            this.ultraPanel35.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel35.ClientArea
            // 
            this.ultraPanel35.ClientArea.Controls.Add(this.txtItemB10);
            this.ultraPanel35.Location = new System.Drawing.Point(723, 753);
            this.ultraPanel35.Name = "ultraPanel35";
            this.ultraPanel35.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel35.TabIndex = 322;
            // 
            // txtItemB10
            // 
            appearance218.TextHAlignAsString = "Right";
            this.txtItemB10.Appearance = appearance218;
            this.txtItemB10.AutoSize = false;
            this.txtItemB10.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemB10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemB10.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemB10.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemB10.Location = new System.Drawing.Point(0, 0);
            this.txtItemB10.Name = "txtItemB10";
            this.txtItemB10.NullText = "0";
            this.txtItemB10.PromptChar = ' ';
            this.txtItemB10.Size = new System.Drawing.Size(225, 27);
            this.txtItemB10.TabIndex = 6;
            this.txtItemB10.ValueChanged += new System.EventHandler(this.txtItemB9_ValueChanged);
            this.txtItemB10.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemB10.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel180
            // 
            appearance219.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance219.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance219.BorderColor = System.Drawing.Color.Black;
            appearance219.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance219.TextHAlignAsString = "Center";
            appearance219.TextVAlignAsString = "Middle";
            this.ultraLabel180.Appearance = appearance219;
            this.ultraLabel180.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel180.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel180.Location = new System.Drawing.Point(567, 753);
            this.ultraLabel180.Name = "ultraLabel180";
            this.ultraLabel180.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel180.TabIndex = 321;
            this.ultraLabel180.Text = "B10";
            // 
            // NameB10
            // 
            appearance220.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance220.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance220.BorderColor = System.Drawing.Color.Black;
            appearance220.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance220.TextHAlignAsString = "Left";
            appearance220.TextVAlignAsString = "Middle";
            this.NameB10.Appearance = appearance220;
            this.NameB10.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameB10.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameB10.Location = new System.Drawing.Point(118, 753);
            this.NameB10.Name = "NameB10";
            this.NameB10.Size = new System.Drawing.Size(450, 29);
            this.NameB10.TabIndex = 320;
            this.NameB10.Text = "Chi phí của phần doanh thu điều chỉnh tăng";
            // 
            // ultraLabel195
            // 
            appearance221.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance221.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance221.BorderColor = System.Drawing.Color.Black;
            appearance221.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance221.TextHAlignAsString = "Center";
            appearance221.TextVAlignAsString = "Middle";
            this.ultraLabel195.Appearance = appearance221;
            this.ultraLabel195.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel195.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel195.Location = new System.Drawing.Point(80, 753);
            this.ultraLabel195.Name = "ultraLabel195";
            this.ultraLabel195.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel195.TabIndex = 319;
            this.ultraLabel195.Text = "2.2";
            // 
            // ultraPanel34
            // 
            appearance222.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance222.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance222.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel34.Appearance = appearance222;
            this.ultraPanel34.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel34.ClientArea
            // 
            this.ultraPanel34.ClientArea.Controls.Add(this.txtItemB9);
            this.ultraPanel34.Location = new System.Drawing.Point(723, 725);
            this.ultraPanel34.Name = "ultraPanel34";
            this.ultraPanel34.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel34.TabIndex = 318;
            // 
            // txtItemB9
            // 
            appearance223.TextHAlignAsString = "Right";
            this.txtItemB9.Appearance = appearance223;
            this.txtItemB9.AutoSize = false;
            this.txtItemB9.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemB9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemB9.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemB9.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemB9.Location = new System.Drawing.Point(0, 0);
            this.txtItemB9.Name = "txtItemB9";
            this.txtItemB9.NullText = "0";
            this.txtItemB9.PromptChar = ' ';
            this.txtItemB9.Size = new System.Drawing.Size(225, 27);
            this.txtItemB9.TabIndex = 6;
            this.txtItemB9.ValueChanged += new System.EventHandler(this.txtItemB9_ValueChanged);
            this.txtItemB9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemB9.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel177
            // 
            appearance224.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance224.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance224.BorderColor = System.Drawing.Color.Black;
            appearance224.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance224.TextHAlignAsString = "Center";
            appearance224.TextVAlignAsString = "Middle";
            this.ultraLabel177.Appearance = appearance224;
            this.ultraLabel177.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel177.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel177.Location = new System.Drawing.Point(567, 725);
            this.ultraLabel177.Name = "ultraLabel177";
            this.ultraLabel177.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel177.TabIndex = 317;
            this.ultraLabel177.Text = "B9";
            // 
            // NameB9
            // 
            appearance225.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance225.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance225.BorderColor = System.Drawing.Color.Black;
            appearance225.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance225.TextHAlignAsString = "Left";
            appearance225.TextVAlignAsString = "Middle";
            this.NameB9.Appearance = appearance225;
            this.NameB9.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameB9.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameB9.Location = new System.Drawing.Point(118, 725);
            this.NameB9.Name = "NameB9";
            this.NameB9.Size = new System.Drawing.Size(450, 29);
            this.NameB9.TabIndex = 316;
            this.NameB9.Text = "Giảm trừ các khoản doanh thu đã tính thuế năm trước";
            // 
            // ultraLabel179
            // 
            appearance226.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance226.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance226.BorderColor = System.Drawing.Color.Black;
            appearance226.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance226.TextHAlignAsString = "Center";
            appearance226.TextVAlignAsString = "Middle";
            this.ultraLabel179.Appearance = appearance226;
            this.ultraLabel179.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel179.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel179.Location = new System.Drawing.Point(80, 725);
            this.ultraLabel179.Name = "ultraLabel179";
            this.ultraLabel179.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel179.TabIndex = 315;
            this.ultraLabel179.Text = "2.1";
            // 
            // ultraPanel33
            // 
            appearance227.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance227.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance227.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel33.Appearance = appearance227;
            this.ultraPanel33.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel33.ClientArea
            // 
            this.ultraPanel33.ClientArea.Controls.Add(this.txtItemB8);
            this.ultraPanel33.Location = new System.Drawing.Point(723, 697);
            this.ultraPanel33.Name = "ultraPanel33";
            this.ultraPanel33.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel33.TabIndex = 314;
            // 
            // txtItemB8
            // 
            appearance228.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance228.TextHAlignAsString = "Right";
            this.txtItemB8.Appearance = appearance228;
            this.txtItemB8.AutoSize = false;
            this.txtItemB8.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemB8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemB8.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemB8.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemB8.Location = new System.Drawing.Point(0, 0);
            this.txtItemB8.Name = "txtItemB8";
            this.txtItemB8.NullText = "0";
            this.txtItemB8.PromptChar = ' ';
            this.txtItemB8.ReadOnly = true;
            this.txtItemB8.Size = new System.Drawing.Size(225, 27);
            this.txtItemB8.TabIndex = 6;
            this.txtItemB8.ValueChanged += new System.EventHandler(this.txtItemA1_ValueChanged);
            // 
            // ultraLabel150
            // 
            appearance229.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance229.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance229.BorderColor = System.Drawing.Color.Black;
            appearance229.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance229.TextHAlignAsString = "Center";
            appearance229.TextVAlignAsString = "Middle";
            this.ultraLabel150.Appearance = appearance229;
            this.ultraLabel150.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel150.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel150.Location = new System.Drawing.Point(567, 697);
            this.ultraLabel150.Name = "ultraLabel150";
            this.ultraLabel150.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel150.TabIndex = 313;
            this.ultraLabel150.Text = "B8";
            // 
            // NameB8
            // 
            appearance230.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance230.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance230.BorderColor = System.Drawing.Color.Black;
            appearance230.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance230.TextHAlignAsString = "Left";
            appearance230.TextVAlignAsString = "Middle";
            this.NameB8.Appearance = appearance230;
            this.NameB8.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameB8.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameB8.Location = new System.Drawing.Point(118, 697);
            this.NameB8.Name = "NameB8";
            this.NameB8.Size = new System.Drawing.Size(450, 29);
            this.NameB8.TabIndex = 312;
            this.NameB8.Text = "Điều chỉnh giảm tổng lợi nhuận trước thuế thu nhập doanh nghiệp                  " +
    "                 (B8 = B9 + B10 + B11)";
            // 
            // ultraLabel176
            // 
            appearance231.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance231.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance231.BorderColor = System.Drawing.Color.Black;
            appearance231.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance231.TextHAlignAsString = "Center";
            appearance231.TextVAlignAsString = "Middle";
            this.ultraLabel176.Appearance = appearance231;
            this.ultraLabel176.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel176.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel176.Location = new System.Drawing.Point(80, 697);
            this.ultraLabel176.Name = "ultraLabel176";
            this.ultraLabel176.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel176.TabIndex = 311;
            this.ultraLabel176.Text = "2";
            // 
            // ultraPanel32
            // 
            appearance232.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance232.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance232.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel32.Appearance = appearance232;
            this.ultraPanel32.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel32.ClientArea
            // 
            this.ultraPanel32.ClientArea.Controls.Add(this.txtItemB7);
            this.ultraPanel32.Location = new System.Drawing.Point(723, 669);
            this.ultraPanel32.Name = "ultraPanel32";
            this.ultraPanel32.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel32.TabIndex = 310;
            // 
            // txtItemB7
            // 
            appearance233.TextHAlignAsString = "Right";
            this.txtItemB7.Appearance = appearance233;
            this.txtItemB7.AutoSize = false;
            this.txtItemB7.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemB7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemB7.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemB7.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemB7.Location = new System.Drawing.Point(0, 0);
            this.txtItemB7.Name = "txtItemB7";
            this.txtItemB7.NullText = "0";
            this.txtItemB7.PromptChar = ' ';
            this.txtItemB7.Size = new System.Drawing.Size(225, 27);
            this.txtItemB7.TabIndex = 6;
            this.txtItemB7.ValueChanged += new System.EventHandler(this.txtItemB2_ValueChanged);
            this.txtItemB7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemB7.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel147
            // 
            appearance234.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance234.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance234.BorderColor = System.Drawing.Color.Black;
            appearance234.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance234.TextHAlignAsString = "Center";
            appearance234.TextVAlignAsString = "Middle";
            this.ultraLabel147.Appearance = appearance234;
            this.ultraLabel147.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel147.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel147.Location = new System.Drawing.Point(567, 669);
            this.ultraLabel147.Name = "ultraLabel147";
            this.ultraLabel147.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel147.TabIndex = 309;
            this.ultraLabel147.Text = "B7";
            // 
            // NameB7
            // 
            appearance235.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance235.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance235.BorderColor = System.Drawing.Color.Black;
            appearance235.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance235.TextHAlignAsString = "Left";
            appearance235.TextVAlignAsString = "Middle";
            this.NameB7.Appearance = appearance235;
            this.NameB7.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameB7.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameB7.Location = new System.Drawing.Point(118, 669);
            this.NameB7.Name = "NameB7";
            this.NameB7.Size = new System.Drawing.Size(450, 29);
            this.NameB7.TabIndex = 308;
            this.NameB7.Text = "Các khoản điều chỉnh làm tăng lợi nhuận trước thuế khác";
            // 
            // ultraLabel149
            // 
            appearance236.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance236.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance236.BorderColor = System.Drawing.Color.Black;
            appearance236.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance236.TextHAlignAsString = "Center";
            appearance236.TextVAlignAsString = "Middle";
            this.ultraLabel149.Appearance = appearance236;
            this.ultraLabel149.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel149.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel149.Location = new System.Drawing.Point(80, 669);
            this.ultraLabel149.Name = "ultraLabel149";
            this.ultraLabel149.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel149.TabIndex = 307;
            this.ultraLabel149.Text = "1.6";
            // 
            // ultraPanel31
            // 
            appearance237.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance237.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance237.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel31.Appearance = appearance237;
            this.ultraPanel31.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel31.ClientArea
            // 
            this.ultraPanel31.ClientArea.Controls.Add(this.txtItemB6);
            this.ultraPanel31.Location = new System.Drawing.Point(723, 641);
            this.ultraPanel31.Name = "ultraPanel31";
            this.ultraPanel31.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel31.TabIndex = 306;
            // 
            // txtItemB6
            // 
            appearance238.TextHAlignAsString = "Right";
            this.txtItemB6.Appearance = appearance238;
            this.txtItemB6.AutoSize = false;
            this.txtItemB6.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemB6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemB6.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemB6.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemB6.Location = new System.Drawing.Point(0, 0);
            this.txtItemB6.Name = "txtItemB6";
            this.txtItemB6.NullText = "0";
            this.txtItemB6.PromptChar = ' ';
            this.txtItemB6.Size = new System.Drawing.Size(225, 27);
            this.txtItemB6.TabIndex = 6;
            this.txtItemB6.ValueChanged += new System.EventHandler(this.txtItemB2_ValueChanged);
            this.txtItemB6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemB6.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel144
            // 
            appearance239.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance239.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance239.BorderColor = System.Drawing.Color.Black;
            appearance239.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance239.TextHAlignAsString = "Center";
            appearance239.TextVAlignAsString = "Middle";
            this.ultraLabel144.Appearance = appearance239;
            this.ultraLabel144.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel144.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel144.Location = new System.Drawing.Point(567, 641);
            this.ultraLabel144.Name = "ultraLabel144";
            this.ultraLabel144.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel144.TabIndex = 305;
            this.ultraLabel144.Text = "B6";
            // 
            // NameB6
            // 
            appearance240.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance240.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance240.BorderColor = System.Drawing.Color.Black;
            appearance240.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance240.TextHAlignAsString = "Left";
            appearance240.TextVAlignAsString = "Middle";
            this.NameB6.Appearance = appearance240;
            this.NameB6.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameB6.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameB6.Location = new System.Drawing.Point(118, 641);
            this.NameB6.Name = "NameB6";
            this.NameB6.Size = new System.Drawing.Size(450, 29);
            this.NameB6.TabIndex = 304;
            this.NameB6.Text = "Điều chỉnh tăng lợi nhuận do xác định giá thị trường đối với                     " +
    "                    giao dịch liên kết";
            // 
            // ultraLabel146
            // 
            appearance241.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance241.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance241.BorderColor = System.Drawing.Color.Black;
            appearance241.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance241.TextHAlignAsString = "Center";
            appearance241.TextVAlignAsString = "Middle";
            this.ultraLabel146.Appearance = appearance241;
            this.ultraLabel146.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel146.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel146.Location = new System.Drawing.Point(80, 641);
            this.ultraLabel146.Name = "ultraLabel146";
            this.ultraLabel146.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel146.TabIndex = 303;
            this.ultraLabel146.Text = "1.5";
            // 
            // ultraPanel30
            // 
            appearance242.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance242.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance242.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel30.Appearance = appearance242;
            this.ultraPanel30.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel30.ClientArea
            // 
            this.ultraPanel30.ClientArea.Controls.Add(this.txtItemB5);
            this.ultraPanel30.Location = new System.Drawing.Point(723, 613);
            this.ultraPanel30.Name = "ultraPanel30";
            this.ultraPanel30.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel30.TabIndex = 302;
            // 
            // txtItemB5
            // 
            appearance243.TextHAlignAsString = "Right";
            this.txtItemB5.Appearance = appearance243;
            this.txtItemB5.AutoSize = false;
            this.txtItemB5.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemB5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemB5.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemB5.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemB5.Location = new System.Drawing.Point(0, 0);
            this.txtItemB5.Name = "txtItemB5";
            this.txtItemB5.NullText = "0";
            this.txtItemB5.PromptChar = ' ';
            this.txtItemB5.Size = new System.Drawing.Size(225, 27);
            this.txtItemB5.TabIndex = 6;
            this.txtItemB5.ValueChanged += new System.EventHandler(this.txtItemB2_ValueChanged);
            this.txtItemB5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemB5.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel141
            // 
            appearance244.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance244.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance244.BorderColor = System.Drawing.Color.Black;
            appearance244.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance244.TextHAlignAsString = "Center";
            appearance244.TextVAlignAsString = "Middle";
            this.ultraLabel141.Appearance = appearance244;
            this.ultraLabel141.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel141.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel141.Location = new System.Drawing.Point(567, 613);
            this.ultraLabel141.Name = "ultraLabel141";
            this.ultraLabel141.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel141.TabIndex = 301;
            this.ultraLabel141.Text = "B5";
            // 
            // NameB5
            // 
            appearance245.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance245.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance245.BorderColor = System.Drawing.Color.Black;
            appearance245.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance245.TextHAlignAsString = "Left";
            appearance245.TextVAlignAsString = "Middle";
            this.NameB5.Appearance = appearance245;
            this.NameB5.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameB5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameB5.Location = new System.Drawing.Point(118, 613);
            this.NameB5.Name = "NameB5";
            this.NameB5.Size = new System.Drawing.Size(450, 29);
            this.NameB5.TabIndex = 300;
            this.NameB5.Text = "Thuế thu nhập đã nộp cho phần thu nhập nhận được ở nước ngoài";
            // 
            // ultraLabel143
            // 
            appearance246.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance246.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance246.BorderColor = System.Drawing.Color.Black;
            appearance246.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance246.TextHAlignAsString = "Center";
            appearance246.TextVAlignAsString = "Middle";
            this.ultraLabel143.Appearance = appearance246;
            this.ultraLabel143.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel143.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel143.Location = new System.Drawing.Point(80, 613);
            this.ultraLabel143.Name = "ultraLabel143";
            this.ultraLabel143.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel143.TabIndex = 299;
            this.ultraLabel143.Text = "1.4";
            // 
            // ultraPanel29
            // 
            appearance247.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance247.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance247.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel29.Appearance = appearance247;
            this.ultraPanel29.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel29.ClientArea
            // 
            this.ultraPanel29.ClientArea.Controls.Add(this.txtItemB4);
            this.ultraPanel29.Location = new System.Drawing.Point(723, 585);
            this.ultraPanel29.Name = "ultraPanel29";
            this.ultraPanel29.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel29.TabIndex = 298;
            // 
            // txtItemB4
            // 
            appearance248.TextHAlignAsString = "Right";
            this.txtItemB4.Appearance = appearance248;
            this.txtItemB4.AutoSize = false;
            this.txtItemB4.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemB4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemB4.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemB4.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemB4.Location = new System.Drawing.Point(0, 0);
            this.txtItemB4.Name = "txtItemB4";
            this.txtItemB4.NullText = "0";
            this.txtItemB4.PromptChar = ' ';
            this.txtItemB4.Size = new System.Drawing.Size(225, 27);
            this.txtItemB4.TabIndex = 6;
            this.txtItemB4.ValueChanged += new System.EventHandler(this.txtItemB2_ValueChanged);
            this.txtItemB4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemB4.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel138
            // 
            appearance249.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance249.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance249.BorderColor = System.Drawing.Color.Black;
            appearance249.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance249.TextHAlignAsString = "Center";
            appearance249.TextVAlignAsString = "Middle";
            this.ultraLabel138.Appearance = appearance249;
            this.ultraLabel138.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel138.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel138.Location = new System.Drawing.Point(567, 585);
            this.ultraLabel138.Name = "ultraLabel138";
            this.ultraLabel138.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel138.TabIndex = 297;
            this.ultraLabel138.Text = "B4";
            // 
            // NameB4
            // 
            appearance250.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance250.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance250.BorderColor = System.Drawing.Color.Black;
            appearance250.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance250.TextHAlignAsString = "Left";
            appearance250.TextVAlignAsString = "Middle";
            this.NameB4.Appearance = appearance250;
            this.NameB4.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameB4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameB4.Location = new System.Drawing.Point(118, 585);
            this.NameB4.Name = "NameB4";
            this.NameB4.Size = new System.Drawing.Size(450, 29);
            this.NameB4.TabIndex = 296;
            this.NameB4.Text = "Các khoản chi không được trừ khi xác định thu nhập chịu thuế";
            // 
            // ultraLabel140
            // 
            appearance251.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance251.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance251.BorderColor = System.Drawing.Color.Black;
            appearance251.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance251.TextHAlignAsString = "Center";
            appearance251.TextVAlignAsString = "Middle";
            this.ultraLabel140.Appearance = appearance251;
            this.ultraLabel140.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel140.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel140.Location = new System.Drawing.Point(80, 585);
            this.ultraLabel140.Name = "ultraLabel140";
            this.ultraLabel140.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel140.TabIndex = 295;
            this.ultraLabel140.Text = "1.3";
            // 
            // ultraPanel28
            // 
            appearance252.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance252.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance252.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel28.Appearance = appearance252;
            this.ultraPanel28.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel28.ClientArea
            // 
            this.ultraPanel28.ClientArea.Controls.Add(this.txtItemB3);
            this.ultraPanel28.Location = new System.Drawing.Point(723, 557);
            this.ultraPanel28.Name = "ultraPanel28";
            this.ultraPanel28.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel28.TabIndex = 294;
            // 
            // txtItemB3
            // 
            appearance253.TextHAlignAsString = "Right";
            this.txtItemB3.Appearance = appearance253;
            this.txtItemB3.AutoSize = false;
            this.txtItemB3.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemB3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemB3.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemB3.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemB3.Location = new System.Drawing.Point(0, 0);
            this.txtItemB3.Name = "txtItemB3";
            this.txtItemB3.NullText = "0";
            this.txtItemB3.PromptChar = ' ';
            this.txtItemB3.Size = new System.Drawing.Size(225, 27);
            this.txtItemB3.TabIndex = 6;
            this.txtItemB3.ValueChanged += new System.EventHandler(this.txtItemB2_ValueChanged);
            this.txtItemB3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemB3.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel135
            // 
            appearance254.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance254.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance254.BorderColor = System.Drawing.Color.Black;
            appearance254.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance254.TextHAlignAsString = "Center";
            appearance254.TextVAlignAsString = "Middle";
            this.ultraLabel135.Appearance = appearance254;
            this.ultraLabel135.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel135.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel135.Location = new System.Drawing.Point(567, 557);
            this.ultraLabel135.Name = "ultraLabel135";
            this.ultraLabel135.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel135.TabIndex = 293;
            this.ultraLabel135.Text = "B3";
            // 
            // NameB3
            // 
            appearance255.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance255.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance255.BorderColor = System.Drawing.Color.Black;
            appearance255.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance255.TextHAlignAsString = "Left";
            appearance255.TextVAlignAsString = "Middle";
            this.NameB3.Appearance = appearance255;
            this.NameB3.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameB3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameB3.Location = new System.Drawing.Point(118, 557);
            this.NameB3.Name = "NameB3";
            this.NameB3.Size = new System.Drawing.Size(450, 29);
            this.NameB3.TabIndex = 292;
            this.NameB3.Text = "Chi phí của phần doanh thu điều chỉnh giảm";
            // 
            // ultraLabel137
            // 
            appearance256.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance256.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance256.BorderColor = System.Drawing.Color.Black;
            appearance256.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance256.TextHAlignAsString = "Center";
            appearance256.TextVAlignAsString = "Middle";
            this.ultraLabel137.Appearance = appearance256;
            this.ultraLabel137.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel137.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel137.Location = new System.Drawing.Point(80, 557);
            this.ultraLabel137.Name = "ultraLabel137";
            this.ultraLabel137.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel137.TabIndex = 291;
            this.ultraLabel137.Text = "1.2";
            // 
            // ultraPanel27
            // 
            appearance257.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance257.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance257.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel27.Appearance = appearance257;
            this.ultraPanel27.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel27.ClientArea
            // 
            this.ultraPanel27.ClientArea.Controls.Add(this.txtItemB2);
            this.ultraPanel27.Location = new System.Drawing.Point(723, 529);
            this.ultraPanel27.Name = "ultraPanel27";
            this.ultraPanel27.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel27.TabIndex = 290;
            // 
            // txtItemB2
            // 
            appearance258.TextHAlignAsString = "Right";
            this.txtItemB2.Appearance = appearance258;
            this.txtItemB2.AutoSize = false;
            this.txtItemB2.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemB2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemB2.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemB2.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemB2.Location = new System.Drawing.Point(0, 0);
            this.txtItemB2.Name = "txtItemB2";
            this.txtItemB2.NullText = "0";
            this.txtItemB2.PromptChar = ' ';
            this.txtItemB2.Size = new System.Drawing.Size(225, 27);
            this.txtItemB2.TabIndex = 6;
            this.txtItemB2.ValueChanged += new System.EventHandler(this.txtItemB2_ValueChanged);
            this.txtItemB2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemB2.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel132
            // 
            appearance259.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance259.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance259.BorderColor = System.Drawing.Color.Black;
            appearance259.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance259.TextHAlignAsString = "Center";
            appearance259.TextVAlignAsString = "Middle";
            this.ultraLabel132.Appearance = appearance259;
            this.ultraLabel132.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel132.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel132.Location = new System.Drawing.Point(567, 529);
            this.ultraLabel132.Name = "ultraLabel132";
            this.ultraLabel132.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel132.TabIndex = 289;
            this.ultraLabel132.Text = "B2";
            // 
            // NameB2
            // 
            appearance260.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance260.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance260.BorderColor = System.Drawing.Color.Black;
            appearance260.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance260.TextHAlignAsString = "Left";
            appearance260.TextVAlignAsString = "Middle";
            this.NameB2.Appearance = appearance260;
            this.NameB2.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameB2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameB2.Location = new System.Drawing.Point(118, 529);
            this.NameB2.Name = "NameB2";
            this.NameB2.Size = new System.Drawing.Size(450, 29);
            this.NameB2.TabIndex = 288;
            this.NameB2.Text = "Các khoản điều chỉnh tăng doanh thu";
            // 
            // ultraLabel134
            // 
            appearance261.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance261.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance261.BorderColor = System.Drawing.Color.Black;
            appearance261.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance261.TextHAlignAsString = "Center";
            appearance261.TextVAlignAsString = "Middle";
            this.ultraLabel134.Appearance = appearance261;
            this.ultraLabel134.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel134.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel134.Location = new System.Drawing.Point(80, 529);
            this.ultraLabel134.Name = "ultraLabel134";
            this.ultraLabel134.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel134.TabIndex = 287;
            this.ultraLabel134.Text = "1.1";
            // 
            // ultraPanel26
            // 
            appearance262.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance262.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance262.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel26.Appearance = appearance262;
            this.ultraPanel26.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel26.ClientArea
            // 
            this.ultraPanel26.ClientArea.Controls.Add(this.txtItemB1);
            this.ultraPanel26.Location = new System.Drawing.Point(723, 501);
            this.ultraPanel26.Name = "ultraPanel26";
            this.ultraPanel26.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel26.TabIndex = 286;
            // 
            // txtItemB1
            // 
            appearance263.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance263.TextHAlignAsString = "Right";
            this.txtItemB1.Appearance = appearance263;
            this.txtItemB1.AutoSize = false;
            this.txtItemB1.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemB1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemB1.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemB1.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemB1.Location = new System.Drawing.Point(0, 0);
            this.txtItemB1.Name = "txtItemB1";
            this.txtItemB1.NullText = "0";
            this.txtItemB1.PromptChar = ' ';
            this.txtItemB1.ReadOnly = true;
            this.txtItemB1.Size = new System.Drawing.Size(225, 27);
            this.txtItemB1.TabIndex = 6;
            // 
            // ultraLabel129
            // 
            appearance264.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance264.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance264.BorderColor = System.Drawing.Color.Black;
            appearance264.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance264.TextHAlignAsString = "Center";
            appearance264.TextVAlignAsString = "Middle";
            this.ultraLabel129.Appearance = appearance264;
            this.ultraLabel129.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel129.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel129.Location = new System.Drawing.Point(567, 501);
            this.ultraLabel129.Name = "ultraLabel129";
            this.ultraLabel129.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel129.TabIndex = 285;
            this.ultraLabel129.Text = "B1";
            // 
            // NameB1
            // 
            appearance265.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance265.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance265.BorderColor = System.Drawing.Color.Black;
            appearance265.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance265.TextHAlignAsString = "Left";
            appearance265.TextVAlignAsString = "Middle";
            this.NameB1.Appearance = appearance265;
            this.NameB1.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameB1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameB1.Location = new System.Drawing.Point(118, 501);
            this.NameB1.Name = "NameB1";
            this.NameB1.Size = new System.Drawing.Size(450, 29);
            this.NameB1.TabIndex = 284;
            this.NameB1.Text = "Điều chỉnh tăng tổng hợp lợi nhuận trước thuế thu nhập doanh nghiệp              " +
    "               (B1 = B2 + B3 + B4 + B5 + B6 + B7)";
            // 
            // ultraLabel131
            // 
            appearance266.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance266.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance266.BorderColor = System.Drawing.Color.Black;
            appearance266.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance266.TextHAlignAsString = "Center";
            appearance266.TextVAlignAsString = "Middle";
            this.ultraLabel131.Appearance = appearance266;
            this.ultraLabel131.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel131.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel131.Location = new System.Drawing.Point(80, 501);
            this.ultraLabel131.Name = "ultraLabel131";
            this.ultraLabel131.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel131.TabIndex = 283;
            this.ultraLabel131.Text = "1";
            // 
            // ultraLabel199
            // 
            appearance267.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel199.Appearance = appearance267;
            this.ultraLabel199.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel199.Location = new System.Drawing.Point(11, 2377);
            this.ultraLabel199.Name = "ultraLabel199";
            this.ultraLabel199.Size = new System.Drawing.Size(167, 16);
            this.ultraLabel199.TabIndex = 282;
            // 
            // ultraPanel25
            // 
            appearance268.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance268.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance268.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel25.Appearance = appearance268;
            this.ultraPanel25.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraPanel25.Location = new System.Drawing.Point(723, 473);
            this.ultraPanel25.Name = "ultraPanel25";
            this.ultraPanel25.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel25.TabIndex = 270;
            // 
            // ultraPanel24
            // 
            appearance269.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance269.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance269.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel24.Appearance = appearance269;
            this.ultraPanel24.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel24.ClientArea
            // 
            this.ultraPanel24.ClientArea.Controls.Add(this.txtItemA1);
            this.ultraPanel24.Location = new System.Drawing.Point(723, 445);
            this.ultraPanel24.Name = "ultraPanel24";
            this.ultraPanel24.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel24.TabIndex = 269;
            // 
            // txtItemA1
            // 
            appearance270.TextHAlignAsString = "Right";
            this.txtItemA1.Appearance = appearance270;
            this.txtItemA1.AutoSize = false;
            this.txtItemA1.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemA1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemA1.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemA1.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemA1.Location = new System.Drawing.Point(0, 0);
            this.txtItemA1.Name = "txtItemA1";
            this.txtItemA1.NullText = "0";
            this.txtItemA1.PromptChar = ' ';
            this.txtItemA1.Size = new System.Drawing.Size(225, 27);
            this.txtItemA1.TabIndex = 6;
            this.txtItemA1.ValueChanged += new System.EventHandler(this.txtItemA1_ValueChanged);
            this.txtItemA1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            this.txtItemA1.Validated += new System.EventHandler(this.txtItemA1_Validated);
            // 
            // ultraLabel166
            // 
            appearance271.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance271.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance271.BorderColor = System.Drawing.Color.Black;
            appearance271.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance271.TextHAlignAsString = "Center";
            appearance271.TextVAlignAsString = "Middle";
            this.ultraLabel166.Appearance = appearance271;
            this.ultraLabel166.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel166.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel166.Location = new System.Drawing.Point(567, 473);
            this.ultraLabel166.Name = "ultraLabel166";
            this.ultraLabel166.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel166.TabIndex = 257;
            this.ultraLabel166.Text = "B";
            // 
            // ultraLabel165
            // 
            appearance272.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance272.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance272.BorderColor = System.Drawing.Color.Black;
            appearance272.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance272.TextHAlignAsString = "Center";
            appearance272.TextVAlignAsString = "Middle";
            this.ultraLabel165.Appearance = appearance272;
            this.ultraLabel165.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel165.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel165.Location = new System.Drawing.Point(567, 445);
            this.ultraLabel165.Name = "ultraLabel165";
            this.ultraLabel165.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel165.TabIndex = 256;
            this.ultraLabel165.Text = "A1";
            // 
            // NameB
            // 
            appearance273.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance273.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance273.BorderColor = System.Drawing.Color.Black;
            appearance273.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance273.TextHAlignAsString = "Left";
            appearance273.TextVAlignAsString = "Middle";
            this.NameB.Appearance = appearance273;
            this.NameB.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameB.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameB.Location = new System.Drawing.Point(118, 473);
            this.NameB.Name = "NameB";
            this.NameB.Size = new System.Drawing.Size(450, 29);
            this.NameB.TabIndex = 233;
            this.NameB.Text = "Xác định thu nhập chịu thuế theo Luật thuế thu nhập doanh nghiệp";
            // 
            // ultraLabel128
            // 
            appearance274.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance274.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance274.BorderColor = System.Drawing.Color.Black;
            appearance274.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance274.TextHAlignAsString = "Center";
            appearance274.TextVAlignAsString = "Middle";
            this.ultraLabel128.Appearance = appearance274;
            this.ultraLabel128.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel128.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel128.Location = new System.Drawing.Point(80, 473);
            this.ultraLabel128.Name = "ultraLabel128";
            this.ultraLabel128.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel128.TabIndex = 232;
            this.ultraLabel128.Text = "B";
            // 
            // NameA1
            // 
            appearance275.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance275.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance275.BorderColor = System.Drawing.Color.Black;
            appearance275.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance275.TextHAlignAsString = "Left";
            appearance275.TextVAlignAsString = "Middle";
            this.NameA1.Appearance = appearance275;
            this.NameA1.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameA1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameA1.Location = new System.Drawing.Point(118, 445);
            this.NameA1.Name = "NameA1";
            this.NameA1.Size = new System.Drawing.Size(450, 29);
            this.NameA1.TabIndex = 231;
            this.NameA1.Text = "Tổng lợi nhuận kế toán trước thuế thu nhập doanh nghiệp";
            // 
            // ultraLabel126
            // 
            appearance276.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance276.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance276.BorderColor = System.Drawing.Color.Black;
            appearance276.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance276.TextHAlignAsString = "Center";
            appearance276.TextVAlignAsString = "Middle";
            this.ultraLabel126.Appearance = appearance276;
            this.ultraLabel126.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel126.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel126.Location = new System.Drawing.Point(80, 445);
            this.ultraLabel126.Name = "ultraLabel126";
            this.ultraLabel126.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel126.TabIndex = 230;
            this.ultraLabel126.Text = "1";
            // 
            // ultraPanel23
            // 
            appearance277.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance277.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance277.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel23.Appearance = appearance277;
            this.ultraPanel23.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraPanel23.Location = new System.Drawing.Point(723, 417);
            this.ultraPanel23.Name = "ultraPanel23";
            this.ultraPanel23.Size = new System.Drawing.Size(227, 29);
            this.ultraPanel23.TabIndex = 229;
            // 
            // ultraLabel122
            // 
            appearance278.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance278.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance278.BorderColor = System.Drawing.Color.Black;
            appearance278.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance278.TextHAlignAsString = "Center";
            appearance278.TextVAlignAsString = "Middle";
            this.ultraLabel122.Appearance = appearance278;
            this.ultraLabel122.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel122.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel122.Location = new System.Drawing.Point(567, 417);
            this.ultraLabel122.Name = "ultraLabel122";
            this.ultraLabel122.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel122.TabIndex = 164;
            this.ultraLabel122.Text = "A";
            // 
            // NameA
            // 
            appearance279.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance279.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance279.BorderColor = System.Drawing.Color.Black;
            appearance279.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance279.TextHAlignAsString = "Left";
            appearance279.TextVAlignAsString = "Middle";
            this.NameA.Appearance = appearance279;
            this.NameA.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.NameA.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameA.Location = new System.Drawing.Point(118, 417);
            this.NameA.Name = "NameA";
            this.NameA.Size = new System.Drawing.Size(450, 29);
            this.NameA.TabIndex = 163;
            this.NameA.Text = "Kết quả kinh doanh ghi nhận theo báo cáo tài chính";
            // 
            // ultraLabel124
            // 
            appearance280.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance280.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance280.BorderColor = System.Drawing.Color.Black;
            appearance280.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance280.TextHAlignAsString = "Center";
            appearance280.TextVAlignAsString = "Middle";
            this.ultraLabel124.Appearance = appearance280;
            this.ultraLabel124.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel124.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel124.Location = new System.Drawing.Point(80, 417);
            this.ultraLabel124.Name = "ultraLabel124";
            this.ultraLabel124.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel124.TabIndex = 162;
            this.ultraLabel124.Text = "A";
            // 
            // txtItem15
            // 
            appearance281.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.txtItem15.Appearance = appearance281;
            this.txtItem15.AutoSize = true;
            this.txtItem15.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtItem15.Location = new System.Drawing.Point(305, 317);
            this.txtItem15.Name = "txtItem15";
            this.txtItem15.Size = new System.Drawing.Size(372, 15);
            this.txtItem15.TabIndex = 161;
            this.txtItem15.Text = "................................................................................." +
    "......................................";
            // 
            // txtItem14
            // 
            appearance282.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.txtItem14.Appearance = appearance282;
            this.txtItem14.AutoSize = true;
            this.txtItem14.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtItem14.Location = new System.Drawing.Point(305, 295);
            this.txtItem14.Name = "txtItem14";
            this.txtItem14.Size = new System.Drawing.Size(372, 15);
            this.txtItem14.TabIndex = 160;
            this.txtItem14.Text = "................................................................................." +
    "......................................";
            // 
            // ultraLabel119
            // 
            appearance283.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel119.Appearance = appearance283;
            this.ultraLabel119.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel119.Location = new System.Drawing.Point(118, 274);
            this.ultraLabel119.Name = "ultraLabel119";
            this.ultraLabel119.Size = new System.Drawing.Size(101, 16);
            this.ultraLabel119.TabIndex = 159;
            this.ultraLabel119.Text = "[07] Mã số thuế:";
            // 
            // ultraLabel118
            // 
            appearance284.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel118.Appearance = appearance284;
            this.ultraLabel118.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel118.Location = new System.Drawing.Point(118, 318);
            this.ultraLabel118.Name = "ultraLabel118";
            this.ultraLabel118.Size = new System.Drawing.Size(167, 16);
            this.ultraLabel118.TabIndex = 158;
            this.ultraLabel118.Text = "[15] Mã số thuế:";
            // 
            // txtItem7
            // 
            appearance285.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.txtItem7.Appearance = appearance285;
            this.txtItem7.AutoSize = true;
            this.txtItem7.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtItem7.Location = new System.Drawing.Point(305, 273);
            this.txtItem7.Name = "txtItem7";
            this.txtItem7.Size = new System.Drawing.Size(372, 15);
            this.txtItem7.TabIndex = 157;
            this.txtItem7.Text = "................................................................................." +
    "......................................";
            // 
            // txtItem6
            // 
            appearance286.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.txtItem6.Appearance = appearance286;
            this.txtItem6.AutoSize = true;
            this.txtItem6.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtItem6.Location = new System.Drawing.Point(305, 251);
            this.txtItem6.Name = "txtItem6";
            this.txtItem6.Size = new System.Drawing.Size(372, 15);
            this.txtItem6.TabIndex = 156;
            this.txtItem6.Text = "................................................................................." +
    "......................................";
            // 
            // ultraLabel110
            // 
            appearance287.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel110.Appearance = appearance287;
            this.ultraLabel110.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel110.Location = new System.Drawing.Point(118, 296);
            this.ultraLabel110.Name = "ultraLabel110";
            this.ultraLabel110.Size = new System.Drawing.Size(167, 16);
            this.ultraLabel110.TabIndex = 155;
            this.ultraLabel110.Text = "[14] Tên đại lý thuế (nếu có):";
            // 
            // ultraLabel114
            // 
            appearance288.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel114.Appearance = appearance288;
            this.ultraLabel114.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel114.Location = new System.Drawing.Point(118, 252);
            this.ultraLabel114.Name = "ultraLabel114";
            this.ultraLabel114.Size = new System.Drawing.Size(144, 16);
            this.ultraLabel114.TabIndex = 154;
            this.ultraLabel114.Text = "[06] Tên người nộp thuế:";
            // 
            // ultraLabel106
            // 
            appearance289.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance289.TextHAlignAsString = "Left";
            this.ultraLabel106.Appearance = appearance289;
            this.ultraLabel106.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel106.Location = new System.Drawing.Point(308, 224);
            this.ultraLabel106.Name = "ultraLabel106";
            this.ultraLabel106.Size = new System.Drawing.Size(83, 21);
            this.ultraLabel106.TabIndex = 153;
            this.ultraLabel106.Text = "[05] Tỷ lệ (%):";
            // 
            // ultraLabel86
            // 
            appearance290.BackColor = System.Drawing.Color.LightGray;
            appearance290.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance290.BorderColor = System.Drawing.Color.Black;
            appearance290.FontData.BoldAsString = "True";
            appearance290.ForeColor = System.Drawing.Color.Black;
            appearance290.TextHAlignAsString = "Center";
            appearance290.TextVAlignAsString = "Middle";
            this.ultraLabel86.Appearance = appearance290;
            this.ultraLabel86.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel86.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel86.Location = new System.Drawing.Point(723, 389);
            this.ultraLabel86.Name = "ultraLabel86";
            this.ultraLabel86.Size = new System.Drawing.Size(227, 29);
            this.ultraLabel86.TabIndex = 151;
            this.ultraLabel86.Text = "(4)";
            // 
            // ultraLabel90
            // 
            appearance291.BackColor = System.Drawing.Color.LightGray;
            appearance291.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance291.BorderColor = System.Drawing.Color.Black;
            appearance291.FontData.BoldAsString = "True";
            appearance291.ForeColor = System.Drawing.Color.Black;
            appearance291.TextHAlignAsString = "Center";
            appearance291.TextVAlignAsString = "Middle";
            this.ultraLabel90.Appearance = appearance291;
            this.ultraLabel90.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel90.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel90.Location = new System.Drawing.Point(118, 389);
            this.ultraLabel90.Name = "ultraLabel90";
            this.ultraLabel90.Size = new System.Drawing.Size(450, 29);
            this.ultraLabel90.TabIndex = 150;
            this.ultraLabel90.Text = "(2)";
            // 
            // ultraLabel94
            // 
            appearance292.BackColor = System.Drawing.Color.LightGray;
            appearance292.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance292.BorderColor = System.Drawing.Color.Black;
            appearance292.FontData.BoldAsString = "True";
            appearance292.ForeColor = System.Drawing.Color.Black;
            appearance292.TextHAlignAsString = "Center";
            appearance292.TextVAlignAsString = "Middle";
            this.ultraLabel94.Appearance = appearance292;
            this.ultraLabel94.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel94.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel94.Location = new System.Drawing.Point(567, 389);
            this.ultraLabel94.Name = "ultraLabel94";
            this.ultraLabel94.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel94.TabIndex = 149;
            this.ultraLabel94.Text = "(3)";
            // 
            // ultraLabel98
            // 
            appearance293.BackColor = System.Drawing.Color.LightGray;
            appearance293.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance293.BorderColor = System.Drawing.Color.Black;
            appearance293.BorderColor2 = System.Drawing.Color.Transparent;
            appearance293.FontData.BoldAsString = "True";
            appearance293.ForeColor = System.Drawing.Color.Black;
            appearance293.TextHAlignAsString = "Center";
            appearance293.TextVAlignAsString = "Middle";
            this.ultraLabel98.Appearance = appearance293;
            this.ultraLabel98.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel98.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel98.Location = new System.Drawing.Point(80, 389);
            this.ultraLabel98.Name = "ultraLabel98";
            this.ultraLabel98.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel98.TabIndex = 148;
            this.ultraLabel98.Text = "(1)";
            // 
            // ultraLabel99
            // 
            appearance294.BackColor = System.Drawing.Color.LightGray;
            appearance294.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance294.BorderColor = System.Drawing.Color.Black;
            appearance294.FontData.BoldAsString = "True";
            appearance294.ForeColor = System.Drawing.Color.Black;
            appearance294.TextHAlignAsString = "Center";
            appearance294.TextVAlignAsString = "Middle";
            this.ultraLabel99.Appearance = appearance294;
            this.ultraLabel99.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel99.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel99.Location = new System.Drawing.Point(723, 354);
            this.ultraLabel99.Name = "ultraLabel99";
            this.ultraLabel99.Size = new System.Drawing.Size(227, 36);
            this.ultraLabel99.TabIndex = 147;
            this.ultraLabel99.Text = "Số tiền";
            // 
            // ultraLabel100
            // 
            appearance295.BackColor = System.Drawing.Color.LightGray;
            appearance295.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance295.BorderColor = System.Drawing.Color.Black;
            appearance295.FontData.BoldAsString = "True";
            appearance295.ForeColor = System.Drawing.Color.Black;
            appearance295.TextHAlignAsString = "Center";
            appearance295.TextVAlignAsString = "Middle";
            this.ultraLabel100.Appearance = appearance295;
            this.ultraLabel100.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel100.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel100.Location = new System.Drawing.Point(118, 354);
            this.ultraLabel100.Name = "ultraLabel100";
            this.ultraLabel100.Size = new System.Drawing.Size(450, 36);
            this.ultraLabel100.TabIndex = 146;
            this.ultraLabel100.Text = "Chỉ tiêu";
            // 
            // ultraLabel101
            // 
            appearance296.BackColor = System.Drawing.Color.LightGray;
            appearance296.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance296.BorderColor = System.Drawing.Color.Black;
            appearance296.FontData.BoldAsString = "True";
            appearance296.ForeColor = System.Drawing.Color.Black;
            appearance296.TextHAlignAsString = "Center";
            appearance296.TextVAlignAsString = "Middle";
            this.ultraLabel101.Appearance = appearance296;
            this.ultraLabel101.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel101.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel101.Location = new System.Drawing.Point(567, 354);
            this.ultraLabel101.Name = "ultraLabel101";
            this.ultraLabel101.Size = new System.Drawing.Size(157, 36);
            this.ultraLabel101.TabIndex = 145;
            this.ultraLabel101.Text = "Mã chỉ tiêu";
            // 
            // ultraLabel102
            // 
            appearance297.BackColor = System.Drawing.Color.LightGray;
            appearance297.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance297.BorderColor = System.Drawing.Color.Black;
            appearance297.BorderColor2 = System.Drawing.Color.Transparent;
            appearance297.FontData.BoldAsString = "True";
            appearance297.ForeColor = System.Drawing.Color.Black;
            appearance297.TextHAlignAsString = "Center";
            appearance297.TextVAlignAsString = "Middle";
            this.ultraLabel102.Appearance = appearance297;
            this.ultraLabel102.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel102.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel102.Location = new System.Drawing.Point(80, 354);
            this.ultraLabel102.Name = "ultraLabel102";
            this.ultraLabel102.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ultraLabel102.Size = new System.Drawing.Size(39, 36);
            this.ultraLabel102.TabIndex = 144;
            this.ultraLabel102.Text = "STT";
            // 
            // cbbTMIndustryType
            // 
            appearance298.BackColor = System.Drawing.SystemColors.Window;
            appearance298.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbTMIndustryType.DisplayLayout.Appearance = appearance298;
            this.cbbTMIndustryType.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbTMIndustryType.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance299.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance299.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance299.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance299.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbTMIndustryType.DisplayLayout.GroupByBox.Appearance = appearance299;
            appearance300.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbTMIndustryType.DisplayLayout.GroupByBox.BandLabelAppearance = appearance300;
            this.cbbTMIndustryType.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance301.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance301.BackColor2 = System.Drawing.SystemColors.Control;
            appearance301.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance301.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbTMIndustryType.DisplayLayout.GroupByBox.PromptAppearance = appearance301;
            this.cbbTMIndustryType.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbTMIndustryType.DisplayLayout.MaxRowScrollRegions = 1;
            appearance302.BackColor = System.Drawing.SystemColors.Window;
            appearance302.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbTMIndustryType.DisplayLayout.Override.ActiveCellAppearance = appearance302;
            appearance303.BackColor = System.Drawing.SystemColors.Highlight;
            appearance303.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbTMIndustryType.DisplayLayout.Override.ActiveRowAppearance = appearance303;
            this.cbbTMIndustryType.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbTMIndustryType.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance304.BackColor = System.Drawing.SystemColors.Window;
            this.cbbTMIndustryType.DisplayLayout.Override.CardAreaAppearance = appearance304;
            appearance305.BorderColor = System.Drawing.Color.Silver;
            appearance305.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbTMIndustryType.DisplayLayout.Override.CellAppearance = appearance305;
            this.cbbTMIndustryType.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbTMIndustryType.DisplayLayout.Override.CellPadding = 0;
            appearance306.BackColor = System.Drawing.SystemColors.Control;
            appearance306.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance306.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance306.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance306.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbTMIndustryType.DisplayLayout.Override.GroupByRowAppearance = appearance306;
            appearance307.TextHAlignAsString = "Left";
            this.cbbTMIndustryType.DisplayLayout.Override.HeaderAppearance = appearance307;
            this.cbbTMIndustryType.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbTMIndustryType.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance308.BackColor = System.Drawing.SystemColors.Window;
            appearance308.BorderColor = System.Drawing.Color.Silver;
            this.cbbTMIndustryType.DisplayLayout.Override.RowAppearance = appearance308;
            this.cbbTMIndustryType.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance309.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbTMIndustryType.DisplayLayout.Override.TemplateAddRowAppearance = appearance309;
            this.cbbTMIndustryType.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbTMIndustryType.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbTMIndustryType.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbTMIndustryType.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbTMIndustryType.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbTMIndustryType.Location = new System.Drawing.Point(539, 189);
            this.cbbTMIndustryType.Name = "cbbTMIndustryType";
            this.cbbTMIndustryType.Size = new System.Drawing.Size(411, 23);
            this.cbbTMIndustryType.TabIndex = 2;
            // 
            // ultraLabel82
            // 
            appearance310.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance310.TextHAlignAsString = "Left";
            this.ultraLabel82.Appearance = appearance310;
            this.ultraLabel82.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel82.Location = new System.Drawing.Point(305, 189);
            this.ultraLabel82.Name = "ultraLabel82";
            this.ultraLabel82.Size = new System.Drawing.Size(228, 21);
            this.ultraLabel82.TabIndex = 25;
            this.ultraLabel82.Text = "[04] Ngành nghề có tỷ lệ doanh thu cao nhất";
            // 
            // txtIsRelatedTransactionEnterp
            // 
            this.txtIsRelatedTransactionEnterp.BackColor = System.Drawing.Color.Transparent;
            this.txtIsRelatedTransactionEnterp.BackColorInternal = System.Drawing.Color.Transparent;
            this.txtIsRelatedTransactionEnterp.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txtIsRelatedTransactionEnterp.Location = new System.Drawing.Point(305, 160);
            this.txtIsRelatedTransactionEnterp.Name = "txtIsRelatedTransactionEnterp";
            this.txtIsRelatedTransactionEnterp.Size = new System.Drawing.Size(14, 20);
            this.txtIsRelatedTransactionEnterp.TabIndex = 24;
            // 
            // ultraLabel78
            // 
            appearance311.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance311.TextHAlignAsString = "Left";
            this.ultraLabel78.Appearance = appearance311;
            this.ultraLabel78.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel78.Location = new System.Drawing.Point(325, 163);
            this.ultraLabel78.Name = "ultraLabel78";
            this.ultraLabel78.Size = new System.Drawing.Size(335, 21);
            this.ultraLabel78.TabIndex = 23;
            this.ultraLabel78.Text = "Doanh nghiệp thuộc đối tượng kê khai thông tin giao dịch liên kết";
            // 
            // txtIsDependentEnterprise
            // 
            this.txtIsDependentEnterprise.BackColor = System.Drawing.Color.Transparent;
            this.txtIsDependentEnterprise.BackColorInternal = System.Drawing.Color.Transparent;
            this.txtIsDependentEnterprise.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txtIsDependentEnterprise.Location = new System.Drawing.Point(305, 133);
            this.txtIsDependentEnterprise.Name = "txtIsDependentEnterprise";
            this.txtIsDependentEnterprise.Size = new System.Drawing.Size(14, 20);
            this.txtIsDependentEnterprise.TabIndex = 22;
            // 
            // ultraLabel74
            // 
            appearance312.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance312.TextHAlignAsString = "Left";
            this.ultraLabel74.Appearance = appearance312;
            this.ultraLabel74.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel74.Location = new System.Drawing.Point(325, 136);
            this.ultraLabel74.Name = "ultraLabel74";
            this.ultraLabel74.Size = new System.Drawing.Size(266, 21);
            this.ultraLabel74.TabIndex = 21;
            this.ultraLabel74.Text = "Doanh nghiệp có cơ sở sản xuất hạch toán phụ thuộc";
            // 
            // txtIsSMEEnterprise
            // 
            this.txtIsSMEEnterprise.BackColor = System.Drawing.Color.Transparent;
            this.txtIsSMEEnterprise.BackColorInternal = System.Drawing.Color.Transparent;
            this.txtIsSMEEnterprise.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txtIsSMEEnterprise.Location = new System.Drawing.Point(305, 106);
            this.txtIsSMEEnterprise.Name = "txtIsSMEEnterprise";
            this.txtIsSMEEnterprise.Size = new System.Drawing.Size(14, 20);
            this.txtIsSMEEnterprise.TabIndex = 20;
            // 
            // ultraLabel70
            // 
            appearance313.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance313.TextHAlignAsString = "Left";
            this.ultraLabel70.Appearance = appearance313;
            this.ultraLabel70.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel70.Location = new System.Drawing.Point(325, 109);
            this.ultraLabel70.Name = "ultraLabel70";
            this.ultraLabel70.Size = new System.Drawing.Size(201, 21);
            this.ultraLabel70.TabIndex = 19;
            this.ultraLabel70.Text = "Doanh nghiệp có quy mô vừa và nhỏ";
            // 
            // txtItem3
            // 
            this.txtItem3.Enabled = false;
            this.txtItem3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtItem3.Location = new System.Drawing.Point(555, 74);
            this.txtItem3.Name = "txtItem3";
            this.txtItem3.Size = new System.Drawing.Size(61, 22);
            this.txtItem3.TabIndex = 1;
            // 
            // ultraLabel66
            // 
            appearance314.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance314.TextHAlignAsString = "Left";
            this.ultraLabel66.Appearance = appearance314;
            this.ultraLabel66.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel66.Location = new System.Drawing.Point(445, 79);
            this.ultraLabel66.Name = "ultraLabel66";
            this.ultraLabel66.Size = new System.Drawing.Size(111, 21);
            this.ultraLabel66.TabIndex = 17;
            this.ultraLabel66.Text = "[03] Bổ sung lần thứ:";
            // 
            // txtItem2
            // 
            this.txtItem2.BackColor = System.Drawing.Color.Transparent;
            this.txtItem2.BackColorInternal = System.Drawing.Color.Transparent;
            this.txtItem2.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txtItem2.Enabled = false;
            this.txtItem2.Location = new System.Drawing.Point(377, 77);
            this.txtItem2.Name = "txtItem2";
            this.txtItem2.Size = new System.Drawing.Size(14, 20);
            this.txtItem2.TabIndex = 16;
            // 
            // ultraLabel62
            // 
            appearance315.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance315.TextHAlignAsString = "Left";
            this.ultraLabel62.Appearance = appearance315;
            this.ultraLabel62.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel62.Location = new System.Drawing.Point(305, 79);
            this.ultraLabel62.Name = "ultraLabel62";
            this.ultraLabel62.Size = new System.Drawing.Size(77, 21);
            this.ultraLabel62.TabIndex = 15;
            this.ultraLabel62.Text = "[02] Lần đầu:";
            // 
            // ultraLabel38
            // 
            appearance316.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance316.TextHAlignAsString = "Left";
            this.ultraLabel38.Appearance = appearance316;
            this.ultraLabel38.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel38.Location = new System.Drawing.Point(305, 52);
            this.ultraLabel38.Name = "ultraLabel38";
            this.ultraLabel38.Size = new System.Drawing.Size(100, 21);
            this.ultraLabel38.TabIndex = 9;
            this.ultraLabel38.Text = "[01] Kỳ tính thuế";
            // 
            // txtDeclarationName
            // 
            appearance317.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance317.TextHAlignAsString = "Center";
            appearance317.TextVAlignAsString = "Bottom";
            this.txtDeclarationName.Appearance = appearance317;
            this.txtDeclarationName.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDeclarationName.Location = new System.Drawing.Point(0, 0);
            this.txtDeclarationName.Name = "txtDeclarationName";
            this.txtDeclarationName.Size = new System.Drawing.Size(1110, 46);
            this.txtDeclarationName.TabIndex = 8;
            this.txtDeclarationName.Text = "TỜ KHAI QUYẾT TOÁN THUẾ THU NHẬP DOANH NGHIỆP (Mẫu số 03/TNDN)";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.AutoScroll = true;
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel9);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel22);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel21);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel20);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel19);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel18);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel17);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel16);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel15);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel14);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel13);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel12);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel11);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel10);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel9);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel8);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel7);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel6);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel5);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel4);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel3);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel115);
            this.ultraTabPageControl2.Controls.Add(this.txtNamePL19);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel117);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel111);
            this.ultraTabPageControl2.Controls.Add(this.txtNamePL18);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel113);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel107);
            this.ultraTabPageControl2.Controls.Add(this.txtNamePL17);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel109);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel103);
            this.ultraTabPageControl2.Controls.Add(this.txtNamePL16);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel105);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel95);
            this.ultraTabPageControl2.Controls.Add(this.txtNamePL15);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel97);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel91);
            this.ultraTabPageControl2.Controls.Add(this.txtNamePL14);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel93);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel87);
            this.ultraTabPageControl2.Controls.Add(this.txtNamePL13);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel89);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel83);
            this.ultraTabPageControl2.Controls.Add(this.txtNamePL12);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel85);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel79);
            this.ultraTabPageControl2.Controls.Add(this.txtNamePL11);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel81);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel75);
            this.ultraTabPageControl2.Controls.Add(this.txtNamePL10);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel77);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel71);
            this.ultraTabPageControl2.Controls.Add(this.txtNamePL9);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel73);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel67);
            this.ultraTabPageControl2.Controls.Add(this.txtNamePL8);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel69);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel63);
            this.ultraTabPageControl2.Controls.Add(this.txtNamePL7);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel65);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel59);
            this.ultraTabPageControl2.Controls.Add(this.txtNamePL6);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel61);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel55);
            this.ultraTabPageControl2.Controls.Add(this.txtNamePL5);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel57);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel51);
            this.ultraTabPageControl2.Controls.Add(this.txtNamePL4);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel53);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel47);
            this.ultraTabPageControl2.Controls.Add(this.txtNamePL3);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel49);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel43);
            this.ultraTabPageControl2.Controls.Add(this.txtNamePL2);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel45);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel39);
            this.ultraTabPageControl2.Controls.Add(this.txtNamePL1);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel41);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel36);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel34);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel35);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel33);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel25);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel26);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel27);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel28);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel29);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel30);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel31);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel32);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel24);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel22);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel23);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1128, 568);
            // 
            // ultraLabel9
            // 
            appearance318.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel9.Appearance = appearance318;
            this.ultraLabel9.Location = new System.Drawing.Point(859, 868);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(100, 200);
            this.ultraLabel9.TabIndex = 248;
            // 
            // ultraPanel22
            // 
            appearance319.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance319.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance319.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel22.Appearance = appearance319;
            this.ultraPanel22.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel22.ClientArea
            // 
            this.ultraPanel22.ClientArea.Controls.Add(this.txtItemPL19);
            this.ultraPanel22.Location = new System.Drawing.Point(746, 815);
            this.ultraPanel22.Name = "ultraPanel22";
            this.ultraPanel22.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel22.TabIndex = 247;
            // 
            // txtItemPL19
            // 
            appearance320.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance320.FontData.BoldAsString = "True";
            appearance320.TextHAlignAsString = "Right";
            this.txtItemPL19.Appearance = appearance320;
            this.txtItemPL19.AutoSize = false;
            this.txtItemPL19.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL19.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL19.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL19.Location = new System.Drawing.Point(0, 0);
            this.txtItemPL19.Name = "txtItemPL19";
            this.txtItemPL19.NullText = "0";
            this.txtItemPL19.PromptChar = ' ';
            this.txtItemPL19.ReadOnly = true;
            this.txtItemPL19.Size = new System.Drawing.Size(225, 30);
            this.txtItemPL19.TabIndex = 7;
            this.txtItemPL19.ValueChanged += new System.EventHandler(this.txtItemPL19_ValueChanged);
            // 
            // ultraPanel21
            // 
            appearance321.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance321.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance321.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel21.Appearance = appearance321;
            this.ultraPanel21.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel21.ClientArea
            // 
            this.ultraPanel21.ClientArea.Controls.Add(this.txtItemPL18);
            this.ultraPanel21.Location = new System.Drawing.Point(746, 784);
            this.ultraPanel21.Name = "ultraPanel21";
            this.ultraPanel21.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel21.TabIndex = 246;
            // 
            // txtItemPL18
            // 
            appearance322.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance322.FontData.BoldAsString = "True";
            appearance322.TextHAlignAsString = "Right";
            this.txtItemPL18.Appearance = appearance322;
            this.txtItemPL18.AutoSize = false;
            this.txtItemPL18.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL18.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL18.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL18.Location = new System.Drawing.Point(0, 0);
            this.txtItemPL18.Name = "txtItemPL18";
            this.txtItemPL18.NullText = "0";
            this.txtItemPL18.PromptChar = ' ';
            this.txtItemPL18.ReadOnly = true;
            this.txtItemPL18.Size = new System.Drawing.Size(225, 30);
            this.txtItemPL18.TabIndex = 7;
            this.txtItemPL18.ValueChanged += new System.EventHandler(this.txtItemPL15_ValueChanged);
            // 
            // ultraPanel20
            // 
            appearance323.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance323.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance323.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel20.Appearance = appearance323;
            this.ultraPanel20.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel20.ClientArea
            // 
            this.ultraPanel20.ClientArea.Controls.Add(this.txtItemPL17);
            this.ultraPanel20.Location = new System.Drawing.Point(746, 753);
            this.ultraPanel20.Name = "ultraPanel20";
            this.ultraPanel20.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel20.TabIndex = 245;
            // 
            // txtItemPL17
            // 
            appearance324.FontData.BoldAsString = "True";
            appearance324.TextHAlignAsString = "Right";
            this.txtItemPL17.Appearance = appearance324;
            this.txtItemPL17.AutoSize = false;
            this.txtItemPL17.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL17.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL17.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL17.Location = new System.Drawing.Point(0, 0);
            this.txtItemPL17.Name = "txtItemPL17";
            this.txtItemPL17.NullText = "0";
            this.txtItemPL17.PromptChar = ' ';
            this.txtItemPL17.Size = new System.Drawing.Size(225, 30);
            this.txtItemPL17.TabIndex = 7;
            this.txtItemPL17.ValueChanged += new System.EventHandler(this.txtItemPL16_ValueChanged);
            this.txtItemPL17.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // ultraPanel19
            // 
            appearance325.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance325.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance325.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel19.Appearance = appearance325;
            this.ultraPanel19.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel19.ClientArea
            // 
            this.ultraPanel19.ClientArea.Controls.Add(this.txtItemPL16);
            this.ultraPanel19.Location = new System.Drawing.Point(746, 722);
            this.ultraPanel19.Name = "ultraPanel19";
            this.ultraPanel19.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel19.TabIndex = 244;
            // 
            // txtItemPL16
            // 
            appearance326.FontData.BoldAsString = "True";
            appearance326.TextHAlignAsString = "Right";
            this.txtItemPL16.Appearance = appearance326;
            this.txtItemPL16.AutoSize = false;
            this.txtItemPL16.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL16.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL16.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL16.Location = new System.Drawing.Point(0, 0);
            this.txtItemPL16.Name = "txtItemPL16";
            this.txtItemPL16.NullText = "0";
            this.txtItemPL16.PromptChar = ' ';
            this.txtItemPL16.Size = new System.Drawing.Size(225, 30);
            this.txtItemPL16.TabIndex = 7;
            this.txtItemPL16.ValueChanged += new System.EventHandler(this.txtItemPL16_ValueChanged);
            this.txtItemPL16.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // ultraPanel18
            // 
            appearance327.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance327.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance327.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel18.Appearance = appearance327;
            this.ultraPanel18.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel18.ClientArea
            // 
            this.ultraPanel18.ClientArea.Controls.Add(this.txtItemPL15);
            this.ultraPanel18.Location = new System.Drawing.Point(746, 691);
            this.ultraPanel18.Name = "ultraPanel18";
            this.ultraPanel18.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel18.TabIndex = 243;
            // 
            // txtItemPL15
            // 
            appearance328.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance328.FontData.BoldAsString = "True";
            appearance328.TextHAlignAsString = "Right";
            this.txtItemPL15.Appearance = appearance328;
            this.txtItemPL15.AutoSize = false;
            this.txtItemPL15.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL15.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL15.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL15.Location = new System.Drawing.Point(0, 0);
            this.txtItemPL15.Name = "txtItemPL15";
            this.txtItemPL15.NullText = "0";
            this.txtItemPL15.PromptChar = ' ';
            this.txtItemPL15.ReadOnly = true;
            this.txtItemPL15.Size = new System.Drawing.Size(225, 30);
            this.txtItemPL15.TabIndex = 7;
            this.txtItemPL15.ValueChanged += new System.EventHandler(this.txtItemPL15_ValueChanged);
            // 
            // ultraPanel17
            // 
            appearance329.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance329.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance329.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel17.Appearance = appearance329;
            this.ultraPanel17.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel17.ClientArea
            // 
            this.ultraPanel17.ClientArea.Controls.Add(this.txtItemPL14);
            this.ultraPanel17.Location = new System.Drawing.Point(746, 660);
            this.ultraPanel17.Name = "ultraPanel17";
            this.ultraPanel17.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel17.TabIndex = 242;
            // 
            // txtItemPL14
            // 
            appearance330.TextHAlignAsString = "Right";
            this.txtItemPL14.Appearance = appearance330;
            this.txtItemPL14.AutoSize = false;
            this.txtItemPL14.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL14.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL14.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL14.Location = new System.Drawing.Point(0, 0);
            this.txtItemPL14.Name = "txtItemPL14";
            this.txtItemPL14.NullText = "0";
            this.txtItemPL14.PromptChar = ' ';
            this.txtItemPL14.Size = new System.Drawing.Size(225, 30);
            this.txtItemPL14.TabIndex = 7;
            this.txtItemPL14.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // ultraPanel16
            // 
            appearance331.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance331.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance331.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel16.Appearance = appearance331;
            this.ultraPanel16.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel16.ClientArea
            // 
            this.ultraPanel16.ClientArea.Controls.Add(this.txtItemPL13);
            this.ultraPanel16.Location = new System.Drawing.Point(746, 629);
            this.ultraPanel16.Name = "ultraPanel16";
            this.ultraPanel16.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel16.TabIndex = 241;
            // 
            // txtItemPL13
            // 
            appearance332.FontData.BoldAsString = "True";
            appearance332.TextHAlignAsString = "Right";
            this.txtItemPL13.Appearance = appearance332;
            this.txtItemPL13.AutoSize = false;
            this.txtItemPL13.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL13.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL13.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL13.Location = new System.Drawing.Point(0, 0);
            this.txtItemPL13.Name = "txtItemPL13";
            this.txtItemPL13.NullText = "0";
            this.txtItemPL13.PromptChar = ' ';
            this.txtItemPL13.Size = new System.Drawing.Size(225, 30);
            this.txtItemPL13.TabIndex = 7;
            this.txtItemPL13.ValueChanged += new System.EventHandler(this.txtItemPL1_ValueChanged);
            this.txtItemPL13.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // ultraPanel15
            // 
            appearance333.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance333.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance333.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel15.Appearance = appearance333;
            this.ultraPanel15.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel15.ClientArea
            // 
            this.ultraPanel15.ClientArea.Controls.Add(this.txtItemPL12);
            this.ultraPanel15.Location = new System.Drawing.Point(746, 598);
            this.ultraPanel15.Name = "ultraPanel15";
            this.ultraPanel15.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel15.TabIndex = 240;
            // 
            // txtItemPL12
            // 
            appearance334.TextHAlignAsString = "Right";
            this.txtItemPL12.Appearance = appearance334;
            this.txtItemPL12.AutoSize = false;
            this.txtItemPL12.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL12.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL12.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL12.Location = new System.Drawing.Point(0, 0);
            this.txtItemPL12.Name = "txtItemPL12";
            this.txtItemPL12.NullText = "0";
            this.txtItemPL12.PromptChar = ' ';
            this.txtItemPL12.Size = new System.Drawing.Size(225, 30);
            this.txtItemPL12.TabIndex = 7;
            this.txtItemPL12.ValueChanged += new System.EventHandler(this.txtItemPL10_ValueChanged);
            this.txtItemPL12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // ultraPanel14
            // 
            appearance335.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance335.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance335.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel14.Appearance = appearance335;
            this.ultraPanel14.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel14.ClientArea
            // 
            this.ultraPanel14.ClientArea.Controls.Add(this.txtItemPL11);
            this.ultraPanel14.Location = new System.Drawing.Point(746, 567);
            this.ultraPanel14.Name = "ultraPanel14";
            this.ultraPanel14.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel14.TabIndex = 239;
            // 
            // txtItemPL11
            // 
            appearance336.TextHAlignAsString = "Right";
            this.txtItemPL11.Appearance = appearance336;
            this.txtItemPL11.AutoSize = false;
            this.txtItemPL11.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL11.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL11.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL11.Location = new System.Drawing.Point(0, 0);
            this.txtItemPL11.Name = "txtItemPL11";
            this.txtItemPL11.NullText = "0";
            this.txtItemPL11.PromptChar = ' ';
            this.txtItemPL11.Size = new System.Drawing.Size(225, 30);
            this.txtItemPL11.TabIndex = 7;
            this.txtItemPL11.ValueChanged += new System.EventHandler(this.txtItemPL10_ValueChanged);
            this.txtItemPL11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // ultraPanel13
            // 
            appearance337.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance337.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance337.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel13.Appearance = appearance337;
            this.ultraPanel13.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel13.ClientArea
            // 
            this.ultraPanel13.ClientArea.Controls.Add(this.txtItemPL10);
            this.ultraPanel13.Location = new System.Drawing.Point(746, 536);
            this.ultraPanel13.Name = "ultraPanel13";
            this.ultraPanel13.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel13.TabIndex = 238;
            // 
            // txtItemPL10
            // 
            appearance338.TextHAlignAsString = "Right";
            this.txtItemPL10.Appearance = appearance338;
            this.txtItemPL10.AutoSize = false;
            this.txtItemPL10.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL10.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL10.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL10.Location = new System.Drawing.Point(0, 0);
            this.txtItemPL10.Name = "txtItemPL10";
            this.txtItemPL10.NullText = "0";
            this.txtItemPL10.PromptChar = ' ';
            this.txtItemPL10.Size = new System.Drawing.Size(225, 30);
            this.txtItemPL10.TabIndex = 7;
            this.txtItemPL10.ValueChanged += new System.EventHandler(this.txtItemPL10_ValueChanged);
            this.txtItemPL10.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // ultraPanel12
            // 
            appearance339.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance339.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance339.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel12.Appearance = appearance339;
            this.ultraPanel12.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel12.ClientArea
            // 
            this.ultraPanel12.ClientArea.Controls.Add(this.txtItemPL9);
            this.ultraPanel12.Location = new System.Drawing.Point(746, 505);
            this.ultraPanel12.Name = "ultraPanel12";
            this.ultraPanel12.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel12.TabIndex = 237;
            // 
            // txtItemPL9
            // 
            appearance340.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance340.FontData.BoldAsString = "True";
            appearance340.TextHAlignAsString = "Right";
            this.txtItemPL9.Appearance = appearance340;
            this.txtItemPL9.AutoSize = false;
            this.txtItemPL9.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL9.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL9.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL9.Location = new System.Drawing.Point(0, 0);
            this.txtItemPL9.Name = "txtItemPL9";
            this.txtItemPL9.NullText = "0";
            this.txtItemPL9.PromptChar = ' ';
            this.txtItemPL9.ReadOnly = true;
            this.txtItemPL9.Size = new System.Drawing.Size(225, 30);
            this.txtItemPL9.TabIndex = 7;
            this.txtItemPL9.ValueChanged += new System.EventHandler(this.txtItemPL1_ValueChanged);
            // 
            // ultraPanel11
            // 
            appearance341.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance341.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance341.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel11.Appearance = appearance341;
            this.ultraPanel11.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel11.ClientArea
            // 
            this.ultraPanel11.ClientArea.Controls.Add(this.txtItemPL8);
            this.ultraPanel11.Location = new System.Drawing.Point(746, 474);
            this.ultraPanel11.Name = "ultraPanel11";
            this.ultraPanel11.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel11.TabIndex = 236;
            // 
            // txtItemPL8
            // 
            appearance342.FontData.BoldAsString = "True";
            appearance342.TextHAlignAsString = "Right";
            this.txtItemPL8.Appearance = appearance342;
            this.txtItemPL8.AutoSize = false;
            this.txtItemPL8.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL8.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL8.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL8.Location = new System.Drawing.Point(0, 0);
            this.txtItemPL8.Name = "txtItemPL8";
            this.txtItemPL8.NullText = "0";
            this.txtItemPL8.PromptChar = ' ';
            this.txtItemPL8.Size = new System.Drawing.Size(225, 30);
            this.txtItemPL8.TabIndex = 7;
            this.txtItemPL8.ValueChanged += new System.EventHandler(this.txtItemPL1_ValueChanged);
            this.txtItemPL8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // ultraPanel10
            // 
            appearance343.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance343.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance343.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel10.Appearance = appearance343;
            this.ultraPanel10.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel10.ClientArea
            // 
            this.ultraPanel10.ClientArea.Controls.Add(this.txtItemPL7);
            this.ultraPanel10.Location = new System.Drawing.Point(746, 443);
            this.ultraPanel10.Name = "ultraPanel10";
            this.ultraPanel10.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel10.TabIndex = 235;
            // 
            // txtItemPL7
            // 
            appearance344.TextHAlignAsString = "Right";
            this.txtItemPL7.Appearance = appearance344;
            this.txtItemPL7.AutoSize = false;
            this.txtItemPL7.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL7.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL7.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL7.Location = new System.Drawing.Point(0, 0);
            this.txtItemPL7.Name = "txtItemPL7";
            this.txtItemPL7.NullText = "0";
            this.txtItemPL7.PromptChar = ' ';
            this.txtItemPL7.Size = new System.Drawing.Size(225, 30);
            this.txtItemPL7.TabIndex = 7;
            this.txtItemPL7.ValueChanged += new System.EventHandler(this.txtItemPL4_ValueChanged);
            this.txtItemPL7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // ultraPanel9
            // 
            appearance345.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance345.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance345.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel9.Appearance = appearance345;
            this.ultraPanel9.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel9.ClientArea
            // 
            this.ultraPanel9.ClientArea.Controls.Add(this.txtItemPL6);
            this.ultraPanel9.Location = new System.Drawing.Point(746, 412);
            this.ultraPanel9.Name = "ultraPanel9";
            this.ultraPanel9.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel9.TabIndex = 234;
            // 
            // txtItemPL6
            // 
            appearance346.TextHAlignAsString = "Right";
            this.txtItemPL6.Appearance = appearance346;
            this.txtItemPL6.AutoSize = false;
            this.txtItemPL6.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL6.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL6.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL6.Location = new System.Drawing.Point(0, 0);
            this.txtItemPL6.Name = "txtItemPL6";
            this.txtItemPL6.NullText = "0";
            this.txtItemPL6.PromptChar = ' ';
            this.txtItemPL6.Size = new System.Drawing.Size(225, 30);
            this.txtItemPL6.TabIndex = 7;
            this.txtItemPL6.ValueChanged += new System.EventHandler(this.txtItemPL4_ValueChanged);
            this.txtItemPL6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // ultraPanel8
            // 
            appearance347.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance347.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance347.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel8.Appearance = appearance347;
            this.ultraPanel8.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel8.ClientArea
            // 
            this.ultraPanel8.ClientArea.Controls.Add(this.txtItemPL5);
            this.ultraPanel8.Location = new System.Drawing.Point(746, 381);
            this.ultraPanel8.Name = "ultraPanel8";
            this.ultraPanel8.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel8.TabIndex = 233;
            // 
            // txtItemPL5
            // 
            appearance348.TextHAlignAsString = "Right";
            this.txtItemPL5.Appearance = appearance348;
            this.txtItemPL5.AutoSize = false;
            this.txtItemPL5.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL5.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL5.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL5.Location = new System.Drawing.Point(0, 0);
            this.txtItemPL5.Name = "txtItemPL5";
            this.txtItemPL5.NullText = "0";
            this.txtItemPL5.PromptChar = ' ';
            this.txtItemPL5.Size = new System.Drawing.Size(225, 30);
            this.txtItemPL5.TabIndex = 7;
            this.txtItemPL5.ValueChanged += new System.EventHandler(this.txtItemPL4_ValueChanged);
            this.txtItemPL5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // ultraPanel7
            // 
            appearance349.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance349.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance349.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel7.Appearance = appearance349;
            this.ultraPanel7.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel7.ClientArea
            // 
            this.ultraPanel7.ClientArea.Controls.Add(this.txtItemPL4);
            this.ultraPanel7.Location = new System.Drawing.Point(746, 350);
            this.ultraPanel7.Name = "ultraPanel7";
            this.ultraPanel7.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel7.TabIndex = 232;
            // 
            // txtItemPL4
            // 
            appearance350.TextHAlignAsString = "Right";
            this.txtItemPL4.Appearance = appearance350;
            this.txtItemPL4.AutoSize = false;
            this.txtItemPL4.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL4.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL4.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL4.Location = new System.Drawing.Point(0, 0);
            this.txtItemPL4.Name = "txtItemPL4";
            this.txtItemPL4.NullText = "0";
            this.txtItemPL4.PromptChar = ' ';
            this.txtItemPL4.Size = new System.Drawing.Size(225, 30);
            this.txtItemPL4.TabIndex = 7;
            this.txtItemPL4.ValueChanged += new System.EventHandler(this.txtItemPL4_ValueChanged);
            this.txtItemPL4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // ultraPanel6
            // 
            appearance351.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance351.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance351.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel6.Appearance = appearance351;
            this.ultraPanel6.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel6.ClientArea
            // 
            this.ultraPanel6.ClientArea.Controls.Add(this.txtItemPL3);
            this.ultraPanel6.Location = new System.Drawing.Point(746, 319);
            this.ultraPanel6.Name = "ultraPanel6";
            this.ultraPanel6.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel6.TabIndex = 231;
            // 
            // txtItemPL3
            // 
            appearance352.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance352.FontData.BoldAsString = "True";
            appearance352.TextHAlignAsString = "Right";
            this.txtItemPL3.Appearance = appearance352;
            this.txtItemPL3.AutoSize = false;
            this.txtItemPL3.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL3.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL3.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL3.Location = new System.Drawing.Point(0, 0);
            this.txtItemPL3.Name = "txtItemPL3";
            this.txtItemPL3.NullText = "0";
            this.txtItemPL3.PromptChar = ' ';
            this.txtItemPL3.ReadOnly = true;
            this.txtItemPL3.Size = new System.Drawing.Size(225, 30);
            this.txtItemPL3.TabIndex = 7;
            this.txtItemPL3.ValueChanged += new System.EventHandler(this.txtItemPL1_ValueChanged);
            // 
            // ultraPanel5
            // 
            appearance353.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance353.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance353.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel5.Appearance = appearance353;
            this.ultraPanel5.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel5.ClientArea
            // 
            this.ultraPanel5.ClientArea.Controls.Add(this.txtItemPL2);
            this.ultraPanel5.Location = new System.Drawing.Point(746, 288);
            this.ultraPanel5.Name = "ultraPanel5";
            this.ultraPanel5.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel5.TabIndex = 230;
            // 
            // txtItemPL2
            // 
            appearance354.TextHAlignAsString = "Right";
            this.txtItemPL2.Appearance = appearance354;
            this.txtItemPL2.AutoSize = false;
            this.txtItemPL2.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2.Location = new System.Drawing.Point(0, 0);
            this.txtItemPL2.Name = "txtItemPL2";
            this.txtItemPL2.NullText = "0";
            this.txtItemPL2.PromptChar = ' ';
            this.txtItemPL2.Size = new System.Drawing.Size(225, 30);
            this.txtItemPL2.TabIndex = 7;
            this.txtItemPL2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // ultraPanel4
            // 
            appearance355.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance355.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance355.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel4.Appearance = appearance355;
            this.ultraPanel4.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel4.ClientArea
            // 
            this.ultraPanel4.ClientArea.Controls.Add(this.txtItemPL1);
            this.ultraPanel4.Location = new System.Drawing.Point(746, 257);
            this.ultraPanel4.Name = "ultraPanel4";
            this.ultraPanel4.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel4.TabIndex = 229;
            // 
            // txtItemPL1
            // 
            appearance356.FontData.BoldAsString = "True";
            appearance356.TextHAlignAsString = "Right";
            this.txtItemPL1.Appearance = appearance356;
            this.txtItemPL1.AutoSize = false;
            this.txtItemPL1.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL1.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL1.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL1.Location = new System.Drawing.Point(0, 0);
            this.txtItemPL1.Name = "txtItemPL1";
            this.txtItemPL1.NullText = "0";
            this.txtItemPL1.PromptChar = ' ';
            this.txtItemPL1.Size = new System.Drawing.Size(225, 30);
            this.txtItemPL1.TabIndex = 7;
            this.txtItemPL1.ValueChanged += new System.EventHandler(this.txtItemPL1_ValueChanged);
            this.txtItemPL1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // ultraPanel3
            // 
            appearance357.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance357.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance357.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel3.Appearance = appearance357;
            this.ultraPanel3.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraPanel3.Location = new System.Drawing.Point(746, 226);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(227, 32);
            this.ultraPanel3.TabIndex = 228;
            // 
            // ultraLabel115
            // 
            appearance358.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance358.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance358.BorderColor = System.Drawing.Color.Black;
            appearance358.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance358.TextHAlignAsString = "Center";
            appearance358.TextVAlignAsString = "Middle";
            this.ultraLabel115.Appearance = appearance358;
            this.ultraLabel115.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel115.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel115.Location = new System.Drawing.Point(590, 815);
            this.ultraLabel115.Name = "ultraLabel115";
            this.ultraLabel115.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel115.TabIndex = 227;
            this.ultraLabel115.Text = "[19]";
            // 
            // txtNamePL19
            // 
            appearance359.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance359.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance359.BorderColor = System.Drawing.Color.Black;
            appearance359.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance359.TextHAlignAsString = "Left";
            appearance359.TextVAlignAsString = "Middle";
            this.txtNamePL19.Appearance = appearance359;
            this.txtNamePL19.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNamePL19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamePL19.Location = new System.Drawing.Point(141, 815);
            this.txtNamePL19.Name = "txtNamePL19";
            this.txtNamePL19.Size = new System.Drawing.Size(450, 32);
            this.txtNamePL19.TabIndex = 226;
            this.txtNamePL19.Text = "Tổng lợi nhuận kế toán trước thuế thu nhập doanh nghiệp ([19] = [15] + [18])";
            // 
            // ultraLabel117
            // 
            appearance360.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance360.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance360.BorderColor = System.Drawing.Color.Black;
            appearance360.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance360.TextHAlignAsString = "Center";
            appearance360.TextVAlignAsString = "Middle";
            this.ultraLabel117.Appearance = appearance360;
            this.ultraLabel117.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel117.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel117.Location = new System.Drawing.Point(103, 815);
            this.ultraLabel117.Name = "ultraLabel117";
            this.ultraLabel117.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel117.TabIndex = 225;
            this.ultraLabel117.Text = "10";
            // 
            // ultraLabel111
            // 
            appearance361.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance361.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance361.BorderColor = System.Drawing.Color.Black;
            appearance361.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance361.TextHAlignAsString = "Center";
            appearance361.TextVAlignAsString = "Middle";
            this.ultraLabel111.Appearance = appearance361;
            this.ultraLabel111.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel111.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel111.Location = new System.Drawing.Point(590, 784);
            this.ultraLabel111.Name = "ultraLabel111";
            this.ultraLabel111.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel111.TabIndex = 223;
            this.ultraLabel111.Text = "[18]";
            // 
            // txtNamePL18
            // 
            appearance362.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance362.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance362.BorderColor = System.Drawing.Color.Black;
            appearance362.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance362.TextHAlignAsString = "Left";
            appearance362.TextVAlignAsString = "Middle";
            this.txtNamePL18.Appearance = appearance362;
            this.txtNamePL18.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNamePL18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamePL18.Location = new System.Drawing.Point(141, 784);
            this.txtNamePL18.Name = "txtNamePL18";
            this.txtNamePL18.Size = new System.Drawing.Size(450, 32);
            this.txtNamePL18.TabIndex = 222;
            this.txtNamePL18.Text = "Lợi nhuận khác ([18] = [16] - [17])";
            // 
            // ultraLabel113
            // 
            appearance363.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance363.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance363.BorderColor = System.Drawing.Color.Black;
            appearance363.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance363.TextHAlignAsString = "Center";
            appearance363.TextVAlignAsString = "Middle";
            this.ultraLabel113.Appearance = appearance363;
            this.ultraLabel113.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel113.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel113.Location = new System.Drawing.Point(103, 784);
            this.ultraLabel113.Name = "ultraLabel113";
            this.ultraLabel113.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel113.TabIndex = 221;
            this.ultraLabel113.Text = "9";
            // 
            // ultraLabel107
            // 
            appearance364.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance364.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance364.BorderColor = System.Drawing.Color.Black;
            appearance364.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance364.TextHAlignAsString = "Center";
            appearance364.TextVAlignAsString = "Middle";
            this.ultraLabel107.Appearance = appearance364;
            this.ultraLabel107.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel107.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel107.Location = new System.Drawing.Point(590, 753);
            this.ultraLabel107.Name = "ultraLabel107";
            this.ultraLabel107.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel107.TabIndex = 219;
            this.ultraLabel107.Text = "[17]";
            // 
            // txtNamePL17
            // 
            appearance365.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance365.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance365.BorderColor = System.Drawing.Color.Black;
            appearance365.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance365.TextHAlignAsString = "Left";
            appearance365.TextVAlignAsString = "Middle";
            this.txtNamePL17.Appearance = appearance365;
            this.txtNamePL17.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNamePL17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamePL17.Location = new System.Drawing.Point(141, 753);
            this.txtNamePL17.Name = "txtNamePL17";
            this.txtNamePL17.Size = new System.Drawing.Size(450, 32);
            this.txtNamePL17.TabIndex = 218;
            this.txtNamePL17.Text = "Chi phí khác";
            // 
            // ultraLabel109
            // 
            appearance366.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance366.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance366.BorderColor = System.Drawing.Color.Black;
            appearance366.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance366.TextHAlignAsString = "Center";
            appearance366.TextVAlignAsString = "Middle";
            this.ultraLabel109.Appearance = appearance366;
            this.ultraLabel109.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel109.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel109.Location = new System.Drawing.Point(103, 753);
            this.ultraLabel109.Name = "ultraLabel109";
            this.ultraLabel109.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel109.TabIndex = 217;
            this.ultraLabel109.Text = "8";
            // 
            // ultraLabel103
            // 
            appearance367.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance367.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance367.BorderColor = System.Drawing.Color.Black;
            appearance367.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance367.TextHAlignAsString = "Center";
            appearance367.TextVAlignAsString = "Middle";
            this.ultraLabel103.Appearance = appearance367;
            this.ultraLabel103.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel103.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel103.Location = new System.Drawing.Point(590, 722);
            this.ultraLabel103.Name = "ultraLabel103";
            this.ultraLabel103.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel103.TabIndex = 215;
            this.ultraLabel103.Text = "[16]";
            // 
            // txtNamePL16
            // 
            appearance368.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance368.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance368.BorderColor = System.Drawing.Color.Black;
            appearance368.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance368.TextHAlignAsString = "Left";
            appearance368.TextVAlignAsString = "Middle";
            this.txtNamePL16.Appearance = appearance368;
            this.txtNamePL16.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNamePL16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamePL16.Location = new System.Drawing.Point(141, 722);
            this.txtNamePL16.Name = "txtNamePL16";
            this.txtNamePL16.Size = new System.Drawing.Size(450, 32);
            this.txtNamePL16.TabIndex = 214;
            this.txtNamePL16.Text = "Thu nhập khác";
            // 
            // ultraLabel105
            // 
            appearance369.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance369.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance369.BorderColor = System.Drawing.Color.Black;
            appearance369.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance369.TextHAlignAsString = "Center";
            appearance369.TextVAlignAsString = "Middle";
            this.ultraLabel105.Appearance = appearance369;
            this.ultraLabel105.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel105.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel105.Location = new System.Drawing.Point(103, 722);
            this.ultraLabel105.Name = "ultraLabel105";
            this.ultraLabel105.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel105.TabIndex = 213;
            this.ultraLabel105.Text = "7";
            // 
            // ultraLabel95
            // 
            appearance370.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance370.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance370.BorderColor = System.Drawing.Color.Black;
            appearance370.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance370.TextHAlignAsString = "Center";
            appearance370.TextVAlignAsString = "Middle";
            this.ultraLabel95.Appearance = appearance370;
            this.ultraLabel95.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel95.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel95.Location = new System.Drawing.Point(590, 691);
            this.ultraLabel95.Name = "ultraLabel95";
            this.ultraLabel95.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel95.TabIndex = 207;
            this.ultraLabel95.Text = "[15]";
            // 
            // txtNamePL15
            // 
            appearance371.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance371.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance371.BorderColor = System.Drawing.Color.Black;
            appearance371.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance371.TextHAlignAsString = "Left";
            appearance371.TextVAlignAsString = "Middle";
            this.txtNamePL15.Appearance = appearance371;
            this.txtNamePL15.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNamePL15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamePL15.Location = new System.Drawing.Point(141, 691);
            this.txtNamePL15.Name = "txtNamePL15";
            this.txtNamePL15.Size = new System.Drawing.Size(450, 32);
            this.txtNamePL15.TabIndex = 206;
            this.txtNamePL15.Text = "Lợi nhuận thuần từ hoạt động kinh doanh ([15] = [1] - [3] + [8] - [9] - [13])";
            // 
            // ultraLabel97
            // 
            appearance372.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance372.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance372.BorderColor = System.Drawing.Color.Black;
            appearance372.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance372.TextHAlignAsString = "Center";
            appearance372.TextVAlignAsString = "Middle";
            this.ultraLabel97.Appearance = appearance372;
            this.ultraLabel97.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel97.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel97.Location = new System.Drawing.Point(103, 691);
            this.ultraLabel97.Name = "ultraLabel97";
            this.ultraLabel97.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel97.TabIndex = 205;
            this.ultraLabel97.Text = "6";
            // 
            // ultraLabel91
            // 
            appearance373.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance373.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance373.BorderColor = System.Drawing.Color.Black;
            appearance373.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance373.TextHAlignAsString = "Center";
            appearance373.TextVAlignAsString = "Middle";
            this.ultraLabel91.Appearance = appearance373;
            this.ultraLabel91.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel91.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel91.Location = new System.Drawing.Point(590, 660);
            this.ultraLabel91.Name = "ultraLabel91";
            this.ultraLabel91.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel91.TabIndex = 203;
            this.ultraLabel91.Text = "[14]";
            // 
            // txtNamePL14
            // 
            appearance374.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance374.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance374.BorderColor = System.Drawing.Color.Black;
            appearance374.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance374.TextHAlignAsString = "Left";
            appearance374.TextVAlignAsString = "Middle";
            this.txtNamePL14.Appearance = appearance374;
            this.txtNamePL14.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNamePL14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamePL14.Location = new System.Drawing.Point(141, 660);
            this.txtNamePL14.Name = "txtNamePL14";
            this.txtNamePL14.Size = new System.Drawing.Size(450, 32);
            this.txtNamePL14.TabIndex = 202;
            this.txtNamePL14.Text = "Trong đó: Chi phí lãi tiền vay dùng cho sản xuất, kinh doanh";
            // 
            // ultraLabel93
            // 
            appearance375.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance375.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance375.BorderColor = System.Drawing.Color.Black;
            appearance375.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance375.TextHAlignAsString = "Center";
            appearance375.TextVAlignAsString = "Middle";
            this.ultraLabel93.Appearance = appearance375;
            this.ultraLabel93.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel93.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel93.Location = new System.Drawing.Point(103, 660);
            this.ultraLabel93.Name = "ultraLabel93";
            this.ultraLabel93.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel93.TabIndex = 201;
            // 
            // ultraLabel87
            // 
            appearance376.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance376.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance376.BorderColor = System.Drawing.Color.Black;
            appearance376.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance376.TextHAlignAsString = "Center";
            appearance376.TextVAlignAsString = "Middle";
            this.ultraLabel87.Appearance = appearance376;
            this.ultraLabel87.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel87.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel87.Location = new System.Drawing.Point(590, 629);
            this.ultraLabel87.Name = "ultraLabel87";
            this.ultraLabel87.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel87.TabIndex = 199;
            this.ultraLabel87.Text = "[13]";
            // 
            // txtNamePL13
            // 
            appearance377.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance377.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance377.BorderColor = System.Drawing.Color.Black;
            appearance377.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance377.TextHAlignAsString = "Left";
            appearance377.TextVAlignAsString = "Middle";
            this.txtNamePL13.Appearance = appearance377;
            this.txtNamePL13.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNamePL13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamePL13.Location = new System.Drawing.Point(141, 629);
            this.txtNamePL13.Name = "txtNamePL13";
            this.txtNamePL13.Size = new System.Drawing.Size(450, 32);
            this.txtNamePL13.TabIndex = 198;
            this.txtNamePL13.Text = "Chi phí tài chính";
            // 
            // ultraLabel89
            // 
            appearance378.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance378.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance378.BorderColor = System.Drawing.Color.Black;
            appearance378.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance378.TextHAlignAsString = "Center";
            appearance378.TextVAlignAsString = "Middle";
            this.ultraLabel89.Appearance = appearance378;
            this.ultraLabel89.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel89.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel89.Location = new System.Drawing.Point(103, 629);
            this.ultraLabel89.Name = "ultraLabel89";
            this.ultraLabel89.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel89.TabIndex = 197;
            this.ultraLabel89.Text = "5";
            // 
            // ultraLabel83
            // 
            appearance379.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance379.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance379.BorderColor = System.Drawing.Color.Black;
            appearance379.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance379.TextHAlignAsString = "Center";
            appearance379.TextVAlignAsString = "Middle";
            this.ultraLabel83.Appearance = appearance379;
            this.ultraLabel83.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel83.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel83.Location = new System.Drawing.Point(590, 598);
            this.ultraLabel83.Name = "ultraLabel83";
            this.ultraLabel83.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel83.TabIndex = 195;
            this.ultraLabel83.Text = "[12]";
            // 
            // txtNamePL12
            // 
            appearance380.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance380.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance380.BorderColor = System.Drawing.Color.Black;
            appearance380.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance380.TextHAlignAsString = "Left";
            appearance380.TextVAlignAsString = "Middle";
            this.txtNamePL12.Appearance = appearance380;
            this.txtNamePL12.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNamePL12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamePL12.Location = new System.Drawing.Point(141, 598);
            this.txtNamePL12.Name = "txtNamePL12";
            this.txtNamePL12.Size = new System.Drawing.Size(450, 32);
            this.txtNamePL12.TabIndex = 194;
            this.txtNamePL12.Text = "Chi phí quản lý doanh nghiệp";
            // 
            // ultraLabel85
            // 
            appearance381.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance381.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance381.BorderColor = System.Drawing.Color.Black;
            appearance381.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance381.TextHAlignAsString = "Center";
            appearance381.TextVAlignAsString = "Middle";
            this.ultraLabel85.Appearance = appearance381;
            this.ultraLabel85.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel85.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel85.Location = new System.Drawing.Point(103, 598);
            this.ultraLabel85.Name = "ultraLabel85";
            this.ultraLabel85.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel85.TabIndex = 193;
            this.ultraLabel85.Text = "c";
            // 
            // ultraLabel79
            // 
            appearance382.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance382.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance382.BorderColor = System.Drawing.Color.Black;
            appearance382.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance382.TextHAlignAsString = "Center";
            appearance382.TextVAlignAsString = "Middle";
            this.ultraLabel79.Appearance = appearance382;
            this.ultraLabel79.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel79.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel79.Location = new System.Drawing.Point(590, 567);
            this.ultraLabel79.Name = "ultraLabel79";
            this.ultraLabel79.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel79.TabIndex = 191;
            this.ultraLabel79.Text = "[11]";
            // 
            // txtNamePL11
            // 
            appearance383.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance383.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance383.BorderColor = System.Drawing.Color.Black;
            appearance383.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance383.TextHAlignAsString = "Left";
            appearance383.TextVAlignAsString = "Middle";
            this.txtNamePL11.Appearance = appearance383;
            this.txtNamePL11.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNamePL11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamePL11.Location = new System.Drawing.Point(141, 567);
            this.txtNamePL11.Name = "txtNamePL11";
            this.txtNamePL11.Size = new System.Drawing.Size(450, 32);
            this.txtNamePL11.TabIndex = 190;
            this.txtNamePL11.Text = "Chi phí hàng bán";
            // 
            // ultraLabel81
            // 
            appearance384.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance384.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance384.BorderColor = System.Drawing.Color.Black;
            appearance384.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance384.TextHAlignAsString = "Center";
            appearance384.TextVAlignAsString = "Middle";
            this.ultraLabel81.Appearance = appearance384;
            this.ultraLabel81.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel81.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel81.Location = new System.Drawing.Point(103, 567);
            this.ultraLabel81.Name = "ultraLabel81";
            this.ultraLabel81.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel81.TabIndex = 189;
            this.ultraLabel81.Text = "b";
            // 
            // ultraLabel75
            // 
            appearance385.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance385.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance385.BorderColor = System.Drawing.Color.Black;
            appearance385.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance385.TextHAlignAsString = "Center";
            appearance385.TextVAlignAsString = "Middle";
            this.ultraLabel75.Appearance = appearance385;
            this.ultraLabel75.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel75.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel75.Location = new System.Drawing.Point(590, 536);
            this.ultraLabel75.Name = "ultraLabel75";
            this.ultraLabel75.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel75.TabIndex = 187;
            this.ultraLabel75.Text = "[10]";
            // 
            // txtNamePL10
            // 
            appearance386.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance386.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance386.BorderColor = System.Drawing.Color.Black;
            appearance386.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance386.TextHAlignAsString = "Left";
            appearance386.TextVAlignAsString = "Middle";
            this.txtNamePL10.Appearance = appearance386;
            this.txtNamePL10.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNamePL10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamePL10.Location = new System.Drawing.Point(141, 536);
            this.txtNamePL10.Name = "txtNamePL10";
            this.txtNamePL10.Size = new System.Drawing.Size(450, 32);
            this.txtNamePL10.TabIndex = 186;
            this.txtNamePL10.Text = "Giá vốn hàng bán";
            // 
            // ultraLabel77
            // 
            appearance387.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance387.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance387.BorderColor = System.Drawing.Color.Black;
            appearance387.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance387.TextHAlignAsString = "Center";
            appearance387.TextVAlignAsString = "Middle";
            this.ultraLabel77.Appearance = appearance387;
            this.ultraLabel77.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel77.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel77.Location = new System.Drawing.Point(103, 536);
            this.ultraLabel77.Name = "ultraLabel77";
            this.ultraLabel77.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel77.TabIndex = 185;
            this.ultraLabel77.Text = "a";
            // 
            // ultraLabel71
            // 
            appearance388.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance388.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance388.BorderColor = System.Drawing.Color.Black;
            appearance388.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance388.TextHAlignAsString = "Center";
            appearance388.TextVAlignAsString = "Middle";
            this.ultraLabel71.Appearance = appearance388;
            this.ultraLabel71.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel71.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel71.Location = new System.Drawing.Point(590, 505);
            this.ultraLabel71.Name = "ultraLabel71";
            this.ultraLabel71.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel71.TabIndex = 183;
            this.ultraLabel71.Text = "[09]";
            // 
            // txtNamePL9
            // 
            appearance389.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance389.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance389.BorderColor = System.Drawing.Color.Black;
            appearance389.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance389.TextHAlignAsString = "Left";
            appearance389.TextVAlignAsString = "Middle";
            this.txtNamePL9.Appearance = appearance389;
            this.txtNamePL9.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNamePL9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamePL9.Location = new System.Drawing.Point(141, 505);
            this.txtNamePL9.Name = "txtNamePL9";
            this.txtNamePL9.Size = new System.Drawing.Size(450, 32);
            this.txtNamePL9.TabIndex = 182;
            this.txtNamePL9.Text = "Chi phí sản xuất, kinh doanh hàng hóa, dịch vụ ([9] = [10] + [11] + [12])";
            // 
            // ultraLabel73
            // 
            appearance390.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance390.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance390.BorderColor = System.Drawing.Color.Black;
            appearance390.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance390.TextHAlignAsString = "Center";
            appearance390.TextVAlignAsString = "Middle";
            this.ultraLabel73.Appearance = appearance390;
            this.ultraLabel73.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel73.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel73.Location = new System.Drawing.Point(103, 505);
            this.ultraLabel73.Name = "ultraLabel73";
            this.ultraLabel73.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel73.TabIndex = 181;
            this.ultraLabel73.Text = "4";
            // 
            // ultraLabel67
            // 
            appearance391.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance391.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance391.BorderColor = System.Drawing.Color.Black;
            appearance391.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance391.TextHAlignAsString = "Center";
            appearance391.TextVAlignAsString = "Middle";
            this.ultraLabel67.Appearance = appearance391;
            this.ultraLabel67.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel67.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel67.Location = new System.Drawing.Point(590, 474);
            this.ultraLabel67.Name = "ultraLabel67";
            this.ultraLabel67.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel67.TabIndex = 179;
            this.ultraLabel67.Text = "[08]";
            // 
            // txtNamePL8
            // 
            appearance392.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance392.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance392.BorderColor = System.Drawing.Color.Black;
            appearance392.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance392.TextHAlignAsString = "Left";
            appearance392.TextVAlignAsString = "Middle";
            this.txtNamePL8.Appearance = appearance392;
            this.txtNamePL8.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNamePL8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamePL8.Location = new System.Drawing.Point(141, 474);
            this.txtNamePL8.Name = "txtNamePL8";
            this.txtNamePL8.Size = new System.Drawing.Size(450, 32);
            this.txtNamePL8.TabIndex = 178;
            this.txtNamePL8.Text = "Doanh thu hoạt động tài chính";
            // 
            // ultraLabel69
            // 
            appearance393.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance393.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance393.BorderColor = System.Drawing.Color.Black;
            appearance393.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance393.TextHAlignAsString = "Center";
            appearance393.TextVAlignAsString = "Middle";
            this.ultraLabel69.Appearance = appearance393;
            this.ultraLabel69.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel69.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel69.Location = new System.Drawing.Point(103, 474);
            this.ultraLabel69.Name = "ultraLabel69";
            this.ultraLabel69.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel69.TabIndex = 177;
            this.ultraLabel69.Text = "3";
            // 
            // ultraLabel63
            // 
            appearance394.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance394.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance394.BorderColor = System.Drawing.Color.Black;
            appearance394.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance394.TextHAlignAsString = "Center";
            appearance394.TextVAlignAsString = "Middle";
            this.ultraLabel63.Appearance = appearance394;
            this.ultraLabel63.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel63.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel63.Location = new System.Drawing.Point(590, 443);
            this.ultraLabel63.Name = "ultraLabel63";
            this.ultraLabel63.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel63.TabIndex = 175;
            this.ultraLabel63.Text = "[07]";
            // 
            // txtNamePL7
            // 
            appearance395.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance395.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance395.BorderColor = System.Drawing.Color.Black;
            appearance395.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance395.TextHAlignAsString = "Left";
            appearance395.TextVAlignAsString = "Middle";
            this.txtNamePL7.Appearance = appearance395;
            this.txtNamePL7.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNamePL7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamePL7.Location = new System.Drawing.Point(141, 443);
            this.txtNamePL7.Name = "txtNamePL7";
            this.txtNamePL7.Size = new System.Drawing.Size(450, 32);
            this.txtNamePL7.TabIndex = 174;
            this.txtNamePL7.Text = "Thuế tiêu thụ đặc biệt, thuế xuất khẩu, thuế giá trị gia tăng theo phương pháp tr" +
    "ực tiếp phải nộp";
            // 
            // ultraLabel65
            // 
            appearance396.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance396.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance396.BorderColor = System.Drawing.Color.Black;
            appearance396.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance396.TextHAlignAsString = "Center";
            appearance396.TextVAlignAsString = "Middle";
            this.ultraLabel65.Appearance = appearance396;
            this.ultraLabel65.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel65.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel65.Location = new System.Drawing.Point(103, 443);
            this.ultraLabel65.Name = "ultraLabel65";
            this.ultraLabel65.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel65.TabIndex = 173;
            this.ultraLabel65.Text = "d";
            // 
            // ultraLabel59
            // 
            appearance397.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance397.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance397.BorderColor = System.Drawing.Color.Black;
            appearance397.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance397.TextHAlignAsString = "Center";
            appearance397.TextVAlignAsString = "Middle";
            this.ultraLabel59.Appearance = appearance397;
            this.ultraLabel59.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel59.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel59.Location = new System.Drawing.Point(590, 412);
            this.ultraLabel59.Name = "ultraLabel59";
            this.ultraLabel59.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel59.TabIndex = 171;
            this.ultraLabel59.Text = "[06]";
            // 
            // txtNamePL6
            // 
            appearance398.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance398.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance398.BorderColor = System.Drawing.Color.Black;
            appearance398.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance398.TextHAlignAsString = "Left";
            appearance398.TextVAlignAsString = "Middle";
            this.txtNamePL6.Appearance = appearance398;
            this.txtNamePL6.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNamePL6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamePL6.Location = new System.Drawing.Point(141, 412);
            this.txtNamePL6.Name = "txtNamePL6";
            this.txtNamePL6.Size = new System.Drawing.Size(450, 32);
            this.txtNamePL6.TabIndex = 170;
            this.txtNamePL6.Text = "Giá trị hàng bán bị trả lại";
            // 
            // ultraLabel61
            // 
            appearance399.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance399.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance399.BorderColor = System.Drawing.Color.Black;
            appearance399.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance399.TextHAlignAsString = "Center";
            appearance399.TextVAlignAsString = "Middle";
            this.ultraLabel61.Appearance = appearance399;
            this.ultraLabel61.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel61.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel61.Location = new System.Drawing.Point(103, 412);
            this.ultraLabel61.Name = "ultraLabel61";
            this.ultraLabel61.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel61.TabIndex = 169;
            this.ultraLabel61.Text = "c";
            // 
            // ultraLabel55
            // 
            appearance400.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance400.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance400.BorderColor = System.Drawing.Color.Black;
            appearance400.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance400.TextHAlignAsString = "Center";
            appearance400.TextVAlignAsString = "Middle";
            this.ultraLabel55.Appearance = appearance400;
            this.ultraLabel55.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel55.Location = new System.Drawing.Point(590, 381);
            this.ultraLabel55.Name = "ultraLabel55";
            this.ultraLabel55.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel55.TabIndex = 167;
            this.ultraLabel55.Text = "[05]";
            // 
            // txtNamePL5
            // 
            appearance401.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance401.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance401.BorderColor = System.Drawing.Color.Black;
            appearance401.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance401.TextHAlignAsString = "Left";
            appearance401.TextVAlignAsString = "Middle";
            this.txtNamePL5.Appearance = appearance401;
            this.txtNamePL5.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNamePL5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamePL5.Location = new System.Drawing.Point(141, 381);
            this.txtNamePL5.Name = "txtNamePL5";
            this.txtNamePL5.Size = new System.Drawing.Size(450, 32);
            this.txtNamePL5.TabIndex = 166;
            this.txtNamePL5.Text = "Giảm giá hàng bán";
            // 
            // ultraLabel57
            // 
            appearance402.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance402.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance402.BorderColor = System.Drawing.Color.Black;
            appearance402.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance402.TextHAlignAsString = "Center";
            appearance402.TextVAlignAsString = "Middle";
            this.ultraLabel57.Appearance = appearance402;
            this.ultraLabel57.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel57.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel57.Location = new System.Drawing.Point(103, 381);
            this.ultraLabel57.Name = "ultraLabel57";
            this.ultraLabel57.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel57.TabIndex = 165;
            this.ultraLabel57.Text = "b";
            // 
            // ultraLabel51
            // 
            appearance403.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance403.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance403.BorderColor = System.Drawing.Color.Black;
            appearance403.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance403.TextHAlignAsString = "Center";
            appearance403.TextVAlignAsString = "Middle";
            this.ultraLabel51.Appearance = appearance403;
            this.ultraLabel51.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel51.Location = new System.Drawing.Point(590, 350);
            this.ultraLabel51.Name = "ultraLabel51";
            this.ultraLabel51.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel51.TabIndex = 163;
            this.ultraLabel51.Text = "[04]";
            // 
            // txtNamePL4
            // 
            appearance404.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance404.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance404.BorderColor = System.Drawing.Color.Black;
            appearance404.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance404.TextHAlignAsString = "Left";
            appearance404.TextVAlignAsString = "Middle";
            this.txtNamePL4.Appearance = appearance404;
            this.txtNamePL4.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNamePL4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamePL4.Location = new System.Drawing.Point(141, 350);
            this.txtNamePL4.Name = "txtNamePL4";
            this.txtNamePL4.Size = new System.Drawing.Size(450, 32);
            this.txtNamePL4.TabIndex = 162;
            this.txtNamePL4.Text = "Chiết khấu thương mại";
            // 
            // ultraLabel53
            // 
            appearance405.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance405.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance405.BorderColor = System.Drawing.Color.Black;
            appearance405.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance405.TextHAlignAsString = "Center";
            appearance405.TextVAlignAsString = "Middle";
            this.ultraLabel53.Appearance = appearance405;
            this.ultraLabel53.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel53.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel53.Location = new System.Drawing.Point(103, 350);
            this.ultraLabel53.Name = "ultraLabel53";
            this.ultraLabel53.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel53.TabIndex = 161;
            this.ultraLabel53.Text = "a";
            // 
            // ultraLabel47
            // 
            appearance406.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance406.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance406.BorderColor = System.Drawing.Color.Black;
            appearance406.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance406.TextHAlignAsString = "Center";
            appearance406.TextVAlignAsString = "Middle";
            this.ultraLabel47.Appearance = appearance406;
            this.ultraLabel47.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel47.Location = new System.Drawing.Point(590, 319);
            this.ultraLabel47.Name = "ultraLabel47";
            this.ultraLabel47.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel47.TabIndex = 159;
            this.ultraLabel47.Text = "[03]";
            // 
            // txtNamePL3
            // 
            appearance407.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance407.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance407.BorderColor = System.Drawing.Color.Black;
            appearance407.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance407.TextHAlignAsString = "Left";
            appearance407.TextVAlignAsString = "Middle";
            this.txtNamePL3.Appearance = appearance407;
            this.txtNamePL3.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNamePL3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamePL3.Location = new System.Drawing.Point(141, 319);
            this.txtNamePL3.Name = "txtNamePL3";
            this.txtNamePL3.Size = new System.Drawing.Size(450, 32);
            this.txtNamePL3.TabIndex = 158;
            this.txtNamePL3.Text = "Các khoản giảm trừ doanh thu ([3] = [4] + [5] + [6] + [7])";
            // 
            // ultraLabel49
            // 
            appearance408.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance408.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance408.BorderColor = System.Drawing.Color.Black;
            appearance408.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance408.TextHAlignAsString = "Center";
            appearance408.TextVAlignAsString = "Middle";
            this.ultraLabel49.Appearance = appearance408;
            this.ultraLabel49.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel49.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel49.Location = new System.Drawing.Point(103, 319);
            this.ultraLabel49.Name = "ultraLabel49";
            this.ultraLabel49.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel49.TabIndex = 157;
            this.ultraLabel49.Text = "2";
            // 
            // ultraLabel43
            // 
            appearance409.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance409.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance409.BorderColor = System.Drawing.Color.Black;
            appearance409.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance409.TextHAlignAsString = "Center";
            appearance409.TextVAlignAsString = "Middle";
            this.ultraLabel43.Appearance = appearance409;
            this.ultraLabel43.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel43.Location = new System.Drawing.Point(590, 288);
            this.ultraLabel43.Name = "ultraLabel43";
            this.ultraLabel43.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel43.TabIndex = 155;
            this.ultraLabel43.Text = "[02]";
            // 
            // txtNamePL2
            // 
            appearance410.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance410.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance410.BorderColor = System.Drawing.Color.Black;
            appearance410.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance410.TextHAlignAsString = "Left";
            appearance410.TextVAlignAsString = "Middle";
            this.txtNamePL2.Appearance = appearance410;
            this.txtNamePL2.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNamePL2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamePL2.Location = new System.Drawing.Point(141, 288);
            this.txtNamePL2.Name = "txtNamePL2";
            this.txtNamePL2.Size = new System.Drawing.Size(450, 32);
            this.txtNamePL2.TabIndex = 154;
            this.txtNamePL2.Text = "Trong đó: - Doanh thu bán hàng hóa, dịch vụ xuất khẩu";
            // 
            // ultraLabel45
            // 
            appearance411.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance411.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance411.BorderColor = System.Drawing.Color.Black;
            appearance411.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance411.TextHAlignAsString = "Center";
            appearance411.TextVAlignAsString = "Middle";
            this.ultraLabel45.Appearance = appearance411;
            this.ultraLabel45.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel45.Location = new System.Drawing.Point(103, 288);
            this.ultraLabel45.Name = "ultraLabel45";
            this.ultraLabel45.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel45.TabIndex = 153;
            // 
            // ultraLabel39
            // 
            appearance412.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance412.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance412.BorderColor = System.Drawing.Color.Black;
            appearance412.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance412.TextHAlignAsString = "Center";
            appearance412.TextVAlignAsString = "Middle";
            this.ultraLabel39.Appearance = appearance412;
            this.ultraLabel39.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel39.Location = new System.Drawing.Point(590, 257);
            this.ultraLabel39.Name = "ultraLabel39";
            this.ultraLabel39.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel39.TabIndex = 151;
            this.ultraLabel39.Text = "[01]";
            // 
            // txtNamePL1
            // 
            appearance413.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance413.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance413.BorderColor = System.Drawing.Color.Black;
            appearance413.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance413.TextHAlignAsString = "Left";
            appearance413.TextVAlignAsString = "Middle";
            this.txtNamePL1.Appearance = appearance413;
            this.txtNamePL1.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNamePL1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamePL1.Location = new System.Drawing.Point(141, 257);
            this.txtNamePL1.Name = "txtNamePL1";
            this.txtNamePL1.Size = new System.Drawing.Size(450, 32);
            this.txtNamePL1.TabIndex = 150;
            this.txtNamePL1.Text = "Doanh thu bán hàng và cung cấp dịch vụ";
            // 
            // ultraLabel41
            // 
            appearance414.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance414.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance414.BorderColor = System.Drawing.Color.Black;
            appearance414.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance414.TextHAlignAsString = "Center";
            appearance414.TextVAlignAsString = "Middle";
            this.ultraLabel41.Appearance = appearance414;
            this.ultraLabel41.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel41.Location = new System.Drawing.Point(103, 257);
            this.ultraLabel41.Name = "ultraLabel41";
            this.ultraLabel41.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel41.TabIndex = 149;
            this.ultraLabel41.Text = "1";
            // 
            // ultraLabel36
            // 
            appearance415.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance415.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance415.BorderColor = System.Drawing.Color.Black;
            appearance415.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance415.TextHAlignAsString = "Left";
            appearance415.TextVAlignAsString = "Middle";
            this.ultraLabel36.Appearance = appearance415;
            this.ultraLabel36.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel36.Location = new System.Drawing.Point(590, 226);
            this.ultraLabel36.Name = "ultraLabel36";
            this.ultraLabel36.Size = new System.Drawing.Size(157, 32);
            this.ultraLabel36.TabIndex = 147;
            // 
            // ultraLabel34
            // 
            appearance416.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance416.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance416.BorderColor = System.Drawing.Color.Black;
            appearance416.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance416.TextHAlignAsString = "Left";
            appearance416.TextVAlignAsString = "Middle";
            this.ultraLabel34.Appearance = appearance416;
            this.ultraLabel34.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel34.Location = new System.Drawing.Point(141, 226);
            this.ultraLabel34.Name = "ultraLabel34";
            this.ultraLabel34.Size = new System.Drawing.Size(450, 32);
            this.ultraLabel34.TabIndex = 146;
            this.ultraLabel34.Text = "Kết quả kinh doanh ghi nhận theo báo cáo tài chính";
            // 
            // ultraLabel35
            // 
            appearance417.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance417.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance417.BorderColor = System.Drawing.Color.Black;
            appearance417.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance417.TextHAlignAsString = "Center";
            appearance417.TextVAlignAsString = "Middle";
            this.ultraLabel35.Appearance = appearance417;
            this.ultraLabel35.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel35.Location = new System.Drawing.Point(103, 226);
            this.ultraLabel35.Name = "ultraLabel35";
            this.ultraLabel35.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel35.TabIndex = 145;
            // 
            // ultraLabel33
            // 
            appearance418.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance418.TextHAlignAsString = "Left";
            this.ultraLabel33.Appearance = appearance418;
            this.ultraLabel33.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel33.Location = new System.Drawing.Point(826, 136);
            this.ultraLabel33.Name = "ultraLabel33";
            this.ultraLabel33.Size = new System.Drawing.Size(147, 21);
            this.ultraLabel33.TabIndex = 144;
            this.ultraLabel33.Text = "Đơn vị tiền: đồng Việt Nam";
            // 
            // ultraLabel25
            // 
            appearance419.BackColor = System.Drawing.Color.LightGray;
            appearance419.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance419.BorderColor = System.Drawing.Color.Black;
            appearance419.FontData.BoldAsString = "True";
            appearance419.ForeColor = System.Drawing.Color.Black;
            appearance419.TextHAlignAsString = "Center";
            appearance419.TextVAlignAsString = "Middle";
            this.ultraLabel25.Appearance = appearance419;
            this.ultraLabel25.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel25.Location = new System.Drawing.Point(746, 198);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(227, 29);
            this.ultraLabel25.TabIndex = 143;
            this.ultraLabel25.Text = "(4)";
            // 
            // ultraLabel26
            // 
            appearance420.BackColor = System.Drawing.Color.LightGray;
            appearance420.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance420.BorderColor = System.Drawing.Color.Black;
            appearance420.FontData.BoldAsString = "True";
            appearance420.ForeColor = System.Drawing.Color.Black;
            appearance420.TextHAlignAsString = "Center";
            appearance420.TextVAlignAsString = "Middle";
            this.ultraLabel26.Appearance = appearance420;
            this.ultraLabel26.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel26.Location = new System.Drawing.Point(141, 198);
            this.ultraLabel26.Name = "ultraLabel26";
            this.ultraLabel26.Size = new System.Drawing.Size(450, 29);
            this.ultraLabel26.TabIndex = 142;
            this.ultraLabel26.Text = "(2)";
            // 
            // ultraLabel27
            // 
            appearance421.BackColor = System.Drawing.Color.LightGray;
            appearance421.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance421.BorderColor = System.Drawing.Color.Black;
            appearance421.FontData.BoldAsString = "True";
            appearance421.ForeColor = System.Drawing.Color.Black;
            appearance421.TextHAlignAsString = "Center";
            appearance421.TextVAlignAsString = "Middle";
            this.ultraLabel27.Appearance = appearance421;
            this.ultraLabel27.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel27.Location = new System.Drawing.Point(590, 198);
            this.ultraLabel27.Name = "ultraLabel27";
            this.ultraLabel27.Size = new System.Drawing.Size(157, 29);
            this.ultraLabel27.TabIndex = 141;
            this.ultraLabel27.Text = "(3)";
            // 
            // ultraLabel28
            // 
            appearance422.BackColor = System.Drawing.Color.LightGray;
            appearance422.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance422.BorderColor = System.Drawing.Color.Black;
            appearance422.BorderColor2 = System.Drawing.Color.Transparent;
            appearance422.FontData.BoldAsString = "True";
            appearance422.ForeColor = System.Drawing.Color.Black;
            appearance422.TextHAlignAsString = "Center";
            appearance422.TextVAlignAsString = "Middle";
            this.ultraLabel28.Appearance = appearance422;
            this.ultraLabel28.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel28.Location = new System.Drawing.Point(103, 198);
            this.ultraLabel28.Name = "ultraLabel28";
            this.ultraLabel28.Size = new System.Drawing.Size(39, 29);
            this.ultraLabel28.TabIndex = 140;
            this.ultraLabel28.Text = "(1)";
            // 
            // ultraLabel29
            // 
            appearance423.BackColor = System.Drawing.Color.LightGray;
            appearance423.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance423.BorderColor = System.Drawing.Color.Black;
            appearance423.FontData.BoldAsString = "True";
            appearance423.ForeColor = System.Drawing.Color.Black;
            appearance423.TextHAlignAsString = "Center";
            appearance423.TextVAlignAsString = "Middle";
            this.ultraLabel29.Appearance = appearance423;
            this.ultraLabel29.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel29.Location = new System.Drawing.Point(746, 163);
            this.ultraLabel29.Name = "ultraLabel29";
            this.ultraLabel29.Size = new System.Drawing.Size(227, 36);
            this.ultraLabel29.TabIndex = 139;
            this.ultraLabel29.Text = "Số tiền";
            // 
            // ultraLabel30
            // 
            appearance424.BackColor = System.Drawing.Color.LightGray;
            appearance424.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance424.BorderColor = System.Drawing.Color.Black;
            appearance424.FontData.BoldAsString = "True";
            appearance424.ForeColor = System.Drawing.Color.Black;
            appearance424.TextHAlignAsString = "Center";
            appearance424.TextVAlignAsString = "Middle";
            this.ultraLabel30.Appearance = appearance424;
            this.ultraLabel30.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel30.Location = new System.Drawing.Point(141, 163);
            this.ultraLabel30.Name = "ultraLabel30";
            this.ultraLabel30.Size = new System.Drawing.Size(450, 36);
            this.ultraLabel30.TabIndex = 138;
            this.ultraLabel30.Text = "Chỉ tiêu";
            // 
            // ultraLabel31
            // 
            appearance425.BackColor = System.Drawing.Color.LightGray;
            appearance425.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance425.BorderColor = System.Drawing.Color.Black;
            appearance425.FontData.BoldAsString = "True";
            appearance425.ForeColor = System.Drawing.Color.Black;
            appearance425.TextHAlignAsString = "Center";
            appearance425.TextVAlignAsString = "Middle";
            this.ultraLabel31.Appearance = appearance425;
            this.ultraLabel31.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel31.Location = new System.Drawing.Point(590, 163);
            this.ultraLabel31.Name = "ultraLabel31";
            this.ultraLabel31.Size = new System.Drawing.Size(157, 36);
            this.ultraLabel31.TabIndex = 137;
            this.ultraLabel31.Text = "Mã chỉ tiêu";
            // 
            // ultraLabel32
            // 
            appearance426.BackColor = System.Drawing.Color.LightGray;
            appearance426.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance426.BorderColor = System.Drawing.Color.Black;
            appearance426.BorderColor2 = System.Drawing.Color.Transparent;
            appearance426.FontData.BoldAsString = "True";
            appearance426.ForeColor = System.Drawing.Color.Black;
            appearance426.TextHAlignAsString = "Center";
            appearance426.TextVAlignAsString = "Middle";
            this.ultraLabel32.Appearance = appearance426;
            this.ultraLabel32.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel32.Location = new System.Drawing.Point(103, 163);
            this.ultraLabel32.Name = "ultraLabel32";
            this.ultraLabel32.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ultraLabel32.Size = new System.Drawing.Size(39, 36);
            this.ultraLabel32.TabIndex = 136;
            this.ultraLabel32.Text = "STT";
            // 
            // ultraLabel24
            // 
            appearance427.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance427.TextHAlignAsString = "Center";
            appearance427.TextVAlignAsString = "Bottom";
            this.ultraLabel24.Appearance = appearance427;
            this.ultraLabel24.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel24.Location = new System.Drawing.Point(0, 71);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(1131, 20);
            this.ultraLabel24.TabIndex = 9;
            this.ultraLabel24.Text = "Dành cho người nộp thuế thuộc các ngành sản xuất, thương mại, dịch vụ";
            // 
            // ultraLabel22
            // 
            appearance428.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance428.TextHAlignAsString = "Center";
            appearance428.TextVAlignAsString = "Bottom";
            this.ultraLabel22.Appearance = appearance428;
            this.ultraLabel22.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel22.Location = new System.Drawing.Point(0, 46);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(1127, 25);
            this.ultraLabel22.TabIndex = 8;
            this.ultraLabel22.Text = "KẾT QUẢ HOẠT ĐỘNG SẢN XUẤT KINH DOANH";
            // 
            // ultraLabel23
            // 
            appearance429.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance429.TextHAlignAsString = "Center";
            appearance429.TextVAlignAsString = "Bottom";
            this.ultraLabel23.Appearance = appearance429;
            this.ultraLabel23.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel23.Location = new System.Drawing.Point(0, 0);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(1127, 46);
            this.ultraLabel23.TabIndex = 7;
            this.ultraLabel23.Text = "PHỤ LỤC 03 - 1A";
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.AutoScroll = true;
            this.ultraTabPageControl3.Controls.Add(this.tablePL2);
            this.ultraTabPageControl3.Controls.Add(this.ultraLabel19);
            this.ultraTabPageControl3.Controls.Add(this.ultraLabel6);
            this.ultraTabPageControl3.Controls.Add(this.ultraLabel5);
            this.ultraTabPageControl3.Controls.Add(this.ultraLabel4);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(1128, 568);
            // 
            // tablePL2
            // 
            this.tablePL2.AutoSize = true;
            this.tablePL2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tablePL2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tablePL2.ColumnCount = 6;
            this.tablePL2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tablePL2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 136F));
            this.tablePL2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 197F));
            this.tablePL2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 206F));
            this.tablePL2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 206F));
            this.tablePL2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 272F));
            this.tablePL2.Controls.Add(this.txtItemPL2_10, 0, 2);
            this.tablePL2.Controls.Add(this.label13, 0, 7);
            this.tablePL2.Controls.Add(this.txtItemPL2_65, 5, 7);
            this.tablePL2.Controls.Add(this.txtItemPL2_55, 4, 7);
            this.tablePL2.Controls.Add(this.txtItemPL2_45, 3, 7);
            this.tablePL2.Controls.Add(this.txtItemPL2_35, 2, 7);
            this.tablePL2.Controls.Add(this.txtItemPL2_64, 5, 6);
            this.tablePL2.Controls.Add(this.txtItemPL2_54, 4, 6);
            this.tablePL2.Controls.Add(this.txtItemPL2_44, 3, 6);
            this.tablePL2.Controls.Add(this.txtItemPL2_34, 2, 6);
            this.tablePL2.Controls.Add(this.txtItemPL2_63, 5, 5);
            this.tablePL2.Controls.Add(this.txtItemPL2_53, 4, 5);
            this.tablePL2.Controls.Add(this.txtItemPL2_43, 3, 5);
            this.tablePL2.Controls.Add(this.txtItemPL2_33, 2, 5);
            this.tablePL2.Controls.Add(this.txtItemPL2_62, 5, 4);
            this.tablePL2.Controls.Add(this.txtItemPL2_52, 4, 4);
            this.tablePL2.Controls.Add(this.txtItemPL2_42, 3, 4);
            this.tablePL2.Controls.Add(this.txtItemPL2_32, 2, 4);
            this.tablePL2.Controls.Add(this.txtItemPL2_61, 5, 3);
            this.tablePL2.Controls.Add(this.txtItemPL2_51, 4, 3);
            this.tablePL2.Controls.Add(this.txtItemPL2_41, 3, 3);
            this.tablePL2.Controls.Add(this.txtItemPL2_31, 2, 3);
            this.tablePL2.Controls.Add(this.txtItemPL2_60, 5, 2);
            this.tablePL2.Controls.Add(this.txtItemPL2_50, 4, 2);
            this.tablePL2.Controls.Add(this.txtItemPL2_40, 3, 2);
            this.tablePL2.Controls.Add(this.txtItemPL2_30, 2, 2);
            this.tablePL2.Controls.Add(this.label12, 5, 1);
            this.tablePL2.Controls.Add(this.label11, 4, 1);
            this.tablePL2.Controls.Add(this.label10, 3, 1);
            this.tablePL2.Controls.Add(this.label9, 2, 1);
            this.tablePL2.Controls.Add(this.label8, 1, 1);
            this.tablePL2.Controls.Add(this.label7, 0, 1);
            this.tablePL2.Controls.Add(this.label6, 5, 0);
            this.tablePL2.Controls.Add(this.label5, 4, 0);
            this.tablePL2.Controls.Add(this.label4, 3, 0);
            this.tablePL2.Controls.Add(this.label3, 2, 0);
            this.tablePL2.Controls.Add(this.label1, 1, 0);
            this.tablePL2.Controls.Add(this.label2, 0, 0);
            this.tablePL2.Controls.Add(this.txtItemPL2_11, 0, 3);
            this.tablePL2.Controls.Add(this.txtItemPL2_12, 0, 4);
            this.tablePL2.Controls.Add(this.txtItemPL2_13, 0, 5);
            this.tablePL2.Controls.Add(this.txtItemPL2_14, 0, 6);
            this.tablePL2.Controls.Add(this.txtItemPL2_20, 1, 2);
            this.tablePL2.Controls.Add(this.txtItemPL2_21, 1, 3);
            this.tablePL2.Controls.Add(this.txtItemPL2_22, 1, 4);
            this.tablePL2.Controls.Add(this.txtItemPL2_23, 1, 5);
            this.tablePL2.Controls.Add(this.txtItemPL2_24, 1, 6);
            this.tablePL2.Location = new System.Drawing.Point(45, 153);
            this.tablePL2.Name = "tablePL2";
            this.tablePL2.RowCount = 8;
            this.tablePL2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tablePL2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePL2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePL2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePL2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePL2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePL2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePL2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePL2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tablePL2.Size = new System.Drawing.Size(1063, 288);
            this.tablePL2.TabIndex = 140;
            // 
            // txtItemPL2_10
            // 
            this.txtItemPL2_10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance430.TextHAlignAsString = "Center";
            this.txtItemPL2_10.Appearance = appearance430;
            this.txtItemPL2_10.AutoSize = false;
            this.txtItemPL2_10.Location = new System.Drawing.Point(1, 90);
            this.txtItemPL2_10.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_10.Name = "txtItemPL2_10";
            this.txtItemPL2_10.ReadOnly = true;
            this.txtItemPL2_10.Size = new System.Drawing.Size(39, 32);
            this.txtItemPL2_10.TabIndex = 141;
            this.txtItemPL2_10.Text = "1";
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.LightGray;
            this.tablePL2.SetColumnSpan(this.label13, 2);
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(1, 255);
            this.label13.Margin = new System.Windows.Forms.Padding(0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(176, 32);
            this.label13.TabIndex = 177;
            this.label13.Text = "Tổng cộng";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtItemPL2_65
            // 
            appearance431.TextHAlignAsString = "Right";
            this.txtItemPL2_65.Appearance = appearance431;
            this.txtItemPL2_65.AutoSize = false;
            this.txtItemPL2_65.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_65.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_65.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_65.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_65.Location = new System.Drawing.Point(790, 255);
            this.txtItemPL2_65.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_65.Name = "txtItemPL2_65";
            this.txtItemPL2_65.NullText = "0";
            this.txtItemPL2_65.PromptChar = ' ';
            this.txtItemPL2_65.ReadOnly = true;
            this.txtItemPL2_65.Size = new System.Drawing.Size(272, 32);
            this.txtItemPL2_65.TabIndex = 176;
            // 
            // txtItemPL2_55
            // 
            appearance432.TextHAlignAsString = "Right";
            this.txtItemPL2_55.Appearance = appearance432;
            this.txtItemPL2_55.AutoSize = false;
            this.txtItemPL2_55.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_55.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_55.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_55.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_55.Location = new System.Drawing.Point(583, 255);
            this.txtItemPL2_55.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_55.Name = "txtItemPL2_55";
            this.txtItemPL2_55.NullText = "0";
            this.txtItemPL2_55.PromptChar = ' ';
            this.txtItemPL2_55.ReadOnly = true;
            this.txtItemPL2_55.Size = new System.Drawing.Size(206, 32);
            this.txtItemPL2_55.TabIndex = 175;
            this.txtItemPL2_55.ValueChanged += new System.EventHandler(this.txtItemPL2_55_ValueChanged);
            // 
            // txtItemPL2_45
            // 
            appearance433.TextHAlignAsString = "Right";
            this.txtItemPL2_45.Appearance = appearance433;
            this.txtItemPL2_45.AutoSize = false;
            this.txtItemPL2_45.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_45.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_45.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_45.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_45.Location = new System.Drawing.Point(376, 255);
            this.txtItemPL2_45.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_45.Name = "txtItemPL2_45";
            this.txtItemPL2_45.NullText = "0";
            this.txtItemPL2_45.PromptChar = ' ';
            this.txtItemPL2_45.ReadOnly = true;
            this.txtItemPL2_45.Size = new System.Drawing.Size(206, 32);
            this.txtItemPL2_45.TabIndex = 174;
            // 
            // txtItemPL2_35
            // 
            appearance434.TextHAlignAsString = "Right";
            this.txtItemPL2_35.Appearance = appearance434;
            this.txtItemPL2_35.AutoSize = false;
            this.txtItemPL2_35.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_35.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_35.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_35.Location = new System.Drawing.Point(178, 255);
            this.txtItemPL2_35.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_35.Name = "txtItemPL2_35";
            this.txtItemPL2_35.NullText = "0";
            this.txtItemPL2_35.PromptChar = ' ';
            this.txtItemPL2_35.ReadOnly = true;
            this.txtItemPL2_35.Size = new System.Drawing.Size(197, 32);
            this.txtItemPL2_35.TabIndex = 173;
            // 
            // txtItemPL2_64
            // 
            appearance435.TextHAlignAsString = "Right";
            this.txtItemPL2_64.Appearance = appearance435;
            this.txtItemPL2_64.AutoSize = false;
            this.txtItemPL2_64.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_64.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_64.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_64.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_64.Location = new System.Drawing.Point(790, 222);
            this.txtItemPL2_64.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_64.Name = "txtItemPL2_64";
            this.txtItemPL2_64.NullText = "0";
            this.txtItemPL2_64.PromptChar = ' ';
            this.txtItemPL2_64.ReadOnly = true;
            this.txtItemPL2_64.Size = new System.Drawing.Size(272, 32);
            this.txtItemPL2_64.TabIndex = 170;
            this.txtItemPL2_64.ValueChanged += new System.EventHandler(this.txtItemPL2_60_ValueChanged);
            // 
            // txtItemPL2_54
            // 
            appearance436.TextHAlignAsString = "Right";
            this.txtItemPL2_54.Appearance = appearance436;
            this.txtItemPL2_54.AutoSize = false;
            this.txtItemPL2_54.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_54.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_54.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_54.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_54.Location = new System.Drawing.Point(583, 222);
            this.txtItemPL2_54.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_54.Name = "txtItemPL2_54";
            this.txtItemPL2_54.NullText = "0";
            this.txtItemPL2_54.PromptChar = ' ';
            this.txtItemPL2_54.Size = new System.Drawing.Size(206, 32);
            this.txtItemPL2_54.TabIndex = 169;
            this.txtItemPL2_54.ValueChanged += new System.EventHandler(this.txtItemPL2_50_ValueChanged);
            this.txtItemPL2_54.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // txtItemPL2_44
            // 
            appearance437.TextHAlignAsString = "Right";
            this.txtItemPL2_44.Appearance = appearance437;
            this.txtItemPL2_44.AutoSize = false;
            this.txtItemPL2_44.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_44.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_44.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_44.Location = new System.Drawing.Point(376, 222);
            this.txtItemPL2_44.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_44.Name = "txtItemPL2_44";
            this.txtItemPL2_44.NullText = "0";
            this.txtItemPL2_44.PromptChar = ' ';
            this.txtItemPL2_44.Size = new System.Drawing.Size(206, 32);
            this.txtItemPL2_44.TabIndex = 168;
            this.txtItemPL2_44.ValueChanged += new System.EventHandler(this.txtItemPL2_40_ValueChanged);
            this.txtItemPL2_44.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // txtItemPL2_34
            // 
            appearance438.TextHAlignAsString = "Right";
            this.txtItemPL2_34.Appearance = appearance438;
            this.txtItemPL2_34.AutoSize = false;
            this.txtItemPL2_34.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_34.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_34.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_34.Location = new System.Drawing.Point(178, 222);
            this.txtItemPL2_34.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_34.Name = "txtItemPL2_34";
            this.txtItemPL2_34.NullText = "0";
            this.txtItemPL2_34.PromptChar = ' ';
            this.txtItemPL2_34.Size = new System.Drawing.Size(197, 32);
            this.txtItemPL2_34.TabIndex = 167;
            this.txtItemPL2_34.ValueChanged += new System.EventHandler(this.txtItemPL2_30_ValueChanged);
            this.txtItemPL2_34.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // txtItemPL2_63
            // 
            appearance439.TextHAlignAsString = "Right";
            this.txtItemPL2_63.Appearance = appearance439;
            this.txtItemPL2_63.AutoSize = false;
            this.txtItemPL2_63.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_63.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_63.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_63.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_63.Location = new System.Drawing.Point(790, 189);
            this.txtItemPL2_63.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_63.Name = "txtItemPL2_63";
            this.txtItemPL2_63.NullText = "0";
            this.txtItemPL2_63.PromptChar = ' ';
            this.txtItemPL2_63.ReadOnly = true;
            this.txtItemPL2_63.Size = new System.Drawing.Size(272, 32);
            this.txtItemPL2_63.TabIndex = 164;
            this.txtItemPL2_63.ValueChanged += new System.EventHandler(this.txtItemPL2_60_ValueChanged);
            // 
            // txtItemPL2_53
            // 
            appearance440.TextHAlignAsString = "Right";
            this.txtItemPL2_53.Appearance = appearance440;
            this.txtItemPL2_53.AutoSize = false;
            this.txtItemPL2_53.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_53.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_53.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_53.Location = new System.Drawing.Point(583, 189);
            this.txtItemPL2_53.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_53.Name = "txtItemPL2_53";
            this.txtItemPL2_53.NullText = "0";
            this.txtItemPL2_53.PromptChar = ' ';
            this.txtItemPL2_53.Size = new System.Drawing.Size(206, 32);
            this.txtItemPL2_53.TabIndex = 163;
            this.txtItemPL2_53.ValueChanged += new System.EventHandler(this.txtItemPL2_50_ValueChanged);
            this.txtItemPL2_53.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // txtItemPL2_43
            // 
            appearance441.TextHAlignAsString = "Right";
            this.txtItemPL2_43.Appearance = appearance441;
            this.txtItemPL2_43.AutoSize = false;
            this.txtItemPL2_43.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_43.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_43.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_43.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_43.Location = new System.Drawing.Point(376, 189);
            this.txtItemPL2_43.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_43.Name = "txtItemPL2_43";
            this.txtItemPL2_43.NullText = "0";
            this.txtItemPL2_43.PromptChar = ' ';
            this.txtItemPL2_43.Size = new System.Drawing.Size(206, 32);
            this.txtItemPL2_43.TabIndex = 162;
            this.txtItemPL2_43.ValueChanged += new System.EventHandler(this.txtItemPL2_40_ValueChanged);
            this.txtItemPL2_43.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // txtItemPL2_33
            // 
            appearance442.TextHAlignAsString = "Right";
            this.txtItemPL2_33.Appearance = appearance442;
            this.txtItemPL2_33.AutoSize = false;
            this.txtItemPL2_33.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_33.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_33.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_33.Location = new System.Drawing.Point(178, 189);
            this.txtItemPL2_33.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_33.Name = "txtItemPL2_33";
            this.txtItemPL2_33.NullText = "0";
            this.txtItemPL2_33.PromptChar = ' ';
            this.txtItemPL2_33.Size = new System.Drawing.Size(197, 32);
            this.txtItemPL2_33.TabIndex = 161;
            this.txtItemPL2_33.ValueChanged += new System.EventHandler(this.txtItemPL2_30_ValueChanged);
            this.txtItemPL2_33.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // txtItemPL2_62
            // 
            appearance443.TextHAlignAsString = "Right";
            this.txtItemPL2_62.Appearance = appearance443;
            this.txtItemPL2_62.AutoSize = false;
            this.txtItemPL2_62.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_62.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_62.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_62.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_62.Location = new System.Drawing.Point(790, 156);
            this.txtItemPL2_62.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_62.Name = "txtItemPL2_62";
            this.txtItemPL2_62.NullText = "0";
            this.txtItemPL2_62.PromptChar = ' ';
            this.txtItemPL2_62.ReadOnly = true;
            this.txtItemPL2_62.Size = new System.Drawing.Size(272, 32);
            this.txtItemPL2_62.TabIndex = 158;
            this.txtItemPL2_62.ValueChanged += new System.EventHandler(this.txtItemPL2_60_ValueChanged);
            // 
            // txtItemPL2_52
            // 
            appearance444.TextHAlignAsString = "Right";
            this.txtItemPL2_52.Appearance = appearance444;
            this.txtItemPL2_52.AutoSize = false;
            this.txtItemPL2_52.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_52.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_52.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_52.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_52.Location = new System.Drawing.Point(583, 156);
            this.txtItemPL2_52.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_52.Name = "txtItemPL2_52";
            this.txtItemPL2_52.NullText = "0";
            this.txtItemPL2_52.PromptChar = ' ';
            this.txtItemPL2_52.Size = new System.Drawing.Size(206, 32);
            this.txtItemPL2_52.TabIndex = 157;
            this.txtItemPL2_52.ValueChanged += new System.EventHandler(this.txtItemPL2_50_ValueChanged);
            this.txtItemPL2_52.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // txtItemPL2_42
            // 
            appearance445.TextHAlignAsString = "Right";
            this.txtItemPL2_42.Appearance = appearance445;
            this.txtItemPL2_42.AutoSize = false;
            this.txtItemPL2_42.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_42.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_42.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_42.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_42.Location = new System.Drawing.Point(376, 156);
            this.txtItemPL2_42.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_42.Name = "txtItemPL2_42";
            this.txtItemPL2_42.NullText = "0";
            this.txtItemPL2_42.PromptChar = ' ';
            this.txtItemPL2_42.Size = new System.Drawing.Size(206, 32);
            this.txtItemPL2_42.TabIndex = 156;
            this.txtItemPL2_42.ValueChanged += new System.EventHandler(this.txtItemPL2_40_ValueChanged);
            this.txtItemPL2_42.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // txtItemPL2_32
            // 
            appearance446.TextHAlignAsString = "Right";
            this.txtItemPL2_32.Appearance = appearance446;
            this.txtItemPL2_32.AutoSize = false;
            this.txtItemPL2_32.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_32.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_32.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_32.Location = new System.Drawing.Point(178, 156);
            this.txtItemPL2_32.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_32.Name = "txtItemPL2_32";
            this.txtItemPL2_32.NullText = "0";
            this.txtItemPL2_32.PromptChar = ' ';
            this.txtItemPL2_32.Size = new System.Drawing.Size(197, 32);
            this.txtItemPL2_32.TabIndex = 155;
            this.txtItemPL2_32.ValueChanged += new System.EventHandler(this.txtItemPL2_30_ValueChanged);
            this.txtItemPL2_32.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // txtItemPL2_61
            // 
            appearance447.TextHAlignAsString = "Right";
            this.txtItemPL2_61.Appearance = appearance447;
            this.txtItemPL2_61.AutoSize = false;
            this.txtItemPL2_61.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_61.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_61.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_61.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_61.Location = new System.Drawing.Point(790, 123);
            this.txtItemPL2_61.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_61.Name = "txtItemPL2_61";
            this.txtItemPL2_61.NullText = "0";
            this.txtItemPL2_61.PromptChar = ' ';
            this.txtItemPL2_61.ReadOnly = true;
            this.txtItemPL2_61.Size = new System.Drawing.Size(272, 32);
            this.txtItemPL2_61.TabIndex = 152;
            this.txtItemPL2_61.ValueChanged += new System.EventHandler(this.txtItemPL2_60_ValueChanged);
            // 
            // txtItemPL2_51
            // 
            appearance448.TextHAlignAsString = "Right";
            this.txtItemPL2_51.Appearance = appearance448;
            this.txtItemPL2_51.AutoSize = false;
            this.txtItemPL2_51.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_51.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_51.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_51.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_51.Location = new System.Drawing.Point(583, 123);
            this.txtItemPL2_51.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_51.Name = "txtItemPL2_51";
            this.txtItemPL2_51.NullText = "0";
            this.txtItemPL2_51.PromptChar = ' ';
            this.txtItemPL2_51.Size = new System.Drawing.Size(206, 32);
            this.txtItemPL2_51.TabIndex = 151;
            this.txtItemPL2_51.ValueChanged += new System.EventHandler(this.txtItemPL2_50_ValueChanged);
            this.txtItemPL2_51.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // txtItemPL2_41
            // 
            appearance449.TextHAlignAsString = "Right";
            this.txtItemPL2_41.Appearance = appearance449;
            this.txtItemPL2_41.AutoSize = false;
            this.txtItemPL2_41.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_41.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_41.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_41.Location = new System.Drawing.Point(376, 123);
            this.txtItemPL2_41.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_41.Name = "txtItemPL2_41";
            this.txtItemPL2_41.NullText = "0";
            this.txtItemPL2_41.PromptChar = ' ';
            this.txtItemPL2_41.Size = new System.Drawing.Size(206, 32);
            this.txtItemPL2_41.TabIndex = 150;
            this.txtItemPL2_41.ValueChanged += new System.EventHandler(this.txtItemPL2_40_ValueChanged);
            this.txtItemPL2_41.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // txtItemPL2_31
            // 
            appearance450.TextHAlignAsString = "Right";
            this.txtItemPL2_31.Appearance = appearance450;
            this.txtItemPL2_31.AutoSize = false;
            this.txtItemPL2_31.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_31.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_31.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_31.Location = new System.Drawing.Point(178, 123);
            this.txtItemPL2_31.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_31.Name = "txtItemPL2_31";
            this.txtItemPL2_31.NullText = "0";
            this.txtItemPL2_31.PromptChar = ' ';
            this.txtItemPL2_31.Size = new System.Drawing.Size(197, 32);
            this.txtItemPL2_31.TabIndex = 149;
            this.txtItemPL2_31.ValueChanged += new System.EventHandler(this.txtItemPL2_30_ValueChanged);
            this.txtItemPL2_31.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // txtItemPL2_60
            // 
            appearance451.TextHAlignAsString = "Right";
            this.txtItemPL2_60.Appearance = appearance451;
            this.txtItemPL2_60.AutoSize = false;
            this.txtItemPL2_60.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_60.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_60.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_60.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_60.Location = new System.Drawing.Point(790, 90);
            this.txtItemPL2_60.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_60.Name = "txtItemPL2_60";
            this.txtItemPL2_60.NullText = "0";
            this.txtItemPL2_60.PromptChar = ' ';
            this.txtItemPL2_60.ReadOnly = true;
            this.txtItemPL2_60.Size = new System.Drawing.Size(272, 32);
            this.txtItemPL2_60.TabIndex = 146;
            this.txtItemPL2_60.ValueChanged += new System.EventHandler(this.txtItemPL2_60_ValueChanged);
            // 
            // txtItemPL2_50
            // 
            appearance452.TextHAlignAsString = "Right";
            this.txtItemPL2_50.Appearance = appearance452;
            this.txtItemPL2_50.AutoSize = false;
            this.txtItemPL2_50.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_50.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_50.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_50.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_50.Location = new System.Drawing.Point(583, 90);
            this.txtItemPL2_50.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_50.Name = "txtItemPL2_50";
            this.txtItemPL2_50.NullText = "0";
            this.txtItemPL2_50.PromptChar = ' ';
            this.txtItemPL2_50.Size = new System.Drawing.Size(206, 32);
            this.txtItemPL2_50.TabIndex = 145;
            this.txtItemPL2_50.ValueChanged += new System.EventHandler(this.txtItemPL2_50_ValueChanged);
            this.txtItemPL2_50.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // txtItemPL2_40
            // 
            appearance453.TextHAlignAsString = "Right";
            this.txtItemPL2_40.Appearance = appearance453;
            this.txtItemPL2_40.AutoSize = false;
            this.txtItemPL2_40.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_40.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_40.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_40.Location = new System.Drawing.Point(376, 90);
            this.txtItemPL2_40.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_40.Name = "txtItemPL2_40";
            this.txtItemPL2_40.NullText = "0";
            this.txtItemPL2_40.PromptChar = ' ';
            this.txtItemPL2_40.Size = new System.Drawing.Size(206, 32);
            this.txtItemPL2_40.TabIndex = 144;
            this.txtItemPL2_40.ValueChanged += new System.EventHandler(this.txtItemPL2_40_ValueChanged);
            this.txtItemPL2_40.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // txtItemPL2_30
            // 
            appearance454.TextHAlignAsString = "Right";
            this.txtItemPL2_30.Appearance = appearance454;
            this.txtItemPL2_30.AutoSize = false;
            this.txtItemPL2_30.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtItemPL2_30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemPL2_30.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtItemPL2_30.InputMask = "-n,nnn,nnn,nnn,nnn,nnn";
            this.txtItemPL2_30.Location = new System.Drawing.Point(178, 90);
            this.txtItemPL2_30.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_30.Name = "txtItemPL2_30";
            this.txtItemPL2_30.NullText = "0";
            this.txtItemPL2_30.PromptChar = ' ';
            this.txtItemPL2_30.Size = new System.Drawing.Size(197, 32);
            this.txtItemPL2_30.TabIndex = 143;
            this.txtItemPL2_30.ValueChanged += new System.EventHandler(this.txtItemPL2_30_ValueChanged);
            this.txtItemPL2_30.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.LightGray;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(790, 57);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(272, 32);
            this.label12.TabIndex = 12;
            this.label12.Text = "(6)";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.LightGray;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(583, 57);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(206, 32);
            this.label11.TabIndex = 11;
            this.label11.Text = "(5)";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.LightGray;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(376, 57);
            this.label10.Margin = new System.Windows.Forms.Padding(0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(206, 32);
            this.label10.TabIndex = 10;
            this.label10.Text = "(4)";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.LightGray;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(178, 57);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(197, 32);
            this.label9.TabIndex = 9;
            this.label9.Text = "(3)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.LightGray;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(41, 57);
            this.label8.Margin = new System.Windows.Forms.Padding(0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(136, 32);
            this.label8.TabIndex = 8;
            this.label8.Text = "(2)";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.LightGray;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(1, 57);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 32);
            this.label7.TabIndex = 7;
            this.label7.Text = "(1)";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.LightGray;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(790, 1);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(272, 55);
            this.label6.TabIndex = 6;
            this.label6.Text = "Số lỗ còn được chuyển sang các kỳ tính thuế sau";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.LightGray;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(583, 1);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(206, 55);
            this.label5.TabIndex = 5;
            this.label5.Text = "Số lỗ được chuyển trong kỳ tính thuế này";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.LightGray;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(376, 1);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(206, 55);
            this.label4.TabIndex = 4;
            this.label4.Text = "Số lỗ đã chuyển trong các kỳ tính thuế trước";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.LightGray;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(178, 1);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(197, 55);
            this.label3.TabIndex = 3;
            this.label3.Text = "Số lỗ phát sinh";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.LightGray;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(41, 1);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 55);
            this.label1.TabIndex = 2;
            this.label1.Text = "Năm phát sinh lỗ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.LightGray;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1, 1);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 55);
            this.label2.TabIndex = 1;
            this.label2.Text = "STT";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtItemPL2_11
            // 
            this.txtItemPL2_11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance455.TextHAlignAsString = "Center";
            this.txtItemPL2_11.Appearance = appearance455;
            this.txtItemPL2_11.AutoSize = false;
            this.txtItemPL2_11.Location = new System.Drawing.Point(1, 123);
            this.txtItemPL2_11.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_11.Name = "txtItemPL2_11";
            this.txtItemPL2_11.ReadOnly = true;
            this.txtItemPL2_11.Size = new System.Drawing.Size(39, 32);
            this.txtItemPL2_11.TabIndex = 141;
            this.txtItemPL2_11.Text = "2";
            // 
            // txtItemPL2_12
            // 
            this.txtItemPL2_12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance456.TextHAlignAsString = "Center";
            this.txtItemPL2_12.Appearance = appearance456;
            this.txtItemPL2_12.AutoSize = false;
            this.txtItemPL2_12.Location = new System.Drawing.Point(1, 156);
            this.txtItemPL2_12.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_12.Name = "txtItemPL2_12";
            this.txtItemPL2_12.ReadOnly = true;
            this.txtItemPL2_12.Size = new System.Drawing.Size(39, 32);
            this.txtItemPL2_12.TabIndex = 141;
            this.txtItemPL2_12.Text = "3";
            // 
            // txtItemPL2_13
            // 
            this.txtItemPL2_13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance457.TextHAlignAsString = "Center";
            this.txtItemPL2_13.Appearance = appearance457;
            this.txtItemPL2_13.AutoSize = false;
            this.txtItemPL2_13.Location = new System.Drawing.Point(1, 189);
            this.txtItemPL2_13.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_13.Name = "txtItemPL2_13";
            this.txtItemPL2_13.ReadOnly = true;
            this.txtItemPL2_13.Size = new System.Drawing.Size(39, 32);
            this.txtItemPL2_13.TabIndex = 141;
            this.txtItemPL2_13.Text = "4";
            // 
            // txtItemPL2_14
            // 
            this.txtItemPL2_14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance458.TextHAlignAsString = "Center";
            this.txtItemPL2_14.Appearance = appearance458;
            this.txtItemPL2_14.AutoSize = false;
            this.txtItemPL2_14.Location = new System.Drawing.Point(1, 222);
            this.txtItemPL2_14.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_14.Name = "txtItemPL2_14";
            this.txtItemPL2_14.ReadOnly = true;
            this.txtItemPL2_14.Size = new System.Drawing.Size(39, 32);
            this.txtItemPL2_14.TabIndex = 141;
            this.txtItemPL2_14.Text = "5";
            // 
            // txtItemPL2_20
            // 
            this.txtItemPL2_20.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance459.TextHAlignAsString = "Center";
            this.txtItemPL2_20.Appearance = appearance459;
            this.txtItemPL2_20.AutoSize = false;
            this.txtItemPL2_20.Location = new System.Drawing.Point(41, 90);
            this.txtItemPL2_20.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_20.Name = "txtItemPL2_20";
            this.txtItemPL2_20.ReadOnly = true;
            this.txtItemPL2_20.Size = new System.Drawing.Size(136, 32);
            this.txtItemPL2_20.TabIndex = 141;
            this.txtItemPL2_20.Text = "1";
            // 
            // txtItemPL2_21
            // 
            this.txtItemPL2_21.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance460.TextHAlignAsString = "Center";
            this.txtItemPL2_21.Appearance = appearance460;
            this.txtItemPL2_21.AutoSize = false;
            this.txtItemPL2_21.Location = new System.Drawing.Point(41, 123);
            this.txtItemPL2_21.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_21.Name = "txtItemPL2_21";
            this.txtItemPL2_21.ReadOnly = true;
            this.txtItemPL2_21.Size = new System.Drawing.Size(136, 32);
            this.txtItemPL2_21.TabIndex = 141;
            this.txtItemPL2_21.Text = "1";
            // 
            // txtItemPL2_22
            // 
            this.txtItemPL2_22.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance461.TextHAlignAsString = "Center";
            this.txtItemPL2_22.Appearance = appearance461;
            this.txtItemPL2_22.AutoSize = false;
            this.txtItemPL2_22.Location = new System.Drawing.Point(41, 156);
            this.txtItemPL2_22.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_22.Name = "txtItemPL2_22";
            this.txtItemPL2_22.ReadOnly = true;
            this.txtItemPL2_22.Size = new System.Drawing.Size(136, 32);
            this.txtItemPL2_22.TabIndex = 141;
            this.txtItemPL2_22.Text = "1";
            // 
            // txtItemPL2_23
            // 
            this.txtItemPL2_23.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance462.TextHAlignAsString = "Center";
            this.txtItemPL2_23.Appearance = appearance462;
            this.txtItemPL2_23.AutoSize = false;
            this.txtItemPL2_23.Location = new System.Drawing.Point(41, 189);
            this.txtItemPL2_23.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_23.Name = "txtItemPL2_23";
            this.txtItemPL2_23.ReadOnly = true;
            this.txtItemPL2_23.Size = new System.Drawing.Size(136, 32);
            this.txtItemPL2_23.TabIndex = 141;
            this.txtItemPL2_23.Text = "1";
            // 
            // txtItemPL2_24
            // 
            this.txtItemPL2_24.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance463.TextHAlignAsString = "Center";
            this.txtItemPL2_24.Appearance = appearance463;
            this.txtItemPL2_24.AutoSize = false;
            this.txtItemPL2_24.Location = new System.Drawing.Point(41, 222);
            this.txtItemPL2_24.Margin = new System.Windows.Forms.Padding(0);
            this.txtItemPL2_24.Name = "txtItemPL2_24";
            this.txtItemPL2_24.ReadOnly = true;
            this.txtItemPL2_24.Size = new System.Drawing.Size(136, 32);
            this.txtItemPL2_24.TabIndex = 141;
            this.txtItemPL2_24.Text = "1";
            // 
            // ultraLabel19
            // 
            appearance464.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance464.TextHAlignAsString = "Left";
            this.ultraLabel19.Appearance = appearance464;
            this.ultraLabel19.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel19.Location = new System.Drawing.Point(900, 126);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(147, 21);
            this.ultraLabel19.TabIndex = 138;
            this.ultraLabel19.Text = "Đơn vị tiền: đồng Việt Nam";
            // 
            // ultraLabel6
            // 
            appearance465.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance465.TextHAlignAsString = "Center";
            appearance465.TextVAlignAsString = "Bottom";
            this.ultraLabel6.Appearance = appearance465;
            this.ultraLabel6.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel6.Location = new System.Drawing.Point(0, 71);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(1131, 35);
            this.ultraLabel6.TabIndex = 7;
            this.ultraLabel6.Text = "Xác định số lỗ được chuyển trong kỳ tính thuế:";
            // 
            // ultraLabel5
            // 
            appearance466.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance466.TextHAlignAsString = "Center";
            appearance466.TextVAlignAsString = "Bottom";
            this.ultraLabel5.Appearance = appearance466;
            this.ultraLabel5.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel5.Location = new System.Drawing.Point(0, 46);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(1130, 25);
            this.ultraLabel5.TabIndex = 6;
            this.ultraLabel5.Text = "CHUYỂN LỖ TỪ HOẠT ĐỘNG SXKD";
            // 
            // ultraLabel4
            // 
            appearance467.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance467.TextHAlignAsString = "Center";
            appearance467.TextVAlignAsString = "Bottom";
            this.ultraLabel4.Appearance = appearance467;
            this.ultraLabel4.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel4.Location = new System.Drawing.Point(0, 0);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(1130, 46);
            this.ultraLabel4.TabIndex = 5;
            this.ultraLabel4.Text = "PHỤ LỤC 03 - 2A";
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.ultraTextEditor7);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel194);
            this.ultraTabPageControl4.Controls.Add(this.ultraTextEditor6);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel193);
            this.ultraTabPageControl4.Controls.Add(this.ultraTextEditor5);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel192);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel191);
            this.ultraTabPageControl4.Controls.Add(this.ultraTextEditor4);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel190);
            this.ultraTabPageControl4.Controls.Add(this.ultraTextEditor3);
            this.ultraTabPageControl4.Controls.Add(this.ultraDateTimeEditor1);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel189);
            this.ultraTabPageControl4.Controls.Add(this.txtReason);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel188);
            this.ultraTabPageControl4.Controls.Add(this.ultraTextEditor2);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel187);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel185);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel184);
            this.ultraTabPageControl4.Controls.Add(this.ultraTextEditor1);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel183);
            this.ultraTabPageControl4.Controls.Add(this.txtDateDelay);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel182);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel3);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel174);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel173);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel172);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel171);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel170);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel168);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel167);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel181);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel169);
            this.ultraTabPageControl4.Controls.Add(this.ultraPanel40);
            this.ultraTabPageControl4.Controls.Add(this.ultraPanel76);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel161);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel158);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel156);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel157);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel159);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel160);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel162);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel155);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel154);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel153);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel152);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel151);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(1128, 568);
            // 
            // ultraTextEditor7
            // 
            this.ultraTextEditor7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraTextEditor7.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraTextEditor7.Location = new System.Drawing.Point(148, 800);
            this.ultraTextEditor7.MaxLength = 512;
            this.ultraTextEditor7.Multiline = true;
            this.ultraTextEditor7.Name = "ultraTextEditor7";
            this.ultraTextEditor7.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.ultraTextEditor7.Size = new System.Drawing.Size(851, 60);
            this.ultraTextEditor7.TabIndex = 10;
            // 
            // ultraLabel194
            // 
            appearance468.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance468.TextHAlignAsString = "Left";
            this.ultraLabel194.Appearance = appearance468;
            this.ultraLabel194.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel194.Location = new System.Drawing.Point(126, 773);
            this.ultraLabel194.Name = "ultraLabel194";
            this.ultraLabel194.Size = new System.Drawing.Size(425, 21);
            this.ultraLabel194.TabIndex = 181;
            this.ultraLabel194.Text = "2. Lý do khác:";
            // 
            // ultraTextEditor6
            // 
            this.ultraTextEditor6.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraTextEditor6.Location = new System.Drawing.Point(437, 733);
            this.ultraTextEditor6.Name = "ultraTextEditor6";
            this.ultraTextEditor6.Size = new System.Drawing.Size(244, 22);
            this.ultraTextEditor6.TabIndex = 9;
            // 
            // ultraLabel193
            // 
            appearance469.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance469.TextHAlignAsString = "Left";
            this.ultraLabel193.Appearance = appearance469;
            this.ultraLabel193.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel193.Location = new System.Drawing.Point(148, 737);
            this.ultraLabel193.Name = "ultraLabel193";
            this.ultraLabel193.Size = new System.Drawing.Size(225, 21);
            this.ultraLabel193.TabIndex = 179;
            this.ultraLabel193.Text = "- Số tiền chậm nộp";
            // 
            // ultraTextEditor5
            // 
            this.ultraTextEditor5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraTextEditor5.Location = new System.Drawing.Point(437, 705);
            this.ultraTextEditor5.Name = "ultraTextEditor5";
            this.ultraTextEditor5.Size = new System.Drawing.Size(244, 22);
            this.ultraTextEditor5.TabIndex = 8;
            // 
            // ultraLabel192
            // 
            appearance470.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance470.TextHAlignAsString = "Left";
            this.ultraLabel192.Appearance = appearance470;
            this.ultraLabel192.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel192.Location = new System.Drawing.Point(148, 709);
            this.ultraLabel192.Name = "ultraLabel192";
            this.ultraLabel192.Size = new System.Drawing.Size(225, 21);
            this.ultraLabel192.TabIndex = 177;
            this.ultraLabel192.Text = "- Số ngày nhận được tiền hoàn thuế";
            // 
            // ultraLabel191
            // 
            appearance471.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance471.TextHAlignAsString = "Left";
            this.ultraLabel191.Appearance = appearance471;
            this.ultraLabel191.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel191.Location = new System.Drawing.Point(148, 681);
            this.ultraLabel191.Name = "ultraLabel191";
            this.ultraLabel191.Size = new System.Drawing.Size(225, 21);
            this.ultraLabel191.TabIndex = 176;
            this.ultraLabel191.Text = "Tên cơ quan thuế quyết định hoàn thuế";
            // 
            // ultraTextEditor4
            // 
            this.ultraTextEditor4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraTextEditor4.Location = new System.Drawing.Point(437, 677);
            this.ultraTextEditor4.Name = "ultraTextEditor4";
            this.ultraTextEditor4.Size = new System.Drawing.Size(386, 22);
            this.ultraTextEditor4.TabIndex = 7;
            // 
            // ultraLabel190
            // 
            appearance472.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance472.TextHAlignAsString = "Left";
            this.ultraLabel190.Appearance = appearance472;
            this.ultraLabel190.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel190.Location = new System.Drawing.Point(148, 653);
            this.ultraLabel190.Name = "ultraLabel190";
            this.ultraLabel190.Size = new System.Drawing.Size(157, 21);
            this.ultraLabel190.TabIndex = 174;
            this.ultraLabel190.Text = "Tên cơ quan thuế cấp Cục";
            // 
            // ultraTextEditor3
            // 
            this.ultraTextEditor3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraTextEditor3.Location = new System.Drawing.Point(437, 649);
            this.ultraTextEditor3.Name = "ultraTextEditor3";
            this.ultraTextEditor3.Size = new System.Drawing.Size(386, 22);
            this.ultraTextEditor3.TabIndex = 6;
            // 
            // ultraDateTimeEditor1
            // 
            this.ultraDateTimeEditor1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraDateTimeEditor1.Location = new System.Drawing.Point(437, 622);
            this.ultraDateTimeEditor1.MaskInput = "dd/mm/yyyy";
            this.ultraDateTimeEditor1.Name = "ultraDateTimeEditor1";
            this.ultraDateTimeEditor1.Size = new System.Drawing.Size(244, 22);
            this.ultraDateTimeEditor1.TabIndex = 5;
            // 
            // ultraLabel189
            // 
            appearance473.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance473.TextHAlignAsString = "Left";
            this.ultraLabel189.Appearance = appearance473;
            this.ultraLabel189.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel189.Location = new System.Drawing.Point(148, 625);
            this.ultraLabel189.Name = "ultraLabel189";
            this.ultraLabel189.Size = new System.Drawing.Size(120, 21);
            this.ultraLabel189.TabIndex = 171;
            this.ultraLabel189.Text = "Ngày";
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReason.Location = new System.Drawing.Point(437, 584);
            this.txtReason.MaxLength = 512;
            this.txtReason.Multiline = true;
            this.txtReason.Name = "txtReason";
            this.txtReason.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtReason.Size = new System.Drawing.Size(400, 32);
            this.txtReason.TabIndex = 4;
            // 
            // ultraLabel188
            // 
            appearance474.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance474.TextHAlignAsString = "Left";
            this.ultraLabel188.Appearance = appearance474;
            this.ultraLabel188.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel188.Location = new System.Drawing.Point(148, 586);
            this.ultraLabel188.Name = "ultraLabel188";
            this.ultraLabel188.Size = new System.Drawing.Size(267, 30);
            this.ultraLabel188.TabIndex = 169;
            this.ultraLabel188.Text = "Lệnh hoàn trả khoản thu NSNN hoặc Lệnh hoàn trả kiêm bù trừ khoản thu NSNN số";
            // 
            // ultraTextEditor2
            // 
            this.ultraTextEditor2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraTextEditor2.Location = new System.Drawing.Point(437, 555);
            this.ultraTextEditor2.Name = "ultraTextEditor2";
            this.ultraTextEditor2.Size = new System.Drawing.Size(244, 22);
            this.ultraTextEditor2.TabIndex = 3;
            // 
            // ultraLabel187
            // 
            appearance475.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance475.TextHAlignAsString = "Left";
            this.ultraLabel187.Appearance = appearance475;
            this.ultraLabel187.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel187.Location = new System.Drawing.Point(148, 559);
            this.ultraLabel187.Name = "ultraLabel187";
            this.ultraLabel187.Size = new System.Drawing.Size(120, 21);
            this.ultraLabel187.TabIndex = 167;
            this.ultraLabel187.Text = "Số tiền";
            // 
            // ultraLabel185
            // 
            appearance476.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance476.TextHAlignAsString = "Left";
            this.ultraLabel185.Appearance = appearance476;
            this.ultraLabel185.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel185.Location = new System.Drawing.Point(126, 531);
            this.ultraLabel185.Name = "ultraLabel185";
            this.ultraLabel185.Size = new System.Drawing.Size(425, 21);
            this.ultraLabel185.TabIndex = 166;
            this.ultraLabel185.Text = "1. Người nộp thuế tự phát hiện số tiền thuế đã được hoàn phải nộp trả NSNN:";
            // 
            // ultraLabel184
            // 
            appearance477.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance477.TextHAlignAsString = "Left";
            this.ultraLabel184.Appearance = appearance477;
            this.ultraLabel184.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel184.Location = new System.Drawing.Point(111, 504);
            this.ultraLabel184.Name = "ultraLabel184";
            this.ultraLabel184.Size = new System.Drawing.Size(288, 21);
            this.ultraLabel184.TabIndex = 165;
            this.ultraLabel184.Text = "C. Nội dung giải thích và tài liệu đính kèm";
            // 
            // ultraTextEditor1
            // 
            this.ultraTextEditor1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraTextEditor1.Location = new System.Drawing.Point(437, 471);
            this.ultraTextEditor1.Name = "ultraTextEditor1";
            this.ultraTextEditor1.Size = new System.Drawing.Size(244, 22);
            this.ultraTextEditor1.TabIndex = 2;
            // 
            // ultraLabel183
            // 
            appearance478.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance478.TextHAlignAsString = "Left";
            this.ultraLabel183.Appearance = appearance478;
            this.ultraLabel183.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel183.Location = new System.Drawing.Point(148, 474);
            this.ultraLabel183.Name = "ultraLabel183";
            this.ultraLabel183.Size = new System.Drawing.Size(120, 21);
            this.ultraLabel183.TabIndex = 163;
            this.ultraLabel183.Text = "Số tiền chậm nộp";
            // 
            // txtDateDelay
            // 
            this.txtDateDelay.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDateDelay.Location = new System.Drawing.Point(437, 444);
            this.txtDateDelay.Name = "txtDateDelay";
            this.txtDateDelay.Size = new System.Drawing.Size(244, 22);
            this.txtDateDelay.TabIndex = 1;
            // 
            // ultraLabel182
            // 
            appearance479.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance479.TextHAlignAsString = "Left";
            this.ultraLabel182.Appearance = appearance479;
            this.ultraLabel182.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel182.Location = new System.Drawing.Point(148, 447);
            this.ultraLabel182.Name = "ultraLabel182";
            this.ultraLabel182.Size = new System.Drawing.Size(120, 21);
            this.ultraLabel182.TabIndex = 161;
            this.ultraLabel182.Text = "Số ngày nộp chậm";
            // 
            // ultraLabel3
            // 
            appearance480.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance480.TextHAlignAsString = "Left";
            this.ultraLabel3.Appearance = appearance480;
            this.ultraLabel3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel3.Location = new System.Drawing.Point(110, 411);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(288, 21);
            this.ultraLabel3.TabIndex = 160;
            this.ultraLabel3.Text = "B. Tính số tiền chậm nộp";
            // 
            // ultraLabel174
            // 
            appearance481.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance481.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance481.BorderColor = System.Drawing.Color.Black;
            appearance481.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance481.TextHAlignAsString = "Center";
            appearance481.TextVAlignAsString = "Middle";
            this.ultraLabel174.Appearance = appearance481;
            this.ultraLabel174.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel174.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel174.Location = new System.Drawing.Point(822, 361);
            this.ultraLabel174.Name = "ultraLabel174";
            this.ultraLabel174.Size = new System.Drawing.Size(163, 32);
            this.ultraLabel174.TabIndex = 144;
            // 
            // ultraLabel173
            // 
            appearance482.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance482.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance482.BorderColor = System.Drawing.Color.Black;
            appearance482.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance482.TextHAlignAsString = "Center";
            appearance482.TextVAlignAsString = "Middle";
            this.ultraLabel173.Appearance = appearance482;
            this.ultraLabel173.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel173.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel173.Location = new System.Drawing.Point(680, 361);
            this.ultraLabel173.Name = "ultraLabel173";
            this.ultraLabel173.Size = new System.Drawing.Size(143, 32);
            this.ultraLabel173.TabIndex = 143;
            // 
            // ultraLabel172
            // 
            appearance483.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance483.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance483.BorderColor = System.Drawing.Color.Black;
            appearance483.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance483.TextHAlignAsString = "Center";
            appearance483.TextVAlignAsString = "Middle";
            this.ultraLabel172.Appearance = appearance483;
            this.ultraLabel172.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel172.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel172.Location = new System.Drawing.Point(538, 361);
            this.ultraLabel172.Name = "ultraLabel172";
            this.ultraLabel172.Size = new System.Drawing.Size(143, 32);
            this.ultraLabel172.TabIndex = 142;
            // 
            // ultraLabel171
            // 
            appearance484.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance484.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance484.BorderColor = System.Drawing.Color.Black;
            appearance484.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance484.TextHAlignAsString = "Center";
            appearance484.TextVAlignAsString = "Middle";
            this.ultraLabel171.Appearance = appearance484;
            this.ultraLabel171.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel171.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel171.Location = new System.Drawing.Point(437, 361);
            this.ultraLabel171.Name = "ultraLabel171";
            this.ultraLabel171.Size = new System.Drawing.Size(102, 32);
            this.ultraLabel171.TabIndex = 141;
            // 
            // ultraLabel170
            // 
            appearance485.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance485.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance485.BorderColor = System.Drawing.Color.Black;
            appearance485.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance485.TextHAlignAsString = "Left";
            appearance485.TextVAlignAsString = "Middle";
            this.ultraLabel170.Appearance = appearance485;
            this.ultraLabel170.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel170.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel170.Location = new System.Drawing.Point(148, 361);
            this.ultraLabel170.Name = "ultraLabel170";
            this.ultraLabel170.Size = new System.Drawing.Size(290, 32);
            this.ultraLabel170.TabIndex = 140;
            // 
            // ultraLabel168
            // 
            appearance486.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance486.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance486.BorderColor = System.Drawing.Color.Black;
            appearance486.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance486.TextHAlignAsString = "Left";
            appearance486.TextVAlignAsString = "Middle";
            this.ultraLabel168.Appearance = appearance486;
            this.ultraLabel168.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel168.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel168.Location = new System.Drawing.Point(110, 330);
            this.ultraLabel168.Name = "ultraLabel168";
            this.ultraLabel168.Size = new System.Drawing.Size(875, 32);
            this.ultraLabel168.TabIndex = 139;
            this.ultraLabel168.Text = "III. Tổng hợp điều chỉnh số thuế phải nộp (tăng: +; giảm: -):";
            // 
            // ultraLabel167
            // 
            appearance487.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance487.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance487.BorderColor = System.Drawing.Color.Black;
            appearance487.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance487.TextHAlignAsString = "Center";
            appearance487.TextVAlignAsString = "Middle";
            this.ultraLabel167.Appearance = appearance487;
            this.ultraLabel167.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel167.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel167.Location = new System.Drawing.Point(110, 361);
            this.ultraLabel167.Name = "ultraLabel167";
            this.ultraLabel167.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel167.TabIndex = 138;
            this.ultraLabel167.Text = "1";
            // 
            // ultraLabel181
            // 
            appearance488.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance488.TextHAlignAsString = "Left";
            this.ultraLabel181.Appearance = appearance488;
            this.ultraLabel181.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel181.Location = new System.Drawing.Point(0, 873);
            this.ultraLabel181.Name = "ultraLabel181";
            this.ultraLabel181.Size = new System.Drawing.Size(86, 21);
            this.ultraLabel181.TabIndex = 137;
            // 
            // ultraLabel169
            // 
            appearance489.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance489.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance489.BorderColor = System.Drawing.Color.Black;
            appearance489.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance489.TextHAlignAsString = "Left";
            appearance489.TextVAlignAsString = "Middle";
            this.ultraLabel169.Appearance = appearance489;
            this.ultraLabel169.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel169.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel169.Location = new System.Drawing.Point(110, 268);
            this.ultraLabel169.Name = "ultraLabel169";
            this.ultraLabel169.Size = new System.Drawing.Size(875, 32);
            this.ultraLabel169.TabIndex = 129;
            this.ultraLabel169.Text = "II. Chỉ tiêu điều chỉnh giảm số thuế phải nộp";
            // 
            // ultraPanel40
            // 
            appearance490.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance490.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance490.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel40.Appearance = appearance490;
            this.ultraPanel40.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel40.ClientArea
            // 
            this.ultraPanel40.ClientArea.Controls.Add(this.ultraLabel1);
            this.ultraPanel40.ClientArea.Controls.Add(this.ultraLabel2);
            this.ultraPanel40.ClientArea.Controls.Add(this.ultraGrid1);
            this.ultraPanel40.Location = new System.Drawing.Point(110, 299);
            this.ultraPanel40.Name = "ultraPanel40";
            this.ultraPanel40.Size = new System.Drawing.Size(875, 32);
            this.ultraPanel40.TabIndex = 128;
            // 
            // ultraLabel1
            // 
            appearance491.BackColor = System.Drawing.Color.LightGray;
            appearance491.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance491.BorderColor = System.Drawing.Color.Black;
            appearance491.FontData.BoldAsString = "True";
            appearance491.ForeColor = System.Drawing.Color.Black;
            appearance491.TextHAlignAsString = "Center";
            appearance491.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance491;
            this.ultraLabel1.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel1.Location = new System.Drawing.Point(667, 635);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(143, 32);
            this.ultraLabel1.TabIndex = 128;
            this.ultraLabel1.Text = "(6)";
            // 
            // ultraLabel2
            // 
            appearance492.BackColor = System.Drawing.Color.LightGray;
            appearance492.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance492.BorderColor = System.Drawing.Color.Black;
            appearance492.FontData.BoldAsString = "True";
            appearance492.ForeColor = System.Drawing.Color.Black;
            appearance492.TextHAlignAsString = "Center";
            appearance492.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance492;
            this.ultraLabel2.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel2.Location = new System.Drawing.Point(809, 635);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(170, 32);
            this.ultraLabel2.TabIndex = 127;
            this.ultraLabel2.Text = "(7)";
            // 
            // ultraGrid1
            // 
            appearance493.BackColor = System.Drawing.SystemColors.Window;
            appearance493.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid1.DisplayLayout.Appearance = appearance493;
            this.ultraGrid1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ultraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid1.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            appearance494.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance494.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance494.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance494.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.GroupByBox.Appearance = appearance494;
            appearance495.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance495;
            this.ultraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance496.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance496.BackColor2 = System.Drawing.SystemColors.Control;
            appearance496.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance496.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance496;
            this.ultraGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance497.BackColor = System.Drawing.SystemColors.Window;
            appearance497.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance497;
            appearance498.BackColor = System.Drawing.SystemColors.Highlight;
            appearance498.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance498;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance499.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.CardAreaAppearance = appearance499;
            appearance500.BorderColor = System.Drawing.Color.Silver;
            appearance500.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid1.DisplayLayout.Override.CellAppearance = appearance500;
            this.ultraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance501.BackColor = System.Drawing.SystemColors.Control;
            appearance501.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance501.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance501.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance501.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance501;
            this.ultraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance502.BackColor = System.Drawing.SystemColors.Window;
            appearance502.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid1.DisplayLayout.Override.RowAppearance = appearance502;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance503.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance503;
            this.ultraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid1.DisplayLayout.UseFixedHeaders = true;
            this.ultraGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGrid1.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(873, 30);
            this.ultraGrid1.TabIndex = 29;
            this.ultraGrid1.Text = "ultraGrid1";
            // 
            // ultraPanel76
            // 
            appearance504.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance504.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance504.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel76.Appearance = appearance504;
            this.ultraPanel76.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel76.ClientArea
            // 
            this.ultraPanel76.ClientArea.Controls.Add(this.ultraLabel163);
            this.ultraPanel76.ClientArea.Controls.Add(this.ultraLabel164);
            this.ultraPanel76.ClientArea.Controls.Add(this.ultraGrid5);
            this.ultraPanel76.Location = new System.Drawing.Point(110, 237);
            this.ultraPanel76.Name = "ultraPanel76";
            this.ultraPanel76.Size = new System.Drawing.Size(875, 32);
            this.ultraPanel76.TabIndex = 127;
            // 
            // ultraLabel163
            // 
            appearance505.BackColor = System.Drawing.Color.LightGray;
            appearance505.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance505.BorderColor = System.Drawing.Color.Black;
            appearance505.FontData.BoldAsString = "True";
            appearance505.ForeColor = System.Drawing.Color.Black;
            appearance505.TextHAlignAsString = "Center";
            appearance505.TextVAlignAsString = "Middle";
            this.ultraLabel163.Appearance = appearance505;
            this.ultraLabel163.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel163.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel163.Location = new System.Drawing.Point(667, 635);
            this.ultraLabel163.Name = "ultraLabel163";
            this.ultraLabel163.Size = new System.Drawing.Size(143, 32);
            this.ultraLabel163.TabIndex = 128;
            this.ultraLabel163.Text = "(6)";
            // 
            // ultraLabel164
            // 
            appearance506.BackColor = System.Drawing.Color.LightGray;
            appearance506.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance506.BorderColor = System.Drawing.Color.Black;
            appearance506.FontData.BoldAsString = "True";
            appearance506.ForeColor = System.Drawing.Color.Black;
            appearance506.TextHAlignAsString = "Center";
            appearance506.TextVAlignAsString = "Middle";
            this.ultraLabel164.Appearance = appearance506;
            this.ultraLabel164.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel164.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel164.Location = new System.Drawing.Point(809, 635);
            this.ultraLabel164.Name = "ultraLabel164";
            this.ultraLabel164.Size = new System.Drawing.Size(170, 32);
            this.ultraLabel164.TabIndex = 127;
            this.ultraLabel164.Text = "(7)";
            // 
            // ultraGrid5
            // 
            appearance507.BackColor = System.Drawing.SystemColors.Window;
            appearance507.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid5.DisplayLayout.Appearance = appearance507;
            this.ultraGrid5.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ultraGrid5.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid5.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.ultraGrid5.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            appearance508.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance508.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance508.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance508.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid5.DisplayLayout.GroupByBox.Appearance = appearance508;
            appearance509.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid5.DisplayLayout.GroupByBox.BandLabelAppearance = appearance509;
            this.ultraGrid5.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance510.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance510.BackColor2 = System.Drawing.SystemColors.Control;
            appearance510.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance510.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid5.DisplayLayout.GroupByBox.PromptAppearance = appearance510;
            this.ultraGrid5.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid5.DisplayLayout.MaxRowScrollRegions = 1;
            appearance511.BackColor = System.Drawing.SystemColors.Window;
            appearance511.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid5.DisplayLayout.Override.ActiveCellAppearance = appearance511;
            appearance512.BackColor = System.Drawing.SystemColors.Highlight;
            appearance512.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid5.DisplayLayout.Override.ActiveRowAppearance = appearance512;
            this.ultraGrid5.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid5.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance513.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid5.DisplayLayout.Override.CardAreaAppearance = appearance513;
            appearance514.BorderColor = System.Drawing.Color.Silver;
            appearance514.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid5.DisplayLayout.Override.CellAppearance = appearance514;
            this.ultraGrid5.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid5.DisplayLayout.Override.CellPadding = 0;
            appearance515.BackColor = System.Drawing.SystemColors.Control;
            appearance515.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance515.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance515.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance515.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid5.DisplayLayout.Override.GroupByRowAppearance = appearance515;
            this.ultraGrid5.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid5.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance516.BackColor = System.Drawing.SystemColors.Window;
            appearance516.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid5.DisplayLayout.Override.RowAppearance = appearance516;
            this.ultraGrid5.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance517.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid5.DisplayLayout.Override.TemplateAddRowAppearance = appearance517;
            this.ultraGrid5.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid5.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid5.DisplayLayout.UseFixedHeaders = true;
            this.ultraGrid5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGrid5.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid5.Name = "ultraGrid5";
            this.ultraGrid5.Size = new System.Drawing.Size(873, 30);
            this.ultraGrid5.TabIndex = 29;
            this.ultraGrid5.Text = "ultraGrid1";
            // 
            // ultraLabel161
            // 
            appearance518.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance518.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance518.BorderColor = System.Drawing.Color.Black;
            appearance518.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance518.TextHAlignAsString = "Left";
            appearance518.TextVAlignAsString = "Middle";
            this.ultraLabel161.Appearance = appearance518;
            this.ultraLabel161.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel161.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel161.Location = new System.Drawing.Point(110, 206);
            this.ultraLabel161.Name = "ultraLabel161";
            this.ultraLabel161.Size = new System.Drawing.Size(875, 32);
            this.ultraLabel161.TabIndex = 126;
            this.ultraLabel161.Text = "I. Chỉ tiêu điều chỉnh tăng số thuế phải nộp";
            // 
            // ultraLabel158
            // 
            appearance519.BackColor = System.Drawing.Color.LightGray;
            appearance519.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance519.BorderColor = System.Drawing.Color.Black;
            appearance519.FontData.BoldAsString = "True";
            appearance519.ForeColor = System.Drawing.Color.Black;
            appearance519.TextHAlignAsString = "Center";
            appearance519.TextVAlignAsString = "Middle";
            this.ultraLabel158.Appearance = appearance519;
            this.ultraLabel158.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel158.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel158.Location = new System.Drawing.Point(680, 152);
            this.ultraLabel158.Name = "ultraLabel158";
            this.ultraLabel158.Size = new System.Drawing.Size(143, 55);
            this.ultraLabel158.TabIndex = 125;
            this.ultraLabel158.Text = "Số điều chỉnh";
            // 
            // ultraLabel156
            // 
            appearance520.BackColor = System.Drawing.Color.LightGray;
            appearance520.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance520.BorderColor = System.Drawing.Color.Black;
            appearance520.FontData.BoldAsString = "True";
            appearance520.ForeColor = System.Drawing.Color.Black;
            appearance520.TextHAlignAsString = "Center";
            appearance520.TextVAlignAsString = "Middle";
            this.ultraLabel156.Appearance = appearance520;
            this.ultraLabel156.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel156.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel156.Location = new System.Drawing.Point(822, 152);
            this.ultraLabel156.Name = "ultraLabel156";
            this.ultraLabel156.Size = new System.Drawing.Size(163, 55);
            this.ultraLabel156.TabIndex = 124;
            this.ultraLabel156.Text = "Chênh lệch giữa số điều chỉnh với số đã kê khai";
            // 
            // ultraLabel157
            // 
            appearance521.BackColor = System.Drawing.Color.LightGray;
            appearance521.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance521.BorderColor = System.Drawing.Color.Black;
            appearance521.FontData.BoldAsString = "True";
            appearance521.ForeColor = System.Drawing.Color.Black;
            appearance521.TextHAlignAsString = "Center";
            appearance521.TextVAlignAsString = "Middle";
            this.ultraLabel157.Appearance = appearance521;
            this.ultraLabel157.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel157.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel157.Location = new System.Drawing.Point(538, 152);
            this.ultraLabel157.Name = "ultraLabel157";
            this.ultraLabel157.Size = new System.Drawing.Size(143, 55);
            this.ultraLabel157.TabIndex = 123;
            this.ultraLabel157.Text = "Số đã kê khai";
            // 
            // ultraLabel159
            // 
            appearance522.BackColor = System.Drawing.Color.LightGray;
            appearance522.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance522.BorderColor = System.Drawing.Color.Black;
            appearance522.FontData.BoldAsString = "True";
            appearance522.ForeColor = System.Drawing.Color.Black;
            appearance522.TextHAlignAsString = "Center";
            appearance522.TextVAlignAsString = "Middle";
            this.ultraLabel159.Appearance = appearance522;
            this.ultraLabel159.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel159.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel159.Location = new System.Drawing.Point(148, 152);
            this.ultraLabel159.Name = "ultraLabel159";
            this.ultraLabel159.Size = new System.Drawing.Size(290, 55);
            this.ultraLabel159.TabIndex = 122;
            this.ultraLabel159.Text = "Chỉ tiêu điều chỉnh";
            // 
            // ultraLabel160
            // 
            appearance523.BackColor = System.Drawing.Color.LightGray;
            appearance523.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance523.BorderColor = System.Drawing.Color.Black;
            appearance523.FontData.BoldAsString = "True";
            appearance523.ForeColor = System.Drawing.Color.Black;
            appearance523.TextHAlignAsString = "Center";
            appearance523.TextVAlignAsString = "Middle";
            this.ultraLabel160.Appearance = appearance523;
            this.ultraLabel160.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel160.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel160.Location = new System.Drawing.Point(437, 152);
            this.ultraLabel160.Name = "ultraLabel160";
            this.ultraLabel160.Size = new System.Drawing.Size(102, 55);
            this.ultraLabel160.TabIndex = 121;
            this.ultraLabel160.Text = "Mã số chỉ tiêu";
            // 
            // ultraLabel162
            // 
            appearance524.BackColor = System.Drawing.Color.LightGray;
            appearance524.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance524.BorderColor = System.Drawing.Color.Black;
            appearance524.BorderColor2 = System.Drawing.Color.Transparent;
            appearance524.FontData.BoldAsString = "True";
            appearance524.ForeColor = System.Drawing.Color.Black;
            appearance524.TextHAlignAsString = "Center";
            appearance524.TextVAlignAsString = "Middle";
            this.ultraLabel162.Appearance = appearance524;
            this.ultraLabel162.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel162.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel162.Location = new System.Drawing.Point(110, 152);
            this.ultraLabel162.Name = "ultraLabel162";
            this.ultraLabel162.Size = new System.Drawing.Size(39, 55);
            this.ultraLabel162.TabIndex = 120;
            this.ultraLabel162.Text = "STT";
            // 
            // ultraLabel155
            // 
            appearance525.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance525.TextHAlignAsString = "Left";
            this.ultraLabel155.Appearance = appearance525;
            this.ultraLabel155.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel155.Location = new System.Drawing.Point(110, 125);
            this.ultraLabel155.Name = "ultraLabel155";
            this.ultraLabel155.Size = new System.Drawing.Size(288, 21);
            this.ultraLabel155.TabIndex = 119;
            this.ultraLabel155.Text = "A. Nội dung bổ sung, điều chỉnh thông tin đã kê khai:";
            // 
            // ultraLabel154
            // 
            appearance526.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance526.TextHAlignAsString = "Left";
            this.ultraLabel154.Appearance = appearance526;
            this.ultraLabel154.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel154.Location = new System.Drawing.Point(559, 71);
            this.ultraLabel154.Name = "ultraLabel154";
            this.ultraLabel154.Size = new System.Drawing.Size(287, 21);
            this.ultraLabel154.TabIndex = 9;
            this.ultraLabel154.Text = "Tờ khai thuế TNDN mẫu số 01/GTGT kỳ tính thuế:";
            // 
            // ultraLabel153
            // 
            appearance527.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance527.TextHAlignAsString = "Center";
            this.ultraLabel153.Appearance = appearance527;
            this.ultraLabel153.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel153.Location = new System.Drawing.Point(277, 71);
            this.ultraLabel153.Name = "ultraLabel153";
            this.ultraLabel153.Size = new System.Drawing.Size(288, 21);
            this.ultraLabel153.TabIndex = 8;
            this.ultraLabel153.Text = "Tờ khai thuế TNDN mẫu số 03/TNDN kỳ tính thuế:";
            // 
            // ultraLabel152
            // 
            appearance528.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance528.TextHAlignAsString = "Center";
            this.ultraLabel152.Appearance = appearance528;
            this.ultraLabel152.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel152.Location = new System.Drawing.Point(0, 46);
            this.ultraLabel152.Name = "ultraLabel152";
            this.ultraLabel152.Size = new System.Drawing.Size(1130, 19);
            this.ultraLabel152.TabIndex = 7;
            this.ultraLabel152.Text = "(Bổ sung, điều chỉnh các thông tin đã khai tại";
            // 
            // ultraLabel151
            // 
            appearance529.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance529.TextHAlignAsString = "Center";
            appearance529.TextVAlignAsString = "Bottom";
            this.ultraLabel151.Appearance = appearance529;
            this.ultraLabel151.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel151.Location = new System.Drawing.Point(0, 0);
            this.ultraLabel151.Name = "ultraLabel151";
            this.ultraLabel151.Size = new System.Drawing.Size(1130, 46);
            this.ultraLabel151.TabIndex = 4;
            this.ultraLabel151.Text = "BẢN GIẢI TRÌNH KHAI BỔ SUNG ĐIỀU CHỈNH";
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl3);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl4);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(1132, 594);
            this.ultraTabControl1.TabIndex = 1;
            ultraTab1.Key = "C";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Tờ khai chính";
            ultraTab2.Key = "03-1A";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "PL 03-1A";
            ultraTab3.Key = "03-2A";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "PL 03-2A";
            ultraTab4.Key = "BS";
            ultraTab4.TabPage = this.ultraTabPageControl4;
            ultraTab4.Text = "Bản giải trình khai bổ sung, điều chỉnh";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3,
            ultraTab4});
            this.ultraTabControl1.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.ultraTabControl1_SelectedTabChanged);
            this.ultraTabControl1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemPL1_KeyPress);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1128, 568);
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Top
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.SystemColors.ControlText;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Top";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(1140, 152);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.ToolbarsManager = this.utmDetailBaseToolBar;
            // 
            // utmDetailBaseToolBar
            // 
            appearance530.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Appearance = appearance530;
            this.utmDetailBaseToolBar.DesignerFlags = 1;
            appearance531.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.DockAreaAppearance = appearance531;
            appearance532.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.DockAreaVerticalAppearance = appearance532;
            this.utmDetailBaseToolBar.DockWithinContainer = this;
            this.utmDetailBaseToolBar.DockWithinContainerBaseType = typeof(System.Windows.Forms.Form);
            this.utmDetailBaseToolBar.FormDisplayStyle = Infragistics.Win.UltraWinToolbars.FormDisplayStyle.RoundedSizable;
            appearance533.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.HorizontalToolbarGrabHandleAppearance = appearance533;
            this.utmDetailBaseToolBar.MenuAnimationStyle = Infragistics.Win.UltraWinToolbars.MenuAnimationStyle.Slide;
            appearance534.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.MenuSettings.Appearance = appearance534;
            appearance535.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.MenuSettings.EditAppearance = appearance535;
            appearance536.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.MenuSettings.HotTrackAppearance = appearance536;
            appearance537.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.MenuSettings.IconAreaAppearance = appearance537;
            appearance538.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.MenuSettings.IconAreaExpandedAppearance = appearance538;
            appearance539.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.MenuSettings.PressedAppearance = appearance539;
            appearance540.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.MenuSettings.SideStripAppearance = appearance540;
            appearance541.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.MenuSettings.ToolAppearance = appearance541;
            this.utmDetailBaseToolBar.MiniToolbar.ToolRowCount = 1;
            appearance542.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.NavigationToolbar.MenuSettings.Appearance = appearance542;
            appearance543.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.NavigationToolbar.MenuSettings.HotTrackAppearance = appearance543;
            appearance544.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.NavigationToolbar.MenuSettings.IconAreaAppearance = appearance544;
            appearance545.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.NavigationToolbar.MenuSettings.PressedAppearance = appearance545;
            appearance546.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.ApplicationMenu2010.HeaderAppearance = appearance546;
            this.utmDetailBaseToolBar.Ribbon.ApplicationMenuButtonImage = global::Accounting.Properties.Resources.logox48;
            appearance547.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.CaptionAreaActiveAppearance = appearance547;
            appearance548.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.CaptionAreaAppearance = appearance548;
            appearance549.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.FileMenuButtonActiveAppearance = appearance549;
            appearance550.BackColor = System.Drawing.Color.Transparent;
            appearance550.BackColor2 = System.Drawing.Color.Transparent;
            appearance550.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance550.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance550.ImageAlpha = Infragistics.Win.Alpha.Transparent;
            appearance550.ImageBackgroundAlpha = Infragistics.Win.Alpha.Transparent;
            this.utmDetailBaseToolBar.Ribbon.FileMenuButtonAppearance = appearance550;
            this.utmDetailBaseToolBar.Ribbon.FileMenuButtonCaption = "";
            this.utmDetailBaseToolBar.Ribbon.FileMenuButtonCaptionVisible = false;
            appearance551.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance551.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.utmDetailBaseToolBar.Ribbon.FileMenuButtonHotTrackAppearance = appearance551;
            this.utmDetailBaseToolBar.Ribbon.GroupBorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            appearance552.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance552.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance552.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(227)))), ((int)(((byte)(228)))));
            appearance552.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance552.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance552.TextHAlignAsString = "Center";
            appearance552.TextVAlignAsString = "Middle";
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.Appearance = appearance552;
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.CanCollapse = Infragistics.Win.DefaultableBoolean.False;
            appearance553.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance553.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance553.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance553.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(146)))), ((int)(((byte)(146)))));
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.CaptionAppearance = appearance553;
            appearance554.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance554.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.CollapsedAppearance = appearance554;
            appearance555.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.EditAppearance = appearance555;
            appearance556.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance556.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(227)))), ((int)(((byte)(228)))));
            appearance556.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance556.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.HotTrackAppearance = appearance556;
            appearance557.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance557.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance557.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance557.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance557.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(146)))), ((int)(((byte)(146)))));
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.HotTrackCaptionAppearance = appearance557;
            appearance558.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance558.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance558.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.HotTrackCollapsedAppearance = appearance558;
            appearance559.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance559.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance559.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(227)))), ((int)(((byte)(228)))));
            appearance559.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance559.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.HotTrackGroupAppearance = appearance559;
            appearance560.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance560.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.PressedAppearance = appearance560;
            appearance561.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance561.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.PressedCollapsedAppearance = appearance561;
            appearance562.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance562.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance562.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(227)))), ((int)(((byte)(228)))));
            appearance562.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance562.ForeColor = System.Drawing.Color.Black;
            appearance562.ImageBackgroundAlpha = Infragistics.Win.Alpha.Transparent;
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.ToolAppearance = appearance562;
            ribbonTab1.Caption = "Chức năng";
            ribbonGroup1.Caption = "";
            buttonTool34.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool35.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool49.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            popupMenuTool1.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool2.InstanceProps.MinimumSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool2.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool3.InstanceProps.MinimumSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool3.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool11.InstanceProps.MinimumSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool11.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup1.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool34,
            buttonTool35,
            buttonTool49,
            popupMenuTool1,
            buttonTool2,
            buttonTool3,
            buttonTool11});
            ribbonTab1.Groups.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonGroup[] {
            ribbonGroup1});
            this.utmDetailBaseToolBar.Ribbon.NonInheritedRibbonTabs.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonTab[] {
            ribbonTab1});
            appearance563.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.RibbonAreaAppearance = appearance563;
            appearance564.BackColor = System.Drawing.Color.White;
            appearance564.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance564.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(219)))), ((int)(((byte)(220)))));
            appearance564.BorderColor2 = System.Drawing.Color.Transparent;
            appearance564.BorderColor3DBase = System.Drawing.Color.Transparent;
            appearance564.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(91)))));
            this.utmDetailBaseToolBar.Ribbon.TabAreaAppearance = appearance564;
            appearance565.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            appearance565.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance565.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance565.ForeColor = System.Drawing.Color.Black;
            this.utmDetailBaseToolBar.Ribbon.TabSettings.ActiveTabItemAppearance = appearance565;
            appearance566.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance566.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(219)))), ((int)(((byte)(220)))));
            appearance566.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance566.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance566.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(91)))));
            this.utmDetailBaseToolBar.Ribbon.TabSettings.Appearance = appearance566;
            appearance567.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.TabSettings.ClientAreaAppearance = appearance567;
            appearance568.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance568.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance568.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(219)))), ((int)(((byte)(220)))));
            appearance568.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance568.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            this.utmDetailBaseToolBar.Ribbon.TabSettings.HotTrackSelectedTabItemAppearance = appearance568;
            appearance569.BackColor = System.Drawing.Color.White;
            appearance569.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance569.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(236)))));
            appearance569.BorderColor2 = System.Drawing.Color.White;
            appearance569.BorderColor3DBase = System.Drawing.Color.White;
            this.utmDetailBaseToolBar.Ribbon.TabSettings.HotTrackTabItemAppearance = appearance569;
            appearance570.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance570.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance570.ForeColor = System.Drawing.Color.Black;
            this.utmDetailBaseToolBar.Ribbon.TabSettings.SelectedAppearance = appearance570;
            appearance571.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance571.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance571.ForeColor = System.Drawing.Color.Black;
            this.utmDetailBaseToolBar.Ribbon.TabSettings.SelectedTabItemAppearance = appearance571;
            appearance572.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.TabSettings.TabItemAppearance = appearance572;
            this.utmDetailBaseToolBar.Ribbon.Visible = true;
            this.utmDetailBaseToolBar.ShowFullMenusDelay = 500;
            this.utmDetailBaseToolBar.ShowShortcutsInToolTips = true;
            this.utmDetailBaseToolBar.Style = Infragistics.Win.UltraWinToolbars.ToolbarStyle.Office2007;
            ultraToolbar1.DockedColumn = 0;
            ultraToolbar1.DockedRow = 0;
            ultraToolbar1.FloatingLocation = new System.Drawing.Point(124, 412);
            ultraToolbar1.FloatingSize = new System.Drawing.Size(107, 24);
            ultraToolbar1.IsMainMenuBar = true;
            appearance573.ImageVAlign = Infragistics.Win.VAlign.Bottom;
            buttonTool39.InstanceProps.AppearancesSmall.AppearanceOnToolbar = appearance573;
            ultraToolbar1.NonInheritedTools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool39,
            buttonTool40,
            buttonTool41,
            buttonTool42,
            buttonTool43,
            buttonTool44,
            buttonTool45,
            buttonTool46,
            buttonTool47,
            buttonTool48,
            popupMenuTool8,
            popupMenuTool2,
            popupMenuTool6,
            buttonTool52,
            buttonTool53});
            ultraToolbar2.DockedColumn = 0;
            ultraToolbar2.DockedRow = 1;
            ultraToolbar2.NonInheritedTools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool37,
            buttonTool38,
            buttonTool54,
            buttonTool55,
            buttonTool56,
            buttonTool57,
            buttonTool58});
            appearance574.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance574.ImageVAlign = Infragistics.Win.VAlign.Bottom;
            ultraToolbar2.Settings.Appearance = appearance574;
            this.utmDetailBaseToolBar.Toolbars.AddRange(new Infragistics.Win.UltraWinToolbars.UltraToolbar[] {
            ultraToolbar1,
            ultraToolbar2});
            appearance575.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.ToolbarSettings.Appearance = appearance575;
            this.utmDetailBaseToolBar.ToolbarSettings.BorderStyleDocked = Infragistics.Win.UIElementBorderStyle.None;
            appearance576.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.ToolbarSettings.DockedAppearance = appearance576;
            appearance577.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.ToolbarSettings.EditAppearance = appearance577;
            appearance578.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.ToolbarSettings.FloatingAppearance = appearance578;
            appearance579.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.ToolbarSettings.HotTrackAppearance = appearance579;
            appearance580.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.ToolbarSettings.PressedAppearance = appearance580;
            appearance581.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.ToolbarSettings.ToolAppearance = appearance581;
            this.utmDetailBaseToolBar.ToolbarSettings.UseLargeImages = Infragistics.Win.DefaultableBoolean.False;
            appearance582.Image = global::Accounting.Properties.Resources.ubtnBack;
            buttonTool16.SharedPropsInternal.AppearancesSmall.Appearance = appearance582;
            buttonTool16.SharedPropsInternal.Caption = "Lùi";
            buttonTool16.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance583.Image = global::Accounting.Properties.Resources.ubtnForward;
            buttonTool17.SharedPropsInternal.AppearancesSmall.Appearance = appearance583;
            buttonTool17.SharedPropsInternal.Caption = "Tiến";
            buttonTool17.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance584.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            buttonTool18.SharedPropsInternal.AppearancesLarge.Appearance = appearance584;
            appearance585.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            buttonTool18.SharedPropsInternal.AppearancesSmall.Appearance = appearance585;
            buttonTool18.SharedPropsInternal.Caption = "Thêm";
            buttonTool18.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            buttonTool18.SharedPropsInternal.Shortcut = System.Windows.Forms.Shortcut.CtrlN;
            appearance586.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            buttonTool19.SharedPropsInternal.AppearancesLarge.Appearance = appearance586;
            appearance587.Image = global::Accounting.Properties.Resources.iEdit;
            buttonTool19.SharedPropsInternal.AppearancesSmall.Appearance = appearance587;
            buttonTool19.SharedPropsInternal.Caption = "Sửa";
            buttonTool19.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            buttonTool19.SharedPropsInternal.Shortcut = System.Windows.Forms.Shortcut.CtrlE;
            appearance588.Image = global::Accounting.Properties.Resources.ubtnSave1;
            buttonTool20.SharedPropsInternal.AppearancesLarge.Appearance = appearance588;
            appearance589.Image = global::Accounting.Properties.Resources.ubtnSave;
            buttonTool20.SharedPropsInternal.AppearancesSmall.Appearance = appearance589;
            buttonTool20.SharedPropsInternal.Caption = "Lưu";
            buttonTool20.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            buttonTool20.SharedPropsInternal.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            appearance590.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            buttonTool21.SharedPropsInternal.AppearancesLarge.Appearance = appearance590;
            appearance591.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            buttonTool21.SharedPropsInternal.AppearancesSmall.Appearance = appearance591;
            buttonTool21.SharedPropsInternal.Caption = "Xóa";
            buttonTool21.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance592.Image = global::Accounting.Properties.Resources.ubtnComeBack;
            buttonTool22.SharedPropsInternal.AppearancesSmall.Appearance = appearance592;
            buttonTool22.SharedPropsInternal.Caption = "Trở về";
            buttonTool22.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance593.Image = global::Accounting.Properties.Resources.ubtnPost;
            buttonTool23.SharedPropsInternal.AppearancesLarge.Appearance = appearance593;
            appearance594.Image = global::Accounting.Properties.Resources.ubtnPost;
            buttonTool23.SharedPropsInternal.AppearancesSmall.Appearance = appearance594;
            buttonTool23.SharedPropsInternal.Caption = "Ghi sổ";
            buttonTool23.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance595.Image = global::Accounting.Properties.Resources.ubtnReset;
            buttonTool24.SharedPropsInternal.AppearancesSmall.Appearance = appearance595;
            buttonTool24.SharedPropsInternal.Caption = "Làm mới";
            buttonTool24.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance596.Image = global::Accounting.Properties.Resources.ubtnViewList;
            buttonTool25.SharedPropsInternal.AppearancesLarge.Appearance = appearance596;
            appearance597.Image = global::Accounting.Properties.Resources.ubtnViewList;
            buttonTool25.SharedPropsInternal.AppearancesSmall.Appearance = appearance597;
            buttonTool25.SharedPropsInternal.Caption = "Duyệt";
            buttonTool25.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance598.Image = global::Accounting.Properties.Resources.ubtnHelp;
            buttonTool29.SharedPropsInternal.AppearancesSmall.Appearance = appearance598;
            buttonTool29.SharedPropsInternal.Caption = "Giúp";
            buttonTool29.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance599.Image = global::Accounting.Properties.Resources.ubtnClose;
            buttonTool30.SharedPropsInternal.AppearancesSmall.Appearance = appearance599;
            buttonTool30.SharedPropsInternal.Caption = "Đóng";
            buttonTool30.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            buttonTool30.SharedPropsInternal.Shortcut = System.Windows.Forms.Shortcut.AltF4;
            taskPaneTool4.SharedPropsInternal.Caption = "TaskPaneTool1";
            taskPaneTool5.SharedPropsInternal.Caption = "TaskPaneTool2";
            taskPaneTool6.SharedPropsInternal.Caption = "TaskPaneTool3";
            appearance600.Image = global::Accounting.Properties.Resources.boghi;
            buttonTool32.SharedPropsInternal.AppearancesLarge.Appearance = appearance600;
            appearance601.Image = global::Accounting.Properties.Resources.boghi;
            buttonTool32.SharedPropsInternal.AppearancesSmall.Appearance = appearance601;
            buttonTool32.SharedPropsInternal.Caption = "Bỏ ghi sổ";
            buttonTool36.SharedPropsInternal.Caption = "Mẫu chuẩn";
            appearance602.Image = global::Accounting.Properties.Resources.ubtnTemplate;
            popupMenuTool3.SharedPropsInternal.AppearancesSmall.Appearance = appearance602;
            popupMenuTool3.SharedPropsInternal.Caption = "Mẫu";
            popupMenuTool3.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance603.Image = global::Accounting.Properties.Resources.ubtnUtilities;
            popupMenuTool9.SharedPropsInternal.AppearancesSmall.Appearance = appearance603;
            popupMenuTool9.SharedPropsInternal.Caption = "Tiện ích     ";
            popupMenuTool9.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance604.Image = global::Accounting.Properties.Resources.ubtnPrint;
            popupMenuTool7.SharedPropsInternal.AppearancesSmall.Appearance = appearance604;
            popupMenuTool7.SharedPropsInternal.Caption = "In";
            popupMenuTool7.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            popupMenuTool7.SharedPropsInternal.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            popupMenuTool7.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool6});
            appearance605.Image = global::Accounting.Properties.Resources._2000px_Text_xml_svg;
            buttonTool4.SharedPropsInternal.AppearancesLarge.Appearance = appearance605;
            appearance606.Image = global::Accounting.Properties.Resources._2000px_Text_xml_svg;
            buttonTool4.SharedPropsInternal.AppearancesSmall.Appearance = appearance606;
            buttonTool4.SharedPropsInternal.Caption = "Xuất xml";
            fontListTool2.SharedPropsInternal.Caption = "FontListTool1";
            appearance607.Image = global::Accounting.Properties.Resources._1__10_;
            buttonTool1.SharedPropsInternal.AppearancesLarge.Appearance = appearance607;
            appearance608.Image = global::Accounting.Properties.Resources._1__10_;
            buttonTool1.SharedPropsInternal.AppearancesSmall.Appearance = appearance608;
            buttonTool1.SharedPropsInternal.Caption = "Tổng hợp KHBS";
            comboBoxTool2.SharedPropsInternal.Caption = "In tờ khai quyết toàn thuế thu nhập doanh nghiệp";
            comboBoxTool2.ValueList = valueList1;
            buttonTool7.SharedPropsInternal.Caption = "In tờ khai quyết toán thuế thu nhập doanh nghiệp ";
            this.utmDetailBaseToolBar.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool16,
            buttonTool17,
            buttonTool18,
            buttonTool19,
            buttonTool20,
            buttonTool21,
            buttonTool22,
            buttonTool23,
            buttonTool24,
            buttonTool25,
            buttonTool29,
            buttonTool30,
            taskPaneTool4,
            taskPaneTool5,
            taskPaneTool6,
            buttonTool32,
            buttonTool36,
            popupMenuTool3,
            popupMenuTool9,
            popupMenuTool7,
            buttonTool4,
            fontListTool2,
            buttonTool1,
            comboBoxTool2,
            buttonTool7});
            appearance609.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance609.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.utmDetailBaseToolBar.VerticalToolbarGrabHandleAppearance = appearance609;
            this.utmDetailBaseToolBar.ToolClick += new Infragistics.Win.UltraWinToolbars.ToolClickEventHandler(this.utmDetailBaseToolBar_ToolClick);
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Bottom
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.SystemColors.ControlText;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.InitialResizeAreaExtent = 4;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 746);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Bottom";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(1140, 4);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.ToolbarsManager = this.utmDetailBaseToolBar;
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Left
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.SystemColors.ControlText;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.InitialResizeAreaExtent = 4;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 152);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Left";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(4, 594);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.ToolbarsManager = this.utmDetailBaseToolBar;
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Right
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.SystemColors.ControlText;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.InitialResizeAreaExtent = 4;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(1136, 152);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Right";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(4, 594);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.ToolbarsManager = this.utmDetailBaseToolBar;
            // 
            // _03_TNDN_Detail_Fill_Panel
            // 
            // 
            // _03_TNDN_Detail_Fill_Panel.ClientArea
            // 
            this._03_TNDN_Detail_Fill_Panel.ClientArea.Controls.Add(this.ultraTabControl1);
            this._03_TNDN_Detail_Fill_Panel.Cursor = System.Windows.Forms.Cursors.Default;
            this._03_TNDN_Detail_Fill_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._03_TNDN_Detail_Fill_Panel.Location = new System.Drawing.Point(4, 152);
            this._03_TNDN_Detail_Fill_Panel.Name = "_03_TNDN_Detail_Fill_Panel";
            this._03_TNDN_Detail_Fill_Panel.Size = new System.Drawing.Size(1132, 594);
            this._03_TNDN_Detail_Fill_Panel.TabIndex = 10;
            // 
            // _03_TNDN_Detail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1140, 750);
            this.Controls.Add(this._03_TNDN_Detail_Fill_Panel);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Left);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Right);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Top);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "_03_TNDN_Detail";
            this.Text = "TỜ KHAI QUYẾT TOÁN THUẾ THU NHẬP DOANH NGHIỆP (Mẫu số 03/TNDN)";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this._03_TNDN_Detail_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl1.PerformLayout();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ClientArea.PerformLayout();
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSignDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameSign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCCHN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbExtensionCase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteToDateLate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFromDateLate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteExtendTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIsExtend)).EndInit();
            this.ultraPanel74.ClientArea.ResumeLayout(false);
            this.ultraPanel74.ResumeLayout(false);
            this.ultraPanel73.ClientArea.ResumeLayout(false);
            this.ultraPanel73.ResumeLayout(false);
            this.ultraPanel69.ClientArea.ResumeLayout(false);
            this.ultraPanel69.ResumeLayout(false);
            this.ultraPanel70.ClientArea.ResumeLayout(false);
            this.ultraPanel70.ResumeLayout(false);
            this.ultraPanel71.ClientArea.ResumeLayout(false);
            this.ultraPanel71.ResumeLayout(false);
            this.ultraPanel72.ClientArea.ResumeLayout(false);
            this.ultraPanel72.ResumeLayout(false);
            this.ultraPanel68.ClientArea.ResumeLayout(false);
            this.ultraPanel68.ResumeLayout(false);
            this.ultraPanel67.ClientArea.ResumeLayout(false);
            this.ultraPanel67.ResumeLayout(false);
            this.ultraPanel66.ClientArea.ResumeLayout(false);
            this.ultraPanel66.ResumeLayout(false);
            this.ultraPanel64.ClientArea.ResumeLayout(false);
            this.ultraPanel64.ResumeLayout(false);
            this.ultraPanel65.ClientArea.ResumeLayout(false);
            this.ultraPanel65.ResumeLayout(false);
            this.ultraPanel63.ClientArea.ResumeLayout(false);
            this.ultraPanel63.ResumeLayout(false);
            this.ultraPanel62.ClientArea.ResumeLayout(false);
            this.ultraPanel62.ResumeLayout(false);
            this.ultraPanel61.ClientArea.ResumeLayout(false);
            this.ultraPanel61.ResumeLayout(false);
            this.ultraPanel60.ClientArea.ResumeLayout(false);
            this.ultraPanel60.ResumeLayout(false);
            this.ultraPanel59.ClientArea.ResumeLayout(false);
            this.ultraPanel59.ResumeLayout(false);
            this.ultraPanel58.ClientArea.ResumeLayout(false);
            this.ultraPanel58.ResumeLayout(false);
            this.ultraPanel57.ClientArea.ResumeLayout(false);
            this.ultraPanel57.ResumeLayout(false);
            this.ultraPanel56.ClientArea.ResumeLayout(false);
            this.ultraPanel56.ResumeLayout(false);
            this.ultraPanel55.ClientArea.ResumeLayout(false);
            this.ultraPanel55.ResumeLayout(false);
            this.ultraPanel54.ClientArea.ResumeLayout(false);
            this.ultraPanel54.ResumeLayout(false);
            this.ultraPanel53.ClientArea.ResumeLayout(false);
            this.ultraPanel53.ResumeLayout(false);
            this.ultraPanel52.ClientArea.ResumeLayout(false);
            this.ultraPanel52.ResumeLayout(false);
            this.ultraPanel51.ClientArea.ResumeLayout(false);
            this.ultraPanel51.ResumeLayout(false);
            this.ultraPanel50.ClientArea.ResumeLayout(false);
            this.ultraPanel50.ResumeLayout(false);
            this.ultraPanel49.ClientArea.ResumeLayout(false);
            this.ultraPanel49.ResumeLayout(false);
            this.ultraPanel48.ClientArea.ResumeLayout(false);
            this.ultraPanel48.ResumeLayout(false);
            this.ultraPanel47.ClientArea.ResumeLayout(false);
            this.ultraPanel47.ResumeLayout(false);
            this.ultraPanel46.ClientArea.ResumeLayout(false);
            this.ultraPanel46.ResumeLayout(false);
            this.ultraPanel45.ClientArea.ResumeLayout(false);
            this.ultraPanel45.ResumeLayout(false);
            this.ultraPanel44.ClientArea.ResumeLayout(false);
            this.ultraPanel44.ResumeLayout(false);
            this.ultraPanel43.ClientArea.ResumeLayout(false);
            this.ultraPanel43.ResumeLayout(false);
            this.ultraPanel42.ClientArea.ResumeLayout(false);
            this.ultraPanel42.ResumeLayout(false);
            this.ultraPanel41.ResumeLayout(false);
            this.ultraPanel39.ClientArea.ResumeLayout(false);
            this.ultraPanel39.ResumeLayout(false);
            this.ultraPanel38.ClientArea.ResumeLayout(false);
            this.ultraPanel38.ResumeLayout(false);
            this.ultraPanel37.ClientArea.ResumeLayout(false);
            this.ultraPanel37.ResumeLayout(false);
            this.ultraPanel36.ClientArea.ResumeLayout(false);
            this.ultraPanel36.ResumeLayout(false);
            this.ultraPanel35.ClientArea.ResumeLayout(false);
            this.ultraPanel35.ResumeLayout(false);
            this.ultraPanel34.ClientArea.ResumeLayout(false);
            this.ultraPanel34.ResumeLayout(false);
            this.ultraPanel33.ClientArea.ResumeLayout(false);
            this.ultraPanel33.ResumeLayout(false);
            this.ultraPanel32.ClientArea.ResumeLayout(false);
            this.ultraPanel32.ResumeLayout(false);
            this.ultraPanel31.ClientArea.ResumeLayout(false);
            this.ultraPanel31.ResumeLayout(false);
            this.ultraPanel30.ClientArea.ResumeLayout(false);
            this.ultraPanel30.ResumeLayout(false);
            this.ultraPanel29.ClientArea.ResumeLayout(false);
            this.ultraPanel29.ResumeLayout(false);
            this.ultraPanel28.ClientArea.ResumeLayout(false);
            this.ultraPanel28.ResumeLayout(false);
            this.ultraPanel27.ClientArea.ResumeLayout(false);
            this.ultraPanel27.ResumeLayout(false);
            this.ultraPanel26.ClientArea.ResumeLayout(false);
            this.ultraPanel26.ResumeLayout(false);
            this.ultraPanel25.ResumeLayout(false);
            this.ultraPanel24.ClientArea.ResumeLayout(false);
            this.ultraPanel24.ResumeLayout(false);
            this.ultraPanel23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbTMIndustryType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIsRelatedTransactionEnterp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIsDependentEnterprise)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIsSMEEnterprise)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItem2)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.ultraPanel22.ClientArea.ResumeLayout(false);
            this.ultraPanel22.ResumeLayout(false);
            this.ultraPanel21.ClientArea.ResumeLayout(false);
            this.ultraPanel21.ResumeLayout(false);
            this.ultraPanel20.ClientArea.ResumeLayout(false);
            this.ultraPanel20.ResumeLayout(false);
            this.ultraPanel19.ClientArea.ResumeLayout(false);
            this.ultraPanel19.ResumeLayout(false);
            this.ultraPanel18.ClientArea.ResumeLayout(false);
            this.ultraPanel18.ResumeLayout(false);
            this.ultraPanel17.ClientArea.ResumeLayout(false);
            this.ultraPanel17.ResumeLayout(false);
            this.ultraPanel16.ClientArea.ResumeLayout(false);
            this.ultraPanel16.ResumeLayout(false);
            this.ultraPanel15.ClientArea.ResumeLayout(false);
            this.ultraPanel15.ResumeLayout(false);
            this.ultraPanel14.ClientArea.ResumeLayout(false);
            this.ultraPanel14.ResumeLayout(false);
            this.ultraPanel13.ClientArea.ResumeLayout(false);
            this.ultraPanel13.ResumeLayout(false);
            this.ultraPanel12.ClientArea.ResumeLayout(false);
            this.ultraPanel12.ResumeLayout(false);
            this.ultraPanel11.ClientArea.ResumeLayout(false);
            this.ultraPanel11.ResumeLayout(false);
            this.ultraPanel10.ClientArea.ResumeLayout(false);
            this.ultraPanel10.ResumeLayout(false);
            this.ultraPanel9.ClientArea.ResumeLayout(false);
            this.ultraPanel9.ResumeLayout(false);
            this.ultraPanel8.ClientArea.ResumeLayout(false);
            this.ultraPanel8.ResumeLayout(false);
            this.ultraPanel7.ClientArea.ResumeLayout(false);
            this.ultraPanel7.ResumeLayout(false);
            this.ultraPanel6.ClientArea.ResumeLayout(false);
            this.ultraPanel6.ResumeLayout(false);
            this.ultraPanel5.ClientArea.ResumeLayout(false);
            this.ultraPanel5.ResumeLayout(false);
            this.ultraPanel4.ClientArea.ResumeLayout(false);
            this.ultraPanel4.ResumeLayout(false);
            this.ultraPanel3.ResumeLayout(false);
            this.ultraTabPageControl3.ResumeLayout(false);
            this.ultraTabPageControl3.PerformLayout();
            this.tablePL2.ResumeLayout(false);
            this.tablePL2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemPL2_24)).EndInit();
            this.ultraTabPageControl4.ResumeLayout(false);
            this.ultraTabPageControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateDelay)).EndInit();
            this.ultraPanel40.ClientArea.ResumeLayout(false);
            this.ultraPanel40.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            this.ultraPanel76.ClientArea.ResumeLayout(false);
            this.ultraPanel76.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.utmDetailBaseToolBar)).EndInit();
            this._03_TNDN_Detail_Fill_Panel.ClientArea.ResumeLayout(false);
            this._03_TNDN_Detail_Fill_Panel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel321;
        private Infragistics.Win.Misc.UltraLabel ultraLabel320;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteToDateLate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel319;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteFromDateLate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel318;
        private Infragistics.Win.Misc.UltraLabel ultraLabel317;
        private Infragistics.Win.Misc.UltraLabel ultraLabel316;
        private Infragistics.Win.Misc.UltraLabel ultraLabel315;
        private Infragistics.Win.Misc.UltraLabel ultraLabel314;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteExtendTime;
        private Infragistics.Win.Misc.UltraLabel ultraLabel313;
        private Infragistics.Win.Misc.UltraLabel ultraLabel312;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor txtIsExtend;
        private Infragistics.Win.Misc.UltraLabel ultraLabel311;
        private Infragistics.Win.Misc.UltraLabel ultraLabel310;
        private Infragistics.Win.Misc.UltraPanel ultraPanel74;
        private Infragistics.Win.Misc.UltraLabel ultraLabel307;
        private Infragistics.Win.Misc.UltraLabel NameI;
        private Infragistics.Win.Misc.UltraLabel ultraLabel309;
        private Infragistics.Win.Misc.UltraPanel ultraPanel73;
        private Infragistics.Win.Misc.UltraLabel ultraLabel304;
        private Infragistics.Win.Misc.UltraLabel NameH;
        private Infragistics.Win.Misc.UltraLabel ultraLabel306;
        private Infragistics.Win.Misc.UltraPanel ultraPanel69;
        private Infragistics.Win.Misc.UltraPanel ultraPanel70;
        private Infragistics.Win.Misc.UltraPanel ultraPanel71;
        private Infragistics.Win.Misc.UltraLabel ultraLabel292;
        private Infragistics.Win.Misc.UltraLabel NameG3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel294;
        private Infragistics.Win.Misc.UltraLabel ultraLabel295;
        private Infragistics.Win.Misc.UltraLabel NameG2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel297;
        private Infragistics.Win.Misc.UltraLabel ultraLabel298;
        private Infragistics.Win.Misc.UltraLabel NameG1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel300;
        private Infragistics.Win.Misc.UltraPanel ultraPanel72;
        private Infragistics.Win.Misc.UltraLabel ultraLabel301;
        private Infragistics.Win.Misc.UltraLabel NameG;
        private Infragistics.Win.Misc.UltraLabel ultraLabel303;
        private Infragistics.Win.Misc.UltraPanel ultraPanel68;
        private Infragistics.Win.Misc.UltraPanel ultraPanel67;
        private Infragistics.Win.Misc.UltraPanel ultraPanel66;
        private Infragistics.Win.Misc.UltraLabel ultraLabel283;
        private Infragistics.Win.Misc.UltraLabel NameE3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel285;
        private Infragistics.Win.Misc.UltraLabel ultraLabel286;
        private Infragistics.Win.Misc.UltraLabel NameE2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel288;
        private Infragistics.Win.Misc.UltraLabel ultraLabel289;
        private Infragistics.Win.Misc.UltraLabel NameE1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel291;
        private Infragistics.Win.Misc.UltraPanel ultraPanel64;
        private Infragistics.Win.Misc.UltraPanel ultraPanel65;
        private Infragistics.Win.Misc.UltraLabel ultraLabel280;
        private Infragistics.Win.Misc.UltraLabel NameE;
        private Infragistics.Win.Misc.UltraLabel ultraLabel282;
        private Infragistics.Win.Misc.UltraLabel ultraLabel277;
        private Infragistics.Win.Misc.UltraLabel NameD3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel279;
        private Infragistics.Win.Misc.UltraPanel ultraPanel63;
        private Infragistics.Win.Misc.UltraLabel ultraLabel274;
        private Infragistics.Win.Misc.UltraLabel NameD2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel276;
        private Infragistics.Win.Misc.UltraPanel ultraPanel62;
        private Infragistics.Win.Misc.UltraLabel ultraLabel271;
        private Infragistics.Win.Misc.UltraLabel NameD1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel273;
        private Infragistics.Win.Misc.UltraPanel ultraPanel61;
        private Infragistics.Win.Misc.UltraLabel ultraLabel268;
        private Infragistics.Win.Misc.UltraLabel NameD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel270;
        private Infragistics.Win.Misc.UltraPanel ultraPanel60;
        private Infragistics.Win.Misc.UltraLabel ultraLabel265;
        private Infragistics.Win.Misc.UltraLabel NameC16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel267;
        private Infragistics.Win.Misc.UltraPanel ultraPanel59;
        private Infragistics.Win.Misc.UltraLabel ultraLabel262;
        private Infragistics.Win.Misc.UltraLabel NameC15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel264;
        private Infragistics.Win.Misc.UltraPanel ultraPanel58;
        private Infragistics.Win.Misc.UltraLabel ultraLabel259;
        private Infragistics.Win.Misc.UltraLabel NameC14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel261;
        private Infragistics.Win.Misc.UltraPanel ultraPanel57;
        private Infragistics.Win.Misc.UltraLabel ultraLabel256;
        private Infragistics.Win.Misc.UltraLabel NameC13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel258;
        private Infragistics.Win.Misc.UltraPanel ultraPanel56;
        private Infragistics.Win.Misc.UltraLabel ultraLabel253;
        private Infragistics.Win.Misc.UltraLabel NameC12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel255;
        private Infragistics.Win.Misc.UltraPanel ultraPanel55;
        private Infragistics.Win.Misc.UltraLabel ultraLabel250;
        private Infragistics.Win.Misc.UltraLabel NameC11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel252;
        private Infragistics.Win.Misc.UltraPanel ultraPanel54;
        private Infragistics.Win.Misc.UltraLabel ultraLabel247;
        private Infragistics.Win.Misc.UltraLabel NameC10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel249;
        private Infragistics.Win.Misc.UltraPanel ultraPanel53;
        private Infragistics.Win.Misc.UltraLabel ultraLabel245;
        private Infragistics.Win.Misc.UltraLabel NameC9a;
        private Infragistics.Win.Misc.UltraPanel ultraPanel52;
        private Infragistics.Win.Misc.UltraLabel ultraLabel242;
        private Infragistics.Win.Misc.UltraLabel NameC9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel244;
        private Infragistics.Win.Misc.UltraPanel ultraPanel51;
        private Infragistics.Win.Misc.UltraLabel ultraLabel239;
        private Infragistics.Win.Misc.UltraLabel NameC8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel241;
        private Infragistics.Win.Misc.UltraPanel ultraPanel50;
        private Infragistics.Win.Misc.UltraLabel ultraLabel236;
        private Infragistics.Win.Misc.UltraLabel NameC7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel238;
        private Infragistics.Win.Misc.UltraPanel ultraPanel49;
        private Infragistics.Win.Misc.UltraLabel ultraLabel233;
        private Infragistics.Win.Misc.UltraLabel NameC6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel235;
        private Infragistics.Win.Misc.UltraPanel ultraPanel48;
        private Infragistics.Win.Misc.UltraLabel ultraLabel230;
        private Infragistics.Win.Misc.UltraLabel NameC5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel232;
        private Infragistics.Win.Misc.UltraPanel ultraPanel47;
        private Infragistics.Win.Misc.UltraLabel ultraLabel227;
        private Infragistics.Win.Misc.UltraLabel NameC4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel229;
        private Infragistics.Win.Misc.UltraPanel ultraPanel46;
        private Infragistics.Win.Misc.UltraLabel ultraLabel224;
        private Infragistics.Win.Misc.UltraLabel NameC3b;
        private Infragistics.Win.Misc.UltraLabel ultraLabel226;
        private Infragistics.Win.Misc.UltraPanel ultraPanel45;
        private Infragistics.Win.Misc.UltraLabel ultraLabel221;
        private Infragistics.Win.Misc.UltraLabel NameC3a;
        private Infragistics.Win.Misc.UltraLabel ultraLabel223;
        private Infragistics.Win.Misc.UltraPanel ultraPanel44;
        private Infragistics.Win.Misc.UltraLabel ultraLabel218;
        private Infragistics.Win.Misc.UltraLabel NameC3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel220;
        private Infragistics.Win.Misc.UltraPanel ultraPanel43;
        private Infragistics.Win.Misc.UltraLabel ultraLabel215;
        private Infragistics.Win.Misc.UltraLabel NameC2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel217;
        private Infragistics.Win.Misc.UltraPanel ultraPanel42;
        private Infragistics.Win.Misc.UltraLabel ultraLabel212;
        private Infragistics.Win.Misc.UltraLabel NameC1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel214;
        private Infragistics.Win.Misc.UltraPanel ultraPanel41;
        private Infragistics.Win.Misc.UltraLabel ultraLabel209;
        private Infragistics.Win.Misc.UltraLabel NameC;
        private Infragistics.Win.Misc.UltraLabel ultraLabel211;
        private Infragistics.Win.Misc.UltraPanel ultraPanel39;
        private Infragistics.Win.Misc.UltraLabel ultraLabel206;
        private Infragistics.Win.Misc.UltraLabel NameB14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel208;
        private Infragistics.Win.Misc.UltraPanel ultraPanel38;
        private Infragistics.Win.Misc.UltraLabel ultraLabel203;
        private Infragistics.Win.Misc.UltraLabel NameB13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel205;
        private Infragistics.Win.Misc.UltraPanel ultraPanel37;
        private Infragistics.Win.Misc.UltraLabel ultraLabel200;
        private Infragistics.Win.Misc.UltraLabel NameB12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel202;
        private Infragistics.Win.Misc.UltraPanel ultraPanel36;
        private Infragistics.Win.Misc.UltraLabel ultraLabel196;
        private Infragistics.Win.Misc.UltraLabel NameB11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel198;
        private Infragistics.Win.Misc.UltraPanel ultraPanel35;
        private Infragistics.Win.Misc.UltraLabel ultraLabel180;
        private Infragistics.Win.Misc.UltraLabel NameB10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel195;
        private Infragistics.Win.Misc.UltraPanel ultraPanel34;
        private Infragistics.Win.Misc.UltraLabel ultraLabel177;
        private Infragistics.Win.Misc.UltraLabel NameB9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel179;
        private Infragistics.Win.Misc.UltraPanel ultraPanel33;
        private Infragistics.Win.Misc.UltraLabel ultraLabel150;
        private Infragistics.Win.Misc.UltraLabel NameB8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel176;
        private Infragistics.Win.Misc.UltraPanel ultraPanel32;
        private Infragistics.Win.Misc.UltraLabel ultraLabel147;
        private Infragistics.Win.Misc.UltraLabel NameB7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel149;
        private Infragistics.Win.Misc.UltraPanel ultraPanel31;
        private Infragistics.Win.Misc.UltraLabel ultraLabel144;
        private Infragistics.Win.Misc.UltraLabel NameB6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel146;
        private Infragistics.Win.Misc.UltraPanel ultraPanel30;
        private Infragistics.Win.Misc.UltraLabel ultraLabel141;
        private Infragistics.Win.Misc.UltraLabel NameB5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel143;
        private Infragistics.Win.Misc.UltraPanel ultraPanel29;
        private Infragistics.Win.Misc.UltraLabel ultraLabel138;
        private Infragistics.Win.Misc.UltraLabel NameB4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel140;
        private Infragistics.Win.Misc.UltraPanel ultraPanel28;
        private Infragistics.Win.Misc.UltraLabel ultraLabel135;
        private Infragistics.Win.Misc.UltraLabel NameB3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel137;
        private Infragistics.Win.Misc.UltraPanel ultraPanel27;
        private Infragistics.Win.Misc.UltraLabel ultraLabel132;
        private Infragistics.Win.Misc.UltraLabel NameB2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel134;
        private Infragistics.Win.Misc.UltraPanel ultraPanel26;
        private Infragistics.Win.Misc.UltraLabel ultraLabel129;
        private Infragistics.Win.Misc.UltraLabel NameB1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel131;
        private Infragistics.Win.Misc.UltraLabel ultraLabel199;
        private Infragistics.Win.Misc.UltraPanel ultraPanel25;
        private Infragistics.Win.Misc.UltraPanel ultraPanel24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel166;
        private Infragistics.Win.Misc.UltraLabel ultraLabel165;
        private Infragistics.Win.Misc.UltraLabel NameB;
        private Infragistics.Win.Misc.UltraLabel ultraLabel128;
        private Infragistics.Win.Misc.UltraLabel NameA1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel126;
        private Infragistics.Win.Misc.UltraPanel ultraPanel23;
        private Infragistics.Win.Misc.UltraLabel ultraLabel122;
        private Infragistics.Win.Misc.UltraLabel NameA;
        private Infragistics.Win.Misc.UltraLabel ultraLabel124;
        private Infragistics.Win.Misc.UltraLabel txtItem15;
        private Infragistics.Win.Misc.UltraLabel txtItem14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel119;
        private Infragistics.Win.Misc.UltraLabel ultraLabel118;
        private Infragistics.Win.Misc.UltraLabel txtItem7;
        private Infragistics.Win.Misc.UltraLabel txtItem6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel110;
        private Infragistics.Win.Misc.UltraLabel ultraLabel114;
        private Infragistics.Win.Misc.UltraLabel ultraLabel106;
        private Infragistics.Win.Misc.UltraLabel ultraLabel86;
        private Infragistics.Win.Misc.UltraLabel ultraLabel90;
        private Infragistics.Win.Misc.UltraLabel ultraLabel94;
        private Infragistics.Win.Misc.UltraLabel ultraLabel98;
        private Infragistics.Win.Misc.UltraLabel ultraLabel99;
        private Infragistics.Win.Misc.UltraLabel ultraLabel100;
        private Infragistics.Win.Misc.UltraLabel ultraLabel101;
        private Infragistics.Win.Misc.UltraLabel ultraLabel102;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbTMIndustryType;
        private Infragistics.Win.Misc.UltraLabel ultraLabel82;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor txtIsRelatedTransactionEnterp;
        private Infragistics.Win.Misc.UltraLabel ultraLabel78;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor txtIsDependentEnterprise;
        private Infragistics.Win.Misc.UltraLabel ultraLabel74;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor txtIsSMEEnterprise;
        private Infragistics.Win.Misc.UltraLabel ultraLabel70;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtItem3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel66;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor txtItem2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel62;
        private Infragistics.Win.Misc.UltraLabel ultraLabel38;
        private Infragistics.Win.Misc.UltraLabel txtDeclarationName;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.Misc.UltraPanel ultraPanel22;
        private Infragistics.Win.Misc.UltraPanel ultraPanel21;
        private Infragistics.Win.Misc.UltraPanel ultraPanel20;
        private Infragistics.Win.Misc.UltraPanel ultraPanel19;
        private Infragistics.Win.Misc.UltraPanel ultraPanel18;
        private Infragistics.Win.Misc.UltraPanel ultraPanel17;
        private Infragistics.Win.Misc.UltraPanel ultraPanel16;
        private Infragistics.Win.Misc.UltraPanel ultraPanel15;
        private Infragistics.Win.Misc.UltraPanel ultraPanel14;
        private Infragistics.Win.Misc.UltraPanel ultraPanel13;
        private Infragistics.Win.Misc.UltraPanel ultraPanel12;
        private Infragistics.Win.Misc.UltraPanel ultraPanel11;
        private Infragistics.Win.Misc.UltraPanel ultraPanel10;
        private Infragistics.Win.Misc.UltraPanel ultraPanel9;
        private Infragistics.Win.Misc.UltraPanel ultraPanel8;
        private Infragistics.Win.Misc.UltraPanel ultraPanel7;
        private Infragistics.Win.Misc.UltraPanel ultraPanel6;
        private Infragistics.Win.Misc.UltraPanel ultraPanel5;
        private Infragistics.Win.Misc.UltraPanel ultraPanel4;
        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel115;
        private Infragistics.Win.Misc.UltraLabel txtNamePL19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel117;
        private Infragistics.Win.Misc.UltraLabel ultraLabel111;
        private Infragistics.Win.Misc.UltraLabel txtNamePL18;
        private Infragistics.Win.Misc.UltraLabel ultraLabel113;
        private Infragistics.Win.Misc.UltraLabel ultraLabel107;
        private Infragistics.Win.Misc.UltraLabel txtNamePL17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel109;
        private Infragistics.Win.Misc.UltraLabel ultraLabel103;
        private Infragistics.Win.Misc.UltraLabel txtNamePL16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel105;
        private Infragistics.Win.Misc.UltraLabel ultraLabel95;
        private Infragistics.Win.Misc.UltraLabel txtNamePL15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel97;
        private Infragistics.Win.Misc.UltraLabel ultraLabel91;
        private Infragistics.Win.Misc.UltraLabel txtNamePL14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel93;
        private Infragistics.Win.Misc.UltraLabel ultraLabel87;
        private Infragistics.Win.Misc.UltraLabel txtNamePL13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel89;
        private Infragistics.Win.Misc.UltraLabel ultraLabel83;
        private Infragistics.Win.Misc.UltraLabel txtNamePL12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel85;
        private Infragistics.Win.Misc.UltraLabel ultraLabel79;
        private Infragistics.Win.Misc.UltraLabel txtNamePL11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel81;
        private Infragistics.Win.Misc.UltraLabel ultraLabel75;
        private Infragistics.Win.Misc.UltraLabel txtNamePL10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel77;
        private Infragistics.Win.Misc.UltraLabel ultraLabel71;
        private Infragistics.Win.Misc.UltraLabel txtNamePL9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel73;
        private Infragistics.Win.Misc.UltraLabel ultraLabel67;
        private Infragistics.Win.Misc.UltraLabel txtNamePL8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel69;
        private Infragistics.Win.Misc.UltraLabel ultraLabel63;
        private Infragistics.Win.Misc.UltraLabel txtNamePL7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel65;
        private Infragistics.Win.Misc.UltraLabel ultraLabel59;
        private Infragistics.Win.Misc.UltraLabel txtNamePL6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel61;
        private Infragistics.Win.Misc.UltraLabel ultraLabel55;
        private Infragistics.Win.Misc.UltraLabel txtNamePL5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel57;
        private Infragistics.Win.Misc.UltraLabel ultraLabel51;
        private Infragistics.Win.Misc.UltraLabel txtNamePL4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel53;
        private Infragistics.Win.Misc.UltraLabel ultraLabel47;
        private Infragistics.Win.Misc.UltraLabel txtNamePL3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel49;
        private Infragistics.Win.Misc.UltraLabel ultraLabel43;
        private Infragistics.Win.Misc.UltraLabel txtNamePL2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel45;
        private Infragistics.Win.Misc.UltraLabel ultraLabel39;
        private Infragistics.Win.Misc.UltraLabel txtNamePL1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel41;
        private Infragistics.Win.Misc.UltraLabel ultraLabel36;
        private Infragistics.Win.Misc.UltraLabel ultraLabel34;
        private Infragistics.Win.Misc.UltraLabel ultraLabel35;
        private Infragistics.Win.Misc.UltraLabel ultraLabel33;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.Misc.UltraLabel ultraLabel26;
        private Infragistics.Win.Misc.UltraLabel ultraLabel27;
        private Infragistics.Win.Misc.UltraLabel ultraLabel28;
        private Infragistics.Win.Misc.UltraLabel ultraLabel29;
        private Infragistics.Win.Misc.UltraLabel ultraLabel30;
        private Infragistics.Win.Misc.UltraLabel ultraLabel31;
        private Infragistics.Win.Misc.UltraLabel ultraLabel32;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel194;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel193;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel192;
        private Infragistics.Win.Misc.UltraLabel ultraLabel191;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel190;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel189;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.Misc.UltraLabel ultraLabel188;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel187;
        private Infragistics.Win.Misc.UltraLabel ultraLabel185;
        private Infragistics.Win.Misc.UltraLabel ultraLabel184;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel183;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDateDelay;
        private Infragistics.Win.Misc.UltraLabel ultraLabel182;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel174;
        private Infragistics.Win.Misc.UltraLabel ultraLabel173;
        private Infragistics.Win.Misc.UltraLabel ultraLabel172;
        private Infragistics.Win.Misc.UltraLabel ultraLabel171;
        private Infragistics.Win.Misc.UltraLabel ultraLabel170;
        private Infragistics.Win.Misc.UltraLabel ultraLabel168;
        private Infragistics.Win.Misc.UltraLabel ultraLabel167;
        private Infragistics.Win.Misc.UltraLabel ultraLabel181;
        private Infragistics.Win.Misc.UltraLabel ultraLabel169;
        private Infragistics.Win.Misc.UltraPanel ultraPanel40;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel76;
        private Infragistics.Win.Misc.UltraLabel ultraLabel163;
        private Infragistics.Win.Misc.UltraLabel ultraLabel164;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel161;
        private Infragistics.Win.Misc.UltraLabel ultraLabel158;
        private Infragistics.Win.Misc.UltraLabel ultraLabel156;
        private Infragistics.Win.Misc.UltraLabel ultraLabel157;
        private Infragistics.Win.Misc.UltraLabel ultraLabel159;
        private Infragistics.Win.Misc.UltraLabel ultraLabel160;
        private Infragistics.Win.Misc.UltraLabel ultraLabel162;
        private Infragistics.Win.Misc.UltraLabel ultraLabel155;
        private Infragistics.Win.Misc.UltraLabel ultraLabel154;
        private Infragistics.Win.Misc.UltraLabel ultraLabel153;
        private Infragistics.Win.Misc.UltraLabel ultraLabel152;
        private Infragistics.Win.Misc.UltraLabel ultraLabel151;
        public Infragistics.Win.UltraWinToolbars.UltraToolbarsManager utmDetailBaseToolBar;
        private Infragistics.Win.Misc.UltraPanel _03_TNDN_Detail_Fill_Panel;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Left;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Right;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Bottom;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Top;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemI;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemH;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemG2;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemG3;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemG1;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemG;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemE2;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemE3;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemE1;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemD3;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemE;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemD2;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemD1;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemD;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemC16;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemC15;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemC14;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemC13;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemC12;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemC11;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemC10;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemC9a;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemC9;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemC8;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemC7;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemC6;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemC5;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemC4;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemC3b;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemC3a;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemC3;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemC2;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemC1;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemB14;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemB13;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemB12;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemB11;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemB10;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemB9;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemB8;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemB7;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemB6;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemB5;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemB4;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemB3;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemB2;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemB1;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemA1;
        private Infragistics.Win.Misc.UltraLabel txtItem1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbExtensionCase;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtLateAmount;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtLateDays;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtNotExtendTaxAmount;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtExtendTaxAmount;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL19;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL18;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL17;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL16;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL15;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL14;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL13;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL12;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL11;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL10;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL9;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL8;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL7;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL6;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL5;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL4;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL3;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL1;
        private System.Windows.Forms.TableLayoutPanel tablePL2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_65;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_55;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_45;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_35;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_64;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_54;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_44;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_34;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_63;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_53;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_43;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_33;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_62;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_52;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_42;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_32;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_61;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_51;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_41;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_31;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_60;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_50;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_40;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItemPL2_30;
        private System.Windows.Forms.Label label13;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtItem5;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel330;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtSignDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel329;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNameSign;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCCHN;
        private Infragistics.Win.Misc.UltraLabel ultraLabel328;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel327;
        private Infragistics.Win.Misc.UltraLabel ultraLabel326;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtItemPL2_10;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtItemPL2_11;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtItemPL2_12;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtItemPL2_13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtItemPL2_14;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtItemPL2_20;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtItemPL2_21;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtItemPL2_22;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtItemPL2_23;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtItemPL2_24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
    }
}