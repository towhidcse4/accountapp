﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting
{
    public partial class TaxPeriod_03_TNDN : Form
    {
        public List<TMAppendixList> lstTMAppendixList = new List<TMAppendixList>();

        public TaxPeriod_03_TNDN()
        {
            InitializeComponent();
            cbbJob.DropDownStyle = DropDownStyle.DropDownList;
            txtYear.Value = DateTime.Now.Year;
            LoadGUI();
            cbbJob.Value = 0;
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
        private bool CheckErr()
        {
            if (int.Parse(txtYear.Value.ToString()) > DateTime.Now.Year)
            {
                MSG.Error("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                return false;
            }
            if (int.Parse(txtYear.Value.ToString()) < dteDateFrom.MinDate.Year)
            {
                MSG.Error("Năm không tồn tại");
                return false;
            }
            if (cbbJob.Value == null)
            {
                MSG.Error("Danh mục ngành nghề không được để trống");
                return false;
            }
            return true;
        }
        private void txtYear_ValueChanged(object sender, EventArgs e)
        {
            DateTime t1 = dteDateFrom.DateTime;
            DateTime t2 = dteDateTo.DateTime;
            try
            {
                dteDateFrom.Value = new DateTime((int)txtYear.Value, 1, 1);
                dteDateTo.Value = new DateTime((int)txtYear.Value, 12, DateTime.DaysInMonth((int)txtYear.Value, 12));
            }
            catch(Exception ex) { };
        }

        private void ultraOptionSet2_ValueChanged(object sender, EventArgs e)
        {
            if (ultraOptionSet2.CheckedIndex == 1)
            {
                ultraOptionSet2.CheckedIndex = 0;
                return;//
                ultraLabel4.Visible = true;
                txtNumber.Visible = true;
                ultraLabel5.Visible = true;
                dteKHBS.Visible = true;
            }
            else
            {
                ultraLabel4.Visible = false;
                txtNumber.Visible = false;
                ultraLabel5.Visible = false;
                dteKHBS.Visible = false;
            }
        }

        private void LoadGUI()
        {
            #region ccb ngành nghề
            cbbJob.Items.Add(0, "Ngành hàng sản xuất, kinh doanh thông thường");
            cbbJob.Items.Add(1, "Từ hoạt động thăm dò, phát triển mỏ và khai thác dầu, khí thiên nhiên");
            cbbJob.Items.Add(2, "Từ hoạt động xổ số kiến thiết của các công ty xổ số kiến thiết");
            #endregion
            #region Config grid
            lstTMAppendixList = Utils.ITMAppendixListService.GetAppendixListByTypeGroup(120);
            foreach (var item in lstTMAppendixList)
            {
                item.Status = false;
            }
            uGrid.SetDataBinding(lstTMAppendixList, "");
            Utils.ConfigGrid(uGrid, ConstDatabase.TMAppendixList_TableName);
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;
            uGrid.DisplayLayout.Bands[0].Columns["Status"].CellClickAction = CellClickAction.EditAndSelectText;
            #endregion
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!CheckErr())
            {
                return;
            }
            
            string item01 = txtYear.Value + " từ " + dteDateFrom.DateTime.ToString("dd/MM/yyyy") + " đến " + dteDateTo.DateTime.ToString("dd/MM/yyyy");
            bool item02 = false;
            if (ultraOptionSet2.Value.ToString() == "0")
            {
                item02 = true;
            }
            if (chkPhuLuc.Checked)
            {

            }
            if (ultraOptionSet2.Value.ToString() == "0")
            {
                item02 = true;
                List<TM03TNDN> lstTm = Utils.ListTM03TNDN.Where(n => n.Year == (int)txtYear.Value).ToList();
                if (lstTm.Any(n => n.Year == (int)txtYear.Value && n.IsFirstDeclaration))
                {
                    if (DialogResult.Yes == MessageBox.Show("Đã tồn tại tờ khai này trong kỳ trên. Bạn có muốn mở không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        TM03TNDN tM03TNDN = lstTm.FirstOrDefault();
                        _03_TNDN_Detail TNDN03_Detail_ = new _03_TNDN_Detail(tM03TNDN, true);
                        this.Hide();
                        this.Close();
                        this.Dispose();
                        TNDN03_Detail_.Show();
                       
                    }
                    return;
                }
                else
                {
                }
            }
            else
            {
                if (txtNumber.Value.ToString() == "0")
                {
                    MSG.Error("Chưa nhập lần bổ sung");
                    return;
                }
                MSG.Information("");
                return;
                //TNDN03_Detail.item03 = txtNumber.Value.ToString();
                //TNDN03_Detail.DateBS = dteKHBS.DateTime;
            }
            _03_TNDN_Detail TNDN03_Detail = new _03_TNDN_Detail(new TM03TNDN(), false);
            TNDN03_Detail.Career = (int)cbbJob.Value;
            TNDN03_Detail.year = (int)txtYear.Value;
            TNDN03_Detail.FromDate = dteDateFrom.DateTime;
            TNDN03_Detail.ToDate = dteDateTo.DateTime;
            TNDN03_Detail.lstTMAppendixList_ = lstTMAppendixList;
            TNDN03_Detail.item01 = item01;
            TNDN03_Detail.item02 = item02;
            TNDN03_Detail.LoadInitialize();
            this.Hide();
            this.Close();
            this.Dispose();
            TNDN03_Detail.Show();
            
        }

        private void TaxPeriod_03_TNDN_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (Form)sender;
            frm.Dispose();

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}
