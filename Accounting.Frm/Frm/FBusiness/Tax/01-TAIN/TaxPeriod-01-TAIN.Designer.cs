﻿namespace Accounting
{
    partial class TaxPeriod_01_TAIN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TaxPeriod_01_TAIN));
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.cbbMonth = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtDay = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAdditionTime = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.dteKHBS = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtYear = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.ultraOptionSet2 = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraOptionSet1 = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.ultraPanel4 = new Infragistics.Win.Misc.UltraPanel();
            this.dteDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dteDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdditionTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteKHBS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet1)).BeginInit();
            this.ultraPanel4.ClientArea.SuspendLayout();
            this.ultraPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDateFrom)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.cbbMonth);
            this.ultraPanel1.ClientArea.Controls.Add(this.txtDay);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel6);
            this.ultraPanel1.ClientArea.Controls.Add(this.txtAdditionTime);
            this.ultraPanel1.ClientArea.Controls.Add(this.dteKHBS);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel5);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel4);
            this.ultraPanel1.ClientArea.Controls.Add(this.txtYear);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraOptionSet2);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel2);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel1);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraOptionSet1);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(523, 136);
            this.ultraPanel1.TabIndex = 0;
            // 
            // cbbMonth
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbMonth.DisplayLayout.Appearance = appearance1;
            this.cbbMonth.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbMonth.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbMonth.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbMonth.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.cbbMonth.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbMonth.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.cbbMonth.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbMonth.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbMonth.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbMonth.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.cbbMonth.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbMonth.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.cbbMonth.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbMonth.DisplayLayout.Override.CellAppearance = appearance8;
            this.cbbMonth.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbMonth.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbMonth.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.cbbMonth.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.cbbMonth.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbMonth.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.cbbMonth.DisplayLayout.Override.RowAppearance = appearance11;
            this.cbbMonth.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbMonth.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.cbbMonth.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbMonth.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbMonth.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbMonth.Location = new System.Drawing.Point(203, 46);
            this.cbbMonth.Name = "cbbMonth";
            this.cbbMonth.Size = new System.Drawing.Size(87, 22);
            this.cbbMonth.TabIndex = 71;
            this.cbbMonth.ValueChanged += new System.EventHandler(this.cbbMonth_ValueChanged);
            // 
            // txtDay
            // 
            this.txtDay.Location = new System.Drawing.Point(50, 47);
            this.txtDay.Name = "txtDay";
            this.txtDay.PromptChar = ' ';
            this.txtDay.Size = new System.Drawing.Size(74, 21);
            this.txtDay.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.txtDay.TabIndex = 0;
            this.txtDay.Visible = false;
            // 
            // ultraLabel6
            // 
            this.ultraLabel6.Location = new System.Drawing.Point(12, 51);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(32, 17);
            this.ultraLabel6.TabIndex = 70;
            this.ultraLabel6.Text = "Ngày";
            this.ultraLabel6.Visible = false;
            // 
            // txtAdditionTime
            // 
            this.txtAdditionTime.Location = new System.Drawing.Point(173, 95);
            this.txtAdditionTime.Name = "txtAdditionTime";
            this.txtAdditionTime.PromptChar = ' ';
            this.txtAdditionTime.Size = new System.Drawing.Size(75, 21);
            this.txtAdditionTime.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.txtAdditionTime.TabIndex = 3;
            this.txtAdditionTime.Visible = false;
            // 
            // dteKHBS
            // 
            this.dteKHBS.Location = new System.Drawing.Point(355, 95);
            this.dteKHBS.MaskInput = "dd/mm/yyyy";
            this.dteKHBS.Name = "dteKHBS";
            this.dteKHBS.Size = new System.Drawing.Size(98, 21);
            this.dteKHBS.TabIndex = 4;
            this.dteKHBS.Visible = false;
            // 
            // ultraLabel5
            // 
            this.ultraLabel5.Location = new System.Drawing.Point(254, 99);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(95, 17);
            this.ultraLabel5.TabIndex = 9;
            this.ultraLabel5.Text = "Ngày lập KHBS";
            this.ultraLabel5.Visible = false;
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.Location = new System.Drawing.Point(130, 99);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(37, 17);
            this.ultraLabel4.TabIndex = 8;
            this.ultraLabel4.Text = "Lần";
            this.ultraLabel4.Visible = false;
            // 
            // txtYear
            // 
            this.txtYear.Location = new System.Drawing.Point(362, 47);
            this.txtYear.MaxValue = 9999;
            this.txtYear.MinValue = 1;
            this.txtYear.Name = "txtYear";
            this.txtYear.PromptChar = ' ';
            this.txtYear.Size = new System.Drawing.Size(91, 21);
            this.txtYear.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.txtYear.TabIndex = 2;
            this.txtYear.ValueChanged += new System.EventHandler(this.txtYear_ValueChanged);
            // 
            // ultraOptionSet2
            // 
            this.ultraOptionSet2.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraOptionSet2.CheckedIndex = 0;
            this.ultraOptionSet2.Enabled = false;
            valueListItem3.DataValue = 0;
            valueListItem3.DisplayText = "Tờ khai lần đầu";
            valueListItem4.DataValue = 1;
            valueListItem4.DisplayText = "Tờ khai bổ sung";
            this.ultraOptionSet2.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem4});
            this.ultraOptionSet2.Location = new System.Drawing.Point(12, 85);
            this.ultraOptionSet2.Name = "ultraOptionSet2";
            this.ultraOptionSet2.Size = new System.Drawing.Size(125, 35);
            this.ultraOptionSet2.TabIndex = 3;
            this.ultraOptionSet2.Text = "Tờ khai lần đầu";
            this.ultraOptionSet2.ValueChanged += new System.EventHandler(this.ultraOptionSet2_ValueChanged);
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(324, 51);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(32, 17);
            this.ultraLabel2.TabIndex = 3;
            this.ultraLabel2.Text = "Năm";
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(151, 51);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(46, 17);
            this.ultraLabel1.TabIndex = 1;
            this.ultraLabel1.Text = "Tháng";
            // 
            // ultraOptionSet1
            // 
            this.ultraOptionSet1.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraOptionSet1.CheckedIndex = 0;
            valueListItem1.DataValue = 0;
            valueListItem1.DisplayText = "Tờ khai tháng";
            valueListItem2.DataValue = 1;
            valueListItem2.DisplayText = "Tờ khai lần phát sinh";
            this.ultraOptionSet1.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.ultraOptionSet1.Location = new System.Drawing.Point(12, 12);
            this.ultraOptionSet1.Name = "ultraOptionSet1";
            this.ultraOptionSet1.Size = new System.Drawing.Size(222, 19);
            this.ultraOptionSet1.TabIndex = 0;
            this.ultraOptionSet1.Text = "Tờ khai tháng";
            this.ultraOptionSet1.ValueChanged += new System.EventHandler(this.ultraOptionSet1_ValueChanged);
            // 
            // ultraPanel4
            // 
            // 
            // ultraPanel4.ClientArea
            // 
            this.ultraPanel4.ClientArea.Controls.Add(this.dteDateTo);
            this.ultraPanel4.ClientArea.Controls.Add(this.dteDateFrom);
            this.ultraPanel4.ClientArea.Controls.Add(this.btnClose);
            this.ultraPanel4.ClientArea.Controls.Add(this.btnSave);
            this.ultraPanel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel4.Location = new System.Drawing.Point(0, 136);
            this.ultraPanel4.Name = "ultraPanel4";
            this.ultraPanel4.Size = new System.Drawing.Size(523, 55);
            this.ultraPanel4.TabIndex = 13;
            // 
            // dteDateTo
            // 
            this.dteDateTo.Enabled = false;
            this.dteDateTo.Location = new System.Drawing.Point(123, 16);
            this.dteDateTo.MaskInput = "dd/mm/yyyy";
            this.dteDateTo.Name = "dteDateTo";
            this.dteDateTo.Size = new System.Drawing.Size(96, 21);
            this.dteDateTo.TabIndex = 63;
            this.dteDateTo.Visible = false;
            // 
            // dteDateFrom
            // 
            this.dteDateFrom.Enabled = false;
            this.dteDateFrom.Location = new System.Drawing.Point(21, 16);
            this.dteDateFrom.MaskInput = "dd/mm/yyyy";
            this.dteDateFrom.Name = "dteDateFrom";
            this.dteDateFrom.Size = new System.Drawing.Size(96, 21);
            this.dteDateFrom.TabIndex = 62;
            this.dteDateFrom.Visible = false;
            // 
            // btnClose
            // 
            appearance13.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance13;
            this.btnClose.Location = new System.Drawing.Point(425, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 61;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance14.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance14;
            this.btnSave.Location = new System.Drawing.Point(344, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 60;
            this.btnSave.Text = "Đồng ý";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // TaxPeriod_01_TAIN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 191);
            this.Controls.Add(this.ultraPanel4);
            this.Controls.Add(this.ultraPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TaxPeriod_01_TAIN";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chọn kỳ tính thuế";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TaxPeriod_01_TAIN_FormClosed);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ClientArea.PerformLayout();
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdditionTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteKHBS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet1)).EndInit();
            this.ultraPanel4.ClientArea.ResumeLayout(false);
            this.ultraPanel4.ClientArea.PerformLayout();
            this.ultraPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dteDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDateFrom)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtAdditionTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteKHBS;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtYear;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet ultraOptionSet2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet ultraOptionSet1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel4;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtDay;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbMonth;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDateTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDateFrom;
    }
}