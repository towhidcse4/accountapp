﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinTabControl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Accounting
{

    public partial class _01_TAIN_Detail : Form
    {
        #region khai báo
        public string DeclarationTerm = "";
        public bool IsFirst = false;
        public string AdditionalTime = "";
        public DateTime FromDate = DateTime.Now;
        public DateTime ToDate = DateTime.Now;
        public int Day = DateTime.Now.Day;
        public int Month = DateTime.Now.Month;
        public int Year = DateTime.Now.Year;
        public DateTime KHBS = DateTime.Now;
        public bool isDeclaredArising = false;
        public bool isDeclaredMonth = false;
        private bool _isEdit;
        private TM01TAIN _select;
        public bool checkSave = false;
        //public ISystemOptionService _ISystemOptionService { get { return IoC.Resolve<ISystemOptionService>(); } }

        List<TM01TAINDetail> lst = new List<TM01TAINDetail>();
        List<MaterialGoodsResourceTaxGroup> lstMaterialGoodsResourceTaxGroup = new List<MaterialGoodsResourceTaxGroup>();
        List<MaterialGoodsResourceTaxGroup> lstMaterialGoodsResourceTaxGroup1 = new List<MaterialGoodsResourceTaxGroup>();
        #endregion

        public _01_TAIN_Detail(TM01TAIN temp, bool isEdit)
        {
            InitializeComponent();
            lstMaterialGoodsResourceTaxGroup = new List<MaterialGoodsResourceTaxGroup>();
            lstMaterialGoodsResourceTaxGroup1 = new List<MaterialGoodsResourceTaxGroup>();
            _select = temp;
            _isEdit = isEdit;
            if (isEdit)
            {
                ObjandGUI(temp, false);
                LoadReadOnly(true);
                utmDetailBaseToolBar.Tools["mnbtnSave"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = true;
                utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Enabled = true;

            }
            else
            {
                LoadReadOnly(false);
                utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["mnbtnDelete"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Enabled = false;
            }

            Utils.ClearCacheByType<SystemOption>();

            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
                this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
            }


            LoadCbb();
            FormatText();
            utmDetailBaseToolBar.Ribbon.NonInheritedRibbonTabs[0].Caption = @"Chức năng";
        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams handleParam = base.CreateParams;
                handleParam.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED       
                return handleParam;
            }
        }

        public IEnumerable<Control> GetAll(Control control, System.Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }

        #region set read only
        public void LoadReadOnly(bool isReadOnly)
        {
            utmDetailBaseToolBar.Tools["mnbtnDelete"].SharedProps.Enabled = isReadOnly;
            tsmAdd.Visible = !isReadOnly;
            tsmDelete.Visible = !isReadOnly;
            tsmAdd2.Visible = !isReadOnly;
            tsmDelete2.Visible = !isReadOnly;
            tsmAdd3.Visible = !isReadOnly;
            tsmDelete3.Visible = !isReadOnly;

            foreach (var item in GetAll(this, typeof(UltraTextEditor)))
            {
                try
                {
                    if (item is UltraTextEditor)
                    {
                        (item as UltraTextEditor).ReadOnly = isReadOnly;
                    }
                }
                catch (Exception ex)
                {

                }


            }
            foreach (var item in GetAll(this, typeof(UltraCombo)))
            {
                try
                {
                    if (item is UltraCombo)
                    {
                        (item as UltraCombo).ReadOnly = isReadOnly;
                    }
                }
                catch (Exception ex)
                {

                }

            }
            for (int i = 0; i < tableLayoutPanel1.RowCount; i++)
            {
                var control = tableLayoutPanel1.GetControlFromPosition(0, i);
                var control1 = tableLayoutPanel1.GetControlFromPosition(4, i);
                var control2 = tableLayoutPanel1.GetControlFromPosition(7, i);
                var control3 = tableLayoutPanel1.GetControlFromPosition(9, i);
                var control4 = tableLayoutPanel1.GetControlFromPosition(5, i);
                var control5 = tableLayoutPanel1.GetControlFromPosition(6, i);
                var control6 = tableLayoutPanel1.GetControlFromPosition(8, i);
                var control7 = tableLayoutPanel1.GetControlFromPosition(3, i);
                if (control is UltraTextEditor)
                {
                    UltraTextEditor ultraTextEditor = control as UltraTextEditor;
                    ultraTextEditor.ReadOnly = true;
                }
                if (control1 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control1 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = isReadOnly;
                }
                if (control2 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control2 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = true;
                }
                if (control3 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control3 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = true;
                }
                if (control4 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control4 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = true;
                }
                if (control5 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control5 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = isReadOnly;
                }
                if (control6 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control6 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = isReadOnly;
                }
                if (control7 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control7 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = isReadOnly;
                }
            }
            for (int i = 0; i < tableLayoutPanel2.RowCount; i++)
            {
                var control = tableLayoutPanel2.GetControlFromPosition(0, i);
                var control1 = tableLayoutPanel2.GetControlFromPosition(4, i);
                var control2 = tableLayoutPanel2.GetControlFromPosition(7, i);
                var control3 = tableLayoutPanel2.GetControlFromPosition(9, i);
                var control4 = tableLayoutPanel2.GetControlFromPosition(5, i);
                var control5 = tableLayoutPanel2.GetControlFromPosition(6, i);
                var control6 = tableLayoutPanel2.GetControlFromPosition(8, i);
                var control7 = tableLayoutPanel2.GetControlFromPosition(3, i);
                if (control is UltraTextEditor)
                {
                    UltraTextEditor ultraTextEditor = control as UltraTextEditor;
                    ultraTextEditor.ReadOnly = true;
                }
                if (control1 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control1 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = isReadOnly;
                }
                if (control2 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control2 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = true;
                }
                if (control3 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control3 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = true;
                }
                if (control4 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control4 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = true;
                }
                if (control5 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control5 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = isReadOnly;
                }
                if (control6 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control6 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = isReadOnly;
                }
                if (control7 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control7 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = isReadOnly;
                }
            }
            for (int i = 0; i < tableLayoutPanel3.RowCount; i++)
            {
                var control = tableLayoutPanel3.GetControlFromPosition(0, i);
                var control1 = tableLayoutPanel3.GetControlFromPosition(4, i);
                var control2 = tableLayoutPanel3.GetControlFromPosition(7, i);
                var control3 = tableLayoutPanel3.GetControlFromPosition(9, i);
                var control4 = tableLayoutPanel3.GetControlFromPosition(5, i);
                var control5 = tableLayoutPanel3.GetControlFromPosition(6, i);
                var control6 = tableLayoutPanel3.GetControlFromPosition(8, i);
                var control7 = tableLayoutPanel3.GetControlFromPosition(3, i);
                if (control is UltraTextEditor)
                {
                    UltraTextEditor ultraTextEditor = control as UltraTextEditor;
                    ultraTextEditor.ReadOnly = true;
                }
                if (control1 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control1 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = isReadOnly;
                }
                if (control2 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control2 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = true;
                }
                if (control3 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control3 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = true;
                }
                if (control4 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control4 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = true;
                }
                if (control5 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control5 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = isReadOnly;
                }
                if (control6 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control6 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = isReadOnly;
                }
                if (control7 is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control7 as UltraMaskedEdit;
                    ultraTextEditor.ReadOnly = isReadOnly;
                }
            }
        }
        #endregion

        #region FormatText
        private void FormatText()
        {
            Utils.FormatNumberic(txtQuantity1, ConstDatabase.Format_Quantity);
            Utils.FormatNumberic(txtUnitPrice1, ConstDatabase.Format_Quantity);
            Utils.FormatNumberic(txtTaxRate1, ConstDatabase.Format_Quantity);
            Utils.FormatNumberic(txtResourceTaxTaxAmountUnit1, ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(txtResourceTaxAmountIncurration1, ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(txtResourceTaxAmountDeduction1, ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(txtResourceTaxAmount1, ConstDatabase.Format_TienVND);

            Utils.FormatNumberic(txtQuantity2, ConstDatabase.Format_Quantity);
            Utils.FormatNumberic(txtUnitPrice2, ConstDatabase.Format_Quantity);
            Utils.FormatNumberic(txtTaxRate2, ConstDatabase.Format_Quantity);
            Utils.FormatNumberic(txtResourceTaxTaxAmountUnit2, ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(txtResourceTaxAmountIncurration2, ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(txtResourceTaxAmountDeduction2, ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(txtResourceTaxAmount2, ConstDatabase.Format_TienVND);

            Utils.FormatNumberic(txtQuantity3, ConstDatabase.Format_Quantity);
            Utils.FormatNumberic(txtUnitPrice3, ConstDatabase.Format_Quantity);
            Utils.FormatNumberic(txtTaxRate3, ConstDatabase.Format_Quantity);
            Utils.FormatNumberic(txtResourceTaxTaxAmountUnit3, ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(txtResourceTaxAmountIncurration3, ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(txtResourceTaxAmountDeduction3, ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(txtResourceTaxAmount3, ConstDatabase.Format_TienVND);


        }
        #endregion

        #region Config MaskedEdit
        private void ConfigMaskedEdit(UltraMaskedEdit txt)
        {
            txt.AutoSize = false;
            txt.Dock = DockStyle.Fill;
            txt.Margin = new Padding(0);
            txt.PromptChar = ' ';
            txt.DisplayMode = MaskMode.IncludeLiterals;
            txt.EditAs = EditAsType.Double;
            txt.InputMask = "n,nnn,nnn,nnn,nnn,nnn";

        }
        private void ConfigMaskedEdit1(UltraMaskedEdit txt)
        {
            txt.AutoSize = false;
            txt.Dock = DockStyle.Fill;
            txt.Margin = new Padding(0);
            txt.PromptChar = ' ';
            txt.DisplayMode = MaskMode.IncludeLiterals;
            txt.EditAs = EditAsType.Double;
            txt.InputMask = "n,nnn,nnn,nnn,nnn,nnn.nn";

        }
        #endregion

        #region Config cbb
        private void LoadCbb()
        {
            ConfigCbb(cbbName1);
            ConfigCbb(cbbName2);
            ConfigCbb(cbbName3);
        }
        public void ConfigCbb(UltraCombo cbb)
        {
            lstMaterialGoodsResourceTaxGroup = Utils.ListMaterialGoodsResourceTaxGroup.Where(n => n.TaxRate != null).OrderBy(n => n.MaterialGoodsResourceTaxGroupCode).ToList();
            cbb.DataSource = lstMaterialGoodsResourceTaxGroup;
            cbb.DisplayMember = "MaterialGoodsResourceTaxGroupName";
            cbb.ValueMember = "ID";
            Utils.ConfigGrid(cbb, ConstDatabase.MaterialGoodsResourceTaxGroup_TableName);

        }
        #endregion

        #region LoadInitialize
        public bool LoadInitialize(bool isGet)
        {
            if (isGet)
            {

            }
            else
            {
                lblCompanyName.Text = Utils.GetCompanyName();
                lblCompanyTaxCode.Text = Utils.GetCompanyTaxCode();
                lblDL_Thue.Text = Utils.GetTenDLT();
                lblMST_DL.Text = Utils.GetMSTDLT();
                txtName.Text = Utils.GetHVTNhanVienDLT();
                txtChungChi.Text = Utils.GetCCHNDLT();
                txtSignName.Text = Utils.GetCompanyDirector();
                txtSignDate.DateTime = DateTime.Now;
                if (isDeclaredMonth == true)
                {
                    txtIsTaxPeriod.Text = "[x]";
                    lblTaxPeriod.Text = DeclarationTerm;
                    txtIsArisingTax.Text = "[]";
                    lblArisingTime.Text = "ngày...tháng...năm...";
                }
                if (isDeclaredArising == true)
                {
                    txtIsArisingTax.Text = "[x]";
                    lblArisingTime.Text = DeclarationTerm;
                    txtIsTaxPeriod.Text = "[]";
                    lblTaxPeriod.Text = "Tháng...năm...";
                }
                if (IsFirst == true)
                {
                    txtIsFirst.Text = "[x]";
                    lblAdditionTime.Text = "";
                }
                else
                {
                    txtIsFirst.Text = "[]";
                    lblAdditionTime.Text = AdditionalTime;
                }
            }
            return true;
        }
        #endregion

        #region ObjandGUI
        private TM01TAIN ObjandGUI(TM01TAIN input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == null || input.ID == Guid.Empty)
                {
                    input.ID = Guid.NewGuid();
                }
                input.TypeID = 924;
                input.DeclarationName = ultraLabel37.Text;
                if (isDeclaredMonth == true)
                {
                    input.DeclarationTerm = lblTaxPeriod.Text;
                    input.isMonth = true;
                }
                if (isDeclaredArising == true)
                {
                    input.DeclarationTerm = lblArisingTime.Text;
                    input.isDeclaredArising = true;
                }
                if (txtIsFirst.Text == "[x]" && lblAdditionTime.Text == "")
                    input.IsFirstDeclaration = true;
                try
                {
                    input.AdditionTime = int.Parse(lblAdditionTime.Text);
                }
                catch { }
                input.CompanyName = lblCompanyName.Text;
                input.CompanyTaxCode = lblCompanyTaxCode.Text;
                input.TaxAgencyName = lblDL_Thue.Text;
                input.TaxAgencyTaxCode = lblMST_DL.Text;
                input.TaxAgencyEmployeeName = txtName.Text;
                input.CertificationNo = txtChungChi.Text;
                input.SignName = txtSignName.Text;
                if (txtSignDate.Value != null)
                    input.SignDate = txtSignDate.DateTime;
                else
                    input.SignDate = null;
                input.FromDate = FromDate;
                input.ToDate = ToDate;

                #region Detail
                input.TM01TAINDetails.Clear();
                for (int t = 1; t < tableLayoutPanel1.RowCount; t++)
                {
                    var control0 = tableLayoutPanel1.GetControlFromPosition(0, t);
                    var control1 = tableLayoutPanel1.GetControlFromPosition(1, t);
                    var control2 = tableLayoutPanel1.GetControlFromPosition(2, t);
                    var control3 = tableLayoutPanel1.GetControlFromPosition(3, t);
                    var control4 = tableLayoutPanel1.GetControlFromPosition(4, t);
                    var control5 = tableLayoutPanel1.GetControlFromPosition(5, t);
                    var control6 = tableLayoutPanel1.GetControlFromPosition(6, t);
                    var control7 = tableLayoutPanel1.GetControlFromPosition(7, t);
                    var control8 = tableLayoutPanel1.GetControlFromPosition(8, t);
                    var control9 = tableLayoutPanel1.GetControlFromPosition(9, t);
                    UltraTextEditor uLabel = control0 as UltraTextEditor;
                    UltraCombo uCombo = control1 as UltraCombo;
                    UltraTextEditor uText = control2 as UltraTextEditor;
                    UltraMaskedEdit uMasked3 = control3 as UltraMaskedEdit;
                    UltraMaskedEdit uMasked4 = control4 as UltraMaskedEdit;
                    UltraMaskedEdit uMasked5 = control5 as UltraMaskedEdit;
                    UltraMaskedEdit uMasked6 = control6 as UltraMaskedEdit;
                    UltraMaskedEdit uMasked7 = control7 as UltraMaskedEdit;
                    UltraMaskedEdit uMasked8 = control8 as UltraMaskedEdit;
                    UltraMaskedEdit uMasked9 = control9 as UltraMaskedEdit;
                    if (uCombo.Text != "")
                    {
                        input.TM01TAINDetails.Add(new TM01TAINDetail
                        {
                            TM01TAINID = input.ID,
                            OrderPriority = uLabel.Value.ToInt(),
                            MaterialGoodsResourceTaxGroupID = Guid.Parse(uCombo.Value.ToString()),
                            MaterialGoodsResourceTaxGroupName = uCombo.Text,
                            Unit = uText.Text,
                            Quantity = decimal.Parse(uMasked3.Value.ToString()),
                            UnitPrice = decimal.Parse(uMasked4.Value.ToString()),
                            TaxRate = decimal.Parse(uMasked5.Value.ToString()),
                            ResourceTaxTaxAmountUnit = decimal.Parse(uMasked6.Value.ToString()),
                            ResourceTaxAmountIncurration = decimal.Parse(uMasked7.Value.ToString()),
                            ResourceTaxAmountDeduction = decimal.Parse(uMasked8.Value.ToString()),
                            ResourceTaxAmount = decimal.Parse(uMasked9.Value.ToString()),
                            Type = 1
                        });
                    }
                }
                for (int t = 1; t < tableLayoutPanel2.RowCount; t++)
                {
                    var control0 = tableLayoutPanel2.GetControlFromPosition(0, t);
                    var control1 = tableLayoutPanel2.GetControlFromPosition(1, t);
                    var control2 = tableLayoutPanel2.GetControlFromPosition(2, t);
                    var control3 = tableLayoutPanel2.GetControlFromPosition(3, t);
                    var control4 = tableLayoutPanel2.GetControlFromPosition(4, t);
                    var control5 = tableLayoutPanel2.GetControlFromPosition(5, t);
                    var control6 = tableLayoutPanel2.GetControlFromPosition(6, t);
                    var control7 = tableLayoutPanel2.GetControlFromPosition(7, t);
                    var control8 = tableLayoutPanel2.GetControlFromPosition(8, t);
                    var control9 = tableLayoutPanel2.GetControlFromPosition(9, t);
                    UltraTextEditor uLabel = control0 as UltraTextEditor;
                    UltraCombo uCombo = control1 as UltraCombo;
                    UltraTextEditor uText = control2 as UltraTextEditor;
                    UltraMaskedEdit uMasked3 = control3 as UltraMaskedEdit;
                    UltraMaskedEdit uMasked4 = control4 as UltraMaskedEdit;
                    UltraMaskedEdit uMasked5 = control5 as UltraMaskedEdit;
                    UltraMaskedEdit uMasked6 = control6 as UltraMaskedEdit;
                    UltraMaskedEdit uMasked7 = control7 as UltraMaskedEdit;
                    UltraMaskedEdit uMasked8 = control8 as UltraMaskedEdit;
                    UltraMaskedEdit uMasked9 = control9 as UltraMaskedEdit;
                    if (uCombo.Text != "")
                    {
                        input.TM01TAINDetails.Add(new TM01TAINDetail
                        {
                            TM01TAINID = input.ID,
                            OrderPriority = uLabel.Value.ToInt(),


                            MaterialGoodsResourceTaxGroupID = Guid.Parse(uCombo.Value.ToString()),


                            MaterialGoodsResourceTaxGroupName = uCombo.Text,
                            Unit = uText.Text,
                            Quantity = decimal.Parse(uMasked3.Value.ToString()),
                            UnitPrice = decimal.Parse(uMasked4.Value.ToString()),
                            TaxRate = decimal.Parse(uMasked5.Value.ToString()),
                            ResourceTaxTaxAmountUnit = decimal.Parse(uMasked6.Value.ToString()),
                            ResourceTaxAmountIncurration = decimal.Parse(uMasked7.Value.ToString()),
                            ResourceTaxAmountDeduction = decimal.Parse(uMasked8.Value.ToString()),
                            ResourceTaxAmount = decimal.Parse(uMasked9.Value.ToString()),
                            Type = 2
                        });
                    }
                }
                for (int t = 1; t < tableLayoutPanel3.RowCount; t++)
                {
                    var control0 = tableLayoutPanel3.GetControlFromPosition(0, t);
                    var control1 = tableLayoutPanel3.GetControlFromPosition(1, t);
                    var control2 = tableLayoutPanel3.GetControlFromPosition(2, t);
                    var control3 = tableLayoutPanel3.GetControlFromPosition(3, t);
                    var control4 = tableLayoutPanel3.GetControlFromPosition(4, t);
                    var control5 = tableLayoutPanel3.GetControlFromPosition(5, t);
                    var control6 = tableLayoutPanel3.GetControlFromPosition(6, t);
                    var control7 = tableLayoutPanel3.GetControlFromPosition(7, t);
                    var control8 = tableLayoutPanel3.GetControlFromPosition(8, t);
                    var control9 = tableLayoutPanel3.GetControlFromPosition(9, t);
                    UltraTextEditor uLabel = control0 as UltraTextEditor;
                    UltraCombo uCombo = control1 as UltraCombo;
                    UltraTextEditor uText = control2 as UltraTextEditor;
                    UltraMaskedEdit uMasked3 = control3 as UltraMaskedEdit;
                    UltraMaskedEdit uMasked4 = control4 as UltraMaskedEdit;
                    UltraMaskedEdit uMasked5 = control5 as UltraMaskedEdit;
                    UltraMaskedEdit uMasked6 = control6 as UltraMaskedEdit;
                    UltraMaskedEdit uMasked7 = control7 as UltraMaskedEdit;
                    UltraMaskedEdit uMasked8 = control8 as UltraMaskedEdit;
                    UltraMaskedEdit uMasked9 = control9 as UltraMaskedEdit;
                    if (uCombo.Text != "")
                    {
                        input.TM01TAINDetails.Add(new TM01TAINDetail
                        {
                            TM01TAINID = input.ID,
                            OrderPriority = uLabel.Value.ToInt(),
                            MaterialGoodsResourceTaxGroupID = Guid.Parse(uCombo.Value.ToString()),
                            MaterialGoodsResourceTaxGroupName = uCombo.Text,
                            Unit = uText.Text,
                            Quantity = decimal.Parse(uMasked3.Value.ToString()),
                            UnitPrice = decimal.Parse(uMasked4.Value.ToString()),
                            TaxRate = decimal.Parse(uMasked5.Value.ToString()),
                            ResourceTaxTaxAmountUnit = decimal.Parse(uMasked6.Value.ToString()),
                            ResourceTaxAmountIncurration = decimal.Parse(uMasked7.Value.ToString()),
                            ResourceTaxAmountDeduction = decimal.Parse(uMasked8.Value.ToString()),
                            ResourceTaxAmount = decimal.Parse(uMasked9.Value.ToString()),
                            Type = 3
                        });
                    }
                }
                #endregion

            }
            else
            {
                ultraLabel37.Text = input.DeclarationName;
                if (input.isMonth == true)
                {
                    txtIsTaxPeriod.Text = "[x]";
                    lblTaxPeriod.Text = input.DeclarationTerm;
                    txtIsArisingTax.Text = "[]";
                    lblArisingTime.Text = "ngày...tháng...năm...";

                }
                if (input.isDeclaredArising == true)
                {
                    txtIsArisingTax.Text = "[x]";
                    lblArisingTime.Text = input.DeclarationTerm;
                    txtIsTaxPeriod.Text = "[]";
                    lblTaxPeriod.Text = "Tháng...năm...";
                }
                if (input.IsFirstDeclaration == true)
                {
                    txtIsFirst.Text = "[x]";
                    lblAdditionTime.Text = "";
                }
                else
                {
                    txtIsFirst.Text = "[]";
                    lblAdditionTime.Text = input.AdditionTime.ToString();
                }
                lblCompanyName.Text = input.CompanyName;
                lblCompanyTaxCode.Text = input.CompanyTaxCode;
                lblDL_Thue.Text = input.TaxAgencyName;
                lblMST_DL.Text = input.TaxAgencyTaxCode;
                txtName.Text = input.TaxAgencyEmployeeName;
                txtChungChi.Text = input.CertificationNo;
                txtSignName.Text = input.SignName;
                txtSignDate.DateTime = input.SignDate ?? DateTime.Now;
                #region Detail
                #region Table 1
                List<TM01TAINDetail> lst1 = new List<TM01TAINDetail>();
                lst1 = input.TM01TAINDetails.Where(n => n.Type == 1).OrderBy(n => n.OrderPriority).ToList();
                foreach (var item in lst1)
                {
                    if (lst1.IndexOf(item) == 0)
                    {
                        var control1 = tableLayoutPanel1.GetControlFromPosition(1, 1);
                        (control1 as UltraCombo).Value = item.MaterialGoodsResourceTaxGroupID;
                        var control2 = tableLayoutPanel1.GetControlFromPosition(2, 1);
                        (control2 as UltraTextEditor).Value = item.Unit;
                        var control3 = tableLayoutPanel1.GetControlFromPosition(3, 1);
                        (control3 as UltraMaskedEdit).Value = item.Quantity;
                        var control4 = tableLayoutPanel1.GetControlFromPosition(4, 1);
                        (control4 as UltraMaskedEdit).Value = item.UnitPrice;
                        var control5 = tableLayoutPanel1.GetControlFromPosition(5, 1);
                        (control5 as UltraMaskedEdit).Value = item.TaxRate;
                        var control6 = tableLayoutPanel1.GetControlFromPosition(6, 1);
                        (control6 as UltraMaskedEdit).Value = item.ResourceTaxTaxAmountUnit;
                        var control7 = tableLayoutPanel1.GetControlFromPosition(7, 1);
                        (control7 as UltraMaskedEdit).Value = item.ResourceTaxAmountIncurration;
                        var control8 = tableLayoutPanel1.GetControlFromPosition(8, 1);
                        (control8 as UltraMaskedEdit).Value = item.ResourceTaxAmountDeduction;
                        var control9 = tableLayoutPanel1.GetControlFromPosition(9, 1);
                        (control9 as UltraMaskedEdit).Value = item.ResourceTaxAmount;

                    }
                    else
                    {
                        RowStyle temp = tableLayoutPanel1.RowStyles[tableLayoutPanel1.RowCount - 1];
                        tableLayoutPanel1.RowCount++;
                        int h = tableLayoutPanel1.Height;
                        RowStyle rowStyle = new RowStyle(temp.SizeType, temp.Height);
                        tableLayoutPanel1.RowStyles.Add(rowStyle);

                        UltraTextEditor txtSTT = new UltraTextEditor();
                        txtSTT.Value = item.OrderPriority;
                        txtSTT.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        txtSTT.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                        txtSTT.AutoSize = false;
                        txtSTT.Dock = DockStyle.Fill;
                        txtSTT.Margin = new Padding(0);

                        UltraTextEditor txtUnit = new UltraTextEditor();

                        txtUnit.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                        txtUnit.AutoSize = false;
                        txtUnit.Dock = DockStyle.Fill;
                        txtUnit.Margin = new Padding(0);

                        UltraMaskedEdit txtQuantity = new UltraMaskedEdit();
                        txtQuantity.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit1(txtQuantity);

                        UltraMaskedEdit txtUnitPrice = new UltraMaskedEdit();
                        txtUnitPrice.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit1(txtUnitPrice);

                        UltraMaskedEdit txtTaxRate = new UltraMaskedEdit();
                        txtTaxRate.ReadOnly = true;
                        txtTaxRate.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit1(txtTaxRate);

                        UltraCombo cbbName = new UltraCombo();
                        cbbName.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                        cbbName.Dock = DockStyle.Fill;
                        cbbName.AutoSize = false;
                        cbbName.Margin = new Padding(0);
                        ConfigCbb(cbbName);

                        UltraMaskedEdit txtResourceTaxTaxAmountUnit = new UltraMaskedEdit();
                        txtResourceTaxTaxAmountUnit.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit(txtResourceTaxTaxAmountUnit);

                        UltraMaskedEdit txtResourceTaxAmountIncurration = new UltraMaskedEdit();
                        txtResourceTaxAmountIncurration.ReadOnly = true;
                        txtResourceTaxAmountIncurration.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit(txtResourceTaxAmountIncurration);

                        UltraMaskedEdit txtResourceTaxAmountDeduction = new UltraMaskedEdit();
                        txtResourceTaxAmountDeduction.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit(txtResourceTaxAmountDeduction);

                        UltraMaskedEdit txtResourceTaxAmount = new UltraMaskedEdit();
                        txtResourceTaxAmount.ReadOnly = true;
                        txtResourceTaxAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit(txtResourceTaxAmount);

                        txtSTT.ContextMenuStrip = contextMenuStrip1;
                        txtUnit.ContextMenuStrip = contextMenuStrip1;
                        txtQuantity.ContextMenuStrip = contextMenuStrip1;
                        txtUnitPrice.ContextMenuStrip = contextMenuStrip1;
                        txtTaxRate.ContextMenuStrip = contextMenuStrip1;
                        cbbName.ContextMenuStrip = contextMenuStrip1;
                        txtResourceTaxTaxAmountUnit.ContextMenuStrip = contextMenuStrip1;
                        txtResourceTaxAmountIncurration.ContextMenuStrip = contextMenuStrip1;
                        txtResourceTaxAmountDeduction.ContextMenuStrip = contextMenuStrip1;
                        txtResourceTaxAmount.ContextMenuStrip = contextMenuStrip1;

                        #region add event control
                        cbbName.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler((s, a) => cbb_RowSelected(s, a, txtUnit, txtTaxRate));
                        cbbName.TextChanged += new System.EventHandler((s, a) => cbbName_TextChanged(s, a, txtUnit, txtTaxRate));
                        txtQuantity.ValueChanged += new System.EventHandler((s, a) => txtQuantity_ValueChanged(s, a, txtQuantity, txtUnitPrice, txtTaxRate, txtResourceTaxTaxAmountUnit, txtResourceTaxAmountIncurration));
                        txtUnitPrice.ValueChanged += new System.EventHandler((s, a) => txtUnitPrice_ValueChanged(s, a, txtQuantity, txtUnitPrice, txtTaxRate, txtResourceTaxTaxAmountUnit, txtResourceTaxAmountIncurration));
                        txtTaxRate.ValueChanged += new System.EventHandler((s, a) => txtTaxRate_ValueChanged(s, a, txtQuantity, txtUnitPrice, txtTaxRate, txtResourceTaxAmountIncurration));
                        txtResourceTaxTaxAmountUnit.ValueChanged += new System.EventHandler((s, a) => txtResourceTaxTaxAmountUnit_TextChanged(s, a, txtQuantity, txtResourceTaxTaxAmountUnit, txtResourceTaxAmountIncurration, txtUnitPrice, txtTaxRate));
                        txtResourceTaxAmountIncurration.ValueChanged += new System.EventHandler((s, a) => txtResourceTaxAmountIncurration_TextChanged(s, a, txtResourceTaxAmount, txtResourceTaxAmountIncurration, txtResourceTaxAmountDeduction));
                        txtResourceTaxAmountDeduction.ValueChanged += new EventHandler((s, a) => txtResourceTaxAmountDeduction_TextChanged(s, a, txtResourceTaxAmount, txtResourceTaxAmountIncurration, txtResourceTaxAmountDeduction));
                        txtResourceTaxAmount.ValueChanged += new EventHandler(txtResourceTaxAmount_TextChanged);
                        #endregion

                        tableLayoutPanel1.Controls.Add(txtSTT, 0, tableLayoutPanel1.RowCount - 1);
                        tableLayoutPanel1.Controls.Add(cbbName, 1, tableLayoutPanel1.RowCount - 1);
                        tableLayoutPanel1.Controls.Add(txtUnit, 2, tableLayoutPanel1.RowCount - 1);
                        tableLayoutPanel1.Controls.Add(txtQuantity, 3, tableLayoutPanel1.RowCount - 1);
                        tableLayoutPanel1.Controls.Add(txtUnitPrice, 4, tableLayoutPanel1.RowCount - 1);
                        tableLayoutPanel1.Controls.Add(txtTaxRate, 5, tableLayoutPanel1.RowCount - 1);
                        tableLayoutPanel1.Controls.Add(txtResourceTaxTaxAmountUnit, 6, tableLayoutPanel1.RowCount - 1);
                        tableLayoutPanel1.Controls.Add(txtResourceTaxAmountIncurration, 7, tableLayoutPanel1.RowCount - 1);
                        tableLayoutPanel1.Controls.Add(txtResourceTaxAmountDeduction, 8, tableLayoutPanel1.RowCount - 1);
                        tableLayoutPanel1.Controls.Add(txtResourceTaxAmount, 9, tableLayoutPanel1.RowCount - 1);

                        txtQuantity.FormatNumberic(item.Quantity, ConstDatabase.Format_Quantity);
                        txtUnitPrice.FormatNumberic(item.UnitPrice, ConstDatabase.Format_Quantity);
                        txtTaxRate.FormatNumberic(item.TaxRate, ConstDatabase.Format_Quantity);
                        cbbName.Value = item.MaterialGoodsResourceTaxGroupID;
                        txtUnit.Value = item.Unit;
                        txtResourceTaxTaxAmountUnit.FormatNumberic(item.ResourceTaxTaxAmountUnit, ConstDatabase.Format_TienVND);
                        txtResourceTaxAmountIncurration.FormatNumberic(item.ResourceTaxAmountIncurration, ConstDatabase.Format_TienVND);
                        txtResourceTaxAmountDeduction.FormatNumberic(item.ResourceTaxAmountDeduction, ConstDatabase.Format_TienVND);
                        txtResourceTaxAmount.FormatNumberic(item.ResourceTaxAmount, ConstDatabase.Format_TienVND);

                        tableLayoutPanel2.Location = new Point(tableLayoutPanel2.Location.X, tableLayoutPanel1.Location.Y + tableLayoutPanel1.Height);
                        tableLayoutPanel3.Location = new Point(tableLayoutPanel3.Location.X, tableLayoutPanel2.Location.Y + tableLayoutPanel2.Height);
                        tableLayoutPanel5.Location = new Point(tableLayoutPanel5.Location.X, tableLayoutPanel3.Location.Y + tableLayoutPanel3.Height);
                        ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel5.Location.Y + tableLayoutPanel5.Height);
                    }
                }
                #endregion

                #region Table 2
                List<TM01TAINDetail> lst2 = new List<TM01TAINDetail>();
                lst2 = input.TM01TAINDetails.Where(n => n.Type == 2).OrderBy(n => n.OrderPriority).ToList();
                foreach (var item in lst2)
                {
                    if (lst2.IndexOf(item) == 0)
                    {
                        var control1 = tableLayoutPanel2.GetControlFromPosition(1, 1);
                        (control1 as UltraCombo).Value = item.MaterialGoodsResourceTaxGroupID;
                        var control2 = tableLayoutPanel2.GetControlFromPosition(2, 1);
                        (control2 as UltraTextEditor).Value = item.Unit;
                        var control3 = tableLayoutPanel2.GetControlFromPosition(3, 1);
                        (control3 as UltraMaskedEdit).Value = item.Quantity;
                        var control4 = tableLayoutPanel2.GetControlFromPosition(4, 1);
                        (control4 as UltraMaskedEdit).Value = item.UnitPrice;
                        var control5 = tableLayoutPanel2.GetControlFromPosition(5, 1);
                        (control5 as UltraMaskedEdit).Value = item.TaxRate;
                        var control6 = tableLayoutPanel2.GetControlFromPosition(6, 1);
                        (control6 as UltraMaskedEdit).Value = item.ResourceTaxTaxAmountUnit;
                        var control7 = tableLayoutPanel2.GetControlFromPosition(7, 1);
                        (control7 as UltraMaskedEdit).Value = item.ResourceTaxAmountIncurration;
                        var control8 = tableLayoutPanel2.GetControlFromPosition(8, 1);
                        (control8 as UltraMaskedEdit).Value = item.ResourceTaxAmountDeduction;
                        var control9 = tableLayoutPanel2.GetControlFromPosition(9, 1);
                        (control9 as UltraMaskedEdit).Value = item.ResourceTaxAmount;
                    }
                    else
                    {
                        RowStyle temp = tableLayoutPanel2.RowStyles[tableLayoutPanel2.RowCount - 1];
                        tableLayoutPanel2.RowCount++;
                        int h = tableLayoutPanel2.Height;
                        RowStyle rowStyle = new RowStyle(temp.SizeType, temp.Height);
                        tableLayoutPanel2.RowStyles.Add(rowStyle);

                        UltraTextEditor txtSTT = new UltraTextEditor();
                        txtSTT.Value = item.OrderPriority;
                        txtSTT.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        txtSTT.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                        txtSTT.AutoSize = false;
                        txtSTT.Dock = DockStyle.Fill;
                        txtSTT.Margin = new Padding(0);

                        UltraTextEditor txtUnit = new UltraTextEditor();

                        txtUnit.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                        txtUnit.AutoSize = false;
                        txtUnit.Dock = DockStyle.Fill;
                        txtUnit.Margin = new Padding(0);

                        UltraMaskedEdit txtQuantity = new UltraMaskedEdit();
                        txtQuantity.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit1(txtQuantity);

                        UltraMaskedEdit txtUnitPrice = new UltraMaskedEdit();
                        txtUnitPrice.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit1(txtUnitPrice);

                        UltraMaskedEdit txtTaxRate = new UltraMaskedEdit();
                        txtTaxRate.ReadOnly = true;
                        txtTaxRate.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit1(txtTaxRate);

                        UltraCombo cbbName = new UltraCombo();
                        cbbName.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                        cbbName.Dock = DockStyle.Fill;
                        cbbName.AutoSize = false;
                        cbbName.Margin = new Padding(0);
                        ConfigCbb(cbbName);

                        UltraMaskedEdit txtResourceTaxTaxAmountUnit = new UltraMaskedEdit();
                        txtResourceTaxTaxAmountUnit.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit(txtResourceTaxTaxAmountUnit);

                        UltraMaskedEdit txtResourceTaxAmountIncurration = new UltraMaskedEdit();
                        txtResourceTaxAmountIncurration.ReadOnly = true;
                        txtResourceTaxAmountIncurration.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit(txtResourceTaxAmountIncurration);

                        UltraMaskedEdit txtResourceTaxAmountDeduction = new UltraMaskedEdit();
                        txtResourceTaxAmountDeduction.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit(txtResourceTaxAmountDeduction);

                        UltraMaskedEdit txtResourceTaxAmount = new UltraMaskedEdit();
                        txtResourceTaxAmount.ReadOnly = true;
                        txtResourceTaxAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit(txtResourceTaxAmount);

                        txtSTT.ContextMenuStrip = contextMenuStrip2;
                        txtUnit.ContextMenuStrip = contextMenuStrip2;
                        txtQuantity.ContextMenuStrip = contextMenuStrip2;
                        txtUnitPrice.ContextMenuStrip = contextMenuStrip2;
                        txtTaxRate.ContextMenuStrip = contextMenuStrip2;
                        cbbName.ContextMenuStrip = contextMenuStrip2;
                        txtResourceTaxTaxAmountUnit.ContextMenuStrip = contextMenuStrip2;
                        txtResourceTaxAmountIncurration.ContextMenuStrip = contextMenuStrip2;
                        txtResourceTaxAmountDeduction.ContextMenuStrip = contextMenuStrip2;
                        txtResourceTaxAmount.ContextMenuStrip = contextMenuStrip2;

                        #region add event control
                        cbbName.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler((s, a) => cbb_RowSelected(s, a, txtUnit, txtTaxRate));
                        cbbName.TextChanged += new System.EventHandler((s, a) => cbbName_TextChanged(s, a, txtUnit, txtTaxRate));
                        txtQuantity.ValueChanged += new System.EventHandler((s, a) => txtQuantity_ValueChanged(s, a, txtQuantity, txtUnitPrice, txtTaxRate, txtResourceTaxTaxAmountUnit, txtResourceTaxAmountIncurration));
                        txtUnitPrice.ValueChanged += new System.EventHandler((s, a) => txtUnitPrice_ValueChanged(s, a, txtQuantity, txtUnitPrice, txtTaxRate, txtResourceTaxTaxAmountUnit, txtResourceTaxAmountIncurration));
                        txtTaxRate.ValueChanged += new System.EventHandler((s, a) => txtTaxRate_ValueChanged(s, a, txtQuantity, txtUnitPrice, txtTaxRate, txtResourceTaxAmountIncurration));
                        txtResourceTaxTaxAmountUnit.ValueChanged += new System.EventHandler((s, a) => txtResourceTaxTaxAmountUnit_TextChanged(s, a, txtQuantity, txtResourceTaxTaxAmountUnit, txtResourceTaxAmountIncurration, txtUnitPrice, txtTaxRate));
                        txtResourceTaxAmountIncurration.ValueChanged += new System.EventHandler((s, a) => txtResourceTaxAmountIncurration_TextChanged(s, a, txtResourceTaxAmount, txtResourceTaxAmountIncurration, txtResourceTaxAmountDeduction));
                        txtResourceTaxAmountDeduction.ValueChanged += new EventHandler((s, a) => txtResourceTaxAmountDeduction_TextChanged(s, a, txtResourceTaxAmount, txtResourceTaxAmountIncurration, txtResourceTaxAmountDeduction));
                        txtResourceTaxAmount.ValueChanged += new EventHandler(txtResourceTaxAmount_TextChanged);
                        #endregion

                        tableLayoutPanel2.Controls.Add(txtSTT, 0, tableLayoutPanel2.RowCount - 1);
                        tableLayoutPanel2.Controls.Add(cbbName, 1, tableLayoutPanel2.RowCount - 1);
                        tableLayoutPanel2.Controls.Add(txtUnit, 2, tableLayoutPanel2.RowCount - 1);
                        tableLayoutPanel2.Controls.Add(txtQuantity, 3, tableLayoutPanel2.RowCount - 1);
                        tableLayoutPanel2.Controls.Add(txtUnitPrice, 4, tableLayoutPanel2.RowCount - 1);
                        tableLayoutPanel2.Controls.Add(txtTaxRate, 5, tableLayoutPanel2.RowCount - 1);
                        tableLayoutPanel2.Controls.Add(txtResourceTaxTaxAmountUnit, 6, tableLayoutPanel2.RowCount - 1);
                        tableLayoutPanel2.Controls.Add(txtResourceTaxAmountIncurration, 7, tableLayoutPanel2.RowCount - 1);
                        tableLayoutPanel2.Controls.Add(txtResourceTaxAmountDeduction, 8, tableLayoutPanel2.RowCount - 1);
                        tableLayoutPanel2.Controls.Add(txtResourceTaxAmount, 9, tableLayoutPanel2.RowCount - 1);

                        txtQuantity.FormatNumberic(item.Quantity, ConstDatabase.Format_Quantity);
                        txtUnitPrice.FormatNumberic(item.UnitPrice, ConstDatabase.Format_Quantity);
                        txtTaxRate.FormatNumberic(item.TaxRate, ConstDatabase.Format_Quantity);
                        cbbName.Value = item.MaterialGoodsResourceTaxGroupID;
                        txtUnit.Value = item.Unit;
                        txtResourceTaxTaxAmountUnit.FormatNumberic(item.ResourceTaxTaxAmountUnit, ConstDatabase.Format_TienVND);
                        txtResourceTaxAmountIncurration.FormatNumberic(item.ResourceTaxAmountIncurration, ConstDatabase.Format_TienVND);
                        txtResourceTaxAmountDeduction.FormatNumberic(item.ResourceTaxAmountDeduction, ConstDatabase.Format_TienVND);
                        txtResourceTaxAmount.FormatNumberic(item.ResourceTaxAmount, ConstDatabase.Format_TienVND);

                        tableLayoutPanel3.Location = new Point(tableLayoutPanel3.Location.X, tableLayoutPanel2.Location.Y + tableLayoutPanel2.Height);
                        tableLayoutPanel5.Location = new Point(tableLayoutPanel5.Location.X, tableLayoutPanel3.Location.Y + tableLayoutPanel3.Height);
                        ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel5.Location.Y + tableLayoutPanel5.Height);
                    }
                }
                #endregion

                #region Table 3
                List<TM01TAINDetail> lst3 = new List<TM01TAINDetail>();
                lst3 = input.TM01TAINDetails.Where(n => n.Type == 3).OrderByDescending(n => n.OrderPriority).ToList();
                foreach (var item in lst3)
                {
                    if (lst3.IndexOf(item) == 0)
                    {
                        var control1 = tableLayoutPanel3.GetControlFromPosition(1, 1);
                        (control1 as UltraCombo).Value = item.MaterialGoodsResourceTaxGroupID;
                        var control2 = tableLayoutPanel3.GetControlFromPosition(2, 1);
                        (control2 as UltraTextEditor).Value = item.Unit;
                        var control3 = tableLayoutPanel3.GetControlFromPosition(3, 1);
                        (control3 as UltraMaskedEdit).Value = item.Quantity;
                        var control4 = tableLayoutPanel3.GetControlFromPosition(4, 1);
                        (control4 as UltraMaskedEdit).Value = item.UnitPrice;
                        var control5 = tableLayoutPanel3.GetControlFromPosition(5, 1);
                        (control5 as UltraMaskedEdit).Value = item.TaxRate;
                        var control6 = tableLayoutPanel3.GetControlFromPosition(6, 1);
                        (control6 as UltraMaskedEdit).Value = item.ResourceTaxTaxAmountUnit;
                        var control7 = tableLayoutPanel3.GetControlFromPosition(7, 1);
                        (control7 as UltraMaskedEdit).Value = item.ResourceTaxAmountIncurration;
                        var control8 = tableLayoutPanel3.GetControlFromPosition(8, 1);
                        (control8 as UltraMaskedEdit).Value = item.ResourceTaxAmountDeduction;
                        var control9 = tableLayoutPanel3.GetControlFromPosition(9, 1);
                        (control9 as UltraMaskedEdit).Value = item.ResourceTaxAmount;
                    }
                    else
                    {
                        RowStyle temp = tableLayoutPanel3.RowStyles[tableLayoutPanel3.RowCount - 1];
                        tableLayoutPanel3.RowCount++;
                        int h = tableLayoutPanel3.Height;
                        RowStyle rowStyle = new RowStyle(temp.SizeType, temp.Height);
                        tableLayoutPanel3.RowStyles.Add(rowStyle);

                        UltraTextEditor txtSTT = new UltraTextEditor();
                        txtSTT.Value = item.OrderPriority;
                        txtSTT.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        txtSTT.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                        txtSTT.AutoSize = false;
                        txtSTT.Dock = DockStyle.Fill;
                        txtSTT.Margin = new Padding(0);

                        UltraTextEditor txtUnit = new UltraTextEditor();

                        txtUnit.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                        txtUnit.AutoSize = false;
                        txtUnit.Dock = DockStyle.Fill;
                        txtUnit.Margin = new Padding(0);

                        UltraMaskedEdit txtQuantity = new UltraMaskedEdit();
                        txtQuantity.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit1(txtQuantity);

                        UltraMaskedEdit txtUnitPrice = new UltraMaskedEdit();
                        txtUnitPrice.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit1(txtUnitPrice);

                        UltraMaskedEdit txtTaxRate = new UltraMaskedEdit();
                        txtTaxRate.ReadOnly = true;
                        txtTaxRate.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit1(txtTaxRate);

                        UltraCombo cbbName = new UltraCombo();
                        cbbName.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                        cbbName.Dock = DockStyle.Fill;
                        cbbName.AutoSize = false;
                        cbbName.Margin = new Padding(0);
                        ConfigCbb(cbbName);

                        UltraMaskedEdit txtResourceTaxTaxAmountUnit = new UltraMaskedEdit();
                        txtResourceTaxTaxAmountUnit.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit(txtResourceTaxTaxAmountUnit);

                        UltraMaskedEdit txtResourceTaxAmountIncurration = new UltraMaskedEdit();
                        txtResourceTaxAmountIncurration.ReadOnly = true;
                        txtResourceTaxAmountIncurration.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit(txtResourceTaxAmountIncurration);

                        UltraMaskedEdit txtResourceTaxAmountDeduction = new UltraMaskedEdit();
                        txtResourceTaxAmountDeduction.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit(txtResourceTaxAmountDeduction);

                        UltraMaskedEdit txtResourceTaxAmount = new UltraMaskedEdit();
                        txtResourceTaxAmount.ReadOnly = true;
                        txtResourceTaxAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        ConfigMaskedEdit(txtResourceTaxAmount);

                        txtSTT.ContextMenuStrip = contextMenuStrip3;
                        txtUnit.ContextMenuStrip = contextMenuStrip3;
                        txtQuantity.ContextMenuStrip = contextMenuStrip3;
                        txtUnitPrice.ContextMenuStrip = contextMenuStrip3;
                        txtTaxRate.ContextMenuStrip = contextMenuStrip3;
                        cbbName.ContextMenuStrip = contextMenuStrip3;
                        txtResourceTaxTaxAmountUnit.ContextMenuStrip = contextMenuStrip3;
                        txtResourceTaxAmountIncurration.ContextMenuStrip = contextMenuStrip3;
                        txtResourceTaxAmountDeduction.ContextMenuStrip = contextMenuStrip3;
                        txtResourceTaxAmount.ContextMenuStrip = contextMenuStrip3;

                        #region add event control
                        cbbName.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler((s, a) => cbb_RowSelected(s, a, txtUnit, txtTaxRate));
                        cbbName.TextChanged += new System.EventHandler((s, a) => cbbName_TextChanged(s, a, txtUnit, txtTaxRate));
                        txtQuantity.ValueChanged += new System.EventHandler((s, a) => txtQuantity_ValueChanged(s, a, txtQuantity, txtUnitPrice, txtTaxRate, txtResourceTaxTaxAmountUnit, txtResourceTaxAmountIncurration));
                        txtUnitPrice.ValueChanged += new System.EventHandler((s, a) => txtUnitPrice_ValueChanged(s, a, txtQuantity, txtUnitPrice, txtTaxRate, txtResourceTaxTaxAmountUnit, txtResourceTaxAmountIncurration));
                        txtTaxRate.ValueChanged += new System.EventHandler((s, a) => txtTaxRate_ValueChanged(s, a, txtQuantity, txtUnitPrice, txtTaxRate, txtResourceTaxAmountIncurration));
                        txtResourceTaxTaxAmountUnit.ValueChanged += new System.EventHandler((s, a) => txtResourceTaxTaxAmountUnit_TextChanged(s, a, txtQuantity, txtResourceTaxTaxAmountUnit, txtResourceTaxAmountIncurration, txtUnitPrice, txtTaxRate));
                        txtResourceTaxAmountIncurration.ValueChanged += new System.EventHandler((s, a) => txtResourceTaxAmountIncurration_TextChanged(s, a, txtResourceTaxAmount, txtResourceTaxAmountIncurration, txtResourceTaxAmountDeduction));
                        txtResourceTaxAmountDeduction.ValueChanged += new EventHandler((s, a) => txtResourceTaxAmountDeduction_TextChanged(s, a, txtResourceTaxAmount, txtResourceTaxAmountIncurration, txtResourceTaxAmountDeduction));
                        txtResourceTaxAmount.ValueChanged += new EventHandler(txtResourceTaxAmount_TextChanged);
                        #endregion

                        tableLayoutPanel3.Controls.Add(txtSTT, 0, tableLayoutPanel3.RowCount - 1);
                        tableLayoutPanel3.Controls.Add(cbbName, 1, tableLayoutPanel3.RowCount - 1);
                        tableLayoutPanel3.Controls.Add(txtUnit, 2, tableLayoutPanel3.RowCount - 1);
                        tableLayoutPanel3.Controls.Add(txtQuantity, 3, tableLayoutPanel3.RowCount - 1);
                        tableLayoutPanel3.Controls.Add(txtUnitPrice, 4, tableLayoutPanel3.RowCount - 1);
                        tableLayoutPanel3.Controls.Add(txtTaxRate, 5, tableLayoutPanel3.RowCount - 1);
                        tableLayoutPanel3.Controls.Add(txtResourceTaxTaxAmountUnit, 6, tableLayoutPanel3.RowCount - 1);
                        tableLayoutPanel3.Controls.Add(txtResourceTaxAmountIncurration, 7, tableLayoutPanel3.RowCount - 1);
                        tableLayoutPanel3.Controls.Add(txtResourceTaxAmountDeduction, 8, tableLayoutPanel3.RowCount - 1);
                        tableLayoutPanel3.Controls.Add(txtResourceTaxAmount, 9, tableLayoutPanel3.RowCount - 1);

                        txtQuantity.FormatNumberic(item.Quantity, ConstDatabase.Format_Quantity);
                        txtUnitPrice.FormatNumberic(item.UnitPrice, ConstDatabase.Format_Quantity);
                        txtTaxRate.FormatNumberic(item.TaxRate, ConstDatabase.Format_Quantity);
                        cbbName.Value = item.MaterialGoodsResourceTaxGroupID;
                        txtUnit.Value = item.Unit;
                        txtResourceTaxTaxAmountUnit.FormatNumberic(item.ResourceTaxTaxAmountUnit, ConstDatabase.Format_TienVND);
                        txtResourceTaxAmountIncurration.FormatNumberic(item.ResourceTaxAmountIncurration, ConstDatabase.Format_TienVND);
                        txtResourceTaxAmountDeduction.FormatNumberic(item.ResourceTaxAmountDeduction, ConstDatabase.Format_TienVND);
                        txtResourceTaxAmount.FormatNumberic(item.ResourceTaxAmount, ConstDatabase.Format_TienVND);

                        tableLayoutPanel5.Location = new Point(tableLayoutPanel5.Location.X, tableLayoutPanel3.Location.Y + tableLayoutPanel3.Height);
                        ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel5.Location.Y + tableLayoutPanel5.Height);
                    }
                }
                #endregion
                #endregion
            }
            return input;
        }
        #endregion

        #region event
        #region event cho control 
        private void cbb_RowSelected(object sender, RowSelectedEventArgs e, UltraTextEditor txt1, UltraMaskedEdit txt2)
        {
            var cbb = (UltraCombo)sender;
            MaterialGoodsResourceTaxGroup model = (MaterialGoodsResourceTaxGroup)Utils.getSelectCbbItem(cbb);
            //Guid ID_ = Guid.Parse(cbb.Value.ToString());
            if (model != null)
            {
                MaterialGoodsResourceTaxGroup materialGoodsResourceTaxGroup = lstMaterialGoodsResourceTaxGroup.FirstOrDefault(n => n.ID == model.ID);
                txt1.Value = materialGoodsResourceTaxGroup.Unit;
                txt2.FormatNumberic(materialGoodsResourceTaxGroup.TaxRate, ConstDatabase.Format_Quantity);
            }
        }
        private void txtQuantity_ValueChanged(object sender, EventArgs e, UltraMaskedEdit txtQuantity, UltraMaskedEdit txtUnitPrice, UltraMaskedEdit txtTaxRate,
            UltraMaskedEdit txtResourceTaxTaxAmountUnit, UltraMaskedEdit txtResourceTaxAmountIncurration)
        {
            if (txtQuantity.Value.IsNullOrEmpty())
            {
                txtQuantity.Value = 0;
            }
            if (txtUnitPrice.Value.IsNullOrEmpty())
            {
                txtUnitPrice.Value = 0;
            }
            if (txtTaxRate.Value.IsNullOrEmpty())
            {
                txtTaxRate.Value = 0;
            }
            if (txtResourceTaxTaxAmountUnit.Value.IsNullOrEmpty())
            {
                txtResourceTaxTaxAmountUnit.Value = 0;
            }
            if (txtResourceTaxTaxAmountUnit.Value.ToString() != "0" && txtResourceTaxTaxAmountUnit.Text != "0,00")
                txtResourceTaxAmountIncurration.FormatNumberic((double.Parse(txtQuantity.Value.ToString()) * double.Parse(txtResourceTaxTaxAmountUnit.Value.ToString())), ConstDatabase.Format_TienVND);
            else
                txtResourceTaxAmountIncurration.FormatNumberic(((double.Parse(txtQuantity.Value.ToString()) * double.Parse(txtUnitPrice.Value.ToString()) * double.Parse(txtTaxRate.Value.ToString())) / 100), ConstDatabase.Format_TienVND);

        }
        private void txtUnitPrice_ValueChanged(object sender, EventArgs e, UltraMaskedEdit txtQuantity, UltraMaskedEdit txtUnitPrice, UltraMaskedEdit txtTaxRate,
             UltraMaskedEdit txtResourceTaxTaxAmountUnit, UltraMaskedEdit txtResourceTaxAmountIncurration)
        {
            if (txtQuantity.Value.IsNullOrEmpty())
            {
                txtQuantity.Value = 0;
            }
            if (txtUnitPrice.Value.IsNullOrEmpty())
            {
                txtUnitPrice.Value = 0;
            }
            if (txtTaxRate.Value.IsNullOrEmpty())
            {
                txtTaxRate.Value = 0;
            }
            if (txtResourceTaxTaxAmountUnit.Value.IsNullOrEmpty())
            {
                txtResourceTaxTaxAmountUnit.Value = 0;
            }
            //if (txtUnitPrice.Value.ToString() == "0")
            //    txtResourceTaxTaxAmountUnit.ReadOnly = false;
            //else
            //    txtResourceTaxTaxAmountUnit.ReadOnly = true;
            if (txtUnitPrice.Value.ToString() != "0")
            {
                txtResourceTaxAmountIncurration.FormatNumberic(((double.Parse(txtQuantity.Value.ToString()) * double.Parse(txtUnitPrice.Value.ToString()) * double.Parse(txtTaxRate.Value.ToString())) / 100), ConstDatabase.Format_TienVND);
                txtResourceTaxTaxAmountUnit.Value = "0";
            }
            else
                txtResourceTaxAmountIncurration.FormatNumberic((double.Parse(txtQuantity.Value.ToString()) * double.Parse(txtResourceTaxTaxAmountUnit.Value.ToString())), ConstDatabase.Format_TienVND);

        }
        private void txtTaxRate_ValueChanged(object sender, EventArgs e, UltraMaskedEdit txtQuantity, UltraMaskedEdit txtUnitPrice, UltraMaskedEdit txtTaxRate,
             UltraMaskedEdit txtResourceTaxAmountIncurration)
        {
            if (txtQuantity.Value.IsNullOrEmpty())
            {
                txtQuantity.Value = 0;
            }
            if (txtUnitPrice.Value.IsNullOrEmpty())
            {
                txtUnitPrice.Value = 0;
            }
            if (txtTaxRate.Value.IsNullOrEmpty())
            {
                txtTaxRate.Value = 0;
            }
            txtResourceTaxAmountIncurration.FormatNumberic(((double.Parse(txtQuantity.Value.ToString()) * double.Parse(txtUnitPrice.Value.ToString()) * double.Parse(txtTaxRate.Value.ToString())) / 100), ConstDatabase.Format_TienVND);
        }
        private void txtResourceTaxTaxAmountUnit_TextChanged(object sender, EventArgs e, UltraMaskedEdit txtQuantity, UltraMaskedEdit txtResourceTaxTaxAmountUnit, UltraMaskedEdit txtResourceTaxAmountIncurration, UltraMaskedEdit txtUnitPrice, UltraMaskedEdit txtTaxRate)
        {
            if (txtQuantity.Value.IsNullOrEmpty())
            {
                txtQuantity.Value = 0;
            }
            if (txtQuantity.Value.IsNullOrEmpty())
            {
                txtQuantity.Value = 0;
            }
            if (txtUnitPrice.Value.IsNullOrEmpty())
            {
                txtUnitPrice.Value = 0;
            }
            //if (txtResourceTaxTaxAmountUnit.Value.ToString() == "0")
            //    txtUnitPrice.ReadOnly = false;
            //else
            //    txtUnitPrice.ReadOnly = true;
            //if (txtResourceTaxTaxAmountUnit.Value.ToString() != "0" && txtResourceTaxTaxAmountUnit.Text != "0,00")
            //    txtResourceTaxAmountIncurration.Value = double.Parse(txtQuantity.Value.ToString()) * double.Parse(txtResourceTaxTaxAmountUnit.Value.ToString());
            //else
            //    txtResourceTaxAmountIncurration.Value = (double.Parse(txtQuantity.Value.ToString()) * double.Parse(txtUnitPrice.Value.ToString()) * double.Parse(txtTaxRate.Value.ToString())) / 100;
            if (txtResourceTaxTaxAmountUnit.Value.ToString() != "0")
            {
                txtResourceTaxAmountIncurration.FormatNumberic((double.Parse(txtQuantity.Value.ToString()) * double.Parse(txtResourceTaxTaxAmountUnit.Value.ToString())), ConstDatabase.Format_TienVND);
                txtUnitPrice.Value = "0";
            }
            else
                txtResourceTaxAmountIncurration.FormatNumberic(((double.Parse(txtQuantity.Value.ToString()) * double.Parse(txtUnitPrice.Value.ToString()) * double.Parse(txtTaxRate.Value.ToString())) / 100), ConstDatabase.Format_TienVND);

        }
        private void cbbName_TextChanged(object sender, EventArgs e, UltraTextEditor txtUnit, UltraMaskedEdit txtTaxRate)
        {
            var cbb = (UltraCombo)sender;
            if (cbb.Text == "")
            {
                txtUnit.Text = "";
                txtTaxRate.FormatNumberic(txtTaxRate.Value.ToString(), ConstDatabase.Format_Quantity);
            }
        }
        #endregion

        #region event table 1
        private void cbbName1_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (_isEdit)
            {
                List<TM01TAINDetail> lst1 = new List<TM01TAINDetail>();
                lst1 = _select.TM01TAINDetails.Where(n => n.Type == 1).OrderBy(n => n.OrderPriority).ToList();
                if (lst1.Count != 0)
                {
                    foreach (var item in lst1)
                    {
                        if (lst1.IndexOf(item) == 0)
                        {
                            txtUnit1.Value = item.Unit;
                            txtTaxRate1.FormatNumberic(item.TaxRate, ConstDatabase.Format_Quantity);
                        }
                    }
                }
                else
                {
                    MaterialGoodsResourceTaxGroup model = (MaterialGoodsResourceTaxGroup)Utils.getSelectCbbItem(cbbName1);
                    //Guid ID_ = Guid.Parse(cbbName1.Value.ToString());
                    if (model != null)
                    {
                        MaterialGoodsResourceTaxGroup materialGoodsResourceTaxGroup = lstMaterialGoodsResourceTaxGroup.FirstOrDefault(n => n.ID == model.ID);
                        txtUnit1.Value = materialGoodsResourceTaxGroup.Unit;
                        txtTaxRate1.FormatNumberic(materialGoodsResourceTaxGroup.TaxRate, ConstDatabase.Format_Quantity);
                    }
                }

            }
            else
            {
                MaterialGoodsResourceTaxGroup model = (MaterialGoodsResourceTaxGroup)Utils.getSelectCbbItem(cbbName1);
                //Guid ID_ = Guid.Parse(cbbName1.Value.ToString());
                if (model != null)
                {
                    MaterialGoodsResourceTaxGroup materialGoodsResourceTaxGroup = lstMaterialGoodsResourceTaxGroup.FirstOrDefault(n => n.ID == model.ID);
                    txtUnit1.Value = materialGoodsResourceTaxGroup.Unit;
                    txtTaxRate1.FormatNumberic(materialGoodsResourceTaxGroup.TaxRate, ConstDatabase.Format_Quantity);
                }
            }


        }

        private void txtQuantity1_TextChanged(object sender, EventArgs e)
        {
            if (txtQuantity1.Value.IsNullOrEmpty())
            {
                txtQuantity1.Value = 0;
            }
            if (txtUnitPrice1.Value.IsNullOrEmpty())
            {
                txtUnitPrice1.Value = 0;
            }
            if (txtTaxRate1.Value.IsNullOrEmpty())
            {
                txtTaxRate1.Value = 0;
            }
            if (txtResourceTaxTaxAmountUnit1.Value.IsNullOrEmpty())
            {
                txtResourceTaxTaxAmountUnit1.Value = 0;
            }
            if (txtResourceTaxTaxAmountUnit1.Value.ToString() != "0" && txtResourceTaxTaxAmountUnit1.Text != "0,00")
                txtResourceTaxAmountIncurration1.Value = double.Parse(txtQuantity1.Value.ToString()) * double.Parse(txtResourceTaxTaxAmountUnit1.Value.ToString());
            else
                txtResourceTaxAmountIncurration1.Value = (double.Parse(txtQuantity1.Value.ToString()) * double.Parse(txtUnitPrice1.Value.ToString()) * double.Parse(txtTaxRate1.Value.ToString())) / 100;

        }

        private void txtUnitPrice1_TextChanged(object sender, EventArgs e)
        {
            if (txtQuantity1.Value.IsNullOrEmpty())
            {
                txtQuantity1.Value = 0;
            }
            if (txtUnitPrice1.Value.IsNullOrEmpty())
            {
                txtUnitPrice1.Value = 0;
            }
            if (txtTaxRate1.Value.IsNullOrEmpty())
            {
                txtTaxRate1.Value = 0;
            }
            if (txtResourceTaxTaxAmountUnit1.Value.IsNullOrEmpty())
            {
                txtResourceTaxTaxAmountUnit1.Value = 0;
            }
            if (txtUnitPrice1.Value.ToString() != "0")
            {
                txtResourceTaxAmountIncurration1.FormatNumberic(((double.Parse(txtQuantity1.Value.ToString()) * double.Parse(txtUnitPrice1.Value.ToString()) * double.Parse(txtTaxRate1.Value.ToString())) / 100), ConstDatabase.Format_TienVND);
                txtResourceTaxTaxAmountUnit1.Value = "0";
            }
            else
                txtResourceTaxAmountIncurration1.FormatNumberic((double.Parse(txtQuantity1.Value.ToString()) * double.Parse(txtResourceTaxTaxAmountUnit1.Value.ToString())), ConstDatabase.Format_TienVND);

        }

        private void txtTaxRate1_TextChanged(object sender, EventArgs e)
        {
            if (txtQuantity1.Value.IsNullOrEmpty())
            {
                txtQuantity1.Value = 0;
            }
            if (txtUnitPrice1.Value.IsNullOrEmpty())
            {
                txtUnitPrice1.Value = 0;
            }
            if (txtTaxRate1.Value.IsNullOrEmpty())
            {
                txtTaxRate1.Value = 0;
            }
            txtResourceTaxAmountIncurration1.Value = (double.Parse(txtQuantity1.Value.ToString()) * double.Parse(txtUnitPrice1.Value.ToString()) * double.Parse(txtTaxRate1.Value.ToString())) / 100;
        }

        private void txtResourceTaxTaxAmountUnit1_TextChanged(object sender, EventArgs e)
        {
            if (txtQuantity1.Value.IsNullOrEmpty())
            {
                txtQuantity1.Value = 0;
            }
            if (txtUnitPrice1.Value.IsNullOrEmpty())
            {
                txtUnitPrice1.Value = 0;
            }
            if (txtResourceTaxTaxAmountUnit1.Value.IsNullOrEmpty())
            {
                txtResourceTaxTaxAmountUnit1.Value = 0;
            }
            if (txtResourceTaxTaxAmountUnit1.Value.ToString() != "0")
            {
                txtResourceTaxAmountIncurration1.FormatNumberic((double.Parse(txtQuantity1.Value.ToString()) * double.Parse(txtResourceTaxTaxAmountUnit1.Value.ToString())), ConstDatabase.Format_TienVND);
                txtUnitPrice1.Value = "0";
            }
            else
                txtResourceTaxAmountIncurration1.Value = (double.Parse(txtQuantity1.Value.ToString()) * double.Parse(txtUnitPrice1.Value.ToString()) * double.Parse(txtTaxRate1.Value.ToString())) / 100;

        }
        private void cbbName1_TextChanged(object sender, EventArgs e)
        {
            if (cbbName1.Text == "")
            {
                txtUnit1.Text = "";
                txtTaxRate1.FormatNumberic(txtTaxRate1.Value.ToString(), ConstDatabase.Format_Quantity);
            }
        }
        #endregion

        #region event table 2
        private void cbbName2_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (_isEdit)
            {
                List<TM01TAINDetail> lst2 = new List<TM01TAINDetail>();
                lst2 = _select.TM01TAINDetails.Where(n => n.Type == 2).OrderBy(n => n.OrderPriority).ToList();
                if (lst2.Count != 0)
                {
                    foreach (var item in lst2)
                    {
                        if (lst2.IndexOf(item) == 0)
                        {
                            txtUnit2.Value = item.Unit;
                            txtTaxRate2.FormatNumberic(item.TaxRate, ConstDatabase.Format_Quantity);
                        }
                    }
                }
                else
                {
                    MaterialGoodsResourceTaxGroup model = (MaterialGoodsResourceTaxGroup)Utils.getSelectCbbItem(cbbName2);
                    //Guid ID_ = Guid.Parse(cbbName2.Value.ToString());
                    if (model != null)
                    {
                        MaterialGoodsResourceTaxGroup materialGoodsResourceTaxGroup = lstMaterialGoodsResourceTaxGroup.FirstOrDefault(n => n.ID == model.ID);
                        txtUnit2.Value = materialGoodsResourceTaxGroup.Unit;
                        txtTaxRate2.FormatNumberic(materialGoodsResourceTaxGroup.TaxRate, ConstDatabase.Format_Quantity);
                    }
                }

            }
            else
            {
                MaterialGoodsResourceTaxGroup model = (MaterialGoodsResourceTaxGroup)Utils.getSelectCbbItem(cbbName2);
                //Guid ID_ = Guid.Parse(cbbName2.Value.ToString());
                if (model != null)
                {
                    MaterialGoodsResourceTaxGroup materialGoodsResourceTaxGroup = lstMaterialGoodsResourceTaxGroup.FirstOrDefault(n => n.ID == model.ID);
                    txtUnit2.Value = materialGoodsResourceTaxGroup.Unit;
                    txtTaxRate2.FormatNumberic(materialGoodsResourceTaxGroup.TaxRate, ConstDatabase.Format_Quantity);
                }
            }
        }
        private void txtQuantity2_TextChanged(object sender, EventArgs e)
        {
            if (txtQuantity2.Value.IsNullOrEmpty())
            {
                txtQuantity2.Value = 0;
            }
            if (txtUnitPrice2.Value.IsNullOrEmpty())
            {
                txtUnitPrice2.Value = 0;
            }
            if (txtTaxRate2.Value.IsNullOrEmpty())
            {
                txtTaxRate2.Value = 0;
            }
            if (txtResourceTaxTaxAmountUnit2.Value.IsNullOrEmpty())
            {
                txtResourceTaxTaxAmountUnit2.Value = 0;
            }
            if (txtResourceTaxTaxAmountUnit2.Value.ToString() != "0" && txtResourceTaxTaxAmountUnit2.Text != "0,00")
                txtResourceTaxAmountIncurration2.Value = double.Parse(txtQuantity2.Value.ToString()) * double.Parse(txtResourceTaxTaxAmountUnit2.Value.ToString());
            else
                txtResourceTaxAmountIncurration2.Value = (double.Parse(txtQuantity2.Value.ToString()) * double.Parse(txtUnitPrice2.Value.ToString()) * double.Parse(txtTaxRate2.Value.ToString())) / 100;

        }

        private void txtUnitPrice2_TextChanged(object sender, EventArgs e)
        {
            if (txtQuantity2.Value.IsNullOrEmpty())
            {
                txtQuantity2.Value = 0;
            }
            if (txtUnitPrice2.Value.IsNullOrEmpty())
            {
                txtUnitPrice2.Value = 0;
            }
            if (txtTaxRate2.Value.IsNullOrEmpty())
            {
                txtTaxRate2.Value = 0;
            }
            if (txtResourceTaxTaxAmountUnit2.Value.IsNullOrEmpty())
            {
                txtResourceTaxTaxAmountUnit2.Value = 0;
            }
            if (txtUnitPrice2.Value.ToString() != "0")
            {
                txtResourceTaxAmountIncurration2.FormatNumberic(((double.Parse(txtQuantity2.Value.ToString()) * double.Parse(txtUnitPrice2.Value.ToString()) * double.Parse(txtTaxRate2.Value.ToString())) / 100), ConstDatabase.Format_TienVND);
                txtResourceTaxTaxAmountUnit2.Value = "0";
            }
            else
                txtResourceTaxAmountIncurration2.FormatNumberic((double.Parse(txtQuantity2.Value.ToString()) * double.Parse(txtResourceTaxTaxAmountUnit2.Value.ToString())), ConstDatabase.Format_TienVND);

        }

        private void txtTaxRate2_TextChanged(object sender, EventArgs e)
        {
            if (txtQuantity2.Value.IsNullOrEmpty())
            {
                txtQuantity2.Value = 0;
            }
            if (txtUnitPrice2.Value.IsNullOrEmpty())
            {
                txtUnitPrice2.Value = 0;
            }
            if (txtTaxRate2.Value.IsNullOrEmpty())
            {
                txtTaxRate2.Value = 0;
            }
            txtResourceTaxAmountIncurration2.Value = (double.Parse(txtQuantity2.Value.ToString()) * double.Parse(txtUnitPrice2.Value.ToString()) * double.Parse(txtTaxRate2.Value.ToString())) / 100;
        }

        private void txtResourceTaxTaxAmountUnit2_TextChanged(object sender, EventArgs e)
        {
            if (txtQuantity2.Value.IsNullOrEmpty())
            {
                txtQuantity2.Value = 0;
            }
            if (txtUnitPrice2.Value.IsNullOrEmpty())
            {
                txtUnitPrice2.Value = 0;
            }
            if (txtResourceTaxTaxAmountUnit2.Value.IsNullOrEmpty())
            {
                txtResourceTaxTaxAmountUnit2.Value = 0;
            }
            if (txtResourceTaxTaxAmountUnit2.Value.ToString() != "0")
            {
                txtResourceTaxAmountIncurration2.FormatNumberic((double.Parse(txtQuantity2.Value.ToString()) * double.Parse(txtResourceTaxTaxAmountUnit2.Value.ToString())), ConstDatabase.Format_TienVND);
                txtUnitPrice2.Value = "0";
            }
            else
                txtResourceTaxAmountIncurration2.FormatNumberic(((double.Parse(txtQuantity2.Value.ToString()) * double.Parse(txtUnitPrice2.Value.ToString()) * double.Parse(txtTaxRate2.Value.ToString())) / 100), ConstDatabase.Format_TienVND);

        }
        private void cbbName2_TextChanged(object sender, EventArgs e)
        {
            if (cbbName2.Text == "")
            {
                txtUnit2.Text = "";
                txtTaxRate2.FormatNumberic(txtTaxRate2.Value.ToString(), ConstDatabase.Format_Quantity);
            }
        }
        #endregion

        #region event table 3
        private void cbbName3_RowSelected(object sender, RowSelectedEventArgs e)
        {

            if (_isEdit)
            {
                List<TM01TAINDetail> lst3 = new List<TM01TAINDetail>();
                lst3 = _select.TM01TAINDetails.Where(n => n.Type == 3).OrderBy(n => n.OrderPriority).ToList();
                if (lst3.Count != 0)
                {
                    foreach (var item in lst3)
                    {
                        if (lst3.IndexOf(item) == 0)
                        {
                            txtUnit3.Value = item.Unit;
                            txtTaxRate3.FormatNumberic(item.TaxRate, ConstDatabase.Format_Quantity);
                        }
                    }
                }
                else
                {
                    MaterialGoodsResourceTaxGroup model = (MaterialGoodsResourceTaxGroup)Utils.getSelectCbbItem(cbbName3);
                    //Guid ID_ = Guid.Parse(cbbName3.Value.ToString());
                    if (model != null)
                    {
                        MaterialGoodsResourceTaxGroup materialGoodsResourceTaxGroup = lstMaterialGoodsResourceTaxGroup.FirstOrDefault(n => n.ID == model.ID);
                        txtUnit3.Value = materialGoodsResourceTaxGroup.Unit;
                        txtTaxRate3.FormatNumberic(materialGoodsResourceTaxGroup.TaxRate, ConstDatabase.Format_Quantity);
                    }
                }

            }
            else
            {
                MaterialGoodsResourceTaxGroup model = (MaterialGoodsResourceTaxGroup)Utils.getSelectCbbItem(cbbName3);
                //Guid ID_ = Guid.Parse(cbbName3.Value.ToString());
                if (model != null)
                {
                    MaterialGoodsResourceTaxGroup materialGoodsResourceTaxGroup = lstMaterialGoodsResourceTaxGroup.FirstOrDefault(n => n.ID == model.ID);
                    txtUnit3.Value = materialGoodsResourceTaxGroup.Unit;
                    txtTaxRate3.FormatNumberic(materialGoodsResourceTaxGroup.TaxRate, ConstDatabase.Format_Quantity);
                }
            }
        }
        private void txtQuantity3_TextChanged(object sender, EventArgs e)
        {
            if (txtQuantity3.Value.IsNullOrEmpty())
            {
                txtQuantity3.Value = 0;
            }
            if (txtUnitPrice3.Value.IsNullOrEmpty())
            {
                txtUnitPrice3.Value = 0;
            }
            if (txtTaxRate3.Value.IsNullOrEmpty())
            {
                txtTaxRate3.Value = 0;
            }
            if (txtResourceTaxTaxAmountUnit3.Value.IsNullOrEmpty())
            {
                txtResourceTaxTaxAmountUnit3.Value = 0;
            }
            if (txtResourceTaxTaxAmountUnit3.Value.ToString() != "0" && txtResourceTaxTaxAmountUnit3.Text != "0,00")
                txtResourceTaxAmountIncurration3.Value = double.Parse(txtQuantity3.Value.ToString()) * double.Parse(txtResourceTaxTaxAmountUnit3.Value.ToString());
            else
                txtResourceTaxAmountIncurration3.Value = (double.Parse(txtQuantity3.Value.ToString()) * double.Parse(txtUnitPrice3.Value.ToString()) * double.Parse(txtTaxRate3.Value.ToString())) / 100;

        }

        private void txtUnitPrice3_TextChanged(object sender, EventArgs e)
        {
            if (txtQuantity3.Value.IsNullOrEmpty())
            {
                txtQuantity3.Value = 0;
            }
            if (txtUnitPrice3.Value.IsNullOrEmpty())
            {
                txtUnitPrice3.Value = 0;
            }
            if (txtTaxRate3.Value.IsNullOrEmpty())
            {
                txtTaxRate3.Value = 0;
            }
            if (txtResourceTaxTaxAmountUnit3.Value.IsNullOrEmpty())
            {
                txtResourceTaxTaxAmountUnit3.Value = 0;
            }
            if (txtUnitPrice3.Value.ToString() != "0")
            {
                txtResourceTaxAmountIncurration3.FormatNumberic(((double.Parse(txtQuantity3.Value.ToString()) * double.Parse(txtUnitPrice3.Value.ToString()) * double.Parse(txtTaxRate3.Value.ToString())) / 100), ConstDatabase.Format_TienVND);
                txtResourceTaxTaxAmountUnit3.Value = "0";
            }
            else
                txtResourceTaxAmountIncurration3.FormatNumberic((double.Parse(txtQuantity3.Value.ToString()) * double.Parse(txtResourceTaxTaxAmountUnit3.Value.ToString())), ConstDatabase.Format_TienVND);

        }

        private void txtTaxRate3_TextChanged(object sender, EventArgs e)
        {
            if (txtQuantity3.Value.IsNullOrEmpty())
            {
                txtQuantity3.Value = 0;
            }
            if (txtUnitPrice3.Value.IsNullOrEmpty())
            {
                txtUnitPrice3.Value = 0;
            }
            if (txtTaxRate3.Value.IsNullOrEmpty())
            {
                txtTaxRate3.Value = 0;
            }
            if (txtResourceTaxTaxAmountUnit3.Value.IsNullOrEmpty())
            {
                txtResourceTaxTaxAmountUnit3.Value = 0;
            }
            txtResourceTaxAmountIncurration3.Value = (double.Parse(txtQuantity3.Value.ToString()) * double.Parse(txtUnitPrice3.Value.ToString()) * double.Parse(txtTaxRate3.Value.ToString())) / 100;
        }

        private void txtResourceTaxTaxAmountUnit3_TextChanged(object sender, EventArgs e)
        {
            if (txtQuantity3.Value.IsNullOrEmpty())
            {
                txtQuantity3.Value = 0;
            }
            if (txtUnitPrice3.Value.IsNullOrEmpty())
            {
                txtUnitPrice3.Value = 0;
            }
            if (txtResourceTaxTaxAmountUnit3.Value.IsNullOrEmpty())
            {
                txtResourceTaxTaxAmountUnit3.Value = 0;
            }
            if (txtResourceTaxTaxAmountUnit3.Value.ToString() != "0")
            {
                txtResourceTaxAmountIncurration3.FormatNumberic((double.Parse(txtQuantity3.Value.ToString()) * double.Parse(txtResourceTaxTaxAmountUnit3.Value.ToString())), ConstDatabase.Format_TienVND);
                txtUnitPrice3.Value = "0";
            }
            else
                txtResourceTaxAmountIncurration3.FormatNumberic(((double.Parse(txtQuantity3.Value.ToString()) * double.Parse(txtUnitPrice3.Value.ToString()) * double.Parse(txtTaxRate3.Value.ToString())) / 100), ConstDatabase.Format_TienVND);

        }
        private void cbbName3_TextChanged(object sender, EventArgs e)
        {
            if (cbbName3.Text == "")
            {
                txtUnit3.Text = "";
                txtTaxRate3.FormatNumberic(txtTaxRate3.Value.ToString(), ConstDatabase.Format_Quantity);
            }
        }
        #endregion
        #endregion

        #region hàm xóa dòng
        public void remove_row(TableLayoutPanel panel, int row_index_to_remove)
        {
            if (row_index_to_remove >= panel.RowCount)
            {
                return;
            }

            // delete all controls of row that we want to delete
            for (int i = 0; i < panel.ColumnCount; i++)
            {
                var control = panel.GetControlFromPosition(i, row_index_to_remove);
                panel.Controls.Remove(control);
            }

            // move up row controls that comes after row we want to remove
            for (int i = row_index_to_remove + 1; i < panel.RowCount; i++)
            {
                for (int j = 0; j < panel.ColumnCount; j++)
                {
                    var control = panel.GetControlFromPosition(j, i);
                    if (control != null)
                    {
                        panel.SetRow(control, i - 1);
                    }
                }
            }

            // remove last row
            //panel.RowStyles.RemoveAt(panel.RowCount - 1);
            panel.RowCount--;
        }
        #endregion
        private void FillNull(TableLayoutPanel tableLayoutPanel)
        {
            var control1 = tableLayoutPanel.GetControlFromPosition(1, 1);
            var control2 = tableLayoutPanel.GetControlFromPosition(2, 1);
            var control3 = tableLayoutPanel.GetControlFromPosition(3, 1);
            var control4 = tableLayoutPanel.GetControlFromPosition(4, 1);
            var control5 = tableLayoutPanel.GetControlFromPosition(5, 1);
            var control6 = tableLayoutPanel.GetControlFromPosition(6, 1);
            var control7 = tableLayoutPanel.GetControlFromPosition(7, 1);
            var control8 = tableLayoutPanel.GetControlFromPosition(8, 1);
            var control9 = tableLayoutPanel.GetControlFromPosition(9, 1);
            if (control1 is UltraCombo)
                control1.Text = "";
            if (control2 is UltraTextEditor)
                control2.Text = null;
            if (control3 is UltraMaskedEdit)
                control3.FormatNumberic(0, ConstDatabase.Format_Quantity);
            if (control4 is UltraMaskedEdit)
                control4.FormatNumberic(0, ConstDatabase.Format_Quantity);
            if (control5 is UltraMaskedEdit)
                control5.FormatNumberic(0, ConstDatabase.Format_Quantity);
            if (control6 is UltraMaskedEdit)
                control6.FormatNumberic(0, ConstDatabase.Format_TienVND);
            if (control7 is UltraMaskedEdit)
                control7.FormatNumberic(0, ConstDatabase.Format_TienVND);
            if (control8 is UltraMaskedEdit)
                control8.FormatNumberic(0, ConstDatabase.Format_TienVND);
            if (control9 is UltraMaskedEdit)
                control9.FormatNumberic(0, ConstDatabase.Format_TienVND);
        }

        #region tsm1 add
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int row = GetRow(tableLayoutPanel1);
                if (row == 0) return;

                RowStyle temp = tableLayoutPanel1.RowStyles[tableLayoutPanel1.RowCount - 1];
                RowStyle rowStyle = new RowStyle(temp.SizeType, temp.Height);
                tableLayoutPanel1.RowStyles.Insert(GetRow(tableLayoutPanel1), rowStyle);
                tableLayoutPanel1.RowCount++;

                foreach (Control ExistControl in tableLayoutPanel1.Controls)
                {
                    if (tableLayoutPanel1.GetRow(ExistControl) > row)
                        tableLayoutPanel1.SetRow(ExistControl, tableLayoutPanel1.GetRow(ExistControl) + 1);
                }

                UltraTextEditor txtSTT = new UltraTextEditor();
                txtSTT.ReadOnly = true;
                txtSTT.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                txtSTT.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                txtSTT.AutoSize = false;
                txtSTT.Dock = DockStyle.Fill;
                txtSTT.Margin = new Padding(0);

                UltraTextEditor txtUnit = new UltraTextEditor();
                txtUnit.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                txtUnit.AutoSize = false;
                txtUnit.Dock = DockStyle.Fill;
                txtUnit.Margin = new Padding(0);

                UltraMaskedEdit txtQuantity = new UltraMaskedEdit();
                txtQuantity.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtQuantity.Value = 0;
                ConfigMaskedEdit1(txtQuantity);

                UltraMaskedEdit txtUnitPrice = new UltraMaskedEdit();
                txtUnitPrice.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtUnitPrice.Value = 0;
                ConfigMaskedEdit1(txtUnitPrice);

                UltraMaskedEdit txtTaxRate = new UltraMaskedEdit();
                txtTaxRate.ReadOnly = true;
                txtTaxRate.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtTaxRate.Value = 0;
                ConfigMaskedEdit1(txtTaxRate);

                UltraCombo cbbName = new UltraCombo();
                cbbName.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                cbbName.Dock = DockStyle.Fill;
                cbbName.AutoSize = false;
                cbbName.Margin = new Padding(0);
                ConfigCbb(cbbName);

                UltraMaskedEdit txtResourceTaxTaxAmountUnit = new UltraMaskedEdit();
                txtResourceTaxTaxAmountUnit.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtResourceTaxTaxAmountUnit.Value = 0;
                ConfigMaskedEdit(txtResourceTaxTaxAmountUnit);

                UltraMaskedEdit txtResourceTaxAmountIncurration = new UltraMaskedEdit();
                txtResourceTaxAmountIncurration.ReadOnly = true;
                txtResourceTaxAmountIncurration.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtResourceTaxAmountIncurration.Value = 0;
                ConfigMaskedEdit(txtResourceTaxAmountIncurration);

                UltraMaskedEdit txtResourceTaxAmountDeduction = new UltraMaskedEdit();
                txtResourceTaxAmountDeduction.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtResourceTaxAmountDeduction.Value = 0;
                ConfigMaskedEdit(txtResourceTaxAmountDeduction);

                UltraMaskedEdit txtResourceTaxAmount = new UltraMaskedEdit();
                txtResourceTaxAmount.ReadOnly = true;
                txtResourceTaxAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtResourceTaxAmount.Value = 0;
                ConfigMaskedEdit(txtResourceTaxAmount);

                txtSTT.ContextMenuStrip = contextMenuStrip1;
                txtUnit.ContextMenuStrip = contextMenuStrip1;
                txtQuantity.ContextMenuStrip = contextMenuStrip1;
                txtUnitPrice.ContextMenuStrip = contextMenuStrip1;
                txtTaxRate.ContextMenuStrip = contextMenuStrip1;
                cbbName.ContextMenuStrip = contextMenuStrip1;
                txtResourceTaxTaxAmountUnit.ContextMenuStrip = contextMenuStrip1;
                txtResourceTaxAmountIncurration.ContextMenuStrip = contextMenuStrip1;
                txtResourceTaxAmountDeduction.ContextMenuStrip = contextMenuStrip1;
                txtResourceTaxAmount.ContextMenuStrip = contextMenuStrip1;

                #region Format control
                Utils.FormatNumberic(txtQuantity, ConstDatabase.Format_Quantity);
                Utils.FormatNumberic(txtUnitPrice, ConstDatabase.Format_Quantity);
                Utils.FormatNumberic(txtTaxRate, ConstDatabase.Format_Quantity);
                Utils.FormatNumberic(txtResourceTaxTaxAmountUnit, ConstDatabase.Format_TienVND);
                Utils.FormatNumberic(txtResourceTaxAmountIncurration, ConstDatabase.Format_TienVND);
                Utils.FormatNumberic(txtResourceTaxAmountDeduction, ConstDatabase.Format_TienVND);
                Utils.FormatNumberic(txtResourceTaxAmount, ConstDatabase.Format_TienVND);
                #endregion

                #region add event control
                cbbName.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler((s, a) => cbb_RowSelected(s, a, txtUnit, txtTaxRate));
                cbbName.TextChanged += new System.EventHandler((s, a) => cbbName_TextChanged(s, a, txtUnit, txtTaxRate));
                txtQuantity.ValueChanged += new System.EventHandler((s, a) => txtQuantity_ValueChanged(s, a, txtQuantity, txtUnitPrice, txtTaxRate, txtResourceTaxTaxAmountUnit, txtResourceTaxAmountIncurration));
                txtUnitPrice.ValueChanged += new System.EventHandler((s, a) => txtUnitPrice_ValueChanged(s, a, txtQuantity, txtUnitPrice, txtTaxRate, txtResourceTaxTaxAmountUnit, txtResourceTaxAmountIncurration));
                txtTaxRate.ValueChanged += new System.EventHandler((s, a) => txtTaxRate_ValueChanged(s, a, txtQuantity, txtUnitPrice, txtTaxRate, txtResourceTaxAmountIncurration));
                txtResourceTaxTaxAmountUnit.ValueChanged += new System.EventHandler((s, a) => txtResourceTaxTaxAmountUnit_TextChanged(s, a, txtQuantity, txtResourceTaxTaxAmountUnit, txtResourceTaxAmountIncurration, txtUnitPrice, txtTaxRate));
                txtResourceTaxAmountIncurration.ValueChanged += new System.EventHandler((s, a) => txtResourceTaxAmountIncurration_TextChanged(s, a, txtResourceTaxAmount, txtResourceTaxAmountIncurration, txtResourceTaxAmountDeduction));
                txtResourceTaxAmountDeduction.ValueChanged += new EventHandler((s, a) => txtResourceTaxAmountDeduction_TextChanged(s, a, txtResourceTaxAmount, txtResourceTaxAmountIncurration, txtResourceTaxAmountDeduction));
                txtResourceTaxAmount.ValueChanged += new EventHandler(txtResourceTaxAmount_TextChanged);
                #endregion

                tableLayoutPanel1.Controls.Add(txtSTT, 0, row + 1);
                tableLayoutPanel1.Controls.Add(cbbName, 1, row + 1);
                tableLayoutPanel1.Controls.Add(txtUnit, 2, row + 1);
                tableLayoutPanel1.Controls.Add(txtQuantity, 3, row + 1);
                tableLayoutPanel1.Controls.Add(txtUnitPrice, 4, row + 1);
                tableLayoutPanel1.Controls.Add(txtTaxRate, 5, row + 1);
                tableLayoutPanel1.Controls.Add(txtResourceTaxTaxAmountUnit, 6, row + 1);
                tableLayoutPanel1.Controls.Add(txtResourceTaxAmountIncurration, 7, row + 1);
                tableLayoutPanel1.Controls.Add(txtResourceTaxAmountDeduction, 8, row + 1);
                tableLayoutPanel1.Controls.Add(txtResourceTaxAmount, 9, row + 1);

                tableLayoutPanel2.Location = new Point(tableLayoutPanel2.Location.X, tableLayoutPanel1.Location.Y + tableLayoutPanel1.Height);
                tableLayoutPanel3.Location = new Point(tableLayoutPanel3.Location.X, tableLayoutPanel2.Location.Y + tableLayoutPanel2.Height);
                tableLayoutPanel5.Location = new Point(tableLayoutPanel5.Location.X, tableLayoutPanel3.Location.Y + tableLayoutPanel3.Height);
                ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel5.Location.Y + tableLayoutPanel5.Height);

                for (int i = 0; i < tableLayoutPanel1.RowCount; i++)
                {
                    var control = tableLayoutPanel1.GetControlFromPosition(0, i);
                    if (control is UltraTextEditor)
                    {
                        UltraTextEditor ultraTextEditor = control as UltraTextEditor;
                        ultraTextEditor.Value = i;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region tsm1 delete
        private void tsmDelete_Click(object sender, EventArgs e)
        {
            int r = tableLayoutPanel1.RowCount - 1;
            if (r == 1)
            {
                FillNull(tableLayoutPanel1);
                return;
            }
            int row = GetRow(tableLayoutPanel1);
            if (row == 0) return;
            remove_row(tableLayoutPanel1, row);
            tableLayoutPanel2.Location = new Point(tableLayoutPanel2.Location.X, tableLayoutPanel1.Location.Y + tableLayoutPanel1.Height);
            tableLayoutPanel3.Location = new Point(tableLayoutPanel3.Location.X, tableLayoutPanel2.Location.Y + tableLayoutPanel2.Height);
            tableLayoutPanel5.Location = new Point(tableLayoutPanel5.Location.X, tableLayoutPanel3.Location.Y + tableLayoutPanel3.Height);
            ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel5.Location.Y + tableLayoutPanel5.Height);

            for (int i = 0; i < tableLayoutPanel1.RowCount; i++)
            {
                var control = tableLayoutPanel1.GetControlFromPosition(0, i);
                if (control is UltraTextEditor)
                {
                    UltraTextEditor ultraTextEditor = control as UltraTextEditor;
                    ultraTextEditor.Value = i;
                }
            }
            if (row == tableLayoutPanel1.RowCount)
            {
                var control1 = tableLayoutPanel1.GetControlFromPosition(1, row - 1);
                if (control1 is UltraCombo)
                    control1.Focus();
            }
            else
            {
                var control1 = tableLayoutPanel1.GetControlFromPosition(1, row);
                if (control1 is UltraCombo)
                    control1.Focus();
            }
        }
        #endregion

        #region tsm2 add
        private void tsmAdd2_Click(object sender, EventArgs e)
        {
            try
            {
                int row = GetRow(tableLayoutPanel2);
                if (row == 0) return;

                RowStyle temp = tableLayoutPanel2.RowStyles[tableLayoutPanel2.RowCount - 1];
                RowStyle rowStyle = new RowStyle(temp.SizeType, temp.Height);
                tableLayoutPanel2.RowStyles.Insert(GetRow(tableLayoutPanel2), rowStyle);
                tableLayoutPanel2.RowCount++;

                foreach (Control ExistControl in tableLayoutPanel2.Controls)
                {
                    if (tableLayoutPanel2.GetRow(ExistControl) > row)
                        tableLayoutPanel2.SetRow(ExistControl, tableLayoutPanel2.GetRow(ExistControl) + 1);
                }

                UltraTextEditor txtSTT = new UltraTextEditor();
                txtSTT.ReadOnly = true;
                txtSTT.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                txtSTT.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                txtSTT.AutoSize = false;
                txtSTT.Dock = DockStyle.Fill;
                txtSTT.Margin = new Padding(0);

                UltraTextEditor txtUnit = new UltraTextEditor();
                txtUnit.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                txtUnit.AutoSize = false;
                txtUnit.Dock = DockStyle.Fill;
                txtUnit.Margin = new Padding(0);

                UltraMaskedEdit txtQuantity = new UltraMaskedEdit();
                txtQuantity.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtQuantity.Value = 0;
                ConfigMaskedEdit1(txtQuantity);

                UltraMaskedEdit txtUnitPrice = new UltraMaskedEdit();
                txtUnitPrice.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtUnitPrice.Value = 0;
                ConfigMaskedEdit1(txtUnitPrice);

                UltraMaskedEdit txtTaxRate = new UltraMaskedEdit();
                txtTaxRate.ReadOnly = true;
                txtTaxRate.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtTaxRate.Value = 0;
                ConfigMaskedEdit1(txtTaxRate);

                UltraCombo cbbName = new UltraCombo();
                cbbName.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                cbbName.Dock = DockStyle.Fill;
                cbbName.AutoSize = false;
                cbbName.Margin = new Padding(0);
                ConfigCbb(cbbName);

                UltraMaskedEdit txtResourceTaxTaxAmountUnit = new UltraMaskedEdit();
                txtResourceTaxTaxAmountUnit.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtResourceTaxTaxAmountUnit.Value = 0;
                ConfigMaskedEdit(txtResourceTaxTaxAmountUnit);

                UltraMaskedEdit txtResourceTaxAmountIncurration = new UltraMaskedEdit();
                txtResourceTaxAmountIncurration.ReadOnly = true;
                txtResourceTaxAmountIncurration.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtResourceTaxAmountIncurration.Value = 0;
                ConfigMaskedEdit(txtResourceTaxAmountIncurration);

                UltraMaskedEdit txtResourceTaxAmountDeduction = new UltraMaskedEdit();
                txtResourceTaxAmountDeduction.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtResourceTaxAmountDeduction.Value = 0;
                ConfigMaskedEdit(txtResourceTaxAmountDeduction);

                UltraMaskedEdit txtResourceTaxAmount = new UltraMaskedEdit();
                txtResourceTaxAmount.ReadOnly = true;
                txtResourceTaxAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtResourceTaxAmount.Value = 0;
                ConfigMaskedEdit(txtResourceTaxAmount);

                txtSTT.ContextMenuStrip = contextMenuStrip2;
                txtUnit.ContextMenuStrip = contextMenuStrip2;
                txtQuantity.ContextMenuStrip = contextMenuStrip2;
                txtUnitPrice.ContextMenuStrip = contextMenuStrip2;
                txtTaxRate.ContextMenuStrip = contextMenuStrip2;
                cbbName.ContextMenuStrip = contextMenuStrip2;
                txtResourceTaxTaxAmountUnit.ContextMenuStrip = contextMenuStrip2;
                txtResourceTaxAmountIncurration.ContextMenuStrip = contextMenuStrip2;
                txtResourceTaxAmountDeduction.ContextMenuStrip = contextMenuStrip2;
                txtResourceTaxAmount.ContextMenuStrip = contextMenuStrip2;

                #region Format control
                Utils.FormatNumberic(txtQuantity, ConstDatabase.Format_Quantity);
                Utils.FormatNumberic(txtUnitPrice, ConstDatabase.Format_Quantity);
                Utils.FormatNumberic(txtTaxRate, ConstDatabase.Format_Quantity);
                Utils.FormatNumberic(txtResourceTaxTaxAmountUnit, ConstDatabase.Format_TienVND);
                Utils.FormatNumberic(txtResourceTaxAmountIncurration, ConstDatabase.Format_TienVND);
                Utils.FormatNumberic(txtResourceTaxAmountDeduction, ConstDatabase.Format_TienVND);
                Utils.FormatNumberic(txtResourceTaxAmount, ConstDatabase.Format_TienVND);
                #endregion

                #region add event control
                cbbName.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler((s, a) => cbb_RowSelected(s, a, txtUnit, txtTaxRate));
                cbbName.TextChanged += new System.EventHandler((s, a) => cbbName_TextChanged(s, a, txtUnit, txtTaxRate));
                txtQuantity.ValueChanged += new System.EventHandler((s, a) => txtQuantity_ValueChanged(s, a, txtQuantity, txtUnitPrice, txtTaxRate, txtResourceTaxTaxAmountUnit, txtResourceTaxAmountIncurration));
                txtUnitPrice.ValueChanged += new System.EventHandler((s, a) => txtUnitPrice_ValueChanged(s, a, txtQuantity, txtUnitPrice, txtTaxRate, txtResourceTaxTaxAmountUnit, txtResourceTaxAmountIncurration));
                txtTaxRate.ValueChanged += new System.EventHandler((s, a) => txtTaxRate_ValueChanged(s, a, txtQuantity, txtUnitPrice, txtTaxRate, txtResourceTaxAmountIncurration));
                txtResourceTaxTaxAmountUnit.ValueChanged += new System.EventHandler((s, a) => txtResourceTaxTaxAmountUnit_TextChanged(s, a, txtQuantity, txtResourceTaxTaxAmountUnit, txtResourceTaxAmountIncurration, txtUnitPrice, txtTaxRate));
                txtResourceTaxAmountIncurration.ValueChanged += new EventHandler((s, a) => txtResourceTaxAmountIncurration_TextChanged(s, a, txtResourceTaxAmount, txtResourceTaxAmountIncurration, txtResourceTaxAmountDeduction));
                txtResourceTaxAmountDeduction.ValueChanged += new EventHandler((s, a) => txtResourceTaxAmountDeduction_TextChanged(s, a, txtResourceTaxAmount, txtResourceTaxAmountIncurration, txtResourceTaxAmountDeduction));
                txtResourceTaxAmount.ValueChanged += new EventHandler(txtResourceTaxAmount_TextChanged);
                #endregion

                tableLayoutPanel2.Controls.Add(txtSTT, 0, row + 1);
                tableLayoutPanel2.Controls.Add(cbbName, 1, row + 1);
                tableLayoutPanel2.Controls.Add(txtUnit, 2, row + 1);
                tableLayoutPanel2.Controls.Add(txtQuantity, 3, row + 1);
                tableLayoutPanel2.Controls.Add(txtUnitPrice, 4, row + 1);
                tableLayoutPanel2.Controls.Add(txtTaxRate, 5, row + 1);
                tableLayoutPanel2.Controls.Add(txtResourceTaxTaxAmountUnit, 6, row + 1);
                tableLayoutPanel2.Controls.Add(txtResourceTaxAmountIncurration, 7, row + 1);
                tableLayoutPanel2.Controls.Add(txtResourceTaxAmountDeduction, 8, row + 1);
                tableLayoutPanel2.Controls.Add(txtResourceTaxAmount, 9, row + 1);

                tableLayoutPanel3.Location = new Point(tableLayoutPanel3.Location.X, tableLayoutPanel2.Location.Y + tableLayoutPanel2.Height);
                tableLayoutPanel5.Location = new Point(tableLayoutPanel5.Location.X, tableLayoutPanel3.Location.Y + tableLayoutPanel3.Height);
                ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel5.Location.Y + tableLayoutPanel5.Height);

                for (int i = 0; i < tableLayoutPanel2.RowCount; i++)
                {
                    var control = tableLayoutPanel2.GetControlFromPosition(0, i);
                    if (control is UltraTextEditor)
                    {
                        UltraTextEditor ultraTextEditor = control as UltraTextEditor;
                        ultraTextEditor.Value = i;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region tsm2 delete
        private void tsmDelete2_Click(object sender, EventArgs e)
        {
            int r = tableLayoutPanel2.RowCount - 1;
            if (r == 1)
            {
                FillNull(tableLayoutPanel2);
                return;
            }
            int row = GetRow(tableLayoutPanel2);
            if (row == 0) return;
            remove_row(tableLayoutPanel2, row);
            tableLayoutPanel3.Location = new Point(tableLayoutPanel3.Location.X, tableLayoutPanel2.Location.Y + tableLayoutPanel2.Height);
            tableLayoutPanel5.Location = new Point(tableLayoutPanel5.Location.X, tableLayoutPanel3.Location.Y + tableLayoutPanel3.Height);
            ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel5.Location.Y + tableLayoutPanel5.Height);

            for (int i = 0; i < tableLayoutPanel2.RowCount; i++)
            {
                var control = tableLayoutPanel2.GetControlFromPosition(0, i);
                if (control is UltraTextEditor)
                {
                    UltraTextEditor ultraTextEditor = control as UltraTextEditor;
                    ultraTextEditor.Value = i;
                }
            }
            if (row == tableLayoutPanel2.RowCount)
            {
                var control1 = tableLayoutPanel2.GetControlFromPosition(1, row - 1);
                if (control1 is UltraCombo)
                    control1.Focus();
            }
            else
            {
                var control1 = tableLayoutPanel2.GetControlFromPosition(1, row);
                if (control1 is UltraCombo)
                    control1.Focus();
            }
        }
        #endregion

        #region tsm3 add
        private void tsmAdd3_Click(object sender, EventArgs e)
        {
            try
            {
                int row = GetRow(tableLayoutPanel3);
                if (row == 0) return;

                RowStyle temp = tableLayoutPanel3.RowStyles[tableLayoutPanel3.RowCount - 1];
                RowStyle rowStyle = new RowStyle(temp.SizeType, temp.Height);
                tableLayoutPanel3.RowStyles.Insert(GetRow(tableLayoutPanel3), rowStyle);
                tableLayoutPanel3.RowCount++;

                foreach (Control ExistControl in tableLayoutPanel3.Controls)
                {
                    if (tableLayoutPanel3.GetRow(ExistControl) > row)
                        tableLayoutPanel3.SetRow(ExistControl, tableLayoutPanel3.GetRow(ExistControl) + 1);
                }

                UltraTextEditor txtSTT = new UltraTextEditor();
                txtSTT.ReadOnly = true;
                txtSTT.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                txtSTT.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                txtSTT.AutoSize = false;
                txtSTT.Dock = DockStyle.Fill;
                txtSTT.Margin = new Padding(0);

                UltraTextEditor txtUnit = new UltraTextEditor();
                txtUnit.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                txtUnit.AutoSize = false;
                txtUnit.Dock = DockStyle.Fill;
                txtUnit.Margin = new Padding(0);

                UltraMaskedEdit txtQuantity = new UltraMaskedEdit();
                txtQuantity.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtQuantity.Value = 0;
                ConfigMaskedEdit1(txtQuantity);

                UltraMaskedEdit txtUnitPrice = new UltraMaskedEdit();
                txtUnitPrice.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtUnitPrice.Value = 0;
                ConfigMaskedEdit1(txtUnitPrice);

                UltraMaskedEdit txtTaxRate = new UltraMaskedEdit();
                txtTaxRate.ReadOnly = true;
                txtTaxRate.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtTaxRate.Value = 0;
                ConfigMaskedEdit1(txtTaxRate);

                UltraCombo cbbName = new UltraCombo();
                cbbName.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                cbbName.Dock = DockStyle.Fill;
                cbbName.AutoSize = false;
                cbbName.Margin = new Padding(0);
                ConfigCbb(cbbName);

                UltraMaskedEdit txtResourceTaxTaxAmountUnit = new UltraMaskedEdit();
                txtResourceTaxTaxAmountUnit.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtResourceTaxTaxAmountUnit.Value = 0;
                ConfigMaskedEdit(txtResourceTaxTaxAmountUnit);

                UltraMaskedEdit txtResourceTaxAmountIncurration = new UltraMaskedEdit();
                txtResourceTaxAmountIncurration.ReadOnly = true;
                txtResourceTaxAmountIncurration.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtResourceTaxAmountIncurration.Value = 0;
                ConfigMaskedEdit(txtResourceTaxAmountIncurration);

                UltraMaskedEdit txtResourceTaxAmountDeduction = new UltraMaskedEdit();
                txtResourceTaxAmountDeduction.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtResourceTaxAmountDeduction.Value = 0;
                ConfigMaskedEdit(txtResourceTaxAmountDeduction);

                UltraMaskedEdit txtResourceTaxAmount = new UltraMaskedEdit();
                txtResourceTaxAmount.ReadOnly = true;
                txtResourceTaxAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                txtResourceTaxAmount.Value = 0;
                ConfigMaskedEdit(txtResourceTaxAmount);

                txtSTT.ContextMenuStrip = contextMenuStrip3;
                txtUnit.ContextMenuStrip = contextMenuStrip3;
                txtQuantity.ContextMenuStrip = contextMenuStrip3;
                txtUnitPrice.ContextMenuStrip = contextMenuStrip3;
                txtTaxRate.ContextMenuStrip = contextMenuStrip3;
                cbbName.ContextMenuStrip = contextMenuStrip3;
                txtResourceTaxTaxAmountUnit.ContextMenuStrip = contextMenuStrip3;
                txtResourceTaxAmountIncurration.ContextMenuStrip = contextMenuStrip3;
                txtResourceTaxAmountDeduction.ContextMenuStrip = contextMenuStrip3;
                txtResourceTaxAmount.ContextMenuStrip = contextMenuStrip3;

                #region Format control
                Utils.FormatNumberic(txtQuantity, ConstDatabase.Format_Quantity);
                Utils.FormatNumberic(txtUnitPrice, ConstDatabase.Format_Quantity);
                Utils.FormatNumberic(txtTaxRate, ConstDatabase.Format_Quantity);
                Utils.FormatNumberic(txtResourceTaxTaxAmountUnit, ConstDatabase.Format_TienVND);
                Utils.FormatNumberic(txtResourceTaxAmountIncurration, ConstDatabase.Format_TienVND);
                Utils.FormatNumberic(txtResourceTaxAmountDeduction, ConstDatabase.Format_TienVND);
                Utils.FormatNumberic(txtResourceTaxAmount, ConstDatabase.Format_TienVND);
                #endregion

                #region add event control
                cbbName.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler((s, a) => cbb_RowSelected(s, a, txtUnit, txtTaxRate));
                cbbName.TextChanged += new System.EventHandler((s, a) => cbbName_TextChanged(s, a, txtUnit, txtTaxRate));
                txtQuantity.ValueChanged += new System.EventHandler((s, a) => txtQuantity_ValueChanged(s, a, txtQuantity, txtUnitPrice, txtTaxRate, txtResourceTaxTaxAmountUnit, txtResourceTaxAmountIncurration));
                txtUnitPrice.ValueChanged += new System.EventHandler((s, a) => txtUnitPrice_ValueChanged(s, a, txtQuantity, txtUnitPrice, txtTaxRate, txtResourceTaxTaxAmountUnit, txtResourceTaxAmountIncurration));
                txtTaxRate.ValueChanged += new System.EventHandler((s, a) => txtTaxRate_ValueChanged(s, a, txtQuantity, txtUnitPrice, txtTaxRate, txtResourceTaxAmountIncurration));
                txtResourceTaxTaxAmountUnit.ValueChanged += new System.EventHandler((s, a) => txtResourceTaxTaxAmountUnit_TextChanged(s, a, txtQuantity, txtResourceTaxTaxAmountUnit, txtResourceTaxAmountIncurration, txtUnitPrice, txtTaxRate));
                txtResourceTaxAmountIncurration.ValueChanged += new EventHandler((s, a) => txtResourceTaxAmountIncurration_TextChanged(s, a, txtResourceTaxAmount, txtResourceTaxAmountIncurration, txtResourceTaxAmountDeduction));
                txtResourceTaxAmountDeduction.ValueChanged += new EventHandler((s, a) => txtResourceTaxAmountDeduction_TextChanged(s, a, txtResourceTaxAmount, txtResourceTaxAmountIncurration, txtResourceTaxAmountDeduction));
                txtResourceTaxAmount.ValueChanged += new EventHandler(txtResourceTaxAmount_TextChanged);
                #endregion

                tableLayoutPanel3.Controls.Add(txtSTT, 0, row + 1);
                tableLayoutPanel3.Controls.Add(cbbName, 1, row + 1);
                tableLayoutPanel3.Controls.Add(txtUnit, 2, row + 1);
                tableLayoutPanel3.Controls.Add(txtQuantity, 3, row + 1);
                tableLayoutPanel3.Controls.Add(txtUnitPrice, 4, row + 1);
                tableLayoutPanel3.Controls.Add(txtTaxRate, 5, row + 1);
                tableLayoutPanel3.Controls.Add(txtResourceTaxTaxAmountUnit, 6, row + 1);
                tableLayoutPanel3.Controls.Add(txtResourceTaxAmountIncurration, 7, row + 1);
                tableLayoutPanel3.Controls.Add(txtResourceTaxAmountDeduction, 8, row + 1);
                tableLayoutPanel3.Controls.Add(txtResourceTaxAmount, 9, row + 1);

                tableLayoutPanel5.Location = new Point(tableLayoutPanel5.Location.X, tableLayoutPanel3.Location.Y + tableLayoutPanel3.Height);
                ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel5.Location.Y + tableLayoutPanel5.Height);

                for (int i = 0; i < tableLayoutPanel3.RowCount; i++)
                {
                    var control = tableLayoutPanel3.GetControlFromPosition(0, i);
                    if (control is UltraTextEditor)
                    {
                        UltraTextEditor ultraTextEditor = control as UltraTextEditor;
                        ultraTextEditor.Value = i;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region tsm3 delete
        private void tsmDelete3_Click(object sender, EventArgs e)
        {
            int r = tableLayoutPanel3.RowCount - 1;
            if (r == 1)
            {
                FillNull(tableLayoutPanel3);
                return;
            }
            int row = GetRow(tableLayoutPanel3);
            if (row == 0) return;
            remove_row(tableLayoutPanel3, row);
            tableLayoutPanel5.Location = new Point(tableLayoutPanel5.Location.X, tableLayoutPanel3.Location.Y + tableLayoutPanel3.Height);
            ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel5.Location.Y + tableLayoutPanel5.Height);

            for (int i = 0; i < tableLayoutPanel3.RowCount; i++)
            {
                var control = tableLayoutPanel3.GetControlFromPosition(0, i);
                if (control is UltraTextEditor)
                {
                    UltraTextEditor ultraTextEditor = control as UltraTextEditor;
                    ultraTextEditor.Value = i;
                }
            }
            if (row == tableLayoutPanel3.RowCount)
            {
                var control1 = tableLayoutPanel3.GetControlFromPosition(1, row - 1);
                if (control1 is UltraCombo)
                    control1.Focus();
            }
            else
            {
                var control1 = tableLayoutPanel3.GetControlFromPosition(1, row);
                if (control1 is UltraCombo)
                    control1.Focus();
            }
        }

        #endregion

        #region event tính tổng cột 8
        private void txtResourceTaxAmountIncurration1_TextChanged(object sender, EventArgs e)
        {
            double sum1 = 0;
            double sum2 = 0;
            double sum3 = 0;
            for (int t = 1; t < tableLayoutPanel1.RowCount; t++)
            {
                var control = tableLayoutPanel1.GetControlFromPosition(7, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum1 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel2.RowCount; t++)
            {
                var control = tableLayoutPanel2.GetControlFromPosition(7, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum2 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel3.RowCount; t++)
            {
                var control = tableLayoutPanel3.GetControlFromPosition(7, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum3 += double.Parse(ultraTextEditor.Text);
                }
            }
            ultraLabel79.FormatNumberic((sum1 + sum2 + sum3), ConstDatabase.Format_TienVND);
            if (txtResourceTaxAmountIncurration1.Value.IsNullOrEmpty())
            {
                txtResourceTaxAmountIncurration1.Value = 0;
            }
            if (txtResourceTaxAmountDeduction1.Value.IsNullOrEmpty())
            {
                txtResourceTaxAmountDeduction1.Value = 0;
            }
            txtResourceTaxAmount1.FormatNumberic((double.Parse(txtResourceTaxAmountIncurration1.Value.ToString()) - double.Parse(txtResourceTaxAmountDeduction1.Value.ToString())), ConstDatabase.Format_TienVND);
        }

        private void txtResourceTaxAmountIncurration2_TextChanged(object sender, EventArgs e)
        {
            double sum1 = 0;
            double sum2 = 0;
            double sum3 = 0;
            for (int t = 1; t < tableLayoutPanel1.RowCount; t++)
            {
                var control = tableLayoutPanel1.GetControlFromPosition(7, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum1 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel2.RowCount; t++)
            {
                var control = tableLayoutPanel2.GetControlFromPosition(7, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum2 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel3.RowCount; t++)
            {
                var control = tableLayoutPanel3.GetControlFromPosition(7, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum3 += double.Parse(ultraTextEditor.Text);
                }
            }
            ultraLabel79.FormatNumberic((sum1 + sum2 + sum3), ConstDatabase.Format_TienVND);
            if (txtResourceTaxAmountIncurration2.Value.IsNullOrEmpty())
            {
                txtResourceTaxAmountIncurration2.Value = 0;
            }
            if (txtResourceTaxAmountDeduction2.Value.IsNullOrEmpty())
            {
                txtResourceTaxAmountDeduction2.Value = 0;
            }
            txtResourceTaxAmount2.FormatNumberic((double.Parse(txtResourceTaxAmountIncurration2.Value.ToString()) - double.Parse(txtResourceTaxAmountDeduction2.Value.ToString())), ConstDatabase.Format_TienVND);
        }

        private void txtResourceTaxAmountIncurration3_TextChanged(object sender, EventArgs e)
        {
            double sum1 = 0;
            double sum2 = 0;
            double sum3 = 0;
            for (int t = 1; t < tableLayoutPanel1.RowCount; t++)
            {
                var control = tableLayoutPanel1.GetControlFromPosition(7, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum1 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel2.RowCount; t++)
            {
                var control = tableLayoutPanel2.GetControlFromPosition(7, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum2 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel3.RowCount; t++)
            {
                var control = tableLayoutPanel3.GetControlFromPosition(7, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum3 += double.Parse(ultraTextEditor.Text);
                }
            }
            ultraLabel79.FormatNumberic((sum1 + sum2 + sum3), ConstDatabase.Format_TienVND);
            if (txtResourceTaxAmountIncurration3.Value.IsNullOrEmpty())
            {
                txtResourceTaxAmountIncurration3.Value = 0;
            }
            if (txtResourceTaxAmountDeduction3.Value.IsNullOrEmpty())
            {
                txtResourceTaxAmountDeduction3.Value = 0;
            }
            txtResourceTaxAmount3.FormatNumberic((double.Parse(txtResourceTaxAmountIncurration3.Text) - double.Parse(txtResourceTaxAmountDeduction3.Text)), ConstDatabase.Format_TienVND);
        }

        private void txtResourceTaxAmountIncurration_TextChanged(object sender, EventArgs e, UltraMaskedEdit txtResourceTaxAmount, UltraMaskedEdit txtResourceTaxAmountIncurration, UltraMaskedEdit txtResourceTaxAmountDeduction)
        {
            double sum1 = 0;
            double sum2 = 0;
            double sum3 = 0;
            for (int t = 1; t < tableLayoutPanel1.RowCount; t++)
            {
                var control = tableLayoutPanel1.GetControlFromPosition(7, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum1 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel2.RowCount; t++)
            {
                var control = tableLayoutPanel2.GetControlFromPosition(7, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum2 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel3.RowCount; t++)
            {
                var control = tableLayoutPanel3.GetControlFromPosition(7, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum3 += double.Parse(ultraTextEditor.Text);
                }
            }
            ultraLabel79.FormatNumberic((sum1 + sum2 + sum3), ConstDatabase.Format_TienVND);
            if (txtResourceTaxAmountIncurration.Value.IsNullOrEmpty())
            {
                txtResourceTaxAmountIncurration.Value = 0;
            }
            if (txtResourceTaxAmountDeduction.Value.IsNullOrEmpty())
            {
                txtResourceTaxAmountDeduction.Value = 0;
            }
            txtResourceTaxAmount.FormatNumberic((double.Parse(txtResourceTaxAmountIncurration.Value.ToString()) - double.Parse(txtResourceTaxAmountDeduction.Value.ToString())), ConstDatabase.Format_TienVND);
        }
        #endregion

        #region event tính tổng cột 9
        private void txtResourceTaxAmountDeduction1_TextChanged(object sender, EventArgs e)
        {
            double sum1 = 0;
            double sum2 = 0;
            double sum3 = 0;
            for (int t = 1; t < tableLayoutPanel1.RowCount; t++)
            {
                var control = tableLayoutPanel1.GetControlFromPosition(8, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum1 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel2.RowCount; t++)
            {
                var control = tableLayoutPanel2.GetControlFromPosition(8, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum2 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel3.RowCount; t++)
            {
                var control = tableLayoutPanel3.GetControlFromPosition(8, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum3 += double.Parse(ultraTextEditor.Text);
                }
            }
            ultraLabel78.FormatNumberic((sum1 + sum2 + sum3), ConstDatabase.Format_TienVND);
            if (txtResourceTaxAmountIncurration1.Value.IsNullOrEmpty())
            {
                txtResourceTaxAmountIncurration1.Value = 0;
            }
            if (txtResourceTaxAmountDeduction1.Value.IsNullOrEmpty())
            {
                txtResourceTaxAmountDeduction1.Value = 0;
            }
            txtResourceTaxAmount1.FormatNumberic((double.Parse(txtResourceTaxAmountIncurration1.Value.ToString()) - double.Parse(txtResourceTaxAmountDeduction1.Value.ToString())), ConstDatabase.Format_TienVND);
        }

        private void txtResourceTaxAmountDeduction2_TextChanged(object sender, EventArgs e)
        {
            double sum1 = 0;
            double sum2 = 0;
            double sum3 = 0;
            for (int t = 1; t < tableLayoutPanel1.RowCount; t++)
            {
                var control = tableLayoutPanel1.GetControlFromPosition(8, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum1 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel2.RowCount; t++)
            {
                var control = tableLayoutPanel2.GetControlFromPosition(8, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum2 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel3.RowCount; t++)
            {
                var control = tableLayoutPanel3.GetControlFromPosition(8, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum3 += double.Parse(ultraTextEditor.Text);
                }
            }
            ultraLabel78.FormatNumberic((sum1 + sum2 + sum3), ConstDatabase.Format_TienVND);
            if (txtResourceTaxAmountIncurration2.Value.IsNullOrEmpty())
            {
                txtResourceTaxAmountIncurration2.Value = 0;
            }
            if (txtResourceTaxAmountDeduction2.Value.IsNullOrEmpty())
            {
                txtResourceTaxAmountDeduction2.Value = 0;
            }
            txtResourceTaxAmount2.FormatNumberic((double.Parse(txtResourceTaxAmountIncurration2.Value.ToString()) - double.Parse(txtResourceTaxAmountDeduction2.Value.ToString())), ConstDatabase.Format_TienVND);
        }

        private void txtResourceTaxAmountDeduction3_TextChanged(object sender, EventArgs e)
        {
            double sum1 = 0;
            double sum2 = 0;
            double sum3 = 0;
            for (int t = 1; t < tableLayoutPanel1.RowCount; t++)
            {
                var control = tableLayoutPanel1.GetControlFromPosition(8, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum1 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel2.RowCount; t++)
            {
                var control = tableLayoutPanel2.GetControlFromPosition(8, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum2 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel3.RowCount; t++)
            {
                var control = tableLayoutPanel3.GetControlFromPosition(8, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum3 += double.Parse(ultraTextEditor.Text);
                }
            }
            ultraLabel78.FormatNumberic((sum1 + sum2 + sum3), ConstDatabase.Format_TienVND);
            if (txtResourceTaxAmountIncurration3.Value.IsNullOrEmpty())
            {
                txtResourceTaxAmountIncurration3.Value = 0;
            }
            if (txtResourceTaxAmountDeduction3.Value.IsNullOrEmpty())
            {
                txtResourceTaxAmountDeduction3.Value = 0;
            }
            txtResourceTaxAmount3.FormatNumberic((double.Parse(txtResourceTaxAmountIncurration3.Value.ToString()) - double.Parse(txtResourceTaxAmountDeduction3.Value.ToString())), ConstDatabase.Format_TienVND);
        }

        private void txtResourceTaxAmountDeduction_TextChanged(object sender, EventArgs e, UltraMaskedEdit txtResourceTaxAmount, UltraMaskedEdit txtResourceTaxAmountIncurration, UltraMaskedEdit txtResourceTaxAmountDeduction)
        {
            double sum1 = 0;
            double sum2 = 0;
            double sum3 = 0;
            for (int t = 1; t < tableLayoutPanel1.RowCount; t++)
            {
                var control = tableLayoutPanel1.GetControlFromPosition(8, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum1 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel2.RowCount; t++)
            {
                var control = tableLayoutPanel2.GetControlFromPosition(8, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum2 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel3.RowCount; t++)
            {
                var control = tableLayoutPanel3.GetControlFromPosition(8, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum3 += double.Parse(ultraTextEditor.Text);
                }
            }
            ultraLabel78.FormatNumberic((sum1 + sum2 + sum3), ConstDatabase.Format_TienVND);
            if (txtResourceTaxAmountIncurration.Value.IsNullOrEmpty())
            {
                txtResourceTaxAmountIncurration.Value = 0;
            }
            if (txtResourceTaxAmountDeduction.Value.IsNullOrEmpty())
            {
                txtResourceTaxAmountDeduction.Value = 0;
            }
            txtResourceTaxAmount.FormatNumberic((double.Parse(txtResourceTaxAmountIncurration.Value.ToString()) - double.Parse(txtResourceTaxAmountDeduction.Value.ToString())), ConstDatabase.Format_TienVND);
        }
        #endregion

        #region event tính tổng cột 10
        private void txtResourceTaxAmount1_TextChanged(object sender, EventArgs e)
        {
            double sum1 = 0;
            double sum2 = 0;
            double sum3 = 0;
            for (int t = 1; t < tableLayoutPanel1.RowCount; t++)
            {
                var control = tableLayoutPanel1.GetControlFromPosition(9, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum1 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel2.RowCount; t++)
            {
                var control = tableLayoutPanel2.GetControlFromPosition(9, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum2 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel3.RowCount; t++)
            {
                var control = tableLayoutPanel3.GetControlFromPosition(9, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum3 += double.Parse(ultraTextEditor.Text);
                }
            }
            ultraLabel77.FormatNumberic((sum1 + sum2 + sum3), ConstDatabase.Format_TienVND);
        }

        private void txtResourceTaxAmount2_TextChanged(object sender, EventArgs e)
        {
            double sum1 = 0;
            double sum2 = 0;
            double sum3 = 0;
            for (int t = 1; t < tableLayoutPanel1.RowCount; t++)
            {
                var control = tableLayoutPanel1.GetControlFromPosition(9, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum1 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel2.RowCount; t++)
            {
                var control = tableLayoutPanel2.GetControlFromPosition(9, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum2 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel3.RowCount; t++)
            {
                var control = tableLayoutPanel3.GetControlFromPosition(9, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum3 += double.Parse(ultraTextEditor.Text);
                }
            }
            ultraLabel77.FormatNumberic((sum1 + sum2 + sum3), ConstDatabase.Format_TienVND);
        }

        private void txtResourceTaxAmount3_TextChanged(object sender, EventArgs e)
        {
            double sum1 = 0;
            double sum2 = 0;
            double sum3 = 0;
            for (int t = 1; t < tableLayoutPanel1.RowCount; t++)
            {
                var control = tableLayoutPanel1.GetControlFromPosition(9, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum1 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel2.RowCount; t++)
            {
                var control = tableLayoutPanel2.GetControlFromPosition(9, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum2 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel3.RowCount; t++)
            {
                var control = tableLayoutPanel3.GetControlFromPosition(9, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum3 += double.Parse(ultraTextEditor.Text);
                }
            }
            ultraLabel77.FormatNumberic((sum1 + sum2 + sum3), ConstDatabase.Format_TienVND);
        }

        private void txtResourceTaxAmount_TextChanged(object sender, EventArgs e)
        {
            double sum1 = 0;
            double sum2 = 0;
            double sum3 = 0;
            for (int t = 1; t < tableLayoutPanel1.RowCount; t++)
            {
                var control = tableLayoutPanel1.GetControlFromPosition(9, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum1 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel2.RowCount; t++)
            {
                var control = tableLayoutPanel2.GetControlFromPosition(9, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum2 += double.Parse(ultraTextEditor.Text);
                }
            }
            for (int t = 1; t < tableLayoutPanel3.RowCount; t++)
            {
                var control = tableLayoutPanel3.GetControlFromPosition(9, t);
                if (control is UltraMaskedEdit)
                {
                    UltraMaskedEdit ultraTextEditor = control as UltraMaskedEdit;
                    Utils.FormatNumberic(ultraTextEditor, ConstDatabase.Format_TienVND);
                    if (ultraTextEditor.Text != "")
                        sum3 += double.Parse(ultraTextEditor.Text);
                }
            }
            ultraLabel77.FormatNumberic((sum1 + sum2 + sum3), ConstDatabase.Format_TienVND);
        }
        #endregion

        #region check lỗi
        public bool checktable1()
        {

            for (int i = 1; i < tableLayoutPanel1.RowCount; i++)
            {
                var control1 = tableLayoutPanel1.GetControlFromPosition(1, i);

                if (control1.Text == "")
                {

                    return false;
                    tableLayoutPanel1.Focus();
                }
            }
            return true;
        }
        public bool checktable2()
        {
            for (int i = 0; i < tableLayoutPanel2.RowCount; i++)
            {
                var control1 = tableLayoutPanel2.GetControlFromPosition(1, i);

                if (control1.Text == "")
                {

                    return false;
                    tableLayoutPanel2.Focus();
                }
            }
            return true;
        }
        public bool checktable3()
        {
            for (int i = 0; i < tableLayoutPanel3.RowCount; i++)
            {
                var control1 = tableLayoutPanel3.GetControlFromPosition(1, i);

                if (control1.Text == "")
                {

                    return false;
                    tableLayoutPanel3.Focus();
                }
            }
            return true;
        }
        public bool CheckError()
        {

            if (checktable1() || checktable2() || checktable3())
            {
                #region check lỗi sai tên tài nguyên
                for (int d = 1; d < tableLayoutPanel1.RowCount; d++)
                {
                    var control100 = tableLayoutPanel1.GetControlFromPosition(1, d);
                    UltraCombo uCombo100 = control100 as UltraCombo;
                    if (uCombo100.Text != "")
                    {
                        try
                        {
                            string t7 = uCombo100.Text.ToString();
                            lstMaterialGoodsResourceTaxGroup1 = (Utils.ListMaterialGoodsResourceTaxGroup.Where(n => n.MaterialGoodsResourceTaxGroupName == t7)).ToList();
                            if (lstMaterialGoodsResourceTaxGroup1.Count == 0)
                            {
                                MSG.Error("Sai tên tài nguyên");
                                uCombo100.Focus();
                                return false;
                            };


                        }

                        catch { }
                    }
                }
                for (int d = 1; d < tableLayoutPanel2.RowCount; d++)
                {
                    var control100 = tableLayoutPanel2.GetControlFromPosition(1, d);
                    UltraCombo uCombo100 = control100 as UltraCombo;
                    if (uCombo100.Text != "")
                    {
                        try
                        {
                            string t7 = uCombo100.Text.ToString();
                            lstMaterialGoodsResourceTaxGroup1 = (Utils.ListMaterialGoodsResourceTaxGroup.Where(n => n.MaterialGoodsResourceTaxGroupName == t7)).ToList();
                            if (lstMaterialGoodsResourceTaxGroup1.Count == 0)
                            {
                                MSG.Error("Sai tên tài nguyên");
                                uCombo100.Focus();
                                return false;
                            };


                        }

                        catch { }
                    }
                }
                for (int d = 1; d < tableLayoutPanel3.RowCount; d++)
                {
                    var control100 = tableLayoutPanel3.GetControlFromPosition(1, d);
                    UltraCombo uCombo100 = control100 as UltraCombo;
                    if (uCombo100.Text != "")
                    {
                        try
                        {
                            string t7 = uCombo100.Text.ToString();
                            lstMaterialGoodsResourceTaxGroup1 = (Utils.ListMaterialGoodsResourceTaxGroup.Where(n => n.MaterialGoodsResourceTaxGroupName == t7)).ToList();
                            if (lstMaterialGoodsResourceTaxGroup1.Count == 0)
                            {
                                MSG.Error("Sai tên tài nguyên");
                                uCombo100.Focus();
                                return false;
                            };


                        }

                        catch { }
                    }
                }
            }
            else
            {
                MSG.Error("Chưa chọn loại tài nguyên");
                return false;
            }

            #endregion
            return true;
        }


        #endregion
        #region button top
        private void utmDetailBaseToolBar_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
        {
            switch (e.Tool.Key)
            {
                case "mnbtnBack":
                    break;

                case "mnbtnForward":
                    break;

                case "mnbtnAdd":

                    break;

                case "mnbtnEdit":
                    utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Enabled = false;
                    utmDetailBaseToolBar.Tools["mnbtnSave"].SharedProps.Enabled = true;
                    utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = false;
                    utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = false;
                    utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Enabled = true;
                    LoadReadOnly(false);
                    _isEdit = true;
                    break;

                case "mnbtnSave":
                    try
                    {
                        if (!CheckError()) return;
                        if (txtSignDate.Value == null)
                        {
                            MSG.Error("Bạn chưa chọn ngày ký");
                            return;
                        }
                        Utils.ITM01TAINService.BeginTran();
                        if (_isEdit)
                        {
                            TM01TAIN input = new TM01TAIN();
                            input = ObjandGUI(_select, true);
                            Utils.ITM01TAINService.Update(input);

                        }
                        else
                        {
                            TM01TAIN input = new TM01TAIN();
                            input = ObjandGUI(input, true);
                            Utils.ITM01TAINService.CreateNew(input);
                            _select = input;
                        }
                        Utils.ITM01TAINService.CommitTran();
                        MSG.Information("Lưu thành công");
                        Utils.ClearCacheByType<TM01TAIN>();
                        Utils.ClearCacheByType<TM01TAINDetail>();
                        _select = Utils.ITM01TAINService.Getbykey(_select.ID);
                        utmDetailBaseToolBar.Tools["mnbtnSave"].SharedProps.Enabled = false;
                        utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = true;
                        utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = true;
                        utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Enabled = true;
                        LoadReadOnly(true);
                        checkSave = true;
                    }
                    catch (Exception ex)
                    {
                        MSG.Error(ex.Message);
                        Utils.ITM01TAINService.RolbackTran();
                    }

                    break;

                case "mnbtnDelete":
                    if (!(DialogResult.Yes == MessageBox.Show("Bạn có muốn xóa tờ khai này không ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question)))
                        return;
                    TM01TAIN tM01TAIN = Utils.ListTM01TAIN.FirstOrDefault(n => n.ID == _select.ID);
                    Utils.ITM01TAINService.BeginTran();
                    Utils.ITM01TAINService.Delete(tM01TAIN);
                    Utils.ITM01TAINService.CommitTran();
                    MSG.Information("Xóa thành công");
                    this.Close();
                    break;

                case "mnbtnComeBack":
                    break;

                case "mnbtnPrint":
                    break;
                case "mnbtnClose":
                    this.Close();
                    break;
                case "PrintTAIN01":

                    TM01TAINDetail item = new TM01TAINDetail();
                    item.ID = Guid.Empty;
                    item.TM01TAINID = _select.ID;
                    //item.Type = 1;
                    item.MaterialGoodsResourceTaxGroupID = Guid.Empty;
                    item.MaterialGoodsResourceTaxGroupName = "";
                    item.Unit = "";
                    item.Quantity = 0;
                    item.UnitPrice = 0;
                    item.TaxRate = 0;
                    item.ResourceTaxTaxAmountUnit = 0;
                    item.ResourceTaxAmount = 0;
                    item.ResourceTaxAmountDeduction = 0;
                    item.ResourceTaxAmountIncurration = 0;




                    _select.TM01TAINDetails1 = _select.TM01TAINDetails.Where(n => n.Type == 1).OrderBy(n => n.OrderPriority).ToList();
                    _select.TM01TAINDetails2 = _select.TM01TAINDetails.Where(n => n.Type == 2).OrderBy(n => n.OrderPriority).ToList();
                    _select.TM01TAINDetails3 = _select.TM01TAINDetails.Where(n => n.Type == 3).OrderBy(n => n.OrderPriority).ToList();


                    if (_select.TM01TAINDetails3.Count == 0)
                    {
                        item.Type = 3;
                        _select.TM01TAINDetails3.Add(item);
                    }
                    if (_select.TM01TAINDetails2.Count == 0)
                    {
                        item.Type = 2;
                        _select.TM01TAINDetails2.Add(item);
                    }
                    if (_select.TM01TAINDetails1.Count == 0)
                    {
                        item.Type = 3;
                        _select.TM01TAINDetails1.Add(item);
                    }


                    Utils.Print("TAIN-01", _select, this);
                    break;
                case "btnExportXml":
                    ExportXml(_select);
                    break;

            }

        }
        #endregion
        private void txtItem_Validated(object sender, EventArgs e)
        {
            UltraMaskedEdit txt = (UltraMaskedEdit)sender;
            txt.Text = txt.Text.TrimStart('0');
        }
        public int GetRow(TableLayoutPanel tableLayoutPanel)
        {
            for (int t = 1; t < tableLayoutPanel.RowCount; t++)
            {
                for (int i = 0; i < tableLayoutPanel.ColumnCount; i++)
                {
                    var control = tableLayoutPanel.GetControlFromPosition(i, t);
                    if (control != null)
                        if (control.ContainsFocus)
                        {
                            return t;

                        }
                }
            }
            return 0;
        }
        private bool ExportXml(TM01TAIN tM01TAIN)
        {
            SaveFileDialog sf = new SaveFileDialog
            {
                FileName = "TM01TAIN.xml",
                AddExtension = true,
                Filter = "Excel Document(*.xml)|*.xml"
            };
            if (sf.ShowDialog() != DialogResult.OK)
            {
                return true;
            }
            string path = System.IO.Path.GetDirectoryName(
                        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            string filePath = string.Format("{0}\\TemplateResource\\xml\\01_TAIN_xml.xml", path);
            XmlDocument doc = new XmlDocument();
            doc.Load(filePath);

            #region TTChung
            string tt = "HSoThueDTu/HSoKhaiThue/TTinChung/";
            //doc.SelectSingleNode(tt + "TTinDVu/maDVu").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinDVu/tenDVu").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinDVu/pbanDVu").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinDVu/ttinNhaCCapDVu").InnerText = "";

            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/maTKhai").InnerText = "06";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/tenTKhai").InnerText = tM01TAIN.DeclarationName;
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/moTaBMau").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/pbanTKhaiXML").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/loaiTKhai").InnerText = "C";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/soLan").InnerText = "0";

            if (tM01TAIN.isMonth)
            {
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kieuKy").InnerText = "M";
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = tM01TAIN.DeclarationTerm.Split(' ')[1] + "/" + tM01TAIN.DeclarationTerm.Split(' ')[3];
            }
            else
            {
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kieuKy").InnerText = "D";
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = tM01TAIN.DeclarationTerm.Split(' ')[1] + "/" + tM01TAIN.DeclarationTerm.Split(' ')[3] + "/" + tM01TAIN.DeclarationTerm.Split(' ')[5];
            }
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhaiTuNgay").InnerText = tM01TAIN.FromDate.ToString("dd/MM/yyyy");
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhaiDenNgay").InnerText = tM01TAIN.ToDate.ToString("dd/MM/yyyy");

            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/maCQTNoiNop").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/tenCQTNoiNop").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/ngayLapTKhai").InnerText = "";

            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/GiaHan/maLyDoGiaHan").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/GiaHan/lyDoGiaHan").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/GiaHan/lyDoGiaHan").InnerText = "";

            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/nguoiKy").InnerText = tM01TAIN.SignName;
            if (tM01TAIN.SignDate != null)
            {
                DateTime dt = (DateTime)tM01TAIN.SignDate;
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/ngayKy").InnerText = dt.ToString("yyyy-MM-dd");
            }
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/nganhNgheKD").InnerText = tM01TAIN.CertificationNo;

            if (!Utils.GetDLTNgay().IsNullOrEmpty())
            {
                DateTime ngdlt = DateTime.ParseExact(Utils.GetDLTNgay(), "dd/MM/yyyy", null);
                doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/ngayKyHDDLyThue").InnerText = ngdlt.ToString("yyyy-MM-dd");
            }
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/mst").InnerText = Utils.GetCompanyTaxCodeForInvoice();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/tenNNT").InnerText = tM01GTGT.CompanyName;
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/dchiNNT").InnerText = Utils.GetCompanyAddress();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/phuongXa").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/maHuyenNNT").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/tenHuyenNNT").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/maTinhNNT").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/tenTinhNNT").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/dthoaiNNT").InnerText = Utils.GetCompanyPhoneNumber();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/faxNNT").InnerText = tM01GTGT.CompanyTaxCode;
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/emailNNT").InnerText = Utils.GetCompanyEmail();

            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/mstDLyThue").InnerText = Utils.GetMSTDLT();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/tenDLyThue").InnerText = tM01GTGT.TaxAgencyName;
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/dchiDLyThue").InnerText = Utils.GetDLTAddress();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/maHuyenDLyThue").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/tenHuyenDLyThue").InnerText = Utils.GetDLTDistrict();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/maTinhDLyThue").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/tenTinhDLyThue").InnerText = Utils.GetDLTCity();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/dthoaiDLyThue").InnerText = Utils.GetDLTPhoneNumber();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/faxDLyThue").InnerText = Utils.GetDLTFax();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/emailDLyThue").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/soHDongDLyThue").InnerText = Utils.GetDLTSoHD();

            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/NVienDLy/tenNVienDLyThue").InnerText = Utils.GetHVTNhanVienDLT();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/NVienDLy/cchiHNghe").InnerText = Utils.GetCCHNDLT();
            if (!Utils.GetTTDLT())
            {
                doc.SelectSingleNode(tt + "TTinTKhaiThue").RemoveChild(doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue"));
            }
            #endregion
            doc.DocumentElement.SetAttribute("xmlns", "http://kekhaithue.gdt.gov.vn/TKhaiThue");
            doc.DocumentElement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            tt = "HSoThueDTu/HSoKhaiThue/CTieuTKhaiChinh/BangChiTietTNguyen/";
            List<TM01TAINDetail> lst1 = tM01TAIN.TM01TAINDetails.Where(n => n.Type == 1).ToList().OrderBy(n => n.OrderPriority).ToList();
            List<TM01TAINDetail> lst2 = tM01TAIN.TM01TAINDetails.Where(n => n.Type == 2).ToList().OrderBy(n => n.OrderPriority).ToList();
            List<TM01TAINDetail> lst3 = tM01TAIN.TM01TAINDetails.Where(n => n.Type == 3).ToList().OrderBy(n => n.OrderPriority).ToList();
            if (lst1.Count == 0)
            {
                //doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac").Attributes["id"].Value = "ID_" + (i + 1).ToString();
                // Hautv comment
                /*doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct1").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct2").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct3").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct4").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct5").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct6").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct7").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct8").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct9").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct10").InnerText = "";*/
            }
            if (lst2.Count == 0)
            {
                //doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom").Attributes["id"].Value = "ID_" + (i + 1).ToString();
                /*doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct1").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct2").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct3").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct4").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct5").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct6").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct7").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct8").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct9").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct10").InnerText = "";*/
            }
            if (lst3.Count == 0)
            {
                //doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan").Attributes["id"].Value = "ID_" + (i + 1).ToString();
                /*doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct1").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct2").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct3").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct4").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct5").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct6").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct7").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct8").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct9").InnerText = "";
                doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct10").InnerText = "";*/
            }
            for (int i = 0; i < lst1.Count; i++)
            {
                if (i == 0)
                {
                    doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac").Attributes["id"].Value = "ID_" + (i + 1).ToString();
                    doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct1").InnerText = (i + 1).ToString();
                    doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct2").InnerText = lst1[i].MaterialGoodsResourceTaxGroupName;
                    doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct3").InnerText = lst1[i].Unit;
                    doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct4").InnerText = lst1[i].Quantity.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct5").InnerText = lst1[i].UnitPrice.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct6").InnerText = lst1[i].TaxRate.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct7").InnerText = lst1[i].ResourceTaxTaxAmountUnit.ToString("0");
                    doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct8").InnerText = lst1[i].ResourceTaxAmountIncurration.ToString("0");
                    doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct9").InnerText = lst1[i].ResourceTaxAmountDeduction.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac/ct10").InnerText = lst1[i].ResourceTaxAmount.ToString("0");
                }
                else
                {
                    XmlNode xmlNode = doc.SelectSingleNode(tt + "TaiNguyenKhaiThac/ThueTaiNguyenKhaiThac").CloneNode(true);
                    xmlNode.Attributes["id"].Value = "ID_" + (i + 1).ToString();
                    xmlNode.SelectSingleNode("ct1").InnerText = (i + 1).ToString();
                    xmlNode.SelectSingleNode("ct2").InnerText = lst1[i].MaterialGoodsResourceTaxGroupName;
                    xmlNode.SelectSingleNode("ct3").InnerText = lst1[i].Unit;
                    xmlNode.SelectSingleNode("ct4").InnerText = lst1[i].Quantity.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct5").InnerText = lst1[i].UnitPrice.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct6").InnerText = lst1[i].TaxRate.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct7").InnerText = lst1[i].ResourceTaxTaxAmountUnit.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct8").InnerText = lst1[i].ResourceTaxAmountIncurration.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct9").InnerText = lst1[i].ResourceTaxAmountDeduction.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct10").InnerText = lst1[i].ResourceTaxAmount.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TaiNguyenKhaiThac").AppendChild(xmlNode);
                }
            }
            for (int i = 0; i < lst2.ToList().Count; i++)
            {
                if (i == 0)
                {
                    doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom").Attributes["id"].Value = "ID_" + (i + 1).ToString();
                    doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct1").InnerText = (i + 1).ToString();
                    doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct2").InnerText = lst2[i].MaterialGoodsResourceTaxGroupName;
                    doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct3").InnerText = lst2[i].Unit;
                    doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct4").InnerText = lst2[i].Quantity.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct5").InnerText = lst2[i].UnitPrice.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct6").InnerText = lst2[i].TaxRate.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct7").InnerText = lst2[i].ResourceTaxTaxAmountUnit.ToString("0");
                    doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct8").InnerText = lst2[i].ResourceTaxAmountIncurration.ToString("0");
                    doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct9").InnerText = lst2[i].ResourceTaxAmountDeduction.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom/ct10").InnerText = lst2[i].ResourceTaxAmount.ToString("0");
                }
                else
                {
                    XmlNode xmlNode = doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom/ThueTaiNguyenThuMuaGom").CloneNode(true);
                    xmlNode.Attributes["id"].Value = "ID_" + (i + 1).ToString();
                    xmlNode.SelectSingleNode("ct1").InnerText = (i + 1).ToString();
                    xmlNode.SelectSingleNode("ct2").InnerText = lst2[i].MaterialGoodsResourceTaxGroupName;
                    xmlNode.SelectSingleNode("ct3").InnerText = lst2[i].Unit;
                    xmlNode.SelectSingleNode("ct4").InnerText = lst2[i].Quantity.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct5").InnerText = lst2[i].UnitPrice.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct6").InnerText = lst2[i].TaxRate.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct7").InnerText = lst2[i].ResourceTaxTaxAmountUnit.ToString("0");
                    xmlNode.SelectSingleNode("ct8").InnerText = lst2[i].ResourceTaxAmountIncurration.ToString("0");
                    xmlNode.SelectSingleNode("ct9").InnerText = lst2[i].ResourceTaxAmountDeduction.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct10").InnerText = lst2[i].ResourceTaxAmount.ToString("0");
                    doc.SelectSingleNode(tt + "TaiNguyenThuMuaGom").AppendChild(xmlNode);
                }
            }
            for (int i = 0; i < lst3.ToList().Count; i++)
            {
                if (i == 0)
                {
                    doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan").Attributes["id"].Value = "ID_" + (i + 1).ToString();
                    doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct1").InnerText = (i + 1).ToString();
                    doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct2").InnerText = lst3[i].MaterialGoodsResourceTaxGroupName;
                    doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct3").InnerText = lst3[i].Unit;
                    doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct4").InnerText = lst3[i].Quantity.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct5").InnerText = lst3[i].UnitPrice.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct6").InnerText = lst3[i].TaxRate.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct7").InnerText = lst3[i].ResourceTaxTaxAmountUnit.ToString("0");
                    doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct8").InnerText = lst3[i].ResourceTaxAmountIncurration.ToString("0");
                    doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct9").InnerText = lst3[i].ResourceTaxAmountDeduction.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan/ct10").InnerText = lst3[i].ResourceTaxAmount.ToString("0");
                }
                else
                {
                    XmlNode xmlNode = doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan/ThueTaiNguyenTichThuGiaoBan").CloneNode(true);
                    xmlNode.Attributes["id"].Value = "ID_" + (i + 1).ToString();
                    xmlNode.SelectSingleNode("ct1").InnerText = (i + 1).ToString();
                    xmlNode.SelectSingleNode("ct2").InnerText = lst3[i].MaterialGoodsResourceTaxGroupName;
                    xmlNode.SelectSingleNode("ct3").InnerText = lst3[i].Unit;
                    xmlNode.SelectSingleNode("ct4").InnerText = lst3[i].Quantity.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct5").InnerText = lst3[i].UnitPrice.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct6").InnerText = lst3[i].TaxRate.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct7").InnerText = lst3[i].ResourceTaxTaxAmountUnit.ToString("0");
                    xmlNode.SelectSingleNode("ct8").InnerText = lst3[i].ResourceTaxAmountIncurration.ToString("0");
                    xmlNode.SelectSingleNode("ct9").InnerText = lst3[i].ResourceTaxAmountDeduction.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct10").InnerText = lst3[i].ResourceTaxAmount.ToString("0");
                    doc.SelectSingleNode(tt + "TaiNguyenTichThuGiaoBan").AppendChild(xmlNode);
                }
            }
            doc.SelectSingleNode(tt + "tongThueTNPSinh").InnerText = ultraLabel79.Text.ToString().Replace(",", "");
            doc.SelectSingleNode(tt + "tongThueMienGiam").InnerText = ultraLabel78.Text.ToString().Replace(",", "");
            doc.SelectSingleNode(tt + "tongThuePNop").InnerText = ultraLabel77.Text.ToString().Replace(",", "");

            doc.Save(sf.FileName);
            if (MSG.Question("Xuất xml thành công, Bạn có muốn mở file vừa tải") == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    System.Diagnostics.Process.Start(sf.FileName);
                }
                catch
                {
                    MSG.Error("Lỗi mở file");
                }

            }

            return true;
        }

        private void _01_TAIN_Detail_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (Form)sender;
            frm.Dispose();

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}
