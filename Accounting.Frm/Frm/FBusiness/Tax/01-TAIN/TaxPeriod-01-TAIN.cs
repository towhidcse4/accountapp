﻿using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting
{
    public partial class TaxPeriod_01_TAIN : Form
    {
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        public Tax Tax;
        public TaxPeriod_01_TAIN()
        {
            InitializeComponent();
            txtYear.Value = DateTime.Now.Year;
            txtDay.Value = DateTime.Now.Day;
            ultraLabel1.Location = new Point(12, 51);
            cbbMonth.Location = new Point(60, 47);
            ultraLabel2.Location = new Point(160, 51);
            txtYear.Location = new Point(203, 47);
            InitializeGUI();
        }
        private void InitializeGUI()
        {
            #region Config cbbMonth
            cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 21 && int.Parse(n.Value) > 8).ToList(), "Name");
            cbbMonth.DropDownStyle = UltraComboStyle.DropDownList;
            try
            {
                foreach (var item in cbbMonth.Rows)
                {
                    if (int.Parse((item.ListObject as Item).Value) == DateTime.Now.Month + 8) cbbMonth.SelectedRow = item;
                }
            }
            catch (Exception e)
            {

            }
            #endregion
        }


        private void ultraOptionSet1_ValueChanged(object sender, EventArgs e)
        {
            if (ultraOptionSet1.CheckedIndex == 1)
            {
                ultraLabel6.Visible = true;
                txtDay.Visible = true;
                ultraLabel6.Location = new Point(12, 51);
                txtDay.Location = new Point(50, 47);
                ultraLabel1.Location = new Point(151, 51);
                cbbMonth.Location = new Point(203, 47);
                ultraLabel2.Location = new Point(324, 51);
                txtYear.Location = new Point(362, 47);
            }
            else
            {
                ultraLabel6.Visible = false;
                txtDay.Visible = false;
                ultraLabel1.Location = new Point(12, 51);
                cbbMonth.Location = new Point(60, 47);
                ultraLabel2.Location = new Point(160, 51);
                txtYear.Location = new Point(203, 47);
            }
        }

        private void ultraOptionSet2_ValueChanged(object sender, EventArgs e)
        {
            if (ultraOptionSet2.CheckedIndex == 1)
            {
                ultraLabel4.Visible = true;
                txtAdditionTime.Visible = true;
                ultraLabel5.Visible = true;
                dteKHBS.Visible = true;
                var temp = Utils.ListTM01TAIN.Where(n => dteDateFrom.DateTime <= n.FromDate && dteDateTo.DateTime >= n.ToDate || dteDateFrom.DateTime >= n.FromDate && dteDateTo.DateTime <= n.ToDate && n.IsFirstDeclaration == false).ToList();
                int maxAdditionalTime = temp.Max(n => n.AdditionTime) ?? 0;
                txtAdditionTime.Value = maxAdditionalTime + 1;
            }
            else
            {
                ultraLabel4.Visible = false;
                txtAdditionTime.Visible = false;
                ultraLabel5.Visible = false;
                dteKHBS.Visible = false;
            }
        }
        private void cbbMonth_ValueChanged(object sender, EventArgs e)
        {
            if (cbbMonth.SelectedRow != null)
            {
                var model = cbbMonth.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd((int)txtYear.Value,DateTime.Now, model, out dtBegin, out dtEnd);
                dteDateFrom.Value = new DateTime((int)txtYear.Value, dtBegin.Month, dtBegin.Day);
                dteDateTo.Value = new DateTime((int)txtYear.Value, dtEnd.Month, dtEnd.Day);
                //var temp = Utils.ListTM01TAIN.FirstOrDefault(p => p.FromDate.Month == dteDateFrom.DateTime.Month && p.ToDate.Month == dteDateTo.DateTime.Month && p.IsFirstDeclaration == true);
                //if (temp != null)
                //{
                //    ultraOptionSet2.CheckedIndex = 1;
                //}
                //else
                //{
                //    ultraOptionSet2.CheckedIndex = 0;
                //}
            }
        }
        private void txtYear_ValueChanged(object sender, EventArgs e)
        {
            if (txtYear.Value.ToInt() >= dteDateFrom.MinDate.Year && txtYear.Value.ToInt() <= dteDateFrom.MaxDate.Year)
            {
                DateTime t1 = dteDateFrom.DateTime;
                DateTime t2 = dteDateTo.DateTime;
                if (cbbMonth.Text == "Tháng 2")
                {
                    var model = cbbMonth.SelectedRow.ListObject as Item;
                    DateTime dtBegin;
                    DateTime dtEnd;
                    Utils.GetDateBeginDateEnd((int)txtYear.Value, DateTime.Now, model, out dtBegin, out dtEnd);
                    dteDateFrom.Value = new DateTime((int)txtYear.Value, t1.Month, t1.Day);
                    dteDateTo.Value = new DateTime((int)txtYear.Value, t2.Month, dtEnd.Day);
                }
                else
                {
                    dteDateFrom.Value = new DateTime((int)txtYear.Value, t1.Month, t1.Day);
                    dteDateTo.Value = new DateTime((int)txtYear.Value, t2.Month, t2.Day);
                }
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtYear.Value.ToInt() < dteDateFrom.MinDate.Year || txtYear.Value.ToInt() > dteDateFrom.MaxDate.Year)
            {
                txtYear.Value = DateTime.Now.Year;
                txtYear.Focus();
                return;
            }
            
            string[] s = cbbMonth.Text.Split();
            string DeclarationTerm = "";
            bool isDeclaredMonth = false;
            DateTime FromDate = DateTime.Now.Date;
            DateTime ToDate = DateTime.Now.Date;
            bool isDeclaredArising = false;
            int Day = 1;
            int Month = 1;
            int Year = 1;
            bool IsFirst = true;
            string AdditionalTime = "";
            DateTime KHBS= DateTime.Now.Date;

            if (ultraOptionSet1.CheckedIndex == 0)
            {
                DeclarationTerm = cbbMonth.Text + " năm " + txtYear.Value;
                isDeclaredMonth = true;
                FromDate = dteDateFrom.DateTime;
                ToDate = dteDateTo.DateTime;
            }
            else
            {
                isDeclaredArising = true;
                DeclarationTerm = "ngày " + txtDay.Value + " tháng " + s[1] + " năm " + txtYear.Value;
                Day = txtDay.Value.ToInt();
                Month = int.Parse(s[1]);
                Year = txtYear.Value.ToInt();
            }
            if (ultraOptionSet2.Value.ToString() == "0")
            {
                if (ultraOptionSet1.Value.ToString() == "0")
                {
                    if (txtYear.Value.ToInt() > DateTime.Now.Year)
                    {
                        MSG.Warning("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                        return;
                    }
                    else if (txtYear.Value.ToInt() == DateTime.Now.Year)
                    {
                        if (int.Parse(s[1]) > DateTime.Now.Month)
                        {
                            MSG.Warning("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                            return;
                        }
                    }
                    List<TM01TAIN> lstTm = Utils.ListTM01TAIN.Where(n => dteDateFrom.DateTime == n.FromDate && dteDateTo.DateTime == n.ToDate).ToList();
                    if (lstTm.Count > 0)
                    {
                        if (DialogResult.Yes == MessageBox.Show("Đã tồn tại tờ khai này trong kỳ trên. Bạn có muốn mở không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                        {
                            TM01TAIN tM01TAIN = lstTm.FirstOrDefault();
                            _01_TAIN_Detail temp = new _01_TAIN_Detail(tM01TAIN, true);
                            this.Close();
                            this.Dispose();
                            temp.Show();
                            
                        }
                        return;
                    }
                    else
                    {
                    }
                }
                else
                {

                    if (txtYear.Value.ToInt() > DateTime.Now.Year)
                    {
                        MSG.Warning("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                        return;
                    }
                    else if (txtYear.Value.ToInt() == DateTime.Now.Year)
                    {
                        if (int.Parse(s[1]) > DateTime.Now.Month)
                        {
                            MSG.Warning("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                            return;
                        }
                        else if (int.Parse(s[1]) == DateTime.Now.Month)
                        {
                            if (txtDay.Value.ToInt() > DateTime.Now.Day)
                            {
                                MSG.Warning("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                                return;
                            }
                        }
                    }
                    List<TM01TAIN> lstTm = Utils.ListTM01TAIN.Where(n => txtDay.Value.ToInt() == n.Day && int.Parse(s[1]) == n.Month && txtYear.Value.ToInt() == n.Year).ToList();
                    if (lstTm.Count > 0)
                    {
                        if (DialogResult.Yes == MessageBox.Show("Đã tồn tại tờ khai này trong kỳ trên. Bạn có muốn mở không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                        {
                            TM01TAIN tM01TAIN = lstTm.FirstOrDefault();
                            _01_TAIN_Detail temp = new _01_TAIN_Detail(tM01TAIN, true);
                            this.Close();
                            this.Dispose();
                            temp.Show();
                           
                        }
                        return;
                    }
                    else
                    {
                    }
                }
                IsFirst = true;

            }
            else
            {

                var temp = Utils.ListTM01TAIN.Where(n => dteDateFrom.DateTime <= n.FromDate && dteDateTo.DateTime >= n.ToDate || dteDateFrom.DateTime >= n.FromDate && dteDateTo.DateTime <= n.ToDate && n.IsFirstDeclaration == false).ToList();
                if (temp.Count > 0)
                {
                    int maxAdditionalTime = temp.Max(n => n.AdditionTime) ?? 0;
                    if (int.Parse(txtAdditionTime.Value.ToString()) != (maxAdditionalTime + 1))
                    {
                        MSG.Warning("Chưa tồn tại kỳ khai lần " + (int.Parse(txtAdditionTime.Value.ToString()) - 1));
                        return;
                    }
                    else
                    {
                        IsFirst = false;
                        AdditionalTime = txtAdditionTime.Value.ToString();
                        KHBS = dteKHBS.DateTime;
                    }
                }

            }
            _01_TAIN_Detail _01_TAIN_Detail = new _01_TAIN_Detail(new TM01TAIN(), false);
            _01_TAIN_Detail.DeclarationTerm = DeclarationTerm;
            _01_TAIN_Detail.isDeclaredMonth = isDeclaredMonth;
            _01_TAIN_Detail.FromDate = FromDate;
            _01_TAIN_Detail.ToDate = ToDate;
            _01_TAIN_Detail.isDeclaredArising = isDeclaredArising;
            _01_TAIN_Detail.Day = Day;
            _01_TAIN_Detail.Month = Month;
            _01_TAIN_Detail.Year = Year;

            _01_TAIN_Detail.IsFirst = IsFirst;
            _01_TAIN_Detail.AdditionalTime = AdditionalTime;
            _01_TAIN_Detail.KHBS = KHBS;
            this.Close();
            this.Dispose();
            if (_01_TAIN_Detail.LoadInitialize(false))
                _01_TAIN_Detail.Show();
           
        }

        private void TaxPeriod_01_TAIN_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (Form)sender;
            frm.Dispose();

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}
