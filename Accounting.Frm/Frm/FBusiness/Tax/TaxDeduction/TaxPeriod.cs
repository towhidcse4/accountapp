﻿using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.DAO;
using Accounting.Core.Domain.obj.Report;
using Accounting.TextMessage;

namespace Accounting
{
    public partial class TaxPeriod : Form
    {
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        public TaxPeriod()
        {
            InitializeComponent();
            txtYear.Value = DateTime.Now.Year;
            InitializeGUI();
        }
        private void InitializeGUI()
        {
            cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 21 && int.Parse(n.Value) > 8).ToList(), "Name");
            cbbMonth.DropDownStyle = UltraComboStyle.DropDownList;
            try
            {
                foreach (var item in cbbMonth.Rows)
                {
                    if (int.Parse((item.ListObject as Item).Value) == DateTime.Now.Month + 8) cbbMonth.SelectedRow = item;
                }
            }
            catch (Exception e)
            {

            }

        }
        private void ultraOptionSet1_ValueChanged(object sender, EventArgs e)
        {
            if (OptionChoose.CheckedIndex == 0)
            {
                ultraLabel1.Text = "Tháng";
                cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 21 && int.Parse(n.Value) > 8).ToList(), "Name");
                foreach (var item in cbbMonth.Rows)
                {
                    if (int.Parse((item.ListObject as Item).Value) == DateTime.Now.Month + 8) cbbMonth.SelectedRow = item;
                }
            }
            else
            {
                ultraLabel1.Text = "Quý";
                cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 25 && int.Parse(n.Value) > 20).ToList(), "Name");
                float now = float.Parse(DateTime.Now.Month.ToString()) / 3;
                foreach (var item in cbbMonth.Rows)
                {
                    if (now <= 1)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 21) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 2 && now > 1)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 22) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 3 && now > 2)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 23) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 4 && now > 3)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 24) cbbMonth.SelectedRow = item;
                    }

                }

            }
        }
        private void cbbMonth_ValueChanged(object sender, EventArgs e)
        {
            if (cbbMonth.SelectedRow != null)
            {
                var model = cbbMonth.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd((int)txtYear.Value, DateTime.Now, model, out dtBegin, out dtEnd);
                dteDateFrom.Value = new DateTime((int)txtYear.Value, dtBegin.Month, dtBegin.Day);
                dteDateTo.Value = new DateTime((int)txtYear.Value, dtEnd.Month, dtEnd.Day);
            }
        }
        private void txtYear_ValueChanged(object sender, EventArgs e)
        {
            DateTime t1 = dteDateFrom.DateTime;
            DateTime t2 = dteDateTo.DateTime;
            if (cbbMonth.Text == "Tháng 2")
            {
                var model = cbbMonth.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd((int)txtYear.Value, DateTime.Now, model, out dtBegin, out dtEnd);
                dteDateFrom.Value = new DateTime((int)txtYear.Value, t1.Month, t1.Day);
                dteDateTo.Value = new DateTime((int)txtYear.Value, t2.Month, dtEnd.Day);
            }
            else
            {
                dteDateFrom.Value = new DateTime((int)txtYear.Value, t1.Month, t1.Day);
                dteDateTo.Value = new DateTime((int)txtYear.Value, t2.Month, t2.Day);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            DateTime _dateFrom = (DateTime)dteDateFrom.Value;
            DateTime _dateTo = (DateTime)dteDateTo.Value;

            #region lay gia tri thue dc khau tru tu to khai thue
            //lay gia tri thue dc khau tru tu to khai thue
            var ToKhaiThue = Utils.ListTM01GTGT.FirstOrDefault(m => m.FromDate == _dateFrom && m.ToDate == _dateTo && m.IsFirstDeclaration == true);

            //neu ko co to khai thue trong ky thi thong bao ko co du lieu
            if (ToKhaiThue == null)
            {
                MSG.Warning("Không có số liệu khấu trừ thuế kỳ này.");
                return;
            }

            var idThue = ToKhaiThue.ID;
            
            decimal ThueDauVao = 0;
            decimal ThueDauRa = 0;

            decimal ThueCT22 = 0;
            decimal ThueCT25 = 0;

            decimal TongThue133 = 0;

            //var ToKhaiThueDetail = new List<TM01GTGTDetail>();

            ThueCT22 = Convert.ToDecimal(Utils.ListTM01GTGTDetail.FirstOrDefault(m => m.TM01GTGTID == idThue && m.Code == "Item22").Data);
            ThueCT25 = Convert.ToDecimal(Utils.ListTM01GTGTDetail.FirstOrDefault(m => m.TM01GTGTID == idThue && m.Code == "Item25").Data);

            ThueDauVao = ThueCT22 + ThueCT25;

            ThueDauRa = Convert.ToDecimal(Utils.ListTM01GTGTDetail.FirstOrDefault(m => m.TM01GTGTID == idThue && m.Code == "Item35").Data);

            //so sanh gia tri thue dau ra, gia tri thue dau vao, lay gia tri nho hon la tong cua 1332 va 1331
            if (ThueDauRa < ThueDauVao)
                TongThue133 = ThueDauRa;
            else
                TongThue133 = ThueDauVao;

            #endregion

            if(TongThue133 <= 0)
            {
                MSG.Warning("Không có số liệu khấu trừ thuế kỳ này.");
                return;
            }

            #region lay gia tri thue cua 1332 va 1331
            //lay gia tri thue cua 1332 va 1331
            decimal Thue1332 = 0;
            decimal Thue1331 = 0;

            ReportProcedureSDS sp = new ReportProcedureSDS();
            //comment by cuongpv
            //var _lstThue = new List<BangKeMuaVao_Model>();

            //_lstThue = sp.GetProc_GetBkeInv_BuyTax(_dateFrom, _dateTo, 1, 0, "", 1);//add by cuongpv: them dk check khau tru thue hay ko

            //if (_lstThue.Count > 0)
            //{
            //    var _lstThueTong = new List<BangKeMuaVao_Model>();

            //    _lstThueTong = _lstThue.GroupBy(m => m.TK_THUE).Select(t => new BangKeMuaVao_Model
            //    {
            //        TK_THUE = t.Key,
            //        THUE_GTGT = t.Sum(k => k.THUE_GTGT)
            //    }).ToList();

            //    if (_lstThueTong.Count > 0)
            //    {
            //        var _lstThue1332 = _lstThueTong.FirstOrDefault(m => m.TK_THUE == "1332");
            //        if (_lstThue1332 != null)
            //            Thue1332 = _lstThueTong.FirstOrDefault(m => m.TK_THUE == "1332").THUE_GTGT;
            //    }
            //}
            //end comment by cuongpv
            //Thue1332 = _lstThueTong.Where(m => m.TK_THUE == "1332").ToList().Select(t => t.THUE_GTGT).FirstOrDefault();

            //add by cuongpv to fix khau tru thue 1332
            var _lstThue = new List<GL_GetBookDetailPaymentByAccountNumber>();
            _lstThue = sp.GetSoChiTietCacTaiKhoan("", "", _dateFrom, _dateTo, "1332", 0);
            if(_lstThue.Count > 0)
            {
                decimal soDuCuoiCredit = _lstThue[_lstThue.Count() - 1].ClosingCreditAmount;
                decimal soDuCuoiDebit = _lstThue[_lstThue.Count() - 1].ClosingDebitAmount;
                if(soDuCuoiCredit > 0 || soDuCuoiDebit > 0)
                {
                    if (soDuCuoiCredit >= soDuCuoiDebit)
                        Thue1332 = soDuCuoiCredit;
                    else
                        Thue1332 = soDuCuoiDebit;
                }
            }
            //end add by cuongpv

            //add by cuongpv to fix bug SDSACC_6286: sai khau tru thue truong hop TongThue133 < Thue1332
            if (TongThue133 < Thue1332)
            {
                Thue1332 = TongThue133;
            }
            //end add by cuongpv

            Thue1331 = TongThue133 - Thue1332;
            
            #endregion
            //check da sinh phieu thue ky nay trong govoucher chua
            DateTime _datecheck = new DateTime(_dateTo.Year, _dateTo.Month, _dateTo.Day);
            Utils.ClearCacheByType<GOtherVoucher>(); // Hautv edit
            var _lstCheckCreate = Utils.ListGOtherVoucher.FirstOrDefault(g => g.PostedDate == _datecheck && g.TypeID == 602);
            if(_lstCheckCreate != null)
            {
                MSG.Warning("Kỳ này đã phát sinh chứng từ nghiệp vụ khấu trừ thuế - Số chứng từ: [" + _lstCheckCreate.No + "]. \n Vui lòng xóa chứng từ xử lý trước khi thực hiện khấu trừ thuế kỳ này!");
                return;
            }
            #region tao chung tu khac va luu
            //tao chung tu khac va luu
            GOtherVoucher govoucher = new GOtherVoucher();
            govoucher.ID = Guid.NewGuid();
            govoucher.TypeID = 602;
            govoucher.Date = new DateTime(_dateTo.Year, _dateTo.Month, _dateTo.Day);
            govoucher.PostedDate = new DateTime(_dateTo.Year, _dateTo.Month, _dateTo.Day);
            govoucher.No = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(60));
            if (OptionChoose.CheckedIndex == 0)
                govoucher.Reason = "Khấu trừ thuế " + cbbMonth.Value + " năm " + txtYear.Value;//lay theo thang
            else
                govoucher.Reason = "Khấu trừ thuế " + cbbMonth.Value + " năm  " + txtYear.Value;//lay theo quy
            govoucher.TotalAmount = TongThue133;//gia tri tong duoc khau tru
            govoucher.TotalAmountOriginal = TongThue133;//gia tri tong duoc khau tru
            govoucher.CurrencyID = "VND";
            govoucher.ExchangeRate = 1;
            govoucher.Recorded = true;
            govoucher.Exported = true;
            govoucher.TemplateID = new Guid("70453208-B426-40B2-BDEC-4F28CD2EEC87");
            //tao chung tu khac detail
            GOtherVoucherDetail govoucherDetail = new GOtherVoucherDetail();
            if (Thue1332 > 0)
            {
                govoucherDetail = new GOtherVoucherDetail
                {
                    GOtherVoucherID = govoucher.ID,
                    Description = govoucher.Reason,
                    CurrencyID = govoucher.CurrencyID,
                    DebitAccount = "33311",
                    CreditAccount = "1332",
                    Amount = Thue1332,
                    AmountOriginal = Thue1332,
                    ExchangeRate = govoucher.ExchangeRate ?? 0
                };
                govoucher.GOtherVoucherDetails.Add(govoucherDetail);
            }

            if(Thue1331 > 0)
            {
                govoucherDetail = new GOtherVoucherDetail
                {
                    GOtherVoucherID = govoucher.ID,
                    Description = govoucher.Reason,
                    CurrencyID = govoucher.CurrencyID,
                    DebitAccount = "33311",
                    CreditAccount = "1331",
                    Amount = Thue1331,
                    AmountOriginal = Thue1331,
                    ExchangeRate = govoucher.ExchangeRate ?? 0
                };
                govoucher.GOtherVoucherDetails.Add(govoucherDetail);
            }

            Utils.IGOtherVoucherService.BeginTran();
            Utils.IGOtherVoucherService.CreateNew(govoucher);

            #endregion

            #region Luu vao GL
            //if (Utils.ListSystemOption.FirstOrDefault(x => x.ID == 90).Data == "1")
            //{
            //    Utils.SaveLedger<GOtherVoucher>(govoucher);
            //}
            List<GeneralLedger> generalLedgers = GenGeneralLedgers(govoucher);
            foreach (GeneralLedger detiail in generalLedgers)
            {
                Utils.IGeneralLedgerService.Save(detiail);
            }

            bool status = Utils.IGenCodeService.UpdateGenCodeForm(60, govoucher.No, govoucher.ID);
            if (!status)
            {
                MSG.Warning(resSystem.MSG_System_52);
                Utils.IGOtherVoucherService.RolbackTran();
                return;
            }

            Utils.IGOtherVoucherService.CommitTran();

            Utils.ClearCacheByType<GOtherVoucher>();
            if (
                MSG.Question("Hệ thống đã sinh chứng từ số " + govoucher.No + " để hạch toán khấu trừ thuế GTGT" +
                             " \nBạn có muốn xem chứng từ vừa tạo không") == DialogResult.Yes)
            {
                FGOtherVoucherDetail frm = new FGOtherVoucherDetail(govoucher, new List<GOtherVoucher>() { govoucher }, ConstFrm.optStatusForm.View);
                frm.ShowDialog(this);
            }
            Utils.ClearCacheByType<GOtherVoucher>();
            #endregion
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private List<GeneralLedger> GenGeneralLedgers(GOtherVoucher gOtherVoucher)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            for (int i = 0; i < gOtherVoucher.GOtherVoucherDetails.Count; i++)
            {
                #region Cặp Nợ - Có
                if (gOtherVoucher.TypeID == 840 && gOtherVoucher.GOtherVoucherDetails[i].Amount == 0 && gOtherVoucher.GOtherVoucherDetails[i].AmountOriginal == 0)
                    continue;
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = gOtherVoucher.ID,
                    TypeID = gOtherVoucher.TypeID,
                    Date = gOtherVoucher.Date,
                    PostedDate = gOtherVoucher.PostedDate,
                    No = gOtherVoucher.No,
                    Account = gOtherVoucher.GOtherVoucherDetails[i].DebitAccount,
                    AccountCorresponding = gOtherVoucher.GOtherVoucherDetails[i].CreditAccount,
                    CurrencyID = gOtherVoucher.CurrencyID,
                    ExchangeRate = gOtherVoucher.ExchangeRate,
                    DebitAmount = (decimal)gOtherVoucher.GOtherVoucherDetails[i].Amount,
                    DebitAmountOriginal = (decimal)gOtherVoucher.GOtherVoucherDetails[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = gOtherVoucher.Reason,
                    Description = gOtherVoucher.GOtherVoucherDetails[i].Description,
                    AccountingObjectID = gOtherVoucher.GOtherVoucherDetails[i].DebitAccountingObjectID,
                    EmployeeID = gOtherVoucher.GOtherVoucherDetails[i].EmployeeID,
                    BudgetItemID = gOtherVoucher.GOtherVoucherDetails[i].BudgetItemID,
                    CostSetID = gOtherVoucher.GOtherVoucherDetails[i].CostSetID,
                    ContractID = gOtherVoucher.GOtherVoucherDetails[i].ContractID,
                    StatisticsCodeID = gOtherVoucher.GOtherVoucherDetails[i].StatisticsCodeID,
                    DetailID = gOtherVoucher.GOtherVoucherDetails[i].ID,
                    RefNo = gOtherVoucher.No,
                    RefDate = gOtherVoucher.Date,
                    DepartmentID = gOtherVoucher.GOtherVoucherDetails[i].DepartmentID,
                    ExpenseItemID = gOtherVoucher.GOtherVoucherDetails[i].ExpenseItemID,
                    IsIrrationalCost = false
                };
                if (((gOtherVoucher.GOtherVoucherDetails[i].CreditAccount.StartsWith("133")) ||
                     gOtherVoucher.GOtherVoucherDetails[i].CreditAccount.StartsWith("33311")))
                {
                    genTemp.VATDescription = gOtherVoucher.GOtherVoucherDetails.Count > i ? gOtherVoucher.GOtherVoucherDetails[i].Description : null;
                }
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = gOtherVoucher.ID,
                    TypeID = gOtherVoucher.TypeID,
                    Date = gOtherVoucher.Date,
                    PostedDate = gOtherVoucher.PostedDate,
                    No = gOtherVoucher.No,
                    Account = gOtherVoucher.GOtherVoucherDetails[i].CreditAccount,
                    AccountCorresponding = gOtherVoucher.GOtherVoucherDetails[i].DebitAccount,
                    CurrencyID = gOtherVoucher.CurrencyID,
                    ExchangeRate = gOtherVoucher.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = (decimal)gOtherVoucher.GOtherVoucherDetails[i].Amount,
                    CreditAmountOriginal = (decimal)gOtherVoucher.GOtherVoucherDetails[i].AmountOriginal,
                    Reason = gOtherVoucher.Reason,
                    Description = gOtherVoucher.GOtherVoucherDetails[i].Description,
                    AccountingObjectID = gOtherVoucher.GOtherVoucherDetails[i].CreditAccountingObjectID,
                    EmployeeID = gOtherVoucher.GOtherVoucherDetails[i].EmployeeID,
                    BudgetItemID = gOtherVoucher.GOtherVoucherDetails[i].BudgetItemID,
                    CostSetID = gOtherVoucher.GOtherVoucherDetails[i].CostSetID,
                    ContractID = gOtherVoucher.GOtherVoucherDetails[i].ContractID,
                    StatisticsCodeID = gOtherVoucher.GOtherVoucherDetails[i].StatisticsCodeID,
                    DetailID = gOtherVoucher.GOtherVoucherDetails[i].ID,
                    RefNo = gOtherVoucher.No,
                    RefDate = gOtherVoucher.Date,
                    DepartmentID = gOtherVoucher.GOtherVoucherDetails[i].DepartmentID,
                    ExpenseItemID = gOtherVoucher.GOtherVoucherDetails[i].ExpenseItemID,
                    IsIrrationalCost = false
                };
                if (((gOtherVoucher.GOtherVoucherDetails[i].CreditAccount.StartsWith("133")) ||
                     gOtherVoucher.GOtherVoucherDetails[i].CreditAccount.StartsWith("33311")))
                {
                    genTempCorresponding.VATDescription = gOtherVoucher.GOtherVoucherDetails.Count > i ? gOtherVoucher.GOtherVoucherDetails[i].Description : null;
                }
                listGenTemp.Add(genTempCorresponding);

                #endregion
            }
            return listGenTemp;
        }
    }
}
