﻿namespace Accounting
{
    partial class TaxPeriod_02_TAIN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TaxPeriod_02_TAIN));
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.dteDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dteDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblBeginDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblEndDate = new Infragistics.Win.Misc.UltraLabel();
            this.txtNumber = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.dteKHBS = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtYear = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.ultraOptionSet2 = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel4 = new Infragistics.Win.Misc.UltraPanel();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteKHBS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet2)).BeginInit();
            this.ultraPanel4.ClientArea.SuspendLayout();
            this.ultraPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.dteDateTo);
            this.ultraPanel1.ClientArea.Controls.Add(this.dteDateFrom);
            this.ultraPanel1.ClientArea.Controls.Add(this.lblBeginDate);
            this.ultraPanel1.ClientArea.Controls.Add(this.lblEndDate);
            this.ultraPanel1.ClientArea.Controls.Add(this.txtNumber);
            this.ultraPanel1.ClientArea.Controls.Add(this.dteKHBS);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel5);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel4);
            this.ultraPanel1.ClientArea.Controls.Add(this.txtYear);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraOptionSet2);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel2);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(527, 118);
            this.ultraPanel1.TabIndex = 0;
            // 
            // dteDateTo
            // 
            appearance1.TextHAlignAsString = "Center";
            this.dteDateTo.Appearance = appearance1;
            this.dteDateTo.Location = new System.Drawing.Point(423, 23);
            this.dteDateTo.MaskInput = "dd/mm/yyyy";
            this.dteDateTo.Name = "dteDateTo";
            this.dteDateTo.Nullable = false;
            this.dteDateTo.Size = new System.Drawing.Size(91, 21);
            this.dteDateTo.TabIndex = 2;
            // 
            // dteDateFrom
            // 
            appearance2.TextHAlignAsString = "Center";
            this.dteDateFrom.Appearance = appearance2;
            this.dteDateFrom.Location = new System.Drawing.Point(236, 23);
            this.dteDateFrom.MaskInput = "dd/mm/yyyy";
            this.dteDateFrom.Name = "dteDateFrom";
            this.dteDateFrom.Nullable = false;
            this.dteDateFrom.Size = new System.Drawing.Size(96, 21);
            this.dteDateFrom.TabIndex = 1;
            // 
            // lblBeginDate
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextHAlignAsString = "Left";
            appearance3.TextVAlignAsString = "Bottom";
            this.lblBeginDate.Appearance = appearance3;
            this.lblBeginDate.Location = new System.Drawing.Point(176, 21);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.Size = new System.Drawing.Size(54, 20);
            this.lblBeginDate.TabIndex = 63;
            this.lblBeginDate.Text = "Từ tháng";
            // 
            // lblEndDate
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextHAlignAsString = "Left";
            appearance4.TextVAlignAsString = "Bottom";
            this.lblEndDate.Appearance = appearance4;
            this.lblEndDate.Location = new System.Drawing.Point(352, 24);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(65, 17);
            this.lblEndDate.TabIndex = 64;
            this.lblEndDate.Text = "Đến tháng";
            // 
            // txtNumber
            // 
            this.txtNumber.Location = new System.Drawing.Point(219, 74);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.PromptChar = ' ';
            this.txtNumber.Size = new System.Drawing.Size(56, 21);
            this.txtNumber.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.txtNumber.TabIndex = 3;
            this.txtNumber.Visible = false;
            // 
            // dteKHBS
            // 
            this.dteKHBS.Location = new System.Drawing.Point(401, 74);
            this.dteKHBS.MaskInput = "dd/mm/yyyy";
            this.dteKHBS.Name = "dteKHBS";
            this.dteKHBS.Size = new System.Drawing.Size(98, 21);
            this.dteKHBS.TabIndex = 4;
            this.dteKHBS.Visible = false;
            // 
            // ultraLabel5
            // 
            this.ultraLabel5.Location = new System.Drawing.Point(300, 78);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(95, 17);
            this.ultraLabel5.TabIndex = 9;
            this.ultraLabel5.Text = "Ngày lập KHBS";
            this.ultraLabel5.Visible = false;
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.Location = new System.Drawing.Point(176, 78);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(37, 17);
            this.ultraLabel4.TabIndex = 8;
            this.ultraLabel4.Text = "Lần";
            this.ultraLabel4.Visible = false;
            // 
            // txtYear
            // 
            this.txtYear.Location = new System.Drawing.Point(49, 23);
            this.txtYear.Name = "txtYear";
            this.txtYear.PromptChar = ' ';
            this.txtYear.Size = new System.Drawing.Size(97, 21);
            this.txtYear.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.txtYear.TabIndex = 0;
            this.txtYear.ValueChanged += new System.EventHandler(this.txtYear_ValueChanged);
            // 
            // ultraOptionSet2
            // 
            this.ultraOptionSet2.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraOptionSet2.CheckedIndex = 0;
            valueListItem3.DataValue = 0;
            valueListItem3.DisplayText = "Tờ khai lần đầu";
            valueListItem4.DataValue = 1;
            valueListItem4.DisplayText = "Tờ khai bổ sung";
            this.ultraOptionSet2.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem4});
            this.ultraOptionSet2.Location = new System.Drawing.Point(11, 60);
            this.ultraOptionSet2.Name = "ultraOptionSet2";
            this.ultraOptionSet2.Size = new System.Drawing.Size(159, 35);
            this.ultraOptionSet2.TabIndex = 3;
            this.ultraOptionSet2.Text = "Tờ khai lần đầu";
            this.ultraOptionSet2.ValueChanged += new System.EventHandler(this.ultraOptionSet2_ValueChanged);
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(11, 27);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(32, 17);
            this.ultraLabel2.TabIndex = 3;
            this.ultraLabel2.Text = "Năm";
            // 
            // ultraPanel4
            // 
            // 
            // ultraPanel4.ClientArea
            // 
            this.ultraPanel4.ClientArea.Controls.Add(this.btnClose);
            this.ultraPanel4.ClientArea.Controls.Add(this.btnSave);
            this.ultraPanel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel4.Location = new System.Drawing.Point(0, 119);
            this.ultraPanel4.Name = "ultraPanel4";
            this.ultraPanel4.Size = new System.Drawing.Size(527, 55);
            this.ultraPanel4.TabIndex = 13;
            // 
            // btnClose
            // 
            appearance5.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance5;
            this.btnClose.Location = new System.Drawing.Point(439, 13);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 61;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance6.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance6;
            this.btnSave.Location = new System.Drawing.Point(358, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 60;
            this.btnSave.Text = "Đồng ý";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // TaxPeriod_02_TAIN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(527, 174);
            this.Controls.Add(this.ultraPanel4);
            this.Controls.Add(this.ultraPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "TaxPeriod_02_TAIN";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chọn kỳ tính thuế";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TaxPeriod_02_TAIN_FormClosed);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ClientArea.PerformLayout();
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dteDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteKHBS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet2)).EndInit();
            this.ultraPanel4.ClientArea.ResumeLayout(false);
            this.ultraPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDateTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDateFrom;
        private Infragistics.Win.Misc.UltraLabel lblBeginDate;
        private Infragistics.Win.Misc.UltraLabel lblEndDate;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtNumber;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteKHBS;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtYear;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet ultraOptionSet2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraPanel ultraPanel4;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
    }
}