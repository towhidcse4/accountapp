﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting
{
    public partial class TaxPeriod_02_TAIN : Form
    {
        public TaxPeriod_02_TAIN()
        {
            InitializeComponent();
            txtYear.Value = DateTime.Now.Year;
        }

        private void txtYear_ValueChanged(object sender, EventArgs e)
        {
            try {
                DateTime t1 = dteDateFrom.DateTime;
                DateTime t2 = dteDateTo.DateTime;
                dteDateFrom.Value = new DateTime((int)txtYear.Value, 1, 1);
                dteDateTo.Value = new DateTime((int)txtYear.Value, 12, DateTime.DaysInMonth((int)txtYear.Value, 12));
            }
            catch(Exception ex)
            { }
            }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ultraOptionSet2_ValueChanged(object sender, EventArgs e)
        {
            if (ultraOptionSet2.CheckedIndex == 1)
            {
                ultraOptionSet2.CheckedIndex = 0;
                return;//

                ultraLabel4.Visible = true;
                txtNumber.Visible = true;
                ultraLabel5.Visible = true;
                dteKHBS.Visible = true;
            }
            else
            {
                ultraLabel4.Visible = false;
                txtNumber.Visible = false;
                ultraLabel5.Visible = false;
                dteKHBS.Visible = false;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool item02 = false;
            if (int.Parse(txtYear.Value.ToString()) > DateTime.Now.Year)
            {
                MSG.Error("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                return;
            }
            if (int.Parse(txtYear.Value.ToString()) < dteDateFrom.MinDate.Year)
            {
                MSG.Error("Năm không tồn tại");
                return;
            }
            if (ultraOptionSet2.Value.ToString() == "0")
            {
                item02 = true;
            }
            var lst = Utils.ListTM02TAIN.Select(n => n.DeclarationTerm);
            if (lst.Contains("Năm " + txtYear.Value.ToString()))
            {
                if (DialogResult.Yes == MessageBox.Show("Đã tồn tại tờ khai này trong kỳ trên. Bạn có muốn mở không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    TM02TAIN tM02TAIN = Utils.ListTM02TAIN.FirstOrDefault(n => n.DeclarationTerm.Contains("Năm " + txtYear.Value.ToString()));
                    _02_TAIN_Detail _02_TAIN_DetailE = new _02_TAIN_Detail(tM02TAIN, true);
                    this.Hide();
                    this.Close();
                    this.Dispose();
                    _02_TAIN_DetailE.Show();
                    
                }
                return;
            }
            _02_TAIN_Detail _02_TAIN_Detail = new _02_TAIN_Detail(new TM02TAIN(), false);
            _02_TAIN_Detail.FromDate = dteDateFrom.DateTime;
            _02_TAIN_Detail.ToDate = dteDateTo.DateTime;
            _02_TAIN_Detail.year = (int)txtYear.Value;
            _02_TAIN_Detail.item02 = item02;
            _02_TAIN_Detail.LoadInitialize();
            this.Hide();
            this.Close();
            this.Dispose();
            _02_TAIN_Detail.Show();
           

        }

        private void TaxPeriod_02_TAIN_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (Form)sender;
            frm.Dispose();

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}
