﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraDataGridView;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Xml;

namespace Accounting
{
    public partial class _02_TAIN_Detail : Form
    {
        private TM02TAIN _select;
        private bool _isEdit;
        public bool item02 = false;
        //public ISystemOptionService _ISystemOptionService { get { return IoC.Resolve<ISystemOptionService>(); } }
        public string item03 = "";
        public int year = 2019;
        public DateTime FromDate;
        public DateTime ToDate;
        List<MaterialGoodsResourceTaxGroup> lstMaterialGoodsResourceTaxGroup1 = new List<MaterialGoodsResourceTaxGroup>();
        List<MaterialGoodsResourceTaxGroup> lstMaterialGoodsResourceTaxGroup = new List<MaterialGoodsResourceTaxGroup>();
        List<string> lstReadOnly = new List<string>();
        public _02_TAIN_Detail(TM02TAIN tM02TAIN, bool isEdit)
        {
            lstMaterialGoodsResourceTaxGroup = new List<MaterialGoodsResourceTaxGroup>();
            _select = tM02TAIN;
            _isEdit = isEdit;
            InitializeComponent();
            lstReadOnly = lstReadOnlyDefaut();
            LoadTable(tableLayoutPanel4);
            LoadTable(tableLayoutPanel5);
            AddControlToTable(tableLayoutPanel4, true);
            AddControlToTable(tableLayoutPanel5, true);
            txtSum8.SelectedTextForeColor = Color.Blue;
            txtSum9.SelectedTextForeColor = Color.Blue;
            txtSum10.SelectedTextForeColor = Color.Blue;
            txtSum11.SelectedTextForeColor = Color.Blue;
            txtSum12.SelectedTextForeColor = Color.Blue;
            if (isEdit)
            {
                utmDetailBaseToolBar.Tools["mnbtnSave"].SharedProps.Enabled = false;
                ObjandGUI(tM02TAIN, false);
                LoadReadOnly(true);
            }
            else
            {
                utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["mnbtnDelete"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Visible = false;
                LoadReadOnly(false);
            }

            Utils.ClearCacheByType<SystemOption>();

            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
                this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
            }


            //utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Visible = false;
        }
        public bool LoadInitialize()
        {
            txtItem1.Text = year.ToString();
            txtItem2.Checked = item02;
            txtItem3.Text = item03;
            txtItem4.Text = Utils.GetCompanyName();
            txtItem5.Text = Utils.GetCompanyTaxCode();
            txtItem12.Text = Utils.GetTenDLT();
            txtItem13.Text = Utils.GetMSTDLT();
            txtName.Value = Utils.GetHVTNhanVienDLT();
            txtCCHN.Value = Utils.GetCCHNDLT();
            txtNameSign.Value = Utils.GetCompanyDirector();
            txtSignDate.Value = DateTime.Now;
            if (!item02)
            {

            }
            else
            {
                ultraTabControl1.Tabs[1].Visible = false;
                utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Visible = false;
            }
            return true;
        }

        #region check lỗi

        #endregion
        public bool CheckError()
        {
            if (tableLayoutPanel4.RowCount == 1 && tableLayoutPanel5.RowCount > 1)
            {
                for (int i = 0; i < tableLayoutPanel5.RowCount; i++)
                {
                    if ((tableLayoutPanel5.GetControlFromPosition(1, i) as UltraCombo).Value.IsNullOrEmpty())
                    {
                        MSG.Error("Chưa chọn loại tài nguyên");
                        return false;
                    }
                }
            }
            else if (tableLayoutPanel4.RowCount > 1 && tableLayoutPanel5.RowCount == 1)
            {
                for (int i = 0; i < tableLayoutPanel4.RowCount; i++)
                {
                    if ((tableLayoutPanel4.GetControlFromPosition(1, i) as UltraCombo).Value.IsNullOrEmpty())
                    {
                        MSG.Error("Chưa chọn loại tài nguyên");
                        return false;
                    }
                }
            }
            else if (tableLayoutPanel4.RowCount == 1 && tableLayoutPanel5.RowCount == 1)
            {
                if ((tableLayoutPanel4.GetControlFromPosition(1, 0) as UltraCombo).Value.IsNullOrEmpty() && (tableLayoutPanel5.GetControlFromPosition(1, 0) as UltraCombo).Value.IsNullOrEmpty())
                {
                    MSG.Error("Chưa chọn loại tài nguyên");
                    return false;
                }
            }
            else
            {
                for (int i = 0; i < tableLayoutPanel4.RowCount; i++)
                {
                    if ((tableLayoutPanel4.GetControlFromPosition(1, i) as UltraCombo).Value.IsNullOrEmpty())
                    {
                        MSG.Error("Chưa chọn loại tài nguyên");
                        return false;
                    }
                }
                for (int i = 0; i < tableLayoutPanel5.RowCount; i++)
                {
                    if ((tableLayoutPanel5.GetControlFromPosition(1, i) as UltraCombo).Value.IsNullOrEmpty())
                    {
                        MSG.Error("Chưa chọn loại tài nguyên");
                        return false;
                    }
                }
            }
            #region check lỗi sai tên tài nguyên
            for (int d = 0; d < tableLayoutPanel4.RowCount; d++)
            {
                var control100 = tableLayoutPanel4.GetControlFromPosition(1, d);
                UltraCombo uCombo100 = control100 as UltraCombo;
                if (uCombo100.Text != "")
                {
                    try
                    {
                        string t7 = uCombo100.Text.ToString();
                        lstMaterialGoodsResourceTaxGroup1 = (Utils.ListMaterialGoodsResourceTaxGroup.Where(n => n.MaterialGoodsResourceTaxGroupName == t7)).ToList();
                        if (lstMaterialGoodsResourceTaxGroup1.Count == 0)
                        {
                            MSG.Error("Sai tên tài nguyên");
                            uCombo100.Focus();
                            return false;
                        };


                    }

                    catch { }
                }
            }
            for (int d = 0; d < tableLayoutPanel5.RowCount; d++)
            {
                var control100 = tableLayoutPanel5.GetControlFromPosition(1, d);
                UltraCombo uCombo100 = control100 as UltraCombo;
                if (uCombo100.Text != "")
                {
                    try
                    {
                        string t7 = uCombo100.Text.ToString();
                        lstMaterialGoodsResourceTaxGroup1 = (Utils.ListMaterialGoodsResourceTaxGroup.Where(n => n.MaterialGoodsResourceTaxGroupName == t7)).ToList();
                        if (lstMaterialGoodsResourceTaxGroup1.Count == 0)
                        {
                            MSG.Error("Sai tên tài nguyên");
                            uCombo100.Focus();
                            return false;
                        };


                    }

                    catch { }
                }
            }
            #endregion
            return true;
        }
        public TM02TAIN ObjandGUI(TM02TAIN input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == null || input.ID == Guid.Empty)
                {
                    input.ID = Guid.NewGuid();
                }
                input.TypeID = 925;
                input.DeclarationName = "TỜ KHAI QUYẾT TOÁN THUẾ TÀI NGUYÊN (MẪU SỐ 02/TAIN)";
                input.DeclarationTerm = "Năm " + txtItem1.Text;
                input.IsFirstDeclaration = txtItem2.Checked;
                input.CompanyName = txtItem4.Text;
                input.CompanyTaxCode = txtItem5.Text;
                input.TaxAgencyTaxCode = txtItem13.Text;
                input.TaxAgencyName = txtItem12.Text;
                input.TaxAgencyEmployeeName = txtName.Text;
                input.CertificationNo = txtCCHN.Text;
                input.SignName = txtNameSign.Text;
                if (txtSignDate.Value != null)
                    input.SignDate = txtSignDate.DateTime;
                else
                    input.SignDate = null;
                if (!_isEdit)
                {
                    input.FromDate = FromDate;
                    input.ToDate = ToDate;
                }
                input.TM02TAINDetails.Clear();
                for (int i = 0; i < tableLayoutPanel4.RowCount; i++)
                {
                    TM02TAINDetail tM02TAINDetail = new TM02TAINDetail();
                    tM02TAINDetail.Type = 0;
                    tM02TAINDetail.TM02TAINID = input.ID;
                    tM02TAINDetail.OrderPriority = int.Parse(((UltraLabel)tableLayoutPanel4.GetControlFromPosition(0, i)).Text);
                    if ((tableLayoutPanel4.GetControlFromPosition(1, i) as UltraCombo).Value.IsNullOrEmpty()) continue;
                    tM02TAINDetail.MaterialGoodsResourceTaxGroupID = Guid.Parse((tableLayoutPanel4.GetControlFromPosition(1, i) as UltraCombo).Value.ToString());
                    MaterialGoodsResourceTaxGroup materialGoodsResourceTaxGroup = lstMaterialGoodsResourceTaxGroup.FirstOrDefault(n => n.ID == tM02TAINDetail.MaterialGoodsResourceTaxGroupID);
                    tM02TAINDetail.MaterialGoodsResourceTaxGroupName = materialGoodsResourceTaxGroup.MaterialGoodsResourceTaxGroupName;
                    tM02TAINDetail.Unit = (tableLayoutPanel4.GetControlFromPosition(2, i) as UltraTextEditor).Value.IsNullOrEmpty() ? "" : (tableLayoutPanel4.GetControlFromPosition(2, i) as UltraTextEditor).Value.ToString();
                    tM02TAINDetail.Quantity = getValueFromPosition(tableLayoutPanel4, 3, i);
                    tM02TAINDetail.UnitPrice = getValueFromPosition(tableLayoutPanel4, 4, i);
                    tM02TAINDetail.TaxRate = getValueFromPosition(tableLayoutPanel4, 5, i);
                    tM02TAINDetail.ResourceTaxTaxAmountUnit = getValueFromPosition(tableLayoutPanel4, 6, i);
                    tM02TAINDetail.ResourceTaxAmountIncurration = getValueFromPosition(tableLayoutPanel4, 7, i);
                    tM02TAINDetail.ResourceTaxAmountDeduction = getValueFromPosition(tableLayoutPanel4, 8, i);
                    tM02TAINDetail.ResourceTaxAmount = getValueFromPosition(tableLayoutPanel4, 9, i);
                    tM02TAINDetail.ResourceTaxAmountDeclaration = getValueFromPosition(tableLayoutPanel4, 10, i);
                    tM02TAINDetail.DifferAmount = getValueFromPosition(tableLayoutPanel4, 11, i);
                    input.TM02TAINDetails.Add(tM02TAINDetail);
                }
                for (int i = 0; i < tableLayoutPanel5.RowCount; i++)
                {
                    TM02TAINDetail tM02TAINDetail = new TM02TAINDetail();
                    tM02TAINDetail.Type = 1;
                    tM02TAINDetail.TM02TAINID = input.ID;
                    tM02TAINDetail.OrderPriority = int.Parse(((UltraLabel)tableLayoutPanel5.GetControlFromPosition(0, i)).Text);
                    if ((tableLayoutPanel5.GetControlFromPosition(1, i) as UltraCombo).Value.IsNullOrEmpty()) continue;
                    tM02TAINDetail.MaterialGoodsResourceTaxGroupID = Guid.Parse((tableLayoutPanel5.GetControlFromPosition(1, i) as UltraCombo).Value.ToString());
                    MaterialGoodsResourceTaxGroup materialGoodsResourceTaxGroup = lstMaterialGoodsResourceTaxGroup.FirstOrDefault(n => n.ID == tM02TAINDetail.MaterialGoodsResourceTaxGroupID);
                    tM02TAINDetail.MaterialGoodsResourceTaxGroupName = materialGoodsResourceTaxGroup.MaterialGoodsResourceTaxGroupName;
                    tM02TAINDetail.Unit = (tableLayoutPanel5.GetControlFromPosition(2, i) as UltraTextEditor).Value.IsNullOrEmpty() ? "" : (tableLayoutPanel5.GetControlFromPosition(2, i) as UltraTextEditor).Value.ToString();
                    tM02TAINDetail.Quantity = getValueFromPosition(tableLayoutPanel5, 3, i);
                    tM02TAINDetail.UnitPrice = getValueFromPosition(tableLayoutPanel5, 4, i);
                    tM02TAINDetail.TaxRate = getValueFromPosition(tableLayoutPanel5, 5, i);
                    tM02TAINDetail.ResourceTaxTaxAmountUnit = getValueFromPosition(tableLayoutPanel5, 6, i);
                    tM02TAINDetail.ResourceTaxAmountIncurration = getValueFromPosition(tableLayoutPanel5, 7, i);
                    tM02TAINDetail.ResourceTaxAmountDeduction = getValueFromPosition(tableLayoutPanel5, 8, i);
                    tM02TAINDetail.ResourceTaxAmount = getValueFromPosition(tableLayoutPanel5, 9, i);
                    tM02TAINDetail.ResourceTaxAmountDeclaration = getValueFromPosition(tableLayoutPanel5, 10, i);
                    tM02TAINDetail.DifferAmount = getValueFromPosition(tableLayoutPanel5, 11, i);
                    input.TM02TAINDetails.Add(tM02TAINDetail);
                }

            }
            else
            {
                try
                {
                    txtItem1.Text = input.DeclarationTerm.Split(' ')[1];
                    txtItem2.Checked = input.IsFirstDeclaration;
                    txtItem4.Text = input.CompanyName;
                    txtItem5.Text = input.CompanyTaxCode;
                    txtItem13.Text = input.TaxAgencyTaxCode;
                    txtItem12.Text = input.TaxAgencyName;
                    txtName.Text = input.TaxAgencyEmployeeName;
                    txtCCHN.Text = input.CertificationNo;
                    txtNameSign.Text = input.SignName;
                    txtSignDate.Value = input.SignDate;
                    if (input.AdditionTime != null)
                    {
                        txtItem3.Value = input.AdditionTime;
                    }
                    if (input.IsFirstDeclaration)
                    {
                        ultraTabControl1.Tabs[1].Visible = false;
                        utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Visible = false;
                    }
                    var lst1 = input.TM02TAINDetails.Where(n => n.Type == 0).OrderBy(n => n.OrderPriority).ToList();
                    var lst2 = input.TM02TAINDetails.Where(n => n.Type == 1).OrderBy(n => n.OrderPriority).ToList();
                    foreach (var item in lst1)
                    {
                        int index = lst1.IndexOf(item);
                        if (index == 0)
                        {
                            //AddControlToTable(tableLayoutPanel4, true);
                        }
                        else
                        {
                            AddControlToTable(tableLayoutPanel4, false, tableLayoutPanel4.RowCount - 1);
                        }
                        var con = tableLayoutPanel4.GetControlFromPosition(0, index);
                        ((UltraLabel)tableLayoutPanel4.GetControlFromPosition(0, index)).Text = item.OrderPriority.ToString();
                        ((UltraCombo)tableLayoutPanel4.GetControlFromPosition(1, index)).Value = item.MaterialGoodsResourceTaxGroupID;
                        (tableLayoutPanel4.GetControlFromPosition(2, index) as UltraTextEditor).Value = item.Unit;
                        BindData1(tableLayoutPanel4.GetControlFromPosition(3, index) as UltraMaskedEdit, item.Quantity);
                        BindData1(tableLayoutPanel4.GetControlFromPosition(4, index) as UltraMaskedEdit, item.UnitPrice);
                        BindData1(tableLayoutPanel4.GetControlFromPosition(5, index) as UltraMaskedEdit, item.TaxRate);
                        BindData1(tableLayoutPanel4.GetControlFromPosition(6, index) as UltraMaskedEdit, item.ResourceTaxTaxAmountUnit);
                        BindData(tableLayoutPanel4.GetControlFromPosition(8, index) as UltraMaskedEdit, item.ResourceTaxAmountDeduction);
                        BindData(tableLayoutPanel4.GetControlFromPosition(10, index) as UltraMaskedEdit, item.ResourceTaxAmountDeclaration);
                    }
                    foreach (var item in lst2)
                    {
                        int index = lst2.IndexOf(item);
                        if (index == 0)
                        {
                            //AddControlToTable(tableLayoutPanel5, true);
                        }
                        else
                        {
                            AddControlToTable(tableLayoutPanel5, false, tableLayoutPanel5.RowCount - 1);
                        }
                        ((UltraLabel)tableLayoutPanel5.GetControlFromPosition(0, index)).Text = item.OrderPriority.ToString();
                        ((UltraCombo)tableLayoutPanel5.GetControlFromPosition(1, index)).Value = item.MaterialGoodsResourceTaxGroupID;
                        (tableLayoutPanel5.GetControlFromPosition(2, index) as UltraTextEditor).Value = item.Unit;
                        BindData1(tableLayoutPanel5.GetControlFromPosition(3, index) as UltraMaskedEdit, item.Quantity);
                        BindData1(tableLayoutPanel5.GetControlFromPosition(4, index) as UltraMaskedEdit, item.UnitPrice);
                        BindData1(tableLayoutPanel5.GetControlFromPosition(5, index) as UltraMaskedEdit, item.TaxRate);
                        BindData1(tableLayoutPanel5.GetControlFromPosition(6, index) as UltraMaskedEdit, item.ResourceTaxTaxAmountUnit);
                        BindData(tableLayoutPanel5.GetControlFromPosition(8, index) as UltraMaskedEdit, item.ResourceTaxAmountDeduction);
                        BindData(tableLayoutPanel5.GetControlFromPosition(10, index) as UltraMaskedEdit, item.ResourceTaxAmountDeclaration);
                    }
                    tableLayoutPanel2.Location = new Point(tableLayoutPanel2.Location.X, tableLayoutPanel4.Location.Y + tableLayoutPanel4.Height);
                    tableLayoutPanel5.Location = new Point(tableLayoutPanel5.Location.X, tableLayoutPanel2.Location.Y + tableLayoutPanel2.Height);
                    tableLayoutPanel3.Location = new Point(tableLayoutPanel3.Location.X, tableLayoutPanel5.Location.Y + tableLayoutPanel5.Height);
                    ultraPanel2.Location = new Point(ultraPanel2.Location.X, tableLayoutPanel3.Location.Y + tableLayoutPanel3.Height + 10);

                }
                catch (Exception ex)
                {
                    MSG.Error(ex.Message);
                }


            }
            return input;
        }
        public List<string> lstReadOnlyDefaut()
        {
            List<string> lst = new List<string>();
            foreach (var item in GetAll(this, typeof(UltraMaskedEdit)))
            {
                try
                {
                    if (item is UltraMaskedEdit)
                    {
                        if ((item as UltraMaskedEdit).ReadOnly)
                        {
                            lst.Add((item as UltraMaskedEdit).Name);
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }
            foreach (var item in GetAll(this, typeof(UltraTextEditor)))
            {
                try
                {
                    if ((item as UltraTextEditor).ReadOnly)
                    {
                        lst.Add((item as UltraTextEditor).Name);
                    }
                }
                catch (Exception ex)
                {

                }
            }
            foreach (var item in GetAll(this, typeof(UltraCombo)))
            {
                try
                {
                    if ((item as UltraCombo).ReadOnly)
                    {
                        lst.Add((item as UltraCombo).Name);
                    }
                }
                catch (Exception ex)
                {

                }
            }
            foreach (var item in GetAll(this, typeof(UltraDateTimeEditor)))
            {
                try
                {
                    if ((item as UltraDateTimeEditor).ReadOnly)
                    {
                        lst.Add((item as UltraDateTimeEditor).Name);
                    }
                }
                catch (Exception ex)
                {

                }
            }
            return lst;
        }
        public void LoadReadOnly(bool isReadOnly)
        {
            utmDetailBaseToolBar.Tools["mnbtnDelete"].SharedProps.Enabled = isReadOnly;
            foreach (var item in GetAll(this, typeof(UltraMaskedEdit)))
            {
                try
                {
                    if (item is UltraMaskedEdit)
                    {
                        if (!lstReadOnly.Contains((item as UltraMaskedEdit).Name))
                        {
                            (item as UltraMaskedEdit).ReadOnly = isReadOnly;
                        }

                    }
                }
                catch (Exception ex)
                {

                }


            }
            for (int i = 0; i < tableLayoutPanel4.RowCount; i++)
            {
                (tableLayoutPanel4.GetControlFromPosition(5, i) as UltraMaskedEdit).ReadOnly = true;
                (tableLayoutPanel4.GetControlFromPosition(7, i) as UltraMaskedEdit).ReadOnly = true;
                (tableLayoutPanel4.GetControlFromPosition(9, i) as UltraMaskedEdit).ReadOnly = true;
                (tableLayoutPanel4.GetControlFromPosition(11, i) as UltraMaskedEdit).ReadOnly = true;
            }
            for (int i = 0; i < tableLayoutPanel5.RowCount; i++)
            {
                (tableLayoutPanel5.GetControlFromPosition(5, i) as UltraMaskedEdit).ReadOnly = true;
                (tableLayoutPanel5.GetControlFromPosition(7, i) as UltraMaskedEdit).ReadOnly = true;
                (tableLayoutPanel5.GetControlFromPosition(9, i) as UltraMaskedEdit).ReadOnly = true;
                (tableLayoutPanel5.GetControlFromPosition(11, i) as UltraMaskedEdit).ReadOnly = true;
            }
            foreach (var item in GetAll(this, typeof(UltraDateTimeEditor)))
            {
                try
                {
                    if (!lstReadOnly.Contains((item as UltraDateTimeEditor).Name))
                    {
                        (item as UltraDateTimeEditor).ReadOnly = isReadOnly;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            foreach (var item in GetAll(this, typeof(UltraTextEditor)))
            {
                try
                {
                    if (!lstReadOnly.Contains((item as UltraTextEditor).Name))
                    {
                        (item as UltraTextEditor).ReadOnly = isReadOnly;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            foreach (var item in GetAll(this, typeof(UltraCombo)))
            {
                try
                {
                    if (!lstReadOnly.Contains((item as UltraCombo).Name))
                    {
                        (item as UltraCombo).ReadOnly = isReadOnly;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            contextMenuStrip1.Enabled = !isReadOnly;
        }
        public IEnumerable<Control> GetAll(Control control, System.Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }
        private void ultraLabel137_Click(object sender, EventArgs e)
        {

        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            try
            {
                ToolStripMenuItem ts = (ToolStripMenuItem)sender;
                ContextMenuStrip cs = (ContextMenuStrip)ts.Owner;
                var t = GetControlFocus(tableLayoutPanel4);
                //int row = GetRow(tableLayoutPanel2);
                //MSG.Information(GetRow(tableLayoutPanel2).ToString());
                if (tableLayoutPanel4.Controls.Contains(cs.SourceControl.Parent) || tableLayoutPanel4.Controls.Contains(cs.SourceControl))
                {
                    AddControlToTable(tableLayoutPanel4);
                    tableLayoutPanel2.Location = new Point(tableLayoutPanel2.Location.X, tableLayoutPanel4.Location.Y + tableLayoutPanel4.Height);
                    tableLayoutPanel5.Location = new Point(tableLayoutPanel5.Location.X, tableLayoutPanel2.Location.Y + tableLayoutPanel2.Height);
                    tableLayoutPanel3.Location = new Point(tableLayoutPanel3.Location.X, tableLayoutPanel5.Location.Y + tableLayoutPanel5.Height);
                    ultraPanel2.Location = new Point(ultraPanel2.Location.X, tableLayoutPanel3.Location.Y + tableLayoutPanel3.Height + 10);
                }
                else if (tableLayoutPanel5.Controls.Contains(cs.SourceControl.Parent) || tableLayoutPanel5.Controls.Contains(cs.SourceControl))
                {
                    AddControlToTable(tableLayoutPanel5);
                    tableLayoutPanel3.Location = new Point(tableLayoutPanel3.Location.X, tableLayoutPanel5.Location.Y + tableLayoutPanel5.Height);
                    ultraPanel2.Location = new Point(ultraPanel2.Location.X, tableLayoutPanel3.Location.Y + tableLayoutPanel3.Height + 10);
                }
                else if (cs.SourceControl.Name.Equals("ultraGrid1"))
                {
                    //ultraGrid1.Size = new Size(ultraGrid1.Size.Width, ultraGrid1.Size.Height + ultraGrid1.DisplayLayout.Override.MinRowHeight);
                    //ultraPanel1.Height = ultraGrid1.Height + 2;
                    ////uGrid1.AddNewRow4Grid();
                    //ultraGrid1.DisplayLayout.Bands[0].AddNew();

                    //ultraGrid1.Rows[ultraGrid1.Rows.Count - 1].Update();
                    ////uGrid1.Dock = DockStyle.Fill;
                    //ultraGrid1.Refresh();
                    //ultraGrid1.ActiveRow = ultraGrid1.Rows[0];
                    //ultraGrid1.ActiveRow = ultraGrid1.Rows[ultraGrid1.Rows.Count - 1];


                    //ultraLabel87.Location = new Point(ultraLabel87.Location.X, ultraLabel87.Location.Y + 29);
                    //ultraPanel2.Location = new Point(ultraPanel2.Location.X, ultraPanel2.Location.Y + 29);
                    //ultraPanel20.Location = new Point(ultraPanel20.Location.X, ultraPanel20.Location.Y + 29);
                    //ultraLabel88.Location = new Point(ultraLabel88.Location.X, ultraLabel88.Location.Y + 29);
                    //ultraPanel21.Location = new Point(ultraPanel21.Location.X, ultraPanel21.Location.Y + 29);
                }

            }
            catch (Exception ex)
            {

            }
        }
        public void AddControlToTable(TableLayoutPanel tableLayoutPanel, bool first = false, int? row_ = null)
        {
            int row = GetRow(tableLayoutPanel);
            if (row_ != null)
            {
                row = row_ ?? 0;
            }
            if (!first)
            {
                RowStyle temp = tableLayoutPanel.RowStyles[tableLayoutPanel.RowCount - 1];
                RowStyle rowStyle = new RowStyle(temp.SizeType, temp.Height);
                tableLayoutPanel.RowStyles.Insert(GetRow(tableLayoutPanel), rowStyle);
                tableLayoutPanel.RowCount++;

                foreach (Control ExistControl in tableLayoutPanel.Controls)
                {
                    if (tableLayoutPanel.GetRow(ExistControl) > row)
                        tableLayoutPanel.SetRow(ExistControl, tableLayoutPanel.GetRow(ExistControl) + 1);
                }
            }
            else
            {
                row = -1;
            }

            UltraLabel txtSTT = new UltraLabel();
            txtSTT.Text = "1";
            txtSTT.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
            txtSTT.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
            txtSTT.AutoSize = false;
            txtSTT.Dock = DockStyle.Fill;
            txtSTT.Margin = new Padding(0);

            UltraCombo cbbName = new UltraCombo();
            cbbName.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
            cbbName.Dock = DockStyle.Fill;
            cbbName.AutoSize = false;
            cbbName.Margin = new Padding(0);
            ConfigCbb2(cbbName);

            //UltraCombo cbbName2 = new UltraCombo();
            //cbbName2.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
            //cbbName2.Dock = DockStyle.Fill;
            //cbbName2.AutoSize = false;
            //cbbName2.Margin = new Padding(0);
            //ConfigCbb2(cbbName2);

            UltraTextEditor CT3 = new UltraTextEditor();
            CT3.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            CT3.AutoSize = false;
            CT3.Dock = DockStyle.Fill;
            CT3.Margin = new Padding(0);
            CT3.ContextMenuStrip = contextMenuStrip1;
            //ConfigMaskedEdit(ultraMaskedEdit2);

            UltraMaskedEdit CT4 = new UltraMaskedEdit();
            CT4.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            CT4.Value = 0;
            ConfigMaskedEdit1(CT4);
            CT4.FormatNumberic(ConstDatabase.Format_Quantity);

            UltraMaskedEdit CT5 = new UltraMaskedEdit();
            CT5.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            CT5.Value = 0;
            ConfigMaskedEdit1(CT5);
            CT4.FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);

            UltraMaskedEdit CT6 = new UltraMaskedEdit();
            CT6.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            CT6.ReadOnly = true;
            CT6.Value = 0;
            ConfigMaskedEdit1(CT6);

            UltraMaskedEdit CT7 = new UltraMaskedEdit();
            CT7.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            CT7.Value = 0;
            ConfigMaskedEdit1(CT7);

            UltraMaskedEdit CT8 = new UltraMaskedEdit();
            CT8.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            CT8.SelectedTextForeColor = Color.Blue;
            CT8.Appearance.BackColorAlpha = Alpha.Transparent;
            CT8.ReadOnly = true;
            ConfigMaskedEdit(CT8);

            UltraMaskedEdit CT9 = new UltraMaskedEdit();
            CT9.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            CT9.Value = 0;
            ConfigMaskedEdit(CT9);

            UltraMaskedEdit CT10 = new UltraMaskedEdit();
            CT10.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            CT10.Appearance.BackColorAlpha = Alpha.Transparent;
            CT10.SelectedTextForeColor = Color.Blue;
            CT10.ReadOnly = true;
            ConfigMaskedEdit(CT10);

            UltraMaskedEdit CT11 = new UltraMaskedEdit();
            CT11.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            ConfigMaskedEdit(CT11);

            UltraMaskedEdit CT12 = new UltraMaskedEdit();
            CT12.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            CT12.Appearance.BackColorAlpha = Alpha.Transparent;
            CT12.SelectedTextForeColor = Color.Blue;
            CT12.ReadOnly = true;
            ConfigMaskedEdit(CT12);

            tableLayoutPanel.Controls.Add(txtSTT, 0, row + 1);
            tableLayoutPanel.Controls.Add(cbbName, 1, row + 1);
            tableLayoutPanel.Controls.Add(CT3, 2, row + 1);
            tableLayoutPanel.Controls.Add(CT4, 3, row + 1);
            tableLayoutPanel.Controls.Add(CT5, 4, row + 1);
            tableLayoutPanel.Controls.Add(CT6, 5, row + 1);
            tableLayoutPanel.Controls.Add(CT7, 6, row + 1);
            tableLayoutPanel.Controls.Add(CT8, 7, row + 1);
            tableLayoutPanel.Controls.Add(CT9, 8, row + 1);
            tableLayoutPanel.Controls.Add(CT10, 9, row + 1);
            tableLayoutPanel.Controls.Add(CT11, 10, row + 1);
            tableLayoutPanel.Controls.Add(CT12, 11, row + 1);
            #region Event
            cbbName.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler((s, a) => cbb_RowSelected(s, a, CT3, CT6));
            CT4.ValueChanged += new System.EventHandler((s, a) => CT4_ValueChanged(s, a, CT4, CT5, CT6, CT7, CT8, CT9, CT10, CT11, CT12));
            CT5.ValueChanged += new System.EventHandler((s, a) => CT4_ValueChanged(s, a, CT4, CT5, CT6, CT7, CT8, CT9, CT10, CT11, CT12));
            CT6.ValueChanged += new System.EventHandler((s, a) => CT4_ValueChanged(s, a, CT4, CT5, CT6, CT7, CT8, CT9, CT10, CT11, CT12));
            CT7.ValueChanged += new System.EventHandler((s, a) => CT4_ValueChanged(s, a, CT4, CT5, CT6, CT7, CT8, CT9, CT10, CT11, CT12));
            CT8.ValueChanged += new System.EventHandler((s, a) => CT4_ValueChanged(s, a, CT4, CT5, CT6, CT7, CT8, CT9, CT10, CT11, CT12));
            CT9.ValueChanged += new System.EventHandler((s, a) => CT4_ValueChanged(s, a, CT4, CT5, CT6, CT7, CT8, CT9, CT10, CT11, CT12));
            CT10.ValueChanged += new System.EventHandler((s, a) => CT4_ValueChanged(s, a, CT4, CT5, CT6, CT7, CT8, CT9, CT10, CT11, CT12));
            CT11.ValueChanged += new System.EventHandler((s, a) => CT4_ValueChanged(s, a, CT4, CT5, CT6, CT7, CT8, CT9, CT10, CT11, CT12));
            CT12.ValueChanged += new System.EventHandler((s, a) => CT4_ValueChanged(s, a, CT4, CT5, CT6, CT7, CT8, CT9, CT10, CT11, CT12));

            //txtTax.KeyPress += new KeyPressEventHandler(uMaskedEdit_KeyPress);
            #endregion

            for (int i = 0; i < tableLayoutPanel.RowCount; i++)
            {
                var control = tableLayoutPanel.GetControlFromPosition(0, i);
                if (control is UltraLabel)
                {
                    UltraLabel ultraTextEditor = control as UltraLabel;
                    ultraTextEditor.Text = (i + 1).ToString();
                }
            }
        }
        private void uMaskedEdit_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(sender is UltraMaskedEdit)) return;
            UltraMaskedEdit ultraMaskedEdit = (UltraMaskedEdit)sender;
            //ultraMaskedEdit.Text = ultraMaskedEdit.Value.ToString().Trim(' ', '_');
            if (e.KeyChar == '-')
            {
                if (ultraMaskedEdit.InputMask == "n,nnn,nnn,nnn,nnn,nnn")
                {
                    ultraMaskedEdit.InputMask = "(n,nnn,nnn,nnn,nnn,nnn)";
                }
                else
                {
                    ultraMaskedEdit.InputMask = "n,nnn,nnn,nnn,nnn,nnn";
                }
            }

        }
        public bool BindData(UltraMaskedEdit ultraMaskedEdit, decimal r)
        {
            try
            {
                if (r < 0)
                {
                    ultraMaskedEdit.InputMask = "(n,nnn,nnn,nnn,nnn,nnn)";
                    if (ultraMaskedEdit.Appearance.FontData.Bold == DefaultableBoolean.True)
                        ultraMaskedEdit.FormatString = "(###,###,###,###,###,##0)";
                    ultraMaskedEdit.Value = -Math.Round(r, MidpointRounding.AwayFromZero);
                }
                else
                {
                    ultraMaskedEdit.InputMask = "n,nnn,nnn,nnn,nnn,nnn";
                    if (ultraMaskedEdit.Appearance.FontData.Bold == DefaultableBoolean.True)
                        ultraMaskedEdit.FormatString = "###,###,###,###,###,##0";
                    ultraMaskedEdit.Value = Math.Round(r, MidpointRounding.AwayFromZero);
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        public bool BindData1(UltraMaskedEdit ultraMaskedEdit, decimal r)
        {
            try
            {
                if (r < 0)
                {
                    ultraMaskedEdit.InputMask = "(n,nnn,nnn,nnn,nnn,nnn.nn)";
                    if (ultraMaskedEdit.Appearance.FontData.Bold == DefaultableBoolean.True)
                        ultraMaskedEdit.FormatString = "(###,###,###,###,###,##0.00)";
                    ultraMaskedEdit.Value = -Math.Round(r, MidpointRounding.AwayFromZero);
                }
                else
                {
                    //ultraMaskedEdit.InputMask = "n,nnn,nnn,nnn,nnn,nnn.nn";
                    //if (ultraMaskedEdit.Appearance.FontData.Bold == DefaultableBoolean.True)
                    //    ultraMaskedEdit.FormatString = "###,###,###,###,###,##0.00";
                    ultraMaskedEdit.Value = Math.Round(r, MidpointRounding.AwayFromZero);
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        private void CT4_ValueChanged(object sender, EventArgs e, UltraMaskedEdit CT4, UltraMaskedEdit CT5, UltraMaskedEdit CT6,
            UltraMaskedEdit CT7, UltraMaskedEdit CT8, UltraMaskedEdit CT9, UltraMaskedEdit CT10, UltraMaskedEdit CT11, UltraMaskedEdit CT12)
        {
            UltraMaskedEdit ultraMaskedEdit = (UltraMaskedEdit)sender;
            if (ultraMaskedEdit == CT4)
            {
                try
                {
                    if (getValueMaskedEdit(CT5) != 0)
                    {
                        decimal result = getValueMaskedEdit(CT4) * getValueMaskedEdit(CT5) * getValueMaskedEdit(CT6) / 100;
                        BindData(CT8, result);
                    }
                    else
                    {
                        decimal result = getValueMaskedEdit(CT4) * getValueMaskedEdit(CT7);
                        BindData(CT8, result);
                    }

                }
                catch (Exception ex)
                {

                }
            }
            else if (ultraMaskedEdit == CT5)
            {
                try
                {
                    if (getValueMaskedEdit(CT5) != 0)
                    {
                        CT7.Value = 0;
                        decimal result = getValueMaskedEdit(CT4) * getValueMaskedEdit(CT5) * getValueMaskedEdit(CT6) / 100;
                        BindData(CT8, result);
                    }
                }
                catch (Exception ex)
                {

                }
            }
            else if (ultraMaskedEdit == CT6)
            {
                try
                {
                    if (getValueMaskedEdit(CT5) != 0)
                    {
                        decimal result = getValueMaskedEdit(CT4) * getValueMaskedEdit(CT5) * getValueMaskedEdit(CT6) / 100;
                        BindData(CT8, result);
                    }
                    else
                    {
                        decimal result = getValueMaskedEdit(CT4) * getValueMaskedEdit(CT7);
                        BindData(CT8, result);
                    }
                }
                catch (Exception ex)
                {

                }
            }
            else if (ultraMaskedEdit == CT7)
            {
                try
                {
                    if (getValueMaskedEdit(CT7) != 0)
                    {
                        decimal result = getValueMaskedEdit(CT4) * getValueMaskedEdit(CT7);
                        BindData(CT8, result);
                        CT5.Value = 0;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            else if (ultraMaskedEdit == CT8 || ultraMaskedEdit == CT9)
            {
                try
                {
                    decimal result = getValueMaskedEdit(CT8) - getValueMaskedEdit(CT9);
                    BindData(CT10, result);
                    if (ultraMaskedEdit == CT8)
                    {
                        decimal sum = 0;
                        for (int i = 0; i < tableLayoutPanel4.RowCount; i++)
                        {
                            sum += getValueMaskedEdit((UltraMaskedEdit)tableLayoutPanel4.GetControlFromPosition(7, i));
                        }
                        for (int i = 0; i < tableLayoutPanel5.RowCount; i++)
                        {
                            sum += getValueMaskedEdit((UltraMaskedEdit)tableLayoutPanel5.GetControlFromPosition(7, i));
                        }
                        BindData(txtSum8, sum);
                    }
                    else
                    {
                        decimal sum = 0;
                        for (int i = 0; i < tableLayoutPanel4.RowCount; i++)
                        {
                            sum += getValueMaskedEdit((UltraMaskedEdit)tableLayoutPanel4.GetControlFromPosition(8, i));
                        }
                        for (int i = 0; i < tableLayoutPanel5.RowCount; i++)
                        {
                            sum += getValueMaskedEdit((UltraMaskedEdit)tableLayoutPanel5.GetControlFromPosition(8, i));
                        }
                        BindData(txtSum9, sum);
                    }

                }
                catch (Exception ex)
                {

                }
            }
            else if (ultraMaskedEdit == CT10 || ultraMaskedEdit == CT11)
            {
                try
                {
                    decimal result = getValueMaskedEdit(CT10) - getValueMaskedEdit(CT11);
                    BindData(CT12, result);
                    if (ultraMaskedEdit == CT10)
                    {
                        decimal sum = 0;
                        for (int i = 0; i < tableLayoutPanel4.RowCount; i++)
                        {
                            sum += getValueMaskedEdit((UltraMaskedEdit)tableLayoutPanel4.GetControlFromPosition(9, i));
                        }
                        for (int i = 0; i < tableLayoutPanel5.RowCount; i++)
                        {
                            sum += getValueMaskedEdit((UltraMaskedEdit)tableLayoutPanel5.GetControlFromPosition(9, i));
                        }
                        BindData(txtSum10, sum);
                    }
                    else
                    {
                        decimal sum = 0;
                        for (int i = 0; i < tableLayoutPanel4.RowCount; i++)
                        {
                            sum += getValueMaskedEdit((UltraMaskedEdit)tableLayoutPanel4.GetControlFromPosition(10, i));
                        }
                        for (int i = 0; i < tableLayoutPanel5.RowCount; i++)
                        {
                            sum += getValueMaskedEdit((UltraMaskedEdit)tableLayoutPanel5.GetControlFromPosition(10, i));
                        }
                        BindData(txtSum11, sum);
                    }

                }
                catch (Exception ex)
                {

                }
            }
            else if (ultraMaskedEdit == CT12)
            {
                try
                {
                    decimal sum = 0;
                    for (int i = 0; i < tableLayoutPanel4.RowCount; i++)
                    {
                        sum += getValueMaskedEdit((UltraMaskedEdit)tableLayoutPanel4.GetControlFromPosition(11, i));
                    }
                    for (int i = 0; i < tableLayoutPanel5.RowCount; i++)
                    {
                        sum += getValueMaskedEdit((UltraMaskedEdit)tableLayoutPanel5.GetControlFromPosition(11, i));
                    }
                    BindData(txtSum12, sum);
                }
                catch (Exception ex)
                {

                }
            }

        }
        private void ultraMaskedEdit8_ValueChanged(object sender, EventArgs e, UltraMaskedEdit ultraMaskedEdit8, UltraMaskedEdit ultraMaskedEdit9, UltraMaskedEdit ultraMaskedEdit10)
        {
            if (ultraMaskedEdit8.Value.IsNullOrEmpty())
            {
                ultraMaskedEdit8.Value = 0;
            }
            if (ultraMaskedEdit9.Value.IsNullOrEmpty())
            {
                ultraMaskedEdit9.Value = 0;
            }
            decimal m = getValueMaskedEdit(ultraMaskedEdit8) - getValueMaskedEdit(ultraMaskedEdit9);
            if (m < 0)
            {
                ultraMaskedEdit10.InputMask = "(n,nnn,nnn,nnn,nnn,nnn)";
                ultraMaskedEdit10.Value = -m;
            }
            else
            {
                ultraMaskedEdit10.InputMask = "n,nnn,nnn,nnn,nnn,nnn";
                ultraMaskedEdit10.Value = m;
            }
        }
        private void ultraMaskedEdit10_ValueChanged(object sender, EventArgs e, UltraMaskedEdit ultraMaskedEdit8, UltraMaskedEdit ultraMaskedEdit9, UltraMaskedEdit ultraMaskedEdit10)
        {
            //if (ultraMaskedEdit10.Value.IsNullOrEmpty())
            //{
            //    ultraMaskedEdit10.Value = 0;
            //}
            //if (ultraMaskedEdit11.Value.IsNullOrEmpty())
            //{
            //    ultraMaskedEdit11.Value = 0;
            //}
            //decimal m = getValueMaskedEdit(ultraMaskedEdit10) - getValueMaskedEdit(ultraMaskedEdit11);
            //if (m < 0)
            //{
            //    ultraMaskedEdit12.InputMask = "(n,nnn,nnn,nnn,nnn,nnn)";
            //    ultraMaskedEdit12.Value = -m;
            //}
            //else
            //{
            //    ultraMaskedEdit12.InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            //    ultraMaskedEdit12.Value = m;
            //}
        }

        private void cbbName_TextChanged(object sender, EventArgs e, UltraTextEditor txtUnit, UltraMaskedEdit txtTaxRate)
        {
            var cbb = (UltraCombo)sender;
            if (cbb.Text == "")
            {
                txtUnit.Text = "";
                txtTaxRate.FormatNumberic(txtTaxRate.Value.ToString(), ConstDatabase.Format_Quantity);
            }
        }
        private void tsmDelete_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem t = (ToolStripMenuItem)sender;
            ContextMenuStrip s = (ContextMenuStrip)t.Owner;
            string a = s.SourceControl.Name;
            //int row = GetRow(tableLayoutPanel2);
            //MSG.Information(GetRow(tableLayoutPanel2).ToString());
            if (tableLayoutPanel4.Controls.Contains(s.SourceControl.Parent) || tableLayoutPanel4.Controls.Contains(s.SourceControl))
            {
                int row = GetRow(tableLayoutPanel4);
                int r = tableLayoutPanel4.RowCount - 1;
                if (r == 0)
                {
                    for (int i = 1; i < tableLayoutPanel4.ColumnCount; i++)
                    {
                        var control = tableLayoutPanel4.GetControlFromPosition(i, 0);
                        if (control is UltraCombo)
                        {
                            (control as UltraCombo).Value = null;
                        }
                        if (control is UltraTextEditor)
                        {
                            (control as UltraTextEditor).Value = null;
                        }
                        if (control is UltraMaskedEdit)
                        {
                            if (i == 7 || i == 9 || i == 11)
                            {
                                BindData(control as UltraMaskedEdit, 0);
                            }
                            else
                            {
                                (control as UltraMaskedEdit).Value = 0;
                            }
                        }
                    }
                    return;
                }
                remove_row(tableLayoutPanel4, row);
                for (int i = 0; i < tableLayoutPanel4.RowCount; i++)
                {
                    var control = tableLayoutPanel4.GetControlFromPosition(0, i);
                    if (control is UltraLabel)
                    {
                        UltraLabel ultraTextEditor = control as UltraLabel;
                        ultraTextEditor.Text = (i + 1).ToString();
                    }
                }
                tableLayoutPanel2.Location = new Point(tableLayoutPanel2.Location.X, tableLayoutPanel4.Location.Y + tableLayoutPanel4.Height);
                tableLayoutPanel5.Location = new Point(tableLayoutPanel5.Location.X, tableLayoutPanel2.Location.Y + tableLayoutPanel2.Height);
                tableLayoutPanel3.Location = new Point(tableLayoutPanel3.Location.X, tableLayoutPanel5.Location.Y + tableLayoutPanel5.Height);
                ultraPanel2.Location = new Point(ultraPanel2.Location.X, tableLayoutPanel3.Location.Y + tableLayoutPanel3.Height + 10);
            }
            else if (tableLayoutPanel5.Controls.Contains(s.SourceControl.Parent) || tableLayoutPanel5.Controls.Contains(s.SourceControl))
            {
                int row = GetRow(tableLayoutPanel5);
                int r = tableLayoutPanel5.RowCount - 1;
                if (r == 0)
                {
                    for (int i = 1; i < tableLayoutPanel5.ColumnCount; i++)
                    {
                        var control = tableLayoutPanel5.GetControlFromPosition(i, 0);
                        if (control is UltraCombo)
                        {
                            (control as UltraCombo).Value = null;
                        }
                        if (control is UltraTextEditor)
                        {
                            (control as UltraTextEditor).Value = null;
                        }
                        if (control is UltraMaskedEdit)
                        {
                            if (i == 7 || i == 9 || i == 11)
                            {
                                BindData(control as UltraMaskedEdit, 0);
                            }
                            else
                            {
                                (control as UltraMaskedEdit).Value = 0;
                            }
                        }
                    }
                    return;
                }
                remove_row(tableLayoutPanel5, row);
                for (int i = 0; i < tableLayoutPanel5.RowCount; i++)
                {
                    var control = tableLayoutPanel5.GetControlFromPosition(0, i);
                    if (control is UltraLabel)
                    {
                        UltraLabel ultraTextEditor = control as UltraLabel;
                        ultraTextEditor.Text = (i + 1).ToString();
                    }
                }
                tableLayoutPanel3.Location = new Point(tableLayoutPanel3.Location.X, tableLayoutPanel5.Location.Y + tableLayoutPanel5.Height);
                ultraPanel2.Location = new Point(ultraPanel2.Location.X, tableLayoutPanel3.Location.Y + tableLayoutPanel3.Height + 10);
            }
        }
        public void remove_row(TableLayoutPanel panel, int row_index_to_remove)
        {
            if (row_index_to_remove >= panel.RowCount)
            {
                return;
            }

            // delete all controls of row that we want to delete
            for (int i = 0; i < panel.ColumnCount; i++)
            {
                var control = panel.GetControlFromPosition(i, row_index_to_remove);
                panel.Controls.Remove(control);
            }

            // move up row controls that comes after row we want to remove
            for (int i = row_index_to_remove + 1; i < panel.RowCount; i++)
            {
                for (int j = 0; j < panel.ColumnCount; j++)
                {
                    var control = panel.GetControlFromPosition(j, i);
                    if (control != null)
                    {
                        panel.SetRow(control, i - 1);
                    }
                }
            }

            // remove last row
            //panel.RowStyles.RemoveAt(panel.RowCount - 1);
            panel.RowCount--;
        }
        private void ConfigMaskedEdit(UltraMaskedEdit txt)
        {
            txt.AutoSize = false;
            txt.Dock = DockStyle.Fill;
            txt.Margin = new Padding(0);
            txt.PromptChar = ' ';
            txt.DisplayMode = MaskMode.IncludeLiterals;
            txt.EditAs = EditAsType.Double;
            txt.Nullable = true;
            txt.NullText = "0";
            txt.InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            txt.ContextMenuStrip = contextMenuStrip1;

        }
        private void ConfigMaskedEdit1(UltraMaskedEdit txt)
        {
            txt.AutoSize = false;
            txt.Dock = DockStyle.Fill;
            txt.Margin = new Padding(0);
            txt.PromptChar = ' ';
            txt.DisplayMode = MaskMode.IncludeLiterals;
            txt.EditAs = EditAsType.Double;
            txt.Nullable = true;
            txt.NullText = "0";
            txt.InputMask = "n,nnn,nnn,nnn,nnn,nnn.nn";
            txt.ContextMenuStrip = contextMenuStrip1;

        }
        public void ConfigCbb(UltraGridComboColumn ultraGridComboColumn)
        {
            try
            {
                lstMaterialGoodsResourceTaxGroup = Utils.ListMaterialGoodsResourceTaxGroup.Where(n => n.TaxRate != null).OrderBy(n => n.MaterialGoodsResourceTaxGroupCode).ToList();
                //cbb.DataSource = lstMaterialGoodsResourceTaxGroup;
                //cbb.DisplayMember = "MaterialGoodsResourceTaxGroupName";
                //cbb.ValueMember = "ID";
                //Utils.ConfigGrid(cbb, ConstDatabase.MaterialGoodsResourceTaxGroup_TableName);
                ////Column2.DataSource = lstMaterialGoodsResourceTaxGroup;
                ultraGridComboColumn.DataSource = lstMaterialGoodsResourceTaxGroup;
                ultraGridComboColumn.ValueMember = "ID";
                ultraGridComboColumn.DisplayMember = "MaterialGoodsResourceTaxGroupName";
                List<string> lstHeader = new List<string>()
                {
                    "MaterialGoodsResourceTaxGroupCode","MaterialGoodsResourceTaxGroupName"
                };
                ultraGridComboColumn.DisplayLayout.Bands[0].Columns["MaterialGoodsResourceTaxGroupCode"].Header.Caption = "Mã tài nguyên";
                ultraGridComboColumn.DisplayLayout.Bands[0].Columns["MaterialGoodsResourceTaxGroupName"].Header.Caption = "Tên tài nguyên";
                ultraGridComboColumn.DisplayLayout.ScrollBounds = ScrollBounds.ScrollToFill;
                //ultraGridComboColumn.DisplayLayout.Bands[0].Columns["MaterialGoodsResourceTaxGroupCode"].MinWidth = ultraGridComboColumn.Width / 2;
                //ultraGridComboColumn.DisplayLayout.Bands[0].Columns["MaterialGoodsResourceTaxGroupName"].MinWidth = ultraGridComboColumn.Width / 2;
                ultraGridComboColumn.DisplayLayout.Override.ActiveRowAppearance.ForeColor = Color.Black;
                ultraGridComboColumn.DisplayLayout.BorderStyle = UIElementBorderStyle.None;
                foreach (var item in ultraGridComboColumn.DisplayLayout.Bands[0].Columns)
                {
                    if (!lstHeader.Contains(item.Key))
                    {
                        item.Hidden = true;
                    }
                }
                //Column2.DisplayLayout.Bands[0].Header
                //if (!ConstDatabase.TablesInDatabase.Any(x => x.Key.Equals(nameTable))) return;
                //foreach (var item in Column2.DisplayLayout.)
                //{
                //    if (ConstDatabase.TablesInDatabase[nameTable].ContainsKey(item.Key))
                //    {
                //        ultraCombo.DisplayLayout.ScrollBounds = ScrollBounds.ScrollToFill;
                //        TemplateColumn temp = ConstDatabase.TablesInDatabase[nameTable][item.Key];
                //        if (string.IsNullOrEmpty(headerCaptionFirst) && temp.IsVisible) headerCaptionFirst = item.Key;   //Lấy dòng Header Caption đầu tiên
                //        item.Header.Caption = temp.ColumnCaption;
                //        item.Header.ToolTipText = temp.ColumnToolTip;
                //        item.CellActivation = temp.IsReadOnly ? Activation.NoEdit : Activation.AllowEdit;
                //        //Set màu header
                //        item.Header.Appearance.BorderAlpha = Alpha.Transparent;
                //        item.Header.Appearance.TextHAlign = HAlign.Left;
                //        item.Width = temp.ColumnWidth == -1 ? item.Width : temp.ColumnWidth;
                //        item.MaxWidth = temp.ColumnMaxWidth == -1 ? item.MaxWidth : temp.ColumnMaxWidth;
                //        item.MinWidth = temp.ColumnMinWidth == -1 ? item.MinWidth : temp.ColumnMinWidth;
                //        item.Hidden = !temp.IsVisibleCbb;
                //        item.Header.VisiblePosition = temp.VisiblePosition == -1 ? item.Header.VisiblePosition : temp.VisiblePosition;
                //        if (item.Hidden == false && item.Key.Contains("Quantity"))
                //            FormatNumberic(item, ConstDatabase.Format_Quantity);
                //        if (item.Key.Contains("Date"))
                //        {
                //            item.Format = string.Format("dd/MM/yyyy", item);
                //        }
                //    }
                //    else
                //    {
                //        item.Hidden = true;
                //    }
                //}
            }
            catch (Exception ex)
            {

            }

        }
        public void ConfigCbb2(UltraCombo cbb)
        {
            try
            {
                lstMaterialGoodsResourceTaxGroup = Utils.ListMaterialGoodsResourceTaxGroup.Where(n => n.TaxRate != null).OrderBy(n => n.MaterialGoodsResourceTaxGroupCode).ToList();
                cbb.DataSource = lstMaterialGoodsResourceTaxGroup;
                cbb.DisplayMember = "MaterialGoodsResourceTaxGroupName";
                cbb.ValueMember = "ID";
                Utils.ConfigGrid(cbb, ConstDatabase.MaterialGoodsResourceTaxGroup_TableName);
                cbb.ContextMenuStrip = contextMenuStrip1;
                //cbb.DisplayLayout.Override.RowAlternateAppearance
                //lstMaterialGoodsResourceTaxGroup = Utils.ListMaterialGoodsResourceTaxGroup.Where(n => !n.Unit.IsNullOrEmpty()).ToList();
                //if (lstMaterialGoodsResourceTaxGroup.Count > 0)
                //{
                //    List<string> lst = lstMaterialGoodsResourceTaxGroup.Select(n => n.Unit).ToList();
                //    cbb.DataSource = lst;
                //    cbb.DisplayLayout.Bands[0].Columns[0].Header.Caption = "Đơn vị tính";
                //    //cbb.DisplayMember = "Unit";
                //}
            }
            catch (Exception ex)
            {

            }

        }
        public Point? GetIndex(TableLayoutPanel tlp, Point point)
        {
            // Method adapted from: stackoverflow.com/a/15449969
            if (point.X > tlp.Width || point.Y > tlp.Height)
                return null;

            int w = 0, h = 0;
            int[] widths = tlp.GetColumnWidths(), heights = tlp.GetRowHeights();

            int i;
            for (i = 0; i < widths.Length && point.X > w; i++)
            {
                w += widths[i];
            }
            int col = i - 1;

            for (i = 0; i < heights.Length && point.Y + tlp.VerticalScroll.Value > h; i++)
            {
                h += heights[i];
            }
            int row = i - 1;

            return new Point(col, row);
        }

        private void tableLayoutPanel2_MouseClick(object sender, MouseEventArgs e)
        {
            int row = 0;
            int verticalOffset = 0;
            Control control = FindFocusedControl(tableLayoutPanel1);
            foreach (int h in tableLayoutPanel1.GetRowHeights())
            {
                int column = 0;
                int horizontalOffset = 0;
                foreach (int w in tableLayoutPanel1.GetColumnWidths())
                {
                    Rectangle rectangle = new Rectangle(horizontalOffset, verticalOffset, w, h);
                    if (rectangle.Contains(e.Location))
                    {
                        MSG.Information(String.Format("row {0}, column {1} was clicked", row, column));
                        return;
                    }
                    horizontalOffset += w;
                    column++;
                }
                verticalOffset += h;
                row++;
            }

        }
        public int GetRow(TableLayoutPanel tableLayoutPanel)
        {
            int OrderPriority = 1;
            for (int t = 1; t < tableLayoutPanel.RowCount; t++)
            {
                for (int i = 0; i < tableLayoutPanel.ColumnCount; i++)
                {
                    var control = tableLayoutPanel.GetControlFromPosition(i, t);
                    if (control != null)
                        if (control.Focused)
                        {
                            return t;
                        }
                }
            }
            return 0;
        }
        public Control GetControlFocus(TableLayoutPanel tableLayoutPanel)
        {
            int OrderPriority = 1;
            for (int t = 1; t < tableLayoutPanel.RowCount; t++)
            {
                for (int i = 0; i < tableLayoutPanel.ColumnCount; i++)
                {
                    var control = tableLayoutPanel.GetControlFromPosition(i, t);
                    if (control != null)
                        if (control.Focused) return control;
                }
            }
            return null;
        }
        public Control FindFocusedControl(Control control)
        {
            var container = control as IContainerControl;
            while (container != null)
            {
                control = container.ActiveControl;
                container = control as IContainerControl;
            }
            return control;
        }
        private void utmDetailBaseToolBar_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
        {
            switch (e.Tool.Key)
            {
                case "mnbtnBack":
                    break;

                case "mnbtnForward":
                    break;

                case "mnbtnAdd":

                    break;

                case "mnbtnEdit":
                    utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Visible = false;
                    utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = false;
                    utmDetailBaseToolBar.Tools["mnbtnSave"].SharedProps.Enabled = true;
                    utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = false;
                    utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Enabled = true;
                    LoadReadOnly(false);
                    break;

                case "mnbtnSave":
                    try
                    {

                        if (!CheckError()) return;
                        if (txtSignDate.Value == null)
                        {
                            MSG.Error("Bạn chưa chọn ngày ký");
                            return;
                        }
                        if (_isEdit)
                        {
                            TM02TAIN tM02TAIN = new TM02TAIN();
                            tM02TAIN = ObjandGUI(_select, true);
                            Utils.ITM03TNDNService.BeginTran();
                            Utils.ITM02TAINService.Update(tM02TAIN);
                        }
                        else
                        {
                            TM02TAIN tM02TAIN = new TM02TAIN();
                            tM02TAIN = ObjandGUI(tM02TAIN, true);
                            Utils.ITM03TNDNService.BeginTran();
                            Utils.ITM02TAINService.CreateNew(tM02TAIN);
                            _select = tM02TAIN;
                            _isEdit = true;
                        }
                        Utils.ITM03TNDNService.CommitTran();
                        MSG.Information("Lưu thành công");
                        Utils.ClearCacheByType<TM02TAIN>();
                        Utils.ClearCacheByType<TM02TAINDetail>();
                        _select = Utils.ITM02TAINService.Getbykey(_select.ID);
                        utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Visible = true;
                        utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = true;
                        utmDetailBaseToolBar.Tools["mnbtnSave"].SharedProps.Enabled = false;
                        utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = true;
                        LoadReadOnly(true);
                        //checkSave = true;
                    }
                    catch (Exception ex)
                    {
                        MSG.Error(ex.Message);
                        Utils.ITM03TNDNService.RolbackTran();
                    }

                    break;

                case "mnbtnDelete":
                    if (!(DialogResult.Yes == MessageBox.Show("Bạn có muốn xóa tờ khai này không ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question)))
                        return;
                    TM02TAIN tM02TAIN_D = Utils.ListTM02TAIN.FirstOrDefault(n => n.ID == _select.ID);
                    Utils.ITM02TAINService.BeginTran();
                    Utils.ITM02TAINService.Delete(tM02TAIN_D);
                    Utils.ITM02TAINService.CommitTran();
                    MSG.Information("Xóa thành công");
                    this.Close();
                    break;

                case "mnbtnComeBack":
                    break;

                case "mnbtnPrint":
                    break;
                case "btnKBS":
                    break;
                case "mnbtnClose":
                    this.Close();
                    break;
                case "Print-TAIN02":
                    #region In báo cáo
                    TM02TAINDetail item = new TM02TAINDetail();
                    item.ID = Guid.Empty;
                    item.TM02TAINID = _select.ID;
                    //item.Type = 1;
                    item.MaterialGoodsResourceTaxGroupID = Guid.Empty;
                    item.MaterialGoodsResourceTaxGroupName = "";
                    item.Unit = "";
                    item.Quantity = 0;
                    item.UnitPrice = 0;
                    item.TaxRate = 0;
                    item.ResourceTaxTaxAmountUnit = 0;
                    item.ResourceTaxAmount = 0;
                    item.ResourceTaxAmountDeduction = 0;
                    item.ResourceTaxAmountIncurration = 0;
                    item.ResourceTaxAmountDeclaration = 0;
                    item.DifferAmount = 0;
                    item.OrderPriority = 1;


                    _select.TM02TAINDetails1 = _select.TM02TAINDetails.Where(n => n.Type == 0).OrderBy(n => n.OrderPriority).ToList();
                    _select.TM02TAINDetails2 = _select.TM02TAINDetails.Where(n => n.Type == 1).OrderBy(n => n.OrderPriority).ToList();

                    if (_select.TM02TAINDetails1.Count == 0)
                    {
                        item.Type = 0;
                        _select.TM02TAINDetails1.Add(item);
                    }
                    if (_select.TM02TAINDetails2.Count == 0)
                    {
                        item.Type = 1;
                        _select.TM02TAINDetails2.Add(item);
                    }

                    Utils.Print("TAIN-02", _select, this);
                    break;
                #endregion
                case "btnExportXml":
                    ExportXml(_select);
                    break;
            }
        }
        private void cbb_RowSelected(object sender, RowSelectedEventArgs e, UltraTextEditor CT3, UltraMaskedEdit CT6)
        {
            try
            {
                var cbb = (UltraCombo)sender;
                MaterialGoodsResourceTaxGroup model = (MaterialGoodsResourceTaxGroup)Utils.getSelectCbbItem(cbb);
                //Guid ID_ = Guid.Parse(cbb.Value.ToString());
                if (model != null)
                {
                    MaterialGoodsResourceTaxGroup materialGoodsResourceTaxGroup = lstMaterialGoodsResourceTaxGroup.FirstOrDefault(n => n.ID == model.ID);
                    CT3.Value = materialGoodsResourceTaxGroup.Unit;
                    CT6.Value = materialGoodsResourceTaxGroup.TaxRate;
                }
            }
            catch (Exception)
            {

            }

        }

        private decimal getValueMaskedEdit(UltraMaskedEdit ultraMaskedEdit)
        {
            if (!ultraMaskedEdit.Value.IsNullOrEmpty())
            {
                if (ultraMaskedEdit.Text.Contains("("))
                {
                    if (decimal.Parse(ultraMaskedEdit.Value.ToString()) == 0)
                    {
                        return 0;
                    }
                    else
                        return -decimal.Parse(ultraMaskedEdit.Value.ToString());
                }
                else
                {
                    return decimal.Parse(ultraMaskedEdit.Value.ToString());
                }
            }
            else
            {
                ultraMaskedEdit.Value = 0;
            }
            return 0;
        }
        public void LoadUgrid(UltraGrid ultraGrid, List<TM02TN> lstTaxP)
        {
            //ultraGrid.Location = new Point(tableLayoutPanel3.Location.X, tableLayoutPanel3.Location.Y + tableLayoutPanel3.Height);
            //ultraGrid.Width = tableLayoutPanel1.Width;
            ultraGrid.Height = 62;
            //ultraGrid.DisplayLayout.BorderStyle = UIElementBorderStyle.Dashed;
            ultraGrid.DisplayLayout.Override.BorderStyleCell = UIElementBorderStyle.Solid;
            ultraGrid.DataSource = new BindingList<TM02TN>(lstTaxP);
            ultraGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            ultraGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            //ultraGrid.DisplayLayout.Bands[0].Columns["Type"].Hidden = true;
            ultraGrid.DisplayLayout.Bands[0].ColHeadersVisible = false;
            ultraGrid.Margin = new Padding(0);
            ultraGrid.DisplayLayout.Bands[0].Columns["index1"].Width = (int)tableLayoutPanel1.ColumnStyles[0].Width;
            ultraGrid.DisplayLayout.Bands[0].Columns["index2"].Width = (int)tableLayoutPanel1.ColumnStyles[1].Width;
            ultraGrid.DisplayLayout.Bands[0].Columns["index3"].Width = (int)tableLayoutPanel1.ColumnStyles[2].Width;
            ultraGrid.DisplayLayout.Bands[0].Columns["index4"].Width = (int)tableLayoutPanel1.ColumnStyles[3].Width;
            ultraGrid.DisplayLayout.Bands[0].Columns["index5"].Width = (int)tableLayoutPanel1.ColumnStyles[4].Width;
            ultraGrid.DisplayLayout.Bands[0].Columns["index6"].Width = (int)tableLayoutPanel1.ColumnStyles[5].Width;
            ultraGrid.DisplayLayout.Bands[0].Columns["index7"].Width = (int)tableLayoutPanel1.ColumnStyles[6].Width;
            ultraGrid.DisplayLayout.Bands[0].Columns["index8"].Width = (int)tableLayoutPanel1.ColumnStyles[7].Width;
            ultraGrid.DisplayLayout.Bands[0].Columns["index9"].Width = (int)tableLayoutPanel1.ColumnStyles[8].Width;
            ultraGrid.DisplayLayout.Bands[0].Columns["index10"].Width = (int)tableLayoutPanel1.ColumnStyles[9].Width;
            ultraGrid.DisplayLayout.Bands[0].Columns["index11"].Width = (int)tableLayoutPanel1.ColumnStyles[10].Width;
            ultraGrid.DisplayLayout.Bands[0].Columns["index12"].Width = (int)tableLayoutPanel1.ColumnStyles[11].Width;
            //ultraGrid.DisplayLayout. = new Padding(0);
            //ultraGrid.DisplayLayout.Override.MinRowHeight = 30;
            ultraGrid.DisplayLayout.Override.RowSizingAutoMaxLines = 30;
            ultraGrid.DisplayLayout.Override.BorderStyleCell = UIElementBorderStyle.Default;
            ultraGrid.DisplayLayout.Override.CellAppearance.BorderColor = Color.Black;
            ultraGrid.DisplayLayout.Override.CellPadding = 0;

            //Utils.ConfigGrid(uGrid, ConstDatabase.TaxPriod02GTGT_TableName,false);
            //this.uGrid1.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            //this.uGrid1.DisplayLayout.ViewStyleBand = ViewStyleBand.OutlookGroupBy;
            //this.uGrid1.DisplayLayout.Bands[0].SortedColumns.Add("Type", false, true);
            //this.uGrid1.DisplayLayout.GroupByBox.Hidden = true;
            //this.uGrid1.DisplayLayout.EmptyRowSettings.ShowEmptyRows = false;
            //this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            //uGrid.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.False;
            //this.uGrid1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;

        }
        public void LoadDatagrid(DataGridView ultraGrid)
        {
            ultraGrid.Height = 60;
            //ultraGrid.DisplayLayout.BorderStyle = UIElementBorderStyle.Dashed;
            //ultraGrid.DisplayLayout.Override.BorderStyleCell = UIElementBorderStyle.Solid;
            //ultraGrid.DataSource = new BindingList<TM02TN>(lstTaxP);
            //ultraGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            //ultraGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            //ultraGrid.DisplayLayout.Bands[0].Columns["Type"].Hidden = true;
            ultraGrid.ColumnHeadersVisible = false;
            ultraGrid.Columns[0].Width = (int)tableLayoutPanel1.ColumnStyles[0].Width;
            ultraGrid.Columns[1].Width = (int)tableLayoutPanel1.ColumnStyles[1].Width + 2;
            ultraGrid.Columns[2].Width = (int)tableLayoutPanel1.ColumnStyles[2].Width + 1;
            ultraGrid.Columns[3].Width = (int)tableLayoutPanel1.ColumnStyles[3].Width + 1;
            ultraGrid.Columns[4].Width = (int)tableLayoutPanel1.ColumnStyles[4].Width + 1;
            ultraGrid.Columns[5].Width = (int)tableLayoutPanel1.ColumnStyles[5].Width + 1;
            ultraGrid.Columns[6].Width = (int)tableLayoutPanel1.ColumnStyles[6].Width + 1;
            ultraGrid.Columns[7].Width = (int)tableLayoutPanel1.ColumnStyles[7].Width + 1;
            ultraGrid.Columns[8].Width = (int)tableLayoutPanel1.ColumnStyles[8].Width + 1;
            ultraGrid.Columns[9].Width = (int)tableLayoutPanel1.ColumnStyles[9].Width + 1;
            ultraGrid.Columns[10].Width = (int)tableLayoutPanel1.ColumnStyles[10].Width + 1;
            ultraGrid.Columns[11].Width = (int)tableLayoutPanel1.ColumnStyles[11].Width + 1;
            ultraGrid.RowTemplate.Height = 30;
            //ultraGrid.RowsDefaultCellStyle.SelectionBackColor = Color.LightYellow;
            ultraGrid.RowsDefaultCellStyle.SelectionForeColor = Color.Black;
            //((UltraTextEditorColumn)ultraGrid.Columns[9]).
            //ultraGrid.SelectionMode = DataGridViewSelectionMode.CellSelect;
            ultraGrid.EditMode = DataGridViewEditMode.EditOnEnter;
            ultraGrid.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            ultraGrid.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            ultraGrid.AllowUserToResizeColumns = false;
            ultraGrid.AllowUserToResizeRows = false;
            ultraGrid.AllowUserToAddRows = false;
            ultraGrid.Rows.Add();
            ultraGrid.Rows[0].Cells[0].Value = 1;
            ultraGrid.Height = ultraGrid.RowCount * ultraGrid.RowTemplate.Height + 1;
            if (ultraGrid.Name == "dataGridView1")
            {
                ultraGrid.Location = new Point(tableLayoutPanel1.Location.X, tableLayoutPanel1.Location.Y + tableLayoutPanel1.Height);
                ultraGrid.Width = tableLayoutPanel1.Width;
                tableLayoutPanel2.Location = new Point(ultraGrid.Location.X, ultraGrid.Location.Y + ultraGrid.Height);
            }
            if (ultraGrid.Name == "dataGridView2")
            {
                ultraGrid.Location = new Point(tableLayoutPanel2.Location.X, tableLayoutPanel2.Location.Y + tableLayoutPanel2.Height);
                ultraGrid.Width = tableLayoutPanel1.Width;
                tableLayoutPanel3.Location = new Point(ultraGrid.Location.X, ultraGrid.Location.Y + ultraGrid.Height);
                ultraPanel2.Location = new Point(tableLayoutPanel3.Location.X, tableLayoutPanel3.Location.Y + tableLayoutPanel3.Height + 10);
            }

        }
        public void LoadTable(TableLayoutPanel tableLayoutPanel_)
        {
            tableLayoutPanel_.ColumnStyles[0].Width = tableLayoutPanel1.ColumnStyles[0].Width;
            tableLayoutPanel_.ColumnStyles[1].Width = tableLayoutPanel1.ColumnStyles[1].Width;
            tableLayoutPanel_.ColumnStyles[2].Width = tableLayoutPanel1.ColumnStyles[2].Width;
            tableLayoutPanel_.ColumnStyles[3].Width = tableLayoutPanel1.ColumnStyles[3].Width;
            tableLayoutPanel_.ColumnStyles[4].Width = tableLayoutPanel1.ColumnStyles[4].Width;
            tableLayoutPanel_.ColumnStyles[5].Width = tableLayoutPanel1.ColumnStyles[5].Width;
            tableLayoutPanel_.ColumnStyles[6].Width = tableLayoutPanel1.ColumnStyles[6].Width;
            tableLayoutPanel_.ColumnStyles[7].Width = tableLayoutPanel1.ColumnStyles[7].Width;
            tableLayoutPanel_.ColumnStyles[8].Width = tableLayoutPanel1.ColumnStyles[8].Width;
            tableLayoutPanel_.ColumnStyles[9].Width = tableLayoutPanel1.ColumnStyles[9].Width;
            tableLayoutPanel_.ColumnStyles[10].Width = tableLayoutPanel1.ColumnStyles[10].Width;
            tableLayoutPanel_.ColumnStyles[11].Width = tableLayoutPanel1.ColumnStyles[11].Width;
            tableLayoutPanel_.RowStyles[0].Height = 30;
            if (tableLayoutPanel_.Name == "tableLayoutPanel4")
            {
                tableLayoutPanel_.Location = new Point(tableLayoutPanel1.Location.X, tableLayoutPanel1.Location.Y + tableLayoutPanel1.Height);
                tableLayoutPanel2.Location = new Point(tableLayoutPanel_.Location.X, tableLayoutPanel_.Location.Y + tableLayoutPanel_.Height);
            }
            else if (tableLayoutPanel_.Name == "tableLayoutPanel5")
            {
                tableLayoutPanel_.Location = new Point(tableLayoutPanel2.Location.X, tableLayoutPanel2.Location.Y + tableLayoutPanel2.Height);
                tableLayoutPanel3.Location = new Point(tableLayoutPanel_.Location.X, tableLayoutPanel_.Location.Y + tableLayoutPanel_.Height);
                ultraPanel2.Location = new Point(tableLayoutPanel3.Location.X, tableLayoutPanel3.Location.Y + tableLayoutPanel3.Height + 10);
            }
        }
        private decimal getValueFromPosition(TableLayoutPanel tableLayoutPanel, int column, int row)
        {
            return getValueMaskedEdit((UltraMaskedEdit)tableLayoutPanel.GetControlFromPosition(column, row));
        }


        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams handleParam = base.CreateParams;
                handleParam.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED       
                return handleParam;
            }
        }
        private bool ExportXml(TM02TAIN tM02TAIN)
        {
            SaveFileDialog sf = new SaveFileDialog
            {
                FileName = "02TAIN.xml",
                AddExtension = true,
                Filter = "Excel Document(*.xml)|*.xml"
            };
            if (sf.ShowDialog() != DialogResult.OK)
            {
                return true;
            }
            string path = System.IO.Path.GetDirectoryName(
                        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            string filePath = string.Format("{0}\\TemplateResource\\xml\\02_TAIN_xml.xml", path);
            XmlDocument doc = new XmlDocument();
            doc.Load(filePath);

            #region TTChung
            string tt = "HSoThueDTu/HSoKhaiThue/TTinChung/";
            doc.SelectSingleNode(tt + "TTinDVu/maDVu").InnerText = "";
            doc.SelectSingleNode(tt + "TTinDVu/tenDVu").InnerText = "";
            doc.SelectSingleNode(tt + "TTinDVu/pbanDVu").InnerText = "";
            doc.SelectSingleNode(tt + "TTinDVu/ttinNhaCCapDVu").InnerText = "";

            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/maTKhai").InnerText = "02";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/tenTKhai").InnerText = tM02TAIN.DeclarationName;
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/moTaBMau").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/pbanTKhaiXML").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/loaiTKhai").InnerText = "C";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/soLan").InnerText = "";

            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kieuKy").InnerText = "Y";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = tM02TAIN.DeclarationTerm.Split(' ')[1];
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhaiTuNgay").InnerText = tM02TAIN.FromDate.ToString("dd/MM/yyyy");
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhaiDenNgay").InnerText = tM02TAIN.ToDate.ToString("dd/MM/yyyy");

            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/maCQTNoiNop").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/tenCQTNoiNop").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/ngayLapTKhai").InnerText = "";

            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/GiaHan/maLyDoGiaHan").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/GiaHan/lyDoGiaHan").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/GiaHan/lyDoGiaHan").InnerText = "";

            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/nguoiKy").InnerText = tM02TAIN.SignName;
            if (tM02TAIN.SignDate != null)
            {
                DateTime dt = (DateTime)tM02TAIN.SignDate;
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/ngayKy").InnerText = dt.ToString("dd/MM/yyyy");
            }
            else
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/ngayKy").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/nganhNgheKD").InnerText = tM02TAIN.CertificationNo;

            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/mst").InnerText = tM02TAIN.CompanyTaxCode;
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/tenNNT").InnerText = tM02TAIN.CompanyName;
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/dchiNNT").InnerText = Utils.GetCompanyAddress();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/phuongXa").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/maHuyenNNT").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/tenHuyenNNT").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/maTinhNNT").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/tenTinhNNT").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/dthoaiNNT").InnerText = Utils.GetCompanyPhoneNumber();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/faxNNT").InnerText = tM02TAIN.CompanyTaxCode;
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/emailNNT").InnerText = Utils.GetCompanyEmail();

            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/mstDLyThue").InnerText = tM02TAIN.TaxAgencyTaxCode;
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/tenDLyThue").InnerText = tM02TAIN.TaxAgencyName;
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/dchiDLyThue").InnerText = Utils.GetDLTAddress();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/maHuyenDLyThue").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/tenHuyenDLyThue").InnerText = Utils.GetDLTDistrict();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/maTinhDLyThue").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/tenTinhDLyThue").InnerText = Utils.GetDLTCity();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/dthoaiDLyThue").InnerText = Utils.GetDLTPhoneNumber();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/faxDLyThue").InnerText = Utils.GetDLTFax();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/emailDLyThue").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/soHDongDLyThue").InnerText = Utils.GetDLTSoHD();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/ngayKyHDDLyThue").InnerText = Utils.GetDLTNgay();

            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/NVienDLy/tenNVienDLyThue").InnerText = Utils.GetHVTNhanVienDLT();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/NVienDLy/cchiHNghe").InnerText = Utils.GetCCHNDLT();
            #endregion
            tt = "HSoThueDTu/HSoKhaiThue/CTieuTKhaiChinh/";
            List<TM02TAINDetail> lst1 = tM02TAIN.TM02TAINDetails.Where(n => n.Type == 0).ToList().OrderBy(n => n.OrderPriority).ToList();
            List<TM02TAINDetail> lst2 = tM02TAIN.TM02TAINDetails.Where(n => n.Type == 1).ToList().OrderBy(n => n.OrderPriority).ToList();
            if (lst1.Count == 0)
            {
                //doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac").Attributes["id"].Value = "ID_" + (i + 1).ToString();
                doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct1").InnerText = "";
                doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct2").InnerText = "";
                doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct3").InnerText = "";
                doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct4").InnerText = "";
                doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct5").InnerText = "";
                doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct6").InnerText = "";
                doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct7").InnerText = "";
                doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct8").InnerText = "";
                doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct9").InnerText = "";
                doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct10").InnerText = "";
                doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct11").InnerText = "";
                doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct12").InnerText = "";
            }
            if (lst2.Count == 0)
            {
                //doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom").Attributes["id"].Value = "ID_" + (i + 1).ToString();
                doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct1").InnerText = "";
                doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct2").InnerText = "";
                doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct3").InnerText = "";
                doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct4").InnerText = "";
                doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct5").InnerText = "";
                doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct6").InnerText = "";
                doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct7").InnerText = "";
                doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct8").InnerText = "";
                doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct9").InnerText = "";
                doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct10").InnerText = "";
                doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct11").InnerText = "";
                doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct12").InnerText = "";
            }
            for (int i = 0; i < lst1.Count(); i++)
            {
                if (i == 0)
                {
                    doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac").Attributes["id"].Value = "ID_" + (i + 1).ToString();
                    doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct1").InnerText = lst1[i].OrderPriority.ToString();
                    doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct2").InnerText = lst1[i].MaterialGoodsResourceTaxGroupName;
                    doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct3").InnerText = lst1[i].Unit;
                    doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct4").InnerText = lst1[i].Quantity.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct5").InnerText = lst1[i].UnitPrice.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct6").InnerText = lst1[i].TaxRate.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct7").InnerText = lst1[i].ResourceTaxTaxAmountUnit.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct8").InnerText = lst1[i].ResourceTaxAmountIncurration.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct9").InnerText = lst1[i].ResourceTaxAmountDeduction.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct10").InnerText = lst1[i].ResourceTaxAmount.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct11").InnerText = lst1[i].ResourceTaxAmountDeclaration.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac/ct12").InnerText = lst1[i].DifferAmount.ToString("0.00").Replace(',', '.');
                }
                else
                {
                    XmlNode xmlNode = doc.SelectSingleNode(tt + "TNKhaiThac/CTietTNKhaiThac").CloneNode(true);
                    xmlNode.Attributes["id"].Value = "ID_" + (i + 1).ToString();
                    xmlNode.SelectSingleNode("ct1").InnerText = lst1[i].OrderPriority.ToString();
                    xmlNode.SelectSingleNode("ct2").InnerText = lst1[i].MaterialGoodsResourceTaxGroupName;
                    xmlNode.SelectSingleNode("ct3").InnerText = lst1[i].Unit;
                    xmlNode.SelectSingleNode("ct4").InnerText = lst1[i].Quantity.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct5").InnerText = lst1[i].UnitPrice.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct6").InnerText = lst1[i].TaxRate.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct7").InnerText = lst1[i].ResourceTaxTaxAmountUnit.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct8").InnerText = lst1[i].ResourceTaxAmountIncurration.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct9").InnerText = lst1[i].ResourceTaxAmountDeduction.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct10").InnerText = lst1[i].ResourceTaxAmount.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct11").InnerText = lst1[i].ResourceTaxAmountDeclaration.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct12").InnerText = lst1[i].DifferAmount.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TNKhaiThac").AppendChild(xmlNode);
                }
            }
            for (int i = 0; i < lst2.ToList().Count(); i++)
            {
                if (i == 0)
                {
                    doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom").Attributes["id"].Value = "ID_" + (i + 1).ToString();
                    doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct1").InnerText = lst2[i].OrderPriority.ToString();
                    doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct2").InnerText = lst2[i].MaterialGoodsResourceTaxGroupName;
                    doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct3").InnerText = lst2[i].Unit;
                    doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct4").InnerText = lst2[i].Quantity.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct5").InnerText = lst2[i].UnitPrice.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct6").InnerText = lst2[i].TaxRate.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct7").InnerText = lst2[i].ResourceTaxTaxAmountUnit.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct8").InnerText = lst2[i].ResourceTaxAmountIncurration.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct9").InnerText = lst2[i].ResourceTaxAmountDeduction.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct10").InnerText = lst2[i].ResourceTaxAmount.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct11").InnerText = lst2[i].ResourceTaxAmountDeclaration.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom/ct12").InnerText = lst2[i].DifferAmount.ToString("0.00").Replace(',', '.');
                }
                else
                {
                    XmlNode xmlNode = doc.SelectSingleNode(tt + "TNThuMuaGom/CTietTNThuMuaGom").CloneNode(true);
                    xmlNode.Attributes["id"].Value = "ID_" + (i + 1).ToString();
                    xmlNode.SelectSingleNode("ct1").InnerText = lst2[i].OrderPriority.ToString();
                    xmlNode.SelectSingleNode("ct2").InnerText = lst2[i].MaterialGoodsResourceTaxGroupName;
                    xmlNode.SelectSingleNode("ct3").InnerText = lst2[i].Unit;
                    xmlNode.SelectSingleNode("ct4").InnerText = lst2[i].Quantity.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct5").InnerText = lst2[i].UnitPrice.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct6").InnerText = lst2[i].TaxRate.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct7").InnerText = lst2[i].ResourceTaxTaxAmountUnit.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct8").InnerText = lst2[i].ResourceTaxAmountIncurration.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct9").InnerText = lst2[i].ResourceTaxAmountDeduction.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct10").InnerText = lst2[i].ResourceTaxAmount.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct11").InnerText = lst2[i].ResourceTaxAmountDeclaration.ToString("0.00").Replace(',', '.');
                    xmlNode.SelectSingleNode("ct12").InnerText = lst2[i].DifferAmount.ToString("0.00").Replace(',', '.');
                    doc.SelectSingleNode(tt + "TNThuMuaGom").AppendChild(xmlNode);
                }
            }
            doc.SelectSingleNode(tt + "tong_ct8").InnerText = getValueMaskedEdit(txtSum8).ToString("0.00");
            doc.SelectSingleNode(tt + "tong_ct9").InnerText = getValueMaskedEdit(txtSum9).ToString("0.00");
            doc.SelectSingleNode(tt + "tong_ct10").InnerText = getValueMaskedEdit(txtSum10).ToString("0.00");
            doc.SelectSingleNode(tt + "tong_ct11").InnerText = getValueMaskedEdit(txtSum11).ToString("0.00");
            doc.SelectSingleNode(tt + "tong_ct12").InnerText = getValueMaskedEdit(txtSum12).ToString("0.00");

            doc.Save(sf.FileName);
            if (MSG.Question("Xuất xml thành công, Bạn có muốn mở file vừa tải") == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    System.Diagnostics.Process.Start(sf.FileName);
                }
                catch
                {
                    MSG.Error("Lỗi mở file");
                }

            }

            return true;
        }

        private void _02_TAIN_Detail_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (Form)sender;
            frm.Dispose();

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}
