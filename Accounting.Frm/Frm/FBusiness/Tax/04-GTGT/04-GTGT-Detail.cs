﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinMaskedEdit;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Accounting
{
    public partial class _04_GTGT_Detail : Form
    {
        #region khai báo
        public string DeclarationTerm = "";
        public bool IsFirst = false;
        public string AdditionalTime = "";
        //public ISystemOptionService _ISystemOptionService { get { return IoC.Resolve<ISystemOptionService>(); } }
        public DateTime FromDate = DateTime.Now;
        public DateTime ToDate = DateTime.Now;
        public int Day = DateTime.Now.Day;
        public int Month = DateTime.Now.Month;
        public int Year = DateTime.Now.Year;
        public DateTime KHBS = DateTime.Now;
        public bool isDeclaredArising = false;
        public bool isDeclaredMonth = false;
        public bool isDeclaredQuarter = false;
        private bool _isEdit;
        private TM04GTGT _select;
        public bool checkSave = false;
        public int MaxAdditionTime = 0;
        private TM04GTGT _tM04GTGTBS;
        public bool CheckBS;
        List<TM04GTGTAdjust> lst1 = new List<TM04GTGTAdjust>();
        List<TM04GTGTAdjust> lst2 = new List<TM04GTGTAdjust>();
        #endregion

        #region khởi tạo form
        public _04_GTGT_Detail(TM04GTGT temp, bool isEdit)
        {
            InitializeComponent();
            _select = temp;
            _isEdit = isEdit;
            if (isEdit)
            {
                ObjandGUI(temp, false);
                LoadReadOnly(true);
                utmDetailBaseToolBar.Tools["mnbtnSave"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = true;
                utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Enabled = true;
                utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Enabled = false;

            }
            else
            {
                LoadReadOnly(false);
                utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Enabled = false;
            }
            Utils.ClearCacheByType<SystemOption>();

            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
                this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
            }
            ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[0];
            utmDetailBaseToolBar.Ribbon.NonInheritedRibbonTabs[0].Caption = @"Chức năng";
        }
        #endregion
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams handleParam = base.CreateParams;
                handleParam.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED       
                return handleParam;
            }
        }

        #region hàm xóa dòng và get row
        public int GetRow(TableLayoutPanel tableLayoutPanel)
        {
            for (int t = 1; t < tableLayoutPanel.RowCount; t++)
            {
                for (int i = 0; i < tableLayoutPanel.ColumnCount; i++)
                {
                    var control = tableLayoutPanel.GetControlFromPosition(i, t);
                    if (control != null)
                        if (control.ContainsFocus)
                        {
                            return t;

                        }
                }
            }
            return 0;
        }
        public void remove_row(TableLayoutPanel panel, int row_index_to_remove)
        {
            if (row_index_to_remove >= panel.RowCount)
            {
                return;
            }

            // delete all controls of row that we want to delete
            for (int i = 0; i < panel.ColumnCount; i++)
            {
                var control = panel.GetControlFromPosition(i, row_index_to_remove);
                panel.Controls.Remove(control);
            }

            // move up row controls that comes after row we want to remove
            for (int i = row_index_to_remove + 1; i < panel.RowCount; i++)
            {
                for (int j = 0; j < panel.ColumnCount; j++)
                {
                    var control = panel.GetControlFromPosition(j, i);
                    if (control != null)
                    {
                        panel.SetRow(control, i - 1);
                    }
                }
            }

            // remove last row
            //panel.RowStyles.RemoveAt(panel.RowCount - 1);
            panel.RowCount--;
        }
        #endregion

        #region LoadInitialize 
        public bool LoadInitialize(bool isGet)
        {
            if (isGet)
            {

            }
            else
            {
                lblCompanyName.Text = Utils.GetCompanyName();
                lblCompanyTaxCode.Text = Utils.GetCompanyTaxCode();
                lblDL_Thue.Text = Utils.GetTenDLT();
                lblMST_DL.Text = Utils.GetMSTDLT();
                txtName.Text = Utils.GetHVTNhanVienDLT();
                txtChungChi.Text = Utils.GetCCHNDLT();
                txtSignName.Text = Utils.GetCompanyDirector();
                txtSignDate.DateTime = DateTime.Now;
                List<GeneralLedger> lstGL = Utils.ListGeneralLedger.Where(n => n.PostedDate >= FromDate && n.PostedDate <= ToDate && n.Account != null && n.AccountCorresponding != null).ToList();
                #region txtItem21
                var lstGL1 = (from a in lstGL
                              join b in Utils.ListSAInvoiceDetail on a.DetailID equals b.ID
                              where a.Account.StartsWith("511") && b.CareerGroupID == Guid.Parse("58A72ACD-13E1-43D1-88E9-F8C1FE583F44")
                              select a.CreditAmount - a.DebitAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                var lstGL2 = (from a in lstGL
                              join b in Utils.ListSAReturnDetail on a.DetailID equals b.ID
                              where a.Account.StartsWith("511") && b.CareerGroupID == Guid.Parse("58A72ACD-13E1-43D1-88E9-F8C1FE583F44")
                              select a.CreditAmount - a.DebitAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                BindData(txtItem21, (lstGL1 + lstGL2));
                #endregion
                #region txtItem22
                var lstGL3 = (from a in lstGL
                              join b in Utils.ListSAInvoiceDetail on a.DetailID equals b.ID
                              where a.Account.StartsWith("511") && b.CareerGroupID == Guid.Parse("F63A3B76-8F45-472B-9A50-DC3F90DE9056")
                              select a.CreditAmount - a.DebitAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                var lstGL4 = (from a in lstGL
                              join b in Utils.ListSAReturnDetail on a.DetailID equals b.ID
                              where a.Account.StartsWith("511") && b.CareerGroupID == Guid.Parse("F63A3B76-8F45-472B-9A50-DC3F90DE9056")
                              select a.CreditAmount - a.DebitAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                BindData(txtItem22, (lstGL3 + lstGL4));
                #endregion
                #region txtItem24
                var lstGL5 = (from a in lstGL
                              join b in Utils.ListSAInvoiceDetail on a.DetailID equals b.ID
                              where a.Account.StartsWith("511") && b.CareerGroupID == Guid.Parse("8CC59044-8F45-45D5-A886-62D3EFDB3517")
                              select a.CreditAmount - a.DebitAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                var lstGL6 = (from a in lstGL
                              join b in Utils.ListSAReturnDetail on a.DetailID equals b.ID
                              where a.Account.StartsWith("511") && b.CareerGroupID == Guid.Parse("8CC59044-8F45-45D5-A886-62D3EFDB3517")
                              select a.CreditAmount - a.DebitAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                BindData(txtItem24, (lstGL5 + lstGL6));
                #endregion
                #region txtItem26
                var lstGL7 = (from a in lstGL
                              join b in Utils.ListSAInvoiceDetail on a.DetailID equals b.ID
                              where a.Account.StartsWith("511") && b.CareerGroupID == Guid.Parse("BD586600-1B06-49C8-AB11-C3A61294A6DF")
                              select a.CreditAmount - a.DebitAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                var lstGL8 = (from a in lstGL
                              join b in Utils.ListSAReturnDetail on a.DetailID equals b.ID
                              where a.Account.StartsWith("511") && b.CareerGroupID == Guid.Parse("BD586600-1B06-49C8-AB11-C3A61294A6DF")
                              select a.CreditAmount - a.DebitAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                BindData(txtItem26, (lstGL7 + lstGL8));
                #endregion
                #region txtItem28
                var lstGL9 = (from a in lstGL
                              join b in Utils.ListSAInvoiceDetail on a.DetailID equals b.ID
                              where a.Account.StartsWith("511") && b.CareerGroupID == Guid.Parse("FF99230B-805E-45F3-83A0-22FEEEF159DA")
                              select a.CreditAmount - a.DebitAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                var lstGL10 = (from a in lstGL
                               join b in Utils.ListSAReturnDetail on a.DetailID equals b.ID
                               where a.Account.StartsWith("511") && b.CareerGroupID == Guid.Parse("FF99230B-805E-45F3-83A0-22FEEEF159DA")
                               select a.CreditAmount - a.DebitAmount).Sum(n => Math.Round(n, MidpointRounding.AwayFromZero));
                BindData(txtItem28, (lstGL9 + lstGL10));
                #endregion
                if (isDeclaredMonth)
                {
                    txtIsArisingTax.Visible = false;
                    ultraLabel6.Visible = false;
                    lblArisingTime.Location = new Point(433, 92);
                    lblArisingTime.Text = DeclarationTerm;
                }
                if (isDeclaredQuarter)
                {
                    txtIsArisingTax.Visible = false;
                    ultraLabel6.Visible = false;
                    lblArisingTime.Location = new Point(433, 92);
                    lblArisingTime.Text = DeclarationTerm;
                }
                if (isDeclaredArising)
                {
                    txtIsArisingTax.Text = "[X]";
                    lblArisingTime.Text = DeclarationTerm;
                }
                if (IsFirst == true)
                {
                    txtIsFirst.Text = "[x]";
                    lblAdditionTime.Text = "";
                    ultraTabControl1.Tabs[1].Visible = false;
                    utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Visible = false;
                    ultraTabControl1.Tabs[1].Visible = false;
                }
                else
                {
                    txtIsFirst.Text = "[]";
                    lblAdditionTime.Text = AdditionalTime;
                    ultraTabControl1.Tabs[1].Visible = !IsFirst;
                    ultraLabel154.Text = DeclarationTerm + " ngày " + KHBS.Day + " tháng " + KHBS.Month + " năm " + KHBS.Year + ")";
                    if (isDeclaredMonth)
                    {
                        DateTime dt = ToDate.AddDays(20);
                        txtDateDelay.Value = KHBS.Subtract(dt).Days;
                    }
                    if (isDeclaredQuarter)
                    {
                        DateTime dt = ToDate.AddDays(30);
                        txtDateDelay.Value = KHBS.Subtract(dt).Days;
                    }
                    if (isDeclaredArising)
                    {
                        DateTime datetime = new DateTime(Year, Month, Day);
                        DateTime dt = datetime.AddDays(10);
                        txtDateDelay.Value = KHBS.Subtract(dt).Days;
                    }
                    if (txtDateDelay.Value.ToInt() < 0)
                        txtDateDelay.Value = 0;
                    if (Utils.ListTM04GTGT.Where(n => n.FromDate == FromDate && n.ToDate == ToDate).Count() > 0)
                    {
                        if (MaxAdditionTime == 0)
                        {
                            TM04GTGT tm04GTGTBS = Utils.ListTM04GTGT.Where(n => n.FromDate == FromDate && n.AdditionTime == null).FirstOrDefault();
                            _tM04GTGTBS = tm04GTGTBS;
                            ObjandGUIKHBS(tm04GTGTBS, false);

                        }
                        else
                        {
                            TM04GTGT tm04GTGTBS = Utils.ListTM04GTGT.Where(n => n.FromDate == FromDate && n.AdditionTime == MaxAdditionTime).FirstOrDefault();
                            _tM04GTGTBS = tm04GTGTBS;
                            ObjandGUIKHBS(tm04GTGTBS, false);
                        }

                    }
                    else
                    {
                        MSG.Error("Không tồn tại tờ khai cùng kỳ");
                        return false;
                    }

                }

            }
            return true;
        }
        #endregion

        #region Load read only
        private void LoadReadOnly(bool isReadOnly)
        {
            utmDetailBaseToolBar.Tools["mnbtnDelete"].SharedProps.Enabled = isReadOnly;
            txtItem21.ReadOnly = isReadOnly;
            txtItem22.ReadOnly = isReadOnly;
            txtItem24.ReadOnly = isReadOnly;
            txtItem26.ReadOnly = isReadOnly;
            txtItem28.ReadOnly = isReadOnly;
            txtName.ReadOnly = isReadOnly;
            txtChungChi.ReadOnly = isReadOnly;
            txtSignName.ReadOnly = isReadOnly;
            txtSignDate.ReadOnly = isReadOnly;
        }
        #endregion

        #region ObjandGUI
        private TM04GTGT ObjandGUI(TM04GTGT input, bool isGet)
        {
            #region Lưu vào DB
            if (isGet)
            {
                if (input.ID == null || input.ID == Guid.Empty)
                {
                    input.ID = Guid.NewGuid();
                }
                input.TypeID = 926;
                input.DeclarationName = ultraLabel1.Text;
                if (isDeclaredMonth == true)
                {
                    input.DeclarationTerm = lblArisingTime.Text;
                    input.IsMonth = true;
                }
                if (isDeclaredQuarter == true)
                {
                    input.DeclarationTerm = lblArisingTime.Text;
                    input.IsQuarter = true;
                }
                if (isDeclaredArising == true)
                {
                    input.DeclarationTerm = lblArisingTime.Text;
                    input.IsArising = true;
                }
                if (txtIsFirst.Text == "[x]" && lblAdditionTime.Text == "")
                    input.IsFirstDeclaration = true;
                else
                    input.IsFirstDeclaration = false;
                try
                {
                    input.AdditionTime = int.Parse(lblAdditionTime.Text);
                }
                catch { }
                input.AdditionTerm = ultraLabel154.Text;
                input.CompanyName = lblCompanyName.Text;
                input.CompanyTaxCode = lblCompanyTaxCode.Text;
                input.TaxAgencyName = lblDL_Thue.Text;
                input.TaxAgencyTaxCode = lblMST_DL.Text;
                input.TaxAgencyEmployeeName = txtName.Text;
                input.CertificationNo = txtChungChi.Text;
                input.SignName = txtSignName.Text;
                if (txtSignDate.Value != null)
                    input.SignDate = txtSignDate.DateTime;
                else
                    input.SignDate = null;
                input.FromDate = FromDate;
                input.ToDate = ToDate;

                #region tờ khai chính
                if (_isEdit)
                {
                    input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item21" && n.TM04GTGTID == input.ID).Data = getValueMaskedEdit(txtItem22).ToString();
                    input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item22" && n.TM04GTGTID == input.ID).Data = getValueMaskedEdit(txtItem22).ToString();
                    input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item23" && n.TM04GTGTID == input.ID).Data = getValueMaskedEdit(txtItem23).ToString();
                    input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item24" && n.TM04GTGTID == input.ID).Data = getValueMaskedEdit(txtItem24).ToString();
                    input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item25" && n.TM04GTGTID == input.ID).Data = getValueMaskedEdit(txtItem25).ToString();
                    input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item26" && n.TM04GTGTID == input.ID).Data = getValueMaskedEdit(txtItem26).ToString();
                    input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item27" && n.TM04GTGTID == input.ID).Data = getValueMaskedEdit(txtItem27).ToString();
                    input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item28" && n.TM04GTGTID == input.ID).Data = getValueMaskedEdit(txtItem28).ToString();
                    input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item29" && n.TM04GTGTID == input.ID).Data = getValueMaskedEdit(txtItem29).ToString();
                    input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item30" && n.TM04GTGTID == input.ID).Data = getValueMaskedEdit(txtItem30).ToString();
                    input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item31" && n.TM04GTGTID == input.ID).Data = getValueMaskedEdit(txtItem31).ToString();
                    input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item32" && n.TM04GTGTID == input.ID).Data = getValueLabel(txtItem32).ToString();
                    input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item33" && n.TM04GTGTID == input.ID).Data = getValueLabel(txtItem33).ToString();
                }

                else
                {
                    int i = 0;
                    input.TM04GTGTDetails.Add(new TM04GTGTDetail
                    {
                        TM04GTGTID = input.ID,
                        Code = "Item21",
                        //Name = ultraLabel22.Text,
                        Data = getValueMaskedEdit(txtItem21).ToString(),
                        OrderPriority = i++
                    });
                    input.TM04GTGTDetails.Add(new TM04GTGTDetail
                    {

                        TM04GTGTID = input.ID,
                        Code = "Item22",
                        //Name = ultraLabel23.Text,
                        Data = getValueMaskedEdit(txtItem22).ToString(),
                        OrderPriority = i++
                    });
                    input.TM04GTGTDetails.Add(new TM04GTGTDetail
                    {

                        TM04GTGTID = input.ID,
                        Code = "Item23",
                        //Name = ultraLabel24.Text,
                        Data = getValueMaskedEdit(txtItem23).ToString(),
                        OrderPriority = i++
                    });
                    input.TM04GTGTDetails.Add(new TM04GTGTDetail
                    {

                        TM04GTGTID = input.ID,
                        Code = "Item24",
                        //Name = ultraLabel25.Text,
                        Data = getValueMaskedEdit(txtItem24).ToString(),
                        OrderPriority = i++
                    });
                    input.TM04GTGTDetails.Add(new TM04GTGTDetail
                    {

                        TM04GTGTID = input.ID,
                        Code = "Item25",
                        //Name = ultraLabel33.Text,
                        Data = getValueMaskedEdit(txtItem25).ToString(),
                        OrderPriority = i++
                    });
                    input.TM04GTGTDetails.Add(new TM04GTGTDetail
                    {
                        TM04GTGTID = input.ID,
                        Code = "Item26",
                        //Name = ultraLabel50.Text,
                        Data = getValueMaskedEdit(txtItem26).ToString(),
                        OrderPriority = i++
                    });
                    input.TM04GTGTDetails.Add(new TM04GTGTDetail
                    {
                        TM04GTGTID = input.ID,
                        Code = "Item27",
                        //Name = ultraLabel47.Text,
                        Data = getValueMaskedEdit(txtItem27).ToString(),
                        OrderPriority = i++
                    });
                    input.TM04GTGTDetails.Add(new TM04GTGTDetail
                    {
                        TM04GTGTID = input.ID,
                        Code = "Item28",
                        //Name = ultraLabel47.Text,
                        Data = getValueMaskedEdit(txtItem28).ToString(),
                        OrderPriority = i++
                    });
                    input.TM04GTGTDetails.Add(new TM04GTGTDetail
                    {
                        TM04GTGTID = input.ID,
                        Code = "Item29",
                        //Name = ultraLabel41.Text,
                        Data = getValueMaskedEdit(txtItem29).ToString(),
                        OrderPriority = i++
                    });
                    input.TM04GTGTDetails.Add(new TM04GTGTDetail
                    {
                        TM04GTGTID = input.ID,
                        Code = "Item30",
                        //Name = ultraLabel53.Text,
                        Data = getValueMaskedEdit(txtItem30).ToString(),
                        OrderPriority = i++
                    });
                    input.TM04GTGTDetails.Add(new TM04GTGTDetail
                    {
                        TM04GTGTID = input.ID,
                        Code = "Item31",
                        //Name = ultraLabel53.Text,
                        Data = getValueMaskedEdit(txtItem31).ToString(),
                        OrderPriority = i++
                    });
                    input.TM04GTGTDetails.Add(new TM04GTGTDetail
                    {
                        TM04GTGTID = input.ID,
                        Code = "Item32",
                        //Name = ultraLabel53.Text,
                        Data = getValueLabel(txtItem32).ToString(),
                        OrderPriority = i++
                    });
                    input.TM04GTGTDetails.Add(new TM04GTGTDetail
                    {
                        TM04GTGTID = input.ID,
                        Code = "Item33",
                        //Name = ultraLabel53.Text,
                        Data = getValueLabel(txtItem33).ToString(),
                        OrderPriority = i++
                    });
                }
                #endregion

                #region KHBS

                if (ultraTabControl1.Tabs[1].Visible == true)
                {
                    input.TM04GTGTAdjusts.Clear();
                    #region table 8
                    if (lst1.Count != 0)
                    {
                        for (int t = 1; t < tableLayoutPanel8.RowCount; t++)
                        {
                            var control0 = tableLayoutPanel8.GetControlFromPosition(0, t);
                            var control1 = tableLayoutPanel8.GetControlFromPosition(1, t);
                            var control2 = tableLayoutPanel8.GetControlFromPosition(2, t);
                            var control3 = tableLayoutPanel8.GetControlFromPosition(3, t);
                            var control4 = tableLayoutPanel8.GetControlFromPosition(4, t);
                            var control5 = tableLayoutPanel8.GetControlFromPosition(5, t);

                            UltraLabel STT = control0 as UltraLabel;
                            UltraTextEditor Name = control1 as UltraTextEditor;
                            UltraTextEditor Code = control2 as UltraTextEditor;
                            UltraTextEditor DecAmount = control3 as UltraTextEditor;
                            UltraTextEditor AdjAmount = control4 as UltraTextEditor;
                            UltraTextEditor DiffAmount = control5 as UltraTextEditor;

                            input.TM04GTGTAdjusts.Add(new TM04GTGTAdjust
                            {
                                TM04GTGTID = input.ID,
                                OrderPriority = int.Parse(STT.Text),
                                Name = Name.Text,
                                Code = Code.Text,
                                DeclaredAmount = getValueTextEdit(DecAmount),
                                AdjustAmount = getValueTextEdit(AdjAmount),
                                DifferAmount = getValueTextEdit(DiffAmount),
                                Type = 1

                            });
                        }
                    }
                    #endregion

                    #region table 9
                    if (lst2.Count != 0)
                    {
                        for (int t = 1; t < tableLayoutPanel9.RowCount; t++)
                        {
                            var control0 = tableLayoutPanel9.GetControlFromPosition(0, t);
                            var control1 = tableLayoutPanel9.GetControlFromPosition(1, t);
                            var control2 = tableLayoutPanel9.GetControlFromPosition(2, t);
                            var control3 = tableLayoutPanel9.GetControlFromPosition(3, t);
                            var control4 = tableLayoutPanel9.GetControlFromPosition(4, t);
                            var control5 = tableLayoutPanel9.GetControlFromPosition(5, t);

                            UltraLabel STT = control0 as UltraLabel;
                            UltraTextEditor Name = control1 as UltraTextEditor;
                            UltraTextEditor Code = control2 as UltraTextEditor;
                            UltraTextEditor DecAmount = control3 as UltraTextEditor;
                            UltraTextEditor AdjAmount = control4 as UltraTextEditor;
                            UltraTextEditor DiffAmount = control5 as UltraTextEditor;

                            input.TM04GTGTAdjusts.Add(new TM04GTGTAdjust
                            {
                                TM04GTGTID = input.ID,
                                OrderPriority = int.Parse(STT.Text),
                                Name = Name.Text,
                                Code = Code.Text,
                                DeclaredAmount = getValueTextEdit(DecAmount),
                                AdjustAmount = getValueTextEdit(AdjAmount),
                                DifferAmount = getValueTextEdit(DiffAmount),
                                Type = 2
                            });
                        }
                    }
                    #endregion
                    if (lst1.Count != 0 || lst2.Count != 0)
                    {
                        input.TM04GTGTAdjusts.Add(new TM04GTGTAdjust
                        {
                            TM04GTGTID = input.ID,
                            Name = "Tổng số thuế GTGT còn phải nộp",
                            Code = "33",
                            DeclaredAmount = getValueLabel(lblDeclaredTaxAmount),
                            AdjustAmount = getValueLabel(lblAdjustTaxAmount),
                            DifferAmount = getValueLabel(lblDifTaxAmount),
                            LateDays = int.Parse(txtDateDelay.Value.ToString()),
                            LateAmount = getValueMaskedEdit(txtLateAmount),
                            ExplainAmount = getValueMaskedEdit(txtExplainAmount),
                            CommandNo = txtCommandNo.Text,
                            CommandDate = dteDate.DateTime,
                            TaxCompanyName = txtTaxCompanyName.Text,
                            TaxCompanyDecisionName = txtTaxCompanyDecisionName.Text,
                            ReceiveDays = int.Parse(txtReceiveDays.Value.ToString()),
                            ExplainLateAmount = getValueMaskedEdit(txtExplainLateAmount),
                            DifferReason = txtReason.Text,
                            OrderPriority = 1,
                            Type = 3
                        });
                    }

                }
                #endregion
            }
            #endregion

            #region Lấy DB lên
            else
            {
                if (input.IsMonth == true)
                {
                    txtIsArisingTax.Visible = false;
                    ultraLabel6.Visible = false;
                    lblArisingTime.Location = new Point(433, 92);
                    lblArisingTime.Text = input.DeclarationTerm;

                }
                if (input.IsQuarter == true)
                {
                    txtIsArisingTax.Visible = false;
                    ultraLabel6.Visible = false;
                    lblArisingTime.Location = new Point(433, 92);
                    lblArisingTime.Text = input.DeclarationTerm;
                }
                if (input.IsArising == true)
                {
                    txtIsArisingTax.Text = "[X]";
                    lblArisingTime.Text = input.DeclarationTerm;
                }
                if (input.IsFirstDeclaration == true)
                {
                    txtIsFirst.Text = "[x]";
                    lblAdditionTime.Text = "";
                    ultraTabControl1.Tabs[1].Visible = false;
                    utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Visible = false;
                }
                else
                {
                    txtIsFirst.Text = "[]";
                    lblAdditionTime.Text = input.AdditionTime.ToString();
                    ultraTabControl1.Tabs[1].Visible = true;
                }
                FromDate = input.FromDate;
                ToDate = input.ToDate;
                lblCompanyName.Text = input.CompanyName;
                lblCompanyTaxCode.Text = input.CompanyTaxCode;
                lblDL_Thue.Text = input.TaxAgencyName;
                lblMST_DL.Text = input.TaxAgencyTaxCode;
                txtName.Text = input.TaxAgencyEmployeeName;
                txtChungChi.Text = input.CertificationNo;
                txtSignName.Text = input.SignName;
                txtSignDate.DateTime = input.SignDate ?? DateTime.Now;
                ultraLabel154.Text = input.AdditionTerm;

                #region tờ khai chính
                BindData(txtItem21, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item21" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem22, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item22" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem23, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item23" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem24, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item24" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem25, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item25" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem26, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item26" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem27, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item27" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem28, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item28" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem29, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item29" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem30, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item30" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem31, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item31" && n.TM04GTGTID == input.ID).Data));
                txtItem32.FormatNumberic(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item32" && n.TM04GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                txtItem33.FormatNumberic(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item33" && n.TM04GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                #endregion

                #region KHBS
                if (ultraTabControl1.Tabs[1].Visible == true)
                {
                    #region table 8
                    var lstTM04GTGTAdjust1 = new List<TM04GTGTAdjust>();
                    lstTM04GTGTAdjust1 = input.TM04GTGTAdjusts.Where(n => n.Type == 1 && n.TM04GTGTID == input.ID).OrderBy(n => n.OrderPriority).ToList();
                    foreach (var item in lstTM04GTGTAdjust1)
                    {
                        if (lstTM04GTGTAdjust1.IndexOf(item) == 0)
                        {
                            var control1 = tableLayoutPanel8.GetControlFromPosition(1, 1);
                            (control1 as UltraTextEditor).Value = item.Name;
                            var control2 = tableLayoutPanel8.GetControlFromPosition(2, 1);
                            (control2 as UltraTextEditor).Value = item.Code;
                            var control3 = tableLayoutPanel8.GetControlFromPosition(3, 1);
                            (control3 as UltraTextEditor).Value = item.DeclaredAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                            var control4 = tableLayoutPanel8.GetControlFromPosition(4, 1);
                            (control4 as UltraTextEditor).Value = item.AdjustAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                            var control5 = tableLayoutPanel8.GetControlFromPosition(5, 1);
                            (control5 as UltraTextEditor).Value = item.DifferAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                        }
                        else
                        {
                            RowStyle temp = tableLayoutPanel8.RowStyles[tableLayoutPanel8.RowCount - 1];
                            //increase panel rows count by one
                            tableLayoutPanel8.RowCount++;
                            int h = tableLayoutPanel8.Height;
                            //tableLayoutPanel1.Height = h + (int)temp.Height;
                            RowStyle rowStyle = new RowStyle(temp.SizeType, temp.Height);

                            //add a new RowStyle as a copy of the previous one
                            tableLayoutPanel8.RowStyles.Add(rowStyle);

                            UltraLabel txtSTT = new UltraLabel();
                            txtSTT.Text = item.OrderPriority.ToString();
                            txtSTT.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                            txtSTT.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtSTT.AutoSize = false;
                            txtSTT.Dock = DockStyle.Fill;
                            txtSTT.Margin = new Padding(0);

                            UltraTextEditor txtName = new UltraTextEditor();
                            txtName.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                            txtName.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtName.AutoSize = false;
                            txtName.Dock = DockStyle.Fill;
                            txtName.Margin = new Padding(0);
                            txtName.Text = item.Name;

                            UltraTextEditor txtCode = new UltraTextEditor();
                            txtCode.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                            txtCode.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtCode.AutoSize = false;
                            txtCode.Dock = DockStyle.Fill;
                            txtCode.Margin = new Padding(0);
                            txtCode.Text = item.Code;

                            UltraTextEditor txtDeclaredAmount = new UltraTextEditor();
                            txtDeclaredAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            txtDeclaredAmount.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtDeclaredAmount.AutoSize = false;
                            txtDeclaredAmount.Dock = DockStyle.Fill;
                            txtDeclaredAmount.Margin = new Padding(0);
                            txtDeclaredAmount.Text = item.DeclaredAmount.FormatNumberic(ConstDatabase.Format_TienVND);

                            UltraTextEditor txtAdjustAmount = new UltraTextEditor();
                            txtAdjustAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            txtAdjustAmount.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtAdjustAmount.AutoSize = false;
                            txtAdjustAmount.Dock = DockStyle.Fill;
                            txtAdjustAmount.Margin = new Padding(0);
                            txtAdjustAmount.Text = item.AdjustAmount.FormatNumberic(ConstDatabase.Format_TienVND);

                            UltraTextEditor txtDifferAmount = new UltraTextEditor();
                            txtDifferAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            txtDifferAmount.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtDifferAmount.AutoSize = false;
                            txtDifferAmount.Dock = DockStyle.Fill;
                            txtDifferAmount.Margin = new Padding(0);
                            txtDifferAmount.Text = item.DifferAmount.FormatNumberic(ConstDatabase.Format_TienVND);

                            tableLayoutPanel8.Controls.Add(txtSTT, 0, tableLayoutPanel8.RowCount - 1);
                            tableLayoutPanel8.Controls.Add(txtName, 1, tableLayoutPanel8.RowCount - 1);
                            tableLayoutPanel8.Controls.Add(txtCode, 2, tableLayoutPanel8.RowCount - 1);
                            tableLayoutPanel8.Controls.Add(txtDeclaredAmount, 3, tableLayoutPanel8.RowCount - 1);
                            tableLayoutPanel8.Controls.Add(txtAdjustAmount, 4, tableLayoutPanel8.RowCount - 1);
                            tableLayoutPanel8.Controls.Add(txtDifferAmount, 5, tableLayoutPanel8.RowCount - 1);

                            tableLayoutPanel9.Location = new Point(tableLayoutPanel9.Location.X, tableLayoutPanel8.Location.Y + tableLayoutPanel8.Height);
                            tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, tableLayoutPanel9.Location.Y + tableLayoutPanel9.Height);
                            ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);
                        }
                    }
                    #endregion

                    #region table 9
                    var lstTM04GTGTAdjust2 = new List<TM04GTGTAdjust>();
                    lstTM04GTGTAdjust2 = input.TM04GTGTAdjusts.Where(n => n.Type == 2 && n.TM04GTGTID == input.ID).OrderBy(n => n.OrderPriority).ToList();
                    foreach (var item in lstTM04GTGTAdjust2)
                    {
                        if (lstTM04GTGTAdjust2.IndexOf(item) == 0)
                        {
                            var control1 = tableLayoutPanel9.GetControlFromPosition(1, 1);
                            (control1 as UltraTextEditor).Value = item.Name;
                            var control2 = tableLayoutPanel9.GetControlFromPosition(2, 1);
                            (control2 as UltraTextEditor).Value = item.Code;
                            var control3 = tableLayoutPanel9.GetControlFromPosition(3, 1);
                            (control3 as UltraTextEditor).Value = item.DeclaredAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                            var control4 = tableLayoutPanel9.GetControlFromPosition(4, 1);
                            (control4 as UltraTextEditor).Value = item.AdjustAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                            var control5 = tableLayoutPanel9.GetControlFromPosition(5, 1);
                            (control5 as UltraTextEditor).Value = item.DifferAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                        }
                        else
                        {
                            RowStyle temp = tableLayoutPanel9.RowStyles[tableLayoutPanel9.RowCount - 1];
                            //increase panel rows count by one
                            tableLayoutPanel9.RowCount++;
                            int h = tableLayoutPanel9.Height;
                            //tableLayoutPanel1.Height = h + (int)temp.Height;
                            RowStyle rowStyle = new RowStyle(temp.SizeType, temp.Height);

                            //add a new RowStyle as a copy of the previous one
                            tableLayoutPanel9.RowStyles.Add(rowStyle);

                            UltraLabel txtSTT = new UltraLabel();
                            txtSTT.Text = item.OrderPriority.ToString();
                            txtSTT.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                            txtSTT.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtSTT.AutoSize = false;
                            txtSTT.Dock = DockStyle.Fill;
                            txtSTT.Margin = new Padding(0);

                            UltraTextEditor txtName = new UltraTextEditor();
                            txtName.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                            txtName.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtName.AutoSize = false;
                            txtName.Dock = DockStyle.Fill;
                            txtName.Margin = new Padding(0);
                            txtName.Text = item.Name;

                            UltraTextEditor txtCode = new UltraTextEditor();
                            txtCode.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                            txtCode.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtCode.AutoSize = false;
                            txtCode.Dock = DockStyle.Fill;
                            txtCode.Margin = new Padding(0);
                            txtCode.Text = item.Code;

                            UltraTextEditor txtDeclaredAmount = new UltraTextEditor();
                            txtDeclaredAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            txtDeclaredAmount.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtDeclaredAmount.AutoSize = false;
                            txtDeclaredAmount.Dock = DockStyle.Fill;
                            txtDeclaredAmount.Margin = new Padding(0);
                            txtDeclaredAmount.Text = item.DeclaredAmount.FormatNumberic(ConstDatabase.Format_TienVND);

                            UltraTextEditor txtAdjustAmount = new UltraTextEditor();
                            txtAdjustAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            txtAdjustAmount.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtAdjustAmount.AutoSize = false;
                            txtAdjustAmount.Dock = DockStyle.Fill;
                            txtAdjustAmount.Margin = new Padding(0);
                            txtAdjustAmount.Text = item.AdjustAmount.FormatNumberic(ConstDatabase.Format_TienVND);

                            UltraTextEditor txtDifferAmount = new UltraTextEditor();
                            txtDifferAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            txtDifferAmount.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtDifferAmount.AutoSize = false;
                            txtDifferAmount.Dock = DockStyle.Fill;
                            txtDifferAmount.Margin = new Padding(0);
                            txtDifferAmount.Text = item.DifferAmount.FormatNumberic(ConstDatabase.Format_TienVND);

                            tableLayoutPanel9.Controls.Add(txtSTT, 0, tableLayoutPanel9.RowCount - 1);
                            tableLayoutPanel9.Controls.Add(txtName, 1, tableLayoutPanel9.RowCount - 1);
                            tableLayoutPanel9.Controls.Add(txtCode, 2, tableLayoutPanel9.RowCount - 1);
                            tableLayoutPanel9.Controls.Add(txtDeclaredAmount, 3, tableLayoutPanel9.RowCount - 1);
                            tableLayoutPanel9.Controls.Add(txtAdjustAmount, 4, tableLayoutPanel9.RowCount - 1);
                            tableLayoutPanel9.Controls.Add(txtDifferAmount, 5, tableLayoutPanel9.RowCount - 1);

                            tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, tableLayoutPanel9.Location.Y + tableLayoutPanel9.Height);
                            ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);
                        }
                    }
                    #endregion

                    var _tM04GTGTAdjust33 = new TM04GTGTAdjust();
                    _tM04GTGTAdjust33 = input.TM04GTGTAdjusts.Where(n => n.Code == "33" && n.TM04GTGTID == input.ID).FirstOrDefault();
                    if (_tM04GTGTAdjust33 != null)
                    {
                        lblCodeItem40.Text = _tM04GTGTAdjust33.Code ?? "";
                        lblDeclaredTaxAmount.FormatNumberic(_tM04GTGTAdjust33.DeclaredAmount.ToString() ?? "0", ConstDatabase.Format_TienVND);
                        lblAdjustTaxAmount.FormatNumberic(_tM04GTGTAdjust33.AdjustAmount.ToString() ?? "0", ConstDatabase.Format_TienVND);
                        lblDifTaxAmount.FormatNumberic(_tM04GTGTAdjust33.DifferAmount.ToString() ?? "0", ConstDatabase.Format_TienVND);
                        txtDateDelay.Value = _tM04GTGTAdjust33.LateDays ?? 0;
                        BindData(txtLateAmount, _tM04GTGTAdjust33.LateAmount);
                        BindData(txtExplainAmount, _tM04GTGTAdjust33.ExplainAmount);
                        txtCommandNo.Text = _tM04GTGTAdjust33.CommandNo ?? "0";
                        dteDate.DateTime = _tM04GTGTAdjust33.CommandDate ?? DateTime.Now;
                        txtTaxCompanyName.Text = _tM04GTGTAdjust33.TaxCompanyName ?? "";
                        txtTaxCompanyDecisionName.Text = _tM04GTGTAdjust33.TaxCompanyDecisionName ?? "";
                        txtReceiveDays.Value = _tM04GTGTAdjust33.ReceiveDays ?? 0;
                        BindData(txtExplainLateAmount, _tM04GTGTAdjust33.ExplainLateAmount);
                        txtReason.Text = _tM04GTGTAdjust33.DifferReason ?? "";
                    }


                }
                #endregion
            }
            #endregion
            return input;
        }
        #endregion

        #region ObjandGUIKHBS
        private TM04GTGT ObjandGUIKHBS(TM04GTGT input, bool isGet)
        {
            if (isGet)
            { }

            #region Lấy DB lên
            else
            {
                if (input.IsMonth == true)
                {
                    txtIsArisingTax.Visible = false;
                    ultraLabel6.Visible = false;
                    lblArisingTime.Location = new Point(433, 92);
                    lblArisingTime.Text = input.DeclarationTerm;

                }
                if (input.IsQuarter == true)
                {
                    txtIsArisingTax.Visible = false;
                    ultraLabel6.Visible = false;
                    lblArisingTime.Location = new Point(433, 92);
                    lblArisingTime.Text = input.DeclarationTerm;
                }
                if (input.IsArising == true)
                {
                    txtIsArisingTax.Text = "[X]";
                    lblArisingTime.Text = input.DeclarationTerm;
                }
                txtIsFirst.Text = "[]";
                //lblAdditionTime.Text = input.AdditionTime.ToString();
                ultraTabControl1.Tabs[1].Visible = true;
                utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Visible = true;
                lblCompanyName.Text = input.CompanyName;
                lblCompanyTaxCode.Text = input.CompanyTaxCode;
                lblDL_Thue.Text = input.TaxAgencyName;
                lblMST_DL.Text = input.TaxAgencyTaxCode;
                txtName.Text = input.TaxAgencyEmployeeName;
                txtChungChi.Text = input.CertificationNo;
                txtSignName.Text = input.SignName;
                txtSignDate.DateTime = input.SignDate ?? DateTime.Now;

                #region tờ khai chính
                BindData(txtItem21, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item21" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem22, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item22" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem23, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item23" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem24, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item24" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem25, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item25" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem26, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item26" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem27, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item27" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem28, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item28" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem29, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item29" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem30, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item30" && n.TM04GTGTID == input.ID).Data));
                BindData(txtItem31, decimal.Parse(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item31" && n.TM04GTGTID == input.ID).Data));
                txtItem32.FormatNumberic(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item32" && n.TM04GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                txtItem33.FormatNumberic(input.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item33" && n.TM04GTGTID == input.ID).Data, ConstDatabase.Format_TienVND);
                #endregion

                #region KHBS
                if (ultraTabControl1.Tabs[1].Visible == true)
                {
                    #region table 8
                    var lstTM04GTGTAdjust1 = new List<TM04GTGTAdjust>();
                    lstTM04GTGTAdjust1 = input.TM04GTGTAdjusts.Where(n => n.Type == 1 && n.TM04GTGTID == input.ID).OrderBy(n => n.OrderPriority).ToList();
                    foreach (var item in lstTM04GTGTAdjust1)
                    {
                        if (lstTM04GTGTAdjust1.IndexOf(item) == 0)
                        {
                            var control1 = tableLayoutPanel8.GetControlFromPosition(1, 1);
                            (control1 as UltraTextEditor).Value = item.Name;
                            var control2 = tableLayoutPanel8.GetControlFromPosition(2, 1);
                            (control2 as UltraTextEditor).Value = item.Code;
                            var control3 = tableLayoutPanel8.GetControlFromPosition(3, 1);
                            (control3 as UltraTextEditor).Value = item.DeclaredAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                            var control4 = tableLayoutPanel8.GetControlFromPosition(4, 1);
                            (control4 as UltraTextEditor).Value = item.AdjustAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                            var control5 = tableLayoutPanel8.GetControlFromPosition(5, 1);
                            (control5 as UltraTextEditor).Value = item.DifferAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                        }
                        else
                        {
                            RowStyle temp = tableLayoutPanel8.RowStyles[tableLayoutPanel8.RowCount - 1];
                            //increase panel rows count by one
                            tableLayoutPanel8.RowCount++;
                            int h = tableLayoutPanel8.Height;
                            //tableLayoutPanel1.Height = h + (int)temp.Height;
                            RowStyle rowStyle = new RowStyle(temp.SizeType, temp.Height);

                            //add a new RowStyle as a copy of the previous one
                            tableLayoutPanel8.RowStyles.Add(rowStyle);

                            UltraLabel txtSTT = new UltraLabel();
                            txtSTT.Text = item.OrderPriority.ToString();
                            txtSTT.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                            txtSTT.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtSTT.AutoSize = false;
                            txtSTT.Dock = DockStyle.Fill;
                            txtSTT.Margin = new Padding(0);

                            UltraTextEditor txtName = new UltraTextEditor();
                            txtName.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                            txtName.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtName.AutoSize = false;
                            txtName.Dock = DockStyle.Fill;
                            txtName.Margin = new Padding(0);
                            txtName.Text = item.Name;

                            UltraTextEditor txtCode = new UltraTextEditor();
                            txtCode.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                            txtCode.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtCode.AutoSize = false;
                            txtCode.Dock = DockStyle.Fill;
                            txtCode.Margin = new Padding(0);
                            txtCode.Text = item.Code;

                            UltraTextEditor txtDeclaredAmount = new UltraTextEditor();
                            txtDeclaredAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            txtDeclaredAmount.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtDeclaredAmount.AutoSize = false;
                            txtDeclaredAmount.Dock = DockStyle.Fill;
                            txtDeclaredAmount.Margin = new Padding(0);
                            txtDeclaredAmount.Text = item.DeclaredAmount.FormatNumberic(ConstDatabase.Format_TienVND);

                            UltraTextEditor txtAdjustAmount = new UltraTextEditor();
                            txtAdjustAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            txtAdjustAmount.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtAdjustAmount.AutoSize = false;
                            txtAdjustAmount.Dock = DockStyle.Fill;
                            txtAdjustAmount.Margin = new Padding(0);
                            txtAdjustAmount.Text = item.AdjustAmount.FormatNumberic(ConstDatabase.Format_TienVND);

                            UltraTextEditor txtDifferAmount = new UltraTextEditor();
                            txtDifferAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            txtDifferAmount.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtDifferAmount.AutoSize = false;
                            txtDifferAmount.Dock = DockStyle.Fill;
                            txtDifferAmount.Margin = new Padding(0);
                            txtDifferAmount.Text = item.DifferAmount.FormatNumberic(ConstDatabase.Format_TienVND);

                            tableLayoutPanel8.Controls.Add(txtSTT, 0, tableLayoutPanel8.RowCount - 1);
                            tableLayoutPanel8.Controls.Add(txtName, 1, tableLayoutPanel8.RowCount - 1);
                            tableLayoutPanel8.Controls.Add(txtCode, 2, tableLayoutPanel8.RowCount - 1);
                            tableLayoutPanel8.Controls.Add(txtDeclaredAmount, 3, tableLayoutPanel8.RowCount - 1);
                            tableLayoutPanel8.Controls.Add(txtAdjustAmount, 4, tableLayoutPanel8.RowCount - 1);
                            tableLayoutPanel8.Controls.Add(txtDifferAmount, 5, tableLayoutPanel8.RowCount - 1);

                            tableLayoutPanel9.Location = new Point(tableLayoutPanel9.Location.X, tableLayoutPanel8.Location.Y + tableLayoutPanel8.Height);
                            tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, tableLayoutPanel9.Location.Y + tableLayoutPanel9.Height);
                            ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);
                        }
                    }
                    #endregion

                    #region table 9
                    var lstTM04GTGTAdjust2 = new List<TM04GTGTAdjust>();
                    lstTM04GTGTAdjust2 = input.TM04GTGTAdjusts.Where(n => n.Type == 2 && n.TM04GTGTID == input.ID).OrderBy(n => n.OrderPriority).ToList();
                    foreach (var item in lstTM04GTGTAdjust2)
                    {
                        if (lstTM04GTGTAdjust2.IndexOf(item) == 0)
                        {
                            var control1 = tableLayoutPanel9.GetControlFromPosition(1, 1);
                            (control1 as UltraTextEditor).Value = item.Name;
                            var control2 = tableLayoutPanel9.GetControlFromPosition(2, 1);
                            (control2 as UltraTextEditor).Value = item.Code;
                            var control3 = tableLayoutPanel9.GetControlFromPosition(3, 1);
                            (control3 as UltraTextEditor).Value = item.DeclaredAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                            var control4 = tableLayoutPanel9.GetControlFromPosition(4, 1);
                            (control4 as UltraTextEditor).Value = item.AdjustAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                            var control5 = tableLayoutPanel9.GetControlFromPosition(5, 1);
                            (control5 as UltraTextEditor).Value = item.DifferAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                        }
                        else
                        {
                            RowStyle temp = tableLayoutPanel9.RowStyles[tableLayoutPanel9.RowCount - 1];
                            //increase panel rows count by one
                            tableLayoutPanel9.RowCount++;
                            int h = tableLayoutPanel9.Height;
                            //tableLayoutPanel1.Height = h + (int)temp.Height;
                            RowStyle rowStyle = new RowStyle(temp.SizeType, temp.Height);

                            //add a new RowStyle as a copy of the previous one
                            tableLayoutPanel9.RowStyles.Add(rowStyle);

                            UltraLabel txtSTT = new UltraLabel();
                            txtSTT.Text = item.OrderPriority.ToString();
                            txtSTT.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                            txtSTT.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtSTT.AutoSize = false;
                            txtSTT.Dock = DockStyle.Fill;
                            txtSTT.Margin = new Padding(0);

                            UltraTextEditor txtName = new UltraTextEditor();
                            txtName.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                            txtName.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtName.AutoSize = false;
                            txtName.Dock = DockStyle.Fill;
                            txtName.Margin = new Padding(0);
                            txtName.Text = item.Name;

                            UltraTextEditor txtCode = new UltraTextEditor();
                            txtCode.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                            txtCode.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtCode.AutoSize = false;
                            txtCode.Dock = DockStyle.Fill;
                            txtCode.Margin = new Padding(0);
                            txtCode.Text = item.Code;

                            UltraTextEditor txtDeclaredAmount = new UltraTextEditor();
                            txtDeclaredAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            txtDeclaredAmount.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtDeclaredAmount.AutoSize = false;
                            txtDeclaredAmount.Dock = DockStyle.Fill;
                            txtDeclaredAmount.Margin = new Padding(0);
                            txtDeclaredAmount.Text = item.DeclaredAmount.FormatNumberic(ConstDatabase.Format_TienVND);

                            UltraTextEditor txtAdjustAmount = new UltraTextEditor();
                            txtAdjustAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            txtAdjustAmount.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtAdjustAmount.AutoSize = false;
                            txtAdjustAmount.Dock = DockStyle.Fill;
                            txtAdjustAmount.Margin = new Padding(0);
                            txtAdjustAmount.Text = item.AdjustAmount.FormatNumberic(ConstDatabase.Format_TienVND);

                            UltraTextEditor txtDifferAmount = new UltraTextEditor();
                            txtDifferAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            txtDifferAmount.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                            txtDifferAmount.AutoSize = false;
                            txtDifferAmount.Dock = DockStyle.Fill;
                            txtDifferAmount.Margin = new Padding(0);
                            txtDifferAmount.Text = item.DifferAmount.FormatNumberic(ConstDatabase.Format_TienVND);

                            tableLayoutPanel9.Controls.Add(txtSTT, 0, tableLayoutPanel9.RowCount - 1);
                            tableLayoutPanel9.Controls.Add(txtName, 1, tableLayoutPanel9.RowCount - 1);
                            tableLayoutPanel9.Controls.Add(txtCode, 2, tableLayoutPanel9.RowCount - 1);
                            tableLayoutPanel9.Controls.Add(txtDeclaredAmount, 3, tableLayoutPanel9.RowCount - 1);
                            tableLayoutPanel9.Controls.Add(txtAdjustAmount, 4, tableLayoutPanel9.RowCount - 1);
                            tableLayoutPanel9.Controls.Add(txtDifferAmount, 5, tableLayoutPanel9.RowCount - 1);

                            tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, tableLayoutPanel9.Location.Y + tableLayoutPanel9.Height);
                            ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);
                        }
                    }
                    #endregion

                    var _tM04GTGTAdjust33 = new TM04GTGTAdjust();
                    _tM04GTGTAdjust33 = input.TM04GTGTAdjusts.Where(n => n.Code == "33" && n.TM04GTGTID == input.ID).FirstOrDefault();
                    if (_tM04GTGTAdjust33 != null)
                    {
                        lblCodeItem40.Text = _tM04GTGTAdjust33.Code ?? "";
                        lblDeclaredTaxAmount.FormatNumberic(_tM04GTGTAdjust33.DeclaredAmount.ToString() ?? "0", ConstDatabase.Format_TienVND);
                        lblAdjustTaxAmount.FormatNumberic(_tM04GTGTAdjust33.AdjustAmount.ToString() ?? "0", ConstDatabase.Format_TienVND);
                        lblDifTaxAmount.FormatNumberic(_tM04GTGTAdjust33.DifferAmount.ToString() ?? "0", ConstDatabase.Format_TienVND);
                        txtDateDelay.Value = _tM04GTGTAdjust33.LateDays ?? 0;
                        BindData(txtLateAmount, _tM04GTGTAdjust33.LateAmount);
                        BindData(txtExplainAmount, _tM04GTGTAdjust33.ExplainAmount);
                        txtCommandNo.Text = _tM04GTGTAdjust33.CommandNo ?? "0";
                        dteDate.DateTime = _tM04GTGTAdjust33.CommandDate ?? DateTime.Now;
                        txtTaxCompanyName.Text = _tM04GTGTAdjust33.TaxCompanyName ?? "";
                        txtTaxCompanyDecisionName.Text = _tM04GTGTAdjust33.TaxCompanyDecisionName ?? "";
                        txtReceiveDays.Value = _tM04GTGTAdjust33.ReceiveDays ?? 0;
                        BindData(txtExplainLateAmount, _tM04GTGTAdjust33.ExplainLateAmount);
                        txtReason.Text = _tM04GTGTAdjust33.DifferReason ?? "";
                    }


                }
                #endregion
            }
            #endregion
            return input;
        }
        #endregion       

        #region Format dấu
        private decimal getValueTextEdit(UltraTextEditor ultraMaskedEdit)
        {
            if (!ultraMaskedEdit.Value.IsNullOrEmpty())
            {
                if (!ultraMaskedEdit.Text.IsNullOrEmpty())
                {
                    if (ultraMaskedEdit.Text.Contains("("))
                    {
                        return -decimal.Parse(ultraMaskedEdit.Value.ToString().Trim('(', ')'));
                    }
                    else
                    {
                        return decimal.Parse(ultraMaskedEdit.Value.ToString());
                    }
                }
            }
            return 0;
        }
        private decimal getValueLabel(UltraLabel ultraMaskedEdit)
        {
            if (!ultraMaskedEdit.Text.IsNullOrEmpty())
            {
                if (ultraMaskedEdit.Text.Contains("("))
                {
                    return -decimal.Parse(ultraMaskedEdit.Text.Trim('(', ')'));
                }
                else
                {
                    return decimal.Parse(ultraMaskedEdit.Text);
                }
            }
            return 0;
        }
        public bool BindData(UltraMaskedEdit ultraMaskedEdit, decimal r)
        {
            try
            {
                if (r < 0)
                {
                    ultraMaskedEdit.InputMask = "(n,nnn,nnn,nnn,nnn,nnn)";
                    ultraMaskedEdit.Value = -r;
                }
                else
                {
                    ultraMaskedEdit.InputMask = "n,nnn,nnn,nnn,nnn,nnn";
                    ultraMaskedEdit.Value = r;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        private decimal getValueMaskedEdit(UltraMaskedEdit ultraMaskedEdit)
        {
            if (!ultraMaskedEdit.Value.IsNullOrEmpty())
            {
                if (ultraMaskedEdit.Text.Contains("("))
                {
                    return -decimal.Parse(ultraMaskedEdit.Value.ToString());
                }
                else
                {
                    return decimal.Parse(ultraMaskedEdit.Value.ToString());
                }
            }
            return 0;
        }
        #endregion

        #region check lỗi

        #endregion

        #region Load Grid tab KBS
        #region Grid 1
        public void LoadUgridKBS1(List<TM04GTGTAdjust> lstTaxP)
        {
            if (lstTaxP.Count != 0)
            {
                foreach (var item in lstTaxP)
                {
                    if (lstTaxP.IndexOf(item) == 0)
                    {
                        var control1 = tableLayoutPanel8.GetControlFromPosition(1, 1);
                        (control1 as UltraTextEditor).Value = item.Name;
                        var control2 = tableLayoutPanel8.GetControlFromPosition(2, 1);
                        (control2 as UltraTextEditor).Value = item.Code;
                        var control3 = tableLayoutPanel8.GetControlFromPosition(3, 1);
                        (control3 as UltraTextEditor).Value = item.DeclaredAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                        var control4 = tableLayoutPanel8.GetControlFromPosition(4, 1);
                        (control4 as UltraTextEditor).Value = item.AdjustAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                        var control5 = tableLayoutPanel8.GetControlFromPosition(5, 1);
                        (control5 as UltraTextEditor).Value = item.DifferAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                    }
                    else
                    {
                        RowStyle temp = tableLayoutPanel8.RowStyles[tableLayoutPanel8.RowCount - 1];
                        tableLayoutPanel8.RowCount++;
                        RowStyle rowStyle = new RowStyle(temp.SizeType, temp.Height);
                        tableLayoutPanel8.RowStyles.Add(rowStyle);

                        UltraLabel txtSTT = new UltraLabel();
                        txtSTT.Text = (tableLayoutPanel8.RowCount - 1).ToString();
                        txtSTT.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        txtSTT.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                        txtSTT.AutoSize = false;
                        txtSTT.Dock = DockStyle.Fill;
                        txtSTT.Margin = new Padding(0);

                        UltraTextEditor txtName = new UltraTextEditor();
                        txtName.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                        txtName.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                        txtName.AutoSize = false;
                        txtName.Dock = DockStyle.Fill;
                        txtName.Margin = new Padding(0);
                        txtName.Text = item.Name;

                        UltraTextEditor txtCode = new UltraTextEditor();
                        txtCode.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        txtCode.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                        txtCode.AutoSize = false;
                        txtCode.Dock = DockStyle.Fill;
                        txtCode.Margin = new Padding(0);
                        txtCode.Text = item.Code;

                        UltraTextEditor txtDeclaredAmount = new UltraTextEditor();
                        txtDeclaredAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        txtDeclaredAmount.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                        txtDeclaredAmount.AutoSize = false;
                        txtDeclaredAmount.Dock = DockStyle.Fill;
                        txtDeclaredAmount.Margin = new Padding(0);
                        txtDeclaredAmount.Text = item.DeclaredAmount.FormatNumberic(ConstDatabase.Format_TienVND);

                        UltraTextEditor txtAdjustAmount = new UltraTextEditor();
                        txtAdjustAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        txtAdjustAmount.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                        txtAdjustAmount.AutoSize = false;
                        txtAdjustAmount.Dock = DockStyle.Fill;
                        txtAdjustAmount.Margin = new Padding(0);
                        txtAdjustAmount.Text = item.AdjustAmount.FormatNumberic(ConstDatabase.Format_TienVND);

                        UltraTextEditor txtDifferAmount = new UltraTextEditor();
                        txtDifferAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        txtDifferAmount.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                        txtDifferAmount.AutoSize = false;
                        txtDifferAmount.Dock = DockStyle.Fill;
                        txtDifferAmount.Margin = new Padding(0);
                        txtDifferAmount.Text = item.DifferAmount.FormatNumberic(ConstDatabase.Format_TienVND);

                        tableLayoutPanel8.Controls.Add(txtSTT, 0, tableLayoutPanel8.RowCount - 1);
                        tableLayoutPanel8.Controls.Add(txtName, 1, tableLayoutPanel8.RowCount - 1);
                        tableLayoutPanel8.Controls.Add(txtCode, 2, tableLayoutPanel8.RowCount - 1);
                        tableLayoutPanel8.Controls.Add(txtDeclaredAmount, 3, tableLayoutPanel8.RowCount - 1);
                        tableLayoutPanel8.Controls.Add(txtAdjustAmount, 4, tableLayoutPanel8.RowCount - 1);
                        tableLayoutPanel8.Controls.Add(txtDifferAmount, 5, tableLayoutPanel8.RowCount - 1);

                        tableLayoutPanel9.Location = new Point(tableLayoutPanel9.Location.X, tableLayoutPanel8.Location.Y + tableLayoutPanel8.Height);
                        tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, tableLayoutPanel9.Location.Y + tableLayoutPanel9.Height);
                        ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);
                    }
                }
            }
        }
        #endregion

        #region Grid 2
        public void LoadUgridKBS2(List<TM04GTGTAdjust> lstTaxP)
        {
            if (lstTaxP.Count != 0)
            {
                foreach (var item in lstTaxP)
                {
                    if (lstTaxP.IndexOf(item) == 0)
                    {
                        var control1 = tableLayoutPanel9.GetControlFromPosition(1, 1);
                        (control1 as UltraTextEditor).Value = item.Name;
                        var control2 = tableLayoutPanel9.GetControlFromPosition(2, 1);
                        (control2 as UltraTextEditor).Value = item.Code;
                        var control3 = tableLayoutPanel9.GetControlFromPosition(3, 1);
                        (control3 as UltraTextEditor).Value = item.DeclaredAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                        var control4 = tableLayoutPanel9.GetControlFromPosition(4, 1);
                        (control4 as UltraTextEditor).Value = item.AdjustAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                        var control5 = tableLayoutPanel9.GetControlFromPosition(5, 1);
                        (control5 as UltraTextEditor).Value = item.DifferAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                    }
                    else
                    {
                        RowStyle temp = tableLayoutPanel9.RowStyles[tableLayoutPanel9.RowCount - 1];
                        tableLayoutPanel9.RowCount++;
                        RowStyle rowStyle = new RowStyle(temp.SizeType, temp.Height);
                        tableLayoutPanel9.RowStyles.Add(rowStyle);

                        UltraLabel txtSTT = new UltraLabel();
                        txtSTT.Text = (tableLayoutPanel9.RowCount - 1).ToString();
                        txtSTT.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        txtSTT.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                        txtSTT.AutoSize = false;
                        txtSTT.Dock = DockStyle.Fill;
                        txtSTT.Margin = new Padding(0);

                        UltraTextEditor txtName = new UltraTextEditor();
                        txtName.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                        txtName.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                        txtName.AutoSize = false;
                        txtName.Dock = DockStyle.Fill;
                        txtName.Margin = new Padding(0);
                        txtName.Text = item.Name;

                        UltraTextEditor txtCode = new UltraTextEditor();
                        txtCode.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                        txtCode.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                        txtCode.AutoSize = false;
                        txtCode.Dock = DockStyle.Fill;
                        txtCode.Margin = new Padding(0);
                        txtCode.Text = item.Code;

                        UltraTextEditor txtDeclaredAmount = new UltraTextEditor();
                        txtDeclaredAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        txtDeclaredAmount.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                        txtDeclaredAmount.AutoSize = false;
                        txtDeclaredAmount.Dock = DockStyle.Fill;
                        txtDeclaredAmount.Margin = new Padding(0);
                        txtDeclaredAmount.Text = item.DeclaredAmount.FormatNumberic(ConstDatabase.Format_TienVND);

                        UltraTextEditor txtAdjustAmount = new UltraTextEditor();
                        txtAdjustAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        txtAdjustAmount.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                        txtAdjustAmount.AutoSize = false;
                        txtAdjustAmount.Dock = DockStyle.Fill;
                        txtAdjustAmount.Margin = new Padding(0);
                        txtAdjustAmount.Text = item.AdjustAmount.FormatNumberic(ConstDatabase.Format_TienVND);

                        UltraTextEditor txtDifferAmount = new UltraTextEditor();
                        txtDifferAmount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        txtDifferAmount.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                        txtDifferAmount.AutoSize = false;
                        txtDifferAmount.Dock = DockStyle.Fill;
                        txtDifferAmount.Margin = new Padding(0);
                        txtDifferAmount.Text = item.DifferAmount.FormatNumberic(ConstDatabase.Format_TienVND);

                        tableLayoutPanel9.Controls.Add(txtSTT, 0, tableLayoutPanel9.RowCount - 1);
                        tableLayoutPanel9.Controls.Add(txtName, 1, tableLayoutPanel9.RowCount - 1);
                        tableLayoutPanel9.Controls.Add(txtCode, 2, tableLayoutPanel9.RowCount - 1);
                        tableLayoutPanel9.Controls.Add(txtDeclaredAmount, 3, tableLayoutPanel9.RowCount - 1);
                        tableLayoutPanel9.Controls.Add(txtAdjustAmount, 4, tableLayoutPanel9.RowCount - 1);
                        tableLayoutPanel9.Controls.Add(txtDifferAmount, 5, tableLayoutPanel9.RowCount - 1);

                        tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, tableLayoutPanel9.Location.Y + tableLayoutPanel9.Height);
                        ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);
                    }
                }
            }
        }
        #endregion
        #endregion

        #region Load lại dữ liệu khi khai bổ sung
        private void LoadDataKHBS1(List<TM04GTGTAdjust> lstBS1)
        {
            #region table 8
            if (lstBS1 != null)
            {
                for (int t = 1; t < tableLayoutPanel8.RowCount; t++)
                {
                    var control0 = tableLayoutPanel8.GetControlFromPosition(0, t);
                    var control1 = tableLayoutPanel8.GetControlFromPosition(1, t);
                    var control2 = tableLayoutPanel8.GetControlFromPosition(2, t);
                    var control3 = tableLayoutPanel8.GetControlFromPosition(3, t);
                    var control4 = tableLayoutPanel8.GetControlFromPosition(4, t);
                    var control5 = tableLayoutPanel8.GetControlFromPosition(5, t);
                    if (control0 is UltraLabel)
                    {
                        if (control1 is UltraTextEditor)
                        {
                            if (control2 is UltraTextEditor)
                            {
                                if (control3 is UltraTextEditor)
                                {
                                    if (control4 is UltraTextEditor)
                                    {
                                        if (control5 is UltraTextEditor)
                                        {
                                            UltraLabel STT = control0 as UltraLabel;
                                            UltraTextEditor DeclareName = control1 as UltraTextEditor;
                                            UltraTextEditor DeclareCode = control2 as UltraTextEditor;
                                            UltraTextEditor DeclareAmount = control3 as UltraTextEditor;
                                            UltraTextEditor AdjustAmount = control4 as UltraTextEditor;
                                            UltraTextEditor DiffAmount = control5 as UltraTextEditor;
                                            if (DeclareName.Text != "")
                                            {
                                                int r = tableLayoutPanel8.RowCount - 1;
                                                if (r == 1)
                                                {
                                                    DeclareName.Text = "";
                                                    DeclareCode.Text = "";
                                                    DeclareAmount.Text = "0";
                                                    AdjustAmount.Text = "0";
                                                    DiffAmount.Text = "0";
                                                    return;
                                                }
                                                DeclareName.Focus();
                                                int row = GetRow(tableLayoutPanel8);
                                                if (row == 0) return;
                                                remove_row(tableLayoutPanel8, row);
                                                tableLayoutPanel9.Location = new Point(tableLayoutPanel9.Location.X, tableLayoutPanel8.Location.Y + tableLayoutPanel8.Height);
                                                tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, tableLayoutPanel9.Location.Y + tableLayoutPanel9.Height);
                                                ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);
                                                t = 1;
                                                for (int i = 1; i < tableLayoutPanel8.RowCount; i++)
                                                {
                                                    var control = tableLayoutPanel8.GetControlFromPosition(0, i);
                                                    if (control is UltraLabel)
                                                    {
                                                        UltraLabel ultraTextEditor = control as UltraLabel;
                                                        ultraTextEditor.Text = i.ToString();
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
            #endregion
        }
        private void LoadDataKHBS2(List<TM04GTGTAdjust> lstBS2)
        {
            #region table 9
            if (lstBS2 != null)
            {
                for (int t = 1; t < tableLayoutPanel9.RowCount; t++)
                {
                    var control0 = tableLayoutPanel9.GetControlFromPosition(0, t);
                    var control1 = tableLayoutPanel9.GetControlFromPosition(1, t);
                    var control2 = tableLayoutPanel9.GetControlFromPosition(2, t);
                    var control3 = tableLayoutPanel9.GetControlFromPosition(3, t);
                    var control4 = tableLayoutPanel9.GetControlFromPosition(4, t);
                    var control5 = tableLayoutPanel9.GetControlFromPosition(5, t);
                    if (control0 is UltraLabel)
                    {
                        if (control1 is UltraTextEditor)
                        {
                            if (control2 is UltraTextEditor)
                            {
                                if (control3 is UltraTextEditor)
                                {
                                    if (control4 is UltraTextEditor)
                                    {
                                        if (control5 is UltraTextEditor)
                                        {
                                            UltraLabel STT = control0 as UltraLabel;
                                            UltraTextEditor DeclareName = control1 as UltraTextEditor;
                                            UltraTextEditor DeclareCode = control2 as UltraTextEditor;
                                            UltraTextEditor DeclareAmount = control3 as UltraTextEditor;
                                            UltraTextEditor AdjustAmount = control4 as UltraTextEditor;
                                            UltraTextEditor DiffAmount = control5 as UltraTextEditor;
                                            if (DeclareName.Text != "")
                                            {
                                                int r = tableLayoutPanel9.RowCount - 1;
                                                if (r == 1)
                                                {
                                                    DeclareName.Text = "";
                                                    DeclareCode.Text = "";
                                                    DeclareAmount.Text = "0";
                                                    AdjustAmount.Text = "0";
                                                    DiffAmount.Text = "0";
                                                    return;
                                                }
                                                DeclareName.Focus();
                                                int row = GetRow(tableLayoutPanel9);
                                                if (row == 0) return;
                                                remove_row(tableLayoutPanel9, row);
                                                tableLayoutPanel10.Location = new Point(tableLayoutPanel10.Location.X, tableLayoutPanel9.Location.Y + tableLayoutPanel9.Height);
                                                ultraPanel1.Location = new Point(ultraPanel1.Location.X, tableLayoutPanel10.Location.Y + tableLayoutPanel10.Height);
                                                t = 1;
                                                for (int i = 1; i < tableLayoutPanel9.RowCount; i++)
                                                {
                                                    var control = tableLayoutPanel9.GetControlFromPosition(0, i);
                                                    if (control is UltraLabel)
                                                    {
                                                        UltraLabel ultraTextEditor = control as UltraLabel;
                                                        ultraTextEditor.Text = i.ToString();
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
            #endregion
        }
        #endregion

        #region chỉ tiêu nào thay đổi thì fill sang tab bổ sung
        public List<TM04GTGTAdjust> CheckChangeBS()
        {
            lst1 = new List<TM04GTGTAdjust>();
            lst2 = new List<TM04GTGTAdjust>();
            if (lblAdditionTime.Text == "1")
            {
                {
                    TM04GTGT _tm04GTGT = Utils.ListTM04GTGT.Where(n => n.FromDate == FromDate && n.AdditionTime == null).FirstOrDefault();
                    if (_tm04GTGT.TM04GTGTDetails != null)
                    {
                        foreach (var item in _tm04GTGT.TM04GTGTDetails)
                        {
                            if (item.Code == "Item23")
                            {
                                if (txtItem23.Value.ToString() != item.Data)
                                {
                                    var s1 = getValueMaskedEdit(txtItem23) - decimal.Parse(item.Data);
                                    if (s1 != 0M)
                                    {
                                        if (s1 > 0M)
                                        {
                                            lst1.Add(new TM04GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "23",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem23),
                                                DifferAmount = s1,
                                                Type = 1

                                            });


                                        }
                                        else
                                        {
                                            lst2.Add(new TM04GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "23",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem23),
                                                DifferAmount = s1,
                                                Type = 2
                                            });
                                        }
                                    }
                                }
                            }
                            if (item.Code == "Item25")
                            {
                                if (txtItem25.Value.ToString() != item.Data)
                                {
                                    var s1 = getValueMaskedEdit(txtItem25) - decimal.Parse(item.Data);
                                    if (s1 != 0M)
                                    {
                                        if (s1 > 0M)
                                        {
                                            lst1.Add(new TM04GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "25",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem25),
                                                DifferAmount = s1,
                                                Type = 1
                                            });

                                        }
                                        else
                                        {
                                            lst2.Add(new TM04GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "25",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem25),
                                                DifferAmount = s1,
                                                Type = 2
                                            });
                                        }
                                    }
                                }
                            }
                            if (item.Code == "Item27")
                            {
                                if (txtItem27.Value.ToString() != item.Data)
                                {
                                    var s1 = getValueMaskedEdit(txtItem27) - decimal.Parse(item.Data);
                                    if (s1 != 0M)
                                    {
                                        if (s1 > 0M)
                                        {
                                            lst1.Add(new TM04GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "27",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem27),
                                                DifferAmount = s1,
                                                Type = 1
                                            });

                                        }
                                        else
                                        {
                                            lst2.Add(new TM04GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "27",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem27),
                                                DifferAmount = s1,
                                                Type = 2
                                            });

                                        }
                                    }
                                }
                            }
                            if (item.Code == "Item29")
                            {
                                if (txtItem29.Value.ToString() != item.Data)
                                {
                                    var s1 = getValueMaskedEdit(txtItem29) - decimal.Parse(item.Data);
                                    if (s1 != 0M)
                                    {
                                        if (s1 > 0M)
                                        {
                                            lst1.Add(new TM04GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "29",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem29),
                                                DifferAmount = s1,
                                                Type = 1
                                            });

                                        }
                                        else
                                        {
                                            lst2.Add(new TM04GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "29",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem29),
                                                DifferAmount = s1,
                                                Type = 2
                                            });

                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }

            else
            {
                {
                    TM04GTGT _tm04GTGT = Utils.ListTM04GTGT.Where(n => n.FromDate == FromDate && n.AdditionTime == lblAdditionTime.Text.ToInt() - 1).FirstOrDefault();
                    if (_tm04GTGT.TM04GTGTDetails != null)
                    {
                        foreach (var item in _tm04GTGT.TM04GTGTDetails)
                        {
                            if (item.Code == "Item23")
                            {
                                if (txtItem23.Value.ToString() != item.Data)
                                {
                                    var s1 = getValueMaskedEdit(txtItem23) - decimal.Parse(item.Data);
                                    if (s1 != 0M)
                                    {
                                        if (s1 > 0M)
                                        {
                                            lst1.Add(new TM04GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "23",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem23),
                                                DifferAmount = s1,
                                                Type = 1

                                            });


                                        }
                                        else
                                        {
                                            lst2.Add(new TM04GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "23",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem23),
                                                DifferAmount = s1,
                                                Type = 2
                                            });
                                        }
                                    }
                                }
                            }
                            if (item.Code == "Item25")
                            {
                                if (txtItem25.Value.ToString() != item.Data)
                                {
                                    var s1 = getValueMaskedEdit(txtItem25) - decimal.Parse(item.Data);
                                    if (s1 != 0M)
                                    {
                                        if (s1 > 0M)
                                        {
                                            lst1.Add(new TM04GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "25",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem25),
                                                DifferAmount = s1,
                                                Type = 1
                                            });

                                        }
                                        else
                                        {
                                            lst2.Add(new TM04GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "25",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem25),
                                                DifferAmount = s1,
                                                Type = 2
                                            });
                                        }
                                    }
                                }
                            }
                            if (item.Code == "Item27")
                            {
                                if (txtItem27.Value.ToString() != item.Data)
                                {
                                    var s1 = getValueMaskedEdit(txtItem27) - decimal.Parse(item.Data);
                                    if (s1 != 0M)
                                    {
                                        if (s1 > 0M)
                                        {
                                            lst1.Add(new TM04GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "27",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem27),
                                                DifferAmount = s1,
                                                Type = 1
                                            });

                                        }
                                        else
                                        {
                                            lst2.Add(new TM04GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "27",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem27),
                                                DifferAmount = s1,
                                                Type = 2
                                            });

                                        }
                                    }
                                }
                            }
                            if (item.Code == "Item29")
                            {
                                if (txtItem29.Value.ToString() != item.Data)
                                {
                                    var s1 = getValueMaskedEdit(txtItem29) - decimal.Parse(item.Data);
                                    if (s1 != 0M)
                                    {
                                        if (s1 > 0M)
                                        {
                                            lst1.Add(new TM04GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "29",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem29),
                                                DifferAmount = s1,
                                                Type = 1
                                            });

                                        }
                                        else
                                        {
                                            lst2.Add(new TM04GTGTAdjust
                                            {
                                                Name = item.Name,
                                                Code = "29",
                                                DeclaredAmount = decimal.Parse(item.Data),
                                                AdjustAmount = getValueMaskedEdit(txtItem29),
                                                DifferAmount = s1,
                                                Type = 2
                                            });

                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
            return lst1;
            return lst2;

        }

        #endregion

        #region Tổng hợp KHBS
        private void THKBS()
        {
            CheckChangeBS();
            LoadDataKHBS1(lst1);
            LoadDataKHBS2(lst2);
            LoadUgridKBS1(lst1);
            LoadUgridKBS2(lst2);

            if (lst1.Count == 0 && lst2.Count == 0)
            {
                lblDeclaredTaxAmount.Text = "0";
                lblAdjustTaxAmount.Text = "0";
                lblDifTaxAmount.Text = "0";

            }
            else
            {
                if (AdditionalTime == "1")
                {
                    TM04GTGT temp = Utils.ListTM04GTGT.Where(n => n.FromDate == FromDate && n.ToDate == ToDate && n.AdditionTime == null).FirstOrDefault();
                    lblCodeItem40.Text = "33";
                    lblDeclaredTaxAmount.FormatNumberic(temp.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item33").Data, ConstDatabase.Format_TienVND);
                    lblAdjustTaxAmount.FormatNumberic(txtItem33.Text, ConstDatabase.Format_TienVND);
                    lblDifTaxAmount.FormatNumberic((getValueLabel(lblAdjustTaxAmount) - getValueLabel(lblDeclaredTaxAmount)), ConstDatabase.Format_TienVND);

                }
                if (MaxAdditionTime != 0)
                {
                    TM04GTGT temp = Utils.ListTM04GTGT.Where(n => n.FromDate == FromDate && n.ToDate == ToDate && n.AdditionTime == MaxAdditionTime).FirstOrDefault();
                    lblCodeItem40.Text = "33";
                    lblDeclaredTaxAmount.FormatNumberic(temp.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item33").Data, ConstDatabase.Format_TienVND);
                    lblAdjustTaxAmount.FormatNumberic(txtItem33.Text, ConstDatabase.Format_TienVND);
                    lblDifTaxAmount.FormatNumberic((getValueLabel(lblAdjustTaxAmount) - getValueLabel(lblDeclaredTaxAmount)), ConstDatabase.Format_TienVND);
                }
            }
            if (getValueLabel(lblDifTaxAmount) > 0)
                BindData(txtLateAmount, (getValueLabel(lblDifTaxAmount) * decimal.Parse(txtDateDelay.Value.ToString()) * decimal.Parse("0,03")) / 100);
        }
        #endregion

        #region event
        private void txtItem_Validated(object sender, EventArgs e)
        {
            UltraMaskedEdit txt = (UltraMaskedEdit)sender;
            txt.Text = txt.Text.TrimStart('0');
        }
        private void utmDetailBaseToolBar_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
        {
            switch (e.Tool.Key)
            {
                case "mnbtnEdit":
                    utmDetailBaseToolBar.Tools["mnbtnSave"].SharedProps.Enabled = true;
                    utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = false;
                    utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Enabled = true;
                    utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = false;
                    utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Enabled = false;
                    LoadReadOnly(false);
                    _isEdit = true;
                    break;

                case "mnbtnSave":
                    try
                    {
                        if (ultraTabControl1.Tabs[1].Visible == true)
                            if (CheckBS == false)
                                THKBS();
                        if (txtSignDate.Value == null)
                        {
                            MSG.Error("Bạn chưa chọn ngày ký");
                            return;
                        }
                        Utils.ITM04GTGTService.BeginTran();
                        if (_isEdit)
                        {
                            TM04GTGT input = new TM04GTGT();
                            input = ObjandGUI(_select, true);
                            Utils.ITM04GTGTService.Update(input);

                        }
                        else
                        {
                            TM04GTGT input = new TM04GTGT();
                            input = ObjandGUI(input, true);
                            Utils.ITM04GTGTService.CreateNew(input);
                            _select = input;
                        }

                        Utils.ITM04GTGTService.CommitTran();
                        MSG.Information("Lưu thành công");
                        Utils.ClearCacheByType<TM04GTGT>();
                        Utils.ClearCacheByType<TM04GTGTDetail>();
                        _select = Utils.ITM04GTGTService.Getbykey(_select.ID);
                        utmDetailBaseToolBar.Tools["mnbtnSave"].SharedProps.Enabled = false;
                        utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = true;
                        utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = true;
                        utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Enabled = false;
                        utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Enabled = true;
                        LoadReadOnly(true);
                        checkSave = true;
                    }
                    catch (Exception ex)
                    {
                        MSG.Error(ex.Message);
                        Utils.ITM04GTGTService.RolbackTran();
                    }

                    break;

                case "mnbtnDelete":
                    if (!(DialogResult.Yes == MessageBox.Show("Bạn có muốn xóa tờ khai này không ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question)))
                        return;
                    DateTime _datecheck = new DateTime(_select.ToDate.Year, _select.ToDate.Month, _select.ToDate.Day);
                    var _lstCheckCreate = Utils.ListGOtherVoucher.FirstOrDefault(g => g.TypeID == 602 && g.PostedDate == _datecheck);
                    if (_lstCheckCreate != null && _select.IsFirstDeclaration)
                    {
                        MSG.Warning("Tờ khai này đã phát sinh chứng từ nghiệp vụ khấu trừ thuế. Vui lòng xóa chứng từ khấu trừ thuế trước khi thực hiện xóa tờ khai này!");
                        return;
                    }
                    TM04GTGT tM04GTGT = Utils.ListTM04GTGT.FirstOrDefault(n => n.ID == _select.ID);
                    Utils.ITM04GTGTService.BeginTran();
                    Utils.ITM04GTGTService.Delete(tM04GTGT);
                    Utils.ITM04GTGTService.CommitTran();
                    MSG.Information("Xóa thành công");
                    this.Close();
                    break;

                case "mnbtnComeBack":
                    this.Close();//add by cuongpv
                    break;

                case "mnbtnPrint":
                    break;

                case "btnKBS":
                    THKBS();
                    CheckBS = true;
                    break;
                case "mnbtnClose":
                    this.Close();
                    break;
                case "btnPrint":
                    //utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = false;
                    //  FillReport();
                    #region fill dữ liệu
                    //GTGT01DetailReport gTGT01DetailReport = new GTGT01DetailReport();
                    //gTGT01DetailReport.Item21 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item21").Data;
                    //gTGT01DetailReport.Item22 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item22").Data;
                    //gTGT01DetailReport.Item23 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item23").Data;
                    //gTGT01DetailReport.Item24 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item24").Data;
                    //gTGT01DetailReport.Item25 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item25").Data;
                    //gTGT01DetailReport.Item26 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item26").Data;
                    //gTGT01DetailReport.Item27 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item27").Data;
                    //gTGT01DetailReport.Item28 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item28").Data;
                    //gTGT01DetailReport.Item29 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item29").Data;
                    //gTGT01DetailReport.Item30 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item30").Data;
                    //gTGT01DetailReport.Item31 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item31").Data;
                    //gTGT01DetailReport.Item32 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item32").Data;
                    //gTGT01DetailReport.Item32a = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item32a").Data;
                    //gTGT01DetailReport.Item33 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item33").Data;
                    //gTGT01DetailReport.Item34 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item34").Data;
                    //gTGT01DetailReport.Item35 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item35").Data;
                    //gTGT01DetailReport.Item36 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item36").Data;
                    //gTGT01DetailReport.Item37 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item37").Data;
                    //gTGT01DetailReport.Item38 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item38").Data;
                    //gTGT01DetailReport.Item39 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item39").Data;
                    //gTGT01DetailReport.Item40 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40").Data;
                    //gTGT01DetailReport.Item40a = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40a").Data;
                    //gTGT01DetailReport.Item40b = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item40b").Data;
                    //gTGT01DetailReport.Item41 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item41").Data;
                    //gTGT01DetailReport.Item42 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item42").Data;
                    //gTGT01DetailReport.Item43 = _select.TM01GTGTDetails.FirstOrDefault(n => n.Code == "Item43").Data;

                    //gTGT01DetailReport.ID = _select.ID;
                    //gTGT01DetailReport.BranchID = _select.BranchID;
                    //gTGT01DetailReport.TypeID = _select.TypeID;
                    //gTGT01DetailReport.DeclarationName = _select.DeclarationName;
                    //gTGT01DetailReport.DeclarationTerm = _select.DeclarationTerm;
                    //gTGT01DetailReport.FromDate = _select.FromDate;
                    //gTGT01DetailReport.ToDate = _select.ToDate;
                    //gTGT01DetailReport.IsFirstDeclaration = _select.IsFirstDeclaration;
                    //gTGT01DetailReport.AdditionTime = _select.AdditionTime;
                    //gTGT01DetailReport.AdditionDate = _select.AdditionDate;
                    //gTGT01DetailReport.Career = _select.Career;
                    //gTGT01DetailReport.IsExtend = _select.IsExtend;
                    //gTGT01DetailReport.ExtensionCase = _select.ExtensionCase;
                    //gTGT01DetailReport.IsAppendix011GTGT = _select.IsAppendix011GTGT;
                    //gTGT01DetailReport.IsAppendix012GTGT = _select.IsAppendix012GTGT;
                    //gTGT01DetailReport.CompanyName = _select.CompanyName;
                    //gTGT01DetailReport.CompanyTaxCode = _select.CompanyTaxCode;
                    //gTGT01DetailReport.TaxAgencyTaxCode = _select.CompanyTaxCode;
                    //gTGT01DetailReport.TaxAgencyName = _select.TaxAgencyName;
                    //gTGT01DetailReport.TaxAgencyEmployeeName = _select.TaxAgencyEmployeeName;
                    //gTGT01DetailReport.CertificationNo = _select.CertificationNo;
                    //gTGT01DetailReport.SignName = _select.SignName;
                    //gTGT01DetailReport.SignDate = _select.SignDate;
                    //gTGT01DetailReport.TM01GTGTDetails = _select.TM01GTGTDetails;

                    //// var lst = Utils.ListTM01GTGTAdjust.CloneObject();

                    //gTGT01DetailReport.TM01GTGTAdjusts = _select.TM01GTGTAdjusts;

                    //if (gTGT01DetailReport.AdditionTime != null)
                    //{
                    //    //gTGT01DetailReport.TM01GTGTAdjusts = lst;
                    //    gTGT01DetailReport.TM01GTGTAdjusts1 = gTGT01DetailReport.TM01GTGTAdjusts.Where(n => n.Type == 1).OrderBy(n => n.OrderPriority).ToList();
                    //    gTGT01DetailReport.TM01GTGTAdjusts2 = gTGT01DetailReport.TM01GTGTAdjusts.Where(n => n.Type == 2).OrderBy(n => n.OrderPriority).ToList();
                    //    gTGT01DetailReport.TM01GTGTAdjusts3 = gTGT01DetailReport.TM01GTGTAdjusts.Where(n => n.Type == 3).OrderBy(n => n.OrderPriority).ToList();
                    //    if (gTGT01DetailReport.TM01GTGTAdjusts1.Count == 0)
                    //    {
                    //        TM01GTGTAdjust adjust = new TM01GTGTAdjust();
                    //        adjust.ID = Guid.Empty;
                    //        adjust.TM01GTGTID = _select.ID;
                    //        adjust.Name = "";
                    //        adjust.Code = "";
                    //        adjust.DeclaredAmount = 0;
                    //        adjust.AdjustAmount = 0;
                    //        adjust.DifferAmount = 0;
                    //        adjust.Type = 1;
                    //        gTGT01DetailReport.TM01GTGTAdjusts.Add(adjust);
                    //    }
                    //    if (gTGT01DetailReport.TM01GTGTAdjusts3.Count == 0)
                    //    {
                    //        TM01GTGTAdjust adjust3 = new TM01GTGTAdjust();
                    //        adjust3.ID = Guid.Empty;
                    //        adjust3.TM01GTGTID = _select.ID;
                    //        adjust3.Name = "";
                    //        adjust3.Code = "";
                    //        adjust3.DeclaredAmount = 0;
                    //        adjust3.AdjustAmount = 0;
                    //        adjust3.DifferAmount = 0;
                    //        adjust3.Type = 3;
                    //        gTGT01DetailReport.TM01GTGTAdjusts.Add(adjust3);
                    //    }
                    //    if (gTGT01DetailReport.TM01GTGTAdjusts2.Count == 0)
                    //    {
                    //        TM01GTGTAdjust adjust2 = new TM01GTGTAdjust();
                    //        adjust2.ID = Guid.Empty;
                    //        adjust2.TM01GTGTID = _select.ID;
                    //        adjust2.Name = "";
                    //        adjust2.Code = "";
                    //        adjust2.DeclaredAmount = 0;
                    //        adjust2.AdjustAmount = 0;
                    //        adjust2.DifferAmount = 0;
                    //        adjust2.Type = 2;
                    //        gTGT01DetailReport.TM01GTGTAdjusts.Add(adjust2);
                    //    }
                    //}

                    //gTGT01DetailReport.TM011GTGTs = _select.TM011GTGTs;
                    //gTGT01DetailReport.TM012GTGTs = _select.TM012GTGTs;



                    //if (gTGT01DetailReport.IsAppendix011GTGT)
                    //{
                    //    if (gTGT01DetailReport.TM011GTGTs != null)
                    //    {
                    //        gTGT01DetailReport.lstKCT = gTGT01DetailReport.TM011GTGTs.Where(n => n.VATRate == -1).OrderBy(n => n.OrderPriority).ToList();
                    //        gTGT01DetailReport.lstRate0 = gTGT01DetailReport.TM011GTGTs.Where(n => n.VATRate == 0).OrderBy(n => n.OrderPriority).ToList();
                    //        gTGT01DetailReport.lstRate5 = gTGT01DetailReport.TM011GTGTs.Where(n => n.VATRate == 5).OrderBy(n => n.OrderPriority).ToList();
                    //        gTGT01DetailReport.lstRate10 = gTGT01DetailReport.TM011GTGTs.Where(n => n.VATRate == 10).OrderBy(n => n.OrderPriority).ToList();
                    //    }

                    //    TM011GTGT item = new TM011GTGT();
                    //    item.ID = Guid.Empty;
                    //    item.TM01GTGTID = _select.ID;
                    //    item.VoucherID = Guid.Empty;
                    //    item.VoucherDetailID = Guid.Empty;
                    //    item.InvoiceDate = DateTime.MinValue;
                    //    item.InvoiceNo = "";
                    //    item.InvoiceSeries = "";
                    //    item.AccountingObjectID = Guid.Empty;
                    //    item.AccountingObjectName = "";
                    //    item.TaxCode = "";
                    //    item.PretaxAmount = 0;
                    //    item.TaxAmount = 0;
                    //    item.Note = "";
                    //    item.OrderPriority = 0;
                    //    item.STT = 1;
                    //    item.GroupName = "";

                    //    if (gTGT01DetailReport.lstKCT.Count == 0)
                    //    {
                    //        item.VATRate = -1;
                    //        gTGT01DetailReport.lstKCT.Add(item);
                    //    }
                    //    if (gTGT01DetailReport.lstRate0.Count == 0)
                    //    {
                    //        item.VATRate = 0;
                    //        gTGT01DetailReport.lstRate0.Add(item);
                    //    }
                    //    if (gTGT01DetailReport.lstRate5.Count == 0)
                    //    {
                    //        item.VATRate = 5;
                    //        gTGT01DetailReport.lstRate5.Add(item);

                    //    }
                    //    if (gTGT01DetailReport.lstRate10.Count == 0)
                    //    {
                    //        item.VATRate = 10;
                    //        gTGT01DetailReport.lstRate10.Add(item);
                    //    }
                    //}

                    //if (gTGT01DetailReport.IsAppendix012GTGT)
                    //{
                    //    gTGT01DetailReport.TM012GTGTs1 = gTGT01DetailReport.TM012GTGTs.Where(n => n.Type == 1).ToList();
                    //    gTGT01DetailReport.TM012GTGTs2 = gTGT01DetailReport.TM012GTGTs.Where(n => n.Type == 2).ToList();
                    //    gTGT01DetailReport.TM012GTGTs3 = gTGT01DetailReport.TM012GTGTs.Where(n => n.Type == 3).ToList();

                    //    if (gTGT01DetailReport.TM012GTGTs1.Count == 0)
                    //    {

                    //        TM012GTGT item1 = new TM012GTGT();
                    //        item1.ID = Guid.Empty;
                    //        item1.TM01GTGTID = _select.ID;
                    //        item1.VoucherID = Guid.Empty;
                    //        item1.VoucherDetailID = Guid.Empty;
                    //        item1.InvoiceNo = "";
                    //        item1.InvoiceDate = DateTime.MinValue;
                    //        item1.InvoiceSeries = "";
                    //        item1.AccountingObjectID = Guid.Empty;
                    //        item1.AccountingObjectName = "";
                    //        item1.TaxCode = "";
                    //        item1.PretaxAmount = 0;
                    //        item1.TaxAmount = 0;
                    //        item1.Note = "";
                    //        item1.OrderPriority = 0;
                    //        item1.STT = 1;
                    //        item1.GroupName = "";
                    //        item1.Type = 1;
                    //        gTGT01DetailReport.TM012GTGTs1.Add(item1);
                    //    }
                    //    if (gTGT01DetailReport.TM012GTGTs2.Count == 0)
                    //    {
                    //        TM012GTGT item2 = new TM012GTGT();
                    //        item2.ID = Guid.Empty;
                    //        item2.TM01GTGTID = _select.ID;
                    //        item2.VoucherID = Guid.Empty;
                    //        item2.VoucherDetailID = Guid.Empty;
                    //        item2.InvoiceSeries = "";
                    //        item2.InvoiceNo = "";
                    //        item2.InvoiceDate = DateTime.MinValue;
                    //        item2.AccountingObjectID = Guid.Empty;
                    //        item2.AccountingObjectName = "";
                    //        item2.TaxCode = "";
                    //        item2.PretaxAmount = 0;
                    //        item2.TaxAmount = 0;
                    //        item2.Note = "";
                    //        item2.OrderPriority = 0;
                    //        item2.STT = 1;
                    //        item2.GroupName = "";
                    //        item2.Type = 2;
                    //        gTGT01DetailReport.TM012GTGTs2.Add(item2);
                    //    }
                    //    if (gTGT01DetailReport.TM012GTGTs3.Count == 0)
                    //    {
                    //        TM012GTGT item3 = new TM012GTGT();
                    //        item3.ID = Guid.Empty;
                    //        item3.TM01GTGTID = _select.ID;
                    //        item3.VoucherID = Guid.Empty;
                    //        item3.VoucherDetailID = Guid.Empty;
                    //        item3.InvoiceNo = "";
                    //        item3.InvoiceDate = DateTime.MinValue;
                    //        item3.InvoiceSeries = "";
                    //        item3.AccountingObjectID = Guid.Empty;
                    //        item3.AccountingObjectName = "";
                    //        item3.TaxCode = "";
                    //        item3.PretaxAmount = 0;
                    //        item3.TaxAmount = 0;
                    //        item3.Note = "";
                    //        item3.OrderPriority = 0;
                    //        item3.STT = 1;
                    //        item3.GroupName = "";
                    //        item3.Type = 3;
                    //        gTGT01DetailReport.TM012GTGTs3.Add(item3);
                    //    }
                    //}


                    //gTGT01DetailReport.TM012GTGTs = gTGT01DetailReport.TM012GTGTs.OrderBy(n => n.Type).ToList();
                    //gTGT01DetailReport.TM01GTGTAdjusts = gTGT01DetailReport.TM01GTGTAdjusts.OrderBy(n => n.Type).ToList();
                    #endregion
                    //Utils.Print("01-GTGT", gTGT01DetailReport, this);
                    break;
                case "btnExportXml":
                    ExportXml(_select);
                    break;
                case "PrintTAIN01":
                    #region fill dữ liệu
                    GTGT04Report gTGT04Report = new GTGT04Report();
                    gTGT04Report.Item21 = _select.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item21").Data;
                    gTGT04Report.Item22 = _select.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item22").Data;
                    gTGT04Report.Item23 = _select.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item23").Data;
                    gTGT04Report.Item24 = _select.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item24").Data;
                    gTGT04Report.Item25 = _select.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item25").Data;
                    gTGT04Report.Item26 = _select.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item26").Data;
                    gTGT04Report.Item27 = _select.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item27").Data;
                    gTGT04Report.Item28 = _select.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item28").Data;
                    gTGT04Report.Item29 = _select.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item29").Data;
                    gTGT04Report.Item30 = _select.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item30").Data;
                    gTGT04Report.Item31 = _select.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item31").Data;
                    gTGT04Report.Item32 = _select.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item32").Data;
                    gTGT04Report.Item33 = _select.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item33").Data;
                    gTGT04Report.ID = _select.ID;
                    gTGT04Report.BranchID = _select.BranchID;
                    gTGT04Report.TypeID = _select.TypeID;
                    gTGT04Report.DeclarationName = _select.DeclarationName;
                    gTGT04Report.DeclarationTerm = _select.DeclarationTerm;
                    gTGT04Report.FromDate = _select.FromDate;
                    gTGT04Report.ToDate = _select.ToDate;
                    gTGT04Report.IsFirstDeclaration = _select.IsFirstDeclaration;
                    gTGT04Report.AdditionTime = _select.AdditionTime;
                    gTGT04Report.AdditionDate = _select.AdditionDate;

                    gTGT04Report.CompanyName = _select.CompanyName;
                    gTGT04Report.CompanyTaxCode = _select.CompanyTaxCode;
                    gTGT04Report.TaxAgencyTaxCode = _select.CompanyTaxCode;
                    gTGT04Report.TaxAgencyName = _select.TaxAgencyName;
                    gTGT04Report.TaxAgencyEmployeeName = _select.TaxAgencyEmployeeName;
                    gTGT04Report.CertificationNo = _select.CertificationNo;
                    gTGT04Report.SignName = _select.SignName;
                    gTGT04Report.SignDate = _select.SignDate;
                    gTGT04Report.TM04GTGTDetails = _select.TM04GTGTDetails;
                    gTGT04Report.TM04GTGTAdjusts = _select.TM04GTGTAdjusts;


                    {


                        if ((gTGT04Report.TM04GTGTAdjusts.Where(n => n.Type == 1).ToList()).Count == 0)
                        {
                            TM04GTGTAdjust adjust = new TM04GTGTAdjust();
                            adjust.ID = Guid.Empty;
                            adjust.TM04GTGTID = _select.ID;
                            adjust.Name = "";
                            adjust.Code = "";
                            adjust.DeclaredAmount = 0;
                            adjust.AdjustAmount = 0;
                            adjust.DifferAmount = 0;
                            adjust.Type = 1;
                            gTGT04Report.TM04GTGTAdjusts.Add(adjust);
                        }
                        if ((gTGT04Report.TM04GTGTAdjusts.Where(n => n.Type == 3).ToList()).Count == 0)
                        {
                            TM04GTGTAdjust adjust3 = new TM04GTGTAdjust();
                            adjust3.ID = Guid.Empty;
                            adjust3.TM04GTGTID = _select.ID;
                            adjust3.Name = "";
                            adjust3.Code = "";
                            adjust3.DeclaredAmount = 0;
                            adjust3.AdjustAmount = 0;
                            adjust3.DifferAmount = 0;
                            adjust3.Type = 3;
                            gTGT04Report.TM04GTGTAdjusts.Add(adjust3);
                        }
                        if ((gTGT04Report.TM04GTGTAdjusts.Where(n => n.Type == 2).ToList()).Count == 0)
                        {
                            TM04GTGTAdjust adjust2 = new TM04GTGTAdjust();
                            adjust2.ID = Guid.Empty;
                            adjust2.TM04GTGTID = _select.ID;
                            adjust2.Name = "";
                            adjust2.Code = "";
                            adjust2.DeclaredAmount = 0;
                            adjust2.AdjustAmount = 0;
                            adjust2.DifferAmount = 0;
                            adjust2.Type = 2;
                            gTGT04Report.TM04GTGTAdjusts.Add(adjust2);
                        }
                    }
                    gTGT04Report.TM04GTGTAdjusts = gTGT04Report.TM04GTGTAdjusts.OrderBy(n => n.Type).ToList();
                    #endregion
                    Utils.Print("04-GTGT", gTGT04Report, this);
                    break;
            }
        }

        private void txtItem22_ValueChanged(object sender, EventArgs e)
        {
            if (txtItem22.Value.IsNullOrEmpty())
                txtItem22.Value = 0;
            if (txtItem24.Value.IsNullOrEmpty())
                txtItem24.Value = 0;
            if (txtItem26.Value.IsNullOrEmpty())
                txtItem26.Value = 0;
            if (txtItem28.Value.IsNullOrEmpty())
                txtItem28.Value = 0;
            BindData(txtItem30, getValueMaskedEdit(txtItem22) + getValueMaskedEdit(txtItem24) + getValueMaskedEdit(txtItem26) + getValueMaskedEdit(txtItem28));
            BindData(txtItem23, Math.Round((getValueMaskedEdit(txtItem22) * 1) / 100, MidpointRounding.AwayFromZero));
        }

        private void txtItem24_ValueChanged(object sender, EventArgs e)
        {
            if (txtItem22.Value.IsNullOrEmpty())
                txtItem22.Value = 0;
            if (txtItem24.Value.IsNullOrEmpty())
                txtItem24.Value = 0;
            if (txtItem26.Value.IsNullOrEmpty())
                txtItem26.Value = 0;
            if (txtItem28.Value.IsNullOrEmpty())
                txtItem28.Value = 0;
            BindData(txtItem30, getValueMaskedEdit(txtItem22) + getValueMaskedEdit(txtItem24) + getValueMaskedEdit(txtItem26) + getValueMaskedEdit(txtItem28));
            BindData(txtItem25, Math.Round((getValueMaskedEdit(txtItem24) * 5) / 100, MidpointRounding.AwayFromZero));
        }

        private void txtItem26_ValueChanged(object sender, EventArgs e)
        {
            if (txtItem22.Value.IsNullOrEmpty())
                txtItem22.Value = 0;
            if (txtItem24.Value.IsNullOrEmpty())
                txtItem24.Value = 0;
            if (txtItem26.Value.IsNullOrEmpty())
                txtItem26.Value = 0;
            if (txtItem28.Value.IsNullOrEmpty())
                txtItem28.Value = 0;
            BindData(txtItem30, getValueMaskedEdit(txtItem22) + getValueMaskedEdit(txtItem24) + getValueMaskedEdit(txtItem26) + getValueMaskedEdit(txtItem28));
            BindData(txtItem27, Math.Round((getValueMaskedEdit(txtItem26) * 3) / 100, MidpointRounding.AwayFromZero));
        }

        private void txtItem28_ValueChanged(object sender, EventArgs e)
        {
            if (txtItem22.Value.IsNullOrEmpty())
                txtItem22.Value = 0;
            if (txtItem24.Value.IsNullOrEmpty())
                txtItem24.Value = 0;
            if (txtItem26.Value.IsNullOrEmpty())
                txtItem26.Value = 0;
            if (txtItem28.Value.IsNullOrEmpty())
                txtItem28.Value = 0;
            BindData(txtItem30, getValueMaskedEdit(txtItem22) + getValueMaskedEdit(txtItem24) + getValueMaskedEdit(txtItem26) + getValueMaskedEdit(txtItem28));
            BindData(txtItem29, Math.Round((getValueMaskedEdit(txtItem28) * 2) / 100, MidpointRounding.AwayFromZero));
        }
        private void txtItem23_ValueChanged(object sender, EventArgs e)
        {
            if (txtItem23.Value.IsNullOrEmpty())
                txtItem23.Value = 0;
            if (txtItem25.Value.IsNullOrEmpty())
                txtItem25.Value = 0;
            if (txtItem27.Value.IsNullOrEmpty())
                txtItem27.Value = 0;
            if (txtItem29.Value.IsNullOrEmpty())
                txtItem29.Value = 0;
            BindData(txtItem31, getValueMaskedEdit(txtItem23) + getValueMaskedEdit(txtItem25) + getValueMaskedEdit(txtItem27) + getValueMaskedEdit(txtItem29));
        }
        private void txtItem21_ValueChanged(object sender, EventArgs e)
        {
            if (txtItem21.Value.IsNullOrEmpty())
                txtItem21.Value = 0;
            if (txtItem30.Value.IsNullOrEmpty())
                txtItem30.Value = 0;
            txtItem32.FormatNumberic((getValueMaskedEdit(txtItem21) + getValueMaskedEdit(txtItem30)), ConstDatabase.Format_TienVND);
        }
        private void txtItem31_ValueChanged(object sender, EventArgs e)
        {
            if (txtItem31.Value.IsNullOrEmpty())
                txtItem31.Value = 0;
            txtItem33.FormatNumberic(getValueMaskedEdit(txtItem31), ConstDatabase.Format_TienVND);
        }
        private bool ExportXml(TM04GTGT tM04GTGT)
        {
            SaveFileDialog sf = new SaveFileDialog
            {
                FileName = "04_GTGT_xml.xml",
                AddExtension = true,
                Filter = "Excel Document(*.xml)|*.xml"
            };
            if (!tM04GTGT.IsFirstDeclaration)
            {
                sf.FileName = "04_GTGT_BS_xml.xml";
            }
            if (sf.ShowDialog() != DialogResult.OK)
            {
                return true;
            }
            string path = System.IO.Path.GetDirectoryName(
                        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            string filePath = string.Format("{0}\\TemplateResource\\xml\\04_GTGT_xml.xml", path);
            XmlDocument doc = new XmlDocument();
            doc.Load(filePath);
            string tt = "HSoThueDTu/HSoKhaiThue/TTinChung/";
            doc.SelectSingleNode(tt + "TTinDVu/maDVu").InnerText = "";
            doc.SelectSingleNode(tt + "TTinDVu/tenDVu").InnerText = "";
            doc.SelectSingleNode(tt + "TTinDVu/pbanDVu").InnerText = "";
            doc.SelectSingleNode(tt + "TTinDVu/ttinNhaCCapDVu").InnerText = "";

            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/maTKhai").InnerText = "07";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/tenTKhai").InnerText = tM04GTGT.DeclarationName;
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/moTaBMau").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/pbanTKhaiXML").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/loaiTKhai").InnerText = "C";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/soLan").InnerText = "";

            if (tM04GTGT.IsFirstDeclaration)
            {
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/loaiTKhai").InnerText = "C";
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/soLan").InnerText = "";
            }
            else
            {
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/loaiTKhai").InnerText = "B";
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/soLan").InnerText = (tM04GTGT.AdditionTime ?? 0).ToString();
            }

            if (tM04GTGT.DeclarationTerm.Contains("Quý"))
            {
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kieuKy").InnerText = "Q";
                if (tM04GTGT.DeclarationTerm.Split(' ')[1].Equals("I"))
                {
                    doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = "1/" + tM04GTGT.DeclarationTerm.Split(' ')[3];
                }
                else if (tM04GTGT.DeclarationTerm.Split(' ')[1].Equals("II"))
                {
                    doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = "2/" + tM04GTGT.DeclarationTerm.Split(' ')[3];
                }
                else if (tM04GTGT.DeclarationTerm.Split(' ')[1].Equals("III"))
                {
                    doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = "3/" + tM04GTGT.DeclarationTerm.Split(' ')[3];
                }
                else
                {
                    doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = "4/" + tM04GTGT.DeclarationTerm.Split(' ')[3];
                }
            }
            else if (tM04GTGT.DeclarationTerm.Contains("Tháng"))
            {
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kieuKy").InnerText = "M";
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = tM04GTGT.DeclarationTerm.Split(' ')[1] + "/" + tM04GTGT.DeclarationTerm.Split(' ')[3];
            }
            else
            {
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kieuKy").InnerText = "D";
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = tM04GTGT.FromDate.ToString("dd/MM/yyyy");
            }
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhaiTuNgay").InnerText = tM04GTGT.FromDate.ToString("dd/MM/yyyy");
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhaiDenNgay").InnerText = tM04GTGT.ToDate.ToString("dd/MM/yyyy");

            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/maCQTNoiNop").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/tenCQTNoiNop").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/ngayLapTKhai").InnerText = "";

            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/GiaHan/maLyDoGiaHan").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/GiaHan/lyDoGiaHan").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/GiaHan/lyDoGiaHan").InnerText = "";

            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/nguoiKy").InnerText = tM04GTGT.SignName;
            if (tM04GTGT.SignDate == null)
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/ngayKy").InnerText = "";
            else
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/ngayKy").InnerText = (tM04GTGT.SignDate ?? DateTime.Now).ToString("dd/MM/yyyy");
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/nganhNgheKD").InnerText = tM04GTGT.CertificationNo;

            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/mst").InnerText = tM04GTGT.CompanyTaxCode;
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/tenNNT").InnerText = tM04GTGT.CompanyName;
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/dchiNNT").InnerText = Utils.GetCompanyAddress();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/phuongXa").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/maHuyenNNT").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/tenHuyenNNT").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/maTinhNNT").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/tenTinhNNT").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/dthoaiNNT").InnerText = Utils.GetCompanyPhoneNumber();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/faxNNT").InnerText = tM04GTGT.CompanyTaxCode;
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/emailNNT").InnerText = Utils.GetCompanyEmail();

            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/mstDLyThue").InnerText = tM04GTGT.TaxAgencyTaxCode;
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/tenDLyThue").InnerText = tM04GTGT.TaxAgencyName;
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/dchiDLyThue").InnerText = Utils.GetDLTAddress();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/maHuyenDLyThue").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/tenHuyenDLyThue").InnerText = Utils.GetDLTDistrict();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/maTinhDLyThue").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/tenTinhDLyThue").InnerText = Utils.GetDLTCity();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/dthoaiDLyThue").InnerText = Utils.GetDLTPhoneNumber();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/faxDLyThue").InnerText = Utils.GetDLTFax();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/emailDLyThue").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/soHDongDLyThue").InnerText = Utils.GetDLTSoHD();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/ngayKyHDDLyThue").InnerText = Utils.GetDLTNgay();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/NVienDLy/tenNVienDLyThue").InnerText = Utils.GetHVTNhanVienDLT();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/NVienDLy/cchiHNghe").InnerText = Utils.GetCCHNDLT();

            tt = "HSoThueDTu/HSoKhaiThue/CTieuTKhaiChinh/";

            doc.SelectSingleNode(tt + "ct21").InnerText = tM04GTGT.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item21").Data;
            doc.SelectSingleNode(tt + "PPhoiCCapHHoa/ct22").InnerText = tM04GTGT.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item22").Data;
            doc.SelectSingleNode(tt + "PPhoiCCapHHoa/ct23").InnerText = tM04GTGT.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item23").Data;
            doc.SelectSingleNode(tt + "DVuXDungKoBaoThauNVL/ct24").InnerText = tM04GTGT.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item24").Data;
            doc.SelectSingleNode(tt + "DVuXDungKoBaoThauNVL/ct25").InnerText = tM04GTGT.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item25").Data;
            doc.SelectSingleNode(tt + "HHoaXDungCoBaoThauNVL/ct26").InnerText = tM04GTGT.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item26").Data;
            doc.SelectSingleNode(tt + "HHoaXDungCoBaoThauNVL/ct27").InnerText = tM04GTGT.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item27").Data;
            doc.SelectSingleNode(tt + "HDongKDoanhKhac/ct28").InnerText = tM04GTGT.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item28").Data;
            doc.SelectSingleNode(tt + "HDongKDoanhKhac/ct29").InnerText = tM04GTGT.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item29").Data;
            doc.SelectSingleNode(tt + "TongCong/ct30").InnerText = tM04GTGT.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item30").Data;
            doc.SelectSingleNode(tt + "TongCong/ct31").InnerText = tM04GTGT.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item31").Data;
            doc.SelectSingleNode(tt + "ct32").InnerText = tM04GTGT.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item32").Data;
            doc.SelectSingleNode(tt + "ct33").InnerText = tM04GTGT.TM04GTGTDetails.FirstOrDefault(n => n.Code == "Item33").Data;

            if (tM04GTGT.IsFirstDeclaration)
            {
                doc.SelectSingleNode("HSoThueDTu/HSoKhaiThue/PLuc").InnerText = "";

            }
            else
            {
                string pathBS = System.IO.Path.GetDirectoryName(
                        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                string filePathBS = string.Format("{0}\\TemplateResource\\xml\\PL01_04GTGT.xml", path);
                XmlDocument docBS = new XmlDocument();
                docBS.Load(filePathBS);
                string ttbs = "PL01_KHBS/NDUNGKHAIBOSUNGDIEUCHINH/";
                List<TM04GTGTAdjust> lst1 = tM04GTGT.TM04GTGTAdjusts.Where(n => n.Type == 1).ToList();
                List<TM04GTGTAdjust> lst2 = tM04GTGT.TM04GTGTAdjusts.Where(n => n.Type == 2).ToList();
                List<TM04GTGTAdjust> lst3 = tM04GTGT.TM04GTGTAdjusts.Where(n => n.Type == 3).ToList();
                for (int i = 0; i < lst1.Count(); i++)
                {
                    if (i == 0)
                    {
                        docBS.SelectSingleNode(ttbs + "DieuChinhTang/KHBSDieuChinhTang/ct2").InnerText = lst1[i].Name;
                        docBS.SelectSingleNode(ttbs + "DieuChinhTang/KHBSDieuChinhTang/ct3").InnerText = lst1[i].Code;
                        docBS.SelectSingleNode(ttbs + "DieuChinhTang/KHBSDieuChinhTang/ct4").InnerText = lst1[i].DeclaredAmount.ToString("0.00").Replace(',', '.');
                        docBS.SelectSingleNode(ttbs + "DieuChinhTang/KHBSDieuChinhTang/ct5").InnerText = lst1[i].AdjustAmount.ToString("0.00").Replace(',', '.');
                        docBS.SelectSingleNode(ttbs + "DieuChinhTang/KHBSDieuChinhTang/ct6").InnerText = lst1[i].DifferAmount.ToString("0.00").Replace(',', '.');
                    }
                    else
                    {
                        XmlNode xmlNode1 = doc.SelectSingleNode(ttbs + "DieuChinhTang/KHBSDieuChinhTang").CloneNode(true);
                        xmlNode1.Attributes["id"].Value = "ID_" + (i + 1).ToString();
                        xmlNode1.SelectSingleNode("ct2").InnerText = lst1[i].Name;
                        xmlNode1.SelectSingleNode("ct3").InnerText = lst1[i].Code;
                        xmlNode1.SelectSingleNode("ct4").InnerText = lst1[i].DeclaredAmount.ToString("0.00").Replace(',', '.');
                        xmlNode1.SelectSingleNode("ct5").InnerText = lst1[i].AdjustAmount.ToString("0.00").Replace(',', '.');
                        xmlNode1.SelectSingleNode("ct6").InnerText = lst1[i].DifferAmount.ToString("0.00").Replace(',', '.');
                        docBS.SelectSingleNode(ttbs + "DieuChinhTang").AppendChild(xmlNode1);
                    }
                }
                for (int i = 0; i < lst2.Count(); i++)
                {
                    if (i == 0)
                    {
                        docBS.SelectSingleNode(ttbs + "DieuChinhGiam/KHBSDieuChinhGiam/ct2").InnerText = lst1[i].Name;
                        docBS.SelectSingleNode(ttbs + "DieuChinhGiam/KHBSDieuChinhGiam/ct3").InnerText = lst1[i].Code;
                        docBS.SelectSingleNode(ttbs + "DieuChinhGiam/KHBSDieuChinhGiam/ct4").InnerText = lst1[i].DeclaredAmount.ToString("0.00").Replace(',', '.');
                        docBS.SelectSingleNode(ttbs + "DieuChinhGiam/KHBSDieuChinhGiam/ct5").InnerText = lst1[i].AdjustAmount.ToString("0.00").Replace(',', '.');
                        docBS.SelectSingleNode(ttbs + "DieuChinhGiam/KHBSDieuChinhGiam/ct6").InnerText = lst1[i].DifferAmount.ToString("0.00").Replace(',', '.');
                    }
                    else
                    {
                        XmlNode xmlNode1 = doc.SelectSingleNode(ttbs + "DieuChinhGiam/KHBSDieuChinhGiam").CloneNode(true);
                        xmlNode1.Attributes["id"].Value = "ID_" + (i + 1).ToString();
                        xmlNode1.SelectSingleNode("ct2").InnerText = lst1[i].Name;
                        xmlNode1.SelectSingleNode("ct3").InnerText = lst1[i].Code;
                        xmlNode1.SelectSingleNode("ct4").InnerText = lst1[i].DeclaredAmount.ToString("0.00").Replace(',', '.');
                        xmlNode1.SelectSingleNode("ct5").InnerText = lst1[i].AdjustAmount.ToString("0.00").Replace(',', '.');
                        xmlNode1.SelectSingleNode("ct6").InnerText = lst1[i].DifferAmount.ToString("0.00").Replace(',', '.');
                        docBS.SelectSingleNode(ttbs + "DieuChinhGiam").AppendChild(xmlNode1);
                    }
                }
                docBS.SelectSingleNode(ttbs + "TongHopDCTangGiamPNop/ct2").InnerText = lst3.First().Name;
                docBS.SelectSingleNode(ttbs + "TongHopDCTangGiamPNop/ct3").InnerText = lst3.First().Code;
                docBS.SelectSingleNode(ttbs + "TongHopDCTangGiamPNop/ct4").InnerText = lst3.First().DeclaredAmount.ToString("0.00").Replace(',', '.');
                docBS.SelectSingleNode(ttbs + "TongHopDCTangGiamPNop/ct5").InnerText = lst3.First().AdjustAmount.ToString("0.00").Replace(',', '.');
                docBS.SelectSingleNode(ttbs + "TongHopDCTangGiamPNop/ct6").InnerText = lst3.First().DifferAmount.ToString("0.00").Replace(',', '.');

                ttbs = "PL01_KHBS/PhatChamNop/";
                docBS.SelectSingleNode("PL01_KHBS/PhatChamNop/soNgayNopCham").InnerText = lst3.First().LateDays == null ? "" : lst3.First().LateDays.ToString();
                docBS.SelectSingleNode("PL01_KHBS/PhatChamNop/soTienPhatNopCham").InnerText = lst3.First().LateAmount.ToString("0.00").Replace(',', '.');

                docBS.SelectSingleNode("PL01_KHBS/NOIDUNGGIAITHICH/soTienTraNSNN").InnerText = lst3.First().ExplainAmount.ToString("0.00").Replace(',', '.');
                docBS.SelectSingleNode("PL01_KHBS/NOIDUNGGIAITHICH/quyetDinhHoanThueSo").InnerText = lst3.First().CommandNo;
                docBS.SelectSingleNode("PL01_KHBS/NOIDUNGGIAITHICH/ngay").InnerText = lst3.First().CommandDate == null ? "" : (lst3.First().CommandDate ?? DateTime.Now).ToString("dd/MM/yyyy");
                docBS.SelectSingleNode("PL01_KHBS/NOIDUNGGIAITHICH/coQuanThue").InnerText = lst3.First().TaxCompanyName;
                docBS.SelectSingleNode("PL01_KHBS/NOIDUNGGIAITHICH/soNgayNhanDuocTienHoan").InnerText = lst3.First().ReceiveDays == null ? "" : lst3.First().ReceiveDays.ToString();
                docBS.SelectSingleNode("PL01_KHBS/NOIDUNGGIAITHICH/soTienChamNop").InnerText = lst3.First().ExplainLateAmount.ToString("0.00").Replace(',', '.');
                docBS.SelectSingleNode("PL01_KHBS/NOIDUNGGIAITHICH/lyDoKhac").InnerText = lst3.First().DifferReason;

                doc.SelectSingleNode("HSoThueDTu/HSoKhaiThue/PLuc").InnerXml = docBS.InnerXml;
            }
            doc.Save(sf.FileName);
            if (MSG.Question("Xuất xml thành công, Bạn có muốn mở file vừa tải") == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    System.Diagnostics.Process.Start(sf.FileName);
                }
                catch
                {
                    MSG.Error("Lỗi mở file");
                }

            }
            //var address = doc.Root.Element("address");
            //if (address != null)
            //{
            //    address.Value = "new value";
            //}

            return true;
        }

        #endregion

        private void _04_GTGT_Detail_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (Form)sender;
            frm.Dispose();

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}
