﻿using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting
{
    public partial class TaxPeriod_04_GTGT : Form
    {
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        public Tax Tax;
        public TaxPeriod_04_GTGT()
        {
            InitializeComponent();
            txtYear.Value = DateTime.Now.Year;
            txtDay.Value = DateTime.Now.Day;
            ultraLabel1.Location = new Point(12, 51);
            cbbMonth.Location = new Point(60, 47);
            ultraLabel2.Location = new Point(160, 51);
            txtYear.Location = new Point(203, 47);
            InitializeGUI();
        }
        private void InitializeGUI()
        {
            #region Config cbbMonth
            cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 21 && int.Parse(n.Value) > 8).ToList(), "Name");
            cbbMonth.DropDownStyle = UltraComboStyle.DropDownList;
            try
            {
                foreach (var item in cbbMonth.Rows)
                {
                    if (int.Parse((item.ListObject as Item).Value) == DateTime.Now.Month + 8) cbbMonth.SelectedRow = item;
                }
            }
            catch (Exception e)
            {

            }
            #endregion
        }


        private void ultraOptionSet1_ValueChanged(object sender, EventArgs e)
        {
            if (ultraOptionSet1.CheckedIndex == 2)
            {
                ultraLabel1.Text = "Tháng";
                ultraLabel6.Visible = true;
                txtDay.Visible = true;
                ultraLabel6.Location = new Point(12, 51);
                txtDay.Location = new Point(50, 47);
                ultraLabel1.Location = new Point(151, 51);
                cbbMonth.Location = new Point(203, 47);
                ultraLabel2.Location = new Point(324, 51);
                txtYear.Location = new Point(362, 47);
                cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 21 && int.Parse(n.Value) > 8).ToList(), "Name");
                foreach (var item in cbbMonth.Rows)
                {
                    if (int.Parse((item.ListObject as Item).Value) == DateTime.Now.Month + 8) cbbMonth.SelectedRow = item;
                }
            }
            else if (ultraOptionSet1.CheckedIndex == 1)
            {
                ultraLabel1.Text = "Quý";
                ultraLabel6.Visible = false;
                txtDay.Visible = false;
                ultraLabel1.Location = new Point(12, 51);
                cbbMonth.Location = new Point(60, 47);
                ultraLabel2.Location = new Point(160, 51);
                txtYear.Location = new Point(203, 47);
                cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 25 && int.Parse(n.Value) > 20).ToList(), "Name");
                float now = float.Parse(DateTime.Now.Month.ToString()) / 3;
                foreach (var item in cbbMonth.Rows)
                {
                    if (now <= 1)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 21) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 2 && now > 1)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 22) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 3 && now > 2)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 23) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 4 && now > 3)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 24) cbbMonth.SelectedRow = item;
                    }

                }
            }
            else
            {
                ultraLabel1.Text = "Tháng";
                ultraLabel6.Visible = false;
                txtDay.Visible = false;
                ultraLabel1.Location = new Point(12, 51);
                cbbMonth.Location = new Point(60, 47);
                ultraLabel2.Location = new Point(160, 51);
                txtYear.Location = new Point(203, 47);
                cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 21 && int.Parse(n.Value) > 8).ToList(), "Name");
                foreach (var item in cbbMonth.Rows)
                {
                    if (int.Parse((item.ListObject as Item).Value) == DateTime.Now.Month + 8) cbbMonth.SelectedRow = item;
                }
            }
        }

        private void ultraOptionSet2_ValueChanged(object sender, EventArgs e)
        {
            if (ultraOptionSet2.CheckedIndex == 1)
            {
                ultraLabel4.Visible = true;
                txtAdditionTime.Visible = true;
                ultraLabel5.Visible = true;
                dteKHBS.Visible = true;
                if (ultraOptionSet2.CheckedIndex == 2)
                {
                    string[] s = cbbMonth.Text.Split();
                    var temp1 = Utils.ListTM04GTGT.Where(p => p.Day == txtDay.Value.ToInt() && p.Month == s[1].ToInt() && p.Year == txtYear.Value.ToInt() && p.IsFirstDeclaration == false).ToList();
                    int maxAdditionalTime = temp1.Max(n => n.AdditionTime) ?? 0;
                    txtAdditionTime.Value = maxAdditionalTime + 1;
                }
                else
                {
                    var temp = Utils.ListTM04GTGT.Where(n => dteDateFrom.DateTime <= n.FromDate && dteDateTo.DateTime >= n.ToDate || dteDateFrom.DateTime >= n.FromDate && dteDateTo.DateTime <= n.ToDate && n.IsFirstDeclaration == false).ToList();
                    int maxAdditionalTime = temp.Max(n => n.AdditionTime) ?? 0;
                    txtAdditionTime.Value = maxAdditionalTime + 1;
                }
            }
            else
            {
                ultraLabel4.Visible = false;
                txtAdditionTime.Visible = false;
                ultraLabel5.Visible = false;
                dteKHBS.Visible = false;
            }
        }
        private void cbbMonth_ValueChanged(object sender, EventArgs e)
        {
            if (cbbMonth.SelectedRow != null)
            {
                var model = cbbMonth.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd((int)txtYear.Value,DateTime.Now, model, out dtBegin, out dtEnd);
                dteDateFrom.Value = new DateTime((int)txtYear.Value, dtBegin.Month, dtBegin.Day);
                dteDateTo.Value = new DateTime((int)txtYear.Value, dtEnd.Month, dtEnd.Day);
                if (ultraOptionSet2.CheckedIndex == 2)
                {
                    string[] s = cbbMonth.Text.Split();
                    var temp = Utils.ListTM04GTGT.FirstOrDefault(p => p.Day == txtDay.Value.ToInt() && p.Month == s[1].ToInt() && p.Year == txtYear.Value.ToInt() && p.IsFirstDeclaration == true);
                    if (temp != null)
                        ultraOptionSet2.CheckedIndex = 1;
                    else
                        ultraOptionSet2.CheckedIndex = 0;
                    var temp1 = Utils.ListTM04GTGT.Where(p => p.Day == txtDay.Value.ToInt() && p.Month == s[1].ToInt() && p.Year == txtYear.Value.ToInt() && p.IsFirstDeclaration == false).ToList();
                    int maxAdditionalTime = temp1.Max(n => n.AdditionTime) ?? 0;
                    txtAdditionTime.Value = maxAdditionalTime + 1;
                }
                else
                {
                    var temp = Utils.ListTM04GTGT.FirstOrDefault(p => p.FromDate.Month == dteDateFrom.DateTime.Month && p.ToDate.Month == dteDateTo.DateTime.Month && p.IsFirstDeclaration == true);
                    if (temp != null)
                        ultraOptionSet2.CheckedIndex = 1;
                    else
                        ultraOptionSet2.CheckedIndex = 0;
                    var temp1 = Utils.ListTM04GTGT.Where(n => dteDateFrom.DateTime <= n.FromDate && dteDateTo.DateTime >= n.ToDate || dteDateFrom.DateTime >= n.FromDate && dteDateTo.DateTime <= n.ToDate && n.IsFirstDeclaration == false).ToList();
                    int maxAdditionalTime = temp1.Max(n => n.AdditionTime) ?? 0;
                    txtAdditionTime.Value = maxAdditionalTime + 1;
                }
                
            }
        }
        private void txtYear_ValueChanged(object sender, EventArgs e)
        {
            if (txtYear.Value.ToInt() >= dteDateFrom.MinDate.Year && txtYear.Value.ToInt() <= dteDateFrom.MaxDate.Year)
            {
                DateTime t1 = dteDateFrom.DateTime;
                DateTime t2 = dteDateTo.DateTime;
                if (cbbMonth.Text == "Tháng 2")
                {
                    var model = cbbMonth.SelectedRow.ListObject as Item;
                    DateTime dtBegin;
                    DateTime dtEnd;
                    Utils.GetDateBeginDateEnd((int)txtYear.Value, DateTime.Now, model, out dtBegin, out dtEnd);
                    dteDateFrom.Value = new DateTime((int)txtYear.Value, t1.Month, t1.Day);
                    dteDateTo.Value = new DateTime((int)txtYear.Value, t2.Month, dtEnd.Day);
                }
                else
                {
                    dteDateFrom.Value = new DateTime((int)txtYear.Value, t1.Month, t1.Day);
                    dteDateTo.Value = new DateTime((int)txtYear.Value, t2.Month, t2.Day);
                }
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtYear.Value.ToInt() < dteDateFrom.MinDate.Year || txtYear.Value.ToInt() > dteDateFrom.MaxDate.Year)
            {
                txtYear.Value = DateTime.Now.Year;
                txtYear.Focus();
                return;
            }
            string DeclarationTerm = "";
            bool isDeclaredMonth = false;
            DateTime FromDate = DateTime.Now.Date;
            DateTime ToDate = DateTime.Now.Date;
            bool isDeclaredQuarter = false;
            bool isDeclaredArising = false;
            bool IsFirst = false;
            int MaxAdditionTime = 1;
            DateTime KHBS = DateTime.Now.Date;
            int Day = 1;
            int Month = 1;
            int Year = 1;
            string AdditionalTime = "";

            string[] s = cbbMonth.Text.Split();
            if (ultraOptionSet1.CheckedIndex == 0)
            {
                DeclarationTerm = cbbMonth.Text + " năm " + txtYear.Value;
                isDeclaredMonth = true;
                FromDate = dteDateFrom.DateTime;
                ToDate = dteDateTo.DateTime;
            }
            else if (ultraOptionSet1.CheckedIndex == 1)
            {
                DeclarationTerm = cbbMonth.Text + " năm " + txtYear.Value;
                isDeclaredQuarter = true;
                FromDate = dteDateFrom.DateTime;
                ToDate = dteDateTo.DateTime;
            }
            else
            {
                isDeclaredArising = true;
                DeclarationTerm = "ngày " + txtDay.Value + " tháng " + s[1] + " năm " + txtYear.Value;
                Day = txtDay.Value.ToInt();
                Month = int.Parse(s[1]);
                Year = txtYear.Value.ToInt();
            }
            if (ultraOptionSet2.Value.ToString() == "0")
            {
                if (ultraOptionSet1.Value.ToString() == "0")
                {
                    if (txtYear.Value.ToInt() > DateTime.Now.Year)
                    {
                        MSG.Warning("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                        return;
                    }
                    else if (txtYear.Value.ToInt() == DateTime.Now.Year)
                    {
                        if(int.Parse(s[1]) > DateTime.Now.Month)
                        {
                            MSG.Warning("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                            return;
                        }
                    }
                    if (txtYear.Value.ToInt() < dteDateFrom.MinDate.Year)
                    {
                        MSG.Warning("Năm không tồn tại");
                        return;
                    }
                    List<TM04GTGT> lstTm = Utils.ListTM04GTGT.Where(n => dteDateFrom.DateTime == n.FromDate && dteDateTo.DateTime == n.ToDate).ToList();
                    if (lstTm.Count > 0)
                    {
                        if (DialogResult.Yes == MessageBox.Show("Đã tồn tại tờ khai này trong kỳ trên. Bạn có muốn mở không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                        {
                            TM04GTGT tM04GTGT = lstTm.FirstOrDefault();
                            _04_GTGT_Detail temp = new _04_GTGT_Detail(tM04GTGT, true);
                            this.Hide();
                            this.Close();
                            this.Dispose();
                            temp.Show();
                           
                        }
                        return;
                    }
                    else
                    {
                    }
                }
                else if (ultraOptionSet1.Value.ToString() == "1")
                {
                    float now = float.Parse(DateTime.Now.Month.ToString()) / 3;
                    float diff = float.Parse(dteDateFrom.DateTime.Month.ToString()) / 3;
                    if (txtYear.Value.ToInt() > DateTime.Now.Year)
                    {
                        MSG.Warning("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                        return;
                    }
                    else if (txtYear.Value.ToInt() == DateTime.Now.Year)
                    {
                        if (now < diff)
                        {
                            MSG.Warning("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                            return;
                        }
                    }
                    if (txtYear.Value.ToInt() < dteDateFrom.MinDate.Year)
                    {
                        MSG.Warning("Năm không tồn tại");
                        return;
                    }
                    List<TM04GTGT> lstTm = Utils.ListTM04GTGT.Where(n => n.FromDate >= dteDateFrom.DateTime && dteDateTo.DateTime >= n.ToDate).ToList();
                    if (lstTm.Count > 0)
                    {
                        if (DialogResult.Yes == MessageBox.Show("Đã tồn tại tờ khai này trong kỳ trên. Bạn có muốn mở không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                        {
                            TM04GTGT tM04GTGT = lstTm.FirstOrDefault();
                            _04_GTGT_Detail temp = new _04_GTGT_Detail(tM04GTGT, true);
                            this.Hide();
                            this.Close();
                            this.Dispose();
                            temp.Show();
                            
                        }
                        return;
                    }
                    else
                    {
                    }
                }
                else
                {

                    if (txtYear.Value.ToInt() > DateTime.Now.Year)
                    {
                        MSG.Warning("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                        return;
                    }
                    else if (txtYear.Value.ToInt() == DateTime.Now.Year)
                    {
                        if (int.Parse(s[1]) > DateTime.Now.Month)
                        {
                            MSG.Warning("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                            return;
                        }
                        else if (int.Parse(s[1]) == DateTime.Now.Month)
                        {
                            if(txtDay.Value.ToInt() > DateTime.Now.Day)
                            {
                                MSG.Warning("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                                return;
                            }
                        }
                    }
                    if (txtYear.Value.ToInt() < dteDateFrom.MinDate.Year)
                    {
                        MSG.Warning("Năm không tồn tại");
                        return;
                    }
                    List<TM04GTGT> lstTm = Utils.ListTM04GTGT.Where(n => txtDay.Value.ToInt() == n.Day && int.Parse(s[1]) == n.Month && txtYear.Value.ToInt() == n.Year).ToList();
                    if (lstTm.Count > 0)
                    {
                        if (DialogResult.Yes == MessageBox.Show("Đã tồn tại tờ khai này trong kỳ trên. Bạn có muốn mở không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                        {
                            TM04GTGT tM04GTGT = lstTm.FirstOrDefault();
                            _04_GTGT_Detail temp = new _04_GTGT_Detail(tM04GTGT, true);
                            this.Hide();
                            this.Close();
                            this.Dispose();
                            temp.Show();
                            
                        }
                        return;
                    }
                    else
                    {
                    }
                }
                IsFirst = true;
                
            }
            else
            {

                var temp = Utils.ListTM04GTGT.Where(n => dteDateFrom.DateTime <= n.FromDate && dteDateTo.DateTime >= n.ToDate || dteDateFrom.DateTime >= n.FromDate && dteDateTo.DateTime <= n.ToDate && n.IsFirstDeclaration == false).ToList();
                if (temp.Count > 0)
                {
                    int maxAdditionalTime = temp.Max(n => n.AdditionTime) ?? 0;
                    MaxAdditionTime = maxAdditionalTime;
                    if (int.Parse(txtAdditionTime.Value.ToString()) != (maxAdditionalTime + 1))
                    {
                        MSG.Warning("Chưa tồn tại kỳ khai lần " + (int.Parse(txtAdditionTime.Value.ToString()) - 1));
                        return;
                    }
                    else
                    {
                        IsFirst = false;
                        AdditionalTime = txtAdditionTime.Value.ToString();
                        KHBS = dteKHBS.DateTime;
                    }
                }

            }
            _04_GTGT_Detail _04_GTGT_Detail = new _04_GTGT_Detail(new TM04GTGT(), false);
            _04_GTGT_Detail.DeclarationTerm = DeclarationTerm;
            _04_GTGT_Detail.isDeclaredMonth = isDeclaredMonth;
            _04_GTGT_Detail.FromDate = FromDate;
            _04_GTGT_Detail.ToDate = ToDate;
            _04_GTGT_Detail.isDeclaredQuarter = isDeclaredQuarter;
            _04_GTGT_Detail.isDeclaredArising = isDeclaredArising;
            _04_GTGT_Detail.IsFirst = IsFirst;
            _04_GTGT_Detail.MaxAdditionTime = MaxAdditionTime;
            _04_GTGT_Detail.KHBS = KHBS;
            _04_GTGT_Detail.AdditionalTime = AdditionalTime;
            _04_GTGT_Detail.Day = Day;
            _04_GTGT_Detail.Month = Month;
                _04_GTGT_Detail.Year = Year;
            this.Hide();
            this.Close();
            this.Dispose();
            if (_04_GTGT_Detail.LoadInitialize(false))
                _04_GTGT_Detail.Show();
         
        }

        private void TaxPeriod_04_GTGT_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (Form)sender;
            frm.Dispose();

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}
