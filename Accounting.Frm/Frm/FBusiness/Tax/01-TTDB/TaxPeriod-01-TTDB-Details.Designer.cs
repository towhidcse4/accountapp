﻿namespace Accounting
{
    partial class TaxPeriod_01_TTDB_Details
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TaxPeriod_01_TTDB_Details));
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel69 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.lblAgencyTaxCode = new Infragistics.Win.Misc.UltraLabel();
            this.lblTaxAgencyName = new Infragistics.Win.Misc.UltraLabel();
            this.lblCompanyName = new Infragistics.Win.Misc.UltraLabel();
            this.lblCompanyTaxCode = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel103 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAdditionTime = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.chkFirstDeclared = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel29 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel30 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel31 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel32 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel33 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel34 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel35 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel36 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel37 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel38 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel39 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel40 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel41 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraCheckEditor1 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraCheckEditor2 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel42 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel43 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel44 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel45 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel46 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel47 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel48 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel49 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel50 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdditionTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFirstDeclared)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCheckEditor1)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCheckEditor2)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.AutoScroll = true;
            this.ultraTabPageControl1.Controls.Add(this.ultraCheckEditor2);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel42);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel43);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel44);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel45);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel46);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel47);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel48);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel49);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel50);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel1);
            this.ultraTabPageControl1.Controls.Add(this.ultraCheckEditor1);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel33);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel34);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel35);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel36);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel37);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel38);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel39);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel40);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel41);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel24);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel25);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel26);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel27);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel28);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel29);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel30);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel31);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel32);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel23);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel4);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel3);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel2);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel1);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel69);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel19);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel20);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel21);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel22);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel18);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel17);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel16);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel15);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel14);
            this.ultraTabPageControl1.Controls.Add(this.lblAgencyTaxCode);
            this.ultraTabPageControl1.Controls.Add(this.lblTaxAgencyName);
            this.ultraTabPageControl1.Controls.Add(this.lblCompanyName);
            this.ultraTabPageControl1.Controls.Add(this.lblCompanyTaxCode);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel10);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel11);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel12);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel13);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel9);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel8);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel6);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel103);
            this.ultraTabPageControl1.Controls.Add(this.txtAdditionTime);
            this.ultraTabPageControl1.Controls.Add(this.chkFirstDeclared);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel5);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel7);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1444, 734);
            // 
            // ultraLabel23
            // 
            appearance31.BackColor = System.Drawing.Color.LightGray;
            appearance31.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance31.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance31.BorderColor = System.Drawing.Color.Black;
            appearance31.FontData.BoldAsString = "True";
            appearance31.ForeColor = System.Drawing.Color.Black;
            appearance31.TextHAlignAsString = "Center";
            appearance31.TextVAlignAsString = "Middle";
            this.ultraLabel23.Appearance = appearance31;
            this.ultraLabel23.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel23.Location = new System.Drawing.Point(1275, 238);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(155, 49);
            this.ultraLabel23.TabIndex = 412;
            this.ultraLabel23.Text = "Giá tính thuế TTĐB";
            // 
            // ultraLabel4
            // 
            appearance32.BackColor = System.Drawing.Color.LightGray;
            appearance32.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance32.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance32.BorderColor = System.Drawing.Color.Black;
            appearance32.FontData.BoldAsString = "True";
            appearance32.ForeColor = System.Drawing.Color.Black;
            appearance32.TextHAlignAsString = "Center";
            appearance32.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance32;
            this.ultraLabel4.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel4.Location = new System.Drawing.Point(1121, 238);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(155, 49);
            this.ultraLabel4.TabIndex = 411;
            this.ultraLabel4.Text = "Thuế tiêu thụ đặc biệt được trừ khấu";
            // 
            // ultraLabel3
            // 
            appearance33.BackColor = System.Drawing.Color.LightGray;
            appearance33.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance33.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance33.BorderColor = System.Drawing.Color.Black;
            appearance33.FontData.BoldAsString = "True";
            appearance33.ForeColor = System.Drawing.Color.Black;
            appearance33.TextHAlignAsString = "Center";
            appearance33.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance33;
            this.ultraLabel3.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel3.Location = new System.Drawing.Point(1082, 238);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(40, 49);
            this.ultraLabel3.TabIndex = 410;
            this.ultraLabel3.Text = "Thuế suất(%)";
            // 
            // ultraLabel2
            // 
            appearance34.BackColor = System.Drawing.Color.LightGray;
            appearance34.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance34.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance34.BorderColor = System.Drawing.Color.Black;
            appearance34.FontData.BoldAsString = "True";
            appearance34.ForeColor = System.Drawing.Color.Black;
            appearance34.TextHAlignAsString = "Center";
            appearance34.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance34;
            this.ultraLabel2.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel2.Location = new System.Drawing.Point(928, 238);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(155, 49);
            this.ultraLabel2.TabIndex = 409;
            this.ultraLabel2.Text = "Giá tính thuế TTĐB";
            // 
            // ultraLabel1
            // 
            appearance35.BackColor = System.Drawing.Color.LightGray;
            appearance35.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance35.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance35.BorderColor = System.Drawing.Color.Black;
            appearance35.FontData.BoldAsString = "True";
            appearance35.ForeColor = System.Drawing.Color.Black;
            appearance35.TextHAlignAsString = "Center";
            appearance35.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance35;
            this.ultraLabel1.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel1.Location = new System.Drawing.Point(774, 238);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(155, 49);
            this.ultraLabel1.TabIndex = 408;
            this.ultraLabel1.Text = "Doanh số bán(chưa có thuế GTGT)";
            // 
            // ultraLabel69
            // 
            appearance36.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel69.Appearance = appearance36;
            this.ultraLabel69.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel69.Location = new System.Drawing.Point(1206, 210);
            this.ultraLabel69.Name = "ultraLabel69";
            this.ultraLabel69.Size = new System.Drawing.Size(194, 22);
            this.ultraLabel69.TabIndex = 407;
            this.ultraLabel69.Text = "Đơn vị tiền: Đồng Việt Nam";
            // 
            // ultraLabel19
            // 
            appearance37.BackColor = System.Drawing.Color.LightGray;
            appearance37.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance37.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance37.BorderColor = System.Drawing.Color.Black;
            appearance37.FontData.BoldAsString = "True";
            appearance37.ForeColor = System.Drawing.Color.Black;
            appearance37.TextHAlignAsString = "Center";
            appearance37.TextVAlignAsString = "Middle";
            this.ultraLabel19.Appearance = appearance37;
            this.ultraLabel19.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel19.Location = new System.Drawing.Point(620, 238);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(155, 49);
            this.ultraLabel19.TabIndex = 362;
            this.ultraLabel19.Text = "Sản lượng tiêu thụ";
            // 
            // ultraLabel20
            // 
            appearance38.BackColor = System.Drawing.Color.LightGray;
            appearance38.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance38.BorderColor = System.Drawing.Color.Black;
            appearance38.FontData.BoldAsString = "True";
            appearance38.ForeColor = System.Drawing.Color.Black;
            appearance38.TextHAlignAsString = "Center";
            appearance38.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance38;
            this.ultraLabel20.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel20.Location = new System.Drawing.Point(500, 238);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(121, 49);
            this.ultraLabel20.TabIndex = 361;
            this.ultraLabel20.Text = "Đơn vị tính";
            // 
            // ultraLabel21
            // 
            appearance39.BackColor = System.Drawing.Color.LightGray;
            appearance39.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance39.BorderColor = System.Drawing.Color.Black;
            appearance39.FontData.BoldAsString = "True";
            appearance39.ForeColor = System.Drawing.Color.Black;
            appearance39.TextHAlignAsString = "Center";
            appearance39.TextVAlignAsString = "Middle";
            this.ultraLabel21.Appearance = appearance39;
            this.ultraLabel21.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel21.Location = new System.Drawing.Point(49, 238);
            this.ultraLabel21.Margin = new System.Windows.Forms.Padding(3, 3, 3, 5);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(452, 49);
            this.ultraLabel21.TabIndex = 360;
            this.ultraLabel21.Text = "Tên hàng hóa dịch vụ";
            // 
            // ultraLabel22
            // 
            appearance40.BackColor = System.Drawing.Color.LightGray;
            appearance40.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance40.BorderColor = System.Drawing.Color.Black;
            appearance40.BorderColor2 = System.Drawing.Color.Transparent;
            appearance40.FontData.BoldAsString = "True";
            appearance40.ForeColor = System.Drawing.Color.Black;
            appearance40.TextHAlignAsString = "Center";
            appearance40.TextVAlignAsString = "Middle";
            this.ultraLabel22.Appearance = appearance40;
            this.ultraLabel22.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel22.Location = new System.Drawing.Point(11, 238);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(39, 49);
            this.ultraLabel22.TabIndex = 359;
            this.ultraLabel22.Text = "STT";
            // 
            // ultraLabel18
            // 
            appearance41.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel18.Appearance = appearance41;
            this.ultraLabel18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel18.Location = new System.Drawing.Point(21, 189);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(32, 19);
            this.ultraLabel18.TabIndex = 358;
            this.ultraLabel18.Text = "[13]";
            // 
            // ultraLabel17
            // 
            appearance42.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel17.Appearance = appearance42;
            this.ultraLabel17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel17.Location = new System.Drawing.Point(21, 161);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(32, 19);
            this.ultraLabel17.TabIndex = 357;
            this.ultraLabel17.Text = "[12]";
            // 
            // ultraLabel16
            // 
            appearance43.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel16.Appearance = appearance43;
            this.ultraLabel16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel16.Location = new System.Drawing.Point(21, 134);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(32, 19);
            this.ultraLabel16.TabIndex = 356;
            this.ultraLabel16.Text = "[05]";
            // 
            // ultraLabel15
            // 
            appearance44.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel15.Appearance = appearance44;
            this.ultraLabel15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel15.Location = new System.Drawing.Point(21, 106);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(32, 19);
            this.ultraLabel15.TabIndex = 355;
            this.ultraLabel15.Text = "[04]";
            // 
            // ultraLabel14
            // 
            appearance45.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel14.Appearance = appearance45;
            this.ultraLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel14.Location = new System.Drawing.Point(575, 49);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(32, 19);
            this.ultraLabel14.TabIndex = 354;
            this.ultraLabel14.Text = "[01]";
            // 
            // lblAgencyTaxCode
            // 
            appearance46.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.lblAgencyTaxCode.Appearance = appearance46;
            this.lblAgencyTaxCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgencyTaxCode.Location = new System.Drawing.Point(220, 189);
            this.lblAgencyTaxCode.Name = "lblAgencyTaxCode";
            this.lblAgencyTaxCode.Size = new System.Drawing.Size(447, 16);
            this.lblAgencyTaxCode.TabIndex = 353;
            this.lblAgencyTaxCode.Text = "................................................................................." +
    "......................................";
            // 
            // lblTaxAgencyName
            // 
            appearance47.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.lblTaxAgencyName.Appearance = appearance47;
            this.lblTaxAgencyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaxAgencyName.Location = new System.Drawing.Point(220, 161);
            this.lblTaxAgencyName.Name = "lblTaxAgencyName";
            this.lblTaxAgencyName.Size = new System.Drawing.Size(447, 16);
            this.lblTaxAgencyName.TabIndex = 352;
            this.lblTaxAgencyName.Text = "................................................................................." +
    "......................................";
            // 
            // lblCompanyName
            // 
            appearance48.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.lblCompanyName.Appearance = appearance48;
            this.lblCompanyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompanyName.Location = new System.Drawing.Point(220, 134);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(447, 16);
            this.lblCompanyName.TabIndex = 351;
            this.lblCompanyName.Text = "................................................................................." +
    "......................................";
            // 
            // lblCompanyTaxCode
            // 
            appearance49.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.lblCompanyTaxCode.Appearance = appearance49;
            this.lblCompanyTaxCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompanyTaxCode.Location = new System.Drawing.Point(220, 106);
            this.lblCompanyTaxCode.Name = "lblCompanyTaxCode";
            this.lblCompanyTaxCode.Size = new System.Drawing.Size(447, 16);
            this.lblCompanyTaxCode.TabIndex = 350;
            this.lblCompanyTaxCode.Text = "................................................................................." +
    "......................................";
            // 
            // ultraLabel10
            // 
            appearance50.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel10.Appearance = appearance50;
            this.ultraLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel10.Location = new System.Drawing.Point(59, 189);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(106, 16);
            this.ultraLabel10.TabIndex = 349;
            this.ultraLabel10.Text = "Mã số thuế :";
            // 
            // ultraLabel11
            // 
            appearance51.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel11.Appearance = appearance51;
            this.ultraLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel11.Location = new System.Drawing.Point(59, 161);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(147, 16);
            this.ultraLabel11.TabIndex = 348;
            this.ultraLabel11.Text = "Đại lý thuế (nếu có):";
            // 
            // ultraLabel12
            // 
            appearance52.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel12.Appearance = appearance52;
            this.ultraLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel12.Location = new System.Drawing.Point(59, 134);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(119, 16);
            this.ultraLabel12.TabIndex = 347;
            this.ultraLabel12.Text = "Mã số thuế:";
            // 
            // ultraLabel13
            // 
            appearance53.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel13.Appearance = appearance53;
            this.ultraLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel13.Location = new System.Drawing.Point(59, 106);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(144, 14);
            this.ultraLabel13.TabIndex = 346;
            this.ultraLabel13.Text = "Tên người nộp thuế:";
            // 
            // ultraLabel9
            // 
            appearance54.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel9.Appearance = appearance54;
            this.ultraLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel9.Location = new System.Drawing.Point(675, 71);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(32, 19);
            this.ultraLabel9.TabIndex = 345;
            this.ultraLabel9.Text = "[03]";
            // 
            // ultraLabel8
            // 
            appearance55.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance55.TextHAlignAsString = "Center";
            appearance55.TextVAlignAsString = "Bottom";
            this.ultraLabel8.Appearance = appearance55;
            this.ultraLabel8.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.ultraLabel8.Location = new System.Drawing.Point(575, 71);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(56, 19);
            this.ultraLabel8.TabIndex = 344;
            this.ultraLabel8.Text = "Lần đầu:";
            // 
            // ultraLabel6
            // 
            appearance56.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance56.TextHAlignAsString = "Center";
            appearance56.TextVAlignAsString = "Bottom";
            this.ultraLabel6.Appearance = appearance56;
            this.ultraLabel6.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.ultraLabel6.Location = new System.Drawing.Point(602, 51);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(189, 17);
            this.ultraLabel6.TabIndex = 343;
            this.ultraLabel6.Text = "Kỳ tính thuế : Tháng .... năm .....";
            // 
            // ultraLabel103
            // 
            appearance57.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraLabel103.Appearance = appearance57;
            this.ultraLabel103.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel103.Location = new System.Drawing.Point(537, 71);
            this.ultraLabel103.Name = "ultraLabel103";
            this.ultraLabel103.Size = new System.Drawing.Size(32, 19);
            this.ultraLabel103.TabIndex = 342;
            this.ultraLabel103.Text = "[02]";
            // 
            // txtAdditionTime
            // 
            this.txtAdditionTime.Location = new System.Drawing.Point(808, 70);
            this.txtAdditionTime.Name = "txtAdditionTime";
            this.txtAdditionTime.Size = new System.Drawing.Size(40, 21);
            this.txtAdditionTime.TabIndex = 341;
            // 
            // chkFirstDeclared
            // 
            this.chkFirstDeclared.BackColor = System.Drawing.Color.Transparent;
            this.chkFirstDeclared.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkFirstDeclared.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkFirstDeclared.Location = new System.Drawing.Point(637, 71);
            this.chkFirstDeclared.Name = "chkFirstDeclared";
            this.chkFirstDeclared.Size = new System.Drawing.Size(19, 20);
            this.chkFirstDeclared.TabIndex = 340;
            // 
            // ultraLabel5
            // 
            appearance58.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance58.TextHAlignAsString = "Center";
            appearance58.TextVAlignAsString = "Bottom";
            this.ultraLabel5.Appearance = appearance58;
            this.ultraLabel5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel5.Location = new System.Drawing.Point(713, 74);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(89, 16);
            this.ultraLabel5.TabIndex = 339;
            this.ultraLabel5.Text = "Bổ sung lần thứ:";
            // 
            // ultraLabel7
            // 
            appearance59.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance59.TextHAlignAsString = "Center";
            appearance59.TextVAlignAsString = "Bottom";
            this.ultraLabel7.Appearance = appearance59;
            this.ultraLabel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraLabel7.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel7.Location = new System.Drawing.Point(0, 0);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(1444, 45);
            this.ultraLabel7.TabIndex = 338;
            this.ultraLabel7.Text = "TỜ KHAI TIÊU THỤ ĐẶC BIỆT(Mẫu số 01/TTĐB)";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.AutoScroll = true;
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1444, 734);
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(1448, 760);
            this.ultraTabControl1.TabIndex = 0;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Tờ khai chính";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Phụ lục 01-1/TTĐB";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1444, 734);
            // 
            // ultraLabel24
            // 
            appearance22.BackColor = System.Drawing.Color.LightGray;
            appearance22.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance22.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance22.BorderColor = System.Drawing.Color.Black;
            appearance22.ForeColor = System.Drawing.Color.Black;
            appearance22.TextHAlignAsString = "Center";
            appearance22.TextVAlignAsString = "Middle";
            this.ultraLabel24.Appearance = appearance22;
            this.ultraLabel24.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel24.Location = new System.Drawing.Point(1275, 286);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(155, 32);
            this.ultraLabel24.TabIndex = 421;
            this.ultraLabel24.Text = "(9) = (6) x (7) - (8)";
            // 
            // ultraLabel25
            // 
            appearance23.BackColor = System.Drawing.Color.LightGray;
            appearance23.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance23.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance23.BorderColor = System.Drawing.Color.Black;
            appearance23.ForeColor = System.Drawing.Color.Black;
            appearance23.TextHAlignAsString = "Center";
            appearance23.TextVAlignAsString = "Middle";
            this.ultraLabel25.Appearance = appearance23;
            this.ultraLabel25.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel25.Location = new System.Drawing.Point(1121, 286);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(155, 32);
            this.ultraLabel25.TabIndex = 420;
            this.ultraLabel25.Text = "(8)";
            // 
            // ultraLabel26
            // 
            appearance24.BackColor = System.Drawing.Color.LightGray;
            appearance24.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance24.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance24.BorderColor = System.Drawing.Color.Black;
            appearance24.ForeColor = System.Drawing.Color.Black;
            appearance24.TextHAlignAsString = "Center";
            appearance24.TextVAlignAsString = "Middle";
            this.ultraLabel26.Appearance = appearance24;
            this.ultraLabel26.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel26.Location = new System.Drawing.Point(1082, 286);
            this.ultraLabel26.Name = "ultraLabel26";
            this.ultraLabel26.Size = new System.Drawing.Size(40, 32);
            this.ultraLabel26.TabIndex = 419;
            this.ultraLabel26.Text = "(7)";
            // 
            // ultraLabel27
            // 
            appearance25.BackColor = System.Drawing.Color.LightGray;
            appearance25.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance25.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance25.BorderColor = System.Drawing.Color.Black;
            appearance25.ForeColor = System.Drawing.Color.Black;
            appearance25.TextHAlignAsString = "Center";
            appearance25.TextVAlignAsString = "Middle";
            this.ultraLabel27.Appearance = appearance25;
            this.ultraLabel27.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel27.Location = new System.Drawing.Point(928, 286);
            this.ultraLabel27.Name = "ultraLabel27";
            this.ultraLabel27.Size = new System.Drawing.Size(155, 32);
            this.ultraLabel27.TabIndex = 418;
            this.ultraLabel27.Text = "(6)";
            // 
            // ultraLabel28
            // 
            appearance26.BackColor = System.Drawing.Color.LightGray;
            appearance26.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance26.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance26.BorderColor = System.Drawing.Color.Black;
            appearance26.ForeColor = System.Drawing.Color.Black;
            appearance26.TextHAlignAsString = "Center";
            appearance26.TextVAlignAsString = "Middle";
            this.ultraLabel28.Appearance = appearance26;
            this.ultraLabel28.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel28.Location = new System.Drawing.Point(774, 286);
            this.ultraLabel28.Name = "ultraLabel28";
            this.ultraLabel28.Size = new System.Drawing.Size(155, 32);
            this.ultraLabel28.TabIndex = 417;
            this.ultraLabel28.Text = "(5)";
            // 
            // ultraLabel29
            // 
            appearance27.BackColor = System.Drawing.Color.LightGray;
            appearance27.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance27.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance27.BorderColor = System.Drawing.Color.Black;
            appearance27.ForeColor = System.Drawing.Color.Black;
            appearance27.TextHAlignAsString = "Center";
            appearance27.TextVAlignAsString = "Middle";
            this.ultraLabel29.Appearance = appearance27;
            this.ultraLabel29.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel29.Location = new System.Drawing.Point(620, 286);
            this.ultraLabel29.Name = "ultraLabel29";
            this.ultraLabel29.Size = new System.Drawing.Size(155, 32);
            this.ultraLabel29.TabIndex = 416;
            this.ultraLabel29.Text = "(4)";
            // 
            // ultraLabel30
            // 
            appearance28.BackColor = System.Drawing.Color.LightGray;
            appearance28.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance28.BorderColor = System.Drawing.Color.Black;
            appearance28.ForeColor = System.Drawing.Color.Black;
            appearance28.TextHAlignAsString = "Center";
            appearance28.TextVAlignAsString = "Middle";
            this.ultraLabel30.Appearance = appearance28;
            this.ultraLabel30.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel30.Location = new System.Drawing.Point(500, 286);
            this.ultraLabel30.Name = "ultraLabel30";
            this.ultraLabel30.Size = new System.Drawing.Size(121, 32);
            this.ultraLabel30.TabIndex = 415;
            this.ultraLabel30.Text = "(3)";
            // 
            // ultraLabel31
            // 
            appearance29.BackColor = System.Drawing.Color.LightGray;
            appearance29.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance29.BorderColor = System.Drawing.Color.Black;
            appearance29.ForeColor = System.Drawing.Color.Black;
            appearance29.TextHAlignAsString = "Center";
            appearance29.TextVAlignAsString = "Middle";
            this.ultraLabel31.Appearance = appearance29;
            this.ultraLabel31.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel31.Location = new System.Drawing.Point(49, 286);
            this.ultraLabel31.Margin = new System.Windows.Forms.Padding(3, 3, 3, 5);
            this.ultraLabel31.Name = "ultraLabel31";
            this.ultraLabel31.Size = new System.Drawing.Size(452, 32);
            this.ultraLabel31.TabIndex = 414;
            this.ultraLabel31.Text = "(2)";
            // 
            // ultraLabel32
            // 
            appearance30.BackColor = System.Drawing.Color.LightGray;
            appearance30.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance30.BorderColor = System.Drawing.Color.Black;
            appearance30.BorderColor2 = System.Drawing.Color.Transparent;
            appearance30.ForeColor = System.Drawing.Color.Black;
            appearance30.TextHAlignAsString = "Center";
            appearance30.TextVAlignAsString = "Middle";
            this.ultraLabel32.Appearance = appearance30;
            this.ultraLabel32.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel32.Location = new System.Drawing.Point(11, 286);
            this.ultraLabel32.Name = "ultraLabel32";
            this.ultraLabel32.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel32.TabIndex = 413;
            this.ultraLabel32.Text = "(1)";
            // 
            // ultraLabel33
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance13.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance13.BorderColor = System.Drawing.Color.Black;
            appearance13.ForeColor = System.Drawing.Color.Black;
            appearance13.TextHAlignAsString = "Center";
            appearance13.TextVAlignAsString = "Middle";
            this.ultraLabel33.Appearance = appearance13;
            this.ultraLabel33.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel33.Location = new System.Drawing.Point(1275, 317);
            this.ultraLabel33.Name = "ultraLabel33";
            this.ultraLabel33.Size = new System.Drawing.Size(155, 32);
            this.ultraLabel33.TabIndex = 430;
            // 
            // ultraLabel34
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance14.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance14.BorderColor = System.Drawing.Color.Black;
            appearance14.ForeColor = System.Drawing.Color.Black;
            appearance14.TextHAlignAsString = "Center";
            appearance14.TextVAlignAsString = "Middle";
            this.ultraLabel34.Appearance = appearance14;
            this.ultraLabel34.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel34.Location = new System.Drawing.Point(1121, 317);
            this.ultraLabel34.Name = "ultraLabel34";
            this.ultraLabel34.Size = new System.Drawing.Size(155, 32);
            this.ultraLabel34.TabIndex = 429;
            // 
            // ultraLabel35
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance15.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance15.BorderColor = System.Drawing.Color.Black;
            appearance15.ForeColor = System.Drawing.Color.Black;
            appearance15.TextHAlignAsString = "Center";
            appearance15.TextVAlignAsString = "Middle";
            this.ultraLabel35.Appearance = appearance15;
            this.ultraLabel35.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel35.Location = new System.Drawing.Point(1082, 317);
            this.ultraLabel35.Name = "ultraLabel35";
            this.ultraLabel35.Size = new System.Drawing.Size(40, 32);
            this.ultraLabel35.TabIndex = 428;
            // 
            // ultraLabel36
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance16.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance16.BorderColor = System.Drawing.Color.Black;
            appearance16.ForeColor = System.Drawing.Color.Black;
            appearance16.TextHAlignAsString = "Center";
            appearance16.TextVAlignAsString = "Middle";
            this.ultraLabel36.Appearance = appearance16;
            this.ultraLabel36.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel36.Location = new System.Drawing.Point(928, 317);
            this.ultraLabel36.Name = "ultraLabel36";
            this.ultraLabel36.Size = new System.Drawing.Size(155, 32);
            this.ultraLabel36.TabIndex = 427;
            // 
            // ultraLabel37
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance17.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance17.BorderColor = System.Drawing.Color.Black;
            appearance17.ForeColor = System.Drawing.Color.Black;
            appearance17.TextHAlignAsString = "Center";
            appearance17.TextVAlignAsString = "Middle";
            this.ultraLabel37.Appearance = appearance17;
            this.ultraLabel37.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel37.Location = new System.Drawing.Point(774, 317);
            this.ultraLabel37.Name = "ultraLabel37";
            this.ultraLabel37.Size = new System.Drawing.Size(155, 32);
            this.ultraLabel37.TabIndex = 426;
            // 
            // ultraLabel38
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance18.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance18.BorderColor = System.Drawing.Color.Black;
            appearance18.ForeColor = System.Drawing.Color.Black;
            appearance18.TextHAlignAsString = "Center";
            appearance18.TextVAlignAsString = "Middle";
            this.ultraLabel38.Appearance = appearance18;
            this.ultraLabel38.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel38.Location = new System.Drawing.Point(620, 317);
            this.ultraLabel38.Name = "ultraLabel38";
            this.ultraLabel38.Size = new System.Drawing.Size(155, 32);
            this.ultraLabel38.TabIndex = 425;
            // 
            // ultraLabel39
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance19.BorderColor = System.Drawing.Color.Black;
            appearance19.ForeColor = System.Drawing.Color.Black;
            appearance19.TextHAlignAsString = "Center";
            appearance19.TextVAlignAsString = "Middle";
            this.ultraLabel39.Appearance = appearance19;
            this.ultraLabel39.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel39.Location = new System.Drawing.Point(500, 317);
            this.ultraLabel39.Name = "ultraLabel39";
            this.ultraLabel39.Size = new System.Drawing.Size(121, 32);
            this.ultraLabel39.TabIndex = 424;
            // 
            // ultraLabel40
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            appearance20.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance20.BorderColor = System.Drawing.Color.Black;
            appearance20.ForeColor = System.Drawing.Color.Black;
            appearance20.TextVAlignAsString = "Middle";
            this.ultraLabel40.Appearance = appearance20;
            this.ultraLabel40.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel40.Location = new System.Drawing.Point(49, 317);
            this.ultraLabel40.Margin = new System.Windows.Forms.Padding(3, 3, 3, 5);
            this.ultraLabel40.Name = "ultraLabel40";
            this.ultraLabel40.Size = new System.Drawing.Size(411, 32);
            this.ultraLabel40.TabIndex = 423;
            this.ultraLabel40.Text = "Không phát sinh giá trị tính thuế TTĐB trong kỳ";
            // 
            // ultraLabel41
            // 
            appearance21.BackColor = System.Drawing.Color.Transparent;
            appearance21.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance21.BorderColor = System.Drawing.Color.Black;
            appearance21.BorderColor2 = System.Drawing.Color.Transparent;
            appearance21.ForeColor = System.Drawing.Color.Black;
            appearance21.TextHAlignAsString = "Center";
            appearance21.TextVAlignAsString = "Middle";
            this.ultraLabel41.Appearance = appearance21;
            this.ultraLabel41.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel41.Location = new System.Drawing.Point(11, 317);
            this.ultraLabel41.Name = "ultraLabel41";
            this.ultraLabel41.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel41.TabIndex = 422;
            // 
            // ultraCheckEditor1
            // 
            appearance12.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance12.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance12.BorderColor = System.Drawing.Color.Black;
            this.ultraCheckEditor1.Appearance = appearance12;
            this.ultraCheckEditor1.BackColor = System.Drawing.Color.Transparent;
            this.ultraCheckEditor1.BackColorInternal = System.Drawing.Color.White;
            this.ultraCheckEditor1.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraCheckEditor1.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ultraCheckEditor1.Location = new System.Drawing.Point(459, 317);
            this.ultraCheckEditor1.Name = "ultraCheckEditor1";
            this.ultraCheckEditor1.Size = new System.Drawing.Size(42, 32);
            this.ultraCheckEditor1.TabIndex = 431;
            // 
            // ultraPanel1
            // 
            appearance11.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance11.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance11.BorderColor = System.Drawing.Color.Black;
            this.ultraPanel1.Appearance = appearance11;
            this.ultraPanel1.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.uGrid1);
            this.ultraPanel1.Location = new System.Drawing.Point(11, 435);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(1419, 32);
            this.ultraPanel1.TabIndex = 432;
            // 
            // uGrid1
            // 
            this.uGrid1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid1.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid1.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGrid1.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid1.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid1.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid1.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid1.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid1.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid1.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid1.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGrid1.Location = new System.Drawing.Point(8, 8);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1002, 30);
            this.uGrid1.TabIndex = 25;
            this.uGrid1.Text = "uGrid";
            // 
            // ultraCheckEditor2
            // 
            appearance1.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance1.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance1.BorderColor = System.Drawing.Color.Black;
            appearance1.FontData.BoldAsString = "True";
            this.ultraCheckEditor2.Appearance = appearance1;
            this.ultraCheckEditor2.BackColor = System.Drawing.Color.Transparent;
            this.ultraCheckEditor2.BackColorInternal = System.Drawing.Color.White;
            this.ultraCheckEditor2.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraCheckEditor2.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ultraCheckEditor2.Location = new System.Drawing.Point(459, 355);
            this.ultraCheckEditor2.Name = "ultraCheckEditor2";
            this.ultraCheckEditor2.Size = new System.Drawing.Size(42, 32);
            this.ultraCheckEditor2.TabIndex = 442;
            // 
            // ultraLabel42
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance2.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance2.BorderColor = System.Drawing.Color.Black;
            appearance2.FontData.BoldAsString = "True";
            appearance2.ForeColor = System.Drawing.Color.Black;
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel42.Appearance = appearance2;
            this.ultraLabel42.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel42.Location = new System.Drawing.Point(1275, 355);
            this.ultraLabel42.Name = "ultraLabel42";
            this.ultraLabel42.Size = new System.Drawing.Size(155, 32);
            this.ultraLabel42.TabIndex = 441;
            // 
            // ultraLabel43
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance3.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance3.BorderColor = System.Drawing.Color.Black;
            appearance3.FontData.BoldAsString = "True";
            appearance3.ForeColor = System.Drawing.Color.Black;
            appearance3.TextHAlignAsString = "Center";
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel43.Appearance = appearance3;
            this.ultraLabel43.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel43.Location = new System.Drawing.Point(1121, 355);
            this.ultraLabel43.Name = "ultraLabel43";
            this.ultraLabel43.Size = new System.Drawing.Size(155, 32);
            this.ultraLabel43.TabIndex = 440;
            // 
            // ultraLabel44
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance4.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance4.BorderColor = System.Drawing.Color.Black;
            appearance4.FontData.BoldAsString = "True";
            appearance4.ForeColor = System.Drawing.Color.Black;
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel44.Appearance = appearance4;
            this.ultraLabel44.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel44.Location = new System.Drawing.Point(1082, 355);
            this.ultraLabel44.Name = "ultraLabel44";
            this.ultraLabel44.Size = new System.Drawing.Size(40, 32);
            this.ultraLabel44.TabIndex = 439;
            // 
            // ultraLabel45
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance5.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance5.BorderColor = System.Drawing.Color.Black;
            appearance5.FontData.BoldAsString = "True";
            appearance5.ForeColor = System.Drawing.Color.Black;
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel45.Appearance = appearance5;
            this.ultraLabel45.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel45.Location = new System.Drawing.Point(928, 355);
            this.ultraLabel45.Name = "ultraLabel45";
            this.ultraLabel45.Size = new System.Drawing.Size(155, 32);
            this.ultraLabel45.TabIndex = 438;
            // 
            // ultraLabel46
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance6.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance6.BorderColor = System.Drawing.Color.Black;
            appearance6.FontData.BoldAsString = "True";
            appearance6.ForeColor = System.Drawing.Color.Black;
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel46.Appearance = appearance6;
            this.ultraLabel46.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel46.Location = new System.Drawing.Point(774, 355);
            this.ultraLabel46.Name = "ultraLabel46";
            this.ultraLabel46.Size = new System.Drawing.Size(155, 32);
            this.ultraLabel46.TabIndex = 437;
            // 
            // ultraLabel47
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance7.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance7.BorderColor = System.Drawing.Color.Black;
            appearance7.FontData.BoldAsString = "True";
            appearance7.ForeColor = System.Drawing.Color.Black;
            appearance7.TextHAlignAsString = "Center";
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel47.Appearance = appearance7;
            this.ultraLabel47.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel47.Location = new System.Drawing.Point(620, 355);
            this.ultraLabel47.Name = "ultraLabel47";
            this.ultraLabel47.Size = new System.Drawing.Size(155, 32);
            this.ultraLabel47.TabIndex = 436;
            // 
            // ultraLabel48
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance8.BorderColor = System.Drawing.Color.Black;
            appearance8.FontData.BoldAsString = "True";
            appearance8.ForeColor = System.Drawing.Color.Black;
            appearance8.TextHAlignAsString = "Center";
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel48.Appearance = appearance8;
            this.ultraLabel48.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel48.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel48.Location = new System.Drawing.Point(500, 355);
            this.ultraLabel48.Name = "ultraLabel48";
            this.ultraLabel48.Size = new System.Drawing.Size(121, 32);
            this.ultraLabel48.TabIndex = 435;
            // 
            // ultraLabel49
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance9.BorderColor = System.Drawing.Color.Black;
            appearance9.FontData.BoldAsString = "True";
            appearance9.ForeColor = System.Drawing.Color.Black;
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel49.Appearance = appearance9;
            this.ultraLabel49.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel49.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel49.Location = new System.Drawing.Point(49, 355);
            this.ultraLabel49.Margin = new System.Windows.Forms.Padding(3, 3, 3, 5);
            this.ultraLabel49.Name = "ultraLabel49";
            this.ultraLabel49.Size = new System.Drawing.Size(411, 32);
            this.ultraLabel49.TabIndex = 434;
            this.ultraLabel49.Text = "Không phát sinh giá trị tính thuế TTĐB trong kỳ";
            // 
            // ultraLabel50
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance10.BorderColor = System.Drawing.Color.Black;
            appearance10.BorderColor2 = System.Drawing.Color.Transparent;
            appearance10.FontData.BoldAsString = "True";
            appearance10.ForeColor = System.Drawing.Color.Black;
            appearance10.TextHAlignAsString = "Center";
            appearance10.TextVAlignAsString = "Middle";
            this.ultraLabel50.Appearance = appearance10;
            this.ultraLabel50.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraLabel50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel50.Location = new System.Drawing.Point(11, 355);
            this.ultraLabel50.Name = "ultraLabel50";
            this.ultraLabel50.Size = new System.Drawing.Size(39, 32);
            this.ultraLabel50.TabIndex = 433;
            // 
            // TaxPeriod_01_TTDB_Details
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1448, 760);
            this.Controls.Add(this.ultraTabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TaxPeriod_01_TTDB_Details";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tờ khai tiêu thụ đặc biệt (01/TTĐB)";
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdditionTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFirstDeclared)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraCheckEditor1)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCheckEditor2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel lblAgencyTaxCode;
        private Infragistics.Win.Misc.UltraLabel lblTaxAgencyName;
        private Infragistics.Win.Misc.UltraLabel lblCompanyName;
        private Infragistics.Win.Misc.UltraLabel lblCompanyTaxCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel103;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAdditionTime;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkFirstDeclared;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel69;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.Misc.UltraLabel ultraLabel26;
        private Infragistics.Win.Misc.UltraLabel ultraLabel27;
        private Infragistics.Win.Misc.UltraLabel ultraLabel28;
        private Infragistics.Win.Misc.UltraLabel ultraLabel29;
        private Infragistics.Win.Misc.UltraLabel ultraLabel30;
        private Infragistics.Win.Misc.UltraLabel ultraLabel31;
        private Infragistics.Win.Misc.UltraLabel ultraLabel32;
        private Infragistics.Win.Misc.UltraLabel ultraLabel33;
        private Infragistics.Win.Misc.UltraLabel ultraLabel34;
        private Infragistics.Win.Misc.UltraLabel ultraLabel35;
        private Infragistics.Win.Misc.UltraLabel ultraLabel36;
        private Infragistics.Win.Misc.UltraLabel ultraLabel37;
        private Infragistics.Win.Misc.UltraLabel ultraLabel38;
        private Infragistics.Win.Misc.UltraLabel ultraLabel39;
        private Infragistics.Win.Misc.UltraLabel ultraLabel40;
        private Infragistics.Win.Misc.UltraLabel ultraLabel41;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor ultraCheckEditor1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor ultraCheckEditor2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel42;
        private Infragistics.Win.Misc.UltraLabel ultraLabel43;
        private Infragistics.Win.Misc.UltraLabel ultraLabel44;
        private Infragistics.Win.Misc.UltraLabel ultraLabel45;
        private Infragistics.Win.Misc.UltraLabel ultraLabel46;
        private Infragistics.Win.Misc.UltraLabel ultraLabel47;
        private Infragistics.Win.Misc.UltraLabel ultraLabel48;
        private Infragistics.Win.Misc.UltraLabel ultraLabel49;
        private Infragistics.Win.Misc.UltraLabel ultraLabel50;
    }
}