﻿using Accounting.Core;
using Accounting.Core.Domain;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting
{
    public partial class TaxPeriod_01_TTDB : Form
    {
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        public Tax Tax;
        public TaxPeriod_01_TTDB()
        {
            InitializeComponent();
            txtYear.Value = DateTime.Now.Year;
            ultraLabel1.Location = new Point(12, 53);
            cbbMonth.Location = new Point(50, 49);
            ultraLabel2.Location = new Point(147, 53);
            txtYear.Location = new Point(199, 49);
            InitializeGUI();
        }
        private void InitializeGUI()
        {
            #region Config cbbMonth
            cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 21 && int.Parse(n.Value) > 8).ToList(), "Name");
            cbbMonth.DropDownStyle = UltraComboStyle.DropDownList;
            try
            {
                foreach (var item in cbbMonth.Rows)
                {
                    if (int.Parse((item.ListObject as Item).Value) == DateTime.Now.Month + 8) cbbMonth.SelectedRow = item;
                }
            }
            catch (Exception e)
            {

            }
            #endregion
        }
        private void ultraOptionSet1_ValueChanged(object sender, EventArgs e)
        {
            if (ultraOptionSet1.CheckedIndex == 1)
            {
                ultraLabel6.Visible = true;
                txtDay.Visible = true;
                ultraLabel6.Location = new Point(12, 53);
                txtDay.Location = new Point(50, 49);
                ultraLabel1.Location = new Point(147, 53);
                cbbMonth.Location = new Point(199, 49);
                ultraLabel2.Location = new Point(312, 53);
                txtYear.Location = new Point(350, 49);
            }
            else
            {
                ultraLabel6.Visible = false;
                txtDay.Visible = false;
                ultraLabel1.Location = new Point(12, 53);
                cbbMonth.Location = new Point(50, 49);
                ultraLabel2.Location = new Point(147, 53);
                txtYear.Location = new Point(199, 49);
            }
        }

        private void ultraOptionSet2_ValueChanged(object sender, EventArgs e)
        {
            if (ultraOptionSet1.CheckedIndex == 1)
            {
                ultraLabel4.Visible = true;
                txtNumber.Visible = true;
                ultraLabel5.Visible = true;
                dteKHBS.Visible = true;
            }
            else
            {
                ultraLabel4.Visible = false;
                txtNumber.Visible = false;
                ultraLabel5.Visible = false;
                dteKHBS.Visible = false;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            TaxPeriod_01_TTDB_Details taxPeriod_01_TTDB_Details = new TaxPeriod_01_TTDB_Details();
            taxPeriod_01_TTDB_Details.Show(this);
            this.Close();
        }
    }
}
