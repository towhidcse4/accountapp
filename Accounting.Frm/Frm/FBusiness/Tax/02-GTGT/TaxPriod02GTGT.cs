﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting
{
    public class TaxPriod02GTGT
    {
        public string index { get; set; }
        public string AdjustmentCriteria { get; set; }
        public string AdjustmentCriteriaCodeNumber { get; set; }
        public string NumberDeclared { get; set; }
        public string NumberAdjustment { get; set; }
        public string Difference { get; set; }
        public int Type { get; set; }
    }

    public class TM02TN
    {
        public string index1 { get; set; }
        public string index2 { get; set; }
        public string index3 { get; set; }
        public string index4 { get; set; }
        public string index5 { get; set; }
        public string index6 { get; set; }
        public string index7 { get; set; }
        public string index8 { get; set; }
        public string index9 { get; set; }
        public string index10 { get; set; }
        public string index11 { get; set; }
        public string index12 { get; set; }
    }
}
