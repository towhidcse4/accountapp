﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting
{
    public partial class TaxPeriod_02_GTGT : Form
    {
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        public TaxPeriod_02_GTGT()
        {
            InitializeComponent();
            txtYear.Value = DateTime.Now.Year;
            InitializeGUI();

        }
        private void InitializeGUI()
        {
            cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 21 && int.Parse(n.Value) > 8).ToList(), "Name");
            cbbMonth.DropDownStyle = UltraComboStyle.DropDownList;
            foreach (var item in cbbMonth.Rows)
            {
                if (int.Parse((item.ListObject as Item).Value) == DateTime.Now.Month + 8) cbbMonth.SelectedRow = item;
            }
        }
        private void ultraLabel1_Click(object sender, EventArgs e)
        {

        }

        private void ultraOptionSet1_ValueChanged(object sender, EventArgs e)
        {
            if (ultraOptionSet1.CheckedIndex == 0)
            {
                ultraLabel1.Text = "Tháng";
                try
                {
                    cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 21 && int.Parse(n.Value) > 8).ToList(), "Name");
                }
                catch (Exception ex)
                {

                }
                foreach (var item in cbbMonth.Rows)
                {
                    if (int.Parse((item.ListObject as Item).Value) == DateTime.Now.Month + 8) cbbMonth.SelectedRow = item;
                }
            }
            else
            {
                ultraLabel1.Text = "Quý";
                try
                {
                    cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 25 && int.Parse(n.Value) > 20).ToList(), "Name");
                }
                catch (Exception ex)
                {

                }
                
                float now = float.Parse(DateTime.Now.Month.ToString()) / 3;
                foreach (var item in cbbMonth.Rows)
                {
                    if (now <= 1)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 21) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 2 && now > 1)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 22) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 3 && now > 2)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 23) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 4 && now > 3)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 24) cbbMonth.SelectedRow = item;
                    }

                }

            }
        }

        private void cbbMonth_ValueChanged(object sender, EventArgs e)
        {
            string g = cbbMonth.Value.ToString();
            string[] arrListStr = g.Split(' ');
              int t9 = arrListStr[1].ToInt();
            if ((t9>DateTime.Now.Month)&&(txtYear.Value.ToInt()>=DateTime.Now.Year))
            {
                MSG.Warning("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                return;
            }
            if (cbbMonth.SelectedRow != null)
            {
                var model = cbbMonth.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd((int)txtYear.Value, DateTime.Now, model, out dtBegin, out dtEnd);
                dteDateFrom.Value = new DateTime((int)txtYear.Value, dtBegin.Month, dtBegin.Day);
                dteDateTo.Value = new DateTime((int)txtYear.Value, dtEnd.Month, dtEnd.Day);
            }
        }

        private void txtYear_ValueChanged(object sender, EventArgs e)
        {
            //if (txtYear.Value.ToInt() < dteDateFrom.MinDate.Year)
            //{
            //    MSG.Warning("Năm không tồn tại");
            //    return;
            //}
            //if (txtYear.Value.ToInt() > DateTime.Now.Year)
            //{
            //    MSG.Warning("Kỳ kê khai không được lớn hơn kỳ hiện tại");
            //    return;
            //}
            DateTime t1 = dteDateFrom.DateTime;
            DateTime t2 = dteDateTo.DateTime;
            if (cbbMonth.Text == "Tháng 2")
            {
                var model = cbbMonth.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd((int)txtYear.Value, DateTime.Now, model, out dtBegin, out dtEnd);
                dteDateFrom.Value = new DateTime((int)txtYear.Value, t1.Month, t1.Day);
                dteDateTo.Value = new DateTime((int)txtYear.Value, t2.Month, dtEnd.Day);
            }
            else
            {
                dteDateFrom.Value = new DateTime((int)txtYear.Value, t1.Month, t1.Day);
                dteDateTo.Value = new DateTime((int)txtYear.Value, t2.Month, t2.Day);
            }
        }

        private void ultraOptionSet2_ValueChanged(object sender, EventArgs e)
        {
            if (ultraOptionSet2.CheckedIndex == 1)
            {
                ultraOptionSet2.CheckedIndex = 0;
                return;//

                ultraLabel4.Visible = true;
                txtNumber.Visible = true;
                ultraLabel5.Visible = true;
                dteKHBS.Visible = true;
            }
            else
            {
                ultraLabel4.Visible = false;
                txtNumber.Visible = false;
                ultraLabel5.Visible = false;
                dteKHBS.Visible = false;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool item02 = false;
            string item03 = "";
            DateTime DateBS = DateTime.Now;
            if (ultraOptionSet2.Value.ToString() == "0")
            {
                item02 = true;
                List<TM02GTGT> lstTm = Utils.ListTM02GTGT.Where(n => dteDateFrom.DateTime <= n.FromDate && dteDateTo.DateTime >= n.ToDate || dteDateFrom.DateTime >= n.FromDate && dteDateTo.DateTime <= n.ToDate).ToList();
                if (lstTm.Count > 0)
                {
                    if (DialogResult.Yes == MessageBox.Show("Đã tồn tại tờ khai này trong kỳ trên. Bạn có muốn mở không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        TM02GTGT tM02GTGT = lstTm.FirstOrDefault();
                        //declaration_02_GTGT(tM02GTGT, true);
                        Declaration_02_GTGT declaration_02_GTGT_ = new Declaration_02_GTGT(tM02GTGT, true);
                        this.Close();
                        this.Dispose();
                        declaration_02_GTGT_.Show();
                      
                    }
                    return;
                }
                else
                {
                }
            }
            else
            {
                if (txtNumber.Value.ToString() == "0")
                {
                    MSG.Error("Chưa nhập lần bổ sung");
                    return;
                }
                item03 = txtNumber.Value.ToString();
                DateBS = dteKHBS.DateTime;
            }
            Declaration_02_GTGT declaration_02_GTGT = new Declaration_02_GTGT(new TM02GTGT(), false);
            declaration_02_GTGT.item02 = item02;
            declaration_02_GTGT.item03 = item03;
            declaration_02_GTGT.DateBS = DateBS;
            declaration_02_GTGT.FromDate = dteDateFrom.DateTime;
            declaration_02_GTGT.ToDate = dteDateTo.DateTime;
            if (ultraOptionSet1.Value.ToString() == "0")
            {
                declaration_02_GTGT.item01 = "Tháng " + dteDateFrom.DateTime.Month.ToString().PadLeft(2, '0') + " năm " + txtYear.Value;
            }
            else
            {
                declaration_02_GTGT.item01 = "Quý " + cbbMonth.Text.Split(' ')[1] + " năm " + txtYear.Value;
            }

            if (ultraOptionSet1.Value.ToString() == "0")
            {
              if(dteDateFrom.DateTime.Year> DateTime.Now.Year)
                {
                    MSG.Error("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                    return;
                }
              else if(dteDateFrom.DateTime.Year == DateTime.Now.Year)
                if (dteDateFrom.DateTime.Month > DateTime.Now.Month)
                {
                    MSG.Error("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                    return;
                }
            }
            else
            {
                int year = DateTime.Now.Year;
                int now = DateTime.Now.Month / 3;
                decimal nowd = decimal.Parse(DateTime.Now.Month.ToString()) / 3;
                if (nowd - now > 0)
                {
                    now += 1;
                }
                if (year == dteDateTo.DateTime.Year)
                {
                    if (dteDateTo.DateTime.Month / 3 > now)
                    {
                        MSG.Error("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                        return;
                    }
                }
                else if(dteDateTo.DateTime.Year>year)
                {
                    MSG.Error("Kỳ kê khai không được lớn hơn kỳ hiện tại");
                    return;
                }
            }

            this.Hide();
            this.Close();
            if (declaration_02_GTGT.LoadInitialize(false))
            {
                declaration_02_GTGT.Show();
                
            }
            //if (declaration_02_GTGT.checkSave)
            //{
            //    this.Close();
            //}
        }

        private void TaxPeriod_02_GTGT_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (Form)sender;
            frm.Dispose();

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}
