﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Accounting
{
    public partial class Declaration_02_GTGT : Form
    {
        public string item01 = "";
        public DateTime DateBS = DateTime.Now;
        public bool checkSave = false;
        public DateTime FromDate = DateTime.Now;
        public DateTime ToDate = DateTime.Now;
        public bool item02 = false;
        //public ISystemOptionService _ISystemOptionService { get { return IoC.Resolve<ISystemOptionService>(); } }
        public string item03 = "";
        private TM02GTGT _select;
        private bool _isEdit;
        bool editKeypress = false;
        private TM02GTGT _tM02GTGTBS;
        public Declaration_02_GTGT(TM02GTGT tM02GTGT, bool isEdit)
        {
            InitializeComponent();
            WindowState = FormWindowState.Maximized;
            List<TaxPriod02GTGT> lstTaxP = new List<TaxPriod02GTGT>();
            //LoadUgrid();
            //txtItem22.FormatNumberic(ConstDatabase.Format_TienVND);
            _select = tM02GTGT;
            _isEdit = isEdit;
            if (isEdit)
            {
                LoadReadOnly(true);
                utmDetailBaseToolBar.Tools["mnbtnSave"].SharedProps.Enabled = false;
                ObjandGUI(tM02GTGT, false);
                LoadMaskInp();
            }
            else
            {
                LoadReadOnly(false);
                utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["mnbtnDelete"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = false;
                utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Visible = false;
                LoadMaskInp();
            }
            Utils.ClearCacheByType<SystemOption>();

            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
                this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
            }
            //utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Visible = false;

        }
        public bool LoadInitialize(bool isGet)
        {
            if (isGet)
            {

            }
            else
            {
                txtItem1.Text = item01;
                txtItem2.Checked = item02;
                if (!item03.StrIsNullOrEmpty())
                    txtItem3.Value = item03;
                txtItem4.Text = Utils.GetCompanyName();
                txtItem5.Text = Utils.GetCompanyTaxCode();
                txtItem12.Text = Utils.GetTenDLT();
                txtItem13.Text = Utils.GetMSTDLT();
                txtName.Value = Utils.GetHVTNhanVienDLT();
                txtCCHN.Value = Utils.GetCCHNDLT();
                txtNameSign.Value = Utils.GetCompanyDirector();
                txtSignDate.Value = DateTime.Now;
                if (!item02)
                {
                    txtItem1BS.Text = item01 + " ngày " + DateBS.Day + " tháng " + DateBS.Month + " năm " + DateBS.Year;
                    List<TaxPriod02GTGT> lstTaxP = new List<TaxPriod02GTGT>();
                    lstTaxP.Add(new TaxPriod02GTGT());
                    LoadUgrid(uGrid1, lstTaxP);
                    LoadUgrid(uGrid2, lstTaxP);
                    LoadUgrid(uGrid3, lstTaxP);
                    TM02GTGT tM02GTGT = new TM02GTGT();
                    if (Utils.ListTM02GTGT.Where(n => n.FromDate == FromDate && n.ToDate == ToDate).Count() > 0)
                    {
                        //ObjandGUI(Utils.ListTM02GTGT.Where(n => n.FromDate == FromDate).FirstOrDefault(n), false);
                        TM02GTGT tM02GTGTBS = Utils.ListTM02GTGT.Where(n => n.FromDate == FromDate).FirstOrDefault();
                        _tM02GTGTBS = tM02GTGTBS;
                        tM02GTGTBS.ID = Guid.Empty;
                        tM02GTGTBS.IsFirstDeclaration = false;
                        ObjandGUI(tM02GTGTBS, false);
                    }
                    else
                    {
                        MSG.Error("Không tồn tại tờ khai cùng kỳ");
                        return false;
                    }
                    txtItem2.Checked = false;

                }
                else
                {
                    ultraTabControl1.Tabs[1].Visible = false;
                    utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Visible = false;
                }
            }
            return true;
        }

        #region check lỗi

        #endregion
        public void LoadReadOnly(bool isReadOnly)
        {
            utmDetailBaseToolBar.Tools["mnbtnDelete"].SharedProps.Enabled = isReadOnly;
            txtItem21.ReadOnly = isReadOnly;
            txtItem21a.ReadOnly = isReadOnly;
            txtItem22.ReadOnly = isReadOnly;
            txtItem23.ReadOnly = isReadOnly;
            txtItem24.ReadOnly = isReadOnly;
            txtItem25.ReadOnly = isReadOnly;
            txtItem26.ReadOnly = isReadOnly;
            txtItem27.ReadOnly = isReadOnly;
            //txtItem28.ReadOnly = isReadOnly;
            txtItem28a.ReadOnly = isReadOnly;
            //txtItem29.ReadOnly = isReadOnly;
            txtItem30a.ReadOnly = isReadOnly;
            txtItem30.ReadOnly = isReadOnly;
            txtItem31.ReadOnly = isReadOnly;
            //txtItem32.ReadOnly = isReadOnly;
            txtName.ReadOnly = isReadOnly;
            txtCCHN.ReadOnly = isReadOnly;
            txtNameSign.ReadOnly = isReadOnly;
            txtSignDate.ReadOnly = isReadOnly;
        }
        public TM02GTGT ObjandGUI(TM02GTGT tM02GTGT, bool isGet)
        {
            if (isGet)
            {
                if (tM02GTGT.ID == null || tM02GTGT.ID == Guid.Empty)
                {
                    tM02GTGT.ID = Guid.NewGuid();
                }
                tM02GTGT.TypeID = 921;
                tM02GTGT.DeclarationName = txtDeclarationName.Text;
                tM02GTGT.DeclarationTerm = txtItem1.Text;

                tM02GTGT.IsFirstDeclaration = txtItem2.Checked;
                try
                {
                    tM02GTGT.AdditionTime = int.Parse(txtItem3.Text);
                }
                catch { }
                //tM02GTGT.AdditionDate 
                tM02GTGT.CompanyName = txtItem4.Text;
                tM02GTGT.CompanyTaxCode = txtItem5.Text;
                tM02GTGT.TaxAgencyTaxCode = txtItem13.Text;
                tM02GTGT.TaxAgencyName = txtItem12.Text;
                tM02GTGT.TaxAgencyEmployeeName = txtName.Text;
                tM02GTGT.CertificationNo = txtCCHN.Text;
                tM02GTGT.SignName = txtNameSign.Text;
                if (txtSignDate.Value != null)
                    tM02GTGT.SignDate = txtSignDate.DateTime;
                else
                    tM02GTGT.SignDate = null;

                #region  Details
                int i = 0;
                if (_isEdit)
                {
                    tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item21").Data = getValueMaskedEdit(txtItem21).ToString();
                    tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item21a").Data = getValueMaskedEdit(txtItem21a).ToString();
                    tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item22").Data = getValueMaskedEdit(txtItem22).ToString();
                    tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item23").Data = getValueMaskedEdit(txtItem23).ToString();
                    tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item24").Data = getValueMaskedEdit(txtItem24).ToString();
                    tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item25").Data = getValueMaskedEdit(txtItem25).ToString();
                    tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item26").Data = getValueMaskedEdit(txtItem26).ToString();
                    tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item27").Data = getValueMaskedEdit(txtItem27).ToString();
                    tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item28").Data = getValueMaskedEdit(txtItem28).ToString();
                    tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item28a").Data = getValueMaskedEdit(txtItem28a).ToString();
                    tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item29").Data = getValueMaskedEdit(txtItem29).ToString();
                    tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item30").Data = getValueMaskedEdit(txtItem30).ToString();
                    tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item30a").Data = getValueMaskedEdit(txtItem30a).ToString();
                    tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item31").Data = getValueMaskedEdit(txtItem31).ToString();
                    tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item32").Data = getValueMaskedEdit(txtItem32).ToString();

                }
                else
                {
                    tM02GTGT.FromDate = FromDate;
                    tM02GTGT.ToDate = ToDate;
                    tM02GTGT.TM02GTGTDetails.Add(new TM02GTGTDetail
                    {
                        TM02GTGTID = tM02GTGT.ID,
                        Code = "Item21",
                        Name = Name21.Text,
                        Data = getValueMaskedEdit(txtItem21).ToString(),
                        OrderPriority = i++
                    });
                    tM02GTGT.TM02GTGTDetails.Add(new TM02GTGTDetail
                    {
                        TM02GTGTID = tM02GTGT.ID,
                        Code = "Item21a",
                        Name = Name21a.Text,
                        Data = getValueMaskedEdit(txtItem21a).ToString(),
                        OrderPriority = i++
                    });
                    tM02GTGT.TM02GTGTDetails.Add(new TM02GTGTDetail
                    {
                        TM02GTGTID = tM02GTGT.ID,
                        Code = "Item22",
                        Name = Name22.Text,
                        Data = getValueMaskedEdit(txtItem22).ToString(),
                        OrderPriority = i++
                    });
                    tM02GTGT.TM02GTGTDetails.Add(new TM02GTGTDetail
                    {
                        TM02GTGTID = tM02GTGT.ID,
                        Code = "Item23",
                        Name = Name22.Text,
                        Data = getValueMaskedEdit(txtItem23).ToString(),
                        OrderPriority = i++
                    });
                    tM02GTGT.TM02GTGTDetails.Add(new TM02GTGTDetail
                    {
                        TM02GTGTID = tM02GTGT.ID,
                        Code = "Item24",
                        Name = Name24.Text,
                        Data = getValueMaskedEdit(txtItem24).ToString(),
                        OrderPriority = i++
                    });
                    tM02GTGT.TM02GTGTDetails.Add(new TM02GTGTDetail
                    {
                        TM02GTGTID = tM02GTGT.ID,
                        Code = "Item25",
                        Name = Name24.Text,
                        Data = getValueMaskedEdit(txtItem25).ToString(),
                        OrderPriority = i++
                    });
                    tM02GTGT.TM02GTGTDetails.Add(new TM02GTGTDetail
                    {
                        TM02GTGTID = tM02GTGT.ID,
                        Code = "Item26",
                        Name = Name26.Text,
                        Data = getValueMaskedEdit(txtItem26).ToString(),
                        OrderPriority = i++
                    });
                    tM02GTGT.TM02GTGTDetails.Add(new TM02GTGTDetail
                    {
                        TM02GTGTID = tM02GTGT.ID,
                        Code = "Item27",
                        Name = Name26.Text,
                        Data = getValueMaskedEdit(txtItem27).ToString(),
                        OrderPriority = i++
                    });
                    tM02GTGT.TM02GTGTDetails.Add(new TM02GTGTDetail
                    {
                        TM02GTGTID = tM02GTGT.ID,
                        Code = "Item28",
                        Name = Name28.Text,
                        Data = getValueMaskedEdit(txtItem28).ToString(),
                        OrderPriority = i++
                    });
                    tM02GTGT.TM02GTGTDetails.Add(new TM02GTGTDetail
                    {
                        TM02GTGTID = tM02GTGT.ID,
                        Code = "Item28a",
                        Name = Name28a.Text,
                        Data = getValueMaskedEdit(txtItem28a).ToString(),
                        OrderPriority = i++
                    });
                    tM02GTGT.TM02GTGTDetails.Add(new TM02GTGTDetail
                    {
                        TM02GTGTID = tM02GTGT.ID,
                        Code = "Item29",
                        Name = Name29.Text,
                        Data = getValueMaskedEdit(txtItem29).ToString(),
                        OrderPriority = i++
                    });
                    tM02GTGT.TM02GTGTDetails.Add(new TM02GTGTDetail
                    {
                        TM02GTGTID = tM02GTGT.ID,
                        Code = "Item30a",
                        Name = Name30a.Text,
                        Data = getValueMaskedEdit(txtItem30a).ToString(),
                        OrderPriority = i++
                    });
                    tM02GTGT.TM02GTGTDetails.Add(new TM02GTGTDetail
                    {
                        TM02GTGTID = tM02GTGT.ID,
                        Code = "Item30",
                        Name = Name30.Text,
                        Data = getValueMaskedEdit(txtItem30).ToString(),
                        OrderPriority = i++
                    });
                    tM02GTGT.TM02GTGTDetails.Add(new TM02GTGTDetail
                    {
                        TM02GTGTID = tM02GTGT.ID,
                        Code = "Item31",
                        Name = Name31.Text,
                        Data = getValueMaskedEdit(txtItem31).ToString(),
                        OrderPriority = i++
                    });
                    tM02GTGT.TM02GTGTDetails.Add(new TM02GTGTDetail
                    {
                        TM02GTGTID = tM02GTGT.ID,
                        Code = "Item32",
                        Name = Name32.Text,
                        Data = getValueMaskedEdit(txtItem32).ToString(),
                        OrderPriority = i++
                    });
                }

                #endregion
            }
            else
            {
                txtItem1.Text = tM02GTGT.DeclarationTerm;
                txtItem2.Checked = tM02GTGT.IsFirstDeclaration;
                if (tM02GTGT.AdditionTime != null)
                {
                    txtItem3.Value = tM02GTGT.AdditionTime;
                }
                if (tM02GTGT.IsFirstDeclaration)
                {
                    ultraTabControl1.Tabs[1].Visible = false;
                    utmDetailBaseToolBar.Tools["btnKBS"].SharedProps.Visible = false;
                }
                txtItem4.Text = tM02GTGT.CompanyName;
                txtItem5.Text = tM02GTGT.CompanyTaxCode;
                txtItem12.Text = tM02GTGT.TaxAgencyName;
                txtItem13.Text = tM02GTGT.TaxAgencyTaxCode;
                txtName.Value = tM02GTGT.TaxAgencyEmployeeName;
                txtCCHN.Value = tM02GTGT.CertificationNo;
                txtNameSign.Value = tM02GTGT.SignName;
                txtSignDate.Value = tM02GTGT.SignDate;
                #region Details
                BindData(txtItem21, decimal.Parse(tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item21").Data));
                BindData(txtItem21a, decimal.Parse(tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item21a").Data));
                BindData(txtItem22, decimal.Parse(tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item22").Data));
                BindData(txtItem23, decimal.Parse(tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item23").Data));
                BindData(txtItem24, decimal.Parse(tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item24").Data));
                BindData(txtItem25, decimal.Parse(tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item25").Data));
                BindData(txtItem26, decimal.Parse(tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item26").Data));
                BindData(txtItem27, decimal.Parse(tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item27").Data));
                BindData(txtItem28, decimal.Parse(tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item28").Data));
                BindData(txtItem28a, decimal.Parse(tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item28a").Data));
                BindData(txtItem29, decimal.Parse(tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item29").Data));
                BindData(txtItem30a, decimal.Parse(tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item30a").Data));
                BindData(txtItem30, decimal.Parse(tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item30").Data));
                BindData(txtItem31, decimal.Parse(tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item31").Data));
                BindData(txtItem32, decimal.Parse(tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item32").Data));
                #endregion
            }
            return tM02GTGT;
        }

        public void LoadUgrid(UltraGrid ultraGrid, List<TaxPriod02GTGT> lstTaxP)
        {
            ultraGrid.DataSource = new BindingList<TaxPriod02GTGT>(lstTaxP);
            ultraGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            ultraGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGrid.DisplayLayout.Bands[0].Columns["Type"].Hidden = true;
            ultraGrid.DisplayLayout.Bands[0].ColHeadersVisible = false;
            ultraGrid.DisplayLayout.Bands[0].Columns["index"].Width = ultraLabel77.Width - 1 - 1;
            ultraGrid.DisplayLayout.Bands[0].Columns["AdjustmentCriteria"].Width = ultraLabel76.Width - 1 - 1;
            ultraGrid.DisplayLayout.Bands[0].Columns["AdjustmentCriteriaCodeNumber"].Width = ultraLabel78.Width - 2;
            ultraGrid.DisplayLayout.Bands[0].Columns["NumberDeclared"].Width = ultraLabel79.Width - 2;
            ultraGrid.DisplayLayout.Bands[0].Columns["NumberAdjustment"].Width = ultraLabel75.Width - 2;
            ultraGrid.DisplayLayout.Bands[0].Columns["Difference"].Width = ultraLabel74.Width - 2;
            ultraGrid.DisplayLayout.Override.MinRowHeight = ultraGrid.Height;

            //Utils.ConfigGrid(uGrid, ConstDatabase.TaxPriod02GTGT_TableName,false);
            //this.uGrid1.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            //this.uGrid1.DisplayLayout.ViewStyleBand = ViewStyleBand.OutlookGroupBy;
            //this.uGrid1.DisplayLayout.Bands[0].SortedColumns.Add("Type", false, true);
            //this.uGrid1.DisplayLayout.GroupByBox.Hidden = true;
            //this.uGrid1.DisplayLayout.EmptyRowSettings.ShowEmptyRows = false;
            //this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            //uGrid.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.False;
            //this.uGrid1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;

        }

        private void utmDetailBaseToolBar_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
        {
            switch (e.Tool.Key)
            {
                case "mnbtnBack":
                    break;

                case "mnbtnForward":
                    break;

                case "mnbtnAdd":

                    break;

                case "mnbtnEdit":
                    editKeypress = true;
                    utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Visible = false;
                    utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = false;
                    utmDetailBaseToolBar.Tools["mnbtnSave"].SharedProps.Enabled = true;
                    utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = false;
                    LoadReadOnly(false);
                    break;

                case "mnbtnSave":
                    try
                    {

                        if (txtSignDate.Value == null)
                        {
                            MSG.Error("Bạn chưa chọn ngày ký");
                            return;
                        }
                        editKeypress = false;
                        Utils.ITM02GTGTService.BeginTran();

                        if (_isEdit)
                        {
                            TM02GTGT tM02GTGT = new TM02GTGT();
                            tM02GTGT = ObjandGUI(_select, true);
                            Utils.ITM02GTGTService.Update(tM02GTGT);
                        }
                        else
                        {
                            TM02GTGT tM02GTGT = new TM02GTGT();
                            tM02GTGT = ObjandGUI(tM02GTGT, true);
                            Utils.ITM02GTGTService.CreateNew(tM02GTGT);
                            _select = tM02GTGT;
                            _isEdit = true;
                        }
                        Utils.ITM02GTGTService.CommitTran();
                        MSG.Information("Lưu thành công");
                        Utils.ClearCacheByType<TM02GTGT>();
                        Utils.ClearCacheByType<TM02GTGTDetail>();
                        _select = Utils.ITM02GTGTService.Getbykey(_select.ID);
                        utmDetailBaseToolBar.Tools["btnExportXml"].SharedProps.Visible = true;
                        utmDetailBaseToolBar.Tools["mnbtnPrint"].SharedProps.Enabled = true;
                        utmDetailBaseToolBar.Tools["mnbtnSave"].SharedProps.Enabled = false;
                        utmDetailBaseToolBar.Tools["mnbtnEdit"].SharedProps.Enabled = true;
                        LoadReadOnly(true);
                        checkSave = true;
                    }
                    catch (Exception ex)
                    {
                        MSG.Error(ex.Message);
                        Utils.ITM02GTGTService.RolbackTran();
                    }

                    break;

                case "mnbtnDelete":
                    if (!(DialogResult.Yes == MessageBox.Show("Bạn có muốn xóa tờ khai này không ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question)))
                        return;
                    TM02GTGT tM02GTGT1 = Utils.ListTM02GTGT.FirstOrDefault(n => n.ID == _select.ID);
                    Utils.ITM02GTGTService.BeginTran();
                    Utils.ITM02GTGTService.Delete(tM02GTGT1);
                    Utils.ITM02GTGTService.CommitTran();
                    MSG.Information("Xóa thành công");

                    this.Close();
                    break;

                case "mnbtnComeBack":
                    break;

                case "mnbtnPrint":
                    break;
                case "btnKBS":
                    CheckChangeBS();
                    break;
                case "mnbtnClose":

                    this.Close();

                    break;
                case "PrintGTGTTM02":
                    #region fill du lieu
                    GTGT02Report gTGT02Report = new GTGT02Report();
                    gTGT02Report.CompanyName = _select.CompanyName;
                    gTGT02Report.DeclarationTerm = _select.DeclarationTerm;
                    gTGT02Report.IsFirstDeclaration = _select.IsFirstDeclaration;
                    gTGT02Report.AdditionTime = _select.AdditionTime;
                    gTGT02Report.CompanyTaxCode = _select.CompanyTaxCode;
                    gTGT02Report.TaxAgencyEmployeeName = _select.TaxAgencyEmployeeName;
                    gTGT02Report.CertificationNo = _select.CertificationNo;
                    if (_select.SignDate != null)
                        gTGT02Report.SignDate = _select.SignDate;
                    else
                        gTGT02Report.SignDate = null;
                    gTGT02Report.SignName = _select.SignName;

                    gTGT02Report.Item30a = _select.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item30a").Data;
                    gTGT02Report.Item24 = _select.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item24").Data;
                    gTGT02Report.Item21a = _select.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item21a").Data;
                    gTGT02Report.Item23 = _select.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item23").Data;
                    gTGT02Report.Item26 = _select.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item26").Data;
                    gTGT02Report.Item27 = _select.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item27").Data;
                    gTGT02Report.Item30 = _select.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item30").Data;
                    gTGT02Report.Item22 = _select.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item22").Data;
                    gTGT02Report.Item32 = _select.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item32").Data;
                    gTGT02Report.Item31 = _select.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item31").Data;
                    gTGT02Report.Item21 = _select.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item21").Data;
                    gTGT02Report.Item28 = _select.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item28").Data;
                    gTGT02Report.Item28a = _select.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item28a").Data;
                    gTGT02Report.Item29 = _select.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item29").Data;
                    gTGT02Report.Item25 = _select.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item25").Data;

                    #endregion

                    Utils.Print("GTGT-02", gTGT02Report, this);
                    break;
                case "btnExportXml":
                    ExportXml(_select);
                    break;
            }
        }
        public bool CheckChangeBS()
        {

            foreach (var item in _tM02GTGTBS.TM02GTGTDetails)
            {
                switch (item.Code)
                {
                    case "Item21":
                        if (txtItem21.Value != item.Data)
                        {

                        }
                        break;
                    case "Item21a":
                        break;
                    case "Item22":
                        break;
                    case "Item23":
                        break;
                    case "Item24":
                        break;
                    case "Item25":
                        break;
                    case "Item26":
                        break;
                    case "Item27":
                        break;
                    case "Item28":
                        break;
                    case "Item28a":
                        break;
                    case "Item29":
                        break;
                    case "Item30a":
                        break;
                    case "Item30":
                        break;
                    case "Item31":
                        break;
                    case "Item32":
                        break;
                }
            }
            return true;
        }
        private void txtItem21_Validated(object sender, EventArgs e)
        {
            if (txtItem21.Value.IsNullOrEmpty())
            {
                txtItem21.Value = 0;
            }
            if (txtItem21a.Value.IsNullOrEmpty())
            {
                txtItem21a.Value = 0;
            }
            if (txtItem28.Value.IsNullOrEmpty())
            {
                txtItem28.Value = 0;
            }
            if (txtItem28a.Value.IsNullOrEmpty())
            {
                txtItem28a.Value = 0;
            }
            BindData(txtItem29, getValueMaskedEdit(txtItem21) + getValueMaskedEdit(txtItem21a) + getValueMaskedEdit(txtItem28) - getValueMaskedEdit(txtItem28a));
        }

        private void txtItem23_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            BindData(txtItem28, getValueMaskedEdit(txtItem23) + getValueMaskedEdit(txtItem25) - getValueMaskedEdit(txtItem27));
        }

        private void txtItem29_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
            }
            BindData(txtItem32, getValueMaskedEdit(txtItem29) - getValueMaskedEdit(txtItem30a) - getValueMaskedEdit(txtItem30) - getValueMaskedEdit(txtItem31));
        }

        private void txtItem24_ValueChanged(object sender, EventArgs e)
        {

        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            uGrid1.Size = new Size(uGrid1.Size.Width, uGrid1.Size.Height + uGrid1.DisplayLayout.Override.MinRowHeight);
            ultraPanel1.Height = uGrid1.Height + 2;
            //uGrid1.AddNewRow4Grid();
            uGrid1.DisplayLayout.Bands[0].AddNew();

            uGrid1.Rows[uGrid1.Rows.Count - 1].Update();
            //uGrid1.Dock = DockStyle.Fill;
            uGrid1.Refresh();
            uGrid1.ActiveRow = uGrid1.Rows[0];
            uGrid1.ActiveRow = uGrid1.Rows[uGrid1.Rows.Count - 1];
            ultraLabel87.Location = new Point(ultraLabel87.Location.X, ultraLabel87.Location.Y + 29);
            ultraPanel2.Location = new Point(ultraPanel2.Location.X, ultraPanel2.Location.Y + 29);
            ultraPanel20.Location = new Point(ultraPanel20.Location.X, ultraPanel20.Location.Y + 29);
            ultraLabel88.Location = new Point(ultraLabel88.Location.X, ultraLabel88.Location.Y + 29);
            ultraPanel21.Location = new Point(ultraPanel21.Location.X, ultraPanel21.Location.Y + 29);
        }
        public bool LoadMaskInp()
        {
            foreach (var item in GetAll(this, typeof(UltraMaskedEdit)))
            {
                UltraMaskedEdit ultraMaskedEdit = item as UltraMaskedEdit;
                if (item is UltraMaskedEdit)
                {
                    if ((item as UltraMaskedEdit).Appearance.FontData.Bold == DefaultableBoolean.True)
                        (item as UltraMaskedEdit).FormatString = "###,###,###,###,###,##0";
                    decimal r = getValueMaskedEdit(item as UltraMaskedEdit);
                    if (r < 0)
                    {
                        (item as UltraMaskedEdit).InputMask = "(n,nnn,nnn,nnn,nnn,nnn)";
                        if ((item as UltraMaskedEdit).Appearance.FontData.Bold == DefaultableBoolean.True)
                            (item as UltraMaskedEdit).FormatString = "(###,###,###,###,###,##0)";
                        (item as UltraMaskedEdit).Value = -r;

                    }
                    if (ultraMaskedEdit.InputMask == "-n,nnn,nnn,nnn,nnn,nnn")
                    {
                        (item as UltraMaskedEdit).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
                        if ((item as UltraMaskedEdit).Appearance.FontData.Bold == DefaultableBoolean.True)
                            (item as UltraMaskedEdit).FormatString = "###,###,###,###,###,##0";
                    }

                }
            }
            return true;
        }
        public IEnumerable<Control> GetAll(Control control, System.Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams handleParam = base.CreateParams;
                handleParam.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED       
                return handleParam;
            }
        }
        private void ultraMaskedEdit3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(sender is UltraMaskedEdit)) return;
            if (editKeypress || _select.ID == null || _select.ID == Guid.Empty)
            {
                UltraMaskedEdit ultraMaskedEdit = (UltraMaskedEdit)sender;
                if (getValueMaskedEdit(ultraMaskedEdit) == 0) return;
                if (e.KeyChar == '-')
                {
                    if (ultraMaskedEdit.InputMask == "-n,nnn,nnn,nnn,nnn,nnn" || ultraMaskedEdit.InputMask == "n,nnn,nnn,nnn,nnn,nnn")
                    {
                        ultraMaskedEdit.InputMask = "(n,nnn,nnn,nnn,nnn,nnn)";
                        if (ultraMaskedEdit.Appearance.FontData.Bold == DefaultableBoolean.True)
                            ultraMaskedEdit.FormatString = "(###,###,###,###,###,##0)";
                    }
                    else
                    {
                        ultraMaskedEdit.InputMask = "n,nnn,nnn,nnn,nnn,nnn";
                        if (ultraMaskedEdit.Appearance.FontData.Bold == DefaultableBoolean.True)
                            ultraMaskedEdit.FormatString = "###,###,###,###,###,##0";
                    }
                }
            }
        }
        public bool BindData(UltraMaskedEdit ultraMaskedEdit, decimal r)
        {
            try
            {
                if (r < 0)
                {
                    ultraMaskedEdit.InputMask = "(n,nnn,nnn,nnn,nnn,nnn)";
                    if (ultraMaskedEdit.Appearance.FontData.Bold == DefaultableBoolean.True)
                        ultraMaskedEdit.FormatString = "(###,###,###,###,###,##0)";
                    ultraMaskedEdit.Value = -Math.Round(r, MidpointRounding.AwayFromZero);
                }
                else
                {
                    ultraMaskedEdit.InputMask = "n,nnn,nnn,nnn,nnn,nnn";
                    if (ultraMaskedEdit.Appearance.FontData.Bold == DefaultableBoolean.True)
                        ultraMaskedEdit.FormatString = "###,###,###,###,###,##0";
                    ultraMaskedEdit.Value = Math.Round(r, MidpointRounding.AwayFromZero);
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        private decimal getValueMaskedEdit(UltraMaskedEdit ultraMaskedEdit)
        {
            if (!ultraMaskedEdit.Value.IsNullOrEmpty())
            {
                if (ultraMaskedEdit.Text.Contains("("))
                {
                    if (decimal.Parse(ultraMaskedEdit.Value.ToString()) == 0)
                    {
                        return 0;
                    }
                    else
                        return -decimal.Parse(ultraMaskedEdit.Value.ToString());
                }
                else
                {
                    return decimal.Parse(ultraMaskedEdit.Value.ToString());
                }
            }
            return 0;
        }
        private void tsmDelete_Click(object sender, EventArgs e)
        {

        }

        private void txtItem22_ValueChanged(object sender, EventArgs e)
        {
            if (getValueMaskedEdit((UltraMaskedEdit)sender) == 0)
            {
                ((UltraMaskedEdit)sender).InputMask = "n,nnn,nnn,nnn,nnn,nnn";
                if (((UltraMaskedEdit)sender).Appearance.FontData.Bold == DefaultableBoolean.True)
                    ((UltraMaskedEdit)sender).FormatString = "###,###,###,###,###,##0";
            }
        }

        private void txtItem21_Validated_1(object sender, EventArgs e)
        {
            if (!(sender as UltraMaskedEdit).Value.IsNullOrEmpty())
                (sender as UltraMaskedEdit).Value = decimal.Parse((sender as UltraMaskedEdit).Value.ToString());
        }
        private bool ExportXml(TM02GTGT tM02GTGT)
        {
            SaveFileDialog sf = new SaveFileDialog
            {
                FileName = "02GTGT.xml",
                AddExtension = true,
                Filter = "Excel Document(*.xml)|*.xml"
            };
            if (sf.ShowDialog() != DialogResult.OK)
            {
                return true;
            }
            string path = System.IO.Path.GetDirectoryName(
                        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            string filePath = string.Format("{0}\\TemplateResource\\xml\\02_GTGT_xml.xml", path);
            XmlDocument doc = new XmlDocument();
            doc.Load(filePath);
            string tt = "HSoThueDTu/HSoKhaiThue/TTinChung/";
            doc.DocumentElement.SetAttribute("xmlns", "http://kekhaithue.gdt.gov.vn/TKhaiThue");
            doc.DocumentElement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            //doc.SelectSingleNode(tt + "TTinDVu/maDVu").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinDVu/tenDVu").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinDVu/pbanDVu").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinDVu/ttinNhaCCapDVu").InnerText = "";

            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/maTKhai").InnerText = "02";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/tenTKhai").InnerText = tM02GTGT.DeclarationName;
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/moTaBMau").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/pbanTKhaiXML").InnerText = "";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/loaiTKhai").InnerText = "C";
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/soLan").InnerText = "0";

            if (tM02GTGT.DeclarationTerm.Contains("Quý"))
            {
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kieuKy").InnerText = "Q";
                if (tM02GTGT.DeclarationTerm.Split(' ')[1].Equals("I"))
                {
                    doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = "1/" + tM02GTGT.DeclarationTerm.Split(' ')[3];
                }
                else if (tM02GTGT.DeclarationTerm.Split(' ')[1].Equals("II"))
                {
                    doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = "2/" + tM02GTGT.DeclarationTerm.Split(' ')[3];
                }
                else if (tM02GTGT.DeclarationTerm.Split(' ')[1].Equals("III"))
                {
                    doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = "3/" + tM02GTGT.DeclarationTerm.Split(' ')[3];
                }
                else
                {
                    doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = "4/" + tM02GTGT.DeclarationTerm.Split(' ')[3];
                }
            }
            else
            {
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kieuKy").InnerText = "M";
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhai").InnerText = tM02GTGT.DeclarationTerm.Split(' ')[1] + "/" + tM02GTGT.DeclarationTerm.Split(' ')[3];
            }
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhaiTuNgay").InnerText = tM02GTGT.FromDate.ToString("dd/MM/yyyy");
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhaiDenNgay").InnerText = tM02GTGT.ToDate.ToString("dd/MM/yyyy");

            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/maCQTNoiNop").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/tenCQTNoiNop").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/ngayLapTKhai").InnerText = "";

            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/GiaHan/maLyDoGiaHan").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/GiaHan/lyDoGiaHan").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/GiaHan/lyDoGiaHan").InnerText = "";

            if (!tM02GTGT.SignName.IsNullOrEmpty())
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/nguoiKy").InnerText = tM02GTGT.SignName;
            if (tM02GTGT.SignDate != null)
            {
                DateTime dt = (DateTime)tM02GTGT.SignDate;
                doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/ngayKy").InnerText = dt.ToString("yyyy-MM-dd");
            }
            doc.SelectSingleNode(tt + "TTinTKhaiThue/TKhaiThue/nganhNgheKD").InnerText = tM02GTGT.CertificationNo;

            if (!Utils.GetDLTNgay().IsNullOrEmpty())
            {
                DateTime ngdlt = DateTime.ParseExact(Utils.GetDLTNgay(), "dd/MM/yyyy", null);
                doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/ngayKyHDDLyThue").InnerText = ngdlt.ToString("yyyy-MM-dd");
            }
            doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/mst").InnerText = Utils.GetCompanyTaxCodeForInvoice();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/tenNNT").InnerText = tM01GTGT.CompanyName;
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/dchiNNT").InnerText = Utils.GetCompanyAddress();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/phuongXa").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/maHuyenNNT").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/tenHuyenNNT").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/maTinhNNT").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/tenTinhNNT").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/dthoaiNNT").InnerText = Utils.GetCompanyPhoneNumber();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/faxNNT").InnerText = tM01GTGT.CompanyTaxCode;
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/NNT/emailNNT").InnerText = Utils.GetCompanyEmail();

            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/mstDLyThue").InnerText = Utils.GetMSTDLT();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/tenDLyThue").InnerText = tM01GTGT.TaxAgencyName;
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/dchiDLyThue").InnerText = Utils.GetDLTAddress();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/maHuyenDLyThue").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/tenHuyenDLyThue").InnerText = Utils.GetDLTDistrict();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/maTinhDLyThue").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/tenTinhDLyThue").InnerText = Utils.GetDLTCity();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/dthoaiDLyThue").InnerText = Utils.GetDLTPhoneNumber();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/faxDLyThue").InnerText = Utils.GetDLTFax();
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/emailDLyThue").InnerText = "";
            //doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/soHDongDLyThue").InnerText = Utils.GetDLTSoHD();

            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/NVienDLy/tenNVienDLyThue").InnerText = Utils.GetHVTNhanVienDLT();
            doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue/NVienDLy/cchiHNghe").InnerText = Utils.GetCCHNDLT();

            if (!Utils.GetTTDLT())
            {
                doc.SelectSingleNode(tt + "TTinTKhaiThue").RemoveChild(doc.SelectSingleNode(tt + "TTinTKhaiThue/DLyThue"));
            }

            tt = "HSoThueDTu/HSoKhaiThue/CTieuTKhaiChinh/";

            //doc.SelectSingleNode(tt + "tieuMucHachToan").InnerText = "";
            doc.SelectSingleNode(tt + "ct21").InnerText = tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item21").Data;
            doc.SelectSingleNode(tt + "ct21a").InnerText = tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item21a").Data;
            doc.SelectSingleNode(tt + "HHDVMuaVaoTrongKy/ct22").InnerText = tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item22").Data;
            doc.SelectSingleNode(tt + "HHDVMuaVaoTrongKy/ct23").InnerText = tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item23").Data;
            doc.SelectSingleNode(tt + "DieuChinhTangGTGTCacKyTruoc/ct24").InnerText = tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item24").Data;
            doc.SelectSingleNode(tt + "DieuChinhTangGTGTCacKyTruoc/ct25").InnerText = tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item25").Data;
            doc.SelectSingleNode(tt + "DieuChinhGiamGTGTCacKyTruoc/ct26").InnerText = tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item26").Data;
            doc.SelectSingleNode(tt + "DieuChinhGiamGTGTCacKyTruoc/ct27").InnerText = tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item27").Data;
            doc.SelectSingleNode(tt + "ct28").InnerText = tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item28").Data;
            doc.SelectSingleNode(tt + "ct28a").InnerText = tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item28a").Data;
            doc.SelectSingleNode(tt + "ct29").InnerText = tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item29").Data;
            doc.SelectSingleNode(tt + "ct30a").InnerText = tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item30a").Data;
            doc.SelectSingleNode(tt + "ct30").InnerText = tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item30").Data;
            doc.SelectSingleNode(tt + "ct31").InnerText = tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item31").Data;
            doc.SelectSingleNode(tt + "ct32").InnerText = tM02GTGT.TM02GTGTDetails.FirstOrDefault(n => n.Code == "Item32").Data;

            //doc.SelectSingleNode("HSoThueDTu/HSoKhaiThue/PLuc").InnerText = "";

            doc.Save(sf.FileName);
            if (MSG.Question("Xuất xml thành công, Bạn có muốn mở file vừa tải") == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    System.Diagnostics.Process.Start(sf.FileName);
                }
                catch
                {
                    MSG.Error("Lỗi mở file");
                }

            }
            //var address = doc.Root.Element("address");
            //if (address != null)
            //{
            //    address.Value = "new value";
            //}

            return true;
        }

        private void Declaration_02_GTGT_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (Form)sender;
            frm.Dispose();

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();

        }

        private void Declaration_02_GTGT_FormClosing(object sender, FormClosingEventArgs e)
        {
            // this.Disposed();
            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            //GC.Collect();
        }
    }
}
