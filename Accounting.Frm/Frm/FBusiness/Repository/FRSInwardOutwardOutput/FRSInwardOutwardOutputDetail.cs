﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.Text.RegularExpressions;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using System.Globalization;
using System.Threading;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;
namespace Accounting
{
    public partial class FRSInwardOutwardOutputDetail : FrmTmp2
    {
        #region khởi tạo
        UltraGrid ugrid2 = null;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private IRefVoucherRSInwardOutwardService _refVoucherRSInwardOutwardService { get { return IoC.Resolve<IRefVoucherRSInwardOutwardService>(); } }
        public FRSInwardOutwardOutputDetail(RSInwardOutward temp, List<RSInwardOutward> dsRSInwardOutward, int statusForm)
        {//Add -> thêm, Sửa -> sửa, Xem -> Sửa ở trạng thái Enable = False


            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();
            #endregion

            #region Thiết lập ban đầu cho Form
            _statusForm = statusForm;
            if (statusForm == ConstFrm.optStatusForm.Add) _select.TypeID = 410;
            else _select = temp;
            _listSelects.AddRange(dsRSInwardOutward);
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, _listSelects, statusForm);
            btnOfNVL.Enabled = false;
            btnOfDDH.Enabled = false;
            #endregion
        }

        #endregion

        #region override

        public override void InitializeGUI(RSInwardOutward input)
        {
            #region Lấy các giá trị const từ Database

            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)

            Template mauGiaoDien = Utils.GetMauGiaoDien(input.TypeID, input.TemplateID, Utils.ListTemplate);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;

            #endregion
            _select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add ? "VND" : input.CurrencyID;
            _select.ExchangeRate = _statusForm == ConstFrm.optStatusForm.Add ? 1 : input.ExchangeRate;
            if (_statusForm != ConstFrm.optStatusForm.Add) _select.SaInvoiceCustormIds = ISAInvoiceService.Query.Where(sa => sa.OutwardRefID == _select.ID).Select(sa => sa.ID).ToList();
            #endregion

            #region Thiết lập dữ liệu và Fill dữ liệu Obj vào control (nếu đang sửa)
            if (_select.IsOutwardQuantum.HasValue) chkXuatkhotheoNVL.CheckState = _select.IsOutwardQuantum.Value ? CheckState.Checked : CheckState.Unchecked;
            if (_select.IsOutwardSAInvoice.HasValue) chkXuatkhotheoHoadon.CheckState = _select.IsOutwardSAInvoice.Value ? CheckState.Checked : CheckState.Unchecked;
            #endregion

            var type = Utils.ListType.FirstOrDefault(p => p.ID.Equals(_select.TypeID));
            if (type != null)
                grpTop.Text = type.TypeName.ToUpper();
            this.ConfigTopVouchersNo<RSInwardOutward>(pnlTopVouchers, "No", "PostedDate", "Date");
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            BindingList<RefVoucherRSInwardOutward> bdlRefVoucherRS = new BindingList<RefVoucherRSInwardOutward>();
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
            }
            
            if (input.TypeID.Equals(412) || input.TypeID.Equals(415))
            {
                BindingList<SAInvoiceDetail> commonBinding = new BindingList<SAInvoiceDetail>();
                _selectJoin = new SAInvoice();
                _selectJoin = ISAInvoiceService.Getbykey(input.ID);
                if (_selectJoin != null)
                {
                    if (_statusForm != ConstFrm.optStatusForm.Add)
                    {
                        commonBinding = new BindingList<SAInvoiceDetail>(((SAInvoice)_selectJoin).SAInvoiceDetails);
                        bdlRefVoucher = new BindingList<RefVoucher>(((SAInvoice)_selectJoin).RefVouchers);
                    }
                    
                    _listObjectInput = new BindingList<System.Collections.IList> { commonBinding };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                    this.ConfigGridByTemplete_General<RSInwardOutward>(pnlUgrid, mauGiaoDien);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, false };
                    this.ConfigGridByManyTemplete<RSInwardOutward>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    ugrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
                    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                }
                btnOriginalVoucher.Visible = false;
            }
            else if (input.TypeID.Equals(413))
            {
                BindingList<PPDiscountReturnDetail> commonBinding = new BindingList<PPDiscountReturnDetail>();
                _selectJoin = new PPDiscountReturn();
                _selectJoin = IPPDiscountReturnService.Getbykey(input.ID);
                //foreach (var x in ((PPDiscountReturn)_selectJoin).PPDiscountReturnDetails)
                //{
                //    x.Amount = x.Amount + x.VATAmount;
                //    x.AmountOriginal = x.AmountOriginal + x.VATAmountOriginal;
                //    x.UnitPrice = x.Quantity == 0 ? 0 : x.Amount / x.Quantity;
                //    x.UnitPriceOriginal = x.Quantity == 0 ? 0 : x.AmountOriginal / x.Quantity;
                //}
                if (_selectJoin != null)
                {
                    if (_statusForm != ConstFrm.optStatusForm.Add)
                    {
                        commonBinding = new BindingList<PPDiscountReturnDetail>(((PPDiscountReturn)_selectJoin).PPDiscountReturnDetails);
                        bdlRefVoucher = new BindingList<RefVoucher>(((PPDiscountReturn)_selectJoin).RefVouchers);
                    }
                    
                    _listObjectInput = new BindingList<System.Collections.IList> { commonBinding };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                    this.ConfigGridByTemplete_General<RSInwardOutward>(pnlUgrid, mauGiaoDien);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, false };
                    this.ConfigGridByManyTemplete<RSInwardOutward>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    ugrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
                    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                }
                btnOriginalVoucher.Visible = false;
            }
            else
            {
                BindingList<RSInwardOutwardDetail> commonBinding = new BindingList<RSInwardOutwardDetail>();
                
                if (_statusForm != ConstFrm.optStatusForm.Add)
                {
                    commonBinding = new BindingList<RSInwardOutwardDetail>(input.RSInwardOutwardDetails);
                    bdlRefVoucherRS = new BindingList<RefVoucherRSInwardOutward>(input.RefVoucherRSInwardOutwards);
                }

                if (input.TypeID.Equals(411))
                {
                    _listObjectInput = new BindingList<System.Collections.IList> { commonBinding };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucherRS };
                    this.ConfigGridByTemplete_General<RSInwardOutward>(pnlUgrid, mauGiaoDien);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true,false };
                    this.ConfigGridByManyTemplete<RSInwardOutward>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    ugrid2 = Controls.Find("uGrid1", true).FirstOrDefault() as UltraGrid;
                    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                    btnOriginalVoucher.Visible = true;//add by cuongpv
                }
                else
                {
                    _listObjectInput = new BindingList<System.Collections.IList> { commonBinding };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucherRS };
                    this.ConfigGridByTemplete_General<RSInwardOutward>(pnlUgrid, mauGiaoDien);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, false };
                    this.ConfigGridByManyTemplete<RSInwardOutward>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    ugrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
                    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                    if (input.TypeID.Equals(414))
                    {
                        var ultraTabControl = (UltraTabControl)pnlUgrid.Controls.Find("ultraTabControl", true).FirstOrDefault();
                        ultraTabControl.Tabs[2].Visible = false;
                        btnOriginalVoucher.Visible = false;
                    }
                    else
                    {
                        btnOriginalVoucher.Visible = true;//add by cuongpv
                    }
                }
            }
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            _select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmount;
            _select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add ? "VND" : input.CurrencyID;

            this.ConfigCombo(Utils.ListAccountingObject, cbbCustomerID, "AccountingObjectCode", "ID", input,
                             "AccountingObjectID");

            DataBinding(new List<Control> { txtAddress, txtAccountingObjectName, txtReason, txtContactName, txtOriginalNo }, "AccountingObjectAddress,AccountingObjectName,Reason,ContactName,OriginalNo");

            grpCheckBox.Visible = _select.TypeID.Equals(410);
            UltraGrid ugridTrung = Controls.Find("uGrid0", true).FirstOrDefault() as UltraGrid;
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 410)//trungnq thêm vào để  làm task 6234
                {
                    txtReason.Value = "Xuất kho";
                    txtReason.Text = "Xuất kho";
                    _select.Reason = "Xuất kho";
                    if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "AllowInputCostUnit").Data == "1")//trungnq thêm vào để  làm task 6240 mở/ khóa cột đơn giá, thành tiền của màn xuất kho
                    {
                        ugridTrung.DisplayLayout.Bands[0].Columns["UnitPriceOriginal"].CellActivation = Activation.AllowEdit;
                        ugridTrung.DisplayLayout.Bands[0].Columns["AmountOriginal"].CellActivation = Activation.AllowEdit;
                    }
                    else
                    {
                        ugridTrung.DisplayLayout.Bands[0].Columns["UnitPriceOriginal"].CellActivation = Activation.NoEdit;
                        ugridTrung.DisplayLayout.Bands[0].Columns["AmountOriginal"].CellActivation = Activation.NoEdit;
                    }
                };
            }
        }

        #endregion

        #region Utils

        #endregion

        #region Event
        #endregion

        private void btnOfNVL_Click(object sender, EventArgs e)
        {
            try
            {
                UltraGrid uGrid = (UltraGrid)Controls.Find("uGrid0", true).FirstOrDefault();
                if (uGrid != null) uGrid.UpdateDataGrid();
                if (_listObjectInput[0] is BindingList<RSInwardOutwardDetail>)
                {
                    var frm = new FSelectMaterialQuantum((_listObjectInput[0] as BindingList<RSInwardOutwardDetail>).ToList());
                    frm.ShowDialog(this);
                    if (frm.DialogResult != DialogResult.OK) return;
                    var lstSelect = frm.Selecteds;
                    if (lstSelect == null) return;
                    if (lstSelect.Count > 0)
                    {
                        _listObjectInput[0].Clear();
                    }
                    if (_select.RSAssemblyDismantlementID != null) _select.RSAssemblyDismantlementID = null;
                    for (int i = 0; i < _listObjectInput[0].Count; i++)
                    {
                        if ((_listObjectInput[0][i] as RSInwardOutwardDetail).MaterialQuantumID.HasValue) _listObjectInput[0].RemoveAt(i);
                    }
                    foreach (ObjMaterialQuantum ma in lstSelect)
                    {
                        foreach (var maDetail in ma.MaterialQuantumDetails)
                        {
                            RSInwardOutwardDetail temp = new RSInwardOutwardDetail();
                            ReflectionUtils.Copy(typeof(MaterialQuantumDetail), maDetail, typeof(RSInwardOutwardDetail), temp);
                            temp.ID = Guid.Empty;
                            temp.Quantity = ma.TotalQuantity * maDetail.Quantity;
                            temp.MaterialQuantity = ma.TotalQuantity * maDetail.Quantity;
                            temp.UnitPrice = maDetail.UnitPrice;
                            temp.UnitPriceOriginal = maDetail.UnitPrice;
                            temp.AmountOriginal = temp.UnitPriceOriginal * ma.TotalQuantity * maDetail.Quantity??0;
                            temp.Amount = temp.UnitPrice * ma.TotalQuantity * maDetail.Quantity??0;
                            temp.MaterialQuantumID = ma.ID;
                            _listObjectInput[0].Add(temp.CloneObject());
                        }
                    }
                }
                if (uGrid != null) uGrid.DataBind();
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }               
        private void chkXuatkhotheoNVL_CheckedChanged(object sender, EventArgs e)
        {
            bool _checked = ((UltraCheckEditor)sender).CheckState == CheckState.Checked;
            btnOfNVL.Enabled = _checked;
            _select.IsOutwardQuantum = _checked;
            if (_checked) chkXuatkhotheoHoadon.CheckState = CheckState.Unchecked;
        }

        private void chkXuatkhotheoHoadon_CheckStateChanged(object sender, EventArgs e)
        {
            bool _checked = ((UltraCheckEditor)sender).CheckState == CheckState.Checked;
            btnOfDDH.Enabled = _checked;
            _select.IsOutwardSAInvoice = _checked;
            if (_checked)
            {
                chkXuatkhotheoNVL.CheckState = CheckState.Unchecked;
                if(_listObjectInput.Count > 0)
                    _listObjectInput[0].Clear();
            }
            var uTabs = Controls.Find("ultraTabControl", true).FirstOrDefault() as UltraTabControl;
            if (uTabs != null)
            {
                foreach (var tab in uTabs.Tabs)
                {
                    var grid = Controls.Find(string.Format("uGrid{0}", tab.Index), true).FirstOrDefault() as UltraGrid;
                    if (grid == null) continue;
                    grid.DisplayLayout.Override.AllowAddNew = _checked ? AllowAddNew.No : grid.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
                    //if(_checked)
                    //foreach (var column in grid.DisplayLayout.Bands[0].Columns)
                    //{
                    //    column.CellActivation = Activation.NoEdit;
                    //}
                }
            }
        }

        private void btnOfDDH_Click(object sender, EventArgs e)
        {
            try
            {
                var frm = new FSAInvoiceNoDeliveryVoucher(_select);
                frm.ShowDialog(this);
                if (frm.DialogResult != DialogResult.OK) return;
                var lstSelect = frm.Selecteds;
                if (lstSelect == null) return;
                if (_listObjectInput[0] is BindingList<RSInwardOutwardDetail>)
                {
                    if (lstSelect.Count > 0)
                    {
                        _listObjectInput[0].Clear();
                        _select.SaInvoiceCustormIds.Clear();
                        if (_select.RSAssemblyDismantlementID != null) _select.RSAssemblyDismantlementID = null;
                    }
                    foreach (SaInvoiceCustorm saCustorm in lstSelect)
                    {
                        foreach (var saInvoiceDetail in saCustorm.SAInvoiceDetails)
                        {
                            RSInwardOutwardDetail temp = new RSInwardOutwardDetail();
                            ReflectionUtils.Copy(typeof(SAInvoiceDetail), saInvoiceDetail, typeof(RSInwardOutwardDetail), temp, new List<string> { "UnitPrice", "UnitPriceOriginal", "Amount", "AmountOriginal" });
                            //ReflectionUtils.Copy(typeof(RSInwardOutwardDetail), objRsInwardOutward.RSInwardOutwardDetail, typeof(SAInvoiceDetail), temp);
                            temp.ID = Guid.Empty;
                            temp.UnitPrice = saInvoiceDetail.OWPrice;
                            temp.UnitPriceOriginal = saInvoiceDetail.OWPriceOriginal;
                            temp.Amount = saInvoiceDetail.OWAmount;
                            temp.AmountOriginal = saInvoiceDetail.OWAmountOriginal;
                            _listObjectInput[0].Add(temp.CloneObject());
                        }
                        _select.SaInvoiceCustormIds.Add(saCustorm.ID.CloneObject());
                    }
                }
                UltraGrid uGrid = (UltraGrid)Controls.Find("uGrid0", true).FirstOrDefault();
                foreach(var x in uGrid.Rows)
                {
                    x.Cells["OWPurpose"].Value = 1;
                }
                if (uGrid != null) uGrid.DataBind();
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }

        private void btnselectOrder_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> _lstTypeID = new List<int> { 900, 901, 310 };
                var f = new FSelectType(_lstTypeID);
                f.FormClosed += new FormClosedEventHandler(fSelectType_FormClosed);
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                return;
            }
        }

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucherRSInwardOutward> datasource = (BindingList<RefVoucherRSInwardOutward>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucherRSInwardOutward>();
                    var f = new FViewVoucher(_select.CurrencyID, datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucher)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucherRSInwardOutward>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucherRSInwardOutward)
                    {
                        source.Add(new RefVoucherRSInwardOutward
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                if (_select.TypeID != 412 && _select.TypeID != 413 && _select.TypeID != 415)
                {
                    RefVoucherRSInwardOutward _temp = (RefVoucherRSInwardOutward)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                    if (_temp != null)
                        editFuntion(_temp);
                }
                else
                {
                    RefVoucher _temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                    if (_temp != null)
                        editFuntion(_temp);
                }

            }
        }

        private void editFuntion(RefVoucherRSInwardOutward temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void FRSInwardOutwardOutputDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }
    public class FrmTmp2 : DetailBase<RSInwardOutward>
    {
    }
}