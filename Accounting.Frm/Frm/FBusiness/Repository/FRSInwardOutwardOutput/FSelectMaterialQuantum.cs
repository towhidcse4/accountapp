﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FSelectMaterialQuantum : DialogForm
    {
        public List<ObjMaterialQuantum> Selecteds { get; private set; }
        public FSelectMaterialQuantum(List<RSInwardOutwardDetail> rs)
        {
            InitializeComponent();

            ConfigControl(rs);
        }

        private void ConfigControl(List<RSInwardOutwardDetail> rs)
        {
            List<Guid?> lstMaterialQuantumID = rs.GroupBy(r => r.MaterialQuantumID).Select(r => r.Key).ToList();
            var result = Utils.IMaterialQuantumService.Query.Where(m => m.IsActive /*&& m.FromDate.HasValue && m.ToDate.HasValue && m.FromDate.Value.Date < m.ToDate.Value.Date*/).OrderBy(m => m.MaterialQuantumCode).ToList();
            Utils.IMaterialQuantumService.UnbindSession(result);
            result = Utils.IMaterialQuantumService.Query.Where(m => m.IsActive /*&& m.FromDate.HasValue && m.ToDate.HasValue && m.FromDate.Value.Date < m.ToDate.Value.Date*/).OrderBy(m => m.MaterialQuantumCode).ToList();
            var input = new List<ObjMaterialQuantum>();
            foreach (var item in result)
            {
                var temp = new ObjMaterialQuantum();
                ReflectionUtils.Copy(typeof(MaterialQuantum), item, typeof(ObjMaterialQuantum), temp);
                temp.TotalQuantity = 1;
                //temp.Quantity = temp.MaterialQuantumDetails.Sum(m => m.Quantity ?? 0);
                input.Add(temp);
            }
            ConfigGrid(input);
            foreach (var row in uGrid.Rows)
            {
                row.Cells["Check"].Value = lstMaterialQuantumID.Contains((row.ListObject as MaterialQuantum).ID);
                if ((bool)row.Cells["Check"].Value)
                {
                    var md = row.ListObject as MaterialQuantum;
                    decimal q = 1;
                    if (result.Any(x => x.ID == (row.ListObject as MaterialQuantum).ID) && result.FirstOrDefault(x => x.ID == (row.ListObject as MaterialQuantum).ID).MaterialQuantumDetails.Count > 0
                        && result.FirstOrDefault(x => x.ID == (row.ListObject as MaterialQuantum).ID).MaterialQuantumDetails.Any(a => a.MaterialGoodsID == rs[0].MaterialGoodsID))
                    {
                       q = (result.FirstOrDefault(x => x.ID == (row.ListObject as MaterialQuantum).ID).MaterialQuantumDetails.FirstOrDefault(a => a.MaterialGoodsID == rs[0].MaterialGoodsID).Quantity ?? 1);
                    }                     
                    row.Cells["TotalQuantity"].Value = q == 0 ? 0 : rs[0].MaterialQuantity / q;
                }
                    
            }
        }

        private void ConfigGrid(List<ObjMaterialQuantum> input)
        {
            uGrid.DataSource = input;
            Utils.ConfigGrid(uGrid, ConstDatabase.ObjMaterialQuantum_KeyName, false);
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.UseFixedHeaders = false;
            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid);
            }
            uGrid.DisplayLayout.Bands[0].Columns["Check"].Header.VisiblePosition = 0;
            UltraGridColumn ugc = uGrid.DisplayLayout.Bands[0].Columns["Check"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            uGrid.UpdateData();
            Selecteds = (uGrid.DataSource as List<ObjMaterialQuantum>).Where(k => k.Check).ToList();
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
