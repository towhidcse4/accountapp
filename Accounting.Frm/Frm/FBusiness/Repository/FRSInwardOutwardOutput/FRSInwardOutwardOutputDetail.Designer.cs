﻿namespace Accounting
{
    partial class FRSInwardOutwardOutputDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            this.pnlUgrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.btnOfDDH = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnselectOrder = new Infragistics.Win.Misc.UltraButton();
            this.cbbCustomerID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtOriginalNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtContactName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.btnOfNVL = new Infragistics.Win.Misc.UltraButton();
            this.chkXuatkhotheoHoadon = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkXuatkhotheoNVL = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.pnlFill = new Infragistics.Win.Misc.UltraPanel();
            this.grpCheckBox = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this.grpTop = new Infragistics.Win.Misc.UltraGroupBox();
            this.pnlTopVouchers = new Infragistics.Win.Misc.UltraPanel();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.pnlUgrid.ClientArea.SuspendLayout();
            this.pnlUgrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCustomerID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkXuatkhotheoHoadon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkXuatkhotheoNVL)).BeginInit();
            this.pnlFill.ClientArea.SuspendLayout();
            this.pnlFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpCheckBox)).BeginInit();
            this.grpCheckBox.SuspendLayout();
            this.ultraPanel3.ClientArea.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpTop)).BeginInit();
            this.grpTop.SuspendLayout();
            this.pnlTopVouchers.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlUgrid
            // 
            // 
            // pnlUgrid.ClientArea
            // 
            this.pnlUgrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.pnlUgrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlUgrid.Location = new System.Drawing.Point(0, 64);
            this.pnlUgrid.Name = "pnlUgrid";
            this.pnlUgrid.Size = new System.Drawing.Size(1362, 413);
            this.pnlUgrid.TabIndex = 0;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance1;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(1264, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 7;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // btnOfDDH
            // 
            this.btnOfDDH.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnOfDDH.Enabled = false;
            this.btnOfDDH.Location = new System.Drawing.Point(248, 34);
            this.btnOfDDH.Name = "btnOfDDH";
            this.btnOfDDH.Size = new System.Drawing.Size(44, 23);
            this.btnOfDDH.TabIndex = 1013;
            this.btnOfDDH.Tag = "false";
            this.btnOfDDH.Text = "...";
            this.btnOfDDH.Click += new System.EventHandler(this.btnOfDDH_Click);
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.btnselectOrder);
            this.ultraGroupBox3.Controls.Add(this.cbbCustomerID);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox3.Controls.Add(this.txtOriginalNo);
            this.ultraGroupBox3.Controls.Add(this.txtReason);
            this.ultraGroupBox3.Controls.Add(this.txtContactName);
            this.ultraGroupBox3.Controls.Add(this.txtAddress);
            this.ultraGroupBox3.Controls.Add(this.txtAccountingObjectName);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance20.FontData.BoldAsString = "True";
            appearance20.FontData.SizeInPoints = 10F;
            this.ultraGroupBox3.HeaderAppearance = appearance20;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 84);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(1362, 170);
            this.ultraGroupBox3.TabIndex = 0;
            this.ultraGroupBox3.Text = "Thông tin chung";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnselectOrder
            // 
            this.btnselectOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnselectOrder.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnselectOrder.Location = new System.Drawing.Point(1120, 26);
            this.btnselectOrder.Name = "btnselectOrder";
            this.btnselectOrder.Size = new System.Drawing.Size(230, 22);
            this.btnselectOrder.TabIndex = 1025;
            this.btnselectOrder.Text = "Chọn chứng từ phát sinh nghiệp vụ";
            this.btnselectOrder.Click += new System.EventHandler(this.btnselectOrder_Click);
            // 
            // cbbCustomerID
            // 
            this.cbbCustomerID.AutoSize = false;
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbCustomerID.DisplayLayout.Appearance = appearance2;
            this.cbbCustomerID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbCustomerID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCustomerID.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCustomerID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.cbbCustomerID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCustomerID.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.cbbCustomerID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbCustomerID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbCustomerID.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbCustomerID.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.cbbCustomerID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbCustomerID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.cbbCustomerID.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbCustomerID.DisplayLayout.Override.CellAppearance = appearance9;
            this.cbbCustomerID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbCustomerID.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCustomerID.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.cbbCustomerID.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.cbbCustomerID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbCustomerID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.cbbCustomerID.DisplayLayout.Override.RowAppearance = appearance12;
            this.cbbCustomerID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbCustomerID.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.cbbCustomerID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbCustomerID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbCustomerID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbCustomerID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbCustomerID.Location = new System.Drawing.Point(112, 27);
            this.cbbCustomerID.Name = "cbbCustomerID";
            this.cbbCustomerID.Size = new System.Drawing.Size(196, 22);
            this.cbbCustomerID.TabIndex = 1003;
            // 
            // ultraLabel5
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance14;
            this.ultraLabel5.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel5.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel5.Location = new System.Drawing.Point(12, 137);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(81, 22);
            this.ultraLabel5.TabIndex = 1017;
            this.ultraLabel5.Text = "Kèm theo";
            // 
            // ultraLabel4
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance15;
            this.ultraLabel4.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel4.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel4.Location = new System.Drawing.Point(12, 96);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(81, 22);
            this.ultraLabel4.TabIndex = 1016;
            this.ultraLabel4.Text = "Lý do xuất";
            // 
            // ultraLabel6
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance16;
            this.ultraLabel6.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel6.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel6.Location = new System.Drawing.Point(12, 73);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(81, 22);
            this.ultraLabel6.TabIndex = 1015;
            this.ultraLabel6.Text = "Người nhận";
            // 
            // ultraLabel2
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance17;
            this.ultraLabel2.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel2.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel2.Location = new System.Drawing.Point(12, 50);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(81, 22);
            this.ultraLabel2.TabIndex = 1014;
            this.ultraLabel2.Text = "Địa chỉ";
            // 
            // ultraLabel1
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance18;
            this.ultraLabel1.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel1.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel1.Location = new System.Drawing.Point(12, 27);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(81, 22);
            this.ultraLabel1.TabIndex = 1013;
            this.ultraLabel1.Text = "Đối tượng";
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance19;
            this.ultraLabel3.Location = new System.Drawing.Point(1279, 137);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(71, 22);
            this.ultraLabel3.TabIndex = 11;
            this.ultraLabel3.Text = "Chứng từ gốc";
            // 
            // txtOriginalNo
            // 
            this.txtOriginalNo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOriginalNo.AutoSize = false;
            this.txtOriginalNo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtOriginalNo.Location = new System.Drawing.Point(112, 137);
            this.txtOriginalNo.MaxLength = 512;
            this.txtOriginalNo.Name = "txtOriginalNo";
            this.txtOriginalNo.Size = new System.Drawing.Size(1161, 22);
            this.txtOriginalNo.TabIndex = 1009;
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReason.Location = new System.Drawing.Point(112, 96);
            this.txtReason.MaxLength = 512;
            this.txtReason.Multiline = true;
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(1238, 40);
            this.txtReason.TabIndex = 1008;
            // 
            // txtContactName
            // 
            this.txtContactName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtContactName.AutoSize = false;
            this.txtContactName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtContactName.Location = new System.Drawing.Point(112, 73);
            this.txtContactName.MaxLength = 512;
            this.txtContactName.Name = "txtContactName";
            this.txtContactName.Size = new System.Drawing.Size(1238, 22);
            this.txtContactName.TabIndex = 1007;
            // 
            // txtAddress
            // 
            this.txtAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAddress.AutoSize = false;
            this.txtAddress.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAddress.Location = new System.Drawing.Point(112, 50);
            this.txtAddress.MaxLength = 512;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(1238, 22);
            this.txtAddress.TabIndex = 1006;
            // 
            // txtAccountingObjectName
            // 
            this.txtAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectName.AutoSize = false;
            this.txtAccountingObjectName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectName.Location = new System.Drawing.Point(312, 27);
            this.txtAccountingObjectName.MaxLength = 512;
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(802, 22);
            this.txtAccountingObjectName.TabIndex = 1004;
            // 
            // btnOfNVL
            // 
            this.btnOfNVL.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnOfNVL.Enabled = false;
            this.btnOfNVL.Location = new System.Drawing.Point(248, 8);
            this.btnOfNVL.Name = "btnOfNVL";
            this.btnOfNVL.Size = new System.Drawing.Size(44, 23);
            this.btnOfNVL.TabIndex = 1011;
            this.btnOfNVL.Tag = "false";
            this.btnOfNVL.Text = "...";
            this.btnOfNVL.Click += new System.EventHandler(this.btnOfNVL_Click);
            // 
            // chkXuatkhotheoHoadon
            // 
            this.chkXuatkhotheoHoadon.AutoSize = true;
            this.chkXuatkhotheoHoadon.BackColor = System.Drawing.Color.Transparent;
            this.chkXuatkhotheoHoadon.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkXuatkhotheoHoadon.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.chkXuatkhotheoHoadon.Location = new System.Drawing.Point(12, 38);
            this.chkXuatkhotheoHoadon.Name = "chkXuatkhotheoHoadon";
            this.chkXuatkhotheoHoadon.Size = new System.Drawing.Size(202, 17);
            this.chkXuatkhotheoHoadon.TabIndex = 1;
            this.chkXuatkhotheoHoadon.Text = "Xuất kho cho các hóa đơn bán hàng";
            this.chkXuatkhotheoHoadon.CheckStateChanged += new System.EventHandler(this.chkXuatkhotheoHoadon_CheckStateChanged);
            // 
            // chkXuatkhotheoNVL
            // 
            this.chkXuatkhotheoNVL.AutoSize = true;
            this.chkXuatkhotheoNVL.BackColor = System.Drawing.Color.Transparent;
            this.chkXuatkhotheoNVL.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkXuatkhotheoNVL.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.chkXuatkhotheoNVL.Location = new System.Drawing.Point(12, 12);
            this.chkXuatkhotheoNVL.Name = "chkXuatkhotheoNVL";
            this.chkXuatkhotheoNVL.Size = new System.Drawing.Size(165, 17);
            this.chkXuatkhotheoNVL.TabIndex = 0;
            this.chkXuatkhotheoNVL.Text = "Xuất kho theo định mức NVL";
            this.chkXuatkhotheoNVL.CheckedChanged += new System.EventHandler(this.chkXuatkhotheoNVL_CheckedChanged);
            // 
            // pnlFill
            // 
            this.pnlFill.AutoSize = true;
            // 
            // pnlFill.ClientArea
            // 
            this.pnlFill.ClientArea.Controls.Add(this.pnlUgrid);
            this.pnlFill.ClientArea.Controls.Add(this.grpCheckBox);
            this.pnlFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlFill.Location = new System.Drawing.Point(0, 264);
            this.pnlFill.Name = "pnlFill";
            this.pnlFill.Size = new System.Drawing.Size(1362, 477);
            this.pnlFill.TabIndex = 1015;
            // 
            // grpCheckBox
            // 
            this.grpCheckBox.Controls.Add(this.chkXuatkhotheoHoadon);
            this.grpCheckBox.Controls.Add(this.btnOfNVL);
            this.grpCheckBox.Controls.Add(this.btnOfDDH);
            this.grpCheckBox.Controls.Add(this.chkXuatkhotheoNVL);
            this.grpCheckBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpCheckBox.Location = new System.Drawing.Point(0, 0);
            this.grpCheckBox.Name = "grpCheckBox";
            this.grpCheckBox.Size = new System.Drawing.Size(1362, 64);
            this.grpCheckBox.TabIndex = 1014;
            this.grpCheckBox.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraPanel3
            // 
            // 
            // ultraPanel3.ClientArea
            // 
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraGroupBox3);
            this.ultraPanel3.ClientArea.Controls.Add(this.grpTop);
            this.ultraPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel3.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(1362, 254);
            this.ultraPanel3.TabIndex = 0;
            // 
            // grpTop
            // 
            this.grpTop.Controls.Add(this.pnlTopVouchers);
            this.grpTop.Dock = System.Windows.Forms.DockStyle.Top;
            appearance22.FontData.BoldAsString = "True";
            appearance22.FontData.SizeInPoints = 13F;
            this.grpTop.HeaderAppearance = appearance22;
            this.grpTop.Location = new System.Drawing.Point(0, 0);
            this.grpTop.Name = "grpTop";
            this.grpTop.Size = new System.Drawing.Size(1362, 84);
            this.grpTop.TabIndex = 0;
            this.grpTop.Text = "ultraGroupBox1";
            this.grpTop.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // pnlTopVouchers
            // 
            appearance21.BackColor = System.Drawing.Color.Transparent;
            appearance21.BackColor2 = System.Drawing.Color.Transparent;
            this.pnlTopVouchers.Appearance = appearance21;
            this.pnlTopVouchers.Location = new System.Drawing.Point(12, 28);
            this.pnlTopVouchers.Name = "pnlTopVouchers";
            this.pnlTopVouchers.Size = new System.Drawing.Size(422, 47);
            this.pnlTopVouchers.TabIndex = 0;
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 254);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 0;
            this.ultraSplitter1.Size = new System.Drawing.Size(1362, 10);
            this.ultraSplitter1.TabIndex = 1;
            // 
            // FRSInwardOutwardOutputDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 741);
            this.Controls.Add(this.pnlFill);
            this.Controls.Add(this.ultraSplitter1);
            this.Controls.Add(this.ultraPanel3);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FRSInwardOutwardOutputDetail";
            this.Text = "Chi tiết xuất kho";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FRSInwardOutwardOutputDetail_FormClosing);
            this.pnlUgrid.ClientArea.ResumeLayout(false);
            this.pnlUgrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ultraGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCustomerID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkXuatkhotheoHoadon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkXuatkhotheoNVL)).EndInit();
            this.pnlFill.ClientArea.ResumeLayout(false);
            this.pnlFill.ResumeLayout(false);
            this.pnlFill.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpCheckBox)).EndInit();
            this.grpCheckBox.ResumeLayout(false);
            this.grpCheckBox.PerformLayout();
            this.ultraPanel3.ClientArea.ResumeLayout(false);
            this.ultraPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpTop)).EndInit();
            this.grpTop.ResumeLayout(false);
            this.pnlTopVouchers.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOriginalNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAddress;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCustomerID;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraButton btnOfDDH;
        private Infragistics.Win.Misc.UltraButton btnOfNVL;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraPanel pnlUgrid;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkXuatkhotheoHoadon;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkXuatkhotheoNVL;
        private Infragistics.Win.Misc.UltraPanel pnlFill;
        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
        private Infragistics.Win.Misc.UltraGroupBox grpTop;
        private Infragistics.Win.Misc.UltraPanel pnlTopVouchers;
        private Infragistics.Win.Misc.UltraGroupBox grpCheckBox;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraButton btnselectOrder;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
    }
}