﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FSASelectOrders : CustormForm
    {
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        private DateTime _startDate;
        readonly BindingList<SASelectOrder> _lstSelectOrder = new BindingList<SASelectOrder>();
        public List<SASelectOrder> SelectOrders { get; private set; }

        public FSASelectOrders(Guid? accountingObjectId)
        {

            InitializeComponent();

            ConfigControl(accountingObjectId);
        }

        private void ConfigControl(Guid? accountingObjectId)
        {
            //var itemsCbbTime = new ValueListItem[]{};
            //foreach (var item in Utils.DicSelectTimes)
            //{
            //    cbbTime.Items.Add(item.Key, item.Value.Name);
            //}
            //cbbTime.SelectedItem = cbbTime.Items[4];
            //cbbTime.Items.AddRange(itemsCbbTime);

            var stringToDateTime = Utils.StringToDateTime(ConstFrm.DbStartDate);
            if (stringToDateTime != null)
                _startDate = (DateTime)stringToDateTime;
            else _startDate = DateTime.Now;
            //cbbAboutTime.DataSource = _lstItems;
            //cbbAboutTime.DisplayMember = "Name";
            //Utils.ConfigGrid(cbbAboutTime, ConstDatabase.ConfigXML_TableName);
            //cbbAboutTime.SelectedRow = cbbAboutTime.Rows[4];
            cbbAboutTime.ConfigComboSelectTime(_lstItems, "Name");
            //var thisMonthStart = _startDate.AddDays(1 - _startDate.Day);
            //var thisMonthEnd = thisMonthStart.AddMonths(1).AddSeconds(-1);
            //dteDateFrom.DateTime = thisMonthStart;
            //dteDateTo.DateTime = thisMonthEnd;

            this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObject, "AccountingObjectCode", "ID");
            if (accountingObjectId != null)
            {
                cbbAccountingObject.Value = accountingObjectId;
                btnGetData.PerformClick();
            }
            ConfigGrid(new BindingList<SASelectOrder>());
        }

        private void ConfigGrid(BindingList<SASelectOrder> input)
        {
            uGrid.DataSource = input;
            Utils.ConfigGrid(uGrid, ConstDatabase.SASelectOrder_KeyName, false, false, false);
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.UseFixedHeaders = false;
            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid);
            }
            uGrid.DisplayLayout.Bands[0].Columns["CheckColumn"].Header.VisiblePosition = 0;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private List<SAOrder> getData()
        {
            var result = Utils.ISAOrderService.Query.Where(x => x.Date >= dteDateFrom.DateTime && x.Date <= dteDateTo.DateTime).OrderByDescending(x => x.Date).ThenByDescending(x => x.No);
            if (cbbAccountingObject.Value != null && (Guid)cbbAccountingObject.Value != Guid.Empty)
            {
                return result.Where(x => x.AccountingObjectID == (Guid)cbbAccountingObject.Value).ToList();
            }
            return result.ToList();
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            _lstSelectOrder.Clear();
            var result = getData();
            Utils.ISAOrderService.UnbindSession(result);
            result = getData();
            foreach (var saOrder in result)
            {
                foreach (var saOrderDetail in saOrder.SAOrderDetails.Where(x => ((x.Quantity ?? 0) - (x.QuantityReceipt ?? 0)) > 0))
                {
                    var selectOrder = new SASelectOrder
                                          {
                                              ID = saOrder.ID,
                                              No = saOrder.No,
                                              Date = saOrder.Date,
                                              Reason = saOrder.Reason,
                                              MaterialGoodsID = saOrderDetail.MaterialGoodsID,
                                              QuantityRemaining = (saOrderDetail.Quantity ?? 0) - (saOrderDetail.QuantityReceipt ?? 0),
                                              QuantityOutward = (saOrderDetail.Quantity ?? 0) - (saOrderDetail.QuantityReceipt ?? 0),
                                              AccountingObjectID = saOrder.AccountingObjectID,
                                              SaOrderDetail = saOrderDetail
                                          };
                    var materialGoodsCustom = Utils.ListMaterialGoodsCustom.FirstOrDefault(x => x.ID == saOrderDetail.MaterialGoodsID);
                    if (materialGoodsCustom != null)
                        selectOrder.MaterialGoodsName =
                            materialGoodsCustom.
                                MaterialGoodsName;
                    _lstSelectOrder.Add(selectOrder);
                }
            }
            ConfigGrid(_lstSelectOrder);
        }

        private void btnViewSaOrder_Click(object sender, EventArgs e)
        {
            ViewSaOrder();
        }

        private void ViewSaOrder()
        {
            if (uGrid.ActiveRow == null) return;
            foreach (var item in uGrid.Rows)
            {
                if ((bool)item.Cells["CheckColumn"].Value == true)
                {
                    var selectOrder = item.ListObject as SASelectOrder;
                    if (selectOrder == null) return;
                    if (selectOrder.CheckColumn == true)
                    {
                        var saOder = Utils.ISAOrderService.Query.FirstOrDefault(x => x.ID == selectOrder.ID);
                        var f = new FSAOrderDetail(saOder, new List<SAOrder>() { saOder }, ConstFrm.optStatusForm.View);
                        f.ShowDialog(this);
                    }
                    //else { MessageBox.Show("Bạn cần chọn đơn đặt hàng trước", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
                }
            }
        }

        private void cbbAboutTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAboutTime.SelectedRow != null)
            {
                var model = cbbAboutTime.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(_startDate.Year,_startDate, model, out dtBegin, out dtEnd);
                dteDateFrom.DateTime = dtBegin;
                dteDateTo.DateTime = dtEnd;
            }
        }

        private void dteDateFrom_ValueChanged(object sender, EventArgs e)
        {
            if (ActiveControl != null && ActiveControl.Name == ((UltraDateTimeEditor)sender).Name)
                cbbAboutTime.SelectedRow = cbbAboutTime.Rows[34];
        }

        private void dteDateTo_ValueChanged(object sender, EventArgs e)
        {
            if (ActiveControl != null && ActiveControl.Name == ((UltraDateTimeEditor)sender).Name)
                cbbAboutTime.SelectedRow = cbbAboutTime.Rows[34];
        }

        private void uGrid_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            // edit by tungnt
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                SASelectOrder temp = (SASelectOrder)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                var saOder = Utils.ISAOrderService.Query.FirstOrDefault(x => x.ID == temp.ID);
                var f = new FSAOrderDetail(saOder, new List<SAOrder>() { saOder }, ConstFrm.optStatusForm.View);
                f.ShowDialog(this);
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            var lstSaOrder = from u in _lstSelectOrder where u.CheckColumn select u;
            if (lstSaOrder.Any(x => x.CheckColumn == true))
            {
                SelectOrders = lstSaOrder.ToList();
                DialogResult = DialogResult.OK;
                Close();
            }
            else
            {
                MSG.Warning("Bạn cần chọn đơn đặt hàng trước");
            }
           
        }
    }
}
