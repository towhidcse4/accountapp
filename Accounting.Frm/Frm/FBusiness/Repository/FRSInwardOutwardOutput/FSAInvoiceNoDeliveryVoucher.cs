﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FSAInvoiceNoDeliveryVoucher : DialogForm
    {
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        private DateTime _startDate;
        public List<SaInvoiceCustorm> Selecteds { get; private set; }
        RSInwardOutward _rsInwardOutward;
        public FSAInvoiceNoDeliveryVoucher(RSInwardOutward rs)
        {
            InitializeComponent();
            _rsInwardOutward = rs;
            ConfigControl(rs);
        }

        private void ConfigControl(RSInwardOutward rs)
        {
            var stringToDateTime = Utils.StringToDateTime(ConstFrm.DbStartDate);
            if (stringToDateTime != null)
                _startDate = (DateTime)stringToDateTime;
            else _startDate = DateTime.Now;
            cbbAboutTime.ConfigComboSelectTime(_lstItems, "Name");
            cbbAboutTime.SelectedRow = cbbAboutTime.Rows[4];

            ConfigGrid(rs);

            foreach (var row in uGrid.Rows)
            {
                row.Cells["Check"].Value = (row.ListObject as SAInvoice).OutwardRefID == rs.ID || rs.SaInvoiceCustormIds.Contains((row.ListObject as SAInvoice).ID);
            }
        }
        private void ConfigGrid(RSInwardOutward rs)
        {
            var result = Utils.ISAInvoiceService.Query.Where(x => x.Date >= dteDateFrom.DateTime && x.Date <= dteDateTo.DateTime && x.IsDeliveryVoucher == false && x.Recorded && (x.OutwardRefID == null || x.OutwardRefID == (rs.ID == Guid.Empty ? (Guid?)null : rs.ID))).OrderByDescending(s => s.PostedDate).ThenByDescending(s => s.No);
            Utils.ISAInvoiceService.UnbindSession(result);
            result = Utils.ISAInvoiceService.Query.Where(x => x.Date >= dteDateFrom.DateTime && x.Date <= dteDateTo.DateTime && x.IsDeliveryVoucher == false && x.Recorded && (x.OutwardRefID == null || x.OutwardRefID == (rs.ID == Guid.Empty ? (Guid?)null : rs.ID))).OrderByDescending(s => s.PostedDate).ThenByDescending(s => s.No);
            var input = new List<SaInvoiceCustorm>();
            foreach (var item in result)
            {
                var temp = new SaInvoiceCustorm();
                ReflectionUtils.Copy(typeof(SAInvoice), item, typeof(SaInvoiceCustorm), temp);
                input.Add(temp);
            }
            uGrid.DataSource = input;
            Utils.ConfigGrid(uGrid, ConstDatabase.SAInvoice_TableName_FSAInvoiceNoDeliveryVoucher, false);
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.UseFixedHeaders = false;
            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid);
                if (column.Key != "Check")
                    column.CellActivation = Activation.NoEdit;
            }
            uGrid.DisplayLayout.Bands[0].Columns["Check"].Header.VisiblePosition = 0;
            UltraGridColumn ugc = uGrid.DisplayLayout.Bands[0].Columns["Check"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            //List<SaInvoiceCustorm> SAInvoiceCustorm;
           var lst =  uGrid.DataSource as List<SaInvoiceCustorm>;
            if (lst.Any(x => x.Check == true))
            {
                uGrid.UpdateData();
                //lấy những cái được chọn và cosang nvert SainVoidDetails rồi import vào Grid bên dưới
                Selecteds = lst.Where(p => p.Check).ToList();
                DialogResult = DialogResult.OK;
                Close();
           }
            else
            {
               MessageBox.Show("Bạn cần phải chọn chứng từ trước");
           }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            ConfigGrid(_rsInwardOutward);
        }

        private void cbbAboutTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAboutTime.SelectedRow != null)
            {
                var model = cbbAboutTime.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(_startDate.Year,_startDate, model, out dtBegin, out dtEnd);
                dteDateFrom.DateTime = dtBegin;
                dteDateTo.DateTime = dtEnd;
            }
        }

        private void dteDateFrom_ValueChanged(object sender, EventArgs e)
        {
            if (ActiveControl != null && ActiveControl.Name == ((UltraDateTimeEditor)sender).Name)
                cbbAboutTime.SelectedRow = cbbAboutTime.Rows[34];
        }

        private void dteDateTo_ValueChanged(object sender, EventArgs e)
        {
            if (ActiveControl != null && ActiveControl.Name == ((UltraDateTimeEditor)sender).Name)
                cbbAboutTime.SelectedRow = cbbAboutTime.Rows[34];
        }
    }
}
