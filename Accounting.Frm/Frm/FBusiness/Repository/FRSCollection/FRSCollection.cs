﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Model;
using FX.Core;
using Accounting.TextMessage;
using System.Text.RegularExpressions;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using System.Globalization;
using System.Threading;
using Infragistics.Win.UltraWinGrid;
using Accounting;


namespace Accounting
{
    public partial class FRSCollection : FRSCollectionBase
    {
        private readonly IAccountingObjectService _IAccountingObjectService;
        private readonly IBankAccountDetailService _IBankAccountDetailService;
        public readonly ICurrencyService _ICurrencyService;
        List<Currency>lstCurrencies= new List<Currency>();

        public FRSCollection()
        {

            InitializeComponent();
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            _ICurrencyService = IoC.Resolve<ICurrencyService>();
            base.InitializeComponent1();
            List<string> list = new List<string>(new string[] { "Chuyển khoản", "Tiền mặt" });
            cbbTT.DataSource = list.ToArray();

            gbTM.Dock = DockStyle.Fill;
            
           
            //uGridHoaDon.DataSource = lstInvoices;
            //truyền dữ liệu vào combox đối tượng 
            cbbDT1.DataSource = _IAccountingObjectService.GetAll();
            cbbDT1.DisplayMember = "AccountingObjectCode";
            cbbDT2.DataSource = _IAccountingObjectService.GetAll();
            cbbDT2.DisplayMember = "AccountingObjectCode";
            Utils.ConfigGrid(cbbDT1, ConstDatabase.AccountingObject_TableName);
            Utils.ConfigGrid(cbbDT2, ConstDatabase.AccountingObject_TableName);
            //truyền dữ liệu và lấy dữ liệu từ bảng BanksAccount
            cbbBanks.DataSource = _IBankAccountDetailService.GetAll();
            cbbBanks.DisplayMember = "BankName";
            Utils.ConfigGrid(cbbBanks, ConstDatabase.BankAccountDetail_TableName);

            //uGridHachToan.DataSource = model.Posteds;
            // UltraGridBand band = uGridHoaDon.DisplayLayout.Bands[0];

            //truyền giá trị vào grid
            this.uGridTien.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            //uGridTien.DataSource = _ICurrencyService.GetAll();
            //Utils.ConfigGrid(uGridTien, ConstDatabase.Currency_TableName);


        }

        private void ultraGroupBox2_Click(object sender, System.EventArgs e)
        {

        }

        private void FRSCollection_Load(object sender, System.EventArgs e)
        {

        }

        private void ultraTextEditor4_ValueChanged(object sender, System.EventArgs e)
        {

        }

        private void lblLD_Click(object sender, System.EventArgs e)
        {

        }

        private void ultraTextEditor5_ValueChanged(object sender, System.EventArgs e)
        {

        }

        private void ultraCombo1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {


        }

        private void cbbTT_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
        
        }

        private void ultraTextEditor1_ValueChanged(object sender, System.EventArgs e)
        {

        }

        private void lblDC_Click(object sender, System.EventArgs e)
        {

        }

        private void cbbTT_ValueChanged(object sender, EventArgs e)
        {
            
            if (cbbTT.Text == "Tiền mặt")
            {
                gbTM.Visible = true;
                gbCK.Visible = false;
                gbTM.Dock=DockStyle.Fill;
                }
            else
            {
                gbTM.Visible = false;
                gbCK.Visible = true;
                gbCK.Dock = DockStyle.Fill;

            }
        }

        private void tabHT_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {

        }

        private void uGridTien_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
          
        }
     
    }
    
    public class FRSCollectionBase : DetailBase<SAInvoice>
    {
        
    }
}
