﻿namespace Accounting
{
    partial class FRSCollection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.tabHD = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridHoaDon = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridHachToan = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbTT = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblThanhToan = new Infragistics.Win.Misc.UltraLabel();
            this.dteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtePostedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblPostedDate = new Infragistics.Win.Misc.UltraLabel();
            this.txtNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblNo = new Infragistics.Win.Misc.UltraLabel();
            this.gbTM = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtKT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtlydo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtNN = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDC = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbDT1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblKT = new Infragistics.Win.Misc.UltraLabel();
            this.lblLD = new Infragistics.Win.Misc.UltraLabel();
            this.lblNN = new Infragistics.Win.Misc.UltraLabel();
            this.lblDC = new Infragistics.Win.Misc.UltraLabel();
            this.lblDT = new Infragistics.Win.Misc.UltraLabel();
            this.gbCK = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtBanks = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbBanks = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraTextEditor6 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtNN1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDC1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDT1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbDT2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridTien = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tabHT = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.tabHD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHoaDon)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHachToan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtePostedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbTM)).BeginInit();
            this.gbTM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtKT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlydo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDT1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbCK)).BeginInit();
            this.gbCK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBanks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBanks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNN1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDC1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDT1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDT2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridTien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabHT)).BeginInit();
            this.tabHT.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabHD
            // 
            this.tabHD.Controls.Add(this.uGridHoaDon);
            this.tabHD.Location = new System.Drawing.Point(-10000, -10000);
            this.tabHD.Name = "tabHD";
            this.tabHD.Size = new System.Drawing.Size(862, 229);
            // 
            // uGridHoaDon
            // 
            this.uGridHoaDon.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridHoaDon.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridHoaDon.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance1.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance1.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHoaDon.DisplayLayout.GroupByBox.Appearance = appearance1;
            appearance2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHoaDon.DisplayLayout.GroupByBox.BandLabelAppearance = appearance2;
            this.uGridHoaDon.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance3.BackColor2 = System.Drawing.SystemColors.Control;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHoaDon.DisplayLayout.GroupByBox.PromptAppearance = appearance3;
            this.uGridHoaDon.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridHoaDon.DisplayLayout.MaxRowScrollRegions = 1;
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridHoaDon.DisplayLayout.Override.ActiveCellAppearance = appearance4;
            appearance5.BackColor = System.Drawing.SystemColors.Highlight;
            appearance5.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridHoaDon.DisplayLayout.Override.ActiveRowAppearance = appearance5;
            this.uGridHoaDon.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridHoaDon.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            this.uGridHoaDon.DisplayLayout.Override.CardAreaAppearance = appearance6;
            appearance7.BorderColor = System.Drawing.Color.Silver;
            appearance7.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridHoaDon.DisplayLayout.Override.CellAppearance = appearance7;
            this.uGridHoaDon.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridHoaDon.DisplayLayout.Override.CellPadding = 0;
            appearance8.BackColor = System.Drawing.SystemColors.Control;
            appearance8.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance8.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHoaDon.DisplayLayout.Override.GroupByRowAppearance = appearance8;
            appearance9.TextHAlignAsString = "Left";
            this.uGridHoaDon.DisplayLayout.Override.HeaderAppearance = appearance9;
            this.uGridHoaDon.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridHoaDon.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            this.uGridHoaDon.DisplayLayout.Override.RowAppearance = appearance10;
            this.uGridHoaDon.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance11.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridHoaDon.DisplayLayout.Override.TemplateAddRowAppearance = appearance11;
            this.uGridHoaDon.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridHoaDon.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridHoaDon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridHoaDon.Location = new System.Drawing.Point(0, 0);
            this.uGridHoaDon.Name = "uGridHoaDon";
            this.uGridHoaDon.Size = new System.Drawing.Size(862, 229);
            this.uGridHoaDon.TabIndex = 0;
            this.uGridHoaDon.Text = "ultraGrid1";
            this.uGridHoaDon.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridHachToan);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(862, 229);
            // 
            // uGridHachToan
            // 
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridHachToan.DisplayLayout.Appearance = appearance12;
            this.uGridHachToan.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridHachToan.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHachToan.DisplayLayout.GroupByBox.Appearance = appearance13;
            appearance14.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHachToan.DisplayLayout.GroupByBox.BandLabelAppearance = appearance14;
            this.uGridHachToan.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance15.BackColor2 = System.Drawing.SystemColors.Control;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHachToan.DisplayLayout.GroupByBox.PromptAppearance = appearance15;
            this.uGridHachToan.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridHachToan.DisplayLayout.MaxRowScrollRegions = 1;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridHachToan.DisplayLayout.Override.ActiveCellAppearance = appearance16;
            appearance17.BackColor = System.Drawing.SystemColors.Highlight;
            appearance17.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridHachToan.DisplayLayout.Override.ActiveRowAppearance = appearance17;
            this.uGridHachToan.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridHachToan.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            this.uGridHachToan.DisplayLayout.Override.CardAreaAppearance = appearance18;
            appearance19.BorderColor = System.Drawing.Color.Silver;
            appearance19.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridHachToan.DisplayLayout.Override.CellAppearance = appearance19;
            this.uGridHachToan.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridHachToan.DisplayLayout.Override.CellPadding = 0;
            appearance20.BackColor = System.Drawing.SystemColors.Control;
            appearance20.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance20.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance20.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHachToan.DisplayLayout.Override.GroupByRowAppearance = appearance20;
            appearance21.TextHAlignAsString = "Left";
            this.uGridHachToan.DisplayLayout.Override.HeaderAppearance = appearance21;
            this.uGridHachToan.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridHachToan.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            this.uGridHachToan.DisplayLayout.Override.RowAppearance = appearance22;
            this.uGridHachToan.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance23.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridHachToan.DisplayLayout.Override.TemplateAddRowAppearance = appearance23;
            this.uGridHachToan.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridHachToan.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridHachToan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridHachToan.Location = new System.Drawing.Point(0, 0);
            this.uGridHachToan.Name = "uGridHachToan";
            this.uGridHachToan.Size = new System.Drawing.Size(862, 229);
            this.uGridHachToan.TabIndex = 1;
            this.uGridHachToan.Text = "ultraGrid2";
            // 
            // ultraGroupBox1
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox1.Appearance = appearance24;
            this.ultraGroupBox1.Controls.Add(this.cbbTT);
            this.ultraGroupBox1.Controls.Add(this.lblThanhToan);
            this.ultraGroupBox1.Controls.Add(this.dteDate);
            this.ultraGroupBox1.Controls.Add(this.dtePostedDate);
            this.ultraGroupBox1.Controls.Add(this.lblDate);
            this.ultraGroupBox1.Controls.Add(this.lblPostedDate);
            this.ultraGroupBox1.Controls.Add(this.txtNo);
            this.ultraGroupBox1.Controls.Add(this.lblNo);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            appearance43.FontData.BoldAsString = "True";
            appearance43.FontData.SizeInPoints = 13F;
            this.ultraGroupBox1.HeaderAppearance = appearance43;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 177);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(864, 74);
            this.ultraGroupBox1.TabIndex = 31;
            this.ultraGroupBox1.Text = "Thu Tiền Khách Hàng";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbTT
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbTT.DisplayLayout.Appearance = appearance25;
            this.cbbTT.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbTT.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbTT.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbTT.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.cbbTT.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbTT.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.cbbTT.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbTT.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbTT.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbTT.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.cbbTT.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbTT.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.cbbTT.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbTT.DisplayLayout.Override.CellAppearance = appearance32;
            this.cbbTT.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbTT.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbTT.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.cbbTT.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.cbbTT.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbTT.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.cbbTT.DisplayLayout.Override.RowAppearance = appearance35;
            this.cbbTT.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbTT.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.cbbTT.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbTT.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbTT.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbTT.Location = new System.Drawing.Point(741, 27);
            this.cbbTT.Name = "cbbTT";
            this.cbbTT.Size = new System.Drawing.Size(110, 22);
            this.cbbTT.TabIndex = 35;
            this.cbbTT.Text = "Tiền Mặt";
            this.cbbTT.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.cbbTT_InitializeLayout);
            this.cbbTT.ValueChanged += new System.EventHandler(this.cbbTT_ValueChanged);
            // 
            // lblThanhToan
            // 
            this.lblThanhToan.Location = new System.Drawing.Point(615, 32);
            this.lblThanhToan.Name = "lblThanhToan";
            this.lblThanhToan.Size = new System.Drawing.Size(120, 23);
            this.lblThanhToan.TabIndex = 34;
            this.lblThanhToan.Text = "Hình thức thanh toán";
            // 
            // dteDate
            // 
            appearance37.TextHAlignAsString = "Center";
            appearance37.TextVAlignAsString = "Middle";
            this.dteDate.Appearance = appearance37;
            this.dteDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteDate.Location = new System.Drawing.Point(203, 50);
            this.dteDate.MaskInput = "";
            this.dteDate.Name = "dteDate";
            this.dteDate.Size = new System.Drawing.Size(98, 21);
            this.dteDate.TabIndex = 33;
            this.dteDate.Value = null;
            // 
            // dtePostedDate
            // 
            appearance38.TextHAlignAsString = "Center";
            appearance38.TextVAlignAsString = "Middle";
            this.dtePostedDate.Appearance = appearance38;
            this.dtePostedDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtePostedDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtePostedDate.Location = new System.Drawing.Point(104, 50);
            this.dtePostedDate.MaskInput = "";
            this.dtePostedDate.Name = "dtePostedDate";
            this.dtePostedDate.Size = new System.Drawing.Size(99, 21);
            this.dtePostedDate.TabIndex = 32;
            this.dtePostedDate.Value = null;
            // 
            // lblDate
            // 
            appearance39.BackColor = System.Drawing.Color.Transparent;
            appearance39.TextHAlignAsString = "Center";
            appearance39.TextVAlignAsString = "Middle";
            this.lblDate.Appearance = appearance39;
            this.lblDate.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded4Thick;
            this.lblDate.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Etched;
            this.lblDate.Location = new System.Drawing.Point(201, 27);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(100, 24);
            this.lblDate.TabIndex = 31;
            this.lblDate.Text = "Ngày chứng từ";
            // 
            // lblPostedDate
            // 
            appearance40.BackColor = System.Drawing.Color.Transparent;
            appearance40.TextHAlignAsString = "Center";
            appearance40.TextVAlignAsString = "Middle";
            this.lblPostedDate.Appearance = appearance40;
            this.lblPostedDate.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded4Thick;
            this.lblPostedDate.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Etched;
            this.lblPostedDate.Location = new System.Drawing.Point(103, 27);
            this.lblPostedDate.Name = "lblPostedDate";
            this.lblPostedDate.Size = new System.Drawing.Size(100, 24);
            this.lblPostedDate.TabIndex = 30;
            this.lblPostedDate.Text = "Ngày hạch toán";
            // 
            // txtNo
            // 
            appearance41.TextHAlignAsString = "Center";
            this.txtNo.Appearance = appearance41;
            this.txtNo.Location = new System.Drawing.Point(5, 50);
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(99, 21);
            this.txtNo.TabIndex = 29;
            // 
            // lblNo
            // 
            appearance42.BackColor = System.Drawing.Color.Transparent;
            appearance42.TextHAlignAsString = "Center";
            appearance42.TextVAlignAsString = "Middle";
            this.lblNo.Appearance = appearance42;
            this.lblNo.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded4Thick;
            this.lblNo.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Etched;
            this.lblNo.Location = new System.Drawing.Point(5, 27);
            this.lblNo.Name = "lblNo";
            this.lblNo.Size = new System.Drawing.Size(100, 24);
            this.lblNo.TabIndex = 28;
            this.lblNo.Text = "Số chứng từ";
            // 
            // gbTM
            // 
            appearance44.BackColor = System.Drawing.Color.Transparent;
            this.gbTM.Appearance = appearance44;
            this.gbTM.Controls.Add(this.ultraLabel1);
            this.gbTM.Controls.Add(this.txtKT);
            this.gbTM.Controls.Add(this.txtlydo);
            this.gbTM.Controls.Add(this.txtNN);
            this.gbTM.Controls.Add(this.txtDC);
            this.gbTM.Controls.Add(this.txtDT);
            this.gbTM.Controls.Add(this.cbbDT1);
            this.gbTM.Controls.Add(this.lblKT);
            this.gbTM.Controls.Add(this.lblLD);
            this.gbTM.Controls.Add(this.lblNN);
            this.gbTM.Controls.Add(this.lblDC);
            this.gbTM.Controls.Add(this.lblDT);
            appearance57.FontData.BoldAsString = "True";
            appearance57.FontData.SizeInPoints = 10F;
            this.gbTM.HeaderAppearance = appearance57;
            this.gbTM.Location = new System.Drawing.Point(6, 4);
            this.gbTM.Name = "gbTM";
            this.gbTM.Size = new System.Drawing.Size(346, 156);
            this.gbTM.TabIndex = 12;
            this.gbTM.Text = "Thông tin chung";
            this.gbTM.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            this.gbTM.Click += new System.EventHandler(this.ultraGroupBox2_Click);
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraLabel1.Location = new System.Drawing.Point(247, 134);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(86, 23);
            this.ultraLabel1.TabIndex = 11;
            this.ultraLabel1.Text = "Chứng từ gốc";
            // 
            // txtKT
            // 
            this.txtKT.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtKT.Location = new System.Drawing.Point(89, 133);
            this.txtKT.Name = "txtKT";
            this.txtKT.Size = new System.Drawing.Size(152, 21);
            this.txtKT.TabIndex = 10;
            this.txtKT.ValueChanged += new System.EventHandler(this.ultraTextEditor5_ValueChanged);
            // 
            // txtlydo
            // 
            this.txtlydo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtlydo.Location = new System.Drawing.Point(88, 107);
            this.txtlydo.Name = "txtlydo";
            this.txtlydo.Size = new System.Drawing.Size(244, 21);
            this.txtlydo.TabIndex = 9;
            this.txtlydo.ValueChanged += new System.EventHandler(this.ultraTextEditor4_ValueChanged);
            // 
            // txtNN
            // 
            this.txtNN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNN.Location = new System.Drawing.Point(89, 80);
            this.txtNN.Name = "txtNN";
            this.txtNN.Size = new System.Drawing.Size(244, 21);
            this.txtNN.TabIndex = 8;
            // 
            // txtDC
            // 
            this.txtDC.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDC.Location = new System.Drawing.Point(89, 53);
            this.txtDC.Name = "txtDC";
            this.txtDC.Size = new System.Drawing.Size(244, 21);
            this.txtDC.TabIndex = 7;
            // 
            // txtDT
            // 
            this.txtDT.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDT.Location = new System.Drawing.Point(239, 26);
            this.txtDT.Name = "txtDT";
            this.txtDT.Size = new System.Drawing.Size(94, 21);
            this.txtDT.TabIndex = 6;
            this.txtDT.ValueChanged += new System.EventHandler(this.ultraTextEditor1_ValueChanged);
            // 
            // cbbDT1
            // 
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            appearance45.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbDT1.DisplayLayout.Appearance = appearance45;
            this.cbbDT1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbDT1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance46.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance46.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance46.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance46.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDT1.DisplayLayout.GroupByBox.Appearance = appearance46;
            appearance47.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDT1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance47;
            this.cbbDT1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance48.BackColor2 = System.Drawing.SystemColors.Control;
            appearance48.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance48.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDT1.DisplayLayout.GroupByBox.PromptAppearance = appearance48;
            this.cbbDT1.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbDT1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbDT1.DisplayLayout.Override.ActiveCellAppearance = appearance49;
            appearance50.BackColor = System.Drawing.SystemColors.Highlight;
            appearance50.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbDT1.DisplayLayout.Override.ActiveRowAppearance = appearance50;
            this.cbbDT1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbDT1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance51.BackColor = System.Drawing.SystemColors.Window;
            this.cbbDT1.DisplayLayout.Override.CardAreaAppearance = appearance51;
            appearance52.BorderColor = System.Drawing.Color.Silver;
            appearance52.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbDT1.DisplayLayout.Override.CellAppearance = appearance52;
            this.cbbDT1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbDT1.DisplayLayout.Override.CellPadding = 0;
            appearance53.BackColor = System.Drawing.SystemColors.Control;
            appearance53.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance53.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance53.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance53.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDT1.DisplayLayout.Override.GroupByRowAppearance = appearance53;
            appearance54.TextHAlignAsString = "Left";
            this.cbbDT1.DisplayLayout.Override.HeaderAppearance = appearance54;
            this.cbbDT1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbDT1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            appearance55.BorderColor = System.Drawing.Color.Silver;
            this.cbbDT1.DisplayLayout.Override.RowAppearance = appearance55;
            this.cbbDT1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance56.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbDT1.DisplayLayout.Override.TemplateAddRowAppearance = appearance56;
            this.cbbDT1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbDT1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbDT1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbDT1.Location = new System.Drawing.Point(89, 25);
            this.cbbDT1.Name = "cbbDT1";
            this.cbbDT1.Size = new System.Drawing.Size(144, 22);
            this.cbbDT1.TabIndex = 5;
            this.cbbDT1.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ultraCombo1_InitializeLayout);
            // 
            // lblKT
            // 
            this.lblKT.Location = new System.Drawing.Point(4, 137);
            this.lblKT.Name = "lblKT";
            this.lblKT.Size = new System.Drawing.Size(78, 23);
            this.lblKT.TabIndex = 4;
            this.lblKT.Text = "Kèm Theo";
            // 
            // lblLD
            // 
            this.lblLD.Location = new System.Drawing.Point(3, 112);
            this.lblLD.Name = "lblLD";
            this.lblLD.Size = new System.Drawing.Size(79, 23);
            this.lblLD.TabIndex = 3;
            this.lblLD.Text = "Lý Do Nộp";
            this.lblLD.Click += new System.EventHandler(this.lblLD_Click);
            // 
            // lblNN
            // 
            this.lblNN.Location = new System.Drawing.Point(4, 84);
            this.lblNN.Name = "lblNN";
            this.lblNN.Size = new System.Drawing.Size(79, 23);
            this.lblNN.TabIndex = 2;
            this.lblNN.Text = "Người Nộp";
            // 
            // lblDC
            // 
            this.lblDC.Location = new System.Drawing.Point(5, 55);
            this.lblDC.Name = "lblDC";
            this.lblDC.Size = new System.Drawing.Size(77, 23);
            this.lblDC.TabIndex = 1;
            this.lblDC.Text = "Địa Chỉ";
            this.lblDC.Click += new System.EventHandler(this.lblDC_Click);
            // 
            // lblDT
            // 
            this.lblDT.Location = new System.Drawing.Point(6, 26);
            this.lblDT.Name = "lblDT";
            this.lblDT.Size = new System.Drawing.Size(78, 23);
            this.lblDT.TabIndex = 0;
            this.lblDT.Text = "Đối Tượng";
            // 
            // gbCK
            // 
            appearance58.BackColor = System.Drawing.Color.Transparent;
            this.gbCK.Appearance = appearance58;
            this.gbCK.Controls.Add(this.txtBanks);
            this.gbCK.Controls.Add(this.cbbBanks);
            this.gbCK.Controls.Add(this.ultraTextEditor6);
            this.gbCK.Controls.Add(this.txtNN1);
            this.gbCK.Controls.Add(this.txtDC1);
            this.gbCK.Controls.Add(this.txtDT1);
            this.gbCK.Controls.Add(this.cbbDT2);
            this.gbCK.Controls.Add(this.ultraLabel3);
            this.gbCK.Controls.Add(this.ultraLabel4);
            this.gbCK.Controls.Add(this.ultraLabel5);
            this.gbCK.Controls.Add(this.ultraLabel6);
            this.gbCK.Controls.Add(this.ultraLabel7);
            appearance83.FontData.BoldAsString = "True";
            appearance83.FontData.SizeInPoints = 10F;
            this.gbCK.HeaderAppearance = appearance83;
            this.gbCK.Location = new System.Drawing.Point(358, 4);
            this.gbCK.Name = "gbCK";
            this.gbCK.Size = new System.Drawing.Size(490, 160);
            this.gbCK.TabIndex = 32;
            this.gbCK.Text = "Thông tin chung";
            this.gbCK.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            this.gbCK.Visible = false;
            // 
            // txtBanks
            // 
            this.txtBanks.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBanks.Location = new System.Drawing.Point(238, 107);
            this.txtBanks.Name = "txtBanks";
            this.txtBanks.Size = new System.Drawing.Size(239, 21);
            this.txtBanks.TabIndex = 13;
            // 
            // cbbBanks
            // 
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            appearance59.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbBanks.DisplayLayout.Appearance = appearance59;
            this.cbbBanks.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbBanks.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance60.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance60.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance60.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance60.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBanks.DisplayLayout.GroupByBox.Appearance = appearance60;
            appearance61.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBanks.DisplayLayout.GroupByBox.BandLabelAppearance = appearance61;
            this.cbbBanks.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance62.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance62.BackColor2 = System.Drawing.SystemColors.Control;
            appearance62.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance62.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBanks.DisplayLayout.GroupByBox.PromptAppearance = appearance62;
            this.cbbBanks.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbBanks.DisplayLayout.MaxRowScrollRegions = 1;
            appearance63.BackColor = System.Drawing.SystemColors.Window;
            appearance63.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbBanks.DisplayLayout.Override.ActiveCellAppearance = appearance63;
            appearance64.BackColor = System.Drawing.SystemColors.Highlight;
            appearance64.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbBanks.DisplayLayout.Override.ActiveRowAppearance = appearance64;
            this.cbbBanks.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbBanks.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            this.cbbBanks.DisplayLayout.Override.CardAreaAppearance = appearance65;
            appearance66.BorderColor = System.Drawing.Color.Silver;
            appearance66.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbBanks.DisplayLayout.Override.CellAppearance = appearance66;
            this.cbbBanks.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbBanks.DisplayLayout.Override.CellPadding = 0;
            appearance67.BackColor = System.Drawing.SystemColors.Control;
            appearance67.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance67.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance67.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance67.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBanks.DisplayLayout.Override.GroupByRowAppearance = appearance67;
            appearance68.TextHAlignAsString = "Left";
            this.cbbBanks.DisplayLayout.Override.HeaderAppearance = appearance68;
            this.cbbBanks.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbBanks.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance69.BackColor = System.Drawing.SystemColors.Window;
            appearance69.BorderColor = System.Drawing.Color.Silver;
            this.cbbBanks.DisplayLayout.Override.RowAppearance = appearance69;
            this.cbbBanks.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance70.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbBanks.DisplayLayout.Override.TemplateAddRowAppearance = appearance70;
            this.cbbBanks.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbBanks.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbBanks.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbBanks.Location = new System.Drawing.Point(90, 107);
            this.cbbBanks.Name = "cbbBanks";
            this.cbbBanks.Size = new System.Drawing.Size(144, 22);
            this.cbbBanks.TabIndex = 12;
            // 
            // ultraTextEditor6
            // 
            this.ultraTextEditor6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraTextEditor6.Location = new System.Drawing.Point(88, 135);
            this.ultraTextEditor6.Multiline = true;
            this.ultraTextEditor6.Name = "ultraTextEditor6";
            this.ultraTextEditor6.Size = new System.Drawing.Size(389, 17);
            this.ultraTextEditor6.TabIndex = 10;
            // 
            // txtNN1
            // 
            this.txtNN1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNN1.Location = new System.Drawing.Point(89, 82);
            this.txtNN1.Name = "txtNN1";
            this.txtNN1.Size = new System.Drawing.Size(388, 21);
            this.txtNN1.TabIndex = 8;
            // 
            // txtDC1
            // 
            this.txtDC1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDC1.Location = new System.Drawing.Point(89, 57);
            this.txtDC1.Name = "txtDC1";
            this.txtDC1.Size = new System.Drawing.Size(388, 21);
            this.txtDC1.TabIndex = 7;
            // 
            // txtDT1
            // 
            this.txtDT1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDT1.Location = new System.Drawing.Point(239, 31);
            this.txtDT1.Name = "txtDT1";
            this.txtDT1.Size = new System.Drawing.Size(239, 21);
            this.txtDT1.TabIndex = 6;
            // 
            // cbbDT2
            // 
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            appearance71.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbDT2.DisplayLayout.Appearance = appearance71;
            this.cbbDT2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbDT2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance72.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance72.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance72.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance72.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDT2.DisplayLayout.GroupByBox.Appearance = appearance72;
            appearance73.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDT2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance73;
            this.cbbDT2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance74.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance74.BackColor2 = System.Drawing.SystemColors.Control;
            appearance74.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance74.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDT2.DisplayLayout.GroupByBox.PromptAppearance = appearance74;
            this.cbbDT2.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbDT2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance75.BackColor = System.Drawing.SystemColors.Window;
            appearance75.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbDT2.DisplayLayout.Override.ActiveCellAppearance = appearance75;
            appearance76.BackColor = System.Drawing.SystemColors.Highlight;
            appearance76.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbDT2.DisplayLayout.Override.ActiveRowAppearance = appearance76;
            this.cbbDT2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbDT2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance77.BackColor = System.Drawing.SystemColors.Window;
            this.cbbDT2.DisplayLayout.Override.CardAreaAppearance = appearance77;
            appearance78.BorderColor = System.Drawing.Color.Silver;
            appearance78.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbDT2.DisplayLayout.Override.CellAppearance = appearance78;
            this.cbbDT2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbDT2.DisplayLayout.Override.CellPadding = 0;
            appearance79.BackColor = System.Drawing.SystemColors.Control;
            appearance79.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance79.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance79.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance79.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDT2.DisplayLayout.Override.GroupByRowAppearance = appearance79;
            appearance80.TextHAlignAsString = "Left";
            this.cbbDT2.DisplayLayout.Override.HeaderAppearance = appearance80;
            this.cbbDT2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbDT2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance81.BackColor = System.Drawing.SystemColors.Window;
            appearance81.BorderColor = System.Drawing.Color.Silver;
            this.cbbDT2.DisplayLayout.Override.RowAppearance = appearance81;
            this.cbbDT2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance82.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbDT2.DisplayLayout.Override.TemplateAddRowAppearance = appearance82;
            this.cbbDT2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbDT2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbDT2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbDT2.Location = new System.Drawing.Point(89, 30);
            this.cbbDT2.Name = "cbbDT2";
            this.cbbDT2.Size = new System.Drawing.Size(144, 22);
            this.cbbDT2.TabIndex = 5;
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Location = new System.Drawing.Point(6, 138);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(78, 23);
            this.ultraLabel3.TabIndex = 4;
            this.ultraLabel3.Text = "Diễn giải";
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.Location = new System.Drawing.Point(6, 109);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(78, 23);
            this.ultraLabel4.TabIndex = 3;
            this.ultraLabel4.Text = "Nộp vào TK";
            // 
            // ultraLabel5
            // 
            this.ultraLabel5.Location = new System.Drawing.Point(6, 82);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(79, 23);
            this.ultraLabel5.TabIndex = 2;
            this.ultraLabel5.Text = "Người Nộp";
            // 
            // ultraLabel6
            // 
            this.ultraLabel6.Location = new System.Drawing.Point(6, 59);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(77, 23);
            this.ultraLabel6.TabIndex = 1;
            this.ultraLabel6.Text = "Địa Chỉ";
            // 
            // ultraLabel7
            // 
            this.ultraLabel7.Location = new System.Drawing.Point(7, 30);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(78, 23);
            this.ultraLabel7.TabIndex = 0;
            this.ultraLabel7.Text = "Đối Tượng";
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.gbTM);
            this.ultraGroupBox3.Controls.Add(this.gbCK);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(864, 177);
            this.ultraGroupBox3.TabIndex = 34;
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.uGridTien);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 503);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(864, 45);
            this.ultraPanel1.TabIndex = 35;
            // 
            // uGridTien
            // 
            appearance84.BackColor = System.Drawing.SystemColors.Window;
            appearance84.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridTien.DisplayLayout.Appearance = appearance84;
            this.uGridTien.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridTien.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance85.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance85.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance85.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance85.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridTien.DisplayLayout.GroupByBox.Appearance = appearance85;
            appearance86.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridTien.DisplayLayout.GroupByBox.BandLabelAppearance = appearance86;
            this.uGridTien.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance87.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance87.BackColor2 = System.Drawing.SystemColors.Control;
            appearance87.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance87.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridTien.DisplayLayout.GroupByBox.PromptAppearance = appearance87;
            this.uGridTien.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridTien.DisplayLayout.MaxRowScrollRegions = 1;
            appearance88.BackColor = System.Drawing.SystemColors.Window;
            appearance88.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridTien.DisplayLayout.Override.ActiveCellAppearance = appearance88;
            appearance89.BackColor = System.Drawing.SystemColors.Highlight;
            appearance89.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridTien.DisplayLayout.Override.ActiveRowAppearance = appearance89;
            this.uGridTien.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridTien.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance90.BackColor = System.Drawing.SystemColors.Window;
            this.uGridTien.DisplayLayout.Override.CardAreaAppearance = appearance90;
            appearance91.BorderColor = System.Drawing.Color.Silver;
            appearance91.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridTien.DisplayLayout.Override.CellAppearance = appearance91;
            this.uGridTien.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridTien.DisplayLayout.Override.CellPadding = 0;
            appearance92.BackColor = System.Drawing.SystemColors.Control;
            appearance92.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance92.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance92.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance92.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridTien.DisplayLayout.Override.GroupByRowAppearance = appearance92;
            appearance93.TextHAlignAsString = "Left";
            this.uGridTien.DisplayLayout.Override.HeaderAppearance = appearance93;
            this.uGridTien.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridTien.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance94.BackColor = System.Drawing.SystemColors.Window;
            appearance94.BorderColor = System.Drawing.Color.Silver;
            this.uGridTien.DisplayLayout.Override.RowAppearance = appearance94;
            this.uGridTien.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance95.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridTien.DisplayLayout.Override.TemplateAddRowAppearance = appearance95;
            this.uGridTien.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridTien.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridTien.Dock = System.Windows.Forms.DockStyle.Right;
            this.uGridTien.Location = new System.Drawing.Point(536, 0);
            this.uGridTien.Name = "uGridTien";
            this.uGridTien.Size = new System.Drawing.Size(328, 45);
            this.uGridTien.TabIndex = 0;
            this.uGridTien.Text = "ultraGrid3";
            this.uGridTien.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.uGridTien_InitializeLayout);
            // 
            // tabHT
            // 
            this.tabHT.Controls.Add(this.ultraTabSharedControlsPage1);
            this.tabHT.Controls.Add(this.ultraTabPageControl2);
            this.tabHT.Controls.Add(this.tabHD);
            this.tabHT.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabHT.Location = new System.Drawing.Point(0, 251);
            this.tabHT.Name = "tabHT";
            this.tabHT.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.tabHT.Size = new System.Drawing.Size(864, 252);
            this.tabHT.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon;
            this.tabHT.TabIndex = 36;
            ultraTab1.TabPage = this.tabHD;
            ultraTab1.Text = "Hóa đơn";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Hạch toán";
            this.tabHT.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            this.tabHT.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            this.tabHT.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.tabHT_SelectedTabChanged);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(862, 229);
            // 
            // FRSCollection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(864, 548);
            this.Controls.Add(this.ultraPanel1);
            this.Controls.Add(this.tabHT);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.ultraGroupBox3);
            this.Name = "FRSCollection";
            this.Text = "FRSCollection";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FRSCollection_Load);
            this.tabHD.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridHoaDon)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridHachToan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtePostedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbTM)).EndInit();
            this.gbTM.ResumeLayout(false);
            this.gbTM.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtKT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlydo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDT1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbCK)).EndInit();
            this.gbCK.ResumeLayout(false);
            this.gbCK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBanks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBanks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNN1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDC1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDT1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDT2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridTien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabHT)).EndInit();
            this.tabHT.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox gbTM;
        public Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtePostedDate;
        private Infragistics.Win.Misc.UltraLabel lblDate;
        private Infragistics.Win.Misc.UltraLabel lblPostedDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNo;
        private Infragistics.Win.Misc.UltraLabel lblNo;
        private Infragistics.Win.Misc.UltraLabel lblThanhToan;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtlydo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNN;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDC;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDT;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDT1;
        private Infragistics.Win.Misc.UltraLabel lblKT;
        private Infragistics.Win.Misc.UltraLabel lblLD;
        private Infragistics.Win.Misc.UltraLabel lblNN;
        private Infragistics.Win.Misc.UltraLabel lblDC;
        private Infragistics.Win.Misc.UltraLabel lblDT;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtKT;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraGroupBox gbCK;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNN1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDC1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDT1;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDT2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBanks;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBanks;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbTT;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl tabHT;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabHD;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridTien;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridHachToan;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridHoaDon;
    }
}