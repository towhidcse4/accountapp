﻿namespace Accounting
{
    partial class FRSAssemblyDismantlementDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.pnlGrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this.Grb_IWA = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.lblAmount = new Infragistics.Win.Misc.UltraLabel();
            this.cbbMaterialGoods = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.lbkMateGoodID = new Infragistics.Win.Misc.UltraLabel();
            this.txtQuantity = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.lblUnitPrice = new Infragistics.Win.Misc.UltraLabel();
            this.txtIWReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtUnit = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtMaterialGoodsName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.pnlIwVoucher = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.optAccountingObjectType = new Accounting.UltraOptionSet_Ex();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.ultraPanel4 = new Infragistics.Win.Misc.UltraPanel();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.pnlGrid.ClientArea.SuspendLayout();
            this.pnlGrid.SuspendLayout();
            this.ultraPanel3.ClientArea.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grb_IWA)).BeginInit();
            this.Grb_IWA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaterialGoods)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIWReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaterialGoodsName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            this.pnlIwVoucher.SuspendLayout();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optAccountingObjectType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.ultraPanel4.ClientArea.SuspendLayout();
            this.ultraPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.pnlGrid);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 197);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(790, 217);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // pnlGrid
            // 
            // 
            // pnlGrid.ClientArea
            // 
            this.pnlGrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.pnlGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGrid.Location = new System.Drawing.Point(3, 0);
            this.pnlGrid.Name = "pnlGrid";
            this.pnlGrid.Size = new System.Drawing.Size(784, 214);
            this.pnlGrid.TabIndex = 0;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance1;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(692, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 7;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // ultraPanel3
            // 
            appearance2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ultraPanel3.Appearance = appearance2;
            // 
            // ultraPanel3.ClientArea
            // 
            this.ultraPanel3.ClientArea.Controls.Add(this.Grb_IWA);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraGroupBox4);
            this.ultraPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel3.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(790, 218);
            this.ultraPanel3.TabIndex = 2;
            // 
            // Grb_IWA
            // 
            this.Grb_IWA.Controls.Add(this.ultraLabel1);
            this.Grb_IWA.Controls.Add(this.lblAmount);
            this.Grb_IWA.Controls.Add(this.cbbMaterialGoods);
            this.Grb_IWA.Controls.Add(this.ultraLabel13);
            this.Grb_IWA.Controls.Add(this.ultraLabel9);
            this.Grb_IWA.Controls.Add(this.ultraLabel10);
            this.Grb_IWA.Controls.Add(this.ultraLabel4);
            this.Grb_IWA.Controls.Add(this.lbkMateGoodID);
            this.Grb_IWA.Controls.Add(this.txtQuantity);
            this.Grb_IWA.Controls.Add(this.lblUnitPrice);
            this.Grb_IWA.Controls.Add(this.txtIWReason);
            this.Grb_IWA.Controls.Add(this.txtUnit);
            this.Grb_IWA.Controls.Add(this.txtMaterialGoodsName);
            this.Grb_IWA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grb_IWA.Location = new System.Drawing.Point(0, 53);
            this.Grb_IWA.Name = "Grb_IWA";
            this.Grb_IWA.Size = new System.Drawing.Size(790, 165);
            this.Grb_IWA.TabIndex = 3;
            this.Grb_IWA.Text = "Phiếu nhập từ lắp ráp";
            this.Grb_IWA.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel1
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextHAlignAsString = "Left";
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance3;
            this.ultraLabel1.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel1.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel1.Location = new System.Drawing.Point(612, 45);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(66, 22);
            this.ultraLabel1.TabIndex = 1029;
            this.ultraLabel1.Text = "Thành tiền";
            // 
            // lblAmount
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextHAlignAsString = "Right";
            appearance4.TextVAlignAsString = "Middle";
            this.lblAmount.Appearance = appearance4;
            this.lblAmount.Location = new System.Drawing.Point(684, 44);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Padding = new System.Drawing.Size(5, 0);
            this.lblAmount.Size = new System.Drawing.Size(97, 22);
            this.lblAmount.TabIndex = 1028;
            this.lblAmount.Text = "00";
            // 
            // cbbMaterialGoods
            // 
            this.cbbMaterialGoods.AutoSize = false;
            this.cbbMaterialGoods.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbMaterialGoods.Location = new System.Drawing.Point(133, 16);
            this.cbbMaterialGoods.Name = "cbbMaterialGoods";
            this.cbbMaterialGoods.NullText = "<chọn dữ liệu>";
            this.cbbMaterialGoods.Size = new System.Drawing.Size(173, 22);
            this.cbbMaterialGoods.TabIndex = 32;
            // 
            // ultraLabel13
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextHAlignAsString = "Left";
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance5;
            this.ultraLabel13.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel13.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel13.Location = new System.Drawing.Point(472, 45);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(47, 22);
            this.ultraLabel13.TabIndex = 1027;
            this.ultraLabel13.Text = "Đơn giá";
            // 
            // ultraLabel9
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance6;
            this.ultraLabel9.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel9.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel9.Location = new System.Drawing.Point(310, 44);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel9.TabIndex = 1024;
            this.ultraLabel9.Text = "Số lượng";
            // 
            // ultraLabel10
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance7;
            this.ultraLabel10.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel10.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel10.Location = new System.Drawing.Point(3, 71);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(112, 22);
            this.ultraLabel10.TabIndex = 1023;
            this.ultraLabel10.Text = "Diễn giải";
            // 
            // ultraLabel4
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextHAlignAsString = "Left";
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance8;
            this.ultraLabel4.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel4.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel4.Location = new System.Drawing.Point(4, 44);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(112, 22);
            this.ultraLabel4.TabIndex = 1020;
            this.ultraLabel4.Text = "Đơn vị tính";
            // 
            // lbkMateGoodID
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextHAlignAsString = "Left";
            appearance9.TextVAlignAsString = "Middle";
            this.lbkMateGoodID.Appearance = appearance9;
            this.lbkMateGoodID.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lbkMateGoodID.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lbkMateGoodID.Location = new System.Drawing.Point(4, 16);
            this.lbkMateGoodID.Name = "lbkMateGoodID";
            this.lbkMateGoodID.Size = new System.Drawing.Size(112, 22);
            this.lbkMateGoodID.TabIndex = 1018;
            this.lbkMateGoodID.Text = "Mã thành phẩm (*)";
            // 
            // txtQuantity
            // 
            appearance10.TextHAlignAsString = "Right";
            this.txtQuantity.Appearance = appearance10;
            this.txtQuantity.AutoSize = false;
            this.txtQuantity.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtQuantity.InputMask = "{LOC}nn,nnn,nnn.nn";
            this.txtQuantity.Location = new System.Drawing.Point(366, 45);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.NullText = "0";
            this.txtQuantity.PromptChar = ' ';
            this.txtQuantity.Size = new System.Drawing.Size(100, 22);
            this.txtQuantity.TabIndex = 1011;
            this.txtQuantity.ValueChanged += new System.EventHandler(this.txtQuantity_ValueChanged);
            // 
            // lblUnitPrice
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextHAlignAsString = "Right";
            appearance11.TextVAlignAsString = "Middle";
            this.lblUnitPrice.Appearance = appearance11;
            this.lblUnitPrice.Location = new System.Drawing.Point(525, 44);
            this.lblUnitPrice.Name = "lblUnitPrice";
            this.lblUnitPrice.Padding = new System.Drawing.Size(5, 0);
            this.lblUnitPrice.Size = new System.Drawing.Size(81, 22);
            this.lblUnitPrice.TabIndex = 1017;
            this.lblUnitPrice.Text = "00";
            this.lblUnitPrice.TextChanged += new System.EventHandler(this.lbldowmgia_TextChanged);
            // 
            // txtIWReason
            // 
            this.txtIWReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIWReason.AutoSize = false;
            this.txtIWReason.Location = new System.Drawing.Point(132, 72);
            this.txtIWReason.MaxLength = 512;
            this.txtIWReason.Name = "txtIWReason";
            this.txtIWReason.Size = new System.Drawing.Size(649, 22);
            this.txtIWReason.TabIndex = 1015;
            // 
            // txtUnit
            // 
            this.txtUnit.AutoSize = false;
            this.txtUnit.Location = new System.Drawing.Point(133, 44);
            this.txtUnit.MaxLength = 30;
            this.txtUnit.Name = "txtUnit";
            this.txtUnit.Size = new System.Drawing.Size(173, 22);
            this.txtUnit.TabIndex = 1010;
            // 
            // txtMaterialGoodsName
            // 
            this.txtMaterialGoodsName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaterialGoodsName.AutoSize = false;
            this.txtMaterialGoodsName.Location = new System.Drawing.Point(310, 16);
            this.txtMaterialGoodsName.MaxLength = 512;
            this.txtMaterialGoodsName.Name = "txtMaterialGoodsName";
            this.txtMaterialGoodsName.Size = new System.Drawing.Size(472, 22);
            this.txtMaterialGoodsName.TabIndex = 1006;
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.pnlIwVoucher);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(790, 53);
            this.ultraGroupBox4.TabIndex = 2;
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // pnlIwVoucher
            // 
            this.pnlIwVoucher.Location = new System.Drawing.Point(9, 6);
            this.pnlIwVoucher.Name = "pnlIwVoucher";
            this.pnlIwVoucher.Size = new System.Drawing.Size(297, 41);
            this.pnlIwVoucher.TabIndex = 0;
            // 
            // ultraPanel2
            // 
            appearance12.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ultraPanel2.Appearance = appearance12;
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.optAccountingObjectType);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel2.Location = new System.Drawing.Point(3, 0);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(790, 30);
            this.ultraPanel2.TabIndex = 0;
            // 
            // optAccountingObjectType
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextHAlignAsString = "Center";
            appearance13.TextVAlignAsString = "Middle";
            this.optAccountingObjectType.Appearance = appearance13;
            this.optAccountingObjectType.BackColor = System.Drawing.Color.Transparent;
            this.optAccountingObjectType.BackColorInternal = System.Drawing.Color.Transparent;
            this.optAccountingObjectType.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.optAccountingObjectType.CheckedIndex = 0;
            valueListItem2.DataValue = "Default Item";
            valueListItem2.DisplayText = "1. Lắp ráp";
            valueListItem3.DataValue = "ValueListItem1";
            valueListItem3.DisplayText = "2. Tháo dỡ";
            this.optAccountingObjectType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem2,
            valueListItem3});
            this.optAccountingObjectType.Location = new System.Drawing.Point(9, 9);
            this.optAccountingObjectType.Name = "optAccountingObjectType";
            this.optAccountingObjectType.ReadOnly = false;
            this.optAccountingObjectType.Size = new System.Drawing.Size(176, 15);
            this.optAccountingObjectType.TabIndex = 30;
            this.optAccountingObjectType.Text = "1. Lắp ráp";
            this.optAccountingObjectType.ValueChanged += new System.EventHandler(this.optAccountingObjectType_ValueChanged);
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.ultraGroupBox1);
            this.ultraGroupBox2.Controls.Add(this.ultraSplitter1);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel4);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel2);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(796, 417);
            this.ultraGroupBox2.TabIndex = 1027;
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(3, 187);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 219;
            this.ultraSplitter1.Size = new System.Drawing.Size(790, 10);
            this.ultraSplitter1.TabIndex = 1;
            // 
            // ultraPanel4
            // 
            // 
            // ultraPanel4.ClientArea
            // 
            this.ultraPanel4.ClientArea.Controls.Add(this.ultraPanel3);
            this.ultraPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel4.Location = new System.Drawing.Point(3, 30);
            this.ultraPanel4.Name = "ultraPanel4";
            this.ultraPanel4.Size = new System.Drawing.Size(790, 157);
            this.ultraPanel4.TabIndex = 2;
            // 
            // FRSAssemblyDismantlementDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 417);
            this.Controls.Add(this.ultraGroupBox2);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MinimumSize = new System.Drawing.Size(812, 456);
            this.Name = "FRSAssemblyDismantlementDetail";
            this.Text = "Chi tiết Lắp ráp tháo dỡ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FRSAssemblyDismantlementDetail_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.pnlGrid.ClientArea.ResumeLayout(false);
            this.pnlGrid.ResumeLayout(false);
            this.ultraPanel3.ClientArea.ResumeLayout(false);
            this.ultraPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grb_IWA)).EndInit();
            this.Grb_IWA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaterialGoods)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIWReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaterialGoodsName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            this.pnlIwVoucher.ResumeLayout(false);
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optAccountingObjectType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraPanel4.ClientArea.ResumeLayout(false);
            this.ultraPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox Grb_IWA;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIWReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtUnit;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMaterialGoodsName;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraLabel lblUnitPrice;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtQuantity;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel lbkMateGoodID;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraPanel pnlGrid;
        private Accounting.UltraOptionSet_Ex optAccountingObjectType;
        private Infragistics.Win.Misc.UltraPanel pnlIwVoucher;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbMaterialGoods;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel lblAmount;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
    }
}