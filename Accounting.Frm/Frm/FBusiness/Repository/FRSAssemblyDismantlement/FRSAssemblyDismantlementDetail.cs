﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.Text.RegularExpressions;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using System.Globalization;
using System.Threading;
using Infragistics.Win.UltraWinGrid;


namespace Accounting
{
    public partial class FRSAssemblyDismantlementDetail : FrmTmpFRSAssemblyDismantlementDetail
    {
        #region khai báo
        protected Dictionary<string, List<Account>> _getAccountDefaultByTypeId = new Dictionary<string, List<Account>>(); //Chứa các tài khoản mặc định
        BindingList<RSAssemblyDismantlementDetail> _bdlSumDetail = new BindingList<RSAssemblyDismantlementDetail>();
        List<RSAssemblyDismantlementDetail> lstdetail = new List<RSAssemblyDismantlementDetail>();
        List<MaterialGoods> listMaterialGoods = new List<MaterialGoods>();
        FRSAssemblyDismantlement _frsAssemblyDismantlement = new FRSAssemblyDismantlement();
        Template mauGiaoDienIW;
        private UltraGrid uGrid;
        UltraGrid ugrid2 = null;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        #endregion
        #region khởi tạo
        public FRSAssemblyDismantlementDetail(RSAssemblyDismantlement temp, List<RSAssemblyDismantlement> dsRSAssemblyDismantlement, int statusForm)
        {//Add -> thêm, Sửa -> sửa, Xem -> Sửa ở trạng thái Enable = False

            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();


           // this.WindowState = FormWindowState.Maximized;

            #endregion

            _typeID = temp.TypeID;
            _statusForm = statusForm;
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                _select = new RSAssemblyDismantlement();
                // _select.TypeID = 404;
                _select.TypeID = 900;
                _select.Quantity = 1;
            }
            else _select = temp;
            _listSelects.AddRange(dsRSAssemblyDismantlement);
            #region Thiết lập ban đầu cho Form
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, _listSelects, statusForm);

            //this.WindowState = FormWindowState.Maximized;
           

            
            #endregion

        }
        #endregion
        #region Data Combox
        //public void LoadObjIWAccountingObject()
        //{
        //    // người giao
        //    this.ConfigCombo(Utils.ListAccountingObject, cbbIWAccountingObjectID, "AccountingObjectCode", "ID", _select, _select.TypeID.Equals(900) ? "IWAccountingObjectID" : "OWAccountingObjectID", accountingObjectType: 2);
        //}
        //public void LoadObjOWAccountingObject()
        //{

        //    //người nhận
        //    this.ConfigCombo(Utils.ListAccountingObject, cbbOWAccountingObjectID, "AccountingObjectCode", "ID", _select, _select.TypeID.Equals(901) ? "IWAccountingObjectID" : "OWAccountingObjectID", accountingObjectType: 2);
        //}
        //public void LoadObjCreditAccount()
        //{
        //    //tài khoản có
        //    IList<Account> lstTemp =
        //        IAccountDefaultService.GetAccountDefaultByTypeId(_select.TypeID, _select.TypeID.Equals(900) ? "DebitAccount" : "CreditAccount", Utils.ListAccountDefault,
        //                                                             Utils.ListAccount);
        //    this.ConfigCombo(lstTemp, cbbCreditAccount, "AccountNumber", "AccountNumber", _select,
        //                     _select.TypeID.Equals(900) ? "CreditAccount" : "DebitAccount", haveEditorButton: false);
        //    cbbCreditAccount.ClearAutoComplete();
        //    if (cbbCreditAccount.Value != null)
        //        cbbCreditAccount.UpdateData();
        //}
        //public void LoadObjDebitAccount()
        //{
        //    // tài khoản nợ            
        //    IList<Account> lstTemp =
        //        IAccountDefaultService.GetAccountDefaultByTypeId(_select.TypeID, _select.TypeID.Equals(901) ? "DebitAccount" : "CreditAccount", Utils.ListAccountDefault,
        //                                                             Utils.ListAccount);
        //    this.ConfigCombo(lstTemp, cbbDebitAccount, "AccountNumber", "AccountNumber", _select,
        //                     _select.TypeID.Equals(901) ? "CreditAccount" : "DebitAccount", haveEditorButton: false);
        //    cbbDebitAccount.ClearAutoComplete();
        //}
        #endregion

        #region override
        public override void InitializeGUI(RSAssemblyDismantlement input)
        {
            #region Lấy các giá trị const từ Database

            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)

            mauGiaoDienIW = Utils.GetMauGiaoDien(input.TypeID, input.TemplateID, Utils.ListTemplate);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDienIW.ID : input.TemplateID;
            #endregion
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                bdlRefVoucher = new BindingList<RefVoucher>(input.RefVouchers);
                
            }
                
            //lấy danh sách những tài khoản mặc định của chứng từ
            //_getAccountDefaultByTypeId = Utils.GetAccountDefaultByTypeID(input.TypeID);
            #endregion

            #region Thiết lập dữ liệu và Fill dữ liệu Obj vào control (nếu đang sửa)

            #endregion

            this.ConfigTopVouchersNo<RSAssemblyDismantlement>(pnlIwVoucher, "No", "", "Date", Utils.ListType.FirstOrDefault(o => o.ID == _select.TypeID).TypeGroupID, false);
            var commonBinding = new BindingList<RSAssemblyDismantlementDetail>();
            optAccountingObjectType.Enabled = _statusForm == ConstFrm.optStatusForm.Add;
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
                optAccountingObjectType.CheckedItem = _select.TypeID.Equals(900) ? optAccountingObjectType.Items[0] : optAccountingObjectType.Items[1];
                _select.AmountOriginal = 0;
                _select.Amount = 0;
                _select.CurrencyID = "VND";
                _select.ExchangeRate = 1;
                //Thực hiện set ngày hoạch toán và ngày chứng từ (lấy từ cài đặt chung) <hiện đang tạm thời set là ngày hiện tại để test>

            }
            else
            {
                optAccountingObjectType.CheckedItem = _select.TypeID.Equals(900) ? optAccountingObjectType.Items[0] : optAccountingObjectType.Items[1];//add by cuongpv
                commonBinding = new BindingList<RSAssemblyDismantlementDetail>(input.RSAssemblyDismantlementDetails);
            }

            _listObjectInput = new BindingList<System.Collections.IList> { commonBinding };
            _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
            this.ConfigGridByTemplete_General<RSAssemblyDismantlement>(pnlGrid, mauGiaoDienIW);

            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { true, false };
            this.ConfigGridByManyTemplete<RSAssemblyDismantlement>(input.TypeID, mauGiaoDienIW, true, manyTemp, manyStandard, _select.ID.ToString());

            ugrid2 = Controls.Find("uGrid1", true).FirstOrDefault() as UltraGrid;
            ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            ConfigMovingParts();
            var lst = Utils.ListMaterialGoodsCustom.Where(m => m.MaterialGoodsType == 1 || m.MaterialGoodsType == 3).ToList();
            this.ConfigCombo(lst, cbbMaterialGoods, "MaterialGoodsCode", "ID", _select, "MaterialGoodsID");
            //this.ConfigCombo(Utils.ListRepository, cbbRipository, "RepositoryCode", "ID", _select, "RepositoryID", haveEditorButton: false);
            //if (_statusForm.Equals(ConstFrm.optStatusForm.Add))
            //{
            //    //if (_select.TypeID.Equals(900) && IAccountDefaultService.DefaultCreditAccount() != null)
            //    //{
            //    //    _select.CreditAccount = IAccountDefaultService.DefaultCreditAccount();
            //    //}
            //    //else if (_select.TypeID.Equals(901) && IAccountDefaultService.DefaultDebitAccount() != null)
            //    //{
            //    //    _select.DebitAccount = IAccountDefaultService.DefaultDebitAccount();
            //    //}
            //}
            //else
            //{
            //    if (!lst.Any(m => m.ID == _select.MaterialGoodsID))
            //        _select.MaterialGoodsID = (Guid?)null;
            //}
            //LoadObjIWAccountingObject();
            //LoadObjOWAccountingObject();
            //LoadObjDebitAccount();
            //LoadObjCreditAccount();
            //LoadTextBox();
            DataBinding(new List<Control> { txtMaterialGoodsName, txtQuantity, txtUnit, lblUnitPrice }, "MaterialGoodsName,Quantity,Unit,UnitPrice");
            txtQuantity.FormatNumberic(ConstDatabase.Format_Quantity);
            ComboAutoFilterRowSelected = ComboAutoFilter_RowSelected;
            if(_select.TypeID==900&&_statusForm== ConstFrm.optStatusForm.Add)
            {
                txtIWReason.Value = "Lắp ráp";
                txtIWReason.Text = "Lắp ráp";
                _select.Reason = "Lắp ráp";
            }
        }



        #endregion
        #region Utils
        void LoadTextBox()
        {
            DataBinding(new List<Control> { txtIWReason }, "Reason");
        }
        bool checkObjinCbbAccountingObjectID(string strText, List<AccountingObject> dsAccountingObject)
        {
            //check xem đối tượng có chính xác hay không?
            return dsAccountingObject.Any(item => item.AccountingObjectCode.Equals(strText));
        }
        bool checkObjinCbbMaterialObjectID(string strTextMateria, List<MaterialGoods> dsMaterialGoodObject)
        {
            //check xem hàng hóa có chính xác hay không?
            return dsMaterialGoodObject.Any(item => item.MaterialGoodsCode.Equals(strTextMateria));
        }

        void ConfigMovingParts()
        {
            if (optAccountingObjectType.CheckedItem != null)
            {
                if (optAccountingObjectType.CheckedIndex == 0)
                {
                    if (isRunCheckedChanged) return;

                    Grb_IWA.Text = "Lệnh lắp ráp";
                    utmDetailBaseToolBar.Ribbon.NonInheritedRibbonTabs[0].Caption = "Lắp ráp";
                    if (_statusForm == ConstFrm.optStatusForm.Add) _select.TypeID = 900;
                    _typeID = _select.TypeID;
                    //kiem tra su thay doi cu moi
                    var isAdd = _statusForm == ConstFrm.optStatusForm.Add;
                    mauGiaoDienIW = Utils.GetMauGiaoDien(_select.TypeID, isAdd ? null : _select.TemplateID, Utils.ListTemplate);
                    if (_select.TypeID == 900 && _statusForm == ConstFrm.optStatusForm.Add)
                    {
                        txtIWReason.Value = "Lắp ráp";
                        txtIWReason.Text = "Lắp ráp";
                        _select.Reason = "Lắp ráp";
                    }
                }
                if (optAccountingObjectType.CheckedIndex == 1)
                {
                    Grb_IWA.Text = "Lệnh tháo dỡ";
                    utmDetailBaseToolBar.Ribbon.NonInheritedRibbonTabs[0].Caption = "Tháo dỡ";
                    if (_statusForm == ConstFrm.optStatusForm.Add) _select.TypeID = 901;
                    _typeID = _select.TypeID;
                    //kiem tra su thay doi cu moi
                    var isAdd = _statusForm == ConstFrm.optStatusForm.Add;

                    mauGiaoDienIW = Utils.GetMauGiaoDien(_select.TypeID, isAdd ? null : _select.TemplateID, Utils.ListTemplate);
                    if (_select.TypeID == 901 && _statusForm == ConstFrm.optStatusForm.Add)
                    {
                        txtIWReason.Value = "Tháo dỡ";
                        txtIWReason.Text = "Tháo dỡ";
                        _select.Reason = "Tháo dỡ";
                    }
                }
                _select.TemplateID = mauGiaoDienIW.ID;
                //string temp = _select.CreditAccount;
                //_select.CreditAccount = _select.DebitAccount;
                //_select.DebitAccount = temp;
                //this.ConfigTopVouchersNo<RSAssemblyDismantlement>(pnlIwVoucher, _select.TypeID.Equals(900) ? "IWNo" : "OWNo",
                //                                              _select.TypeID.Equals(900)
                //                                                  ? "IWPostedDate"
                //                                                  : "OWPostedDate",
                //                                              _select.TypeID.Equals(900) ? "IWDate" : "OWDate",
                //                                              _select.TypeID.Equals(900) ? 40 : 41);

                //this.ConfigTopVouchersNo<RSAssemblyDismantlement>(pnlIwVoucher, "No", "", "Date", _select.TypeID.Equals(900) ? 40 : 41, false);
                List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInputPost };
                List<Boolean> manyStandard = new List<Boolean>() { true, false };
                this.ConfigGridByManyTemplete<RSAssemblyDismantlement>(_select.TypeID, mauGiaoDienIW, true, manyTemp, manyStandard, _select.ID.ToString());
                uGrid = (UltraGrid)this.Controls.Find("uGrid0", true).FirstOrDefault();
                if (uGrid != null)
                {
                    string[] keyColumn = new string[] { "UnitPriceOriginal", "UnitPrice", "AmountOriginal", "Amount" };
                    foreach (string s in keyColumn)
                    {
                        uGrid.DisplayLayout.Bands[0].Columns[s].CellActivation = Activation.NoEdit;
                        uGrid.DisplayLayout.Bands[0].Columns[s].CellAppearance.BackColor = Color.FromArgb(225, 225, 225);
                        uGrid.DisplayLayout.Bands[0].Columns[s].CellAppearance.BorderColor = Color.White;
                    }
                    if (cbbMaterialGoods.SelectedRow != null)
                    {
                        MaterialGoodsCustom model = (MaterialGoodsCustom)cbbMaterialGoods.SelectedRow.ListObject;
                        var dtSource = model.MaterialGoodsAssemblys;
                        foreach (UltraGridRow row in uGrid.Rows)
                        {
                            int index =
                                dtSource.IndexOf(dtSource.FirstOrDefault(p => p.ID == (Guid)row.Cells["ID"].Value));
                            row.Cells["UnitPrice"].Value = index.Equals(-1) ? 0 : dtSource[index].UnitPrice;
                            row.Cells["UnitPriceOriginal"].Value = row.Cells["UnitPrice"].Value;
                            row.Cells["AmountOriginal"].Value = (decimal)row.Cells["UnitPrice"].Value * (decimal)row.Cells["Quantity"].Value;
                            row.Cells["Amount"].Value = row.Cells["AmountOriginal"].Value;
                        }
                    }
                }
                //LoadObjIWAccountingObject();
                //LoadObjOWAccountingObject();
                //LoadObjDebitAccount();
                //LoadObjCreditAccount();
                LoadTextBox();
            }
        }

        #endregion
        #region Event
        private void ComboAutoFilter_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row != null)
            {
                if (e.Row.ListObject.GetType().Name.Equals(typeof(MaterialGoodsCustom).Name))
                {
                    decimal sl = Convert.ToDecimal(string.IsNullOrEmpty(txtQuantity.Text) ? "1" : txtQuantity.Text);
                    MaterialGoodsCustom model = (MaterialGoodsCustom)e.Row.ListObject;
                    //if (_select.TypeID.Equals(900))
                    //    _select.DebitAccount = model.ReponsitoryAccount;
                    //else
                    //    _select.CreditAccount = model.ReponsitoryAccount;
                    var dtSource = model.MaterialGoodsAssemblys;
                    BindingList<RSAssemblyDismantlementDetail> commonBinding = new BindingList<RSAssemblyDismantlementDetail>();
                    lstdetail.Clear();
                    foreach (MaterialGoodsAssembly mgAssembly in dtSource)
                    {
                        RSAssemblyDismantlementDetail temp = new RSAssemblyDismantlementDetail();
                        temp.MaterialGoodsID = mgAssembly.MaterialAssemblyID;
                        temp.Description = mgAssembly.MaterialAssemblyDescription;
                        temp.RepositoryID = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == mgAssembly.MaterialAssemblyID).RepositoryID;
                        temp.Unit = mgAssembly.Unit;
                        temp.UnitConvert = mgAssembly.UnitConvert;
                        temp.Quantity = (mgAssembly.Quantity);
                        temp.QuantityConvert = (mgAssembly.QuantityConvert);
                        temp.UnitPrice = mgAssembly.UnitPrice;
                        temp.UnitPriceOriginal = temp.UnitPrice;
                        temp.Amount = temp.UnitPrice * (decimal)temp.Quantity;
                        temp.AmountOriginal = temp.Amount;
                        commonBinding.Add(temp);
                        lstdetail.Add(temp.CloneObject());
                    }
                    lstdetail = lstdetail.CloneObject();
                    if (sl != 1)
                    {
                        foreach (var x in commonBinding)
                        {
                            x.Quantity = (x.Quantity * sl);
                            x.QuantityConvert = (x.QuantityConvert * sl);
                            x.Amount = x.UnitPrice * (decimal)x.Quantity;
                            x.AmountOriginal = x.Amount;
                        }
                    }
                    _listObjectInput = new BindingList<System.Collections.IList> { commonBinding };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { dsTIOriginalVouchers };
                    //this.ConfigGridByTemplete_General<RSAssemblyDismantlement>(pnlGrid, mauGiaoDienIW);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, false };
                    this.ConfigGridByManyTemplete<RSAssemblyDismantlement>(_select.TypeID, mauGiaoDienIW, true, manyTemp, manyStandard, _select.ID.ToString());
                    //uGrid.DataSource = dtSource;
                    if (uGrid != null)
                    {
                        string[] keyColumn = new string[] { "UnitPriceOriginal", "UnitPrice", "AmountOriginal", "Amount" };
                        foreach (string s in keyColumn)
                        {
                            uGrid.DisplayLayout.Bands[0].Columns[s].CellActivation = Activation.NoEdit;
                            uGrid.DisplayLayout.Bands[0].Columns[s].CellAppearance.BackColor = _select.TypeID.Equals(900) ? Color.FromArgb(225, 225, 225) : Color.Transparent;
                            uGrid.DisplayLayout.Bands[0].Columns[s].CellAppearance.BorderColor = _select.TypeID.Equals(900) ? Color.White : Color.FromArgb(218, 220, 221);
                        }
                    }
                }
            }
        }

        private void cbbIWAccountingObjectID_EditorButtonClick(object sender, EditorButtonEventArgs e)
        {
            try
            {
                new FAccountingObjectEmployeeDetail().ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                return;
            }
            //LoadObjIWAccountingObject();
            //cbbIWAccountingObjectID.SelectedText = FAccountingObjectEmployeeDetail.AccounttingObjectCode;
        }

        private void cbbOWAccountingObjectID_EditorButtonClick(object sender, EditorButtonEventArgs e)
        {
            try
            {
                new FAccountingObjectEmployeeDetail().ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                return;
            }
            //LoadObjOWAccountingObject();
            //cbbOWAccountingObjectID.SelectedText = FAccountingObjectEmployeeDetail.AccounttingObjectCode;
        }

        private void cbbMaterialGoods_Leave(object sender, EventArgs e)
        {
            //rời khởi control thì kiểm tra
            string textcbMaterialGoodsObjectId = cbbMaterialGoods.Text;
            if (string.IsNullOrEmpty(textcbMaterialGoodsObjectId)) return;
            List<MaterialGoods> dsMaterialObject = (List<MaterialGoods>)cbbMaterialGoods.DataSource;
            if (!checkObjinCbbMaterialObjectID(textcbMaterialGoodsObjectId, dsMaterialObject)) MSG.Error("Không có hàng hóa này trong danh mục");  //không có hàng hóa
        }

        //private void cbbIWAccountingObjectID_Leave(object sender, EventArgs e)
        //{
        //    //rời khởi control thì kiểm tra
        //    string textOfCbbAccountingObjectId = cbbIWAccountingObjectID.Text;
        //    if (string.IsNullOrEmpty(textOfCbbAccountingObjectId)) return;
        //    List<AccountingObject> dsAccountingObject = (List<AccountingObject>)cbbIWAccountingObjectID.DataSource;
        //    if (!checkObjinCbbAccountingObjectID(textOfCbbAccountingObjectId, dsAccountingObject)) MSG.Error("Không có đối tượng kế toán này trong danh mục");  //không có đối tượng này
        //}

        //private void cbbOWAccountingObjectID_Leave(object sender, EventArgs e)
        //{
        //    //rời khởi control thì kiểm tra
        //    string textOfCbbAccountingObjectId = cbbIWAccountingObjectID.Text;
        //    if (string.IsNullOrEmpty(textOfCbbAccountingObjectId)) return;
        //    List<AccountingObject> dsAccountingObject = (List<AccountingObject>)cbbIWAccountingObjectID.DataSource;
        //    if (!checkObjinCbbAccountingObjectID(textOfCbbAccountingObjectId, dsAccountingObject)) MSG.Error("Không có đối tượng kế toán này trong danh mục");  //không có đối tượng này
        //}

        private void cbbMaterialGoods_RowSelected(object sender, RowSelectedEventArgs e)
        {

            if (e.Row == null || e.Row.ListObject == null) return;
            MaterialGoods item = (MaterialGoods)e.Row.ListObject;
            txtMaterialGoodsName.Text = item.MaterialGoodsName;
            txtUnit.Text = item.Unit;
            //cbbRipository.Text = item.RepositoryID.ToString();
            txtQuantity.Text = item.Quantity.ToString();

            //foreach (var item2 in cbbRipository.Rows)
            //{
            //    if ((item2.ListObject as Repository).ID == item.RepositoryID)
            //    {
            //        cbbRipository.SelectedRow = item2;
            //        break;
            //    }
            //}

            //foreach (var item2 in cbbDebitAccount.Rows)
            //{
            //    if ((item2.ListObject as Account).AccountNumber == item.ReponsitoryAccount)
            //    {
            //        cbbDebitAccount.SelectedRow = item2;
            //        break;
            //    }
            //}

            //foreach (var item2 in cbbCreditAccount.Rows)
            //{
            //    if ((item2.ListObject as Account).AccountNumber == item.ExpenseAccount)
            //    {
            //        cbbCreditAccount.SelectedRow = item2;
            //        break;
            //    }
            //}
        }

        //private void cbbIWAccountingObjectID_RowSelected(object sender, RowSelectedEventArgs e)
        //{
        //    if (e.Row == null || e.Row.ListObject == null) return;
        //    AccountingObject item = (AccountingObject)e.Row.ListObject;
        //    txtIWAccountingObjectName.Text = item.AccountingObjectName;
        //    txtIWDepartment.Text = item.DepartmentIDViewName;
        //    cbbOWAccountingObjectID.Text = item.AccountingObjectCode;
        //    txtOWAccountingObjectName.Text = item.AccountingObjectName;
        //    txtIWDepartment.Text = item.Department == null ? string.Empty : item.Department.DepartmentName;
        //    txtOWDepartment.Text = item.Department == null ? string.Empty : item.Department.DepartmentName;

        //}

        //private void cbbOWAccountingObjectID_RowSelected(object sender, RowSelectedEventArgs e)
        //{
        //    if (e.Row == null || e.Row.ListObject == null) return;
        //    AccountingObject item = (AccountingObject)e.Row.ListObject;
        //    cbbOWAccountingObjectID.Text = item.AccountingObjectCode;
        //    txtOWAccountingObjectName.Text = item.AccountingObjectName;
        //    txtOWDepartment.Text = item.Department == null ? string.Empty : item.Department.DepartmentName;
        //}


        #endregion

        bool isRunCheckedChanged = false;
        private void optAccountingObjectType_ValueChanged(object sender, EventArgs e)
        {
            ConfigMovingParts();
        }

        private void lbldowmgia_TextChanged(object sender, EventArgs e)
        {
            lblUnitPrice.FormatNumberic(txtUnit.Text, ConstDatabase.Format_DonGiaQuyDoi);
        }

        private void txtQuantity_ValueChanged(object sender, EventArgs e)
        {
            decimal sl = Convert.ToDecimal(string.IsNullOrEmpty(txtQuantity.Text) ? "1" : txtQuantity.Text);
            uGrid = (UltraGrid)this.Controls.Find("uGrid0", true).FirstOrDefault();
            foreach (var row in uGrid.Rows)
            {
                row.Cells["Quantity"].Value = lstdetail.FirstOrDefault(d => d.MaterialGoodsID == (Guid)row.Cells["MaterialGoodsID"].Value) != null ? (lstdetail.FirstOrDefault(d => d.MaterialGoodsID == (Guid)row.Cells["MaterialGoodsID"].Value).Quantity * sl) : row.Cells["Quantity"].Value;
                row.Cells["UnitPrice"].Value = lstdetail.FirstOrDefault(d => d.MaterialGoodsID == (Guid)row.Cells["MaterialGoodsID"].Value) != null ? (lstdetail.FirstOrDefault(d => d.MaterialGoodsID == (Guid)row.Cells["MaterialGoodsID"].Value).UnitPrice ) : row.Cells["UnitPrice"].Value;
                row.Cells["UnitPriceOriginal"].Value = row.Cells["UnitPrice"].Value;
                row.Cells["AmountOriginal"].Value = (decimal)row.Cells["UnitPrice"].Value * (decimal)row.Cells["Quantity"].Value;
                row.Cells["Amount"].Value = row.Cells["AmountOriginal"].Value;
            }
        }

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucher>();
                    var f = new FViewVoucherOriginal(_select.CurrencyID, datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID,
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void FRSAssemblyDismantlementDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }
    public class FrmTmpFRSAssemblyDismantlementDetail : DetailBase<RSAssemblyDismantlement>
    {
    }
}