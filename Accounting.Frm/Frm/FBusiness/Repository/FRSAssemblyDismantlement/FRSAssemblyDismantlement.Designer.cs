﻿namespace Accounting
{
    partial class FRSAssemblyDismantlement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.labeReason = new Infragistics.Win.Misc.UltraLabel();
            this.labeDepartment = new Infragistics.Win.Misc.UltraLabel();
            this.labeUnit = new Infragistics.Win.Misc.UltraLabel();
            this.LabeUnitPrice = new Infragistics.Win.Misc.UltraLabel();
            this.labeQuantity = new Infragistics.Win.Misc.UltraLabel();
            this.labeMaterialGoods = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.btnEdit = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridRSAssemblyDismantlement = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl1.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetail)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.cms4Grid.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridRSAssemblyDismantlement)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.labeReason);
            this.ultraTabPageControl1.Controls.Add(this.labeDepartment);
            this.ultraTabPageControl1.Controls.Add(this.labeUnit);
            this.ultraTabPageControl1.Controls.Add(this.LabeUnitPrice);
            this.ultraTabPageControl1.Controls.Add(this.labeQuantity);
            this.ultraTabPageControl1.Controls.Add(this.labeMaterialGoods);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel19);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel13);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel14);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel15);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel16);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel17);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(907, 236);
            // 
            // labeReason
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.labeReason.Appearance = appearance1;
            this.labeReason.Location = new System.Drawing.Point(117, 140);
            this.labeReason.Name = "labeReason";
            this.labeReason.Size = new System.Drawing.Size(749, 21);
            this.labeReason.TabIndex = 19;
            // 
            // labeDepartment
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            this.labeDepartment.Appearance = appearance2;
            this.labeDepartment.Location = new System.Drawing.Point(117, 113);
            this.labeDepartment.Name = "labeDepartment";
            this.labeDepartment.Size = new System.Drawing.Size(735, 25);
            this.labeDepartment.TabIndex = 17;
            // 
            // labeUnit
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            this.labeUnit.Appearance = appearance3;
            this.labeUnit.Location = new System.Drawing.Point(116, 88);
            this.labeUnit.Name = "labeUnit";
            this.labeUnit.Size = new System.Drawing.Size(750, 25);
            this.labeUnit.TabIndex = 16;
            // 
            // LabeUnitPrice
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            this.LabeUnitPrice.Appearance = appearance4;
            this.LabeUnitPrice.Location = new System.Drawing.Point(116, 61);
            this.LabeUnitPrice.Name = "LabeUnitPrice";
            this.LabeUnitPrice.Size = new System.Drawing.Size(756, 23);
            this.LabeUnitPrice.TabIndex = 15;
            // 
            // labeQuantity
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            this.labeQuantity.Appearance = appearance5;
            this.labeQuantity.Location = new System.Drawing.Point(117, 31);
            this.labeQuantity.Name = "labeQuantity";
            this.labeQuantity.Size = new System.Drawing.Size(749, 23);
            this.labeQuantity.TabIndex = 14;
            this.labeQuantity.Text = " ";
            // 
            // labeMaterialGoods
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            this.labeMaterialGoods.Appearance = appearance6;
            this.labeMaterialGoods.Location = new System.Drawing.Point(117, 3);
            this.labeMaterialGoods.Name = "labeMaterialGoods";
            this.labeMaterialGoods.Size = new System.Drawing.Size(749, 23);
            this.labeMaterialGoods.TabIndex = 13;
            // 
            // ultraLabel19
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel19.Appearance = appearance7;
            this.ultraLabel19.Location = new System.Drawing.Point(11, 140);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(123, 21);
            this.ultraLabel19.TabIndex = 11;
            this.ultraLabel19.Text = "Diễn giải            :";
            // 
            // ultraLabel13
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel13.Appearance = appearance8;
            this.ultraLabel13.Location = new System.Drawing.Point(11, 113);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(150, 25);
            this.ultraLabel13.TabIndex = 9;
            this.ultraLabel13.Text = "Phòng ban         :";
            // 
            // ultraLabel14
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel14.Appearance = appearance9;
            this.ultraLabel14.Location = new System.Drawing.Point(11, 88);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(123, 25);
            this.ultraLabel14.TabIndex = 8;
            this.ultraLabel14.Text = "Đơn vi tính         :";
            // 
            // ultraLabel15
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel15.Appearance = appearance10;
            this.ultraLabel15.Location = new System.Drawing.Point(10, 61);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(124, 23);
            this.ultraLabel15.TabIndex = 7;
            this.ultraLabel15.Text = "Tổng tiền           :";
            // 
            // ultraLabel16
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel16.Appearance = appearance11;
            this.ultraLabel16.Location = new System.Drawing.Point(11, 32);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(141, 23);
            this.ultraLabel16.TabIndex = 6;
            this.ultraLabel16.Text = "Số lượng            : ";
            // 
            // ultraLabel17
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel17.Appearance = appearance12;
            this.ultraLabel17.Location = new System.Drawing.Point(12, 3);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(107, 23);
            this.ultraLabel17.TabIndex = 5;
            this.ultraLabel17.Text = "Thành phẩm      :";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridDetail);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(907, 236);
            // 
            // uGridDetail
            // 
            this.uGridDetail.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridDetail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridDetail.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridDetail.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            this.uGridDetail.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGridDetail.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            this.uGridDetail.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGridDetail.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridDetail.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridDetail.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridDetail.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGridDetail.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGridDetail.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGridDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridDetail.Location = new System.Drawing.Point(0, 0);
            this.uGridDetail.Name = "uGridDetail";
            this.uGridDetail.Size = new System.Drawing.Size(907, 236);
            this.uGridDetail.TabIndex = 6;
            this.uGridDetail.Text = "ultraGrid1";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(911, 47);
            this.panel1.TabIndex = 6;
            // 
            // btnAdd
            // 
            appearance13.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.btnAdd.Appearance = appearance13;
            this.btnAdd.Location = new System.Drawing.Point(12, 12);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 30);
            this.btnAdd.TabIndex = 14;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            appearance14.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.btnDelete.Appearance = appearance14;
            this.btnDelete.Location = new System.Drawing.Point(195, 12);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 13;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            appearance15.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.btnEdit.Appearance = appearance15;
            this.btnEdit.Location = new System.Drawing.Point(104, 12);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 30);
            this.btnEdit.TabIndex = 12;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(3, 14);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(123, 23);
            this.ultraLabel1.TabIndex = 0;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(3, 43);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(123, 23);
            this.ultraLabel2.TabIndex = 1;
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Location = new System.Drawing.Point(3, 101);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(124, 23);
            this.ultraLabel3.TabIndex = 2;
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.Location = new System.Drawing.Point(4, 130);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(123, 25);
            this.ultraLabel4.TabIndex = 3;
            // 
            // ultraLabel5
            // 
            this.ultraLabel5.Location = new System.Drawing.Point(4, 161);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(124, 25);
            this.ultraLabel5.TabIndex = 4;
            // 
            // ultraLabel10
            // 
            this.ultraLabel10.Location = new System.Drawing.Point(551, 14);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(118, 23);
            this.ultraLabel10.TabIndex = 5;
            // 
            // ultraLabel9
            // 
            this.ultraLabel9.Location = new System.Drawing.Point(551, 43);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(118, 23);
            this.ultraLabel9.TabIndex = 6;
            // 
            // ultraLabel8
            // 
            this.ultraLabel8.Location = new System.Drawing.Point(551, 72);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(118, 23);
            this.ultraLabel8.TabIndex = 7;
            // 
            // ultraLabel7
            // 
            this.ultraLabel7.Location = new System.Drawing.Point(551, 101);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(118, 25);
            this.ultraLabel7.TabIndex = 8;
            // 
            // ultraLabel6
            // 
            this.ultraLabel6.Location = new System.Drawing.Point(551, 132);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(118, 25);
            this.ultraLabel6.TabIndex = 9;
            // 
            // ultraLabel12
            // 
            this.ultraLabel12.Location = new System.Drawing.Point(5, 70);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(123, 25);
            this.ultraLabel12.TabIndex = 20;
            this.ultraLabel12.Text = "Điện thoại : ";
            // 
            // ultraLabel11
            // 
            this.ultraLabel11.Location = new System.Drawing.Point(134, 70);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(225, 25);
            this.ultraLabel11.TabIndex = 21;
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 251);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(911, 262);
            this.ultraTabControl1.TabIndex = 8;
            ultraTab4.TabPage = this.ultraTabPageControl1;
            ultraTab4.Text = "1 Thành phần";
            ultraTab5.TabPage = this.ultraTabPageControl2;
            ultraTab5.Text = "2 Chi tiết";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab4,
            ultraTab5});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(907, 236);
            // 
            // cms4Grid
            // 
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tsmAdd,
            this.tsmEdit,
            this.tsmDelete,
            this.tmsReLoad});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(150, 98);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(146, 6);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.Size = new System.Drawing.Size(149, 22);
            this.tsmAdd.Text = "Thêm";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmEdit
            // 
            this.tsmEdit.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.tsmEdit.Name = "tsmEdit";
            this.tsmEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.tsmEdit.Size = new System.Drawing.Size(149, 22);
            this.tsmEdit.Text = "Sửa";
            this.tsmEdit.Click += new System.EventHandler(this.tsmEdit_Click);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.Size = new System.Drawing.Size(149, 22);
            this.tsmDelete.Text = "Xóa";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // tmsReLoad
            // 
            this.tmsReLoad.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tmsReLoad.Name = "tmsReLoad";
            this.tmsReLoad.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.tmsReLoad.Size = new System.Drawing.Size(149, 22);
            this.tmsReLoad.Text = "Tải Lại";
            this.tmsReLoad.Click += new System.EventHandler(this.tmsReLoad_Click);
            this.tmsReLoad.DoubleClick += new System.EventHandler(this.tmsReLoad_Click);
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.uGridRSAssemblyDismantlement);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraTabControl1);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 47);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(911, 513);
            this.ultraPanel1.TabIndex = 9;
            // 
            // uGridRSAssemblyDismantlement
            // 
            this.uGridRSAssemblyDismantlement.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridRSAssemblyDismantlement.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridRSAssemblyDismantlement.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridRSAssemblyDismantlement.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridRSAssemblyDismantlement.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGridRSAssemblyDismantlement.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGridRSAssemblyDismantlement.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGridRSAssemblyDismantlement.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridRSAssemblyDismantlement.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridRSAssemblyDismantlement.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridRSAssemblyDismantlement.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridRSAssemblyDismantlement.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGridRSAssemblyDismantlement.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGridRSAssemblyDismantlement.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGridRSAssemblyDismantlement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridRSAssemblyDismantlement.Location = new System.Drawing.Point(0, 0);
            this.uGridRSAssemblyDismantlement.Name = "uGridRSAssemblyDismantlement";
            this.uGridRSAssemblyDismantlement.Size = new System.Drawing.Size(911, 251);
            this.uGridRSAssemblyDismantlement.TabIndex = 5;
            this.uGridRSAssemblyDismantlement.Text = "uGrid";
            this.uGridRSAssemblyDismantlement.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.uGrid_AfterSelectChange);
            this.uGridRSAssemblyDismantlement.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid_DoubleClickRow);
            this.uGridRSAssemblyDismantlement.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uGrid_MouseDown);
            // 
            // FRSAssemblyDismantlement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 560);
            this.Controls.Add(this.ultraPanel1);
            this.Controls.Add(this.panel1);
            this.Name = "FRSAssemblyDismantlement";
            this.Text = "Lắp ráp, tháo dỡ";
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetail)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.cms4Grid.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridRSAssemblyDismantlement)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.Misc.UltraLabel labeReason;
        private Infragistics.Win.Misc.UltraLabel labeDepartment;
        private Infragistics.Win.Misc.UltraLabel labeUnit;
        private Infragistics.Win.Misc.UltraLabel LabeUnitPrice;
        private Infragistics.Win.Misc.UltraLabel labeQuantity;
        private Infragistics.Win.Misc.UltraLabel labeMaterialGoods;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.Misc.UltraButton btnAdd;
        private Infragistics.Win.Misc.UltraButton btnDelete;
        private Infragistics.Win.Misc.UltraButton btnEdit;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmEdit;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private System.Windows.Forms.ToolStripMenuItem tmsReLoad;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDetail;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridRSAssemblyDismantlement;
    }
}