﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using Infragistics.Win.AppStyling;
using System.Linq;
using Infragistics.Win;
namespace Accounting
{
    public partial class FRSAssemblyDismantlement : CustormForm
    {
        #region Khai Báo
        public readonly IRSAssemblyDismantlementService _IRSAssemblyDismantlementService;
        public readonly IRSAssemblyDismantlementDetailService _IRSAssemblyDismantlementDetailService;
        public readonly ITypeService _ITypeService;
        public readonly IMaterialGoodsService _IMaterialGoodsService;
        public readonly IRepositoryService _IRepositoryService;
        public readonly IAccountingObjectService _IAccountingObjectService;
        List<RSAssemblyDismantlement> listIRSAssemblyDismantlement = new List<RSAssemblyDismantlement>();
        List<RSAssemblyDismantlementDetail> listIRSAssemblyDismantlementDetail = new List<RSAssemblyDismantlementDetail>();
        List<AccountingObject> listAccounObject = new List<AccountingObject>();
        List<Accounting.Core.Domain.Type> listType = new List<Accounting.Core.Domain.Type>();
        public static Dictionary<string, Template> DsTemplate = new Dictionary<string, Template>();
        public static bool isClose = true;
        #endregion
        #region Khởi tạo
        public FRSAssemblyDismantlement()
        {
            InitializeComponent();
            this.CenterToScreen();
            _IRSAssemblyDismantlementService = IoC.Resolve<IRSAssemblyDismantlementService>();
            _IRSAssemblyDismantlementDetailService = IoC.Resolve<IRSAssemblyDismantlementDetailService>();
            _IRepositoryService = IoC.Resolve<IRepositoryService>();
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            _ITypeService = IoC.Resolve<ITypeService>();
            this.uGridRSAssemblyDismantlement.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            LoadDulieu();
        }
        #endregion
        #region Hàm
        void LoadDulieu()
        {
            LoadDuLieu(true);
        }

        private void LoadDuLieu(bool configGrid)
        {

            #region Lấy dữ liệu từ CSDL
            listIRSAssemblyDismantlement = _IRSAssemblyDismantlementService.getAllOrderbyDate();
            // listAccounObject = _IAccountingObjectService.GetAll();
            listIRSAssemblyDismantlementDetail = _IRSAssemblyDismantlementDetailService.GetAll();
            listType = _ITypeService.GetAll();
            #endregion
            #region Xử lý dữ liệu
            #endregion
            #region hiển thị và xử lý hiển thị
            // uGridRSAssemblyDismantlement.DataSource = listIRSAssemblyDismantlement.Where(p => p.TypeID == 420).ToArray();
            uGridRSAssemblyDismantlement.DataSource = listIRSAssemblyDismantlement.OrderByDescending(x => x.Date).ToList();
            if (uGridRSAssemblyDismantlement.Rows.Count > 0)
            {
                uGridRSAssemblyDismantlement.Rows[0].Selected = true;
            }
            foreach (var item in listIRSAssemblyDismantlement)
            {
                if (item.TypeID != null) item.TypeIDViewNameCode = listType.Where(p => p.ID == item.TypeID).SingleOrDefault().TypeName;
            }
            //uGridRSAssemblyDismantlement.Selected.Rows[0].Cells[0].Value.ToString();
            if (configGrid) ConfigGrid(uGridRSAssemblyDismantlement);

            #endregion
        }
        #endregion
        #region Nghiệp vụ
        /// Nghiệp vụ Update
        void editFunction()
        {
            if (uGridRSAssemblyDismantlement.Selected.Rows.Count > 0)
            {
                Core.Domain.RSAssemblyDismantlement temp = uGridRSAssemblyDismantlement.Selected.Rows[0].ListObject as Core.Domain.RSAssemblyDismantlement;
                new FRSAssemblyDismantlementDetail(temp, listIRSAssemblyDismantlement, ConstFrm.optStatusForm.View).ShowDialog(this);
                if (!FRSTransferDetail.IsClose) LoadDulieu();

            }
            else
                Accounting.TextMessage.MSG.Error(Accounting.TextMessage.resSystem.MSG_System_04);
        }
        void addFunction()
        {
            RSAssemblyDismantlement temp = uGridRSAssemblyDismantlement.Selected.Rows.Count <= 0 ? new RSAssemblyDismantlement() : uGridRSAssemblyDismantlement.Selected.Rows[0].ListObject as RSAssemblyDismantlement;
            new FRSAssemblyDismantlementDetail(temp, listIRSAssemblyDismantlement, ConstFrm.optStatusForm.Add).ShowDialog(this);
            if (!FRSAssemblyDismantlementDetail.IsClose) LoadDulieu();
        }
        void deleteFunction()
        {
            if (uGridRSAssemblyDismantlement.Selected.Rows.Count > 0)
            {
                RSAssemblyDismantlement temp = uGridRSAssemblyDismantlement.Selected.Rows[0].ListObject as RSAssemblyDismantlement;
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.MaterialGoodsName)) == System.Windows.Forms.DialogResult.Yes)
                {
                   // List<bool> list = _IRSAssemblyDismantlementService.Query.Select(p => p.Recorded).ToList();
                    List<bool> list = _IRSAssemblyDismantlementService.GetByRecorded();
                    // đang ghi sổ ko dc xóa
                    if (temp.Recorded == true)
                    {
                        MSG.Error("Chứng từ đang ghi sổ không được phép xóa");
                        return;
                    }
                    _IRSAssemblyDismantlementService.BeginTran();
                    _IRSAssemblyDismantlementService.Delete(temp);
                    _IRSAssemblyDismantlementService.CommitTran();

                    LoadDulieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion
        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(uGridRSAssemblyDismantlement, TextMessage.ConstDatabase.RSAssemblyDismantlement_TableName);
            //uGridRSAssemblyDismantlement.DisplayLayout.Bands[0].Columns["IWPostedDate"].Format = Constants.DdMMyyyy;
            //uGridRSAssemblyDismantlement.DisplayLayout.Bands[0].Columns["IWDate"].Format = Constants.DdMMyyyy;
            //uGridRSAssemblyDismantlement.DisplayLayout.Bands[0].Columns["OWPostedDate"].Format = Constants.DdMMyyyy;
            //uGridRSAssemblyDismantlement.DisplayLayout.Bands[0].Columns["OWDate"].Format = Constants.DdMMyyyy;
        }


        #endregion
        #region Event
        private void btnAdd_Click(object sender, EventArgs e)
        {
            addFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            editFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            deleteFunction();
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            addFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            editFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            deleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            LoadDulieu();
        }

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGridRSAssemblyDismantlement, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index == -1) return;
            editFunction();
        }

        private void uGrid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {

            if (uGridRSAssemblyDismantlement.Selected.Rows.Count > 0)
            {

                if (uGridRSAssemblyDismantlement.Selected.Rows.Count < 1) return;
                object listObject = uGridRSAssemblyDismantlement.Selected.Rows[0].ListObject;
                if (listObject == null) return;
                RSAssemblyDismantlement mcRSAssemblyDismantlement = listObject as RSAssemblyDismantlement;
                if (mcRSAssemblyDismantlement == null) return;


                //labePostedDate.Text = temp.PostedDate.ToString("dd/MM/yyyy");
                //labeNo.Text = temp.No;
                //LabeDate.Text = temp.Date.ToString("dd/MM/yyyy");

                labeMaterialGoods.Text = mcRSAssemblyDismantlement.MaterialGoodsName;
                labeQuantity.Text = mcRSAssemblyDismantlement.Quantity.ToString();
                LabeUnitPrice.Text = mcRSAssemblyDismantlement.Amount.ToString();
                labeUnit.Text = mcRSAssemblyDismantlement.Unit;
                //labeDepartment.Text = mcRSAssemblyDismantlement.IWDepartment;
                //labeReason.Text = mcRSAssemblyDismantlement.IWReason;

                if (mcRSAssemblyDismantlement.RSAssemblyDismantlementDetails.Count == 0) mcRSAssemblyDismantlement.RSAssemblyDismantlementDetails = new List<RSAssemblyDismantlementDetail>();
                uGridDetail.DataSource = mcRSAssemblyDismantlement.RSAssemblyDismantlementDetails;    //Utils.CloneObject(mcPayment.MCPaymentDetails);


                #region Config Grid
                //hiển thị 1 band
                // uGridDetail.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
                //tắt lọc cột
                uGridDetail.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
                uGridDetail.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
                //tự thay đổi kích thước cột
                uGridDetail.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                //tắt tiêu đề
                uGridDetail.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
                //select cả hàng hay ko?
                uGridDetail.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
                uGridDetail.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;

                //Hiện những dòng trống?
                uGridDetail.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
                uGridDetail.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
                //Fix Header
                uGridDetail.DisplayLayout.UseFixedHeaders = true;

                UltraGridBand band = uGridDetail.DisplayLayout.Bands[0];
                band.Summaries.Clear();

                Template mauGiaoDien = GetMauGiaoDien(mcRSAssemblyDismantlement.TypeID, mcRSAssemblyDismantlement.TemplateID, DsTemplate);

                Utils.ConfigGrid(uGridDetail, ConstDatabase.RSInwardOutwardDetail_TableName, mauGiaoDien == null ? new List<TemplateColumn>() : mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 0).TemplateColumns, 0);
                //Thêm tổng số ở cột "Số tiền"
                SummarySettings summary = band.Summaries.Add("SumAmountOriginal", SummaryType.Sum, band.Columns["AmountOriginal"]);
                summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                summary.DisplayFormat = "{0:N0}";
                summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
                #endregion
                foreach (var item in band.Columns)
                {
                    if (item.Key.Equals("MaterialGoodsID"))
                    {
                        //List<MaterialGoods> MaterialGoodsItems = _IMaterialGoodsService.GetAll().OrderBy(p => p.MaterialGoodsCode).Where(p => p.IsActive == true).ToList(); ;
                        List<MaterialGoods> MaterialGoodsItems = _IMaterialGoodsService.GetByMateriaGoodCode(true);
                        item.Editor = this.CreateCombobox(MaterialGoodsItems, ConstDatabase.MaterialGoods_TableName, "ID", "MaterialGoodsCode");

                    }
                    else if (item.Key.Equals("RepositoryID"))
                    {
                       // List<Repository> Repositorys = _IRepositoryService.GetAll().OrderBy(p => p.RepositoryCode).Where(p => p.IsActive == true).ToList(); 
                        List<Repository> Repositorys = _IRepositoryService.GetByRepositoryCode(true);
                        item.Editor = this.CreateCombobox(Repositorys, ConstDatabase.Repository_TableName, "ID", "RepositoryCode");

                    }
                }
            }
        }
        #endregion
        public static Template GetMauGiaoDien(int typeId, Guid? templateId, Dictionary<string, Template> dsTemplate)    //out string keyofdsTemplate
        {
            string keyofdsTemplate = string.Format("{0}@{1}", typeId, templateId);
            if (!dsTemplate.Keys.Contains(keyofdsTemplate))
            {
                //Note: các chứng từ được sinh tự động từ nghiệp vụ khác thì phải truyền TypeID của nghiệp vụ khác đó
                int typeIdTemp = GetTypeIdRef(typeId);
                Template template = Utils.GetTemplateInDatabase(templateId, typeIdTemp);
                dsTemplate.Add(keyofdsTemplate, template);
                return template;
            }
            //keyofdsTemplate = string.Empty;
            return dsTemplate[keyofdsTemplate];
        }
        static int GetTypeIdRef(int typeId)
        {
            return typeId;
        }

    }
}
