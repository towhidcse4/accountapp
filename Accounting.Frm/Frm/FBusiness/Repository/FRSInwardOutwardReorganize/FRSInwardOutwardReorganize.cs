﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Drawing;

namespace Accounting
{
    public partial class FRSInwardOutwardReorganize : CustormForm
    {
        public IRepositoryService _IRepositoryService { get { return IoC.Resolve<IRepositoryService>(); } }
        public IRSInwardOutwardService _IRSInwardOutwardService { get { return IoC.Resolve<IRSInwardOutwardService>(); } }
        List<RSInwardOutward> _listRSIOVouchers = new List<RSInwardOutward>();
        List<RSInwardOutward> _listRSIOVouchersOriginal = new List<RSInwardOutward>();
        public FRSInwardOutwardReorganize()
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();

            inputPostedDate.DateTime = DateTime.Today;
            
            uGridRSIOVouchers.SetDataBinding(_listRSIOVouchers, "");

            Utils.ConfigGrid(uGridRSIOVouchers, ConstDatabase.RSInwardOutwardUpdate_TableName);

            uGridRSIOVouchers.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGridRSIOVouchers.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGridRSIOVouchers.DisplayLayout.Override.SelectedRowAppearance.BackColor = Color.White;
            uGridRSIOVouchers.DisplayLayout.Override.HotTrackRowAppearance.ForeColor = Color.White;
            uGridRSIOVouchers.DisplayLayout.Override.HotTrackRowSelectorAppearance.BackColor = Color.FromArgb(51, 153, 255);
            uGridRSIOVouchers.DisplayLayout.Override.HotTrackRowCellAppearance.BackColor = Color.FromArgb(51, 153, 255);
            uGridRSIOVouchers.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            //uGridRSIOVouchers.BeforeCellUpdate += new BeforeCellUpdateEventHandler(uGridRSIOVouchers_BeforCellUpdate);
            uGridRSIOVouchers.AfterCellUpdate += new CellEventHandler(uGridRSIOVouchers_AfterCellUpdate);
            //uGridSelectMG.DisplayLayout.Bands[0].Summaries.Clear();
            uGridRSIOVouchers.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            UltraGridBand band = uGridRSIOVouchers.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["RefDateTime"];
            //ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            //ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            //ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            //ugc.Header.Fixed = true;
            Utils.FormatNumberic(band.Columns["TotalAmountOriginal"], ConstDatabase.Format_TienVND);
            //_listrepos = new List<Repository>();
            //_listrepos = _IRepositoryService.GetAll_ByIsActive(true);

            WaitingFrm.StopWaiting();

        }

        private void btnKetThuc_Click(object sender, EventArgs e)
        {
            _IRSInwardOutwardService.UnbindSession(_listRSIOVouchers);
            Close();
        }

        private void btnThucHien_Click(object sender, EventArgs e)
        {
            _IRSInwardOutwardService.BeginTran();
            try
            {
                foreach (RSInwardOutward item in _listRSIOVouchers)
                {
                    var original = _listRSIOVouchersOriginal.FirstOrDefault(o => o.ID == item.ID);
                    if (!original.Equals(item))
                    {
                        _IRSInwardOutwardService.Update(item);
                    }
                }
                _IRSInwardOutwardService.CommitTran();
                MSG.Information("Cập nhật dữ liệu thành công!");
            }
            catch(Exception ex)
            {
                _IRSInwardOutwardService.RolbackTran();
                MSG.Information("Cập nhật dữ liệu thất bại. Xin vui lòng thử lại!");
            }
        }
        private void uGridRSIOVouchers_BeforCellUpdate(object sender, BeforeCellUpdateEventArgs e)
        {
            UltraGrid grid = sender as UltraGrid;
            RSInwardOutward item = grid.ActiveRow.ListObject as RSInwardOutward;
            DateTime refDate = item.RefDateTime ?? DateTime.Now;
            if (item.PostedDate.ToString("ddMMyyyy").Equals(refDate.ToString("ddMMyyyy")))
            {
                if(e.NewValue is DateTime)
                {
                    DateTime change = (DateTime)e.NewValue;
                    DateTime result = new DateTime(item.PostedDate.Year, item.PostedDate.Month, item.PostedDate.Day, change.Hour, change.Minute, change.Millisecond);
                    e.SetProperty("NewValue", result);
                }
            }

            //_listRSIOVouchers = _listRSIOVouchers.OrderBy(o => o.RefDateTime).ToList();
            //uGridRSIOVouchers.DataSource = _listRSIOVouchers;
            //uGridRSIOVouchers.UpdateData();
        }
        private void uGridRSIOVouchers_AfterCellUpdate(object sender, CellEventArgs e)
        {
            UltraGrid grid = sender as UltraGrid;
            RSInwardOutward item = grid.ActiveRow.ListObject as RSInwardOutward;
            DateTime refDate = item.RefDateTime ?? DateTime.Now;
            RSInwardOutward original = _listRSIOVouchersOriginal.FirstOrDefault(o => o.ID == item.ID);
            if (!original.PostedDate.ToString("ddMMyyyy").Equals(refDate.ToString("ddMMyyyy")))
            {
                DateTime result = new DateTime(item.PostedDate.Year, item.PostedDate.Month, item.PostedDate.Day, refDate.Hour, refDate.Minute, refDate.Millisecond);
                int index = _listRSIOVouchers.IndexOf(item);
                _listRSIOVouchers[index].RefDateTime = result;
            }

            _listRSIOVouchers = _listRSIOVouchers.OrderBy(o => o.RefDateTime).ToList();
            uGridRSIOVouchers.DataSource = _listRSIOVouchers;
            uGridRSIOVouchers.UpdateData();
        }
        /// <summary>
        /// Lấy dữ liệu phiếu xuất nhập kho theo ngày
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGetData_Click(object sender, EventArgs e)
        {
            DateTime startDay = inputPostedDate.DateTime;
            DateTime endDay = startDay.AddDays(1);
            _IRSInwardOutwardService.UnbindSession(_listRSIOVouchers);
            _listRSIOVouchers = _IRSInwardOutwardService.Query.Where(o => o.Recorded == true && o.PostedDate >= startDay && o.PostedDate < endDay).OrderBy(o => o.RefDateTime).ToList();
            uGridRSIOVouchers.DataSource = _listRSIOVouchers;
            _listRSIOVouchersOriginal = _listRSIOVouchers;
            uGridRSIOVouchers.UpdateData();
        }
        /// <summary>
        /// Chuyển phiếu xuất nhập kho lên trên
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDownRow_Click(object sender, EventArgs e)
        {
            if(uGridRSIOVouchers.ActiveRow != null && uGridRSIOVouchers.ActiveRow.ListObject != null)
            {
                RSInwardOutward item = uGridRSIOVouchers.ActiveRow.ListObject as RSInwardOutward;
                int index = _listRSIOVouchers.IndexOf(item);
                if (index == _listRSIOVouchers.Count - 1) return;

                RSInwardOutward nextItem = _listRSIOVouchers[index + 1];
                DateTime nextRefDateTime = nextItem.RefDateTime ?? inputPostedDate.DateTime;
                _listRSIOVouchers[index].RefDateTime = nextRefDateTime.AddSeconds(1);

                _listRSIOVouchers = _listRSIOVouchers.OrderBy(o => o.RefDateTime).ToList();
                uGridRSIOVouchers.DataSource = _listRSIOVouchers;
                uGridRSIOVouchers.UpdateData();
            }
        }

        private void btnUpRow_Click(object sender, EventArgs e)
        {
            if (uGridRSIOVouchers.ActiveRow != null && uGridRSIOVouchers.ActiveRow.ListObject != null)
            {
                RSInwardOutward item = uGridRSIOVouchers.ActiveRow.ListObject as RSInwardOutward;
                int index = _listRSIOVouchers.IndexOf(item);
                if (index == 0) return;

                RSInwardOutward nextItem = _listRSIOVouchers[index - 1];
                if(nextItem.RefDateTime != null)
                {
                    DateTime nextRefDateTime = nextItem.RefDateTime ?? inputPostedDate.DateTime;
                    _listRSIOVouchers[index].RefDateTime = nextRefDateTime.AddSeconds(-1);

                    _listRSIOVouchers = _listRSIOVouchers.OrderBy(o => o.RefDateTime).ToList();
                    uGridRSIOVouchers.DataSource = _listRSIOVouchers;
                    uGridRSIOVouchers.UpdateData();
                }
                else
                {
                    MSG.Warning(string.Format("Cần xác định thời gian xuất, nhập kho của chứng từ {0} trước khi sắp xếp lại.", nextItem.No));
                }
            }
        }

        private void uGridRSIOVouchers_CellDataError(object sender, CellDataErrorEventArgs e)
        {
            // CellDataError gets fired when the user attempts to exit the edit mode
            // after entering an invalid value in the cell. There are several properties
            // on the passed in event args that you can set to control the UltraGrid's
            // behaviour.

            // Typically ForceExit is false. The UltraGrid forces exits on cells under
            // circumstances like when it's being disposed of. If ForceExit is true, then 
            // the UltraGrid will ignore StayInEditMode property and exit the cell 
            // restoring the original value ignoring the value you set to StayInEditMode
            // property.
            if (!e.ForceExit)
            {
                // Default for StayInEditMode is true. However you can set it to false to
                // cause the grid to exit the edit mode and restore the original value. We
                // will just leave it true for this example.
                e.StayInEditMode = true;

                // Set the RaiseErrorEvent to false to prevent the grid from raising 
                // the error event and displaying any message.
                e.RaiseErrorEvent = false;

                // Instead display our own message.
                if (this.uGridRSIOVouchers.ActiveCell.Column.DataType != typeof(DateTime))
                {
                    MessageBox.Show(this, "Vui lòng nhập giờ hợp lệ.", "Đầu vào không hợp lệ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                //else if (this.uGrid.ActiveCell.Column.DataType == typeof(decimal))
                //{
                //    MessageBox.Show(this, "Please enter a valid numer.", "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //}
            }
            else
            {
                // Set the RaiseErrorEvent to false to prevent the grid from raising 
                // the error event.
                e.RaiseErrorEvent = false;
            }
        }
    }
}
