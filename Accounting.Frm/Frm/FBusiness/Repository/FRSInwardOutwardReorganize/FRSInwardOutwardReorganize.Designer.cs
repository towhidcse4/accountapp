﻿namespace Accounting
{
    partial class FRSInwardOutwardReorganize
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRSInwardOutwardReorganize));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            this.ultraPanelBottom = new Infragistics.Win.Misc.UltraPanel();
            this.uGridRSIOVouchers = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.btnKetThuc = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanelCenter = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBoxBottom = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnDownRow = new Infragistics.Win.Misc.UltraButton();
            this.btnUpRow = new Infragistics.Win.Misc.UltraButton();
            this.btnGetData = new Infragistics.Win.Misc.UltraButton();
            this.inputPostedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.btnThucHien = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanelBottom.ClientArea.SuspendLayout();
            this.ultraPanelBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridRSIOVouchers)).BeginInit();
            this.ultraPanelCenter.ClientArea.SuspendLayout();
            this.ultraPanelCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxBottom)).BeginInit();
            this.ultraGroupBoxBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputPostedDate)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraPanelBottom
            // 
            // 
            // ultraPanelBottom.ClientArea
            // 
            this.ultraPanelBottom.ClientArea.Controls.Add(this.uGridRSIOVouchers);
            resources.ApplyResources(this.ultraPanelBottom, "ultraPanelBottom");
            this.ultraPanelBottom.Name = "ultraPanelBottom";
            // 
            // uGridRSIOVouchers
            // 
            resources.ApplyResources(this.uGridRSIOVouchers, "uGridRSIOVouchers");
            this.uGridRSIOVouchers.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridRSIOVouchers.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridRSIOVouchers.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridRSIOVouchers.Name = "uGridRSIOVouchers";
            this.uGridRSIOVouchers.CellDataError += new Infragistics.Win.UltraWinGrid.CellDataErrorEventHandler(this.uGridRSIOVouchers_CellDataError);
            // 
            // btnKetThuc
            // 
            appearance1.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnKetThuc.Appearance = appearance1;
            resources.ApplyResources(this.btnKetThuc, "btnKetThuc");
            this.btnKetThuc.Name = "btnKetThuc";
            this.btnKetThuc.Click += new System.EventHandler(this.btnKetThuc_Click);
            // 
            // ultraPanelCenter
            // 
            // 
            // ultraPanelCenter.ClientArea
            // 
            this.ultraPanelCenter.ClientArea.Controls.Add(this.ultraGroupBoxBottom);
            resources.ApplyResources(this.ultraPanelCenter, "ultraPanelCenter");
            this.ultraPanelCenter.Name = "ultraPanelCenter";
            // 
            // ultraGroupBoxBottom
            // 
            appearance2.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraGroupBoxBottom.Appearance = appearance2;
            this.ultraGroupBoxBottom.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rounded;
            this.ultraGroupBoxBottom.Controls.Add(this.btnDownRow);
            this.ultraGroupBoxBottom.Controls.Add(this.btnUpRow);
            this.ultraGroupBoxBottom.Controls.Add(this.btnGetData);
            this.ultraGroupBoxBottom.Controls.Add(this.inputPostedDate);
            this.ultraGroupBoxBottom.Controls.Add(this.ultraLabel2);
            resources.ApplyResources(this.ultraGroupBoxBottom, "ultraGroupBoxBottom");
            this.ultraGroupBoxBottom.Name = "ultraGroupBoxBottom";
            // 
            // btnDownRow
            // 
            appearance3.Image = global::Accounting.Properties.Resources.down;
            this.btnDownRow.Appearance = appearance3;
            resources.ApplyResources(this.btnDownRow, "btnDownRow");
            this.btnDownRow.Name = "btnDownRow";
            this.btnDownRow.Click += new System.EventHandler(this.btnDownRow_Click);
            // 
            // btnUpRow
            // 
            appearance4.Image = global::Accounting.Properties.Resources.up;
            this.btnUpRow.Appearance = appearance4;
            resources.ApplyResources(this.btnUpRow, "btnUpRow");
            this.btnUpRow.Name = "btnUpRow";
            this.btnUpRow.Click += new System.EventHandler(this.btnUpRow_Click);
            // 
            // btnGetData
            // 
            appearance5.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnGetData.Appearance = appearance5;
            resources.ApplyResources(this.btnGetData, "btnGetData");
            this.btnGetData.Name = "btnGetData";
            this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
            // 
            // inputPostedDate
            // 
            resources.ApplyResources(this.inputPostedDate, "inputPostedDate");
            this.inputPostedDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.inputPostedDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.inputPostedDate.MaskInput = "";
            this.inputPostedDate.Name = "inputPostedDate";
            this.inputPostedDate.Value = null;
            // 
            // ultraLabel2
            // 
            resources.ApplyResources(this.ultraLabel2, "ultraLabel2");
            this.ultraLabel2.Name = "ultraLabel2";
            // 
            // btnThucHien
            // 
            appearance6.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnThucHien.Appearance = appearance6;
            resources.ApplyResources(this.btnThucHien, "btnThucHien");
            this.btnThucHien.Name = "btnThucHien";
            this.btnThucHien.Click += new System.EventHandler(this.btnThucHien_Click);
            // 
            // FRSInwardOutwardReorganize
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ultraPanelBottom);
            this.Controls.Add(this.btnKetThuc);
            this.Controls.Add(this.ultraPanelCenter);
            this.Controls.Add(this.btnThucHien);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FRSInwardOutwardReorganize";
            this.TopMost = true;
            this.ultraPanelBottom.ClientArea.ResumeLayout(false);
            this.ultraPanelBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridRSIOVouchers)).EndInit();
            this.ultraPanelCenter.ClientArea.ResumeLayout(false);
            this.ultraPanelCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxBottom)).EndInit();
            this.ultraGroupBoxBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.inputPostedDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Infragistics.Win.Misc.UltraPanel ultraPanelCenter;
        private Infragistics.Win.Misc.UltraPanel ultraPanelBottom;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxBottom;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor inputPostedDate;
        private Infragistics.Win.Misc.UltraButton btnKetThuc;
        private Infragistics.Win.Misc.UltraButton btnThucHien;
        private Infragistics.Win.Misc.UltraButton btnGetData;
        private Infragistics.Win.Misc.UltraButton btnDownRow;
        private Infragistics.Win.Misc.UltraButton btnUpRow;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridRSIOVouchers;
    }
}