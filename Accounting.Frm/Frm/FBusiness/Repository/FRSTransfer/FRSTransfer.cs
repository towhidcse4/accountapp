﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using Infragistics.Win.AppStyling;
using System.Linq;
using Infragistics.Win;
namespace Accounting
{
    public partial class FRSTransfer : CustormForm
    {
        #region Khai Báo
        public readonly IRSTransferService _IRSTransferService;
        public readonly ITypeService _ITypeService;
        public readonly IRSTransferDetailService _IRSTransferDetailService;
        public readonly IAccountingObjectService _IAccountingObjectService;
        List<RSTransfer> listRSTransfer = new List<RSTransfer>();
        public readonly IMaterialGoodsService _IMaterialGoodsService;
        public readonly IRepositoryService _IRepositoryService;
        List<RSTransferDetail> listRSTransferDetail = new List<RSTransferDetail>();
        List<AccountingObject> listAccounObject = new List<AccountingObject>();
        List<Accounting.Core.Domain.Type> listType = new List<Accounting.Core.Domain.Type>();
        public static Dictionary<string, Template> DsTemplate = new Dictionary<string, Template>();
      
        public static bool isClose = true;
        #endregion
        #region Khởi tạo
        public FRSTransfer()
        
        {
            InitializeComponent();
            this.CenterToScreen();
            _IRSTransferService = IoC.Resolve<IRSTransferService>();
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            _IRSTransferDetailService = IoC.Resolve<IRSTransferDetailService>();
            _IRepositoryService = IoC.Resolve<IRepositoryService>();
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            _ITypeService = IoC.Resolve<ITypeService>();
            this.uGridTransfer.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            LoadDulieu();
        }
        #endregion
        #region Hàm 
        void LoadDulieu()
        {
            LoadDuLieu(true);
        }
    
        private void LoadDuLieu(bool configGrid)
        {
          
            #region Lấy dữ liệu từ CSDL
            listRSTransfer = _IRSTransferService.GetAll();
            listAccounObject = _IAccountingObjectService.GetAll();
            listRSTransferDetail = _IRSTransferDetailService.GetAll();
            listType = _ITypeService.GetAll();
            #endregion
            #region Xử lý dữ liệu
            #endregion
            #region hiển thị và xử lý hiển thị
            uGridTransfer.DataSource = listRSTransfer.Where(p=>p.TypeID==420).ToArray();
            if (uGridTransfer.Rows.Count > 0)
            {
                uGridTransfer.Rows[0].Selected = true;
            }
            foreach (var item in listRSTransfer)
            {
                if (item.TypeID != null) item.TypeIDViewNameCode = listType.Where(p => p.ID == item.TypeID).SingleOrDefault().TypeName;
            }
            //uGrid.Selected.Rows[0].Cells[0].Value.ToString();
            if (configGrid) ConfigGrid(uGridTransfer);
           
            #endregion
        }
        #endregion
        #region Nghiệp vụ
        /// Nghiệp vụ Update
        void editFunction()
        {
            if (uGridTransfer.Selected.Rows.Count > 0)
            {
                RSTransfer temp = uGridTransfer.Selected.Rows[0].ListObject as RSTransfer;
                new FRSTransferDetail(temp, listRSTransfer, ConstFrm.optStatusForm.View).ShowDialog(this);
                if (!FRSTransferDetail.IsClose) LoadDulieu();

            }
            else
                Accounting.TextMessage.MSG.Error(Accounting.TextMessage.resSystem.MSG_System_04);
        }
        void addFunction()
        {
            RSTransfer temp = uGridTransfer.Selected.Rows.Count <= 0 ? new RSTransfer() : uGridTransfer.Selected.Rows[0].ListObject as RSTransfer;
            new FRSTransferDetail(temp, listRSTransfer, ConstFrm.optStatusForm.Add).ShowDialog(this);
            if (!FRSTransferDetail.IsClose) LoadDulieu();
        }
        void deleteFunction()
        {
            if (uGridTransfer.Selected.Rows.Count > 0)
            {
                RSTransfer temp = uGridTransfer.Selected.Rows[0].ListObject as RSTransfer;
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.No)) == System.Windows.Forms.DialogResult.Yes)
                {
                    // đang ghi sổ ko dc xóa
                    if (temp.Recorded == true)
                    {
                        MSG.Error("Chứng từ đang ghi sổ không được phép xóa");
                        return;
                    }
                    _IRSTransferService.BeginTran();
                    _IRSTransferService.Delete(temp);
                    _IRSTransferService.CommitTran();

                    LoadDulieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion
        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(uGridTransfer, TextMessage.ConstDatabase.RSTransfer_TableName);
            //uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Format = Constants.DdMMyyyy;
            //uGrid.DisplayLayout.Bands[0].Columns["Date"].Format = Constants.DdMMyyyy;
        }

        
        #endregion
        #region Event
        private void btnAdd_Click(object sender, EventArgs e)
        {
            addFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            editFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            deleteFunction();
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            addFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            editFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            deleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            LoadDulieu();
        }

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGridTransfer, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index == -1) return;
            editFunction();
        }

        private void uGrid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
               if (uGridTransfer.Selected.Rows.Count > 0)
            {

                if (uGridTransfer.Selected.Rows.Count < 1) return;
                object listObject = uGridTransfer.Selected.Rows[0].ListObject;
                if (listObject == null) return;
                RSTransfer mcRSTransfer = listObject as RSTransfer;
                if (mcRSTransfer == null) return;

                //RSTransfer temp = uGridTransfer.Selected.Rows[0].ListObject as RSTransfer;

                labePostedDate.Text = mcRSTransfer.PostedDate.ToString("dd/MM/yyyy");
                labeNo.Text = mcRSTransfer.No;
                LabeDate.Text = mcRSTransfer.Date.ToString("dd/MM/yyyy");

                labeAccountingObjectName.Text = mcRSTransfer.AccountingObjectName;
                labeAddres.Text = mcRSTransfer.AccountingObjectAddress;
                labeReason.Text = mcRSTransfer.Reason;
                LabetotalAmount.Text = mcRSTransfer.TotalAmount.ToString();
                //uGridDetail.DataSource = listRSTransferDetail.Where(p => p.RSTransferID == mcmcRSInwardOutward.ID).ToList();
                //Utils.ConfigGrid(uGridDetail, TextMessage.ConstDatabase.RSTransferDetail_TableName);

                if (mcRSTransfer.RSTransferDetails.Count == 0) mcRSTransfer.RSTransferDetails = new List<RSTransferDetail>();
                uGridDetail.DataSource = mcRSTransfer.RSTransferDetails;    //Utils.CloneObject(mcPayment.MCPaymentDetails);

                #region Config Grid
                //hiển thị 1 band
                // uGridDetail.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
                //tắt lọc cột
                uGridDetail.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
                uGridDetail.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
                //tự thay đổi kích thước cột
                uGridDetail.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                //tắt tiêu đề
                uGridDetail.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
                //select cả hàng hay ko?
                uGridDetail.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
                uGridDetail.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;

                //Hiện những dòng trống?
                uGridDetail.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
                uGridDetail.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
                //Fix Header
                uGridDetail.DisplayLayout.UseFixedHeaders = true;

                UltraGridBand band = uGridDetail.DisplayLayout.Bands[0];
                band.Summaries.Clear();

                Template mauGiaoDien = GetMauGiaoDien(mcRSTransfer.TypeID, mcRSTransfer.TemplateID, DsTemplate);

                Utils.ConfigGrid(uGridDetail, ConstDatabase.RSInwardOutwardDetail_TableName, mauGiaoDien == null ? new List<TemplateColumn>() : mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 0).TemplateColumns, 0);
                //Thêm tổng số ở cột "Số tiền"
                SummarySettings summary = band.Summaries.Add("SumAmountOriginal", SummaryType.Sum, band.Columns["AmountOriginal"]);
                summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                summary.DisplayFormat = "{0:N0}";
                summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
                #endregion
                foreach (var item in band.Columns)
                {
                    if (item.Key.Equals("MaterialGoodsID"))
                    {
                        // List<MaterialGoods> MaterialGoodsItems = _IMaterialGoodsService.GetAll().OrderBy(p => p.MaterialGoodsCode).Where(p => p.IsActive == true).ToList(); ;
                        List<MaterialGoods> MaterialGoodsItems = _IMaterialGoodsService.GetByMateriaGoodCode(true);
                        item.Editor = this.CreateCombobox(MaterialGoodsItems, ConstDatabase.MaterialGoods_TableName, "ID", "MaterialGoodsCode");

                    }
                    else if (item.Key.Equals("ToRepositoryID"))
                    {
                        // List<Repository> Repositorys = _IRepositoryService.GetAll().OrderBy(p => p.RepositoryCode).Where(p => p.IsActive == true).ToList(); ;
                        List<Repository> Repositorys = _IRepositoryService.GetByRepositoryCode(true);
                        item.Editor = this.CreateCombobox(Repositorys, ConstDatabase.Repository_TableName, "ID", "RepositoryCode");

                    }
                    else if (item.Key.Equals("FromRepositoryID"))
                    {
                        List<Repository> Repositorys = _IRepositoryService.GetAll().OrderBy(p => p.RepositoryCode).Where(p => p.IsActive == true).ToList(); ;
                        item.Editor = this.CreateCombobox(Repositorys, ConstDatabase.Repository_TableName, "ID", "RepositoryCode");

                    }
                }
            }
        }
        #endregion
        public static Template GetMauGiaoDien(int typeId, Guid? templateId, Dictionary<string, Template> dsTemplate)    //out string keyofdsTemplate
        {
            string keyofdsTemplate = string.Format("{0}@{1}", typeId, templateId);
            if (!dsTemplate.Keys.Contains(keyofdsTemplate))
            {
                //Note: các chứng từ được sinh tự động từ nghiệp vụ khác thì phải truyền TypeID của nghiệp vụ khác đó
                int typeIdTemp = GetTypeIdRef(typeId);
                Template template = Utils.GetTemplateInDatabase(templateId, typeIdTemp);
                dsTemplate.Add(keyofdsTemplate, template);
                return template;
            }
            //keyofdsTemplate = string.Empty;
            return dsTemplate[keyofdsTemplate];
        }
        static int GetTypeIdRef(int typeId)
        {
            return typeId;
        }
    }
}
