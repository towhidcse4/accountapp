﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.Text.RegularExpressions;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using System.Globalization;
using System.Threading;
using Infragistics.Win.UltraWinGrid;
using System.Reflection;

namespace Accounting
{
    public partial class FRSTransferDetail : FrmTmpFRSTransferDetail
    {
        #region khai báo
        UltraGrid ugrid2 = null;
        UltraGrid ugrid1 = null;
        UltraGrid ugrid0 = null;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private int type = 420;
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        #endregion

        #region khởi tạo
        public FRSTransferDetail(RSTransfer temp, List<RSTransfer> dsRSTransfer, int statusForm)
        {//Add -> thêm, Sửa -> sửa, Xem -> Sửa ở trạng thái Enable = False

            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();
            #endregion
            #region Thiết lập ban đầu cho Form  
            _statusForm = statusForm;
            _select = statusForm == ConstFrm.optStatusForm.Add ? (new RSTransfer { TypeID = 420 }) : temp;
            if (_select.TypeID == 420)
                cbbChonLoaiXK.SelectedIndex = 0;
            else if (_select.TypeID == 421)
                cbbChonLoaiXK.SelectedIndex = 1;
            else
                cbbChonLoaiXK.SelectedIndex = 2;
            txtInvoiceNo1.MaxLength = 7;
            txtInvoiceNo2.MaxLength = 7;
            _listSelects.AddRange(dsRSTransfer);
            //SetDetailData();
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, _listSelects, statusForm);



            #endregion
        }

        #endregion

        #region

        public override void InitializeGUI(RSTransfer input)
        {
            #region Lấy các giá trị const từ Database

            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)

            Template mauGiaoDien = Utils.GetMauGiaoDien(420, input.TemplateID);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;

            #endregion

            #endregion

            #region Thiết lập dữ liệu và Fill dữ liệu Obj vào control (nếu đang sửa)

            #endregion

            #region Phần đầu

            //Accounting.Core.Domain.Type type = Utils.ListType.FirstOrDefault(p => p.ID.Equals(_select.TypeID));
            //if (type != null)
            grpTop.Text = "Chuyển kho";
            this.ConfigTopVouchersNo<RSTransfer>(pnlTopVouchers, "No", "PostedDate", "Date");

            #endregion
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
            }
            else
            {
                bdlRefVoucher = new BindingList<RefVoucher>(input.RefVouchers);
                if (input.TypeID == 420)
                {
                    cbbChonLoaiXK.SelectedIndex = 0;
                }
                else if (input.TypeID == 421)
                {
                    cbbChonLoaiXK.SelectedIndex = 1;
                }
                else
                    cbbChonLoaiXK.SelectedIndex = 2;
               
            }
            BindingList<RSTransferDetail> commonBinding = new BindingList<RSTransferDetail>();
            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                commonBinding = new BindingList<RSTransferDetail>(input.RSTransferDetails);
            }
            _listObjectInput = new BindingList<System.Collections.IList> { commonBinding };
            _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
            this.ConfigGridByTemplete_General<RSTransfer>(pnlUgrid, mauGiaoDien);

            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { true, true, false };
            this.ConfigGridByManyTemplete<RSTransfer>(420, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());

            //Cấu hình Grid động
            _select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmount;
            _select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add ? "VND" : input.CurrencyID;
            List<RSTransfer> inputCurrency = new List<RSTransfer> { _select };
            this.ConfigGridCurrencyByTemplate<RSTransfer>(420,
                                                          new BindingList<System.Collections.IList> { inputCurrency },
                                                          uGridControl, mauGiaoDien);
            loadPTVC(input);
            ugrid0 = Controls.Find("uGrid0", true).FirstOrDefault() as UltraGrid;
            ugrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
            if (ugrid2 != null)
            {
                ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            }
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 420)//trungnq thêm vào để  làm task 6357
                {
                    
                    if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "AllowInputCostUnit").Data == "1")//trungnq thêm vào để  làm task 6357 mở/ khóa cột đơn giá, thành tiền của màn xuất kho
                    {
                        ugrid0.DisplayLayout.Bands[0].Columns["UnitPrice"].CellActivation = Activation.AllowEdit;
                        ugrid0.DisplayLayout.Bands[0].Columns["Amount"].CellActivation = Activation.AllowEdit;
                    }
                    else
                    {
                        ugrid0.DisplayLayout.Bands[0].Columns["UnitPrice"].CellActivation = Activation.NoEdit;
                        ugrid0.DisplayLayout.Bands[0].Columns["Amount"].CellActivation = Activation.NoEdit;
                    }
                };
            }
        }
        #endregion

        #region Utils

        #endregion

        #region Event

        #endregion

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucher>();
                    var f = new FViewVoucherOriginal(_select.CurrencyID, datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID,
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void FRSTransferDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        private void cbbChonThanhToan_SelectionChanged(object sender, EventArgs e)
        {
            var combo = (UltraComboEditor)sender;
            switch (combo.SelectedIndex)
            {
                case 0:
                    tabXuatKhoKiemVCNV.Visible = true;
                    tabXuatChuyenNB.Visible = false;
                    tabXuatGuiDL.Visible = false;
                    _select.TypeID = 420;
                    if (_statusForm == ConstFrm.optStatusForm.Add)
                    {
                        txtDescription1.Text = 
                        _select.Reason = "Xuất kho kiêm vận chuyển nội bộ";
                    }
                    ResetValueControl();
                    loadPTVC(_select);
                    break;
                case 1:
                    tabXuatGuiDL.Visible = true;
                    tabXuatKhoKiemVCNV.Visible = false;
                    tabXuatChuyenNB.Visible = false;
                    _select.TypeID = 421;
                    if (_statusForm == ConstFrm.optStatusForm.Add)
                    {
                        txtDescription2.Text = "Xuất gửi đại lý";
                        _select.Reason = "Xuất gửi đại lý";
                    }
                    ResetValueControl();
                    loadPTVC(_select);    
                    break;
                case 2:
                    tabXuatChuyenNB.Visible = true;
                    tabXuatKhoKiemVCNV.Visible = false;
                    tabXuatGuiDL.Visible = false;
                    _select.TypeID = 422;
                    if (_statusForm == ConstFrm.optStatusForm.Add)
                    {
                        txtDescription3.Text = "Xuất chuyển nội bộ";
                        _select.Reason = "Xuất chuyển nội bộ";
                    }
                    ResetValueControl();
                    loadPTVC(_select);
                    break;

            }

        }
        private void ResetValueControl()
        {
            txtInvoiceNo1.Text = "";
            txtInvoiceNo2.Text = "";
            txtInvoiceSeries1.Text = "";
            txtInvoiceSeries2.Text = "";
            txtMobilizationDate1.Value = null;
            txtMobilizationDate2.Value = null;
            txtMobilizationOrderFor1.Text = "";
            txtMobilizationOrderFor2.Text = "";
            txtMobilizationOrderNo1.Text = "";
            txtMobilizationOrderNo2.Text = "";
            txtMobilizationOrderOf1.Text = "";
            txtMobilizationOrderOf2.Text = "";
            cbbInvoiceTemplate1.Value = null;
            cbbInvoiceTemplate2.Value = null;
        }
        private void cbbTransferMethod_EditorButtonClick(object sender, EditorButtonEventArgs e)
        {
            UltraCombo combo = (UltraCombo)sender;
            new FTransportMethodDetail().ShowDialog(this);
            Utils.ClearCacheByType<TransportMethod>();
            loadPTVC(_select);
            if (FTransportMethodDetail._TransportMethodCode != null && FTransportMethodDetail._TransportMethodCode != "")
                combo.SelectedText = FTransportMethodDetail._TransportMethodCode;

        }
        private void loadPTVC(RSTransfer input)
        {
            if (input.TypeID == 420)
            {
                this.ConfigCombo(Utils.ListTransportMethod, cbbTransferMethod, "TransportMethodCode", "ID", input, "TransportMethodID", haveEditorButton: true);              
                this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectName1, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 2, haveEditorButton: true);
                List<TT153Report> lstinvoice = (from a in Utils.ListTT153Report
                                                join b in Utils.ITT153PublishInvoiceDetailService.Query
                                                     on a.ID equals b.TT153ReportID
                                                select a).ToList();
                lstinvoice = lstinvoice.GroupBy(x => x.ID).Select(x => x.First()).ToList();
                this.ConfigCombo(lstinvoice, cbbInvoiceTemplate1, "InvoiceTemplate", "InvoiceTemplate", input, "InvTemplate", haveEditorButton: false);
                DataBinding(new List<Control> { txtMobilizationDate1, txtAccountingObjectName1, txtMobilizationOrderNo1, txtMobilizationOrderOf1, txtMobilizationOrderFor1, txtDescription1, txtInvoiceNo1, txtInvoiceSeries1 },
                  "MobilizationOrderDate, AccountingObjectName, MobilizationOrderNo, MobilizationOrderOf, MobilizationOrderFor, Reason, InvNo, InvSeries",
                  "2, 0, 0, 0, 0, 0, 0, 0");               
            }
            else if (input.TypeID == 421)
            {
                this.ConfigCombo(Utils.ListTransportMethod, cbbTransferMethod2, "TransportMethodCode", "ID", input, "TransportMethodID", haveEditorButton: true);
                this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObject2, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 2, haveEditorButton: true);
                List<TT153Report> lstinvoice = (from a in Utils.ListTT153Report
                                                join b in Utils.ITT153PublishInvoiceDetailService.Query
                                                     on a.ID equals b.TT153ReportID
                                                select a).ToList();
                lstinvoice = lstinvoice.GroupBy(x => x.ID).Select(x => x.First()).ToList();
                this.ConfigCombo(lstinvoice, cbbInvoiceTemplate2, "InvoiceTemplate", "InvoiceTemplate", input, "InvTemplate", haveEditorButton: false);
                DataBinding(new List<Control> { txtMobilizationDate2, txtAccountingObject2, txtMobilizationOrderNo2, txtMobilizationOrderOf2, txtMobilizationOrderFor2, txtDescription2, txtInvoiceNo2, txtInvoiceSeries2 },
                  "MobilizationOrderDate, AccountingObjectName, MobilizationOrderNo, MobilizationOrderOf, MobilizationOrderFor, Reason, InvNo, InvSeries",
                  "2, 0, 0, 0, 0, 0, 0, 0");               
            }
            else
            {
                this.ConfigCombo(Utils.ListTransportMethod, cbbTransferMethod3, "TransportMethodCode", "ID", input, "TransportMethodID", haveEditorButton: true);
                this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObject3, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 2, haveEditorButton: true);
                DataBinding(new List<Control> { txtDescription3, txtAccountingObject3 }, "Reason, AccountingObjectName", "0, 0");
            }
        }
        private void txtInvoice_Validated(object sender, EventArgs e)
        {
            UltraTextEditor txtInvoiceNo = (UltraTextEditor)sender;
            if (txtInvoiceNo.Value != null && txtInvoiceNo.Value.ToString() != "")
            {
                string t = txtInvoiceNo.Value.ToString();
                txtInvoiceNo.Value = t.PadLeft(7, '0');
            }
        }
    }
    public class FrmTmpFRSTransferDetail : DetailBase<RSTransfer>
    {
    }
}