﻿namespace Accounting
{
    partial class FRSTransferDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance127 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance116 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance123 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance130 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance129 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem12 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.pnlUgrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridControl = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.tabXuatChuyenNB = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbTransferMethod3 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel29 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObject3 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel30 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObject3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDescription3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.tabXuatGuiDL = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbTransferMethod2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbInvoiceTemplate2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtInvoiceNo2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.txtInvoiceSeries2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.txtDescription2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.txtMobilizationDate2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.cbbAccountingObject2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.txtMobilizationOrderOf2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtMobilizationOrderNo2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtMobilizationOrderFor2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObject2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tabXuatKhoKiemVCNV = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbTransferMethod = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbInvoiceTemplate1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtInvoiceNo1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.txtInvoiceSeries1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.txtDescription1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtMobilizationDate1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.cbbAccountingObjectName1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.txtMobilizationOrderOf1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtMobilizationOrderNo1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtMobilizationOrderFor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectName1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.grpTop = new Infragistics.Win.Misc.UltraGroupBox();
            this.lblTT = new Infragistics.Win.Misc.UltraLabel();
            this.cbbChonLoaiXK = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.pnlTopVouchers = new Infragistics.Win.Misc.UltraPanel();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.pnlUgrid.ClientArea.SuspendLayout();
            this.pnlUgrid.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).BeginInit();
            this.ultraPanel3.ClientArea.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabXuatChuyenNB)).BeginInit();
            this.tabXuatChuyenNB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTransferMethod3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObject3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabXuatGuiDL)).BeginInit();
            this.tabXuatGuiDL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTransferMethod2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbInvoiceTemplate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceNo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceSeries2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilizationDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilizationOrderOf2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilizationOrderNo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilizationOrderFor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObject2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabXuatKhoKiemVCNV)).BeginInit();
            this.tabXuatKhoKiemVCNV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTransferMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbInvoiceTemplate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceNo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceSeries1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilizationDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilizationOrderOf1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilizationOrderNo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilizationOrderFor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTop)).BeginInit();
            this.grpTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChonLoaiXK)).BeginInit();
            this.pnlTopVouchers.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.pnlUgrid);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 284);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1063, 371);
            this.ultraGroupBox1.TabIndex = 4;
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // pnlUgrid
            // 
            this.pnlUgrid.AutoSize = true;
            // 
            // pnlUgrid.ClientArea
            // 
            this.pnlUgrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.pnlUgrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlUgrid.Location = new System.Drawing.Point(3, 0);
            this.pnlUgrid.Name = "pnlUgrid";
            this.pnlUgrid.Size = new System.Drawing.Size(1057, 368);
            this.pnlUgrid.TabIndex = 0;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance1;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(957, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 7;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // ultraPanel1
            // 
            appearance2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ultraPanel1.Appearance = appearance2;
            this.ultraPanel1.AutoSize = true;
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.uGridControl);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 612);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(1063, 43);
            this.ultraPanel1.TabIndex = 5;
            // 
            // uGridControl
            // 
            this.uGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridControl.Location = new System.Drawing.Point(613, 0);
            this.uGridControl.Name = "uGridControl";
            this.uGridControl.Size = new System.Drawing.Size(450, 40);
            this.uGridControl.TabIndex = 10067;
            this.uGridControl.Text = "Test3";
            // 
            // ultraPanel3
            // 
            // 
            // ultraPanel3.ClientArea
            // 
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraPanel2);
            this.ultraPanel3.ClientArea.Controls.Add(this.grpTop);
            this.ultraPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel3.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(1063, 270);
            this.ultraPanel3.TabIndex = 0;
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.tabXuatChuyenNB);
            this.ultraPanel2.ClientArea.Controls.Add(this.tabXuatGuiDL);
            this.ultraPanel2.ClientArea.Controls.Add(this.tabXuatKhoKiemVCNV);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 90);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(1063, 180);
            this.ultraPanel2.TabIndex = 4;
            // 
            // tabXuatChuyenNB
            // 
            this.tabXuatChuyenNB.Controls.Add(this.cbbTransferMethod3);
            this.tabXuatChuyenNB.Controls.Add(this.ultraLabel29);
            this.tabXuatChuyenNB.Controls.Add(this.cbbAccountingObject3);
            this.tabXuatChuyenNB.Controls.Add(this.ultraLabel30);
            this.tabXuatChuyenNB.Controls.Add(this.txtAccountingObject3);
            this.tabXuatChuyenNB.Controls.Add(this.txtDescription3);
            this.tabXuatChuyenNB.Controls.Add(this.ultraLabel23);
            this.tabXuatChuyenNB.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance31.FontData.BoldAsString = "True";
            this.tabXuatChuyenNB.HeaderAppearance = appearance31;
            this.tabXuatChuyenNB.Location = new System.Drawing.Point(0, 0);
            this.tabXuatChuyenNB.Name = "tabXuatChuyenNB";
            this.tabXuatChuyenNB.Size = new System.Drawing.Size(1063, 180);
            this.tabXuatChuyenNB.TabIndex = 10091;
            this.tabXuatChuyenNB.Text = "Thông tin chung";
            this.tabXuatChuyenNB.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbTransferMethod3
            // 
            this.cbbTransferMethod3.AutoSize = false;
            appearance3.Image = global::Accounting.Properties.Resources.plus;
            editorButton1.Appearance = appearance3;
            this.cbbTransferMethod3.ButtonsRight.Add(editorButton1);
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbTransferMethod3.DisplayLayout.Appearance = appearance4;
            this.cbbTransferMethod3.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbTransferMethod3.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance5.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance5.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbTransferMethod3.DisplayLayout.GroupByBox.Appearance = appearance5;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbTransferMethod3.DisplayLayout.GroupByBox.BandLabelAppearance = appearance6;
            this.cbbTransferMethod3.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance7.BackColor2 = System.Drawing.SystemColors.Control;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbTransferMethod3.DisplayLayout.GroupByBox.PromptAppearance = appearance7;
            this.cbbTransferMethod3.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbTransferMethod3.DisplayLayout.MaxRowScrollRegions = 1;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbTransferMethod3.DisplayLayout.Override.ActiveCellAppearance = appearance8;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbTransferMethod3.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.cbbTransferMethod3.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbTransferMethod3.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.cbbTransferMethod3.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            appearance11.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbTransferMethod3.DisplayLayout.Override.CellAppearance = appearance11;
            this.cbbTransferMethod3.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbTransferMethod3.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbTransferMethod3.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            appearance13.TextHAlignAsString = "Left";
            this.cbbTransferMethod3.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.cbbTransferMethod3.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbTransferMethod3.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.cbbTransferMethod3.DisplayLayout.Override.RowAppearance = appearance14;
            this.cbbTransferMethod3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbTransferMethod3.DisplayLayout.Override.TemplateAddRowAppearance = appearance15;
            this.cbbTransferMethod3.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbTransferMethod3.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbTransferMethod3.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbTransferMethod3.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbTransferMethod3.Location = new System.Drawing.Point(163, 85);
            this.cbbTransferMethod3.Name = "cbbTransferMethod3";
            this.cbbTransferMethod3.Size = new System.Drawing.Size(201, 22);
            this.cbbTransferMethod3.TabIndex = 10069;
            // 
            // ultraLabel29
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.TextHAlignAsString = "Left";
            appearance16.TextVAlignAsString = "Middle";
            this.ultraLabel29.Appearance = appearance16;
            this.ultraLabel29.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel29.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel29.Location = new System.Drawing.Point(13, 87);
            this.ultraLabel29.Name = "ultraLabel29";
            this.ultraLabel29.Size = new System.Drawing.Size(140, 22);
            this.ultraLabel29.TabIndex = 10068;
            this.ultraLabel29.Text = "Phương thức vận chuyển";
            // 
            // cbbAccountingObject3
            // 
            this.cbbAccountingObject3.AutoSize = false;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountingObject3.DisplayLayout.Appearance = appearance17;
            this.cbbAccountingObject3.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountingObject3.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance18.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance18.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance18.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject3.DisplayLayout.GroupByBox.Appearance = appearance18;
            appearance19.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObject3.DisplayLayout.GroupByBox.BandLabelAppearance = appearance19;
            this.cbbAccountingObject3.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance20.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance20.BackColor2 = System.Drawing.SystemColors.Control;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObject3.DisplayLayout.GroupByBox.PromptAppearance = appearance20;
            this.cbbAccountingObject3.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountingObject3.DisplayLayout.MaxRowScrollRegions = 1;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountingObject3.DisplayLayout.Override.ActiveCellAppearance = appearance21;
            appearance22.BackColor = System.Drawing.SystemColors.Highlight;
            appearance22.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountingObject3.DisplayLayout.Override.ActiveRowAppearance = appearance22;
            this.cbbAccountingObject3.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountingObject3.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject3.DisplayLayout.Override.CardAreaAppearance = appearance23;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            appearance24.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountingObject3.DisplayLayout.Override.CellAppearance = appearance24;
            this.cbbAccountingObject3.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountingObject3.DisplayLayout.Override.CellPadding = 0;
            appearance25.BackColor = System.Drawing.SystemColors.Control;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject3.DisplayLayout.Override.GroupByRowAppearance = appearance25;
            appearance26.TextHAlignAsString = "Left";
            this.cbbAccountingObject3.DisplayLayout.Override.HeaderAppearance = appearance26;
            this.cbbAccountingObject3.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountingObject3.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountingObject3.DisplayLayout.Override.RowAppearance = appearance27;
            this.cbbAccountingObject3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountingObject3.DisplayLayout.Override.TemplateAddRowAppearance = appearance28;
            this.cbbAccountingObject3.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObject3.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountingObject3.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountingObject3.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObject3.Location = new System.Drawing.Point(163, 56);
            this.cbbAccountingObject3.Name = "cbbAccountingObject3";
            this.cbbAccountingObject3.Size = new System.Drawing.Size(201, 22);
            this.cbbAccountingObject3.TabIndex = 10066;
            // 
            // ultraLabel30
            // 
            appearance29.BackColor = System.Drawing.Color.Transparent;
            appearance29.TextHAlignAsString = "Left";
            appearance29.TextVAlignAsString = "Middle";
            this.ultraLabel30.Appearance = appearance29;
            this.ultraLabel30.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel30.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel30.Location = new System.Drawing.Point(13, 59);
            this.ultraLabel30.Name = "ultraLabel30";
            this.ultraLabel30.Size = new System.Drawing.Size(105, 22);
            this.ultraLabel30.TabIndex = 10065;
            this.ultraLabel30.Text = "Người vận chuyển";
            // 
            // txtAccountingObject3
            // 
            this.txtAccountingObject3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObject3.AutoSize = false;
            this.txtAccountingObject3.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObject3.Location = new System.Drawing.Point(379, 56);
            this.txtAccountingObject3.MaxLength = 512;
            this.txtAccountingObject3.Name = "txtAccountingObject3";
            this.txtAccountingObject3.Size = new System.Drawing.Size(652, 22);
            this.txtAccountingObject3.TabIndex = 10067;
            // 
            // txtDescription3
            // 
            this.txtDescription3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescription3.AutoSize = false;
            this.txtDescription3.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtDescription3.Location = new System.Drawing.Point(163, 30);
            this.txtDescription3.MaxLength = 512;
            this.txtDescription3.Name = "txtDescription3";
            this.txtDescription3.Size = new System.Drawing.Size(868, 22);
            this.txtDescription3.TabIndex = 10064;
            // 
            // ultraLabel23
            // 
            appearance30.BackColor = System.Drawing.Color.Transparent;
            appearance30.TextHAlignAsString = "Left";
            appearance30.TextVAlignAsString = "Middle";
            this.ultraLabel23.Appearance = appearance30;
            this.ultraLabel23.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel23.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel23.Location = new System.Drawing.Point(13, 29);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel23.TabIndex = 10063;
            this.ultraLabel23.Text = "Diễn giải";
            // 
            // tabXuatGuiDL
            // 
            this.tabXuatGuiDL.Controls.Add(this.cbbTransferMethod2);
            this.tabXuatGuiDL.Controls.Add(this.cbbInvoiceTemplate2);
            this.tabXuatGuiDL.Controls.Add(this.txtInvoiceNo2);
            this.tabXuatGuiDL.Controls.Add(this.ultraLabel11);
            this.tabXuatGuiDL.Controls.Add(this.txtInvoiceSeries2);
            this.tabXuatGuiDL.Controls.Add(this.ultraLabel12);
            this.tabXuatGuiDL.Controls.Add(this.txtDescription2);
            this.tabXuatGuiDL.Controls.Add(this.ultraLabel13);
            this.tabXuatGuiDL.Controls.Add(this.txtMobilizationDate2);
            this.tabXuatGuiDL.Controls.Add(this.cbbAccountingObject2);
            this.tabXuatGuiDL.Controls.Add(this.ultraLabel14);
            this.tabXuatGuiDL.Controls.Add(this.ultraLabel15);
            this.tabXuatGuiDL.Controls.Add(this.ultraLabel16);
            this.tabXuatGuiDL.Controls.Add(this.ultraLabel17);
            this.tabXuatGuiDL.Controls.Add(this.ultraLabel18);
            this.tabXuatGuiDL.Controls.Add(this.ultraLabel19);
            this.tabXuatGuiDL.Controls.Add(this.ultraLabel20);
            this.tabXuatGuiDL.Controls.Add(this.txtMobilizationOrderOf2);
            this.tabXuatGuiDL.Controls.Add(this.txtMobilizationOrderNo2);
            this.tabXuatGuiDL.Controls.Add(this.txtMobilizationOrderFor2);
            this.tabXuatGuiDL.Controls.Add(this.txtAccountingObject2);
            this.tabXuatGuiDL.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance79.FontData.BoldAsString = "True";
            this.tabXuatGuiDL.HeaderAppearance = appearance79;
            this.tabXuatGuiDL.Location = new System.Drawing.Point(0, 0);
            this.tabXuatGuiDL.Name = "tabXuatGuiDL";
            this.tabXuatGuiDL.Size = new System.Drawing.Size(1063, 180);
            this.tabXuatGuiDL.TabIndex = 14;
            this.tabXuatGuiDL.Text = "Thông tin chung";
            this.tabXuatGuiDL.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbTransferMethod2
            // 
            this.cbbTransferMethod2.AutoSize = false;
            appearance32.Image = global::Accounting.Properties.Resources.plus;
            editorButton2.Appearance = appearance32;
            this.cbbTransferMethod2.ButtonsRight.Add(editorButton2);
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbTransferMethod2.DisplayLayout.Appearance = appearance33;
            this.cbbTransferMethod2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbTransferMethod2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance34.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance34.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance34.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbTransferMethod2.DisplayLayout.GroupByBox.Appearance = appearance34;
            appearance35.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbTransferMethod2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance35;
            this.cbbTransferMethod2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance36.BackColor2 = System.Drawing.SystemColors.Control;
            appearance36.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance36.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbTransferMethod2.DisplayLayout.GroupByBox.PromptAppearance = appearance36;
            this.cbbTransferMethod2.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbTransferMethod2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbTransferMethod2.DisplayLayout.Override.ActiveCellAppearance = appearance37;
            appearance38.BackColor = System.Drawing.SystemColors.Highlight;
            appearance38.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbTransferMethod2.DisplayLayout.Override.ActiveRowAppearance = appearance38;
            this.cbbTransferMethod2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbTransferMethod2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance39.BackColor = System.Drawing.SystemColors.Window;
            this.cbbTransferMethod2.DisplayLayout.Override.CardAreaAppearance = appearance39;
            appearance40.BorderColor = System.Drawing.Color.Silver;
            appearance40.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbTransferMethod2.DisplayLayout.Override.CellAppearance = appearance40;
            this.cbbTransferMethod2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbTransferMethod2.DisplayLayout.Override.CellPadding = 0;
            appearance41.BackColor = System.Drawing.SystemColors.Control;
            appearance41.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance41.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance41.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance41.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbTransferMethod2.DisplayLayout.Override.GroupByRowAppearance = appearance41;
            appearance42.TextHAlignAsString = "Left";
            this.cbbTransferMethod2.DisplayLayout.Override.HeaderAppearance = appearance42;
            this.cbbTransferMethod2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbTransferMethod2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            appearance43.BorderColor = System.Drawing.Color.Silver;
            this.cbbTransferMethod2.DisplayLayout.Override.RowAppearance = appearance43;
            this.cbbTransferMethod2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance44.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbTransferMethod2.DisplayLayout.Override.TemplateAddRowAppearance = appearance44;
            this.cbbTransferMethod2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbTransferMethod2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbTransferMethod2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbTransferMethod2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbTransferMethod2.Location = new System.Drawing.Point(183, 111);
            this.cbbTransferMethod2.Name = "cbbTransferMethod2";
            this.cbbTransferMethod2.Size = new System.Drawing.Size(201, 22);
            this.cbbTransferMethod2.TabIndex = 10068;
            // 
            // cbbInvoiceTemplate2
            // 
            this.cbbInvoiceTemplate2.AutoSize = false;
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            appearance45.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbInvoiceTemplate2.DisplayLayout.Appearance = appearance45;
            this.cbbInvoiceTemplate2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbInvoiceTemplate2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance46.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance46.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance46.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance46.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbInvoiceTemplate2.DisplayLayout.GroupByBox.Appearance = appearance46;
            appearance47.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbInvoiceTemplate2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance47;
            this.cbbInvoiceTemplate2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance48.BackColor2 = System.Drawing.SystemColors.Control;
            appearance48.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance48.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbInvoiceTemplate2.DisplayLayout.GroupByBox.PromptAppearance = appearance48;
            this.cbbInvoiceTemplate2.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbInvoiceTemplate2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbInvoiceTemplate2.DisplayLayout.Override.ActiveCellAppearance = appearance49;
            appearance50.BackColor = System.Drawing.SystemColors.Highlight;
            appearance50.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbInvoiceTemplate2.DisplayLayout.Override.ActiveRowAppearance = appearance50;
            this.cbbInvoiceTemplate2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbInvoiceTemplate2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance51.BackColor = System.Drawing.SystemColors.Window;
            this.cbbInvoiceTemplate2.DisplayLayout.Override.CardAreaAppearance = appearance51;
            appearance52.BorderColor = System.Drawing.Color.Silver;
            appearance52.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbInvoiceTemplate2.DisplayLayout.Override.CellAppearance = appearance52;
            this.cbbInvoiceTemplate2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbInvoiceTemplate2.DisplayLayout.Override.CellPadding = 0;
            appearance53.BackColor = System.Drawing.SystemColors.Control;
            appearance53.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance53.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance53.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance53.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbInvoiceTemplate2.DisplayLayout.Override.GroupByRowAppearance = appearance53;
            appearance54.TextHAlignAsString = "Left";
            this.cbbInvoiceTemplate2.DisplayLayout.Override.HeaderAppearance = appearance54;
            this.cbbInvoiceTemplate2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbInvoiceTemplate2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            appearance55.BorderColor = System.Drawing.Color.Silver;
            this.cbbInvoiceTemplate2.DisplayLayout.Override.RowAppearance = appearance55;
            this.cbbInvoiceTemplate2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance56.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbInvoiceTemplate2.DisplayLayout.Override.TemplateAddRowAppearance = appearance56;
            this.cbbInvoiceTemplate2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbInvoiceTemplate2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbInvoiceTemplate2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbInvoiceTemplate2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbInvoiceTemplate2.Location = new System.Drawing.Point(183, 138);
            this.cbbInvoiceTemplate2.Name = "cbbInvoiceTemplate2";
            this.cbbInvoiceTemplate2.Size = new System.Drawing.Size(201, 22);
            this.cbbInvoiceTemplate2.TabIndex = 10067;
            // 
            // txtInvoiceNo2
            // 
            this.txtInvoiceNo2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInvoiceNo2.AutoSize = false;
            this.txtInvoiceNo2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtInvoiceNo2.Location = new System.Drawing.Point(760, 138);
            this.txtInvoiceNo2.MaxLength = 412;
            this.txtInvoiceNo2.Name = "txtInvoiceNo2";
            this.txtInvoiceNo2.Size = new System.Drawing.Size(271, 22);
            this.txtInvoiceNo2.TabIndex = 10066;
            this.txtInvoiceNo2.Validated += new System.EventHandler(this.txtInvoice_Validated);
            // 
            // ultraLabel11
            // 
            appearance57.BackColor = System.Drawing.Color.Transparent;
            appearance57.TextHAlignAsString = "Left";
            appearance57.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance57;
            this.ultraLabel11.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel11.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel11.Location = new System.Drawing.Point(683, 138);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(70, 22);
            this.ultraLabel11.TabIndex = 10065;
            this.ultraLabel11.Text = "Số hóa đơn";
            // 
            // txtInvoiceSeries2
            // 
            this.txtInvoiceSeries2.AutoSize = false;
            this.txtInvoiceSeries2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtInvoiceSeries2.Location = new System.Drawing.Point(455, 138);
            this.txtInvoiceSeries2.MaxLength = 412;
            this.txtInvoiceSeries2.Name = "txtInvoiceSeries2";
            this.txtInvoiceSeries2.Size = new System.Drawing.Size(210, 22);
            this.txtInvoiceSeries2.TabIndex = 10064;
            // 
            // ultraLabel12
            // 
            appearance58.BackColor = System.Drawing.Color.Transparent;
            appearance58.TextHAlignAsString = "Left";
            appearance58.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance58;
            this.ultraLabel12.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel12.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel12.Location = new System.Drawing.Point(399, 138);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel12.TabIndex = 10063;
            this.ultraLabel12.Text = "Ký hiệu";
            // 
            // txtDescription2
            // 
            this.txtDescription2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescription2.AutoSize = false;
            this.txtDescription2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtDescription2.Location = new System.Drawing.Point(455, 112);
            this.txtDescription2.MaxLength = 512;
            this.txtDescription2.Name = "txtDescription2";
            this.txtDescription2.Size = new System.Drawing.Size(576, 22);
            this.txtDescription2.TabIndex = 10062;
            // 
            // ultraLabel13
            // 
            appearance59.BackColor = System.Drawing.Color.Transparent;
            appearance59.TextHAlignAsString = "Left";
            appearance59.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance59;
            this.ultraLabel13.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel13.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel13.Location = new System.Drawing.Point(399, 113);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel13.TabIndex = 10061;
            this.ultraLabel13.Text = "Diễn giải";
            // 
            // txtMobilizationDate2
            // 
            this.txtMobilizationDate2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtMobilizationDate2.Location = new System.Drawing.Point(183, 57);
            this.txtMobilizationDate2.MaskInput = "dd/mm/yyyy";
            this.txtMobilizationDate2.Name = "txtMobilizationDate2";
            this.txtMobilizationDate2.Size = new System.Drawing.Size(201, 21);
            this.txtMobilizationDate2.TabIndex = 10055;
            // 
            // cbbAccountingObject2
            // 
            this.cbbAccountingObject2.AutoSize = false;
            appearance60.BackColor = System.Drawing.SystemColors.Window;
            appearance60.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountingObject2.DisplayLayout.Appearance = appearance60;
            this.cbbAccountingObject2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountingObject2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance61.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance61.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance61.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance61.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject2.DisplayLayout.GroupByBox.Appearance = appearance61;
            appearance62.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObject2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance62;
            this.cbbAccountingObject2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance63.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance63.BackColor2 = System.Drawing.SystemColors.Control;
            appearance63.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance63.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObject2.DisplayLayout.GroupByBox.PromptAppearance = appearance63;
            this.cbbAccountingObject2.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountingObject2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance64.BackColor = System.Drawing.SystemColors.Window;
            appearance64.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountingObject2.DisplayLayout.Override.ActiveCellAppearance = appearance64;
            appearance65.BackColor = System.Drawing.SystemColors.Highlight;
            appearance65.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountingObject2.DisplayLayout.Override.ActiveRowAppearance = appearance65;
            this.cbbAccountingObject2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountingObject2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance66.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject2.DisplayLayout.Override.CardAreaAppearance = appearance66;
            appearance67.BorderColor = System.Drawing.Color.Silver;
            appearance67.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountingObject2.DisplayLayout.Override.CellAppearance = appearance67;
            this.cbbAccountingObject2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountingObject2.DisplayLayout.Override.CellPadding = 0;
            appearance68.BackColor = System.Drawing.SystemColors.Control;
            appearance68.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance68.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance68.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance68.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject2.DisplayLayout.Override.GroupByRowAppearance = appearance68;
            appearance69.TextHAlignAsString = "Left";
            this.cbbAccountingObject2.DisplayLayout.Override.HeaderAppearance = appearance69;
            this.cbbAccountingObject2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountingObject2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance70.BackColor = System.Drawing.SystemColors.Window;
            appearance70.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountingObject2.DisplayLayout.Override.RowAppearance = appearance70;
            this.cbbAccountingObject2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance71.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountingObject2.DisplayLayout.Override.TemplateAddRowAppearance = appearance71;
            this.cbbAccountingObject2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObject2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountingObject2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountingObject2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObject2.Location = new System.Drawing.Point(183, 85);
            this.cbbAccountingObject2.Name = "cbbAccountingObject2";
            this.cbbAccountingObject2.Size = new System.Drawing.Size(201, 22);
            this.cbbAccountingObject2.TabIndex = 10057;
            // 
            // ultraLabel14
            // 
            appearance72.BackColor = System.Drawing.Color.Transparent;
            appearance72.TextHAlignAsString = "Left";
            appearance72.TextVAlignAsString = "Middle";
            this.ultraLabel14.Appearance = appearance72;
            this.ultraLabel14.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel14.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel14.Location = new System.Drawing.Point(399, 58);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel14.TabIndex = 10018;
            this.ultraLabel14.Text = "Về việc";
            // 
            // ultraLabel15
            // 
            appearance73.BackColor = System.Drawing.Color.Transparent;
            appearance73.TextHAlignAsString = "Left";
            appearance73.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance73;
            this.ultraLabel15.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel15.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel15.Location = new System.Drawing.Point(399, 29);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel15.TabIndex = 10017;
            this.ultraLabel15.Text = "Của";
            // 
            // ultraLabel16
            // 
            appearance74.BackColor = System.Drawing.Color.Transparent;
            appearance74.TextHAlignAsString = "Left";
            appearance74.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance74;
            this.ultraLabel16.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel16.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel16.Location = new System.Drawing.Point(13, 57);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(55, 22);
            this.ultraLabel16.TabIndex = 10015;
            this.ultraLabel16.Text = "Ngày";
            // 
            // ultraLabel17
            // 
            appearance75.BackColor = System.Drawing.Color.Transparent;
            appearance75.TextHAlignAsString = "Left";
            appearance75.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance75;
            this.ultraLabel17.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel17.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel17.Location = new System.Drawing.Point(12, 29);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(115, 22);
            this.ultraLabel17.TabIndex = 10014;
            this.ultraLabel17.Text = "Hợp đồng KT số";
            // 
            // ultraLabel18
            // 
            appearance76.BackColor = System.Drawing.Color.Transparent;
            appearance76.TextHAlignAsString = "Left";
            appearance76.TextVAlignAsString = "Middle";
            this.ultraLabel18.Appearance = appearance76;
            this.ultraLabel18.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel18.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel18.Location = new System.Drawing.Point(13, 138);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(64, 22);
            this.ultraLabel18.TabIndex = 10013;
            this.ultraLabel18.Text = "Mẫu số";
            // 
            // ultraLabel19
            // 
            appearance77.BackColor = System.Drawing.Color.Transparent;
            appearance77.TextHAlignAsString = "Left";
            appearance77.TextVAlignAsString = "Middle";
            this.ultraLabel19.Appearance = appearance77;
            this.ultraLabel19.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel19.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel19.Location = new System.Drawing.Point(13, 113);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(140, 22);
            this.ultraLabel19.TabIndex = 10011;
            this.ultraLabel19.Text = "Phương thức vận chuyển";
            // 
            // ultraLabel20
            // 
            appearance78.BackColor = System.Drawing.Color.Transparent;
            appearance78.TextHAlignAsString = "Left";
            appearance78.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance78;
            this.ultraLabel20.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel20.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel20.Location = new System.Drawing.Point(13, 85);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(164, 22);
            this.ultraLabel20.TabIndex = 1016;
            this.ultraLabel20.Text = "Người vận chuyển";
            // 
            // txtMobilizationOrderOf2
            // 
            this.txtMobilizationOrderOf2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMobilizationOrderOf2.AutoSize = false;
            this.txtMobilizationOrderOf2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtMobilizationOrderOf2.Location = new System.Drawing.Point(455, 28);
            this.txtMobilizationOrderOf2.MaxLength = 512;
            this.txtMobilizationOrderOf2.Name = "txtMobilizationOrderOf2";
            this.txtMobilizationOrderOf2.Size = new System.Drawing.Size(576, 22);
            this.txtMobilizationOrderOf2.TabIndex = 10054;
            // 
            // txtMobilizationOrderNo2
            // 
            this.txtMobilizationOrderNo2.AutoSize = false;
            this.txtMobilizationOrderNo2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtMobilizationOrderNo2.Location = new System.Drawing.Point(183, 28);
            this.txtMobilizationOrderNo2.MaxLength = 512;
            this.txtMobilizationOrderNo2.Name = "txtMobilizationOrderNo2";
            this.txtMobilizationOrderNo2.Size = new System.Drawing.Size(201, 22);
            this.txtMobilizationOrderNo2.TabIndex = 10053;
            // 
            // txtMobilizationOrderFor2
            // 
            this.txtMobilizationOrderFor2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMobilizationOrderFor2.AutoSize = false;
            this.txtMobilizationOrderFor2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtMobilizationOrderFor2.Location = new System.Drawing.Point(455, 57);
            this.txtMobilizationOrderFor2.MaxLength = 512;
            this.txtMobilizationOrderFor2.Name = "txtMobilizationOrderFor2";
            this.txtMobilizationOrderFor2.Size = new System.Drawing.Size(576, 22);
            this.txtMobilizationOrderFor2.TabIndex = 10056;
            // 
            // txtAccountingObject2
            // 
            this.txtAccountingObject2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObject2.AutoSize = false;
            this.txtAccountingObject2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObject2.Location = new System.Drawing.Point(399, 84);
            this.txtAccountingObject2.MaxLength = 512;
            this.txtAccountingObject2.Name = "txtAccountingObject2";
            this.txtAccountingObject2.Size = new System.Drawing.Size(632, 22);
            this.txtAccountingObject2.TabIndex = 10058;
            // 
            // tabXuatKhoKiemVCNV
            // 
            this.tabXuatKhoKiemVCNV.Controls.Add(this.cbbTransferMethod);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.cbbInvoiceTemplate1);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.txtInvoiceNo1);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.ultraLabel10);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.txtInvoiceSeries1);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.ultraLabel8);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.txtDescription1);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.ultraLabel2);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.txtMobilizationDate1);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.cbbAccountingObjectName1);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.ultraLabel3);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.ultraLabel9);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.ultraLabel5);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.ultraLabel7);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.ultraLabel4);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.ultraLabel1);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.ultraLabel6);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.txtMobilizationOrderOf1);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.txtMobilizationOrderNo1);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.txtMobilizationOrderFor1);
            this.tabXuatKhoKiemVCNV.Controls.Add(this.txtAccountingObjectName1);
            this.tabXuatKhoKiemVCNV.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance127.FontData.BoldAsString = "True";
            this.tabXuatKhoKiemVCNV.HeaderAppearance = appearance127;
            this.tabXuatKhoKiemVCNV.Location = new System.Drawing.Point(0, 0);
            this.tabXuatKhoKiemVCNV.Name = "tabXuatKhoKiemVCNV";
            this.tabXuatKhoKiemVCNV.Size = new System.Drawing.Size(1063, 180);
            this.tabXuatKhoKiemVCNV.TabIndex = 5;
            this.tabXuatKhoKiemVCNV.Text = "Thông tin chung";
            this.tabXuatKhoKiemVCNV.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbTransferMethod
            // 
            this.cbbTransferMethod.AutoSize = false;
            appearance80.Image = global::Accounting.Properties.Resources.plus;
            editorButton3.Appearance = appearance80;
            this.cbbTransferMethod.ButtonsRight.Add(editorButton3);
            appearance81.BackColor = System.Drawing.SystemColors.Window;
            appearance81.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbTransferMethod.DisplayLayout.Appearance = appearance81;
            this.cbbTransferMethod.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbTransferMethod.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance82.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance82.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance82.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance82.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbTransferMethod.DisplayLayout.GroupByBox.Appearance = appearance82;
            appearance83.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbTransferMethod.DisplayLayout.GroupByBox.BandLabelAppearance = appearance83;
            this.cbbTransferMethod.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance84.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance84.BackColor2 = System.Drawing.SystemColors.Control;
            appearance84.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance84.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbTransferMethod.DisplayLayout.GroupByBox.PromptAppearance = appearance84;
            this.cbbTransferMethod.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbTransferMethod.DisplayLayout.MaxRowScrollRegions = 1;
            appearance85.BackColor = System.Drawing.SystemColors.Window;
            appearance85.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbTransferMethod.DisplayLayout.Override.ActiveCellAppearance = appearance85;
            appearance86.BackColor = System.Drawing.SystemColors.Highlight;
            appearance86.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbTransferMethod.DisplayLayout.Override.ActiveRowAppearance = appearance86;
            this.cbbTransferMethod.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbTransferMethod.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance87.BackColor = System.Drawing.SystemColors.Window;
            this.cbbTransferMethod.DisplayLayout.Override.CardAreaAppearance = appearance87;
            appearance88.BorderColor = System.Drawing.Color.Silver;
            appearance88.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbTransferMethod.DisplayLayout.Override.CellAppearance = appearance88;
            this.cbbTransferMethod.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbTransferMethod.DisplayLayout.Override.CellPadding = 0;
            appearance89.BackColor = System.Drawing.SystemColors.Control;
            appearance89.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance89.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance89.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance89.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbTransferMethod.DisplayLayout.Override.GroupByRowAppearance = appearance89;
            appearance90.TextHAlignAsString = "Left";
            this.cbbTransferMethod.DisplayLayout.Override.HeaderAppearance = appearance90;
            this.cbbTransferMethod.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbTransferMethod.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance91.BackColor = System.Drawing.SystemColors.Window;
            appearance91.BorderColor = System.Drawing.Color.Silver;
            this.cbbTransferMethod.DisplayLayout.Override.RowAppearance = appearance91;
            this.cbbTransferMethod.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance92.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbTransferMethod.DisplayLayout.Override.TemplateAddRowAppearance = appearance92;
            this.cbbTransferMethod.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbTransferMethod.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbTransferMethod.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbTransferMethod.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbTransferMethod.Location = new System.Drawing.Point(183, 111);
            this.cbbTransferMethod.Name = "cbbTransferMethod";
            this.cbbTransferMethod.Size = new System.Drawing.Size(201, 22);
            this.cbbTransferMethod.TabIndex = 10068;
            this.cbbTransferMethod.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbTransferMethod_EditorButtonClick);
            // 
            // cbbInvoiceTemplate1
            // 
            this.cbbInvoiceTemplate1.AutoSize = false;
            appearance93.BackColor = System.Drawing.SystemColors.Window;
            appearance93.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbInvoiceTemplate1.DisplayLayout.Appearance = appearance93;
            this.cbbInvoiceTemplate1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbInvoiceTemplate1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance94.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance94.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance94.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance94.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbInvoiceTemplate1.DisplayLayout.GroupByBox.Appearance = appearance94;
            appearance95.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbInvoiceTemplate1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance95;
            this.cbbInvoiceTemplate1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance96.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance96.BackColor2 = System.Drawing.SystemColors.Control;
            appearance96.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance96.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbInvoiceTemplate1.DisplayLayout.GroupByBox.PromptAppearance = appearance96;
            this.cbbInvoiceTemplate1.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbInvoiceTemplate1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance97.BackColor = System.Drawing.SystemColors.Window;
            appearance97.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbInvoiceTemplate1.DisplayLayout.Override.ActiveCellAppearance = appearance97;
            appearance98.BackColor = System.Drawing.SystemColors.Highlight;
            appearance98.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbInvoiceTemplate1.DisplayLayout.Override.ActiveRowAppearance = appearance98;
            this.cbbInvoiceTemplate1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbInvoiceTemplate1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance99.BackColor = System.Drawing.SystemColors.Window;
            this.cbbInvoiceTemplate1.DisplayLayout.Override.CardAreaAppearance = appearance99;
            appearance100.BorderColor = System.Drawing.Color.Silver;
            appearance100.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbInvoiceTemplate1.DisplayLayout.Override.CellAppearance = appearance100;
            this.cbbInvoiceTemplate1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbInvoiceTemplate1.DisplayLayout.Override.CellPadding = 0;
            appearance101.BackColor = System.Drawing.SystemColors.Control;
            appearance101.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance101.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance101.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance101.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbInvoiceTemplate1.DisplayLayout.Override.GroupByRowAppearance = appearance101;
            appearance102.TextHAlignAsString = "Left";
            this.cbbInvoiceTemplate1.DisplayLayout.Override.HeaderAppearance = appearance102;
            this.cbbInvoiceTemplate1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbInvoiceTemplate1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance103.BackColor = System.Drawing.SystemColors.Window;
            appearance103.BorderColor = System.Drawing.Color.Silver;
            this.cbbInvoiceTemplate1.DisplayLayout.Override.RowAppearance = appearance103;
            this.cbbInvoiceTemplate1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance104.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbInvoiceTemplate1.DisplayLayout.Override.TemplateAddRowAppearance = appearance104;
            this.cbbInvoiceTemplate1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbInvoiceTemplate1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbInvoiceTemplate1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbInvoiceTemplate1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbInvoiceTemplate1.Location = new System.Drawing.Point(183, 138);
            this.cbbInvoiceTemplate1.Name = "cbbInvoiceTemplate1";
            this.cbbInvoiceTemplate1.Size = new System.Drawing.Size(201, 22);
            this.cbbInvoiceTemplate1.TabIndex = 10067;
            // 
            // txtInvoiceNo1
            // 
            this.txtInvoiceNo1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInvoiceNo1.AutoSize = false;
            this.txtInvoiceNo1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtInvoiceNo1.Location = new System.Drawing.Point(760, 138);
            this.txtInvoiceNo1.MaxLength = 412;
            this.txtInvoiceNo1.Name = "txtInvoiceNo1";
            this.txtInvoiceNo1.Size = new System.Drawing.Size(271, 22);
            this.txtInvoiceNo1.TabIndex = 10066;
            this.txtInvoiceNo1.Validated += new System.EventHandler(this.txtInvoice_Validated);
            // 
            // ultraLabel10
            // 
            appearance105.BackColor = System.Drawing.Color.Transparent;
            appearance105.TextHAlignAsString = "Left";
            appearance105.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance105;
            this.ultraLabel10.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel10.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel10.Location = new System.Drawing.Point(685, 138);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(63, 22);
            this.ultraLabel10.TabIndex = 10065;
            this.ultraLabel10.Text = "Số hóa đơn";
            // 
            // txtInvoiceSeries1
            // 
            this.txtInvoiceSeries1.AutoSize = false;
            this.txtInvoiceSeries1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtInvoiceSeries1.Location = new System.Drawing.Point(455, 138);
            this.txtInvoiceSeries1.MaxLength = 412;
            this.txtInvoiceSeries1.Name = "txtInvoiceSeries1";
            this.txtInvoiceSeries1.Size = new System.Drawing.Size(210, 22);
            this.txtInvoiceSeries1.TabIndex = 10064;
            // 
            // ultraLabel8
            // 
            appearance106.BackColor = System.Drawing.Color.Transparent;
            appearance106.TextHAlignAsString = "Left";
            appearance106.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance106;
            this.ultraLabel8.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel8.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel8.Location = new System.Drawing.Point(399, 138);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel8.TabIndex = 10063;
            this.ultraLabel8.Text = "Ký hiệu";
            // 
            // txtDescription1
            // 
            this.txtDescription1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescription1.AutoSize = false;
            this.txtDescription1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtDescription1.Location = new System.Drawing.Point(455, 112);
            this.txtDescription1.MaxLength = 512;
            this.txtDescription1.Name = "txtDescription1";
            this.txtDescription1.Size = new System.Drawing.Size(576, 22);
            this.txtDescription1.TabIndex = 10062;
            // 
            // ultraLabel2
            // 
            appearance107.BackColor = System.Drawing.Color.Transparent;
            appearance107.TextHAlignAsString = "Left";
            appearance107.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance107;
            this.ultraLabel2.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel2.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel2.Location = new System.Drawing.Point(399, 113);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel2.TabIndex = 10061;
            this.ultraLabel2.Text = "Diễn giải";
            // 
            // txtMobilizationDate1
            // 
            this.txtMobilizationDate1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtMobilizationDate1.Location = new System.Drawing.Point(183, 57);
            this.txtMobilizationDate1.MaskInput = "dd/mm/yyyy";
            this.txtMobilizationDate1.Name = "txtMobilizationDate1";
            this.txtMobilizationDate1.Size = new System.Drawing.Size(201, 21);
            this.txtMobilizationDate1.TabIndex = 10055;
            // 
            // cbbAccountingObjectName1
            // 
            this.cbbAccountingObjectName1.AutoSize = false;
            appearance108.BackColor = System.Drawing.SystemColors.Window;
            appearance108.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountingObjectName1.DisplayLayout.Appearance = appearance108;
            this.cbbAccountingObjectName1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountingObjectName1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance109.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance109.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance109.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance109.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectName1.DisplayLayout.GroupByBox.Appearance = appearance109;
            appearance110.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectName1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance110;
            this.cbbAccountingObjectName1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance111.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance111.BackColor2 = System.Drawing.SystemColors.Control;
            appearance111.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance111.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectName1.DisplayLayout.GroupByBox.PromptAppearance = appearance111;
            this.cbbAccountingObjectName1.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountingObjectName1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance112.BackColor = System.Drawing.SystemColors.Window;
            appearance112.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountingObjectName1.DisplayLayout.Override.ActiveCellAppearance = appearance112;
            appearance113.BackColor = System.Drawing.SystemColors.Highlight;
            appearance113.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountingObjectName1.DisplayLayout.Override.ActiveRowAppearance = appearance113;
            this.cbbAccountingObjectName1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountingObjectName1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance114.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectName1.DisplayLayout.Override.CardAreaAppearance = appearance114;
            appearance115.BorderColor = System.Drawing.Color.Silver;
            appearance115.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountingObjectName1.DisplayLayout.Override.CellAppearance = appearance115;
            this.cbbAccountingObjectName1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountingObjectName1.DisplayLayout.Override.CellPadding = 0;
            appearance116.BackColor = System.Drawing.SystemColors.Control;
            appearance116.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance116.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance116.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance116.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectName1.DisplayLayout.Override.GroupByRowAppearance = appearance116;
            appearance117.TextHAlignAsString = "Left";
            this.cbbAccountingObjectName1.DisplayLayout.Override.HeaderAppearance = appearance117;
            this.cbbAccountingObjectName1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountingObjectName1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance118.BackColor = System.Drawing.SystemColors.Window;
            appearance118.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountingObjectName1.DisplayLayout.Override.RowAppearance = appearance118;
            this.cbbAccountingObjectName1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance119.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountingObjectName1.DisplayLayout.Override.TemplateAddRowAppearance = appearance119;
            this.cbbAccountingObjectName1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectName1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountingObjectName1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountingObjectName1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjectName1.Location = new System.Drawing.Point(183, 85);
            this.cbbAccountingObjectName1.Name = "cbbAccountingObjectName1";
            this.cbbAccountingObjectName1.Size = new System.Drawing.Size(201, 22);
            this.cbbAccountingObjectName1.TabIndex = 10057;
            // 
            // ultraLabel3
            // 
            appearance120.BackColor = System.Drawing.Color.Transparent;
            appearance120.TextHAlignAsString = "Left";
            appearance120.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance120;
            this.ultraLabel3.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel3.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel3.Location = new System.Drawing.Point(399, 58);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel3.TabIndex = 10018;
            this.ultraLabel3.Text = "Về việc";
            // 
            // ultraLabel9
            // 
            appearance121.BackColor = System.Drawing.Color.Transparent;
            appearance121.TextHAlignAsString = "Left";
            appearance121.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance121;
            this.ultraLabel9.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel9.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel9.Location = new System.Drawing.Point(399, 29);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel9.TabIndex = 10017;
            this.ultraLabel9.Text = "Của";
            // 
            // ultraLabel5
            // 
            appearance122.BackColor = System.Drawing.Color.Transparent;
            appearance122.TextHAlignAsString = "Left";
            appearance122.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance122;
            this.ultraLabel5.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel5.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel5.Location = new System.Drawing.Point(13, 57);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(55, 22);
            this.ultraLabel5.TabIndex = 10015;
            this.ultraLabel5.Text = "Ngày";
            // 
            // ultraLabel7
            // 
            appearance123.BackColor = System.Drawing.Color.Transparent;
            appearance123.TextHAlignAsString = "Left";
            appearance123.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance123;
            this.ultraLabel7.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel7.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel7.Location = new System.Drawing.Point(12, 29);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(115, 22);
            this.ultraLabel7.TabIndex = 10014;
            this.ultraLabel7.Text = "Lệnh điều động số";
            // 
            // ultraLabel4
            // 
            appearance124.BackColor = System.Drawing.Color.Transparent;
            appearance124.TextHAlignAsString = "Left";
            appearance124.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance124;
            this.ultraLabel4.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel4.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel4.Location = new System.Drawing.Point(13, 138);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(64, 22);
            this.ultraLabel4.TabIndex = 10013;
            this.ultraLabel4.Text = "Mẫu số";
            // 
            // ultraLabel1
            // 
            appearance125.BackColor = System.Drawing.Color.Transparent;
            appearance125.TextHAlignAsString = "Left";
            appearance125.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance125;
            this.ultraLabel1.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel1.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel1.Location = new System.Drawing.Point(13, 113);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(140, 22);
            this.ultraLabel1.TabIndex = 10011;
            this.ultraLabel1.Text = "Phương thức vận chuyển";
            // 
            // ultraLabel6
            // 
            appearance126.BackColor = System.Drawing.Color.Transparent;
            appearance126.TextHAlignAsString = "Left";
            appearance126.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance126;
            this.ultraLabel6.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel6.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel6.Location = new System.Drawing.Point(13, 85);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(164, 22);
            this.ultraLabel6.TabIndex = 1016;
            this.ultraLabel6.Text = "Người vận chuyển";
            // 
            // txtMobilizationOrderOf1
            // 
            this.txtMobilizationOrderOf1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMobilizationOrderOf1.AutoSize = false;
            this.txtMobilizationOrderOf1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtMobilizationOrderOf1.Location = new System.Drawing.Point(455, 28);
            this.txtMobilizationOrderOf1.MaxLength = 512;
            this.txtMobilizationOrderOf1.Name = "txtMobilizationOrderOf1";
            this.txtMobilizationOrderOf1.Size = new System.Drawing.Size(576, 22);
            this.txtMobilizationOrderOf1.TabIndex = 10054;
            // 
            // txtMobilizationOrderNo1
            // 
            this.txtMobilizationOrderNo1.AutoSize = false;
            this.txtMobilizationOrderNo1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtMobilizationOrderNo1.Location = new System.Drawing.Point(183, 28);
            this.txtMobilizationOrderNo1.MaxLength = 512;
            this.txtMobilizationOrderNo1.Name = "txtMobilizationOrderNo1";
            this.txtMobilizationOrderNo1.Size = new System.Drawing.Size(201, 22);
            this.txtMobilizationOrderNo1.TabIndex = 10053;
            // 
            // txtMobilizationOrderFor1
            // 
            this.txtMobilizationOrderFor1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMobilizationOrderFor1.AutoSize = false;
            this.txtMobilizationOrderFor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtMobilizationOrderFor1.Location = new System.Drawing.Point(455, 57);
            this.txtMobilizationOrderFor1.MaxLength = 512;
            this.txtMobilizationOrderFor1.Name = "txtMobilizationOrderFor1";
            this.txtMobilizationOrderFor1.Size = new System.Drawing.Size(576, 22);
            this.txtMobilizationOrderFor1.TabIndex = 10056;
            // 
            // txtAccountingObjectName1
            // 
            this.txtAccountingObjectName1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectName1.AutoSize = false;
            this.txtAccountingObjectName1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectName1.Location = new System.Drawing.Point(399, 84);
            this.txtAccountingObjectName1.MaxLength = 512;
            this.txtAccountingObjectName1.Name = "txtAccountingObjectName1";
            this.txtAccountingObjectName1.Size = new System.Drawing.Size(632, 22);
            this.txtAccountingObjectName1.TabIndex = 10058;
            // 
            // grpTop
            // 
            appearance128.BackColor = System.Drawing.Color.Transparent;
            this.grpTop.Appearance = appearance128;
            this.grpTop.Controls.Add(this.lblTT);
            this.grpTop.Controls.Add(this.cbbChonLoaiXK);
            this.grpTop.Controls.Add(this.pnlTopVouchers);
            this.grpTop.Dock = System.Windows.Forms.DockStyle.Top;
            appearance130.FontData.BoldAsString = "True";
            appearance130.FontData.SizeInPoints = 13F;
            this.grpTop.HeaderAppearance = appearance130;
            this.grpTop.Location = new System.Drawing.Point(0, 0);
            this.grpTop.Name = "grpTop";
            this.grpTop.Size = new System.Drawing.Size(1063, 90);
            this.grpTop.TabIndex = 2;
            this.grpTop.Text = "CHUYỂN KHO";
            this.grpTop.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // lblTT
            // 
            this.lblTT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance129.TextVAlignAsString = "Middle";
            this.lblTT.Appearance = appearance129;
            this.lblTT.Location = new System.Drawing.Point(689, 40);
            this.lblTT.Name = "lblTT";
            this.lblTT.Size = new System.Drawing.Size(112, 22);
            this.lblTT.TabIndex = 49;
            this.lblTT.Text = "Loại xuất chuyển kho";
            // 
            // cbbChonLoaiXK
            // 
            this.cbbChonLoaiXK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbChonLoaiXK.AutoSize = false;
            this.cbbChonLoaiXK.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem6.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem6.DataValue = 0;
            valueListItem6.DisplayText = "Xuất kho kiêm vận chuyển nội bộ";
            valueListItem12.DataValue = 1;
            valueListItem12.DisplayText = "Xuất gửi đại lý";
            valueListItem2.DataValue = 2;
            valueListItem2.DisplayText = "Xuất chuyển nội bộ";
            this.cbbChonLoaiXK.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem6,
            valueListItem12,
            valueListItem2});
            this.cbbChonLoaiXK.Location = new System.Drawing.Point(812, 40);
            this.cbbChonLoaiXK.Name = "cbbChonLoaiXK";
            this.cbbChonLoaiXK.Size = new System.Drawing.Size(219, 22);
            this.cbbChonLoaiXK.TabIndex = 48;
            this.cbbChonLoaiXK.SelectionChanged += new System.EventHandler(this.cbbChonThanhToan_SelectionChanged);
            // 
            // pnlTopVouchers
            // 
            this.pnlTopVouchers.Location = new System.Drawing.Point(13, 35);
            this.pnlTopVouchers.Name = "pnlTopVouchers";
            this.pnlTopVouchers.Size = new System.Drawing.Size(298, 46);
            this.pnlTopVouchers.TabIndex = 0;
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 270);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 265;
            this.ultraSplitter1.Size = new System.Drawing.Size(1063, 14);
            this.ultraSplitter1.TabIndex = 1;
            // 
            // FRSTransferDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1063, 655);
            this.Controls.Add(this.ultraPanel1);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.ultraSplitter1);
            this.Controls.Add(this.ultraPanel3);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MinimumSize = new System.Drawing.Size(1079, 694);
            this.Name = "FRSTransferDetail";
            this.Text = "Chi tiết chuyển kho";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FRSTransferDetail_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            this.pnlUgrid.ClientArea.ResumeLayout(false);
            this.pnlUgrid.ResumeLayout(false);
            this.pnlUgrid.PerformLayout();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ultraPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).EndInit();
            this.ultraPanel3.ClientArea.ResumeLayout(false);
            this.ultraPanel3.ResumeLayout(false);
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabXuatChuyenNB)).EndInit();
            this.tabXuatChuyenNB.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbTransferMethod3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObject3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabXuatGuiDL)).EndInit();
            this.tabXuatGuiDL.ResumeLayout(false);
            this.tabXuatGuiDL.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTransferMethod2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbInvoiceTemplate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceNo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceSeries2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilizationDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilizationOrderOf2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilizationOrderNo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilizationOrderFor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObject2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabXuatKhoKiemVCNV)).EndInit();
            this.tabXuatKhoKiemVCNV.ResumeLayout(false);
            this.tabXuatKhoKiemVCNV.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTransferMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbInvoiceTemplate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceNo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceSeries1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilizationDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilizationOrderOf1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilizationOrderNo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilizationOrderFor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTop)).EndInit();
            this.grpTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbChonLoaiXK)).EndInit();
            this.pnlTopVouchers.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridControl;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
        private Infragistics.Win.Misc.UltraGroupBox grpTop;
        private Infragistics.Win.Misc.UltraPanel pnlUgrid;
        private Infragistics.Win.Misc.UltraPanel pnlTopVouchers;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraGroupBox tabXuatKhoKiemVCNV;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbTransferMethod;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbInvoiceTemplate1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtInvoiceNo1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtInvoiceSeries1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtMobilizationDate1;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectName1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMobilizationOrderOf1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMobilizationOrderNo1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMobilizationOrderFor1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName1;
        private Infragistics.Win.Misc.UltraGroupBox tabXuatChuyenNB;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbTransferMethod3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel29;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObject3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel30;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObject3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.Misc.UltraGroupBox tabXuatGuiDL;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbTransferMethod2;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbInvoiceTemplate2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtInvoiceNo2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtInvoiceSeries2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtMobilizationDate2;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObject2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMobilizationOrderOf2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMobilizationOrderNo2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMobilizationOrderFor2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObject2;
        private Infragistics.Win.Misc.UltraLabel lblTT;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbChonLoaiXK;
    }
}