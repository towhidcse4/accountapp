﻿#region

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using AutoCompleteMode = Infragistics.Win.AutoCompleteMode;
using Nullable = Infragistics.Win.UltraWinGrid.Nullable;
using System.Drawing;
using System.Reflection;
using Accounting.Core.IService;
using FX.Core;

#endregion

namespace Accounting
{
    public partial class FRSInventoryAdjustments : CustormForm
    {
        #region Khai báo
        public readonly IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
        private List<Account> _dsAccount = new List<Account>(); //Bảng Account

        private List<MaterialGoodsCustom> _dsMaterialGoods = new List<MaterialGoodsCustom>();

        private List<RSInwardOutwardDetail> _dsRSInwardOutwardDetail = new List<RSInwardOutwardDetail>();
        private List<Repository> _dsRepository = new List<Repository>(); //Bảng Repository

        private bool _isGetData;

        #endregion

        #region Khởi tạo

        public FRSInventoryAdjustments()
        {
            WaitingFrm.StartWaiting();
            #region Khởi tạo giá trị mặc định của Form

            InitializeComponent();
            Utils.SortFormControls(this);
            uGrid.CellChange += uGrid_CellChange;




            uGrid.DisplayLayout.Override.SelectedRowAppearance.BackColor = Color.White;
            uGrid.DisplayLayout.Override.HotTrackRowAppearance.ForeColor = Color.White;
            uGrid.DisplayLayout.Override.HotTrackRowSelectorAppearance.BackColor = Color.FromArgb(51, 153, 255);
            uGrid.DisplayLayout.Override.HotTrackRowCellAppearance.BackColor = Color.FromArgb(51, 153, 255);

            #endregion

            #region Thiết lập ban đầu cho Form
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
            WaitingFrm.StopWaiting();
        }

        #endregion

        #region overide

        private void InitializeGUI()
        {
            #region Fill dữ liệu vào Obj

            _dsRepository = Utils.IRepositoryService.GetAll_ByOrderbyIsActive(true);
            _dsAccount = Utils.IAccountService.GetListAccountIsActive(true);
            _dsRSInwardOutwardDetail = Utils.IRSInwardOutwardDetailService.GetAll();

            _dsMaterialGoods = Utils.IMaterialGoodsService.GetMateriaGoodAndRepositoryLedger(DateTime.Now);


            #endregion

            #region Thiết lập dữ liệu và Fill dữ liệu Obj vào control (nếu đang sửa)

            cbbRepository.DataSource = _dsRepository;
            cbbRepository.ValueMember = "ID";
            cbbRepository.DisplayMember = "RepositoryCode";
            cbbRepository.AutoCompleteMode = AutoCompleteMode.Suggest;
            cbbRepository.AutoSuggestFilterMode = AutoSuggestFilterMode.Contains;
            Utils.ConfigGrid(cbbRepository, ConstDatabase.Repository_TableName);
            if (_dsRepository.Count > 0) cbbRepository.SelectedRow = cbbRepository.Rows[0];

            cbbAccountUp.DataSource = _dsAccount;
            cbbAccountUp.DisplayMember = "AccountNumber";
            cbbAccountUp.AutoCompleteMode = AutoCompleteMode.Suggest;
            cbbAccountUp.AutoSuggestFilterMode = AutoSuggestFilterMode.Contains;
            Utils.ConfigGrid(cbbAccountUp, ConstDatabase.Account_TableName);

            cbbAccountDown.DataSource = _dsAccount;
            cbbAccountDown.DisplayMember = "AccountNumber";
            cbbAccountDown.AutoCompleteMode = AutoCompleteMode.Suggest;
            cbbAccountDown.AutoSuggestFilterMode = AutoSuggestFilterMode.Contains;
            Utils.ConfigGrid(cbbAccountDown, ConstDatabase.Account_TableName);

            ratioRepository.CheckedIndex = 0;
            ConfigGrid(uGrid);
            CheckChkIsValue();

            if (_dsAccount.Count > 0)
            {
                foreach (var row in cbbAccountUp.Rows)
                {
                    var account = row.ListObject as Account;
                    if (account != null && account.AccountNumber == "3381")
                    {
                        cbbAccountUp.SelectedRow = row;
                        cbbAccountUp.ActiveRow = row;
                        break;
                    }
                }
                foreach (var row in cbbAccountDown.Rows)
                {
                    var account = row.ListObject as Account;
                    if (account != null && account.AccountNumber == "1381")
                    {
                        cbbAccountDown.SelectedRow = row;
                        cbbAccountDown.ActiveRow = row;
                        break;
                    }
                }
                if (cbbAccountUp.SelectedRow.Selected == false)
                {
                    cbbAccountUp.SelectedRow = cbbAccountUp.Rows[0];
                    cbbAccountUp.ActiveRow = cbbAccountUp.Rows[0];
                }

                if (cbbAccountDown.SelectedRow.Selected == false)
                {
                    cbbAccountDown.SelectedRow = cbbAccountUp.Rows[0];
                    cbbAccountDown.ActiveRow = cbbAccountUp.Rows[0];
                }

            }

            #endregion
        }

        #endregion

        #region Utils

        private void ConfigGrid(UltraGrid ultraGrid)
        {
            Utils.ConfigGrid(ultraGrid, ConstDatabase.MaterialGoodsCustom_TableName, false);
            UltraGridBand band = ultraGrid.DisplayLayout.Bands[0];
            //const string nhom = "Kéo thả một tiêu đề cột vào đây để nhóm dữ liệu theo cột đó";
            //ultraGrid.DisplayLayout.GroupByBox.Prompt = nhom;
            Utils.FormatNumberic(ultraGrid.DisplayLayout.Bands[0].Columns["SumIWQuantity"],
                ConstDatabase.Format_Quantity);
            Utils.AddSumColumn(ultraGrid, "SumIWQuantity", false, "", ConstDatabase.Format_Quantity);

            Utils.FormatNumberic(ultraGrid.DisplayLayout.Bands[0].Columns["InventoryIWQuantity"],
                ConstDatabase.Format_Quantity);
            Utils.AddSumColumn(ultraGrid, "InventoryIWQuantity", false, "", ConstDatabase.Format_Quantity);

            Utils.FormatNumberic(ultraGrid.DisplayLayout.Bands[0].Columns["DiffSumIWQuantity"],
                ConstDatabase.Format_Quantity);
            Utils.AddSumColumn(ultraGrid, "DiffSumIWQuantity", false, "", ConstDatabase.Format_Quantity, false);

            Utils.FormatNumberic(ultraGrid.DisplayLayout.Bands[0].Columns["SumIWAmount"], ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(ultraGrid, "SumIWAmount", false, "", ConstDatabase.Format_TienVND, false);
            Utils.FormatNumberic(ultraGrid.DisplayLayout.Bands[0].Columns["InventoryIWAmount"],
                ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(ultraGrid, "InventoryIWAmount", false, "", ConstDatabase.Format_TienVND, false);
            Utils.FormatNumberic(ultraGrid.DisplayLayout.Bands[0].Columns["DiffSumIWAmount"],
                ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(ultraGrid, "DiffSumIWAmount", false, "", ConstDatabase.Format_TienVND, false);


            band.Columns["InventoryIWQuantity"].Nullable = Nullable.Nothing;
            band.Columns["InventoryIWQuantity"].NullText = "0.0";
            band.Columns["InventoryIWAmount"].Nullable = Nullable.Nothing;
            band.Columns["InventoryIWAmount"].NullText = "0.0";
            ultraGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;


        }

        private void CheckChkIsValue()
        {
            if (chkIsValue.CheckState == CheckState.Checked)
            {
                uGrid.DisplayLayout.Bands[0].Columns["InventoryIWAmount"].Hidden = false;
                uGrid.DisplayLayout.Bands[0].Columns["SumIWAmount"].Hidden = false;
                uGrid.DisplayLayout.Bands[0].Columns["DiffSumIWAmount"].Hidden = false;
                uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
            }
            else
            {
                uGrid.DisplayLayout.Bands[0].Columns["InventoryIWAmount"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["SumIWAmount"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["DiffSumIWAmount"].Hidden = true;
                uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            }
        }
        private void LoadDuLieu(bool isGetData = false)
        {
            DateTime dt = dteFrom.Value == null ? DateTime.Now : (DateTime)dteFrom.Value;
            List<MaterialGoodsCustom> lstTemp = new List<MaterialGoodsCustom>();

            if (isGetData)
            {
                _dsMaterialGoods = Utils.IMaterialGoodsService.GetMateriaGoodAndRepositoryLedger(dt);
            }
            if (ratioRepository.CheckedIndex == 0)
            {
                cbbRepository.Enabled = true;
                if (cbbRepository.Value != null)
                    lstTemp = _dsMaterialGoods.Where(p => p.RepositoryID == (Guid)cbbRepository.Value).ToList();
            }
            else
            {
                cbbRepository.Enabled = false;
                lstTemp = _dsMaterialGoods;
            }
            //if (cbbRepository.SelectedRow.Activated)
            //{
            //    lstTemp = lstTemp.Where(p => p.RepositoryID == ((Repository)cbbRepository.SelectedRow.ListObject).ID).ToList();
            //}
            uGrid.DataSource = lstTemp;
            if (chkIsInwardOutward.CheckState == CheckState.Checked)
            {
                uGrid.DataSource = lstTemp.Where(c => c.SumIWQuantity > 0).ToList();
            }
            //uGrid.ResetDisplayLayout();
            uGrid.Layouts.Clear();

            foreach (var row in uGrid.Rows)
            {
                Calculator(row);
            }
        }

        private void Calculator(UltraGridRow row)
        {
            try
            {
                if (row.Cells["InventoryIWQuantity"].Value == null)
                    row.Cells["InventoryIWQuantity"].Value = row.Cells["SumIWQuantity"].Value;
                if (row.Cells["InventoryIWAmount"].Value == null)
                    row.Cells["InventoryIWAmount"].Value = row.Cells["SumIWAmount"].Value;

                row.Cells["DiffSumIWQuantity"].Value = (row.Cells["InventoryIWQuantity"].Value == null
                    ? 0
                    : (decimal)row.Cells["InventoryIWQuantity"].Value) -
                                                       (decimal)row.Cells["SumIWQuantity"].Value;
                row.Cells["DiffSumIWAmount"].Value = (row.Cells["InventoryIWAmount"].Value == null
                    ? 0
                    : (decimal)row.Cells["InventoryIWAmount"].Value) -
                                                     (decimal)row.Cells["SumIWAmount"].Value;
            }
            catch (Exception)
            {

                if (row.ChildBands.LastRow.Cells["InventoryIWQuantity"].Value == null)
                    row.ChildBands.LastRow.Cells["InventoryIWQuantity"].Value = row.ChildBands.LastRow.Cells["SumIWQuantity"].Value;
                if (row.ChildBands.LastRow.Cells["InventoryIWAmount"].Value == null)
                    row.ChildBands.LastRow.Cells["InventoryIWAmount"].Value = row.ChildBands.LastRow.Cells["SumIWAmount"].Value;

                row.ChildBands.LastRow.Cells["DiffSumIWQuantity"].Value = (row.ChildBands.LastRow.Cells["InventoryIWQuantity"].Value == null
                    ? 0
                    : (decimal)row.ChildBands.LastRow.Cells["InventoryIWQuantity"].Value) -
                                                       (decimal)row.ChildBands.LastRow.Cells["SumIWQuantity"].Value;
                row.ChildBands.LastRow.Cells["DiffSumIWAmount"].Value = (row.ChildBands.LastRow.Cells["InventoryIWAmount"].Value == null
                    ? 0
                    : (decimal)row.ChildBands.LastRow.Cells["InventoryIWAmount"].Value) -
                                                     (decimal)row.ChildBands.LastRow.Cells["SumIWAmount"].Value;

            }

        }

        private bool Saveleged(string no, bool isInward)
        {
            // luu d? li?u phi?u thu vào các b?ng s? cái và c?p nh?t tr?ng thái c?a phi?u thu
            // 1. Luu d? li?u vào b?ng s? cái GenegedLeged
            // 2. C?p nh?t tr?ng thái c?a phi?u thu

            #region th?c hi?n set giá tr? post = true

            RSInwardOutward temp = Utils.IRSInwardOutwardService.getMCRSInwardOutwardbyNo(no);
            if (temp == null)
            {
                return false;
            }
            List<RSInwardOutwardDetail> listTempDetail = Utils.IRSInwardOutwardDetailService.getRSInwardOutwardDetaillbyID(temp.ID).ToList();
            RepositoryLedger repositoryTemp = new RepositoryLedger
            {
                Reason = temp.Reason
            };
            return Utils.SaveLedger(temp);
            #endregion
        }

        private bool Add(RSInwardOutward rsInwardOutward, string no, bool isInward)
        {
            #region Thao tác CSDL
            try
            {
                Utils.IRSInwardOutwardService.BeginTran();

                //IPPDiscountReturnService.CommitChanges();
                Utils.IRSInwardOutwardService.AddRSInwardOutward(rsInwardOutward);

                #region Up bảng gencode sinh chứng từ tiếp theo

                GenCode gencode =
                    Utils.IGenCodeService.getGenCode(isInward
                        ? ConstFrm.TypeGroup_RSInwardOutward
                        : ConstFrm.TypeGroup_RSInwardOutwardOutput);
                Dictionary<string, string> dicNo = Utils.CheckNo(no);
                if (dicNo != null)
                {
                    gencode.Prefix = dicNo["Prefix"];
                    decimal _decimal;
                    decimal.TryParse(dicNo["Value"], out _decimal);
                    gencode.CurrentValue = _decimal + 1;
                    gencode.Suffix = dicNo["Suffix"];
                    Utils.IGenCodeService.Update(gencode);
                }

                #endregion

                try
                {
                    Utils.IRSInwardOutwardService.CommitTran();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                Reset(rsInwardOutward);
                return true;
            }
            catch (Exception ex)
            {
                Utils.IRSInwardOutwardService.RolbackTran();
                return false;
            }

            #endregion
        }

        public virtual void Reset(RSInwardOutward item)
        {
            Utils.ClearCache();
            PropertyInfo propId = item.GetType().GetProperty("ID", BindingFlags.Public | BindingFlags.Instance);
            if (null != propId && propId.CanRead)
            {
                object temp = propId.GetValue(item, null);
                if (temp is Guid)
                {
                    var currentId = (Guid)temp;
                    try
                    {
                        item = Utils.IRSInwardOutwardService.Getbykey(currentId);
                        Utils.IRSInwardOutwardService.UnbindSession(item);
                        item = Utils.IRSInwardOutwardService.Getbykey(currentId);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(resSystem.MSG_Error_13);
                    }
                }
            }
        }

        private bool CheckError()
        {
            if (cbbAccountDown.Value == null)
            {
                MSG.Error("Bạn chưa chọn Tài khoản đ/c giảm.");
                return false;
            }
            if (cbbAccountUp.Value == null)
            {
                MSG.Error("Bạn chưa chọn Tài khoản đ/c tăng");
                return false;
            }
            return true;
        }

        #endregion

        #region Event

        private void ratioRepository_ValueChanged(object sender, EventArgs e)
        {
            LoadDuLieu(_isGetData);
        }

        private void cbbRepository_RowSelected(object sender, RowSelectedEventArgs e)
        {

            LoadDuLieu(_isGetData);
        }

        private void chkIsValue_CheckedChanged(object sender, EventArgs e)
        {
            CheckChkIsValue();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            UltraGrid ultraGrid = (UltraGrid)sender;
            ultraGrid.UpdateData();
            if (!e.Cell.Row.IsFilterRow)
                Calculator(ultraGrid.Rows[e.Cell.Row.Index]);
        }

        private void chkIsInwardOutward_CheckedChanged(object sender, EventArgs e)
        {
            LoadDuLieu(_isGetData);
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            _isGetData = true;
            LoadDuLieu(_isGetData);
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                if (CheckError())
                    using (var msg = new MsgFRSInventoryAdjustments())
                    {
                        msg.FormClosed += Msg_FormClosed;
                        msg.Closing += msg_Closing;
                        msg.ShowDialog(this);
                    }
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                return;
            }
        }

        private void msg_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //check ngay khoa so ky ke toan;
        }

        private void Msg_FormClosed(object sender, FormClosedEventArgs e)
        {
            var msg = (MsgFRSInventoryAdjustments)sender;
            if (e.CloseReason == CloseReason.UserClosing)
                if (msg.DialogResult == DialogResult.OK)
                {
                    List<MaterialGoodsCustom> lstTempMaterialGoods = new List<MaterialGoodsCustom>();
                    bool check = true;
                    lstTempMaterialGoods = (List<MaterialGoodsCustom>)uGrid.DataSource;
                    foreach (var row in lstTempMaterialGoods)
                    {
                        string no;
                        RSInwardOutward temp = new RSInwardOutward
                        {
                            ID = Guid.NewGuid(),
                            Date = msg._refDateTime,
                            PostedDate = msg._refDateTime,
                            Exported = true,
                            RefDateTime = msg.RefDateTime
                        };
                        if (row.DiffSumIWQuantity > 0 || row.DiffSumIWAmount > 0)
                        {
                            //Phát hiện thừa thì Nhập kho
                            no = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(ConstFrm.TypeGroup_RSInwardOutward));
                            temp.No = no;
                            temp.TypeID = 401;
                            temp.Reason = "Kiểm kê phát hiện thừa";
                            temp.CurrencyID = "VND";
                            temp.ExchangeRate = 1;
                            temp.IsInwardOutwardType = 0;
                            RSInwardOutwardDetail rsInwardOutwardDetail = new RSInwardOutwardDetail();
                            rsInwardOutwardDetail.MaterialGoodsID = row.ID;
                            rsInwardOutwardDetail.Description = row.MaterialGoodsName;
                            rsInwardOutwardDetail.RepositoryID = row.RepositoryID;
                            rsInwardOutwardDetail.DebitAccount = row.ReponsitoryAccount;
                            rsInwardOutwardDetail.CreditAccount = cbbAccountUp.Value.ToString();
                            rsInwardOutwardDetail.Quantity = row.DiffSumIWQuantity;
                            //rsInwardOutwardDetail.UnitPriceOriginal = row.DiffSumIWAmount; 
                            //rsInwardOutwardDetail.UnitPrice = rsInwardOutwardDetail.UnitPriceOriginal *
                            //                                  (temp.ExchangeRate == null
                            //                                      ? 1
                            //                                      : (decimal)temp.ExchangeRate);
                            rsInwardOutwardDetail.AmountOriginal = row.DiffSumIWAmount; //rsInwardOutwardDetail.UnitPriceOriginal *
                                                                                        //(rsInwardOutwardDetail.Quantity == null
                                                                                        //? 0
                                                                                        //: (decimal)rsInwardOutwardDetail.Quantity);
                            rsInwardOutwardDetail.Amount = rsInwardOutwardDetail.AmountOriginal *
                                                           (temp.ExchangeRate == null
                                                               ? 1
                                                               : (decimal)temp.ExchangeRate);
                            rsInwardOutwardDetail.UnitPriceConvertOriginal = 0;
                            rsInwardOutwardDetail.UnitPriceConvert = 0;
                            rsInwardOutwardDetail.Unit = row.Unit;
                            rsInwardOutwardDetail.UnitConvert = row.Unit;
                            rsInwardOutwardDetail.RSInwardOutwardID = temp.ID;
                            temp.RSInwardOutwardDetails = new List<RSInwardOutwardDetail> { rsInwardOutwardDetail };
                            temp.TotalAmountOriginal = temp.RSInwardOutwardDetails.Sum(x => x.AmountOriginal);
                            rsInwardOutwardDetail.OWPurpose = -1;
                            check = Add(temp, no, true);
                            if (!check) break;
                            check = Saveleged(no, true);
                            if (!check) break;
                        }
                        else if (row.DiffSumIWQuantity < 0 || row.DiffSumIWAmount < 0)
                        {
                            //Phát hiện thiếu thì Xuất kho
                            no =
                                Utils.TaoMaChungTu(
                                    Utils.IGenCodeService.getGenCode(ConstFrm.TypeGroup_RSInwardOutwardOutput));
                            temp.No = no;
                            temp.TypeID = 414;
                            temp.Reason = "Kiểm kê phát hiện thiếu";
                            temp.CurrencyID = "VND";
                            temp.ExchangeRate = 1;
                            temp.IsInwardOutwardType = 1;
                            temp.IsOutwardSAInvoice = false;
                            RSInwardOutwardDetail rsInwardOutwardDetail = new RSInwardOutwardDetail
                            {
                                MaterialGoodsID = row.ID,
                                Description = row.MaterialGoodsName,
                                RepositoryID = row.RepositoryID,
                                DebitAccount = cbbAccountDown.Value.ToString(),
                                CreditAccount = row.ReponsitoryAccount,
                                Quantity = -row.DiffSumIWQuantity,
                                //UnitPriceOriginal = -(row.DiffSumIWAmount)
                            };
                            //rsInwardOutwardDetail.UnitPrice = rsInwardOutwardDetail.UnitPriceOriginal *
                            //                                  (temp.ExchangeRate == null
                            //                                      ? 1
                            //                                      : (decimal)temp.ExchangeRate);
                            rsInwardOutwardDetail.AmountOriginal = -(row.DiffSumIWAmount);//rsInwardOutwardDetail.UnitPriceOriginal *
                                                                                          //(rsInwardOutwardDetail.Quantity == null
                                                                                          //? 0
                                                                                          //: (decimal)rsInwardOutwardDetail.Quantity);
                            rsInwardOutwardDetail.Amount = rsInwardOutwardDetail.AmountOriginal *
                                                           (temp.ExchangeRate == null
                                                               ? 1
                                                               : (decimal)temp.ExchangeRate);
                            rsInwardOutwardDetail.UnitPriceConvertOriginal = 0;
                            rsInwardOutwardDetail.UnitPriceConvert = 0;
                            rsInwardOutwardDetail.Unit = row.Unit;
                            rsInwardOutwardDetail.UnitConvert = row.Unit;
                            rsInwardOutwardDetail.OWPurpose = 0;
                            rsInwardOutwardDetail.RSInwardOutwardID = temp.ID;
                            temp.RSInwardOutwardDetails = new List<RSInwardOutwardDetail> { rsInwardOutwardDetail };
                            temp.TotalAmountOriginal = temp.RSInwardOutwardDetails.Sum(x => x.AmountOriginal);
                            check = Add(temp, no, false);
                            if (!check) break;
                            check = Saveleged(no, false);
                            if (!check) break;
                        }
                    }
                    if (check)
                    {
                        MessageBox.Show("Điều chỉnh thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        btnGetData_Click(sender, null);
                    }
                    else
                    {
                        MSG.Error("Điều chỉnh thất bại!");
                    }
                }
        }

        #endregion
    }
}