﻿namespace Accounting
{
    partial class FRSInventoryAdjustments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRSInventoryAdjustments));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnApply = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.dteFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.chkIsValue = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.cbbAccountDown = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label3 = new System.Windows.Forms.Label();
            this.cbbAccountUp = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGetData = new Infragistics.Win.Misc.UltraButton();
            this.label1 = new System.Windows.Forms.Label();
            this.chkIsInwardOutward = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.cbbRepository = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ratioRepository = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsInwardOutward)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbRepository)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ratioRepository)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            this.btnCancel.Appearance = appearance1;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(865, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 27);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Đóng";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // uGrid
            // 
            this.uGrid.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(10, 77);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(943, 419);
            this.uGrid.TabIndex = 3;
            this.uGrid.Text = "Danh sách VTHH";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnApply);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(10, 496);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(943, 43);
            this.panel1.TabIndex = 2;
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnApply.Appearance = appearance2;
            this.btnApply.Location = new System.Drawing.Point(762, 3);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(97, 28);
            this.btnApply.TabIndex = 1;
            this.btnApply.Text = "Điều chỉnh";
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.dteFrom);
            this.ultraGroupBox1.Controls.Add(this.chkIsValue);
            this.ultraGroupBox1.Controls.Add(this.cbbAccountDown);
            this.ultraGroupBox1.Controls.Add(this.label3);
            this.ultraGroupBox1.Controls.Add(this.cbbAccountUp);
            this.ultraGroupBox1.Controls.Add(this.label2);
            this.ultraGroupBox1.Controls.Add(this.btnGetData);
            this.ultraGroupBox1.Controls.Add(this.label1);
            this.ultraGroupBox1.Controls.Add(this.chkIsInwardOutward);
            this.ultraGroupBox1.Controls.Add(this.cbbRepository);
            this.ultraGroupBox1.Controls.Add(this.ratioRepository);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.Location = new System.Drawing.Point(10, 10);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(943, 67);
            this.ultraGroupBox1.TabIndex = 0;
            // 
            // dteFrom
            // 
            this.dteFrom.AutoSize = false;
            this.dteFrom.Location = new System.Drawing.Point(310, 10);
            this.dteFrom.MaskInput = "dd/mm/yyyy";
            this.dteFrom.Name = "dteFrom";
            this.dteFrom.Size = new System.Drawing.Size(86, 22);
            this.dteFrom.TabIndex = 11;
            // 
            // chkIsValue
            // 
            this.chkIsValue.BackColor = System.Drawing.Color.Transparent;
            this.chkIsValue.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsValue.Location = new System.Drawing.Point(539, 41);
            this.chkIsValue.Name = "chkIsValue";
            this.chkIsValue.Size = new System.Drawing.Size(128, 24);
            this.chkIsValue.TabIndex = 10;
            this.chkIsValue.Text = "Kiểm kê theo giá trị";
            this.chkIsValue.CheckedChanged += new System.EventHandler(this.chkIsValue_CheckedChanged);
            // 
            // cbbAccountDown
            // 
            this.cbbAccountDown.AutoSize = false;
            this.cbbAccountDown.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            appearance3.BackColor = System.Drawing.SystemColors.Window;
            appearance3.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountDown.DisplayLayout.Appearance = appearance3;
            this.cbbAccountDown.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountDown.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance4.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance4.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountDown.DisplayLayout.GroupByBox.Appearance = appearance4;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountDown.DisplayLayout.GroupByBox.BandLabelAppearance = appearance5;
            this.cbbAccountDown.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance6.BackColor2 = System.Drawing.SystemColors.Control;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountDown.DisplayLayout.GroupByBox.PromptAppearance = appearance6;
            this.cbbAccountDown.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountDown.DisplayLayout.MaxRowScrollRegions = 1;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountDown.DisplayLayout.Override.ActiveCellAppearance = appearance7;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountDown.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.cbbAccountDown.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountDown.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountDown.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountDown.DisplayLayout.Override.CellAppearance = appearance10;
            this.cbbAccountDown.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountDown.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountDown.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance12.TextHAlignAsString = "Left";
            this.cbbAccountDown.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.cbbAccountDown.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountDown.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountDown.DisplayLayout.Override.RowAppearance = appearance13;
            this.cbbAccountDown.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountDown.DisplayLayout.Override.TemplateAddRowAppearance = appearance14;
            this.cbbAccountDown.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountDown.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountDown.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountDown.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.ScenicRibbon;
            this.cbbAccountDown.Location = new System.Drawing.Point(845, 10);
            this.cbbAccountDown.Name = "cbbAccountDown";
            this.cbbAccountDown.Size = new System.Drawing.Size(84, 22);
            this.cbbAccountDown.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(735, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = " Tài khoản đ/c giảm";
            // 
            // cbbAccountUp
            // 
            this.cbbAccountUp.AutoSize = false;
            this.cbbAccountUp.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountUp.DisplayLayout.Appearance = appearance15;
            this.cbbAccountUp.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountUp.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountUp.DisplayLayout.GroupByBox.Appearance = appearance16;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountUp.DisplayLayout.GroupByBox.BandLabelAppearance = appearance17;
            this.cbbAccountUp.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountUp.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.cbbAccountUp.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountUp.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountUp.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountUp.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.cbbAccountUp.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountUp.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountUp.DisplayLayout.Override.CardAreaAppearance = appearance21;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            appearance22.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountUp.DisplayLayout.Override.CellAppearance = appearance22;
            this.cbbAccountUp.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountUp.DisplayLayout.Override.CellPadding = 0;
            appearance23.BackColor = System.Drawing.SystemColors.Control;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountUp.DisplayLayout.Override.GroupByRowAppearance = appearance23;
            appearance24.TextHAlignAsString = "Left";
            this.cbbAccountUp.DisplayLayout.Override.HeaderAppearance = appearance24;
            this.cbbAccountUp.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountUp.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountUp.DisplayLayout.Override.RowAppearance = appearance25;
            this.cbbAccountUp.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountUp.DisplayLayout.Override.TemplateAddRowAppearance = appearance26;
            this.cbbAccountUp.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountUp.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountUp.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountUp.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.ScenicRibbon;
            this.cbbAccountUp.Location = new System.Drawing.Point(645, 10);
            this.cbbAccountUp.Name = "cbbAccountUp";
            this.cbbAccountUp.Size = new System.Drawing.Size(84, 22);
            this.cbbAccountUp.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(536, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = " Tài khoản đ/c tăng";
            // 
            // btnGetData
            // 
            this.btnGetData.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnGetData.Location = new System.Drawing.Point(402, 10);
            this.btnGetData.Name = "btnGetData";
            this.btnGetData.Size = new System.Drawing.Size(75, 23);
            this.btnGetData.TabIndex = 5;
            this.btnGetData.Text = "Lấy số liệu";
            this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(211, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Kiểm kê đến ngày";
            // 
            // chkIsInwardOutward
            // 
            this.chkIsInwardOutward.BackColor = System.Drawing.Color.Transparent;
            this.chkIsInwardOutward.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsInwardOutward.ButtonStyle = Infragistics.Win.UIElementButtonStyle.ScenicRibbonButton;
            this.chkIsInwardOutward.Location = new System.Drawing.Point(109, 42);
            this.chkIsInwardOutward.Name = "chkIsInwardOutward";
            this.chkIsInwardOutward.Size = new System.Drawing.Size(159, 20);
            this.chkIsInwardOutward.TabIndex = 2;
            this.chkIsInwardOutward.Text = "Chỉ lấy vật tư có phát sinh";
            this.chkIsInwardOutward.CheckedChanged += new System.EventHandler(this.chkIsInwardOutward_CheckedChanged);
            // 
            // cbbRepository
            // 
            this.cbbRepository.AutoSize = false;
            this.cbbRepository.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbRepository.DisplayLayout.Appearance = appearance27;
            this.cbbRepository.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbRepository.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance28.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance28.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance28.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbRepository.DisplayLayout.GroupByBox.Appearance = appearance28;
            appearance29.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbRepository.DisplayLayout.GroupByBox.BandLabelAppearance = appearance29;
            this.cbbRepository.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance30.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance30.BackColor2 = System.Drawing.SystemColors.Control;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance30.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbRepository.DisplayLayout.GroupByBox.PromptAppearance = appearance30;
            this.cbbRepository.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbRepository.DisplayLayout.MaxRowScrollRegions = 1;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            appearance31.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbRepository.DisplayLayout.Override.ActiveCellAppearance = appearance31;
            appearance32.BackColor = System.Drawing.SystemColors.Highlight;
            appearance32.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbRepository.DisplayLayout.Override.ActiveRowAppearance = appearance32;
            this.cbbRepository.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbRepository.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            this.cbbRepository.DisplayLayout.Override.CardAreaAppearance = appearance33;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            appearance34.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbRepository.DisplayLayout.Override.CellAppearance = appearance34;
            this.cbbRepository.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbRepository.DisplayLayout.Override.CellPadding = 0;
            appearance35.BackColor = System.Drawing.SystemColors.Control;
            appearance35.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance35.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance35.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance35.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbRepository.DisplayLayout.Override.GroupByRowAppearance = appearance35;
            appearance36.TextHAlignAsString = "Left";
            this.cbbRepository.DisplayLayout.Override.HeaderAppearance = appearance36;
            this.cbbRepository.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbRepository.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.Color.Silver;
            this.cbbRepository.DisplayLayout.Override.RowAppearance = appearance37;
            this.cbbRepository.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbRepository.DisplayLayout.Override.TemplateAddRowAppearance = appearance38;
            this.cbbRepository.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbRepository.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbRepository.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbRepository.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.ScenicRibbon;
            this.cbbRepository.Location = new System.Drawing.Point(56, 10);
            this.cbbRepository.Name = "cbbRepository";
            this.cbbRepository.Size = new System.Drawing.Size(149, 22);
            this.cbbRepository.TabIndex = 1;
            this.cbbRepository.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbRepository_RowSelected);
            // 
            // ratioRepository
            // 
            this.ratioRepository.BackColor = System.Drawing.Color.Transparent;
            this.ratioRepository.BackColorInternal = System.Drawing.Color.Transparent;
            this.ratioRepository.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem1.DataValue = "Default Item";
            valueListItem1.DisplayText = "Kho";
            valueListItem2.DataValue = "ValueListItem1";
            valueListItem2.DisplayText = "Tất cả các kho";
            this.ratioRepository.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.ratioRepository.ItemSpacingVertical = 15;
            this.ratioRepository.Location = new System.Drawing.Point(10, 7);
            this.ratioRepository.Name = "ratioRepository";
            this.ratioRepository.Size = new System.Drawing.Size(96, 56);
            this.ratioRepository.TabIndex = 0;
            this.ratioRepository.ValueChanged += new System.EventHandler(this.ratioRepository_ValueChanged);
            // 
            // FRSInventoryAdjustments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(963, 549);
            this.Controls.Add(this.uGrid);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ultraGroupBox1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(971, 574);
            this.Name = "FRSInventoryAdjustments";
            this.Padding = new System.Windows.Forms.Padding(10, 10, 10, 10);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Điều chỉnh tồn kho";
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsInwardOutward)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbRepository)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ratioRepository)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet ratioRepository;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsValue;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountDown;
        private System.Windows.Forms.Label label3;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountUp;
        private System.Windows.Forms.Label label2;
        private Infragistics.Win.Misc.UltraButton btnGetData;
        private System.Windows.Forms.Label label1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsInwardOutward;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbRepository;
        private Infragistics.Win.Misc.UltraButton btnApply;
        private Infragistics.Win.Misc.UltraButton btnCancel;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteFrom;
    }
}