﻿namespace Accounting
{
    partial class MsgFRSInventoryAdjustments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MsgFRSInventoryAdjustments));
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            this.lblDescription = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.btnApply = new Infragistics.Win.Misc.UltraButton();
            this.lblImage = new Infragistics.Win.Misc.UltraLabel();
            this.dteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDescription
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.lblDescription.Appearance = appearance1;
            this.lblDescription.Location = new System.Drawing.Point(51, 12);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(295, 46);
            this.lblDescription.TabIndex = 2;
            this.lblDescription.Text = "Nếu kiểm kê thiếu hệ thống sinh Phiếu xuất kho, nếu thừa hệ thống sinh Phiếu nhập" +
    " kho. Để hoàn thành việc kiểm kê bạn cần nhập ngày chứng từ xuất (nhập).";
            // 
            // ultraLabel1
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Location = new System.Drawing.Point(13, 64);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(141, 23);
            this.ultraLabel1.TabIndex = 3;
            this.ultraLabel1.Text = "Ngày chứng từ xuất (nhập)";
            // 
            // btnCancel
            // 
            appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
            this.btnCancel.Appearance = appearance3;
            this.btnCancel.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(263, 98);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 27);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            appearance4.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnApply.Appearance = appearance4;
            this.btnApply.Location = new System.Drawing.Point(172, 98);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(85, 27);
            this.btnApply.TabIndex = 6;
            this.btnApply.Text = "Đồng ý";
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // lblImage
            // 
            appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
            this.lblImage.Appearance = appearance5;
            this.lblImage.ImageSize = new System.Drawing.Size(32, 32);
            this.lblImage.Location = new System.Drawing.Point(13, 16);
            this.lblImage.Name = "lblImage";
            this.lblImage.Size = new System.Drawing.Size(32, 32);
            this.lblImage.TabIndex = 7;
            // 
            // dteDate
            // 
            this.dteDate.AutoSize = false;
            this.dteDate.Location = new System.Drawing.Point(160, 64);
            this.dteDate.MaskInput = "dd/mm/yyyy";
            this.dteDate.Name = "dteDate";
            this.dteDate.Size = new System.Drawing.Size(119, 22);
            this.dteDate.TabIndex = 8;
            // 
            // MsgFRSInventoryAdjustments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(348, 131);
            this.Controls.Add(this.dteDate);
            this.Controls.Add(this.lblImage);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.lblDescription);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "MsgFRSInventoryAdjustments";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chú ý";
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel lblDescription;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraButton btnCancel;
        private Infragistics.Win.Misc.UltraButton btnApply;
        private Infragistics.Win.Misc.UltraLabel lblImage;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDate;
    }
}