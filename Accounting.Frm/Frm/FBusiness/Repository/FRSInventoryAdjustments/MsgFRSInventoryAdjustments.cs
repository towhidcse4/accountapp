﻿#region

using System;
using System.Windows.Forms;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinEditors;

#endregion

namespace Accounting
{
    public partial class MsgFRSInventoryAdjustments : CustormForm
    {
        public DateTime _date;
        public DateTime _refDateTime;

        public MsgFRSInventoryAdjustments()
        {
            InitializeComponent();
            dteDate.Value = Utils.StringToDateTime(ConstFrm.DbStartDate);
            addBtnRefDateTime();
        }

        public MsgFRSInventoryAdjustments(string title, string description)
        {
            InitializeComponent();
            dteDate.Value = Utils.StringToDateTime(ConstFrm.DbStartDate);
            Text = title;
            lblDescription.Text = description;
            addBtnRefDateTime();
        }

        private void addBtnRefDateTime()
        {
            var btnRefDateTime = new EditorButton("btnRefDateTime");
            var appearance = new Infragistics.Win.Appearance
            {
                Image = Properties.Resources.clock,
                ImageHAlign = Infragistics.Win.HAlign.Center,
                ImageVAlign = Infragistics.Win.VAlign.Middle
            };
            btnRefDateTime.Appearance = appearance;
            btnRefDateTime.Key = "btnRefDateTime";
            btnRefDateTime.Click += (s, e) => btnRefDateTime_Click(s, e);
            dteDate.ButtonsRight.Add(btnRefDateTime);
            _refDateTime = dteDate.DateTime;
            dteDate.ValueChanged += new EventHandler(dteDate_ValueChanged);
        }

        private void dteDate_ValueChanged(object sender, EventArgs e)
        {
            _date = dteDate.DateTime;

            DateTime result = new DateTime(_date.Year, _date.Month, _date.Day, _refDateTime.Hour, _refDateTime.Minute, _refDateTime.Second);

            _refDateTime = result;
        }

        private void btnRefDateTime_Click(object sender, EditorButtonEventArgs e)
        {
            try
            {
                DateTime refTime = new DateTime(_date.Year, _date.Month, _date.Day, _refDateTime.Hour, _refDateTime.Minute, _refDateTime.Second);
                //if (!_objectDataSource.HasProperty("TypeID")) return;
                //var TypeId = (int?)_objectDataSource.GetProperty("TypeID");
                //if (new[] { 900, 901 }.Any(x => x == TypeId))
                //{
                //    if (_noBindName == "IWNo")
                //    {
                //        if (input.HasProperty("RefDateTimeIn"))
                //            refTime = (DateTime?)input.GetProperty("RefDateTimeIn") ?? refTime;
                //    }
                //    else if (_noBindName == "OWNo")
                //    {
                //        if (input.HasProperty("RefDateTimeOut"))
                //            refTime = (DateTime?)input.GetProperty("RefDateTimeOut") ?? refTime;
                //    }
                //}
                //else if (input.HasProperty("RefDateTime"))
                //    refTime = (DateTime?)input.GetProperty("RefDateTime") ?? refTime;
                var f = new MsgRefDateTime(refTime);
                f.FormClosed += new FormClosedEventHandler(MsgRefDateTime_FormClosed);
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }

        private void MsgRefDateTime_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason != CloseReason.UserClosing) return;
            var frm = (MsgRefDateTime)sender;
            if (frm.DialogResult != DialogResult.OK) return;
            var refTime = frm.RefDateTime;
            _refDateTime = refTime;
            //var refDateTime = GetRefDateTime(refTime, Utils.StringToDateTime(dtePostedDate.Text) ?? DateTime.Now);
            //if (_objectDataSource.HasProperty("TypeID") && new[] { 900, 901 }.Any(x => x == (int?)_objectDataSource.GetProperty("TypeID")))
            //{
            //    if (_noBindName == "IWNo")
            //        AddRefDateTimeByKey("RefDateTimeIn", refDateTime);
            //    else if (_noBindName == "OWNo")
            //        AddRefDateTimeByKey("RefDateTimeOut", refDateTime);
            //}
            //else
            //    AddRefDateTimeByKey("RefDateTime", refDateTime);
        }

        public DateTime Date
        {
            get { return _date; }
        }

        public DateTime RefDateTime
        {
            get { return _refDateTime; }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            //IsApply = true;            
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            //IsApply = false;
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}