﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FRSSelectIW : CustormForm
    {
        private readonly IRepositoryLedgerService IRepositoryLedgerService;
        public List<RepositoryLedger> lstRepositoryLedger = new List<RepositoryLedger>();
        public RepositoryLedger repository = new RepositoryLedger();
        public FRSSelectIW(Guid ID)
        {
            IRepositoryLedgerService = IoC.Resolve<IRepositoryLedgerService>();
            InitializeComponent();
            ReportUtils.ProcessControls(this);
            cbbDateTime.SelectedRow = cbbDateTime.Rows[3];
            lstRepositoryLedger = IRepositoryLedgerService.GetByMaterialGoods(ID);
            uGridSelectMG.SetDataBinding(lstRepositoryLedger.Where(x => x.Date >= (DateTime)dtBeginDate.Value && x.Date <= (DateTime)dtEndDate.Value).ToList(), "");
            UltraGridBand band3 = uGridSelectMG.DisplayLayout.Bands[0];
            foreach (UltraGridColumn column in band3.Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGridSelectMG, true);
            }
            Utils.ConfigGrid(uGridSelectMG, ConstDatabase.RepositoryLedger_TableName);
            uGridSelectMG.DisplayLayout.Bands[0].Columns["RepositoryID"].LockColumn();
            uGridSelectMG.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGridSelectMG.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            uGridSelectMG.DisplayLayout.Override.FilterClearButtonLocation = FilterClearButtonLocation.Row;
            uGridSelectMG.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGridSelectMG.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
        }

        private void uGridSelectMG_CellChange(object sender, CellEventArgs e)
        {
            uGridSelectMG.UpdateData();
        }

        private void uBtnHuyBo_Click(object sender, EventArgs e)
        {
            uGridSelectMG.UpdateData();
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void uBtnDongY_Click(object sender, EventArgs e)
        {
            uGridSelectMG.UpdateData();
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void uGridSelectMG_BeforeRowActivate(object sender, RowEventArgs e)
        {
            if (e.Row == null) return;
                repository = e.Row.ListObject as RepositoryLedger;
        }

        private void uGridSelectMG_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (uGridSelectMG.Selected.Rows.Count > 0)
            {
                repository = uGridSelectMG.ActiveRow.ListObject as RepositoryLedger;
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            List<RepositoryLedger> lst = new List<RepositoryLedger>();
            lst = lstRepositoryLedger.Where(x => x.Date >= (DateTime)dtBeginDate.Value && x.Date <= (DateTime)dtEndDate.Value).ToList();
            uGridSelectMG.SetDataBinding(lst, "");
        }
    }
}
