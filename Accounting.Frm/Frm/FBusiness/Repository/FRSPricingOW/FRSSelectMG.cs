﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FRSSelectMG : CustormForm
    {
        private readonly IMaterialGoodsService _IMaterialGoodsService;
        public List<MaterialGoods> _listMaterialGoods;
        public FRSSelectMG()
        {
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            InitializeComponent();
            _listMaterialGoods = _IMaterialGoodsService.GetByIsActive(true);
            uGridSelectMG.SetDataBinding(_listMaterialGoods, "");
            Utils.ConfigGrid(uGridSelectMG, ConstDatabase.MaterialGoods_TableName_FRSSelectMG);
            uGridSelectMG.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            uGridSelectMG.DisplayLayout.Override.FilterClearButtonLocation = FilterClearButtonLocation.Row;
            uGridSelectMG.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGridSelectMG.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGridSelectMG.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGridSelectMG.DisplayLayout.Bands[0].Summaries.Clear();
            uGridSelectMG.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            UltraGridBand band = uGridSelectMG.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            foreach (var row in uGridSelectMG.Rows) row.Cells["Status"].Value = false;
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;
        }

        private void uGridSelectMG_CellChange(object sender, CellEventArgs e)
        {
            uGridSelectMG.UpdateData();
        }

        private void uBtnHuyBo_Click(object sender, EventArgs e)
        {
            uGridSelectMG.UpdateData();
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void uBtnDongY_Click(object sender, EventArgs e)
        {
            uGridSelectMG.UpdateData();
            DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
