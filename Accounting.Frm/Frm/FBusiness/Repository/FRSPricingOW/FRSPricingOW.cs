﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FRSPricingOW : CustormForm
    {
        public IMaterialGoodsService _IMaterialGoodsService { get { return IoC.Resolve<IMaterialGoodsService>(); } }
        public IRepositoryLedgerService _IRepositoryLedgerService { get { return IoC.Resolve<IRepositoryLedgerService>(); } }
        public ISystemOptionService _ISystemOptionService { get { return IoC.Resolve<ISystemOptionService>(); } }
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        private readonly List<Item> _lstItemsPeriod = Utils.ObjConstValue.TypePeriod;
        private DateTime ngayHoachToan;

        private List<MaterialGoods> _lstMaterialGoods;
        private int _counter = 1;
        FRSSelectMG fRSSelectMG = new FRSSelectMG();
        private string VTHH_PPTinhGiaXKho = "";
        public FRSPricingOW()
        {
            //WaitingFrm.StartWaiting();
            InitializeComponent();
            VTHH_PPTinhGiaXKho = _ISystemOptionService.GetByCode("VTHH_PPTinhGiaXKho").Data;
            const string s =
                "Hệ thống sẽ không cập nhật giá xuất kho cho các vật tư có tồn đầu kỳ âm." +
                "\nThao tác tính giá xuất kho sẽ được tính một lần vào cuối kỳ";
            lbDescription.Text = s;

            gbDescription.Text = "Phương pháp tính giá đang áp dụng: " + VTHH_PPTinhGiaXKho;
            HideLoading(false);
            cbbAboutTime.ValueChanged += cbbAboutTime_ValueChanged;
            ngayHoachToan = (DateTime)Utils.StringToDateTime(ConstFrm.DbStartDate);
            btnSelect.Enabled = false;
            //cbbAboutTime.DataSource = _lstItems;
            //cbbAboutTime.DisplayMember = "Name";
            //Utils.ConfigGrid(cbbAboutTime, ConstDatabase.ConfigXML_TableName);
            //cbbAboutTime.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;

            cbbAboutTime.ConfigComboSelectTime(_lstItems, "Name");
            cbbAboutTime.SelectedRow = cbbAboutTime.Rows[3];
            var thisMonthStart = ngayHoachToan.AddDays(1 - ngayHoachToan.Day);
            var thisMonthEnd = thisMonthStart.AddMonths(1).AddSeconds(-1);
            dteFromDate.DateTime = thisMonthStart;
            dteToDate.DateTime = thisMonthEnd;
            _lstMaterialGoods = _IMaterialGoodsService.GetByIsActive(true);
            optAccountingObjectType.CheckedIndex = 0;
            
            //WaitingFrm.StopWaiting();
        }

        private void cbbAboutTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAboutTime.SelectedRow != null)
            {
                var model = cbbAboutTime.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(ngayHoachToan.Year,ngayHoachToan, model, out dtBegin, out dtEnd);
                dteFromDate.DateTime = dtBegin;
                dteToDate.DateTime = dtEnd;
            }

        }

        //private void ddPeriod_MouseDown(object sender, MouseEventArgs e)
        //{
        //    ddPeriod.Text = _counter.ToString(CultureInfo.InvariantCulture);
        //    _counter += 1;
        //}

        private void btnSelect_Click(object sender, EventArgs e)
        {
            using (var fRSSelectMG = new FRSSelectMG())
            {
                //fRSSelectMG.FormClosed += frm_FormClosed;
                if (fRSSelectMG.ShowDialog(this) == DialogResult.OK)
                {
                    _lstMaterialGoods = fRSSelectMG._listMaterialGoods.Where(c => c.Status).ToList();
                }
            }
        }

        void frm_FormClosed(object sender, FormClosedEventArgs e)
        {
            fRSSelectMG = (FRSSelectMG)sender;
            if (e.CloseReason == CloseReason.UserClosing)
                if (fRSSelectMG.DialogResult == DialogResult.OK)
                {
                    _lstMaterialGoods = fRSSelectMG._listMaterialGoods.Where(c => c.Status).ToList();
                }
        }

        private void optAccountingObjectType_ValueChanged(object sender, EventArgs e)
        {
            if (optAccountingObjectType.CheckedIndex == 1)
            {
                btnSelect.Enabled = true;
                _lstMaterialGoods = fRSSelectMG._listMaterialGoods.Where(c => c.Status).ToList();
                //btnSelect.PerformClick();
            }
            if (optAccountingObjectType.CheckedIndex == 0)
            {
                btnSelect.Enabled = false;
                _lstMaterialGoods = _IMaterialGoodsService.GetByIsActive(true);
            }
        }

        private void btnKetThuc_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnThucHien_Click(object sender, EventArgs e)
        {
            if ((_lstMaterialGoods != null) && (_lstMaterialGoods.Count > 0))
            {
                try
                {
                    string s = "";
                    foreach (var x in _lstMaterialGoods)
                    {
                        decimal quantityIn = Utils.ListRepositoryLedger.Where(c => c.MaterialGoodsID == x.ID && c.PostedDate < dteToDate.DateTime).Sum(t => t.IWQuantity) ?? 0;
                        decimal quantityOut = Utils.ListRepositoryLedger.Where(c => c.MaterialGoodsID == x.ID && c.PostedDate < dteToDate.DateTime).Sum(t => t.OWQuantity) ?? 0;
                        if(quantityIn < quantityOut)
                        {
                            s = s + x.MaterialGoodsCode + ", ";
                        }
                    }
                    if (!string.IsNullOrEmpty(s))
                    {
                        if (MSG.Question("Vật tư "+ s.Trim().TrimEnd(',') + " đang có số lượng tồn âm. Bạn có muốn tiếp tục thực hiện tính giá xuất kho?") == DialogResult.No) return;
                    }
                    if (VTHH_PPTinhGiaXKho == "Bình quân cuối kỳ")
                    {
                        if (Utils.ListSystemOption.FirstOrDefault(x => x.ID == 47).Data == "1")
                        {
                            bool value = true;
                            WaitingFrm.StartWaiting();
                            foreach (var r in Utils.ListRepository)
                            {
                                value = _IRepositoryLedgerService.PricingAndUpdateOW_AverageForEndOfPeriods(_lstMaterialGoods, dteFromDate.DateTime, dteToDate.DateTime, r.ID);
                                if (!value)
                                {
                                    WaitingFrm.StopWaiting();
                                    MessageBox.Show("Có lỗi khi thực hiện", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    break;
                                }
                            }
                            WaitingFrm.StopWaiting();
                            if (value) MessageBox.Show("Tính giá xuất thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            WaitingFrm.StartWaiting();
                            if (!_IRepositoryLedgerService.PricingAndUpdateOW_AverageForEndOfPeriods(_lstMaterialGoods, dteFromDate.DateTime, dteToDate.DateTime))
                            {
                                WaitingFrm.StopWaiting();
                                MessageBox.Show("Có lỗi khi thực hiện", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            else
                            {
                                WaitingFrm.StopWaiting();
                                MessageBox.Show("Tính giá xuất thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                    }
                    else if (VTHH_PPTinhGiaXKho == "Bình quân tức thời")
                    {

                        WaitingFrm.StartWaiting();
                        if (!_IRepositoryLedgerService.PricingAndUpdateOW_AverageForInstantaneous(_lstMaterialGoods, dteFromDate.DateTime, dteToDate.DateTime))
                        {
                            WaitingFrm.StopWaiting();
                            MessageBox.Show("Có lỗi khi thực hiện", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                        {
                            WaitingFrm.StopWaiting();
                            MessageBox.Show("Tính giá xuất thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else if (VTHH_PPTinhGiaXKho == "Nhập trước xuất trước")
                    {
                        WaitingFrm.StartWaiting();
                        if (Utils.ListSystemOption.FirstOrDefault(x => x.ID == 47).Data == "1")
                        {
                            bool value = true;                           
                            foreach (var r in Utils.ListRepository)
                            {
                                value = _IRepositoryLedgerService.PricingAndUpdateOW_InFirstOutFirst(_lstMaterialGoods, dteFromDate.DateTime, dteToDate.DateTime, r.ID);
                                if (!value)
                                {
                                    WaitingFrm.StopWaiting();
                                    MessageBox.Show("Có lỗi khi thực hiện", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    break;
                                }
                            }
                            WaitingFrm.StopWaiting();
                            if (value) MessageBox.Show("Tính giá xuất thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            if (!_IRepositoryLedgerService.PricingAndUpdateOW_InFirstOutFirst(_lstMaterialGoods, dteFromDate.DateTime, dteToDate.DateTime))
                            {
                                WaitingFrm.StopWaiting();
                                MessageBox.Show("Có lỗi khi thực hiện", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            else
                            {
                                WaitingFrm.StopWaiting();
                                MessageBox.Show("Tính giá xuất thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }

                    }
                    else
                    {
                        MessageBox.Show("Không có phương thức tính giá xuất kho cho phương thức này", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    //if (MessageBox.Show("Tính giá xuất thành công", "Thông báo", MessageBoxButtons.OK,
                    //        MessageBoxIcon.Information) == DialogResult.OK)
                    //{
                    //    // HideLoading(false);
                    //}
                }
                catch (Exception)
                {
                    MessageBox.Show("Có lỗi khi thực hiện", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Chưa chọn vật tư nào để tính", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void HideLoading(bool hide)
        {
            pbLoading.Visible = hide;
            lbLoading.Visible = hide;
        }

        private void dteToDate_BeforeEnterEditMode(object sender, System.ComponentModel.CancelEventArgs e)
        {
           // dteToDate.SelectAll();
        }

        private void dteFromDate_BeforeEnterEditMode(object sender, System.ComponentModel.CancelEventArgs e)
        {
            
            ////dteFromDate.();
            //var ultraDateTimeEditor = sender as Infragistics.Win.UltraWinEditors.UltraDateTimeEditor;

            //// Return if something is wrong
            //if (ultraDateTimeEditor == null)
            //{
            //    return;
            //}

            ////  Call SelectAll to select the entire text of the editor
            //ultraDateTimeEditor.SelectAll();
        }

        private void dteFromDate_Enter(object sender, EventArgs e)
        {
            var ultraDateTimeEditor = sender as Infragistics.Win.UltraWinEditors.UltraDateTimeEditor;//trungnq thêm để sửa bug 6424

            // Return if something is wrong
            if (ultraDateTimeEditor == null)
            {
                return;
            }

            //  Call SelectAll to select the entire text of the editor
            ultraDateTimeEditor.SelectAll();

        }

        private void dteToDate_Enter(object sender, EventArgs e)
        {
            var ultraDateTimeEditor = sender as Infragistics.Win.UltraWinEditors.UltraDateTimeEditor;

            // Return if something is wrong
            if (ultraDateTimeEditor == null)
            {
                return;
            }

            //  Call SelectAll to select the entire text of the editor
            ultraDateTimeEditor.SelectAll();
        }
    }
}
