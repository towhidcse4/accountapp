﻿namespace Accounting
{
    partial class FRSSelectMG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            this.ultraPanelGrid = new Infragistics.Win.Misc.UltraPanel();
            this.uBtnDongY = new Infragistics.Win.Misc.UltraButton();
            this.uBtnHuyBo = new Infragistics.Win.Misc.UltraButton();
            this.uGridSelectMG = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanelGrid.ClientArea.SuspendLayout();
            this.ultraPanelGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSelectMG)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraPanelGrid
            // 
            // 
            // ultraPanelGrid.ClientArea
            // 
            this.ultraPanelGrid.ClientArea.Controls.Add(this.uBtnDongY);
            this.ultraPanelGrid.ClientArea.Controls.Add(this.uBtnHuyBo);
            this.ultraPanelGrid.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanelGrid.Location = new System.Drawing.Point(0, 335);
            this.ultraPanelGrid.Name = "ultraPanelGrid";
            this.ultraPanelGrid.Size = new System.Drawing.Size(545, 40);
            this.ultraPanelGrid.TabIndex = 0;
            // 
            // uBtnDongY
            // 
            appearance1.Image = global::Accounting.Properties.Resources.apply_16;
            this.uBtnDongY.Appearance = appearance1;
            this.uBtnDongY.Location = new System.Drawing.Point(381, 4);
            this.uBtnDongY.Name = "uBtnDongY";
            this.uBtnDongY.Size = new System.Drawing.Size(75, 30);
            this.uBtnDongY.TabIndex = 1;
            this.uBtnDongY.Text = "Đồng ý";
            this.uBtnDongY.Click += new System.EventHandler(this.uBtnDongY_Click);
            // 
            // uBtnHuyBo
            // 
            appearance2.Image = global::Accounting.Properties.Resources.cancel_16;
            this.uBtnHuyBo.Appearance = appearance2;
            this.uBtnHuyBo.Location = new System.Drawing.Point(462, 4);
            this.uBtnHuyBo.Name = "uBtnHuyBo";
            this.uBtnHuyBo.Size = new System.Drawing.Size(75, 30);
            this.uBtnHuyBo.TabIndex = 2;
            this.uBtnHuyBo.Text = "Hủy bỏ";
            this.uBtnHuyBo.Click += new System.EventHandler(this.uBtnHuyBo_Click);
            // 
            // uGridSelectMG
            // 
            this.uGridSelectMG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridSelectMG.Location = new System.Drawing.Point(0, 0);
            this.uGridSelectMG.Name = "uGridSelectMG";
            this.uGridSelectMG.Size = new System.Drawing.Size(545, 335);
            this.uGridSelectMG.TabIndex = 0;
            this.uGridSelectMG.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridSelectMG_CellChange);
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.uGridSelectMG);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(545, 335);
            this.ultraPanel1.TabIndex = 1;
            // 
            // FRSSelectMG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 375);
            this.Controls.Add(this.ultraPanel1);
            this.Controls.Add(this.ultraPanelGrid);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FRSSelectMG";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lựa chọn vật tư hàng hóa để tính giá xuất";
            this.ultraPanelGrid.ClientArea.ResumeLayout(false);
            this.ultraPanelGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridSelectMG)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanelGrid;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSelectMG;
        private Infragistics.Win.Misc.UltraButton uBtnDongY;
        private Infragistics.Win.Misc.UltraButton uBtnHuyBo;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;

    }
}