﻿namespace Accounting
{
    partial class FRSPricingOW
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            this.ultraPanelTop = new Infragistics.Win.Misc.UltraPanel();
            this.gbDescription = new Infragistics.Win.Misc.UltraGroupBox();
            this.lbDescription = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanelCenter = new Infragistics.Win.Misc.UltraPanel();
            this.btnSelect = new Infragistics.Win.Misc.UltraButton();
            this.optAccountingObjectType = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.ultraPanelBottom = new Infragistics.Win.Misc.UltraPanel();
            this.lbLoading = new Infragistics.Win.Misc.UltraLabel();
            this.pbLoading = new System.Windows.Forms.PictureBox();
            this.btnKetThuc = new Infragistics.Win.Misc.UltraButton();
            this.btnThucHien = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBoxBottom = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbAboutTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dteToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dteFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanelTop.ClientArea.SuspendLayout();
            this.ultraPanelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbDescription)).BeginInit();
            this.gbDescription.SuspendLayout();
            this.ultraPanelCenter.ClientArea.SuspendLayout();
            this.ultraPanelCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optAccountingObjectType)).BeginInit();
            this.ultraPanelBottom.ClientArea.SuspendLayout();
            this.ultraPanelBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLoading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxBottom)).BeginInit();
            this.ultraGroupBoxBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAboutTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFromDate)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraPanelTop
            // 
            // 
            // ultraPanelTop.ClientArea
            // 
            this.ultraPanelTop.ClientArea.Controls.Add(this.gbDescription);
            this.ultraPanelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanelTop.Location = new System.Drawing.Point(0, 0);
            this.ultraPanelTop.Name = "ultraPanelTop";
            this.ultraPanelTop.Size = new System.Drawing.Size(354, 101);
            this.ultraPanelTop.TabIndex = 0;
            // 
            // gbDescription
            // 
            this.gbDescription.Controls.Add(this.lbDescription);
            this.gbDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbDescription.Location = new System.Drawing.Point(0, 0);
            this.gbDescription.Name = "gbDescription";
            this.gbDescription.Size = new System.Drawing.Size(354, 101);
            this.gbDescription.TabIndex = 1;
            this.gbDescription.Text = "ultraGroupBox1";
            // 
            // lbDescription
            // 
            this.lbDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDescription.Location = new System.Drawing.Point(6, 28);
            this.lbDescription.Name = "lbDescription";
            this.lbDescription.Size = new System.Drawing.Size(342, 62);
            this.lbDescription.TabIndex = 0;
            this.lbDescription.Text = "lb";
            // 
            // ultraPanelCenter
            // 
            // 
            // ultraPanelCenter.ClientArea
            // 
            this.ultraPanelCenter.ClientArea.Controls.Add(this.btnSelect);
            this.ultraPanelCenter.ClientArea.Controls.Add(this.optAccountingObjectType);
            this.ultraPanelCenter.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanelCenter.Location = new System.Drawing.Point(0, 101);
            this.ultraPanelCenter.Name = "ultraPanelCenter";
            this.ultraPanelCenter.Size = new System.Drawing.Size(354, 47);
            this.ultraPanelCenter.TabIndex = 1;
            // 
            // btnSelect
            // 
            this.btnSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.Image = global::Accounting.Properties.Resources.btnsearch;
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnSelect.Appearance = appearance1;
            this.btnSelect.Location = new System.Drawing.Point(307, 4);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(29, 29);
            this.btnSelect.TabIndex = 75;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // optAccountingObjectType
            // 
            this.optAccountingObjectType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.optAccountingObjectType.Appearance = appearance2;
            this.optAccountingObjectType.BackColor = System.Drawing.Color.Transparent;
            this.optAccountingObjectType.BackColorInternal = System.Drawing.Color.Transparent;
            this.optAccountingObjectType.CheckedIndex = 0;
            valueListItem4.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem4.DataValue = "Default Item";
            valueListItem4.DisplayText = "Tính tất cả vật tư, hàng hóa";
            valueListItem1.DataValue = "ValueListItem1";
            valueListItem1.DisplayText = "Chọn vật tư, hàng hóa";
            this.optAccountingObjectType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem4,
            valueListItem1});
            this.optAccountingObjectType.Location = new System.Drawing.Point(12, 9);
            this.optAccountingObjectType.Margin = new System.Windows.Forms.Padding(0);
            this.optAccountingObjectType.Name = "optAccountingObjectType";
            this.optAccountingObjectType.Size = new System.Drawing.Size(292, 24);
            this.optAccountingObjectType.TabIndex = 29;
            this.optAccountingObjectType.Text = "Tính tất cả vật tư, hàng hóa";
            this.optAccountingObjectType.ValueChanged += new System.EventHandler(this.optAccountingObjectType_ValueChanged);
            // 
            // ultraPanelBottom
            // 
            // 
            // ultraPanelBottom.ClientArea
            // 
            this.ultraPanelBottom.ClientArea.Controls.Add(this.lbLoading);
            this.ultraPanelBottom.ClientArea.Controls.Add(this.pbLoading);
            this.ultraPanelBottom.ClientArea.Controls.Add(this.btnKetThuc);
            this.ultraPanelBottom.ClientArea.Controls.Add(this.btnThucHien);
            this.ultraPanelBottom.ClientArea.Controls.Add(this.ultraGroupBoxBottom);
            this.ultraPanelBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanelBottom.Location = new System.Drawing.Point(0, 148);
            this.ultraPanelBottom.Name = "ultraPanelBottom";
            this.ultraPanelBottom.Size = new System.Drawing.Size(354, 107);
            this.ultraPanelBottom.TabIndex = 2;
            // 
            // lbLoading
            // 
            this.lbLoading.Location = new System.Drawing.Point(51, 81);
            this.lbLoading.Name = "lbLoading";
            this.lbLoading.Size = new System.Drawing.Size(60, 14);
            this.lbLoading.TabIndex = 5;
            this.lbLoading.Text = "Đang xử lý";
            // 
            // pbLoading
            // 
            this.pbLoading.Location = new System.Drawing.Point(13, 70);
            this.pbLoading.Name = "pbLoading";
            this.pbLoading.Size = new System.Drawing.Size(32, 32);
            this.pbLoading.TabIndex = 4;
            this.pbLoading.TabStop = false;
            // 
            // btnKetThuc
            // 
            appearance3.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnKetThuc.Appearance = appearance3;
            this.btnKetThuc.Location = new System.Drawing.Point(256, 74);
            this.btnKetThuc.Name = "btnKetThuc";
            this.btnKetThuc.Size = new System.Drawing.Size(80, 28);
            this.btnKetThuc.TabIndex = 3;
            this.btnKetThuc.Text = "Đóng";
            this.btnKetThuc.Click += new System.EventHandler(this.btnKetThuc_Click);
            // 
            // btnThucHien
            // 
            appearance4.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnThucHien.Appearance = appearance4;
            this.btnThucHien.Location = new System.Drawing.Point(167, 74);
            this.btnThucHien.Name = "btnThucHien";
            this.btnThucHien.Size = new System.Drawing.Size(84, 28);
            this.btnThucHien.TabIndex = 2;
            this.btnThucHien.Text = "Thực hiện";
            this.btnThucHien.Click += new System.EventHandler(this.btnThucHien_Click);
            // 
            // ultraGroupBoxBottom
            // 
            appearance5.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraGroupBoxBottom.Appearance = appearance5;
            this.ultraGroupBoxBottom.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rounded;
            this.ultraGroupBoxBottom.Controls.Add(this.cbbAboutTime);
            this.ultraGroupBoxBottom.Controls.Add(this.dteToDate);
            this.ultraGroupBoxBottom.Controls.Add(this.dteFromDate);
            this.ultraGroupBoxBottom.Controls.Add(this.ultraLabel3);
            this.ultraGroupBoxBottom.Controls.Add(this.ultraLabel2);
            this.ultraGroupBoxBottom.Controls.Add(this.ultraLabel1);
            this.ultraGroupBoxBottom.Location = new System.Drawing.Point(12, 6);
            this.ultraGroupBoxBottom.Name = "ultraGroupBoxBottom";
            this.ultraGroupBoxBottom.Size = new System.Drawing.Size(324, 62);
            this.ultraGroupBoxBottom.TabIndex = 1;
            // 
            // cbbAboutTime
            // 
            this.cbbAboutTime.AutoSize = false;
            this.cbbAboutTime.Location = new System.Drawing.Point(102, 5);
            this.cbbAboutTime.Name = "cbbAboutTime";
            this.cbbAboutTime.Size = new System.Drawing.Size(222, 22);
            this.cbbAboutTime.TabIndex = 69;
            // 
            // dteToDate
            // 
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.dteToDate.Appearance = appearance6;
            this.dteToDate.AutoSize = false;
            this.dteToDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteToDate.Location = new System.Drawing.Point(239, 33);
            this.dteToDate.MaskInput = "dd/mm/yyyy";
            this.dteToDate.Name = "dteToDate";
            this.dteToDate.Size = new System.Drawing.Size(85, 22);
            this.dteToDate.TabIndex = 68;
            this.dteToDate.Value = null;
            this.dteToDate.BeforeEnterEditMode += new System.ComponentModel.CancelEventHandler(this.dteToDate_BeforeEnterEditMode);
            this.dteToDate.Enter += new System.EventHandler(this.dteToDate_Enter);
            // 
            // dteFromDate
            // 
            appearance7.TextHAlignAsString = "Center";
            appearance7.TextVAlignAsString = "Middle";
            this.dteFromDate.Appearance = appearance7;
            this.dteFromDate.AutoSize = false;
            this.dteFromDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteFromDate.Location = new System.Drawing.Point(102, 33);
            this.dteFromDate.MaskInput = "dd/mm/yyyy";
            this.dteFromDate.Name = "dteFromDate";
            this.dteFromDate.Size = new System.Drawing.Size(93, 22);
            this.dteFromDate.TabIndex = 67;
            this.dteFromDate.Value = null;
            this.dteFromDate.BeforeEnterEditMode += new System.ComponentModel.CancelEventHandler(this.dteFromDate_BeforeEnterEditMode);
            this.dteFromDate.Enter += new System.EventHandler(this.dteFromDate_Enter);
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.AutoSize = true;
            this.ultraLabel3.Location = new System.Drawing.Point(208, 37);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(25, 14);
            this.ultraLabel3.TabIndex = 65;
            this.ultraLabel3.Text = "Đến";
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.AutoSize = true;
            this.ultraLabel2.Location = new System.Drawing.Point(79, 37);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(17, 14);
            this.ultraLabel2.TabIndex = 64;
            this.ultraLabel2.Text = "Từ";
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.AutoSize = true;
            this.ultraLabel1.Location = new System.Drawing.Point(10, 9);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(89, 14);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Khoảng thời gian";
            // 
            // FRSPricingOW
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(354, 255);
            this.Controls.Add(this.ultraPanelBottom);
            this.Controls.Add(this.ultraPanelCenter);
            this.Controls.Add(this.ultraPanelTop);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(370, 294);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(370, 294);
            this.Name = "FRSPricingOW";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tính giá xuất kho VTHH";
            this.ultraPanelTop.ClientArea.ResumeLayout(false);
            this.ultraPanelTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gbDescription)).EndInit();
            this.gbDescription.ResumeLayout(false);
            this.ultraPanelCenter.ClientArea.ResumeLayout(false);
            this.ultraPanelCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optAccountingObjectType)).EndInit();
            this.ultraPanelBottom.ClientArea.ResumeLayout(false);
            this.ultraPanelBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbLoading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxBottom)).EndInit();
            this.ultraGroupBoxBottom.ResumeLayout(false);
            this.ultraGroupBoxBottom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAboutTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFromDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanelTop;
        private Infragistics.Win.Misc.UltraPanel ultraPanelCenter;
        private Infragistics.Win.Misc.UltraPanel ultraPanelBottom;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxBottom;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet optAccountingObjectType;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteFromDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteToDate;
        private Infragistics.Win.Misc.UltraLabel lbDescription;
        private Infragistics.Win.Misc.UltraButton btnSelect;
        private Infragistics.Win.Misc.UltraButton btnKetThuc;
        private Infragistics.Win.Misc.UltraButton btnThucHien;
        private System.Windows.Forms.PictureBox pbLoading;
        private Infragistics.Win.Misc.UltraLabel lbLoading;
        private Infragistics.Win.Misc.UltraGroupBox gbDescription;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAboutTime;
    }
}