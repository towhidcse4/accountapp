﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using Infragistics.Win.AppStyling;
using System.Linq;
using Infragistics.Win;
namespace Accounting
{
    public partial class FRSInwardOutward : CustormForm
    {
        #region Khai Báo
        public readonly IRSInwardOutwardService _IRSInwardOutwardService;
        public readonly IMaterialGoodsService _IMaterialGoodsService;
        public readonly IRepositoryService _IRepositoryService;
        public readonly ITypeService _ITypeService;
        public readonly IRSInwardOutwardDetailService _IRSInwardOutwardDetailService;
        public readonly IAccountingObjectService _IAccountingObjectService;
        List<RSInwardOutward> listRSInwardOutward = new List<RSInwardOutward>();
        List<RSInwardOutwardDetail> listRSInwardOutwardDetail = new List<RSInwardOutwardDetail>();
        List<AccountingObject> listAccounObject = new List<AccountingObject>();
        List<Accounting.Core.Domain.Type> listType = new List<Accounting.Core.Domain.Type>();
        public static Dictionary<string, Template> DsTemplate = new Dictionary<string, Template>();
      
        public static bool isClose = true;
        #endregion
        #region Khởi tạo
        public FRSInwardOutward()
        {
            InitializeComponent();
            this.CenterToScreen();
            _IRSInwardOutwardService = IoC.Resolve<IRSInwardOutwardService>();
            _IRepositoryService = IoC.Resolve<IRepositoryService>();
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            _IRSInwardOutwardDetailService = IoC.Resolve<IRSInwardOutwardDetailService>();

            _ITypeService = IoC.Resolve<ITypeService>();
            
          
            IQueryable<RSInwardOutward> lstQueryable = _IRSInwardOutwardService.Query;
       
            lstQueryable =
                lstQueryable.Where(outward => (outward.TypeID == 400 ||
                                   outward.TypeID == 401 ||
                                   outward.TypeID == 402 ||
                                   outward.TypeID == 403 ||
                                   outward.TypeID == 404));
            listRSInwardOutward = new List<RSInwardOutward>(lstQueryable);    
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            LoadDulieu();
        }
        #endregion
        #region Hàm 
        void LoadDulieu()
        {
            LoadDuLieu(true);
        }
      
        private void LoadDuLieu(bool configGrid)
        {
          
            #region Lấy dữ liệu từ CSDL
            //listRSInwardOutward = _IRSInwardOutwardService.GetAll();
            listAccounObject = _IAccountingObjectService.GetAll();
            listRSInwardOutwardDetail = _IRSInwardOutwardDetailService.GetAll();
            listType = _ITypeService.GetAll();
            #endregion
            #region Xử lý dữ liệu
            #endregion
            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = listRSInwardOutward;
            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }
            //foreach (var item in listRSInwardOutward)
            //{
            //    if (item.TypeID != null) item.TypeIDViewNameCode = listType.Where(p => p.ID == item.TypeID).SingleOrDefault().TypeName;
            //}
            //uGrid.Selected.Rows[0].Cells[0].Value.ToString();
            if (configGrid) ConfigGrid(uGrid);
           
            #endregion
        }
        #endregion
        #region Nghiệp vụ
        /// Nghiệp vụ Update
        void editFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                RSInwardOutward temp = uGrid.Selected.Rows[0].ListObject as RSInwardOutward;
                new FRSInwardOutwardDetail(temp, listRSInwardOutward, ConstFrm.optStatusForm.View).ShowDialog(this);
                if (!FRSInwardOutwardDetail.IsClose) LoadDulieu();
                
            }
            else
                Accounting.TextMessage.MSG.Error(Accounting.TextMessage.resSystem.MSG_System_04);
        }
        void addFunction()
        {
            RSInwardOutward temp = uGrid.Selected.Rows.Count <= 0 ? new RSInwardOutward() : uGrid.Selected.Rows[0].ListObject as RSInwardOutward;
            new FRSInwardOutwardDetail(temp, listRSInwardOutward, ConstFrm.optStatusForm.Add).ShowDialog(this);
            if (!FRSInwardOutwardDetail.IsClose) LoadDulieu();
        }
        void deleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                RSInwardOutward temp = uGrid.Selected.Rows[0].ListObject as RSInwardOutward;
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.No)) == System.Windows.Forms.DialogResult.Yes)
                {
                   // List<bool> list = _IRSInwardOutwardService.Query.Select(p => p.Recorded).ToList();

                   
                        if (temp.Recorded==true)
                        {
                            MSG.Error("Chứng từ đang ghi sổ không được phép xóa");
                            return;
                        }
                    
                    _IRSInwardOutwardService.BeginTran();
                    _IRSInwardOutwardService.Delete(temp);
                    _IRSInwardOutwardService.CommitTran();
                    LoadDulieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion
        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(uGrid, TextMessage.ConstDatabase.RSInwardOutward_TableName);
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Format = Constants.DdMMyyyy;
            uGrid.DisplayLayout.Bands[0].Columns["Date"].Format = Constants.DdMMyyyy;
            utralGrid.DisplayLayout.Bands[0].Columns["PostedDate"].CellAppearance.TextHAlign = HAlign.Center;
            utralGrid.DisplayLayout.Bands[0].Columns["Date"].CellAppearance.TextHAlign = HAlign.Center;
        }

        
        #endregion
        #region Event
        private void btnAdd_Click(object sender, EventArgs e)
        {
            addFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            editFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            deleteFunction();
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            addFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            editFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            deleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            LoadDulieu();
        }

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index == -1) return;
            editFunction();
        }

        private void uGrid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {

                if (uGrid.Selected.Rows.Count < 1) return;
                object listObject = uGrid.Selected.Rows[0].ListObject;
                if (listObject == null) return;
                RSInwardOutward mcRSInwardOutward = listObject as RSInwardOutward;
                if (mcRSInwardOutward == null) return;

              //  RSInwardOutward temp = uGrid.Selected.Rows[0].ListObject as RSInwardOutward;

                labePostedDate.Text = mcRSInwardOutward.PostedDate.ToString("dd/MM/yyyy");
                labeNo.Text = mcRSInwardOutward.No;
                LabeDate.Text = mcRSInwardOutward.Date.ToString("dd/MM/yyyy");

                labeAccountingObjectName.Text = mcRSInwardOutward.AccountingObjectName;
                labeAddres.Text = mcRSInwardOutward.AccountingObjectAddress;
                labeReason.Text = mcRSInwardOutward.Reason;
                LabetotalAmount.Text = mcRSInwardOutward.TotalAmountOriginal.ToString();
                //uGridDetail.DataSource = listRSInwardOutwardDetail.Where(p => p.RSInwardOutwardID == temp.ID).ToList();
                //Utils.ConfigGrid(uGridDetail, TextMessage.ConstDatabase.RSInwardOutwardDetail_TableName);

                //Tab hoạch toán
                if (mcRSInwardOutward.RSInwardOutwardDetails.Count == 0) mcRSInwardOutward.RSInwardOutwardDetails = new List<RSInwardOutwardDetail>();
                uGridDetail.DataSource = mcRSInwardOutward.RSInwardOutwardDetails;    //Utils.CloneObject(mcPayment.MCPaymentDetails);

                #region Config Grid
                //hiển thị 1 band
               // uGridDetail.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
                //tắt lọc cột
                uGridDetail.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
                uGridDetail.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
                //tự thay đổi kích thước cột
                uGridDetail.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                //tắt tiêu đề
                uGridDetail.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
                //select cả hàng hay ko?
                uGridDetail.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
                uGridDetail.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;

                //Hiện những dòng trống?
                uGridDetail.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
                uGridDetail.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
                //Fix Header
                uGridDetail.DisplayLayout.UseFixedHeaders = true;

                UltraGridBand band = uGridDetail.DisplayLayout.Bands[0];
                band.Summaries.Clear();

                Template mauGiaoDien = GetMauGiaoDien(mcRSInwardOutward.TypeID, mcRSInwardOutward.TemplateID, DsTemplate);

                Utils.ConfigGrid(uGridDetail, ConstDatabase.RSInwardOutwardDetail_TableName, mauGiaoDien == null ? new List<TemplateColumn>() : mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 0).TemplateColumns, 0);
                //Thêm tổng số ở cột "Số tiền"
                SummarySettings summary = band.Summaries.Add("SumAmountOriginal", SummaryType.Sum, band.Columns["AmountOriginal"]);
                summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                summary.DisplayFormat = "{0:N0}";
                summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
                #endregion
                foreach (var item in band.Columns)
                {
                    if (item.Key.Equals("MaterialGoodsID"))
                    {
                        List<MaterialGoods> MaterialGoodsItems = _IMaterialGoodsService.GetAll().OrderBy(p => p.MaterialGoodsCode).Where(p => p.IsActive == true).ToList(); ;
                        item.Editor = this.CreateCombobox(MaterialGoodsItems, ConstDatabase.MaterialGoods_TableName, "ID", "MaterialGoodsCode");

                    }
                    else if (item.Key.Equals("RepositoryID"))
                    {
                        //List<Repository> Repositorys = _IRepositoryService.GetAll().OrderBy(p => p.RepositoryCode).Where(p => p.IsActive == true).ToList(); ;
                        List<Repository> Repositorys = _IRepositoryService.GetByRepositoryCode(true);
                        item.Editor = this.CreateCombobox(Repositorys, ConstDatabase.Repository_TableName, "ID", "RepositoryCode");

                    }
                }
            }
        }
        #endregion
        public static Template GetMauGiaoDien(int typeId, Guid? templateId, Dictionary<string, Template> dsTemplate)    //out string keyofdsTemplate
        {
            string keyofdsTemplate = string.Format("{0}@{1}", typeId, templateId);
            if (!dsTemplate.Keys.Contains(keyofdsTemplate))
            {
                //Note: các chứng từ được sinh tự động từ nghiệp vụ khác thì phải truyền TypeID của nghiệp vụ khác đó
                int typeIdTemp = GetTypeIdRef(typeId);
                Template template = Utils.GetTemplateInDatabase(templateId, typeIdTemp);
                dsTemplate.Add(keyofdsTemplate, template);
                return template;
            }
            //keyofdsTemplate = string.Empty;
            return dsTemplate[keyofdsTemplate];
        }
        static int GetTypeIdRef(int typeId)
        {
            return typeId;
        }
    }
}
