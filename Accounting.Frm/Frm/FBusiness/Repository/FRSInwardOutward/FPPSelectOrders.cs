﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;

namespace Accounting
{
    public partial class FPPSelectOrders : CustormForm
    {
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        private DateTime _startDate;
        readonly BindingList<PPSelectOrder> _lstSelectOrder = new BindingList<PPSelectOrder>();
        public List<PPSelectOrder> SelectOrders { get; private set; }

        public FPPSelectOrders(Guid? accountingObjectId)
        {

            InitializeComponent();

            ConfigControl(accountingObjectId);
        }

        private void ConfigControl(Guid? accountingObjectId)
        {
            //var itemsCbbTime = new ValueListItem[]{};
            //foreach (var item in Utils.DicSelectTimes)
            //{
            //    cbbTime.Items.Add(item.Key, item.Value.Name);
            //}
            //cbbTime.SelectedItem = cbbTime.Items[4];
            //cbbTime.Items.AddRange(itemsCbbTime);

            var stringToDateTime = Utils.StringToDateTime(ConstFrm.DbStartDate);
            if (stringToDateTime != null)
                _startDate = (DateTime)stringToDateTime;
            else _startDate = DateTime.Now;
            //cbbAboutTime.DataSource = _lstItems;
            //cbbAboutTime.DisplayMember = "Name";
            //Utils.ConfigGrid(cbbAboutTime, ConstDatabase.ConfigXML_TableName);
            //cbbAboutTime.SelectedRow = cbbAboutTime.Rows[4];
            cbbAboutTime.ConfigComboSelectTime(_lstItems, "Name");
            //var thisMonthStart = _startDate.AddDays(1 - _startDate.Day);
            //var thisMonthEnd = thisMonthStart.AddMonths(1).AddSeconds(-1);
            //dteDateFrom.DateTime = thisMonthStart;
            //dteDateTo.DateTime = thisMonthEnd;

            this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObject, "AccountingObjectCode", "ID");
            if (accountingObjectId != null)
            {
                cbbAccountingObject.Value = accountingObjectId;
                btnGetData.PerformClick();
            }
            ConfigGrid(new BindingList<PPSelectOrder>());
        }

        private void ConfigGrid(BindingList<PPSelectOrder> input)
        {
            uGrid.DataSource = input;
            Utils.ConfigGrid(uGrid, ConstDatabase.PPSelectOrder_KeyName, false, false, false);
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.UseFixedHeaders = false;
            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid);
            }
            uGrid.DisplayLayout.Bands[0].Columns["CheckColumn"].Header.VisiblePosition = 0;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private List<PPOrder> getData()
        {
            var result = Utils.IPPOrderService.Query.Where(x => x.Date >= dteDateFrom.DateTime && x.Date <= dteDateTo.DateTime).OrderByDescending(x => x.Date).ThenByDescending(x => x.No);
            if (cbbAccountingObject.Value != null && (Guid)cbbAccountingObject.Value != Guid.Empty)
            {
                return result.Where(x => x.AccountingObjectID == (Guid)cbbAccountingObject.Value).ToList();
            }
            return result.ToList();
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            _lstSelectOrder.Clear();
            var result = getData();
            Utils.IPPOrderService.UnbindSession(result);
            result = getData();
            foreach (var ppOrder in result)
            {
                foreach (var ppOrderDetail in ppOrder.PPOrderDetails.Where(x => ((x.Quantity ?? 0) - (x.QuantityReceipt ?? 0)) > 0))
                {
                    var selectOrder = new PPSelectOrder
                                          {
                                              ID = ppOrder.ID,
                                              No = ppOrder.No,
                                              Date = ppOrder.Date,
                                              Reason = ppOrder.Reason,
                                              MaterialGoodsID = ppOrderDetail.MaterialGoodsID,
                                              QuantityRemaining = (ppOrderDetail.Quantity ?? 0) - (ppOrderDetail.QuantityReceipt ?? 0),
                                              QuantityInward = (ppOrderDetail.Quantity ?? 0) - (ppOrderDetail.QuantityReceipt ?? 0),
                                              AccountingObjectID = ppOrder.AccountingObjectID,
                                              PpOrderDetail = ppOrderDetail
                    };
                    var materialGoodsCustom = Utils.ListMaterialGoodsCustom.FirstOrDefault(x => x.ID == ppOrderDetail.MaterialGoodsID);
                    if (materialGoodsCustom != null)
                        selectOrder.MaterialGoodsName =
                            materialGoodsCustom.
                                MaterialGoodsName;
                    _lstSelectOrder.Add(selectOrder);
                }
            }
            ConfigGrid(_lstSelectOrder);
        }

        private void btnViewSaOrder_Click(object sender, EventArgs e)
        {
            ViewPPOrder();
        }

        private void ViewPPOrder()
        {
            if (uGrid.ActiveRow == null) return;
            var selectOrder = uGrid.ActiveRow.ListObject as PPSelectOrder;
            if (selectOrder == null) return;
            if (selectOrder.CheckColumn == true) 
            {
                var ppOder = Utils.IPPOrderService.Query.FirstOrDefault(x => x.ID == selectOrder.ID);
                var f = new FPPOrderDetail(ppOder, new List<PPOrder>() { ppOder }, ConstFrm.optStatusForm.View);
                f.ShowDialog(this);
            }
            else { MessageBox.Show("Bạn cần chọn đơn mua hàng trước", "Thông Báo" ,MessageBoxButtons.OK,MessageBoxIcon.Warning); }                  
        }

        private void cbbAboutTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAboutTime.SelectedRow != null)
            {
                var model = cbbAboutTime.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(_startDate, model, out dtBegin, out dtEnd);
                dteDateFrom.DateTime = dtBegin;
                dteDateTo.DateTime = dtEnd;
            }
        }

        private void dteDateFrom_ValueChanged(object sender, EventArgs e)
        {
            if (ActiveControl != null && ActiveControl.Name == ((UltraDateTimeEditor)sender).Name)
                cbbAboutTime.SelectedRow = cbbAboutTime.Rows[34];
        }

        private void dteDateTo_ValueChanged(object sender, EventArgs e)
        {
            if (ActiveControl != null && ActiveControl.Name == ((UltraDateTimeEditor)sender).Name)
                cbbAboutTime.SelectedRow = cbbAboutTime.Rows[34];
        }

        private void uGrid_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            ViewPPOrder();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            var lstPPOrder = from u in _lstSelectOrder where u.CheckColumn select u;
            if (lstPPOrder.Any(x => x.CheckColumn == true))
            {
                SelectOrders = lstPPOrder.ToList();
                DialogResult = DialogResult.OK;
                Close();
            }
            else
            {
                MessageBox.Show("Bạn cần chọn đơn mua hàng trước");
            }
           
        }
    }
}
