﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;

namespace Accounting
{
    public partial class FIWSelectOrders : CustormForm
    {
        private DateTime _startDate;
        public object SelectOrders { get; private set; }
        private int _typeID { get; set; }
        public FIWSelectOrders(int TypeID)
        {

            InitializeComponent();
            _typeID = TypeID;
            ConfigControl();
            setTitle(TypeID);
        }

        private void setTitle(int TypeID)
        {
            switch (TypeID)
            {
                case 900:
                    this.Text = "Chọn lệnh lắp ráp";
                    break;
                case 901:
                    this.Text = "Chọn lệnh tháo rỡ";
                    break;
            }
        }

        private void ConfigControl()
        {
            var stringToDateTime = Utils.StringToDateTime(ConstFrm.DbStartDate);
            if (stringToDateTime != null)
                _startDate = (DateTime)stringToDateTime;
            else _startDate = DateTime.Now;
            List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
            cbbAboutTime.ConfigComboSelectTime(_lstItems, "Name");
            ConfigGrid<RSAssemblyDismantlement>(new BindingList<RSAssemblyDismantlement>());
        }

        private void ConfigGrid<T>(BindingList<T> input)
        {
            uGrid.DataSource = input;
            Utils.ConfigGrid(uGrid, ConstDatabase.SASelectOrder_KeyName, true, false, false);

            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            if (_typeID == 900 || _typeID == 901)
            {
                List<RSAssemblyDismantlement> data = Utils.IRSAssemblyDismantlementService.getByType(_typeID, dteDateFrom.DateTime, dteDateTo.DateTime);
                ConfigGrid<RSAssemblyDismantlement>(new BindingList<RSAssemblyDismantlement>(data));
            }
        }
        private void cbbAboutTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAboutTime.SelectedRow != null)
            {
                var model = cbbAboutTime.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(_startDate.Year,_startDate, model, out dtBegin, out dtEnd);
                dteDateFrom.DateTime = dtBegin;
                dteDateTo.DateTime = dtEnd;
            }
        }

        private void dteDateFrom_ValueChanged(object sender, EventArgs e)
        {
            if (ActiveControl != null && ActiveControl.Name == ((UltraDateTimeEditor)sender).Name)
                cbbAboutTime.SelectedRow = cbbAboutTime.Rows[34];
        }

        private void dteDateTo_ValueChanged(object sender, EventArgs e)
        {
            if (ActiveControl != null && ActiveControl.Name == ((UltraDateTimeEditor)sender).Name)
                cbbAboutTime.SelectedRow = cbbAboutTime.Rows[34];
        }

        private void btnViewSaOrder_Click(object sender, EventArgs e)
        {
            ViewSaOrder();
        }

        private void ViewSaOrder()
        {
            if (uGrid.ActiveRow == null) return;
            var selectOrder = uGrid.ActiveRow.ListObject as SASelectOrder;
            if (selectOrder == null) return;
            if (selectOrder.CheckColumn == true)
            {
                var saOder = Utils.ISAOrderService.Query.FirstOrDefault(x => x.ID == selectOrder.ID);
                var f = new FSAOrderDetail(saOder, new List<SAOrder>() { saOder }, ConstFrm.optStatusForm.View);
                f.ShowDialog(this);
            }
            else { MessageBox.Show("Bạn cần chọn chứng từ trước", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
        }


        private void uGrid_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            //ViewSaOrder();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            if (uGrid.ActiveRow != null)
            {
                SelectOrders = uGrid.ActiveRow.ListObject;
                DialogResult = DialogResult.OK;
                Close();
            }
            else
            {
                MSG.Warning("Bạn cần chọn chứng từ trước");
            }

        }

    }
}
