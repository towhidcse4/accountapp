﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FPPSelectOrderrs : CustormForm
    {
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        private DateTime _startDate;
        readonly BindingList<PPSelectOrder> _lstSelectOrder = new BindingList<PPSelectOrder>();
        public List<PPSelectOrder> SelectOrders { get; private set; }

        public FPPSelectOrderrs(Guid? accountingObjectId)
        {

            InitializeComponent();

            ConfigControl(accountingObjectId);
        }

        private void ConfigControl(Guid? accountingObjectId)
        {
            var stringToDateTime = Utils.StringToDateTime(ConstFrm.DbStartDate);
            if (stringToDateTime != null)
                _startDate = (DateTime)stringToDateTime;
            else _startDate = DateTime.Now;
            cbbAboutTime.ConfigComboSelectTime(_lstItems, "Name");

            List<int> lstType = new  List<int>(){ 0, 2};
            List<AccountingObject> lstAccountingObject = Utils.ListAccountingObject.ToList();
            this.ConfigCombo(lstAccountingObject, cbbAccountingObject, "AccountingObjectCode", "ID");
            if (accountingObjectId != null)
            {
                cbbAccountingObject.Value = accountingObjectId;
                btnGetData.PerformClick();
            }
            ConfigGrid(new BindingList<PPSelectOrder>());
        }

        private void ConfigGrid(BindingList<PPSelectOrder> input)
        {
            uGrid.DataSource = input;
            Utils.ConfigGrid(uGrid, ConstDatabase.PPSelectOrder_KeyName, false, false, false);
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.UseFixedHeaders = false;
            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid);
            }
            uGrid.DisplayLayout.Bands[0].Columns["CheckColumn"].Header.VisiblePosition = 0;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private List<PPOrder> getData()
        {
            var result = Utils.IPPOrderService.Query.Where(x => x.Date >= dteDateFrom.DateTime && x.Date <= dteDateTo.DateTime).OrderByDescending(x => x.Date).ThenByDescending(x => x.No);
            if (cbbAccountingObject.Value != null && (Guid)cbbAccountingObject.Value != Guid.Empty)
            {
                return result.Where(x => x.AccountingObjectID == (Guid)cbbAccountingObject.Value).ToList();
            }
            return result.ToList();
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            _lstSelectOrder.Clear();
            var result = getData();
            Utils.IPPOrderService.UnbindSession(result);
            result = getData();
            foreach (var ppOrder in result)
            {
                foreach (var ppOrderDetail in ppOrder.PPOrderDetails.Where(x => ((x.Quantity ?? 0) - (x.QuantityReceipt ?? 0)) > 0).OrderBy(a => a.OrderPriority))//edit by cuongpv: add them order by OrderPriority
                {
                    var selectOrder = new PPSelectOrder
                                          {
                                              ID = ppOrder.ID,
                                              Date = ppOrder.Date,
                                              No = ppOrder.No,
                                              Reason = ppOrder.Reason,
                                              MaterialGoodsID = ppOrderDetail.MaterialGoodsID,
                                              Description = ppOrderDetail.Description,
                                              QuantityRemaining = (ppOrderDetail.Quantity ?? 0) - (ppOrderDetail.QuantityReceipt ?? 0),
                                              QuantityInward = (ppOrderDetail.Quantity ?? 0) - (ppOrderDetail.QuantityReceipt ?? 0),
                                              //AccountingObjectID = ppOrder.AccountingObjectID,
                                              OrderPriority = ppOrderDetail.OrderPriority,//add by cuongpv
                                              PpOrderDetail = ppOrderDetail
                    };
                    var materialGoodsCustom = Utils.ListMaterialGoodsCustom.FirstOrDefault(x => x.ID == ppOrderDetail.MaterialGoodsID);
                    if (materialGoodsCustom != null)
                        selectOrder.MaterialGoodsName =
                            materialGoodsCustom.
                                MaterialGoodsName;
                    _lstSelectOrder.Add(selectOrder);
                }
            }
            ConfigGrid(_lstSelectOrder);
        }

        private void btnViewSaOrder_Click(object sender, EventArgs e)
        {
            ViewPpOrder();
        }

        private void ViewPpOrder()
        {
            //if (uGrid.ActiveRow == null) return;
            //foreach (var item in uGrid.Rows)
            //{
            //    if ((bool)item.Cells["CheckColumn"].Value == true)
            //    {
            //        var selectOrder = item.ListObject as PPSelectOrder;
            //        if (selectOrder == null) return;
            //        if (selectOrder.CheckColumn == true)
            //        {
            //            var ppOder = Utils.IPPOrderService.Query.FirstOrDefault(x => x.ID == selectOrder.ID);
            //            var f = new FPPOrderDetail(ppOder, new List<PPOrder>() { ppOder }, ConstFrm.optStatusForm.View);
            //            f.ShowDialog(this);
            //        }
            //    }
            //    //else { MessageBox.Show("Bạn cần chọn đơn mua hàng trước", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
            //}
            
        }

        private void cbbAboutTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAboutTime.SelectedRow != null)
            {
                var model = cbbAboutTime.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(_startDate.Year,_startDate, model, out dtBegin, out dtEnd);
                dteDateFrom.DateTime = dtBegin;
                dteDateTo.DateTime = dtEnd;
            }
        }

        private void dteDateFrom_ValueChanged(object sender, EventArgs e)
        {
            if (ActiveControl != null && ActiveControl.Name == ((UltraDateTimeEditor)sender).Name)
                cbbAboutTime.SelectedRow = cbbAboutTime.Rows[34];
        }

        private void dteDateTo_ValueChanged(object sender, EventArgs e)
        {
            if (ActiveControl != null && ActiveControl.Name == ((UltraDateTimeEditor)sender).Name)
                cbbAboutTime.SelectedRow = cbbAboutTime.Rows[34];
        }

        private void uGrid_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            // edit by tungnt
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                PPSelectOrder temp = (PPSelectOrder)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                var ppOder = Utils.IPPOrderService.Query.FirstOrDefault(x => x.ID == temp.ID);
                var f = new FPPOrderDetail(ppOder, new List<PPOrder>() { ppOder }, ConstFrm.optStatusForm.View);
                f.ShowDialog(this);
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            var lstPpOrder = from u in _lstSelectOrder where u.CheckColumn select u;
            if (lstPpOrder.Any(x => x.CheckColumn == true))
            {
                SelectOrders = lstPpOrder.ToList();
                DialogResult = DialogResult.OK;
                Close();
            }
            else
            {
                MSG.Warning("Bạn cần chọn đơn mua hàng trước");
            }
           
        }

        private void cbbAboutTime_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {

        }
    }
}
