﻿namespace Accounting
{
    partial class FPPSelectOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FSASelectOrders));
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObject = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.dteDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dteDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.btnGetData = new Infragistics.Win.Misc.UltraButton();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.btnApply = new Infragistics.Win.Misc.UltraButton();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.btnViewSaOrder = new Infragistics.Win.Misc.UltraButton();
            this.cbbAboutTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAboutTime)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraLabel1
            // 
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance1;
            this.ultraLabel1.Location = new System.Drawing.Point(13, 13);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Khoảng thời gian";
            // 
            // ultraLabel2
            // 
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance2;
            this.ultraLabel2.Location = new System.Drawing.Point(13, 41);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel2.TabIndex = 1;
            this.ultraLabel2.Text = "Đối tượng";
            // 
            // cbbAccountingObject
            // 
            this.cbbAccountingObject.AutoSize = false;
            appearance3.BackColor = System.Drawing.SystemColors.Window;
            appearance3.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountingObject.DisplayLayout.Appearance = appearance3;
            this.cbbAccountingObject.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountingObject.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance4.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance4.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.Appearance = appearance4;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.BandLabelAppearance = appearance5;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance6.BackColor2 = System.Drawing.SystemColors.Control;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.PromptAppearance = appearance6;
            this.cbbAccountingObject.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountingObject.DisplayLayout.MaxRowScrollRegions = 1;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountingObject.DisplayLayout.Override.ActiveCellAppearance = appearance7;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountingObject.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.cbbAccountingObject.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountingObject.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountingObject.DisplayLayout.Override.CellAppearance = appearance10;
            this.cbbAccountingObject.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountingObject.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance12.TextHAlignAsString = "Left";
            this.cbbAccountingObject.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.cbbAccountingObject.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountingObject.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountingObject.DisplayLayout.Override.RowAppearance = appearance13;
            this.cbbAccountingObject.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountingObject.DisplayLayout.Override.TemplateAddRowAppearance = appearance14;
            this.cbbAccountingObject.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObject.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountingObject.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountingObject.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObject.Location = new System.Drawing.Point(120, 41);
            this.cbbAccountingObject.Name = "cbbAccountingObject";
            this.cbbAccountingObject.Size = new System.Drawing.Size(170, 22);
            this.cbbAccountingObject.TabIndex = 3;
            // 
            // ultraLabel3
            // 
            appearance15.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance15;
            this.ultraLabel3.Location = new System.Drawing.Point(271, 13);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(31, 20);
            this.ultraLabel3.TabIndex = 4;
            this.ultraLabel3.Text = "Từ";
            // 
            // dteDateFrom
            // 
            this.dteDateFrom.AutoSize = false;
            this.dteDateFrom.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.dteDateFrom.Location = new System.Drawing.Point(298, 13);
            this.dteDateFrom.MaskInput = "dd/mm/yyyy";
            this.dteDateFrom.Name = "dteDateFrom";
            this.dteDateFrom.Size = new System.Drawing.Size(84, 22);
            this.dteDateFrom.TabIndex = 5;
            this.dteDateFrom.ValueChanged += new System.EventHandler(this.dteDateFrom_ValueChanged);
            // 
            // dteDateTo
            // 
            this.dteDateTo.AutoSize = false;
            this.dteDateTo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.dteDateTo.Location = new System.Drawing.Point(428, 13);
            this.dteDateTo.MaskInput = "dd/mm/yyyy";
            this.dteDateTo.Name = "dteDateTo";
            this.dteDateTo.Size = new System.Drawing.Size(84, 22);
            this.dteDateTo.TabIndex = 7;
            this.dteDateTo.ValueChanged += new System.EventHandler(this.dteDateTo_ValueChanged);
            // 
            // ultraLabel4
            // 
            appearance16.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance16;
            this.ultraLabel4.Location = new System.Drawing.Point(391, 13);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(31, 20);
            this.ultraLabel4.TabIndex = 6;
            this.ultraLabel4.Text = "Đến";
            // 
            // btnGetData
            // 
            this.btnGetData.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnGetData.Location = new System.Drawing.Point(298, 41);
            this.btnGetData.Name = "btnGetData";
            this.btnGetData.Size = new System.Drawing.Size(84, 22);
            this.btnGetData.TabIndex = 8;
            this.btnGetData.Text = "&Lấy dữ liệu";
            this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
            // 
            // uGrid
            // 
            this.uGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.Location = new System.Drawing.Point(12, 82);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(868, 489);
            this.uGrid.TabIndex = 9;
            this.uGrid.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid_DoubleClickRow);
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance17.Image = ((object)(resources.GetObject("appearance17.Image")));
            this.btnApply.Appearance = appearance17;
            this.btnApply.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnApply.Location = new System.Drawing.Point(696, 577);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(89, 31);
            this.btnApply.TabIndex = 10;
            this.btnApply.Text = "Đồ&ng ý";
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance18.Image = ((object)(resources.GetObject("appearance18.Image")));
            this.btnCancel.Appearance = appearance18;
            this.btnCancel.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnCancel.Location = new System.Drawing.Point(791, 577);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(89, 31);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "&Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnViewSaOrder
            // 
            this.btnViewSaOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewSaOrder.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnViewSaOrder.Location = new System.Drawing.Point(567, 577);
            this.btnViewSaOrder.Name = "btnViewSaOrder";
            this.btnViewSaOrder.Size = new System.Drawing.Size(123, 31);
            this.btnViewSaOrder.TabIndex = 12;
            this.btnViewSaOrder.Text = "&Xem đơn đặt hàng";
            this.btnViewSaOrder.Click += new System.EventHandler(this.btnViewSaOrder_Click);
            // 
            // cbbAboutTime
            // 
            this.cbbAboutTime.AutoSize = false;
            this.cbbAboutTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAboutTime.Location = new System.Drawing.Point(120, 13);
            this.cbbAboutTime.Name = "cbbAboutTime";
            this.cbbAboutTime.Size = new System.Drawing.Size(145, 22);
            this.cbbAboutTime.TabIndex = 64;
            this.cbbAboutTime.ValueChanged += new System.EventHandler(this.cbbAboutTime_ValueChanged);
            // 
            // FSASelectOrders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(892, 615);
            this.Controls.Add(this.cbbAboutTime);
            this.Controls.Add(this.btnViewSaOrder);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.uGrid);
            this.Controls.Add(this.btnGetData);
            this.Controls.Add(this.dteDateTo);
            this.Controls.Add(this.ultraLabel4);
            this.Controls.Add(this.dteDateFrom);
            this.Controls.Add(this.ultraLabel3);
            this.Controls.Add(this.cbbAccountingObject);
            this.Controls.Add(this.ultraLabel2);
            this.Controls.Add(this.ultraLabel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FSASelectOrders";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chọn đơn đặt hàng";
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAboutTime)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObject;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDateFrom;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDateTo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraButton btnGetData;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.Misc.UltraButton btnApply;
        private Infragistics.Win.Misc.UltraButton btnCancel;
        private Infragistics.Win.Misc.UltraButton btnViewSaOrder;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAboutTime;
    }
}