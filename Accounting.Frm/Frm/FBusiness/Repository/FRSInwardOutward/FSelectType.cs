﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;

namespace Accounting
{
    public partial class FSelectType : CustormForm
    {
        private DateTime _startDate;
        public int TypeID { get; private set; }
        private List<int> _listTypeID { get; set; }
        public FSelectType(List<int> ListTypeID)
        {

            InitializeComponent();
            TypeID = 0;
            _listTypeID = ListTypeID;
            ConfigControl();
        }

        private void ConfigControl()
        {
            var stringToDateTime = Utils.StringToDateTime(ConstFrm.DbStartDate);
            if (stringToDateTime != null)
                _startDate = (DateTime)stringToDateTime;
            else _startDate = DateTime.Now;
            // config combobox Type
            List<Accounting.Core.Domain.Type> _lstItems = Utils.ListType.Where(o => _listTypeID.Contains(o.ID)).ToList();
            Utils.ConfigCombo(this, _lstItems, cbbType, "TypeName", "ID");
        }

        
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

       
        private void cbbType_ValueChanged(object sender, EventArgs e)
        {
            if (cbbType.SelectedRow != null && cbbType.SelectedRow.ListObject != null)
            {
                var model = cbbType.SelectedRow.ListObject as Accounting.Core.Domain.Type;
                TypeID = model.ID;
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            if (TypeID != 0)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
            else
            {
                MSG.Warning("Bạn cần chọn loại chứng từ trước");
            }
           
        }
        
    }
}
