﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinGrid;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinTabControl;

namespace Accounting
{
    public partial class FRSInwardOutwardDetail : FrmTmp1
    {

        #region khai báo
        BindingList<RSInwardOutwardDetail> _bdlSumDetail = new BindingList<RSInwardOutwardDetail>();
        public  double exchangerate;
        private UltraGrid uGridInwardOutwardDetail;
        private UltraGrid uGridStatistics;
        UltraGrid uGrid0;
        UltraGrid uGrid1;
        UltraGrid ugrid2 = null;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private IRefVoucherRSInwardOutwardService _refVoucherRSInwardOutwardService { get { return IoC.Resolve<IRefVoucherRSInwardOutwardService>(); } }
        #endregion
        #region khởi tạo
        public FRSInwardOutwardDetail(RSInwardOutward temp, List<RSInwardOutward> dsRSInwardOutward, int statusForm)
        {//Add -> thêm, Sửa -> sửa, Xem -> Sửa ở trạng thái Enable = False

            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();
            
            //this.WindowState = FormWindowState.Maximized;
            utmDetailBaseToolBar.Ribbon.NonInheritedRibbonTabs[0].Caption = "Phiếu nhập";
            #endregion

            #region Thiết lập ban đầu cho Form
            _statusForm = statusForm;
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.TypeID = 400;
            }
            else _select = temp;
            _listSelects.AddRange(dsRSInwardOutward);
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, dsRSInwardOutward, statusForm);
            //uGridInwardOutward.Rows[0].Cells["TotalAmountOriginal"].Value = _select.TotalAmountOriginal;
            #endregion
        }
        List<Currency> ListCurrency = new List<Currency>();
        #endregion
        #region override
        public override void InitializeGUI(RSInwardOutward input)
        {
            #region Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            Template mauGiaoDien = Utils.GetMauGiaoDien(input.TypeID, input.TemplateID, Utils.ListTemplate);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;
            #endregion
            _select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add ? "VND" : input.CurrencyID;
            _select.ExchangeRate = _statusForm == ConstFrm.optStatusForm.Add ? 1 : input.ExchangeRate;
            #endregion

            #region Thiết lập dữ liệu và Fill dữ liệu Obj vào control (nếu đang sửa)

            #endregion

            var type = Utils.ListType.FirstOrDefault(p => p.ID.Equals(_select.TypeID));
            if (type != null)
                grpTop.Text = type.TypeName.ToUpper();

            this.ConfigTopVouchersNo<RSInwardOutward>(palTopVouchers, "No", "PostedDate", "Date");
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            BindingList<RefVoucherRSInwardOutward> bdlRefVoucherRS = new BindingList<RefVoucherRSInwardOutward>();
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
            }
            if (input.TypeID.Equals(402))
            {
                BindingList<PPInvoiceDetail> commonBinding = new BindingList<PPInvoiceDetail>();
                _selectJoin = new PPInvoice();
                _selectJoin = IPPInvoiceService.Getbykey(input.ID);
                if (_selectJoin != null)
                {
                    if (_statusForm != ConstFrm.optStatusForm.Add)
                    {
                        commonBinding = new BindingList<PPInvoiceDetail>(((PPInvoice)_selectJoin).PPInvoiceDetails);
                        bdlRefVoucher = new BindingList<RefVoucher>(((PPInvoice)_selectJoin).RefVouchers);
                    }
                    
                    _listObjectInput = new BindingList<System.Collections.IList> { commonBinding };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                    this.ConfigGridByTemplete_General<RSInwardOutward>(pnlUgrid, mauGiaoDien);

                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
                    this.ConfigGridByManyTemplete<RSInwardOutward>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
                    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                }
                btnOriginalVoucher.Visible = false;
            }
            else if (input.TypeID.Equals(403))
            {
                BindingList<SAReturnDetail> commonBinding = new BindingList<SAReturnDetail>();
                _selectJoin = new SAReturn();
                _selectJoin = ISAReturnService.Getbykey(input.ID);
                if (_selectJoin != null)
                {
                    if (_statusForm != ConstFrm.optStatusForm.Add)
                    {
                        commonBinding = new BindingList<SAReturnDetail>(((SAReturn)_selectJoin).SAReturnDetails);
                        bdlRefVoucher = new BindingList<RefVoucher>(((SAReturn)_selectJoin).RefVouchers);
                    }
                    
                    _listObjectInput = new BindingList<System.Collections.IList> { commonBinding };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                    this.ConfigGridByTemplete_General<RSInwardOutward>(pnlUgrid, mauGiaoDien);

                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, false };
                    this.ConfigGridByManyTemplete<RSInwardOutward>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    ugrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
                    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                }
                btnOriginalVoucher.Visible = false;
            }
            else
            {
                BindingList<RSInwardOutwardDetail> commonBinding = new BindingList<RSInwardOutwardDetail>();
                if (_statusForm != ConstFrm.optStatusForm.Add)
                {
                    commonBinding = new BindingList<RSInwardOutwardDetail>(input.RSInwardOutwardDetails);
                    bdlRefVoucherRS = new BindingList<RefVoucherRSInwardOutward>(input.RefVoucherRSInwardOutwards);
                }
                
                if (input.TypeID.Equals(404))
                {
                    _listObjectInput = new BindingList<System.Collections.IList> { commonBinding };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucherRS };
                    this.ConfigGridByTemplete_General<RSInwardOutward>(pnlUgrid, mauGiaoDien);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, false };
                    this.ConfigGridByManyTemplete<RSInwardOutward>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    ugrid2 = Controls.Find("uGrid1", true).FirstOrDefault() as UltraGrid;
                    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                    btnOriginalVoucher.Visible = true;//add by cuongpv
                }
                else
                {
                    _listObjectInput = new BindingList<System.Collections.IList> { commonBinding };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucherRS };
                    this.ConfigGridByTemplete_General<RSInwardOutward>(pnlUgrid, mauGiaoDien);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, false };
                    this.ConfigGridByManyTemplete<RSInwardOutward>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    ugrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
                    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                    btnOriginalVoucher.Visible = true;//add by cuongpv
                }
                if(input.TypeID.Equals(401))
                {
                    var ultraTabControl = (UltraTabControl)pnlUgrid.Controls.Find("ultraTabControl", true).FirstOrDefault();
                    ultraTabControl.Tabs[2].Visible = false;
                    btnOriginalVoucher.Visible = false;
                }
            }

            if (input.TypeID.Equals(402))
            {
                uGrid0 = (UltraGrid)pnlUgrid.Controls.Find("uGrid0", true).FirstOrDefault();
                uGrid1 = (UltraGrid)pnlUgrid.Controls.Find("uGrid1", true).FirstOrDefault();
                uGrid0.SummaryValueChanged += uGrid0_SummaryValueChanged;
                uGrid0.DisplayLayout.Bands[0].Columns["ImportTaxExpenseAmount"].Header.Caption = "Phí trước hải quan";
                if (input.IsImportPurchase == false)
                {
                    uGrid1.DisplayLayout.Bands[0].Columns["ImportTaxRate"].Hidden = true;
                    uGrid1.DisplayLayout.Bands[0].Columns["ImportTaxAmountOriginal"].Hidden = true;
                    uGrid1.DisplayLayout.Bands[0].Columns["ImportTaxAccount"].Hidden = true;
                    uGrid1.DisplayLayout.Bands[0].Columns["ImportTaxAmount"].Hidden = true;
                }
                else
                {
                    
                    
                }
            }
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            _select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmount;
            _select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add ? "VND" : input.CurrencyID;
            List<RSInwardOutward> inputCurrency = new List<RSInwardOutward> { _select };
            this.ConfigGridCurrencyByTemplate<RSInwardOutward>(_select.TypeID, new BindingList<System.Collections.IList> { inputCurrency },
                                              uGridInwardOutward, mauGiaoDien);
            
            this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectName, "AccountingObjectCode", "ID", input, "AccountingObjectID");

            DataBinding(new List<Control> { txtAddress, txtAccountingObjectName, txtReason, txtContactName, txtOriginalNo }, "AccountingObjectAddress,AccountingObjectName,Reason,ContactName,OriginalNo");
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 400)//trungnq thêm vào để  làm task 6234
                {
                    txtReason.Value = "Nhập kho";
                    txtReason.Text = "Nhập kho";
                    _select.Reason = "Nhập kho";
                };
            }
        }

        private void btnselectOrder_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> _lstTypeID = new List<int> { 900, 901, 200 };
                var f = new FSelectType(_lstTypeID);
                f.FormClosed += new FormClosedEventHandler(fSelectType_FormClosed);
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                return;
            }
        }

        #endregion

        #region Utils

        #endregion

        private void uGrid0_SummaryValueChanged(object sender, SummaryValueChangedEventArgs e)
        {
            var columns = uGrid0.DisplayLayout.Bands[0].Columns;
            if (columns.Exists("InwardAmountOriginal"))
            {
                if (Convert.ToDecimal(uGrid0.Rows.SummaryValues["SumInwardAmountOriginal"].Value.ToString()) != 0)
                {
                    uGridControl.Rows[0].Cells["TotalAmountOriginal"].Value = Convert.ToDecimal(uGrid0.Rows.SummaryValues["SumInwardAmountOriginal"].Value.ToString());
                    uGridControl.Rows[0].Cells["TotalAmount"].Value = Convert.ToDecimal(uGrid0.Rows.SummaryValues["SumInwardAmount"].Value.ToString());
                }
            }
        }

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucherRSInwardOutward> datasource = (BindingList<RefVoucherRSInwardOutward>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucherRSInwardOutward>();
                    var f = new FViewVoucher(_select.CurrencyID, datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucher)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucherRSInwardOutward>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucherRSInwardOutward)
                    {
                        source.Add(new RefVoucherRSInwardOutward
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                if (_select.TypeID != 402 && _select.TypeID != 403)
                {
                    RefVoucherRSInwardOutward temp = (RefVoucherRSInwardOutward)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                    if (temp != null)
                        editFuntion(temp);
                }
                else
                {
                    RefVoucher _temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                    if (_temp != null)
                        editFuntion(_temp);
                }

            }
        }

        private void editFuntion(RefVoucherRSInwardOutward temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void FRSInwardOutwardDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }
    public class FrmTmp1 : DetailBase<RSInwardOutward>
    {
    }
}