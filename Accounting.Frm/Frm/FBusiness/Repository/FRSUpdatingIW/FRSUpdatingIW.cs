﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Drawing;

namespace Accounting
{
    public partial class FRSUpdatingIW : CustormForm
    {
        public IMaterialGoodsService _IMaterialGoodsService { get { return IoC.Resolve<IMaterialGoodsService>(); } }
        public IMaterialGoodsCategoryService _IMaterialGoodsCategoryService { get { return IoC.Resolve<IMaterialGoodsCategoryService>(); } }
        public IRepositoryService _IRepositoryService { get { return IoC.Resolve<IRepositoryService>(); } }
        public IRSInwardOutwardService _IRSInwardOutwardService { get { return IoC.Resolve<IRSInwardOutwardService>(); } }

        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;

        readonly DateTime _ngayHoachToan = DateTime.ParseExact(ConstFrm.DbStartDate, "dd/MM/yyyy",
                                                    CultureInfo.InvariantCulture);
        private static List<Repository> _listrepos = new List<Repository>();
        private List<MaterialGoods> _lstMaterialGoods = new List<MaterialGoods>();
        private List<MaterialGoods> _lstMaterialGoodsInitial = new List<MaterialGoods>();
        public FRSUpdatingIW()
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();

            cbbAboutTime.DataSource = _lstItems;
            cbbAboutTime.DisplayMember = "Name";
            //cbbAboutTime.Value = _lstItems[3].Name;
            if (_lstItems.Count() > 0)
            {
                cbbAboutTime.Rows[_lstItems.FindIndex(c => c.Value == "34")].Selected = true;
                cbbAboutTime.Rows[_lstItems.FindIndex(c => c.Value == "34")].Activated = true;
            }
            Utils.ConfigGrid(cbbAboutTime, ConstDatabase.ConfigXML_TableName);

            var thisMonthStart = _ngayHoachToan.AddDays(1 - _ngayHoachToan.Day);
            var thisMonthEnd = thisMonthStart.AddMonths(1).AddSeconds(-1);

            dteFromDate.DateTime = thisMonthStart;
            dteToDate.DateTime = thisMonthEnd;

            List<MaterialGoodsCategory> listMgcDb = _IMaterialGoodsCategoryService.GetAll_ByIsActive(true);
            List<MaterialGoodsCategory> listMgCDisplay = new List<MaterialGoodsCategory>();

            MaterialGoodsCategory itemMgCfirst = new MaterialGoodsCategory { MaterialGoodsCategoryName = "Tất cả", MaterialGoodsCategoryCode = "Tất cả" };
            //MaterialGoodsCategory itemMgclast = new MaterialGoodsCategory { MaterialGoodsCategoryName = "Loại khác", MaterialGoodsCategoryCode = "Loại khác" };
            listMgCDisplay.Add(itemMgCfirst);
            listMgCDisplay.AddRange(listMgcDb);
            //listMgCDisplay.Add(itemMgclast);
            cbbMgCategory.DataSource = listMgCDisplay;
            cbbMgCategory.DisplayMember = "MaterialGoodsCategoryName";
            cbbMgCategory.ValueMember = "ID";

            //uMgCategory.Rows[0].Selected = true;


            Utils.ConfigGrid(cbbMgCategory, ConstDatabase.MaterialGoodsCategory_TableName);

            List<Repository> listResDb = _IRepositoryService.GetAll_ByIsActive(true);
            List<Repository> listRepos = new List<Repository>();
            Repository itemRfirst = new Repository { RepositoryName = "Tất cả", RepositoryCode = "Tất cả" };
            listRepos.Add(itemRfirst);
            listRepos.AddRange(listResDb);
            cbbRepository.DataSource = listRepos;
            cbbRepository.DisplayMember = "RepositoryCode";
            cbbRepository.Rows[0].Activated = true;
            cbbRepository.SelectedRow = cbbRepository.Rows[0];

            Utils.ConfigGrid(cbbRepository, ConstDatabase.Repository_TableName);
            _lstMaterialGoodsInitial = _IMaterialGoodsService.GetAll();
            _IMaterialGoodsService.UnbindSession(_lstMaterialGoodsInitial);
            _lstMaterialGoodsInitial = _IMaterialGoodsService.GetAll();
            _lstMaterialGoods = Utils.CloneObject(_lstMaterialGoodsInitial);
            uGridSelectMG.SetDataBinding(_lstMaterialGoods.OrderBy(x => x.MaterialGoodsCode).ToList(), "");

            Utils.ConfigGrid(uGridSelectMG, ConstDatabase.MaterialGoods_TableName_FRSUpdatingIW);

            uGridSelectMG.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGridSelectMG.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGridSelectMG.DisplayLayout.Override.SelectedRowAppearance.BackColor = Color.White;
            uGridSelectMG.DisplayLayout.Override.HotTrackRowAppearance.ForeColor = Color.White;
            uGridSelectMG.DisplayLayout.Override.HotTrackRowSelectorAppearance.BackColor = Color.FromArgb(51, 153, 255);
            uGridSelectMG.DisplayLayout.Override.HotTrackRowCellAppearance.BackColor = Color.FromArgb(51, 153, 255);
            uGridSelectMG.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            uGridSelectMG.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            //uGridSelectMG.DisplayLayout.Bands[0].Summaries.Clear();
            uGridSelectMG.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            UltraGridBand band = uGridSelectMG.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;
            Utils.FormatNumberic(band.Columns["Dgchuyendoi"], ConstDatabase.Format_DonGiaQuyDoi);
            _listrepos = new List<Repository>();
            _listrepos = _IRepositoryService.GetAll_ByIsActive(true);

            cbbMgCategory.Rows[0].Activated = true;
            cbbMgCategory.SelectedRow = cbbMgCategory.Rows[0];
            cbbAboutTime.DropDownStyle = UltraComboStyle.DropDownList;
            cbbRepository.DropDownStyle = UltraComboStyle.DropDownList;
            cbbMgCategory.DropDownStyle = UltraComboStyle.DropDownList;
            WaitingFrm.StopWaiting();

        }

        private void cbbAboutTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAboutTime.SelectedRow != null)
            {
                var model = cbbAboutTime.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(_ngayHoachToan.Year,_ngayHoachToan, model, out dtBegin, out dtEnd);
                dteFromDate.DateTime = dtBegin;
                dteToDate.DateTime = dtEnd;
                if (model != null && model.Value != "34")
                {
                    dteFromDate.ReadOnly = true;
                    dteToDate.ReadOnly = true;
                }
                else
                {
                    dteFromDate.ReadOnly = false;
                    dteToDate.ReadOnly = false;
                }
            }
        }


        private void btnKetThuc_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnThucHien_Click(object sender, EventArgs e)
        {

            if ((_lstMaterialGoods != null) && (_lstMaterialGoods.Any(c => c.Status)))
            {
                bool isDD = Utils.ListSystemOption.FirstOrDefault(x => x.Code == "VTHH_PPTinhGiaXKho").Data == "Đích danh";
                WaitingFrm.StartWaiting();
                if (!_IRSInwardOutwardService.GetListInwardOutwardForUpdatingPrice(_lstMaterialGoods.Where(c => c.Status).ToList(), _listrepos, dteFromDate.DateTime, dteToDate.DateTime, isDD))
                {
                    WaitingFrm.StopWaiting();
                    MessageBox.Show("Có lỗi khi thực hiện", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    WaitingFrm.StopWaiting();
                    MessageBox.Show("Tính giá nhập thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                foreach (var item in uGridSelectMG.Rows)
                {
                    item.Cells["Status"].Value = false;
                    item.Cells["Dgchuyendoi"].Value = 0;
                }
                #region code cũ
                //foreach (var itemMg in _lstMaterialGoods)
                //{
                //    foreach (var itemRepos in _listrepos)
                //    {
                //        try
                //        {
                //            List<RSInwardOutward> listRs = _IRSInwardOutwardService.GetListInwardOutwardForUpdatingPrice(itemMg, itemRepos, dteFromDate.DateTime, dteToDate.DateTime);
                //            foreach (var rsIwUpdating in listRs)
                //            {
                //                //Lấy ra Detail RSInwardOutward để cập nhật đơn giá mới
                //                RSInwardOutwardDetail rsDetailTemp = (RSInwardOutwardDetail)_IRSInwardOutwardDetailService.getRSInwardOutwardDetaillbyID(rsIwUpdating.ID);
                //                rsDetailTemp.UnitPrice = itemMg.Dgchuyendoi;
                //                rsDetailTemp.UnitPriceOriginal = itemMg.Dgchuyendoi;
                //                decimal amountGL = (decimal)(rsDetailTemp.Quantity * itemMg.Dgchuyendoi);
                //                _IRSInwardOutwardDetailService.Update(rsDetailTemp);
                //                //Sửa dữ liệu trong sổ cái
                //                List<GeneralLedger> listGenTemp = _IGeneralLedgerService.GetByReferenceID(rsIwUpdating.ID);
                //                foreach (var generalLedger in listGenTemp)
                //                {
                //                    generalLedger.CreditAmount = amountGL;
                //                    generalLedger.CreditAmountOriginal = amountGL;
                //                    _IGeneralLedgerService.Update(generalLedger);
                //                }
                //                //Sửa dữ liệu trong sổ kho
                //                List<RepositoryLedger> listRepos =
                //                    _IRepositoryLedgerService.GetByListReferenceID(rsIwUpdating.ID);
                //                foreach (var repositoryLedger in listRepos)
                //                {
                //                    repositoryLedger.UnitPrice = 0;
                //                    _IRepositoryLedgerService.Update(repositoryLedger);
                //                }

                //            }
                //        }
                //        catch (Exception)
                //        {
                //            _IRSInwardOutwardService.RolbackTran();
                //            throw;
                //        }
                //    }
                //}
                #endregion
            }

            else
            {
                MessageBox.Show("Chưa chọn vật tư nào để tính", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void uMgCategory_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row != null)
            {
                var selectMGC = (MaterialGoodsCategory)e.Row.ListObject;
                if (selectMGC.MaterialGoodsCategoryName == "Tất cả")
                {
                    _lstMaterialGoods = _lstMaterialGoodsInitial.Where(c => c.IsActive).ToList();
                    uGridSelectMG.SetDataBinding(_lstMaterialGoods.OrderBy(x => x.MaterialGoodsCode).ToList(), "");
                }
                else if (selectMGC.MaterialGoodsCategoryName == "Loại khác")
                {
                    _lstMaterialGoods = _lstMaterialGoodsInitial.Where(c => c.MaterialGoodsCategoryID == null).ToList();
                    uGridSelectMG.SetDataBinding(_lstMaterialGoods.OrderBy(x => x.MaterialGoodsCode).ToList(), "");
                }
                else
                {
                    _lstMaterialGoods = _lstMaterialGoodsInitial.Where(c => c.MaterialGoodsCategoryID == selectMGC.ID && c.IsActive).ToList();
                    uGridSelectMG.SetDataBinding(_lstMaterialGoods.OrderBy(x => x.MaterialGoodsCode).ToList(), "");
                }
            }
        }

        private void uRepository_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row != null)
            {
                var selectRes = (Repository)e.Row.ListObject;
                if (selectRes.RepositoryName == "Tất cả")
                {
                    _lstMaterialGoods = _lstMaterialGoodsInitial.Where(c => c.IsActive).ToList();
                    uGridSelectMG.SetDataBinding(_lstMaterialGoods.OrderBy(x => x.MaterialGoodsCode).ToList(), "");
                    //_listrepos = _IRepositoryService.GetAll_ByIsActive(true);

                }
                else
                {
                    _lstMaterialGoods = _lstMaterialGoodsInitial.Where(c => c.RepositoryID == selectRes.ID && c.IsActive).ToList();
                    uGridSelectMG.SetDataBinding(_lstMaterialGoods.OrderBy(x => x.MaterialGoodsCode).ToList(), "");

                }
            }
        }

        private void uGridSelectMG_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            UltraGridCell activeCell = grid.ActiveCell;
            if (activeCell.Column.Key == "Dgchuyendoi" && activeCell.Text.Trim('_', ',') == "")
            {
                activeCell.Value = 0;
            }
        }
    }
}
