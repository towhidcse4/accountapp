﻿namespace Accounting
{
    partial class FRSUpdatingIW
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            this.ultraPanelBottom = new Infragistics.Win.Misc.UltraPanel();
            this.uGridSelectMG = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.btnKetThuc = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanelCenter = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBoxBottom = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbRepository = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbMgCategory = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.dteToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dteFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAboutTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.btnThucHien = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanelTop = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanelBottom.ClientArea.SuspendLayout();
            this.ultraPanelBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSelectMG)).BeginInit();
            this.ultraPanelCenter.ClientArea.SuspendLayout();
            this.ultraPanelCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxBottom)).BeginInit();
            this.ultraGroupBoxBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbRepository)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMgCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAboutTime)).BeginInit();
            this.ultraPanelTop.ClientArea.SuspendLayout();
            this.ultraPanelTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraPanelBottom
            // 
            // 
            // ultraPanelBottom.ClientArea
            // 
            this.ultraPanelBottom.ClientArea.Controls.Add(this.uGridSelectMG);
            this.ultraPanelBottom.Location = new System.Drawing.Point(0, 177);
            this.ultraPanelBottom.Name = "ultraPanelBottom";
            this.ultraPanelBottom.Size = new System.Drawing.Size(607, 244);
            this.ultraPanelBottom.TabIndex = 2;
            // 
            // uGridSelectMG
            // 
            this.uGridSelectMG.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridSelectMG.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridSelectMG.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridSelectMG.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridSelectMG.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGridSelectMG.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGridSelectMG.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridSelectMG.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridSelectMG.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridSelectMG.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridSelectMG.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGridSelectMG.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGridSelectMG.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGridSelectMG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridSelectMG.Location = new System.Drawing.Point(0, 0);
            this.uGridSelectMG.Name = "uGridSelectMG";
            this.uGridSelectMG.Size = new System.Drawing.Size(607, 244);
            this.uGridSelectMG.TabIndex = 8;
            this.uGridSelectMG.Text = "uGrid";
            this.uGridSelectMG.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.uGridSelectMG_BeforeExitEditMode);
            // 
            // btnKetThuc
            // 
            appearance1.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnKetThuc.Appearance = appearance1;
            this.btnKetThuc.Location = new System.Drawing.Point(518, 429);
            this.btnKetThuc.Name = "btnKetThuc";
            this.btnKetThuc.Size = new System.Drawing.Size(89, 26);
            this.btnKetThuc.TabIndex = 3;
            this.btnKetThuc.Text = "Đóng";
            this.btnKetThuc.Click += new System.EventHandler(this.btnKetThuc_Click);
            // 
            // ultraPanelCenter
            // 
            // 
            // ultraPanelCenter.ClientArea
            // 
            this.ultraPanelCenter.ClientArea.Controls.Add(this.ultraGroupBoxBottom);
            this.ultraPanelCenter.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanelCenter.Location = new System.Drawing.Point(0, 67);
            this.ultraPanelCenter.Name = "ultraPanelCenter";
            this.ultraPanelCenter.Size = new System.Drawing.Size(610, 110);
            this.ultraPanelCenter.TabIndex = 1;
            // 
            // ultraGroupBoxBottom
            // 
            appearance2.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraGroupBoxBottom.Appearance = appearance2;
            this.ultraGroupBoxBottom.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rounded;
            this.ultraGroupBoxBottom.Controls.Add(this.cbbRepository);
            this.ultraGroupBoxBottom.Controls.Add(this.ultraLabel6);
            this.ultraGroupBoxBottom.Controls.Add(this.cbbMgCategory);
            this.ultraGroupBoxBottom.Controls.Add(this.ultraLabel4);
            this.ultraGroupBoxBottom.Controls.Add(this.dteToDate);
            this.ultraGroupBoxBottom.Controls.Add(this.dteFromDate);
            this.ultraGroupBoxBottom.Controls.Add(this.ultraLabel3);
            this.ultraGroupBoxBottom.Controls.Add(this.ultraLabel2);
            this.ultraGroupBoxBottom.Controls.Add(this.cbbAboutTime);
            this.ultraGroupBoxBottom.Controls.Add(this.ultraLabel1);
            this.ultraGroupBoxBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBoxBottom.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxBottom.Name = "ultraGroupBoxBottom";
            this.ultraGroupBoxBottom.Size = new System.Drawing.Size(610, 110);
            this.ultraGroupBoxBottom.TabIndex = 1;
            // 
            // cbbRepository
            // 
            appearance3.BackColor = System.Drawing.SystemColors.Window;
            appearance3.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbRepository.DisplayLayout.Appearance = appearance3;
            this.cbbRepository.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbRepository.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance4.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance4.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbRepository.DisplayLayout.GroupByBox.Appearance = appearance4;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbRepository.DisplayLayout.GroupByBox.BandLabelAppearance = appearance5;
            this.cbbRepository.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance6.BackColor2 = System.Drawing.SystemColors.Control;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbRepository.DisplayLayout.GroupByBox.PromptAppearance = appearance6;
            this.cbbRepository.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbRepository.DisplayLayout.MaxRowScrollRegions = 1;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbRepository.DisplayLayout.Override.ActiveCellAppearance = appearance7;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbRepository.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.cbbRepository.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbRepository.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.cbbRepository.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbRepository.DisplayLayout.Override.CellAppearance = appearance10;
            this.cbbRepository.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbRepository.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbRepository.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance12.TextHAlignAsString = "Left";
            this.cbbRepository.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.cbbRepository.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbRepository.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.cbbRepository.DisplayLayout.Override.RowAppearance = appearance13;
            this.cbbRepository.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbRepository.DisplayLayout.Override.TemplateAddRowAppearance = appearance14;
            this.cbbRepository.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbRepository.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbRepository.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbRepository.Location = new System.Drawing.Point(219, 79);
            this.cbbRepository.Name = "cbbRepository";
            this.cbbRepository.Size = new System.Drawing.Size(204, 22);
            this.cbbRepository.TabIndex = 72;
            this.cbbRepository.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.uRepository_RowSelected);
            // 
            // ultraLabel6
            // 
            this.ultraLabel6.Location = new System.Drawing.Point(219, 57);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(100, 23);
            this.ultraLabel6.TabIndex = 71;
            this.ultraLabel6.Text = "Mã kho";
            // 
            // cbbMgCategory
            // 
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbMgCategory.DisplayLayout.Appearance = appearance15;
            this.cbbMgCategory.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbMgCategory.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbMgCategory.DisplayLayout.GroupByBox.Appearance = appearance16;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbMgCategory.DisplayLayout.GroupByBox.BandLabelAppearance = appearance17;
            this.cbbMgCategory.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbMgCategory.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.cbbMgCategory.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbMgCategory.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbMgCategory.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbMgCategory.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.cbbMgCategory.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbMgCategory.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            this.cbbMgCategory.DisplayLayout.Override.CardAreaAppearance = appearance21;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            appearance22.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbMgCategory.DisplayLayout.Override.CellAppearance = appearance22;
            this.cbbMgCategory.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbMgCategory.DisplayLayout.Override.CellPadding = 0;
            appearance23.BackColor = System.Drawing.SystemColors.Control;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbMgCategory.DisplayLayout.Override.GroupByRowAppearance = appearance23;
            appearance24.TextHAlignAsString = "Left";
            this.cbbMgCategory.DisplayLayout.Override.HeaderAppearance = appearance24;
            this.cbbMgCategory.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbMgCategory.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            this.cbbMgCategory.DisplayLayout.Override.RowAppearance = appearance25;
            this.cbbMgCategory.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbMgCategory.DisplayLayout.Override.TemplateAddRowAppearance = appearance26;
            this.cbbMgCategory.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbMgCategory.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbMgCategory.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbMgCategory.Location = new System.Drawing.Point(7, 79);
            this.cbbMgCategory.Name = "cbbMgCategory";
            this.cbbMgCategory.Size = new System.Drawing.Size(172, 22);
            this.cbbMgCategory.TabIndex = 70;
            this.cbbMgCategory.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.uMgCategory_RowSelected);
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.Location = new System.Drawing.Point(7, 57);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(100, 23);
            this.ultraLabel4.TabIndex = 69;
            this.ultraLabel4.Text = "Loại vật tư";
            // 
            // dteToDate
            // 
            appearance27.TextHAlignAsString = "Center";
            appearance27.TextVAlignAsString = "Middle";
            this.dteToDate.Appearance = appearance27;
            this.dteToDate.AutoSize = false;
            this.dteToDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteToDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteToDate.Location = new System.Drawing.Point(338, 24);
            this.dteToDate.MaskInput = "";
            this.dteToDate.Name = "dteToDate";
            this.dteToDate.Size = new System.Drawing.Size(85, 22);
            this.dteToDate.TabIndex = 68;
            this.dteToDate.Value = null;
            // 
            // dteFromDate
            // 
            appearance28.TextHAlignAsString = "Center";
            appearance28.TextVAlignAsString = "Middle";
            this.dteFromDate.Appearance = appearance28;
            this.dteFromDate.AutoSize = false;
            this.dteFromDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteFromDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteFromDate.Location = new System.Drawing.Point(219, 24);
            this.dteFromDate.MaskInput = "";
            this.dteFromDate.Name = "dteFromDate";
            this.dteFromDate.Size = new System.Drawing.Size(84, 22);
            this.dteFromDate.TabIndex = 67;
            this.dteFromDate.Value = null;
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Location = new System.Drawing.Point(338, 4);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(85, 23);
            this.ultraLabel3.TabIndex = 65;
            this.ultraLabel3.Text = "Đến";
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(219, 4);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(84, 23);
            this.ultraLabel2.TabIndex = 64;
            this.ultraLabel2.Text = "Từ";
            // 
            // cbbAboutTime
            // 
            this.cbbAboutTime.Location = new System.Drawing.Point(6, 24);
            this.cbbAboutTime.Name = "cbbAboutTime";
            this.cbbAboutTime.Size = new System.Drawing.Size(173, 22);
            this.cbbAboutTime.TabIndex = 63;
            this.cbbAboutTime.ValueChanged += new System.EventHandler(this.cbbAboutTime_ValueChanged);
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(7, 4);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(100, 23);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Khoảng thời gian";
            // 
            // btnThucHien
            // 
            appearance29.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnThucHien.Appearance = appearance29;
            this.btnThucHien.Location = new System.Drawing.Point(418, 429);
            this.btnThucHien.Name = "btnThucHien";
            this.btnThucHien.Size = new System.Drawing.Size(94, 26);
            this.btnThucHien.TabIndex = 2;
            this.btnThucHien.Text = "Thực hiện";
            this.btnThucHien.Click += new System.EventHandler(this.btnThucHien_Click);
            // 
            // ultraPanelTop
            // 
            // 
            // ultraPanelTop.ClientArea
            // 
            this.ultraPanelTop.ClientArea.Controls.Add(this.ultraLabel5);
            this.ultraPanelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanelTop.Location = new System.Drawing.Point(0, 0);
            this.ultraPanelTop.Name = "ultraPanelTop";
            this.ultraPanelTop.Size = new System.Drawing.Size(610, 67);
            this.ultraPanelTop.TabIndex = 0;
            // 
            // ultraLabel5
            // 
            this.ultraLabel5.Location = new System.Drawing.Point(12, 18);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(586, 37);
            this.ultraLabel5.TabIndex = 0;
            this.ultraLabel5.Text = "Đối với các đơn vị sử dụng phương pháp tính giá bình quân cuối kỳ, sau khi \"Cập n" +
    "hật kho thành phẩm\" bạn cần thực hiện cập nhật giá xuất kho để tính lại giá trị " +
    "tồn kho cuối kỳ";
            // 
            // FRSUpdatingIW
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(610, 456);
            this.Controls.Add(this.ultraPanelBottom);
            this.Controls.Add(this.btnKetThuc);
            this.Controls.Add(this.ultraPanelCenter);
            this.Controls.Add(this.btnThucHien);
            this.Controls.Add(this.ultraPanelTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(626, 495);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(626, 495);
            this.Name = "FRSUpdatingIW";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cập nhật giá nhập kho thành phẩm";
            this.ultraPanelBottom.ClientArea.ResumeLayout(false);
            this.ultraPanelBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridSelectMG)).EndInit();
            this.ultraPanelCenter.ClientArea.ResumeLayout(false);
            this.ultraPanelCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxBottom)).EndInit();
            this.ultraGroupBoxBottom.ResumeLayout(false);
            this.ultraGroupBoxBottom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbRepository)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMgCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAboutTime)).EndInit();
            this.ultraPanelTop.ClientArea.ResumeLayout(false);
            this.ultraPanelTop.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanelTop;
        private Infragistics.Win.Misc.UltraPanel ultraPanelCenter;
        private Infragistics.Win.Misc.UltraPanel ultraPanelBottom;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxBottom;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAboutTime;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteFromDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteToDate;
        private Infragistics.Win.Misc.UltraButton btnKetThuc;
        private Infragistics.Win.Misc.UltraButton btnThucHien;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbRepository;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbMgCategory;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSelectMG;
    }
}