﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Itenso.TimePeriod;

namespace Accounting
{

    public partial class FOffsetLiabilities : CustormForm
    {
        List<OffsetLiabilities> lstOffsetLiabilitieses = new List<OffsetLiabilities>();
        private static IGeneralLedgerService IGeneralLedgerService
        {
            get { return IoC.Resolve<IGeneralLedgerService>(); }
        }
        public IGenCodeService IGenCodeService
        {
            get { return Utils.IGenCodeService; }
        }
        public IGOtherVoucherService IGOtherVoucherService
        {

            get { return IoC.Resolve<IGOtherVoucherService>(); }
        }
        public FOffsetLiabilities()
        {
            InitializeComponent();
            cbbCurrency.RowSelected += cbbCurrency_RowSelected;
            btnExit.Click += btnExit_Click;
            btnOk.Click += btnOk_Click;
            btLoadData.Click += btLoadData_Click;
            //Utils.ConfigGrid<OffsetLiabilities>(ugridOffsetLiabilitieses, Utils.Of)
            ugridOffsetLiabilitieses.AfterExitEditMode += ugridOffsetLiabilitieses_AfterExitEditMode;
            Utils.FormatNumberic(txtExchangeRate, ConstDatabase.Format_Rate);
            ugridOffsetLiabilitieses.SetDataBinding(lstOffsetLiabilitieses.ToList(), "");
            ReportUtils.ProcessControls(this);
            ViewDanhSach(ugridOffsetLiabilitieses);


        }

        private void ugridOffsetLiabilitieses_AfterExitEditMode(object sender, EventArgs e)
        {
            ExitEdit();
        }

        private void ExitEdit()
        {
            if (ugridOffsetLiabilitieses.ActiveCell != null && ugridOffsetLiabilitieses.ActiveCell.Column.Key == "Status")
            {
                ugridOffsetLiabilitieses.UpdateData();
                //ugridOffsetLiabilitieses.Update();

                //OffsetLiabilities offsetLiabilities = ugridOffsetLiabilitieses.ActiveCell.Row.ListObject as OffsetLiabilities;
                //if (offsetLiabilities != null)
                //{
                //    foreach (OffsetLiabilities o in lstOffsetLiabilitieses)
                //    {
                //        if (o.AccountingObjectID == offsetLiabilities.AccountingObjectID)
                //        {
                //            o.Status = offsetLiabilities.Status;
                //        }
                //    }
                //    ugridOffsetLiabilitieses.UpdateData();
                //    ugridOffsetLiabilitieses.Refresh();
                //}
            }
        }
        private void cbbCurrency_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            if (cbbCurrency.SelectedRow.ListObject != null)
            {
                Currency currency = (Currency)cbbCurrency.SelectedRow.ListObject;
                txtExchangeRate.Visible = currency.ID != "VND";
                lbExchangeRate.Visible = currency.ID != "VND";
                txtExchangeRate.Value = currency.ExchangeRate;
                UltraGridBand band = ugridOffsetLiabilitieses.DisplayLayout.Bands[0];
                band.Columns["DebitAmountLastOriginal"].FormatNumberic(currency.ID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
                band.Columns["CreditAmountLastOriginal"].FormatNumberic(currency.ID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
                band.Columns["DebitAmountLast"].Hidden = currency.ID == "VND";
                band.Columns["CreditAmountLast"].Hidden = currency.ID == "VND";


            }
        }

        private void btLoadData_Click(object sender, EventArgs e)
        {
            loadData();
        }
        private void loadData()
        {
            DateTime dtbegindate = dtBeginDate.DateTime;
            DateTime dtenddate = dtEndDate.DateTime;
            DateTime dtBegin = new DateTime(dtbegindate.Year, dtbegindate.Month, dtbegindate.Day, 0, 0, 0);
            DateTime dtEnd = new DateTime(dtenddate.Year, dtenddate.Month, dtenddate.Day, 23, 59, 59);
            lstOffsetLiabilitieses = IGeneralLedgerService.GetOffsetLiabilitieses(dtBegin,
                dtEnd, (Currency)cbbCurrency.SelectedRow.ListObject);
            ugridOffsetLiabilitieses.SetDataBinding(lstOffsetLiabilitieses.OrderBy(x => x.AccountingObjectCode).ToList(), "");
        }
        private void ViewDanhSach(UltraGrid ultraGrid)
        {
            Currency currency = (Currency)cbbCurrency.SelectedRow.ListObject;
            Utils.ConfigGrid(ultraGrid, ConstDatabase.FGForSetOff_FormName);
            ultraGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            ultraGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            ultraGrid.Text = "";
            // xét style cho các cột có dạng checkbox
            ultraGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            UltraGridBand band = ultraGrid.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;

            band.Columns["Status"].Header.Fixed = true;
            #region Colums Style
            foreach (var item in ultraGrid.DisplayLayout.Bands[0].Columns)
            {
                if (item.Key.Equals("Status"))
                    item.Width = 30;
            }            
            #endregion
            // xóa dòng xét tổng ở cuối Grid
            ultraGrid.DisplayLayout.Override.AllowRowSummaries = AllowRowSummaries.Default;

            foreach (var column in ultraGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, ultraGrid, true);
            }
            band.Columns["Status"].CellActivation = Activation.AllowEdit;
            band.Columns["DebitAmountLast"].Hidden = currency.ID == "VND";
            band.Columns["CreditAmountLast"].Hidden = currency.ID == "VND";
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            ExitEdit();
            if (lstOffsetLiabilitieses.Count < 2)
            {
                MSG.MessageBoxStand("Không có dữ liệu được chọn", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            ugridOffsetLiabilitieses.Update();
            ugridOffsetLiabilitieses.UpdateData();
            List<OffsetLiabilities> listOffsetLiabilities = new List<OffsetLiabilities>();
            listOffsetLiabilities = lstOffsetLiabilitieses.Where(c => c.Status).OrderBy(x => x.AccountingObjectCode).ThenBy(x => x.CreditAmountLast).ToList();
            foreach (OffsetLiabilities offsetLiabilities in listOffsetLiabilities)
            {
                if (listOffsetLiabilities.Any(c => c.AccountingObjectID == offsetLiabilities.AccountingObjectID && c.Status != offsetLiabilities.Status && c.AccountNumber != offsetLiabilities.AccountNumber)
                    || listOffsetLiabilities.Count < 2
                    || listOffsetLiabilities.Count(c => c.AccountingObjectID == offsetLiabilities.AccountingObjectID) < 2)
                {
                    MSG.MessageBoxStand("Một đối tượng ít nhất phải chọn 2 dòng:" + Environment.NewLine + " + Một dòng chứa số tiền dư nợ" + Environment.NewLine + " + Một dòng chứa số tiền dư có", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            #region thao tác với cơ sở dữ liệu
            try
            {
                int numberVoucher = 0;
                for (int i = 0, j = 1; j < listOffsetLiabilities.Count;)
                {
                    string no = Utils.TaoMaChungTu(IGenCodeService.getGenCode(60));
                    Guid id = Guid.NewGuid();
                    var currency = cbbCurrency.SelectedRow.ListObject as Currency;
                    if (currency != null)
                    {
                        GOtherVoucher gOtherVoucher = new GOtherVoucher()
                        {
                            ID = id,
                            TypeID = 600,
                            PostedDate = (DateTime)dtEndDate.Value,
                            Date = (DateTime)dtEndDate.Value,
                            No = no,
                            Reason = "Bù trừ công nợ cho " + listOffsetLiabilities[i].AccountingObjectName,
                            TotalAmount = listOffsetLiabilities[i].DebitAmountLast >= listOffsetLiabilities[j].CreditAmountLast ?
                                listOffsetLiabilities[j].CreditAmountLast : listOffsetLiabilities[i].DebitAmountLast,
                            TotalAmountOriginal = listOffsetLiabilities[i].DebitAmountLastOriginal >= listOffsetLiabilities[j].CreditAmountLastOriginal ?
                                 listOffsetLiabilities[j].CreditAmountLastOriginal : listOffsetLiabilities[i].DebitAmountLastOriginal,
                            Recorded = true,
                            Exported = true,
                            CurrencyID = currency.ID,
                            ExchangeRate = Decimal.Parse(((double)txtExchangeRate.Value).ToString(CultureInfo.InvariantCulture))
                        };
                        GOtherVoucherDetail gOtherVoucherDetail = new GOtherVoucherDetail
                        {
                            GOtherVoucherID = id,
                            Description = "Bù trừ công nợ cho " + listOffsetLiabilities[i].AccountingObjectName,
                            DebitAccount = listOffsetLiabilities[j].AccountNumber,
                            CreditAccount = listOffsetLiabilities[i].AccountNumber,
                            CurrencyID = currency.ID,
                            ExchangeRate =
                                Decimal.Parse(((double)txtExchangeRate.Value).ToString(CultureInfo.InvariantCulture)),
                            Amount =
                                listOffsetLiabilities[i].DebitAmountLast >= listOffsetLiabilities[j].CreditAmountLast ?
                                listOffsetLiabilities[j].CreditAmountLast : listOffsetLiabilities[i].DebitAmountLast,
                            AmountOriginal =
                                listOffsetLiabilities[i].DebitAmountLastOriginal >= listOffsetLiabilities[j].CreditAmountLastOriginal ?
                                listOffsetLiabilities[j].CreditAmountLastOriginal : listOffsetLiabilities[i].DebitAmountLastOriginal,
                            DebitAccountingObjectID = listOffsetLiabilities[i].AccountingObjectID,
                            CreditAccountingObjectID = listOffsetLiabilities[i].AccountingObjectID,
                            IsMatch = false
                        };

                        gOtherVoucher.GOtherVoucherDetails.Add(gOtherVoucherDetail);
                        //listGOtherVouchers.Add(gOtherVoucher);
                        IGOtherVoucherService.BeginTran();
                        IGOtherVoucherService.CreateNew(gOtherVoucher);

                        #region Up bảng gencode sinh chứng từ tiếp theo
                        GenCode gencode = IGenCodeService.getGenCode(60);
                        Dictionary<string, string> dicNo = Utils.CheckNo(gOtherVoucher.No);
                        if (dicNo != null)
                        {
                            gencode.Prefix = dicNo["Prefix"];
                            decimal _decimal = 0;
                            decimal.TryParse(dicNo["Value"], out _decimal);
                            gencode.CurrentValue = _decimal + 1;
                            gencode.Suffix = dicNo["Suffix"];
                            IGenCodeService.Update(gencode);
                        }
                        #endregion

                        IGOtherVoucherService.CommitTran();
                        //ghi sổ luôn
                        if (IGeneralLedgerService.SaveLedger(gOtherVoucher) == false)
                        {
                            IGOtherVoucherService.RolbackTran();
                            return;
                        }
                    }

                    i = i + 2;
                    j = j + 2;
                    numberVoucher++;
                }

                MessageBox.Show(string.Format("Thêm mới thành công {0} chứng từ để bù trừ công nợ tại phân hệ Tổng hợp/ Chứng từ nghiệp vụ khác", numberVoucher), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                loadData();
            }
            catch (Exception ex)
            {
                IGOtherVoucherService.RolbackTran();
                MSG.Error("Thao tác thất bại");
            }
            #endregion
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
