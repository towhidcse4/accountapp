﻿#region

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.AllOPAccount;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

#endregion

namespace Accounting
{
    public partial class FOPAccountDetail : CustormForm
    {
        #region khai báo
        readonly AllOpAccount _allOpAccount = new AllOpAccount();
        readonly List<Guid> _rowDuocThem = new List<Guid>();
        readonly List<OpAccountDetailByOrther> _lstAccountsAll = new List<OpAccountDetailByOrther>();
        readonly List<OpAccountDetailByOrther> _lstAccountsAllCmp = new List<OpAccountDetailByOrther>();
        readonly List<OpAccountDetailByOrther> _lstAccountsAllCmpChange = new List<OpAccountDetailByOrther>();
        List<OpAccountDetailByOrther> _lstAccounts = new List<OpAccountDetailByOrther>();
        bool _isLuu = true;
        public bool CloseOf = true;
        readonly Dictionary<string, List<OpAccountDetailByOrther>> _dictionary = new Dictionary<string, List<OpAccountDetailByOrther>>();
        readonly Dictionary<string, List<OpAccountDetailByOrther>> _dictionaryBackUp = new Dictionary<string, List<OpAccountDetailByOrther>>();
        List<string> _lstCanCheck = new List<string>();
        readonly List<string> _lstCacDoiTuongLienKetCanNhap = new List<string>() { "BankAccountDetailID", "ContractID", "CostSetID", "ExpenseItemID" };
        List<string> _lstParents = new List<string>();
        int LamTron = 0;
        int LamTron1 = 2;
        #endregion
        public FOPAccountDetail(AllOpAccount allOpAccountIn)
        {
            InitializeComponent();
            uGrid.KeyDown += OPNUntil.uGrid_KeyDown;
            _allOpAccount = allOpAccountIn;
            LamTron = int.Parse(Utils.ListSystemOption.FirstOrDefault(x => x.Code == "DDSo_TienVND").Data);
            LamTron1 = int.Parse(Utils.ListSystemOption.FirstOrDefault(x => x.Code == "DDSo_NgoaiTe").Data);
            _lstCanCheck.Add(_allOpAccount.AccountNumber);
            _lstAccounts = new List<OpAccountDetailByOrther>();
            ConfigGrid(_lstAccounts, true, false);
            _lstAccountsAll = IoC.Resolve<IOPAccountService>().GetListOpAccountDetailByOrthers();
            _lstAccountsAllCmp = _lstAccountsAll.CloneObject();
            _lstParents = IoC.Resolve<IAccountService>().getListParentsAccount();
            #region tạo list số dư đầu kỳ tương ứng với loại tiên
            List<OpAccountDetailByOrther> lstAccountsTao = new List<OpAccountDetailByOrther>();
            foreach (Currency currency in OPNUntil.ListCurrency())
            {
                if (_dictionary != null && _dictionary.All(c => c.Key != currency.ID))
                {
                    lstAccountsTao = currency.ID == "VND" ? _lstAccountsAll.Where(c => c.CurrencyID != null && (c.CurrencyID == currency.ID) && c.DetailType.Split(';').ToList().All(f => f != "8")).ToList()
                        : _lstAccountsAll.Where(c => c.CurrencyID != null && (c.CurrencyID == currency.ID || c.CurrencyID == "") && c.DetailType.Split(';').ToList().Any(f => f == "8")).ToList();
                    foreach (OpAccountDetailByOrther opAccountObjectByOrther in lstAccountsTao)
                    {
                        if (opAccountObjectByOrther.ID == Guid.Empty)
                        {
                            opAccountObjectByOrther.ExchangeRate = (decimal)currency.ExchangeRate;
                            opAccountObjectByOrther.CurrencyID = currency.ID;
                        }
                    }
                    if (currency.ID != "VND")
                    {
                        foreach (Account acc in OPNUntil.ListAccountFollowCurrency())
                        {
                            if (lstAccountsTao.All(c => c.AccountNumber != acc.AccountNumber))
                            {
                                OpAccountDetailByOrther opAccountObjectByOrther = new OpAccountDetailByOrther()
                                {
                                    CurrencyID = currency.ID,
                                    ExchangeRate = (decimal)(currency.ExchangeRate ?? 1),
                                    AccountName = acc.AccountName,
                                    ParentID = acc.ParentID,
                                    DetailType = acc.DetailType,
                                    AccountGroupKind = acc.AccountGroupKind,
                                    ID = Guid.Empty,
                                    TypeID = 700,
                                    PostedDate = OPNUntil.ApplicationStartDateOPN,
                                    AccountNumber = acc.AccountNumber,
                                    DebitAmount = 0,
                                    DebitAmountOriginal = 0,
                                    CreditAmount = 0,
                                    CreditAmountOriginal = 0,
                                    BankAccountDetailID = null,
                                    AccountingObjectID = null,
                                    EmployeeID = null,
                                    ContractID = null,
                                    CostSetID = null,
                                    ExpenseItemID = null,
                                    OrderPriority = 0
                                };
                                lstAccountsTao.Add(opAccountObjectByOrther);
                            }
                        }

                    }
                    _dictionary.Add(currency.ID, lstAccountsTao.OrderBy(c => c.AccountNumber).ToList());
                }
            }
            _dictionaryBackUp = Utils.CloneObject(_dictionary);
            #endregion
            uCbbCurrency.DataSource = OPNUntil.ListCurrency();
            bt_xoa.Enabled = false;
            if (uCbbCurrency.Rows.Count > 0)
            {

                try
                {
                    var opAccountDetailByOrther = _lstAccountsAll.FirstOrDefault(c => c.AccountNumber == _allOpAccount.AccountName && c.CurrencyID != "");
                    if (opAccountDetailByOrther != null)
                    {
                        SelectedCombo(new List<OpAccountDetailByOrther> { opAccountDetailByOrther });
                    }
                    else if (allOpAccountIn.DetailType.Split(';').ToList().Contains("8"))
                        uCbbCurrency.SelectedRow = uCbbCurrency.Rows[0];
                    else
                    {
                        foreach (UltraGridRow item in uCbbCurrency.Rows)
                        {
                            if (((Currency)item.ListObject).ID.Equals("VND")) uCbbCurrency.SelectedRow = item;
                        }
                    }
                    ConfigGrid(_lstAccounts, true);
                    var currency = uCbbCurrency.SelectedRow.ListObject as Currency;
                    ConfigRowAllGrid(uGrid, currency != null && currency.ID == "VND", _lstCanCheck);
                }
                catch (Exception)
                {
                    uCbbCurrency.SelectedRow = uCbbCurrency.Rows[0];
                    ConfigGrid(_lstAccounts, true);
                    var currency = uCbbCurrency.SelectedRow.ListObject as Currency;
                    ConfigRowAllGrid(uGrid, currency != null && currency.ID == "VND", _lstCanCheck);
                }

            }
            Utils.ConfigGrid(uCbbCurrency, ConstDatabase.Currency_TableName);
            uGrid.DisplayLayout.Bands[0].Columns["CostSetID"].Header.Caption = "Đối tượng tập hợp CP";
            XoaDongToolStripMenuItem.Visible = false;
            XoaDongToolStripMenuItem.Click += XoaDongToolStripMenuItem_Click;
        }

        #region sự kiện
        private void uCbbCurrency_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (uCbbCurrency.SelectedRow.ListObject != null && uCbbCurrency.SelectedRow.Index >= 0)
            {
                Currency currency = (Currency)uCbbCurrency.SelectedRow.ListObject;
                if (_lstAccounts.Count > 0)
                {
                    foreach (string key in _dictionary.Keys)
                    {
                        var opAccountDetailByOrther = _lstAccounts.FirstOrDefault();
                        if (opAccountDetailByOrther != null && key == opAccountDetailByOrther.CurrencyID)
                        {
                            _dictionary[key] = _lstAccounts;
                            break;
                        }
                    }
                }
                _lstAccounts = _dictionary.Single(c => c.Key == currency.ID).Value;
                ConfigGrid(_lstAccounts, true);
                ConfigRowAllGrid(uGrid, currency != null && currency.ID == "VND", _lstCanCheck);
            }

        }
        private void uGrid_Error(object sender, ErrorEventArgs e)
        {
            e.Cancel = true;
            if (e.ErrorType == ErrorType.Data && e.ErrorText.Contains("System.Decimal"))
            {
                switch (e.DataErrorInfo.Cell.Column.Key)
                {
                    case "DebitAmount":
                    case "CreditAmount":
                    case "DebitAmountOriginal":
                    case "CreditAmountOriginal":
                        e.DataErrorInfo.Cell.Row.Cells[e.DataErrorInfo.Cell.Column.Key].Value = 0;
                        break;
                }
            }

        }
        private void uGridControl_CellDataError(object sender, CellDataErrorEventArgs e)
        {
            Utils.CellDataError((UltraGrid)sender);
            e.RaiseErrorEvent = false;
        }
        private void uGrid_AfterExitEditMode(object sender, EventArgs e)
        {
            Utils.CheckErrorInActiveCell<OpAccountDetailByOrther>(this, uGrid);
            ErrorGrid();
            AfterExitEditMode();
            UltraGridRow row = uGrid.ActiveRow;
            if (row != null)
            {
                OpAccountDetailByOrther temp = uGrid.Rows[row.Index].ListObject as OpAccountDetailByOrther;
                if (_lstAccountsAllCmpChange.Any(x => x.AccountNumber == temp.AccountNumber && x.CurrencyID == temp.CurrencyID && x.ID == temp.ID))
                {
                    _lstAccountsAllCmpChange.Remove(_lstAccountsAllCmpChange.FirstOrDefault(x => x.AccountNumber == temp.AccountNumber && x.CurrencyID == temp.CurrencyID && x.ID == temp.ID));
                }
                _lstAccountsAllCmpChange.Add(temp);
            }
        }
        private void uGrid_AfterCellActivate(object sender, EventArgs e)
        {
            ErrorGrid();
        }
        private void uGrid_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            e.Cancel = true;
        }
        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                UIElement element = uGrid.DisplayLayout.UIElement.ElementFromPoint(e.Location);
                UltraGridRow row = (UltraGridRow)element.GetContext(typeof(UltraGridRow));
                uGrid.Selected.Rows.Clear();
                uGrid.Rows[row.Index].Selected = true;
                uGrid.Rows[row.Index].Activate();
                OpAccountDetailByOrther opAccountDetailByOrther = new OpAccountDetailByOrther();
                opAccountDetailByOrther = (OpAccountDetailByOrther)row.ListObject;
                XoaDongToolStripMenuItem.Visible = (_lstAccounts.Count(c => c.AccountNumber == opAccountDetailByOrther.AccountNumber) > 1);
                //XoaDongToolStripMenuItem.Click += new EventHandler((s, ev) => XoaDongToolStripMenuItem_Click(e, s, ev));
            }

        }
        private void XoaDongToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpAccountDetailByOrther opAccountDetailByOrther = new OpAccountDetailByOrther();
            try
            {
                opAccountDetailByOrther = (OpAccountDetailByOrther)uGrid.ActiveRow.ListObject;
                _lstAccounts.Remove(opAccountDetailByOrther);
                _rowDuocThem.Remove(opAccountDetailByOrther.ID);
                ConfigGrid(_lstAccounts, true);
                _lstCanCheck = new List<string> { opAccountDetailByOrther.AccountNumber };
                var currency = uCbbCurrency.SelectedRow.ListObject as Currency;
                ConfigRowAllGrid(uGrid, currency != null && currency.ID == "VND", _lstCanCheck);
            }
            catch (Exception)
            {

            }
        }
        private void bt_ThemDongMoi_Click(object sender, EventArgs e)
        {
            int viTriThemMoi = uGrid.ActiveRow.Index + 1;
            OpAccountDetailByOrther opAccount = (OpAccountDetailByOrther)uGrid.ActiveRow.ListObject;
            _lstAccounts.Insert(viTriThemMoi, new OpAccountDetailByOrther());
            _lstAccounts[viTriThemMoi].AccountGroupKind = opAccount.AccountGroupKind;
            _lstAccounts[viTriThemMoi].AccountName = opAccount.AccountName;
            _lstAccounts[viTriThemMoi].AccountNumber = opAccount.AccountNumber;
            _lstAccounts[viTriThemMoi].BankAccountDetailID = opAccount.BankAccountDetailID;
            _lstAccounts[viTriThemMoi].CurrencyID = opAccount.CurrencyID;
            _lstAccounts[viTriThemMoi].DetailType = opAccount.DetailType;
            _lstAccounts[viTriThemMoi].AccountingObjectID = opAccount.AccountingObjectID;
            _lstAccounts[viTriThemMoi].BranchID = opAccount.BranchID;
            _lstAccounts[viTriThemMoi].ContractID = opAccount.ContractID;
            _lstAccounts[viTriThemMoi].CostSetID = opAccount.CostSetID;
            _lstAccounts[viTriThemMoi].EmployeeID = opAccount.EmployeeID;
            _lstAccounts[viTriThemMoi].ExpenseItemID = opAccount.ExpenseItemID;
            _lstAccounts[viTriThemMoi].ParentID = opAccount.ParentID;
            _lstAccounts[viTriThemMoi].ExchangeRate = opAccount.ExchangeRate;
            _lstAccounts[viTriThemMoi].OrderPriority = opAccount.OrderPriority;
            _lstAccounts[viTriThemMoi].TypeID = opAccount.TypeID;
            _lstAccounts[viTriThemMoi].PostedDate = opAccount.PostedDate;
            _lstAccounts[viTriThemMoi].DebitAmount = 0;
            _lstAccounts[viTriThemMoi].DebitAmountOriginal = 0;
            _lstAccounts[viTriThemMoi].CreditAmount = 0;
            _lstAccounts[viTriThemMoi].CreditAmountOriginal = 0;
            _lstAccounts[viTriThemMoi].ID = Guid.NewGuid();
            _rowDuocThem.Add(_lstAccounts[viTriThemMoi].ID);
            ConfigGrid(_lstAccounts, ((Currency)uCbbCurrency.SelectedRow.ListObject).ID != "VND", false);
            uGrid.Rows[viTriThemMoi].Activate();
            uGrid.Rows[viTriThemMoi].Selected = true;
            _lstCanCheck = new List<string> { _lstAccounts[viTriThemMoi].AccountNumber };
            var currency = uCbbCurrency.SelectedRow.ListObject as Currency;
            ConfigRowAllGrid(uGrid, currency != null && currency.ID == "VND", _lstCanCheck);
            //ConfigGrid(_lstAccounts, true, false);//add by namnh
            if (_rowDuocThem.Count > 0) bt_xoa.Enabled = true;
            ErrorGrid();
        }
        private void bt_xoa_Click(object sender, EventArgs e)
        {
            if (_rowDuocThem.Count > 0)
            {
                OpAccountDetailByOrther opAccount = _lstAccounts.Single(c => c.ID == _rowDuocThem.Last());
                _lstAccounts.Remove(opAccount);
                _rowDuocThem.Remove(opAccount.ID);
                ConfigGrid(_lstAccounts, ((Currency)uCbbCurrency.SelectedRow.ListObject).ID != "VND", false);
            }
            else
            {
                bt_xoa.Enabled = false;
            }
        }
        private void bt_dong_Click(object sender, EventArgs e)
        {
            CloseOf = true;
            Close();
        }
        private void bt_luu_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CheckErrorIMGGrid(uGrid))
                {
                    MSG.Error("Lỗi nhập liệu. Vui lòng kiểm tra lại!");
                    return;
                }

                List<OpAccountDetailByOrther> lstopOpAccountDetailByOrtherSave = new List<OpAccountDetailByOrther>();
                if (_lstAccounts.Count > 0)
                {
                    uGrid.UpdateData();
                    foreach (string key in _dictionary.Keys)
                    {
                        var opAccountDetailByOrther = _lstAccounts.FirstOrDefault();
                        if (opAccountDetailByOrther != null && key == opAccountDetailByOrther.CurrencyID)
                        {
                            _dictionary[key] = _lstAccounts;
                            break;
                        }
                    }
                }
                //foreach (List<OpAccountDetailByOrther> lstAccountsList in _dictionary.Values)
                //{
                //    if(lstAccountsList.Count > 0)
                //    {
                //        List<OpAccountDetailByOrther> lst = Compare(lstAccountsList, _lstAccountsAllCmp);
                //        for (int i = lst.Count - 1; i >= 0; i--)
                //        {
                //            List<string> lstDetailType = lst[i].DetailType.Split(';').ToList();
                //            if (!CheckDetailType(lstDetailType, lst[i]))
                //            {
                //                SelectedCombo(lst);
                //                ConfigGrid(_lstAccounts, true);
                //                var currency = uCbbCurrency.SelectedRow.ListObject as Currency;
                //                _lstCanCheck = new List<string> { lst[i].AccountNumber };
                //                ConfigRowAllGrid(uGrid, currency != null && currency.ID == "VND", _lstCanCheck);
                //                lst.RemoveAt(i);
                //                return;
                //            }
                //            #region với tài khoản lưỡng tính thực hiện trừ
                //            if (lst[i].AccountGroupKind == 2)
                //            {
                //                if (lst[i].CreditAmountOriginal > lst[i].DebitAmountOriginal)
                //                {
                //                    lst[i].CreditAmount = lst[i].CreditAmount - lst[i].DebitAmount;
                //                    lst[i].CreditAmountOriginal = lst[i].CreditAmountOriginal - lst[i].DebitAmountOriginal;
                //                    lst[i].DebitAmount = 0;
                //                    lst[i].DebitAmountOriginal = 0;
                //                }
                //                else if (lst[i].DebitAmountOriginal > lst[i].CreditAmountOriginal)
                //                {
                //                    lst[i].DebitAmount = lst[i].DebitAmount - lst[i].CreditAmount;
                //                    lst[i].DebitAmountOriginal = lst[i].DebitAmountOriginal - lst[i].CreditAmountOriginal;
                //                    lst[i].CreditAmount = 0;
                //                    lst[i].CreditAmountOriginal = 0;
                //                }
                //                else if (lst[i].DebitAmountOriginal == lst[i].CreditAmountOriginal && lst[i].DebitAmountOriginal != 0 && lst[i].CreditAmountOriginal != 0)
                //                {
                //                    lst[i].DebitAmount = 0;
                //                    lst[i].CreditAmount = 0;
                //                    lst[i].DebitAmountOriginal = 0;
                //                    lst[i].CreditAmountOriginal = 0;
                //                    lst.RemoveAt(i);

                //                }
                //            }
                //            #endregion
                //        }
                //        if (lst.Count > 0)
                //        {
                //            lstopOpAccountDetailByOrtherSave.AddRange(lst);
                //        }
                //    }

                foreach (var key in _dictionary.Keys)
                {
                    var lstcmp = _lstAccountsAllCmpChange.Where(x => x.CurrencyID == key).ToList();
                    List<OpAccountDetailByOrther> lst = Compare(lstcmp, _lstAccountsAllCmp.Where(x => x.CurrencyID == key).ToList());
                    for (int i = lst.Count - 1; i >= 0; i--)
                    {
                        List<string> lstDetailType = lst[i].DetailType.Split(';').ToList();
                        if (!CheckDetailType(lstDetailType, lst[i]))
                        {
                            SelectedCombo(lst);
                            ConfigGrid(_lstAccounts, true);
                            var currency = uCbbCurrency.SelectedRow.ListObject as Currency;
                            _lstCanCheck = new List<string> { lst[i].AccountNumber };
                            ConfigRowAllGrid(uGrid, currency != null && currency.ID == "VND", _lstCanCheck);
                            lst.RemoveAt(i);
                            return;
                        }
                        #region với tài khoản lưỡng tính thực hiện trừ
                        if (lst[i].AccountGroupKind == 2)
                        {
                            if (lst[i].CreditAmountOriginal > lst[i].DebitAmountOriginal)
                            {
                                lst[i].CreditAmount = lst[i].CreditAmount - lst[i].DebitAmount;
                                lst[i].CreditAmountOriginal = lst[i].CreditAmountOriginal - lst[i].DebitAmountOriginal;
                                lst[i].DebitAmount = 0;
                                lst[i].DebitAmountOriginal = 0;
                            }
                            else if (lst[i].DebitAmountOriginal > lst[i].CreditAmountOriginal)
                            {
                                lst[i].DebitAmount = lst[i].DebitAmount - lst[i].CreditAmount;
                                lst[i].DebitAmountOriginal = lst[i].DebitAmountOriginal - lst[i].CreditAmountOriginal;
                                lst[i].CreditAmount = 0;
                                lst[i].CreditAmountOriginal = 0;
                            }
                            else if (lst[i].DebitAmountOriginal == lst[i].CreditAmountOriginal && lst[i].DebitAmountOriginal != 0 && lst[i].CreditAmountOriginal != 0)
                            {
                                lst[i].DebitAmount = 0;
                                lst[i].CreditAmount = 0;
                                lst[i].DebitAmountOriginal = 0;
                                lst[i].CreditAmountOriginal = 0;
                                lst.RemoveAt(i);

                            }
                        }
                        #endregion
                    }
                    if (lst.Count > 0)
                    {
                        lstopOpAccountDetailByOrtherSave.AddRange(lst);
                    }
                }
                IOPAccountService iop = IoC.Resolve<IOPAccountService>();
                iop.SaveOpAccountAndGeneralLedger(lstopOpAccountDetailByOrtherSave);
                MessageBox.Show(resSystem.OPN01, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                _isLuu = false;
                CloseOf = false;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(resSystem.OPN02, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void FOPAccountDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (uGrid.ActiveCell != null)
                uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
            if (!CloseOf || uCbbCurrency.SelectedRow == null) return; // hautv edit
            if (_isLuu)
            {

                if (!CheckErrorIMGGrid(uGrid))
                {
                    MSG.Error("Lỗi nhập liệu. Vui lòng kiểm tra lại!");
                    e.Cancel = true;
                    return;
                }
                try
                {
                    List<OpAccountDetailByOrther> lstopOpAccountDetailByOrtherSave = new List<OpAccountDetailByOrther>();
                    uGrid.UpdateData();
                    AfterExitEditMode();
                    uGrid.UpdateData();
                    uGrid.Refresh();
                    if (_lstAccounts.Count > 0)
                    {
                        foreach (string key in _dictionary.Keys)
                        {
                            var opAccountDetailByOrther = _lstAccounts.FirstOrDefault();
                            if (opAccountDetailByOrther != null && key == opAccountDetailByOrther.CurrencyID)
                            {
                                _dictionary[key] = _lstAccounts.OrderBy(c => c.AccountNumber).ToList();
                                break;
                            }
                        }
                    }
                    bool check = true;
                    foreach (string key in _dictionary.Keys)
                    {
                        if (_dictionary.Any(c => c.Key == key))
                        {
                            check = Utils.Compare(_dictionaryBackUp[key], _dictionary[key]);
                            if (check == false) break;
                        }
                    }
                    if (check == false && MessageBox.Show("Dữ liệu đã bị thay đổi bạn có muốn lưu không", "Dữ liệu ban đầu thay đổi",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        foreach (List<OpAccountDetailByOrther> lstAccountsList in _dictionary.Values)
                        {
                            List<OpAccountDetailByOrther> lst = Compare(lstAccountsList, _lstAccountsAll);
                            for (int i = lst.Count - 1; i >= 0; i--)
                            {
                                List<string> lstDetailType = lst[i].DetailType.Split(';').ToList();
                                if (!CheckDetailType(lstDetailType, lst[i]))
                                {
                                    e.Cancel = true;
                                    SelectedCombo(lst);
                                    ConfigGrid(_lstAccounts, true);
                                    var currency = uCbbCurrency.SelectedRow.ListObject as Currency;
                                    _lstCanCheck = new List<string> { lst[i].AccountNumber };
                                    ConfigRowAllGrid(uGrid, currency != null && currency.ID == "VND", _lstCanCheck);
                                    lst.RemoveAt(i);
                                    return;
                                }
                                #region với tài khoản lưỡng tính thực hiện trừ
                                //if (lst[i].AccountGroupKind == 2)
                                //{
                                //    if (lst[i].CreditAmountOriginal > lst[i].DebitAmountOriginal)
                                //    {
                                //        lst[i].CreditAmount = lst[i].CreditAmount - lst[i].DebitAmount;
                                //        lst[i].CreditAmountOriginal = lst[i].CreditAmountOriginal - lst[i].DebitAmountOriginal;
                                //        lst[i].DebitAmount = 0;
                                //        lst[i].DebitAmountOriginal = 0;
                                //    }
                                //    else if (lst[i].DebitAmountOriginal > lst[i].CreditAmountOriginal)
                                //    {
                                //        lst[i].DebitAmount = lst[i].DebitAmount - lst[i].CreditAmount;
                                //        lst[i].DebitAmountOriginal = lst[i].DebitAmountOriginal - lst[i].CreditAmountOriginal;
                                //        lst[i].CreditAmount = 0;
                                //        lst[i].CreditAmountOriginal = 0;
                                //    }
                                //    else
                                //    {
                                //        lst[i].DebitAmount = 0;
                                //        lst[i].CreditAmount = 0;
                                //        lst[i].DebitAmountOriginal = 0;
                                //        lst[i].CreditAmountOriginal = 0;
                                //        lst.RemoveAt(i);
                                //    }
                                //}
                                #endregion
                            }
                            if (lst.Count > 0)
                            {
                                lstopOpAccountDetailByOrtherSave.AddRange(lst);
                            }
                        }
                        IOPAccountService iop = IoC.Resolve<IOPAccountService>();
                        iop.SaveOpAccountAndGeneralLedger(lstopOpAccountDetailByOrtherSave);
                        MessageBox.Show(resSystem.OPN01, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        _isLuu = false;
                        CloseOf = false;
                    }

                }
                catch (Exception)
                {
                    MessageBox.Show(resSystem.OPN02, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void uGrid_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
        #endregion

        #region hàm xử lý
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lstAccountDetailByOrthers">danh sách dữ liệu</param>
        /// <param name="cauHinhLai">Cần chỉnh lại cấu hình</param>
        /// <param name="loadLanDau">Load dữ liệu ban đầu</param>
        private void ConfigGrid(List<OpAccountDetailByOrther> lstAccountDetailByOrthers, bool cauHinhLai = false, bool loadLanDau = true)//bool oPenCurrency, bool khongChonDong = true, string accountNumber = null
        {
            uGrid.SetDataBinding(lstAccountDetailByOrthers, "");
            if (!loadLanDau)
            {
                //OPNUntil.AddAllConboToGrid(uGrid);
                foreach (UltraGridColumn column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    Utils.ConfigEachColumn4Grid<OpAccountDetailByOrther>(this, 10000, column, uGrid);
                }
                if ((string)uCbbCurrency.Value == "VND")
                {
                    Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOriginal"], ConstDatabase.Format_TienVND);
                    Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOriginal"], ConstDatabase.Format_TienVND);
                    Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"], ConstDatabase.Format_TienVND);
                    Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"], ConstDatabase.Format_TienVND);

                }
                else
                {
                    Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOriginal"], ConstDatabase.Format_ForeignCurrency);
                    Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOriginal"], ConstDatabase.Format_ForeignCurrency);
                    Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"], ConstDatabase.Format_ForeignCurrency);
                    Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"], ConstDatabase.Format_ForeignCurrency);

                }
            }
            if (cauHinhLai)
            {
                Utils.ConfigGrid(uGrid, ConstDatabase.OPAccountDetail_TableName, false);
                if (uGrid.DisplayLayout.Bands[0].Summaries.Count != 0) uGrid.DisplayLayout.Bands[0].Summaries.Clear();

                Utils.AddSumColumn(uGrid, "DebitAmount", false, "", ConstDatabase.Format_TienVND);
                Utils.AddSumColumn(uGrid, "CreditAmount", false, "", ConstDatabase.Format_TienVND);
                if ((string)uCbbCurrency.Value == "VND")
                {
                    Utils.AddSumColumn(uGrid, "DebitAmountOriginal", false, "", ConstDatabase.Format_TienVND);
                    Utils.AddSumColumn(uGrid, "CreditAmountOriginal", false, "", ConstDatabase.Format_TienVND);
                }
                else
                {
                    Utils.AddSumColumn(uGrid, "DebitAmountOriginal", false, "", ConstDatabase.Format_ForeignCurrency);
                    Utils.AddSumColumn(uGrid, "CreditAmountOriginal", false, "", ConstDatabase.Format_ForeignCurrency);
                }


                Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["ExchangeRate"], ConstDatabase.Format_Rate);
                Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"], ConstDatabase.Format_TienVND);
                Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"], ConstDatabase.Format_TienVND);
                if ((string)uCbbCurrency.Value == "VND")
                {
                    Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOriginal"], ConstDatabase.Format_TienVND);
                    Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOriginal"], ConstDatabase.Format_TienVND);
                    uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOriginal"].Header.Caption = "Dư Nợ";
                    uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOriginal"].Header.Caption = "Dư Có";
                }
                else
                {
                    Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOriginal"], ConstDatabase.Format_ForeignCurrency);
                    Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOriginal"], ConstDatabase.Format_ForeignCurrency);
                    uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOriginal"].Header.Caption = "Dư Nợ Nguyên tệ";
                    uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOriginal"].Header.Caption = "Dư Có Nguyên tệ";
                }
                uGrid.DisplayLayout.Bands[0].Columns["AccountName"].Header.Fixed = true;
                uGrid.DisplayLayout.Bands[0].Columns["AccountName"].CellAppearance.TextHAlign = HAlign.Left;
                uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].Header.Fixed = true;

            }
        }
        private void AfterExitEditMode()
        {
            try
            {
                if (uGrid.ActiveCell == null) return;
                UltraGridCell cell = uGrid.ActiveCell;
                if (uGrid.ActiveCell != null)
                {
                    OpAccountDetailByOrther opAccountDetailByOrther =
                        (OpAccountDetailByOrther)uGrid.ActiveCell.Row.ListObject;
                    List<string> lstDetailType = opAccountDetailByOrther.DetailType.Split(';').ToList();
                    if (cell != null)
                    {
                        switch (cell.Column.Key)
                        {
                            case "ExchangeRate":
                                QuyDoi(cell, true);
                                break;
                            case "DebitAmount":
                            case "CreditAmount":
                                if (((OpAccountDetailByOrther)cell.Row.ListObject).AccountNumber == "007") return;
                                QuyDoi(cell, false);
                                break;
                            case "DebitAmountOriginal":
                            case "CreditAmountOriginal":
                                QuyDoi(cell, true);
                                if ((opAccountDetailByOrther.CreditAmountOriginal > 0 || opAccountDetailByOrther.DebitAmountOriginal > 0))
                                {
                                    CheckCellDetailType(uGrid, lstDetailType, opAccountDetailByOrther, false);
                                }
                                break;
                        }
                        if (_lstCacDoiTuongLienKetCanNhap.Any(c => c == cell.Column.Key) && CheckDetailType(lstDetailType, opAccountDetailByOrther, false))
                        {
                            Utils.RemoveNotificationRow(uGrid, uGrid.ActiveCell.Row);
                        }
                    }
                }
            }
            catch (Exceptions)
            {

            }
        }
        private void ConfigRowAllGrid(UltraGrid ultraGrid, bool khongPhaiTienVND, List<string> dsActivate = null)
        {
            List<string> keyDebit = new List<string> { "DebitAmount", "DebitAmountOriginal" };
            List<string> keyCredit = new List<string> { "CreditAmount", "CreditAmountOriginal" };
            ultraGrid.DisplayLayout.AutoFitStyle = khongPhaiTienVND ? AutoFitStyle.ResizeAllColumns : AutoFitStyle.ExtendLastColumn;
            foreach (UltraGridRow row in uGrid.Rows)
            {
                row.Appearance.BackColor = Color.White;
                row.Cells["ExchangeRate"].Activation = Activation.AllowEdit;
                if (((OpAccountDetailByOrther)row.ListObject).AccountGroupKind == 0)
                {

                    OPNUntil.AllowEditRowGrid(row, keyDebit);
                    OPNUntil.NoEditRowGrid(row, keyCredit);
                }
                else if (((OpAccountDetailByOrther)row.ListObject).AccountGroupKind == 1)
                {
                    OPNUntil.AllowEditRowGrid(row, keyCredit);
                    OPNUntil.NoEditRowGrid(row, keyDebit);
                }
                OPNUntil.NoEditRowGrid(row, new List<string> { "AccountName" });
                OPNUntil.NoEditRowGrid(row, new List<string> { "AccountNumber" });
                if (dsActivate != null)
                {
                    foreach (string a in dsActivate)
                    {
                        if (((OpAccountDetailByOrther)row.ListObject).AccountNumber == a)
                        {
                            uGrid.Rows[row.Index].Activate();
                            uGrid.Rows[row.Index].Selected = true;
                        }
                    }
                }
                if (((OpAccountDetailByOrther)row.ListObject).AccountNumber == "007")
                {
                    OPNUntil.NoEditRowGrid(row, new List<string> { "DebitAmount" });
                    OPNUntil.NoEditRowGrid(row, new List<string> { "CreditAmount" });
                }
                if (_lstParents.Contains(((OpAccountDetailByOrther)row.ListObject).AccountNumber))
                {
                    OPNUntil.NoEditRowGrid(row, new List<string> { "DebitAmount" });
                    OPNUntil.NoEditRowGrid(row, new List<string> { "CreditAmount" });
                    OPNUntil.NoEditRowGrid(row, new List<string> { "DebitAmountOriginal" });
                    OPNUntil.NoEditRowGrid(row, new List<string> { "CreditAmountOriginal" });
                }
                List<string> lstQuyDoi = new List<string> { "ExchangeRate", "DebitAmount", "CreditAmount" };
                OPNUntil.HiddenColumnGrid(uGrid, lstQuyDoi, khongPhaiTienVND);
                OPNUntil.HiddenColumnGrid(uGrid, new List<string> { "CurrencyID" }, true);
            }
        }
        private void ErrorGrid()
        {
            if (_rowDuocThem != null)
            {
                foreach (UltraGridRow row in uGrid.Rows)
                {
                    foreach (Guid cacRowDaThem in _rowDuocThem)
                    {
                        try
                        {
                            OpAccountDetailByOrther op = _lstAccounts.Single(c => c.ID == cacRowDaThem);
                            if (!CheckError(op) && ((OpAccountDetailByOrther)row.ListObject).ID == op.ID)
                            {
                                Utils.NotificationRow(uGrid, row, string.Format(resSystem.OPN03, op.AccountNumber, row.Index));
                                break;
                            }
                            if (CheckError(op) && ((OpAccountDetailByOrther)row.ListObject).ID == op.ID)
                            {
                                Utils.RemoveNotificationRow(uGrid, row);
                                break;
                            }
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
        }
        bool CheckError(OpAccountDetailByOrther op)
        {
            if (_lstAccounts.Any(opAccountDetailByOrther => opAccountDetailByOrther.ID != op.ID
                                                            && opAccountDetailByOrther.AccountNumber == op.AccountNumber
                                                            && (opAccountDetailByOrther.ExpenseItemID != op.ExpenseItemID
                                                                || opAccountDetailByOrther.ContractID != op.ContractID
                                                                || opAccountDetailByOrther.CostSetID != op.CostSetID
                                                                || opAccountDetailByOrther.BankAccountDetailID != op.BankAccountDetailID

                                                                )))
            {
                return true;
            }
            return false;
        }
        private List<OpAccountDetailByOrther> Compare(IEnumerable<OpAccountDetailByOrther> listThayDoi1, IEnumerable<OpAccountDetailByOrther> listBanDau1)
        {
            IEnumerable<OpAccountDetailByOrther> listThayDoi = Utils.CloneObject(listThayDoi1);
            IEnumerable<OpAccountDetailByOrther> listBanDau = Utils.CloneObject(listBanDau1);

            List<OpAccountDetailByOrther> lstKq = new List<OpAccountDetailByOrther>();
            //lọc bỏ các trường tiền bằng 0
            List<OpAccountDetailByOrther> listThayDoiLocBo = listThayDoi/*.Where(c => c.CreditAmountOriginal != 0 || c.CreditAmount != 0 || c.DebitAmountOriginal != 0 || c.DebitAmount != 0)*/.OrderBy(c => c.ID).ToList();
            //lọc bỏ các trường tiền bằng 0 và giá trị ID = guid.empty(trường tiền ghép với bảng account)
            List<OpAccountDetailByOrther> listBanDauLocBo = listBanDau.Where(c => (c.CreditAmountOriginal != 0 || c.CreditAmount != 0 || c.DebitAmountOriginal != 0 || c.DebitAmount != 0) && c.ID != Guid.Empty).OrderBy(c => c.ID).ToList();
            foreach (OpAccountDetailByOrther opAccountDetailByOrther in listThayDoiLocBo)
            {
                if (opAccountDetailByOrther.ID == Guid.Empty)//trường hợp có số tiền nhưng chưa tạo guid id (do ghép với bảng account lên không có guid id)
                {
                    opAccountDetailByOrther.ID = Guid.NewGuid();
                    lstKq.Add(opAccountDetailByOrther);
                }
                else if (listBanDauLocBo.Any(c => c.ID == opAccountDetailByOrther.ID))//trường hợp có tồn tại ID trong opaccount nhưng các giá trị khác đã bị thay đổi
                {
                    OpAccountDetailByOrther bandau = listBanDauLocBo.FirstOrDefault(c => c.ID == opAccountDetailByOrther.ID);
                    PropertyInfo[] propertyInfo = typeof(OpAccountDetailByOrther).GetProperties();
                    foreach (PropertyInfo info in propertyInfo)
                    {
                        if (bandau != null)
                        {
                            PropertyInfo propItemBanDau = bandau.GetType().GetProperty(info.Name);
                            PropertyInfo propItemThayDoi = opAccountDetailByOrther.GetType().GetProperty(info.Name);
                            if (propItemBanDau != null && propItemBanDau.CanWrite && propItemThayDoi != null && propItemThayDoi.CanWrite && propItemBanDau.GetValue(bandau, null) != propItemThayDoi.GetValue(opAccountDetailByOrther, null))
                            {
                                lstKq.Add(opAccountDetailByOrther);
                                break;
                            }
                        }
                    }
                }
                else if (listBanDauLocBo.All(c => c.ID != opAccountDetailByOrther.ID))//trường hợp giá trị đó được tạo mới bằng nút thêm
                {
                    lstKq.Add(opAccountDetailByOrther);
                }
            }
            //foreach (OpAccountDetailByOrther opAccountDetailByOrther in listBanDauLocBo)
            //{
            //    //trường hợp một id bên list ban đầu có (id này khác guid empty) mà trong list thay đổi không có và có cùng loại tiền
            //    if (listThayDoiLocBo.Count > 0 && listThayDoiLocBo.All(c => c.ID != Guid.Empty && opAccountDetailByOrther.ID != c.ID && c.CurrencyID == opAccountDetailByOrther.CurrencyID))
            //    {
            //        opAccountDetailByOrther.CreditAmount = 0;
            //        opAccountDetailByOrther.CreditAmountOriginal = 0;
            //        opAccountDetailByOrther.DebitAmount = 0;
            //        opAccountDetailByOrther.DebitAmountOriginal = 0;
            //        lstKq.Add(opAccountDetailByOrther);
            //    }
            //    //else
            //    //{
            //    //    lstKq.Add(opAccountDetailByOrther);
            //    //}
            //}
            foreach (OpAccountDetailByOrther opAccountDetailByOrther in lstKq)
            {
                opAccountDetailByOrther.PostedDate = OPNUntil.ApplicationStartDateOPN;
            }
            return lstKq;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="loai">nếu nhân để ra tiền VND = true, nếu chia ra ngoại tệ = flase</param>
        private void QuyDoi(UltraGridCell cell, bool loai)
        {
            try
            {
                decimal exchangeRate = (decimal)cell.Row.Cells["ExchangeRate"].Value;
                decimal debitAmountOriginal = (decimal)cell.Row.Cells["DebitAmountOriginal"].Value;
                decimal debitAmount = (decimal)cell.Row.Cells["DebitAmount"].Value;
                decimal creditAmount = (decimal)cell.Row.Cells["CreditAmount"].Value;
                decimal creditAmountOriginal = (decimal)cell.Row.Cells["CreditAmountOriginal"].Value;
                if (loai) //nhân
                {
                    cell.Row.Cells["DebitAmount"].Value = Math.Round(exchangeRate * debitAmountOriginal, LamTron, MidpointRounding.AwayFromZero);
                    cell.Row.Cells["CreditAmount"].Value = Math.Round(exchangeRate * creditAmountOriginal, LamTron, MidpointRounding.AwayFromZero);
                }
                else
                {
                    cell.Row.Cells["DebitAmountOriginal"].Value = Math.Round(debitAmount / exchangeRate, exchangeRate == 1 ? LamTron : LamTron1, MidpointRounding.AwayFromZero);
                    cell.Row.Cells["CreditAmountOriginal"].Value = Math.Round(creditAmount / exchangeRate, exchangeRate == 1 ? LamTron : LamTron1, MidpointRounding.AwayFromZero);
                }
            }
            catch (Exception)
            {

            }

        }
        private void SelectedCombo(IEnumerable<OpAccountDetailByOrther> opAccounts)
        {
            var opAccountDetailByOrther = opAccounts.FirstOrDefault();
            if (opAccountDetailByOrther != null)
            {
                string s = opAccountDetailByOrther.CurrencyID;
                foreach (UltraGridRow item in uCbbCurrency.Rows)
                {
                    if (((Currency)item.ListObject).ID.Equals(s))
                    {
                        uCbbCurrency.SelectedRow = item;
                        break;
                    }
                }
            }

        }
        private bool CheckDetailType(List<string> lstDetailType, OpAccountDetailByOrther opAccountDetailByOrther, bool activeMSG = true)
        {
            if (opAccountDetailByOrther.CreditAmountOriginal != 0 || opAccountDetailByOrther.CreditAmount != 0 || opAccountDetailByOrther.DebitAmountOriginal != 0 || opAccountDetailByOrther.DebitAmount != 0)
            {
                if (lstDetailType.Any(c => c.Equals("3")) && opAccountDetailByOrther.CostSetID == null)
                {
                    if (activeMSG) MSG.Error(string.Format(
                          "Tài khoản {0} chi tiết theo đối tượng tập hợp chi phí. Nên vui lòng nhập đối tượng tập hợp chi phí cho tài khoản {0}",
                          opAccountDetailByOrther.AccountNumber));
                    return false;
                }
                if (lstDetailType.Any(c => c.Equals("4")) && opAccountDetailByOrther.ContractID == null)
                {
                    if (activeMSG) MSG.Error(
                        string.Format(
                            "Tài khoản {0} chi tiết theo hợp đồng. Nên vui lòng nhập thêm hợp đồng cho tài khoản {0}",
                            opAccountDetailByOrther.AccountNumber));
                    return false;
                }
                if (lstDetailType.Any(c => c.Equals("6")) && opAccountDetailByOrther.BankAccountDetailID == null)
                {
                    if (activeMSG) MSG.Error(
                       string.Format(
                           "Tài khoản {0} chi tiết theo tài khoản ngân hàng. Nên vui lòng nhập tài khoản ngân hàng cho tài khoản {0}",
                           opAccountDetailByOrther.AccountNumber));
                    return false;
                }
                if (lstDetailType.Any(c => c.Equals("7")) && opAccountDetailByOrther.ExpenseItemID == null)
                {
                    if (activeMSG) MSG.Error(
                       string.Format(
                           "Tài khoản {0} chi tiết theo khoản mục chi phí. Nên vui lòng nhập chi phí cho tài khoản {0}",
                           opAccountDetailByOrther.AccountNumber));
                    return false;
                }
            }

            return true;
        }
        /// <summary>
        /// Kiểm tra lỗi dữ liệu nhập vào
        /// </summary>
        /// /// <param name="ugrid"></param>
        /// <param name="lstDetailType"></param>
        /// <param name="opAccountDetailByOrther"></param>
        /// <param name="activeMSG"></param>
        /// <returns></returns>
        private bool CheckCellDetailType(UltraGrid ugrid, List<string> lstDetailType, OpAccountDetailByOrther opAccountDetailByOrther, bool activeMSG = true)
        {
            bool result = true;
            string notiMsg = "Nhập thiếu thông tin";
            string errorMsg = "";
            if (lstDetailType.Any(c => c.Equals("3")) && opAccountDetailByOrther.CostSetID == null)
            {
                errorMsg += string.Format("Tài khoản {0} chi tiết theo đối tượng tập hợp chi phí. Nên vui lòng nhập đối tượng tập hợp chi phí cho tài khoản {0}\r\n", opAccountDetailByOrther.AccountNumber);
                Utils.NotificationCell(ugrid, ugrid.ActiveCell.Row.Cells["CostSetID"], notiMsg);
                result = false;
            }
            if (lstDetailType.Any(c => c.Equals("4")) && opAccountDetailByOrther.ContractID == null)
            {

                errorMsg += string.Format("Tài khoản {0} chi tiết theo hợp đồng. Nên vui lòng nhập thêm hợp đồng cho tài khoản {0}\r\n", opAccountDetailByOrther.AccountNumber);
                Utils.NotificationCell(ugrid, ugrid.ActiveCell.Row.Cells["ContractID"], notiMsg);
                result = false;
            }
            if (lstDetailType.Any(c => c.Equals("6")) && opAccountDetailByOrther.BankAccountDetailID == null)
            {
                errorMsg += string.Format("Tài khoản {0} chi tiết theo tài khoản ngân hàng. Nên vui lòng nhập tài khoản ngân hàng cho tài khoản {0}", opAccountDetailByOrther.AccountNumber);
                Utils.NotificationCell(ugrid, ugrid.ActiveCell.Row.Cells["BankAccountDetailID"], notiMsg);
                result = false;
            }
            if (lstDetailType.Any(c => c.Equals("7")) && opAccountDetailByOrther.ExpenseItemID == null)
            {
                errorMsg += string.Format("Tài khoản {0} chi tiết theo khoản mục chi phí. Nên vui lòng nhập chi phí cho tài khoản {0}", opAccountDetailByOrther.AccountNumber);
                Utils.NotificationCell(ugrid, ugrid.ActiveCell.Row.Cells["ExpenseItemID"], notiMsg);
                result = false;
            }
            if (activeMSG) MSG.Error(errorMsg);
            return result;
        }
        private bool CheckErrorIMGGrid(UltraGrid ultraGrid)
        {
            foreach (UltraGridRow ultraGridRow in ultraGrid.Rows)
            {
                if (ultraGridRow.Appearance.Image != null) return false;
                foreach (UltraGridCell ultraGridCell in ultraGridRow.Cells)
                {
                    if (ultraGridCell.Appearance.Image != null) return false;
                }
            }
            return true;
        }




        #endregion

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            uGrid.DisplayLayout.Bands[0].Columns["ExchangeRate"].Hidden = false;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOriginal"].Hidden = false;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOriginal"].Hidden = false;
            uGrid.DisplayLayout.Bands[0].Columns["CurrencyID"].Hidden = false;
            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].Hidden = false;
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu cơ sở dữ liệu",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "So_du_dau_ky_theo_tai_khoan"
            };
            if (save.ShowDialog(this) == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    this.ultraGridExcelExporter1.Export(uGrid, duongdan);
                    MSG.Information("Xuất ra file Excel  thành công !");
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
            if (uCbbCurrency.SelectedRow.ListObject != null && uCbbCurrency.SelectedRow.Index >= 0)
            {
                Currency currency = (Currency)uCbbCurrency.SelectedRow.ListObject;
                if (_lstAccounts.Count > 0)
                {
                    foreach (string key in _dictionary.Keys)
                    {
                        var opAccountDetailByOrther = _lstAccounts.FirstOrDefault();
                        if (opAccountDetailByOrther != null && key == opAccountDetailByOrther.CurrencyID)
                        {
                            _dictionary[key] = _lstAccounts;
                            break;
                        }
                    }
                }
                _lstAccounts = _dictionary.Single(c => c.Key == currency.ID).Value;
                ConfigGrid(_lstAccounts, true);
                ConfigRowAllGrid(uGrid, currency != null && currency.ID == "VND", _lstCanCheck);
            }
        }

        private void FOPAccountDetail_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void uGrid_KeyDown(object sender, KeyEventArgs e)
        {
            UltraGrid uGrid = (UltraGrid)sender;
            if (uGrid.ActiveCell != null)
            {
                if (uGrid.ActiveCell.IsInEditMode)
                {
                    if (e.KeyCode == Keys.Up)
                    {
                        if (uGrid.ActiveCell.IsInEditMode && (uGrid.ActiveCell.ValueListResolved != null || uGrid.ActiveCell.EditorComponentResolved != null))
                        {
                            if (!uGrid.ActiveCell.DroppedDown) uGrid.ActiveCell.DroppedDown = true;
                        }
                        else if (uGrid.ActiveCell.ValueListResolved == null)
                        {
                            uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                            uGrid.PerformAction(UltraGridAction.AboveCell, false, false);
                            e.Handled = true;
                            uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                        }
                    }
                    if (e.KeyCode == Keys.Down)
                    {
                        if (uGrid.ActiveCell.IsInEditMode && (uGrid.ActiveCell.ValueListResolved != null || uGrid.ActiveCell.EditorComponentResolved != null))
                        {
                            if (!uGrid.ActiveCell.DroppedDown) uGrid.ActiveCell.DroppedDown = true;
                        }
                        else if (uGrid.ActiveCell.ValueListResolved == null)
                        {
                            uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                            uGrid.PerformAction(UltraGridAction.BelowCell, false, false);
                            e.Handled = true;
                            uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                        }
                    }
                    if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Right)
                    {
                        uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                        uGrid.PerformAction(UltraGridAction.NextCellByTab, false, false);
                        e.Handled = true;
                        uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                    }
                    if (e.KeyCode == Keys.Left)
                    {
                        uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                        uGrid.PerformAction(UltraGridAction.PrevCellByTab, false, false);
                        e.Handled = true;
                        uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                    }
                }
                else
                {
                    switch (e.KeyCode)
                    {
                        case Keys.Enter:
                            uGrid.ActiveCell.Selected = true;
                            uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                            break;
                        case Keys.Down:
                            if (uGrid.ActiveCell.IsInEditMode && (uGrid.ActiveCell.ValueListResolved != null || uGrid.ActiveCell.EditorComponentResolved != null))
                            {
                                if (!uGrid.ActiveCell.DroppedDown) uGrid.ActiveCell.DroppedDown = true;
                            }
                            else if (uGrid.ActiveCell.ValueListResolved == null)
                            {
                                uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                                uGrid.PerformAction(UltraGridAction.BelowCell, false, false);
                                e.Handled = true;
                                uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                            }
                            break;
                    }
                }
            }
            if (uGrid.ActiveCell != null && uGrid.ActiveCell.Column.ValueList != null && uGrid.ActiveCell.Column.AutoCompleteMode == Infragistics.Win.AutoCompleteMode.None)
            {
                if (((e.KeyCode >= Keys.A && e.KeyCode <= Keys.Z)
                     || (e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9)
                     || (e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.Divide)) || e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    var combo = (UltraCombo)uGrid.ActiveCell.Column.ValueList;
                    if (!uGrid.ActiveCell.DroppedDown) uGrid.PerformAction(UltraGridAction.ToggleDropdown);
                }
            }
        }
    }
}