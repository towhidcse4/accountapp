﻿namespace Accounting
{
    partial class FOPAccountObject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.gb_SoDuDauKy = new Infragistics.Win.Misc.UltraGroupBox();
            this.uCbbCurrency = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.linkFileMau = new System.Windows.Forms.LinkLabel();
            this.btnImportExcel = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ThemDongMoiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.XoaDongToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.bt_ThemDongMoi = new Infragistics.Win.Misc.UltraButton();
            this.bt_dong = new Infragistics.Win.Misc.UltraButton();
            this.bt_luu = new Infragistics.Win.Misc.UltraButton();
            this.bt_xoa = new Infragistics.Win.Misc.UltraButton();
            this.ultraGridExcelExporter1 = new Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gb_SoDuDauKy)).BeginInit();
            this.gb_SoDuDauKy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCbbCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(6, 29);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(56, 23);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Loại tiền";
            // 
            // gb_SoDuDauKy
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.BackColor2 = System.Drawing.Color.Transparent;
            this.gb_SoDuDauKy.Appearance = appearance1;
            this.gb_SoDuDauKy.Controls.Add(this.uCbbCurrency);
            this.gb_SoDuDauKy.Controls.Add(this.ultraLabel1);
            this.gb_SoDuDauKy.Dock = System.Windows.Forms.DockStyle.Top;
            appearance2.FontData.BoldAsString = "True";
            appearance2.FontData.SizeInPoints = 13F;
            this.gb_SoDuDauKy.HeaderAppearance = appearance2;
            this.gb_SoDuDauKy.Location = new System.Drawing.Point(0, 0);
            this.gb_SoDuDauKy.Name = "gb_SoDuDauKy";
            this.gb_SoDuDauKy.Size = new System.Drawing.Size(815, 56);
            this.gb_SoDuDauKy.TabIndex = 47;
            this.gb_SoDuDauKy.Text = "!";
            this.gb_SoDuDauKy.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // uCbbCurrency
            // 
            this.uCbbCurrency.Location = new System.Drawing.Point(63, 25);
            this.uCbbCurrency.Name = "uCbbCurrency";
            this.uCbbCurrency.Size = new System.Drawing.Size(233, 22);
            this.uCbbCurrency.TabIndex = 1;
            // 
            // linkFileMau
            // 
            this.linkFileMau.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linkFileMau.AutoSize = true;
            this.linkFileMau.Location = new System.Drawing.Point(181, 11);
            this.linkFileMau.Name = "linkFileMau";
            this.linkFileMau.Size = new System.Drawing.Size(46, 13);
            this.linkFileMau.TabIndex = 10;
            this.linkFileMau.TabStop = true;
            this.linkFileMau.Text = "File mẫu";
            this.linkFileMau.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkFileMau_LinkClicked);
            // 
            // btnImportExcel
            // 
            this.btnImportExcel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance3.Image = global::Accounting.Properties.Resources.Excel;
            this.btnImportExcel.Appearance = appearance3;
            this.btnImportExcel.Location = new System.Drawing.Point(233, 7);
            this.btnImportExcel.Name = "btnImportExcel";
            this.btnImportExcel.Size = new System.Drawing.Size(115, 23);
            this.btnImportExcel.TabIndex = 3;
            this.btnImportExcel.Text = "Nhập từ Excel";
            this.btnImportExcel.Click += new System.EventHandler(this.btnImportExcel_Click);
            // 
            // ultraGroupBox1
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox1.Appearance = appearance4;
            this.ultraGroupBox1.Controls.Add(this.uGrid);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance5.FontData.BoldAsString = "True";
            appearance5.FontData.SizeInPoints = 13F;
            this.ultraGroupBox1.HeaderAppearance = appearance5;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 56);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(815, 298);
            this.ultraGroupBox1.TabIndex = 48;
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // uGrid
            // 
            this.uGrid.ContextMenuStrip = this.contextMenuStrip1;
            this.uGrid.DisplayLayout.RowSelectorImages.ActiveAndAddNewRowImage = global::Accounting.Properties.Resources.new_file_icon;
            this.uGrid.DisplayLayout.RowSelectorImages.ActiveAndDataChangedImage = global::Accounting.Properties.Resources.ubtnTemplate;
            this.uGrid.DisplayLayout.RowSelectorImages.ActiveRowImage = global::Accounting.Properties.Resources.ubtnForward;
            this.uGrid.DisplayLayout.RowSelectorImages.AddNewRowImage = global::Accounting.Properties.Resources.iAdd;
            this.uGrid.DisplayLayout.RowSelectorImages.DataChangedImage = global::Accounting.Properties.Resources.edit_validated_icon;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(3, 0);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(809, 295);
            this.uGrid.TabIndex = 0;
            this.uGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGrid_KeyDown);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ThemDongMoiToolStripMenuItem,
            this.XoaDongToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(203, 48);
            // 
            // ThemDongMoiToolStripMenuItem
            // 
            this.ThemDongMoiToolStripMenuItem.Image = global::Accounting.Properties.Resources._1437136799_add;
            this.ThemDongMoiToolStripMenuItem.Name = "ThemDongMoiToolStripMenuItem";
            this.ThemDongMoiToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.ThemDongMoiToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.ThemDongMoiToolStripMenuItem.Text = "Thêm dòng mới";
            // 
            // XoaDongToolStripMenuItem
            // 
            this.XoaDongToolStripMenuItem.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.XoaDongToolStripMenuItem.Name = "XoaDongToolStripMenuItem";
            this.XoaDongToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.XoaDongToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.XoaDongToolStripMenuItem.Text = "Xóa dòng";
            // 
            // ultraGroupBox2
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox2.Appearance = appearance6;
            this.ultraGroupBox2.Controls.Add(this.linkFileMau);
            this.ultraGroupBox2.Controls.Add(this.ultraButton1);
            this.ultraGroupBox2.Controls.Add(this.btnImportExcel);
            this.ultraGroupBox2.Controls.Add(this.bt_ThemDongMoi);
            this.ultraGroupBox2.Controls.Add(this.bt_dong);
            this.ultraGroupBox2.Controls.Add(this.bt_luu);
            this.ultraGroupBox2.Controls.Add(this.bt_xoa);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            appearance12.FontData.BoldAsString = "True";
            appearance12.FontData.SizeInPoints = 13F;
            this.ultraGroupBox2.HeaderAppearance = appearance12;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 354);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(815, 36);
            this.ultraGroupBox2.TabIndex = 49;
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraButton1
            // 
            this.ultraButton1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.Image = global::Accounting.Properties.Resources.Excel;
            this.ultraButton1.Appearance = appearance7;
            this.ultraButton1.Location = new System.Drawing.Point(352, 7);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(99, 23);
            this.ultraButton1.TabIndex = 4;
            this.ultraButton1.Text = "Xuất Excel";
            this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
            // 
            // bt_ThemDongMoi
            // 
            this.bt_ThemDongMoi.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.Image = global::Accounting.Properties.Resources._1437136799_add;
            this.bt_ThemDongMoi.Appearance = appearance8;
            this.bt_ThemDongMoi.Location = new System.Drawing.Point(457, 6);
            this.bt_ThemDongMoi.Name = "bt_ThemDongMoi";
            this.bt_ThemDongMoi.Size = new System.Drawing.Size(92, 23);
            this.bt_ThemDongMoi.TabIndex = 3;
            this.bt_ThemDongMoi.Text = "Thêm dòng";
            // 
            // bt_dong
            // 
            this.bt_dong.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.bt_dong.Appearance = appearance9;
            this.bt_dong.Location = new System.Drawing.Point(734, 6);
            this.bt_dong.Name = "bt_dong";
            this.bt_dong.Size = new System.Drawing.Size(75, 23);
            this.bt_dong.TabIndex = 2;
            this.bt_dong.Text = "Đóng";
            // 
            // bt_luu
            // 
            this.bt_luu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance10.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.bt_luu.Appearance = appearance10;
            this.bt_luu.Location = new System.Drawing.Point(653, 6);
            this.bt_luu.Name = "bt_luu";
            this.bt_luu.Size = new System.Drawing.Size(75, 23);
            this.bt_luu.TabIndex = 1;
            this.bt_luu.Text = "Lưu";
            // 
            // bt_xoa
            // 
            this.bt_xoa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance11.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.bt_xoa.Appearance = appearance11;
            this.bt_xoa.Location = new System.Drawing.Point(555, 6);
            this.bt_xoa.Name = "bt_xoa";
            this.bt_xoa.Size = new System.Drawing.Size(92, 23);
            this.bt_xoa.TabIndex = 0;
            this.bt_xoa.Text = "Xóa dòng";
            // 
            // FOPAccountObject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 390);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.gb_SoDuDauKy);
            this.Controls.Add(this.ultraGroupBox2);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FOPAccountObject";
            this.Text = "FOPAccountObject";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FOPAccountObject_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FOPAccountObject_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.gb_SoDuDauKy)).EndInit();
            this.gb_SoDuDauKy.ResumeLayout(false);
            this.gb_SoDuDauKy.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCbbCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraGroupBox gb_SoDuDauKy;
        private Infragistics.Win.UltraWinGrid.UltraCombo uCbbCurrency;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ThemDongMoiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem XoaDongToolStripMenuItem;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraButton bt_ThemDongMoi;
        private Infragistics.Win.Misc.UltraButton bt_dong;
        private Infragistics.Win.Misc.UltraButton bt_luu;
        private Infragistics.Win.Misc.UltraButton bt_xoa;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter ultraGridExcelExporter1;
        private Infragistics.Win.Misc.UltraButton btnImportExcel;
        private System.Windows.Forms.LinkLabel linkFileMau;
    }
}