﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Infragistics.Win.Misc;

namespace Accounting.FBusiness.FOPAccount
{
    public partial class FConvertObject : CustormForm
    {
        public FConvertObject()
        {
            InitializeComponent();
            ultraTextEditor1.Text = "CollectionCustomer";
        }

        static string _strColumnNameWithIndex;
        static string _strColumnName = string.Empty;
        private static string _nameTable = "";
        private void ultraButton1_Click(object sender, EventArgs e)
        {
            _nameTable = ultraTextEditor1.Text;
            if (System.Type.GetType(string.Format("Accounting.Core.Domain.{0}", _nameTable)) == null)
            {
                GetStrProObj(Activator.CreateInstance(Core.Utils.getType(string.Format("Accounting.Core.Domain.{0}", _nameTable))));
            }
            List<string> lst = _strColumnName.Split(',').ToList();
            int i = 0;
            _lstTemplateColumn = lst.Select(s1 => new TemplateColumn { ColumnName = s1, VisiblePosition = ++i }).ToList();
            ultraGrid1.SetDataBinding(_lstTemplateColumn, "");
            ultraGrid1.DisplayLayout.Bands[0].Columns["ID"].Hidden = true;
            ultraGrid1.DisplayLayout.Bands[0].Columns["TemplateDetailID"].Hidden = true;
            ultraGrid1.DisplayLayout.Bands[0].Columns["ColumnWidth"].Hidden = true;
            ultraGrid1.DisplayLayout.Bands[0].Columns["ColumnMaxWidth"].Hidden = true;
            ultraGrid1.DisplayLayout.Bands[0].Columns["ColumnMinWidth"].Hidden = true;
            ultraGrid1.DisplayLayout.Bands[0].Columns["ColumnToolTip"].Hidden = true;
            //ultraGrid1.DisplayLayout.Bands[0].Columns[""].Hidden = true;
        }
        List<TemplateColumn> _lstTemplateColumn = new List<TemplateColumn>();
        static string _vTbolIsVisibleString = "";
        static string _bolIsReadOnlyString = "";
        private static string _bolIsVisibleString = "";
        private static string _intVisiblePositionString = "";
        private static string _strColumnCaption = "";
        private void ultraButton2_Click(object sender, EventArgs e)
        {
            _vTbolIsVisibleString = "";
            _bolIsReadOnlyString = "";
            _bolIsVisibleString = "";
            _intVisiblePositionString = "";
            _strColumnCaption = "";
            for (int i = 0; i < _lstTemplateColumn.Count; i++)
            {
                
                if (_lstTemplateColumn[i].IsReadOnly) _bolIsReadOnlyString += "true" + ",";
                else _bolIsReadOnlyString += "false" + ",";


                if (_lstTemplateColumn[i].IsVisible) _bolIsVisibleString += "true" + ",";
                else _bolIsVisibleString += "false" + ",";

                _intVisiblePositionString += (i + 1) + ",";

                if (_lstTemplateColumn[i].IsVisible)
                {
                    _vTbolIsVisibleString += (i + 1) + ",";
                }
                if (string.IsNullOrEmpty(_lstTemplateColumn[i].ColumnCaption))
                    _strColumnCaption += _lstTemplateColumn[i].ColumnName + ",";
                else
                {
                    _strColumnCaption += string.Format("\"{0}\", ", _lstTemplateColumn[i].ColumnCaption);
                }
            }
            richTextBox1.Text = CreateStrConstDatabase();
            Clipboard.SetText(richTextBox1.Text);
            richTextBox1.SelectAll();
        }
        static string CreateStrConstDatabase()
        {
            string s = "";
            const string patternStrBak = "#region |0|\r\n            " +
                                          "nameTable = |0|_TableName;\r\n            " +
                                          "strColumnName = new List<string> { {1} };\r\n " +
                                          "//{3} \r\n" +
                                          "strColumnCaption = strColumnToolTip = new List<string> { {8} };\r\n            " +
                                          "bolIsReadOnly = new List<bool>(){ {5} };\r\n            " +
                                          "bolIsVisible = new List<bool>(){ {6} };\r\n            " +
                                          "bolIsVisibleCbb = new List<bool>();\r\n            " +
                                          "intColumnWidth = new List<int>();\r\n            " +
                                          "intColumnMaxWidth = new List<int>();\r\n            " +
                                          "intColumnMinWidth = new List<int>();\r\n            " +
                                          "intVisiblePosition = new List<int>(){ {7} };\r\n            " +
                                          "VTbolIsVisible = new List<int> { {4} };    //vị trí có giá trị bằng true\r\n            " +
                                          "VTbolIsVisibleCbb = new List<int> { 1, 2 };\r\n            " +
                                          "for (int i = 0; i < strColumnName.Count; i++)\r\n            " +
                                          "{\r\n               " +
                                          "bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));\r\n" +
                                          "intColumnWidth.Add(-1);\r\n                " +
                                          "intColumnMaxWidth.Add(-1);\r\n                " +
                                          "intColumnMinWidth.Add(-1);\r\n                " +
                                          "}\r\n           " +
                                          "dicTemp.Add(nameTable, CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition));\r\n            " +
                                          "#endregion";

            s = patternStrBak.Replace("|0|", _nameTable).Replace("{1}", _strColumnName).Replace("{3}", _strColumnNameWithIndex);
            s = s.Replace("{4}", _vTbolIsVisibleString).Replace("{5}", _bolIsReadOnlyString).Replace("{6}", _bolIsVisibleString).Replace("{7}", _intVisiblePositionString);
            s = s.Replace("{8}", _strColumnCaption);
            return s;
        }

        static void GetStrProObj(object obj)
        {
            PropertyInfo[] dsPropertyInfo = obj.GetType().GetProperties();
            _strColumnName = _strColumnNameWithIndex = string.Empty;
            for (int i = 0; i < dsPropertyInfo.Length; i++)
            {
                _strColumnName += string.Format("\"{0}\", ", dsPropertyInfo[i].Name);
                _strColumnNameWithIndex += string.Format("\"{1}-{0}\", ", dsPropertyInfo[i].Name, i);
            }
            //strColumnName = dsPropertyInfo.Aggregate(string.Empty, (current, propertyInfo) => current + string.Format("\"{0}\", ", propertyInfo.Name));
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            FTemplateDetail.ChangeIndexTemplateColum(ultraGrid1.ActiveRow.Index, ultraGrid1.ActiveRow.Index - (((UltraButton)sender).Name.Equals("btnUp") ? 1 : -1), (BindingList<TemplateColumn>)ultraGrid1.DataSource, ultraGrid1);
        }
    }
}
//const string patternStr = "#region {0}\r\n            " +
//                          "nameTable = {0}_TableName;\r\n            " +
//                          "strColumnName = new List<string> { {1} };\r\n " +
//                          "//{3} \r\n            " +
//                          "strColumnCaption = strColumnToolTip = new List<string> { {1} };\r\n            " +
//                          "bolIsReadOnly = new List<bool>();\r\n" +
//                          "bolIsVisible = new List<bool>();\r\n" +
//                          "bolIsVisibleCbb = new List<bool>();\r\n" +
//                          "intColumnWidth = new List<int>();\r\n" +
//                          "intColumnMaxWidth = new List<int>();\r\n" +
//                          "intColumnMinWidth = new List<int>();\r\n" +
//                          "intVisiblePosition = new List<int>();\r\n " +
//                          "VTbolIsVisible = new List<int> { 1, 2 };    //vị trí có giá trị bằng true\r\n" +
//                          "VTbolIsVisibleCbb = new List<int> { 1, 2 };\r\n            " +
//                          "VTintVisiblePosition = new List<int>() {  };\r\n            " +
//                          "VTbolIsReadOnly = new List<int>() {  };\r\n \r\n            " +
//                          "dicVisiblePosition = new Dictionary<int, int>();   //bolIsVisible/ intVisiblePosition \r\n" +
//                          "if (VTintVisiblePosition.Count > 0) \r\n" +
//                          "{\r\n" +
//                          "for (int j = 0; j < VTbolIsVisible.Count; j++) " +
//                          "dicVisiblePosition.Add(VTbolIsVisible[j], " +
//                          "VTintVisiblePosition[j]); } \r\n \r\n           " +
//                          "for (int i = 0; i < strColumnName.Count; i++)\r\n            " +
//                          "{\r\n                " +
//                          "bool temp = VTbolIsVisible.Contains(i); \r\n " +
//                          "bolIsVisible.Add(temp);\r\n" +
//                          "bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));\r\n" +
//                          "bolIsReadOnly.Add(VTbolIsReadOnly.Contains(i));\r\n" +
//                          "intColumnWidth.Add(-1);\r\n" +
//                          "intColumnMaxWidth.Add(-1);\r\n" +
//                          "intColumnMinWidth.Add(-1);\r\n" +
//                          "intVisiblePosition.Add(dicVisiblePosition.Count > 0 ? (temp ? dicVisiblePosition[i] : strColumnName.Count - 1) : -1);\r\n" +
//                          "}\r\n            " +
//                          "dicTemp.Add(nameTable, CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition));\r\n" +
//                          "#endregion";