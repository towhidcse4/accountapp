﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.AllOPAccount;
using Accounting.Core.IService;
using Accounting.TextMessage;
using ClosedXML.Excel;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using ColumnStyle = System.Windows.Forms.ColumnStyle;

namespace Accounting
{
    public partial class FOPAccountObject : CustormForm
    {
        #region khai báo
        readonly List<OpAccountObjectByOrther> _lstAccountsAll;
        List<OpAccountObjectByOrther> _lstAccountsObject;
        public bool CloseOf = true;
        readonly Dictionary<string, List<OpAccountObjectByOrther>> _dictionary = new Dictionary<string, List<OpAccountObjectByOrther>>();
        readonly Dictionary<string, List<OpAccountObjectByOrther>> _dictionaryBackUp;
        ICurrencyService iCurrencyService { get { return IoC.Resolve<ICurrencyService>(); } }
        readonly AllOpAccount _allOpAccount;
        private readonly List<Guid> _rowDuocThem = new List<Guid>();
        private readonly List<Guid> _rowBiXoa = new List<Guid>();
        private bool _isLuu = true;
        private DateTime _postedDate;
        #endregion
        public FOPAccountObject(AllOpAccount allOpAccountIn)
        {
            InitializeComponent();

            #region Sự kiện
            uGrid.KeyDown += OPNUntil.uGrid_KeyDown;
            uCbbCurrency.RowSelected += uCbbCurrency_RowSelected;
            uGrid.CellDataError += uGrid_CellDataError;
            uGrid.Error += uGrid_Error;
            uGrid.AfterExitEditMode += uGrid_AfterExitEditMode;
            uGrid.AfterCellActivate += uGrid_AfterCellActivate;
            uGrid.BeforeRowsDeleted += uGrid_BeforeRowsDeleted;
            uGrid.MouseDown += uGrid_MouseDown;
            bt_ThemDongMoi.Click += bt_ThemDongMoi_Click;
            ThemDongMoiToolStripMenuItem.Click += bt_ThemDongMoi_Click;
            bt_xoa.Click += bt_xoa_Click;
            bt_dong.Click += bt_dong_Click;
            bt_luu.Click += bt_luu_Click;
            XoaDongToolStripMenuItem.Click += XoaDongToolStripMenuItem_Click;
            Closing += FOPAccountObject_Closing;
            #endregion

            _lstAccountsObject = new List<OpAccountObjectByOrther>();
            _allOpAccount = allOpAccountIn;
            gb_SoDuDauKy.Text = "";
            gb_SoDuDauKy.Text = "Nhập số dư đầu kỳ tài khoản " + allOpAccountIn.AccountNumber;
            _lstAccountsAll = IoC.Resolve<IOPAccountService>().GetListOpAccountObjectByOrthers(allOpAccountIn);

            #region tạo list số dư đầu kỳ tương ứng với loại tiên
            List<string> ds = OPNUntil.AccountService.GetByAccountNumber(allOpAccountIn.AccountNumber).DetailType.Split(';').ToList();
            int i = -1;
            if (ds.Contains("0"))//khách hàng
            {
                i = 1;
            }
            else if (ds.Contains("1"))//nhà cung cấp
            {
                i = 0;
            }
            else if (ds.Contains("2"))//nhân viên
            {
                i = 2;
            }
            else if (ds.Contains("11"))//đối tượng khác  trungnq thêm sửa bug 6190
            {
                i = 5;
            }
            var postedDate = OPNUntil.ApplicationStartDateOPN;
            _postedDate = postedDate;
            List<AccountingObject> lstacAccountingObjects = OPNUntil.AccountingObjectService.GetAccountingObjects(i);
            foreach (Currency currency in OPNUntil.ListCurrency())
            {
                if (_dictionary != null && _dictionary.All(c => c.Key != currency.ID))
                {
                    List<OpAccountObjectByOrther> lstAccountsObjectTao = new List<OpAccountObjectByOrther>();
                    if (_lstAccountsAll.Any(c => c.CurrencyID == currency.ID && c.AccountNumber == _allOpAccount.AccountNumber))
                    {
                        lstAccountsObjectTao.AddRange(_lstAccountsAll.Where(c => c.CurrencyID == currency.ID && c.AccountNumber == _allOpAccount.AccountNumber).ToList());
                    }
                    foreach (AccountingObject acc in lstacAccountingObjects)
                    {
                        if (lstAccountsObjectTao.All(c => c.AccountingObjectID != acc.ID))
                        {
                            OpAccountObjectByOrther opAccountObjectByOrther = new OpAccountObjectByOrther()
                            {
                                CurrencyID = currency.ID,
                                ExchangeRate = (decimal)(currency.ExchangeRate ?? 1),
                                ID = Guid.Empty,
                                TypeID = 701,
                                PostedDate = postedDate,
                                AccountNumber = _allOpAccount.AccountNumber,
                                DebitAmount = 0,
                                DebitAmountOriginal = 0,
                                CreditAmount = 0,
                                CreditAmountOriginal = 0,
                                BankAccountDetailID = null,
                                AccountingObjectID = acc.ID,
                                EmployeeID = null,
                                ContractID = null,
                                CostSetID = null,
                                ExpenseItemID = null,
                                OrderPriority = 0,
                                AccountingObjectCode = acc.AccountingObjectCode,
                                AccountingObjectName = acc.AccountingObjectName
                            };
                            lstAccountsObjectTao.Add(opAccountObjectByOrther);
                        }
                    }
                    _dictionary.Add(currency.ID, lstAccountsObjectTao.OrderBy(c => c.AccountingObjectCode).ToList());
                }
            }
            _dictionaryBackUp = Utils.CloneObject(_dictionary);
            #endregion
            uCbbCurrency.DataSource = iCurrencyService.GetIsActive(true);
            bt_xoa.Enabled = false;
            if (uCbbCurrency.Rows.Count > 0)
            {
                try
                {
                    foreach (UltraGridRow item in uCbbCurrency.Rows)
                    {
                        if (((Currency)item.ListObject).ID.Contains("VN"))
                        {
                            uCbbCurrency.SelectedRow = item;
                            break;
                        }
                    }
                }
                catch (Exception)
                {
                    uCbbCurrency.SelectedRow = uCbbCurrency.Rows[0];
                }

            }
            Utils.ConfigGrid(uCbbCurrency, ConstDatabase.Currency_TableName);
            XoaDongToolStripMenuItem.Visible = false;

            lstAccoutingObject = lstacAccountingObjects;
            lstEmployee = Utils.ListAccountingObject.Where(n => n.IsEmployee).ToList();

        }



        #region sự kiện
        //private void FOPAccountDetail_FormClosed(object sender, FormClosedEventArgs e)
        //{
        //    Currency currency = (Currency)uCbbCurrency.SelectedRow.ListObject;
        //    if (isLuu && Compare(_lstAccountsObject, _lstAccountsAll).Count > IoC.Resolve<IOPAccountService>().GetAll().Count && MSG.Question(resSystem.OPN05) == DialogResult.Yes)
        //    {
        //        List<OpAccountObjectByOrther> lst = Compare(_lstAccountsObject, _lstAccountsAll);
        //        IOPAccountService iop = IoC.Resolve<IOPAccountService>();
        //        iop.SaveOpAccountAndGeneralLedger(lst);
        //        Close();
        //        CloseOf = false;
        //    }
        //}
        private void uCbbCurrency_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (uCbbCurrency.SelectedRow.ListObject != null && uCbbCurrency.SelectedRow.Index >= 0)
            {
                Currency currency = (Currency)uCbbCurrency.SelectedRow.ListObject;
                if (_lstAccountsObject.Count > 0)
                {
                    foreach (string key in _dictionary.Keys)
                    {
                        var opAccountDetailByOrther = _lstAccountsObject.FirstOrDefault();
                        if (opAccountDetailByOrther != null && key == opAccountDetailByOrther.CurrencyID)
                        {
                            _dictionary[key] = _lstAccountsObject;
                            break;
                        }
                    }
                }
                _lstAccountsObject = _dictionary.Single(c => c.Key == currency.ID).Value;
                ConfigGrid(_lstAccountsObject, currency.ID != "VND", false);
            }


        }
        private void uGrid_Error(object sender, ErrorEventArgs e)
        {
            e.Cancel = true;
            if (e.ErrorType == ErrorType.Data && e.ErrorText.Contains("System.Decimal"))
            {
                switch (e.DataErrorInfo.Cell.Column.Key)
                {
                    case "DebitAmount":
                    case "CreditAmount":
                    case "DebitAmountOriginal":
                    case "CreditAmountOriginal":
                        e.DataErrorInfo.Cell.Row.Cells[e.DataErrorInfo.Cell.Column.Key].Value = 0;
                        break;
                }
            }
        }
        private void uGrid_CellDataError(object sender, CellDataErrorEventArgs e)
        {
            Utils.CellDataError((UltraGrid)sender);
            e.RaiseErrorEvent = false;
        }
        private void uGrid_AfterExitEditMode(object sender, EventArgs e)
        {
            Utils.CheckErrorInActiveCell<OpAccountDetailByOrther>(this, uGrid);
            ErrorGrid();
            try
            {
                After();

            }
            catch (Exceptions)
            {

            }
        }
        private void uGrid_AfterCellActivate(object sender, EventArgs e)
        {
            ErrorGrid();
        }
        private void uGrid_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            e.Cancel = true;
        }
        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                UIElement element = uGrid.DisplayLayout.UIElement.ElementFromPoint(e.Location);
                UltraGridRow row = (UltraGridRow)element.GetContext(typeof(UltraGridRow));
                if (row.Index >= 0)
                {
                    uGrid.Selected.Rows.Clear();
                    uGrid.Rows[row.Index].Selected = true;
                    uGrid.Rows[row.Index].Activate();
                    OpAccountObjectByOrther opAccountObjectByOrther = (OpAccountObjectByOrther)row.ListObject;
                    XoaDongToolStripMenuItem.Visible = (_lstAccountsObject.Count(c => c.AccountNumber == opAccountObjectByOrther.AccountNumber) > 1);
                }
            }

        }
        private void XoaDongToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var opAccountObjectByOrther = (OpAccountObjectByOrther)uGrid.ActiveRow.ListObject;
                if(opAccountObjectByOrther.ID != Guid.Empty)
                    _rowBiXoa.Add(opAccountObjectByOrther.ID);
                //_lstAccountsObject.Remove(opAccountObjectByOrther);
                _lstAccountsObject.FirstOrDefault(x => x.ID == opAccountObjectByOrther.ID).CreditAmount = 0;
                _lstAccountsObject.FirstOrDefault(x => x.ID == opAccountObjectByOrther.ID).CreditAmountOriginal = 0;
                _lstAccountsObject.FirstOrDefault(x => x.ID == opAccountObjectByOrther.ID).DebitAmount = 0;
                _lstAccountsObject.FirstOrDefault(x => x.ID == opAccountObjectByOrther.ID).DebitAmountOriginal = 0;
                _rowDuocThem.Remove(opAccountObjectByOrther.ID);
                ConfigGrid(_lstAccountsObject.Where(x=> !_rowBiXoa.Any(c=>c == x.ID)).ToList(), ((Currency)uCbbCurrency.SelectedRow.ListObject).ID != "VND", false);
            }
            catch (Exception)
            {

            }
        }
        private void bt_ThemDongMoi_Click(object sender, EventArgs e)
        {
            int viTriThemMoi = uGrid.ActiveRow.Index + 1;
            OpAccountObjectByOrther opAccount = (OpAccountObjectByOrther)uGrid.ActiveRow.ListObject;
            _lstAccountsObject.Insert(viTriThemMoi, new OpAccountObjectByOrther());
            _lstAccountsObject[viTriThemMoi].AccountingObjectCode = opAccount.AccountingObjectCode;
            _lstAccountsObject[viTriThemMoi].AccountingObjectName = opAccount.AccountingObjectName;
            _lstAccountsObject[viTriThemMoi].AccountNumber = opAccount.AccountNumber;
            _lstAccountsObject[viTriThemMoi].BankAccountDetailID = opAccount.BankAccountDetailID;
            _lstAccountsObject[viTriThemMoi].CurrencyID = opAccount.CurrencyID;
            _lstAccountsObject[viTriThemMoi].AccountingObjectID = opAccount.AccountingObjectID;
            _lstAccountsObject[viTriThemMoi].BranchID = opAccount.BranchID;
            _lstAccountsObject[viTriThemMoi].ContractID = opAccount.ContractID;
            _lstAccountsObject[viTriThemMoi].CostSetID = opAccount.CostSetID;
            _lstAccountsObject[viTriThemMoi].EmployeeID = opAccount.EmployeeID;
            _lstAccountsObject[viTriThemMoi].ExpenseItemID = opAccount.ExpenseItemID;
            _lstAccountsObject[viTriThemMoi].ExchangeRate = opAccount.ExchangeRate;
            _lstAccountsObject[viTriThemMoi].OrderPriority = opAccount.OrderPriority;
            _lstAccountsObject[viTriThemMoi].TypeID = opAccount.TypeID;
            _lstAccountsObject[viTriThemMoi].PostedDate = opAccount.PostedDate;
            _lstAccountsObject[viTriThemMoi].DebitAmount = 0;
            _lstAccountsObject[viTriThemMoi].DebitAmountOriginal = 0;
            _lstAccountsObject[viTriThemMoi].CreditAmount = 0;
            _lstAccountsObject[viTriThemMoi].CreditAmountOriginal = 0;
            _lstAccountsObject[viTriThemMoi].ID = Guid.NewGuid();
            _rowDuocThem.Add(_lstAccountsObject[viTriThemMoi].ID);

            ConfigGrid(_lstAccountsObject, ((Currency)uCbbCurrency.SelectedRow.ListObject).ID != "VND", false);
            uGrid.Rows[viTriThemMoi].Activate();
            uGrid.Rows[viTriThemMoi].Selected = true;
            if (_rowDuocThem.Count > 0) bt_xoa.Enabled = true;
            ErrorGrid();
        }
        private void bt_xoa_Click(object sender, EventArgs e)
        {
            if (_rowDuocThem.Count > 0)
            {
                OpAccountObjectByOrther opAccount = _lstAccountsObject.Single(c => c.ID == _rowDuocThem.Last());                
                if (opAccount.ID != Guid.Empty)
                    _rowBiXoa.Add(opAccount.ID);
                //_lstAccountsObject.Remove(opAccountObjectByOrther);
                _lstAccountsObject.FirstOrDefault(x => x.ID == opAccount.ID).CreditAmount = 0;
                _lstAccountsObject.FirstOrDefault(x => x.ID == opAccount.ID).CreditAmountOriginal = 0;
                _lstAccountsObject.FirstOrDefault(x => x.ID == opAccount.ID).DebitAmount = 0;
                _lstAccountsObject.FirstOrDefault(x => x.ID == opAccount.ID).DebitAmountOriginal = 0;
                _rowDuocThem.Remove(opAccount.ID);
                ConfigGrid(_lstAccountsObject.Where(x => !_rowBiXoa.Any(c => c == x.ID)).ToList(), ((Currency)uCbbCurrency.SelectedRow.ListObject).ID != "VND", false);
            }
            else
            {
                bt_xoa.Enabled = false;
            }
        }
        private void bt_dong_Click(object sender, EventArgs e)
        {
            CloseOf = true;
            Close();
        }
        private void bt_luu_Click(object sender, EventArgs e)
        {
            try
            {
                if (uCbbCurrency.SelectedRow.ListObject != null && uCbbCurrency.SelectedRow.Index >= 0)
                {
                    uGrid.UpdateData();
                    if (_lstAccountsObject.Count > 0)
                    {
                        foreach (string key in _dictionary.Keys)
                        {
                            var opAccountDetailByOrther = _lstAccountsObject.FirstOrDefault();
                            if (opAccountDetailByOrther != null && key == opAccountDetailByOrther.CurrencyID)
                            {
                                _dictionary[key] = _lstAccountsObject;
                                break;
                            }
                        }
                    }
                    List<OpAccountObjectByOrther> lstLuu = new List<OpAccountObjectByOrther>();
                    foreach (List<OpAccountObjectByOrther> lstAccountObjectByOrthers in _dictionary.Values)
                    {
                        List<OpAccountObjectByOrther> lst = Compare(lstAccountObjectByOrthers, _lstAccountsAll).ToList();
                        for (int i = lst.Count - 1; i >= 0; i--)
                        {
                            #region tài khoản lưỡng tính thực hiện trừ

                            if (lst[i].CreditAmountOriginal > lst[i].DebitAmountOriginal)
                            {
                                lst[i].CreditAmount = lst[i].CreditAmount - lst[i].DebitAmount;
                                lst[i].CreditAmountOriginal = lst[i].CreditAmountOriginal - lst[i].DebitAmountOriginal;
                                lst[i].DebitAmount = 0;
                                lst[i].DebitAmountOriginal = 0;
                            }
                            else if (lst[i].DebitAmountOriginal > lst[i].CreditAmountOriginal)
                            {
                                lst[i].DebitAmount = lst[i].DebitAmount - lst[i].CreditAmount;
                                lst[i].DebitAmountOriginal = lst[i].DebitAmountOriginal - lst[i].CreditAmountOriginal;
                                lst[i].CreditAmount = 0;
                                lst[i].CreditAmountOriginal = 0;
                            }
                            else if (lst[i].DebitAmountOriginal == lst[i].CreditAmountOriginal)
                            {
                                lst[i].DebitAmount = 0;
                                lst[i].CreditAmount = 0;
                                lst[i].DebitAmountOriginal = 0;
                                lst[i].CreditAmountOriginal = 0;
                                lst.RemoveAt(i);
                            }
                            #endregion
                        }
                        lstLuu.AddRange(lst);
                    }
                    // Hautv Add 18/12/2019
                    if (isGetDataFromExcel)
                    {
                        if(listForSaveExel.Count > 0)
                        {
                            lstLuu.AddRange(listForSaveExel);
                        }
                    }
                    IOPAccountService iop = IoC.Resolve<IOPAccountService>();
                    if(lstLuu.Count == 0)
                    {
                        iop.SaveOpAccountAndGeneralLedger(lstLuu);
                        //MessageBox.Show(resSystem.OPN01, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        MSG.Information("Lưu dữ liệu thành công");
                        _isLuu = false;
                        CloseOf = false;
                        Close();
                    }
                    else
                    {
                        iop.SaveOpAccountAndGeneralLedger(lstLuu);
                        //MessageBox.Show(resSystem.OPN01, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        MSG.Information("Lưu dữ liệu thành công");
                        _isLuu = false;
                        CloseOf = false;
                        Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MSG.Error(resSystem.OPN02);
            }

        }
        void FOPAccountObject_Closing(object sender, CancelEventArgs e)
        {
            if (_isLuu)
            {
                try
                {
                    uGrid.UpdateData();
                    After();
                    uGrid.Refresh();
                    uGrid.UpdateData();
                    if (uCbbCurrency.SelectedRow.ListObject != null && uCbbCurrency.SelectedRow.Index >= 0)
                    {
                        if (_lstAccountsObject.Count > 0)
                        {
                            foreach (string key in _dictionary.Keys)
                            {
                                var opAccountDetailByOrther = _lstAccountsObject.FirstOrDefault();
                                if (opAccountDetailByOrther != null && key == opAccountDetailByOrther.CurrencyID)
                                {
                                    _dictionary[key] = _lstAccountsObject;
                                    break;
                                }
                            }
                        }
                        List<OpAccountObjectByOrther> lstLuu = new List<OpAccountObjectByOrther>();
                        bool check = true;
                        foreach (string key in _dictionary.Keys)
                        {
                            if (_dictionary.Any(c => c.Key == key))
                            {
                                check = Utils.Compare(_dictionaryBackUp[key], _dictionary[key]);
                                if (check == false) break;
                            }

                        }
                        if (check == false && MSG.Question(resSystem.OPN05) == DialogResult.Yes)
                        {
                            foreach (List<OpAccountObjectByOrther> lstAccountObjectByOrthers in _dictionary.Values)
                            {
                                List<OpAccountObjectByOrther> lst = Compare(lstAccountObjectByOrthers, _lstAccountsAll).ToList();
                                for (int i = lst.Count - 1; i >= 0; i--)
                                {
                                    #region tài khoản lưỡng tính thực hiện trừ

                                    //if (lst[i].CreditAmountOriginal > lst[i].DebitAmountOriginal)
                                    //{
                                    //    lst[i].CreditAmount = lst[i].CreditAmount - lst[i].DebitAmount;
                                    //    lst[i].CreditAmountOriginal = lst[i].CreditAmountOriginal - lst[i].DebitAmountOriginal;
                                    //    lst[i].DebitAmount = 0;
                                    //    lst[i].DebitAmountOriginal = 0;
                                    //}
                                    //else if (lst[i].DebitAmountOriginal > lst[i].CreditAmountOriginal)
                                    //{
                                    //    lst[i].DebitAmount = lst[i].DebitAmount - lst[i].CreditAmount;
                                    //    lst[i].DebitAmountOriginal = lst[i].DebitAmountOriginal - lst[i].CreditAmountOriginal;
                                    //    lst[i].CreditAmount = 0;
                                    //    lst[i].CreditAmountOriginal = 0;
                                    //}
                                    //else if (lst[i].DebitAmountOriginal == lst[i].CreditAmountOriginal)
                                    //{
                                    //    lst[i].DebitAmount = 0;
                                    //    lst[i].CreditAmount = 0;
                                    //    lst[i].DebitAmountOriginal = 0;
                                    //    lst[i].CreditAmountOriginal = 0;
                                    //    lst.RemoveAt(i);
                                    //}
                                    #endregion
                                }
                                lstLuu.AddRange(lst);
                            }

                            // Hautv Add 18/12/2019
                            if (isGetDataFromExcel)
                            {
                                if (listForSaveExel.Count > 0)
                                {
                                    lstLuu.AddRange(listForSaveExel);
                                }
                            }

                            IOPAccountService iop = IoC.Resolve<IOPAccountService>();
                            iop.SaveOpAccountAndGeneralLedger(lstLuu);
                            MSG.Information(resSystem.OPN01);
                            CloseOf = false;
                        }

                    }
                }
                catch (Exception)
                {
                    MSG.Error(resSystem.OPN02);
                }

            }
        }
        #endregion

        #region hàm xử lý
        private void ConfigGrid(List<OpAccountObjectByOrther> lstAccountDetailByOrthersAdd, bool oPenCurrency, bool khongChonDong = true)
        {
            uGrid.SetDataBinding(lstAccountDetailByOrthersAdd, "");
            if (uGrid.DisplayLayout.Bands[0].Summaries.Count != 0) uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            if (!khongChonDong)
            {
                Utils.ConfigGrid(uGrid, ConstDatabase.OPAccountObject_TableName, false);
                foreach (UltraGridColumn column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    this.ConfigEachColumn4Grid<OpAccountDetailByOrther>(10000, column, uGrid);
                }
                Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["ExchangeRate"], ConstDatabase.Format_Rate);
                Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"], ConstDatabase.Format_TienVND);
                Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"], ConstDatabase.Format_TienVND);
                if (uCbbCurrency.Value.ToString() == "VND")
                {                    
                    Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOriginal"], ConstDatabase.Format_TienVND);
                    Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOriginal"], ConstDatabase.Format_TienVND);
                    Utils.AddSumColumn(uGrid, "DebitAmountOriginal", false, constDatabaseFormat: ConstDatabase.Format_TienVND);
                    Utils.AddSumColumn(uGrid, "CreditAmountOriginal", false, constDatabaseFormat: ConstDatabase.Format_TienVND);
                }
                else
                {
                    Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOriginal"], ConstDatabase.Format_ForeignCurrency);
                    Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOriginal"], ConstDatabase.Format_ForeignCurrency);
                    Utils.AddSumColumn(uGrid, "DebitAmountOriginal", false, constDatabaseFormat: ConstDatabase.Format_ForeignCurrency);
                    Utils.AddSumColumn(uGrid, "CreditAmountOriginal", false, constDatabaseFormat: ConstDatabase.Format_ForeignCurrency);
                }
                uGrid.DisplayLayout.AutoFitStyle = oPenCurrency == false ? AutoFitStyle.ResizeAllColumns : AutoFitStyle.ExtendLastColumn;

                Utils.AddSumColumn(uGrid, "DebitAmount", false, constDatabaseFormat: ConstDatabase.Format_TienVND);
                Utils.AddSumColumn(uGrid, "CreditAmount", false, constDatabaseFormat: ConstDatabase.Format_TienVND);



                //uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Header.Fixed = true;
                //uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Fixed = true;
            }
            foreach (UltraGridRow row in uGrid.Rows)
            {
                row.Appearance.BackColor = Color.White;

                row.Cells["AccountingObjectCode"].Activation = Activation.NoEdit;
                row.Cells["AccountingObjectCode"].Appearance.BackColor = Color.FromArgb(218, 220, 221);
                row.Cells["AccountingObjectName"].Activation = Activation.NoEdit;
                row.Cells["AccountingObjectName"].Appearance.BackColor = Color.FromArgb(218, 220, 221);

                row.Cells["DebitAmount"].Activation = Activation.AllowEdit;
                row.Cells["DebitAmountOriginal"].Activation = Activation.AllowEdit;
                row.Cells["CreditAmount"].Activation = Activation.AllowEdit;
                row.Cells["CreditAmountOriginal"].Activation = Activation.AllowEdit;

                uGrid.DisplayLayout.Bands[0].Columns["ExchangeRate"].Hidden = !oPenCurrency;
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOriginal"].Hidden = !oPenCurrency;
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOriginal"].Hidden = !oPenCurrency;
                uGrid.DisplayLayout.Bands[0].Columns["CurrencyID"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].Hidden = true;
            }
        }
        private void ErrorGrid()
        {
            if (_rowDuocThem != null)
            {
                foreach (UltraGridRow row in uGrid.Rows)
                {
                    foreach (Guid cacRowDaThem in _rowDuocThem)
                    {
                        try
                        {
                            OpAccountObjectByOrther op = _lstAccountsObject.Single(c => c.ID == cacRowDaThem);
                            if (!CheckError(op) && ((OpAccountObjectByOrther)row.ListObject).ID == op.ID)
                            {
                                Utils.NotificationRow(uGrid, row, string.Format(resSystem.OPN03, op.AccountNumber, row.Index));
                                break;
                            }
                            if (CheckError(op) && ((OpAccountObjectByOrther)row.ListObject).ID == op.ID)
                            {
                                Utils.RemoveNotificationRow(uGrid, row);
                                break;
                            }
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
        }
        bool CheckError(OpAccountObjectByOrther op)
        {
            if (_lstAccountsObject.Any(opAccountObjectByOrther => opAccountObjectByOrther.ID != op.ID
                                                            && opAccountObjectByOrther.AccountNumber == op.AccountNumber
                                                            && (opAccountObjectByOrther.ExpenseItemID != op.ExpenseItemID
                                                                || opAccountObjectByOrther.ContractID != op.ContractID
                                                                || opAccountObjectByOrther.CostSetID != op.CostSetID
                                                                || opAccountObjectByOrther.BankAccountDetailID != op.BankAccountDetailID
                                                                || opAccountObjectByOrther.CreditAmount != op.CreditAmount
                                                                || opAccountObjectByOrther.DebitAmount != op.DebitAmount
                                                                )))
            {
                return true;
            }
            return false;
            //return _lstAccounts.Any(c => c.ID != op.ID && c.AccountNumber.Contains(op.AccountNumber)
            //                             && (c.ExpenseItemID != op.ExpenseItemID
            //                                 || c.ContractID != op.ContractID
            //                                 || c.CostSetID != op.ContractID
            //                                 || c.BankAccount != op.BankAccount
            //                                 ));
            //&& op.AccountGroupKind != 0 || op.DebitAmount == 0
            //&& op.AccountGroupKind != 1 || op.CreditAmount == 0
            //&& op.AccountGroupKind != 3 || (op.CreditAmount == 0 && op.DebitAmount == 0);
        }
        private IEnumerable<OpAccountObjectByOrther> Compare(IEnumerable<OpAccountObjectByOrther> listThayDoi1, IEnumerable<OpAccountObjectByOrther> listBanDau1)
        {
            List<OpAccountObjectByOrther> listThayDoi = Utils.CloneObject(listThayDoi1).ToList();
            List<OpAccountObjectByOrther> listBanDau = Utils.CloneObject(listBanDau1).ToList();
            List<OpAccountObjectByOrther> lstKq = new List<OpAccountObjectByOrther>();
            //lọc bỏ các trường tiền bằng 0
            listThayDoi = listThayDoi.Where(c => c.CreditAmount != 0 || c.DebitAmount != 0).OrderBy(c => c.ID).ToList();
            //lọc bỏ các trường tiền bằng 0 và giá trị ID = guid.empty(trường tiền ghép với bảng account)
            listBanDau = listBanDau.Where(c => c.ID != Guid.Empty).OrderBy(c => c.ID).ToList();
            foreach (OpAccountObjectByOrther opAccountObjectByOrther in listThayDoi)
            {
                if (opAccountObjectByOrther.ID == Guid.Empty)//trường hợp có số tiền nhưng chưa tạo guid id (do ghép với bảng account lên không có guid id)
                {
                    opAccountObjectByOrther.ID = Guid.NewGuid();
                    lstKq.Add(opAccountObjectByOrther);
                }
                else if (listBanDau.Any(c => c.ID == opAccountObjectByOrther.ID))//trường hợp có tồn tại ID trong opaccount nhưng các giá trị khác đã bị thay đổi
                {
                    OpAccountObjectByOrther bandau = listBanDau.Single(c => c.ID == opAccountObjectByOrther.ID);
                    PropertyInfo[] propertyInfo = typeof(OpAccountObjectByOrther).GetProperties();
                    foreach (PropertyInfo info in propertyInfo)
                    {
                        PropertyInfo propItemBanDau = bandau.GetType().GetProperty(info.Name);
                        PropertyInfo propItemThayDoi = opAccountObjectByOrther.GetType().GetProperty(info.Name);
                        if (propItemBanDau != null && propItemBanDau.CanWrite && propItemThayDoi != null && propItemThayDoi.CanWrite
                            && propItemBanDau.GetValue(bandau, null) != propItemThayDoi.GetValue(opAccountObjectByOrther, null))
                        {
                            lstKq.Add(opAccountObjectByOrther);
                            break;
                        }
                    }
                }
                else if (listBanDau.All(c => c.ID != opAccountObjectByOrther.ID))//trường hợp giá trị đó được tạo mới bằng nút thêm
                {
                    lstKq.Add(opAccountObjectByOrther);
                }
            }
            foreach (OpAccountObjectByOrther opAccountObjectByOrther in listBanDau)
            {
                //trường hợp một id bên list ban đầu có (id này khác guid empty) mà trong list thay đổi không có và có cùng loại tiền
                if (listThayDoi.All(c => c.ID != Guid.Empty && opAccountObjectByOrther.ID != c.ID && c.CurrencyID == opAccountObjectByOrther.CurrencyID) && listThayDoi.Count > 0)
                {
                    //opAccountObjectByOrther.CreditAmount = 0;
                    //opAccountObjectByOrther.CreditAmountOriginal = 0;
                    //opAccountObjectByOrther.DebitAmount = 0;
                    //opAccountObjectByOrther.DebitAmountOriginal = 0;
                    lstKq.Add(opAccountObjectByOrther);
                }
            }
            foreach (OpAccountObjectByOrther opAccountObjectByOrther in lstKq)
            {
                opAccountObjectByOrther.PostedDate = OPNUntil.ApplicationStartDateOPN;
            }
            return lstKq;

        }
        private void QuyDoi(UltraGridCell cell, bool loai)
        {
            try
            {
                decimal exchangeRate = (decimal)cell.Row.Cells["ExchangeRate"].Value;
                decimal debitAmountOriginal = (decimal)cell.Row.Cells["DebitAmountOriginal"].Value;
                decimal debitAmount = (decimal)cell.Row.Cells["DebitAmount"].Value;
                decimal creditAmount = (decimal)cell.Row.Cells["CreditAmount"].Value;
                decimal creditAmountOriginal = (decimal)cell.Row.Cells["CreditAmountOriginal"].Value;
                if (loai) //chia lấy tiền nguyên tệ
                {
                    cell.Row.Cells["DebitAmountOriginal"].Value = debitAmount / exchangeRate;
                    cell.Row.Cells["CreditAmountOriginal"].Value = creditAmount / exchangeRate;
                }
                else // nhân lấy tiền việt nam
                {
                    cell.Row.Cells["DebitAmount"].Value = debitAmountOriginal * exchangeRate;
                    cell.Row.Cells["CreditAmount"].Value = creditAmountOriginal * exchangeRate;
                }
            }
            catch (Exception)
            {

            }

        }
        private void After()
        {
            UltraGridCell cell = uGrid.ActiveCell;
            if (cell != null)
            {
                switch (cell.Column.Key)
                {
                    case "ExchangeRate":
                        QuyDoi(cell, false);
                        break;
                    case "DebitAmount":
                    case "CreditAmount":
                        QuyDoi(cell, true);
                        break;
                    case "DebitAmountOriginal":
                    case "CreditAmountOriginal":
                        QuyDoi(cell, false);
                        break;
                }
            }
        }
        #endregion

        private void ultraButton1_Click(object sender, EventArgs e)
        {

            uGrid.DisplayLayout.Bands[0].Columns["ExchangeRate"].Hidden = false;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOriginal"].Hidden = false;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOriginal"].Hidden = false;
            uGrid.DisplayLayout.Bands[0].Columns["CurrencyID"].Hidden = false;
            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].Hidden = false;
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu cơ sở dữ liệu",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "So_du_dau_ky_theo_tai_khoan"
            };
            if (save.ShowDialog(this) == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    this.ultraGridExcelExporter1.Export(uGrid, duongdan);
                    MSG.Information("Xuất ra file Excel  thành công !");
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
            ConfigGrid(_lstAccountsObject, ((Currency)uCbbCurrency.SelectedRow.ListObject).ID != "VND", false);

        }

        private void linkFileMau_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            getFileMau();
        }

        bool isGetDataFromExcel = false;
        List<OpAccountObjectByOrther> listForSaveExel = new List<OpAccountObjectByOrther>();

        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            var openFile = new OpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                Filter = "Excel Files|*.xls;*.xlsx;*.xlsm"

            };

            var result = openFile.ShowDialog(this);
            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                try
                {
                    double fileSize = (double)new System.IO.FileInfo(openFile.FileName).Length / 1024;
                    if (fileSize > 102400)
                    {
                        MSG.Error("Dung lượng file vượt quá mức cho phép (100MB)");
                        return;
                    }
                    //txtFileName.Value = openFile.FileName;
                    //using (FileStream file = new FileStream(openFile.FileName, FileMode.Open, FileAccess.Read))
                    //{


                    //}
                    WaitingFrm.StartWaiting();
                    List<OpAccountObjectByOrther> lst = getFromExcel(openFile.FileName);
                    if (lst.Count > 0)
                    {
                        foreach (var item in lst.GroupBy(n => n.CurrencyID))
                        {
                            List<OpAccountObjectByOrther> lstBOT = _dictionary.Single(c => c.Key == item.Key).Value;
                            foreach (var item_ in lstBOT)
                            {
                                item_.DebitAmount = 0;
                                item_.DebitAmountOriginal = 0;
                                item_.CreditAmount = 0;
                                item_.CreditAmountOriginal = 0;
                                listForSaveExel.Add(item_);
                            }
                            _dictionary.Single(c => c.Key == item.Key).Value.Clear();
                            _dictionaryBackUp.Single(c => c.Key == item.Key).Value.Clear();
                            _dictionary.Single(c => c.Key == item.Key).Value.AddRange(item);
                           
                        }
                        isGetDataFromExcel = true;
                        _lstAccountsObject = _dictionary.Single(c => c.Key == ((Currency)uCbbCurrency.SelectedRow.ListObject).ID).Value;
                        ConfigGrid(_lstAccountsObject, ((Currency)uCbbCurrency.SelectedRow.ListObject).ID != "VND", false);
                    }

                    WaitingFrm.StopWaiting();
                    if (lst.Count > 0) MSG.Information("Nhập dữ liệu thành công");
                    //uGrid.Refresh();

                }
                catch (Exception ex)
                {
                    WaitingFrm.StopWaiting();
                    MSG.Error("File này đang được mở bởi ứng dụng khác");
                    return;
                }

            }
        }
        private void getFileMau()
        {
            SaveFileDialog sf = new SaveFileDialog
            {
                FileName = "Import_chitiettheoDT_dauky.xlsx",
                AddExtension = true,
                Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
            };

            if (sf.ShowDialog() == DialogResult.OK)
            {
                string path = System.IO.Path.GetDirectoryName(
                        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                string filePath = string.Format("{0}\\TemplateResource\\excel\\Import_chitiettheoDT_dauky.xlsx", path);
                ResourceHelper.MakeFileOutOfAStream(filePath, sf.FileName);
                
                if (MSG.Question("Tải xuống thành công, Bạn có muốn mở file vừa tải") == System.Windows.Forms.DialogResult.Yes)
                {
                    try
                    {
                        System.Diagnostics.Process.Start(sf.FileName);
                    }
                    catch
                    {
                    }
                }
            }
        }
        private bool isFileMau(Dictionary<string, int> headers, List<String> HEADER)
        {
            if (headers.Count != HEADER.Count) return false;
            foreach (var item in HEADER)
            {
                if (!headers.ContainsKey(item))
                {
                    return false;
                }
            }
            return true;
        }
        private readonly List<String> HEADER = new List<String> { "Mã loại tiền", "Mã đối tượng", "Tỷ giá", "Dư Nợ Nguyên Tệ", "Dư Nợ", "Dư Có Nguyên Tệ", "Dư Có", "Tài khoản ngân hàng", "Nhân viên", "Hợp đồng", "Đối tượng tập hợp CP", "Khoản mục CP" };
        Dictionary<string, int> headers = new Dictionary<string, int>();
        List<AccountingObject> lstAccoutingObject = new List<AccountingObject>();
        List<AccountingObject> lstEmployee = new List<AccountingObject>();
        public Dictionary<string, int> ReadHeader(IXLWorksheet ws)
        {
            headers = new Dictionary<string, int>();
            var head = ws.Row(1);
            for (int i = 1; i <= ws.ColumnsUsed().Count(); i++)
            {
                try
                {
                    headers.Add((string)head.Cell(i).Value, i);
                }
                catch (Exception ex)
                {
                    //MSG.Error("File mẫu không đúng \n" + ex.Message);
                }
            }
            return headers;
        }
        private List<OpAccountObjectByOrther> getFromExcel(string path)
        {
            XLWorkbook wb;
            IXLWorksheet ws;
            Dictionary<int, List<int>> ErrCell = new Dictionary<int, List<int>>();
            List<IXLCell> lstErrR = new List<IXLCell>();
            List<Currency> lstCurr = iCurrencyService.GetIsActive(true);
            List<OpAccountObjectByOrther> lst = new List<OpAccountObjectByOrther>();
            wb = new XLWorkbook(path);
            ws = wb.Worksheet(1);
            if (!isFileMau(ReadHeader(ws), HEADER))
            {
                WaitingFrm.StopWaiting();
                if (MSG.Question("Mẫu file không đúng. Bạn có muốn tải file mẫu không?") == DialogResult.Yes)
                {
                    getFileMau();
                    return new List<OpAccountObjectByOrther>();
                }
            }
            var nonEmptyDataRows = ws.RowsUsed().Where(n => n.RowNumber() > 1);
            if (nonEmptyDataRows.Count() == 0)
            {
                WaitingFrm.StopWaiting();
                MSG.Error("File không có dữ liệu");
                //Exit();
                return new List<OpAccountObjectByOrther>();
            }
            foreach (var item in nonEmptyDataRows)
            {
                OpAccountObjectByOrther opAccountObjectByOrther = new OpAccountObjectByOrther();
                // Lấy mã loại Tiền
                string maLoaiTien = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[0]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();

                    maLoaiTien = (item.Cell(headers[HEADER[0]]).Value==null?"": item.Cell(headers[HEADER[0]]).Value.ToString());
                }
                catch
                {

                }
                
                // Lấy mã Mã đối tượng
                string maDoiTuong = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[1]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();

                    maDoiTuong = (xLCell.Value==null?"": xLCell.Value.ToString());

                }
                catch
                {

                }
                
                // Lấy Tên đối tượng
                //string tenDoiTuong = "";
                //try
                //{
                //    IXLCell xLCell = item.Cell(headers[HEADER[2]]);
                //    xLCell.MergedRange();
                //    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                //    if (xLCell.HasComment)
                //        xLCell.Comment.Delete();

                //    tenDoiTuong = ((string)item.Cell(headers[HEADER[2]]).Value);
                //}
                //catch
                //{

                //}

                // Lấy tỷ giá
                decimal tyGia = 0;
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[2]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    tyGia = (decimal.Parse(xLCell.Value.ToString()));
                    if(maLoaiTien=="VND")
                    {
                        if(tyGia!=1)
                        {
                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                            xLCell.MergedRange();
                            xLCell.Comment.AddText("Tỷ giá không phù hợp với loại tiền");
                            lstErrR.Add(xLCell);
                        }
                    }
                }
                catch
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[2]]);
                    if (!xLCell.Value.ToString().IsNullOrEmpty())
                    {
                        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                        xLCell.MergedRange();
                        xLCell.Comment.AddText("Tỷ giá không hợp lệ");
                        lstErrR.Add(xLCell);
                    }
                    if (xLCell.Value.ToString().IsNullOrEmpty())
                    {
                        
                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                            xLCell.MergedRange();
                            xLCell.Comment.AddText("Tỷ giá không được để trống");
                            lstErrR.Add(xLCell);
                        
                    }
                }
                // Lấy dư nợ
                decimal DuNo = 0;
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[3]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    DuNo = (decimal.Parse(xLCell.Value.ToString()));
                }
                catch
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[3]]);
                    if (!xLCell.Value.ToString().IsNullOrEmpty())
                    {
                        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                        xLCell.MergedRange();
                        xLCell.Comment.AddText("Dư nợ không hợp lệ");
                        lstErrR.Add(xLCell);
                    }
                }
                // Lấy dư nợ quy đổi
                decimal DuNoQuyDoi = 0;
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[4]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    DuNoQuyDoi = (decimal.Parse(xLCell.Value.ToString()));
                }
                catch
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[4]]);
                    if (!xLCell.Value.ToString().IsNullOrEmpty())
                    {
                        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                        xLCell.MergedRange();
                        xLCell.Comment.AddText("Dư nợ quy đổi không hợp lệ");
                        lstErrR.Add(xLCell);
                    }
                }
                // Lấy dư có
                decimal duCo = 0;
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[5]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    duCo = (decimal.Parse(xLCell.Value.ToString()));
                }
                catch
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[5]]);
                    if (!xLCell.Value.ToString().IsNullOrEmpty())
                    {
                        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                        xLCell.MergedRange();
                        xLCell.Comment.AddText("Dư có không hợp lệ");
                        lstErrR.Add(xLCell);
                    }

                }
                // Lấy dư có quy đổi

                decimal duCoQuyDoi = 0;
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[6]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    duCoQuyDoi = (decimal.Parse(xLCell.Value.ToString()));
                }
                catch
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[6]]);
                    if (!xLCell.Value.ToString().IsNullOrEmpty())
                    {
                        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                        xLCell.MergedRange();
                        xLCell.Comment.AddText("Dư có quy đổi không hợp lệ");
                        lstErrR.Add(xLCell);
                    }
                }
                // Lấy tài khoản ngân hàng
                string taiKhoanNganHang = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[7]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();

                    taiKhoanNganHang = xLCell.Value.ToString();

                }
                catch
                {

                }
                // Lấy mã nhân viên
                string maNhanVien = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[8]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();

                    maNhanVien = xLCell.Value.ToString();

                }
                catch
                {

                }
                // Lấy mã hợp đồng
                string hopDong = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[9]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();

                    hopDong = xLCell.Value.ToString();

                }
                catch
                {

                }
                // Lấy mã đối tượng tập hợp chi phí
                string maDoiTuongTHCP = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[10]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();

                    maDoiTuongTHCP = xLCell.Value==null?"":xLCell.Value.ToString();

                }
                catch
                {

                }
                // Lấy khoản mục chi phí
                string khoanMucCP = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[11]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();

                    khoanMucCP = xLCell.Value.ToString();
                }
                catch
                {

                }
                Guid? idDoiTuong = null;
                Guid? idNhanVien = null;
                Guid? idTaiKhoanNH = null;
                Guid? idDoiTuongTPCH = null;
                Guid? idHopDong = null;
                Guid? idKhoanMucCP = null;
                AccountingObject doituong = new AccountingObject();

                try
                {
                    if (string.IsNullOrEmpty(maLoaiTien))
                    {
                        IXLCell xLCell_ = item.Cell(headers[HEADER[0]]);
                        xLCell_.Style.Fill.BackgroundColor = XLColor.Red;
                        xLCell_.MergedRange();
                        xLCell_.Comment.AddText("Mã loại tiền không được để trống");
                        lstErrR.Add(xLCell_);
                    }
                    else
                    {
                        if (!lstCurr.Any(n => n.ID == maLoaiTien))
                        {
                            IXLCell xLCell_ = item.Cell(headers[HEADER[0]]);
                            xLCell_.Style.Fill.BackgroundColor = XLColor.Red;
                            xLCell_.MergedRange();
                            xLCell_.Comment.AddText("Mã loại tiền không hợp lệ");
                            lstErrR.Add(xLCell_);
                        }
                    }
                    if (string.IsNullOrEmpty(maDoiTuong))
                    {
                        IXLCell xLCell_ = item.Cell(headers[HEADER[1]]);
                        xLCell_.Style.Fill.BackgroundColor = XLColor.Red;
                        xLCell_.MergedRange();
                        xLCell_.Comment.AddText("Mã đối tượng không được để trống");
                        lstErrR.Add(xLCell_);
                    }
                    else
                    {
                        if (!lstAccoutingObject.Any(n => n.AccountingObjectCode == maDoiTuong))
                        {
                            IXLCell xLCell_ = item.Cell(headers[HEADER[1]]);
                            xLCell_.Style.Fill.BackgroundColor = XLColor.Red;
                            xLCell_.MergedRange();
                            xLCell_.Comment.AddText("Mã đối tượng không hợp lệ");
                            lstErrR.Add(xLCell_);
                        }
                        else
                        {
                            doituong = lstAccoutingObject.FirstOrDefault(n => n.AccountingObjectCode == maDoiTuong);
                            idDoiTuong = doituong.ID;
                        }
                    }
                    if (!string.IsNullOrEmpty(maNhanVien))
                        if (!lstEmployee.Any(n => n.AccountingObjectCode == maNhanVien))
                        {
                            IXLCell xLCell_ = item.Cell(headers[HEADER[8]]);
                            xLCell_.Style.Fill.BackgroundColor = XLColor.Red;
                            xLCell_.MergedRange();
                            xLCell_.Comment.AddText("Mã nhân viên không hợp lệ");
                            lstErrR.Add(xLCell_);
                        }
                        else
                        {
                            idNhanVien = lstEmployee.FirstOrDefault(n => n.AccountingObjectCode == maNhanVien).ID;
                        }
                    if (!string.IsNullOrEmpty(taiKhoanNganHang))
                        if (!Utils.ListBankAccountDetail.Any(n => n.BankAccount == taiKhoanNganHang))
                        {
                            IXLCell xLCell_ = item.Cell(headers[HEADER[7]]);
                            xLCell_.Style.Fill.BackgroundColor = XLColor.Red;
                            xLCell_.MergedRange();
                            xLCell_.Comment.AddText("Tai khoản ngân hàng không hợp lệ");
                            lstErrR.Add(xLCell_);
                        }
                        else
                        {
                            idTaiKhoanNH = Utils.ListBankAccountDetail.FirstOrDefault(n => n.BankAccount == taiKhoanNganHang).ID;
                        }
                    if (!string.IsNullOrEmpty(maDoiTuongTHCP))
                        if (!Utils.ListCostSet.Any(n => n.CostSetCode == maDoiTuongTHCP))
                        {
                            IXLCell xLCell_ = item.Cell(headers[HEADER[10]]);
                            xLCell_.Style.Fill.BackgroundColor = XLColor.Red;
                            xLCell_.MergedRange();
                            xLCell_.Comment.AddText("Mã đối tượng tập hợp chi phí không hợp lệ");
                            lstErrR.Add(xLCell_);
                        }
                        else
                        {
                            idDoiTuongTPCH = Utils.ListCostSet.FirstOrDefault(n => n.CostSetCode == maDoiTuongTHCP).ID;
                        }
                    if (!string.IsNullOrEmpty(hopDong))
                        if (!Utils.ListEmContract.Any(n => n.Code == hopDong))
                        {
                            IXLCell xLCell_ = item.Cell(headers[HEADER[9]]);
                            xLCell_.Style.Fill.BackgroundColor = XLColor.Red;
                            xLCell_.MergedRange();
                            xLCell_.Comment.AddText("Mã hợp đồng không hợp lệ");
                            lstErrR.Add(xLCell_);
                        }
                        else
                        {
                            idHopDong = Utils.ListEmContract.FirstOrDefault(n => n.Code == hopDong).ID;
                        }
                    if (!string.IsNullOrEmpty(khoanMucCP))
                        if (!Utils.ListExpenseItem.Any(n => n.ExpenseItemCode == khoanMucCP))
                        {
                            IXLCell xLCell_ = item.Cell(headers[HEADER[11]]);
                            xLCell_.Style.Fill.BackgroundColor = XLColor.Red;
                            xLCell_.MergedRange();
                            xLCell_.Comment.AddText("Mã hợp đồng không hợp lệ");
                            lstErrR.Add(xLCell_);
                        }
                        else
                        {
                            idKhoanMucCP = Utils.ListExpenseItem.FirstOrDefault(n => n.ExpenseItemCode == khoanMucCP).ID;
                        }
                }
                catch (Exception ex)
                {

                }
                opAccountObjectByOrther.CurrencyID = maLoaiTien;
                opAccountObjectByOrther.AccountingObjectCode = maDoiTuong;
                opAccountObjectByOrther.AccountingObjectID = idDoiTuong;
                opAccountObjectByOrther.AccountingObjectName = doituong.AccountingObjectName;
                opAccountObjectByOrther.AccountNumber = _allOpAccount.AccountNumber;
                opAccountObjectByOrther.BankAccountDetailID = idTaiKhoanNH;
                //opAccountObjectByOrther.BranchID = opAccount.BranchID;
                opAccountObjectByOrther.ContractID = idHopDong;
                opAccountObjectByOrther.CostSetID = idDoiTuongTPCH;
                opAccountObjectByOrther.EmployeeID = idNhanVien;
                opAccountObjectByOrther.ExpenseItemID = idKhoanMucCP;
                opAccountObjectByOrther.ExchangeRate = tyGia;
                //opAccountObjectByOrther.OrderPriority = opAccount.OrderPriority;
                opAccountObjectByOrther.TypeID = 701;
                opAccountObjectByOrther.PostedDate = _postedDate;
                opAccountObjectByOrther.DebitAmount = DuNoQuyDoi;
                opAccountObjectByOrther.DebitAmountOriginal = DuNo;
                opAccountObjectByOrther.CreditAmount = duCoQuyDoi;
                opAccountObjectByOrther.CreditAmountOriginal = duCo;
                opAccountObjectByOrther.ID = Guid.NewGuid();

                lst.Add(opAccountObjectByOrther);
            }
            if (lstErrR.Count() > 0)
            {
                WaitingFrm.StopWaiting();
                if (MSG.Question("Dữ liệu file không đúng. Bạn có muốn tải file lỗi về không?") == System.Windows.Forms.DialogResult.Yes)
                {
                    System.Windows.Forms.SaveFileDialog sf = new System.Windows.Forms.SaveFileDialog
                    {
                        FileName = "Import_chitiettheoDT_dauky_Error.xlsx",
                        AddExtension = true,
                        Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
                    };
                    sf.Title = "Chọn thư mục lưu file";
                    if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        //XLWorkbook xLWorkbook = new XLWorkbook();
                        //xLWorkbook.AddWorksheet(ws);
                        try
                        {
                            wb.SaveAs(sf.FileName);
                            //xLWorkbook.SaveAs(sf.FileName);
                            //xLWorkbook.Dispose();
                            //Exit();
                            if (MSG.Question("Tải xuống thành công, Bạn có muốn mở file vừa tải") == System.Windows.Forms.DialogResult.Yes)
                            {
                                try
                                {
                                    System.Diagnostics.Process.Start(sf.FileName);
                                }
                                catch
                                {
                                }
                            }
                        }
                        catch(Exception ex)
                        {
                            MSG.Error("Lỗi khi lưu");
                            //xLWorkbook.Dispose();
                            //Exit();
                        }
                    }
                    return new List<OpAccountObjectByOrther>();
                }
                else
                {
                    //Exit();
                    return new List<OpAccountObjectByOrther>();
                }
            }
            return lst;
        }

        private void FOPAccountObject_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        private void uGrid_KeyDown(object sender, KeyEventArgs e)
        {
            UltraGrid uGrid = (UltraGrid)sender;
            if (uGrid.ActiveCell != null)
            {
                if (uGrid.ActiveCell.IsInEditMode)
                {
                    if (e.KeyCode == Keys.Up)
                    {
                        if (uGrid.ActiveCell.IsInEditMode && (uGrid.ActiveCell.ValueListResolved != null || uGrid.ActiveCell.EditorComponentResolved != null))
                        {
                            if (!uGrid.ActiveCell.DroppedDown) uGrid.ActiveCell.DroppedDown = true;
                        }
                        else if (uGrid.ActiveCell.ValueListResolved == null)
                        {
                            uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                            uGrid.PerformAction(UltraGridAction.AboveCell, false, false);
                            e.Handled = true;
                            uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                        }
                    }
                    if (e.KeyCode == Keys.Down)
                    {
                        if (uGrid.ActiveCell.IsInEditMode && (uGrid.ActiveCell.ValueListResolved != null || uGrid.ActiveCell.EditorComponentResolved != null))
                        {
                            if (!uGrid.ActiveCell.DroppedDown) uGrid.ActiveCell.DroppedDown = true;
                        }
                        else if (uGrid.ActiveCell.ValueListResolved == null)
                        {
                            uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                            uGrid.PerformAction(UltraGridAction.BelowCell, false, false);
                            e.Handled = true;
                            uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                        }
                    }
                    if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Right)
                    {
                        uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                        uGrid.PerformAction(UltraGridAction.NextCellByTab, false, false);
                        e.Handled = true;
                        uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                    }
                    if (e.KeyCode == Keys.Left)
                    {
                        uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                        uGrid.PerformAction(UltraGridAction.PrevCellByTab, false, false);
                        e.Handled = true;
                        uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                    }
                }
                else
                {
                    switch (e.KeyCode)
                    {
                        case Keys.Enter:
                            uGrid.ActiveCell.Selected = true;
                            uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                            break;
                        case Keys.Down:
                            if (uGrid.ActiveCell.IsInEditMode && (uGrid.ActiveCell.ValueListResolved != null || uGrid.ActiveCell.EditorComponentResolved != null))
                            {
                                if (!uGrid.ActiveCell.DroppedDown) uGrid.ActiveCell.DroppedDown = true;
                            }
                            else if (uGrid.ActiveCell.ValueListResolved == null)
                            {
                                uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                                uGrid.PerformAction(UltraGridAction.BelowCell, false, false);
                                e.Handled = true;
                                uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                            }
                            break;
                    }
                }
            }
            if (uGrid.ActiveCell != null && uGrid.ActiveCell.Column.ValueList != null && uGrid.ActiveCell.Column.AutoCompleteMode == Infragistics.Win.AutoCompleteMode.None)
            {
                if (((e.KeyCode >= Keys.A && e.KeyCode <= Keys.Z)
                     || (e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9)
                     || (e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.Divide)) || e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    var combo = (UltraCombo)uGrid.ActiveCell.Column.ValueList;
                    if (!uGrid.ActiveCell.DroppedDown) uGrid.PerformAction(UltraGridAction.ToggleDropdown);
                }
            }
        }

        private void FOPAccountObject_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (uGrid.ActiveCell != null)
                uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
        }
    }
}
