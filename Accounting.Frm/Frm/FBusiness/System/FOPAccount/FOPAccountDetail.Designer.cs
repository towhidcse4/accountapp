﻿namespace Accounting
{
    partial class FOPAccountDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FOPAccountDetail));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ThemDongMoiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.XoaDongToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraGridExcelExporter1 = new Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter(this.components);
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.bt_ThemDongMoi = new Infragistics.Win.Misc.UltraButton();
            this.bt_dong = new Infragistics.Win.Misc.UltraButton();
            this.bt_luu = new Infragistics.Win.Misc.UltraButton();
            this.bt_xoa = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uCbbCurrency = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCbbCurrency)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ThemDongMoiToolStripMenuItem,
            this.XoaDongToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(204, 48);
            // 
            // ThemDongMoiToolStripMenuItem
            // 
            this.ThemDongMoiToolStripMenuItem.Image = global::Accounting.Properties.Resources._1437136799_add;
            this.ThemDongMoiToolStripMenuItem.Name = "ThemDongMoiToolStripMenuItem";
            this.ThemDongMoiToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.ThemDongMoiToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.ThemDongMoiToolStripMenuItem.Text = "Thêm dòng mới";
            this.ThemDongMoiToolStripMenuItem.Click += new System.EventHandler(this.bt_ThemDongMoi_Click);
            // 
            // XoaDongToolStripMenuItem
            // 
            this.XoaDongToolStripMenuItem.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.XoaDongToolStripMenuItem.Name = "XoaDongToolStripMenuItem";
            this.XoaDongToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.XoaDongToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.XoaDongToolStripMenuItem.Text = "Xóa dòng";
            // 
            // ultraGroupBox1
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox1.Appearance = appearance1;
            this.ultraGroupBox1.Controls.Add(this.uGrid);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance2.FontData.BoldAsString = "True";
            appearance2.FontData.SizeInPoints = 13F;
            this.ultraGroupBox1.HeaderAppearance = appearance2;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 36);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(579, 294);
            this.ultraGroupBox1.TabIndex = 45;
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // uGrid
            // 
            this.uGrid.ContextMenuStrip = this.contextMenuStrip1;
            this.uGrid.DisplayLayout.RowSelectorImages.ActiveAndAddNewRowImage = global::Accounting.Properties.Resources.new_file_icon;
            this.uGrid.DisplayLayout.RowSelectorImages.ActiveAndDataChangedImage = global::Accounting.Properties.Resources.ubtnTemplate;
            this.uGrid.DisplayLayout.RowSelectorImages.ActiveRowImage = global::Accounting.Properties.Resources.ubtnForward;
            this.uGrid.DisplayLayout.RowSelectorImages.AddNewRowImage = global::Accounting.Properties.Resources.iAdd;
            this.uGrid.DisplayLayout.RowSelectorImages.DataChangedImage = global::Accounting.Properties.Resources.edit_validated_icon;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(3, 0);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(573, 291);
            this.uGrid.TabIndex = 0;
            this.uGrid.AfterCellActivate += new System.EventHandler(this.uGrid_AfterCellActivate);
            this.uGrid.AfterExitEditMode += new System.EventHandler(this.uGrid_AfterExitEditMode);
            this.uGrid.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.uGrid_BeforeRowsDeleted);
            this.uGrid.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.uGrid_Error);
            this.uGrid.CellDataError += new Infragistics.Win.UltraWinGrid.CellDataErrorEventHandler(this.uGridControl_CellDataError);
            this.uGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGrid_KeyDown);
            this.uGrid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uGrid_KeyPress);
            this.uGrid.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uGrid_MouseDown);
            // 
            // ultraGroupBox2
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox2.Appearance = appearance3;
            this.ultraGroupBox2.Controls.Add(this.ultraButton1);
            this.ultraGroupBox2.Controls.Add(this.bt_ThemDongMoi);
            this.ultraGroupBox2.Controls.Add(this.bt_dong);
            this.ultraGroupBox2.Controls.Add(this.bt_luu);
            this.ultraGroupBox2.Controls.Add(this.bt_xoa);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            appearance9.FontData.BoldAsString = "True";
            appearance9.FontData.SizeInPoints = 13F;
            this.ultraGroupBox2.HeaderAppearance = appearance9;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 330);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(579, 36);
            this.ultraGroupBox2.TabIndex = 46;
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraButton1
            // 
            this.ultraButton1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.Image = global::Accounting.Properties.Resources.Excel;
            this.ultraButton1.Appearance = appearance4;
            this.ultraButton1.Location = new System.Drawing.Point(64, 6);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(99, 23);
            this.ultraButton1.TabIndex = 5;
            this.ultraButton1.Text = "Xuất Excel";
            this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
            // 
            // bt_ThemDongMoi
            // 
            this.bt_ThemDongMoi.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.Image = global::Accounting.Properties.Resources._1437136799_add;
            this.bt_ThemDongMoi.Appearance = appearance5;
            this.bt_ThemDongMoi.Location = new System.Drawing.Point(169, 6);
            this.bt_ThemDongMoi.Name = "bt_ThemDongMoi";
            this.bt_ThemDongMoi.Size = new System.Drawing.Size(118, 23);
            this.bt_ThemDongMoi.TabIndex = 3;
            this.bt_ThemDongMoi.Text = "Thêm dòng";
            this.bt_ThemDongMoi.Click += new System.EventHandler(this.bt_ThemDongMoi_Click);
            // 
            // bt_dong
            // 
            this.bt_dong.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.bt_dong.Appearance = appearance6;
            this.bt_dong.Location = new System.Drawing.Point(498, 6);
            this.bt_dong.Name = "bt_dong";
            this.bt_dong.Size = new System.Drawing.Size(75, 23);
            this.bt_dong.TabIndex = 2;
            this.bt_dong.Text = "Đóng";
            this.bt_dong.Click += new System.EventHandler(this.bt_dong_Click);
            // 
            // bt_luu
            // 
            this.bt_luu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.bt_luu.Appearance = appearance7;
            this.bt_luu.Location = new System.Drawing.Point(417, 6);
            this.bt_luu.Name = "bt_luu";
            this.bt_luu.Size = new System.Drawing.Size(75, 23);
            this.bt_luu.TabIndex = 1;
            this.bt_luu.Text = "Lưu";
            this.bt_luu.Click += new System.EventHandler(this.bt_luu_Click);
            // 
            // bt_xoa
            // 
            this.bt_xoa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.bt_xoa.Appearance = appearance8;
            this.bt_xoa.Location = new System.Drawing.Point(293, 6);
            this.bt_xoa.Name = "bt_xoa";
            this.bt_xoa.Size = new System.Drawing.Size(118, 23);
            this.bt_xoa.TabIndex = 0;
            this.bt_xoa.Text = "Xóa dòng";
            this.bt_xoa.Click += new System.EventHandler(this.bt_xoa_Click);
            // 
            // ultraGroupBox4
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox4.Appearance = appearance10;
            this.ultraGroupBox4.Controls.Add(this.uCbbCurrency);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            appearance11.FontData.BoldAsString = "True";
            appearance11.FontData.SizeInPoints = 13F;
            this.ultraGroupBox4.HeaderAppearance = appearance11;
            this.ultraGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(579, 36);
            this.ultraGroupBox4.TabIndex = 44;
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // uCbbCurrency
            // 
            this.uCbbCurrency.Location = new System.Drawing.Point(63, 8);
            this.uCbbCurrency.Name = "uCbbCurrency";
            this.uCbbCurrency.Size = new System.Drawing.Size(233, 22);
            this.uCbbCurrency.TabIndex = 1;
            this.uCbbCurrency.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.uCbbCurrency_RowSelected);
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(6, 12);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(56, 23);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Loại tiền";
            // 
            // FOPAccountDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 366);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.ultraGroupBox4);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FOPAccountDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FOPAccountDetail";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FOPAccountDetail_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FOPAccountDetail_FormClosed);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            this.ultraGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCbbCurrency)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.UltraWinGrid.UltraCombo uCbbCurrency;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.Misc.UltraButton bt_ThemDongMoi;
        private Infragistics.Win.Misc.UltraButton bt_dong;
        private Infragistics.Win.Misc.UltraButton bt_luu;
        private Infragistics.Win.Misc.UltraButton bt_xoa;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ThemDongMoiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem XoaDongToolStripMenuItem;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter ultraGridExcelExporter1;
    }
}