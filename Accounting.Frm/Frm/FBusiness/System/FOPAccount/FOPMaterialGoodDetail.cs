﻿#region

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.Core.Domain.obj.AllOPAccount;
using ClosedXML.Excel;

#endregion

namespace Accounting
{
    public partial class FOPMaterialGoodDetail : CustormForm
    {
        #region khai báo
        private readonly List<OPMaterialGoodsByOrther> _lstMaterialGoodsAll = new List<OPMaterialGoodsByOrther>();
        private readonly List<OPMaterialGoodsByOrther> _lstMaterialGoodsAllChange = new List<OPMaterialGoodsByOrther>();
        public bool CloseOf = true;
        private bool _isLuu = true;
        readonly Dictionary<Guid, List<OPMaterialGoodsByOrther>> _dictionary = new Dictionary<Guid, List<OPMaterialGoodsByOrther>>();
        readonly Dictionary<Guid, List<OPMaterialGoodsByOrther>> _dictionaryBackUp = new Dictionary<Guid, List<OPMaterialGoodsByOrther>>();
        private IRepositoryService IrepositoryService
        {
            get { return IoC.Resolve<IRepositoryService>(); }
        }
        int LamTron = 0;
        #endregion

        public FOPMaterialGoodDetail(AllOpAccount allOpAccountIn)
        {
            InitializeComponent();
            #region sự kiện
            uGrid.KeyDown += OPNUntil.uGrid_KeyDown;
            FormClosed += FOPMaterialGoodDetail_FormClosed;
            uCbbRepository.RowSelected += uCbbRepository_RowSelected;
            bt_dong.Click += bt_dong_Click;
            uGrid.BeforeRowsDeleted += uGrid_BeforeRowsDeleted;
            uGrid.Error += uGrid_Error;
            bt_luu.Click += bt_luu_Click;
            #endregion

            List<Repository> lstRepositories = IrepositoryService.GetAll_ByIsActive(true);
            _lstRepositories = IrepositoryService.GetAll_ByIsActive(true);
            uCbbRepository.DataSource = lstRepositories;
            uCbbRepository.DisplayMember = "RepositoryCode";
            uCbbRepository.ValueMember = "RepositoryCode";
            LamTron = int.Parse(Utils.ListSystemOption.FirstOrDefault(x => x.Code == "DDSo_TienVND").Data);
            Utils.ConfigGrid(uCbbRepository, ConstDatabase.Repository_TableName);

            _lstMaterialGoodsAll = IoC.Resolve<IOPMaterialGoodsService>().GetListOPMaterialGoodsByOrther(allOpAccountIn.AccountNumber);

            #region tạo list số dư đầu kỳ tương ứng tất cả các tài khoản kho và add AccountNumber vào
            foreach (OPMaterialGoodsByOrther opMaterialGoodsByOrther in _lstMaterialGoodsAll)
            {
                opMaterialGoodsByOrther.AccountNumber = allOpAccountIn.AccountNumber;
            }
            _lstMaterialGoodsAll.OrderBy(c => c.MaterialGoodsCode);

            foreach (Repository repository in lstRepositories)
            {
                if (_dictionary != null && _dictionary.All(c => c.Key != repository.ID))
                {
                    List<OPMaterialGoodsByOrther> lstMaterialGoodsByOrthers = new List<OPMaterialGoodsByOrther>();

                    if (_lstMaterialGoodsAll.Any(c => c.RepositoryID == repository.ID))
                    {
                        lstMaterialGoodsByOrthers.AddRange(Utils.CloneObject(_lstMaterialGoodsAll.Where(c => c.RepositoryID == repository.ID).ToList()));
                    }

                    foreach (MaterialGoods materialGoodse in OPNUntil.MaterialGoodsService.GetByIsActive(true))
                    {
                        if (lstMaterialGoodsByOrthers.All(c => c.MaterialGoodsID != materialGoodse.ID))
                        {
                            OPMaterialGoodsByOrther opMaterialGoodsByOrther = new OPMaterialGoodsByOrther()
                            {
                                MaterialGoodsCode = materialGoodse.MaterialGoodsCode,
                                MaterialGoodsName = materialGoodse.MaterialGoodsName,
                                MaterialGoodsCategoryID = materialGoodse.MaterialGoodsCategoryID ?? Guid.Empty,
                                CurrencyID = "VND",
                                ID = Guid.Empty,
                                PostedDate = (DateTime.Now),
                                AccountNumber = allOpAccountIn.AccountNumber,
                                MaterialGoodsID = materialGoodse.ID, //123
                                RepositoryID = repository.ID, //123
                                ExchangeRate = 1,
                                UnitPrice = 0, //123
                                UnitPriceOriginal = 0, //123
                                Quantity = 0,
                                Amount = 0, //123
                                AmountOriginal = 0, //123
                                ExpiryDate = null,
                                LotNo = "",
                                Unit = "",
                                UnitConvert = "",
                                QuantityConvert = 0,
                                UnitPriceConvert = 0, //123
                                UnitPriceConvertOriginal = 0, //123
                                CostSetID = null,
                                ContractID = null,
                                BankAccountDetailID = null,
                                ContractName = null,
                                CostSetName = null,
                                BankName = null,
                                ExpenseItemID = null,
                                ConvertRate = 0,
                                OrderPriority = 0, //123,
                                TypeID = 702
                            };
                            lstMaterialGoodsByOrthers.Add(opMaterialGoodsByOrther);
                        }
                    }
                    _dictionary.Add(repository.ID, lstMaterialGoodsByOrthers.OrderBy(c => c.MaterialGoodsCode).ToList());
                }
            }
            _dictionaryBackUp = Utils.CloneObject(_dictionary);
            #endregion

            ConfigGrid(new List<OPMaterialGoodsByOrther>(), true, false);
            uGrid.DisplayLayout.Bands[0].Columns["CostSetID"].Header.Caption = "Đối tượng tập hợp CP";
        }

        #region sự kiện
        private void FOPMaterialGoodDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            Utils.ListMaterialGoodsCustom.Clear();
        }
        private void FOPMaterialGoodDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (uGrid.ActiveCell != null)
                uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
            if (uCbbRepository.SelectedRow == null || uCbbRepository.SelectedRow.Index < 0)
            {
                return;
            }
            if (_isLuu)
            {
                uGrid.UpdateData();
                try
                {
                    if (uCbbRepository.SelectedRow.Index >= 0)
                    {
                        List<OPMaterialGoodsByOrther> lstLuu = new List<OPMaterialGoodsByOrther>();
                        //foreach (List<OPMaterialGoodsByOrther> lstOPMaterialGoodsByOrthersdic in _dictionary.Values)
                        //{
                        //    IEnumerable<OPMaterialGoodsByOrther> lst = Compare(lstOPMaterialGoodsByOrthersdic, _lstMaterialGoodsAll);
                        //    lstLuu.AddRange(lst);

                        //}
                        foreach (var Key in _dictionary.Keys)
                        {
                            IEnumerable<OPMaterialGoodsByOrther> lst = Compare(_lstMaterialGoodsAllChange.Where(x => x.RepositoryID == Key).ToList(), _lstMaterialGoodsAll);
                            lstLuu.AddRange(lst);
                        }
                        bool check = true;
                        foreach (Guid key in _dictionary.Keys)
                        {
                            if (_dictionary.Any(c => c.Key == key))
                            {
                                check = Utils.Compare(_dictionaryBackUp[key], _dictionary[key]);
                                if (check == false) break;
                            }

                        }
                        if (check == false && MSG.Question(resSystem.OPN05) == DialogResult.Yes)
                        {
                            IOPMaterialGoodsService iop = IoC.Resolve<IOPMaterialGoodsService>();
                            IMaterialGoodsService iMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
                            #region Check xem mặt hàng đã phát sinh trong chứng từ nào chưa
                            List<string> lstMaterialGoodses = new List<string>();
                            lstMaterialGoodses = iMaterialGoodsService.CheckExistMaterialGoods(lstLuu.Select(c => c.MaterialGoodsID));
                            string s = "";
                            foreach (string lstMaterialGoodse in lstMaterialGoodses)
                            {
                                if (lstMaterialGoodse != null && !string.IsNullOrEmpty(lstMaterialGoodse))
                                    s += "<<" + lstMaterialGoodse + ">>";
                            }
                            if (lstMaterialGoodses.Count > 0)
                            {
                                if (MSG.Question(string.Format(resSystem.OPN04, "")) == DialogResult.Yes)
                                {
                                    try
                                    {
                                        iop.SaveOpAccountAndGeneralLedger(lstLuu);
                                        MSG.Information(resSystem.OPN01);
                                        _isLuu = false;
                                        CloseOf = false;
                                    }
                                    catch (Exception)
                                    {
                                        MSG.Error(resSystem.OPN02);
                                    }
                                }
                            }
                            //nếu phát sinh chứng từ mà không muốn thêm(thay đổi) loại bỏ bớt
                            else if (lstMaterialGoodses.Count > 0)
                            {
                                lstLuu = lstLuu.Where(c => lstMaterialGoodses.All(d => d != c.MaterialGoodsCode)).ToList();
                                try
                                {
                                    iop.SaveOpAccountAndGeneralLedger(lstLuu);
                                    MSG.Information(resSystem.OPN01);
                                    _isLuu = false;
                                    CloseOf = false;
                                }
                                catch (Exception)
                                {
                                    MSG.Error(resSystem.OPN02);
                                }
                            }
                            //nếu không phát sinh trong chứng từ nào
                            else if (lstMaterialGoodses.Count <= 0)
                            {
                                try
                                {
                                    iop.SaveOpAccountAndGeneralLedger(lstLuu);
                                    MSG.Information(resSystem.OPN01);
                                    _isLuu = false;
                                    CloseOf = false;
                                }
                                catch (Exception)
                                {
                                    MSG.Error(resSystem.OPN02);
                                }
                            }
                            //if (!string.IsNullOrEmpty(s))
                            //{
                            //    //nếu phát sinh chứng từ và vẫn muốn thêm (thay đổi)
                            //    if (MSG.Question(string.Format(resSystem.OPN04, "")) == DialogResult.Yes)
                            //    {
                            //        try
                            //        {
                            //            iop.SaveOpAccountAndGeneralLedger(lstLuu);
                            //            MessageBox.Show(resSystem.OPN01);
                            //            _isLuu = false;
                            //            CloseOf = false;
                            //        }
                            //        catch (Exception)
                            //        {
                            //            MSG.Error(resSystem.OPN02);
                            //        }
                            //    }
                            //    else  //nếu phát sinh chứng từ mà không muốn thêm (thay đổi) loại bỏ bớt
                            //    {
                            //        lstLuu = lstLuu.Where(c => lstMaterialGoodses.All(d => d != c.MaterialGoodsCode)).ToList();
                            //        try
                            //        {
                            //            iop.SaveOpAccountAndGeneralLedger(lstLuu);
                            //            MessageBox.Show(resSystem.OPN01);
                            //            _isLuu = false;
                            //            CloseOf = false;
                            //        }
                            //        catch (Exception)
                            //        {
                            //            MSG.Error(resSystem.OPN02);
                            //        }
                            //    }


                            //}
                            ////nếu không phát sinh trong chứng từ nào
                            //else
                            //{
                            //    try
                            //    {
                            //        iop.SaveOpAccountAndGeneralLedger(lstLuu);
                            //        MessageBox.Show(resSystem.OPN01);
                            //        _isLuu = false;
                            //        CloseOf = false;
                            //    }
                            //    catch (Exception)
                            //    {
                            //        MSG.Error(resSystem.OPN02);

                            //    }
                            //}
                            #endregion
                        }
                    }

                }
                catch (Exception)
                {
                    MSG.Error(resSystem.OPN02);
                }
            }
        }
        private void uCbbRepository_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (Utils.ListRepository.Any(x => x.RepositoryCode == uCbbRepository.Text))
            {
                Repository repository = (Repository)uCbbRepository.SelectedRow.ListObject;
                if (_dictionary.ContainsKey(repository.ID))
                    ConfigGrid(_dictionary[repository.ID], true, false);
            }
            uGrid.DisplayLayout.Bands[0].Columns["CostSetID"].Header.Caption = "Đối tượng tập hợp CP";
        }
        private void uGrid_Error(object sender, ErrorEventArgs e)
        {
            List<string> lstCheck = new List<string>() { "Quantity", "Amount", "UnitPrice" };
            e.Cancel = true;
            if (e.ErrorType == ErrorType.Data && e.ErrorText.Contains("System.Decimal") && lstCheck.Any(c => c == e.DataErrorInfo.Cell.Column.Key))
            {
                e.DataErrorInfo.Cell.Row.Cells[e.DataErrorInfo.Cell.Column.Key].Value = (decimal)0;
            }
        }
        private void uGrid_CellDataError(object sender, CellDataErrorEventArgs e)
        {
            Utils.CellDataError((UltraGrid)sender);
            e.RaiseErrorEvent = false;
        }
        private void uGrid_AfterExitEditMode(object sender, EventArgs e)
        {
            try
            {
                Utils.CheckActiveCell2<OpAccountDetailByOrther>(this, uGrid);
                UltraGridCell cell = uGrid.ActiveCell;
                if (cell != null)
                {
                    if (cell.Value == null)
                    {
                        cell.Value = (decimal)0;
                    }

                    switch (cell.Column.Key)
                    {
                        case "Amount":
                            QuyDoi(cell, 1);
                            break;
                        case "Quantity":
                            QuyDoi(cell, 2);
                            break;
                        case "UnitPrice":
                            QuyDoi(cell, 3);
                            if ((decimal)(cell.Row.Cells["UnitPrice"].Value) > 0 && (decimal)(cell.Row.Cells["Quantity"].Value) <= 0)
                                Utils.NotificationRow(uGrid, uGrid.ActiveCell.Row, "Xin nhập thêm số lượng cho vật tư hàng hóa");
                            else if ((decimal)(cell.Row.Cells["UnitPrice"].Value) <= 0 && (decimal)(cell.Row.Cells["Quantity"].Value) <= 0)
                                Utils.RemoveNotificationRow(uGrid, uGrid.ActiveCell.Row);
                            break;
                    }
                    if (cell.Row.Cells["Quantity"].Value != null && (decimal)(cell.Row.Cells["Quantity"].Value) > 0)
                        Utils.RemoveNotificationRow(uGrid, uGrid.ActiveCell.Row);
                }
                UltraGridRow row = uGrid.ActiveRow;
                if (row != null)
                {
                    OPMaterialGoodsByOrther temp = uGrid.Rows[row.Index].ListObject as OPMaterialGoodsByOrther;
                    if (_lstMaterialGoodsAllChange.Any(x => x.AccountNumber == temp.AccountNumber && x.MaterialGoodsID == temp.MaterialGoodsID && x.RepositoryID == temp.RepositoryID))
                    {
                        _lstMaterialGoodsAllChange.Remove(_lstMaterialGoodsAllChange.FirstOrDefault(x => x.AccountNumber == temp.AccountNumber && x.MaterialGoodsID == temp.MaterialGoodsID && x.RepositoryID == temp.RepositoryID));
                    }
                    _lstMaterialGoodsAllChange.Add(temp);
                }
            }
            catch (Exceptions ex)
            {
            }
        }
        private void uGrid_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            e.Cancel = true;
        }
        private void bt_dong_Click(object sender, EventArgs e)
        {
            CloseOf = true;
            Close();
        }
        private void bt_luu_Click(object sender, EventArgs e)
        {
            if (uCbbRepository.SelectedRow == null && uCbbRepository.Rows.Count > 0)
            {
                CloseOf = true;
                Close();
                return;
            }
            if (Luu())
            {
                CloseOf = false;
                _isLuu = false;
                Close();
            }
        }

        #endregion

        #region hàm xử lý


        private void ConfigGrid(List<OPMaterialGoodsByOrther> lstAccountDetailByOrthers, bool oPenCurrency,
            bool khongChonDong = true)
        {
            uGrid.DataSource = lstAccountDetailByOrthers;
            if (uGrid.DisplayLayout.Bands[0].Summaries.Count != 0) uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            if (!khongChonDong)
            {
                Utils.ConfigGrid(uGrid, ConstDatabase.OPMaterialGoods_TableName, false);
                uGrid.DisplayLayout.Bands[0].Columns["RepositoryCode"].Hidden = true;

                foreach (UltraGridColumn column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    this.ConfigEachColumn4Grid<OPMaterialGoodsByOrther>(10000, column, uGrid);
                }
                Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["Quantity"], ConstDatabase.Format_Quantity);
                Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["UnitPrice"], ConstDatabase.Format_DonGiaQuyDoi);
                Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["Amount"], ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

                Utils.AddSumColumn(uGrid, "Quantity", false, "", ConstDatabase.Format_Quantity);
                Utils.AddSumColumn(uGrid, "Amount", false, "", ConstDatabase.Format_TienVND);
                Utils.AddSumColumn(uGrid, "UnitPrice", false, "", ConstDatabase.Format_DonGiaQuyDoi);


                uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsCode"].Header.Fixed = true;
                uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsName"].Header.Fixed = true;

            }
            foreach (UltraGridRow row in uGrid.Rows)
            {
                row.Appearance.BackColor = Color.White;
                row.Cells["MaterialGoodsCode"].Activation = Activation.AllowEdit;
                row.Cells["MaterialGoodsCode"].Appearance.BackColor = Color.FromArgb(218, 220, 221);
                row.Cells["MaterialGoodsName"].Activation = Activation.AllowEdit;
                row.Cells["MaterialGoodsName"].Appearance.BackColor = Color.FromArgb(218, 220, 221);
            }
        }


        private IEnumerable<OPMaterialGoodsByOrther> Compare(IEnumerable<OPMaterialGoodsByOrther> listThayDoi1,
            IEnumerable<OPMaterialGoodsByOrther> listBanDau1)
        {
            List<OPMaterialGoodsByOrther> listThayDoi = Utils.CloneObject(listThayDoi1).ToList();
            List<OPMaterialGoodsByOrther> listBanDau = Utils.CloneObject(listBanDau1).ToList();
            List<OPMaterialGoodsByOrther> lstKq = new List<OPMaterialGoodsByOrther>();
            listThayDoi = listThayDoi./*Where(c => c.Quantity != 0 || c.Amount != 0 || c.UnitPrice != 0).*/OrderBy(c => c.ID).ToList();
            listBanDau = listBanDau.Where(c => (c.Quantity != 0 || c.Amount != 0 || c.UnitPrice != 0) && c.ID != Guid.Empty).OrderBy(c => c.ID).ToList();
            if (listThayDoi.Count == 0) return new List<OPMaterialGoodsByOrther>();

            foreach (OPMaterialGoodsByOrther opAccountDetailByOrther in listThayDoi)
            {
                if (opAccountDetailByOrther.ID == Guid.Empty)
                //trường hợp có số tiền nhưng chưa tạo guid id (do ghép với bảng account lên không có guid id)
                {
                    opAccountDetailByOrther.ID = Guid.NewGuid();
                    lstKq.Add(opAccountDetailByOrther);
                }
                else if (listBanDau.Any(c => c.ID == opAccountDetailByOrther.ID))
                //trường hợp có tồn tại ID trong opaccount nhưng các giá trị khác đã bị thay đổi
                {
                    OPMaterialGoodsByOrther bandau = listBanDau.Single(c => c.ID == opAccountDetailByOrther.ID);
                    PropertyInfo[] propertyInfo = typeof(OPMaterialGoodsByOrther).GetProperties();
                    foreach (PropertyInfo info in propertyInfo)
                    {
                        PropertyInfo propItemBanDau = bandau.GetType().GetProperty(info.Name);
                        PropertyInfo propItemThayDoi = opAccountDetailByOrther.GetType().GetProperty(info.Name);
                        if (propItemBanDau != null && propItemBanDau.CanWrite && propItemThayDoi != null && propItemThayDoi.CanWrite && propItemBanDau.GetValue(bandau, null) != propItemThayDoi.GetValue(opAccountDetailByOrther, null))
                        {
                            lstKq.Add(opAccountDetailByOrther);
                            break;
                        }
                    }
                }
                else if (listBanDau.All(c => c.ID != opAccountDetailByOrther.ID))
                //trường hợp giá trị đó được tạo mới bằng nút thêm
                {
                    lstKq.Add(opAccountDetailByOrther);
                }
            }
            foreach (OPMaterialGoodsByOrther opAccountDetailByOrther in listBanDau)
            {
                //trường hợp một id bên list ban đầu có (id này khác guid empty) mà trong list thay đổi không có và có cùng kho
                //if (listThayDoi.Count() != 0 && listThayDoi.All(c => c.ID != Guid.Empty && opAccountDetailByOrther.ID != c.ID && c.RepositoryID == opAccountDetailByOrther.RepositoryID))
                //{
                //    opAccountDetailByOrther.Amount = 0;
                //    opAccountDetailByOrther.Quantity = 0;
                //    opAccountDetailByOrther.UnitPrice = 0;
                //    lstKq.Add(opAccountDetailByOrther);
                //}
            }
            foreach (OPMaterialGoodsByOrther opAccountDetailByOrther in lstKq)
            {
                opAccountDetailByOrther.PostedDate = OPNUntil.ApplicationStartDateOPN;
            }
            return lstKq;
        }

        private void QuyDoi(UltraGridCell cell, int loai)
        {
            try
            {
                if (cell.Row.Cells["Quantity"].Value == null) { cell.Row.Cells["Quantity"].Value = 0; return; }
                if (cell.Row.Cells["Amount"].Value == null) { cell.Row.Cells["Amount"].Value = 0; return; }
                if (cell.Row.Cells["UnitPrice"].Value == null) { cell.Row.Cells["UnitPrice"].Value = 0; return; }
                decimal amount = (decimal)(cell.Row.Cells["Amount"].Value);
                decimal quantity = (decimal)(cell.Row.Cells["Quantity"].Value);
                decimal unitPrice = (decimal)(cell.Row.Cells["UnitPrice"].Value);
                //if (amount > 0 && quantity > 0 && unitPrice > 0) return;
                if (loai == 1) //nhân
                {
                    //cell.Row.Cells["Amount"].Value = unitPrice * quantity;
                    if (unitPrice > 0 && quantity == 0)
                        cell.Row.Cells["Quantity"].Value = amount / unitPrice;
                    else if (quantity > 0 && unitPrice == 0)
                        cell.Row.Cells["UnitPrice"].Value = amount / quantity;
                }
                if (loai == 2 || loai == 3)
                {
                    if (amount == 0 && quantity > 0 && unitPrice > 0)
                        cell.Row.Cells["Amount"].Value = Math.Round(unitPrice * quantity, LamTron, MidpointRounding.AwayFromZero);
                    else if (amount > 0 && quantity == 0 && loai == 3)
                        cell.Row.Cells["Quantity"].Value = amount / unitPrice;
                    else if (amount > 0 && unitPrice == 0 && loai == 2)
                        cell.Row.Cells["UnitPrice"].Value = amount / quantity;
                    else
                        cell.Row.Cells["Amount"].Value = Math.Round(unitPrice * quantity, LamTron, MidpointRounding.AwayFromZero);
                }
            }
            catch (Exception)
            {
            }
        }

        private bool Luu()
        {
            try
            {
                if (uCbbRepository.SelectedRow.Index >= 0)
                {
                    Repository repository = (Repository)uCbbRepository.SelectedRow.ListObject;
                    List<OPMaterialGoodsByOrther> lstLuu = new List<OPMaterialGoodsByOrther>();
                    //foreach (List<OPMaterialGoodsByOrther> lstOPMaterialGoodsByOrthersdic in _dictionary.Values)
                    //{

                    //    IEnumerable<OPMaterialGoodsByOrther> lst = Compare(lstOPMaterialGoodsByOrthersdic, _lstMaterialGoodsAll);
                    //    lstLuu.AddRange(lst);
                    //}
                    foreach (var Key in _dictionary.Keys)
                    {

                        IEnumerable<OPMaterialGoodsByOrther> lst = Compare(_lstMaterialGoodsAllChange.Where(x => x.RepositoryID == Key).ToList(), _lstMaterialGoodsAll);
                        lstLuu.AddRange(lst);
                    }
                    IOPMaterialGoodsService iop = IoC.Resolve<IOPMaterialGoodsService>();
                    IMaterialGoodsService iMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();

                    #region Check xem mặt hàng đã phát sinh trong chứng từ nào chưa
                    List<string> lstMaterialGoodses = new List<string>();
                    lstMaterialGoodses = iMaterialGoodsService.CheckExistMaterialGoods(lstLuu.Select(c => c.MaterialGoodsID));
                    string s = "";
                    foreach (string lstMaterialGoodse in lstMaterialGoodses)
                    {
                        s += "<<" + lstMaterialGoodse + ">>";
                    }
                    //nếu phát sinh chứng từ và vẫn muốn thêm (thay đổi)
                    if (lstMaterialGoodses.Count > 0)
                    {
                        if (MSG.Question(string.Format(resSystem.OPN04, "")) == DialogResult.Yes)
                        {
                            try
                            {
                                if (lstLuu.Count == 0) return false;
                                iop.SaveOpAccountAndGeneralLedger(lstLuu);
                                MSG.Information(resSystem.OPN01);
                                _isLuu = false;
                                CloseOf = false;
                                return true;
                            }
                            catch (Exception)
                            {
                                MSG.Error(resSystem.OPN02);
                            }
                        }
                    }
                    //nếu phát sinh chứng từ mà không muốn thêm(thay đổi) loại bỏ bớt
                    else if (lstMaterialGoodses.Count > 0)
                    {
                        lstLuu = lstLuu.Where(c => lstMaterialGoodses.All(d => d != c.MaterialGoodsCode)).ToList();
                        try
                        {
                            if (lstLuu.Count == 0) return false;
                            iop.SaveOpAccountAndGeneralLedger(lstLuu);
                            MSG.Information(resSystem.OPN01);
                            _isLuu = false;
                            CloseOf = false;
                            return true;
                        }
                        catch (Exception)
                        {
                            MSG.Error(resSystem.OPN02);
                        }
                    }
                    //nếu không phát sinh trong chứng từ nào
                    else if (lstMaterialGoodses.Count <= 0)
                    {
                        try
                        {
                            if (lstLuu.Count == 0) return false;
                            iop.SaveOpAccountAndGeneralLedger(lstLuu);
                            //MessageBox.Show(resSystem.OPN01);
                            MSG.Information(resSystem.OPN01);
                            _isLuu = false;
                            CloseOf = false;
                            return true;
                        }
                        catch (Exception)
                        {
                            MSG.Error(resSystem.OPN02);
                        }
                    }
                    #endregion
                }
                else
                {
                    return false;
                }

            }
            catch (Exception)
            {
                MSG.Error(resSystem.OPN02);
            }
            return false;
        }

        #endregion
        private void uCbbRepository_ItemNotInList(object sender, Infragistics.Win.UltraWinEditors.ValidationErrorEventArgs e)
        {
            Repository b = new Repository();
            if (uCbbRepository.Text != b.RepositoryCode && uCbbRepository.Text != "")
            {
                MSG.Warning(string.Format(resSystem.MSG_Catalog_FMaterialGoodsDetail5, uCbbRepository.Text));
                uCbbRepository.Focus();
                return;
            }

        }

        private void uGrid_AfterCellUpdate(object sender, CellEventArgs e)
        {


        }

        // Add and Edit by Hautv
        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            var openFile = new OpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                Filter = "Excel Files|*.xls;*.xlsx;*.xlsm"

            };

            var result = openFile.ShowDialog(this);
            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                try
                {
                    double fileSize = (double)new System.IO.FileInfo(openFile.FileName).Length / 1024;
                    if (fileSize > 102400)
                    {
                        MSG.Error("Dung lượng file vượt quá mức cho phép (100MB)");
                        return;
                    }
                    WaitingFrm.StartWaiting();
                    List<OPMaterialGoodsByOrther> lst = getFromExcel(openFile.FileName);
                    if (lst.Count > 0)
                    {
                        foreach (var item in lst.GroupBy(n => n.RepositoryCode))
                        {
                            foreach (var item_ in item)
                            {
                                OPMaterialGoodsByOrther opMaterialGoodsByOrther = _dictionary[item_.RepositoryID].FirstOrDefault(n => n.MaterialGoodsID == item_.MaterialGoodsID);
                                if (opMaterialGoodsByOrther != null)
                                {
                                    opMaterialGoodsByOrther.Quantity = item_.Quantity;
                                    opMaterialGoodsByOrther.UnitPrice = item_.UnitPrice;
                                    opMaterialGoodsByOrther.Amount = item_.Amount;
                                    opMaterialGoodsByOrther.ContractID = item_.ContractID;
                                    opMaterialGoodsByOrther.ExpiryDate = item_.ExpiryDate;
                                    opMaterialGoodsByOrther.BankAccountDetailID = item_.BankAccountDetailID;
                                    opMaterialGoodsByOrther.CostSetID = item_.CostSetID;
                                    opMaterialGoodsByOrther.LotNo = item_.LotNo;
                                    opMaterialGoodsByOrther.ExpenseItemID = item_.ExpenseItemID;
                                    opMaterialGoodsByOrther.RepositoryID = item_.RepositoryID;
                                    if (!_lstMaterialGoodsAllChange.Any(x => x.AccountNumber == opMaterialGoodsByOrther.AccountNumber && x.MaterialGoodsID == opMaterialGoodsByOrther.MaterialGoodsID && x.RepositoryID == opMaterialGoodsByOrther.RepositoryID))
                                    {
                                        _lstMaterialGoodsAllChange.Add(opMaterialGoodsByOrther);
                                    }
                                }
                            }

                        }
                        if (uCbbRepository.SelectedRow != null)
                        {
                            Repository repository = (Repository)uCbbRepository.SelectedRow.ListObject;
                            if (_dictionary.ContainsKey(repository.ID))
                                ConfigGrid(_dictionary[repository.ID], true, false);
                        }
                    }
                    WaitingFrm.StopWaiting();
                    if (lst.Count > 0) MSG.Information("Nhập dữ liệu thành công");
                }
                catch (Exception ex)
                {
                    WaitingFrm.StopWaiting();
                    MSG.Error("File này đang được mở bởi ứng dụng khác");
                    return;
                }

            }
        }
        private void getFileMau()
        {
            SaveFileDialog sf = new SaveFileDialog
            {
                FileName = "Import_VTHH_dauky.xlsx",
                AddExtension = true,
                Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
            };

            if (sf.ShowDialog() == DialogResult.OK)
            {
                string path = System.IO.Path.GetDirectoryName(
                        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                string filePath = string.Format("{0}\\TemplateResource\\excel\\Import_VTHH_dauky.xlsx", path);
                ResourceHelper.MakeFileOutOfAStream(filePath, sf.FileName);
                MSG.Information("Tải xuống thành công");
            }
        }
        private void linkFileMau_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            getFileMau();
        }
        private bool isFileMau(Dictionary<string, int> headers, List<String> HEADER)
        {
            if (headers.Count != HEADER.Count) return false;
            foreach (var item in HEADER)
            {
                if (!headers.ContainsKey(item))
                {
                    return false;
                }
            }
            return true;
        }
        public Dictionary<string, int> ReadHeader(IXLWorksheet ws)
        {
            headers = new Dictionary<string, int>();
            var head = ws.Row(1);
            for (int i = 1; i <= ws.ColumnsUsed().Count(); i++)
            {
                try
                {
                    headers.Add((string)head.Cell(i).Value, i);
                }
                catch (Exception ex)
                {
                    //MSG.Error("File mẫu không đúng \n" + ex.Message);
                }
            }
            return headers;
        }
        private readonly List<String> HEADER = new List<String> { "Mã kho", "Mã VTHH", "Số lượng", "Đơn giá", "Thành tiền", "Số lô", "Hạn dùng", "Đối tượng tập hợp CP", "Hợp đồng", "TK ngân hàng", "Khoản mục CP" };
        Dictionary<string, int> headers = new Dictionary<string, int>();
        List<AccountingObject> lstAccoutingObject = new List<AccountingObject>();
        List<AccountingObject> lstEmployee = new List<AccountingObject>();
        List<Repository> _lstRepositories = new List<Repository>();
        private List<OPMaterialGoodsByOrther> getFromExcel(string path)
        {
            XLWorkbook wb;
            IXLWorksheet ws;
            Dictionary<int, List<int>> ErrCell = new Dictionary<int, List<int>>();
            List<IXLCell> lstErrR = new List<IXLCell>();
            List<OPMaterialGoodsByOrther> lst = new List<OPMaterialGoodsByOrther>();
            wb = new XLWorkbook(path);
            ws = wb.Worksheet(1);
            if (!isFileMau(ReadHeader(ws), HEADER))
            {
                WaitingFrm.StopWaiting();
                if (MSG.Question("Mẫu file không đúng. Bạn có muốn tải file mẫu không?") == DialogResult.Yes)
                {
                    getFileMau();
                    return new List<OPMaterialGoodsByOrther>();
                }
            }
            var nonEmptyDataRows = ws.RowsUsed().Where(n => n.RowNumber() > 1);
            if (nonEmptyDataRows.Count() == 0)
            {
                WaitingFrm.StopWaiting();
                MSG.Error("File không có dữ liệu");
                //Exit();
                return new List<OPMaterialGoodsByOrther>();
            }
            foreach (var item in nonEmptyDataRows)
            {
                OPMaterialGoodsByOrther oPMaterialGoodsByOrther = new OPMaterialGoodsByOrther();
                // Lấy mã kho
                string maKho = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[0]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();

                    maKho = xLCell.Value.ToString();
                }
                catch
                {

                }
                if (string.IsNullOrEmpty(maKho)) continue;
                // Lấy mã Mã VTHH
                string maVTHH = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[1]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();

                    maVTHH = xLCell.Value.ToString();

                }
                catch
                {

                }
                if (string.IsNullOrEmpty(maVTHH)) continue;

                // Lấy số lượng
                decimal soluong = 0;
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[2]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    soluong = (decimal.Parse(xLCell.Value.ToString()));
                }
                catch
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[2]]);
                    if (!xLCell.Value.ToString().IsNullOrEmpty())
                    {
                        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                        xLCell.MergedRange();
                        xLCell.Comment.AddText("Số lượng không hợp lệ");
                        lstErrR.Add(xLCell);
                    }
                }
                // Lấy đơn giá
                decimal donGia = 0;
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[3]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    donGia = (decimal.Parse(xLCell.Value.ToString()));
                }
                catch
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[3]]);
                    if (!xLCell.Value.ToString().IsNullOrEmpty())
                    {
                        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                        xLCell.MergedRange();
                        xLCell.Comment.AddText("Đơn giá không hợp lệ");
                        lstErrR.Add(xLCell);
                    }
                }
                // Lấy Thành tiền
                decimal thanhTien = 0;
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[4]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    if (xLCell.ValueCached != null)
                    {
                        thanhTien = (decimal.Parse(xLCell.ValueCached.ToString()));
                    }
                    else
                    {
                        thanhTien = (decimal.Parse(xLCell.Value.ToString()));
                    }

                }
                catch
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[4]]);
                    if (!xLCell.Value.ToString().IsNullOrEmpty())
                    {
                        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                        xLCell.MergedRange();
                        xLCell.Comment.AddText("Thành tiền không hợp lệ");
                        lstErrR.Add(xLCell);
                    }
                }
                // Lấy số lô
                string soLo = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[5]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();

                    soLo = xLCell.Value.ToString();

                }
                catch
                {

                }

                //Lấy Hạn dùng
                DateTime? hanDung = null;
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[6]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    hanDung = ((DateTime)xLCell.Value);
                }
                catch (Exception ex)
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[6]]);
                    if (!xLCell.Value.ToString().IsNullOrEmpty())
                    {
                        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                        xLCell.MergedRange();
                        xLCell.Comment.AddText("Hạn dùng không hợp lệ");
                        lstErrR.Add(xLCell);
                    }
                }

                // Lấy mã đối tượng tập hợp chi phí
                string maDoiTuongTHCP = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[7]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();

                    maDoiTuongTHCP = xLCell.Value.ToString();

                }
                catch
                {

                }
                // Lấy mã hợp đồng
                string hopDong = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[8]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();

                    hopDong = xLCell.Value.ToString();

                }
                catch
                {

                }
                // Lấy tài khoản ngân hàng
                string taiKhoanNganHang = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[9]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();

                    taiKhoanNganHang = xLCell.Value.ToString();

                }
                catch
                {

                }

                // Lấy khoản mục chi phí
                string khoanMucCP = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[10]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();

                    khoanMucCP = xLCell.Value.ToString();
                }
                catch
                {

                }
                Guid idDoiTuong = Guid.Empty;
                Guid? idTaiKhoanNH = null;
                Guid? idDoiTuongTPCH = null;
                Guid? idHopDong = null;
                Guid? idKhoanMucCP = null;
                MaterialGoods doituong = new MaterialGoods();
                Guid idKho = Guid.NewGuid();
                try
                {
                    if (string.IsNullOrEmpty(maKho))
                    {
                        IXLCell xLCell_ = item.Cell(headers[HEADER[0]]);
                        xLCell_.Style.Fill.BackgroundColor = XLColor.Red;
                        xLCell_.MergedRange();
                        xLCell_.Comment.AddText("Mã loại tiền không hợp lệ");
                        lstErrR.Add(xLCell_);
                    }
                    else
                    {
                        if (!_lstRepositories.Any(n => n.RepositoryCode == maKho))
                        {
                            IXLCell xLCell_ = item.Cell(headers[HEADER[0]]);
                            xLCell_.Style.Fill.BackgroundColor = XLColor.Red;
                            xLCell_.MergedRange();
                            xLCell_.Comment.AddText("Mã kho không hợp lệ");
                            lstErrR.Add(xLCell_);
                        }
                        else
                        {
                            idKho = _lstRepositories.FirstOrDefault(n => n.RepositoryCode == maKho).ID;
                        }
                    }
                    if (!Utils.ListMaterialGoods.Any(n => n.MaterialGoodsCode == maVTHH))
                    {
                        IXLCell xLCell_ = item.Cell(headers[HEADER[1]]);
                        xLCell_.Style.Fill.BackgroundColor = XLColor.Red;
                        xLCell_.MergedRange();
                        xLCell_.Comment.AddText("Mã VTHH không hợp lệ");
                        lstErrR.Add(xLCell_);
                    }
                    else
                    {
                        doituong = Utils.ListMaterialGoods.FirstOrDefault(n => n.MaterialGoodsCode == maVTHH);
                        idDoiTuong = doituong.ID;
                    }

                    if (!string.IsNullOrEmpty(taiKhoanNganHang))
                        if (!Utils.ListBankAccountDetail.Any(n => n.BankAccount == taiKhoanNganHang))
                        {
                            IXLCell xLCell_ = item.Cell(headers[HEADER[9]]);
                            xLCell_.Style.Fill.BackgroundColor = XLColor.Red;
                            xLCell_.MergedRange();
                            xLCell_.Comment.AddText("Tai khoản ngân hàng không hợp lệ");
                            lstErrR.Add(xLCell_);
                        }
                        else
                        {
                            idTaiKhoanNH = Utils.ListBankAccountDetail.FirstOrDefault(n => n.BankAccount == taiKhoanNganHang).ID;
                        }
                    if (!string.IsNullOrEmpty(maDoiTuongTHCP))
                        if (!Utils.ListCostSet.Any(n => n.CostSetCode == maDoiTuongTHCP))
                        {
                            IXLCell xLCell_ = item.Cell(headers[HEADER[7]]);
                            xLCell_.Style.Fill.BackgroundColor = XLColor.Red;
                            xLCell_.MergedRange();
                            xLCell_.Comment.AddText("Mã đối tượng tập hợp chi phí không hợp lệ");
                            lstErrR.Add(xLCell_);
                        }
                        else
                        {
                            idDoiTuongTPCH = Utils.ListCostSet.FirstOrDefault(n => n.CostSetCode == maDoiTuongTHCP).ID;
                        }
                    if (!string.IsNullOrEmpty(hopDong))
                        if (!Utils.ListEmContract.Any(n => n.Code == hopDong))
                        {
                            IXLCell xLCell_ = item.Cell(headers[HEADER[8]]);
                            xLCell_.Style.Fill.BackgroundColor = XLColor.Red;
                            xLCell_.MergedRange();
                            xLCell_.Comment.AddText("Mã hợp đồng không hợp lệ");
                            lstErrR.Add(xLCell_);
                        }
                        else
                        {
                            idHopDong = Utils.ListEmContract.FirstOrDefault(n => n.Code == hopDong).ID;
                        }
                    if (!string.IsNullOrEmpty(khoanMucCP))
                        if (!Utils.ListExpenseItem.Any(n => n.ExpenseItemCode == khoanMucCP))
                        {
                            IXLCell xLCell_ = item.Cell(headers[HEADER[10]]);
                            xLCell_.Style.Fill.BackgroundColor = XLColor.Red;
                            xLCell_.MergedRange();
                            xLCell_.Comment.AddText("Mã hợp đồng không hợp lệ");
                            lstErrR.Add(xLCell_);
                        }
                        else
                        {
                            idKhoanMucCP = Utils.ListExpenseItem.FirstOrDefault(n => n.ExpenseItemCode == khoanMucCP).ID;
                        }
                }
                catch (Exception ex)
                {

                }
                oPMaterialGoodsByOrther.CurrencyID = "VND";
                oPMaterialGoodsByOrther.RepositoryCode = maKho;
                oPMaterialGoodsByOrther.RepositoryID = idKho;
                oPMaterialGoodsByOrther.MaterialGoodsID = idDoiTuong;
                oPMaterialGoodsByOrther.MaterialGoodsName = doituong.MaterialGoodsName;
                oPMaterialGoodsByOrther.MaterialGoodsCode = maVTHH;
                oPMaterialGoodsByOrther.BankAccountDetailID = idTaiKhoanNH;
                //opAccountObjectByOrther.BranchID = opAccount.BranchID;
                oPMaterialGoodsByOrther.ContractID = idHopDong;
                oPMaterialGoodsByOrther.CostSetID = idDoiTuongTPCH;
                oPMaterialGoodsByOrther.ExpenseItemID = idKhoanMucCP;
                //opAccountObjectByOrther.OrderPriority = opAccount.OrderPriority;
                oPMaterialGoodsByOrther.UnitPrice = donGia;
                oPMaterialGoodsByOrther.Quantity = soluong;
                oPMaterialGoodsByOrther.Amount = thanhTien;
                oPMaterialGoodsByOrther.LotNo = soLo;
                oPMaterialGoodsByOrther.ExpiryDate = hanDung;
                oPMaterialGoodsByOrther.ID = Guid.NewGuid();

                lst.Add(oPMaterialGoodsByOrther);
            }
            if (lstErrR.Count() > 0)
            {
                WaitingFrm.StopWaiting();
                if (MSG.Question("Dữ liệu file không đúng. Bạn có muốn tải file lỗi về không?") == System.Windows.Forms.DialogResult.Yes)
                {
                    System.Windows.Forms.SaveFileDialog sf = new System.Windows.Forms.SaveFileDialog
                    {
                        FileName = "Import_VTHH_dauky_Error.xlsx",
                        AddExtension = true,
                        Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
                    };
                    sf.Title = "Chọn thư mục lưu file";
                    if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        //XLWorkbook xLWorkbook = new XLWorkbook();
                        //xLWorkbook.AddWorksheet(ws);
                        try
                        {
                            wb.SaveAs(sf.FileName);
                            //xLWorkbook.SaveAs(sf.FileName);
                            //xLWorkbook.Dispose();
                            //Exit();
                            if (MSG.Question("Tải xuống thành công, Bạn có muốn mở file vừa tải") == System.Windows.Forms.DialogResult.Yes)
                            {
                                try
                                {
                                    System.Diagnostics.Process.Start(sf.FileName);
                                }
                                catch
                                {
                                }
                            }
                        }
                        catch
                        {
                            MSG.Error("Lỗi khi lưu");
                            //xLWorkbook.Dispose();
                            //Exit();
                        }
                    }
                    return new List<OPMaterialGoodsByOrther>();
                }
                else
                {
                    //Exit();
                    return new List<OPMaterialGoodsByOrther>();
                }
            }
            return lst;
        }

        private void btnexportExcel_Click(object sender, EventArgs e)
        {
            //uGrid.DisplayLayout.Bands[0].Columns["ExchangeRate"].Hidden = false;
            //uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOriginal"].Hidden = false;
            //uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOriginal"].Hidden = false;
            uGrid.DisplayLayout.Bands[0].Columns["CurrencyID"].Hidden = false;
            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].Hidden = false;
            uGrid.DisplayLayout.Bands[0].Columns["RepositoryCode"].Hidden = false;
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu cơ sở dữ liệu",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "So_du_dau_ky_theo_tai_khoan"
            };
            if (save.ShowDialog(this) == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    this.ultraGridExcelExporter1.Export(uGrid, duongdan);
                    MSG.Information("Xuất ra file Excel  thành công !");
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
            if (uCbbRepository.SelectedRow != null)
            {
                Repository repository = (Repository)uCbbRepository.SelectedRow.ListObject;
                if (_dictionary.ContainsKey(repository.ID))
                    ConfigGrid(_dictionary[repository.ID], true, false);
            }
        }

        private void FOPMaterialGoodDetail_FormClosed_1(object sender, FormClosedEventArgs e)
        {

        }

        private void uGrid_KeyDown(object sender, KeyEventArgs e)
        {
            UltraGrid uGrid = (UltraGrid)sender;
            if (uGrid.ActiveCell != null)
            {
                if (uGrid.ActiveCell.ValueListResolved != null)
                    if (uGrid.ActiveCell.IsInEditMode)
                    {
                        if (e.KeyCode == Keys.Up)
                        {
                            if (uGrid.ActiveCell.IsInEditMode && (uGrid.ActiveCell.ValueListResolved != null || uGrid.ActiveCell.EditorComponentResolved != null))
                            {
                                if (!uGrid.ActiveCell.DroppedDown) uGrid.ActiveCell.DroppedDown = true;
                            }
                            else if (uGrid.ActiveCell.ValueListResolved == null)
                            {
                                uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                                uGrid.PerformAction(UltraGridAction.AboveCell, false, false);
                                e.Handled = true;
                                uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                            }
                        }
                        if (e.KeyCode == Keys.Down)
                        {
                            if (uGrid.ActiveCell.IsInEditMode && (uGrid.ActiveCell.ValueListResolved != null || uGrid.ActiveCell.EditorComponentResolved != null))
                            {
                                if (!uGrid.ActiveCell.DroppedDown) uGrid.ActiveCell.DroppedDown = true;
                            }
                            else if (uGrid.ActiveCell.ValueListResolved == null)
                            {
                                uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                                uGrid.PerformAction(UltraGridAction.BelowCell, false, false);
                                e.Handled = true;
                                uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                            }
                        }
                        if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Right)
                        {
                            uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                            uGrid.PerformAction(UltraGridAction.NextCellByTab, false, false);
                            e.Handled = true;
                            uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                        }
                        if (e.KeyCode == Keys.Left)
                        {
                            uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                            uGrid.PerformAction(UltraGridAction.PrevCellByTab, false, false);
                            e.Handled = true;
                            uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                        }
                    }
                    else
                    {
                        switch (e.KeyCode)
                        {
                            case Keys.Enter:
                                uGrid.ActiveCell.Selected = true;
                                uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                                break;
                            case Keys.Down:
                                if (uGrid.ActiveCell.IsInEditMode && (uGrid.ActiveCell.ValueListResolved != null || uGrid.ActiveCell.EditorComponentResolved != null))
                                {
                                    if (!uGrid.ActiveCell.DroppedDown) uGrid.ActiveCell.DroppedDown = true;
                                }
                                else if (uGrid.ActiveCell.ValueListResolved == null)
                                {
                                    uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                                    uGrid.PerformAction(UltraGridAction.BelowCell, false, false);
                                    e.Handled = true;
                                    uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                                }
                                break;
                        }
                    }
            }
            if (uGrid.ActiveCell != null && uGrid.ActiveCell.Column.ValueList != null && uGrid.ActiveCell.Column.AutoCompleteMode == Infragistics.Win.AutoCompleteMode.None)
            {
                if (((e.KeyCode >= Keys.A && e.KeyCode <= Keys.Z)
                     || (e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9)
                     || (e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.Divide)) || e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    var combo = (UltraCombo)uGrid.ActiveCell.Column.ValueList;
                    if (!uGrid.ActiveCell.DroppedDown) uGrid.PerformAction(UltraGridAction.ToggleDropdown);
                }
            }
        }
    }
}