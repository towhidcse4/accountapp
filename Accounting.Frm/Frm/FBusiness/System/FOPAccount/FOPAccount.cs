﻿#region

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain.obj;
using Accounting.Core.Domain.obj.AllOPAccount;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraMessageBox;
using Infragistics.Win.UltraWinGrid;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

#endregion

namespace Accounting
{
    public partial class FOPAccount : CustormForm
    {
        #region Khai báo
        readonly List<string> _dsAccObject = new List<string> { "0", "1", "2","11" };
        public IOPAccountService OpAccountService
        {
            get { return IoC.Resolve<IOPAccountService>(); }
        }
        private List<AllOpAccount> _lstAllOpAccount = new List<AllOpAccount>();
        private List<OpAccountDetailByOrther> _lstOPAccountDetailByOrthers = new List<OpAccountDetailByOrther>();
        private List<OpAccountObjectByOrther> _lstOPAccountObjectByOrthers = new List<OpAccountObjectByOrther>();
        private List<OPMaterialGoodsByOrther> _lstOPMaterialGoodsByOrthers = new List<OPMaterialGoodsByOrther>();
        readonly List<string> _dsAccDetail = new List<string>() { "-1", "4", "6", "7", "8", "9" };
        #endregion

        #region Sự kiện
        public FOPAccount()
        {
            InitializeComponent();


            uGridAccount.Visible = false;
            uGridMaterialGood.Visible = false;
            uGridObject.Visible = false;

            uGridAccount.DataSource = _lstOPAccountDetailByOrthers;
            uGridMaterialGood.DataSource = _lstOPMaterialGoodsByOrthers;
            uGridObject.DataSource = _lstOPAccountObjectByOrthers;

            Utils.FormatNumberic(uGridAccount.DisplayLayout.Bands[0].Columns["ExchangeRate"], ConstDatabase.Format_Rate);
            Utils.FormatNumberic(uGridAccount.DisplayLayout.Bands[0].Columns["DebitAmount"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGridAccount.DisplayLayout.Bands[0].Columns["CreditAmount"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGridAccount.DisplayLayout.Bands[0].Columns["DebitAmountOriginal"], ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(uGridAccount.DisplayLayout.Bands[0].Columns["CreditAmountOriginal"], ConstDatabase.Format_ForeignCurrency);
            Utils.ConfigGrid(uGridAccount, ConstDatabase.OPAccountDetail_TableName);


            Utils.ConfigGrid(uGridMaterialGood, ConstDatabase.OPMaterialGoods_TableName);
            Utils.FormatNumberic(uGridMaterialGood.DisplayLayout.Bands[0].Columns["Quantity"], ConstDatabase.Format_Quantity);
            Utils.FormatNumberic(uGridMaterialGood.DisplayLayout.Bands[0].Columns["UnitPrice"], ConstDatabase.Format_DonGiaQuyDoi);
            Utils.FormatNumberic(uGridMaterialGood.DisplayLayout.Bands[0].Columns["Amount"], ConstDatabase.Format_TienVND);
            OPNUntil.AddSumChoCot(uGridMaterialGood, "Amount");

            Utils.FormatNumberic(uGridObject.DisplayLayout.Bands[0].Columns["DebitAmount"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGridObject.DisplayLayout.Bands[0].Columns["CreditAmount"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGridObject.DisplayLayout.Bands[0].Columns["DebitAmountOriginal"], ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(uGridObject.DisplayLayout.Bands[0].Columns["CreditAmountOriginal"], ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(uGridObject.DisplayLayout.Bands[0].Columns["ExchangeRate"], ConstDatabase.Format_Rate);
            Utils.ConfigGrid(uGridObject, ConstDatabase.OPAccountObject_TableName);

            uGridAccount.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGridMaterialGood.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGridObject.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            foreach (UltraGridColumn column in uGridMaterialGood.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid<OpAccountDetailByOrther>(10000, column, uGridMaterialGood);
                column.CellActivation = Activation.NoEdit;
            }
            foreach (UltraGridColumn column in uGridObject.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid<OpAccountDetailByOrther>(10000, column, uGridObject);
                column.CellActivation = Activation.NoEdit;
            }

            _lstOPAccountDetailByOrthers = IoC.Resolve<IOPAccountService>().GetListOpAccountDetailByOrthers_NotDefaultIfEmpty();

            _lstOPMaterialGoodsByOrthers = IoC.Resolve<IOPMaterialGoodsService>().GetListOPMaterialGoodsByOrther_NotDefaultIfEmpty();

            _lstOPAccountObjectByOrthers = IoC.Resolve<IOPAccountService>().GetListOpAccountObjectByOrthers_NotDefaultIfEmpty();

            LoadDl();
        }
        private void uGrid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            #region
            AllOpAccount opAccount = new AllOpAccount();
            uGridAccount.Visible = false;
            uGridMaterialGood.Visible = false;
            uGridObject.Visible = false;
            try
            {
                if (uGrid.Selected.Rows == null) return;
                if (uGrid.Selected.Rows.Count == 0) return;
                opAccount = (AllOpAccount)uGrid.Selected.Rows[0].ListObject;
                if (opAccount != null)
                {
                    if (_lstAllOpAccount.All(c => c.ParentID != opAccount.ID))
                    {
                        List<string> dsDetailType = opAccount.DetailType.Split(';').ToList();
                        if (_dsAccObject.Any(c => dsDetailType.Any(d => d == c)))
                        {
                            uGridObject.Visible = true;
                            var a = _lstOPAccountObjectByOrthers.Where(c => c.AccountNumber == opAccount.AccountNumber).OrderBy(c => c.CurrencyID).ThenBy(c => c.AccountingObjectCode).ToList();
                            uGridObject.DataSource = a.ToList();
                            var d = (from c in a
                                     group c by new { c.CurrencyID }
                                         into g
                                     select g.Key.CurrencyID).ToList();
                            List<string> lst = new List<string>() { "CurrencyID", "ExchangeRate", "DebitAmountOriginal", "CreditAmountOriginal" };
                            foreach (var item in uGridObject.DisplayLayout.Bands[0].Columns)
                            {
                                if (lst.Any(c => c == item.Key))
                                {
                                    item.Hidden = d.Count <= 1;
                                }
                            }
                            uGridObject.DisplayLayout.Bands[0].Columns["AccountNumber"].Hidden = true;
                        }
                        else if (opAccount.DetailType.Contains("5"))
                        {
                            uGridMaterialGood.Visible = true;
                            uGridMaterialGood.DataSource =
                                _lstOPMaterialGoodsByOrthers.Where(c => c.AccountNumber == opAccount.AccountNumber).OrderBy(c => c.RepositoryCode).ThenBy(c => c.MaterialGoodsCode).ToList();
                        }
                        else if (_dsAccDetail.Any(c => dsDetailType.Any(d => d == c)))
                        {
                            uGridAccount.Visible = true;
                            var a = _lstOPAccountDetailByOrthers.Where(c => c.AccountNumber == opAccount.AccountNumber).ToList();
                            uGridAccount.DataSource = a.ToList();
                            var d = (from c in a
                                     group c by new { c.CurrencyID }
                                         into g
                                     select g.Key.CurrencyID).ToList();
                            if (opAccount.AccountNumber == "007")
                            {
                                List<string> lst = new List<string>() { "AccountNumber", "AccountName", "CurrencyID", "CreditAmountOriginal", "DebitAmountOriginal" };
                                foreach (var item in uGridAccount.DisplayLayout.Bands[0].Columns)
                                {
                                    if (lst.Any(c => c == item.Key))
                                    {
                                        item.Hidden = false;
                                    }
                                    else item.Hidden = true;
                                }
                            }
                            else if (d.Count > 1 || dsDetailType.Contains("8"))
                            {
                                List<string> lst = new List<string>() { "AccountNumber", "AccountName", "ExchangeRate", "CurrencyID", "CreditAmount",
                                    "CreditAmountOriginal", "DebitAmount","DebitAmountOriginal" };
                                foreach (var item in uGridAccount.DisplayLayout.Bands[0].Columns)
                                {
                                    if (lst.Any(c => c == item.Key))
                                    {
                                        item.Hidden = false;
                                    }
                                    else item.Hidden = true;
                                }
                            }
                            else if (d.Count == 0 || d.Count == 1)
                            {
                                List<string> lst = null;
                                if (opAccount.AccountGroupID == "214" || opAccount.AccountGroupID == "211")
                                {
                                    lst = new List<string>() { "AccountNumber", "AccountName", "CreditAmount", "DebitAmount" };
                                }
                                else
                                {
                                    lst = new List<string>() { "AccountNumber", "AccountName", "CreditAmountOriginal", "DebitAmountOriginal" };
                                }
                                foreach (var item in uGridAccount.DisplayLayout.Bands[0].Columns)
                                {
                                    if (lst.Any(c => c == item.Key))
                                    {
                                        item.Hidden = false;
                                    }
                                    else item.Hidden = true;
                                }
                            }
                        }
                        if (opAccount.AccountNumber.StartsWith("112"))
                        {
                            List<string> lst = new List<string>() { "BankAccount", "BankName" };
                            foreach (var item in uGridAccount.DisplayLayout.Bands[0].Columns)
                            {
                                if (lst.Any(c => c == item.Key))
                                {
                                    item.Hidden = false;
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception)
            {

            }
            #endregion
            
        }
        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                AllOpAccount allOpAccount = (AllOpAccount)uGrid.Selected.Rows[0].ListObject;
                if (_lstAllOpAccount.All(c => c.ParentID != allOpAccount.ID))
                {

                    List<string> dsDetailType = allOpAccount.DetailType.Split(';').ToList();
                    if (_dsAccObject.Any(dsDetailType.Contains))
                    {
                        FOPAccountObject frm = new FOPAccountObject(allOpAccount)
                        {
                            WindowState = FormWindowState.Maximized,
                            Text = "Nhập số dư cho các tài khoản chi tiết theo đối tượng"
                        };
                        frm.ShowDialog(this);
                        if (!frm.CloseOf) LoadDl(3);
                        return;
                    }
                    else if (dsDetailType.Contains("5"))
                    {
                        FOPMaterialGoodDetail frm = new FOPMaterialGoodDetail(allOpAccount)//, (List<OPMaterialGoodsByOrther>)uGridMaterialGood.DataSource
                        {
                            WindowState = FormWindowState.Maximized,
                            Text = "Nhập số dư cho tài khoản vật tư hàng hóa"
                        };
                        frm.ShowDialog(this);
                        if (!frm.CloseOf) LoadDl(2);
                        return;
                    }
                    else if (_dsAccDetail.Any(dsDetailType.Contains))
                    {
                        FOPAccountDetail frm = new FOPAccountDetail(allOpAccount)
                        {
                            WindowState = FormWindowState.Maximized,
                            Text = "Nhập số dư cho tài khoản"
                        };
                        frm.ShowDialog(this);
                        if (!frm.CloseOf) LoadDl(1);
                        return;
                    }
                }
                else
                {
                    MSG.Warning("Không được chọn tài khoản cấp 1. Vui lòng chọn vào tài khoản cấp 2 của tài khoản trên!");
                }
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }
        #endregion

        #region Hàm sử lý
        private void LoadDl(int ganLaiChoGrid = 0)
        {
            uGridAccount.Visible = false;
            uGridMaterialGood.Visible = false;
            uGridObject.Visible = false;
            _lstAllOpAccount = OpAccountService.GetAllOpAccounts(Utils.StringToDateTime(ConstFrm.DbStartDate) ?? DateTime.Now).OrderBy(c => c.AccountNumber).ToList();
            ConfigGrid(uGrid, _lstAllOpAccount);
            if (ganLaiChoGrid == 1)
            {
                _lstOPAccountDetailByOrthers = IoC.Resolve<IOPAccountService>().GetListOpAccountDetailByOrthers_NotDefaultIfEmpty();
                uGridAccount.DataSource = _lstOPAccountDetailByOrthers;
            }
            else if (ganLaiChoGrid == 2)
            {
                _lstOPMaterialGoodsByOrthers = IoC.Resolve<IOPMaterialGoodsService>().GetListOPMaterialGoodsByOrther_NotDefaultIfEmpty();
                uGridMaterialGood.DataSource = _lstOPMaterialGoodsByOrthers;
            }
            else if (ganLaiChoGrid == 3)
            {
                _lstOPAccountObjectByOrthers = IoC.Resolve<IOPAccountService>().GetListOpAccountObjectByOrthers_NotDefaultIfEmpty();
                uGridObject.DataSource = _lstOPAccountObjectByOrthers;
                
            }
            if (_lstAllOpAccount.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
                uGrid.Rows[0].Activate();
            }
        }
        private void ConfigGrid(UltraGrid ultraGrid, List<AllOpAccount> lstAllOpAccounts)
        {
            decimal tongDuNo = 0;
            decimal tongDuCo = 0;

            //var d = from i in lstAllOpAccounts
            //        where i.ParentID != null
            //        orderby i.ParentID descending
            //        group i by i.ParentID
            //            into g
            //            select new
            //            {
            //                g.Key,
            //                g
            //            }
            //        ;
            //foreach (var t in d)
            //{
            //    foreach (AllOpAccount allOpAccount in lstAllOpAccounts)
            //    {
            //        if (allOpAccount.ID == t.g.Key)
            //        {
            //            allOpAccount.SumCreditAmount = t.g.Sum(c => c.SumCreditAmount);
            //            allOpAccount.SumDebitAmount = t.g.Sum(c => c.SumDebitAmount);
            //            //tongDuNo += t.g.Sum(c => c.SumDebitAmount);
            //            //tongDuCo += t.g.Sum(c => c.SumCreditAmount);
            //        }
            //    }
            //}
            lstAllOpAccounts = lstAllOpAccounts.OrderByDescending(x => x.Grade).ToList();
            foreach (AllOpAccount allOpAccount in lstAllOpAccounts)
            {
                allOpAccount.SumCreditAmount = lstAllOpAccounts.Count(x => x.ParentID == allOpAccount.ID) != 0 ? allOpAccount.SumCreditAmount + lstAllOpAccounts.Where(x => x.ParentID == allOpAccount.ID).Sum(t => t.SumCreditAmount) : allOpAccount.SumCreditAmount;
                allOpAccount.SumDebitAmount = lstAllOpAccounts.Count(x => x.ParentID == allOpAccount.ID) != 0 ? allOpAccount.SumDebitAmount + lstAllOpAccounts.Where(x => x.ParentID == allOpAccount.ID).Sum(t => t.SumDebitAmount) : allOpAccount.SumDebitAmount;
            }
            lstAllOpAccounts = lstAllOpAccounts.OrderBy(x => x.AccountNumber).ToList();
            ultraGrid.SetDataBinding(lstAllOpAccounts, "");

            Utils.ConfigGrid(ultraGrid, ConstDatabase.AllOpAccount_TableName);
            foreach (UltraGridRow rowaccount in ultraGrid.Rows)
            {
                AllOpAccount all = (AllOpAccount)rowaccount.ListObject;
                if (lstAllOpAccounts.Any(c => c.ParentID == all.ID))
                {
                    rowaccount.CellAppearance.FontData.Bold = DefaultableBoolean.True;
                }
                else
                {
                    rowaccount.CellAppearance.ForeColor = Color.FromArgb(0, 146, 242);
                    //tongDuNo += all.SumDebitAmount;
                    //tongDuCo += all.SumCreditAmount;
                }
            }

            tongDuCo = lstAllOpAccounts.Where(c => c.ParentID == null).Sum(c => c.SumCreditAmount);
            tongDuNo = lstAllOpAccounts.Where(c => c.ParentID == null).Sum(c => c.SumDebitAmount);

            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["SumDebitAmount"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["SumCreditAmount"], ConstDatabase.Format_TienVND);
            ultraGrid.DisplayLayout.Bands[0].Summaries.Clear();

            SummarySettings summary = ultraGrid.DisplayLayout.Bands[0].Summaries.Add("SumSumDebitAmount", SummaryType.Sum, ultraGrid.DisplayLayout.Bands[0].Columns["SumDebitAmount"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            TextBox txTextBox = new TextBox();
            Utils.FormatNumberic(txTextBox, tongDuNo, ConstDatabase.Format_TienVND);
            txTextBox.Text = "Tổng dư nợ = " + txTextBox.Text;
            summary.DisplayFormat = txTextBox.Text;
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            summary.Appearance.TextHAlign = HAlign.Right;

            SummarySettings summary1 = ultraGrid.DisplayLayout.Bands[0].Summaries.Add("SumSumCreditAmount", SummaryType.Sum, ultraGrid.DisplayLayout.Bands[0].Columns["SumCreditAmount"]);
            summary1.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            TextBox txTextBox1 = new TextBox();
            Utils.FormatNumberic(txTextBox1, tongDuCo, ConstDatabase.Format_TienVND);
            txTextBox1.Text = "Tổng dư có = " + txTextBox1.Text;
            summary1.DisplayFormat = txTextBox1.Text;
            summary1.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            summary1.Appearance.TextHAlign = HAlign.Right;

            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            ultraGrid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
            ultraGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;


        }
        #endregion

        private void FOPAccount_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();

            uGridAccount.ResetText();
            uGridAccount.ResetUpdateMode();
            uGridAccount.ResetExitEditModeOnLeave();
            uGridAccount.ResetRowUpdateCancelAction();
            uGridAccount.DataSource = null;
            uGridAccount.Layouts.Clear();
            uGridAccount.ResetLayouts();
            uGridAccount.ResetDisplayLayout();
            uGridAccount.Refresh();
            uGridAccount.ClearUndoHistory();
            uGridAccount.ClearXsdConstraints();

            uGridMaterialGood.ResetText();
            uGridMaterialGood.ResetUpdateMode();
            uGridMaterialGood.ResetExitEditModeOnLeave();
            uGridMaterialGood.ResetRowUpdateCancelAction();
            uGridMaterialGood.DataSource = null;
            uGridMaterialGood.Layouts.Clear();
            uGridMaterialGood.ResetLayouts();
            uGridMaterialGood.ResetDisplayLayout();
            uGridMaterialGood.Refresh();
            uGridMaterialGood.ClearUndoHistory();
            uGridMaterialGood.ClearXsdConstraints();

            uGridObject.ResetText();
            uGridObject.ResetUpdateMode();
            uGridObject.ResetExitEditModeOnLeave();
            uGridObject.ResetRowUpdateCancelAction();
            uGridObject.DataSource = null;
            uGridObject.Layouts.Clear();
            uGridObject.ResetLayouts();
            uGridObject.ResetDisplayLayout();
            uGridObject.Refresh();
            uGridObject.ClearUndoHistory();
            uGridObject.ClearXsdConstraints();
        }

        private void FOPAccount_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}