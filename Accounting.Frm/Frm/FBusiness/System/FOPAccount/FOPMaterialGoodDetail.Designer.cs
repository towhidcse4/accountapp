﻿namespace Accounting
{
    partial class FOPMaterialGoodDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FOPMaterialGoodDetail));
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uCbbRepository = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.linkFileMau = new System.Windows.Forms.LinkLabel();
            this.btnImportExcel = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnexportExcel = new Infragistics.Win.Misc.UltraButton();
            this.bt_dong = new Infragistics.Win.Misc.UltraButton();
            this.bt_luu = new Infragistics.Win.Misc.UltraButton();
            this.ultraGridExcelExporter1 = new Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCbbRepository)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(6, 12);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(56, 23);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Kho";
            // 
            // ultraGroupBox4
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox4.Appearance = appearance1;
            this.ultraGroupBox4.Controls.Add(this.uCbbRepository);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            appearance2.FontData.BoldAsString = "True";
            appearance2.FontData.SizeInPoints = 13F;
            this.ultraGroupBox4.HeaderAppearance = appearance2;
            this.ultraGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(579, 36);
            this.ultraGroupBox4.TabIndex = 47;
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // uCbbRepository
            // 
            this.uCbbRepository.Location = new System.Drawing.Point(63, 8);
            this.uCbbRepository.Name = "uCbbRepository";
            this.uCbbRepository.Size = new System.Drawing.Size(233, 22);
            this.uCbbRepository.TabIndex = 1;
            this.uCbbRepository.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.uCbbRepository_ItemNotInList);
            // 
            // linkFileMau
            // 
            this.linkFileMau.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linkFileMau.AutoSize = true;
            this.linkFileMau.Location = new System.Drawing.Point(144, 11);
            this.linkFileMau.Name = "linkFileMau";
            this.linkFileMau.Size = new System.Drawing.Size(46, 13);
            this.linkFileMau.TabIndex = 9;
            this.linkFileMau.TabStop = true;
            this.linkFileMau.Text = "File mẫu";
            this.linkFileMau.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkFileMau_LinkClicked);
            // 
            // btnImportExcel
            // 
            this.btnImportExcel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance3.Image = global::Accounting.Properties.Resources.Excel;
            this.btnImportExcel.Appearance = appearance3;
            this.btnImportExcel.Location = new System.Drawing.Point(196, 7);
            this.btnImportExcel.Name = "btnImportExcel";
            this.btnImportExcel.Size = new System.Drawing.Size(110, 23);
            this.btnImportExcel.TabIndex = 2;
            this.btnImportExcel.Text = "Nhập từ Excel";
            this.btnImportExcel.Click += new System.EventHandler(this.btnImportExcel_Click);
            // 
            // ultraGroupBox1
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox1.Appearance = appearance4;
            this.ultraGroupBox1.Controls.Add(this.uGrid);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance5.FontData.BoldAsString = "True";
            appearance5.FontData.SizeInPoints = 13F;
            this.ultraGroupBox1.HeaderAppearance = appearance5;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 36);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(579, 294);
            this.ultraGroupBox1.TabIndex = 48;
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // uGrid
            // 
            this.uGrid.DisplayLayout.RowSelectorImages.ActiveAndAddNewRowImage = global::Accounting.Properties.Resources.new_file_icon;
            this.uGrid.DisplayLayout.RowSelectorImages.ActiveAndDataChangedImage = global::Accounting.Properties.Resources.ubtnTemplate;
            this.uGrid.DisplayLayout.RowSelectorImages.ActiveRowImage = global::Accounting.Properties.Resources.ubtnForward;
            this.uGrid.DisplayLayout.RowSelectorImages.AddNewRowImage = global::Accounting.Properties.Resources.iAdd;
            this.uGrid.DisplayLayout.RowSelectorImages.DataChangedImage = global::Accounting.Properties.Resources.edit_validated_icon;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(3, 0);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(573, 291);
            this.uGrid.TabIndex = 0;
            this.uGrid.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_AfterCellUpdate);
            this.uGrid.AfterExitEditMode += new System.EventHandler(this.uGrid_AfterExitEditMode);
            this.uGrid.CellDataError += new Infragistics.Win.UltraWinGrid.CellDataErrorEventHandler(this.uGrid_CellDataError);
            this.uGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGrid_KeyDown);
            // 
            // ultraGroupBox2
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox2.Appearance = appearance6;
            this.ultraGroupBox2.Controls.Add(this.linkFileMau);
            this.ultraGroupBox2.Controls.Add(this.btnexportExcel);
            this.ultraGroupBox2.Controls.Add(this.btnImportExcel);
            this.ultraGroupBox2.Controls.Add(this.bt_dong);
            this.ultraGroupBox2.Controls.Add(this.bt_luu);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            appearance10.FontData.BoldAsString = "True";
            appearance10.FontData.SizeInPoints = 13F;
            this.ultraGroupBox2.HeaderAppearance = appearance10;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 330);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(579, 36);
            this.ultraGroupBox2.TabIndex = 49;
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnexportExcel
            // 
            this.btnexportExcel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.Image = global::Accounting.Properties.Resources.Excel;
            this.btnexportExcel.Appearance = appearance7;
            this.btnexportExcel.Location = new System.Drawing.Point(312, 7);
            this.btnexportExcel.Name = "btnexportExcel";
            this.btnexportExcel.Size = new System.Drawing.Size(99, 23);
            this.btnexportExcel.TabIndex = 5;
            this.btnexportExcel.Text = "Xuất Excel";
            this.btnexportExcel.Click += new System.EventHandler(this.btnexportExcel_Click);
            // 
            // bt_dong
            // 
            this.bt_dong.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.bt_dong.Appearance = appearance8;
            this.bt_dong.Location = new System.Drawing.Point(498, 6);
            this.bt_dong.Name = "bt_dong";
            this.bt_dong.Size = new System.Drawing.Size(75, 23);
            this.bt_dong.TabIndex = 2;
            this.bt_dong.Text = "Đóng";
            // 
            // bt_luu
            // 
            this.bt_luu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.bt_luu.Appearance = appearance9;
            this.bt_luu.Location = new System.Drawing.Point(417, 6);
            this.bt_luu.Name = "bt_luu";
            this.bt_luu.Size = new System.Drawing.Size(75, 23);
            this.bt_luu.TabIndex = 1;
            this.bt_luu.Text = "Lưu";
            // 
            // FOPMaterialGoodDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 366);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.ultraGroupBox4);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FOPMaterialGoodDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FOPMaterialGoodDetail";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FOPMaterialGoodDetail_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FOPMaterialGoodDetail_FormClosed_1);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            this.ultraGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCbbRepository)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.UltraWinGrid.UltraCombo uCbbRepository;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraButton bt_dong;
        private Infragistics.Win.Misc.UltraButton bt_luu;
        private Infragistics.Win.Misc.UltraButton btnImportExcel;
        private System.Windows.Forms.LinkLabel linkFileMau;
        private Infragistics.Win.Misc.UltraButton btnexportExcel;
        private Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter ultraGridExcelExporter1;
    }
}