﻿namespace Accounting
{
    partial class FOPAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FOPAccount));
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridMaterialGood = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridObject = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridAccount = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMaterialGood)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox2
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox2.Appearance = appearance1;
            this.ultraGroupBox2.ContentPadding.Top = 10;
            this.ultraGroupBox2.Controls.Add(this.uGridMaterialGood);
            this.ultraGroupBox2.Controls.Add(this.uGridObject);
            this.ultraGroupBox2.Controls.Add(this.uGridAccount);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance2.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox2.HeaderAppearance = appearance2;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 351);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(1059, 177);
            this.ultraGroupBox2.TabIndex = 9;
            this.ultraGroupBox2.Text = "Thông tin chi tiết";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // uGridMaterialGood
            // 
            this.uGridMaterialGood.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridMaterialGood.Location = new System.Drawing.Point(3, 26);
            this.uGridMaterialGood.Name = "uGridMaterialGood";
            this.uGridMaterialGood.Size = new System.Drawing.Size(1053, 148);
            this.uGridMaterialGood.TabIndex = 2;
            // 
            // uGridObject
            // 
            this.uGridObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridObject.Location = new System.Drawing.Point(3, 26);
            this.uGridObject.Name = "uGridObject";
            this.uGridObject.Size = new System.Drawing.Size(1053, 148);
            this.uGridObject.TabIndex = 1;
            // 
            // uGridAccount
            // 
            this.uGridAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridAccount.Location = new System.Drawing.Point(3, 26);
            this.uGridAccount.Name = "uGridAccount";
            this.uGridAccount.Size = new System.Drawing.Size(1053, 148);
            this.uGridAccount.TabIndex = 0;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.uGrid);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            appearance3.FontData.SizeInPoints = 8.25F;
            this.ultraGroupBox1.HeaderAppearance = appearance3;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1059, 351);
            this.ultraGroupBox1.TabIndex = 20;
            this.ultraGroupBox1.Text = "Số dư đầu kỳ của tài khoản";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // uGrid
            // 
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(3, 16);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(1053, 332);
            this.uGrid.TabIndex = 0;
            this.uGrid.Text = "Danh sách số dư đầu kỳ tương ứng tài khoản";
            this.uGrid.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.uGrid_AfterSelectChange);
            this.uGrid.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid_DoubleClickRow);
            // 
            // FOPAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1059, 528);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.ultraGroupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FOPAccount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FOPAccount_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FOPAccount_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridMaterialGood)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMaterialGood;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridObject;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridAccount;
    }
}