﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;

namespace Accounting
{
    public partial class FPSOtherVoucherDetailPopup : CustormForm
    {
        readonly List<PSSalarySheet> _dsPSSalarySheet;
        public GOtherVoucher GOtherVoucher;
        public FPSOtherVoucherDetailPopup()
        {
            //WaitingFrm.StartWaiting();
            InitializeComponent();
            _dsPSSalarySheet = Utils.IPSSalarySheetService.GetByGOtherVoucherIDIsNull();
            cbbSalarySheet.DataSource = _dsPSSalarySheet;
            cbbSalarySheet.DisplayMember = "PSSalarySheetName";
            Utils.ConfigGrid(cbbSalarySheet, ConstDatabase.PSSalarySheet_TableName);
            //WaitingFrm.StopWaiting();

        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            if(cbbSalarySheet.SelectedRow == null)
            {
                MSG.Warning("Bạn chưa chọn bảng lương để hạch toán!");
                return;
            }
            PSSalarySheet PSSalarySheet = (PSSalarySheet)cbbSalarySheet.SelectedRow.ListObject;

            GOtherVoucher = new GOtherVoucher
            {
                Reason = string.Format("Hạch toán chi phi lương tháng {0} năm {1}", PSSalarySheet.Month, PSSalarySheet.Year),
                PostedDate = (new DateTime(PSSalarySheet.Year, PSSalarySheet.Month, 1)).AddMonths(1).AddDays(-1),
                Date = (new DateTime(PSSalarySheet.Year, PSSalarySheet.Month, 1)).AddMonths(1).AddDays(-1),
                TypeID = 840,
                PSSalarySheetID = PSSalarySheet.ID,
                CurrencyID = "VND",
                ExchangeRate = 1
            };
            List<GOtherVoucherDetail> lst = new List<GOtherVoucherDetail>();
            List<Department> departments = Utils.ListDepartment.ToList();

            var groupByDepartment = PSSalarySheet.PSSalarySheetDetails
                                        .GroupBy(u => u.DepartmentID)
                                        .Select(grp => grp.ToList())
                                        .ToList();
            DateTime dateTime = new DateTime(PSSalarySheet.Year, PSSalarySheet.Month,1);
            PSSalaryTaxInsuranceRegulation stir = Utils.IPSSalaryTaxInsuranceRegulationService.FindByDate(dateTime);
            if (stir == null) stir = new PSSalaryTaxInsuranceRegulation();
            bool isHsl = Utils.ISystemOptionService.Query.Where(x => x.Code == "TL_TheoHeSoLuong").First().Data == "1";

            foreach (List<PSSalarySheetDetail> details in groupByDepartment)
            {
                Department department = departments.Where(x => x.ID == details[0].DepartmentID).FirstOrDefault();
                if (department == null) continue;
                #region Code cũ
                //GOtherVoucherDetail employee = new GOtherVoucherDetail()
                //{
                //    Description = "Tiền lương chính",
                //    DebitAccount = department.CostAccount,
                //    CreditAccount = "334",
                //    Amount = details.Sum(x => x.AgreementSalary),
                //    AmountOriginal = details.Sum(x => x.AgreementSalary),
                //    DepartmentID = department.ID,
                //    CurrencyID = "VND",
                //};
                //lst.Add(employee);
                #endregion
                if (PSSalarySheet.TypeID == 832)
                {
                    if (isHsl)
                    {
                        GOtherVoucherDetail employee = new GOtherVoucherDetail()
                        {
                            Description = "Tiền lương chính",
                            DebitAccount = department.CostAccount,
                            CreditAccount = "334",
                            //Amount = details.Sum(x => x.AgreementSalary),
                            //AmountOriginal = details.Sum(x => x.AgreementSalary),
                            DepartmentID = department.ID,
                            CurrencyID = "VND",
                        };
                        Decimal result=0;
                        foreach (var item in details)
                        {
                            result += item.SalaryCoefficient * item.BasicWage;
                        }
                        employee.Amount = result;
                        employee.AmountOriginal = result;
                        if (employee.AmountOriginal > 0)
                            lst.Add(employee);
                    }
                    else
                    {
                        GOtherVoucherDetail employee = new GOtherVoucherDetail()
                        {
                            Description = "Tiền lương chính",
                            DebitAccount = department.CostAccount,
                            CreditAccount = "334",
                            Amount = details.Sum(x => x.AgreementSalary),
                            AmountOriginal = details.Sum(x => x.AgreementSalary),
                            DepartmentID = department.ID,
                            CurrencyID = "VND",
                        };
                        if (employee.AmountOriginal > 0)
                            lst.Add(employee);
                    }
                }
                else
                {
                    GOtherVoucherDetail employee = new GOtherVoucherDetail()
                    {
                        Description = "Tiền lương chính",
                        DebitAccount = department.CostAccount,
                        CreditAccount = "334",
                        Amount = details.Sum(x => x.PaidWorkingDayAmount) + details.Sum(x => x.NonWorkingDayAmount),
                        AmountOriginal = details.Sum(x => x.PaidWorkingDayAmount) + details.Sum(x => x.NonWorkingDayAmount),
                        DepartmentID = department.ID,
                        CurrencyID = "VND",
                    };
                    if (employee.AmountOriginal > 0)
                        lst.Add(employee);
                }
                
                GOtherVoucherDetail employee1 = new GOtherVoucherDetail()
                {
                    Description = "Tiền lương làm thêm",
                    DebitAccount = department.CostAccount,
                    CreditAccount = "334",
                    Amount = details.Sum(x => x.OverTimeAmount),
                    AmountOriginal = details.Sum(x => x.OverTimeAmount),
                    DepartmentID = department.ID,
                    CurrencyID = "VND",
                };
                if(employee1.AmountOriginal > 0)
                    lst.Add(employee1);
                GOtherVoucherDetail phucap1 = new GOtherVoucherDetail()
                {
                    Description = "Phụ cấp thuộc quỹ lương",
                    DebitAccount = department.CostAccount,
                    CreditAccount = "334",
                    Amount = details.Sum(x => x.PayrollFundAllowance)??0,
                    AmountOriginal = details.Sum(x => x.PayrollFundAllowance)??0,
                    DepartmentID = department.ID,
                    CurrencyID = "VND",
                };
                if (phucap1.AmountOriginal > 0)
                    lst.Add(phucap1);
                GOtherVoucherDetail phucap2 = new GOtherVoucherDetail()
                {
                    Description = "Phụ cấp khác",
                    DebitAccount = department.CostAccount,
                    CreditAccount = "334",
                    Amount = details.Sum(x => x.OtherAllowance)??0,
                    AmountOriginal = details.Sum(x => x.OtherAllowance)??0,
                    DepartmentID = department.ID,
                    CurrencyID = "VND",
                };
                if (phucap2.AmountOriginal > 0)
                    lst.Add(phucap2);
                GOtherVoucherDetail phucap3 = new GOtherVoucherDetail()
                {
                    Description = "Phụ cấp khống tính thuế TNCN",
                    DebitAccount = department.CostAccount,
                    CreditAccount = "334",
                    Amount = details.Sum(x => x.NotIncomeTaxAllowance),
                    AmountOriginal = details.Sum(x => x.NotIncomeTaxAllowance),
                    DepartmentID = department.ID,
                    CurrencyID = "VND",
                };
                if (phucap3.AmountOriginal > 0)
                    lst.Add(phucap3);
                GOtherVoucherDetail eBhxh = new GOtherVoucherDetail()
                {
                    Description = "Bảo hiểm xã hội công ty đóng",
                    DebitAccount = department.CostAccount,
                    CreditAccount = "3383",
                    Amount = details.Sum(x => x.CompanySocityInsuranceAmount),
                    AmountOriginal = details.Sum(x => x.CompanySocityInsuranceAmount),
                    DepartmentID = department.ID,
                    CurrencyID = "VND",
                };
                if (eBhxh.AmountOriginal > 0)
                    lst.Add(eBhxh);
                GOtherVoucherDetail eBhtnld = new GOtherVoucherDetail()
                {
                    Description = "Bảo hiểm tai nạn lao động công ty đóng",
                    DebitAccount = department.CostAccount,
                    CreditAccount = "3383",
                    Amount = details.Sum(x => x.CompanytAccidentInsuranceAmount),
                    AmountOriginal = details.Sum(x => x.CompanytAccidentInsuranceAmount),
                    DepartmentID = department.ID,
                    CurrencyID = "VND",
                };
                if (eBhtnld.AmountOriginal > 0)
                    lst.Add(eBhtnld);
                GOtherVoucherDetail eBhyt = new GOtherVoucherDetail()
                {
                    Description = "Bảo hiểm y tế công ty đóng",
                    DebitAccount = department.CostAccount,
                    CreditAccount = "3384",
                    Amount = details.Sum(x => x.CompanyMedicalInsuranceAmount),
                    AmountOriginal = details.Sum(x => x.CompanyMedicalInsuranceAmount),
                    DepartmentID = department.ID,
                    CurrencyID = "VND",
                };
                if (eBhyt.AmountOriginal > 0)
                    lst.Add(eBhyt);
                GOtherVoucherDetail eBhtn = new GOtherVoucherDetail()
                {
                    Description = "Bảo hiểm thất nghiệp công ty đóng",
                    DebitAccount = department.CostAccount,
                    CreditAccount = "3385",
                    Amount = details.Sum(x => x.CompanyUnEmployeeInsuranceAmount),
                    AmountOriginal = details.Sum(x => x.CompanyUnEmployeeInsuranceAmount),
                    DepartmentID = department.ID,
                    CurrencyID = "VND",
                };
                if (eBhtn.AmountOriginal > 0)
                    lst.Add(eBhtn);
                GOtherVoucherDetail eKpcd = new GOtherVoucherDetail()
                {
                    Description = "Kinh phí công đoàn công ty đóng",
                    DebitAccount = department.CostAccount,
                    CreditAccount = "3382",
                    Amount = details.Sum(x => x.CompanyTradeUnionInsuranceAmount),
                    AmountOriginal = details.Sum(x => x.CompanyTradeUnionInsuranceAmount),
                    DepartmentID = department.ID,
                    CurrencyID = "VND",
                };
                if (eKpcd.AmountOriginal > 0)
                    lst.Add(eKpcd);
                GOtherVoucherDetail cBhxh = new GOtherVoucherDetail()
                {
                    Description = "Bảo hiểm xã hội nhân viên đóng",
                    DebitAccount = "334",
                    CreditAccount = "3383",
                    Amount = details.Sum(x => x.EmployeeSocityInsuranceAmount),
                    AmountOriginal = details.Sum(x => x.EmployeeSocityInsuranceAmount),
                    DepartmentID = department.ID,
                    CurrencyID = "VND",
                };
                if (cBhxh.AmountOriginal > 0)
                    lst.Add(cBhxh);
                GOtherVoucherDetail cBhtnld = new GOtherVoucherDetail()
                {
                    Description = "Bảo hiểm tai nạn lao động nhân viên đóng",
                    DebitAccount = "334",
                    CreditAccount = "3382",
                    Amount = details.Sum(x => x.EmployeeAccidentInsuranceAmount),
                    AmountOriginal = details.Sum(x => x.EmployeeAccidentInsuranceAmount),
                    DepartmentID = department.ID,
                    CurrencyID = "VND",
                };
                if (cBhtnld.AmountOriginal > 0)
                    lst.Add(cBhtnld);
                GOtherVoucherDetail cBhyt = new GOtherVoucherDetail()
                {
                    Description = "Bảo hiểm y tế nhân viên đóng",
                    DebitAccount = "334",
                    CreditAccount = "3384",
                    Amount = details.Sum(x => x.EmployeeMedicalInsuranceAmount),
                    AmountOriginal = details.Sum(x => x.EmployeeMedicalInsuranceAmount),
                    DepartmentID = department.ID,
                    CurrencyID = "VND",
                };
                if (cBhyt.AmountOriginal > 0)
                    lst.Add(cBhyt);
                GOtherVoucherDetail cBhtn = new GOtherVoucherDetail()
                {
                    Description = "Bảo hiểm thất nghiệp nhân viên đóng",
                    DebitAccount = "334",
                    CreditAccount = "3385",
                    Amount = details.Sum(x => x.EmployeeUnEmployeeInsuranceAmount),
                    AmountOriginal = details.Sum(x => x.EmployeeUnEmployeeInsuranceAmount),
                    DepartmentID = department.ID,
                    CurrencyID = "VND",
                };
                if (cBhtn.AmountOriginal > 0)
                    lst.Add(cBhtn);
                GOtherVoucherDetail cKpcd = new GOtherVoucherDetail()
                {
                    Description = "Kinh phí công đoàn nhân viên đóng",
                    DebitAccount = "334",
                    CreditAccount = "3382",
                    Amount = details.Sum(x => x.EmployeeTradeUnionInsuranceAmount),
                    AmountOriginal = details.Sum(x => x.EmployeeTradeUnionInsuranceAmount),
                    DepartmentID = department.ID,
                    CurrencyID = "VND",
                };
                if (cKpcd.AmountOriginal > 0)
                    lst.Add(cKpcd);
                GOtherVoucherDetail ttncn = new GOtherVoucherDetail()
                {
                    Description = "Tiền thuế thu nhập cá nhân phải nộp",
                    DebitAccount = "334",
                    CreditAccount = "3335",
                    Amount = details.Sum(x => x.IncomeTaxAmount),
                    AmountOriginal = details.Sum(x => x.IncomeTaxAmount),
                    DepartmentID = department.ID,
                    CurrencyID = "VND",
                };
                if (ttncn.AmountOriginal > 0)
                    lst.Add(ttncn);
            }
            GOtherVoucher.GOtherVoucherDetails = lst;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
