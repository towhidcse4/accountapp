﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Castle.Components.DictionaryAdapter;
using FX.Core;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;
using ColumnStyle = System.Windows.Forms.ColumnStyle;
using Type = System.Type;

namespace Accounting
{
    public partial class FPSOtherVoucherDetail : FPSOtherVoucherDetailBase
    {
        #region khai báo
        UltraGrid uGrid;
        UltraGrid ugrid2 = null;
        UltraGrid ugrid3 = null;
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        #endregion

        #region khởi tạo
        public FPSOtherVoucherDetail(GOtherVoucher gotherVoucher, List<GOtherVoucher> dsGOtherVoucher, int statusForm)
        {
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                //WaitingFrm.StopWaiting();
                var frm = new FPSOtherVoucherDetailPopup();
                if (frm.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
                {
                    Close();
                    return;
                }
                //WaitingFrm.StartWaiting();
                gotherVoucher = frm.GOtherVoucher;
            }
            #region Khởi tạo giá trị mặc định của Form
            //WindowState = System.Windows.Forms.FormWindowState.Maximized;
            InitializeComponent();
            base.InitializeComponent1();
            #endregion

            _statusForm = statusForm;
            _select = gotherVoucher;
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);

            //Change Menu Top
            ReloadToolbar(_select, _listSelects, _statusForm);
        }
        #endregion

        public override void ShowPopup(GOtherVoucher input, int statusForm)
        {
            //WaitingFrm.StopWaiting();
            var frm = new FPSOtherVoucherDetailPopup();
            if (frm.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
            {
                _statusForm = ConstFrm.optStatusForm.View;
                ReloadToolbar(_select, _listSelects, _statusForm);
                return;
            }
            //WaitingFrm.StartWaiting();

            _statusForm = statusForm;
            _select = frm.GOtherVoucher;

            InitializeGUI(_select);
        }

        #region override
        public override void InitializeGUI(GOtherVoucher inputVoucher)
        {
            #region Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            Template mauGiaoDien = Utils.CloneObject(Utils.GetMauGiaoDien(600, inputVoucher.TemplateID, Utils.ListTemplate));
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : inputVoucher.TemplateID;
            //mauGiaoDien.TemplateDetails.Where(x => x.TabIndex == 0).First().TemplateColumns.Where(x => x.ColumnName == "CurrencyID").First().IsReadOnly = true;
            #endregion
            #endregion

            #region Thiết lập dữ liệu và Fill dữ liệu Obj vào control (nếu đang sửa)

            #region Phần đầu
            grpTop.Text = _select.Type == null ? Utils.ListType.FirstOrDefault(p => p.ID.Equals(_select.TypeID)).TypeName.ToUpper() : _select.Type.TypeName.ToUpper();
            this.ConfigTopVouchersNo<GOtherVoucher>(palTop, "No", "PostedDate", "Date");
            #endregion
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
            }

            BindingList<GOtherVoucherDetail> _bdlGOtherVoucherDetail = new BindingList<GOtherVoucherDetail>(inputVoucher.GOtherVoucherDetails);
            BindingList<GOtherVoucherDetailTax> _bdlGOtherVoucherDetailTax = new BindingList<GOtherVoucherDetailTax>(_statusForm == ConstFrm.optStatusForm.Add
                                                                       ? new List<GOtherVoucherDetailTax>()
                                                                       : (inputVoucher.GOtherVoucherDetailTaxs == null ? new List<GOtherVoucherDetailTax>() : inputVoucher.GOtherVoucherDetailTaxs));
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            if (_statusForm != ConstFrm.optStatusForm.Add)
                bdlRefVoucher = new BindingList<RefVoucher>(inputVoucher.RefVouchers);
            _listObjectInput = new BindingList<IList> { _bdlGOtherVoucherDetail, _bdlGOtherVoucherDetailTax };
            _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
            this.ConfigGridByTemplete_General<GOtherVoucher>(palTab, mauGiaoDien);
            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
            this.ConfigGridByManyTemplete<GOtherVoucher>(600, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
            ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
            ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            //Tab Hóa đơn
            txtReason.DataBindings.Clear();
            txtReason.DataBindings.Add("Text", inputVoucher, "Reason", true, DataSourceUpdateMode.OnPropertyChanged);
            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                SetOrGetGuiObject(inputVoucher, false);
            }
            
            this.ConfigReasonCombo<GOtherVoucher>(600, _statusForm, cbbReason, txtReason, "AutoPrincipleName", "ID");
            DataBinding(new List<Control> { txtReason}, "Reason");
            //cbbReason.Value = cbbReason.Rows.GetItem(0);
            uGrid = (UltraGrid)palTab.Controls.Find("uGrid0", true).FirstOrDefault();
            uGrid.DisplayLayout.Bands[0].Columns["ExchangeRate"].Hidden = true;

            ugrid3 = Controls.Find("uGrid0", true).FirstOrDefault() as UltraGrid;
            ugrid3.DisplayLayout.Bands[0].SortedColumns.Add("OrderPriority", false, false);
            #endregion
        }
        #endregion

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucher>();
                    var f = new FViewVoucherOriginal(_select.CurrencyID, datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }

        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }
    }

    public class FPSOtherVoucherDetailBase : DetailBase<GOtherVoucher>
    {
    }
}
