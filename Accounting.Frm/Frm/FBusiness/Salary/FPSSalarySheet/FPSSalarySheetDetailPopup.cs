﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;

namespace Accounting
{
    public partial class FPSSalarySheetDetailPopup : CustormForm
    {
        readonly List<DepartmentReport> _lstDepartmentReport = ReportUtils.LstDepartmentReport;
        readonly List<PSSalarySheet> _dsPSSalarySheet;
        readonly List<PSTimeSheet> _dsPSTimeSheet;
        readonly List<PSTimeSheetSummary> _dsPSTimeSheetSummary;
        public PSSalarySheet PSSalarySheet;
        public int _status;
        public int type = 0;
        public FPSSalarySheetDetailPopup(PSSalarySheet temp, List<PSSalarySheet> dsPSSalarySheet, int statusForm)
        {
            WaitingFrm.StartWaiting();
            PSSalarySheet = temp;
            _dsPSSalarySheet = dsPSSalarySheet;
            _status = statusForm;
            InitializeComponent();
            var dte = Utils.GetDbStartDate().StringToDateTime();
            cbbThang.Value = dte.HasValue ? dte.Value.Month : DateTime.Now.Month;
            txtYear.Value = dte.HasValue ? dte.Value.Year : DateTime.Now.Year;
            ugridDepartment.SetDataBinding(_lstDepartmentReport.OrderBy(x => x.DepartmentCode).ToList(), "");
            ReportUtils.ProcessControls(this);
            _dsPSTimeSheet = Utils.IPSTimeSheetService.GetAll();
            _dsPSTimeSheetSummary = Utils.IPSTimeSheetSummaryService.GetAll();
            cbbCurrentState.SelectedIndex = 0;
            SetTimeSheetName();

            cbbSalarySheet.DisplayMember = "PSSalarySheetName";
            Utils.ConfigGrid(cbbSalarySheet, ConstDatabase.PSSalarySheet_TableName);

            cbbTimeSheetSummary.DisplayMember = "PSTimeSheetSummaryName";
            Utils.ConfigGrid(cbbTimeSheetSummary, ConstDatabase.PSTimeSheetSummary_TableName);

            cbbTimeSheets.DisplayMember = "PSTimeSheetName";
            Utils.ConfigGrid(cbbTimeSheets, ConstDatabase.PSTimeSheet_TableName);

            cbbTimeSheets.Visible = false;
            cbbTimeSheetSummary.Visible = false;
            cbbSalarySheet.Visible = false;
            lblTitle.Visible = false;

            WaitingFrm.StopWaiting();

        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            if (optLoaiDT.Value.ToInt() == 2 && cbbTimeSheets.SelectedRow == null)
            {
                MSG.Warning("Bạn chưa chọn bảng chấm công muốn dựa theo!");
                return;
            }
            else if (optLoaiDT.Value.ToInt() == 3 && cbbTimeSheetSummary.SelectedRow == null)
            {
                MSG.Warning("Bạn chưa chọn bảng tổng hợp chấm công muốn dựa theo!");
                return;
            }
            else if (optLoaiDT.Value.ToInt() == 1 && cbbSalarySheet.SelectedRow == null)
            {
                MSG.Warning("Bạn chưa chọn bảng lương muốn dựa theo!");
                return;
            }
            int thang = int.Parse(cbbThang.Text);
            int nam = Convert.ToInt32(txtYear.Value);
            //var temp = _dsPSSalarySheet.FirstOrDefault(p => p.Month == thang && p.Year == nam);
            //if (temp != null)
            //{
            //    if (MSG.MessageBoxStand(string.Format("Tháng {0} năm {1} đã lập bảng lương. Bạn có muốn xem bảng lương này không?", thang, nam), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            //    {
            //        DialogResult = DialogResult.OK;
            //        _status = ConstFrm.optStatusForm.View;
            //        PSSalarySheet = temp;
            //        Close();
            //    }
            //    return;
            //}
            decimal WorkDayInMonth = (txtWorkDayInMonth.Text == null || string.IsNullOrEmpty(txtWorkDayInMonth.Text)) ? 0 : Convert.ToDecimal(txtWorkDayInMonth.Value.ToString());
            if (WorkDayInMonth <= 0)
            {
                MessageBox.Show("Chưa nhập số ngày làm việc trong tháng! Vui lòng kiểm tra lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            PSSalarySheet.WorkDayInMonth = WorkDayInMonth;
            var dbDateClosed = Utils.GetDBDateClosed();
            var dte = dbDateClosed.StringToDateTime();
            if (dte.HasValue && (dte.Value.Year > nam || (dte.Value.Year == nam && dte.Value.Month > thang)))
            {
                MSG.Warning(string.Format("Ngày hạch toán phải lớn hơn ngày khóa sổ: {0}. Xin vui lòng kiểm tra lại.", dbDateClosed));
                return;
            }
            PSSalarySheet.Departments = _lstDepartmentReport.Where(p => p.Check).Select(x => x.ID).ToList();
            PSSalarySheet.Month = thang;
            PSSalarySheet.Year = nam;
            PSSalarySheet.TypeID = cbbCurrentState.SelectedItem.DataValue.ToInt();
            PSSalarySheet.PSSalarySheetName = txtTimeSheetName.Text;
            if (GeneratePSSalarySheet())
            {
                DialogResult = DialogResult.OK;
                Close();
            }

        }

        private bool GeneratePSSalarySheet()
        {
            //PSSalaryTaxInsuranceRegulation stir = Utils.IPSSalaryTaxInsuranceRegulationService.FindByDate(new DateTime(PSSalarySheet.Year, PSSalarySheet.Month, 1));
            //if (stir != null && stir.WorkDayInMonth == 0)
            decimal WorkDayInMonth = (txtWorkDayInMonth.Text == null || string.IsNullOrEmpty(txtWorkDayInMonth.Text)) ? 0 : Convert.ToDecimal(txtWorkDayInMonth.Value.ToString());
            if (optLoaiDT.Value.ToInt() == 0)
            {
                PSSalarySheet.PSSalarySheetDetails = Utils.IPSSalarySheetDetailService.FindEmployeeByDepartment(PSSalarySheet.Departments, new DateTime(PSSalarySheet.Year, PSSalarySheet.Month, 1), cbbCurrentState.Value.ToInt(), optLoaiDT.Value.ToInt(), WorkDayInMonth);

            }
            else if (optLoaiDT.Value.ToInt() == 1)
            {
                PSSalarySheet.PSSalarySheetDetails = ((PSSalarySheet)cbbSalarySheet.SelectedRow.ListObject).PSSalarySheetDetails.Where(x => _lstDepartmentReport.Where(c => c.Check).ToList().Any(d => d.ID == x.DepartmentID)).ToList();
            }
            else if (optLoaiDT.Value.ToInt() == 2)
            {
                List<PSTimeSheetDetail> psts = ((PSTimeSheet)cbbTimeSheets.SelectedRow.ListObject).PSTimeSheetDetails.Where(x => _lstDepartmentReport.Where(c => c.Check).ToList().Any(d => d.ID == x.DepartmentID)).ToList();
                PSSalarySheet.PSSalarySheetDetails = Utils.IPSSalarySheetDetailService.FindEmployeeByDepartment(PSSalarySheet.Departments, new DateTime(PSSalarySheet.Year, PSSalarySheet.Month, 1), cbbCurrentState.Value.ToInt(), optLoaiDT.Value.ToInt(), WorkDayInMonth, psts);

            }
            else if (optLoaiDT.Value.ToInt() == 3)
            {
                List<PSTimeSheetSummaryDetail> psts = ((PSTimeSheetSummary)cbbTimeSheetSummary.SelectedRow.ListObject).PSTimeSheetSummaryDetails.Where(x => _lstDepartmentReport.Where(c => c.Check).ToList().Any(d => d.ID == x.DepartmentID)).ToList();
                PSSalarySheet.PSSalarySheetDetails = Utils.IPSSalarySheetDetailService.FindEmployeeByDepartment(PSSalarySheet.Departments, new DateTime(PSSalarySheet.Year, PSSalarySheet.Month, 1), cbbCurrentState.Value.ToInt(), optLoaiDT.Value.ToInt(), WorkDayInMonth, null, psts);

            }
            return true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtYear_Validated(object sender, EventArgs e)
        {
            int thang = 0;
            try
            {
                thang = int.Parse(txtYear.Text);
                if (thang > 2100 || thang <= 1980)
                {
                    MSG.Warning("Giá trị năm không hợp lệ");
                    txtYear.Value = DateTime.Now.Year;
                    return;
                }

            }
            catch (Exception)
            {
                MSG.Warning("Giá trị năm không hợp lệ");
                txtYear.Value = DateTime.Now.Year;
                return;
            }
            SetTimeSheetName();
        }

        private void cbbThang_Validated(object sender, EventArgs e)
        {
            int thang = 0;
            try
            {
                thang = int.Parse(cbbThang.Text);
                if (thang > 12 || thang < 1)
                {
                    MSG.Warning("Giá trị tháng không hợp lệ");
                    return;
                }
            }
            catch (Exception)
            {
                MSG.Warning("Giá trị tháng không hợp lệ");
                return;
            }
            SetTimeSheetName();
        }

        private void SetTimeSheetName()
        {
            txtTimeSheetName.Text = string.Format("{0} tháng {1} năm {2}", cbbCurrentState.Text, cbbThang.Text, txtYear.Text);
        }

        private void cbbCurrentState_Validated(object sender, EventArgs e)
        {
            SetTimeSheetName();
        }

        private void optLoaiDT_ValueChanged(object sender, EventArgs e)
        {
            if (optLoaiDT.Value.ToInt() == 1)
            {
                type = 1;
                cbbTimeSheets.Visible = false;
                cbbTimeSheetSummary.Visible = false;
                cbbSalarySheet.Visible = true;
                lblTitle.Visible = true;
                lblTitle.Text = "Dựa theo bảng lương:";

            }
            else if (optLoaiDT.Value.ToInt() == 2)
            {
                type = 2;
                cbbTimeSheets.Visible = true;
                cbbTimeSheetSummary.Visible = false;
                cbbSalarySheet.Visible = false;
                lblTitle.Visible = true;
                lblTitle.Text = "Dựa trên bảng chấm công:";
            }
            else if (optLoaiDT.Value.ToInt() == 3)
            {
                type = 3;
                cbbTimeSheets.Visible = false;
                cbbTimeSheetSummary.Visible = true;
                cbbSalarySheet.Visible = false;
                lblTitle.Visible = true;
                lblTitle.Text = "Dựa theo bảng tổng hợp chấm công:";
            }
            else
            {
                type = 0;
                cbbTimeSheets.Visible = false;
                cbbTimeSheetSummary.Visible = false;
                cbbSalarySheet.Visible = false;
                lblTitle.Visible = false;
            }
        }

        private void FPSSalarySheetPopup_FormClosing(object sender, FormClosingEventArgs e)
        {
            _lstDepartmentReport.Select(x => { x.Check = false; return x; }).ToList();
        }

        private void cbbCurrentState_ValueChanged(object sender, EventArgs e)
        {
            if (cbbCurrentState.Value.ToInt() == 832 || cbbCurrentState.Value.ToInt() == 834)
            {
                if (optLoaiDT.Items.Count > 2)
                {
                    optLoaiDT.Items.RemoveAt(2);
                    optLoaiDT.Items.RemoveAt(2);
                }
                optLoaiDT.Items[0].CheckState = CheckState.Checked;
            }
            else
            {
                if (optLoaiDT.Items.Count < 3)
                {
                    optLoaiDT.Items.Add(2, "Tạo mới dựa trên bảng chấm công khác");
                    optLoaiDT.Items.Add(3, "Tạo mới dựa trên bảng TH chấm công");
                }

            }
            cbbTimeSheets.DataSource = _dsPSTimeSheet.Where(x => x.TypeID == (cbbCurrentState.SelectedIndex == 0 ? 821 : cbbCurrentState.SelectedIndex == 1 ? 820 : 0)).OrderBy(x => x.Year).ThenBy(x => x.Month).ToList();
            cbbTimeSheetSummary.DataSource = _dsPSTimeSheetSummary.Where(x => x.TypeID == (cbbCurrentState.SelectedIndex == 0 ? 810 : cbbCurrentState.SelectedIndex == 1 ? 811 : 0)).OrderBy(x => x.Year).ThenBy(x => x.Month).ToList();
            cbbSalarySheet.DataSource = _dsPSSalarySheet.Where(x => x.TypeID == (cbbCurrentState.SelectedIndex == 0 ? 830 : cbbCurrentState.SelectedIndex == 1 ? 831 : cbbCurrentState.SelectedIndex == 2 ? 832 : cbbCurrentState.SelectedIndex == 3 ? 834 : 0)).OrderBy(x => x.Year).ThenBy(x => x.Month).ToList();
            cbbTimeSheets.Value = null;
            cbbTimeSheetSummary.Value = null;
            cbbSalarySheet.Value = null;
            SetTimeSheetName();
            PSSalarySheet.PSSalarySheetName = txtTimeSheetName.Text;
        }

        private void cbbThang_ValueChanged(object sender, EventArgs e)
        {
            SetTimeSheetName();
        }

        private void txtYear_ValueChanged(object sender, EventArgs e)
        {
            SetTimeSheetName();
        }
    }
}
