﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Castle.Windsor.Installer;
using FX.Core;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;
using System.Drawing;

namespace Accounting
{
    public partial class FPSSalarySheetDetail : FPSSalarySheetDetailBase
    {
        #region khai bao
        public static bool isClose = true;
        readonly IPSSalarySheetDetailService _IFPSSalarySheetDetailService = Utils.IPSSalarySheetDetailService;
        readonly IPSSalaryTaxInsuranceRegulationService _IPSSalaryTaxInsuranceRegulationService = Utils.IPSSalaryTaxInsuranceRegulationService;
        UltraGrid ugrid = null;
        int type = 0;
        #endregion
        #region khởi tạo
        public FPSSalarySheetDetail(PSSalarySheet temp, List<PSSalarySheet> lstPSTimeSheets, int statusForm)
        {
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                WaitingFrm.StopWaiting();
                var frm = new FPSSalarySheetDetailPopup(temp, lstPSTimeSheets, statusForm);
                if (frm.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
                {
                    Close();
                    return;
                }
                WaitingFrm.StartWaiting();
                statusForm = frm._status;
                type = frm.type;
                if (statusForm == ConstFrm.optStatusForm.View)
                    temp = lstPSTimeSheets.FirstOrDefault(k => k.ID == frm.PSSalarySheet.ID);
                else
                {
                    temp = frm.PSSalarySheet;
                }
            }
            #region Khởi tạo giá trị mặc định của Form
            //WindowState = System.Windows.Forms.FormWindowState.Maximized;
            InitializeComponent();
            base.InitializeComponent1();
            
            #endregion

            #region Thiết lập ban đầu cho Form
            _statusForm = statusForm;
            _select = temp;
            if(_statusForm == 2 && type == 0)
            {
                foreach(var x in _select.PSSalarySheetDetails)
                {
                    x.NumberOfPaidWorkingDayTimeSheet = 0;
                }
            }
            _listSelects.AddRange(lstPSTimeSheets);
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, _listSelects, statusForm);
            #endregion
        }
        #endregion

        public override void ShowPopup(PSSalarySheet input, int statusForm)
        {
            WaitingFrm.StopWaiting();
            List<PSSalarySheet> lst = Utils.IPSSalarySheetService.GetAll();
            var frm = new FPSSalarySheetDetailPopup(input, lst, statusForm);
            if (frm.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
            {
                _statusForm = ConstFrm.optStatusForm.View;
                return;
            }
            WaitingFrm.StartWaiting();
            statusForm = frm._status;
            if (statusForm == ConstFrm.optStatusForm.View)
                input = _listSelects.FirstOrDefault(k => k.ID == frm.PSSalarySheet.ID);
            else
            {
                input = frm.PSSalarySheet;
            }
            type = frm.type;
            _statusForm = statusForm;
            _select = input;

            InitializeGUI(_select);
        }

        #region Hàm override
        public override void InitializeGUI(PSSalarySheet input)
        {
            ultraGroupBox1.Text = input.PSSalarySheetName;
            Template mauGiaoDien = Utils.CloneObject(Utils.GetMauGiaoDien(input.TypeID, input.TemplateID));
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;
            if (input.TypeID == 832 || input.TypeID == 831 || input.TypeID == 830)
            {
                if (Utils.ListSystemOption.Where(x => x.Code == "TL_TheoHeSoLuong").First().Data == "1")
                {
                    mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "SalaryCoefficient").FirstOrDefault().IsVisible = true;
                    mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "BasicWage").FirstOrDefault().IsVisible = true;
                    mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "BasicWageAmount").FirstOrDefault().IsVisible = true;
                    mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "AgreementSalary").FirstOrDefault().IsVisible = false;
                }
                else
                {
                    mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "SalaryCoefficient").FirstOrDefault().IsVisible = false;
                    mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "BasicWage").FirstOrDefault().IsVisible = false;
                    mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "BasicWageAmount").FirstOrDefault().IsVisible = false;
                    mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "AgreementSalary").FirstOrDefault().IsVisible = true;
                }

            }

            BindingList<PSSalarySheetDetail> dsFPSSalarySheetDetails = new BindingList<PSSalarySheetDetail>(input.PSSalarySheetDetails);

            #region cau hinh ban dau cho form

            _listObjectInput = new BindingList<System.Collections.IList> { dsFPSSalarySheetDetails };
            this.ConfigGridByTemplete_General<PSSalarySheet>(pnlUgrid, mauGiaoDien);
            this.ConfigGridByTemplete<PSSalarySheet>(input.TypeID, mauGiaoDien, true, _listObjectInput);
            txtWorkDayInMonth.Text = input.WorkDayInMonth.ToString();
            ugrid = Controls.Find("uGrid0", true).FirstOrDefault() as UltraGrid;
            ugrid.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(uGrid_BeforeExitEditMode);
            if (ugrid != null && ugrid.DisplayLayout.Bands[0].Columns.Exists("OrderPriority"))
                ugrid.DisplayLayout.Bands[0].Columns["OrderPriority"].SortIndicator = SortIndicator.Ascending;
            ugrid.DisplayLayout.Bands[0].Columns["NonWorkingDayAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            ugrid.DisplayLayout.Bands[0].Columns["PaidWorkingDayAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            ugrid.DisplayLayout.Bands[0].Columns["WorkingDayUnitPrice"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
            
            ugrid.DisplayLayout.Bands[0].Columns["NumberOfNonWorkingDayTimeSheet"].FormatNumberic(ConstDatabase.Format_Quantity);
            ugrid.DisplayLayout.Bands[0].Columns["NumberOfPaidWorkingDayTimeSheet"].FormatNumberic(ConstDatabase.Format_Quantity);
            if (ugrid.DisplayLayout.Bands[0].Columns.Exists("SalaryCoefficient"))
                ugrid.DisplayLayout.Bands[0].Columns["SalaryCoefficient"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
            if (ugrid.DisplayLayout.Bands[0].Columns.Exists("BasicWage"))
                ugrid.DisplayLayout.Bands[0].Columns["BasicWage"].FormatNumberic(ConstDatabase.Format_TienVND);
            if (ugrid.DisplayLayout.Bands[0].Columns.Exists("BasicWageAmount"))
                ugrid.DisplayLayout.Bands[0].Columns["BasicWageAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            ugrid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
            ugrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            #endregion
        }
        #endregion
        private void uGrid_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            UltraGridCell activeCell = grid.ActiveCell;
            List<string> lst = new List<string>{ "OrderPriority", "DepartmentID", "EmployeeID", "AccountingObjectName", "AccountingObjectTitle" };
            if (!lst.Any(d=>d == activeCell.Column.Key))
            {
                if (activeCell.Text.Trim('_', ',') == "")
                    activeCell.Value = (decimal)0;
            }
        }
    }

    public class FPSSalarySheetDetailBase : DetailBase<PSSalarySheet> { }

}
