﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinSchedule;

namespace Accounting
{


    public partial class FPSTimeSheetPopup : CustormForm
    {
        readonly List<DepartmentReport> _lstDepartmentReport = ReportUtils.LstDepartmentReport;
        readonly List<PSTimeSheet> _dsPSTimeSheets;
        public PSTimeSheet PSTimeSheet;
        public int _status;
        public FPSTimeSheetPopup(PSTimeSheet temp, List<PSTimeSheet> dsPSTimeSheets, int statusForm)
        {
            WaitingFrm.StartWaiting();
            PSTimeSheet = temp;
            _dsPSTimeSheets = dsPSTimeSheets;
            _status = statusForm;
            InitializeComponent();
            var dte = Utils.GetDbStartDate().StringToDateTime();
            cbbThang.Value = dte.HasValue ? dte.Value.Month : DateTime.Now.Month;
            txtYear.Value = dte.HasValue ? dte.Value.Year : DateTime.Now.Year;
            ugridDepartment.SetDataBinding(_lstDepartmentReport.ToList(), "");
            ReportUtils.ProcessControls(this);
            cbbCurrentState.SelectedIndex = 0;
            SetTimeSheetName();
            cbbTimeSheets.Enabled = false;
            chkNewEmployee.Enabled = false;
            chkUnemployee.Enabled = false;

            cbbTimeSheets.DisplayMember = "PSTimeSheetName";
            Utils.ConfigGrid(cbbTimeSheets, ConstDatabase.PSTimeSheet_TableName);
            WaitingFrm.StopWaiting();

        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            if (optLoaiDT.Value.ToInt() == 1 && cbbTimeSheets.SelectedRow == null)
            {
                MSG.Warning("Bạn chưa chọn bảng chấm công muốn dựa theo!");
                return;
            }
            int thang = int.Parse(cbbThang.Text);
            int nam = Convert.ToInt32(txtYear.Value);
            //var temp = _dsPSTimeSheets.FirstOrDefault(p => p.Month == thang && p.Year == nam);
            //if (temp != null)
            //{
            //    if (MSG.MessageBoxStand(string.Format("Tháng {0} năm {1} đã chấm công. Bạn có muốn xem bảng chấm công này không?", thang, nam), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            //    {
            //        DialogResult = DialogResult.OK;
            //        _status = ConstFrm.optStatusForm.View;
            //        PSTimeSheet = temp;
                    
            //        Close();
            //    }
            //    return;
            //}
            var dbDateClosed = Utils.GetDBDateClosed();
            var dte = dbDateClosed.StringToDateTime();
            if (dte.HasValue && (dte.Value.Year > nam || (dte.Value.Year == nam && dte.Value.Month > thang)))
            {
                MSG.Warning(string.Format("Ngày hạch toán phải lớn hơn ngày khóa sổ: {0}. Xin vui lòng kiểm tra lại.", dbDateClosed));
                return;
            }
            
            PSTimeSheet.Month = thang;
            PSTimeSheet.Year = nam;
            PSTimeSheet.TypeID = cbbCurrentState.SelectedIndex == 0 ? 821 : 820;
            PSTimeSheet.PSTimeSheetName = txtTimeSheetName.Text;
            PSTimeSheet.IsNewEmployee = chkNewEmployee.Checked;
            PSTimeSheet.IsUnemployee = chkUnemployee.Checked;
            PSTimeSheet.Departments = _lstDepartmentReport.Where(x => x.Check).Select(x => x.ID).ToList();
            PSTimeSheet.BaseOn = optLoaiDT.Value.ToInt() == 1 ? ((PSTimeSheet)cbbTimeSheets.SelectedRow.ListObject).ID : (Guid?)null;
            _lstDepartmentReport.Select(x => { x.Check = false; return x; }).ToList();
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            
            Close();
        }

        private void txtYear_Validated(object sender, EventArgs e)
        {
            int thang = 0;
            try
            {
                thang = int.Parse(txtYear.Text);
                if (thang > 2100 || thang <= 1980)
                {
                    MSG.Warning("Giá trị năm không hợp lệ");
                    txtYear.Value = DateTime.Now.Year;
                    return;
                }

            }
            catch (Exception)
            {
                MSG.Warning("Giá trị năm không hợp lệ");
                txtYear.Value = DateTime.Now.Year;
                return;
            }
            SetTimeSheetName();
        }

        private void cbbThang_Validated(object sender, EventArgs e)
        {
            int thang = 0;
            try
            {
                thang = int.Parse(cbbThang.Text);
                if (thang > 12 || thang < 1)
                {
                    MSG.Warning("Giá trị tháng không hợp lệ");
                    return;
                }
            }
            catch (Exception)
            {
                MSG.Warning("Giá trị tháng không hợp lệ");
                return;
            }
            SetTimeSheetName();
        }

        private void SetTimeSheetName()
        {
            txtTimeSheetName.Text = string.Format("Bảng {0} tháng {1} năm {2}", cbbCurrentState.Text, cbbThang.Text, txtYear.Text);
        }

        private void cbbCurrentState_Validated(object sender, EventArgs e)
        {
            SetTimeSheetName();
        }

        private void optLoaiDT_ValueChanged(object sender, EventArgs e)
        {
            if (optLoaiDT.Value.ToInt() == 1)
            {
                cbbTimeSheets.Enabled = true;
                chkNewEmployee.Enabled = true;
                chkUnemployee.Enabled = true;
            }
            else
            {
                cbbTimeSheets.Enabled = false;
                chkNewEmployee.Enabled = false;
                chkUnemployee.Enabled = false;
            }
        }

        private void FPSTimeSheetPopup_FormClosing(object sender, FormClosingEventArgs e)
        {
            _lstDepartmentReport.Select(x => { x.Check = false; return x; }).ToList();
        }

        private void cbbCurrentState_ValueChanged(object sender, EventArgs e)
        {
            cbbTimeSheets.DataSource = _dsPSTimeSheets.Where(x => x.TypeID == (cbbCurrentState.SelectedIndex == 0 ? 821 : 820)).ToList();
            cbbTimeSheets.Value = null;
            SetTimeSheetName();
        }

        private void cbbThang_ValueChanged(object sender, EventArgs e)
        {
            SetTimeSheetName();
        }

        private void txtYear_ValueChanged(object sender, EventArgs e)
        {
            SetTimeSheetName();
        }
    }
}
