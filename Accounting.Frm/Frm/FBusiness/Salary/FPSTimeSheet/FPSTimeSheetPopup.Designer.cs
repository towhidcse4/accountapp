﻿namespace Accounting
{
    partial class FPSTimeSheetPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem15 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem16 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem17 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem18 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem19 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem9 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem10 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem11 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem12 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem8 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnApply = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtTimeSheetName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbThang = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtYear = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.lblYears = new Infragistics.Win.Misc.UltraLabel();
            this.lblMonth = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbCurrentState = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ugridDepartment = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.optLoaiDT = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbTimeSheets = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.chkNewEmployee = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkUnemployee = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            ((System.ComponentModel.ISupportInitialize)(this.txtTimeSheetName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbThang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrentState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugridDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optLoaiDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTimeSheets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUnemployee)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            appearance1.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance1;
            this.btnClose.Location = new System.Drawing.Point(614, 647);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 340;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnApply
            // 
            appearance2.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnApply.Appearance = appearance2;
            this.btnApply.Location = new System.Drawing.Point(533, 647);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 30);
            this.btnApply.TabIndex = 341;
            this.btnApply.Text = "Đồng ý";
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // ultraLabel1
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance3;
            this.ultraLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel1.Location = new System.Drawing.Point(10, 93);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(134, 22);
            this.ultraLabel1.TabIndex = 3008;
            this.ultraLabel1.Text = "Tên bảng chấm công:";
            // 
            // txtTimeSheetName
            // 
            this.txtTimeSheetName.AutoSize = false;
            this.txtTimeSheetName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtTimeSheetName.Location = new System.Drawing.Point(163, 93);
            this.txtTimeSheetName.MaxLength = 25;
            this.txtTimeSheetName.Name = "txtTimeSheetName";
            this.txtTimeSheetName.Size = new System.Drawing.Size(523, 22);
            this.txtTimeSheetName.TabIndex = 3009;
            // 
            // cbbThang
            // 
            appearance4.TextHAlignAsString = "Right";
            this.cbbThang.Appearance = appearance4;
            this.cbbThang.AutoSize = false;
            valueListItem2.DataValue = ((short)(1));
            valueListItem15.DataValue = ((short)(2));
            valueListItem16.DataValue = ((short)(3));
            valueListItem17.DataValue = ((short)(4));
            valueListItem18.DataValue = ((short)(5));
            valueListItem19.DataValue = ((short)(6));
            valueListItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem1.DataValue = ((short)(7));
            valueListItem3.DataValue = ((short)(8));
            valueListItem9.DataValue = ((short)(9));
            valueListItem10.DataValue = ((short)(10));
            valueListItem11.DataValue = ((short)(11));
            valueListItem12.DataValue = ((short)(12));
            this.cbbThang.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem2,
            valueListItem15,
            valueListItem16,
            valueListItem17,
            valueListItem18,
            valueListItem19,
            valueListItem1,
            valueListItem3,
            valueListItem9,
            valueListItem10,
            valueListItem11,
            valueListItem12});
            this.cbbThang.Location = new System.Drawing.Point(231, 60);
            this.cbbThang.Name = "cbbThang";
            this.cbbThang.Size = new System.Drawing.Size(77, 22);
            this.cbbThang.TabIndex = 3007;
            this.cbbThang.ValueChanged += new System.EventHandler(this.cbbThang_ValueChanged);
            this.cbbThang.Validated += new System.EventHandler(this.cbbThang_Validated);
            // 
            // txtYear
            // 
            this.txtYear.AutoSize = false;
            this.txtYear.Location = new System.Drawing.Point(395, 60);
            this.txtYear.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtYear.MaskInput = "nnnn";
            this.txtYear.MinValue = 1980;
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(72, 22);
            this.txtYear.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.OnMouseEnter;
            this.txtYear.TabIndex = 3006;
            this.txtYear.Tag = "";
            this.txtYear.Value = 2012;
            this.txtYear.ValueChanged += new System.EventHandler(this.txtYear_ValueChanged);
            this.txtYear.Validated += new System.EventHandler(this.txtYear_Validated);
            // 
            // lblYears
            // 
            appearance5.TextVAlignAsString = "Middle";
            this.lblYears.Appearance = appearance5;
            this.lblYears.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblYears.Location = new System.Drawing.Point(348, 59);
            this.lblYears.Name = "lblYears";
            this.lblYears.Size = new System.Drawing.Size(41, 23);
            this.lblYears.TabIndex = 3005;
            this.lblYears.Text = "Năm :";
            // 
            // lblMonth
            // 
            appearance6.TextVAlignAsString = "Middle";
            this.lblMonth.Appearance = appearance6;
            this.lblMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonth.Location = new System.Drawing.Point(163, 59);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(62, 23);
            this.lblMonth.TabIndex = 3004;
            this.lblMonth.Text = "Tháng :";
            // 
            // ultraLabel2
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(111)))), ((int)(((byte)(33)))));
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance7;
            this.ultraLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel2.Location = new System.Drawing.Point(10, 59);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(134, 22);
            this.ultraLabel2.TabIndex = 3010;
            this.ultraLabel2.Text = "Kỳ chấm công";
            // 
            // ultraLabel3
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(111)))), ((int)(((byte)(33)))));
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance8;
            this.ultraLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel3.Location = new System.Drawing.Point(10, 12);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(148, 22);
            this.ultraLabel3.TabIndex = 3011;
            this.ultraLabel3.Text = "Bảng chấm công";
            // 
            // ultraLabel4
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance9;
            this.ultraLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel4.Location = new System.Drawing.Point(377, 12);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel4.TabIndex = 3012;
            this.ultraLabel4.Text = "Loại chấm công:";
            // 
            // cbbCurrentState
            // 
            this.cbbCurrentState.AutoSize = false;
            this.cbbCurrentState.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCurrentState.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem4.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem4.DataValue = "821";
            valueListItem4.DisplayText = "chấm công theo ngày";
            valueListItem5.DataValue = "820";
            valueListItem5.DisplayText = "chấm công theo giờ";
            this.cbbCurrentState.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem4,
            valueListItem5});
            this.cbbCurrentState.Location = new System.Drawing.Point(483, 12);
            this.cbbCurrentState.Name = "cbbCurrentState";
            this.cbbCurrentState.Size = new System.Drawing.Size(203, 22);
            this.cbbCurrentState.TabIndex = 3017;
            this.cbbCurrentState.ValueMember = "0";
            this.cbbCurrentState.ValueChanged += new System.EventHandler(this.cbbCurrentState_ValueChanged);
            this.cbbCurrentState.Validated += new System.EventHandler(this.cbbCurrentState_Validated);
            // 
            // ugridDepartment
            // 
            this.ugridDepartment.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ugridDepartment.Location = new System.Drawing.Point(10, 121);
            this.ugridDepartment.Name = "ugridDepartment";
            this.ugridDepartment.Size = new System.Drawing.Size(676, 381);
            this.ugridDepartment.TabIndex = 3018;
            this.ugridDepartment.Text = "Danh sách phòng ban";
            this.ugridDepartment.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // optLoaiDT
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.FontData.BoldAsString = "False";
            this.optLoaiDT.Appearance = appearance10;
            this.optLoaiDT.BackColor = System.Drawing.Color.Transparent;
            this.optLoaiDT.BackColorInternal = System.Drawing.Color.Transparent;
            this.optLoaiDT.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.optLoaiDT.CheckedIndex = 0;
            this.optLoaiDT.ItemOrigin = new System.Drawing.Point(5, 0);
            valueListItem7.DataValue = "0";
            valueListItem7.DisplayText = "Tạo mới hoàn toàn ";
            valueListItem7.Tag = "optTM";
            valueListItem8.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem8.DataValue = "1";
            valueListItem8.DisplayText = "Tạo mới dựa trên bảng chấm công khác";
            valueListItem8.Tag = "optTL";
            this.optLoaiDT.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem7,
            valueListItem8});
            this.optLoaiDT.ItemSpacingHorizontal = 35;
            this.optLoaiDT.Location = new System.Drawing.Point(12, 532);
            this.optLoaiDT.Name = "optLoaiDT";
            this.optLoaiDT.Size = new System.Drawing.Size(674, 19);
            this.optLoaiDT.TabIndex = 3019;
            this.optLoaiDT.Text = "Tạo mới hoàn toàn ";
            this.optLoaiDT.ValueChanged += new System.EventHandler(this.optLoaiDT_ValueChanged);
            // 
            // ultraLabel5
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance11;
            this.ultraLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel5.Location = new System.Drawing.Point(12, 567);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(146, 22);
            this.ultraLabel5.TabIndex = 3020;
            this.ultraLabel5.Text = "Dựa trên bảng chấm công";
            // 
            // cbbTimeSheets
            // 
            this.cbbTimeSheets.AutoSize = false;
            this.cbbTimeSheets.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbTimeSheets.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbTimeSheets.Location = new System.Drawing.Point(173, 567);
            this.cbbTimeSheets.Name = "cbbTimeSheets";
            this.cbbTimeSheets.Size = new System.Drawing.Size(513, 22);
            this.cbbTimeSheets.TabIndex = 3021;
            // 
            // chkNewEmployee
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            this.chkNewEmployee.Appearance = appearance12;
            this.chkNewEmployee.BackColor = System.Drawing.Color.Transparent;
            this.chkNewEmployee.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkNewEmployee.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkNewEmployee.Location = new System.Drawing.Point(173, 612);
            this.chkNewEmployee.Name = "chkNewEmployee";
            this.chkNewEmployee.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkNewEmployee.Size = new System.Drawing.Size(199, 16);
            this.chkNewEmployee.TabIndex = 3022;
            this.chkNewEmployee.Text = "Tự động thêm những nhân viên mới";
            // 
            // chkUnemployee
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            this.chkUnemployee.Appearance = appearance13;
            this.chkUnemployee.BackColor = System.Drawing.Color.Transparent;
            this.chkUnemployee.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkUnemployee.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkUnemployee.Location = new System.Drawing.Point(395, 612);
            this.chkUnemployee.Name = "chkUnemployee";
            this.chkUnemployee.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkUnemployee.Size = new System.Drawing.Size(213, 16);
            this.chkUnemployee.TabIndex = 3023;
            this.chkUnemployee.Text = "Lấy cả nhân viên đã ngừng theo dõi";
            // 
            // FPSTimeSheetPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 689);
            this.Controls.Add(this.chkUnemployee);
            this.Controls.Add(this.chkNewEmployee);
            this.Controls.Add(this.cbbTimeSheets);
            this.Controls.Add(this.ultraLabel5);
            this.Controls.Add(this.optLoaiDT);
            this.Controls.Add(this.ugridDepartment);
            this.Controls.Add(this.cbbCurrentState);
            this.Controls.Add(this.ultraLabel4);
            this.Controls.Add(this.ultraLabel3);
            this.Controls.Add(this.ultraLabel2);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.txtTimeSheetName);
            this.Controls.Add(this.cbbThang);
            this.Controls.Add(this.txtYear);
            this.Controls.Add(this.lblYears);
            this.Controls.Add(this.lblMonth);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnApply);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FPSTimeSheetPopup";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bảng chấm công";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FPSTimeSheetPopup_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.txtTimeSheetName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbThang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrentState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugridDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optLoaiDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTimeSheets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUnemployee)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnApply;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTimeSheetName;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbThang;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtYear;
        private Infragistics.Win.Misc.UltraLabel lblYears;
        private Infragistics.Win.Misc.UltraLabel lblMonth;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbCurrentState;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugridDepartment;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet optLoaiDT;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbTimeSheets;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkNewEmployee;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkUnemployee;
    }
}