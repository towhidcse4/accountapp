﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Castle.Windsor.Installer;
using FX.Core;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;
using System.Drawing;

namespace Accounting
{
    public partial class FPSTimeSheetDetail : FPSTimeSheetBase
    {
        #region khai bao
        public static bool isClose = true;
        readonly IPSTimeSheetDetailService _IPSTimeSheetDetailService = Utils.IPSTimeSheetDetailService;
        readonly IPSSalaryTaxInsuranceRegulationService _IPSSalaryTaxInsuranceRegulationService = Utils.IPSSalaryTaxInsuranceRegulationService;
        UltraGrid ugrid = null;
        public List<string> DayLock=new List<string>();
        #endregion
        #region khởi tạo
        public FPSTimeSheetDetail(PSTimeSheet temp, List<PSTimeSheet> lstPSTimeSheets, int statusForm)
        {
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                WaitingFrm.StopWaiting();
                var frm = new FPSTimeSheetPopup(temp, lstPSTimeSheets, statusForm);
                if (frm.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
                {
                    Close();
                    return;
                }
                WaitingFrm.StartWaiting();
                statusForm = frm._status;
                if (statusForm == ConstFrm.optStatusForm.View)
                    temp = lstPSTimeSheets.FirstOrDefault(k => k.ID == frm.PSTimeSheet.ID);
                else
                {
                    temp = frm.PSTimeSheet;
                }
            }
            #region Khởi tạo giá trị mặc định của Form
           // base.WindowState = System.Windows.Forms.FormWindowState.Maximized;
           // this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            InitializeComponent();
            base.InitializeComponent1();
            
               
            
                #endregion
                if (temp.TypeID == 820)
                this.Text = "Bảng chấm công theo giờ";
            else
                this.Text = "Bảng chấm công theo ngày";
            #region Thiết lập ban đầu cho Form
            _statusForm = statusForm;
            _select = temp;
            _listSelects.AddRange(lstPSTimeSheets);
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, _listSelects, statusForm);
            #endregion
        }
        #endregion

        public override void ShowPopup(PSTimeSheet input, int statusForm)
        {
            WaitingFrm.StopWaiting();
            List<PSTimeSheet> lst = Utils.IPSTimeSheetService.GetAll();
            var frm = new FPSTimeSheetPopup(input, lst, statusForm);
            if (frm.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
            {
                _statusForm = ConstFrm.optStatusForm.View;
                return;
            }
            WaitingFrm.StartWaiting();
            statusForm = frm._status;
            if (statusForm == ConstFrm.optStatusForm.View)
                input = _listSelects.FirstOrDefault(k => k.ID == frm.PSTimeSheet.ID);
            else
            {
                input = frm.PSTimeSheet;
            }

            _statusForm = statusForm;
            _select = input;

            InitializeGUI(_select);
        }

        #region Hàm override
        public override void InitializeGUI(PSTimeSheet input)
        {
            lblSheetName.Text = input.PSTimeSheetName;
            Template mauGiaoDien = Utils.CloneObject(Utils.GetMauGiaoDien(821, input.TemplateID));
            int daysInMonth = DateTime.DaysInMonth(input.Year, input.Month);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;
            // ẩn các cột ngày 29 30 31 nếu tháng k có ngày đó
            if (daysInMonth < 31)
            {
                mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "Day31").First().IsVisible = false;
            }
            else
            {
                mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "Day31").First().IsVisible = true;
            }
            if (daysInMonth < 30)
            {
                mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "Day30").First().IsVisible = false;
            }
            else
            {
                mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "Day30").First().IsVisible = true;
            }
            if (daysInMonth < 29)
            {
                mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "Day29").First().IsVisible = false;
            }
            else
            {
                mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "Day29").First().IsVisible = true;
            }
            
            BindingList<PSTimeSheetDetail> dsPSTimeSheetDetails = new BindingList<PSTimeSheetDetail>();

            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                dsPSTimeSheetDetails = new BindingList<PSTimeSheetDetail>(input.PSTimeSheetDetails);
                var psstir = _IPSSalaryTaxInsuranceRegulationService.FindByDate(new DateTime(input.Year, input.Month, 1));
                if (psstir != null)
                {
                    _select.WorkingHoursInDay = psstir.WorkingHoursInDay;
                }
                else
                {
                    psstir = new PSSalaryTaxInsuranceRegulation();
                }
            }
            else
            {
                var psstir = _IPSSalaryTaxInsuranceRegulationService.FindByDate(new DateTime(input.Year, input.Month, 1));
                List<int> t7 = new List<int>();
                List<int> cn = new List<int>();
                if (psstir != null)
                {
                    _select.WorkingHoursInDay = psstir.WorkingHoursInDay;
                }
                else
                {
                    psstir = new PSSalaryTaxInsuranceRegulation();
                }
                for (int i = 1; i <= daysInMonth; i++)
                {
                    DateTime dt = new DateTime(input.Year, input.Month, i);
                    if (dt.DayOfWeek == DayOfWeek.Saturday)
                    {
                        t7.Add(i);
                    }
                    else if (dt.DayOfWeek == DayOfWeek.Sunday)
                    {
                        cn.Add(i);
                    }
                }
                List<PSTimeSheetDetail> lst = _IPSTimeSheetDetailService.FindEmployeeByDepartment(input.Departments, daysInMonth, t7, cn, (psstir != null && psstir.WorkingHoursInDay != 0 && input.TypeID == 820) ? (int)psstir.WorkingHoursInDay : (decimal?)null, input.BaseOn, input.IsNewEmployee, input.IsUnemployee, psstir.IsWorkingOnSaturday, psstir.IsWorkingOnSaturdayNoon, psstir.IsWorkingOnSunday, psstir.IsWorkingOnSundayNoon);
                //lst = lst.OrderBy(n=>n.EmployeeCode).ToList();
                dsPSTimeSheetDetails = new BindingList<PSTimeSheetDetail>(lst);
                
            }

            #region cau hinh ban dau cho form

            _listObjectInput = new BindingList<System.Collections.IList> { dsPSTimeSheetDetails };
            this.ConfigGridByTemplete_General<PSTimeSheet>(pnlUgrid, mauGiaoDien);
            this.ConfigGridByTemplete<PSTimeSheet>(input.TypeID, mauGiaoDien, true, _listObjectInput);
            #endregion
            ugrid = Controls.Find("uGrid0", true).FirstOrDefault() as UltraGrid;
            DateTime dateTime = new DateTime(input.Year, input.Month, 1);
            PSSalaryTaxInsuranceRegulation pSSalaryTaxInsuranceRegulation = Utils.IPSSalaryTaxInsuranceRegulationService.Query.FirstOrDefault(n => n.FromDate <= dateTime && dateTime <= n.ToDate);
            if (ugrid != null && ugrid.DisplayLayout.Bands[0].Columns.Exists("OrderPriority"))
            {
                for (int i = 1; i <= daysInMonth; i++)
                {
                    ugrid.DisplayLayout.Bands[0].Columns["Day" + i].CellActivation = Activation.NoEdit;
                    DateTime dt = new DateTime(input.Year, input.Month, i);
                    if (dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday)
                    {
                        ugrid.DisplayLayout.Bands[0].Columns["Day" + i].Header.Appearance.BackColor = System.Drawing.ColorTranslator.FromHtml("#f26f21");
                        if(dt.DayOfWeek == DayOfWeek.Saturday)
                        {
                            if (pSSalaryTaxInsuranceRegulation != null)
                            {
                                if(!pSSalaryTaxInsuranceRegulation.IsWorkingOnSaturday && !pSSalaryTaxInsuranceRegulation.IsWorkingOnSaturdayNoon)
                                {
                                    DayLock.Add("Day" + i);
                                }
                            }
                        }
                        if(dt.DayOfWeek == DayOfWeek.Sunday)
                        {
                            if (pSSalaryTaxInsuranceRegulation != null)
                            {
                                if (!pSSalaryTaxInsuranceRegulation.IsWorkingOnSunday && !pSSalaryTaxInsuranceRegulation.IsWorkingOnSundayNoon)
                                {
                                    DayLock.Add("Day" + i);
                                }
                            }
                        }
                    }
                    else
                    {
                        ugrid.DisplayLayout.Bands[0].Columns["Day" + i].Header.Appearance.BackColor = Color.FromArgb(255, 50, 104, 189);
                    }
                }
                ugrid.DisplayLayout.Bands[0].Columns["PaidWorkingDay"].CellActivation = Activation.NoEdit;
                ugrid.DisplayLayout.Bands[0].Columns["PaidNonWorkingDay"].CellActivation = Activation.NoEdit;
                ugrid.DisplayLayout.Bands[0].Columns["WorkingDay"].CellActivation = Activation.NoEdit;
                ugrid.DisplayLayout.Bands[0].Columns["WeekendDay"].CellActivation = Activation.NoEdit;
                ugrid.DisplayLayout.Bands[0].Columns["Holiday"].CellActivation = Activation.NoEdit;
                ugrid.DisplayLayout.Bands[0].Columns["WorkingDayNight"].CellActivation = Activation.NoEdit;
                ugrid.DisplayLayout.Bands[0].Columns["WeekendDayNight"].CellActivation = Activation.NoEdit;
                ugrid.DisplayLayout.Bands[0].Columns["HolidayNight"].CellActivation = Activation.NoEdit;
                ugrid.DisplayLayout.Bands[0].Columns["TotalOverTime"].CellActivation = Activation.NoEdit;

                ugrid.DisplayLayout.Bands[0].Columns["OrderPriority"].SortIndicator = SortIndicator.Ascending;
                ugrid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
                ugrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            }
            //ugrid.ClickCell += ugrid_ClickCell;
        }
        #endregion
        public void ugrid_ClickCell(object sender, ClickCellEventArgs e)
        {
            if (!DayLock.Any(n=>n==e.Cell.Column.Key.ToString()) && e.Cell.Column.Key.Contains("Day") && !e.Cell.Column.Key.Contains("Working") && !e.Cell.Column.Key.Contains("Weekend") && _statusForm != ConstFrm.optStatusForm.View)
            {
                if(e.Cell.Row.Index < ugrid.Rows.Count && e.Cell.Row.Index != -1)
                {
                    var f = new FPPSelectTSSymbol(e.Cell.Text);
                    f.FormClosed += new FormClosedEventHandler((s, x) => fTSSybol_FormClosed(s, x, e.Cell.Row.Index, e.Cell.Column.Key));
                    f.ShowDialog(this);
                }                
            }
        }
        private void fTSSybol_FormClosed(object sender, FormClosedEventArgs e, int index, string key)
        {
            var frm = (FPPSelectTSSymbol)sender;
            if (e.CloseReason != CloseReason.UserClosing) return;
            if (frm.DialogResult == DialogResult.OK)
            {
                string s = "";
                foreach (var x in frm.SelectTSSymbols) s += x.TimeSheetSymbolsCode + (x.OT != null && !string.IsNullOrEmpty(x.OT.ToString()) && x.OT > 0 ? " " + x.OT.ToString() : "") + "; ";
                var str = s.Substring(0, s.Length - 2);
                ugrid.Rows[index].Cells[key].Value = str;
            }
        }
    }

    public class FPSTimeSheetBase : DetailBase<PSTimeSheet> { }

}
