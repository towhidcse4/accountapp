﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FPPSelectTSSymbol : CustormForm
    {
        public string symbol { get; set; }
        public List<TimeSheetSymbols> SelectTSSymbols { get; private set; }

        public FPPSelectTSSymbol(string str)
        {
            InitializeComponent();
            symbol = str;
            ConfigGrid();

        }
        private void ConfigGrid()
        {
            var list = new BindingList<TimeSheetSymbols>(Utils.ListTimeSheetSymbols.OrderBy(x => x.TimeSheetSymbolsCode).ToList().CloneObject());
            if (symbol != null && !string.IsNullOrEmpty(symbol))
            {
                var listsymbol = symbol.Split(';');
                foreach (var x in list)
                {
                    var str = listsymbol.FirstOrDefault(c => c.TrimStart().Split(' ')[0] == (x.TimeSheetSymbolsCode));
                    if (str != null)
                    {
                        string[] c = str.TrimStart().Split(' ');
                        x.Selected = true;
                        if (c.Count() > 1) x.OT = Convert.ToDecimal(c[1]);
                    }
                }
            }
            uGrid.DataSource = list;
            uGrid.ConfigGrid(ConstDatabase.TimeSheetSymbols_TableName, false);
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.UseFixedHeaders = false;
            uGrid.DisplayLayout.Bands[0].Columns["OT"].FormatNumberic(ConstDatabase.Format_Quantity);
            UltraGridColumn ugc = uGrid.DisplayLayout.Bands[0].Columns["Selected"];
            ugc.Header.VisiblePosition = 0;
            ugc.Hidden = false;
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            uGrid.DisplayLayout.Bands[0].Columns["SalaryRate"].Hidden = true;
            uGrid.DisplayLayout.Bands[0].Columns["IsDefault"].Hidden = true;
            uGrid.DisplayLayout.Bands[0].Columns["IsHalfDay"].Hidden = true;
            uGrid.DisplayLayout.Bands[0].Columns["IsActive"].Hidden = true;
            uGrid.DisplayLayout.Bands[0].Columns["IsHalfDayDefault"].Hidden = true;
            uGrid.DisplayLayout.Bands[0].Columns["OT"].Hidden = false;
            uGrid.DisplayLayout.Bands[0].Columns["TimeSheetSymbolsCode"].CellActivation = Activation.NoEdit;
            uGrid.DisplayLayout.Bands[0].Columns["TimeSheetSymbolsName"].CellActivation = Activation.NoEdit;
            uGrid.DisplayLayout.Bands[0].Columns["IsDefault"].CellActivation = Activation.NoEdit;
            uGrid.DisplayLayout.Bands[0].Columns["Selected"].CellActivation = Activation.AllowEdit;
            foreach (var row in uGrid.Rows)
            {
                //foreach (var cell in row.Cells)
                //{                   
                //    if (cell.Column.Key != "Selected") cell.Activation = Activation.NoEdit;
                //}
                if ((row.ListObject as TimeSheetSymbols).OverTimeSymbol != null && (row.ListObject as TimeSheetSymbols).OverTimeSymbol >= 0)
                {
                    row.Cells["OT"].Activation = Activation.AllowEdit;
                }
                else row.Cells["OT"].Activation = Activation.NoEdit;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            var lstTSSymbol = from u in (BindingList<TimeSheetSymbols>)uGrid.DataSource where u.Selected select u;
            if (lstTSSymbol.Count() == 0)
            {
                MSG.Error("Bạn phải chọn ít nhât một ký hiệu chấm công");
                return;
            }
            if (lstTSSymbol.Any(n => n.OT == null && n.IsActive && n.TimeSheetSymbolsCode.Contains("LT") || n.OT == 0 && n.IsActive && n.TimeSheetSymbolsCode.Contains("LT")))
            {
                MSG.Error("Bạn chưa nhập thời gian làm thêm");
                return;
            }
            SelectTSSymbols = lstTSSymbol.ToList();
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            uGrid.Update();
            if (!e.Cell.Row.Cells[e.Cell.Column.Key].IsFilterRowCell)
            {
                switch (e.Cell.Column.Key)
                {
                    case "Selected":
                        e.Cell.Row.UpdateData();
                        if ((bool)e.Cell.Row.Cells["Selected"].Value)
                        {
                            bool isHalf = (bool)e.Cell.Row.Cells["IsHalfDay"].Value;
                            bool isDefaut = (bool)e.Cell.Row.Cells["IsDefault"].Value;
                            if (isHalf)
                            {
                                foreach (var row in uGrid.Rows)
                                {
                                    if ((bool)row.Cells["IsDefault"].Value && (bool)row.Cells["Selected"].Value)
                                    {
                                        MSG.Warning("Không được chọn đồng thời ký hiệu chấm công cả ngày và nửa ngày!");
                                        e.Cell.Row.Cells["Selected"].Value = false;
                                        break;
                                    }
                                }
                            }
                            else if (isDefaut)
                            {
                                foreach (var row in uGrid.Rows)
                                {
                                    if ((bool)row.Cells["IsHalfDay"].Value && (bool)row.Cells["Selected"].Value)
                                    {
                                        MSG.Warning("Không được chọn đồng thời ký hiệu chấm công cả ngày và nửa ngày!");
                                        e.Cell.Row.Cells["Selected"].Value = false;
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
