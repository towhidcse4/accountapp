﻿namespace Accounting
{
    partial class FInsurancePaymentPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnApply = new Infragistics.Win.Misc.UltraButton();
            this.ugridDepartment = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbCompanyAccountBank = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.dtBeginDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.cbbCurrentState = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.ugridDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCompanyAccountBank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrentState)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            appearance1.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance1;
            this.btnClose.Location = new System.Drawing.Point(717, 508);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 340;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnApply
            // 
            appearance2.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnApply.Appearance = appearance2;
            this.btnApply.Location = new System.Drawing.Point(636, 508);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 30);
            this.btnApply.TabIndex = 341;
            this.btnApply.Text = "Đồng ý";
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // ugridDepartment
            // 
            this.ugridDepartment.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ugridDepartment.Location = new System.Drawing.Point(10, 40);
            this.ugridDepartment.Name = "ugridDepartment";
            this.ugridDepartment.Size = new System.Drawing.Size(782, 462);
            this.ugridDepartment.TabIndex = 3018;
            this.ugridDepartment.Text = "Danh sách phòng ban";
            this.ugridDepartment.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.ugridDepartment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ugridDepartment_KeyDown);
            // 
            // ultraLabel2
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance3;
            this.ultraLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel2.Location = new System.Drawing.Point(577, 13);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(86, 22);
            this.ultraLabel2.TabIndex = 3032;
            this.ultraLabel2.Text = "Tài khoản chi";
            // 
            // cbbCompanyAccountBank
            // 
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbCompanyAccountBank.DisplayLayout.Appearance = appearance4;
            this.cbbCompanyAccountBank.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbCompanyAccountBank.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance5.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance5.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCompanyAccountBank.DisplayLayout.GroupByBox.Appearance = appearance5;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCompanyAccountBank.DisplayLayout.GroupByBox.BandLabelAppearance = appearance6;
            this.cbbCompanyAccountBank.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance7.BackColor2 = System.Drawing.SystemColors.Control;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCompanyAccountBank.DisplayLayout.GroupByBox.PromptAppearance = appearance7;
            this.cbbCompanyAccountBank.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbCompanyAccountBank.DisplayLayout.MaxRowScrollRegions = 1;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbCompanyAccountBank.DisplayLayout.Override.ActiveCellAppearance = appearance8;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbCompanyAccountBank.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.cbbCompanyAccountBank.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbCompanyAccountBank.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.cbbCompanyAccountBank.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            appearance11.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbCompanyAccountBank.DisplayLayout.Override.CellAppearance = appearance11;
            this.cbbCompanyAccountBank.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbCompanyAccountBank.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCompanyAccountBank.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            appearance13.TextHAlignAsString = "Left";
            this.cbbCompanyAccountBank.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.cbbCompanyAccountBank.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbCompanyAccountBank.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.cbbCompanyAccountBank.DisplayLayout.Override.RowAppearance = appearance14;
            this.cbbCompanyAccountBank.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbCompanyAccountBank.DisplayLayout.Override.TemplateAddRowAppearance = appearance15;
            this.cbbCompanyAccountBank.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbCompanyAccountBank.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbCompanyAccountBank.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbCompanyAccountBank.Enabled = false;
            this.cbbCompanyAccountBank.Location = new System.Drawing.Point(669, 12);
            this.cbbCompanyAccountBank.Name = "cbbCompanyAccountBank";
            this.cbbCompanyAccountBank.Size = new System.Drawing.Size(123, 22);
            this.cbbCompanyAccountBank.TabIndex = 3031;
            // 
            // ultraLabel1
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance16;
            this.ultraLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel1.Location = new System.Drawing.Point(11, 11);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(59, 22);
            this.ultraLabel1.TabIndex = 3036;
            this.ultraLabel1.Text = "Ngày nộp";
            // 
            // dtBeginDate
            // 
            appearance17.TextHAlignAsString = "Center";
            appearance17.TextVAlignAsString = "Middle";
            this.dtBeginDate.Appearance = appearance17;
            this.dtBeginDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtBeginDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtBeginDate.Location = new System.Drawing.Point(76, 12);
            this.dtBeginDate.MaskInput = "";
            this.dtBeginDate.Name = "dtBeginDate";
            this.dtBeginDate.Size = new System.Drawing.Size(99, 21);
            this.dtBeginDate.TabIndex = 3035;
            this.dtBeginDate.Value = null;
            this.dtBeginDate.ValueChanged += new System.EventHandler(this.dtBeginDate_ValueChanged);
            // 
            // cbbCurrentState
            // 
            this.cbbCurrentState.AutoSize = false;
            this.cbbCurrentState.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCurrentState.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem4.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem4.DataValue = "115";
            valueListItem4.DisplayText = "Tiền mặt";
            valueListItem5.DataValue = "125";
            valueListItem5.DisplayText = "Ủy nhiệm chi";
            this.cbbCurrentState.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem4,
            valueListItem5});
            this.cbbCurrentState.Location = new System.Drawing.Point(356, 12);
            this.cbbCurrentState.Name = "cbbCurrentState";
            this.cbbCurrentState.Size = new System.Drawing.Size(203, 22);
            this.cbbCurrentState.TabIndex = 3034;
            this.cbbCurrentState.ValueMember = "0";
            this.cbbCurrentState.ValueChanged += new System.EventHandler(this.cbbCurrentState_ValueChanged);
            // 
            // ultraLabel4
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance18;
            this.ultraLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel4.Location = new System.Drawing.Point(197, 12);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(153, 22);
            this.ultraLabel4.TabIndex = 3033;
            this.ultraLabel4.Text = "Phương thức thanh toán";
            // 
            // FInsurancePaymentPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 545);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.dtBeginDate);
            this.Controls.Add(this.cbbCurrentState);
            this.Controls.Add(this.ultraLabel4);
            this.Controls.Add(this.ultraLabel2);
            this.Controls.Add(this.cbbCompanyAccountBank);
            this.Controls.Add(this.ugridDepartment);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnApply);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FInsurancePaymentPopup";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nộp tiền bảo hiểm";
            ((System.ComponentModel.ISupportInitialize)(this.ugridDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCompanyAccountBank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrentState)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnApply;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugridDepartment;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCompanyAccountBank;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtBeginDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbCurrentState;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
    }
}