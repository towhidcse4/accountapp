﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using Accounting.Core.IService;

namespace Accounting
{
    public partial class FInsurancePaymentPopup : CustormForm
    {
        List<InsurancePayment> lst = new List<InsurancePayment>();
        private readonly IAccountingObjectBankAccountService _IAccountingObjectBankAccountService = Utils.IAccountingObjectBankAccountService;
        public FInsurancePaymentPopup()
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            var dte = Utils.GetDbStartDate().StringToDateTime();
            dtBeginDate.Value = dte.HasValue ? dte.Value : DateTime.Now;
            cbbCurrentState.SelectedIndex = 0;

            //lst = new List<InsurancePayment>
            //{
            //    new InsurancePayment
            //    {
            //        Description = "BHXH và BH tai nạn lao động",
            //    },
            //    new InsurancePayment
            //    {
            //        Description = "Bảo hiểm y tế",
            //    },
            //    new InsurancePayment
            //    {
            //        Description = "Bảo hiểm thất nghiệp",
            //    },
            //    new InsurancePayment
            //    {
            //        Description = "Kinh phí công đoàn",
            //    }
            //};
            ugridDepartment.DataSource = lst;
            Utils.ConfigGrid(ugridDepartment, ConstDatabase.InsurancePayment_TableName);
            //ReportUtils.ConfigGridColumnCheck(ugridDepartment);
            ugridDepartment.DisplayLayout.Bands[0].Summaries.Clear();
            ugridDepartment.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            ugridDepartment.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
            ugridDepartment.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            ugridDepartment.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            UltraGridBand band = ugridDepartment.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Check"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugridDepartment.DisplayLayout.Bands[0].Columns["Check"].CellActivation = Activation.AllowEdit;
            band.Columns["Check"].Header.Fixed = true;
            Fillcbb();
            foreach (var c in band.Columns)
            {
                if (c.Key != "Check" && c.Key != "Amount") c.CellActivation = Activation.NoEdit;
            }
            foreach (var column in ugridDepartment.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, ugridDepartment);
            }
            WaitingFrm.StopWaiting();
        }
        public void Fillcbb()
        {
            this.ConfigCombo(Utils.ListBankAccountDetail, cbbCompanyAccountBank, "bankAccount", "ID");
        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            List<InsurancePayment> checkeds = lst.Where(x => x.Check && x.Amount > 0).ToList();
            if (checkeds.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn loại Bảo hiểm muốn nộp hoặc tiền nộp bảo hiểm bằng 0");
                return;
            }
            if (cbbCurrentState.Value.ToInt() == 125)
            {
                if (cbbCompanyAccountBank.Value == null)
                {
                    MSG.Warning("Bạn chưa chọn tài khoản chi");
                    return;
                }
                foreach (var item in checkeds)
                {
                    if (item.AccountNumber == "")
                    {
                        MSG.Error("Bạn phải thêm Số tài khoản của Nhân viên vào trước khi thanh toán bằng Ủy Nhiệm Chi");
                        return;
                    }
                }
            }
            foreach (InsurancePayment check in checkeds)
            {
                if (check.Amount > check.TotalAmount)
                {
                    MSG.Warning(string.Format("Số tiền nộp lần này của {0} phải nhỏ hơn hoặc bằng số tiền phải nộp", check.Description));
                    return;
                }
            }

            DateTime date = (DateTime)dtBeginDate.Value;
            if (cbbCurrentState.Value.ToInt() == 115)
            {
                List<MCPaymentDetailInsurance> MCPaymentDetailInsurances = new List<MCPaymentDetailInsurance>();
                List<MCPaymentDetail> MCPaymentDetails = new List<MCPaymentDetail>();
                MCPayment mCPayment = new MCPayment()
                {
                    ID = Guid.NewGuid(),
                    TypeID = cbbCurrentState.Value.ToInt(),
                    No = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(11)),
                    TemplateID = new Guid("4B7429C8-FAAC-4CF0-883E-3CEE00B6C768"),
                    TotalAmount = checkeds.Sum(x => x.Amount),
                    TotalAmountOriginal = checkeds.Sum(x => x.Amount),
                    TotalAll = checkeds.Sum(x => x.Amount),
                    TotalAllOriginal = checkeds.Sum(x => x.Amount),
                    PostedDate = new DateTime(date.Year, date.Month, date.Day),
                    Date = new DateTime(date.Year, date.Month, date.Day),
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    Reason = "Nộp tiền bảo hiểm",
                    Recorded = true,
                    AccountingObjectType = 3
                };

                foreach (InsurancePayment ip in checkeds)
                {
                    MCPaymentDetailInsurance paymentDetail = new MCPaymentDetailInsurance()
                    {
                        DebitAccount = ip.DebitAccount,
                        AccumAmount = ip.TotalAmount,
                        PayAmount = ip.Amount,
                        RemainningAmount = ip.TotalAmount - ip.Amount,
                        Description = ip.Description,
                        MCPaymentID = mCPayment.ID,
                        CreditAccount = "1111",

                    };
                    MCPaymentDetail detail = new MCPaymentDetail()
                    {
                        CreditAccount = "1111",
                        DebitAccount = ip.DebitAccount,
                        Amount = ip.Amount,
                        AmountOriginal = ip.Amount,
                        Description = string.Format("Nộp tiền bảo hiểm tháng {0} năm {1}", date.Month, date.Year),
                        MCPaymentID = mCPayment.ID,
                    };
                    MCPaymentDetails.Add(detail);
                    MCPaymentDetailInsurances.Add(paymentDetail);
                }
                try
                {
                    mCPayment.MCPaymentDetailInsurances = MCPaymentDetailInsurances;
                    mCPayment.MCPaymentDetails = MCPaymentDetails;
                    //Utils.IMCPaymentService.BeginTran();
                    //Utils.IMCPaymentService.Save(mCPayment);
                    ////foreach (MCPaymentDetailInsurance detiail in MCPaymentDetailInsurances)
                    ////{
                    ////    Utils.IMCPaymentDetailInsuranceService.Save(detiail);
                    ////}
                    //List<GeneralLedger> generalLedgers = GenGeneralLedgers(mCPayment);
                    //foreach (GeneralLedger detiail in generalLedgers)
                    //{
                    //    Utils.IGeneralLedgerService.Save(detiail);
                    //}
                    //bool status = Utils.IGenCodeService.UpdateGenCodeForm(11, mCPayment.No, mCPayment.ID);
                    //if (!status)
                    //{
                    //    MSG.Warning(resSystem.MSG_System_52);
                    //    Utils.IMBTellerPaperService.RolbackTran();
                    //    return;
                    //}
                    //Utils.IMCPaymentService.CommitTran();
                    //if (MSG.Question(string.Format("Hệ thống đã sinh chứng từ thanh toán bảo hiểm số {0}. Bạn có muốn xem chứng từ vừa tạo không", mCPayment.No)) == DialogResult.Yes)
                    //{
                    new FMCPaymentDetail(mCPayment, new List<MCPayment> { mCPayment }, ConstFrm.optStatusForm.Edit).ShowDialog(this);
                    ugridDepartment.DataSource = lst = Utils.IGOtherVoucherService.FindInsurancePayment(date);
                    //}
                }
                catch (Exception ex)
                {
                    Utils.IMCPaymentService.RolbackTran();
                    MSG.Warning("Có lỗi xảy ra \n\r" + ex.Message);
                }
            }
            else
            {
                List<MBTellerPaperDetailInsurance> MBTellerPaperDetailInsurances = new List<MBTellerPaperDetailInsurance>();
                List<MBTellerPaperDetail> MBTellerPaperDetails = new List<MBTellerPaperDetail>();
                MBTellerPaper MBTellerPaper = new MBTellerPaper()
                {
                    ID = Guid.NewGuid(),
                    TypeID = cbbCurrentState.Value.ToInt(),
                    No = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(12)),
                    TemplateID = new Guid("79092FB0-428E-45C9-84C7-29C5ED4CCF41"),
                    TotalAmount = checkeds.Sum(x => x.Amount),
                    TotalAmountOriginal = checkeds.Sum(x => x.Amount),
                    TotalAll = checkeds.Sum(x => x.Amount),
                    TotalAllOriginal = checkeds.Sum(x => x.Amount),
                    PostedDate = new DateTime(date.Year, date.Month, date.Day),
                    Date = new DateTime(date.Year, date.Month, date.Day),
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    Reason = "Nộp tiền bảo hiểm",
                    Recorded = true,
                    AccountingObjectType = 3,
                    BankAccountDetailID = (Guid)cbbCompanyAccountBank.Value,
                    BankName = Utils.ListBankAccountDetail.FirstOrDefault(n => n.ID == (Guid)cbbCompanyAccountBank.Value).BankName
                };

                foreach (InsurancePayment ip in checkeds)
                {
                    MBTellerPaperDetailInsurance paymentDetail = new MBTellerPaperDetailInsurance()
                    {
                        DebitAccount = ip.DebitAccount,
                        AccumAmount = ip.TotalAmount,
                        PayAmount = ip.Amount,
                        RemainningAmount = ip.TotalAmount - ip.Amount,
                        Description = ip.Description,
                        MBTellerPaperID = MBTellerPaper.ID,
                        CreditAccount = "1121"

                    };
                    MBTellerPaperDetail detail = new MBTellerPaperDetail()
                    {
                        CreditAccount = "1121",
                        DebitAccount = ip.DebitAccount,
                        Amount = ip.Amount,
                        AmountOriginal = ip.Amount,
                        Description = string.Format("Nộp tiền bảo hiểm tháng {0} năm {1}", date.Month, date.Year),
                        MBTellerPaperID = MBTellerPaper.ID,
                    };
                    MBTellerPaperDetails.Add(detail);
                    MBTellerPaperDetailInsurances.Add(paymentDetail);
                }
                try
                {
                    MBTellerPaper.MBTellerPaperDetailInsurances = MBTellerPaperDetailInsurances;
                    MBTellerPaper.MBTellerPaperDetails = MBTellerPaperDetails;
                    //Utils.IMBTellerPaperService.BeginTran();
                    //Utils.IMBTellerPaperService.Save(MBTellerPaper);

                    //List<GeneralLedger> generalLedgers = GenGeneralLedgers(MBTellerPaper);
                    //foreach (GeneralLedger detiail in generalLedgers)
                    //{
                    //    Utils.IGeneralLedgerService.Save(detiail);
                    //}
                    //bool status = Utils.IGenCodeService.UpdateGenCodeForm(12, MBTellerPaper.No, MBTellerPaper.ID);
                    //if (!status)
                    //{
                    //    MSG.Warning(resSystem.MSG_System_52);
                    //    Utils.IMBTellerPaperService.RolbackTran();
                    //    return;
                    //}
                    //Utils.IMBTellerPaperService.CommitTran();
                    //if (MSG.Question(string.Format("Hệ thống đã sinh chứng từ thanh toán bảo hiểm số {0}. Bạn có muốn xem chứng từ vừa tạo không", MBTellerPaper.No)) == DialogResult.Yes)
                    //{
                    new FMBTellerPaperDetail(MBTellerPaper, new List<MBTellerPaper> { MBTellerPaper }, ConstFrm.optStatusForm.Edit).ShowDialog(this);
                    ugridDepartment.DataSource = lst = Utils.IGOtherVoucherService.FindInsurancePayment(date);
                    //}
                }
                catch (Exception ex)
                {
                    Utils.IMBTellerPaperService.RolbackTran();
                    MSG.Warning("Có lỗi xảy ra \n\r" + ex.Message);
                }

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dtBeginDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime date = (DateTime)dtBeginDate.Value;
                ugridDepartment.DataSource = lst = Utils.IGOtherVoucherService.FindInsurancePayment(date);
            }
            catch
            {
                ugridDepartment.DataSource = lst = new List<InsurancePayment>();
            }

        }

        List<GeneralLedger> GenGeneralLedgers(MBTellerPaper mbTellerPaper)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

            #region Cặp Nợ-Có

            for (int i = 0; i < mbTellerPaper.MBTellerPaperDetails.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mbTellerPaper.ID,
                    TypeID = mbTellerPaper.TypeID,
                    Date = mbTellerPaper.Date,
                    PostedDate = mbTellerPaper.PostedDate,
                    No = mbTellerPaper.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = mbTellerPaper.MBTellerPaperDetailInsurances[i].DebitAccount,
                    AccountCorresponding = mbTellerPaper.MBTellerPaperDetailInsurances[i].CreditAccount,
                    BankAccountDetailID = mbTellerPaper.BankAccountDetailID,
                    CurrencyID = mbTellerPaper.CurrencyID,
                    ExchangeRate = mbTellerPaper.ExchangeRate,
                    DebitAmount = mbTellerPaper.MBTellerPaperDetailInsurances[i].PayAmount,
                    DebitAmountOriginal = mbTellerPaper.MBTellerPaperDetailInsurances[i].PayAmount,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = mbTellerPaper.Reason,
                    Description = mbTellerPaper.MBTellerPaperDetailInsurances[i].Description,
                    AccountingObjectID = mbTellerPaper.MBTellerPaperDetailInsurances[i].AccountingObjectID ?? mbTellerPaper.AccountingObjectID,
                    EmployeeID = mbTellerPaper.EmployeeID,
                    BudgetItemID = mbTellerPaper.MBTellerPaperDetailInsurances[i].BudgetItemID,
                    CostSetID = mbTellerPaper.MBTellerPaperDetailInsurances[i].CostSetID,
                    ContractID = mbTellerPaper.MBTellerPaperDetailInsurances[i].ContractID,
                    StatisticsCodeID = mbTellerPaper.MBTellerPaperDetailInsurances[i].StatisticsCodeID,
                    InvoiceSeries = "",
                    ContactName = mbTellerPaper.Receiver,
                    DetailID = mbTellerPaper.MBTellerPaperDetailInsurances[i].ID,
                    RefNo = mbTellerPaper.No,
                    RefDate = mbTellerPaper.Date,
                    DepartmentID = mbTellerPaper.MBTellerPaperDetailInsurances[i].DepartmentID,
                    ExpenseItemID = mbTellerPaper.MBTellerPaperDetailInsurances[i].ExpenseItemID,
                };

                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mbTellerPaper.ID,
                    TypeID = mbTellerPaper.TypeID,
                    Date = mbTellerPaper.Date,
                    PostedDate = mbTellerPaper.PostedDate,
                    No = mbTellerPaper.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = mbTellerPaper.MBTellerPaperDetailInsurances[i].CreditAccount,
                    AccountCorresponding = mbTellerPaper.MBTellerPaperDetailInsurances[i].DebitAccount,
                    BankAccountDetailID = mbTellerPaper.BankAccountDetailID,
                    CurrencyID = mbTellerPaper.CurrencyID,
                    ExchangeRate = mbTellerPaper.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = mbTellerPaper.MBTellerPaperDetailInsurances[i].PayAmount,
                    CreditAmountOriginal = mbTellerPaper.MBTellerPaperDetailInsurances[i].PayAmount,
                    Reason = mbTellerPaper.Reason,
                    Description = mbTellerPaper.MBTellerPaperDetailInsurances[i].Description,
                    AccountingObjectID = mbTellerPaper.MBTellerPaperDetailInsurances[i].AccountingObjectID ?? mbTellerPaper.AccountingObjectID,
                    EmployeeID = mbTellerPaper.EmployeeID,
                    BudgetItemID = mbTellerPaper.MBTellerPaperDetailInsurances[i].BudgetItemID,
                    CostSetID = mbTellerPaper.MBTellerPaperDetailInsurances[i].CostSetID,
                    ContractID = mbTellerPaper.MBTellerPaperDetailInsurances[i].ContractID,
                    StatisticsCodeID = mbTellerPaper.MBTellerPaperDetailInsurances[i].StatisticsCodeID,
                    InvoiceSeries = "",
                    ContactName = mbTellerPaper.Receiver,
                    DetailID = mbTellerPaper.MBTellerPaperDetailInsurances[i].ID,
                    RefNo = mbTellerPaper.No,
                    RefDate = mbTellerPaper.Date,
                    DepartmentID = mbTellerPaper.MBTellerPaperDetailInsurances[i].DepartmentID,
                    ExpenseItemID = mbTellerPaper.MBTellerPaperDetailInsurances[i].ExpenseItemID,
                };
                listGenTemp.Add(genTempCorresponding);
            }

            #endregion

            return listGenTemp;
        }

        List<GeneralLedger> GenGeneralLedgers(MCPayment mcPayment)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

            #region Cặp Nợ-Có

            for (int i = 0; i < mcPayment.MCPaymentDetailInsurances.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mcPayment.ID,
                    TypeID = mcPayment.TypeID,
                    Date = mcPayment.Date,
                    PostedDate = mcPayment.PostedDate,
                    No = mcPayment.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = mcPayment.MCPaymentDetailInsurances[i].DebitAccount,
                    AccountCorresponding = mcPayment.MCPaymentDetailInsurances[i].CreditAccount,
                    CurrencyID = mcPayment.CurrencyID,
                    ExchangeRate = mcPayment.ExchangeRate,
                    DebitAmount = mcPayment.MCPaymentDetailInsurances[i].PayAmount,
                    DebitAmountOriginal = mcPayment.MCPaymentDetailInsurances[i].PayAmount,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = mcPayment.Reason,
                    Description = mcPayment.MCPaymentDetailInsurances[i].Description,
                    AccountingObjectID = mcPayment.MCPaymentDetailInsurances[i].AccountingObjectID ?? mcPayment.AccountingObjectID,
                    EmployeeID = mcPayment.EmployeeID,
                    BudgetItemID = mcPayment.MCPaymentDetailInsurances[i].BudgetItemID,
                    CostSetID = mcPayment.MCPaymentDetailInsurances[i].CostSetID,
                    ContractID = mcPayment.MCPaymentDetailInsurances[i].ContractID,
                    StatisticsCodeID = mcPayment.MCPaymentDetailInsurances[i].StatisticsCodeID,
                    InvoiceSeries = "",
                    ContactName = mcPayment.Receiver,
                    DetailID = mcPayment.MCPaymentDetailInsurances[i].ID,
                    RefNo = mcPayment.No,
                    RefDate = mcPayment.Date,
                    DepartmentID = mcPayment.MCPaymentDetailInsurances[i].DepartmentID,
                    ExpenseItemID = mcPayment.MCPaymentDetailInsurances[i].ExpenseItemID,
                };
                if (!(string.IsNullOrEmpty(mcPayment.MCPaymentDetailInsurances[i].DebitAccount)) &&
                    (((mcPayment.MCPaymentDetailInsurances[i].DebitAccount.StartsWith("133"))
                      || mcPayment.MCPaymentDetailInsurances[i].DebitAccount.StartsWith("33311"))))
                {
                    genTemp.VATDescription = mcPayment.MCPaymentDetailTaxs.Count > i ? mcPayment.MCPaymentDetailTaxs[i].Description : null;
                }
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mcPayment.ID,
                    TypeID = mcPayment.TypeID,
                    Date = mcPayment.Date,
                    PostedDate = mcPayment.PostedDate,
                    No = mcPayment.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = mcPayment.MCPaymentDetailInsurances[i].CreditAccount,
                    AccountCorresponding = mcPayment.MCPaymentDetailInsurances[i].DebitAccount,
                    CurrencyID = mcPayment.CurrencyID,
                    ExchangeRate = mcPayment.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = mcPayment.MCPaymentDetailInsurances[i].PayAmount,
                    CreditAmountOriginal = mcPayment.MCPaymentDetailInsurances[i].PayAmount,
                    Reason = mcPayment.Reason,
                    Description = mcPayment.MCPaymentDetailInsurances[i].Description,
                    AccountingObjectID = mcPayment.MCPaymentDetailInsurances[i].AccountingObjectID ?? mcPayment.AccountingObjectID,
                    EmployeeID = mcPayment.EmployeeID,
                    BudgetItemID = mcPayment.MCPaymentDetailInsurances[i].BudgetItemID,
                    CostSetID = mcPayment.MCPaymentDetailInsurances[i].CostSetID,
                    ContractID = mcPayment.MCPaymentDetailInsurances[i].ContractID,
                    StatisticsCodeID = mcPayment.MCPaymentDetailInsurances[i].StatisticsCodeID,
                    InvoiceSeries = "",
                    ContactName = mcPayment.Receiver,
                    DetailID = mcPayment.MCPaymentDetailInsurances[i].ID,
                    RefNo = mcPayment.No,
                    RefDate = mcPayment.Date,
                    DepartmentID = mcPayment.MCPaymentDetailInsurances[i].DepartmentID,
                    ExpenseItemID = mcPayment.MCPaymentDetailInsurances[i].ExpenseItemID,
                };
                if ((!string.IsNullOrEmpty(mcPayment.MCPaymentDetailInsurances[i].DebitAccount)) &&
                    (((mcPayment.MCPaymentDetailInsurances[i].DebitAccount.StartsWith("133"))
                      || mcPayment.MCPaymentDetailInsurances[i].DebitAccount.StartsWith("33311"))))
                {
                    genTempCorresponding.VATDescription = mcPayment.MCPaymentDetailTaxs.Count > i ? mcPayment.MCPaymentDetailTaxs[i].Description : null;
                }
                listGenTemp.Add(genTempCorresponding);
            }

            #endregion

            return listGenTemp;
        }

        private void cbbCurrentState_ValueChanged(object sender, EventArgs e)
        {
            cbbCompanyAccountBank.Enabled = cbbCurrentState.Value.ToInt() == 125;
        }

        private void ugridDepartment_KeyDown(object sender, KeyEventArgs e)
        {
            UltraGrid uGrid = (UltraGrid)sender;
            if (uGrid.ActiveCell != null)
            {
                if (e.KeyCode == Keys.Up)
                {
                    uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                    uGrid.PerformAction(UltraGridAction.AboveCell, false, false);
                    e.Handled = true;
                    uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                }
                if (e.KeyCode == Keys.Down)
                {
                    uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                    uGrid.PerformAction(UltraGridAction.BelowCell, false, false);
                    e.Handled = true;
                    uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                }
                if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Right)
                {
                    uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                    uGrid.PerformAction(UltraGridAction.NextCellByTab, false, false);
                    e.Handled = true;
                    uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                }
                if (e.KeyCode == Keys.Left)
                {
                    uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                    uGrid.PerformAction(UltraGridAction.PrevCellByTab, false, false);
                    e.Handled = true;
                    uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                }
            }
        }
    }
}
