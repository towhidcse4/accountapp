﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

namespace Accounting
{
    public partial class FWagePaymentDetailPopup : CustormForm
    {
        public FWagePaymentDetailPopup()
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            var dte = Utils.GetDbStartDate().StringToDateTime();

            ugridDepartment.DataSource = new List<WagePaymentDetail>();
            Utils.ConfigGrid(ugridDepartment, ConstDatabase.WagePaymentDetail_TableName);

            UltraGridBand band = ugridDepartment.DisplayLayout.Bands[0];
            foreach (var c in band.Columns)
            {
                c.CellActivation = Activation.NoEdit;
            }
            foreach (var column in ugridDepartment.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, ugridDepartment);
            }
            cbbEmployeeID.DataSource = Utils.IAccountingObjectService.GetListAccountingObjectByEmployee(true);
            cbbEmployeeID.DisplayMember = "AccountingObjectName";
            Utils.ConfigGrid(cbbEmployeeID, ConstDatabase.WagePayment_TableName);
            WaitingFrm.StopWaiting();

        } 

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cbbEmployeeID_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (cbbEmployeeID.Value.ToString().IsNullOrEmpty() || cbbEmployeeID.SelectedRow == null) return;
            ugridDepartment.DataSource = Utils.IGOtherVoucherService.WagePaymentDetail(((AccountingObject)cbbEmployeeID.SelectedRow.ListObject).ID);
        }

        private void cbbEmployeeID_Validated(object sender, EventArgs e)
        {
           
            if (false == cbbEmployeeID.IsItemInList() && cbbEmployeeID.Value != null)
            {
                MSG.Warning("Dữ liệu không có trong danh mục");
                cbbEmployeeID.Value = null;
            }
        }
    }
}
