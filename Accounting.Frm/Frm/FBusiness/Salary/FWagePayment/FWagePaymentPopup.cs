﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;

namespace Accounting
{
    public partial class FWagePaymentPopup : CustormForm
    {
        List<WagePayment> lst = new List<WagePayment>();
        private readonly IAccountingObjectBankAccountService _IAccountingObjectBankAccountService = Utils.IAccountingObjectBankAccountService;
        public FWagePaymentPopup()
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            var dte = Utils.GetDbStartDate().StringToDateTime();
            //dtBeginDate.Value = /*dte.HasValue ? dte :*/ DateTime.Now;
            cbbCurrentState.SelectedIndex = 0;

            ugridDepartment.DataSource = lst;
            Utils.ConfigGrid(ugridDepartment, ConstDatabase.WagePayment_TableName);
            //ReportUtils.ConfigGridColumnCheck(ugridDepartment);
            ugridDepartment.DisplayLayout.Bands[0].Summaries.Clear();
            ugridDepartment.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            ugridDepartment.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
            ugridDepartment.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            ugridDepartment.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            UltraGridBand band = ugridDepartment.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Check"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugridDepartment.DisplayLayout.Bands[0].Columns["Check"].CellActivation = Activation.AllowEdit;
            band.Columns["Check"].Header.Fixed = true;
            // cbb TK CHi
            Fillcbb();
            foreach (var c in band.Columns)
            {
                if (c.Key != "Check" && c.Key != "Amount" && c.Key != "AccountNumber") c.CellActivation = Activation.NoEdit;
            }

            foreach (var column in ugridDepartment.DisplayLayout.Bands[0].Columns)
            {
                if (column.Key != "AccountNumber")
                    this.ConfigEachColumn4Grid(0, column, ugridDepartment);
            }
            dtBeginDate.Value = DateTime.Now;
            WaitingFrm.StopWaiting();

        }

        #region Fill cbbCompanyAccountBank
        public void Fillcbb()
        {
            this.ConfigCombo(Utils.ListBankAccountDetail, cbbCompanyAccountBank, "bankAccount", "ID");
        }

        #endregion

        //Set giá trị cbb trong Grid
        private static EmbeddableEditorBase GetEmbeddableEditorBase<T>(Control.ControlCollection _this, List<T> inputItem, string displayMember, string valueMember, string nameTable)
        {
            UltraCombo cbb = new UltraCombo
            {
                Name = "dropDown1",
                Visible = false,
                DataSource = inputItem,
                ValueMember = valueMember,
                DisplayMember = displayMember
            };
            _this.Add(cbb);
            Utils.ConfigGrid(cbb, nameTable);
            EmbeddableEditorBase editor = new EditorWithCombo(new DefaultEditorOwner(new DefaultEditorOwnerSettings { ValueList = cbb }));
            return editor;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {

            List<WagePayment> checkeds = lst.Where(x => x.Check && x.Amount > 0).ToList();
            if (checkeds.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn nhân viên muốn thanh toán lương hoặc tổng tiền thanh toán lương bằng 0");
                return;
            }
            if (cbbCurrentState.Value.ToInt() == 121)
            {
                if (cbbCompanyAccountBank.Value == null)
                {
                    MSG.Warning("Bạn chưa chọn tài khoản chi");
                    return;
                }
                foreach (var item in checkeds)
                {
                    if (item.AccountNumber == "")
                    {
                        MSG.Error("Bạn phải thêm Số tài khoản của Nhân viên vào trước khi thanh toán bằng Ủy Nhiệm Chi");
                        return;
                    }
                }
            }
            foreach (WagePayment check in checkeds)
            {
                if (check.Amount > check.TotalAmount)
                {
                    MSG.Warning(string.Format("Số tiền phải trả lần này phải nhỏ hơn hoặc bằng số tiền phải trả"));
                    return;
                }
            }

            DateTime date = (DateTime)dtBeginDate.Value;
            if (cbbCurrentState.Value.ToInt() == 111)
            {
                List<MCPaymentDetailSalary> MCPaymentDetailSalarys = new List<MCPaymentDetailSalary>();
                List<MCPaymentDetail> MCPaymentDetails = new List<MCPaymentDetail>();
                MCPayment mCPayment = new MCPayment()
                {
                    ID = Guid.NewGuid(),
                    TypeID = cbbCurrentState.Value.ToInt(),
                    No = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(11)),
                    TemplateID = new Guid("07C5E7D2-4FD6-4029-B243-F7752A46C26F"),
                    TotalAmount = checkeds.Sum(x => x.Amount),
                    TotalAmountOriginal = checkeds.Sum(x => x.Amount),
                    TotalAll = checkeds.Sum(x => x.Amount),
                    TotalAllOriginal = checkeds.Sum(x => x.Amount),
                    PostedDate = new DateTime(date.Year, date.Month, date.Day),
                    Date = new DateTime(date.Year, date.Month, date.Day),
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    //Reason = string.Format("Trả lương nhân viên tháng {0} năm {1}", date.Month, date.Year),
                    Reason = "Thanh toán lương nhân viên",
                    Recorded = true,
                    AccountingObjectType = 2
                };

                foreach (WagePayment ip in checkeds)
                {
                    PSSalarySheetDetail salarySheetDetail = new PSSalarySheetDetail();
                    //var timeMax = Utils.ListGOtherVoucher.Where(n => n.TypeID == 840 && n.Date <= date).Max(n => n.Date);
                    //GOtherVoucher gOtherVoucher = Utils.ListGOtherVoucher.FirstOrDefault(n => n.TypeID == 840 && n.Date == timeMax);
                    //PSSalarySheet salarySheet = Utils.IPSSalarySheetService.GetAll().FirstOrDefault(y => y.Month == date.Month);
                    var newdate = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
                    var lstGO = Utils.ListGOtherVoucher.Where(n => n.TypeID == 840 && n.Date <= newdate).ToList().OrderByDescending(x => x.Date).ToList();
                    foreach (var item in lstGO)
                    {
                        GOtherVoucher gOtherVoucher = item;
                        PSSalarySheet salarySheet = Utils.IPSSalarySheetService.Getbykey(gOtherVoucher.PSSalarySheetID ?? Guid.Empty);
                        salarySheetDetail = salarySheet != null ? Utils.IPSSalarySheetDetailService.Query.FirstOrDefault(x => x.EmployeeID == ip.AccountingObjectID && x.PSSalarySheetID == salarySheet.ID) : new PSSalarySheetDetail();
                        if (salarySheetDetail != null) break;
                    }
                    if (salarySheetDetail == null)
                    {
                        MSG.Warning("Thanh toán lương không thành công!");
                        return;
                    }
                    MCPaymentDetailSalary paymentDetail = new MCPaymentDetailSalary()
                    {
                        EmployeeID = ip.AccountingObjectID,
                        Description = ip.AccountingObjectName,
                        DepartmentID = ip.DepartmentID ?? Guid.Empty,
                        //AccumAmountOriginal = ip.TotalAmount,
                        AccumAmountOriginal = ip.TotalAmount > salarySheetDetail.NetAmount ? ip.TotalAmount - salarySheetDetail.NetAmount : 0,
                        PayAmountOriginal = ip.Amount,
                        CurrentMonthAmountOriginal = ip.TotalAmount - ip.Amount,
                        MCPaymentID = mCPayment.ID,
                        //CreditAccount = "1111",
                        //DebitAccount = "334",
                    };

                    MCPaymentDetailSalarys.Add(paymentDetail);

                }
                MCPaymentDetail detail = new MCPaymentDetail()
                {
                    CreditAccount = "1111",
                    DebitAccount = "334",
                    Amount = checkeds.Sum(x => x.Amount),
                    AmountOriginal = checkeds.Sum(x => x.Amount),
                    //Description = string.Format("Trả lương nhân viên tháng {0} năm {1}", date.Month, date.Year),
                    Description = "Thanh toán lương nhân viên",
                    MCPaymentID = mCPayment.ID,
                };
                MCPaymentDetails.Add(detail);
                try
                {
                    mCPayment.MCPaymentDetailSalarys = MCPaymentDetailSalarys;
                    mCPayment.MCPaymentDetails = MCPaymentDetails;
                    Utils.IMCPaymentService.BeginTran();
                    Utils.IMCPaymentService.Save(mCPayment);
                    //foreach (MCPaymentDetailInsurance detiail in MCPaymentDetailSalarys)
                    //{
                    //    Utils.IMCPaymentDetailSalaryservice.Save(detiail);
                    //}
                    List<GeneralLedger> generalLedgers = GenGeneralLedgers(mCPayment);
                    foreach (GeneralLedger detiail in generalLedgers)
                    {
                        Utils.IGeneralLedgerService.Save(detiail);
                    }
                    bool status = Utils.IGenCodeService.UpdateGenCodeForm(11, mCPayment.No, mCPayment.ID);
                    if (!status)
                    {
                        MSG.Warning(resSystem.MSG_System_52);
                        Utils.IMBTellerPaperService.RolbackTran();
                        return;
                    }
                    Utils.IMCPaymentService.CommitTran();
                    if (MSG.Question(string.Format("Hệ thống đã sinh chứng từ thanh toán lương số {0}. Bạn có muốn xem chứng từ vừa tạo không", mCPayment.No)) == DialogResult.Yes)
                    {
                        new FMCPaymentDetail(mCPayment, new List<MCPayment> { mCPayment }, ConstFrm.optStatusForm.View).ShowDialog(this);
                    }
                    ugridDepartment.DataSource = lst = Utils.IGOtherVoucherService.FindWagePayment(date).OrderBy(x => x.DepartmentCode).ThenBy(c => c.AccountingObjectCode).ToList();
                    UltraGridBand band = ugridDepartment.DisplayLayout.Bands[0];
                    try
                    {
                        CheckState checkState = ugridDepartment.DisplayLayout.Bands[0].Columns[0].GetHeaderCheckedState(ugridDepartment.Rows);
                        ugridDepartment.DisplayLayout.Bands[0].Columns[0].SetHeaderCheckedState(ugridDepartment.Rows, false);
                        foreach (UltraGridRow row in ugridDepartment.Rows)
                        {
                            row.Cells[0].Value = checkState == CheckState.Checked ? true : false;
                        }
                        foreach (var row in ugridDepartment.Rows)
                        {
                            foreach (var c in band.Columns)
                            {
                                if (c.Key.Equals("AccountNumber"))
                                {
                                    List<AccountingObjectBankAccount> cbb = _IAccountingObjectBankAccountService.GetByAccountingObjectID((Guid)row.Cells["AccountingObjectID"].Value);
                                    row.Cells["AccountNumber"].Editor = GetEmbeddableEditorBase(Controls, cbb, "BankAccount", "BankAccount", ConstDatabase.AccountingObjectBankAccount_TableName); // Hautv Edit
                                    ugridDepartment.DisplayLayout.Bands[0].Columns["AccountNumber"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
                                }
                            }
                        }
                    }
                    catch { }
                }
                catch (Exception ex)
                {
                    Utils.IMCPaymentService.RolbackTran();
                    MSG.Warning("Có lỗi xảy ra \n\r" + ex.Message);
                }
            }
            else
            {
                List<MBTellerPaperDetailSalary> MBTellerPaperDetailSalarys = new List<MBTellerPaperDetailSalary>();
                List<MBTellerPaperDetail> MBTellerPaperDetails = new List<MBTellerPaperDetail>();
                MBTellerPaper MBTellerPaper = new MBTellerPaper()
                {
                    ID = Guid.NewGuid(),
                    TypeID = cbbCurrentState.Value.ToInt(),
                    No = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(12)),
                    TemplateID = new Guid("DE122782-0DF9-4041-86AC-6CBA5523D9D7"),
                    //TotalAmount = checkeds.Sum(x => x.Amount),
                    //TotalAmountOriginal = checkeds.Sum(x => x.Amount),  trungnq bỏ để sửa lỗi giữa tổng tiền thanh toán và số tiền thực thanh toán ở ủy nhiệm chi là không khớp nhau
                    //TotalAll = checkeds.Sum(x => x.Amount),
                    //TotalAllOriginal = checkeds.Sum(x => x.Amount),
                    PostedDate = new DateTime(date.Year, date.Month, date.Day),
                    Date = new DateTime(date.Year, date.Month, date.Day),
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    //Reason = string.Format("Trả lương nhân viên tháng {0} năm {1}", date.Month, date.Year),
                    Reason = "Thanh toán lương nhân viên",
                    Recorded = true,
                    AccountingObjectType = 2,
                    BankAccountDetailID = (Guid)cbbCompanyAccountBank.Value,
                    BankName = Utils.ListBankAccountDetail.FirstOrDefault(n => n.ID == (Guid)cbbCompanyAccountBank.Value).BankName
                };

                foreach (WagePayment ip in checkeds)
                {
                    PSSalarySheetDetail salarySheetDetail1 = new PSSalarySheetDetail();
                    var newdate = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
                    //var timeMax = Utils.ListGOtherVoucher.Where(n => n.TypeID == 840 && n.Date <= date).Max(n => n.Date);
                    //GOtherVoucher gOtherVoucher = Utils.ListGOtherVoucher.FirstOrDefault(n => n.TypeID == 840 && n.Date == timeMax);
                    List<GOtherVoucher> timeMax = Utils.ListGOtherVoucher.Where(n => n.TypeID == 840 && n.Date <= newdate).ToList().OrderByDescending(x => x.Date).ToList();
                    foreach (var item in timeMax)//trungnq thêm để lấy đc đúng bảng lương và chi tiết bảng lương, để sửa lỗi khi lập thanh toán lương bằng ủy nhiệm chi thì bị null chi tiết nhân viên thanh toán lương
                    {
                        GOtherVoucher gOtherVoucher1 = item;
                        PSSalarySheet salarySheet1 = Utils.IPSSalarySheetService.Getbykey(gOtherVoucher1.PSSalarySheetID ?? Guid.Empty);
                        salarySheetDetail1 = salarySheet1 != null ? Utils.IPSSalarySheetDetailService.Query.FirstOrDefault(x => x.EmployeeID == ip.AccountingObjectID && x.PSSalarySheetID == salarySheet1.ID) : new PSSalarySheetDetail();
                        if (salarySheetDetail1 != null) break;
                    }
                    if (salarySheetDetail1 == null)
                    {
                        MSG.Warning("Thanh toán lương không thành công!");
                        return;
                    }
                    //GOtherVoucher gOtherVoucher = timeMax.Count > 0 ? timeMax[0] : new GOtherVoucher();  trungnq comment vì lấy sai salarysheetdetail nên chi tiết ủy nhiệm chi bị null
                    //// Hautv comment vì bị duplicated tiền trả cho từng nhân viên
                    ////foreach (var item in gOtherVoucher)
                    ////{
                    //PSSalarySheet salarySheet = Utils.IPSSalarySheetService.Getbykey(gOtherVoucher.PSSalarySheetID ?? Guid.Empty);

                    ////PSSalarySheet salarySheet = Utils.IPSSalarySheetService.GetAll().FirstOrDefault(y => y.Month == date.Month);
                    //PSSalarySheetDetail salarySheetDetail = salarySheet != null ? Utils.IPSSalarySheetDetailService.Query.FirstOrDefault(x => x.EmployeeID == ip.AccountingObjectID && x.PSSalarySheetID == salarySheet.ID) : new PSSalarySheetDetail();
                    if (salarySheetDetail1 != null)
                    {
                        MBTellerPaperDetailSalary paymentDetail = new MBTellerPaperDetailSalary();

                        paymentDetail.EmployeeID = ip.AccountingObjectID;
                        paymentDetail.Description = ip.AccountingObjectName;
                        paymentDetail.DepartmentID = ip.DepartmentID ?? Guid.Empty;
                        //AccumAmountOriginal = ip.TotalAmount,
                        paymentDetail.AccumAmountOriginal = ip.TotalAmount > salarySheetDetail1.NetAmount ? ip.TotalAmount - salarySheetDetail1.NetAmount : 0;
                        paymentDetail.PayAmountOriginal = ip.Amount;
                        paymentDetail.CurrentMonthAmountOriginal = ip.TotalAmount - ip.Amount;
                        paymentDetail.MBTellerPaperID = MBTellerPaper.ID;
                        paymentDetail.BankAccountDetailID = Utils.ListAccountingObjectBank.FirstOrDefault(x => x.BankAccount == (ip.AccountNumber ?? "")).ID;

                        MBTellerPaperDetailSalarys.Add(paymentDetail);
                    }
                    //}
                }
                if (MBTellerPaperDetailSalarys.Count==0)
                {
                    MSG.Warning("Đối tượng đã chọn không có lương để thanh toán hoặc đã thanh toán rồi");
                    Utils.IMBTellerPaperService.RolbackTran();
                    return;
                }
                MBTellerPaper.TotalAll = MBTellerPaperDetailSalarys.Sum(x => x.PayAmountOriginal);//trungnq thêm để sửa lỗi giữa tổng tiền thanh toán và số tiền thực thanh toán ở ủy nhiệm chi là không khớp nhau
                MBTellerPaper.TotalAmount = MBTellerPaperDetailSalarys.Sum(x => x.PayAmountOriginal);
                MBTellerPaper.TotalAmountOriginal = MBTellerPaperDetailSalarys.Sum(x => x.PayAmountOriginal);
                MBTellerPaper.TotalAllOriginal = MBTellerPaperDetailSalarys.Sum(x => x.PayAmountOriginal);//trungnq
                MBTellerPaperDetail detail = new MBTellerPaperDetail()
                {
                    CreditAccount = "1121",
                    DebitAccount = "334",
                    Amount = MBTellerPaperDetailSalarys.Sum(x => x.PayAmountOriginal),
                    AmountOriginal = MBTellerPaperDetailSalarys.Sum(x => x.PayAmountOriginal),
                    //Description = string.Format("Trả lương nhân viên tháng {0} năm {1}", date.Month, date.Year),
                    Description = "Thanh toán lương nhân viên",
                    MBTellerPaperID = MBTellerPaper.ID,
                };
                MBTellerPaperDetails.Add(detail);
                try
                {
                    MBTellerPaper.MBTellerPaperDetailSalarys = MBTellerPaperDetailSalarys;
                    MBTellerPaper.MBTellerPaperDetails = MBTellerPaperDetails;
                    Utils.IMBTellerPaperService.BeginTran();
                    Utils.IMBTellerPaperService.Save(MBTellerPaper);

                    List<GeneralLedger> generalLedgers = GenGeneralLedgers(MBTellerPaper);
                    foreach (GeneralLedger detiail in generalLedgers)
                    {
                        Utils.IGeneralLedgerService.Save(detiail);
                    }
                    bool status = Utils.IGenCodeService.UpdateGenCodeForm(12, MBTellerPaper.No, MBTellerPaper.ID);
                    if (!status)
                    {
                        MSG.Warning(resSystem.MSG_System_52);
                        Utils.IMBTellerPaperService.RolbackTran();
                        return;
                    }
                    Utils.IMBTellerPaperService.CommitTran();
                    if (MSG.Question(string.Format("Hệ thống đã sinh chứng từ thanh toán lương số {0}. Bạn có muốn xem chứng từ vừa tạo không", MBTellerPaper.No)) == DialogResult.Yes)
                    {
                        new FMBTellerPaperDetail(MBTellerPaper, new List<MBTellerPaper> { MBTellerPaper }, ConstFrm.optStatusForm.View).ShowDialog(this);
                    }
                    ugridDepartment.DataSource = lst = Utils.IGOtherVoucherService.FindWagePayment(date).OrderBy(x => x.DepartmentCode).ThenBy(c => c.AccountingObjectCode).ToList();
                    UltraGridBand band = ugridDepartment.DisplayLayout.Bands[0];
                    try
                    {
                        CheckState checkState = ugridDepartment.DisplayLayout.Bands[0].Columns[0].GetHeaderCheckedState(ugridDepartment.Rows);
                        ugridDepartment.DisplayLayout.Bands[0].Columns[0].SetHeaderCheckedState(ugridDepartment.Rows, false);
                        foreach (UltraGridRow row in ugridDepartment.Rows)
                        {
                            row.Cells[0].Value = checkState == CheckState.Checked ? true : false;
                        }
                        foreach (var row in ugridDepartment.Rows)
                        {
                            foreach (var c in band.Columns)
                            {
                                if (c.Key.Equals("AccountNumber"))
                                {
                                    List<AccountingObjectBankAccount> cbb = _IAccountingObjectBankAccountService.GetByAccountingObjectID((Guid)row.Cells["AccountingObjectID"].Value);
                                    row.Cells["AccountNumber"].Editor = GetEmbeddableEditorBase(Controls, cbb, "BankAccount", "BankAccount", ConstDatabase.AccountingObjectBankAccount_TableName); // Hautv Edit
                                    ugridDepartment.DisplayLayout.Bands[0].Columns["AccountNumber"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
                                }
                            }
                        }
                    }
                    catch { }
                }
                catch (Exception ex)
                {
                    Utils.IMBTellerPaperService.RolbackTran();
                    MSG.Warning("Có lỗi xảy ra \n\r" + ex.Message);
                }

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dtBeginDate_ValueChanged(object sender, EventArgs e)
        {

            DateTime date = (DateTime)dtBeginDate.Value;
            ugridDepartment.DataSource = lst = Utils.IGOtherVoucherService.FindWagePayment(date).OrderBy(x => x.DepartmentCode).ThenBy(c => c.AccountingObjectCode).ToList();
            UltraGridBand band = ugridDepartment.DisplayLayout.Bands[0];
            try
            {
                CheckState checkState = ugridDepartment.DisplayLayout.Bands[0].Columns[0].GetHeaderCheckedState(ugridDepartment.Rows);
                ugridDepartment.DisplayLayout.Bands[0].Columns[0].SetHeaderCheckedState(ugridDepartment.Rows, false);
                foreach (UltraGridRow row in ugridDepartment.Rows)
                {
                    row.Cells[0].Value = checkState == CheckState.Checked ? true : false;
                }
                foreach (var row in ugridDepartment.Rows)
                {
                    foreach (var c in band.Columns)
                    {
                        if (c.Key.Equals("AccountNumber"))
                        {
                            List<AccountingObjectBankAccount> cbb = _IAccountingObjectBankAccountService.GetByAccountingObjectID((Guid)row.Cells["AccountingObjectID"].Value);
                            row.Cells["AccountNumber"].Editor = GetEmbeddableEditorBase(Controls, cbb, "BankAccount", "BankAccount", ConstDatabase.AccountingObjectBankAccount_TableName); // Hautv Edit
                            ugridDepartment.DisplayLayout.Bands[0].Columns["AccountNumber"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
                        }
                    }
                }
            }
            catch { }
        }
        List<GeneralLedger> GenGeneralLedgers(MBTellerPaper mbTellerPaper)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

            #region Cặp Nợ-Có

            for (int i = 0; i < mbTellerPaper.MBTellerPaperDetails.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mbTellerPaper.ID,
                    TypeID = mbTellerPaper.TypeID,
                    Date = mbTellerPaper.Date,
                    PostedDate = mbTellerPaper.PostedDate,
                    No = mbTellerPaper.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = "334",
                    AccountCorresponding = "1121",
                    BankAccountDetailID = mbTellerPaper.BankAccountDetailID,
                    CurrencyID = mbTellerPaper.CurrencyID,
                    ExchangeRate = mbTellerPaper.ExchangeRate,
                    DebitAmount = mbTellerPaper.MBTellerPaperDetails[i].Amount,
                    DebitAmountOriginal = mbTellerPaper.MBTellerPaperDetails[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = mbTellerPaper.Reason,
                    Description = mbTellerPaper.MBTellerPaperDetails[i].Description,
                    AccountingObjectID = mbTellerPaper.AccountingObjectID,
                    EmployeeID = mbTellerPaper.EmployeeID,
                    BudgetItemID = mbTellerPaper.MBTellerPaperDetails[i].BudgetItemID,
                    CostSetID = mbTellerPaper.MBTellerPaperDetails[i].CostSetID,
                    ContractID = mbTellerPaper.MBTellerPaperDetails[i].ContractID,
                    StatisticsCodeID = mbTellerPaper.MBTellerPaperDetails[i].StatisticsCodeID,
                    ExpenseItemID = mbTellerPaper.MBTellerPaperDetails[i].ExpenseItemID,
                    InvoiceSeries = "",
                    ContactName = mbTellerPaper.Receiver,
                    DetailID = mbTellerPaper.MBTellerPaperDetails[i].ID,
                    RefNo = mbTellerPaper.No,
                    RefDate = mbTellerPaper.Date,
                    DepartmentID = mbTellerPaper.MBTellerPaperDetails[i].DepartmentID,

                };

                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mbTellerPaper.ID,
                    TypeID = mbTellerPaper.TypeID,
                    Date = mbTellerPaper.Date,
                    PostedDate = mbTellerPaper.PostedDate,
                    No = mbTellerPaper.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = "1121",
                    AccountCorresponding = "334",
                    BankAccountDetailID = mbTellerPaper.BankAccountDetailID,
                    CurrencyID = mbTellerPaper.CurrencyID,
                    ExchangeRate = mbTellerPaper.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = mbTellerPaper.MBTellerPaperDetails[i].Amount,
                    CreditAmountOriginal = mbTellerPaper.MBTellerPaperDetails[i].AmountOriginal,
                    Reason = mbTellerPaper.Reason,
                    Description = mbTellerPaper.MBTellerPaperDetails[i].Description,
                    AccountingObjectID = mbTellerPaper.AccountingObjectID,
                    EmployeeID = mbTellerPaper.EmployeeID,
                    BudgetItemID = mbTellerPaper.MBTellerPaperDetails[i].BudgetItemID,
                    CostSetID = mbTellerPaper.MBTellerPaperDetails[i].CostSetID,
                    ContractID = mbTellerPaper.MBTellerPaperDetails[i].ContractID,
                    StatisticsCodeID = mbTellerPaper.MBTellerPaperDetails[i].StatisticsCodeID,
                    ExpenseItemID = mbTellerPaper.MBTellerPaperDetails[i].ExpenseItemID,
                    InvoiceSeries = "",
                    ContactName = mbTellerPaper.Receiver,
                    DetailID = mbTellerPaper.MBTellerPaperDetails[i].ID,
                    RefNo = mbTellerPaper.No,
                    RefDate = mbTellerPaper.Date,
                    DepartmentID = mbTellerPaper.MBTellerPaperDetails[i].DepartmentID,

                };
                listGenTemp.Add(genTempCorresponding);
            }

            #endregion

            return listGenTemp;
        }

        List<GeneralLedger> GenGeneralLedgers(MCPayment mcPayment)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

            #region Cặp Nợ-Có

            for (int i = 0; i < mcPayment.MCPaymentDetails.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mcPayment.ID,
                    TypeID = mcPayment.TypeID,
                    Date = mcPayment.Date,
                    PostedDate = mcPayment.PostedDate,
                    No = mcPayment.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = "334",
                    AccountCorresponding = "1111",
                    CurrencyID = mcPayment.CurrencyID,
                    ExchangeRate = mcPayment.ExchangeRate,
                    DebitAmount = mcPayment.MCPaymentDetails[i].Amount,
                    DebitAmountOriginal = mcPayment.MCPaymentDetails[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = mcPayment.Reason,
                    Description = mcPayment.MCPaymentDetails[i].Description,
                    AccountingObjectID = mcPayment.AccountingObjectID,
                    EmployeeID = mcPayment.EmployeeID,
                    BudgetItemID = mcPayment.MCPaymentDetails[i].BudgetItemID,
                    CostSetID = mcPayment.MCPaymentDetails[i].CostSetID,
                    ContractID = mcPayment.MCPaymentDetails[i].ContractID,
                    StatisticsCodeID = mcPayment.MCPaymentDetails[i].StatisticsCodeID,
                    ExpenseItemID = mcPayment.MCPaymentDetails[i].ExpenseItemID,
                    InvoiceSeries = "",
                    ContactName = mcPayment.Receiver,
                    DetailID = mcPayment.MCPaymentDetails[i].ID,
                    RefNo = mcPayment.No,
                    RefDate = mcPayment.Date,
                    DepartmentID = mcPayment.MCPaymentDetails[i].DepartmentID,

                };
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mcPayment.ID,
                    TypeID = mcPayment.TypeID,
                    Date = mcPayment.Date,
                    PostedDate = mcPayment.PostedDate,
                    No = mcPayment.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = "1111",
                    AccountCorresponding = "334",
                    CurrencyID = mcPayment.CurrencyID,
                    ExchangeRate = mcPayment.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = mcPayment.MCPaymentDetails[i].Amount,
                    CreditAmountOriginal = mcPayment.MCPaymentDetails[i].AmountOriginal,
                    Reason = mcPayment.Reason,
                    Description = mcPayment.MCPaymentDetails[i].Description,
                    AccountingObjectID = mcPayment.AccountingObjectID,
                    EmployeeID = mcPayment.EmployeeID,
                    BudgetItemID = mcPayment.MCPaymentDetails[i].BudgetItemID,
                    CostSetID = mcPayment.MCPaymentDetails[i].CostSetID,
                    ContractID = mcPayment.MCPaymentDetails[i].ContractID,
                    StatisticsCodeID = mcPayment.MCPaymentDetails[i].StatisticsCodeID,

                    ExpenseItemID = mcPayment.MCPaymentDetails[i].ExpenseItemID,
                    InvoiceSeries = "",
                    ContactName = mcPayment.Receiver,
                    DetailID = mcPayment.MCPaymentDetails[i].ID,
                    RefNo = mcPayment.No,
                    RefDate = mcPayment.Date,
                    DepartmentID = mcPayment.MCPaymentDetails[i].DepartmentID,
                };

                listGenTemp.Add(genTempCorresponding);
            }

            #endregion

            return listGenTemp;
        }

        private void btnHis_Click(object sender, EventArgs e)
        {
            var f = new FWagePaymentDetailPopup();
            f.ShowDialog(this);
        }

        private void ugridDepartment_CellChange(object sender, CellEventArgs e)
        {
            UltraGridBand band = ugridDepartment.DisplayLayout.Bands[0];
            if (e.Cell.Column.Key.Equals("AccountNumber"))
            {
                UltraGridCell cell = ugridDepartment.ActiveCell;
                //UltraCombo ultracombo = (UltraCombo)Controls["dropDown1"];
                //Hautv Edit
                EditorWithCombo ultracombo1 = cell.Editor as EditorWithCombo;
                UltraCombo ultracombo = ultracombo1.ValueList as UltraCombo;
                AccountingObjectBankAccount select = (AccountingObjectBankAccount)(ultracombo.SelectedRow == null ? ultracombo.SelectedRow : ultracombo.SelectedRow.ListObject);
                if (select == null)
                    e.Cell.Row.Cells["BankName"].Value = "";
                else
                    e.Cell.Row.Cells["BankName"].Value = select.BankName;
            }
        }

        private void cbbCurrentState_ValueChanged(object sender, EventArgs e)
        {
            cbbCompanyAccountBank.Enabled = cbbCurrentState.Value.ToInt() == 121;
        }

        private void ugridDepartment_KeyDown(object sender, KeyEventArgs e)
        {
            UltraGrid uGrid = (UltraGrid)sender;
            if (uGrid.ActiveCell != null)
            {
                if (e.KeyCode == Keys.Up)
                {
                    if (uGrid.ActiveCell.IsInEditMode && (uGrid.ActiveCell.ValueListResolved != null))
                    {
                        if (!uGrid.ActiveCell.DroppedDown) uGrid.ActiveCell.DroppedDown = true;
                    }
                    else if (uGrid.ActiveCell.ValueListResolved == null)
                    {
                        uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                        uGrid.PerformAction(UltraGridAction.AboveCell, false, false);
                        e.Handled = true;
                        uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                    }
                }
                if (e.KeyCode == Keys.Down)
                {
                    if (uGrid.ActiveCell.IsInEditMode && (uGrid.ActiveCell.ValueListResolved != null))
                    {
                        if (!uGrid.ActiveCell.DroppedDown) uGrid.ActiveCell.DroppedDown = true;
                    }
                    else if (uGrid.ActiveCell.ValueListResolved == null)
                    {
                        uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                        uGrid.PerformAction(UltraGridAction.BelowCell, false, false);
                        e.Handled = true;
                        uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                    }
                }
                if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Right)
                {
                    uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                    uGrid.PerformAction(UltraGridAction.NextCellByTab, false, false);
                    e.Handled = true;
                    uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                }
                if (e.KeyCode == Keys.Left)
                {
                    uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                    uGrid.PerformAction(UltraGridAction.PrevCellByTab, false, false);
                    e.Handled = true;
                    uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                }
            }
        }
    }
}
