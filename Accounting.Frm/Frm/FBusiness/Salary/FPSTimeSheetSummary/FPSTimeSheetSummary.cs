﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Castle.Windsor.Installer;
using FX.Core;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;
using System.Drawing;

namespace Accounting
{
    public partial class FPSTimeSheetSummaryDetail : FPSTimeSheetSummaryDetailBase
    {
        #region khai bao
        public static bool isClose = true;
        readonly IPSTimeSheetSummaryDetailService _IPSTimeSheetSummaryDetailService = Utils.IPSTimeSheetSummaryDetailService;
        readonly IPSSalaryTaxInsuranceRegulationService _IPSSalaryTaxInsuranceRegulationService = Utils.IPSSalaryTaxInsuranceRegulationService;
        UltraGrid ugrid = null;
        #endregion
        #region khởi tạo
        public FPSTimeSheetSummaryDetail(PSTimeSheetSummary temp, List<PSTimeSheetSummary> lstPSTimeSheets, int statusForm)
        {
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                WaitingFrm.StopWaiting();
                var frm = new FPSTimeSheetSummaryPopup(temp, lstPSTimeSheets, statusForm);
                if (frm.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
                {
                    Close();
                    return;
                }
                WaitingFrm.StartWaiting();
                statusForm = frm._status;
                if (statusForm == ConstFrm.optStatusForm.View)
                    temp = lstPSTimeSheets.FirstOrDefault(k => k.ID == frm.PSTimeSheetSummary.ID);
                else
                {
                    temp = frm.PSTimeSheetSummary;
                }
            }
            #region Khởi tạo giá trị mặc định của Form
           // WindowState = System.Windows.Forms.FormWindowState.Maximized;
            InitializeComponent();
            base.InitializeComponent1();
            #endregion
            Utils.ClearCacheByType<SystemOption>();

            

            #region Thiết lập ban đầu cho Form
            _statusForm = statusForm;
            _select = temp;
            _listSelects.AddRange(lstPSTimeSheets);
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, _listSelects, statusForm);
            #endregion
        }
        #endregion

        public override void ShowPopup(PSTimeSheetSummary input, int statusForm)
        {
            WaitingFrm.StopWaiting();
            List<PSTimeSheetSummary> lst = Utils.IPSTimeSheetSummaryService.GetAll();
            var frm = new FPSTimeSheetSummaryPopup(input, lst, statusForm);
            if (frm.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
            {
                _statusForm = ConstFrm.optStatusForm.View;
                return;
            }
            WaitingFrm.StartWaiting();
            statusForm = frm._status;
            if (statusForm == ConstFrm.optStatusForm.View)
                input = _listSelects.FirstOrDefault(k => k.ID == frm.PSTimeSheetSummary.ID);
            else
            {
                input = frm.PSTimeSheetSummary;
            }

            _statusForm = statusForm;
            _select = input;

            InitializeGUI(_select);
        }

        #region Hàm override
        public override void InitializeGUI(PSTimeSheetSummary input)
        {
            ultraGroupBox1.Text = input.PSTimeSheetSummaryName;
            Template mauGiaoDien = Utils.CloneObject(Utils.GetMauGiaoDien(810, input.TemplateID));
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;
            if (input.TypeID == 810)
            {
                mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "WorkAllDay").First().ColumnCaption = "Số công cả ngày(Quy theo ngày)";
                mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "WorkHalfADay").First().ColumnCaption = "Số công nửa ngày(Quy theo ngày)";
                mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "WorkAllDay").First().ColumnWidth = 200;
                mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "WorkHalfADay").First().ColumnWidth = 200;
            }
            else
            {
                mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "WorkAllDay").First().ColumnCaption = "Số công cả ngày(Quy theo giờ)";
                mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "WorkHalfADay").First().ColumnCaption = "Số công nửa ngày(Quy theo giờ)";
                mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "WorkAllDay").First().ColumnWidth = 250;
                mauGiaoDien.TemplateDetails[0].TemplateColumns.Where(x => x.ColumnName == "WorkHalfADay").First().ColumnWidth = 250;
            }
            BindingList<PSTimeSheetSummaryDetail> dsPSTimeSheetSummaryDetails = new BindingList<PSTimeSheetSummaryDetail>(input.PSTimeSheetSummaryDetails);

            #region cau hinh ban dau cho form

            _listObjectInput = new BindingList<System.Collections.IList> { dsPSTimeSheetSummaryDetails };
            this.ConfigGridByTemplete_General<PSTimeSheetSummary>(pnlUgrid, mauGiaoDien);
            this.ConfigGridByTemplete<PSTimeSheetSummary>(input.TypeID, mauGiaoDien, true, _listObjectInput);
            #endregion
            ugrid = Controls.Find("uGrid0", true).FirstOrDefault() as UltraGrid;
            ugrid.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(uGrid_BeforeExitEditMode);
            if (ugrid != null)
            {
                ugrid.DisplayLayout.Bands[0].Columns["OrderPriority"].SortIndicator = SortIndicator.Ascending;
                ugrid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
                ugrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            }
                
        }
        #endregion
        private void uGrid_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            UltraGridCell activeCell = grid.ActiveCell;
            List<string> lst = new List<string> { "WorkAllDay", "WorkHalfADay", "TotalOverTime", "Total" };
            if (lst.Any(d => d == activeCell.Column.Key))
            {
                if (activeCell.Text.Trim('_', ',') == "")
                    activeCell.Value = (decimal)0;
            }
        }
    }

    public class FPSTimeSheetSummaryDetailBase : DetailBase<PSTimeSheetSummary> { }

}
