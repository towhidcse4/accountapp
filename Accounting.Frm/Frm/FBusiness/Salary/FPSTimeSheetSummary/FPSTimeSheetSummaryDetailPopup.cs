﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

namespace Accounting
{
    public partial class FPSTimeSheetSummaryDetailPopup : CustormForm
    {
        public FPSTimeSheetSummaryDetailPopup(List<PSTimeSheetSummaryDetail> lst)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            var dte = Utils.GetDbStartDate().StringToDateTime();

            ugridDepartment.DataSource = lst;
            Utils.ConfigGrid(ugridDepartment, ConstDatabase.PSTimeSheetSummaryDetail_TableName);

            UltraGridBand band = ugridDepartment.DisplayLayout.Bands[0];
            foreach (var c in band.Columns)
            {
                c.CellActivation = Activation.NoEdit;
            }
            foreach (var column in ugridDepartment.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, ugridDepartment);
            }
            WaitingFrm.StopWaiting();

        } 

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
