﻿namespace Accounting
{
    partial class FPSTimeSheetSummaryDetailPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.ugridDepartment = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.ugridDepartment)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            appearance1.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance1;
            this.btnClose.Location = new System.Drawing.Point(1284, 508);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 340;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // ugridDepartment
            // 
            this.ugridDepartment.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ugridDepartment.Location = new System.Drawing.Point(10, 12);
            this.ugridDepartment.Name = "ugridDepartment";
            this.ugridDepartment.Size = new System.Drawing.Size(1349, 490);
            this.ugridDepartment.TabIndex = 3018;
            this.ugridDepartment.Text = "Danh sách phòng ban";
            this.ugridDepartment.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // FPSTimeSheetSummaryDetailPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 545);
            this.Controls.Add(this.ugridDepartment);
            this.Controls.Add(this.btnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FPSTimeSheetSummaryDetailPopup";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Danh sách lỗi";
            ((System.ComponentModel.ISupportInitialize)(this.ugridDepartment)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugridDepartment;
    }
}