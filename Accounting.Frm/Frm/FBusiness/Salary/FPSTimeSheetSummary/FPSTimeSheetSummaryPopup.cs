﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;

namespace Accounting
{


    public partial class FPSTimeSheetSummaryPopup : CustormForm
    {
        readonly List<DepartmentReport> _lstDepartmentReport = ReportUtils.LstDepartmentReport;
        readonly List<PSTimeSheetSummary> _dsPSTimeSheetSummary;
        readonly List<PSTimeSheet> _dsPSTimeSheet;
        public PSTimeSheetSummary PSTimeSheetSummary;
        public int _status;
        string filePath = "";
        Excel excel;
        public FPSTimeSheetSummaryPopup(PSTimeSheetSummary temp, List<PSTimeSheetSummary> dsPSTimeSheetSummary, int statusForm)
        {
            WaitingFrm.StartWaiting();
            PSTimeSheetSummary = temp;
            _dsPSTimeSheetSummary = dsPSTimeSheetSummary;
            _status = statusForm;
            InitializeComponent();
            var dte = Utils.GetDbStartDate().StringToDateTime();
            cbbThang.Value = dte.HasValue ? dte.Value.Month : DateTime.Now.Month;
            txtYear.Value = dte.HasValue ? dte.Value.Year : DateTime.Now.Year;
            ugridDepartment.SetDataBinding(_lstDepartmentReport.OrderBy(x => x.DepartmentCode).ToList(), "");
            ReportUtils.ProcessControls(this);
            _dsPSTimeSheet = Utils.IPSTimeSheetService.GetAll();
            cbbCurrentState.SelectedIndex = 0;
            SetTimeSheetName();
            
            cbbTimeSheetSummary.DisplayMember = "PSTimeSheetSummaryName";
            Utils.ConfigGrid(cbbTimeSheetSummary, ConstDatabase.PSTimeSheetSummary_TableName);

            cbbTimeSheets.DisplayMember = "PSTimeSheetName";
            Utils.ConfigGrid(cbbTimeSheets, ConstDatabase.PSTimeSheet_TableName);

            cbbTimeSheets.Visible = false;
            cbbTimeSheetSummary.Visible = false;
            cbbSheetName.Visible = false;
            btnBrowser.Visible = false;
            btnDownload.Visible = false;
            lblTitle.Visible = false;

            ToolTip t = new ToolTip();
            t.SetToolTip(btnBrowser, "Mở tệp từ máy tính");
            t.SetToolTip(btnDownload, "Tải về file mẫu");

            WaitingFrm.StopWaiting();

        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            if (optLoaiDT.Value.ToInt() == 2 && cbbTimeSheets.SelectedRow == null)
            {
                MSG.Warning("Bạn chưa chọn bảng chấm công muốn dựa theo!");
                return;
            }
            else if (optLoaiDT.Value.ToInt() == 1 && cbbTimeSheetSummary.SelectedRow == null)
            {
                MSG.Warning("Bạn chưa chọn bảng tổng hợp chấm công muốn dựa theo!");
                return;
            }
            else if (optLoaiDT.Value.ToInt() == 3 && cbbSheetName.SelectedItem == null)
            {
                MSG.Warning("Bạn chưa chọn file excel!");
                return;
            }
            int thang = int.Parse(cbbThang.Text);
            int nam = Convert.ToInt32(txtYear.Value);
            //var temp = _dsPSTimeSheetSummary.FirstOrDefault(p => p.Month == thang && p.Year == nam);
            //if (temp != null)
            //{
            //    if (MSG.MessageBoxStand(string.Format("Tháng {0} năm {1} đã tổng hợp chấm công. Bạn có muốn xem bảng tổng hợp chấm công này không?", thang, nam), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            //    {
            //        DialogResult = DialogResult.OK;
            //        _status = ConstFrm.optStatusForm.View;
            //        PSTimeSheetSummary = temp;
            //        Close();
            //    }
            //    return;
            //}
            var dbDateClosed = Utils.GetDBDateClosed();
            var dte = dbDateClosed.StringToDateTime();
            if (dte.HasValue && (dte.Value.Year > nam || (dte.Value.Year == nam && dte.Value.Month > thang)))
            {
                MSG.Warning(string.Format("Ngày hạch toán phải lớn hơn ngày khóa sổ: {0}. Xin vui lòng kiểm tra lại.", dbDateClosed));
                return;
            }
            PSTimeSheetSummary.Departments = _lstDepartmentReport.Where(p => p.Check).Select(x => x.ID).ToList();
            PSTimeSheetSummary.Month = thang;
            PSTimeSheetSummary.Year = nam;
            PSTimeSheetSummary.TypeID = cbbCurrentState.SelectedIndex == 0 ? 810 : 811;
            PSTimeSheetSummary.PSTimeSheetSummaryName = txtTimeSheetName.Text;
            if (GeneratePSTimeSheetSummary())
            {
                DialogResult = DialogResult.OK;
                Close();
            }

        }

        private bool GeneratePSTimeSheetSummary()
        {
            if (optLoaiDT.Value.ToInt() == 0)
            {
                PSTimeSheetSummary.PSTimeSheetSummaryDetails = Utils.IPSTimeSheetSummaryDetailService.FindEmployeeByDepartment(PSTimeSheetSummary.Departments).Where(x => _lstDepartmentReport.Where(c => c.Check).ToList().Any(d => d.ID == x.DepartmentID)).ToList(); ;
                    
            }
            else if (optLoaiDT.Value.ToInt() == 1)
            {
                PSTimeSheetSummary.PSTimeSheetSummaryDetails = ((PSTimeSheetSummary)cbbTimeSheetSummary.SelectedRow.ListObject).PSTimeSheetSummaryDetails.Where(x => _lstDepartmentReport.Where(c => c.Check).ToList().Any(d => d.ID == x.DepartmentID)).ToList(); ;
            }
            else if (optLoaiDT.Value.ToInt() == 2)
            {
                DateTime dateTime = new DateTime(PSTimeSheetSummary.Year, PSTimeSheetSummary.Month, 1);
                PSSalaryTaxInsuranceRegulation stir = Utils.IPSSalaryTaxInsuranceRegulationService.FindByDate(dateTime);
                if (stir == null) stir = new PSSalaryTaxInsuranceRegulation();
                decimal hour = PSTimeSheetSummary.TypeID == 810 ? 1 : stir.WorkingHoursInDay;
                List<PSTimeSheetDetail> psts = ((PSTimeSheet)cbbTimeSheets.SelectedRow.ListObject).PSTimeSheetDetails.ToList();
                BindingList<TimeSheetSymbols> ListTimeSheetSymbols = Utils.ListTimeSheetSymbols;
                List<PSTimeSheetSummaryDetail> details = new List<PSTimeSheetSummaryDetail>();
                foreach(PSTimeSheetDetail detail in psts)
                {
                    decimal allday = 0;
                    decimal halfday = 0;
                    
                    for(int i = 1; i < 32; i++)
                    {
                        string tag = detail.GetProperty<PSTimeSheetDetail, string>("day" + i);
                        if (tag.IsNullOrEmpty()) continue;
                        string[] tags = tag.Split(new string[] { ",", ";" }, StringSplitOptions.None);

                        foreach (String sym in tags)
                        {
                            TimeSheetSymbols oldSym = ListTimeSheetSymbols.Where(x => sym.Contains(x.TimeSheetSymbolsCode)).FirstOrDefault();
                            if (oldSym != null)
                            {
                                if (oldSym.IsHalfDay == true)
                                {
                                    halfday++;
                                }
                                else 
                                {
                                    allday++;
                                }
                            }
                        }
                    }
                    // khong can tinh nữa vì bên chấm công đã tính rồi
                    //decimal overtime = PSTimeSheetSummary.TypeID == 810 ? detail.TotalOverTime / (stir.WorkingHoursInDay == 0 ? 1 : stir.WorkingHoursInDay) : detail.TotalOverTime;
                    AccountingObject accountingObject = Utils.ListAccountingObject.Where(n=>n.ID==detail.EmployeeID).FirstOrDefault();
                    Department department = Utils.ListDepartment.Where(n => n.ID == detail.DepartmentID).FirstOrDefault();
                    PSTimeSheetSummaryDetail sDetail = new PSTimeSheetSummaryDetail
                    {
                        EmployeeID = detail.EmployeeID,
                        DepartmentID = detail.DepartmentID,
                        EmployeeCode= accountingObject.AccountingObjectCode,
                        DepartmentCode= department.DepartmentCode,
                        AccountingObjectName = detail.AccountingObjectName,
                        AccountingObjectTitle = detail.AccountingObjectTitle,
                        WorkAllDay = allday * hour,
                        WorkHalfADay = halfday / (PSTimeSheetSummary.TypeID == 810 ? 2 : 1) * hour,
                        //TotalOverTime = overtime,
                        //Total = (allday + halfday / (PSTimeSheetSummary.TypeID == 810 ? 2 : 1)) * hour + overtime
                        TotalOverTime = detail.TotalOverTime,
                        Total = (allday + halfday / (PSTimeSheetSummary.TypeID == 810 ? 2 : 1)) * hour + detail.TotalOverTime
                    };
                    details.Add(sDetail);
                }
                PSTimeSheetSummary.PSTimeSheetSummaryDetails = details.OrderBy(n => n.DepartmentCode).ThenBy(n => n.EmployeeCode).ToList().Where(x => _lstDepartmentReport.Where(c => c.Check).ToList().Any(d => d.ID == x.DepartmentID)).ToList(); ;
            }
            else if (optLoaiDT.Value.ToInt() == 3)
            {
                Dictionary<String, int> header = excel.ReadHeader();
                if (!header.ContainsKey(excel.HEADER[0]))
                {
                    if (MSG.Question("Mẫu file không đúng. Bạn có muốn tải file mẫu không?") == DialogResult.Yes)
                        DownloadFile();
                    return false;
                }
                PSTimeSheetSummary.PSTimeSheetSummaryDetails = excel.GetObject(header, PSTimeSheetSummary.TypeID);
            }
            int j = 1;
            bool isError = false;
            foreach (PSTimeSheetSummaryDetail detail in PSTimeSheetSummary.PSTimeSheetSummaryDetails)
            {
                detail.OrderPriority = j++;
                if (!detail.Message.IsNullOrEmpty())
                {
                    isError = true;
                }
                
            }
            if (isError)
            {
                if(MSG.Question("Dữ liệu import bảng tổng hợp chấm công không hợp lệ, Bạn có muốn xem lỗi dữ liệu không?") == DialogResult.Yes)
                {
                    var frm = new FPSTimeSheetSummaryDetailPopup(PSTimeSheetSummary.PSTimeSheetSummaryDetails.ToList());
                    frm.ShowDialog(this);
                }
                return false;
            }
            return true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtYear_Validated(object sender, EventArgs e)
        {
            int thang = 0;
            try
            {
                thang = int.Parse(txtYear.Text);
                if (thang > 2100 || thang <= 1980)
                {
                    MSG.Warning("Giá trị năm không hợp lệ");
                    txtYear.Value = DateTime.Now.Year;
                    return;
                }

            }
            catch (Exception)
            {
                MSG.Warning("Giá trị năm không hợp lệ");
                txtYear.Value = DateTime.Now.Year;
                return;
            }
            SetTimeSheetName();
        }

        private void cbbThang_Validated(object sender, EventArgs e)
        {
            int thang = 0;
            try
            {
                thang = int.Parse(cbbThang.Text);
                if (thang > 12 || thang < 1)
                {
                    MSG.Warning("Giá trị tháng không hợp lệ");
                    return;
                }
            }
            catch (Exception)
            {
                MSG.Warning("Giá trị tháng không hợp lệ");
                return;
            }
            SetTimeSheetName();
        }

        private void SetTimeSheetName()
        {
            txtTimeSheetName.Text = string.Format("Bảng {0} tháng {1} năm {2}", cbbCurrentState.Text, cbbThang.Text, txtYear.Text);
        }

        private void cbbCurrentState_Validated(object sender, EventArgs e)
        {
            SetTimeSheetName();
        }

        private void optLoaiDT_ValueChanged(object sender, EventArgs e)
        {
            if (optLoaiDT.Value.ToInt() == 1)
            {
                cbbTimeSheets.Visible = false;
                cbbTimeSheetSummary.Visible = true;
                cbbSheetName.Visible = false;
                btnBrowser.Visible = false;
                lblTitle.Visible = true;
                btnDownload.Visible = false;
                lblTitle.Text = "Dựa trên bảng tổng hợp chấm công:";

            }
            else if (optLoaiDT.Value.ToInt() == 2)
            {
                cbbTimeSheets.Visible = true;
                cbbTimeSheetSummary.Visible = false;
                cbbSheetName.Visible = false;
                btnBrowser.Visible = false;
                lblTitle.Visible = true;
                btnDownload.Visible = false;
                lblTitle.Text = "Dựa trên bảng chấm công:";
            }
            else if (optLoaiDT.Value.ToInt() == 3)
            {
                cbbTimeSheets.Visible = false;
                cbbTimeSheetSummary.Visible = false;
                cbbSheetName.Visible = true;
                btnBrowser.Visible = true;
                lblTitle.Visible = true;
                btnDownload.Visible = true;
                lblTitle.Text = "Tải lên file excel:";
            }
            else
            {
                cbbTimeSheets.Visible = false;
                cbbTimeSheetSummary.Visible = false;
                cbbSheetName.Visible = false;
                btnBrowser.Visible = false;
                lblTitle.Visible = false;
                btnDownload.Visible = false;
            }
        }

        private void FPSTimeSheetSummaryPopup_FormClosing(object sender, FormClosingEventArgs e)
        {
            _lstDepartmentReport.Select(x => { x.Check = false; return x; }).ToList();
        }

        private void btnBrowser_Click(object sender, EventArgs e)
        {
            var openFile = new OpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                Filter = "Excel Files|*.xls;*.xlsx;*.xlsm"

            };


            var result = openFile.ShowDialog(this);
            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                try
                {
                    using (FileStream file = new FileStream(openFile.FileName, FileMode.Open, FileAccess.Read))
                    {

                        excel = new Excel(openFile.FileName);
                        List<String> sheetNames = excel.ReadSheetName();
                        for(int i = 0; i < sheetNames.Count; i++)
                        {
                            cbbSheetName.Items.Add(i, sheetNames[i]);
                        }
                        if (sheetNames.Count > 0)
                        {
                            cbbSheetName.SelectedIndex = 0;
                            excel.ChooseSheet(cbbSheetName.SelectedIndex + 1);
                        }
                    }
                }
                catch (Exception)
                {
                    MSG.Error("File này đang được mở bởi ứng dụng khác");
                    return;
                }

            }
        }

        private void cbbCurrentState_ValueChanged(object sender, EventArgs e)
        {
            cbbTimeSheets.DataSource = _dsPSTimeSheet.Where(x => x.TypeID == (cbbCurrentState.SelectedIndex == 0 ? 821 : 820)).OrderBy(x => x.Year).ThenBy(x => x.Month).ToList();
            cbbTimeSheetSummary.DataSource = _dsPSTimeSheetSummary.Where(x => x.TypeID == (cbbCurrentState.SelectedIndex == 0 ? 810 : 811)).OrderBy(x => x.Year).ThenBy(x => x.Month).ToList();
            cbbTimeSheets.Value = null;
            cbbTimeSheetSummary.Value = null;
            SetTimeSheetName();
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile();
        }

        private static void DownloadFile()
        {
            SaveFileDialog sf = new SaveFileDialog
            {
                FileName = "MauBangTongHopChamCong.xlsx",
                AddExtension = true,
                Filter = "Excel Document(*.xlsx)|*.xlsx"
            };

            if (sf.ShowDialog() == DialogResult.OK)
            {
                string path = System.IO.Path.GetDirectoryName(
                        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                string filePath = string.Format("{0}\\TemplateResource\\excel\\MauBangTongHopChamCong.xlsx", path);
                ResourceHelper.MakeFileOutOfAStream(filePath, sf.FileName);
            }
        }

        private void cbbSheetName_SelectionChanged(object sender, EventArgs e)
        {
            excel.ChooseSheet(cbbSheetName.SelectedIndex + 1);
        }

        private void cbbThang_ValueChanged(object sender, EventArgs e)
        {
            SetTimeSheetName();
        }

        private void txtYear_ValueChanged(object sender, EventArgs e)
        {
            SetTimeSheetName();
        }
    }
}
