﻿namespace Accounting
{
    partial class FBrowser<T>
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinToolbars.RibbonTab ribbonTab1 = new Infragistics.Win.UltraWinToolbars.RibbonTab("ribbon1");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup1 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool5 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnApply");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool6 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnClose");
            Infragistics.Win.UltraWinToolbars.UltraToolbar ultraToolbar1 = new Infragistics.Win.UltraWinToolbars.UltraToolbar("UltraToolbar1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool1 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnApply");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool3 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnClose");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool2 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnApply");
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool4 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnClose");
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            this.FBrowser_Fill_Panel = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this._FBrowser_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FBrowser_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FBrowser_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FBrowser_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.utmUtils = new Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(this.components);
            this.FBrowser_Fill_Panel.ClientArea.SuspendLayout();
            this.FBrowser_Fill_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.utmUtils)).BeginInit();
            this.SuspendLayout();
            // 
            // FBrowser_Fill_Panel
            // 
            // 
            // FBrowser_Fill_Panel.ClientArea
            // 
            this.FBrowser_Fill_Panel.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.FBrowser_Fill_Panel.Cursor = System.Windows.Forms.Cursors.Default;
            this.FBrowser_Fill_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FBrowser_Fill_Panel.Location = new System.Drawing.Point(0, 25);
            this.FBrowser_Fill_Panel.Name = "FBrowser_Fill_Panel";
            this.FBrowser_Fill_Panel.Size = new System.Drawing.Size(708, 465);
            this.FBrowser_Fill_Panel.TabIndex = 0;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.ContentPadding.Bottom = 10;
            this.ultraGroupBox1.ContentPadding.Left = 5;
            this.ultraGroupBox1.ContentPadding.Right = 5;
            this.ultraGroupBox1.ContentPadding.Top = 10;
            this.ultraGroupBox1.Controls.Add(this.uGrid);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(708, 465);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Danh sách chứng từ";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // uGrid
            // 
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(8, 26);
            this.uGrid.MinimumSize = new System.Drawing.Size(580, 335);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(692, 426);
            this.uGrid.TabIndex = 0;
            this.uGrid.Text = "ultraGrid1";
            this.uGrid.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.uGrid_AfterSelectChange);
            this.uGrid.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid_DoubleClickRow);
            // 
            // _FBrowser_Toolbars_Dock_Area_Left
            // 
            this._FBrowser_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FBrowser_Toolbars_Dock_Area_Left.BackColor = System.Drawing.SystemColors.Control;
            this._FBrowser_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
            this._FBrowser_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.SystemColors.ControlText;
            this._FBrowser_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 25);
            this._FBrowser_Toolbars_Dock_Area_Left.Name = "_FBrowser_Toolbars_Dock_Area_Left";
            this._FBrowser_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(0, 465);
            this._FBrowser_Toolbars_Dock_Area_Left.ToolbarsManager = this.utmUtils;
            // 
            // _FBrowser_Toolbars_Dock_Area_Right
            // 
            this._FBrowser_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FBrowser_Toolbars_Dock_Area_Right.BackColor = System.Drawing.SystemColors.Control;
            this._FBrowser_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right;
            this._FBrowser_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.SystemColors.ControlText;
            this._FBrowser_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(708, 25);
            this._FBrowser_Toolbars_Dock_Area_Right.Name = "_FBrowser_Toolbars_Dock_Area_Right";
            this._FBrowser_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(0, 465);
            this._FBrowser_Toolbars_Dock_Area_Right.ToolbarsManager = this.utmUtils;
            // 
            // _FBrowser_Toolbars_Dock_Area_Bottom
            // 
            this._FBrowser_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FBrowser_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.SystemColors.Control;
            this._FBrowser_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom;
            this._FBrowser_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.SystemColors.ControlText;
            this._FBrowser_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 490);
            this._FBrowser_Toolbars_Dock_Area_Bottom.Name = "_FBrowser_Toolbars_Dock_Area_Bottom";
            this._FBrowser_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(708, 0);
            this._FBrowser_Toolbars_Dock_Area_Bottom.ToolbarsManager = this.utmUtils;
            // 
            // _FBrowser_Toolbars_Dock_Area_Top
            // 
            this._FBrowser_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FBrowser_Toolbars_Dock_Area_Top.BackColor = System.Drawing.SystemColors.Control;
            this._FBrowser_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top;
            this._FBrowser_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.SystemColors.ControlText;
            this._FBrowser_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
            this._FBrowser_Toolbars_Dock_Area_Top.Name = "_FBrowser_Toolbars_Dock_Area_Top";
            this._FBrowser_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(708, 25);
            this._FBrowser_Toolbars_Dock_Area_Top.ToolbarsManager = this.utmUtils;
            // 
            // utmUtils
            // 
            this.utmUtils.DesignerFlags = 1;
            this.utmUtils.DockWithinContainer = this;
            this.utmUtils.DockWithinContainerBaseType = typeof(System.Windows.Forms.Form);
            this.utmUtils.Office2007UICompatibility = false;
            ribbonTab1.Caption = "ribbon1";
            ribbonGroup1.Caption = "ribbonGroup1";
            buttonTool5.InstanceProps.MinimumSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool6.InstanceProps.IsFirstInGroup = true;
            buttonTool6.InstanceProps.MinimumSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup1.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool5,
            buttonTool6});
            ribbonTab1.Groups.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonGroup[] {
            ribbonGroup1});
            this.utmUtils.Ribbon.NonInheritedRibbonTabs.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonTab[] {
            ribbonTab1});
            ultraToolbar1.DockedColumn = 0;
            ultraToolbar1.DockedRow = 0;
            ultraToolbar1.NonInheritedTools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool1,
            buttonTool3});
            ultraToolbar1.Text = "UltraToolbar1";
            this.utmUtils.Toolbars.AddRange(new Infragistics.Win.UltraWinToolbars.UltraToolbar[] {
            ultraToolbar1});
            appearance1.Image = global::Accounting.Properties.Resources.apply_32;
            buttonTool2.SharedPropsInternal.AppearancesLarge.Appearance = appearance1;
            appearance2.Image = global::Accounting.Properties.Resources.apply_16;
            buttonTool2.SharedPropsInternal.AppearancesSmall.Appearance = appearance2;
            buttonTool2.SharedPropsInternal.Caption = "Chọn";
            buttonTool2.SharedPropsInternal.CustomizerCaption = "Chọn";
            buttonTool2.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance3.Image = global::Accounting.Properties.Resources.btnPoweOff;
            buttonTool4.SharedPropsInternal.AppearancesLarge.Appearance = appearance3;
            appearance4.Image = global::Accounting.Properties.Resources.btnPoweOff;
            buttonTool4.SharedPropsInternal.AppearancesSmall.Appearance = appearance4;
            buttonTool4.SharedPropsInternal.Caption = "Đóng";
            buttonTool4.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            this.utmUtils.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool2,
            buttonTool4});
            this.utmUtils.ToolClick += new Infragistics.Win.UltraWinToolbars.ToolClickEventHandler(this.utmUtils_ToolClick);
            // 
            // FBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 490);
            this.Controls.Add(this.FBrowser_Fill_Panel);
            this.Controls.Add(this._FBrowser_Toolbars_Dock_Area_Left);
            this.Controls.Add(this._FBrowser_Toolbars_Dock_Area_Right);
            this.Controls.Add(this._FBrowser_Toolbars_Dock_Area_Bottom);
            this.Controls.Add(this._FBrowser_Toolbars_Dock_Area_Top);
            this.Icon = global::Accounting.Properties.Resources.Checklist;
            this.Name = "FBrowser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Duyệt danh sách chứng từ";
            this.Load += new System.EventHandler(this.FBrowser_Load);
            this.FBrowser_Fill_Panel.ClientArea.ResumeLayout(false);
            this.FBrowser_Fill_Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.utmUtils)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinToolbars.UltraToolbarsManager utmUtils;
        private Infragistics.Win.Misc.UltraPanel FBrowser_Fill_Panel;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FBrowser_Toolbars_Dock_Area_Left;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FBrowser_Toolbars_Dock_Area_Right;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FBrowser_Toolbars_Dock_Area_Bottom;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FBrowser_Toolbars_Dock_Area_Top;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
    }
}