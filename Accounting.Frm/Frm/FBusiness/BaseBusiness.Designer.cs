﻿namespace Accounting
{
    partial class BaseBusiness<T>
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.palTtc = new System.Windows.Forms.Panel();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.palGrid = new Infragistics.Win.Misc.UltraPanel();
            this.uGrid0 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmPost = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmUnPost = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTree = new Infragistics.Win.UltraWinTree.UltraTree();
            this.panel3 = new System.Windows.Forms.Panel();
            this.uTabControl = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.grTitle = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnExportExcel = new Infragistics.Win.Misc.UltraButton();
            this.btnLoc = new Infragistics.Win.Misc.UltraButton();
            this.lblBeginDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblEndDate = new Infragistics.Win.Misc.UltraLabel();
            this.dtBeginDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblKyKeToan = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDateTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.btnUpload = new Infragistics.Win.Misc.UltraButton();
            this.drpbtnAdd = new Infragistics.Win.Misc.UltraDropDownButton();
            this.btnHelp = new Infragistics.Win.Misc.UltraButton();
            this.btnPrint = new Infragistics.Win.Misc.UltraButton();
            this.btnReset = new Infragistics.Win.Misc.UltraButton();
            this.btnUnRecorded = new Infragistics.Win.Misc.UltraButton();
            this.btnRecorded = new Infragistics.Win.Misc.UltraButton();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.btnEdit = new Infragistics.Win.Misc.UltraButton();
            this.cms4ButtonPPDiscountReturn = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnPPReturn = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPPDiscount = new System.Windows.Forms.ToolStripMenuItem();
            this.cms4ButtonSAReturns = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnSAReturn = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSADiscount = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ultraTabPageControl1.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            this.palGrid.ClientArea.SuspendLayout();
            this.palGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetail)).BeginInit();
            this.cms4Grid.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTree)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTabControl)).BeginInit();
            this.uTabControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grTitle)).BeginInit();
            this.grTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).BeginInit();
            this.cms4ButtonPPDiscountReturn.SuspendLayout();
            this.cms4ButtonSAReturns.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.palTtc);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(806, 171);
            // 
            // palTtc
            // 
            this.palTtc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palTtc.Location = new System.Drawing.Point(0, 0);
            this.palTtc.Name = "palTtc";
            this.palTtc.Size = new System.Drawing.Size(806, 171);
            this.palTtc.TabIndex = 20;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.palGrid);
            this.ultraTabPageControl2.Controls.Add(this.uGridDetail);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(806, 171);
            // 
            // palGrid
            // 
            // 
            // palGrid.ClientArea
            // 
            this.palGrid.ClientArea.Controls.Add(this.uGrid0);
            this.palGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palGrid.Location = new System.Drawing.Point(0, 0);
            this.palGrid.Name = "palGrid";
            this.palGrid.Size = new System.Drawing.Size(806, 171);
            this.palGrid.TabIndex = 2;
            // 
            // uGrid0
            // 
            this.uGrid0.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid0.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid0.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid0.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid0.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid0.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid0.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid0.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid0.Location = new System.Drawing.Point(0, 0);
            this.uGrid0.Margin = new System.Windows.Forms.Padding(3, 3, 3, 30);
            this.uGrid0.Name = "uGrid0";
            this.uGrid0.Size = new System.Drawing.Size(806, 171);
            this.uGrid0.TabIndex = 3;
            // 
            // uGridDetail
            // 
            this.uGridDetail.Location = new System.Drawing.Point(0, 124);
            this.uGridDetail.Name = "uGridDetail";
            this.uGridDetail.Size = new System.Drawing.Size(255, 118);
            this.uGridDetail.TabIndex = 1;
            this.uGridDetail.Text = "ultraGrid1";
            this.uGridDetail.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(174, 6);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(177, 22);
            this.tsmAdd.Text = "Thêm";
            // 
            // tsmEdit
            // 
            this.tsmEdit.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.tsmEdit.Name = "tsmEdit";
            this.tsmEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.tsmEdit.Size = new System.Drawing.Size(177, 22);
            this.tsmEdit.Text = "Xem";
            // 
            // tsmDelete
            // 
            this.tsmDelete.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(177, 22);
            this.tsmDelete.Text = "Xóa";
            // 
            // cms4Grid
            // 
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmEdit,
            this.tsmDelete,
            this.tsmPost,
            this.tsmUnPost,
            this.toolStripMenuItem1,
            this.tsmReLoad,
            this.tsmSelectAll});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(178, 164);
            this.cms4Grid.Opening += new System.ComponentModel.CancelEventHandler(this.cms4Grid_Opening);
            // 
            // tsmPost
            // 
            this.tsmPost.Image = global::Accounting.Properties.Resources.ubtnPost;
            this.tsmPost.Name = "tsmPost";
            this.tsmPost.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this.tsmPost.Size = new System.Drawing.Size(177, 22);
            this.tsmPost.Text = "Ghi sổ";
            // 
            // tsmUnPost
            // 
            this.tsmUnPost.Image = global::Accounting.Properties.Resources.boghi;
            this.tsmUnPost.Name = "tsmUnPost";
            this.tsmUnPost.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
            this.tsmUnPost.Size = new System.Drawing.Size(177, 22);
            this.tsmUnPost.Text = "Bỏ ghi sổ";
            // 
            // tsmReLoad
            // 
            this.tsmReLoad.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tsmReLoad.Name = "tsmReLoad";
            this.tsmReLoad.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.tsmReLoad.Size = new System.Drawing.Size(177, 22);
            this.tsmReLoad.Text = "Tải lại";
            // 
            // tsmSelectAll
            // 
            this.tsmSelectAll.Image = global::Accounting.Properties.Resources.select;
            this.tsmSelectAll.Name = "tsmSelectAll";
            this.tsmSelectAll.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.tsmSelectAll.Size = new System.Drawing.Size(177, 22);
            this.tsmSelectAll.Text = "Chọn tất cả";
            this.tsmSelectAll.Click += new System.EventHandler(this.tsmSelectAll_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.uGrid);
            this.panel2.Controls.Add(this.uTree);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 47);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(810, 312);
            this.panel2.TabIndex = 5;
            // 
            // uGrid
            // 
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.Location = new System.Drawing.Point(34, 31);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(248, 141);
            this.uGrid.TabIndex = 2;
            this.uGrid.InitializePrintPreview += new Infragistics.Win.UltraWinGrid.InitializePrintPreviewEventHandler(this.uGrid_InitializePrintPreview);
            // 
            // uTree
            // 
            this.uTree.Location = new System.Drawing.Point(314, 31);
            this.uTree.Name = "uTree";
            this.uTree.Size = new System.Drawing.Size(248, 141);
            this.uTree.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.uTabControl);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 369);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(810, 197);
            this.panel3.TabIndex = 7;
            // 
            // uTabControl
            // 
            this.uTabControl.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTabControl.Controls.Add(this.ultraTabPageControl2);
            this.uTabControl.Controls.Add(this.ultraTabPageControl1);
            this.uTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uTabControl.Location = new System.Drawing.Point(0, 0);
            this.uTabControl.Name = "uTabControl";
            this.uTabControl.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTabControl.Size = new System.Drawing.Size(810, 197);
            this.uTabControl.TabIndex = 20;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Thông tin chung";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Chi tiết";
            this.uTabControl.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(806, 171);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 359);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 197;
            this.ultraSplitter1.Size = new System.Drawing.Size(810, 10);
            this.ultraSplitter1.TabIndex = 3;
            // 
            // grTitle
            // 
            this.grTitle.Controls.Add(this.btnExportExcel);
            this.grTitle.Controls.Add(this.btnLoc);
            this.grTitle.Controls.Add(this.lblBeginDate);
            this.grTitle.Controls.Add(this.lblEndDate);
            this.grTitle.Controls.Add(this.dtBeginDate);
            this.grTitle.Controls.Add(this.dtEndDate);
            this.grTitle.Controls.Add(this.lblKyKeToan);
            this.grTitle.Controls.Add(this.cbbDateTime);
            this.grTitle.Controls.Add(this.btnUpload);
            this.grTitle.Controls.Add(this.drpbtnAdd);
            this.grTitle.Controls.Add(this.btnHelp);
            this.grTitle.Controls.Add(this.btnPrint);
            this.grTitle.Controls.Add(this.btnReset);
            this.grTitle.Controls.Add(this.btnUnRecorded);
            this.grTitle.Controls.Add(this.btnRecorded);
            this.grTitle.Controls.Add(this.btnAdd);
            this.grTitle.Controls.Add(this.btnDelete);
            this.grTitle.Controls.Add(this.btnEdit);
            this.grTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance41.FontData.BoldAsString = "True";
            appearance41.FontData.SizeInPoints = 13F;
            this.grTitle.HeaderAppearance = appearance41;
            this.grTitle.Location = new System.Drawing.Point(0, 0);
            this.grTitle.Name = "grTitle";
            this.grTitle.Size = new System.Drawing.Size(810, 47);
            this.grTitle.TabIndex = 20;
            this.grTitle.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            this.grTitle.Visible = false;
            // 
            // btnExportExcel
            // 
            appearance1.BackColor = System.Drawing.Color.DarkRed;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance1.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance1.Image = global::Accounting.Properties.Resources.Excel1;
            this.btnExportExcel.Appearance = appearance1;
            this.btnExportExcel.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance2.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.btnExportExcel.HotTrackAppearance = appearance2;
            this.btnExportExcel.Location = new System.Drawing.Point(684, 4);
            this.btnExportExcel.Name = "btnExportExcel";
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.btnExportExcel.PressedAppearance = appearance3;
            this.btnExportExcel.Size = new System.Drawing.Size(75, 30);
            this.btnExportExcel.TabIndex = 62;
            this.btnExportExcel.Text = "Kết xuất";
            // 
            // btnLoc
            // 
            this.btnLoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.BackColor = System.Drawing.Color.DarkRed;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance4.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance4.Image = global::Accounting.Properties.Resources.filter;
            this.btnLoc.Appearance = appearance4;
            this.btnLoc.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance5.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.btnLoc.HotTrackAppearance = appearance5;
            this.btnLoc.Location = new System.Drawing.Point(741, 12);
            this.btnLoc.Name = "btnLoc";
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.btnLoc.PressedAppearance = appearance6;
            this.btnLoc.Size = new System.Drawing.Size(63, 30);
            this.btnLoc.TabIndex = 61;
            this.btnLoc.Text = "Lọc";
            this.btnLoc.Click += new System.EventHandler(this.btnLoc_Click);
            // 
            // lblBeginDate
            // 
            this.lblBeginDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Bottom";
            this.lblBeginDate.Appearance = appearance7;
            this.lblBeginDate.Location = new System.Drawing.Point(500, 21);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.Size = new System.Drawing.Size(24, 17);
            this.lblBeginDate.TabIndex = 57;
            this.lblBeginDate.Text = "Từ:";
            // 
            // lblEndDate
            // 
            this.lblEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextHAlignAsString = "Left";
            appearance8.TextVAlignAsString = "Bottom";
            this.lblEndDate.Appearance = appearance8;
            this.lblEndDate.Location = new System.Drawing.Point(617, 21);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(32, 17);
            this.lblEndDate.TabIndex = 58;
            this.lblEndDate.Text = "Đến:";
            // 
            // dtBeginDate
            // 
            this.dtBeginDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.TextHAlignAsString = "Center";
            appearance9.TextVAlignAsString = "Middle";
            this.dtBeginDate.Appearance = appearance9;
            this.dtBeginDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtBeginDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtBeginDate.Location = new System.Drawing.Point(527, 19);
            this.dtBeginDate.MaskInput = "";
            this.dtBeginDate.Name = "dtBeginDate";
            this.dtBeginDate.Size = new System.Drawing.Size(85, 21);
            this.dtBeginDate.TabIndex = 59;
            this.dtBeginDate.Value = null;
            // 
            // dtEndDate
            // 
            this.dtEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance10.TextHAlignAsString = "Center";
            appearance10.TextVAlignAsString = "Middle";
            this.dtEndDate.Appearance = appearance10;
            this.dtEndDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtEndDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtEndDate.Location = new System.Drawing.Point(653, 19);
            this.dtEndDate.MaskInput = "";
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.Size = new System.Drawing.Size(83, 21);
            this.dtEndDate.TabIndex = 60;
            this.dtEndDate.Value = null;
            // 
            // lblKyKeToan
            // 
            this.lblKyKeToan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextHAlignAsString = "Left";
            appearance11.TextVAlignAsString = "Bottom";
            this.lblKyKeToan.Appearance = appearance11;
            this.lblKyKeToan.Location = new System.Drawing.Point(331, 21);
            this.lblKyKeToan.Name = "lblKyKeToan";
            this.lblKyKeToan.Size = new System.Drawing.Size(29, 17);
            this.lblKyKeToan.TabIndex = 56;
            this.lblKyKeToan.Text = "Kỳ:";
            // 
            // cbbDateTime
            // 
            this.cbbDateTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbDateTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDateTime.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbDateTime.Location = new System.Drawing.Point(366, 19);
            this.cbbDateTime.Name = "cbbDateTime";
            this.cbbDateTime.NullText = "<chọn dữ liệu>";
            this.cbbDateTime.Size = new System.Drawing.Size(128, 22);
            this.cbbDateTime.TabIndex = 55;
            // 
            // btnUpload
            // 
            appearance12.BackColor = System.Drawing.Color.DarkRed;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance12.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance12.Image = global::Accounting.Properties.Resources.Avosoft_Warm_Toolbar_Folder_open;
            this.btnUpload.Appearance = appearance12;
            this.btnUpload.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance13.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.btnUpload.HotTrackAppearance = appearance13;
            this.btnUpload.Location = new System.Drawing.Point(586, 12);
            this.btnUpload.Name = "btnUpload";
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.btnUpload.PressedAppearance = appearance14;
            this.btnUpload.Size = new System.Drawing.Size(75, 30);
            this.btnUpload.TabIndex = 21;
            this.btnUpload.Text = "Upload Hóa Đơn";
            // 
            // drpbtnAdd
            // 
            this.drpbtnAdd.AllowDrop = true;
            appearance15.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance15.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.drpbtnAdd.Appearance = appearance15;
            this.drpbtnAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance16.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.drpbtnAdd.HotTrackAppearance = appearance16;
            this.drpbtnAdd.Location = new System.Drawing.Point(10, 12);
            this.drpbtnAdd.Name = "drpbtnAdd";
            this.drpbtnAdd.Size = new System.Drawing.Size(75, 30);
            this.drpbtnAdd.TabIndex = 20;
            this.drpbtnAdd.Text = "Thêm";
            this.drpbtnAdd.DroppingDown += new System.ComponentModel.CancelEventHandler(this.drpbtnAdd_DroppingDown);
            this.drpbtnAdd.Click += new System.EventHandler(this.drpbtnAdd_Click);
            this.drpbtnAdd.MouseHover += new System.EventHandler(this.drpbtnAdd_MouseHover);
            // 
            // btnHelp
            // 
            appearance17.BackColor = System.Drawing.Color.DarkRed;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance17.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance17.Image = global::Accounting.Properties.Resources.ubtnHelp;
            this.btnHelp.Appearance = appearance17;
            this.btnHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance18.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.btnHelp.HotTrackAppearance = appearance18;
            this.btnHelp.Location = new System.Drawing.Point(667, 12);
            this.btnHelp.Name = "btnHelp";
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.btnHelp.PressedAppearance = appearance19;
            this.btnHelp.Size = new System.Drawing.Size(75, 30);
            this.btnHelp.TabIndex = 19;
            this.btnHelp.Text = "Giúp";
            this.btnHelp.Visible = false;
            // 
            // btnPrint
            // 
            appearance20.BackColor = System.Drawing.Color.DarkRed;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance20.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance20.Image = global::Accounting.Properties.Resources.ubtnPrint;
            this.btnPrint.Appearance = appearance20;
            this.btnPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance21.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.btnPrint.HotTrackAppearance = appearance21;
            this.btnPrint.Location = new System.Drawing.Point(505, 12);
            this.btnPrint.Name = "btnPrint";
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.btnPrint.PressedAppearance = appearance22;
            this.btnPrint.Size = new System.Drawing.Size(75, 30);
            this.btnPrint.TabIndex = 18;
            this.btnPrint.Text = "In";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnReset
            // 
            appearance23.BackColor = System.Drawing.Color.DarkRed;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance23.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance23.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.btnReset.Appearance = appearance23;
            this.btnReset.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance24.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.btnReset.HotTrackAppearance = appearance24;
            this.btnReset.Location = new System.Drawing.Point(424, 12);
            this.btnReset.Name = "btnReset";
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.btnReset.PressedAppearance = appearance25;
            this.btnReset.Size = new System.Drawing.Size(75, 30);
            this.btnReset.TabIndex = 17;
            this.btnReset.Text = "Tải lại";
            // 
            // btnUnRecorded
            // 
            appearance26.BackColor = System.Drawing.Color.DarkRed;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance26.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance26.Image = global::Accounting.Properties.Resources.boghi;
            this.btnUnRecorded.Appearance = appearance26;
            this.btnUnRecorded.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance27.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.btnUnRecorded.HotTrackAppearance = appearance27;
            this.btnUnRecorded.Location = new System.Drawing.Point(334, 12);
            this.btnUnRecorded.Name = "btnUnRecorded";
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.btnUnRecorded.PressedAppearance = appearance28;
            this.btnUnRecorded.Size = new System.Drawing.Size(84, 30);
            this.btnUnRecorded.TabIndex = 16;
            this.btnUnRecorded.Text = "Bỏ ghi sổ";
            // 
            // btnRecorded
            // 
            appearance29.BackColor = System.Drawing.Color.DarkRed;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance29.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance29.Image = global::Accounting.Properties.Resources.ubtnPost;
            this.btnRecorded.Appearance = appearance29;
            this.btnRecorded.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance30.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.btnRecorded.HotTrackAppearance = appearance30;
            this.btnRecorded.Location = new System.Drawing.Point(253, 12);
            this.btnRecorded.Name = "btnRecorded";
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.btnRecorded.PressedAppearance = appearance31;
            this.btnRecorded.Size = new System.Drawing.Size(75, 30);
            this.btnRecorded.TabIndex = 15;
            this.btnRecorded.Text = "Ghi sổ";
            // 
            // btnAdd
            // 
            appearance32.BackColor = System.Drawing.Color.DarkRed;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance32.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance32.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.btnAdd.Appearance = appearance32;
            this.btnAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance33.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.btnAdd.HotTrackAppearance = appearance33;
            this.btnAdd.Location = new System.Drawing.Point(10, 12);
            this.btnAdd.Name = "btnAdd";
            appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.btnAdd.PressedAppearance = appearance34;
            this.btnAdd.Size = new System.Drawing.Size(75, 30);
            this.btnAdd.TabIndex = 14;
            this.btnAdd.Text = "Thêm";
            // 
            // btnDelete
            // 
            appearance35.BackColor = System.Drawing.Color.DarkRed;
            appearance35.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance35.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance35.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.btnDelete.Appearance = appearance35;
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance36.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance36.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.btnDelete.HotTrackAppearance = appearance36;
            this.btnDelete.Location = new System.Drawing.Point(172, 12);
            this.btnDelete.Name = "btnDelete";
            appearance37.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.btnDelete.PressedAppearance = appearance37;
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 13;
            this.btnDelete.Text = "Xóa";
            // 
            // btnEdit
            // 
            appearance38.BackColor = System.Drawing.Color.DarkRed;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance38.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance38.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.btnEdit.Appearance = appearance38;
            this.btnEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance39.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance39.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.btnEdit.HotTrackAppearance = appearance39;
            this.btnEdit.Location = new System.Drawing.Point(91, 12);
            this.btnEdit.Name = "btnEdit";
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.btnEdit.PressedAppearance = appearance40;
            this.btnEdit.Size = new System.Drawing.Size(75, 30);
            this.btnEdit.TabIndex = 12;
            this.btnEdit.Text = "Xem";
            this.btnEdit.Visible = false;
            // 
            // cms4ButtonPPDiscountReturn
            // 
            this.cms4ButtonPPDiscountReturn.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnPPReturn,
            this.btnPPDiscount});
            this.cms4ButtonPPDiscountReturn.Name = "cms4Button";
            this.cms4ButtonPPDiscountReturn.Size = new System.Drawing.Size(223, 48);
            // 
            // btnPPReturn
            // 
            this.btnPPReturn.Image = global::Accounting.Properties.Resources._1424873317_arrow_right_square_black_20;
            this.btnPPReturn.Name = "btnPPReturn";
            this.btnPPReturn.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.btnPPReturn.Size = new System.Drawing.Size(222, 22);
            this.btnPPReturn.Text = "Hàng mua trả lại";
            this.btnPPReturn.Click += new System.EventHandler(this.btnPPReturn_Click);
            // 
            // btnPPDiscount
            // 
            this.btnPPDiscount.Image = global::Accounting.Properties.Resources._1424873317_arrow_right_square_black_20;
            this.btnPPDiscount.Name = "btnPPDiscount";
            this.btnPPDiscount.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.btnPPDiscount.Size = new System.Drawing.Size(222, 22);
            this.btnPPDiscount.Text = "Hàng mua giảm giá";
            this.btnPPDiscount.Click += new System.EventHandler(this.btnPPDiscount_Click);
            // 
            // cms4ButtonSAReturns
            // 
            this.cms4ButtonSAReturns.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSAReturn,
            this.btnSADiscount});
            this.cms4ButtonSAReturns.Name = "cms4Button";
            this.cms4ButtonSAReturns.Size = new System.Drawing.Size(218, 48);
            // 
            // btnSAReturn
            // 
            this.btnSAReturn.Image = global::Accounting.Properties.Resources._1424873317_arrow_right_square_black_20;
            this.btnSAReturn.Name = "btnSAReturn";
            this.btnSAReturn.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.btnSAReturn.Size = new System.Drawing.Size(217, 22);
            this.btnSAReturn.Text = "Hàng bán trả lại";
            this.btnSAReturn.Click += new System.EventHandler(this.btnSAReturn_Click);
            // 
            // btnSADiscount
            // 
            this.btnSADiscount.Image = global::Accounting.Properties.Resources._1424873317_arrow_right_square_black_20;
            this.btnSADiscount.Name = "btnSADiscount";
            this.btnSADiscount.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.btnSADiscount.Size = new System.Drawing.Size(217, 22);
            this.btnSADiscount.Text = "Giảm giá hàng bán";
            this.btnSADiscount.Click += new System.EventHandler(this.btnSADiscount_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.grTitle);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(810, 47);
            this.panel1.TabIndex = 22;
            // 
            // BaseBusiness
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 566);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ultraSplitter1);
            this.Controls.Add(this.panel3);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.KeyPreview = true;
            this.Name = "BaseBusiness";
            this.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BaseBusiness_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.BaseBusiness_FormClosed);
            this.Load += new System.EventHandler(this.BaseBusiness_Load);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl2.ResumeLayout(false);
            this.palGrid.ClientArea.ResumeLayout(false);
            this.palGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetail)).EndInit();
            this.cms4Grid.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTree)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTabControl)).EndInit();
            this.uTabControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grTitle)).EndInit();
            this.grTitle.ResumeLayout(false);
            this.grTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).EndInit();
            this.cms4ButtonPPDiscountReturn.ResumeLayout(false);
            this.cms4ButtonSAReturns.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmEdit;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripMenuItem tsmReLoad;
        public Infragistics.Win.UltraWinTree.UltraTree uTree;
        private System.Windows.Forms.Panel panel2;
        public Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private System.Windows.Forms.Panel panel3;
        protected Infragistics.Win.UltraWinTabControl.UltraTabControl uTabControl;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDetail;
        private System.Windows.Forms.Panel palTtc;
        private Infragistics.Win.Misc.UltraPanel palGrid;
        public Infragistics.Win.UltraWinGrid.UltraGrid uGrid0;
        private Infragistics.Win.Misc.UltraGroupBox grTitle;
        public Infragistics.Win.Misc.UltraButton btnAdd;
        public Infragistics.Win.Misc.UltraButton btnDelete;
        public Infragistics.Win.Misc.UltraButton btnEdit;
        public Infragistics.Win.Misc.UltraButton btnReset;
        public Infragistics.Win.Misc.UltraButton btnUnRecorded;
        public Infragistics.Win.Misc.UltraButton btnRecorded;
        public Infragistics.Win.Misc.UltraButton btnHelp;
        public Infragistics.Win.Misc.UltraButton btnPrint;
        private System.Windows.Forms.ToolStripMenuItem tsmPost;
        private System.Windows.Forms.ToolStripMenuItem tsmUnPost;
        private Infragistics.Win.Misc.UltraDropDownButton drpbtnAdd;
        private System.Windows.Forms.ContextMenuStrip cms4ButtonPPDiscountReturn;
        private System.Windows.Forms.ToolStripMenuItem btnPPReturn;
        private System.Windows.Forms.ToolStripMenuItem btnPPDiscount;
        private System.Windows.Forms.ContextMenuStrip cms4ButtonSAReturns;
        private System.Windows.Forms.ToolStripMenuItem btnSAReturn;
        private System.Windows.Forms.ToolStripMenuItem btnSADiscount;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private System.Windows.Forms.ToolStripMenuItem tsmSelectAll;
        public Infragistics.Win.Misc.UltraButton btnUpload;
        private Infragistics.Win.Misc.UltraLabel lblKyKeToan;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDateTime;
        private Infragistics.Win.Misc.UltraLabel lblBeginDate;
        private Infragistics.Win.Misc.UltraLabel lblEndDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtBeginDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtEndDate;
        public Infragistics.Win.Misc.UltraButton btnLoc;
        public Infragistics.Win.Misc.UltraButton btnExportExcel;
        private System.Windows.Forms.Panel panel1;
    }
}