﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
//using HideTabHeaders_CS;
using Infragistics.Win.UltraWinEditors;
using System.Linq;
using Infragistics.Win.UltraWinGrid;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinTabControl;

namespace Accounting
{
    public partial class FPPServiceDetail : FPPServiceDetailTmp
    {
        #region khởi tạo
        private bool _isFirst = true;
        private bool _isFeightService = false;
        private UltraGrid _uGrid0;
        private UltraGrid _uGrid1;
        UltraGrid ugrid2 = null;
        UltraTabControl ultraTab1;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        public FPPServiceDetail(PPService temp, List<PPService> listObject, int statusForm)
        {//Add -> thêm, Sửa -> sửa, Xem -> Sửa ở trạng thái Enable = False
            InitializeComponent();
            base.InitializeComponent1();

            #region Thiết lập ban đầu
            _typeID = 240; _statusForm = statusForm;
            if (statusForm == ConstFrm.optStatusForm.Add && temp == null)
            {
                _isFeightService = true;

            }
            if (statusForm == ConstFrm.optStatusForm.Add)
                _select.TypeID = _typeID;
            else _select = temp;
            _listSelects.AddRange(listObject);
            InitializeGUI(_select); //khởi tạo và hiển thị giá trị ban đầu cho các control
            ReloadToolbar(_select, listObject, statusForm);  //Change Menu Top
            Utils.isPPInvoice = true;
            if (statusForm == ConstFrm.optStatusForm.View)
            {
                this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccreditativeAccountingObjectID, "AccountingObjectID", cbbAccreditativeAccountingObjectBankAccount, "AccountingObjectBankAccountDetailID", accountingObjectType: 1);
            }
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 5;
                this.Location = new System.Drawing.Point(((Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2), ((Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2));
            }
            #endregion
        }
        #endregion

        #region override
        public override void InitializeGUI(PPService input)
        {
            Template mauGiaoDien = Utils.GetMauGiaoDien(input.TypeID, input.TemplateID, Utils.ListTemplate);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;
            // config số đơn hàng, ngày đơn hàng
            this.ConfigTopVouchersNo<PPService>(palTopVouchers, "No", "PostedDate", "Date", resetNo: _statusForm == ConstFrm.optStatusForm.Add);
            #region Top
            List<int> lstId = new List<int> { 116, 126, 133, 143, 173 };
            List<string> lstType = new List<string> { "Tiền mặt", "Ủy nhiệm chi", "Séc chuyển khoản", "Séc tiền mặt", "Thẻ tín dụng" };
            cbbChonThanhToan.Items.Clear();
            for (int i = 0; i < lstId.Count; i++)
            {
                cbbChonThanhToan.Items.Add(lstId[i], lstType[i]);
            }

            if (_statusForm == ConstFrm.optStatusForm.Add) optThanhToan.Value = false;
            else
            {
                if (_select.TypeID == 240) optThanhToan.Value = false;
                else
                {
                    int typeId = _select.TypeID;
                    optThanhToan.Value = true;
                    _select.TypeID = typeId;
                    cbbChonThanhToan.SelectedItem = cbbChonThanhToan.Items.ValueList.FindByDataValue(_select.TypeID);
                }
            }
            _backSelect.TypeID = _select.TypeID.CloneObject();
            _backSelect.No = _select.No.CloneObject();
            #endregion
            //Config Grid
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                bdlRefVoucher = new BindingList<RefVoucher>(input.RefVouchers);

            }

            _listObjectInput.Add(new BindingList<PPServiceDetail>(_statusForm == ConstFrm.optStatusForm.Add && !_isFeightService ? new List<PPServiceDetail>() : input.PPServiceDetails));
            _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };

            this.ConfigGridByTemplete_General<PPService>(palGrid, mauGiaoDien);
            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
            this.ConfigGridByManyTemplete<PPService>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
            List<PPService> select = new List<PPService> { input };
            _select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmount;
            _select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add ? "VND" : input.CurrencyID;
            if (input.HasProperty("TypeID") && input.HasProperty("Exported"))
            {
                int type = int.Parse(_select.GetProperty("TypeID").ToString());
                var isExported = (bool)_select.GetProperty("Exported");
                var ultraGrid = (UltraGrid)palGrid.Controls.Find("uGrid1", true).FirstOrDefault();
                ultraGrid.ConfigColumnByExported(isExported, _select.GetProperty("CurrencyID").ToString(), type);
                //ultraGrid.ConfigColumnByImportPurchase(isExported, _select.GetProperty("CurrencyID").ToString(), _statusForm);
            }

            this.ConfigGridCurrencyByTemplate<PPService>(_select.TypeID, new BindingList<System.Collections.IList> { select },
                                              uGridControl, mauGiaoDien);
            //ConfigGridBase(input, uGridControl, palGrid);
            //config combo
            Utils.isPPInvoice = true;
            this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 1);
            this.ConfigCombo(Utils.ListAccountingObject, cbbPaymentAccountingObjectID, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 1);
            this.ConfigCombo(input, Utils.ListAccountingObject, cbbAccreditativeAccountingObjectID, "AccountingObjectID", cbbAccreditativeAccountingObjectBankAccount, "AccountingObjectBankAccountDetailID", accountingObjectType: 1);
            this.ConfigCombo(input, Utils.ListAccountingObject, cbbSecCKAccountingObjectID, "AccountingObjectID", cbbSecCKAccountingObjectBankAccount, "AccountingObjectBankAccountDetailID", accountingObjectType: 1);
            this.ConfigCombo(Utils.ListAccountingObject, cbbSecAccountingObjectID, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 1);
            this.ConfigCombo(input, Utils.ListAccountingObject, cbbCreditCardAccountingObjectID, "AccountingObjectID", cbbCreditCardAccountingObjectBankAccount, "AccountingObjectBankAccountDetailID", accountingObjectType: 1);
            this.ConfigCombo(Utils.ListBankAccountDetail, cbbPaymentOrderBankAccountDetailID, "BankAccount", "ID", input, "BankAccountDetailID");
            this.ConfigCombo(Utils.ListBankAccountDetail, cbbSecCKBankAccountDetailID, "BankAccount", "ID", input, "BankAccountDetailID");
            this.ConfigCombo(Utils.ListBankAccountDetail, cbbSecBankAccountDetailID, "BankAccount", "ID", input, "BankAccountDetailID");
            this.ConfigCombo(input, "CreditCardNumber", Utils.ListCreditCard, cbbCreditCardNumber, picCreditCardType, txtCreditCardType, txtOwnerCard);
            //Databinding
            DataBinding(new List<Control> { txtAccountingObjectName, txtAccountingObjectAddress, txtReason }, "AccountingObjectName,AccountingObjectAddress,Reason");
            DataBinding(new List<Control> { txtPaymentAccountingObjectName, txtPaymentAccountingObjectAddress, txtPaymentReason, txtReceiver, txtAccreditativeAttach }, "AccountingObjectName,AccountingObjectAddress,Reason,ContactName,NumberAttach");
            DataBinding(new List<Control> { txtPaymentOrderBankName, txtPaymentOrderReason, txtAccreditativeAccountingObjectName, txtAccreditativeAccountingObjectAddress, txtAccreditativeAccountingObjectBankName }, "BankName,Reason,AccountingObjectName,AccountingObjectAddress,AccountingObjectBankName");
            DataBinding(new List<Control> { txtSecCKBankName, txtSecCKReason, txtSecCKAccountingObjectName, txtSecCKAccountingObjectAddress, txtSecCKAccountingObjectBankName }, "BankName,Reason,AccountingObjectName,AccountingObjectAddress,AccountingObjectBankName");
            DataBinding(new List<Control> { txtSecBankName, txtSecReason, txtSecAccountingObjectName, txtSecReceiver, txtSecIdentificationNo, dtSecIssueDate, txtSecIssueBy }, "BankName,Reason,AccountingObjectName,ContactName,IdentificationNo,IssueDate,IssueBy", "0,0,0,0,0,2,0");
            DataBinding(new List<Control> { txtCreditCardReason, txtCreditCardAccountingObjectName, txtCreditCardAccountingObjectAddress, txtCreditCardAccountingObjectBankName }, "Reason,AccountingObjectName,AccountingObjectAddress,AccountingObjectBankName");
            DataBinding(new List<Control> { chkIsFeightService }, "IsFeightService", "1");
            DataBinding(new List<Control> { chkBillReceived }, "BillReceived", "1");

            #region Phần cuối
            lblTotalAmountOriginal.DataBindings.Clear();
            lblTotalAmountOriginal.BindControl("Text", input, "TotalAmountOriginal", ConstDatabase.Format_TienVND, ConstDatabase.Format_ForeignCurrency);
            lblTotalAmount.DataBindings.Clear();
            lblTotalAmount.BindControl("Text", input, "TotalAmount", ConstDatabase.Format_TienVND);
            lblTotalDiscountAmountOriginal.DataBindings.Clear();
            lblTotalDiscountAmountOriginal.BindControl("Text", input, "TotalDiscountAmountOriginal", ConstDatabase.Format_TienVND, ConstDatabase.Format_ForeignCurrency);
            lblTotalDiscountAmount.DataBindings.Clear();
            lblTotalDiscountAmount.BindControl("Text", input, "TotalDiscountAmount", ConstDatabase.Format_TienVND);
            lblTotalAllOriginal.DataBindings.Clear();
            lblTotalAllOriginal.BindControl("Text", input, "TotalAllOriginal", ConstDatabase.Format_TienVND, ConstDatabase.Format_ForeignCurrency);
            lblTotalAll.DataBindings.Clear();
            lblTotalAll.BindControl("Text", input, "TotalAll", ConstDatabase.Format_TienVND);
            lblTotalVATAmount.DataBindings.Clear();
            lblTotalVATAmount.BindControl("Text", input, "TotalVATAmount", ConstDatabase.Format_TienVND);
            lblTotalVATAmountOriginal.DataBindings.Clear();
            lblTotalVATAmountOriginal.BindControl("Text", input, "TotalVATAmountOriginal", ConstDatabase.Format_TienVND, ConstDatabase.Format_ForeignCurrency);
            chkIsFeightService.Enabled = !_isFeightService;

            #endregion
            _uGrid0 = (UltraGrid)Controls.Find("uGrid0", true).FirstOrDefault();
            _uGrid1 = (UltraGrid)Controls.Find("uGrid1", true).FirstOrDefault();
            ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
            ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            if (_statusForm == ConstFrm.optStatusForm.Add)
                _select.ID = Guid.NewGuid();
            lblBillReceived.Visible = false;
            chkBillReceived.Visible = false;
            //lblBillReceived.Visible = chkBillReceived.CheckState == CheckState.Checked;
            //if (_statusForm != ConstFrm.optStatusForm.View)
            //{
            //    if (chkBillReceived.CheckState == CheckState.Checked)
            //    {
            //        {
            //            _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].CellActivation = Activation.AllowEdit;
            //            _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].CellActivation = Activation.AllowEdit;
            //            _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellActivation = Activation.AllowEdit;
            //            _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].CellActivation = Activation.AllowEdit;
            //        }
            //    }
            //    else
            //    {
            //        {
            //            _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].CellActivation = Activation.Disabled;
            //            _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].CellActivation = Activation.Disabled;
            //            _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellActivation = Activation.Disabled;
            //            _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].CellActivation = Activation.Disabled;
            //        }

            //    }
            //}
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 240)//trungnq thêm vào để  làm task 6234
                {
                    txtPaymentOrderReason.Value = "Mua dịch vụ";
                    txtPaymentOrderReason.Text = "Mua dịch vụ";
                    _select.Reason = "Mua dịch vụ";
                };
            }
            var uTabControl = Controls.Find("ultraTabControl", true)[0] as UltraTabControl;

            if (uTabControl != null)
            {
                uTabControl.TabPadding = new Size(1, 4);
            }

            //Quuân edit
            uTabControl.SelectedTabChanged += (s, e) => ultraTabControl_SelectedTabChanged(s, e);
            ultraTab1 = uTabControl;
            txtMauSoHDNew.TextChanged += (s, e) => txtMauSoHD_TextChanged(s, e);
            txtMauSoHD1.TextChanged += (s, e) => txtMauSoHD_TextChanged(s, e);
            txtMauSoHD2.TextChanged += (s, e) => txtMauSoHD_TextChanged(s, e);
            txtMauSoHD3.TextChanged += (s, e) => txtMauSoHD_TextChanged(s, e);
            txtMauSoHD4.TextChanged += (s, e) => txtMauSoHD_TextChanged(s, e);
            txtKyHieuHDNew.TextChanged += (s, e) => txtKyHieuHD_TextChanged(s, e);
            txtKyHieuHD1.TextChanged += (s, e) => txtKyHieuHD_TextChanged(s, e);
            txtKyHieuHD2.TextChanged += (s, e) => txtKyHieuHD_TextChanged(s, e);
            txtKyHieuHD3.TextChanged += (s, e) => txtKyHieuHD_TextChanged(s, e);
            txtKyHieuHD4.TextChanged += (s, e) => txtKyHieuHD_TextChanged(s, e);
            txtSoHDNew.TextChanged += (s, e) => txtSoHD_TextChanged(s, e);
            txtSoHD1.TextChanged += (s, e) => txtSoHD_TextChanged(s, e);
            txtSoHD2.TextChanged += (s, e) => txtSoHD_TextChanged(s, e);
            txtSoHD3.TextChanged += (s, e) => txtSoHD_TextChanged(s, e);
            txtSoHD4.TextChanged += (s, e) => txtSoHD_TextChanged(s, e);
            dteNgayHDNew.ValueChanged += (s, e) => dteNgayHD_ValueChanged(s, e);
            dteNgayHD1.ValueChanged += (s, e) => dteNgayHD_ValueChanged(s, e);
            dteNgayHD2.ValueChanged += (s, e) => dteNgayHD_ValueChanged(s, e);
            dteNgayHD3.ValueChanged += (s, e) => dteNgayHD_ValueChanged(s, e);
            dteNgayHD4.ValueChanged += (s, e) => dteNgayHD_ValueChanged(s, e);
        }
        #endregion
        private void ultraTabControl_SelectedTabChanged(object sender, SelectedTabChangedEventArgs e)
        {
            if (pnNoPayment.Visible == true)
            {
                grHoaDon.Visible = false;
                ugbThongTinChung.Dock = DockStyle.Fill;
                var ppInvoid = new PPServiceDetail();
                var ppInvoiddb = _select.PPServiceDetails.FirstOrDefault();
                if (ppInvoiddb != null)
                {
                    ppInvoid = ppInvoiddb;
                }
                if (ppInvoid.InvoiceTemplate != "" || ppInvoid.InvoiceSeries != "" || ppInvoid.InvoiceNo != "" || ppInvoid.InvoiceDate != null)
                    chkBillReceived.CheckState = CheckState.Checked;
                if (e.Tab.Key == "uTab1")
                {
                    lblBillReceived.Visible = false;
                    chkBillReceived.Visible = false;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].CellActivation = Activation.NoEdit;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].CellActivation = Activation.NoEdit;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellActivation = Activation.NoEdit;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].CellActivation = Activation.NoEdit;

                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;


                    txtMauSoHD.Text = ppInvoid.InvoiceTemplate != null ? ppInvoid.InvoiceTemplate : txtMauSoHD.Text;
                    txtKyHieuHD.Text = ppInvoid.InvoiceSeries != null ? ppInvoid.InvoiceSeries : txtKyHieuHD.Text;
                    txtSoHD.Text = ppInvoid.InvoiceNo != null ? ppInvoid.InvoiceNo : txtSoHD.Text;
                    dteNgayHD.Value = ppInvoid.InvoiceDate != null ? ppInvoid.InvoiceDate : dteNgayHD.Value;

                    grHoaDon.Visible = true;
                    grHoaDon.Anchor = AnchorStyles.Right;
                    grHoaDon.Anchor = AnchorStyles.Top;
                    ugbThongTinChung.Anchor = AnchorStyles.Left;
                    ugbThongTinChung.Anchor = AnchorStyles.Right;
                    ugbThongTinChung.Anchor = AnchorStyles.Top;
                    ugbThongTinChung.Width = pnNoPayment.Width - grHoaDon.Width;
                }
            }
            if (paneTTChungNew.Visible == true)
            {
                grHoaDonNew.Visible = false;
                gpMCPayment.Dock = DockStyle.Fill;
                var ppInvoid = new PPServiceDetail();
                var ppInvoiddb = _select.PPServiceDetails.FirstOrDefault();
                if (ppInvoiddb != null)
                {
                    ppInvoid = ppInvoiddb;
                }
                if (ppInvoid.InvoiceTemplate != "" || ppInvoid.InvoiceSeries != "" || ppInvoid.InvoiceNo != "" || ppInvoid.InvoiceDate != null)
                    chkBillReceived.CheckState = CheckState.Checked;
                if (e.Tab.Key == "uTab1")
                {
                    lblBillReceived.Visible = false;
                    chkBillReceived.Visible = false;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].CellActivation = Activation.NoEdit;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].CellActivation = Activation.NoEdit;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellActivation = Activation.NoEdit;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].CellActivation = Activation.NoEdit;

                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;


                    txtMauSoHDNew.Text = ppInvoid.InvoiceTemplate != null ? ppInvoid.InvoiceTemplate : txtMauSoHDNew.Text;
                    txtKyHieuHDNew.Text = ppInvoid.InvoiceSeries != null ? ppInvoid.InvoiceSeries : txtKyHieuHDNew.Text;
                    txtSoHDNew.Text = ppInvoid.InvoiceNo != null ? ppInvoid.InvoiceNo : txtSoHDNew.Text;
                    dteNgayHDNew.Value = ppInvoid.InvoiceDate != null ? ppInvoid.InvoiceDate : dteNgayHDNew.Value;

                    grHoaDonNew.Visible = true;
                    grHoaDonNew.Dock = DockStyle.Right;
                    gpMCPayment.Dock = DockStyle.Left;
                    gpMCPayment.Width = paneTTChungNew.Width - grHoaDonNew.Width;
                }
            }
            if (pnMBTellerPaper_PayOrder.Visible == true)
            {
                grHoaDon1.Visible = false;
                gpAccountingObject.Dock = DockStyle.Fill;
                var ppInvoid = new PPServiceDetail();
                var ppInvoiddb = _select.PPServiceDetails.FirstOrDefault();
                if (ppInvoiddb != null)
                {
                    ppInvoid = ppInvoiddb;
                }
                if (ppInvoid.InvoiceTemplate != "" || ppInvoid.InvoiceSeries != "" || ppInvoid.InvoiceNo != "" || ppInvoid.InvoiceDate != null)
                    chkBillReceived.CheckState = CheckState.Checked;
                if (e.Tab.Key == "uTab1")
                {
                    lblBillReceived.Visible = false;
                    chkBillReceived.Visible = false;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;

                    txtMauSoHD1.Text = ppInvoid.InvoiceTemplate != null ? ppInvoid.InvoiceTemplate : txtMauSoHD1.Text;
                    txtKyHieuHD1.Text = ppInvoid.InvoiceSeries != null ? ppInvoid.InvoiceSeries : txtKyHieuHD1.Text;
                    txtSoHD1.Text = ppInvoid.InvoiceNo != null ? ppInvoid.InvoiceNo : txtSoHD1.Text;
                    dteNgayHD1.Value = ppInvoid.InvoiceDate != null ? ppInvoid.InvoiceDate : dteNgayHD1.Value;

                    grHoaDon1.Visible = true;
                    grHoaDon1.Dock = DockStyle.Right;
                    gpAccountingObject.Dock = DockStyle.Left;
                    gpAccountingObject.Width = pnMBTellerPaper_PayOrder.Width - grHoaDon1.Width;
                }
            }
            if (pnMBTellerPaper_SecCK.Visible == true)
            {
                grHoaDon2.Visible = false;
                ultraGroupBox3.Dock = DockStyle.Fill;
                var ppInvoid = new PPServiceDetail();
                var ppInvoiddb = _select.PPServiceDetails.FirstOrDefault();
                if (ppInvoiddb != null)
                {
                    ppInvoid = ppInvoiddb;
                }
                if (ppInvoid.InvoiceTemplate != "" || ppInvoid.InvoiceSeries != "" || ppInvoid.InvoiceNo != "" || ppInvoid.InvoiceDate != null)
                    chkBillReceived.CheckState = CheckState.Checked;
                if (e.Tab.Key == "uTab1")
                {
                    lblBillReceived.Visible = false;
                    chkBillReceived.Visible = false;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;

                    txtMauSoHD2.Text = ppInvoid.InvoiceTemplate != null ? ppInvoid.InvoiceTemplate : txtMauSoHD2.Text;
                    txtKyHieuHD2.Text = ppInvoid.InvoiceSeries != null ? ppInvoid.InvoiceSeries : txtKyHieuHD2.Text;
                    txtSoHD2.Text = ppInvoid.InvoiceNo != null ? ppInvoid.InvoiceNo : txtSoHD2.Text;
                    dteNgayHD2.Value = ppInvoid.InvoiceDate != null ? ppInvoid.InvoiceDate : dteNgayHD2.Value;

                    grHoaDon2.Visible = true;
                    grHoaDon2.Dock = DockStyle.Right;
                    ultraGroupBox3.Dock = DockStyle.Left;
                    ultraGroupBox3.Width = pnMBTellerPaper_SecCK.Width - grHoaDon2.Width;
                }
            }
            if (pnMBCreditCard.Visible == true)
            {
                grHoaDon3.Visible = false;
                ultraGroupBox5.Dock = DockStyle.Fill;
                var ppInvoid = new PPServiceDetail();
                var ppInvoiddb = _select.PPServiceDetails.FirstOrDefault();
                if (ppInvoiddb != null)
                {
                    ppInvoid = ppInvoiddb;
                }
                if (ppInvoid.InvoiceTemplate != "" || ppInvoid.InvoiceSeries != "" || ppInvoid.InvoiceNo != "" || ppInvoid.InvoiceDate != null)
                    chkBillReceived.CheckState = CheckState.Checked;
                if (e.Tab.Key == "uTab1")
                {
                    lblBillReceived.Visible = false;
                    chkBillReceived.Visible = false;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;

                    txtMauSoHD3.Text = ppInvoid.InvoiceTemplate != null ? ppInvoid.InvoiceTemplate : txtMauSoHD3.Text;
                    txtKyHieuHD3.Text = ppInvoid.InvoiceSeries != null ? ppInvoid.InvoiceSeries : txtKyHieuHD3.Text;
                    txtSoHD3.Text = ppInvoid.InvoiceNo != null ? ppInvoid.InvoiceNo : txtSoHD3.Text;
                    dteNgayHD3.Value = ppInvoid.InvoiceDate != null ? ppInvoid.InvoiceDate : dteNgayHD3.Value;

                    grHoaDon3.Visible = true;
                    grHoaDon3.Dock = DockStyle.Right;
                    ultraGroupBox5.Dock = DockStyle.Left;
                    ultraGroupBox5.Width = pnMBCreditCard.Width - grHoaDon3.Width;
                }
            }
            if (pnMBTellerPaper_Sec.Visible == true)
            {
                grHoaDon4.Visible = false;
                ultraGroupBox7.Dock = DockStyle.Fill;
                var ppInvoid = new PPServiceDetail();
                var ppInvoiddb = _select.PPServiceDetails.FirstOrDefault();
                if (ppInvoiddb != null)
                {
                    ppInvoid = ppInvoiddb;
                }
                if (ppInvoid.InvoiceTemplate != "" || ppInvoid.InvoiceSeries != "" || ppInvoid.InvoiceNo != "" || ppInvoid.InvoiceDate != null)
                    chkBillReceived.CheckState = CheckState.Checked;
                if (e.Tab.Key == "uTab1")
                {
                    lblBillReceived.Visible = false;
                    chkBillReceived.Visible = false;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
                    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;

                    txtMauSoHD4.Text = ppInvoid.InvoiceTemplate != null ? ppInvoid.InvoiceTemplate : txtMauSoHD4.Text;
                    txtKyHieuHD4.Text = ppInvoid.InvoiceSeries != null ? ppInvoid.InvoiceSeries : txtKyHieuHD4.Text;
                    txtSoHD4.Text = ppInvoid.InvoiceNo != null ? ppInvoid.InvoiceNo : txtSoHD4.Text;
                    dteNgayHD4.Value = ppInvoid.InvoiceDate != null ? ppInvoid.InvoiceDate : dteNgayHD4.Value;

                    grHoaDon4.Visible = true;
                    grHoaDon4.Dock = DockStyle.Right;
                    ultraGroupBox7.Dock = DockStyle.Left;
                    ultraGroupBox7.Width = pnMBTellerPaper_Sec.Width - grHoaDon4.Width;
                }
            }

        }
        #region Event
        private void OptThanhToanValueChanged(object sender, EventArgs e)
        {
            if (_statusForm == ConstFrm.optStatusForm.Edit)
            {
                var opt1 = (UltraOptionSet_Ex)sender;
                if (opt1.Value == null) return;
                if (!(bool)opt1.Value)
                {
                    _select.TypeID = 240;
                    cbbChonThanhToan.Enabled = false;
                    bool status = true;
                    if (_statusForm != ConstFrm.optStatusForm.Add)
                        status = _select.TypeID != _backSelect.TypeID;
                    ConfigGroup(status);
                }
                else
                {
                    if ((Utils.ListMCPaymentDetailVendor.Where(n => n.PPInvoiceID == _select.ID).ToList().Count != 0) || (Utils.ListMBTellerPaperDetailVendor.Where(n => n.PPInvoiceID == _select.ID).ToList().Count != 0) || (Utils.ListMBCreditCardDetailVendor.Where(n => n.PPInvoiceID == _select.ID).ToList().Count != 0))
                    {
                        MSG.Warning("Không thể thực hiện thao tác này! Chứng từ đã phát sinh nghiệp vụ trả tiền nhà cung cấp");
                        opt1.Value = false;
                    }
                    else
                    {
                        cbbChonThanhToan.Enabled = true;
                        if (cbbChonThanhToan.SelectedItem == null)
                        {
                            cbbChonThanhToan.SelectedItem = cbbChonThanhToan.Items.ValueList.FindByDataValue(116);
                            return;
                        }
                        _select.TypeID = (int)cbbChonThanhToan.Value;
                        bool isResetNo = true;
                        if (_statusForm != ConstFrm.optStatusForm.Add)
                            isResetNo = _select.TypeID != _backSelect.TypeID;
                        ConfigGroup(isResetNo);
                    }
                }
                var hienthi = !(new List<int> { 116, 126, 133, 143, 173 }.Any(x => x == _select.TypeID) && _select.CurrencyID != "VND");
                if (_uGrid0 != null)
                {
                    var band = _uGrid0.DisplayLayout.Bands[0];
                    band.Columns["CashOutExchangeRate"].Hidden = hienthi;
                    band.Columns["CashOutAmount"].Hidden = hienthi;
                    band.Columns["CashOutDifferAmount"].Hidden = hienthi;
                    band.Columns["CashOutDifferAccount"].Hidden = hienthi;
                }
                if (_uGrid1 != null)
                {
                    var band = _uGrid1.DisplayLayout.Bands[0];
                    band.Columns["CashOutVATAmount"].Hidden = hienthi;
                    band.Columns["CashOutDifferVATAmount"].Hidden = hienthi;
                }
            }
            else
            {
                var opt = (UltraOptionSet_Ex)sender;
                if (opt.Value == null) return;
                if (!(bool)opt.Value)
                {
                    _select.TypeID = 240;
                    cbbChonThanhToan.Enabled = false;
                    bool status = true;
                    if (_statusForm != ConstFrm.optStatusForm.Add)
                        status = _select.TypeID != _backSelect.TypeID;
                    ConfigGroup(status);
                }
                else
                {
                    cbbChonThanhToan.Enabled = true;
                    if (cbbChonThanhToan.SelectedItem == null)
                    {
                        cbbChonThanhToan.SelectedItem = cbbChonThanhToan.Items.ValueList.FindByDataValue(116);
                        return;
                    }
                    _select.TypeID = (int)cbbChonThanhToan.Value;
                    bool isResetNo = true;
                    if (_statusForm != ConstFrm.optStatusForm.Add)
                        isResetNo = _select.TypeID != _backSelect.TypeID;
                    ConfigGroup(isResetNo);
                }
                var hienthi = !(new List<int> { 116, 126, 133, 143, 173 }.Any(x => x == _select.TypeID) && _select.CurrencyID != "VND");
                if (_uGrid0 != null)
                {
                    var band = _uGrid0.DisplayLayout.Bands[0];
                    band.Columns["CashOutExchangeRate"].Hidden = hienthi;
                    band.Columns["CashOutAmount"].Hidden = hienthi;
                    band.Columns["CashOutDifferAmount"].Hidden = hienthi;
                    band.Columns["CashOutDifferAccount"].Hidden = hienthi;
                }
                if (_uGrid1 != null)
                {
                    var band = _uGrid1.DisplayLayout.Bands[0];
                    band.Columns["CashOutVATAmount"].Hidden = hienthi;
                    band.Columns["CashOutDifferVATAmount"].Hidden = hienthi;
                }
            }

        }

        private void CbbChonThanhToanValueChanged(object sender, EventArgs e)
        {
            palGrid.Visible = false;
            ultraSplitter1.Visible = false;
            var cbb = (UltraComboEditor)sender;
            if (cbb.SelectedItem == null) return;
            _select.TypeID = (int)cbb.Value;
            bool status = true;
            if (_statusForm != ConstFrm.optStatusForm.Add)
                status = _select.TypeID != _backSelect.TypeID;
            ConfigGroup(status);
            palGrid.Visible = true;
            ultraSplitter1.Visible = true;
            var hienthi = (_select.CurrencyID == "VND");
            if (_uGrid0 != null)
            {
                var band = _uGrid0.DisplayLayout.Bands[0];
                band.Columns["CashOutExchangeRate"].Hidden = hienthi;
                band.Columns["CashOutAmount"].Hidden = hienthi;
                band.Columns["CashOutDifferAmount"].Hidden = hienthi;
                band.Columns["CashOutDifferAccount"].Hidden = hienthi;
            }
            if (_uGrid1 != null)
            {
                var band = _uGrid1.DisplayLayout.Bands[0];
                band.Columns["CashOutVATAmount"].Hidden = hienthi;
                band.Columns["CashOutDifferVATAmount"].Hidden = hienthi;
            }
        }
        #endregion

        #region Utils
        void ConfigByType(PPService input, bool status)
        {
            switch (input.TypeID)
            {
                case 240: //Mua hàng chưa thanh toán
                    #region Mua hàng chưa thanh toán
                    {
                        //optThanhToan.CheckedIndex = 0;  //tự động chọn "chưa thanh toán"
                        cbbChonThanhToan.Enabled = false;
                        pnNoPayment.Visible = true;
                        paneTTChungNew.Visible = false;
                        pnMBTellerPaper_PayOrder.Visible = false;
                        pnMBTellerPaper_SecCK.Visible = false;
                        pnMBTellerPaper_Sec.Visible = false;
                        pnMBCreditCard.Visible = false;
                        palThongtinchung.Height = pnNoPayment.Height;
                        if (ultraTab1 != null)
                            if (ultraTab1.SelectedTab.Key == "uTab1")
                            {
                                ugbThongTinChung.Dock = DockStyle.Left;
                                ugbThongTinChung.Width = pnNoPayment.Width - grHoaDon.Width;
                                grHoaDon.Visible = true;
                                grHoaDon.Dock = DockStyle.Right;
                            }
                            else
                            {
                                grHoaDon.Visible = false;
                                ugbThongTinChung.Dock = DockStyle.Fill;
                            }
                        else
                        {
                            grHoaDon.Visible = false;
                            ugbThongTinChung.Dock = DockStyle.Fill;
                        }
                        if (!_isFirst && status)
                            this.ConfigTopVouchersNo<PPService>(palTopVouchers, "No", "PostedDate", "Date", resetNo: status);
                    }
                    #endregion
                    break;
                case 116: //mua hàng thanh toán ngay tiền mặt
                    #region mua hàng thanh toán ngay tiền mặt
                    {
                        //optThanhToan.CheckedIndex = 1;  //tự động chọn "thanh toán ngay"
                        cbbChonThanhToan.Enabled = true;    //hiện cbb cho chọn thanh toán ngay bằng "tiền mặt" hay "chuyển khoản"
                        pnNoPayment.Visible = false;
                        paneTTChungNew.Visible = true;
                        pnMBTellerPaper_PayOrder.Visible = false;
                        pnMBTellerPaper_SecCK.Visible = false;
                        pnMBTellerPaper_Sec.Visible = false;
                        pnMBCreditCard.Visible = false;
                        palThongtinchung.Height = paneTTChungNew.Height;
                        if (ultraTab1 != null)
                            if (ultraTab1.SelectedTab.Key == "uTab1")
                            {
                                gpMCPayment.Dock = DockStyle.Left;
                                gpMCPayment.Width = paneTTChungNew.Width - grHoaDonNew.Width;
                                grHoaDonNew.Visible = true;
                                grHoaDonNew.Dock = DockStyle.Right;
                            }
                            else
                            {
                                grHoaDonNew.Visible = false;
                                gpMCPayment.Dock = DockStyle.Fill;
                            }
                        else
                        {
                            grHoaDonNew.Visible = false;
                            gpMCPayment.Dock = DockStyle.Fill;
                        }
                        this.ConfigTopVouchersNo<PPService>(palTopVouchers, "No", "PostedDate", "Date", ConstFrm.TypeGroupMCPayment, objectDataSource: _select, resetNo: status);
                    }
                    #endregion
                    break;
                case 126: //mua hàng thanh toán ngay UNC
                    #region mua hàng thanh toán ngay UNC
                    {
                        //optThanhToan.CheckedIndex = 1;  //tự động chọn "thanh toán ngay"
                        cbbChonThanhToan.Enabled = true;    //hiện cbb cho chọn thanh toán ngay bằng "tiền mặt" hay "chuyển khoản"
                        pnNoPayment.Visible = false;
                        paneTTChungNew.Visible = false;
                        pnMBTellerPaper_PayOrder.Visible = true;
                        pnMBTellerPaper_SecCK.Visible = false;
                        pnMBTellerPaper_Sec.Visible = false;
                        pnMBCreditCard.Visible = false;
                        palThongtinchung.Height = pnMBTellerPaper_PayOrder.Height;
                        if (ultraTab1 != null)
                            if (ultraTab1.SelectedTab.Key == "uTab1")
                            {
                                gpAccountingObject.Dock = DockStyle.Left;
                                gpAccountingObject.Width = pnMBTellerPaper_PayOrder.Width - grHoaDon1.Width;
                                grHoaDon1.Visible = true;
                                grHoaDon1.Dock = DockStyle.Right;
                            }
                            else
                            {
                                grHoaDon1.Visible = false;
                                gpAccountingObject.Dock = DockStyle.Fill;
                            }
                        else
                        {
                            grHoaDon1.Visible = false;
                            gpAccountingObject.Dock = DockStyle.Fill;
                        }
                        this.ConfigTopVouchersNo<PPService>(palTopVouchers, "No", "PostedDate", "Date", ConstFrm.TypeGruop_MBTellerPaper, objectDataSource: _select, resetNo: status);
                    }
                    #endregion
                    break;
                case 133: //mua hàng thanh toán ngay chuyển khoản
                    #region mua hàng thanh toán ngay séc chuyển khoản
                    {
                        cbbChonThanhToan.Enabled = true;    //hiện cbb cho chọn thanh toán ngay bằng "tiền mặt" hay "chuyển khoản"
                        pnNoPayment.Visible = false;
                        paneTTChungNew.Visible = false;
                        pnMBTellerPaper_PayOrder.Visible = false;
                        pnMBTellerPaper_SecCK.Visible = true;
                        pnMBTellerPaper_Sec.Visible = false;
                        pnMBCreditCard.Visible = false;
                        palThongtinchung.Height = pnMBTellerPaper_SecCK.Height;
                        if (ultraTab1 != null)
                            if (ultraTab1.SelectedTab.Key == "uTab1")
                            {
                                ultraGroupBox3.Dock = DockStyle.Left;
                                ultraGroupBox3.Width = pnMBTellerPaper_SecCK.Width - grHoaDon2.Width;
                                grHoaDon2.Visible = true;
                                grHoaDon2.Dock = DockStyle.Right;
                            }
                            else
                            {
                                grHoaDon2.Visible = false;
                                ultraGroupBox3.Dock = DockStyle.Fill;
                            }
                        else
                        {
                            grHoaDon2.Visible = false;
                            ultraGroupBox3.Dock = DockStyle.Fill;
                        }
                        this.ConfigTopVouchersNo<PPService>(palTopVouchers, "No", "PostedDate", "Date", 13, objectDataSource: _select, resetNo: status);
                    }
                    #endregion
                    break;
                case 173: //mua hàng thanh toán ngay TTD
                    #region mua hàng thanh toán ngay TTD
                    {
                        //optThanhToan.CheckedIndex = 1;  //tự động chọn "thanh toán ngay"
                        cbbChonThanhToan.Enabled = true;    //hiện cbb cho chọn thanh toán ngay bằng "tiền mặt" hay "chuyển khoản"
                        pnNoPayment.Visible = false;
                        paneTTChungNew.Visible = false;
                        pnMBTellerPaper_PayOrder.Visible = false;
                        pnMBTellerPaper_SecCK.Visible = false;
                        pnMBTellerPaper_Sec.Visible = false;
                        pnMBCreditCard.Visible = true;
                        palThongtinchung.Height = pnMBCreditCard.Height;
                        if (ultraTab1 != null)
                            if (ultraTab1.SelectedTab.Key == "uTab1")
                            {
                                ultraGroupBox5.Dock = DockStyle.Left;
                                ultraGroupBox5.Width = pnMBCreditCard.Width - grHoaDon3.Width;
                                grHoaDon3.Visible = true;
                                grHoaDon3.Dock = DockStyle.Right;
                            }
                            else
                            {
                                grHoaDon3.Visible = false;
                                ultraGroupBox5.Dock = DockStyle.Fill;
                            }
                        else
                        {
                            grHoaDon3.Visible = false;
                            ultraGroupBox5.Dock = DockStyle.Fill;
                        }
                        this.ConfigTopVouchersNo<PPService>(palTopVouchers, "No", "PostedDate", "Date", ConstFrm.TypeGroup_MBCreditCard, objectDataSource: _select, resetNo: status);
                    }
                    #endregion
                    break;
                case 143: //mua hàng thanh toán ngay STM
                    #region mua hàng thanh toán ngay STM
                    {
                        //optThanhToan.CheckedIndex = 1;  //tự động chọn "thanh toán ngay"
                        cbbChonThanhToan.Enabled = true;    //hiện cbb cho chọn thanh toán ngay bằng "tiền mặt" hay "chuyển khoản"
                        pnNoPayment.Visible = false;
                        paneTTChungNew.Visible = false;
                        pnMBTellerPaper_PayOrder.Visible = false;
                        pnMBTellerPaper_SecCK.Visible = false;
                        pnMBTellerPaper_Sec.Visible = true;
                        pnMBCreditCard.Visible = false;
                        palThongtinchung.Height = pnMBTellerPaper_Sec.Height;
                        if (ultraTab1 != null)
                            if (ultraTab1.SelectedTab.Key == "uTab1")
                            {
                                ultraGroupBox7.Dock = DockStyle.Left;
                                ultraGroupBox7.Width = pnMBTellerPaper_Sec.Width - grHoaDon4.Width;
                                grHoaDon4.Visible = true;
                                grHoaDon4.Dock = DockStyle.Right;
                            }
                            else
                            {
                                grHoaDon4.Visible = false;
                                ultraGroupBox7.Dock = DockStyle.Fill;
                            }
                        else
                        {
                            grHoaDon4.Visible = false;
                            ultraGroupBox7.Dock = DockStyle.Fill;
                        }
                        this.ConfigTopVouchersNo<PPService>(palTopVouchers, "No", "PostedDate", "Date", 14, objectDataSource: _select, resetNo: status);
                    }
                    #endregion
                    break;
                default:
                    {
                        cbbChonThanhToan.Enabled = false;
                        pnNoPayment.Visible = true;
                        paneTTChungNew.Visible = false;
                        pnMBTellerPaper_PayOrder.Visible = false;
                        pnMBTellerPaper_SecCK.Visible = false;
                        pnMBTellerPaper_Sec.Visible = false;
                        pnMBCreditCard.Visible = false;
                        palThongtinchung.Height = pnNoPayment.Height;
                        this.ConfigTopVouchersNo<PPService>(palTopVouchers, "No", "PostedDate", "Date", resetNo: status);
                    }
                    break;
            }
            if (!status && !_isFirst && _select.TypeID == _backSelect.TypeID)
            {
                _select.No = _backSelect.No.CloneObject();
                var topvoucher = palTopVouchers.GetTopVouchers<PPService>();
                if (topvoucher != null)
                {
                    topvoucher.No = _select.No;
                }
            }
        }
        private void ConfigGroup(bool status = true)
        {
            ConfigByType(_select, status);
            if (TypeGroup == null) return;
            this.ReconfigAccountColumn<PPService>(_select.TypeID);
            if (_select.TemplateID == null) return;
            //this.ReloadTemplate<PPService>((Guid)_select.TemplateID, false, true);
            this.ConfigTemplateEdit<PPService>(utmDetailBaseToolBar, TypeID, _statusForm, _templateID);
            _isFirst = false;
        }
        #endregion

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {

                BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                if (datasource == null)
                    datasource = new BindingList<RefVoucher>();
                var f = new FViewVoucherOriginal(_select.CurrencyID, datasource);
                f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                try
                {
                    f.ShowDialog(this);
                }
                catch (Exception ex)
                {
                    MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                }

            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }
        protected override bool AdditionalFunc()
        {
            //if (lblBillReceived.Visible == true)
            //{
            //    UltraGrid uGridCheck;
            //    uGridCheck = Controls.Find("uGrid1", true).FirstOrDefault() as UltraGrid;
            //    int u = uGridCheck.Rows.Count;
            //    foreach (UltraGridRow row in uGridCheck.Rows)
            //    {
            //        //        UltraGridRow row = uGridCheck.ActiveRow;
            //        if ((row.Cells["InvoiceNo"].Text.IsNullOrEmpty()) || (row.Cells["InvoiceDate"].Text.IsNullOrEmpty()) || (row.Cells["InvoiceSeries"].Text.IsNullOrEmpty()) || (row.Cells["InvoiceTemplate"].Text.IsNullOrEmpty()))
            //        {
            //            MSG.Warning("Chưa nhập đủ thông tin hóa đơn ,vui lòng kiểm tra lại");
            //            return false;
            //        }
            //    }
            //}
            //return true;
            if (pnNoPayment.Visible == true)
            {
                if (txtKyHieuHD.Text != "" || txtMauSoHD.Text != "" || txtSoHD.Text != "" || dteNgayHD.Value != null)
                {
                    if (txtKyHieuHD.Text != "" && txtMauSoHD.Text != "" && txtSoHD.Text != "" && dteNgayHD.Value != null)
                    {
                        if (txtSoHD.Text.Length == 7)
                        {
                            var list = (BindingList<PPServiceDetail>)_listObjectInput.FirstOrDefault();

                        foreach (var item in list)
                        {
                            item.InvoiceTemplate = txtMauSoHD.Text;
                            item.InvoiceSeries = txtKyHieuHD.Text;
                            item.InvoiceNo = txtSoHD.Text;
                            item.InvoiceDate = Convert.ToDateTime(dteNgayHD.Value.ToString());
                        }
                        _select.BillReceived = true;
                        }
                        else
                        {
                            MSG.Warning("Số hoá đơn phải là kiểu số và đúng 7 chữ số.");
                            return false;
                        }
                    }
                    else
                    {
                        MSG.Warning("Chưa nhập đủ thông tin hóa đơn ,vui lòng kiểm tra lại");
                        return false;
                    }
                }
                else
                {
                    var list = (BindingList<PPServiceDetail>)_listObjectInput.FirstOrDefault();
                    foreach (var item in list)
                    {
                        item.InvoiceTemplate = "";
                        item.InvoiceSeries = "";
                        item.InvoiceNo = "";
                        item.InvoiceDate = null;
                    }
                    _select.BillReceived = false;
                }
            }
            if (paneTTChungNew.Visible == true)
            {
                if (txtKyHieuHDNew.Text != "" || txtMauSoHDNew.Text != "" || txtSoHDNew.Text != "" || dteNgayHDNew.Value != null)
                {
                    if (txtKyHieuHDNew.Text != "" && txtMauSoHDNew.Text != "" && txtSoHDNew.Text != "" && dteNgayHDNew.Value != null)
                    {
                        var list = (BindingList<PPServiceDetail>)_listObjectInput.FirstOrDefault();

                        foreach (var item in list)
                        {
                            item.InvoiceTemplate = txtMauSoHDNew.Text;
                            item.InvoiceSeries = txtKyHieuHDNew.Text;
                            item.InvoiceNo = txtSoHDNew.Text;
                            item.InvoiceDate = Convert.ToDateTime(dteNgayHDNew.Value.ToString());
                        }
                        _select.BillReceived = true;
                    }
                    else
                    {
                        MSG.Warning("Chưa nhập đủ thông tin hóa đơn ,vui lòng kiểm tra lại");
                        return false;
                    }
                }
                else
                {
                    var list = (BindingList<PPServiceDetail>)_listObjectInput.FirstOrDefault();
                    foreach (var item in list)
                    {
                        item.InvoiceTemplate = "";
                        item.InvoiceSeries = "";
                        item.InvoiceNo = "";
                        item.InvoiceDate = null;
                    }
                    _select.BillReceived = false;
                }
            }
            if (pnMBTellerPaper_PayOrder.Visible == true)
            {
                if (txtKyHieuHD1.Text != "" || txtMauSoHD1.Text != "" || txtSoHD1.Text != "" || dteNgayHD1.Value != null)
                {
                    if (txtKyHieuHD1.Text != "" && txtMauSoHD1.Text != "" && txtSoHD1.Text != "" && dteNgayHD1.Value != null)
                    {
                        var list = (BindingList<PPServiceDetail>)_listObjectInput.FirstOrDefault();

                        foreach (var item in list)
                        {
                            item.InvoiceTemplate = txtMauSoHD1.Text;
                            item.InvoiceSeries = txtKyHieuHD1.Text;
                            item.InvoiceNo = txtSoHD1.Text;
                            item.InvoiceDate = Convert.ToDateTime(dteNgayHD1.Value.ToString());
                        }
                        _select.BillReceived = true;
                    }
                    else
                    {
                        MSG.Warning("Chưa nhập đủ thông tin hóa đơn ,vui lòng kiểm tra lại");
                        return false;
                    }
                }
                else
                {
                    var list = (BindingList<PPServiceDetail>)_listObjectInput.FirstOrDefault();
                    foreach (var item in list)
                    {
                        item.InvoiceTemplate = "";
                        item.InvoiceSeries = "";
                        item.InvoiceNo = "";
                        item.InvoiceDate = null;
                    }
                    _select.BillReceived = false;
                }
            }
            if (pnMBTellerPaper_SecCK.Visible == true)
            {
                if (txtKyHieuHD2.Text != "" || txtMauSoHD2.Text != "" || txtSoHD2.Text != "" || dteNgayHD2.Value != null)
                {
                    if (txtKyHieuHD2.Text != "" && txtMauSoHD2.Text != "" && txtSoHD2.Text != "" && dteNgayHD2.Value != null)
                    {
                        var list = (BindingList<PPServiceDetail>)_listObjectInput.FirstOrDefault();

                        foreach (var item in list)
                        {
                            item.InvoiceTemplate = txtMauSoHD2.Text;
                            item.InvoiceSeries = txtKyHieuHD2.Text;
                            item.InvoiceNo = txtSoHD2.Text;
                            item.InvoiceDate = Convert.ToDateTime(dteNgayHD2.Value.ToString());
                        }
                        _select.BillReceived = true;
                    }
                    else
                    {
                        MSG.Warning("Chưa nhập đủ thông tin hóa đơn ,vui lòng kiểm tra lại");
                        return false;
                    }
                }
                else
                {
                    var list = (BindingList<PPServiceDetail>)_listObjectInput.FirstOrDefault();
                    foreach (var item in list)
                    {
                        item.InvoiceTemplate = "";
                        item.InvoiceSeries = "";
                        item.InvoiceNo = "";
                        item.InvoiceDate = null;
                    }
                    _select.BillReceived = false;
                }
            }
            if (pnMBCreditCard.Visible == true)
            {
                if (txtKyHieuHD3.Text != "" || txtMauSoHD3.Text != "" || txtSoHD3.Text != "" || dteNgayHD3.Value != null)
                {
                    if (txtKyHieuHD3.Text != "" && txtMauSoHD3.Text != "" && txtSoHD3.Text != "" && dteNgayHD3.Value != null)
                    {
                        var list = (BindingList<PPServiceDetail>)_listObjectInput.FirstOrDefault();

                        foreach (var item in list)
                        {
                            item.InvoiceTemplate = txtMauSoHD3.Text;
                            item.InvoiceSeries = txtKyHieuHD3.Text;
                            item.InvoiceNo = txtSoHD3.Text;
                            item.InvoiceDate = Convert.ToDateTime(dteNgayHD3.Value.ToString());
                        }
                        _select.BillReceived = true;
                    }
                    else
                    {
                        MSG.Warning("Chưa nhập đủ thông tin hóa đơn ,vui lòng kiểm tra lại");
                        return false;
                    }
                }
                else
                {
                    var list = (BindingList<PPServiceDetail>)_listObjectInput.FirstOrDefault();
                    foreach (var item in list)
                    {
                        item.InvoiceTemplate = "";
                        item.InvoiceSeries = "";
                        item.InvoiceNo = "";
                        item.InvoiceDate = null;
                    }
                    _select.BillReceived = false;
                }
            }
            if (pnMBTellerPaper_Sec.Visible == true)
            {
                if (txtKyHieuHD4.Text != "" || txtMauSoHD4.Text != "" || txtSoHD4.Text != "" || dteNgayHD4.Value != null)
                {
                    if (txtKyHieuHD4.Text != "" && txtMauSoHD4.Text != "" && txtSoHD4.Text != "" && dteNgayHD4.Value != null)
                    {
                        var list = (BindingList<PPServiceDetail>)_listObjectInput.FirstOrDefault();

                        foreach (var item in list)
                        {
                            item.InvoiceTemplate = txtMauSoHD4.Text;
                            item.InvoiceSeries = txtKyHieuHD4.Text;
                            item.InvoiceNo = txtSoHD4.Text;
                            item.InvoiceDate = Convert.ToDateTime(dteNgayHD4.Value.ToString());
                        }
                        _select.BillReceived = true;
                    }
                    else
                    {
                        MSG.Warning("Chưa nhập đủ thông tin hóa đơn ,vui lòng kiểm tra lại");
                        return false;
                    }
                }
                else
                {
                    var list = (BindingList<PPServiceDetail>)_listObjectInput.FirstOrDefault();
                    foreach (var item in list)
                    {
                        item.InvoiceTemplate = "";
                        item.InvoiceSeries = "";
                        item.InvoiceNo = "";
                        item.InvoiceDate = null;
                    }
                    _select.BillReceived = false;
                }
            }

            return true;
        }
        private void chkBillReceived_CheckStateChanged(object sender, EventArgs e)
        {
            //lblBillReceived.Visible = chkBillReceived.CheckState == CheckState.Checked;
            lblBillReceived.Visible = false;
            if (_statusForm != ConstFrm.optStatusForm.View)
            {
                //if (chkBillReceived.CheckState == CheckState.Checked)
                //{
                //    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].CellActivation = Activation.AllowEdit;
                //    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].CellActivation = Activation.AllowEdit;
                //    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellActivation = Activation.AllowEdit;
                //    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].CellActivation = Activation.AllowEdit;
                //}
                //else
                //{
                //    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].CellActivation = Activation.Disabled;
                //    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].CellActivation = Activation.Disabled;
                //    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellActivation = Activation.Disabled;
                //    _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].CellActivation = Activation.Disabled;
                //    foreach (var x in _select.PPServiceDetails)
                //    {
                //        x.InvoiceSeries = null;
                //        x.InvoiceTemplate = null;
                //        x.InvoiceNo = null;
                //        x.InvoiceDate = null;
                //    }
                //}
            }
        }

        private void FPPServiceDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        private void txtAccreditativeAccountingObjectName_TextChanged(object sender, EventArgs e)
        {
            UltraTextEditor txt = (UltraTextEditor)sender;
            txt.Text = txt.Text.Replace("\r\n", "");
            if (txt.Text == "")
                txt.Multiline = true;
            else
                txt.Multiline = false;
        }

        private void txtPaymentOrderBankName_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtMauSoHD_TextChanged(object sender, EventArgs e)
        {
            UltraTextEditor txt = (UltraTextEditor)sender;
            txtMauSoHD.Text = txt.Text;
            txtMauSoHDNew.Text = txt.Text;
            txtMauSoHD1.Text = txt.Text;
            txtMauSoHD2.Text = txt.Text;
            txtMauSoHD3.Text = txt.Text;
            txtMauSoHD4.Text = txt.Text;
        }

        private void txtKyHieuHD_TextChanged(object sender, EventArgs e)
        {
            UltraTextEditor txt = (UltraTextEditor)sender;
            txtKyHieuHD.Text = txt.Text;
            txtKyHieuHDNew.Text = txt.Text;
            txtKyHieuHD1.Text = txt.Text;
            txtKyHieuHD2.Text = txt.Text;
            txtKyHieuHD3.Text = txt.Text;
            txtKyHieuHD4.Text = txt.Text;
        }

        private void txtSoHD_TextChanged(object sender, EventArgs e)
        {
            UltraTextEditor txt = (UltraTextEditor)sender;
            txtSoHD.Text = txt.Text;
            txtSoHDNew.Text = txt.Text;
            txtSoHD1.Text = txt.Text;
            txtSoHD2.Text = txt.Text;
            txtSoHD3.Text = txt.Text;
            txtSoHD4.Text = txt.Text;
        }

        private void dteNgayHD_ValueChanged(object sender, EventArgs e)
        {
            UltraDateTimeEditor dte = (UltraDateTimeEditor)sender;
            dteNgayHD.Value = dte.Value;
            dteNgayHDNew.Value = dte.Value;
            dteNgayHD1.Value = dte.Value;
            dteNgayHD2.Value = dte.Value;
            dteNgayHD3.Value = dte.Value;
            dteNgayHD4.Value = dte.Value;
        }
    }

    public class FPPServiceDetailTmp : DetailBase<PPService>
    { }

}