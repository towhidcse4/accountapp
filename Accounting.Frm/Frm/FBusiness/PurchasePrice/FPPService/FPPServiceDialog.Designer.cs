﻿namespace Accounting
{
    partial class FPPServiceDialog
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.uButtonViewPPService = new Infragistics.Win.Misc.UltraButton();
            this.uButtonAccept = new Infragistics.Win.Misc.UltraButton();
            this.uButtonCancel = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.cms4Grid.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // uGrid
            // 
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 0);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(761, 414);
            this.uGrid.TabIndex = 1;
            this.uGrid.Text = "uGrid";
            this.uGrid.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_CellChange);
            this.uGrid.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid_DoubleClickRow);
            this.uGrid.MouseDown += new System.Windows.Forms.MouseEventHandler(this.UGridMouseDown);
            // 
            // cms4Grid
            // 
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tsmAdd,
            this.tsmEdit,
            this.tsmDelete,
            this.tmsReLoad});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(68, 98);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(64, 6);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.Size = new System.Drawing.Size(67, 22);
            // 
            // tsmEdit
            // 
            this.tsmEdit.Name = "tsmEdit";
            this.tsmEdit.Size = new System.Drawing.Size(67, 22);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.Size = new System.Drawing.Size(67, 22);
            // 
            // tmsReLoad
            // 
            this.tmsReLoad.Name = "tmsReLoad";
            this.tmsReLoad.Size = new System.Drawing.Size(67, 22);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.uGrid);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(761, 414);
            this.panel3.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.uButtonCancel);
            this.panel1.Controls.Add(this.uButtonAccept);
            this.panel1.Controls.Add(this.uButtonViewPPService);
            this.panel1.Location = new System.Drawing.Point(0, 420);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(761, 47);
            this.panel1.TabIndex = 3;
            // 
            // uButtonViewPPService
            // 
            this.uButtonViewPPService.Location = new System.Drawing.Point(447, 21);
            this.uButtonViewPPService.Name = "uButtonViewPPService";
            this.uButtonViewPPService.Size = new System.Drawing.Size(146, 23);
            this.uButtonViewPPService.TabIndex = 0;
            this.uButtonViewPPService.Text = "Xem đơn mua hàng";
            this.uButtonViewPPService.Click += new System.EventHandler(this.uButtonViewPPService_Click);
            // 
            // uButtonAccept
            // 
            this.uButtonAccept.Location = new System.Drawing.Point(599, 21);
            this.uButtonAccept.Name = "uButtonAccept";
            this.uButtonAccept.Size = new System.Drawing.Size(75, 23);
            this.uButtonAccept.TabIndex = 1;
            this.uButtonAccept.Text = "Đồng ý";
            this.uButtonAccept.Click += new System.EventHandler(this.uButtonAccept_Click);
            // 
            // uButtonCancel
            // 
            this.uButtonCancel.Location = new System.Drawing.Point(680, 21);
            this.uButtonCancel.Name = "uButtonCancel";
            this.uButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.uButtonCancel.TabIndex = 1;
            this.uButtonCancel.Text = "Hủy bỏ";
            this.uButtonCancel.Click += new System.EventHandler(this.uButtonCancel_Click);
            // 
            // FPPServiceDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 547);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Name = "FPPServiceDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.cms4Grid.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmEdit;
        private System.Windows.Forms.ToolStripMenuItem tmsReLoad;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.Misc.UltraButton uButtonCancel;
        private Infragistics.Win.Misc.UltraButton uButtonAccept;
        private Infragistics.Win.Misc.UltraButton uButtonViewPPService;
    }
}
