﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FPPService : CustormForm //UserControl
    {
        #region khai báo
        private readonly IPPServiceService _iorderService;
        private readonly IPPServiceDetailService _iorderDetailService;
        private readonly IAccountingObjectService _iAccountingObjectService;
        private readonly IBankAccountDetailService _iBankAccountDetailService;
        private readonly ICurrencyService _iCurrencyService;
        private readonly IPaymentClauseService _iPaymentClauseService;
        private readonly ITransportMethodService _iTransportMethodService;

        List<PPService> _dsPPService = new List<PPService>();
        private List<Core.Domain.Type> _dsType = new List<Core.Domain.Type>();

        // Chứa danh sách các template dựa theo TypeID (KEY type of string = TypeID@Guid, VALUES = Template của TypeID và TemplateID ứng với chứng từ đó)
        // - khi một chứng từ được load, nó kiểm tra Template nó cần có trong dsTemplate chưa, chưa có thì load trong csdl có rồi thì lấy luôn trong dsTemplate để tránh truy vấn nhiều
        // - dsTemplate sẽ được cập nhật lại khi có sự thay đổi giao diện
        //Note: nó có tác dụng tăng tốc độ của chương trình, tuy nhiên chú ý nó là hàm Static nên tồn tại ngay cả khi đóng form, chỉ mất đi khi đóng toàn bộ chương trình do đó cần chú ý set nó bằng null hoặc bằng new Dictionary<string, Template>() hoặc clear item trong dic cho giải phóng bộ nhớ
        public static Dictionary<string, Template> DsTemplate = new Dictionary<string, Template>();
        #endregion

        #region khởi tạo
        public FPPService()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _iorderService = IoC.Resolve<IPPServiceService>();
            _iorderDetailService = IoC.Resolve<IPPServiceDetailService>();
            _iAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            _iBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            _iCurrencyService = IoC.Resolve<ICurrencyService>();
            _iPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            _iTransportMethodService = IoC.Resolve<ITransportMethodService>();

            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu            

            LoadDuLieu();

            //chọn tự select dòng đầu tiên
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            #endregion
        }

        private void LoadDuLieu(bool configGrid = true)
        {
            #region Lấy dữ liệu từ CSDL
            _dsPPService = _iorderService.GetAll();
            #endregion

            #region Xử lý dữ liệu
            foreach (PPService order in _dsPPService)
                order.Type = _dsType.SingleOrDefault(k => k.ID == order.TypeID);
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = _dsPPService.ToArray();

            if (configGrid) ConfigGrid(uGrid);
            #endregion
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void TsmAddClick(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void TsmEditClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void TsmDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void TmsReLoadClick(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void BtnAddClick(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void BtnEditClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void BtnDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void UGridMouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void UGridDoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunction();
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        void AddFunction()
        {
            PPService temp = uGrid.Selected.Rows.Count <= 0 ? new PPService() : uGrid.Selected.Rows[0].ListObject as PPService;
            new FPPServiceDetail(temp, _dsPPService, ConstFrm.optStatusForm.Add).ShowDialog(this);
            if (!FPPServiceDetail.IsClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                PPService temp = uGrid.Selected.Rows[0].ListObject as PPService;
                new FPPServiceDetail(temp, _dsPPService, ConstFrm.optStatusForm.View).ShowDialog(this);
                if (!FPPServiceDetail.IsClose) LoadDuLieu();
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                PPService temp = uGrid.Selected.Rows[0].ListObject as PPService;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, temp.No)) == DialogResult.Yes)
                {
                    _iorderService.Delete(temp);
                    _iorderService.CommitChanges();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion

        #region Utils
        void ConfigGrid(UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, ConstDatabase.PPService_TableName);            
            utralGrid.DisplayLayout.Bands[0].Columns["Date"].Format = "dd/MM/yyyy";
        }
        #endregion

        private void uGrid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGrid.Selected.Rows.Count < 1) return;
            object listObject = uGrid.Selected.Rows[0].ListObject;
            if (listObject == null) return;
            PPService order = listObject as PPService;
            if (order == null) return;

            //Tab thông tin chung
            lblDate.Text = order.Date.ToString("dd/MM/yyyy");
            lblNo.Text = order.No;
            lblBranch.Text = "";
            
            lblAccountingObjectName.Text = order.AccountingObjectName;
            lblAccountingObjectAddress.Text = order.AccountingObjectAddress;            
            lblReason.Text = order.Reason;
            lblTotalAllOriginal.Text = string.Format(order.TotalAllOriginal == 0 ? "0" : "{0:0,0}", order.TotalAllOriginal);  //order.TotalAmount.ToString("{0:0,0}");            

            //Tab hoạch toán
            try
            { int test = order.PPServiceDetails.Count; }
            catch (Exception)
            { order.PPServiceDetails = new List<PPServiceDetail>(); }

            if (order.PPServiceDetails.Count == 0)
            {
                IList<PPServiceDetail> lPPServiceDetail = _iorderDetailService.GetPPServiceDetailbyID(order.ID);
                order.PPServiceDetails = (List<PPServiceDetail>)Utils.CloneObject(lPPServiceDetail);
                uGridPosted.DataSource = order.PPServiceDetails;
            }
            else uGridPosted.DataSource = Utils.CloneObject(order.PPServiceDetails);

            #region Config Grid
            //hiển thị 1 band
            uGridPosted.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            //tắt lọc cột
            uGridPosted.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGridPosted.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGridPosted.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGridPosted.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;

            //Hiện những dòng trống?
            uGridPosted.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridPosted.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            //Fix Header
            uGridPosted.DisplayLayout.UseFixedHeaders = true;

            UltraGridBand band = uGridPosted.DisplayLayout.Bands[0];
            band.Summaries.Clear();

            string keyofdsTemplate = string.Format("{0}@{1}", order.TypeID, order.TemplateID);
            Template mauGiaoDien;
            if (DsTemplate.Keys.Contains(keyofdsTemplate)) mauGiaoDien = DsTemplate[keyofdsTemplate];
            else
            {
                //mauGiaoDien = Utils.GetTemplateUIfromDatabase(order.TypeID, false, order.TemplateID);
                //DsTemplate.Add(keyofdsTemplate, mauGiaoDien);
            }
            //Utils.ConfigGrid(uGridPosted, ConstDatabase.PPServiceDetail_TableName, mauGiaoDien == null ? new List<TemplateColumn>() : mauGiaoDien.TemplateDetails[0].TemplateColumns, 0);
            //Thêm tổng số ở cột "Số tiền"
            SummarySettings summary = band.Summaries.Add("SumAmountOriginal", SummaryType.Sum, band.Columns["AmountOriginal"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            #endregion
        }
    }
}
