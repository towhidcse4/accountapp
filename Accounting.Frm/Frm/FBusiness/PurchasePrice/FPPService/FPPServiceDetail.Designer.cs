﻿namespace Accounting
{
    partial class FPPServiceDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance116 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance123 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance127 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance129 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance130 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance131 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance132 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance133 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance134 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance135 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance136 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance137 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance138 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance139 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance140 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance141 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance142 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance143 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance144 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance145 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance146 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance147 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance148 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance149 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance150 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance151 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance152 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance153 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance154 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance155 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance156 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance157 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance158 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance159 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance160 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance161 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance162 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance163 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance164 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance165 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance166 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance167 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance168 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance169 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance170 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance171 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance172 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance173 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance174 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance175 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance176 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance177 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance178 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance179 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance180 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance181 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance182 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance183 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance184 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance185 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance190 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance186 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance187 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance188 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance189 = new Infragistics.Win.Appearance();
            this.palBottom = new System.Windows.Forms.Panel();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.lblBillReceived = new Infragistics.Win.Misc.UltraLabel();
            this.chkBillReceived = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkIsFeightService = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.lblTotalVATAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.lbl1 = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAll = new Infragistics.Win.Misc.UltraLabel();
            this.lbl2 = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAllOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.lbl3 = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalVATAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lbl0 = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalDiscountAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalDiscountAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridControl = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.palChung = new System.Windows.Forms.Panel();
            this.optThanhToan = new Accounting.UltraOptionSet_Ex();
            this.cbbChonThanhToan = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.palTopVouchers = new Infragistics.Win.Misc.UltraPanel();
            this.ultraToolTipManager1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            this.palTop = new Infragistics.Win.Misc.UltraPanel();
            this.palFill = new Infragistics.Win.Misc.UltraPanel();
            this.palGrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.palThongtinchung = new System.Windows.Forms.Panel();
            this.paneTTChungNew = new Infragistics.Win.Misc.UltraPanel();
            this.grHoaDonNew = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtMauSoHDNew = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSoHDNew = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtKyHieuHDNew = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.dteNgayHDNew = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.gpMCPayment = new Infragistics.Win.Misc.UltraGroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtAccreditativeAttach = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtPaymentReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtReceiver = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtPaymentAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbPaymentAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtPaymentAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.D = new System.Windows.Forms.Label();
            this.pnMBTellerPaper_Sec = new System.Windows.Forms.Panel();
            this.grHoaDon4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtMauSoHD4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSoHD4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtKyHieuHD4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.dteNgayHD4 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraGroupBox7 = new Infragistics.Win.Misc.UltraGroupBox();
            this.dtSecIssueDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtSecIdentificationNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSecIssueBy = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.label20 = new System.Windows.Forms.Label();
            this.txtSecAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbSecAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtSecReceiver = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.ultraGroupBox8 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtSecReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSecBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbSecBankAccountDetailID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.pnMBCreditCard = new System.Windows.Forms.Panel();
            this.grHoaDon3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtMauSoHD3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSoHD3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtKyHieuHD3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.dteNgayHD3 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtCreditCardAccountingObjectBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbCreditCardAccountingObjectBankAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label27 = new System.Windows.Forms.Label();
            this.txtCreditCardAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbCreditCardAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtCreditCardAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.ultraGroupBox6 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtCreditCardType = new Infragistics.Win.Misc.UltraLabel();
            this.picCreditCardType = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.txtOwnerCard = new System.Windows.Forms.Label();
            this.lblOwnerCard = new Infragistics.Win.Misc.UltraLabel();
            this.cbbCreditCardNumber = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtCreditCardReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.pnMBTellerPaper_SecCK = new System.Windows.Forms.Panel();
            this.grHoaDon2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtMauSoHD2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSoHD2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtKyHieuHD2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.dteNgayHD2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtSecCKAccountingObjectBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbSecCKAccountingObjectBankAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label14 = new System.Windows.Forms.Label();
            this.txtSecCKAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbSecCKAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtSecCKAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtSecCKReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSecCKBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbSecCKBankAccountDetailID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.pnMBTellerPaper_PayOrder = new System.Windows.Forms.Panel();
            this.grHoaDon1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtMauSoHD1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSoHD1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtKyHieuHD1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.dteNgayHD1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.gpAccountingObject = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtAccreditativeAccountingObjectBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccreditativeAccountingObjectBankAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label13 = new System.Windows.Forms.Label();
            this.txtAccreditativeAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccreditativeAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccreditativeAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.label17 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.gpPaymentOrder = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtPaymentOrderReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtPaymentOrderBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbPaymentOrderBankAccountDetailID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pnNoPayment = new Infragistics.Win.Misc.UltraPanel();
            this.grHoaDon = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtMauSoHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSoHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtKyHieuHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel37 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel38 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel39 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel40 = new Infragistics.Win.Misc.UltraLabel();
            this.dteNgayHD = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ugbThongTinChung = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDescription = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectAddress = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObjectID = new Infragistics.Win.Misc.UltraLabel();
            this.palBottom.SuspendLayout();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBillReceived)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsFeightService)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.palChung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optThanhToan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChonThanhToan)).BeginInit();
            this.palTopVouchers.SuspendLayout();
            this.palTop.ClientArea.SuspendLayout();
            this.palTop.SuspendLayout();
            this.palFill.ClientArea.SuspendLayout();
            this.palFill.SuspendLayout();
            this.palGrid.ClientArea.SuspendLayout();
            this.palGrid.SuspendLayout();
            this.palThongtinchung.SuspendLayout();
            this.paneTTChungNew.ClientArea.SuspendLayout();
            this.paneTTChungNew.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDonNew)).BeginInit();
            this.grHoaDonNew.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHDNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHDNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHDNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHDNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpMCPayment)).BeginInit();
            this.gpMCPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccreditativeAttach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPaymentAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentAccountingObjectAddress)).BeginInit();
            this.pnMBTellerPaper_Sec.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDon4)).BeginInit();
            this.grHoaDon4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHD4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).BeginInit();
            this.ultraGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtSecIssueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecIdentificationNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecIssueBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecReceiver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox8)).BeginInit();
            this.ultraGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecBankAccountDetailID)).BeginInit();
            this.pnMBCreditCard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDon3)).BeginInit();
            this.grHoaDon3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHD3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.ultraGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditCardAccountingObjectBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardAccountingObjectBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditCardAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditCardAccountingObjectAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).BeginInit();
            this.ultraGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditCardReason)).BeginInit();
            this.pnMBTellerPaper_SecCK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDon2)).BeginInit();
            this.grHoaDon2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHD2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKAccountingObjectBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecCKAccountingObjectBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecCKAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKAccountingObjectAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecCKBankAccountDetailID)).BeginInit();
            this.pnMBTellerPaper_PayOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDon1)).BeginInit();
            this.grHoaDon1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHD1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpAccountingObject)).BeginInit();
            this.gpAccountingObject.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccreditativeAccountingObjectBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccreditativeAccountingObjectBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccreditativeAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccreditativeAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccreditativeAccountingObjectAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpPaymentOrder)).BeginInit();
            this.gpPaymentOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentOrderReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentOrderBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPaymentOrderBankAccountDetailID)).BeginInit();
            this.pnNoPayment.ClientArea.SuspendLayout();
            this.pnNoPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDon)).BeginInit();
            this.grHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugbThongTinChung)).BeginInit();
            this.ugbThongTinChung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).BeginInit();
            this.SuspendLayout();
            // 
            // palBottom
            // 
            this.palBottom.Controls.Add(this.ultraPanel2);
            this.palBottom.Controls.Add(this.ultraPanel1);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 898);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(945, 163);
            this.palBottom.TabIndex = 27;
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.lblBillReceived);
            this.ultraPanel2.ClientArea.Controls.Add(this.chkBillReceived);
            this.ultraPanel2.ClientArea.Controls.Add(this.chkIsFeightService);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalVATAmountOriginal);
            this.ultraPanel2.ClientArea.Controls.Add(this.lbl1);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalAll);
            this.ultraPanel2.ClientArea.Controls.Add(this.lbl2);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalAllOriginal);
            this.ultraPanel2.ClientArea.Controls.Add(this.lbl3);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalVATAmount);
            this.ultraPanel2.ClientArea.Controls.Add(this.lbl0);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalAmountOriginal);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalDiscountAmount);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalAmount);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalDiscountAmountOriginal);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(945, 108);
            this.ultraPanel2.TabIndex = 92;
            // 
            // lblBillReceived
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance1.BorderColor = System.Drawing.Color.DarkRed;
            appearance1.BorderColor2 = System.Drawing.Color.DarkRed;
            appearance1.BorderColor3DBase = System.Drawing.Color.DarkRed;
            appearance1.FontData.BoldAsString = "True";
            appearance1.ForeColor = System.Drawing.Color.DarkRed;
            appearance1.ForeColorDisabled = System.Drawing.Color.IndianRed;
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.lblBillReceived.Appearance = appearance1;
            this.lblBillReceived.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.TwoColor;
            this.lblBillReceived.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.TwoColor;
            this.lblBillReceived.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBillReceived.Location = new System.Drawing.Point(11, 64);
            this.lblBillReceived.Name = "lblBillReceived";
            this.lblBillReceived.Size = new System.Drawing.Size(163, 38);
            this.lblBillReceived.TabIndex = 93;
            this.lblBillReceived.Text = "ĐÃ NHẬN HÓA ĐƠN";
            this.lblBillReceived.Visible = false;
            // 
            // chkBillReceived
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.BackColor2 = System.Drawing.Color.Transparent;
            appearance2.ImageAlpha = Infragistics.Win.Alpha.Transparent;
            this.chkBillReceived.Appearance = appearance2;
            this.chkBillReceived.BackColor = System.Drawing.Color.Transparent;
            this.chkBillReceived.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkBillReceived.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkBillReceived.Location = new System.Drawing.Point(11, 30);
            this.chkBillReceived.Name = "chkBillReceived";
            this.chkBillReceived.Size = new System.Drawing.Size(116, 22);
            this.chkBillReceived.TabIndex = 92;
            this.chkBillReceived.Text = "Đã nhận hóa đơn";
            this.chkBillReceived.Visible = false;
            this.chkBillReceived.CheckStateChanged += new System.EventHandler(this.chkBillReceived_CheckStateChanged);
            // 
            // chkIsFeightService
            // 
            this.chkIsFeightService.Location = new System.Drawing.Point(11, 8);
            this.chkIsFeightService.Name = "chkIsFeightService";
            this.chkIsFeightService.Size = new System.Drawing.Size(141, 20);
            this.chkIsFeightService.TabIndex = 91;
            this.chkIsFeightService.Text = "Là chi phí mua hàng";
            // 
            // lblTotalVATAmountOriginal
            // 
            this.lblTotalVATAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance3.TextHAlignAsString = "Right";
            appearance3.TextVAlignAsString = "Middle";
            this.lblTotalVATAmountOriginal.Appearance = appearance3;
            this.lblTotalVATAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalVATAmountOriginal.Location = new System.Drawing.Point(451, 56);
            this.lblTotalVATAmountOriginal.Name = "lblTotalVATAmountOriginal";
            this.lblTotalVATAmountOriginal.Size = new System.Drawing.Size(169, 21);
            this.lblTotalVATAmountOriginal.TabIndex = 87;
            // 
            // lbl1
            // 
            this.lbl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lbl1.Appearance = appearance4;
            this.lbl1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1.Location = new System.Drawing.Point(333, 31);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(116, 21);
            this.lbl1.TabIndex = 72;
            this.lbl1.Text = "Tiền chiết khấu";
            // 
            // lblTotalAll
            // 
            this.lblTotalAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.TextHAlignAsString = "Right";
            appearance5.TextVAlignAsString = "Middle";
            this.lblTotalAll.Appearance = appearance5;
            this.lblTotalAll.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAll.Location = new System.Drawing.Point(626, 81);
            this.lblTotalAll.Name = "lblTotalAll";
            this.lblTotalAll.Size = new System.Drawing.Size(169, 21);
            this.lblTotalAll.TabIndex = 90;
            // 
            // lbl2
            // 
            this.lbl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.lbl2.Appearance = appearance6;
            this.lbl2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl2.Location = new System.Drawing.Point(333, 81);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(116, 21);
            this.lbl2.TabIndex = 74;
            this.lbl2.Text = "Tổng tiền thanh toán";
            // 
            // lblTotalAllOriginal
            // 
            this.lblTotalAllOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.TextHAlignAsString = "Right";
            appearance7.TextVAlignAsString = "Middle";
            this.lblTotalAllOriginal.Appearance = appearance7;
            this.lblTotalAllOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAllOriginal.Location = new System.Drawing.Point(451, 81);
            this.lblTotalAllOriginal.Name = "lblTotalAllOriginal";
            this.lblTotalAllOriginal.Size = new System.Drawing.Size(169, 21);
            this.lblTotalAllOriginal.TabIndex = 89;
            // 
            // lbl3
            // 
            this.lbl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.lbl3.Appearance = appearance8;
            this.lbl3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl3.Location = new System.Drawing.Point(333, 56);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(116, 21);
            this.lbl3.TabIndex = 76;
            this.lbl3.Text = "Tiền thuế GTGT";
            // 
            // lblTotalVATAmount
            // 
            this.lblTotalVATAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.TextHAlignAsString = "Right";
            appearance9.TextVAlignAsString = "Middle";
            this.lblTotalVATAmount.Appearance = appearance9;
            this.lblTotalVATAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalVATAmount.Location = new System.Drawing.Point(626, 56);
            this.lblTotalVATAmount.Name = "lblTotalVATAmount";
            this.lblTotalVATAmount.Size = new System.Drawing.Size(169, 21);
            this.lblTotalVATAmount.TabIndex = 88;
            // 
            // lbl0
            // 
            this.lbl0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextVAlignAsString = "Middle";
            this.lbl0.Appearance = appearance10;
            this.lbl0.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl0.Location = new System.Drawing.Point(333, 7);
            this.lbl0.Name = "lbl0";
            this.lbl0.Size = new System.Drawing.Size(116, 21);
            this.lbl0.TabIndex = 82;
            this.lbl0.Text = "Tổng tiền dịch vụ";
            // 
            // lblTotalAmountOriginal
            // 
            this.lblTotalAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance11.TextHAlignAsString = "Right";
            appearance11.TextVAlignAsString = "Middle";
            this.lblTotalAmountOriginal.Appearance = appearance11;
            this.lblTotalAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmountOriginal.Location = new System.Drawing.Point(451, 6);
            this.lblTotalAmountOriginal.Name = "lblTotalAmountOriginal";
            this.lblTotalAmountOriginal.Size = new System.Drawing.Size(169, 21);
            this.lblTotalAmountOriginal.TabIndex = 83;
            // 
            // lblTotalDiscountAmount
            // 
            this.lblTotalDiscountAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance12.TextHAlignAsString = "Right";
            appearance12.TextVAlignAsString = "Middle";
            this.lblTotalDiscountAmount.Appearance = appearance12;
            this.lblTotalDiscountAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalDiscountAmount.Location = new System.Drawing.Point(626, 31);
            this.lblTotalDiscountAmount.Name = "lblTotalDiscountAmount";
            this.lblTotalDiscountAmount.Size = new System.Drawing.Size(169, 21);
            this.lblTotalDiscountAmount.TabIndex = 86;
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance13.TextHAlignAsString = "Right";
            appearance13.TextVAlignAsString = "Middle";
            this.lblTotalAmount.Appearance = appearance13;
            this.lblTotalAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmount.Location = new System.Drawing.Point(626, 6);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(169, 21);
            this.lblTotalAmount.TabIndex = 84;
            // 
            // lblTotalDiscountAmountOriginal
            // 
            this.lblTotalDiscountAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance14.TextHAlignAsString = "Right";
            appearance14.TextVAlignAsString = "Middle";
            this.lblTotalDiscountAmountOriginal.Appearance = appearance14;
            this.lblTotalDiscountAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalDiscountAmountOriginal.Location = new System.Drawing.Point(451, 31);
            this.lblTotalDiscountAmountOriginal.Name = "lblTotalDiscountAmountOriginal";
            this.lblTotalDiscountAmountOriginal.Size = new System.Drawing.Size(169, 21);
            this.lblTotalDiscountAmountOriginal.TabIndex = 85;
            // 
            // ultraPanel1
            // 
            this.ultraPanel1.AutoSize = true;
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.uGridControl);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 108);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(945, 55);
            this.ultraPanel1.TabIndex = 91;
            // 
            // uGridControl
            // 
            this.uGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridControl.Location = new System.Drawing.Point(453, 3);
            this.uGridControl.Name = "uGridControl";
            this.uGridControl.Size = new System.Drawing.Size(486, 49);
            this.uGridControl.TabIndex = 3;
            this.uGridControl.Text = "ultraGrid1";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.palChung);
            this.ultraGroupBox2.Controls.Add(this.palTopVouchers);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance17.FontData.BoldAsString = "True";
            appearance17.FontData.SizeInPoints = 13F;
            this.ultraGroupBox2.HeaderAppearance = appearance17;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(945, 81);
            this.ultraGroupBox2.TabIndex = 31;
            this.ultraGroupBox2.Text = "MUA DỊCH VỤ";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // palChung
            // 
            this.palChung.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.palChung.BackColor = System.Drawing.Color.Transparent;
            this.palChung.Controls.Add(this.optThanhToan);
            this.palChung.Controls.Add(this.cbbChonThanhToan);
            this.palChung.Location = new System.Drawing.Point(521, 29);
            this.palChung.Name = "palChung";
            this.palChung.Size = new System.Drawing.Size(418, 33);
            this.palChung.TabIndex = 40;
            // 
            // optThanhToan
            // 
            this.optThanhToan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance15.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance15.TextVAlignAsString = "Middle";
            this.optThanhToan.Appearance = appearance15;
            this.optThanhToan.BackColor = System.Drawing.Color.Transparent;
            this.optThanhToan.BackColorInternal = System.Drawing.Color.Transparent;
            this.optThanhToan.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem1.DataValue = false;
            valueListItem1.DisplayText = "Chưa thanh toán";
            valueListItem2.DataValue = true;
            valueListItem2.DisplayText = "Thanh toán ngay";
            this.optThanhToan.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.optThanhToan.Location = new System.Drawing.Point(8, 10);
            this.optThanhToan.Name = "optThanhToan";
            this.optThanhToan.ReadOnly = false;
            this.optThanhToan.Size = new System.Drawing.Size(210, 19);
            this.optThanhToan.TabIndex = 46;
            this.optThanhToan.ValueChanged += new System.EventHandler(this.OptThanhToanValueChanged);
            // 
            // cbbChonThanhToan
            // 
            this.cbbChonThanhToan.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbChonThanhToan.Location = new System.Drawing.Point(262, 6);
            this.cbbChonThanhToan.Name = "cbbChonThanhToan";
            this.cbbChonThanhToan.Size = new System.Drawing.Size(153, 21);
            this.cbbChonThanhToan.TabIndex = 36;
            this.cbbChonThanhToan.ValueChanged += new System.EventHandler(this.CbbChonThanhToanValueChanged);
            // 
            // palTopVouchers
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.BackColor2 = System.Drawing.Color.Transparent;
            this.palTopVouchers.Appearance = appearance16;
            this.palTopVouchers.AutoSize = true;
            this.palTopVouchers.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.palTopVouchers.Location = new System.Drawing.Point(6, 29);
            this.palTopVouchers.Name = "palTopVouchers";
            this.palTopVouchers.Size = new System.Drawing.Size(308, 43);
            this.palTopVouchers.TabIndex = 32;
            // 
            // ultraToolTipManager1
            // 
            this.ultraToolTipManager1.ContainingControl = this;
            // 
            // palTop
            // 
            // 
            // palTop.ClientArea
            // 
            this.palTop.ClientArea.Controls.Add(this.ultraGroupBox2);
            this.palTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTop.Location = new System.Drawing.Point(0, 0);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(945, 81);
            this.palTop.TabIndex = 28;
            // 
            // palFill
            // 
            // 
            // palFill.ClientArea
            // 
            this.palFill.ClientArea.Controls.Add(this.palGrid);
            this.palFill.ClientArea.Controls.Add(this.ultraSplitter1);
            this.palFill.ClientArea.Controls.Add(this.palThongtinchung);
            this.palFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palFill.Location = new System.Drawing.Point(0, 81);
            this.palFill.Name = "palFill";
            this.palFill.Size = new System.Drawing.Size(945, 817);
            this.palFill.TabIndex = 29;
            // 
            // palGrid
            // 
            // 
            // palGrid.ClientArea
            // 
            this.palGrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.palGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palGrid.Location = new System.Drawing.Point(0, 810);
            this.palGrid.Name = "palGrid";
            this.palGrid.Size = new System.Drawing.Size(945, 7);
            this.palGrid.TabIndex = 35;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance18.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance18;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(808, 1);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 6;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 800);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 200;
            this.ultraSplitter1.Size = new System.Drawing.Size(945, 10);
            this.ultraSplitter1.TabIndex = 3;
            // 
            // palThongtinchung
            // 
            this.palThongtinchung.BackColor = System.Drawing.Color.Transparent;
            this.palThongtinchung.Controls.Add(this.paneTTChungNew);
            this.palThongtinchung.Controls.Add(this.pnMBTellerPaper_Sec);
            this.palThongtinchung.Controls.Add(this.pnMBCreditCard);
            this.palThongtinchung.Controls.Add(this.pnMBTellerPaper_SecCK);
            this.palThongtinchung.Controls.Add(this.pnMBTellerPaper_PayOrder);
            this.palThongtinchung.Controls.Add(this.pnNoPayment);
            this.palThongtinchung.Dock = System.Windows.Forms.DockStyle.Top;
            this.palThongtinchung.Location = new System.Drawing.Point(0, 0);
            this.palThongtinchung.Name = "palThongtinchung";
            this.palThongtinchung.Size = new System.Drawing.Size(945, 800);
            this.palThongtinchung.TabIndex = 36;
            // 
            // paneTTChungNew
            // 
            // 
            // paneTTChungNew.ClientArea
            // 
            this.paneTTChungNew.ClientArea.Controls.Add(this.grHoaDonNew);
            this.paneTTChungNew.ClientArea.Controls.Add(this.gpMCPayment);
            this.paneTTChungNew.Dock = System.Windows.Forms.DockStyle.Top;
            this.paneTTChungNew.Location = new System.Drawing.Point(0, 873);
            this.paneTTChungNew.Name = "paneTTChungNew";
            this.paneTTChungNew.Size = new System.Drawing.Size(945, 155);
            this.paneTTChungNew.TabIndex = 26;
            // 
            // grHoaDonNew
            // 
            this.grHoaDonNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grHoaDonNew.Controls.Add(this.txtMauSoHDNew);
            this.grHoaDonNew.Controls.Add(this.txtSoHDNew);
            this.grHoaDonNew.Controls.Add(this.txtKyHieuHDNew);
            this.grHoaDonNew.Controls.Add(this.ultraLabel1);
            this.grHoaDonNew.Controls.Add(this.ultraLabel2);
            this.grHoaDonNew.Controls.Add(this.ultraLabel3);
            this.grHoaDonNew.Controls.Add(this.ultraLabel4);
            this.grHoaDonNew.Controls.Add(this.dteNgayHDNew);
            this.grHoaDonNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grHoaDonNew.Location = new System.Drawing.Point(658, 1);
            this.grHoaDonNew.Name = "grHoaDonNew";
            this.grHoaDonNew.Size = new System.Drawing.Size(286, 152);
            this.grHoaDonNew.TabIndex = 32;
            this.grHoaDonNew.Text = "Hóa đơn";
            this.grHoaDonNew.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtMauSoHDNew
            // 
            this.txtMauSoHDNew.AutoSize = false;
            this.txtMauSoHDNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMauSoHDNew.Location = new System.Drawing.Point(81, 24);
            this.txtMauSoHDNew.Name = "txtMauSoHDNew";
            this.txtMauSoHDNew.Size = new System.Drawing.Size(203, 22);
            this.txtMauSoHDNew.TabIndex = 76;
            // 
            // txtSoHDNew
            // 
            this.txtSoHDNew.AutoSize = false;
            this.txtSoHDNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHDNew.Location = new System.Drawing.Point(81, 75);
            this.txtSoHDNew.Name = "txtSoHDNew";
            this.txtSoHDNew.Size = new System.Drawing.Size(203, 22);
            this.txtSoHDNew.TabIndex = 18;
            // 
            // txtKyHieuHDNew
            // 
            this.txtKyHieuHDNew.AutoSize = false;
            this.txtKyHieuHDNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuHDNew.Location = new System.Drawing.Point(81, 49);
            this.txtKyHieuHDNew.Name = "txtKyHieuHDNew";
            this.txtKyHieuHDNew.Size = new System.Drawing.Size(203, 22);
            this.txtKyHieuHDNew.TabIndex = 17;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance19;
            this.ultraLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel1.Location = new System.Drawing.Point(7, 102);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel1.TabIndex = 69;
            this.ultraLabel1.Text = "Ngày HĐ";
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance20.BackColor = System.Drawing.Color.Transparent;
            appearance20.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance20;
            this.ultraLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel2.Location = new System.Drawing.Point(5, 76);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(40, 22);
            this.ultraLabel2.TabIndex = 68;
            this.ultraLabel2.Text = "Số HĐ";
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance21.BackColor = System.Drawing.Color.Transparent;
            appearance21.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance21;
            this.ultraLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel3.Location = new System.Drawing.Point(5, 51);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel3.TabIndex = 67;
            this.ultraLabel3.Text = "Ký hiệu HĐ";
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance22.BackColor = System.Drawing.Color.Transparent;
            appearance22.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance22;
            this.ultraLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel4.Location = new System.Drawing.Point(7, 27);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel4.TabIndex = 66;
            this.ultraLabel4.Text = "Mẫu số HĐ";
            // 
            // dteNgayHDNew
            // 
            appearance23.TextHAlignAsString = "Center";
            appearance23.TextVAlignAsString = "Middle";
            this.dteNgayHDNew.Appearance = appearance23;
            this.dteNgayHDNew.AutoSize = false;
            this.dteNgayHDNew.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteNgayHDNew.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dteNgayHDNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteNgayHDNew.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteNgayHDNew.Location = new System.Drawing.Point(81, 102);
            this.dteNgayHDNew.MaskInput = "";
            this.dteNgayHDNew.Name = "dteNgayHDNew";
            this.dteNgayHDNew.Size = new System.Drawing.Size(100, 22);
            this.dteNgayHDNew.TabIndex = 19;
            this.dteNgayHDNew.Value = null;
            // 
            // gpMCPayment
            // 
            this.gpMCPayment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpMCPayment.Controls.Add(this.label9);
            this.gpMCPayment.Controls.Add(this.txtAccreditativeAttach);
            this.gpMCPayment.Controls.Add(this.txtPaymentReason);
            this.gpMCPayment.Controls.Add(this.txtReceiver);
            this.gpMCPayment.Controls.Add(this.txtPaymentAccountingObjectName);
            this.gpMCPayment.Controls.Add(this.cbbPaymentAccountingObjectID);
            this.gpMCPayment.Controls.Add(this.txtPaymentAccountingObjectAddress);
            this.gpMCPayment.Controls.Add(this.label8);
            this.gpMCPayment.Controls.Add(this.label7);
            this.gpMCPayment.Controls.Add(this.label6);
            this.gpMCPayment.Controls.Add(this.label5);
            this.gpMCPayment.Controls.Add(this.D);
            this.gpMCPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpMCPayment.Location = new System.Drawing.Point(0, 1);
            this.gpMCPayment.Name = "gpMCPayment";
            this.gpMCPayment.Size = new System.Drawing.Size(655, 153);
            this.gpMCPayment.TabIndex = 24;
            this.gpMCPayment.Text = "THÔNG TIN CHUNG";
            this.gpMCPayment.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(578, 126);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Chứng từ gốc";
            // 
            // txtAccreditativeAttach
            // 
            this.txtAccreditativeAttach.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccreditativeAttach.AutoSize = false;
            this.txtAccreditativeAttach.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccreditativeAttach.Location = new System.Drawing.Point(87, 121);
            this.txtAccreditativeAttach.Name = "txtAccreditativeAttach";
            this.txtAccreditativeAttach.Size = new System.Drawing.Size(485, 22);
            this.txtAccreditativeAttach.TabIndex = 10;
            // 
            // txtPaymentReason
            // 
            this.txtPaymentReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPaymentReason.AutoSize = false;
            this.txtPaymentReason.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaymentReason.Location = new System.Drawing.Point(87, 97);
            this.txtPaymentReason.Name = "txtPaymentReason";
            this.txtPaymentReason.Size = new System.Drawing.Size(562, 22);
            this.txtPaymentReason.TabIndex = 9;
            // 
            // txtReceiver
            // 
            this.txtReceiver.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReceiver.AutoSize = false;
            this.txtReceiver.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReceiver.Location = new System.Drawing.Point(87, 73);
            this.txtReceiver.Name = "txtReceiver";
            this.txtReceiver.Size = new System.Drawing.Size(562, 22);
            this.txtReceiver.TabIndex = 8;
            // 
            // txtPaymentAccountingObjectName
            // 
            this.txtPaymentAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPaymentAccountingObjectName.AutoSize = false;
            this.txtPaymentAccountingObjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaymentAccountingObjectName.Location = new System.Drawing.Point(268, 25);
            this.txtPaymentAccountingObjectName.Name = "txtPaymentAccountingObjectName";
            this.txtPaymentAccountingObjectName.Size = new System.Drawing.Size(381, 22);
            this.txtPaymentAccountingObjectName.TabIndex = 7;
            // 
            // cbbPaymentAccountingObjectID
            // 
            this.cbbPaymentAccountingObjectID.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.None;
            this.cbbPaymentAccountingObjectID.AutoSize = false;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Appearance = appearance24;
            this.cbbPaymentAccountingObjectID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbPaymentAccountingObjectID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbPaymentAccountingObjectID.DisplayLayout.GroupByBox.Appearance = appearance25;
            appearance26.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbPaymentAccountingObjectID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance26;
            this.cbbPaymentAccountingObjectID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance27.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance27.BackColor2 = System.Drawing.SystemColors.Control;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbPaymentAccountingObjectID.DisplayLayout.GroupByBox.PromptAppearance = appearance27;
            this.cbbPaymentAccountingObjectID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbPaymentAccountingObjectID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.ActiveCellAppearance = appearance28;
            appearance29.BackColor = System.Drawing.SystemColors.Highlight;
            appearance29.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.ActiveRowAppearance = appearance29;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.CardAreaAppearance = appearance30;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            appearance31.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.CellAppearance = appearance31;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.CellPadding = 0;
            appearance32.BackColor = System.Drawing.SystemColors.Control;
            appearance32.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance32.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance32.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.GroupByRowAppearance = appearance32;
            appearance33.TextHAlignAsString = "Left";
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.HeaderAppearance = appearance33;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.RowAppearance = appearance34;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.TemplateAddRowAppearance = appearance35;
            this.cbbPaymentAccountingObjectID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbPaymentAccountingObjectID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbPaymentAccountingObjectID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbPaymentAccountingObjectID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbPaymentAccountingObjectID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPaymentAccountingObjectID.Location = new System.Drawing.Point(87, 25);
            this.cbbPaymentAccountingObjectID.Name = "cbbPaymentAccountingObjectID";
            this.cbbPaymentAccountingObjectID.Size = new System.Drawing.Size(175, 22);
            this.cbbPaymentAccountingObjectID.TabIndex = 6;
            // 
            // txtPaymentAccountingObjectAddress
            // 
            this.txtPaymentAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPaymentAccountingObjectAddress.AutoSize = false;
            this.txtPaymentAccountingObjectAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaymentAccountingObjectAddress.Location = new System.Drawing.Point(87, 49);
            this.txtPaymentAccountingObjectAddress.Name = "txtPaymentAccountingObjectAddress";
            this.txtPaymentAccountingObjectAddress.Size = new System.Drawing.Size(562, 22);
            this.txtPaymentAccountingObjectAddress.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 126);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Kèm theo";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 102);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Lý do chi";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Người nhận";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Địa chỉ";
            // 
            // D
            // 
            this.D.AutoSize = true;
            this.D.BackColor = System.Drawing.Color.Transparent;
            this.D.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.D.Location = new System.Drawing.Point(12, 31);
            this.D.Name = "D";
            this.D.Size = new System.Drawing.Size(53, 13);
            this.D.TabIndex = 0;
            this.D.Text = "Đối tượng";
            // 
            // pnMBTellerPaper_Sec
            // 
            this.pnMBTellerPaper_Sec.BackColor = System.Drawing.Color.Transparent;
            this.pnMBTellerPaper_Sec.Controls.Add(this.grHoaDon4);
            this.pnMBTellerPaper_Sec.Controls.Add(this.ultraGroupBox7);
            this.pnMBTellerPaper_Sec.Controls.Add(this.ultraGroupBox8);
            this.pnMBTellerPaper_Sec.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnMBTellerPaper_Sec.Location = new System.Drawing.Point(0, 693);
            this.pnMBTellerPaper_Sec.Name = "pnMBTellerPaper_Sec";
            this.pnMBTellerPaper_Sec.Size = new System.Drawing.Size(945, 180);
            this.pnMBTellerPaper_Sec.TabIndex = 22;
            this.pnMBTellerPaper_Sec.Visible = false;
            // 
            // grHoaDon4
            // 
            this.grHoaDon4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grHoaDon4.Controls.Add(this.txtMauSoHD4);
            this.grHoaDon4.Controls.Add(this.txtSoHD4);
            this.grHoaDon4.Controls.Add(this.txtKyHieuHD4);
            this.grHoaDon4.Controls.Add(this.ultraLabel17);
            this.grHoaDon4.Controls.Add(this.ultraLabel18);
            this.grHoaDon4.Controls.Add(this.ultraLabel19);
            this.grHoaDon4.Controls.Add(this.ultraLabel20);
            this.grHoaDon4.Controls.Add(this.dteNgayHD4);
            this.grHoaDon4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grHoaDon4.Location = new System.Drawing.Point(616, 72);
            this.grHoaDon4.Name = "grHoaDon4";
            this.grHoaDon4.Size = new System.Drawing.Size(327, 106);
            this.grHoaDon4.TabIndex = 35;
            this.grHoaDon4.Text = "Hóa đơn";
            this.grHoaDon4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtMauSoHD4
            // 
            this.txtMauSoHD4.AutoSize = false;
            this.txtMauSoHD4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMauSoHD4.Location = new System.Drawing.Point(72, 26);
            this.txtMauSoHD4.Name = "txtMauSoHD4";
            this.txtMauSoHD4.Size = new System.Drawing.Size(250, 22);
            this.txtMauSoHD4.TabIndex = 76;
            // 
            // txtSoHD4
            // 
            this.txtSoHD4.AutoSize = false;
            this.txtSoHD4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHD4.Location = new System.Drawing.Point(72, 78);
            this.txtSoHD4.Name = "txtSoHD4";
            this.txtSoHD4.Size = new System.Drawing.Size(100, 22);
            this.txtSoHD4.TabIndex = 18;
            // 
            // txtKyHieuHD4
            // 
            this.txtKyHieuHD4.AutoSize = false;
            this.txtKyHieuHD4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuHD4.Location = new System.Drawing.Point(72, 52);
            this.txtKyHieuHD4.Name = "txtKyHieuHD4";
            this.txtKyHieuHD4.Size = new System.Drawing.Size(250, 22);
            this.txtKyHieuHD4.TabIndex = 17;
            // 
            // ultraLabel17
            // 
            this.ultraLabel17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance36.BackColor = System.Drawing.Color.Transparent;
            appearance36.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance36;
            this.ultraLabel17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel17.Location = new System.Drawing.Point(178, 79);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel17.TabIndex = 69;
            this.ultraLabel17.Text = "Ngày HĐ";
            // 
            // ultraLabel18
            // 
            this.ultraLabel18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance37.BackColor = System.Drawing.Color.Transparent;
            appearance37.TextVAlignAsString = "Middle";
            this.ultraLabel18.Appearance = appearance37;
            this.ultraLabel18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel18.Location = new System.Drawing.Point(5, 82);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(40, 22);
            this.ultraLabel18.TabIndex = 68;
            this.ultraLabel18.Text = "Số HĐ";
            // 
            // ultraLabel19
            // 
            this.ultraLabel19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance38.BackColor = System.Drawing.Color.Transparent;
            appearance38.TextVAlignAsString = "Middle";
            this.ultraLabel19.Appearance = appearance38;
            this.ultraLabel19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel19.Location = new System.Drawing.Point(5, 57);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel19.TabIndex = 67;
            this.ultraLabel19.Text = "Ký hiệu HĐ";
            // 
            // ultraLabel20
            // 
            this.ultraLabel20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance39.BackColor = System.Drawing.Color.Transparent;
            appearance39.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance39;
            this.ultraLabel20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel20.Location = new System.Drawing.Point(4, 29);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel20.TabIndex = 66;
            this.ultraLabel20.Text = "Mẫu số HĐ";
            // 
            // dteNgayHD4
            // 
            appearance40.TextHAlignAsString = "Center";
            appearance40.TextVAlignAsString = "Middle";
            this.dteNgayHD4.Appearance = appearance40;
            this.dteNgayHD4.AutoSize = false;
            this.dteNgayHD4.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteNgayHD4.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dteNgayHD4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteNgayHD4.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteNgayHD4.Location = new System.Drawing.Point(233, 79);
            this.dteNgayHD4.MaskInput = "";
            this.dteNgayHD4.Name = "dteNgayHD4";
            this.dteNgayHD4.Size = new System.Drawing.Size(89, 22);
            this.dteNgayHD4.TabIndex = 19;
            this.dteNgayHD4.Value = null;
            // 
            // ultraGroupBox7
            // 
            this.ultraGroupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox7.Controls.Add(this.dtSecIssueDate);
            this.ultraGroupBox7.Controls.Add(this.label26);
            this.ultraGroupBox7.Controls.Add(this.label25);
            this.ultraGroupBox7.Controls.Add(this.txtSecIdentificationNo);
            this.ultraGroupBox7.Controls.Add(this.txtSecIssueBy);
            this.ultraGroupBox7.Controls.Add(this.label20);
            this.ultraGroupBox7.Controls.Add(this.txtSecAccountingObjectName);
            this.ultraGroupBox7.Controls.Add(this.cbbSecAccountingObjectID);
            this.ultraGroupBox7.Controls.Add(this.txtSecReceiver);
            this.ultraGroupBox7.Controls.Add(this.label21);
            this.ultraGroupBox7.Controls.Add(this.label22);
            this.ultraGroupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox7.Location = new System.Drawing.Point(0, 73);
            this.ultraGroupBox7.Name = "ultraGroupBox7";
            this.ultraGroupBox7.Size = new System.Drawing.Size(615, 105);
            this.ultraGroupBox7.TabIndex = 3;
            this.ultraGroupBox7.Text = "Đối tượng nhận tiền";
            this.ultraGroupBox7.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // dtSecIssueDate
            // 
            appearance41.TextHAlignAsString = "Center";
            appearance41.TextVAlignAsString = "Middle";
            this.dtSecIssueDate.Appearance = appearance41;
            this.dtSecIssueDate.AutoSize = false;
            this.dtSecIssueDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtSecIssueDate.Location = new System.Drawing.Point(334, 76);
            this.dtSecIssueDate.MaskInput = "dd/mm/yyyy";
            this.dtSecIssueDate.Name = "dtSecIssueDate";
            this.dtSecIssueDate.Size = new System.Drawing.Size(98, 22);
            this.dtSecIssueDate.TabIndex = 28;
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(438, 77);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(44, 21);
            this.label26.TabIndex = 18;
            this.label26.Text = "Nơi cấp";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(275, 77);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 21);
            this.label25.TabIndex = 17;
            this.label25.Text = "Ngày cấp";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSecIdentificationNo
            // 
            this.txtSecIdentificationNo.AutoSize = false;
            this.txtSecIdentificationNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecIdentificationNo.Location = new System.Drawing.Point(90, 76);
            this.txtSecIdentificationNo.Name = "txtSecIdentificationNo";
            this.txtSecIdentificationNo.Size = new System.Drawing.Size(175, 22);
            this.txtSecIdentificationNo.TabIndex = 16;
            // 
            // txtSecIssueBy
            // 
            this.txtSecIssueBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecIssueBy.AutoSize = false;
            this.txtSecIssueBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecIssueBy.Location = new System.Drawing.Point(484, 76);
            this.txtSecIssueBy.Name = "txtSecIssueBy";
            this.txtSecIssueBy.Size = new System.Drawing.Size(125, 22);
            this.txtSecIssueBy.TabIndex = 15;
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(10, 77);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(77, 21);
            this.label20.TabIndex = 13;
            this.label20.Text = "Số CMND";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSecAccountingObjectName
            // 
            this.txtSecAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecAccountingObjectName.AutoSize = false;
            this.txtSecAccountingObjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecAccountingObjectName.Location = new System.Drawing.Point(271, 28);
            this.txtSecAccountingObjectName.Name = "txtSecAccountingObjectName";
            this.txtSecAccountingObjectName.Size = new System.Drawing.Size(338, 22);
            this.txtSecAccountingObjectName.TabIndex = 12;
            // 
            // cbbSecAccountingObjectID
            // 
            this.cbbSecAccountingObjectID.AutoSize = false;
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            appearance42.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbSecAccountingObjectID.DisplayLayout.Appearance = appearance42;
            this.cbbSecAccountingObjectID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbSecAccountingObjectID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance43.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance43.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance43.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSecAccountingObjectID.DisplayLayout.GroupByBox.Appearance = appearance43;
            appearance44.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecAccountingObjectID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance44;
            this.cbbSecAccountingObjectID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance45.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance45.BackColor2 = System.Drawing.SystemColors.Control;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecAccountingObjectID.DisplayLayout.GroupByBox.PromptAppearance = appearance45;
            this.cbbSecAccountingObjectID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbSecAccountingObjectID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            appearance46.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.ActiveCellAppearance = appearance46;
            appearance47.BackColor = System.Drawing.SystemColors.Highlight;
            appearance47.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.ActiveRowAppearance = appearance47;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance48.BackColor = System.Drawing.SystemColors.Window;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.CardAreaAppearance = appearance48;
            appearance49.BorderColor = System.Drawing.Color.Silver;
            appearance49.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.CellAppearance = appearance49;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.CellPadding = 0;
            appearance50.BackColor = System.Drawing.SystemColors.Control;
            appearance50.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance50.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance50.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance50.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.GroupByRowAppearance = appearance50;
            appearance51.TextHAlignAsString = "Left";
            this.cbbSecAccountingObjectID.DisplayLayout.Override.HeaderAppearance = appearance51;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance52.BackColor = System.Drawing.SystemColors.Window;
            appearance52.BorderColor = System.Drawing.Color.Silver;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.RowAppearance = appearance52;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance53.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.TemplateAddRowAppearance = appearance53;
            this.cbbSecAccountingObjectID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbSecAccountingObjectID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbSecAccountingObjectID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbSecAccountingObjectID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbSecAccountingObjectID.Location = new System.Drawing.Point(90, 28);
            this.cbbSecAccountingObjectID.Name = "cbbSecAccountingObjectID";
            this.cbbSecAccountingObjectID.Size = new System.Drawing.Size(175, 22);
            this.cbbSecAccountingObjectID.TabIndex = 11;
            // 
            // txtSecReceiver
            // 
            this.txtSecReceiver.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecReceiver.AutoSize = false;
            this.txtSecReceiver.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecReceiver.Location = new System.Drawing.Point(90, 52);
            this.txtSecReceiver.Name = "txtSecReceiver";
            this.txtSecReceiver.Size = new System.Drawing.Size(519, 22);
            this.txtSecReceiver.TabIndex = 10;
            // 
            // label21
            // 
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(10, 52);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(77, 21);
            this.label21.TabIndex = 9;
            this.label21.Text = "Người lĩnh tiền";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(10, 28);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(77, 21);
            this.label22.TabIndex = 8;
            this.label22.Text = "Đối tượng";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ultraGroupBox8
            // 
            this.ultraGroupBox8.Controls.Add(this.txtSecReason);
            this.ultraGroupBox8.Controls.Add(this.txtSecBankName);
            this.ultraGroupBox8.Controls.Add(this.cbbSecBankAccountDetailID);
            this.ultraGroupBox8.Controls.Add(this.label23);
            this.ultraGroupBox8.Controls.Add(this.label24);
            this.ultraGroupBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox8.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox8.Name = "ultraGroupBox8";
            this.ultraGroupBox8.Size = new System.Drawing.Size(945, 72);
            this.ultraGroupBox8.TabIndex = 2;
            this.ultraGroupBox8.Text = "Đơn vị trả tiền";
            this.ultraGroupBox8.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtSecReason
            // 
            this.txtSecReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecReason.AutoSize = false;
            this.txtSecReason.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecReason.Location = new System.Drawing.Point(90, 42);
            this.txtSecReason.Name = "txtSecReason";
            this.txtSecReason.Size = new System.Drawing.Size(849, 22);
            this.txtSecReason.TabIndex = 4;
            // 
            // txtSecBankName
            // 
            this.txtSecBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecBankName.AutoSize = false;
            this.txtSecBankName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecBankName.Location = new System.Drawing.Point(271, 18);
            this.txtSecBankName.Name = "txtSecBankName";
            this.txtSecBankName.Size = new System.Drawing.Size(668, 22);
            this.txtSecBankName.TabIndex = 3;
            // 
            // cbbSecBankAccountDetailID
            // 
            this.cbbSecBankAccountDetailID.AutoSize = false;
            appearance54.BackColor = System.Drawing.SystemColors.Window;
            appearance54.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbSecBankAccountDetailID.DisplayLayout.Appearance = appearance54;
            this.cbbSecBankAccountDetailID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbSecBankAccountDetailID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance55.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance55.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance55.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance55.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSecBankAccountDetailID.DisplayLayout.GroupByBox.Appearance = appearance55;
            appearance56.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecBankAccountDetailID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance56;
            this.cbbSecBankAccountDetailID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance57.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance57.BackColor2 = System.Drawing.SystemColors.Control;
            appearance57.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance57.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecBankAccountDetailID.DisplayLayout.GroupByBox.PromptAppearance = appearance57;
            this.cbbSecBankAccountDetailID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbSecBankAccountDetailID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance58.BackColor = System.Drawing.SystemColors.Window;
            appearance58.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.ActiveCellAppearance = appearance58;
            appearance59.BackColor = System.Drawing.SystemColors.Highlight;
            appearance59.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.ActiveRowAppearance = appearance59;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance60.BackColor = System.Drawing.SystemColors.Window;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.CardAreaAppearance = appearance60;
            appearance61.BorderColor = System.Drawing.Color.Silver;
            appearance61.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.CellAppearance = appearance61;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.CellPadding = 0;
            appearance62.BackColor = System.Drawing.SystemColors.Control;
            appearance62.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance62.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance62.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance62.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.GroupByRowAppearance = appearance62;
            appearance63.TextHAlignAsString = "Left";
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.HeaderAppearance = appearance63;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance64.BackColor = System.Drawing.SystemColors.Window;
            appearance64.BorderColor = System.Drawing.Color.Silver;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.RowAppearance = appearance64;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance65.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.TemplateAddRowAppearance = appearance65;
            this.cbbSecBankAccountDetailID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbSecBankAccountDetailID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbSecBankAccountDetailID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbSecBankAccountDetailID.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbSecBankAccountDetailID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbSecBankAccountDetailID.Location = new System.Drawing.Point(90, 18);
            this.cbbSecBankAccountDetailID.Name = "cbbSecBankAccountDetailID";
            this.cbbSecBankAccountDetailID.Size = new System.Drawing.Size(175, 22);
            this.cbbSecBankAccountDetailID.TabIndex = 2;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(10, 42);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(77, 21);
            this.label23.TabIndex = 1;
            this.label23.Text = "Nội dung TT";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(10, 18);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(77, 21);
            this.label24.TabIndex = 0;
            this.label24.Text = "Tài khoản";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnMBCreditCard
            // 
            this.pnMBCreditCard.BackColor = System.Drawing.Color.Transparent;
            this.pnMBCreditCard.Controls.Add(this.grHoaDon3);
            this.pnMBCreditCard.Controls.Add(this.ultraGroupBox5);
            this.pnMBCreditCard.Controls.Add(this.ultraGroupBox6);
            this.pnMBCreditCard.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnMBCreditCard.Location = new System.Drawing.Point(0, 505);
            this.pnMBCreditCard.Name = "pnMBCreditCard";
            this.pnMBCreditCard.Size = new System.Drawing.Size(945, 188);
            this.pnMBCreditCard.TabIndex = 21;
            this.pnMBCreditCard.Visible = false;
            // 
            // grHoaDon3
            // 
            this.grHoaDon3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grHoaDon3.Controls.Add(this.txtMauSoHD3);
            this.grHoaDon3.Controls.Add(this.txtSoHD3);
            this.grHoaDon3.Controls.Add(this.txtKyHieuHD3);
            this.grHoaDon3.Controls.Add(this.ultraLabel13);
            this.grHoaDon3.Controls.Add(this.ultraLabel14);
            this.grHoaDon3.Controls.Add(this.ultraLabel15);
            this.grHoaDon3.Controls.Add(this.ultraLabel16);
            this.grHoaDon3.Controls.Add(this.dteNgayHD3);
            this.grHoaDon3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grHoaDon3.Location = new System.Drawing.Point(617, 75);
            this.grHoaDon3.Name = "grHoaDon3";
            this.grHoaDon3.Size = new System.Drawing.Size(327, 111);
            this.grHoaDon3.TabIndex = 34;
            this.grHoaDon3.Text = "Hóa đơn";
            this.grHoaDon3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtMauSoHD3
            // 
            this.txtMauSoHD3.AutoSize = false;
            this.txtMauSoHD3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMauSoHD3.Location = new System.Drawing.Point(72, 26);
            this.txtMauSoHD3.Name = "txtMauSoHD3";
            this.txtMauSoHD3.Size = new System.Drawing.Size(250, 22);
            this.txtMauSoHD3.TabIndex = 76;
            // 
            // txtSoHD3
            // 
            this.txtSoHD3.AutoSize = false;
            this.txtSoHD3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHD3.Location = new System.Drawing.Point(72, 78);
            this.txtSoHD3.Name = "txtSoHD3";
            this.txtSoHD3.Size = new System.Drawing.Size(100, 22);
            this.txtSoHD3.TabIndex = 18;
            // 
            // txtKyHieuHD3
            // 
            this.txtKyHieuHD3.AutoSize = false;
            this.txtKyHieuHD3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuHD3.Location = new System.Drawing.Point(72, 52);
            this.txtKyHieuHD3.Name = "txtKyHieuHD3";
            this.txtKyHieuHD3.Size = new System.Drawing.Size(250, 22);
            this.txtKyHieuHD3.TabIndex = 17;
            // 
            // ultraLabel13
            // 
            this.ultraLabel13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance66.BackColor = System.Drawing.Color.Transparent;
            appearance66.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance66;
            this.ultraLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel13.Location = new System.Drawing.Point(178, 79);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel13.TabIndex = 69;
            this.ultraLabel13.Text = "Ngày HĐ";
            // 
            // ultraLabel14
            // 
            this.ultraLabel14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance67.BackColor = System.Drawing.Color.Transparent;
            appearance67.TextVAlignAsString = "Middle";
            this.ultraLabel14.Appearance = appearance67;
            this.ultraLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel14.Location = new System.Drawing.Point(5, 82);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(40, 22);
            this.ultraLabel14.TabIndex = 68;
            this.ultraLabel14.Text = "Số HĐ";
            // 
            // ultraLabel15
            // 
            this.ultraLabel15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance68.BackColor = System.Drawing.Color.Transparent;
            appearance68.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance68;
            this.ultraLabel15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel15.Location = new System.Drawing.Point(5, 57);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel15.TabIndex = 67;
            this.ultraLabel15.Text = "Ký hiệu HĐ";
            // 
            // ultraLabel16
            // 
            this.ultraLabel16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance69.BackColor = System.Drawing.Color.Transparent;
            appearance69.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance69;
            this.ultraLabel16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel16.Location = new System.Drawing.Point(4, 29);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel16.TabIndex = 66;
            this.ultraLabel16.Text = "Mẫu số HĐ";
            // 
            // dteNgayHD3
            // 
            appearance70.TextHAlignAsString = "Center";
            appearance70.TextVAlignAsString = "Middle";
            this.dteNgayHD3.Appearance = appearance70;
            this.dteNgayHD3.AutoSize = false;
            this.dteNgayHD3.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteNgayHD3.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dteNgayHD3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteNgayHD3.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteNgayHD3.Location = new System.Drawing.Point(233, 79);
            this.dteNgayHD3.MaskInput = "";
            this.dteNgayHD3.Name = "dteNgayHD3";
            this.dteNgayHD3.Size = new System.Drawing.Size(89, 22);
            this.dteNgayHD3.TabIndex = 19;
            this.dteNgayHD3.Value = null;
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox5.Controls.Add(this.txtCreditCardAccountingObjectBankName);
            this.ultraGroupBox5.Controls.Add(this.cbbCreditCardAccountingObjectBankAccount);
            this.ultraGroupBox5.Controls.Add(this.label27);
            this.ultraGroupBox5.Controls.Add(this.txtCreditCardAccountingObjectName);
            this.ultraGroupBox5.Controls.Add(this.cbbCreditCardAccountingObjectID);
            this.ultraGroupBox5.Controls.Add(this.txtCreditCardAccountingObjectAddress);
            this.ultraGroupBox5.Controls.Add(this.label28);
            this.ultraGroupBox5.Controls.Add(this.label29);
            this.ultraGroupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox5.Location = new System.Drawing.Point(0, 75);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(617, 113);
            this.ultraGroupBox5.TabIndex = 5;
            this.ultraGroupBox5.Text = "Đối tượng nhận tiền";
            this.ultraGroupBox5.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtCreditCardAccountingObjectBankName
            // 
            this.txtCreditCardAccountingObjectBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreditCardAccountingObjectBankName.AutoSize = false;
            this.txtCreditCardAccountingObjectBankName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtCreditCardAccountingObjectBankName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditCardAccountingObjectBankName.Location = new System.Drawing.Point(271, 77);
            this.txtCreditCardAccountingObjectBankName.Name = "txtCreditCardAccountingObjectBankName";
            this.txtCreditCardAccountingObjectBankName.Size = new System.Drawing.Size(340, 22);
            this.txtCreditCardAccountingObjectBankName.TabIndex = 15;
            // 
            // cbbCreditCardAccountingObjectBankAccount
            // 
            this.cbbCreditCardAccountingObjectBankAccount.AutoSize = false;
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            appearance71.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Appearance = appearance71;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance72.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance72.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance72.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance72.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.GroupByBox.Appearance = appearance72;
            appearance73.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance73;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance74.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance74.BackColor2 = System.Drawing.SystemColors.Control;
            appearance74.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance74.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance74;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance75.BackColor = System.Drawing.SystemColors.Window;
            appearance75.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.ActiveCellAppearance = appearance75;
            appearance76.BackColor = System.Drawing.SystemColors.Highlight;
            appearance76.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.ActiveRowAppearance = appearance76;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance77.BackColor = System.Drawing.SystemColors.Window;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.CardAreaAppearance = appearance77;
            appearance78.BorderColor = System.Drawing.Color.Silver;
            appearance78.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.CellAppearance = appearance78;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.CellPadding = 0;
            appearance79.BackColor = System.Drawing.SystemColors.Control;
            appearance79.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance79.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance79.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance79.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.GroupByRowAppearance = appearance79;
            appearance80.TextHAlignAsString = "Left";
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.HeaderAppearance = appearance80;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance81.BackColor = System.Drawing.SystemColors.Window;
            appearance81.BorderColor = System.Drawing.Color.Silver;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.RowAppearance = appearance81;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance82.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance82;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbCreditCardAccountingObjectBankAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCreditCardAccountingObjectBankAccount.Location = new System.Drawing.Point(90, 77);
            this.cbbCreditCardAccountingObjectBankAccount.Name = "cbbCreditCardAccountingObjectBankAccount";
            this.cbbCreditCardAccountingObjectBankAccount.Size = new System.Drawing.Size(175, 22);
            this.cbbCreditCardAccountingObjectBankAccount.TabIndex = 14;
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(17, 77);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(67, 21);
            this.label27.TabIndex = 13;
            this.label27.Text = "Tài khoản";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCreditCardAccountingObjectName
            // 
            this.txtCreditCardAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreditCardAccountingObjectName.AutoSize = false;
            this.txtCreditCardAccountingObjectName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtCreditCardAccountingObjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditCardAccountingObjectName.Location = new System.Drawing.Point(271, 28);
            this.txtCreditCardAccountingObjectName.Name = "txtCreditCardAccountingObjectName";
            this.txtCreditCardAccountingObjectName.Size = new System.Drawing.Size(340, 22);
            this.txtCreditCardAccountingObjectName.TabIndex = 12;
            // 
            // cbbCreditCardAccountingObjectID
            // 
            this.cbbCreditCardAccountingObjectID.AutoSize = false;
            appearance83.BackColor = System.Drawing.SystemColors.Window;
            appearance83.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Appearance = appearance83;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance84.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance84.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance84.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance84.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.GroupByBox.Appearance = appearance84;
            appearance85.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance85;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance86.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance86.BackColor2 = System.Drawing.SystemColors.Control;
            appearance86.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance86.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.GroupByBox.PromptAppearance = appearance86;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance87.BackColor = System.Drawing.SystemColors.Window;
            appearance87.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.ActiveCellAppearance = appearance87;
            appearance88.BackColor = System.Drawing.SystemColors.Highlight;
            appearance88.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.ActiveRowAppearance = appearance88;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance89.BackColor = System.Drawing.SystemColors.Window;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.CardAreaAppearance = appearance89;
            appearance90.BorderColor = System.Drawing.Color.Silver;
            appearance90.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.CellAppearance = appearance90;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.CellPadding = 0;
            appearance91.BackColor = System.Drawing.SystemColors.Control;
            appearance91.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance91.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance91.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance91.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.GroupByRowAppearance = appearance91;
            appearance92.TextHAlignAsString = "Left";
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.HeaderAppearance = appearance92;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance93.BackColor = System.Drawing.SystemColors.Window;
            appearance93.BorderColor = System.Drawing.Color.Silver;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.RowAppearance = appearance93;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance94.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.TemplateAddRowAppearance = appearance94;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbCreditCardAccountingObjectID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCreditCardAccountingObjectID.Location = new System.Drawing.Point(90, 28);
            this.cbbCreditCardAccountingObjectID.Name = "cbbCreditCardAccountingObjectID";
            this.cbbCreditCardAccountingObjectID.Size = new System.Drawing.Size(175, 22);
            this.cbbCreditCardAccountingObjectID.TabIndex = 11;
            // 
            // txtCreditCardAccountingObjectAddress
            // 
            this.txtCreditCardAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreditCardAccountingObjectAddress.AutoSize = false;
            this.txtCreditCardAccountingObjectAddress.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtCreditCardAccountingObjectAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditCardAccountingObjectAddress.Location = new System.Drawing.Point(90, 52);
            this.txtCreditCardAccountingObjectAddress.Name = "txtCreditCardAccountingObjectAddress";
            this.txtCreditCardAccountingObjectAddress.Size = new System.Drawing.Size(521, 22);
            this.txtCreditCardAccountingObjectAddress.TabIndex = 10;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(17, 52);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(67, 21);
            this.label28.TabIndex = 9;
            this.label28.Text = "Địa chỉ";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(17, 28);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(67, 21);
            this.label29.TabIndex = 8;
            this.label29.Text = "Đối tượng";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ultraGroupBox6
            // 
            this.ultraGroupBox6.Controls.Add(this.txtCreditCardType);
            this.ultraGroupBox6.Controls.Add(this.picCreditCardType);
            this.ultraGroupBox6.Controls.Add(this.txtOwnerCard);
            this.ultraGroupBox6.Controls.Add(this.lblOwnerCard);
            this.ultraGroupBox6.Controls.Add(this.cbbCreditCardNumber);
            this.ultraGroupBox6.Controls.Add(this.txtCreditCardReason);
            this.ultraGroupBox6.Controls.Add(this.label30);
            this.ultraGroupBox6.Controls.Add(this.label31);
            this.ultraGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox6.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox6.Name = "ultraGroupBox6";
            this.ultraGroupBox6.Size = new System.Drawing.Size(945, 75);
            this.ultraGroupBox6.TabIndex = 4;
            this.ultraGroupBox6.Text = "Thẻ tín dụng";
            this.ultraGroupBox6.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2000;
            // 
            // txtCreditCardType
            // 
            appearance95.BackColor = System.Drawing.Color.Transparent;
            appearance95.TextHAlignAsString = "Center";
            appearance95.TextVAlignAsString = "Middle";
            this.txtCreditCardType.Appearance = appearance95;
            this.txtCreditCardType.Location = new System.Drawing.Point(323, 18);
            this.txtCreditCardType.Name = "txtCreditCardType";
            this.txtCreditCardType.Size = new System.Drawing.Size(135, 22);
            this.txtCreditCardType.TabIndex = 46;
            // 
            // picCreditCardType
            // 
            appearance96.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance96.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance96.TextHAlignAsString = "Center";
            appearance96.TextVAlignAsString = "Middle";
            this.picCreditCardType.Appearance = appearance96;
            this.picCreditCardType.BackColor = System.Drawing.Color.Transparent;
            this.picCreditCardType.BorderShadowColor = System.Drawing.Color.Transparent;
            this.picCreditCardType.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.picCreditCardType.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.picCreditCardType.Location = new System.Drawing.Point(271, 18);
            this.picCreditCardType.Name = "picCreditCardType";
            this.picCreditCardType.Size = new System.Drawing.Size(43, 22);
            this.picCreditCardType.TabIndex = 45;
            // 
            // txtOwnerCard
            // 
            this.txtOwnerCard.BackColor = System.Drawing.Color.Transparent;
            this.txtOwnerCard.Location = new System.Drawing.Point(517, 17);
            this.txtOwnerCard.Name = "txtOwnerCard";
            this.txtOwnerCard.Size = new System.Drawing.Size(227, 22);
            this.txtOwnerCard.TabIndex = 44;
            this.txtOwnerCard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblOwnerCard
            // 
            appearance97.BackColor = System.Drawing.Color.Transparent;
            appearance97.FontData.BoldAsString = "False";
            appearance97.TextVAlignAsString = "Middle";
            this.lblOwnerCard.Appearance = appearance97;
            this.lblOwnerCard.Location = new System.Drawing.Point(464, 17);
            this.lblOwnerCard.Name = "lblOwnerCard";
            this.lblOwnerCard.Size = new System.Drawing.Size(47, 22);
            this.lblOwnerCard.TabIndex = 43;
            this.lblOwnerCard.Text = "Chủ thẻ:";
            // 
            // cbbCreditCardNumber
            // 
            this.cbbCreditCardNumber.AutoSize = false;
            appearance98.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton1.Appearance = appearance98;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbCreditCardNumber.ButtonsRight.Add(editorButton1);
            this.cbbCreditCardNumber.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbCreditCardNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCreditCardNumber.Location = new System.Drawing.Point(90, 18);
            this.cbbCreditCardNumber.Name = "cbbCreditCardNumber";
            this.cbbCreditCardNumber.Size = new System.Drawing.Size(175, 22);
            this.cbbCreditCardNumber.TabIndex = 32;
            // 
            // txtCreditCardReason
            // 
            this.txtCreditCardReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreditCardReason.AutoSize = false;
            this.txtCreditCardReason.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtCreditCardReason.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditCardReason.Location = new System.Drawing.Point(90, 42);
            this.txtCreditCardReason.Name = "txtCreditCardReason";
            this.txtCreditCardReason.Size = new System.Drawing.Size(849, 22);
            this.txtCreditCardReason.TabIndex = 4;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(17, 42);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(67, 21);
            this.label30.TabIndex = 1;
            this.label30.Text = "Nội dung TT";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(17, 18);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(67, 21);
            this.label31.TabIndex = 0;
            this.label31.Text = "Số thẻ";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnMBTellerPaper_SecCK
            // 
            this.pnMBTellerPaper_SecCK.BackColor = System.Drawing.Color.Transparent;
            this.pnMBTellerPaper_SecCK.Controls.Add(this.grHoaDon2);
            this.pnMBTellerPaper_SecCK.Controls.Add(this.ultraGroupBox3);
            this.pnMBTellerPaper_SecCK.Controls.Add(this.ultraGroupBox4);
            this.pnMBTellerPaper_SecCK.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnMBTellerPaper_SecCK.Location = new System.Drawing.Point(0, 318);
            this.pnMBTellerPaper_SecCK.Name = "pnMBTellerPaper_SecCK";
            this.pnMBTellerPaper_SecCK.Size = new System.Drawing.Size(945, 187);
            this.pnMBTellerPaper_SecCK.TabIndex = 20;
            this.pnMBTellerPaper_SecCK.Visible = false;
            // 
            // grHoaDon2
            // 
            this.grHoaDon2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grHoaDon2.Controls.Add(this.txtMauSoHD2);
            this.grHoaDon2.Controls.Add(this.txtSoHD2);
            this.grHoaDon2.Controls.Add(this.txtKyHieuHD2);
            this.grHoaDon2.Controls.Add(this.ultraLabel9);
            this.grHoaDon2.Controls.Add(this.ultraLabel10);
            this.grHoaDon2.Controls.Add(this.ultraLabel11);
            this.grHoaDon2.Controls.Add(this.ultraLabel12);
            this.grHoaDon2.Controls.Add(this.dteNgayHD2);
            this.grHoaDon2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grHoaDon2.Location = new System.Drawing.Point(617, 76);
            this.grHoaDon2.Name = "grHoaDon2";
            this.grHoaDon2.Size = new System.Drawing.Size(327, 111);
            this.grHoaDon2.TabIndex = 33;
            this.grHoaDon2.Text = "Hóa đơn";
            this.grHoaDon2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtMauSoHD2
            // 
            this.txtMauSoHD2.AutoSize = false;
            this.txtMauSoHD2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMauSoHD2.Location = new System.Drawing.Point(72, 26);
            this.txtMauSoHD2.Name = "txtMauSoHD2";
            this.txtMauSoHD2.Size = new System.Drawing.Size(250, 22);
            this.txtMauSoHD2.TabIndex = 76;
            // 
            // txtSoHD2
            // 
            this.txtSoHD2.AutoSize = false;
            this.txtSoHD2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHD2.Location = new System.Drawing.Point(72, 78);
            this.txtSoHD2.Name = "txtSoHD2";
            this.txtSoHD2.Size = new System.Drawing.Size(100, 22);
            this.txtSoHD2.TabIndex = 18;
            // 
            // txtKyHieuHD2
            // 
            this.txtKyHieuHD2.AutoSize = false;
            this.txtKyHieuHD2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuHD2.Location = new System.Drawing.Point(72, 52);
            this.txtKyHieuHD2.Name = "txtKyHieuHD2";
            this.txtKyHieuHD2.Size = new System.Drawing.Size(250, 22);
            this.txtKyHieuHD2.TabIndex = 17;
            // 
            // ultraLabel9
            // 
            this.ultraLabel9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance99.BackColor = System.Drawing.Color.Transparent;
            appearance99.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance99;
            this.ultraLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel9.Location = new System.Drawing.Point(178, 79);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel9.TabIndex = 69;
            this.ultraLabel9.Text = "Ngày HĐ";
            // 
            // ultraLabel10
            // 
            this.ultraLabel10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance100.BackColor = System.Drawing.Color.Transparent;
            appearance100.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance100;
            this.ultraLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel10.Location = new System.Drawing.Point(5, 82);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(40, 22);
            this.ultraLabel10.TabIndex = 68;
            this.ultraLabel10.Text = "Số HĐ";
            // 
            // ultraLabel11
            // 
            this.ultraLabel11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance101.BackColor = System.Drawing.Color.Transparent;
            appearance101.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance101;
            this.ultraLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel11.Location = new System.Drawing.Point(5, 57);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel11.TabIndex = 67;
            this.ultraLabel11.Text = "Ký hiệu HĐ";
            // 
            // ultraLabel12
            // 
            this.ultraLabel12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance102.BackColor = System.Drawing.Color.Transparent;
            appearance102.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance102;
            this.ultraLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel12.Location = new System.Drawing.Point(4, 29);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel12.TabIndex = 66;
            this.ultraLabel12.Text = "Mẫu số HĐ";
            // 
            // dteNgayHD2
            // 
            appearance103.TextHAlignAsString = "Center";
            appearance103.TextVAlignAsString = "Middle";
            this.dteNgayHD2.Appearance = appearance103;
            this.dteNgayHD2.AutoSize = false;
            this.dteNgayHD2.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteNgayHD2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dteNgayHD2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteNgayHD2.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteNgayHD2.Location = new System.Drawing.Point(233, 79);
            this.dteNgayHD2.MaskInput = "";
            this.dteNgayHD2.Name = "dteNgayHD2";
            this.dteNgayHD2.Size = new System.Drawing.Size(89, 22);
            this.dteNgayHD2.TabIndex = 19;
            this.dteNgayHD2.Value = null;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox3.Controls.Add(this.txtSecCKAccountingObjectBankName);
            this.ultraGroupBox3.Controls.Add(this.cbbSecCKAccountingObjectBankAccount);
            this.ultraGroupBox3.Controls.Add(this.label14);
            this.ultraGroupBox3.Controls.Add(this.txtSecCKAccountingObjectName);
            this.ultraGroupBox3.Controls.Add(this.cbbSecCKAccountingObjectID);
            this.ultraGroupBox3.Controls.Add(this.txtSecCKAccountingObjectAddress);
            this.ultraGroupBox3.Controls.Add(this.label15);
            this.ultraGroupBox3.Controls.Add(this.label16);
            this.ultraGroupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 75);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(618, 112);
            this.ultraGroupBox3.TabIndex = 3;
            this.ultraGroupBox3.Text = "Đối tượng nhận tiền";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtSecCKAccountingObjectBankName
            // 
            this.txtSecCKAccountingObjectBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecCKAccountingObjectBankName.AutoSize = false;
            this.txtSecCKAccountingObjectBankName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtSecCKAccountingObjectBankName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecCKAccountingObjectBankName.Location = new System.Drawing.Point(271, 77);
            this.txtSecCKAccountingObjectBankName.Name = "txtSecCKAccountingObjectBankName";
            this.txtSecCKAccountingObjectBankName.Size = new System.Drawing.Size(341, 22);
            this.txtSecCKAccountingObjectBankName.TabIndex = 15;
            // 
            // cbbSecCKAccountingObjectBankAccount
            // 
            this.cbbSecCKAccountingObjectBankAccount.AutoSize = false;
            appearance104.BackColor = System.Drawing.SystemColors.Window;
            appearance104.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Appearance = appearance104;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance105.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance105.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance105.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance105.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.GroupByBox.Appearance = appearance105;
            appearance106.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance106;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance107.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance107.BackColor2 = System.Drawing.SystemColors.Control;
            appearance107.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance107.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance107;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance108.BackColor = System.Drawing.SystemColors.Window;
            appearance108.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.ActiveCellAppearance = appearance108;
            appearance109.BackColor = System.Drawing.SystemColors.Highlight;
            appearance109.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.ActiveRowAppearance = appearance109;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance110.BackColor = System.Drawing.SystemColors.Window;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.CardAreaAppearance = appearance110;
            appearance111.BorderColor = System.Drawing.Color.Silver;
            appearance111.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.CellAppearance = appearance111;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.CellPadding = 0;
            appearance112.BackColor = System.Drawing.SystemColors.Control;
            appearance112.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance112.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance112.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance112.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.GroupByRowAppearance = appearance112;
            appearance113.TextHAlignAsString = "Left";
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.HeaderAppearance = appearance113;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance114.BackColor = System.Drawing.SystemColors.Window;
            appearance114.BorderColor = System.Drawing.Color.Silver;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.RowAppearance = appearance114;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance115.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance115;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbSecCKAccountingObjectBankAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbSecCKAccountingObjectBankAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbSecCKAccountingObjectBankAccount.Location = new System.Drawing.Point(90, 77);
            this.cbbSecCKAccountingObjectBankAccount.Name = "cbbSecCKAccountingObjectBankAccount";
            this.cbbSecCKAccountingObjectBankAccount.Size = new System.Drawing.Size(175, 22);
            this.cbbSecCKAccountingObjectBankAccount.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(11, 77);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 19);
            this.label14.TabIndex = 13;
            this.label14.Text = "Tài khoản";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSecCKAccountingObjectName
            // 
            this.txtSecCKAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecCKAccountingObjectName.AutoSize = false;
            this.txtSecCKAccountingObjectName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtSecCKAccountingObjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecCKAccountingObjectName.Location = new System.Drawing.Point(271, 28);
            this.txtSecCKAccountingObjectName.Name = "txtSecCKAccountingObjectName";
            this.txtSecCKAccountingObjectName.Size = new System.Drawing.Size(341, 22);
            this.txtSecCKAccountingObjectName.TabIndex = 12;
            // 
            // cbbSecCKAccountingObjectID
            // 
            this.cbbSecCKAccountingObjectID.AutoSize = false;
            appearance116.BackColor = System.Drawing.SystemColors.Window;
            appearance116.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Appearance = appearance116;
            this.cbbSecCKAccountingObjectID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbSecCKAccountingObjectID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance117.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance117.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance117.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance117.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSecCKAccountingObjectID.DisplayLayout.GroupByBox.Appearance = appearance117;
            appearance118.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecCKAccountingObjectID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance118;
            this.cbbSecCKAccountingObjectID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance119.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance119.BackColor2 = System.Drawing.SystemColors.Control;
            appearance119.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance119.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecCKAccountingObjectID.DisplayLayout.GroupByBox.PromptAppearance = appearance119;
            this.cbbSecCKAccountingObjectID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbSecCKAccountingObjectID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance120.BackColor = System.Drawing.SystemColors.Window;
            appearance120.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.ActiveCellAppearance = appearance120;
            appearance121.BackColor = System.Drawing.SystemColors.Highlight;
            appearance121.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.ActiveRowAppearance = appearance121;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance122.BackColor = System.Drawing.SystemColors.Window;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.CardAreaAppearance = appearance122;
            appearance123.BorderColor = System.Drawing.Color.Silver;
            appearance123.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.CellAppearance = appearance123;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.CellPadding = 0;
            appearance124.BackColor = System.Drawing.SystemColors.Control;
            appearance124.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance124.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance124.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance124.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.GroupByRowAppearance = appearance124;
            appearance125.TextHAlignAsString = "Left";
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.HeaderAppearance = appearance125;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance126.BackColor = System.Drawing.SystemColors.Window;
            appearance126.BorderColor = System.Drawing.Color.Silver;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.RowAppearance = appearance126;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance127.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.TemplateAddRowAppearance = appearance127;
            this.cbbSecCKAccountingObjectID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbSecCKAccountingObjectID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbSecCKAccountingObjectID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbSecCKAccountingObjectID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbSecCKAccountingObjectID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbSecCKAccountingObjectID.Location = new System.Drawing.Point(90, 28);
            this.cbbSecCKAccountingObjectID.Name = "cbbSecCKAccountingObjectID";
            this.cbbSecCKAccountingObjectID.Size = new System.Drawing.Size(175, 22);
            this.cbbSecCKAccountingObjectID.TabIndex = 11;
            // 
            // txtSecCKAccountingObjectAddress
            // 
            this.txtSecCKAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecCKAccountingObjectAddress.AutoSize = false;
            this.txtSecCKAccountingObjectAddress.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtSecCKAccountingObjectAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecCKAccountingObjectAddress.Location = new System.Drawing.Point(90, 52);
            this.txtSecCKAccountingObjectAddress.Name = "txtSecCKAccountingObjectAddress";
            this.txtSecCKAccountingObjectAddress.Size = new System.Drawing.Size(522, 22);
            this.txtSecCKAccountingObjectAddress.TabIndex = 10;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(11, 51);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 20);
            this.label15.TabIndex = 9;
            this.label15.Text = "Địa chỉ";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(11, 28);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 21);
            this.label16.TabIndex = 8;
            this.label16.Text = "Đối tượng";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.txtSecCKReason);
            this.ultraGroupBox4.Controls.Add(this.txtSecCKBankName);
            this.ultraGroupBox4.Controls.Add(this.cbbSecCKBankAccountDetailID);
            this.ultraGroupBox4.Controls.Add(this.label18);
            this.ultraGroupBox4.Controls.Add(this.label19);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(945, 75);
            this.ultraGroupBox4.TabIndex = 2;
            this.ultraGroupBox4.Text = "Đơn vị trả tiền";
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtSecCKReason
            // 
            this.txtSecCKReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecCKReason.AutoSize = false;
            this.txtSecCKReason.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtSecCKReason.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecCKReason.Location = new System.Drawing.Point(90, 42);
            this.txtSecCKReason.Name = "txtSecCKReason";
            this.txtSecCKReason.Size = new System.Drawing.Size(849, 22);
            this.txtSecCKReason.TabIndex = 4;
            // 
            // txtSecCKBankName
            // 
            this.txtSecCKBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecCKBankName.AutoSize = false;
            this.txtSecCKBankName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtSecCKBankName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecCKBankName.Location = new System.Drawing.Point(271, 18);
            this.txtSecCKBankName.Name = "txtSecCKBankName";
            this.txtSecCKBankName.Size = new System.Drawing.Size(668, 22);
            this.txtSecCKBankName.TabIndex = 3;
            // 
            // cbbSecCKBankAccountDetailID
            // 
            this.cbbSecCKBankAccountDetailID.AutoSize = false;
            appearance128.BackColor = System.Drawing.SystemColors.Window;
            appearance128.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Appearance = appearance128;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance129.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance129.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance129.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance129.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.GroupByBox.Appearance = appearance129;
            appearance130.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance130;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance131.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance131.BackColor2 = System.Drawing.SystemColors.Control;
            appearance131.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance131.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.GroupByBox.PromptAppearance = appearance131;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance132.BackColor = System.Drawing.SystemColors.Window;
            appearance132.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.ActiveCellAppearance = appearance132;
            appearance133.BackColor = System.Drawing.SystemColors.Highlight;
            appearance133.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.ActiveRowAppearance = appearance133;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance134.BackColor = System.Drawing.SystemColors.Window;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.CardAreaAppearance = appearance134;
            appearance135.BorderColor = System.Drawing.Color.Silver;
            appearance135.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.CellAppearance = appearance135;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.CellPadding = 0;
            appearance136.BackColor = System.Drawing.SystemColors.Control;
            appearance136.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance136.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance136.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance136.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.GroupByRowAppearance = appearance136;
            appearance137.TextHAlignAsString = "Left";
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.HeaderAppearance = appearance137;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance138.BackColor = System.Drawing.SystemColors.Window;
            appearance138.BorderColor = System.Drawing.Color.Silver;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.RowAppearance = appearance138;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance139.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.TemplateAddRowAppearance = appearance139;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbSecCKBankAccountDetailID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbSecCKBankAccountDetailID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbSecCKBankAccountDetailID.Location = new System.Drawing.Point(90, 18);
            this.cbbSecCKBankAccountDetailID.Name = "cbbSecCKBankAccountDetailID";
            this.cbbSecCKBankAccountDetailID.Size = new System.Drawing.Size(175, 22);
            this.cbbSecCKBankAccountDetailID.TabIndex = 2;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(11, 42);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 21);
            this.label18.TabIndex = 1;
            this.label18.Text = "Nội dung TT";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(11, 18);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(73, 21);
            this.label19.TabIndex = 0;
            this.label19.Text = "Tài khoản";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnMBTellerPaper_PayOrder
            // 
            this.pnMBTellerPaper_PayOrder.BackColor = System.Drawing.Color.Transparent;
            this.pnMBTellerPaper_PayOrder.Controls.Add(this.grHoaDon1);
            this.pnMBTellerPaper_PayOrder.Controls.Add(this.gpAccountingObject);
            this.pnMBTellerPaper_PayOrder.Controls.Add(this.gpPaymentOrder);
            this.pnMBTellerPaper_PayOrder.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnMBTellerPaper_PayOrder.Location = new System.Drawing.Point(0, 135);
            this.pnMBTellerPaper_PayOrder.Name = "pnMBTellerPaper_PayOrder";
            this.pnMBTellerPaper_PayOrder.Size = new System.Drawing.Size(945, 183);
            this.pnMBTellerPaper_PayOrder.TabIndex = 19;
            this.pnMBTellerPaper_PayOrder.Visible = false;
            // 
            // grHoaDon1
            // 
            this.grHoaDon1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grHoaDon1.Controls.Add(this.txtMauSoHD1);
            this.grHoaDon1.Controls.Add(this.txtSoHD1);
            this.grHoaDon1.Controls.Add(this.txtKyHieuHD1);
            this.grHoaDon1.Controls.Add(this.ultraLabel5);
            this.grHoaDon1.Controls.Add(this.ultraLabel6);
            this.grHoaDon1.Controls.Add(this.ultraLabel7);
            this.grHoaDon1.Controls.Add(this.ultraLabel8);
            this.grHoaDon1.Controls.Add(this.dteNgayHD1);
            this.grHoaDon1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grHoaDon1.Location = new System.Drawing.Point(618, 72);
            this.grHoaDon1.Name = "grHoaDon1";
            this.grHoaDon1.Size = new System.Drawing.Size(327, 111);
            this.grHoaDon1.TabIndex = 32;
            this.grHoaDon1.Text = "Hóa đơn";
            this.grHoaDon1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtMauSoHD1
            // 
            this.txtMauSoHD1.AutoSize = false;
            this.txtMauSoHD1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMauSoHD1.Location = new System.Drawing.Point(72, 26);
            this.txtMauSoHD1.Name = "txtMauSoHD1";
            this.txtMauSoHD1.Size = new System.Drawing.Size(250, 22);
            this.txtMauSoHD1.TabIndex = 76;
            // 
            // txtSoHD1
            // 
            this.txtSoHD1.AutoSize = false;
            this.txtSoHD1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHD1.Location = new System.Drawing.Point(72, 78);
            this.txtSoHD1.Name = "txtSoHD1";
            this.txtSoHD1.Size = new System.Drawing.Size(100, 22);
            this.txtSoHD1.TabIndex = 18;
            // 
            // txtKyHieuHD1
            // 
            this.txtKyHieuHD1.AutoSize = false;
            this.txtKyHieuHD1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuHD1.Location = new System.Drawing.Point(72, 52);
            this.txtKyHieuHD1.Name = "txtKyHieuHD1";
            this.txtKyHieuHD1.Size = new System.Drawing.Size(250, 22);
            this.txtKyHieuHD1.TabIndex = 17;
            // 
            // ultraLabel5
            // 
            this.ultraLabel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance140.BackColor = System.Drawing.Color.Transparent;
            appearance140.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance140;
            this.ultraLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel5.Location = new System.Drawing.Point(178, 79);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel5.TabIndex = 69;
            this.ultraLabel5.Text = "Ngày HĐ";
            // 
            // ultraLabel6
            // 
            this.ultraLabel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance141.BackColor = System.Drawing.Color.Transparent;
            appearance141.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance141;
            this.ultraLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel6.Location = new System.Drawing.Point(5, 82);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(40, 22);
            this.ultraLabel6.TabIndex = 68;
            this.ultraLabel6.Text = "Số HĐ";
            // 
            // ultraLabel7
            // 
            this.ultraLabel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance142.BackColor = System.Drawing.Color.Transparent;
            appearance142.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance142;
            this.ultraLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel7.Location = new System.Drawing.Point(5, 57);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel7.TabIndex = 67;
            this.ultraLabel7.Text = "Ký hiệu HĐ";
            // 
            // ultraLabel8
            // 
            this.ultraLabel8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance143.BackColor = System.Drawing.Color.Transparent;
            appearance143.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance143;
            this.ultraLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel8.Location = new System.Drawing.Point(4, 29);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel8.TabIndex = 66;
            this.ultraLabel8.Text = "Mẫu số HĐ";
            // 
            // dteNgayHD1
            // 
            appearance144.TextHAlignAsString = "Center";
            appearance144.TextVAlignAsString = "Middle";
            this.dteNgayHD1.Appearance = appearance144;
            this.dteNgayHD1.AutoSize = false;
            this.dteNgayHD1.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteNgayHD1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dteNgayHD1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteNgayHD1.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteNgayHD1.Location = new System.Drawing.Point(233, 79);
            this.dteNgayHD1.MaskInput = "";
            this.dteNgayHD1.Name = "dteNgayHD1";
            this.dteNgayHD1.Size = new System.Drawing.Size(89, 22);
            this.dteNgayHD1.TabIndex = 19;
            this.dteNgayHD1.Value = null;
            // 
            // gpAccountingObject
            // 
            this.gpAccountingObject.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpAccountingObject.Controls.Add(this.txtAccreditativeAccountingObjectBankName);
            this.gpAccountingObject.Controls.Add(this.cbbAccreditativeAccountingObjectBankAccount);
            this.gpAccountingObject.Controls.Add(this.label13);
            this.gpAccountingObject.Controls.Add(this.txtAccreditativeAccountingObjectName);
            this.gpAccountingObject.Controls.Add(this.cbbAccreditativeAccountingObjectID);
            this.gpAccountingObject.Controls.Add(this.txtAccreditativeAccountingObjectAddress);
            this.gpAccountingObject.Controls.Add(this.label17);
            this.gpAccountingObject.Controls.Add(this.label12);
            this.gpAccountingObject.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpAccountingObject.Location = new System.Drawing.Point(0, 72);
            this.gpAccountingObject.Name = "gpAccountingObject";
            this.gpAccountingObject.Size = new System.Drawing.Size(618, 111);
            this.gpAccountingObject.TabIndex = 1;
            this.gpAccountingObject.Text = "Đối tượng nhận tiền";
            this.gpAccountingObject.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtAccreditativeAccountingObjectBankName
            // 
            this.txtAccreditativeAccountingObjectBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccreditativeAccountingObjectBankName.AutoSize = false;
            this.txtAccreditativeAccountingObjectBankName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccreditativeAccountingObjectBankName.Location = new System.Drawing.Point(271, 77);
            this.txtAccreditativeAccountingObjectBankName.Name = "txtAccreditativeAccountingObjectBankName";
            this.txtAccreditativeAccountingObjectBankName.Size = new System.Drawing.Size(341, 22);
            this.txtAccreditativeAccountingObjectBankName.TabIndex = 15;
            // 
            // cbbAccreditativeAccountingObjectBankAccount
            // 
            this.cbbAccreditativeAccountingObjectBankAccount.AutoSize = false;
            appearance145.BackColor = System.Drawing.SystemColors.Window;
            appearance145.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Appearance = appearance145;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance146.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance146.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance146.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance146.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.GroupByBox.Appearance = appearance146;
            appearance147.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance147;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance148.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance148.BackColor2 = System.Drawing.SystemColors.Control;
            appearance148.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance148.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance148;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance149.BackColor = System.Drawing.SystemColors.Window;
            appearance149.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.ActiveCellAppearance = appearance149;
            appearance150.BackColor = System.Drawing.SystemColors.Highlight;
            appearance150.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.ActiveRowAppearance = appearance150;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance151.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.CardAreaAppearance = appearance151;
            appearance152.BorderColor = System.Drawing.Color.Silver;
            appearance152.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.CellAppearance = appearance152;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.CellPadding = 0;
            appearance153.BackColor = System.Drawing.SystemColors.Control;
            appearance153.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance153.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance153.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance153.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.GroupByRowAppearance = appearance153;
            appearance154.TextHAlignAsString = "Left";
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.HeaderAppearance = appearance154;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance155.BackColor = System.Drawing.SystemColors.Window;
            appearance155.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.RowAppearance = appearance155;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance156.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance156;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccreditativeAccountingObjectBankAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbAccreditativeAccountingObjectBankAccount.Location = new System.Drawing.Point(90, 77);
            this.cbbAccreditativeAccountingObjectBankAccount.Name = "cbbAccreditativeAccountingObjectBankAccount";
            this.cbbAccreditativeAccountingObjectBankAccount.Size = new System.Drawing.Size(175, 22);
            this.cbbAccreditativeAccountingObjectBankAccount.TabIndex = 14;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(15, 83);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "Tài khoản";
            // 
            // txtAccreditativeAccountingObjectName
            // 
            this.txtAccreditativeAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccreditativeAccountingObjectName.AutoSize = false;
            this.txtAccreditativeAccountingObjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccreditativeAccountingObjectName.Location = new System.Drawing.Point(271, 27);
            this.txtAccreditativeAccountingObjectName.Name = "txtAccreditativeAccountingObjectName";
            this.txtAccreditativeAccountingObjectName.Size = new System.Drawing.Size(341, 22);
            this.txtAccreditativeAccountingObjectName.TabIndex = 12;
            // 
            // cbbAccreditativeAccountingObjectID
            // 
            this.cbbAccreditativeAccountingObjectID.AutoSize = false;
            appearance157.BackColor = System.Drawing.SystemColors.Window;
            appearance157.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Appearance = appearance157;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance158.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance158.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance158.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance158.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.GroupByBox.Appearance = appearance158;
            appearance159.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance159;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance160.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance160.BackColor2 = System.Drawing.SystemColors.Control;
            appearance160.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance160.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.GroupByBox.PromptAppearance = appearance160;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance161.BackColor = System.Drawing.SystemColors.Window;
            appearance161.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.ActiveCellAppearance = appearance161;
            appearance162.BackColor = System.Drawing.SystemColors.Highlight;
            appearance162.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.ActiveRowAppearance = appearance162;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance163.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.CardAreaAppearance = appearance163;
            appearance164.BorderColor = System.Drawing.Color.Silver;
            appearance164.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.CellAppearance = appearance164;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.CellPadding = 0;
            appearance165.BackColor = System.Drawing.SystemColors.Control;
            appearance165.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance165.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance165.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance165.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.GroupByRowAppearance = appearance165;
            appearance166.TextHAlignAsString = "Left";
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.HeaderAppearance = appearance166;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance167.BackColor = System.Drawing.SystemColors.Window;
            appearance167.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.RowAppearance = appearance167;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance168.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.TemplateAddRowAppearance = appearance168;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccreditativeAccountingObjectID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbAccreditativeAccountingObjectID.Location = new System.Drawing.Point(90, 27);
            this.cbbAccreditativeAccountingObjectID.Name = "cbbAccreditativeAccountingObjectID";
            this.cbbAccreditativeAccountingObjectID.Size = new System.Drawing.Size(175, 22);
            this.cbbAccreditativeAccountingObjectID.TabIndex = 11;
            // 
            // txtAccreditativeAccountingObjectAddress
            // 
            this.txtAccreditativeAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccreditativeAccountingObjectAddress.AutoSize = false;
            this.txtAccreditativeAccountingObjectAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccreditativeAccountingObjectAddress.Location = new System.Drawing.Point(90, 52);
            this.txtAccreditativeAccountingObjectAddress.Name = "txtAccreditativeAccountingObjectAddress";
            this.txtAccreditativeAccountingObjectAddress.Size = new System.Drawing.Size(522, 22);
            this.txtAccreditativeAccountingObjectAddress.TabIndex = 10;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(15, 58);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(40, 13);
            this.label17.TabIndex = 9;
            this.label17.Text = "Địa chỉ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(15, 34);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Đối tượng";
            // 
            // gpPaymentOrder
            // 
            this.gpPaymentOrder.Controls.Add(this.txtPaymentOrderReason);
            this.gpPaymentOrder.Controls.Add(this.txtPaymentOrderBankName);
            this.gpPaymentOrder.Controls.Add(this.cbbPaymentOrderBankAccountDetailID);
            this.gpPaymentOrder.Controls.Add(this.label11);
            this.gpPaymentOrder.Controls.Add(this.label10);
            this.gpPaymentOrder.Dock = System.Windows.Forms.DockStyle.Top;
            this.gpPaymentOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpPaymentOrder.Location = new System.Drawing.Point(0, 0);
            this.gpPaymentOrder.Name = "gpPaymentOrder";
            this.gpPaymentOrder.Size = new System.Drawing.Size(945, 72);
            this.gpPaymentOrder.TabIndex = 0;
            this.gpPaymentOrder.Text = "Đơn vị trả tiền";
            this.gpPaymentOrder.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtPaymentOrderReason
            // 
            this.txtPaymentOrderReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPaymentOrderReason.AutoSize = false;
            this.txtPaymentOrderReason.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaymentOrderReason.Location = new System.Drawing.Point(90, 42);
            this.txtPaymentOrderReason.Name = "txtPaymentOrderReason";
            this.txtPaymentOrderReason.Size = new System.Drawing.Size(849, 22);
            this.txtPaymentOrderReason.TabIndex = 4;
            // 
            // txtPaymentOrderBankName
            // 
            this.txtPaymentOrderBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPaymentOrderBankName.AutoSize = false;
            this.txtPaymentOrderBankName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaymentOrderBankName.Location = new System.Drawing.Point(271, 17);
            this.txtPaymentOrderBankName.Name = "txtPaymentOrderBankName";
            this.txtPaymentOrderBankName.Size = new System.Drawing.Size(668, 22);
            this.txtPaymentOrderBankName.TabIndex = 3;
            this.txtPaymentOrderBankName.ValueChanged += new System.EventHandler(this.txtPaymentOrderBankName_ValueChanged);
            // 
            // cbbPaymentOrderBankAccountDetailID
            // 
            this.cbbPaymentOrderBankAccountDetailID.AutoSize = false;
            appearance169.BackColor = System.Drawing.SystemColors.Window;
            appearance169.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Appearance = appearance169;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance170.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance170.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance170.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance170.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.GroupByBox.Appearance = appearance170;
            appearance171.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance171;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance172.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance172.BackColor2 = System.Drawing.SystemColors.Control;
            appearance172.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance172.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.GroupByBox.PromptAppearance = appearance172;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance173.BackColor = System.Drawing.SystemColors.Window;
            appearance173.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.ActiveCellAppearance = appearance173;
            appearance174.BackColor = System.Drawing.SystemColors.Highlight;
            appearance174.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.ActiveRowAppearance = appearance174;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance175.BackColor = System.Drawing.SystemColors.Window;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.CardAreaAppearance = appearance175;
            appearance176.BorderColor = System.Drawing.Color.Silver;
            appearance176.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.CellAppearance = appearance176;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.CellPadding = 0;
            appearance177.BackColor = System.Drawing.SystemColors.Control;
            appearance177.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance177.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance177.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance177.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.GroupByRowAppearance = appearance177;
            appearance178.TextHAlignAsString = "Left";
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.HeaderAppearance = appearance178;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance179.BackColor = System.Drawing.SystemColors.Window;
            appearance179.BorderColor = System.Drawing.Color.Silver;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.RowAppearance = appearance179;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance180.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.TemplateAddRowAppearance = appearance180;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbPaymentOrderBankAccountDetailID.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbPaymentOrderBankAccountDetailID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPaymentOrderBankAccountDetailID.Location = new System.Drawing.Point(90, 17);
            this.cbbPaymentOrderBankAccountDetailID.Name = "cbbPaymentOrderBankAccountDetailID";
            this.cbbPaymentOrderBankAccountDetailID.Size = new System.Drawing.Size(175, 22);
            this.cbbPaymentOrderBankAccountDetailID.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(15, 44);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Nội dung TT";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(15, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Tài khoản";
            // 
            // pnNoPayment
            // 
            // 
            // pnNoPayment.ClientArea
            // 
            this.pnNoPayment.ClientArea.Controls.Add(this.grHoaDon);
            this.pnNoPayment.ClientArea.Controls.Add(this.ugbThongTinChung);
            this.pnNoPayment.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnNoPayment.Location = new System.Drawing.Point(0, 0);
            this.pnNoPayment.Name = "pnNoPayment";
            this.pnNoPayment.Size = new System.Drawing.Size(945, 135);
            this.pnNoPayment.TabIndex = 18;
            // 
            // grHoaDon
            // 
            this.grHoaDon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grHoaDon.Controls.Add(this.txtMauSoHD);
            this.grHoaDon.Controls.Add(this.txtSoHD);
            this.grHoaDon.Controls.Add(this.txtKyHieuHD);
            this.grHoaDon.Controls.Add(this.ultraLabel37);
            this.grHoaDon.Controls.Add(this.ultraLabel38);
            this.grHoaDon.Controls.Add(this.ultraLabel39);
            this.grHoaDon.Controls.Add(this.ultraLabel40);
            this.grHoaDon.Controls.Add(this.dteNgayHD);
            this.grHoaDon.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grHoaDon.Location = new System.Drawing.Point(655, 0);
            this.grHoaDon.Name = "grHoaDon";
            this.grHoaDon.Size = new System.Drawing.Size(290, 141);
            this.grHoaDon.TabIndex = 31;
            this.grHoaDon.Text = "Hóa đơn";
            this.grHoaDon.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtMauSoHD
            // 
            this.txtMauSoHD.AutoSize = false;
            this.txtMauSoHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMauSoHD.Location = new System.Drawing.Point(81, 21);
            this.txtMauSoHD.Name = "txtMauSoHD";
            this.txtMauSoHD.Size = new System.Drawing.Size(203, 22);
            this.txtMauSoHD.TabIndex = 76;
            this.txtMauSoHD.TextChanged += new System.EventHandler(this.txtMauSoHD_TextChanged);
            // 
            // txtSoHD
            // 
            this.txtSoHD.AutoSize = false;
            this.txtSoHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHD.Location = new System.Drawing.Point(81, 72);
            this.txtSoHD.Name = "txtSoHD";
            this.txtSoHD.Size = new System.Drawing.Size(203, 22);
            this.txtSoHD.TabIndex = 18;
            this.txtSoHD.TextChanged += new System.EventHandler(this.txtSoHD_TextChanged);
            // 
            // txtKyHieuHD
            // 
            this.txtKyHieuHD.AutoSize = false;
            this.txtKyHieuHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuHD.Location = new System.Drawing.Point(81, 46);
            this.txtKyHieuHD.Name = "txtKyHieuHD";
            this.txtKyHieuHD.Size = new System.Drawing.Size(203, 22);
            this.txtKyHieuHD.TabIndex = 17;
            this.txtKyHieuHD.TextChanged += new System.EventHandler(this.txtKyHieuHD_TextChanged);
            // 
            // ultraLabel37
            // 
            this.ultraLabel37.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance181.BackColor = System.Drawing.Color.Transparent;
            appearance181.TextVAlignAsString = "Middle";
            this.ultraLabel37.Appearance = appearance181;
            this.ultraLabel37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel37.Location = new System.Drawing.Point(11, 99);
            this.ultraLabel37.Name = "ultraLabel37";
            this.ultraLabel37.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel37.TabIndex = 69;
            this.ultraLabel37.Text = "Ngày HĐ";
            // 
            // ultraLabel38
            // 
            this.ultraLabel38.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance182.BackColor = System.Drawing.Color.Transparent;
            appearance182.TextVAlignAsString = "Middle";
            this.ultraLabel38.Appearance = appearance182;
            this.ultraLabel38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel38.Location = new System.Drawing.Point(9, 73);
            this.ultraLabel38.Name = "ultraLabel38";
            this.ultraLabel38.Size = new System.Drawing.Size(40, 22);
            this.ultraLabel38.TabIndex = 68;
            this.ultraLabel38.Text = "Số HĐ";
            // 
            // ultraLabel39
            // 
            this.ultraLabel39.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance183.BackColor = System.Drawing.Color.Transparent;
            appearance183.TextVAlignAsString = "Middle";
            this.ultraLabel39.Appearance = appearance183;
            this.ultraLabel39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel39.Location = new System.Drawing.Point(9, 48);
            this.ultraLabel39.Name = "ultraLabel39";
            this.ultraLabel39.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel39.TabIndex = 67;
            this.ultraLabel39.Text = "Ký hiệu HĐ";
            // 
            // ultraLabel40
            // 
            this.ultraLabel40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance184.BackColor = System.Drawing.Color.Transparent;
            appearance184.TextVAlignAsString = "Middle";
            this.ultraLabel40.Appearance = appearance184;
            this.ultraLabel40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel40.Location = new System.Drawing.Point(11, 24);
            this.ultraLabel40.Name = "ultraLabel40";
            this.ultraLabel40.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel40.TabIndex = 66;
            this.ultraLabel40.Text = "Mẫu số HĐ";
            // 
            // dteNgayHD
            // 
            appearance185.TextHAlignAsString = "Center";
            appearance185.TextVAlignAsString = "Middle";
            this.dteNgayHD.Appearance = appearance185;
            this.dteNgayHD.AutoSize = false;
            this.dteNgayHD.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteNgayHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dteNgayHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteNgayHD.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteNgayHD.Location = new System.Drawing.Point(81, 99);
            this.dteNgayHD.MaskInput = "";
            this.dteNgayHD.Name = "dteNgayHD";
            this.dteNgayHD.Size = new System.Drawing.Size(100, 22);
            this.dteNgayHD.TabIndex = 19;
            this.dteNgayHD.Value = null;
            this.dteNgayHD.ValueChanged += new System.EventHandler(this.dteNgayHD_ValueChanged);
            // 
            // ugbThongTinChung
            // 
            this.ugbThongTinChung.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ugbThongTinChung.Controls.Add(this.txtAccountingObjectName);
            this.ugbThongTinChung.Controls.Add(this.cbbAccountingObjectID);
            this.ugbThongTinChung.Controls.Add(this.txtReason);
            this.ugbThongTinChung.Controls.Add(this.lblDescription);
            this.ugbThongTinChung.Controls.Add(this.txtAccountingObjectAddress);
            this.ugbThongTinChung.Controls.Add(this.lblAccountingObjectAddress);
            this.ugbThongTinChung.Controls.Add(this.lblAccountingObjectID);
            appearance190.FontData.BoldAsString = "True";
            appearance190.FontData.SizeInPoints = 10F;
            this.ugbThongTinChung.HeaderAppearance = appearance190;
            this.ugbThongTinChung.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ugbThongTinChung.Location = new System.Drawing.Point(0, 1);
            this.ugbThongTinChung.Name = "ugbThongTinChung";
            this.ugbThongTinChung.Size = new System.Drawing.Size(655, 141);
            this.ugbThongTinChung.TabIndex = 27;
            this.ugbThongTinChung.Text = "Thông tin chung";
            this.ugbThongTinChung.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtAccountingObjectName
            // 
            this.txtAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectName.AutoSize = false;
            this.txtAccountingObjectName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectName.Location = new System.Drawing.Point(287, 26);
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(362, 22);
            this.txtAccountingObjectName.TabIndex = 3;
            // 
            // cbbAccountingObjectID
            // 
            this.cbbAccountingObjectID.AutoSize = false;
            appearance186.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton2.Appearance = appearance186;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectID.ButtonsRight.Add(editorButton2);
            this.cbbAccountingObjectID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjectID.Location = new System.Drawing.Point(85, 26);
            this.cbbAccountingObjectID.Name = "cbbAccountingObjectID";
            this.cbbAccountingObjectID.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID.Size = new System.Drawing.Size(197, 22);
            this.cbbAccountingObjectID.TabIndex = 2;
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason.AutoSize = false;
            this.txtReason.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReason.Location = new System.Drawing.Point(85, 72);
            this.txtReason.Multiline = true;
            this.txtReason.Name = "txtReason";
            this.txtReason.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtReason.Size = new System.Drawing.Size(564, 58);
            this.txtReason.TabIndex = 5;
            // 
            // lblDescription
            // 
            appearance187.BackColor = System.Drawing.Color.Transparent;
            appearance187.TextHAlignAsString = "Left";
            appearance187.TextVAlignAsString = "Middle";
            this.lblDescription.Appearance = appearance187;
            this.lblDescription.Location = new System.Drawing.Point(11, 71);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(68, 19);
            this.lblDescription.TabIndex = 24;
            this.lblDescription.Text = "&Diễn giải";
            // 
            // txtAccountingObjectAddress
            // 
            this.txtAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddress.AutoSize = false;
            this.txtAccountingObjectAddress.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectAddress.Location = new System.Drawing.Point(85, 49);
            this.txtAccountingObjectAddress.Name = "txtAccountingObjectAddress";
            this.txtAccountingObjectAddress.Size = new System.Drawing.Size(564, 22);
            this.txtAccountingObjectAddress.TabIndex = 4;
            // 
            // lblAccountingObjectAddress
            // 
            appearance188.BackColor = System.Drawing.Color.Transparent;
            appearance188.TextHAlignAsString = "Left";
            appearance188.TextVAlignAsString = "Middle";
            this.lblAccountingObjectAddress.Appearance = appearance188;
            this.lblAccountingObjectAddress.Location = new System.Drawing.Point(11, 48);
            this.lblAccountingObjectAddress.Name = "lblAccountingObjectAddress";
            this.lblAccountingObjectAddress.Size = new System.Drawing.Size(68, 19);
            this.lblAccountingObjectAddress.TabIndex = 22;
            this.lblAccountingObjectAddress.Text = "Địa chỉ";
            // 
            // lblAccountingObjectID
            // 
            appearance189.BackColor = System.Drawing.Color.Transparent;
            appearance189.TextHAlignAsString = "Left";
            appearance189.TextVAlignAsString = "Middle";
            this.lblAccountingObjectID.Appearance = appearance189;
            this.lblAccountingObjectID.Location = new System.Drawing.Point(11, 25);
            this.lblAccountingObjectID.Name = "lblAccountingObjectID";
            this.lblAccountingObjectID.Size = new System.Drawing.Size(68, 19);
            this.lblAccountingObjectID.TabIndex = 0;
            this.lblAccountingObjectID.Text = "Đối tượng (*)";
            // 
            // FPPServiceDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 1061);
            this.Controls.Add(this.palFill);
            this.Controls.Add(this.palBottom);
            this.Controls.Add(this.palTop);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FPPServiceDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FPPServiceDetail_FormClosing);
            this.palBottom.ResumeLayout(false);
            this.palBottom.PerformLayout();
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkBillReceived)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsFeightService)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ultraPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            this.palChung.ResumeLayout(false);
            this.palChung.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optThanhToan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChonThanhToan)).EndInit();
            this.palTopVouchers.ResumeLayout(false);
            this.palTopVouchers.PerformLayout();
            this.palTop.ClientArea.ResumeLayout(false);
            this.palTop.ResumeLayout(false);
            this.palFill.ClientArea.ResumeLayout(false);
            this.palFill.ResumeLayout(false);
            this.palGrid.ClientArea.ResumeLayout(false);
            this.palGrid.ResumeLayout(false);
            this.palThongtinchung.ResumeLayout(false);
            this.paneTTChungNew.ClientArea.ResumeLayout(false);
            this.paneTTChungNew.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDonNew)).EndInit();
            this.grHoaDonNew.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHDNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHDNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHDNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHDNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpMCPayment)).EndInit();
            this.gpMCPayment.ResumeLayout(false);
            this.gpMCPayment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccreditativeAttach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPaymentAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentAccountingObjectAddress)).EndInit();
            this.pnMBTellerPaper_Sec.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDon4)).EndInit();
            this.grHoaDon4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHD4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).EndInit();
            this.ultraGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtSecIssueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecIdentificationNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecIssueBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecReceiver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox8)).EndInit();
            this.ultraGroupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSecReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecBankAccountDetailID)).EndInit();
            this.pnMBCreditCard.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDon3)).EndInit();
            this.grHoaDon3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHD3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.ultraGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditCardAccountingObjectBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardAccountingObjectBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditCardAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditCardAccountingObjectAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).EndInit();
            this.ultraGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditCardReason)).EndInit();
            this.pnMBTellerPaper_SecCK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDon2)).EndInit();
            this.grHoaDon2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHD2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKAccountingObjectBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecCKAccountingObjectBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecCKAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKAccountingObjectAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecCKBankAccountDetailID)).EndInit();
            this.pnMBTellerPaper_PayOrder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDon1)).EndInit();
            this.grHoaDon1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHD1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpAccountingObject)).EndInit();
            this.gpAccountingObject.ResumeLayout(false);
            this.gpAccountingObject.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccreditativeAccountingObjectBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccreditativeAccountingObjectBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccreditativeAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccreditativeAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccreditativeAccountingObjectAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpPaymentOrder)).EndInit();
            this.gpPaymentOrder.ResumeLayout(false);
            this.gpPaymentOrder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentOrderReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentOrderBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPaymentOrderBankAccountDetailID)).EndInit();
            this.pnNoPayment.ClientArea.ResumeLayout(false);
            this.pnNoPayment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDon)).EndInit();
            this.grHoaDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugbThongTinChung)).EndInit();
            this.ugbThongTinChung.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private System.Windows.Forms.Panel palBottom;
        private Infragistics.Win.UltraWinToolTip.UltraToolTipManager ultraToolTipManager1;
        private Infragistics.Win.Misc.UltraPanel palTop;
        private Infragistics.Win.Misc.UltraPanel palFill;
        private Infragistics.Win.Misc.UltraPanel palGrid;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridControl;
        private Infragistics.Win.Misc.UltraLabel lbl3;
        private Infragistics.Win.Misc.UltraLabel lbl2;
        private Infragistics.Win.Misc.UltraLabel lbl1;
        private Infragistics.Win.Misc.UltraPanel palTopVouchers;
        private System.Windows.Forms.Panel palThongtinchung;
        private System.Windows.Forms.Panel palChung;
        private UltraOptionSet_Ex optThanhToan;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbChonThanhToan;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraLabel lblTotalAll;
        private Infragistics.Win.Misc.UltraLabel lblTotalAllOriginal;
        private Infragistics.Win.Misc.UltraLabel lblTotalVATAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalVATAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel lblTotalDiscountAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalDiscountAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel lblTotalAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel lbl0;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsFeightService;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
        private Infragistics.Win.Misc.UltraLabel lblBillReceived;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkBillReceived;
        private Infragistics.Win.Misc.UltraPanel pnNoPayment;
        private Infragistics.Win.Misc.UltraGroupBox grHoaDon;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMauSoHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSoHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtKyHieuHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel37;
        private Infragistics.Win.Misc.UltraLabel ultraLabel38;
        private Infragistics.Win.Misc.UltraLabel ultraLabel39;
        private Infragistics.Win.Misc.UltraLabel ultraLabel40;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteNgayHD;
        private Infragistics.Win.Misc.UltraGroupBox ugbThongTinChung;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.Misc.UltraLabel lblDescription;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectID;
        private System.Windows.Forms.Panel pnMBTellerPaper_PayOrder;
        private Infragistics.Win.Misc.UltraGroupBox gpAccountingObject;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccreditativeAccountingObjectBankName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccreditativeAccountingObjectBankAccount;
        private System.Windows.Forms.Label label13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccreditativeAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccreditativeAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccreditativeAccountingObjectAddress;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label12;
        private Infragistics.Win.Misc.UltraGroupBox gpPaymentOrder;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPaymentOrderReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPaymentOrderBankName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbPaymentOrderBankAccountDetailID;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel pnMBTellerPaper_SecCK;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecCKAccountingObjectBankName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbSecCKAccountingObjectBankAccount;
        private System.Windows.Forms.Label label14;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecCKAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbSecCKAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecCKAccountingObjectAddress;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecCKReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecCKBankName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbSecCKBankAccountDetailID;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel pnMBCreditCard;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCreditCardAccountingObjectBankName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCreditCardAccountingObjectBankAccount;
        private System.Windows.Forms.Label label27;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCreditCardAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCreditCardAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCreditCardAccountingObjectAddress;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox6;
        private Infragistics.Win.Misc.UltraLabel txtCreditCardType;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox picCreditCardType;
        private System.Windows.Forms.Label txtOwnerCard;
        private Infragistics.Win.Misc.UltraLabel lblOwnerCard;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCreditCardNumber;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCreditCardReason;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel pnMBTellerPaper_Sec;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox7;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtSecIssueDate;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecIdentificationNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecIssueBy;
        private System.Windows.Forms.Label label20;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbSecAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecReceiver;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecBankName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbSecBankAccountDetailID;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private Infragistics.Win.Misc.UltraPanel paneTTChungNew;
        private Infragistics.Win.Misc.UltraGroupBox grHoaDonNew;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMauSoHDNew;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSoHDNew;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtKyHieuHDNew;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteNgayHDNew;
        private Infragistics.Win.Misc.UltraGroupBox gpMCPayment;
        private System.Windows.Forms.Label label9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccreditativeAttach;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPaymentReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReceiver;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPaymentAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbPaymentAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPaymentAccountingObjectAddress;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label D;
        private Infragistics.Win.Misc.UltraGroupBox grHoaDon1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMauSoHD1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSoHD1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtKyHieuHD1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteNgayHD1;
        private Infragistics.Win.Misc.UltraGroupBox grHoaDon4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMauSoHD4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSoHD4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtKyHieuHD4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteNgayHD4;
        private Infragistics.Win.Misc.UltraGroupBox grHoaDon3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMauSoHD3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSoHD3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtKyHieuHD3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteNgayHD3;
        private Infragistics.Win.Misc.UltraGroupBox grHoaDon2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMauSoHD2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSoHD2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtKyHieuHD2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteNgayHD2;
    }
}
