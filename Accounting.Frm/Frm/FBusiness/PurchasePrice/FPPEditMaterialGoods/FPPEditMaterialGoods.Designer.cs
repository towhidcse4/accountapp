﻿namespace Accounting
{
    partial class FPPEditMaterialGoods
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem11 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem12 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem9 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem10 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraMaskedEdit1 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.uGridMaterialGoods = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBoxTop = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtAugmentMoney = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtRate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.chkRate = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.UltraGruopBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.chkType = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.chkGiaBan1 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkGiaBan2 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkGiaBan3 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.btnHoan = new Infragistics.Win.Misc.UltraButton();
            this.btnDong = new Infragistics.Win.Misc.UltraButton();
            this.btnCat = new Infragistics.Win.Misc.UltraButton();
            this.btnTinhGiaBan = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMaterialGoods)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxTop)).BeginInit();
            this.ultraGroupBoxTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAugmentMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UltraGruopBox1)).BeginInit();
            this.UltraGruopBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkGiaBan1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGiaBan2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGiaBan3)).BeginInit();
            this.ultraPanel3.ClientArea.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraMaskedEdit1);
            this.ultraPanel1.ClientArea.Controls.Add(this.uGridMaterialGoods);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(1038, 253);
            this.ultraPanel1.TabIndex = 0;
            // 
            // ultraMaskedEdit1
            // 
            this.ultraMaskedEdit1.Location = new System.Drawing.Point(349, 246);
            this.ultraMaskedEdit1.Name = "ultraMaskedEdit1";
            this.ultraMaskedEdit1.Size = new System.Drawing.Size(100, 20);
            this.ultraMaskedEdit1.TabIndex = 43;
            this.ultraMaskedEdit1.Text = "ultraMaskedEdit1";
            // 
            // uGridMaterialGoods
            // 
            this.uGridMaterialGoods.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridMaterialGoods.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGridMaterialGoods.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridMaterialGoods.Location = new System.Drawing.Point(0, 0);
            this.uGridMaterialGoods.Name = "uGridMaterialGoods";
            this.uGridMaterialGoods.Size = new System.Drawing.Size(1038, 253);
            this.uGridMaterialGoods.TabIndex = 42;
            this.uGridMaterialGoods.Text = "ultraGrid1";
            this.uGridMaterialGoods.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // ultraGroupBoxTop
            // 
            this.ultraGroupBoxTop.Controls.Add(this.ultraGroupBox2);
            this.ultraGroupBoxTop.Controls.Add(this.UltraGruopBox1);
            this.ultraGroupBoxTop.Controls.Add(this.ultraGroupBox3);
            this.ultraGroupBoxTop.Dock = System.Windows.Forms.DockStyle.Bottom;
            appearance4.FontData.BoldAsString = "False";
            appearance4.FontData.SizeInPoints = 8F;
            this.ultraGroupBoxTop.HeaderAppearance = appearance4;
            this.ultraGroupBoxTop.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOutsideBorder;
            this.ultraGroupBoxTop.Location = new System.Drawing.Point(0, 253);
            this.ultraGroupBoxTop.Name = "ultraGroupBoxTop";
            this.ultraGroupBoxTop.Size = new System.Drawing.Size(1038, 130);
            this.ultraGroupBoxTop.TabIndex = 32;
            this.ultraGroupBoxTop.Text = "Tùy chọn";
            this.ultraGroupBoxTop.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.txtAugmentMoney);
            this.ultraGroupBox2.Controls.Add(this.txtRate);
            this.ultraGroupBox2.Controls.Add(this.chkRate);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox2.Location = new System.Drawing.Point(203, 21);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(684, 106);
            this.ultraGroupBox2.TabIndex = 3;
            this.ultraGroupBox2.Text = "Thay đổi giá bán theo";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtAugmentMoney
            // 
            this.txtAugmentMoney.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAugmentMoney.Location = new System.Drawing.Point(101, 46);
            this.txtAugmentMoney.Name = "txtAugmentMoney";
            this.txtAugmentMoney.Size = new System.Drawing.Size(580, 21);
            this.txtAugmentMoney.TabIndex = 31;
            this.txtAugmentMoney.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAugmentMoney_KeyPress);
            // 
            // txtRate
            // 
            appearance1.TextHAlignAsString = "Right";
            this.txtRate.Appearance = appearance1;
            this.txtRate.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtRate.Location = new System.Drawing.Point(101, 16);
            this.txtRate.Name = "txtRate";
            this.txtRate.Size = new System.Drawing.Size(580, 21);
            this.txtRate.TabIndex = 30;
            this.txtRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRate_KeyPress);
            // 
            // chkRate
            // 
            appearance2.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.chkRate.Appearance = appearance2;
            this.chkRate.BackColor = System.Drawing.Color.Transparent;
            this.chkRate.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkRate.CheckedIndex = 0;
            this.chkRate.Dock = System.Windows.Forms.DockStyle.Left;
            valueListItem11.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem11.DataValue = 0;
            valueListItem11.DisplayText = "Tỷ lệ";
            valueListItem12.DataValue = 1;
            valueListItem12.DisplayText = "Số tiền";
            this.chkRate.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem11,
            valueListItem12});
            this.chkRate.ItemSpacingVertical = 12;
            this.chkRate.Location = new System.Drawing.Point(3, 16);
            this.chkRate.Name = "chkRate";
            this.chkRate.Size = new System.Drawing.Size(98, 87);
            this.chkRate.TabIndex = 29;
            this.chkRate.Text = "Tỷ lệ";
            // 
            // UltraGruopBox1
            // 
            this.UltraGruopBox1.Controls.Add(this.chkType);
            this.UltraGruopBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.UltraGruopBox1.Location = new System.Drawing.Point(3, 21);
            this.UltraGruopBox1.Name = "UltraGruopBox1";
            this.UltraGruopBox1.Size = new System.Drawing.Size(200, 106);
            this.UltraGruopBox1.TabIndex = 32;
            this.UltraGruopBox1.Text = "Thay đổi giá bán theo";
            this.UltraGruopBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // chkType
            // 
            appearance3.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance3.TextHAlignAsString = "Center";
            appearance3.TextVAlignAsString = "Middle";
            this.chkType.Appearance = appearance3;
            this.chkType.BackColor = System.Drawing.Color.Transparent;
            this.chkType.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkType.CheckedIndex = 0;
            this.chkType.Dock = System.Windows.Forms.DockStyle.Fill;
            valueListItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem1.DataValue = 0;
            valueListItem1.DisplayText = "Giá bán hiện tại trong danh mục";
            valueListItem4.DataValue = 1;
            valueListItem4.DisplayText = "Giá bán cố định";
            valueListItem9.DataValue = 2;
            valueListItem9.DisplayText = "Giá nhập gần nhất";
            valueListItem10.DataValue = 3;
            valueListItem10.DisplayText = "Giá mua sau thuế gần nhất";
            this.chkType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem4,
            valueListItem9,
            valueListItem10});
            this.chkType.ItemSpacingVertical = 7;
            this.chkType.Location = new System.Drawing.Point(3, 16);
            this.chkType.Name = "chkType";
            this.chkType.Size = new System.Drawing.Size(194, 87);
            this.chkType.TabIndex = 30;
            this.chkType.Text = "Giá bán hiện tại trong danh mục";
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.chkGiaBan1);
            this.ultraGroupBox3.Controls.Add(this.chkGiaBan2);
            this.ultraGroupBox3.Controls.Add(this.chkGiaBan3);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.ultraGroupBox3.Location = new System.Drawing.Point(887, 21);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(148, 106);
            this.ultraGroupBox3.TabIndex = 4;
            this.ultraGroupBox3.Text = "Áp dụng cho giá bán";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // chkGiaBan1
            // 
            this.chkGiaBan1.BackColor = System.Drawing.Color.Transparent;
            this.chkGiaBan1.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkGiaBan1.Location = new System.Drawing.Point(6, 17);
            this.chkGiaBan1.Name = "chkGiaBan1";
            this.chkGiaBan1.Size = new System.Drawing.Size(93, 20);
            this.chkGiaBan1.TabIndex = 4;
            this.chkGiaBan1.Text = "Giá bán 1";
            this.chkGiaBan1.UseAppStyling = false;
            // 
            // chkGiaBan2
            // 
            this.chkGiaBan2.BackColor = System.Drawing.Color.Transparent;
            this.chkGiaBan2.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkGiaBan2.Location = new System.Drawing.Point(6, 44);
            this.chkGiaBan2.Name = "chkGiaBan2";
            this.chkGiaBan2.Size = new System.Drawing.Size(93, 20);
            this.chkGiaBan2.TabIndex = 3;
            this.chkGiaBan2.Text = "Giá bán 2";
            this.chkGiaBan2.UseAppStyling = false;
            // 
            // chkGiaBan3
            // 
            this.chkGiaBan3.BackColor = System.Drawing.Color.Transparent;
            this.chkGiaBan3.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkGiaBan3.Location = new System.Drawing.Point(6, 70);
            this.chkGiaBan3.Name = "chkGiaBan3";
            this.chkGiaBan3.Size = new System.Drawing.Size(93, 20);
            this.chkGiaBan3.TabIndex = 2;
            this.chkGiaBan3.Text = "Giá bán 3";
            this.chkGiaBan3.UseAppStyling = false;
            // 
            // btnHoan
            // 
            this.btnHoan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHoan.Location = new System.Drawing.Point(874, 2);
            this.btnHoan.Name = "btnHoan";
            this.btnHoan.Size = new System.Drawing.Size(75, 23);
            this.btnHoan.TabIndex = 9;
            this.btnHoan.Text = "Hoãn";
            this.btnHoan.Click += new System.EventHandler(this.btnHoan_Click);
            // 
            // btnDong
            // 
            this.btnDong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDong.Location = new System.Drawing.Point(963, 2);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(75, 23);
            this.btnDong.TabIndex = 8;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnCat
            // 
            this.btnCat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCat.Location = new System.Drawing.Point(785, 2);
            this.btnCat.Name = "btnCat";
            this.btnCat.Size = new System.Drawing.Size(75, 23);
            this.btnCat.TabIndex = 7;
            this.btnCat.Text = "Cất";
            this.btnCat.Click += new System.EventHandler(this.btnCat_Click);
            // 
            // btnTinhGiaBan
            // 
            this.btnTinhGiaBan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTinhGiaBan.Location = new System.Drawing.Point(696, 2);
            this.btnTinhGiaBan.Name = "btnTinhGiaBan";
            this.btnTinhGiaBan.Size = new System.Drawing.Size(75, 23);
            this.btnTinhGiaBan.TabIndex = 6;
            this.btnTinhGiaBan.Text = "Tính giá bán";
            this.btnTinhGiaBan.Click += new System.EventHandler(this.btnTinhGiaBan_Click);
            // 
            // ultraPanel3
            // 
            // 
            // ultraPanel3.ClientArea
            // 
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraGroupBox4);
            this.ultraPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel3.Location = new System.Drawing.Point(0, 383);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(1038, 30);
            this.ultraPanel3.TabIndex = 43;
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.btnHoan);
            this.ultraGroupBox4.Controls.Add(this.btnCat);
            this.ultraGroupBox4.Controls.Add(this.btnTinhGiaBan);
            this.ultraGroupBox4.Controls.Add(this.btnDong);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance5.FontData.BoldAsString = "False";
            appearance5.FontData.SizeInPoints = 8F;
            this.ultraGroupBox4.HeaderAppearance = appearance5;
            this.ultraGroupBox4.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOutsideBorder;
            this.ultraGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(1038, 30);
            this.ultraGroupBox4.TabIndex = 33;
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // FPPEditMaterialGoods
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1038, 413);
            this.Controls.Add(this.ultraPanel1);
            this.Controls.Add(this.ultraGroupBoxTop);
            this.Controls.Add(this.ultraPanel3);
            this.Name = "FPPEditMaterialGoods";
            this.Text = "Tính giá bán";
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ClientArea.PerformLayout();
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridMaterialGoods)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxTop)).EndInit();
            this.ultraGroupBoxTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAugmentMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UltraGruopBox1)).EndInit();
            this.UltraGruopBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkGiaBan1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGiaBan2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGiaBan3)).EndInit();
            this.ultraPanel3.ClientArea.ResumeLayout(false);
            this.ultraPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMaterialGoods;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxTop;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkGiaBan3;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAugmentMoney;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtRate;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet chkRate;
        private Infragistics.Win.Misc.UltraGroupBox UltraGruopBox1;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet chkType;
        private Infragistics.Win.Misc.UltraButton btnHoan;
        private Infragistics.Win.Misc.UltraButton btnDong;
        private Infragistics.Win.Misc.UltraButton btnCat;
        private Infragistics.Win.Misc.UltraButton btnTinhGiaBan;
        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkGiaBan1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkGiaBan2;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit ultraMaskedEdit1;
    }
}