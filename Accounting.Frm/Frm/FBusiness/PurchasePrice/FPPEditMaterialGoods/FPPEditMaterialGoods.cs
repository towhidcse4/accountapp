﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FPPEditMaterialGoods : CustormForm
    {
        private readonly IMaterialGoodsService _materialGoodsSrv;
        public FPPEditMaterialGoods()
        {
            InitializeComponent();
            _materialGoodsSrv = IoC.Resolve<IMaterialGoodsService>();
            List<MaterialGoods> lst = _materialGoodsSrv.GetByIsActive(true);
            ViewDsach(lst);
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTinhGiaBan_Click(object sender, EventArgs e)
        {
            var lstMaterialGoods = uGridMaterialGoods.DataSource as List<MaterialGoods>;
            if (lstMaterialGoods.Count(c => c.Status == true) == 0)
            {
                MessageBox.Show("Bạn phải chọn ít nhất một loại hàng hóa để tính");
                return;
            }
            if (!chkGiaBan1.Checked && !chkGiaBan2.Checked && !chkGiaBan3.Checked)
            {
                MessageBox.Show("Bạn phải chọn ít nhất một giá bán để tính");
                return;
            }
            foreach (var item in lstMaterialGoods.Where(c => c.Status == true))
            {
                var model = _materialGoodsSrv.Getbykey(item.ID);
                decimal? SalePrice = 0;
                decimal? SalePrice2 = 0;
                decimal? SalePrice3 = 0;
                decimal? FixedSalePrice = 0;
                decimal? PurchasePrice = 0;
                decimal? LastPurchasePriceAfterTax = 0;
                switch (Convert.ToInt32(chkType.Value))
                {
                    case 0:
                        if (Convert.ToInt32(chkRate.Value) == 0)
                        {
                            decimal _salePrice = model.SalePrice.HasValue ? model.SalePrice.Value : 0;
                            decimal _salePrice2 = model.SalePrice2.HasValue ? model.SalePrice2.Value : 0;
                            decimal _salePrice3 = model.SalePrice3.HasValue ? model.SalePrice3.Value : 0;
                            SalePrice = _salePrice + _salePrice * Convert.ToDecimal(txtRate.Value) / 100;
                            SalePrice2 = _salePrice2 + _salePrice2 * Convert.ToDecimal(txtRate.Value) / 100;
                            SalePrice3 = _salePrice3 + _salePrice3 * Convert.ToDecimal(txtRate.Value) / 100;
                        }
                        else if (Convert.ToInt32(chkRate.Value) == 1)
                        {
                            SalePrice = model.SalePrice + Convert.ToDecimal(txtAugmentMoney.Value);
                            SalePrice2 = model.SalePrice2 + Convert.ToDecimal(txtAugmentMoney.Value);
                            SalePrice3 = model.SalePrice3 + Convert.ToDecimal(txtAugmentMoney.Value);
                        }
                        item.SalePrice = chkGiaBan1.Checked ? SalePrice : model.SalePrice;
                        item.SalePrice2 = chkGiaBan2.Checked ? SalePrice2 : model.SalePrice2;
                        item.SalePrice3 = chkGiaBan3.Checked ? SalePrice3 : model.SalePrice3;
                        break;
                    case 1:
                        if (Convert.ToInt32(chkRate.Value) == 0)
                        {
                            FixedSalePrice = model.FixedSalePrice + model.FixedSalePrice * Convert.ToDecimal(txtRate.Value) / 100;
                        }
                        else if (Convert.ToInt32(chkRate.Value) == 1)
                        {
                            FixedSalePrice = model.FixedSalePrice + Convert.ToDecimal(txtAugmentMoney.Value);
                        }
                        if (chkGiaBan1.Checked)
                            item.SalePrice = FixedSalePrice;
                        if (chkGiaBan2.Checked)
                            item.SalePrice2 = FixedSalePrice;
                        if (chkGiaBan3.Checked)
                            item.SalePrice3 = FixedSalePrice;
                        break;
                    case 2:
                        if (Convert.ToInt32(chkRate.Value) == 0)
                        {
                            PurchasePrice = item.PurchasePrice + item.PurchasePrice * Convert.ToDecimal(txtRate.Value) / 100;
                        }
                        else if (Convert.ToInt32(chkRate.Value) == 1)
                        {
                            PurchasePrice = item.PurchasePrice + Convert.ToDecimal(txtAugmentMoney.Value);
                        }
                        if (chkGiaBan1.Checked)
                            item.SalePrice = PurchasePrice;
                        if (chkGiaBan2.Checked)
                            item.SalePrice2 = PurchasePrice;
                        if (chkGiaBan3.Checked)
                            item.SalePrice3 = PurchasePrice;
                        break;
                    case 3:
                        if (Convert.ToInt32(chkRate.Value) == 0)
                        {
                            LastPurchasePriceAfterTax = item.LastPurchasePriceAfterTax + item.PurchasePrice * Convert.ToDecimal(txtRate.Value) / 100;
                        }
                        else if (Convert.ToInt32(chkRate.Value) == 1)
                        {
                            LastPurchasePriceAfterTax = item.LastPurchasePriceAfterTax + Convert.ToDecimal(txtAugmentMoney.Value);
                        }
                        if (chkGiaBan1.Checked)
                            item.SalePrice = LastPurchasePriceAfterTax;
                        if (chkGiaBan2.Checked)
                            item.SalePrice2 = LastPurchasePriceAfterTax;
                        if (chkGiaBan3.Checked)
                            item.SalePrice3 = LastPurchasePriceAfterTax;
                        break;
                }
            }
            uGridMaterialGoods.DataSource = lstMaterialGoods;
            uGridMaterialGoods.Refresh();
        }

        private void btnCat_Click(object sender, EventArgs e)
        {
            try
            {
                var lstMaterialGoods = uGridMaterialGoods.DataSource as List<MaterialGoods>;
                if (lstMaterialGoods.Count(c => c.Status == true) == 0)
                {
                    MessageBox.Show("Bạn phải chọn ít nhất một loại hàng hóa để tính");
                    return;
                }
                if (!chkGiaBan1.Checked && !chkGiaBan2.Checked && !chkGiaBan3.Checked)
                {
                    MessageBox.Show("Bạn phải chọn ít nhất một giá bán để tính");
                    return;
                }
                _materialGoodsSrv.BeginTran();
                foreach (var item in lstMaterialGoods.Where(c => c.Status == true))
                {
                    var model = _materialGoodsSrv.Getbykey(item.ID);
                    if (chkGiaBan1.Checked)
                        model.SalePrice = item.SalePrice;
                    if (chkGiaBan2.Checked)
                        model.SalePrice2 = item.SalePrice2;
                    if (chkGiaBan3.Checked)
                        model.SalePrice3 = item.SalePrice3;
                    _materialGoodsSrv.Update(model);
                    _materialGoodsSrv.CommitChanges();
                }
                _materialGoodsSrv.CommitTran();
                uGridMaterialGoods.DataSource = _materialGoodsSrv.GetByIsActive(true);
                //uGridMaterialGoods.Refresh();
                MessageBox.Show("Thay đổi giá bán thành công !");
            }
            catch (Exception)
            {
                _materialGoodsSrv.RolbackTran();
                throw;
            }
        }

        #region Cấu hình hiện thị Grid
        List<TemplateColumn> MauDanhSach =
        new List<TemplateColumn>
                       {
                           new TemplateColumn
                               {
                                   ColumnName = "Status",
                                   ColumnCaption = "",
                                   ColumnWidth = 30,
                                   ColumnMaxWidth = 60,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 1
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "MaterialGoodsCategoryCode",
                                   ColumnCaption = "Loại VTHH",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 2
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "MaterialGoodsCode",
                                   ColumnCaption = "Mã vật tư",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 3
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "MaterialGoodsName",
                                   ColumnCaption = "Tên vật tư",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 4
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "PurchasePrice",
                                   ColumnCaption = "Giá nhập gần nhất",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 5
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "LastPurchasePriceAfterTax",
                                   ColumnCaption = "Giá mua ST gần nhất",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 6
                               },
                               new TemplateColumn
                               {
                                   ColumnName = "FixedSalePrice",
                                   ColumnCaption = "Giá bán cố định",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 7
                               },
                               new TemplateColumn
                               {
                                   ColumnName = "SalePrice",
                                   ColumnCaption = "Giá bán 1",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 8
                               },
                               new TemplateColumn
                               {
                                   ColumnName = "SalePrice2",
                                   ColumnCaption = "Giá bán 2",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 9
                               },
                               new TemplateColumn
                               {
                                   ColumnName = "SalePrice3",
                                   ColumnCaption = "Giá bán 3",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 10
                               }
                       };

        private void ViewDsach(List<MaterialGoods> model)
        {
            uGridMaterialGoods.DataSource = Utils.CloneObject(model);
            Utils.ConfigGrid(uGridMaterialGoods, "", MauDanhSach);
            uGridMaterialGoods.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGridMaterialGoods.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGridMaterialGoods.DisplayLayout.Bands[0].Summaries.Clear();
            uGridMaterialGoods.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            // xét style cho các cột có dạng checkbox
            UltraGridBand band = uGridMaterialGoods.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;
            Utils.FormatNumberic(uGridMaterialGoods.DisplayLayout.Bands[0].Columns["SalePrice"], ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(uGridMaterialGoods.DisplayLayout.Bands[0].Columns["SalePrice2"], ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(uGridMaterialGoods.DisplayLayout.Bands[0].Columns["SalePrice3"], ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(uGridMaterialGoods.DisplayLayout.Bands[0].Columns["PurchasePrice"], ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(uGridMaterialGoods.DisplayLayout.Bands[0].Columns["LastPurchasePriceAfterTax"], ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(uGridMaterialGoods.DisplayLayout.Bands[0].Columns["FixedSalePrice"], ConstDatabase.Format_ForeignCurrency);
        }
        #endregion

        private void btnHoan_Click(object sender, EventArgs e)
        {
            uGridMaterialGoods.DataSource = _materialGoodsSrv.GetByIsActive(true);
            uGridMaterialGoods.Refresh();
        }

        private void txtRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar))
            {

            }
            else
            {
                e.Handled = e.KeyChar != (char)Keys.Back;
            }
        }

        private void txtAugmentMoney_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar))
            {

            }
            else
            {
                e.Handled = e.KeyChar != (char)Keys.Back;
            }
        }


    }
}
