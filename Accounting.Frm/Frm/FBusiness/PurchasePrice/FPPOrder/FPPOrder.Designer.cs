﻿namespace Accounting
{
    partial class FPPOrder
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FPPOrder));
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.label1 = new System.Windows.Forms.Label();
            this.lblOrderDate = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNo = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblDeliveryDate = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblAccountingObjectName = new System.Windows.Forms.Label();
            this.lblReason = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblAccountingObjectAddress = new System.Windows.Forms.Label();
            this.lblTotalPaymentAmountOriginal = new System.Windows.Forms.Label();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridPosted = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.btnEdit = new Infragistics.Win.Misc.UltraButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ultraTabPageControl1.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPosted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.cms4Grid.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.label1);
            this.ultraTabPageControl1.Controls.Add(this.lblOrderDate);
            this.ultraTabPageControl1.Controls.Add(this.label10);
            this.ultraTabPageControl1.Controls.Add(this.label2);
            this.ultraTabPageControl1.Controls.Add(this.lblNo);
            this.ultraTabPageControl1.Controls.Add(this.label7);
            this.ultraTabPageControl1.Controls.Add(this.label3);
            this.ultraTabPageControl1.Controls.Add(this.lblDeliveryDate);
            this.ultraTabPageControl1.Controls.Add(this.label6);
            this.ultraTabPageControl1.Controls.Add(this.lblAccountingObjectName);
            this.ultraTabPageControl1.Controls.Add(this.lblReason);
            this.ultraTabPageControl1.Controls.Add(this.label5);
            this.ultraTabPageControl1.Controls.Add(this.lblAccountingObjectAddress);
            this.ultraTabPageControl1.Controls.Add(this.lblTotalPaymentAmountOriginal);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(757, 232);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(9, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Số đơn hàng";
            // 
            // lblOrderDate
            // 
            this.lblOrderDate.AutoSize = true;
            this.lblOrderDate.BackColor = System.Drawing.Color.Transparent;
            this.lblOrderDate.Location = new System.Drawing.Point(134, 31);
            this.lblOrderDate.Name = "lblOrderDate";
            this.lblOrderDate.Size = new System.Drawing.Size(10, 13);
            this.lblOrderDate.TabIndex = 10;
            this.lblOrderDate.Text = ":";
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(10, 122);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 17);
            this.label10.TabIndex = 9;
            this.label10.Text = "Diễn giải";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(9, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ngày đơn hàng";
            // 
            // lblNo
            // 
            this.lblNo.AutoSize = true;
            this.lblNo.BackColor = System.Drawing.Color.Transparent;
            this.lblNo.Location = new System.Drawing.Point(134, 7);
            this.lblNo.Name = "lblNo";
            this.lblNo.Size = new System.Drawing.Size(10, 13);
            this.lblNo.TabIndex = 11;
            this.lblNo.Text = ":";
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(8, 147);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 20);
            this.label7.TabIndex = 8;
            this.label7.Text = "Tổng tiền thanh toán";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(10, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ngày giao hàng";
            // 
            // lblDeliveryDate
            // 
            this.lblDeliveryDate.AutoSize = true;
            this.lblDeliveryDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDeliveryDate.Location = new System.Drawing.Point(134, 54);
            this.lblDeliveryDate.Name = "lblDeliveryDate";
            this.lblDeliveryDate.Size = new System.Drawing.Size(10, 13);
            this.lblDeliveryDate.TabIndex = 12;
            this.lblDeliveryDate.Text = ":";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(10, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 17);
            this.label6.TabIndex = 3;
            this.label6.Text = "Đối tượng";
            // 
            // lblAccountingObjectName
            // 
            this.lblAccountingObjectName.AutoSize = true;
            this.lblAccountingObjectName.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectName.Location = new System.Drawing.Point(134, 76);
            this.lblAccountingObjectName.Name = "lblAccountingObjectName";
            this.lblAccountingObjectName.Size = new System.Drawing.Size(10, 13);
            this.lblAccountingObjectName.TabIndex = 13;
            this.lblAccountingObjectName.Text = ":";
            // 
            // lblReason
            // 
            this.lblReason.AutoSize = true;
            this.lblReason.BackColor = System.Drawing.Color.Transparent;
            this.lblReason.Location = new System.Drawing.Point(134, 122);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(10, 13);
            this.lblReason.TabIndex = 16;
            this.lblReason.Text = ":";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(10, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Địa chỉ";
            // 
            // lblAccountingObjectAddress
            // 
            this.lblAccountingObjectAddress.AutoSize = true;
            this.lblAccountingObjectAddress.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectAddress.Location = new System.Drawing.Point(134, 100);
            this.lblAccountingObjectAddress.Name = "lblAccountingObjectAddress";
            this.lblAccountingObjectAddress.Size = new System.Drawing.Size(10, 13);
            this.lblAccountingObjectAddress.TabIndex = 14;
            this.lblAccountingObjectAddress.Text = ":";
            // 
            // lblTotalPaymentAmountOriginal
            // 
            this.lblTotalPaymentAmountOriginal.AutoSize = true;
            this.lblTotalPaymentAmountOriginal.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalPaymentAmountOriginal.Location = new System.Drawing.Point(134, 147);
            this.lblTotalPaymentAmountOriginal.Name = "lblTotalPaymentAmountOriginal";
            this.lblTotalPaymentAmountOriginal.Size = new System.Drawing.Size(10, 13);
            this.lblTotalPaymentAmountOriginal.TabIndex = 15;
            this.lblTotalPaymentAmountOriginal.Text = ":";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridPosted);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(757, 232);
            // 
            // uGridPosted
            // 
            this.uGridPosted.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridPosted.Location = new System.Drawing.Point(0, 0);
            this.uGridPosted.Name = "uGridPosted";
            this.uGridPosted.Size = new System.Drawing.Size(757, 232);
            this.uGridPosted.TabIndex = 1;
            this.uGridPosted.Text = "ultraGrid1";
            this.uGridPosted.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // uGrid
            // 
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 0);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(761, 251);
            this.uGrid.TabIndex = 1;
            this.uGrid.Text = "uGrid";
            this.uGrid.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.uGrid_AfterSelectChange);
            this.uGrid.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.UGridDoubleClickRow);
            this.uGrid.MouseDown += new System.Windows.Forms.MouseEventHandler(this.UGridMouseDown);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(761, 49);
            this.panel1.TabIndex = 2;
            // 
            // btnAdd
            // 
            appearance1.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.btnAdd.Appearance = appearance1;
            this.btnAdd.Location = new System.Drawing.Point(12, 10);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 30);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.Click += new System.EventHandler(this.BtnAddClick);
            // 
            // btnDelete
            // 
            appearance2.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.btnDelete.Appearance = appearance2;
            this.btnDelete.Location = new System.Drawing.Point(195, 10);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.BtnDeleteClick);
            // 
            // btnEdit
            // 
            appearance3.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.btnEdit.Appearance = appearance3;
            this.btnEdit.Location = new System.Drawing.Point(104, 10);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 30);
            this.btnEdit.TabIndex = 6;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.Click += new System.EventHandler(this.BtnEditClick);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ultraTabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 300);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(761, 258);
            this.panel2.TabIndex = 3;
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(761, 258);
            this.ultraTabControl1.TabIndex = 20;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Thông tin chung";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Chi tiết";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(757, 232);
            // 
            // cms4Grid
            // 
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tsmAdd,
            this.tsmEdit,
            this.tsmDelete,
            this.tmsReLoad});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(150, 98);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(146, 6);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.Size = new System.Drawing.Size(149, 22);
            this.tsmAdd.Text = "Thêm";
            this.tsmAdd.Click += new System.EventHandler(this.TsmAddClick);
            // 
            // tsmEdit
            // 
            this.tsmEdit.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.tsmEdit.Name = "tsmEdit";
            this.tsmEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.tsmEdit.Size = new System.Drawing.Size(149, 22);
            this.tsmEdit.Text = "Sửa";
            this.tsmEdit.Click += new System.EventHandler(this.TsmEditClick);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.Size = new System.Drawing.Size(149, 22);
            this.tsmDelete.Text = "Xóa";
            this.tsmDelete.Click += new System.EventHandler(this.TsmDeleteClick);
            // 
            // tmsReLoad
            // 
            this.tmsReLoad.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tmsReLoad.Name = "tmsReLoad";
            this.tmsReLoad.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.tmsReLoad.Size = new System.Drawing.Size(149, 22);
            this.tmsReLoad.Text = "Tải Lại";
            this.tmsReLoad.Click += new System.EventHandler(this.TmsReLoadClick);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.uGrid);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 49);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(761, 251);
            this.panel3.TabIndex = 2;
            // 
            // FPPOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 558);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FPPOrder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl1.PerformLayout();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridPosted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.cms4Grid.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmEdit;
        private System.Windows.Forms.ToolStripMenuItem tmsReLoad;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblReason;
        private System.Windows.Forms.Label lblTotalPaymentAmountOriginal;
        private System.Windows.Forms.Label lblAccountingObjectAddress;
        private System.Windows.Forms.Label lblAccountingObjectName;
        private System.Windows.Forms.Label lblDeliveryDate;
        private System.Windows.Forms.Label lblNo;
        private System.Windows.Forms.Label lblOrderDate;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPosted;
        private Infragistics.Win.Misc.UltraButton btnAdd;
        private Infragistics.Win.Misc.UltraButton btnDelete;
        private Infragistics.Win.Misc.UltraButton btnEdit;
    }
}
