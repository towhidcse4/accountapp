﻿namespace Accounting
{
    partial class FPPOrderDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtCompanyTaxCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtShippingPlace = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblReason = new Infragistics.Win.Misc.UltraLabel();
            this.lblCompanyTaxCode = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectAddress = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObjectID = new Infragistics.Win.Misc.UltraLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.palTopVouchers = new Infragistics.Win.Misc.UltraPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pnlUgrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.panel5 = new System.Windows.Forms.Panel();
            this.palBottom = new System.Windows.Forms.Panel();
            this.lblTotalPaymentAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalVATAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalPaymentAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalVATAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.lbl2 = new Infragistics.Win.Misc.UltraLabel();
            this.lbl3 = new Infragistics.Win.Misc.UltraLabel();
            this.lbl1 = new Infragistics.Win.Misc.UltraLabel();
            this.uGridControl = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraToolTipManager1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShippingPlace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.palTopVouchers.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.pnlUgrid.ClientArea.SuspendLayout();
            this.pnlUgrid.SuspendLayout();
            this.panel5.SuspendLayout();
            this.palBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectName);
            this.ultraGroupBox1.Controls.Add(this.txtCompanyTaxCode);
            this.ultraGroupBox1.Controls.Add(this.cbbAccountingObjectID);
            this.ultraGroupBox1.Controls.Add(this.txtShippingPlace);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.txtReason);
            this.ultraGroupBox1.Controls.Add(this.lblReason);
            this.ultraGroupBox1.Controls.Add(this.lblCompanyTaxCode);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectAddress);
            this.ultraGroupBox1.Controls.Add(this.lblAccountingObjectAddress);
            this.ultraGroupBox1.Controls.Add(this.lblAccountingObjectID);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance7.FontData.BoldAsString = "True";
            appearance7.FontData.SizeInPoints = 10F;
            this.ultraGroupBox1.HeaderAppearance = appearance7;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(929, 131);
            this.ultraGroupBox1.TabIndex = 26;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtAccountingObjectName
            // 
            this.txtAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectName.AutoSize = false;
            this.txtAccountingObjectName.Location = new System.Drawing.Point(315, 25);
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(602, 21);
            this.txtAccountingObjectName.TabIndex = 33;
            this.txtAccountingObjectName.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // txtCompanyTaxCode
            // 
            this.txtCompanyTaxCode.AutoSize = false;
            this.txtCompanyTaxCode.Location = new System.Drawing.Point(123, 73);
            this.txtCompanyTaxCode.Name = "txtCompanyTaxCode";
            this.txtCompanyTaxCode.Size = new System.Drawing.Size(186, 21);
            this.txtCompanyTaxCode.TabIndex = 32;
            // 
            // cbbAccountingObjectID
            // 
            this.cbbAccountingObjectID.AutoSize = false;
            appearance1.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton1.Appearance = appearance1;
            this.cbbAccountingObjectID.ButtonsRight.Add(editorButton1);
            this.cbbAccountingObjectID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectID.Location = new System.Drawing.Point(123, 25);
            this.cbbAccountingObjectID.Name = "cbbAccountingObjectID";
            this.cbbAccountingObjectID.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID.Size = new System.Drawing.Size(186, 21);
            this.cbbAccountingObjectID.TabIndex = 31;
            // 
            // txtShippingPlace
            // 
            this.txtShippingPlace.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtShippingPlace.AutoSize = false;
            this.txtShippingPlace.Location = new System.Drawing.Point(425, 73);
            this.txtShippingPlace.Name = "txtShippingPlace";
            this.txtShippingPlace.Size = new System.Drawing.Size(492, 21);
            this.txtShippingPlace.TabIndex = 27;
            // 
            // ultraLabel2
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance2;
            this.ultraLabel2.Location = new System.Drawing.Point(315, 73);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(104, 21);
            this.ultraLabel2.TabIndex = 26;
            this.ultraLabel2.Text = "Địa điểm giao hàng";
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason.AutoSize = false;
            this.txtReason.Location = new System.Drawing.Point(123, 97);
            this.txtReason.Multiline = true;
            this.txtReason.Name = "txtReason";
            this.txtReason.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtReason.Size = new System.Drawing.Size(794, 31);
            this.txtReason.TabIndex = 27;
            // 
            // lblReason
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.lblReason.Appearance = appearance3;
            this.lblReason.Location = new System.Drawing.Point(9, 97);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(67, 21);
            this.lblReason.TabIndex = 26;
            this.lblReason.Text = "Diễn giải";
            // 
            // lblCompanyTaxCode
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblCompanyTaxCode.Appearance = appearance4;
            this.lblCompanyTaxCode.Location = new System.Drawing.Point(9, 73);
            this.lblCompanyTaxCode.Name = "lblCompanyTaxCode";
            this.lblCompanyTaxCode.Size = new System.Drawing.Size(67, 21);
            this.lblCompanyTaxCode.TabIndex = 24;
            this.lblCompanyTaxCode.Text = "Mã số thuế";
            // 
            // txtAccountingObjectAddress
            // 
            this.txtAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddress.AutoSize = false;
            this.txtAccountingObjectAddress.Location = new System.Drawing.Point(123, 49);
            this.txtAccountingObjectAddress.Name = "txtAccountingObjectAddress";
            this.txtAccountingObjectAddress.Size = new System.Drawing.Size(794, 21);
            this.txtAccountingObjectAddress.TabIndex = 23;
            this.txtAccountingObjectAddress.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // lblAccountingObjectAddress
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.lblAccountingObjectAddress.Appearance = appearance5;
            this.lblAccountingObjectAddress.Location = new System.Drawing.Point(9, 49);
            this.lblAccountingObjectAddress.Name = "lblAccountingObjectAddress";
            this.lblAccountingObjectAddress.Size = new System.Drawing.Size(68, 21);
            this.lblAccountingObjectAddress.TabIndex = 22;
            this.lblAccountingObjectAddress.Text = "Địa chỉ";
            // 
            // lblAccountingObjectID
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.lblAccountingObjectID.Appearance = appearance6;
            this.lblAccountingObjectID.Location = new System.Drawing.Point(9, 25);
            this.lblAccountingObjectID.Name = "lblAccountingObjectID";
            this.lblAccountingObjectID.Size = new System.Drawing.Size(68, 21);
            this.lblAccountingObjectID.TabIndex = 0;
            this.lblAccountingObjectID.Text = "Đối tượng";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ultraGroupBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(929, 80);
            this.panel1.TabIndex = 27;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.palTopVouchers);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance9.FontData.BoldAsString = "True";
            appearance9.FontData.SizeInPoints = 13F;
            this.ultraGroupBox2.HeaderAppearance = appearance9;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(929, 80);
            this.ultraGroupBox2.TabIndex = 31;
            this.ultraGroupBox2.Text = "Đơn mua hàng";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // palTopVouchers
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.BackColor2 = System.Drawing.Color.Transparent;
            this.palTopVouchers.Appearance = appearance8;
            this.palTopVouchers.AutoSize = true;
            this.palTopVouchers.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.palTopVouchers.Location = new System.Drawing.Point(9, 28);
            this.palTopVouchers.Name = "palTopVouchers";
            this.palTopVouchers.Size = new System.Drawing.Size(308, 43);
            this.palTopVouchers.TabIndex = 31;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.palBottom);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 80);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(929, 661);
            this.panel2.TabIndex = 28;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.ultraSplitter1);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(929, 532);
            this.panel4.TabIndex = 28;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.pnlUgrid);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 146);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(929, 386);
            this.panel6.TabIndex = 28;
            // 
            // pnlUgrid
            // 
            // 
            // pnlUgrid.ClientArea
            // 
            this.pnlUgrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.pnlUgrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlUgrid.Location = new System.Drawing.Point(0, 0);
            this.pnlUgrid.Name = "pnlUgrid";
            this.pnlUgrid.Size = new System.Drawing.Size(929, 386);
            this.pnlUgrid.TabIndex = 0;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance10.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance10;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(831, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 8;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 131);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 200;
            this.ultraSplitter1.Size = new System.Drawing.Size(929, 15);
            this.ultraSplitter1.TabIndex = 3;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.ultraGroupBox1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(929, 131);
            this.panel5.TabIndex = 27;
            // 
            // palBottom
            // 
            this.palBottom.Controls.Add(this.lblTotalPaymentAmount);
            this.palBottom.Controls.Add(this.lblTotalVATAmount);
            this.palBottom.Controls.Add(this.lblTotalAmount);
            this.palBottom.Controls.Add(this.lblTotalPaymentAmountOriginal);
            this.palBottom.Controls.Add(this.lblTotalVATAmountOriginal);
            this.palBottom.Controls.Add(this.lblTotalAmountOriginal);
            this.palBottom.Controls.Add(this.lbl2);
            this.palBottom.Controls.Add(this.lbl3);
            this.palBottom.Controls.Add(this.lbl1);
            this.palBottom.Controls.Add(this.uGridControl);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 532);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(929, 129);
            this.palBottom.TabIndex = 27;
            // 
            // lblTotalPaymentAmount
            // 
            this.lblTotalPaymentAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextHAlignAsString = "Right";
            appearance11.TextVAlignAsString = "Middle";
            this.lblTotalPaymentAmount.Appearance = appearance11;
            this.lblTotalPaymentAmount.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalPaymentAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalPaymentAmount.Location = new System.Drawing.Point(757, 54);
            this.lblTotalPaymentAmount.Name = "lblTotalPaymentAmount";
            this.lblTotalPaymentAmount.Size = new System.Drawing.Size(169, 21);
            this.lblTotalPaymentAmount.TabIndex = 77;
            // 
            // lblTotalVATAmount
            // 
            this.lblTotalVATAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.TextHAlignAsString = "Right";
            appearance12.TextVAlignAsString = "Middle";
            this.lblTotalVATAmount.Appearance = appearance12;
            this.lblTotalVATAmount.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalVATAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalVATAmount.Location = new System.Drawing.Point(757, 29);
            this.lblTotalVATAmount.Name = "lblTotalVATAmount";
            this.lblTotalVATAmount.Size = new System.Drawing.Size(169, 21);
            this.lblTotalVATAmount.TabIndex = 76;
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextHAlignAsString = "Right";
            appearance13.TextVAlignAsString = "Middle";
            this.lblTotalAmount.Appearance = appearance13;
            this.lblTotalAmount.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmount.Location = new System.Drawing.Point(757, 5);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(169, 21);
            this.lblTotalAmount.TabIndex = 75;
            // 
            // lblTotalPaymentAmountOriginal
            // 
            this.lblTotalPaymentAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextHAlignAsString = "Right";
            appearance14.TextVAlignAsString = "Middle";
            this.lblTotalPaymentAmountOriginal.Appearance = appearance14;
            this.lblTotalPaymentAmountOriginal.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalPaymentAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalPaymentAmountOriginal.Location = new System.Drawing.Point(572, 54);
            this.lblTotalPaymentAmountOriginal.Name = "lblTotalPaymentAmountOriginal";
            this.lblTotalPaymentAmountOriginal.Size = new System.Drawing.Size(169, 21);
            this.lblTotalPaymentAmountOriginal.TabIndex = 74;
            // 
            // lblTotalVATAmountOriginal
            // 
            this.lblTotalVATAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextHAlignAsString = "Right";
            appearance15.TextVAlignAsString = "Middle";
            this.lblTotalVATAmountOriginal.Appearance = appearance15;
            this.lblTotalVATAmountOriginal.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalVATAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalVATAmountOriginal.Location = new System.Drawing.Point(572, 29);
            this.lblTotalVATAmountOriginal.Name = "lblTotalVATAmountOriginal";
            this.lblTotalVATAmountOriginal.Size = new System.Drawing.Size(169, 21);
            this.lblTotalVATAmountOriginal.TabIndex = 73;
            // 
            // lblTotalAmountOriginal
            // 
            this.lblTotalAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.TextHAlignAsString = "Right";
            appearance16.TextVAlignAsString = "Middle";
            this.lblTotalAmountOriginal.Appearance = appearance16;
            this.lblTotalAmountOriginal.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmountOriginal.Location = new System.Drawing.Point(572, 5);
            this.lblTotalAmountOriginal.Name = "lblTotalAmountOriginal";
            this.lblTotalAmountOriginal.Size = new System.Drawing.Size(169, 21);
            this.lblTotalAmountOriginal.TabIndex = 72;
            // 
            // lbl2
            // 
            this.lbl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextVAlignAsString = "Middle";
            this.lbl2.Appearance = appearance17;
            this.lbl2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl2.Location = new System.Drawing.Point(439, 31);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(115, 19);
            this.lbl2.TabIndex = 67;
            this.lbl2.Text = "Tổng thuế GTGT";
            // 
            // lbl3
            // 
            this.lbl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextVAlignAsString = "Middle";
            this.lbl3.Appearance = appearance18;
            this.lbl3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl3.Location = new System.Drawing.Point(439, 54);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(115, 19);
            this.lbl3.TabIndex = 65;
            this.lbl3.Text = "Tổng tiền thanh toán";
            // 
            // lbl1
            // 
            this.lbl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.TextVAlignAsString = "Middle";
            this.lbl1.Appearance = appearance19;
            this.lbl1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1.Location = new System.Drawing.Point(439, 7);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(115, 19);
            this.lbl1.TabIndex = 63;
            this.lbl1.Text = "Tổng tiền hàng";
            // 
            // uGridControl
            // 
            this.uGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridControl.Location = new System.Drawing.Point(441, 81);
            this.uGridControl.Name = "uGridControl";
            this.uGridControl.Size = new System.Drawing.Size(488, 49);
            this.uGridControl.TabIndex = 1;
            this.uGridControl.Text = "ultraGrid1";
            // 
            // ultraToolTipManager1
            // 
            this.ultraToolTipManager1.ContainingControl = this;
            // 
            // FPPOrderDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(929, 741);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FPPOrderDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FPPOrderDetail_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShippingPlace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            this.palTopVouchers.ResumeLayout(false);
            this.palTopVouchers.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.pnlUgrid.ClientArea.ResumeLayout(false);
            this.pnlUgrid.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.palBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectAddress;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.Misc.UltraLabel lblReason;
        private Infragistics.Win.Misc.UltraLabel lblCompanyTaxCode;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel palBottom;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridControl;
        private Infragistics.Win.UltraWinToolTip.UltraToolTipManager ultraToolTipManager1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtShippingPlace;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraPanel pnlUgrid;
        private Infragistics.Win.Misc.UltraPanel palTopVouchers;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxCode;
        private Infragistics.Win.Misc.UltraLabel lbl2;
        private Infragistics.Win.Misc.UltraLabel lbl3;
        private Infragistics.Win.Misc.UltraLabel lbl1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        private Infragistics.Win.Misc.UltraLabel lblTotalAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel lblTotalPaymentAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalVATAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalPaymentAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel lblTotalVATAmountOriginal;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
    }
}
