﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using System.Linq;
using System;
using Infragistics.Win.UltraWinGrid;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinEditors;

namespace Accounting
{
    public partial class FPPOrderDetail : FppOrderDetailTmp
    {
        #region khởi tạo
        UltraGrid ugrid2 = null;
        UltraGrid ugrid0 = null;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        public FPPOrderDetail(PPOrder temp, IList<PPOrder> listObject, int statusForm)
        {//Add -> thêm, Sửa -> sửa, Xem -> Sửa ở trạng thái Enable = False
            InitializeComponent();
            base.InitializeComponent1();

            #region Thiết lập ban đầu
            _typeID = 200; _statusForm = statusForm;
           
                
                if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
                {
                    this.WindowState = FormWindowState.Normal;
                   
                    this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50+40;
                this.StartPosition = FormStartPosition.CenterScreen;
                }

            
            Utils.ClearCacheByType<PaymentClause>();
            if (statusForm == ConstFrm.optStatusForm.Add) _select.TypeID = _typeID; else _select = temp;
            _listSelects.AddRange(listObject);
            InitializeGUI(_select); //khởi tạo và hiển thị giá trị ban đầu cho các control
            ReloadToolbar(_select, listObject, statusForm);  //Change Menu Top
            #endregion
        }
        #endregion

        #region override
        public override void InitializeGUI(PPOrder input)
        {
            Template mauGiaoDien = Utils.GetMauGiaoDien(input.TypeID, input.TemplateID, Utils.ListTemplate);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                bdlRefVoucher = new BindingList<RefVoucher>(input.RefVouchers);
                
            }                
            //config số đơn hàng, ngày đơn hàng
            this.ConfigTopVouchersNo<PPOrder>(palTopVouchers, "No", "", "Date", null, false, fieldNo: "Số đơn hàng", fieldDate: "Ngày đơn hàng");
            if(_statusForm == ConstFrm.optStatusForm.Add)
                _select.ID = Guid.NewGuid();
            //Config Grid
            _listObjectInput.Add(new BindingList<PPOrderDetail>(_statusForm == ConstFrm.optStatusForm.Add ? new BindingList<PPOrderDetail>() : input.PPOrderDetails));
            _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
            //ConfigGridBase(input, uGridControl, pnlUgrid);

            this.ConfigGridByTemplete_General<PPOrder>(pnlUgrid, mauGiaoDien);
            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
            this.ConfigGridByManyTemplete<PPOrder>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
            List<PPOrder> select = new List<PPOrder> { input };
            _select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmount;
            _select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add ? "VND" : input.CurrencyID;
            if (input.HasProperty("TypeID") && input.HasProperty("Exported"))
            {
                int type = int.Parse(_select.GetProperty("TypeID").ToString());
                var isExported = (bool)_select.GetProperty("Exported");
                var ultraGrid = (UltraGrid)pnlUgrid.Controls.Find("uGrid1", true).FirstOrDefault();
                ultraGrid.ConfigColumnByExported(isExported, _select.GetProperty("CurrencyID").ToString(), type);
                //ultraGrid.ConfigColumnByImportPurchase(isExported, _select.GetProperty("CurrencyID").ToString(), _statusForm);
            }
            
            this.ConfigGridCurrencyByTemplate<PPOrder>(_select.TypeID, new BindingList<System.Collections.IList> { select },
                                              uGridControl, mauGiaoDien);
            Utils.isPPInvoice = true;
            //config combo
            //BindingList<AccountingObject> bindingList = new BindingList<AccountingObject>();
            //Utils.AddToBindingList<AccountingObject>(Utils.ListAccountingObject.Where(x => (x.ObjectType == 1 || x.ObjectType == 2) && x.IsActive).ToList(), bindingList);
            //this.ConfigCombo(bindingList, cbbAccountingObjectID, "AccountingObjectCode", "ID", input, "AccountingObjectID");
            this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 1);
            //Databinding
            DataBinding(new List<Control> { txtAccountingObjectName, txtAccountingObjectAddress, txtCompanyTaxCode, txtShippingPlace, txtReason }, "AccountingObjectName, AccountingObjectAddress, CompanyTaxCode, ShippingPlace, Reason");
            DataBinding(new List<Control> { lblTotalAmountOriginal, lblTotalVATAmountOriginal, lblTotalPaymentAmountOriginal }, "TotalAmountOriginal, TotalVATAmountOriginal, TotalPaymentAmountOriginal", "3,3,3", true);
            DataBinding(new List<Control> { lblTotalAmount, lblTotalVATAmount, lblTotalPaymentAmount }, "TotalAmount, TotalVATAmount, TotalPaymentAmount", "3,3,3");
            ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
            ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);

            ugrid0 = Controls.Find("uGrid0", true).FirstOrDefault() as UltraGrid;
            ugrid0.DisplayLayout.Bands[0].Columns["QuantityReceipt"].CellActivation = Activation.NoEdit;// edit by tungnt: khóa cột số lương nhận
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 200)//trungnq thêm vào để  làm task 6234
                {
                    txtReason.Value = "Đơn mua hàng";
                    txtReason.Text = "Đơn mua hàng";
                    _select.Reason = "Đơn mua hàng";
                };
            }
        }
        #endregion

        private void btnOriginalVoucher_Click(object sender, System.EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucher>();
                    var f = new FViewVoucherOriginal(_select.CurrencyID, datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void FPPOrderDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        private void txtAccountingObjectName_TextChanged(object sender, EventArgs e)
        {
            UltraTextEditor txt = (UltraTextEditor)sender;
            txt.Text = txt.Text.Replace("\r\n", "");
            if (txt.Text == "")
                txt.Multiline = true;
            else
                txt.Multiline = false;
        }
    }

    public class FppOrderDetailTmp : DetailBase<PPOrder> { }
}