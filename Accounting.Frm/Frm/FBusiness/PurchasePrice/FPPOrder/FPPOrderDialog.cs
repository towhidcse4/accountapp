﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FPPOrderDialog : CustormForm //UserControl
    {

        public PPOrder PPOrder
        {
            get;
            set;
        }

        #region khởi tạo
        public FPPOrderDialog(object datasource)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu            

            LoadDuLieu(datasource);

            //chọn tự select dòng đầu tiên
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            #endregion
        }

        private void LoadDuLieu(object datasource)
        {

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = datasource;

            ConfigGrid(uGrid);
            #endregion
        }
        #endregion

        #region Nghiệp vụ

        

        private void UGridMouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        #endregion

        #region Utils
        void ConfigGrid(UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, ConstDatabase.PPOrder_TableName,false);            
            utralGrid.DisplayLayout.Bands[0].Columns["Date"].Format = "dd/MM/yyyy";
            utralGrid.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            UltraGridColumn checkColumn = utralGrid.DisplayLayout.Bands[0].Columns.Add("Checkbox");

            checkColumn.DataType = typeof(Boolean);
            checkColumn.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            checkColumn.CellActivation = Activation.AllowEdit;
            checkColumn.Header.VisiblePosition = 0;
        }
        #endregion

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {

            if (uGrid.Selected.Rows.Count > 0)
            {
                PPOrder temp = uGrid.Selected.Rows[0].ListObject as PPOrder;
                //new FPPOrderDetail(temp, _dsPPOrder, ConstFrm.optStatusForm.View).ShowDialog(this);
                //if (!FPPOrderDetail.IsClose) LoadDuLieu();
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }

        private void uButtonViewPPOrder_Click(object sender, EventArgs e)
        {
            new FPPOrderDetail(PPOrder, null, ConstFrm.optStatusForm.View).ShowDialog(this);
        }

        private void uButtonAccept_Click(object sender, EventArgs e)
        {
            PPOrder = (uGrid.Selected.Rows.Count > 0)?uGrid.Selected.Rows[0].ListObject as PPOrder:null;
            this.Close();
        }

        private void uButtonCancel_Click(object sender, EventArgs e)
        {
            PPOrder = null;
            this.Close();
        }

        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key == "Checkbox")
            {
                e.Cell.Row.Selected = bool.Parse(e.Cell.Text);
            }
        }

    }
}
