﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FPPShoppingVouchers : Form
    {
        #region Khai báo

        private PPInvoice _ppInvoice = new PPInvoice();
        private BindingList<AccountingObject> _lstAccountingObject = Utils.ListAccountingObject;
        private List<PPInvoice> _lstPPInvoice = new List<PPInvoice>();
        private List<PPInvoiceDetail> _lstPpInvoiceDetails = new List<PPInvoiceDetail>();

        public List<PPDiscountReturnDetail> PpDiscountReturnDetailSelected { get { return _lstPPDiscountReturnDetail; } }
        List<PPDiscountReturnDetail> _lstPPDiscountReturnDetail = new List<PPDiscountReturnDetail>();
        int typeID = 0;
        List<PPDiscountReturnDetail> _lstSelectAll
        {
            get
            {
                return Utils.ListPPDiscountReturn.ToList().SelectMany(x => x.PPDiscountReturnDetails).ToList();
            }
        }
        List<PPDiscountReturn> lstPpDiscountReturnRecorded = Utils.ListPPDiscountReturn.ToList();
        #endregion

        #region Khởi tạo
        public FPPShoppingVouchers(Guid? _accountingObjectID = null, int? TypeID = null)
        {
            WaitingFrm.StartWaiting();
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion
            typeID = TypeID ?? 0;
            if (_accountingObjectID != null)
                _ppInvoice.AccountingObjectID = _accountingObjectID;
            InitializeGUI(_ppInvoice);
            WaitingFrm.StopWaiting();
        }

        #endregion

        #region override
        public void InitializeGUI(PPInvoice inputVoucher)
        {
            var accObjId = inputVoucher.AccountingObjectID.CloneObject();
            #region Lấy dữ liệu từ Database và fill vào Obj
            //_lstPPInvoice = Utils.IPPInvoiceService.GetAll();
            //Utils.IPPInvoiceService.UnbindSession(_lstPPInvoice);
            //_lstPPInvoice = Utils.IPPInvoiceService.GetAll();
            //_lstPPInvoice = _lstPPInvoice.CloneObject();
            _lstPPInvoice = Utils.ListPPInvoice.ToList().CloneObject();
            foreach (var x in Utils.ListPPService.ToList().CloneObject())
            {
                PPInvoice pp = new PPInvoice()
                {
                    ID = x.ID,
                    TypeID = x.TypeID,
                    No = x.No,
                    Date = x.Date,
                    PostedDate = x.PostedDate,
                    TotalAmount = x.TotalAmount,
                    TotalAmountOriginal = x.TotalAmountOriginal,
                    TotalVATAmount = x.TotalVATAmount,
                    TotalVATAmountOriginal = x.TotalVATAmountOriginal,
                    TotalDiscountAmount = x.TotalDiscountAmount,
                    TotalDiscountAmountOriginal = x.TotalDiscountAmountOriginal,
                    TotalAllOriginal = x.TotalAllOriginal,
                    TotalAll = x.TotalAll,
                    CurrencyID = x.CurrencyID,
                    ExchangeRate = x.ExchangeRate,
                    AccountingObjectID = x.AccountingObjectID,
                    AccountingObjectAddress = x.AccountingObjectAddress,
                    AccountingObjectName = x.AccountingObjectName,
                    BankAccountDetailID = x.BankAccountDetailID,
                    BankName = x.BankName,
                    ContactName = x.ContactName,
                    EmployeeID = x.EmployeeID,
                    CreditCardNumber = x.CreditCardNumber,
                    DueDate = x.DueDate,
                    Exported = x.Exported

                };
                _lstPPInvoice.Add(pp);
            }
            _lstPpInvoiceDetails = Utils.ListPPInvoice.SelectMany(x => x.PPInvoiceDetails).ToList().CloneObject();
            foreach (var x in Utils.ListPPService.SelectMany(c => c.PPServiceDetails).ToList().CloneObject())
            {
                PPInvoiceDetail ppdetail = new PPInvoiceDetail()
                {
                    ID = x.ID,
                    PPInvoiceID = x.PPServiceID,
                    MaterialGoodsID = x.MaterialGoodsID,
                    Quantity = x.Quantity,
                    UnitPrice = x.UnitPrice,
                    UnitPriceOriginal = x.UnitPriceOriginal,
                    Amount = x.Amount ?? 0,
                    AmountOriginal = x.AmountOriginal ?? 0,
                    DiscountAmount = x.DiscountAmount ?? 0,
                    DiscountAmountOriginal = x.DiscountAmountOriginal ?? 0,
                    VATAmount = x.VATAmount ?? 0,
                    VATAmountOriginal = x.VATAmountOriginal ?? 0,
                    VATAccount = x.VATAccount,
                    VATDescription = x.VATDescription,
                    Description = x.Description,
                    DiscountRate = x.DiscountRate,
                    VATRate = x.VATRate,
                    DebitAccount = x.DebitAccount,
                    CreditAccount = x.CreditAccount,
                    DepartmentID = x.DepartmentID,
                    ExpenseItemID = x.ExpenseItemID,
                    CostSetID = x.CostSetID,
                    BudgetItemID = x.BudgetItemID,
                    StatisticsCodeID = x.StatisticsCodeID,
                    AccountingObjectTaxID = x.AccountingObjectTaxID,
                    AccountingObjectID = x.AccountingObjectID,
                    AccountingObjectTaxName = x.AccountingObjectTaxName,
                    ContractID = x.ContractID,
                    InvoiceDate = x.InvoiceDate,
                    InvoiceNo = x.InvoiceNo,
                    InvoiceSeries = x.InvoiceSeries,
                    InvoiceTemplate = x.InvoiceTemplate,
                    InvoiceTypeID = x.InvoiceTypeID
                };
                _lstPpInvoiceDetails.Add(ppdetail);
            }
            foreach (var ppInvoice in _lstPPInvoice)
            {
                ppInvoice.PPInvoiceDetails = _lstPpInvoiceDetails.Where(x => x.PPInvoiceID == ppInvoice.ID && (x.Quantity - (_lstSelectAll.Where(y => y.ConfrontDetailID == x.ID && lstPpDiscountReturnRecorded.Any(z => z.ID == y.PPDiscountReturnID && z.TypeID == typeID)).Sum(y => y.Quantity)) > 0)).ToList();
            }
            #endregion

            this.ConfigCombo(_lstAccountingObject, cbbAccountingObj, "AccountingObjectCode", "ID", accountingObjectType: 1);
            uGridInvoice.DataSource = new List<PPInvoice>();
            Utils.ConfigGrid(uGridInvoice, ConstDatabase.PPInvoice_TableName_FPPShoppingVouchers);
            uGridInvoice.DisplayLayout.Bands[0].Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridInvoice.DisplayLayout.Bands[0].Summaries.Clear();
            foreach (var col in uGridInvoice.DisplayLayout.Bands[0].Columns)
            {
                col.CellActivation = Activation.NoEdit;
                col.Header.Appearance.TextHAlign = HAlign.Center;
            }

            uGridInvoiceDetail.DataSource = new List<PPInvoiceDetail>();
            Utils.ConfigGrid(uGridInvoiceDetail, ConstDatabase.PPInvoiceDetail_TableName_FPPShoppingVouchers);
            uGridInvoiceDetail.DisplayLayout.Bands[0].Columns["Quantity"].FormatNumberic(ConstDatabase.Format_Quantity);
            uGridInvoiceDetail.DisplayLayout.Bands[0].Summaries.Clear();
            foreach (var col in uGridInvoiceDetail.DisplayLayout.Bands[0].Columns)
            {
                col.CellActivation = Activation.NoEdit;
                col.Header.Appearance.TextHAlign = HAlign.Center;
            }

            uGridListed.DataSource = new List<PPDiscountReturnDetail>();
            Utils.ConfigGrid(uGridListed, ConstDatabase.PPDiscountReturnDetail_TableName_FPPShoppingVouchers);
            uGridListed.DisplayLayout.Bands[0].Summaries.Clear();
            foreach (var col in uGridListed.DisplayLayout.Bands[0].Columns)
            {
                col.CellActivation = Activation.NoEdit;
                col.Header.Appearance.TextHAlign = HAlign.Center;
            }
            if (inputVoucher.AccountingObjectID != null)
                cbbAccountingObj.SelectedRow =
                    cbbAccountingObj.Rows.FirstOrDefault(
                        r => (Guid?)r.Cells["ID"].Value == accObjId);
        }
        #endregion

        #region Utils

        void SetDataforuGridInvoiceDetail(UltraGrid ultraGrid)
        {
            PPInvoice model = (PPInvoice)ultraGrid.ActiveRow.ListObject;
            List<PPInvoiceDetail> temp = model.PPInvoiceDetails.ToList();
            if (temp.Count > 0)
            {
                foreach (var item in temp)
                {
                    item.Quantity = item.Quantity - _lstSelectAll.Where(y => y.ConfrontDetailID == item.ID && lstPpDiscountReturnRecorded.Any(z => z.ID == y.PPDiscountReturnID && z.TypeID == typeID)).Sum(y => y.Quantity);
                    item.MaterialGoods = Utils.ListMaterialGoodsCustom.FirstOrDefault(x => x.ID == item.MaterialGoodsID);
                }
                uGridInvoiceDetail.DataSource = temp;
            }
            else
            {
                uGridInvoiceDetail.DataSource = new List<PPInvoiceDetail>();
            }
            CheckSelected();
        }

        void CheckSelected()
        {
            btnUnSelected.Enabled = uGridListed.Rows.Count > 0 && uGridListed.ActiveRow != null;
            btnUnSelected.Appearance.FontData.Bold = (uGridListed.Rows.Count > 0 && uGridListed.ActiveRow != null)
                                                         ? DefaultableBoolean.True
                                                         : DefaultableBoolean.False;
            btnUnSelectedAll.Enabled = uGridListed.Rows.Count > 0;
            btnUnSelectedAll.Appearance.FontData.Bold = uGridListed.Rows.Count > 0
                                                            ? DefaultableBoolean.True
                                                            : DefaultableBoolean.False;
            btnSelected.Enabled = uGridInvoiceDetail.Rows.Count > 0 && uGridInvoiceDetail.ActiveRow != null;
            btnSelected.Appearance.FontData.Bold = (uGridInvoiceDetail.Rows.Count > 0 &&
                                                    uGridInvoiceDetail.ActiveRow != null)
                                                       ? DefaultableBoolean.True
                                                       : DefaultableBoolean.False;
            btnSelectedAll.Enabled = uGridInvoiceDetail.Rows.Count > 0;
            btnSelectedAll.Appearance.FontData.Bold = uGridInvoiceDetail.Rows.Count > 0
                                                       ? DefaultableBoolean.True
                                                       : DefaultableBoolean.False;
        }

        void AddDataToGrid(PPInvoice ppInvoice, List<PPInvoiceDetail> lstppInvoiceDetail)
        {
            int? indexActive = null;
            if (lstppInvoiceDetail.Count == 1)
                indexActive = ((List<PPInvoiceDetail>)uGridInvoiceDetail.DataSource).LastIndexOf(lstppInvoiceDetail[0]).CloneObject();
            var temp = lstppInvoiceDetail.Select(p => new PPDiscountReturnDetail()
            {
                RepositoryID = p.RepositoryID,
                Description = p.Description,
                DebitAccount = p.DebitAccount,
                CreditAccount = p.CreditAccount,
                Unit = p.Unit,
                Quantity = p.Quantity,
                QuantityConvert = p.QuantityConvert,
                UnitPrice = ((p.Amount - p.DiscountAmount) / p.Quantity),
                UnitPriceOriginal = ((p.AmountOriginal - p.DiscountAmountOriginal) / p.Quantity),
                UnitPriceConvert = p.UnitPriceConvert,
                UnitPriceConvertOriginal = p.UnitPriceConvertOriginal,
                Amount = (p.Amount - p.DiscountAmount),
                AmountOriginal = (p.AmountOriginal - p.DiscountAmountOriginal),
                VATRate = p.VATRate,
                VATAmount = p.VATAmount,
                VATAmountOriginal = p.VATAmountOriginal,
                VATAccount = p.VATAccount,
                ConfrontDetailID = p.ID == Guid.Empty ? (Guid?)null : p.ID,
                ConfrontID = p.PPInvoiceID == Guid.Empty ? (Guid?)null : p.PPInvoiceID,
                Date = ppInvoice.Date,
                No = ppInvoice.No,
                ConfrontInvDate = p.InvoiceDate,
                ConfrontInvNo = p.InvoiceNo,
                GoodsServicePurchaseID = p.GoodsServicePurchaseID,
                AccountingObjectID = ppInvoice.AccountingObjectID,
                BudgetItemID = p.BudgetItemID,
                CostSetID = p.CostSetID,
                ContractID = p.ContractID,
                StatisticsCodeID = p.StatisticsCodeID,
                DepartmentID = p.DepartmentID,
                ExpiryDate = p.ExpiryDate,
                LotNo = p.LotNo,
                UnitConvert = p.UnitConvert,
                AccountingObjectTaxID = p.AccountingObjectTaxID,
                AccountingObjectTaxName = p.AccountingObjectTaxName,
                ConvertRate = p.ConvertRate,
                ExpenseItemID = p.ExpenseItemID,
                OrderPriority = p.OrderPriority,
                MaterialGoodsID = p.MaterialGoodsID,
                MaterialGoods = p.MaterialGoods,
                PPInvoice = ppInvoice
            });
            _lstPPDiscountReturnDetail.AddRange(temp.ToList());
            foreach (var item in lstppInvoiceDetail)
            {
                int index = _lstPPInvoice.IndexOf(ppInvoice);
                _lstPPInvoice[index].PPInvoiceDetails.Remove(item);
            }
            uGridListed.DataSource = _lstPPDiscountReturnDetail;
            uGridListed.DataBind();
            SetDataforuGridInvoiceDetail(uGridInvoice);
            if (indexActive.HasValue && uGridInvoiceDetail.Rows.Count > 0)
            {
                try
                {
                    uGridInvoiceDetail.ActiveRow = uGridInvoiceDetail.Rows[indexActive.Value];
                }
                catch
                {

                }
            }
        }

        void RemoveDataFromGrid(List<PPDiscountReturnDetail> lstPPDiscountReturnDetail)
        {
            int? indexActive = null;
            if (lstPPDiscountReturnDetail.Count == 1)
                indexActive = ((List<PPDiscountReturnDetail>)uGridListed.DataSource).LastIndexOf(lstPPDiscountReturnDetail[0]).CloneObject();
            List<PPInvoiceDetail> temp = SplitListObject(lstPPDiscountReturnDetail);
            if (lstPPDiscountReturnDetail.Count > 1)
            {
                for (int i = 0; i < lstPPDiscountReturnDetail.Count; i++)
                {
                    int index = _lstPPInvoice.IndexOf(lstPPDiscountReturnDetail[i].PPInvoice);
                    _lstPPInvoice[index].PPInvoiceDetails.Add(temp[i]);
                }
                _lstPPDiscountReturnDetail.Clear();
            }
            else
            {
                for (int i = 0; i < lstPPDiscountReturnDetail.Count; i++)
                {
                    int index = _lstPPInvoice.IndexOf(lstPPDiscountReturnDetail[i].PPInvoice);
                    _lstPPInvoice[index].PPInvoiceDetails.Add(temp[i]);
                    _lstPPDiscountReturnDetail.Remove(lstPPDiscountReturnDetail[i]);
                }
            }
            uGridListed.DataSource = _lstPPDiscountReturnDetail;
            uGridListed.DataBind();
            SetDataforuGridInvoiceDetail(uGridInvoice);
            if (indexActive.HasValue && uGridListed.Rows.Count > 0 && indexActive < uGridListed.Rows.Count) uGridListed.ActiveRow = uGridListed.Rows[indexActive.Value];
        }

        private List<PPInvoiceDetail> SplitListObject(List<PPDiscountReturnDetail> lstPPDiscountReturnDetail)
        {
            return lstPPDiscountReturnDetail.Select(p => new PPInvoiceDetail()
            {
                ID = p.ConfrontDetailID ?? Guid.Empty,
                RepositoryID = p.RepositoryID,
                Description = p.Description,
                DebitAccount = p.DebitAccount,
                CreditAccount = p.CreditAccount,
                Unit = p.Unit,
                Quantity = p.Quantity,
                QuantityConvert = p.QuantityConvert,
                UnitPrice = p.UnitPrice == null ? 0 : (decimal)p.UnitPrice,
                UnitPriceOriginal = p.UnitPriceOriginal == null ? 0 : (decimal)p.UnitPriceOriginal,
                UnitPriceConvert = p.UnitPriceConvert == null ? 0 : (decimal)p.UnitPriceConvert,
                UnitPriceConvertOriginal = p.UnitPriceConvertOriginal == null ? 0 : (decimal)p.UnitPriceConvertOriginal,
                Amount = p.Amount == null ? 0 : (decimal)p.Amount,
                AmountOriginal = p.AmountOriginal == null ? 0 : (decimal)p.AmountOriginal,
                VATRate = p.VATRate,
                VATAmount = p.VATAmount == null ? 0 : (decimal)p.VATAmount,
                VATAmountOriginal = p.VATAmountOriginal == null ? 0 : (decimal)p.VATAmountOriginal,
                VATAccount = p.VATAccount,
                PPInvoiceID = p.ConfrontID ?? Guid.Empty,
                GoodsServicePurchaseID = p.GoodsServicePurchaseID,
                AccountingObjectID = p.AccountingObjectID,
                BudgetItemID = p.BudgetItemID,
                InvoiceNo = p.ConfrontInvNo,
                InvoiceDate = p.ConfrontInvDate,
                CostSetID = p.CostSetID,
                ContractID = p.ContractID,
                StatisticsCodeID = p.StatisticsCodeID,
                DepartmentID = p.DepartmentID,
                ExpiryDate = p.ExpiryDate,
                LotNo = p.LotNo,
                UnitConvert = p.UnitConvert,
                AccountingObjectTaxID = p.AccountingObjectTaxID,
                AccountingObjectTaxName = p.AccountingObjectTaxName,
                ConvertRate = p.ConvertRate,
                ExpenseItemID = p.ExpenseItemID,
                OrderPriority = p.OrderPriority,
                MaterialGoodsID = p.MaterialGoodsID == null ? Guid.Empty : (Guid)p.MaterialGoodsID,
                MaterialGoods = p.MaterialGoods
            }).ToList();
        }
        #endregion

        #region Event
        private void cbbAccountingObj_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            if (e.Row == null) return;
            if (e.Row.Selected)
            {
                AccountingObject model = (AccountingObject)e.Row.ListObject;
                List<PPInvoice> temp =
                    _lstPPInvoice.Where(p => p.AccountingObjectID == model.ID && p.PPInvoiceDetails.Count > 0).ToList();
                if (temp.Count > 0)
                {
                    uGridInvoice.DataSource = temp.OrderByDescending(c => c.Date).ToArray();
                    uGridInvoice.ActiveRow = uGridInvoice.Rows[0];
                }
                else
                {
                    uGridInvoice.DataSource = new List<PPInvoice>();
                    uGridInvoiceDetail.DataSource = new List<PPInvoiceDetail>();
                }
            }

        }

        private void uGridInvoice_AfterRowActivate(object sender, EventArgs e)
        {
            UltraGrid ultraGrid = (UltraGrid)sender;
            SetDataforuGridInvoiceDetail(ultraGrid);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void uGridInvoiceDetail_DoubleClick(object sender, EventArgs e)
        {
            UltraGrid ultraGrid = (UltraGrid)sender;
            PPInvoice ppInvoice = (PPInvoice)uGridInvoice.ActiveRow.ListObject;
            List<PPInvoiceDetail> lstppInvoiceDetail = new List<PPInvoiceDetail>
                {
                    (PPInvoiceDetail) ultraGrid.ActiveRow.ListObject
                };
            AddDataToGrid(ppInvoice, lstppInvoiceDetail);
            CheckSelected();
        }

        private void btnSelected_Click(object sender, EventArgs e)
        {
            PPInvoice ppInvoice = (PPInvoice)uGridInvoice.ActiveRow.ListObject;
            List<PPInvoiceDetail> lstppInvoiceDetail = new List<PPInvoiceDetail>
                {
                    (PPInvoiceDetail) uGridInvoiceDetail.ActiveRow.ListObject
                };
            AddDataToGrid(ppInvoice, lstppInvoiceDetail);
            CheckSelected();
        }

        private void btnSelectedAll_Click(object sender, EventArgs e)
        {
            PPInvoice ppInvoice = (PPInvoice)uGridInvoice.ActiveRow.ListObject;
            List<PPInvoiceDetail> lstppInvoiceDetail = (List<PPInvoiceDetail>)uGridInvoiceDetail.DataSource;
            AddDataToGrid(ppInvoice, lstppInvoiceDetail);
            CheckSelected();
        }

        private void btnUnSelected_Click(object sender, EventArgs e)
        {
            PPDiscountReturnDetail ppDiscountReturnDetail = (PPDiscountReturnDetail)uGridListed.ActiveRow.ListObject;
            RemoveDataFromGrid(new List<PPDiscountReturnDetail> { ppDiscountReturnDetail });
            CheckSelected();
        }

        private void btnUnSelectedAll_Click(object sender, EventArgs e)
        {
            var temp = uGridListed.DataSource as List<PPDiscountReturnDetail>;
            RemoveDataFromGrid(temp);
            CheckSelected();
        }

        private void uGridInvoiceDetail_AfterRowActivate(object sender, EventArgs e)
        {
            CheckSelected();
        }

        private void uGridListed_AfterRowActivate(object sender, EventArgs e)
        {
            CheckSelected();
        }
        #endregion

    }
}
