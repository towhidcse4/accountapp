﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;
using Accounting.Core.IService;
using FX.Core;

namespace Accounting
{
    public partial class FPPDiscountReturnDetail : FppReturnDetailsStand
    {//Xử lý cách nhập đơn giá, xử lý sinh phiếu nhập xuất kho, xử lý uGridPosted, uGridTax, uGridStatistics
        #region khai báo
        BindingList<PPDiscountReturnDetail> _bdlPPDiscountReturnDetail = new BindingList<PPDiscountReturnDetail>();
        //private RSInwardOutward _rsInwardOutward = new RSInwardOutward();
        //private List<RSInwardOutwardDetail> _dsRSInwardOutwardDetail = new List<RSInwardOutwardDetail>();
        private UltraGrid uGridPosted = new UltraGrid();
        private UltraGrid uGridTax = new UltraGrid();
        private UltraGrid uGridStatistics = new UltraGrid();
        UltraTabControl _tabControl = new UltraTabControl();
        private bool _isFirst = true;
        private bool __isFirstKho = true;
        private bool checkChungTuThamChieuPhieuXuatKhoCuChua = false;
        UltraGrid ugrid2 = null;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        private IPPDiscountReturnService _IPPDiscountReturnService;
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        #endregion

        #region khởi tạo
        public FPPDiscountReturnDetail(PPDiscountReturn temp, IList<PPDiscountReturn> listObject, int statusForm)
        {
            InitializeComponent();

            base.InitializeComponent1();
            _IPPDiscountReturnService = IoC.Resolve<IPPDiscountReturnService>();
            #region Thiết lập ban đầu
            temp.TypeID = temp.TypeID == 0 ? 220 : temp.TypeID;
            _typeID = temp.TypeID; _statusForm = statusForm;
            oldTypeID = temp.TypeID;
            if (statusForm == ConstFrm.optStatusForm.Add)
                _select.TypeID = _typeID;
            else
            {
                _select = temp;
                var ppDis = _IPPDiscountReturnService.Getbykey(_select.ID);
                _select.InvoiceDate = ppDis.InvoiceDate;
                _select.InvoiceForm = ppDis.InvoiceForm;
                _select.InvoiceNo = ppDis.InvoiceNo;
                _select.InvoiceSeries = ppDis.InvoiceSeries;
                _select.InvoiceTemplate = ppDis.InvoiceTemplate;
                _select.InvoiceType = ppDis.InvoiceType;
                _select.InvoiceTypeID = ppDis.InvoiceTypeID;
            }
            if (_select.TypeID == 220) optPhieuXuatKho.Visible = true;
            _listSelects.AddRange(listObject);
            InitializeGUI(_select); //khởi tạo và hiển thị giá trị ban đầu cho các control
            ReloadToolbar(_select, listObject, statusForm);  //Change Menu Top
            #endregion
            txtNoBK.Enabled = false;
            dteDate.Enabled = false;
            txtMatHang.Enabled = false;
            if (statusForm == ConstFrm.optStatusForm.Add && Utils.ListSystemOption.FirstOrDefault(x => x.ID == 122).Data == "1") cbBill.Enabled = false;
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;
                base.WindowState = FormWindowState.Normal;

                base.Height = Screen.PrimaryScreen.WorkingArea.Height - 10;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }
        #endregion

        #region override
        public override void InitializeGUI(PPDiscountReturn input)
        {
            var isDiscount = _select.TypeID.Equals(230);
            //uGroupBoxStand.Dock = uGroupBoxEx.Dock = DockStyle.Fill;
            uGroupBoxStand.Visible = isDiscount;
            uGroupBoxEx.Visible = !isDiscount;
            palThongTinChung.Size = new Size(palThongTinChung.Size.Width, isDiscount ? 137 : 174);
            palTopSctNhtNctPXK.Visible = !isDiscount;
            optHoaDon.Visible = _select.TypeID == 220;
            cbBill.Visible = _select.TypeID == 220;
            btnSelectBill.Visible = _select.TypeID == 220;
            if (_select.TypeID == 220)
            {
                if (_statusForm != ConstFrm.optStatusForm.Add)
                {
                    optPhieuXuatKho.CheckedIndex = (_select == null || !(_select.IsDeliveryVoucher ?? false)) ? 1 : 0;
                }
                else
                {
                    if (Utils.ListSystemOption.FirstOrDefault(x => x.ID == 50).Data == "1")
                        optPhieuXuatKho.CheckedIndex = 1;
                    else optPhieuXuatKho.CheckedIndex = 0;
                }
            }

            //config số đơn hàng, ngày đơn hàng
            this.ConfigTopVouchersNo<PPDiscountReturn>(palTopSctNhtNctHoaDon, "No", "PostedDate", "Date");
            if (_select.TypeID.Equals(220) && _statusForm == ConstFrm.optStatusForm.View) this.ConfigTopVouchersNo<PPDiscountReturn>(palTopSctNhtNctPXK, "OutwardNo", "OPostedDate", "ODate", ConstFrm.TypeGroup_RSInwardOutwardOutput);
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
            }

            //Config Grid
            _listObjectInput.Add(new BindingList<PPDiscountReturnDetail>(_statusForm == ConstFrm.optStatusForm.Add ? new BindingList<PPDiscountReturnDetail>() : input.PPDiscountReturnDetails));
            //ConfigGridBase(input, uGridControl, palGrid);
            Template mauGiaoDien = Utils.GetMauGiaoDien(_select.TypeID, _select.TemplateID, Utils.ListTemplate);
            ConfigGRID(input, mauGiaoDien);
            //Config
            if (_select.TypeID.Equals(230))
            {
                //config combo
                Utils.isPPInvoice = true;
                this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObject, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 1);
                //Databinding
                DataBinding(new List<Control> { txtAccountingObjectName, txtAccountingObjectAddress, txtCompanyTaxCode, txtSContactName, txtReasonDiscount }, "AccountingObjectName, AccountingObjectAddress, CompanyTaxCode, OContactName, Reason");
                txtKyHieuHDI.TextChanged += txtKyHieuHDI_TextChansged;
                txtMauSoHDI.TextChanged += txtMauSoHDI_TextChanged;
                txtSoHDI.TextChanged += txtSoHDI_TextChanged;
                dteNgayHDI.ValueChanged += dteNgayHDI_ValueChanged;
            }
            else
            {
                //config combo
                Utils.isPPInvoice = true;
                //this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectHD, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 1);
                //this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectXK, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 1);
                //this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectHD1, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 1);
                //this.ConfigCombo(Utils.ListAccountingObjectBank, cbbAccountingObjectBankAccount, "BankAccount", "BankAccount", input, "AccountingObjectBankAccount");
                this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectHD, "AccountingObjectID", cbbAccountingObjectBankAccount, "AccountingObjectBankAccount", accountingObjectType: 1);
                this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectXK, "AccountingObjectID", cbbAccountingObjectBankAccount, "AccountingObjectBankAccount", accountingObjectType: 1);
                this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectHD1, "AccountingObjectID", cbbAccountingObjectBankAccount, "AccountingObjectBankAccount", accountingObjectType: 1);
                //Databinding
                DataBinding(new List<Control> { txtAccountingObjectNameHD, txtAccountingObjectAddressHD, txtCompanyTaxCodeHD, txtSContactNameHD, txtReasonReturn }, "AccountingObjectName, AccountingObjectAddress, CompanyTaxCode, OContactName, Reason");
                DataBinding(new List<Control> { chkBangke, txtNoBK, dteDate, txtMatHang }, "isAttachList, ListNo, ListDate, ListCommonNameInventory", "1,0,2,0");
                DataBinding(new List<Control> { txtAccountingObjectNameHD1, txtAccountingObjectAddressHD1, txtCompanyTaxCodeHD1, txtSContactNameHD1, txtReasonReturn1 }, "AccountingObjectName, AccountingObjectAddress, CompanyTaxCode, OContactName, Reason");
                DataBinding(new List<Control> { chkBangke1, txtNoBK1, dteDate1, txtMatHang1 }, "isAttachList, ListNo, ListDate, ListCommonNameInventory", "1,0,2,0");
                DataBinding(new List<Control> { txtAccountingObjectNameXK, txtAccountingObjectAddressXK, txtCompanyTaxCodeXK, txtSContactNameXK, txtSReason }, "AccountingObjectName, AccountingObjectAddress, CompanyTaxCode, OContactName, OReason");
                DataBinding(new List<Control> { txtOriginalNo, txtAccountingBankName }, "OriginalNo, AccountingObjectBankName");
            }
            DataBinding(new List<Control> { lblTotalAmountOriginal, lblTotalVATAmountOriginal, lblTotalPaymentAmountOriginal }, "TotalAmountOriginal, TotalVATAmountOriginal, TotalPaymentAmountOriginal", "3,3,3", true);
            DataBinding(new List<Control> { lblTotalAmount, lblTotalVATAmount, lblTotalPaymentAmount }, "TotalAmount, TotalVATAmount, TotalPaymentAmount", "3,3,3");
            uGridPosted = (UltraGrid)palGrid.Controls.Find("uGrid0", true).FirstOrDefault();
            uGridTax = (UltraGrid)palGrid.Controls.Find("uGrid1", true).FirstOrDefault();
            ugrid2 = (UltraGrid)palGrid.Controls.Find("uGrid3", true).FirstOrDefault();
            ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            uGridStatistics = (UltraGrid)palGrid.Controls.Find("uGrid2", true).FirstOrDefault();
            _tabControl = (UltraTabControl)this.Controls.Find("ultraTabControl", true).FirstOrDefault();
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                //cbbCachNhapDonGia.SelectedIndex = 1;

                //_select.InvoiceType = 0;
                //_select.InvoiceForm = 0;
                //_select.InvoiceDate = null;
            }
            //else _rsInwardOutward = IRSInwardOutwardService.getMCRSInwardOutwardbyNo(input.OutwardNo);
            btnSelectBill.Enabled = cbBill.CheckState == CheckState.Checked;
            btnSelectBill.Tag = new Dictionary<string, object> { { "Enabled", cbBill.CheckState == CheckState.Checked } };
            DataBinding(new List<Control> { cbBill }, "IsAttachListBill", "1");
            if (_statusForm != ConstFrm.optStatusForm.Add)
            {

                optHoaDon.CheckedIndex = input.IsBill ? 0 : 1;

            }
            else
            {
                if (Utils.ListSystemOption.FirstOrDefault(x => x.ID == 122).Data == "1")
                    optHoaDon.CheckedIndex = 0;
                else optHoaDon.CheckedIndex = 1;
            }
            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                if (input.PaymentMethod != null && !string.IsNullOrEmpty(input.PaymentMethod) && _select.TypeID == 220) txtThanhToan.Text = input.PaymentMethod;
            }
            else
            {
                if (_select.TypeID == 220)
                    txtThanhToan.Text = "TM/CK";//add by cuongpv
            }
            if (input != null && input.BillRefID != null)
            {
                var HD = Utils.ListSABill.FirstOrDefault(x => x.ID == input.BillRefID);
                txtOldInvNo.Text = HD.InvoiceNo;
                txtOldInvDate.Text = HD.InvoiceDate == null ? "" : (HD.InvoiceDate ?? DateTime.Now).ToString("dd/MM/yyyy");
                txtOldInvTemplate.Text = HD.InvoiceTemplate;
                txtOldInvSeries.Text = HD.InvoiceSeries;
            }
            else
            {
                panel1.Visible = false;
            }
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 230)//trungnq thêm vào để  làm task 6234
                {
                    txtReasonDiscount.Value = "Giảm giá hàng mua";
                    txtReasonDiscount.Text = "Giảm giá hàng mua";
                    _select.Reason = "Giảm giá hàng mua";
                };
                if (input.TypeID == 220)//trungnq thêm vào để  làm task 6234
                {
                    txtReasonReturn.Value = "Hàng mua trả lại";
                    txtReasonReturn.Text = "Hàng mua trả lại";
                    _select.Reason = "Hàng mua trả lại";
                    txtSReason.Value = "Xuất kho trả lại";
                    txtSReason.Text = "Xuất kho trả lại";
                    _select.OReason = "Xuất kho trả lại";
                };
            }

            AddCbb(input);
            AddData2(input);
        }

        protected override void SetOrGetGuiObject(bool isGet)
        {
            //
        }

        #endregion

        #region Utils
        private void ConfigGRID(PPDiscountReturn inputVoucher, Template mauGiaoDien)
        {
            #region Grid Thay đổi động
            //mặc định là việt nam đồng
            //Currency _Currency = _ICurrencyService.Query.Where(k => k.ID.Equals("VND")).Single();
            _select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : inputVoucher.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : inputVoucher.TotalAmount;
            _select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add ? "VND" : inputVoucher.CurrencyID;

            List<PPDiscountReturn> input = new List<PPDiscountReturn> { _select };
            this.ConfigGridCurrencyByTemplate<PPDiscountReturn>(_select.TypeID, new BindingList<System.Collections.IList> { input },
                                              uGridControl, mauGiaoDien);
            #endregion

            _bdlPPDiscountReturnDetail = new BindingList<PPDiscountReturnDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                       ? new List<PPDiscountReturnDetail>()
                                                                       : inputVoucher.PPDiscountReturnDetails);
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                bdlRefVoucher = new BindingList<RefVoucher>(inputVoucher.RefVouchers);
            }

            if (_bdlPPDiscountReturnDetail.Count > 0)
            {
                var lstPpInvoice = IPPInvoiceService.GetAll();
                foreach (var item in _bdlPPDiscountReturnDetail)
                {
                    var ppInvoice = lstPpInvoice.FirstOrDefault(x => x.ID == item.ConfrontID);
                    if (ppInvoice != null)
                    {
                        item.No = ppInvoice.No;
                        item.Date = ppInvoice.Date;
                    }
                }
            }
            _listObjectInput = new BindingList<System.Collections.IList> { _bdlPPDiscountReturnDetail };
            _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
            #region Phần chung config theo Templete

            this.ConfigGridByTemplete_General<PPDiscountReturn>(palGrid, mauGiaoDien);
            //this.ConfigGridByTemplete<PPDiscountReturn>(inputVoucher.TypeID, mauGiaoDien, true, _listObjectInput);
            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
            this.ConfigGridByManyTemplete<PPDiscountReturn>(inputVoucher.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
            #endregion
        }
        #endregion

        #region Event
        #region Event Chung


        private void SetLockEditColumn(bool isLock)
        {
            uGridPosted = (UltraGrid)Controls.Find("uGrid0", true).FirstOrDefault();
            if (uGridPosted == null) return;
            if (!uGridPosted.DisplayLayout.Bands[0].Columns.Exists("UnitPrice") ||
                !uGridPosted.DisplayLayout.Bands[0].Columns.Exists("UnitPriceOriginal") ||
                !uGridPosted.DisplayLayout.Bands[0].Columns.Exists("Amount") ||
                !uGridPosted.DisplayLayout.Bands[0].Columns.Exists("AmountOriginal")) return;
            uGridPosted.DisplayLayout.Bands[0].Columns["UnitPrice"].LockColumn(isLock);
            uGridPosted.DisplayLayout.Bands[0].Columns["UnitPriceOriginal"].LockColumn(isLock);
            uGridPosted.DisplayLayout.Bands[0].Columns["Amount"].LockColumn(isLock);
            uGridPosted.DisplayLayout.Bands[0].Columns["AmountOriginal"].LockColumn(isLock);
        }

        private void ultraTabControlThongTinChung_ActiveTabChanged(object sender, Infragistics.Win.UltraWinTabControl.ActiveTabChangedEventArgs e)
        {
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceForm"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTypeID"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;
            uGridControl.ConfigSizeGridCurrency(this, false);
            palTopSctNhtNctHoaDon.Visible = false;
            palTopSctNhtNctPXK.Visible = false;
            switch (e.Tab.Key)
            {
                case "tabHoaDon":
                    palTopSctNhtNctHoaDon.Visible = true;
                    break;
                case "tabPhieuXuatKho":
                    palTopSctNhtNctPXK.Visible = true;
                    break;
            }
            bool value = (e.Tab.Key.Equals("tabHoaDon1"));
            if (uGridControl != null)
            {
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceForm"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTypeID"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = !value;
                //uGridControl.ConfigSizeGridCurrency(this, false);
            }
        }

        private void btnVouchers_Click(object sender, EventArgs e)
        {
            try
            {
                using (var frm = new FPPShoppingVouchers(_select.AccountingObjectID, _select.TypeID))
                {
                    frm.FormClosed += FPPShoppingVouchers_FormClosed;
                    frm.ShowDialog(this);
                }
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }

        void FPPShoppingVouchers_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (FPPShoppingVouchers)sender;
            if (e.CloseReason == CloseReason.UserClosing)
                if (frm.DialogResult == DialogResult.OK)
                {
                    List<PPDiscountReturnDetail> temp = frm.PpDiscountReturnDetailSelected;
                    _select.PPDiscountReturnDetails = temp;
                    if (temp != null && temp.Count > 0)
                    {
                        _bdlPPDiscountReturnDetail.Clear();
                        foreach (PPDiscountReturnDetail item in temp)
                        {
                            // edit by tungnt: gán lại TK nợ, TK có từ hóa đơn mua hàng
                            string debitAcc = item.DebitAccount;
                            string creditAcc = item.CreditAccount;
                            item.CreditAccount = debitAcc;
                            item.DebitAccount = creditAcc;
                            _bdlPPDiscountReturnDetail.Add(item.CloneObject());
                            _select.CurrencyID = item.PPInvoice.CurrencyID;
                            _select.ExchangeRate = item.PPInvoice.ExchangeRate;

                        }
                        var tabControl = Controls.Find("ultraTabControl", true).FirstOrDefault() as UltraTabControl;
                        if (tabControl != null)
                        {
                            foreach (var tab in tabControl.Tabs)
                            {
                                var uGrid = Controls.Find(string.Format("uGrid{0}", tab.Index), true).FirstOrDefault() as UltraGrid;
                                if (uGrid != null)
                                {
                                    uGrid.DataBind();
                                    if (tab.Index == 0)
                                    {
                                        foreach (var row in uGrid.Rows)
                                        {
                                            uGrid.ActiveCell = row.Cells["UnitPriceOriginal"];
                                            this.DoEverythingInGrid<PPDiscountReturn>(uGrid);
                                            var model = Utils.ListMaterialGoodTools.FirstOrDefault(x => x.ID == (Guid)row.Cells["MaterialGoodsID"].Value);
                                            Utils.uGrid1_AfterCellUpdate<PPDiscountReturn>(this, uGrid, model, row.Index);
                                        }
                                    }
                                }
                            }
                        }
                        var uGridControl = Controls.Find("uGridControl", true).FirstOrDefault() as UltraGrid;
                        uGridControl.Rows[0].Cells["CurrencyID"].Value = _select.CurrencyID;
                        uGridControl.Rows[0].Cells["ExchangeRate"].Hidden = _select.CurrencyID == "VND" ? true : false;
                        uGridControl.Rows[0].Cells["ExchangeRate"].Value = _select.ExchangeRate;
                        AccountingObject _accTemp = Utils.IAccountingObjectService.Query.FirstOrDefault(p => p.ID == temp[0].AccountingObjectID);
                        //int index =
                        //    Utils.ListAccountingObject.IndexOf(_accTemp);
                        if (_select.TypeID == 220)
                        {
                            //Cấu hình tab chứng từ
                            this.ActiveControl = cbbAccountingObjectHD;
                            cbbAccountingObjectHD.DataSource = new BindingList<AccountingObject>(new List<AccountingObject> { _accTemp });
                            cbbAccountingObjectHD.SelectedRow = cbbAccountingObjectHD.Rows[0];
                            _accTemp.ExcuteRowSelectedEventOfcbbAutoFilter(this, cbbAccountingObjectHD, true);
                            //Cấu hình tab xuất kho
                            this.ActiveControl = cbbAccountingObjectXK;
                            cbbAccountingObjectXK.DataSource = new BindingList<AccountingObject>(new List<AccountingObject> { _accTemp });
                            cbbAccountingObjectXK.SelectedRow = cbbAccountingObjectXK.Rows[0];
                            _accTemp.ExcuteRowSelectedEventOfcbbAutoFilter(this, cbbAccountingObjectXK, true);
                            //Cấu hình tab hoá đơn
                            this.ActiveControl = cbbAccountingObjectHD1;
                            cbbAccountingObjectHD1.DataSource = new BindingList<AccountingObject>(new List<AccountingObject> { _accTemp });
                            cbbAccountingObjectHD1.SelectedRow = cbbAccountingObjectHD1.Rows[0];
                            _accTemp.ExcuteRowSelectedEventOfcbbAutoFilter(this, cbbAccountingObjectHD1, true);
                        }
                        else
                        {
                            this.ActiveControl = cbbAccountingObject;
                            //cbbAccountingObject.SelectedRow = cbbAccountingObject.Rows[index];
                            cbbAccountingObject.DataSource = new BindingList<AccountingObject>(new List<AccountingObject> { _accTemp });
                            cbbAccountingObject.SelectedRow = cbbAccountingObject.Rows[0];
                            _accTemp.ExcuteRowSelectedEventOfcbbAutoFilter(this, cbbAccountingObject, true);
                        }
                    }
                }
        }
        #endregion


        private void txtBottom_TextChanged(object sender, EventArgs e)
        {
            UltraTextEditor txt = (UltraTextEditor)sender;
            if (!string.IsNullOrEmpty(txt.Text))
            {
                Utils.FormatNumberic(txt, txt.Text, ConstDatabase.Format_TienVND);
            }
        }

        private void txtOriginalBottom_TextChanged(object sender, EventArgs e)
        {
            UltraTextEditor txt = (UltraTextEditor)sender;
            if (!string.IsNullOrEmpty(txt.Text))
                Utils.FormatNumberic(txt, txt.Text, ConstDatabase.Format_TienVND);
        }

        private void chkBangke_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBangke.CheckState == CheckState.Checked)
            {
                txtNoBK.Enabled = true;
                dteDate.Enabled = true;
                txtMatHang.Enabled = true;
            }
            else
            {
                txtNoBK.Enabled = false;
                dteDate.Enabled = false;
                txtMatHang.Enabled = false;
            }

        }
        #endregion

        private void optHoaDon_ValueChanged(object sender, EventArgs e)
        {
            if (optHoaDon.CheckedIndex == 1)
            {
                _select.InvoiceForm = null;
                _select.InvoiceTypeID = null;
                _select.InvoiceTemplate = null;
                _select.InvoiceSeries = null;
                _select.InvoiceNo = null;
                _select.InvoiceDate = null;
            }
            uGroupBoxEx.Tabs["tabHoaDon1"].Visible = optHoaDon.CheckedIndex == 0;
            cbBill.Enabled = optHoaDon.CheckedIndex == 1;
            if (uGridControl != null && optHoaDon.CheckedIndex == 1)
            {
                if (_statusForm == ConstFrm.optStatusForm.Edit)
                {
                    if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "HDDT").Data == "1" && !_select.InvoiceNo.IsNullOrEmpty() && _select.StatusInvoice == 1)
                    {
                        MSG.Warning("Không thể thực hiện thao tác này! Hệ thống đang được tích hợp hóa đơn điện tử và hóa đơn này đã được phát hành");
                        optHoaDon.CheckedIndex = 0;
                    }
                    else
                    {
                        //List<RefVoucher> listrefVouchers = Utils.ListRefVoucher.Where(n => n.RefID1 == _select.ID || n.RefID2 == _select.ID).ToList();
                        //if (listrefVouchers.Count != 0)
                        //{
                        //    if (MSG.Question("Chứng từ này đã phát sinh liên kết với chứng từ khác. Thực hiện sửa đổi chứng từ sẽ phải xóa bỏ toàn bộ liên kết. Bạn có muốn tiếp tục ?") == System.Windows.Forms.DialogResult.Yes)
                        //    {
                        //        uGridControl.DisplayLayout.Bands[0].Columns["InvoiceForm"].Hidden = true;
                        //        uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTypeID"].Hidden = true;
                        //        uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
                        //        uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
                        //        uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
                        //        uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;
                        //        uGridControl.ConfigSizeGridCurrency(this, false);
                        //        _select.InvoiceForm = null;
                        //        _select.InvoiceTypeID = null;
                        //        _select.InvoiceTemplate = null;
                        //        _select.InvoiceSeries = null;
                        //        _select.InvoiceNo = null;
                        //        _select.InvoiceDate = null;
                        //    }
                        //    else
                        //    {
                        //        optHoaDon.CheckedIndex = 0;
                        //    }
                        //}
                        //else
                        //{
                        uGridControl.DisplayLayout.Bands[0].Columns["InvoiceForm"].Hidden = true;
                        uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTypeID"].Hidden = true;
                        uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
                        uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
                        uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
                        uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;
                        uGridControl.ConfigSizeGridCurrency(this, false);
                        _select.InvoiceForm = (_select.InvoiceForm != null) ? _select.InvoiceForm : null;
                        _select.InvoiceTypeID = (_select.InvoiceTypeID != null) ? _select.InvoiceTypeID : null;
                        _select.InvoiceTemplate = (_select.InvoiceTemplate != null) ? _select.InvoiceTemplate : null;
                        _select.InvoiceSeries = (_select.InvoiceSeries != null) ? _select.InvoiceSeries : null;
                        _select.InvoiceNo = (_select.InvoiceNo != null) ? _select.InvoiceNo : null;
                        _select.InvoiceDate = (_select.InvoiceDate != null) ? _select.InvoiceDate : null;
                        //}
                    }
                }
                else
                {
                    uGridControl.DisplayLayout.Bands[0].Columns["InvoiceForm"].Hidden = true;
                    uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTypeID"].Hidden = true;
                    uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
                    uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
                    uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
                    uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;
                    uGridControl.ConfigSizeGridCurrency(this, false);
                    _select.InvoiceForm = (_select.InvoiceForm != null) ? _select.InvoiceForm : null;
                    _select.InvoiceTypeID = (_select.InvoiceTypeID != null) ? _select.InvoiceTypeID : null;
                    _select.InvoiceTemplate = (_select.InvoiceTemplate != null) ? _select.InvoiceTemplate : null;
                    _select.InvoiceSeries = (_select.InvoiceSeries != null) ? _select.InvoiceSeries : null;
                    _select.InvoiceNo = (_select.InvoiceNo != null) ? _select.InvoiceNo : null;
                    _select.InvoiceDate = (_select.InvoiceDate != null) ? _select.InvoiceDate : null;
                }
            }
            else
            {
                if (_statusForm == ConstFrm.optStatusForm.Add)
                {
                    if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "HDDT").Data == "1")
                    {
                        if (_select.InvoiceForm == null)
                        {
                            _select.InvoiceForm = 2;
                            UltraCombo ultraComboInvoiceTypeID = uGridControl.Rows[0].Cells["InvoiceTypeID"].ValueListResolved as UltraCombo;
                            if (ultraComboInvoiceTypeID != null && ultraComboInvoiceTypeID.DataSource != null)
                            {
                                List<InvoiceType> lst = ultraComboInvoiceTypeID.DataSource as List<InvoiceType>;
                                if (lst.Count == 1)
                                {
                                    _select.InvoiceTypeID = lst.First().ID;
                                    UltraCombo ultraComboInvoiceTemplate = uGridControl.Rows[0].Cells["InvoiceTemplate"].ValueList as UltraCombo;
                                    if (ultraComboInvoiceTemplate != null && ultraComboInvoiceTemplate.DataSource != null)
                                    {
                                        List<TT153Report> tT153Reports = ultraComboInvoiceTemplate.DataSource as List<TT153Report>;
                                        if (tT153Reports.Count == 1)
                                        {
                                            _select.InvoiceTemplate = tT153Reports.First().InvoiceTemplate;
                                            _select.InvoiceSeries = tT153Reports.First().InvoiceSeries;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                _select.InvoiceForm = (_select.InvoiceForm != null) ? _select.InvoiceForm : null;
                                _select.InvoiceTypeID = (_select.InvoiceTypeID != null) ? _select.InvoiceTypeID : null;
                                _select.InvoiceTemplate = (_select.InvoiceTemplate != null) ? _select.InvoiceTemplate : null;
                                _select.InvoiceSeries = (_select.InvoiceSeries != null) ? _select.InvoiceSeries : null;
                                _select.InvoiceNo = (_select.InvoiceNo != null) ? _select.InvoiceNo : null;
                                _select.InvoiceDate = (_select.InvoiceDate != null) ? _select.InvoiceDate : null;
                            }
                        }
                    }
                }
            }
            if (_select.TypeID == 220)
            {
                _select.IsBill = optHoaDon.CheckedIndex == 0;
            }
            if (optHoaDon.Visible == true)
            { _select.IsBill = optHoaDon.CheckedIndex == 0; }
            UltraGrid ultraGrid = (UltraGrid)this.Controls.Find("uGrid1", true).FirstOrDefault();
            if (ultraGrid != null && ultraGrid.DisplayLayout.Bands[0].Columns.Exists("DeductionDebitAccount"))//trungnq thêm để làm bug 6453
            {
                if (ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"].Hidden == false)
                {
                    Utils.ConfigAccountColumnInGrid<PPDiscountReturn>(this, _select.TypeID, ultraGrid, ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"], isImportPurchase: false);
                }
            }
        }

        private void cbBill_CheckStateChanged(object sender, EventArgs e)
        {
            if (_statusForm != ConstFrm.optStatusForm.View)
            {
                btnSelectBill.Enabled = cbBill.CheckState == CheckState.Checked;
                optHoaDon.Enabled = cbBill.CheckState != CheckState.Checked;
            }
            btnSelectBill.Tag = new Dictionary<string, object> { { "Enabled", cbBill.CheckState == CheckState.Checked } };
        }

        private void btnSelectBill_Click(object sender, EventArgs e)
        {
            var f = new FchoseBillP<PPDiscountReturn>(_listObjectInput, _select, _select.AccountingObjectID);
            f.FormClosed += new FormClosedEventHandler(FchoseBill_FormClosed);
            try
            {
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void FchoseBill_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (FchoseBillP<PPDiscountReturn>)sender;
            if (e.CloseReason != CloseReason.UserClosing) return;
            if (frm.DialogResult == DialogResult.OK)
            {
                var selectSAs = frm._lstSA;
                if (uGridPosted != null)
                {
                    var source = (BindingList<PPDiscountReturnDetail>)uGridPosted.DataSource;
                    if (selectSAs != null) source.Clear();
                    _select.BillRefID = selectSAs.ID;
                    _select.IsAttachListBill = true;
                    foreach (var selectOrder in selectSAs.SABillDetails)
                    {
                        uGridPosted.AddNewRow4Grid();
                        var row = uGridPosted.Rows[uGridPosted.Rows.Count - 1];
                        row.Cells["MaterialGoodsID"].Value = selectOrder.MaterialGoodsID;
                        row.UpdateData();
                        uGridPosted.UpdateData();
                        uGridTax.UpdateData();
                        row.Cells["Description"].Value = selectOrder.Description;
                        row.Cells["Quantity"].Value = selectOrder.Quantity;
                        row.Cells["UnitPrice"].Value = selectOrder.UnitPrice;
                        row.Cells["UnitPriceOriginal"].Value = selectOrder.UnitPriceOriginal;
                        row.Cells["Amount"].Value = selectOrder.Amount;
                        row.Cells["AmountOriginal"].Value = selectOrder.AmountOriginal;
                        row.Cells["AccountingObjectID"].Value = selectOrder.AccountingObjectID;
                        row.Cells["VATRate"].Value = selectOrder.VATRate;
                        row.Cells["VATAmount"].Value = selectOrder.VATAmount;
                        row.Cells["VATAmountOriginal"].Value = selectOrder.VATAmountOriginal;
                        row.Cells["LotNo"].Value = selectOrder.LotNo;
                        row.Cells["ExpiryDate"].Value = selectOrder.ExpiryDate;
                        row.UpdateData();
                        uGridPosted.ActiveCell = row.Cells["Quantity"];
                        this.DoEverythingInGrid<PPDiscountReturn>(uGridPosted);
                    }
                }
                if (uGridPosted.Rows.Count > 0)
                {
                    foreach (var row in uGridPosted.Rows)
                    {
                        foreach (var cell in row.Cells)
                            Utils.CheckErrorInCell<PPDiscountReturn>(this, uGridPosted, cell);
                    }
                    foreach (var row in uGridTax.Rows)
                    {
                        Utils.CheckErrorInCell<PPDiscountReturn>(this, uGridTax, row.Cells["MaterialGoodsID"]);
                    }
                    foreach (var row in uGridStatistics.Rows)
                    {
                        Utils.CheckErrorInCell<PPDiscountReturn>(this, uGridStatistics, row.Cells["MaterialGoodsID"]);
                    }
                }
            }
        }
        private void OptPhieuXuatKhoValueChangedStand(object sender, EventArgs e)
        {//opt Phiếu xuất kho value change
            if (/*_isFirst && _statusForm == ConstFrm.optStatusForm.Add &&*/ optPhieuXuatKho.CheckedIndex == 0)
            {
                bool isreset = (__isFirstKho && _statusForm == ConstFrm.optStatusForm.Add && optPhieuXuatKho.CheckedIndex == 0);
                if (_statusForm == ConstFrm.optStatusForm.Edit && _select.IsDeliveryVoucher == false && optPhieuXuatKho.CheckedIndex == 0)
                { isreset = true; }
                this.ConfigTopVouchersNo<PPDiscountReturn>(palTopSctNhtNctPXK, "OutwardNo", "OPostedDate", "ODate", ConstFrm.TypeGroup_RSInwardOutwardOutput, resetNo: isreset);
                __isFirstKho = false;
                _backSelect.OutwardNo = _select.OutwardNo.CloneObject();
            }
            if (_statusForm == ConstFrm.optStatusForm.Edit)
            {
                if (_select.IsDeliveryVoucher == false)
                {
                    checkChungTuThamChieuPhieuXuatKhoCuChua = true;
                }
            }
            UltraOptionSet optionSet;
            try { optionSet = (UltraOptionSet)sender; if (optionSet == null || _select == null) return; }
            catch { return; }
            if (optionSet.CheckedItem.Tag.Equals("KKPXK"))
            {

                if (_statusForm == ConstFrm.optStatusForm.Edit)
                {// trường hợp này chỉ check các chứng từ nào tham chiếu đến phiếu xuất kho, khi về sau phiếu xuất kho lập kèm mở ra cho sửa thì sẽ phải check thêm trường hợp phiếu xuất kho tham chiếu đến những chứng từ nào
                    List<RefVoucherRSInwardOutward> listrefVouchersRSInwardOutward = Utils.ListRefVoucherRSInwardOutward.Where(n => n.RefID2 == _select.ID && n.TypeID.ToString().StartsWith("41") && n.No == _select.OutwardNo).ToList();
                    List<RefVoucher> listrefVouchers = Utils.ListRefVoucher.Where(n => n.RefID2 == _select.ID && n.TypeID.ToString().StartsWith("41") && n.No == _select.OutwardNo).ToList();
                    if ((listrefVouchersRSInwardOutward.Count != 0 && !checkChungTuThamChieuPhieuXuatKhoCuChua) || (listrefVouchers.Count != 0 && !checkChungTuThamChieuPhieuXuatKhoCuChua))
                    {
                        if (MSG.Question("Chứng từ này đã phát sinh liên kết với chứng từ khác. Thực hiện sửa đổi chứng từ sẽ phải xóa bỏ toàn bộ liên kết. Bạn có muốn tiếp tục ?") == System.Windows.Forms.DialogResult.Yes)
                        {
                            _select.OutwardNo = null;
                            _select.IsDeliveryVoucher = false;
                            checkChungTuThamChieuPhieuXuatKhoCuChua = true;
                        }
                        else
                        {
                            optionSet.CheckedIndex = 0;
                        }
                    }
                    else
                    {
                        _select.OutwardNo = null;
                        _select.IsDeliveryVoucher = false;
                    }
                }
                else
                {
                    _select.OutwardNo = null;
                    _select.IsDeliveryVoucher = false;

                }
            }
            else
            {

                if (!_isFirst && _select.TypeID == _backSelect.TypeID)
                {
                    _select.OutwardNo = _backSelect.OutwardNo.CloneObject();
                    _select.IsDeliveryVoucher = true;
                    var topvoucher = palTopSctNhtNctPXK.GetTopVouchers<PPDiscountReturn>();
                    if (topvoucher != null)
                    {
                        topvoucher.No = _select.OutwardNo;
                    }
                }
                else
                {
                    if (_isFirst && _statusForm != ConstFrm.optStatusForm.Add)
                    { }
                    else
                    {
                        this.ConfigTopVouchersNo<PPDiscountReturn>(palTopSctNhtNctPXK, "OutwardNo", "OPostedDate", "ODate", ConstFrm.TypeGroup_RSInwardOutwardOutput, resetNo: false);
                        _select.IsDeliveryVoucher = true;
                    }

                }

                if (_statusForm == ConstFrm.optStatusForm.Edit)
                {
                    this.ConfigTopVouchersNo<PPDiscountReturn>(palTopSctNhtNctPXK, "OutwardNo", "OPostedDate", "ODate", ConstFrm.TypeGroup_RSInwardOutwardOutput, resetNo: false);
                    _select.IsDeliveryVoucher = true;
                }

            }

            uGroupBoxEx.Tabs["tabPhieuXuatKho"].Visible = (_select.OutwardNo != null && !string.IsNullOrEmpty(_select.OutwardNo));
            UltraGrid ultraGrid = (UltraGrid)this.Controls.Find("uGrid1", true).FirstOrDefault();
            if (ultraGrid != null && ultraGrid.DisplayLayout.Bands[0].Columns.Exists("DeductionDebitAccount"))//trungnq thêm để làm bug 6453
            {
                if (ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"].Hidden == false)
                {
                    Utils.ConfigAccountColumnInGrid<PPDiscountReturn>(this, _select.TypeID, ultraGrid, ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"], isImportPurchase: false);
                }
            }
        }
        private void txtThanhToan_TextChanged(object sender, EventArgs e)
        {
            _select.PaymentMethod = txtThanhToan.Text;
        }

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {

                BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                if (datasource == null)
                    datasource = new BindingList<RefVoucher>();
                var f = new FViewVoucherOriginal(_select.CurrencyID, datasource);
                f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                try
                {
                    f.ShowDialog(this);
                }
                catch (Exception ex)
                {
                    MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                }

            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void FPPDiscountReturnDetail_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void FPPDiscountReturnDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        private void txtAccountingObjectNameHD_TextChanged(object sender, EventArgs e)
        {
            UltraTextEditor txt = (UltraTextEditor)sender;
            txt.Text = txt.Text.Replace("\r\n", "");
            if (txt.Text == "")
                txt.Multiline = true;
            else
                txt.Multiline = false;
        }
        private void AddCbb(PPDiscountReturn input)
        {
            Dictionary<int, string> listIT = new Dictionary<int, string>();
            listIT.Add(2, "Hóa đơn điện tử");
            listIT.Add(1, "Hóa đơn đặt in");
            listIT.Add(0, "Hóa đơn tự in");
            cbbHinhThucHD.DataSource = listIT;
            cbbHinhThucHD.DisplayMember = "Value";
            cbbHinhThucHD.ValueMember = "Key";//trungnq
            Utils.ConfigGrid(cbbHinhThucHD, ConstDatabase.TMIndustryTypeGH_TableName);
            cbbHinhThucHD.Value = input.InvoiceForm;
            cbbHinhThucHD.RowSelected += new RowSelectedEventHandler(combobox_selected);

            List<Guid> lstPLInvoice = Utils.ListTT153PublishInvoiceDetail.Select(n => n.InvoiceTypeID).ToList();
            var data = Utils.ListInvoiceType.Where(n => lstPLInvoice.Any(m => m == n.ID)).ToList();
            cbbLoaiHD.DataSource = data;
            cbbLoaiHD.ValueMember = "ID";
            cbbLoaiHD.DisplayMember = "InvoiceTypeName";
            Utils.ConfigGrid(cbbLoaiHD, ConstDatabase.InvoiceType_TableName);
            cbbLoaiHD.Value = input.InvoiceTypeID != null ? input.InvoiceTypeID : null;
            cbbLoaiHD.RowSelected += new RowSelectedEventHandler(combobox_selected);
            dteNgayHD.Value = input.InvoiceDate != null ? input.InvoiceDate : null;
            txtSoHD.Text = input.InvoiceNo != null ? input.InvoiceNo : null;
            cbbMauHD.Value = input.InvoiceTemplate != null ? input.InvoiceTemplate : null;
            txtKyHieuHD.Text = input.InvoiceSeries != null ? input.InvoiceSeries : null;
            if (cbbLoaiHD.Value != null && cbbHinhThucHD.Value != null)
            {
                var lstinvoice1 = Utils.ListTT153Report.Where(t => t.InvoiceTypeID == Guid.Parse(cbbLoaiHD.Value.ToString()) && t.InvoiceForm == int.Parse(cbbHinhThucHD.Value.ToString())).ToList();
                var templateInvoid = lstinvoice1.FirstOrDefault(t => t.InvoiceTemplate == input.InvoiceTemplate);
                if (templateInvoid != null)
                    cbbMauHD.Value = templateInvoid.ID;
                else
                    cbbMauHD.Value = null;
            }
            cbbMauHD.RowSelected += new RowSelectedEventHandler(combobox_selected);
        }
        private void combobox_selected(object sender, RowSelectedEventArgs e)
        {
            var cbb = (UltraCombo)sender;
            if (cbb.Name == "cbbHinhThucHD")
            {
                List<TT153Report> lstinvoice = new List<TT153Report>();
                _select.InvoiceForm = int.Parse(cbb.Value.ToString());
                if (cbbLoaiHD.Value != null)
                {
                    var lstinvoice1 = Utils.ListTT153Report.Where(t => t.InvoiceTypeID == Guid.Parse(cbbLoaiHD.Value.ToString()) && t.InvoiceForm == int.Parse(cbb.Value.ToString())).ToList();
                    lstinvoice = (from a in lstinvoice1
                                  join b in Utils.ITT153PublishInvoiceDetailService.Query
                                       on a.ID equals b.TT153ReportID
                                  select a).Distinct().ToList();
                    cbbMauHD.DataSource = lstinvoice;
                    cbbMauHD.ValueMember = "ID";
                    cbbMauHD.DisplayMember = "InvoiceTemplate";
                    Utils.ConfigGrid(cbbMauHD, ConstDatabase.TT153Report_TableName);
                    _select.InvoiceTypeID = Guid.Parse(cbbLoaiHD.Value.ToString());
                    if (lstinvoice.Count == 1)
                    {
                        var gt = lstinvoice.FirstOrDefault();
                        if (gt != null)
                        {
                            cbbMauHD.Value = gt.ID != null ? gt.ID : new Guid();
                            txtKyHieuHD.Text = _select.InvoiceSeries != null ? _select.InvoiceSeries : null;
                            _select.InvoiceSeries = _select.InvoiceSeries != null ? _select.InvoiceSeries : null;
                            _select.InvoiceTemplate = _select.InvoiceTemplate != null ? _select.InvoiceTemplate : null;
                            if (_statusForm == ConstFrm.optStatusForm.Add)
                            {
                                List<SAInvoice> lst = new List<SAInvoice>();
                                List<SAInvoice> lstsA = ISAInvoiceService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).ToList();
                                List<SAInvoice> lstpDr = IPPDiscountReturnService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                                    x => new SAInvoice
                                    {
                                        InvoiceTypeID = x.InvoiceTypeID,
                                        InvoiceForm = x.InvoiceForm,
                                        InvoiceTemplate = x.InvoiceTemplate,
                                        InvoiceNo = x.InvoiceNo
                                    }
                                    ).ToList();
                                List<SAInvoice> lstSABill = ISABillService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                                   x => new SAInvoice
                                   {
                                       InvoiceTypeID = x.InvoiceTypeID,
                                       InvoiceForm = x.InvoiceForm,
                                       InvoiceTemplate = x.InvoiceTemplate,
                                       InvoiceNo = x.InvoiceNo
                                   }
                                   ).ToList();
                                if (lstsA.Count > 0) lst.AddRange(lstsA);
                                if (lstpDr.Count > 0) lst.AddRange(lstpDr);
                                if (lstSABill.Count > 0) lst.AddRange(lstSABill);
                                List<SAInvoice> ls = lst.Where(x => x.InvoiceTypeID == _select.InvoiceTypeID && x.InvoiceForm == _select.InvoiceForm && x.InvoiceTemplate == _select.InvoiceTemplate && x.InvoiceNo != null && x.InvoiceNo != "").ToList();
                                if (ls.Count > 0)
                                {
                                    int maxno = ls.Select(n => int.Parse(n.InvoiceNo)).Max() + 1;
                                    txtSoHD.Text = maxno.ToString("0000000");
                                }
                                else
                                {
                                    txtSoHD.Text = "0000001";
                                }
                            }
                        }
                    }
                    else
                    {
                        if (_statusForm == ConstFrm.optStatusForm.Add)
                        {
                            cbbMauHD.Value = null;
                            txtKyHieuHD.Text = null;
                            txtSoHD.Text = null;
                        }
                        else
                        {
                            var templateInvoid = lstinvoice1.FirstOrDefault(t => t.InvoiceTemplate == _select.InvoiceTemplate);
                            if (templateInvoid != null)
                            {
                                cbbMauHD.Value = templateInvoid.ID;
                                txtKyHieuHD.Text = templateInvoid.InvoiceSeries;
                            }
                            else
                            {
                                cbbMauHD.Value = null;
                                txtKyHieuHD.Text = null;
                            }
                        }

                    }
                }
                else
                {
                    cbbMauHD.Value = null;
                    txtKyHieuHD.Text = null;
                }

            }
            if (cbb.Name == "cbbLoaiHD")
            {
                List<TT153Report> lstinvoice = new List<TT153Report>();
                _select.InvoiceTypeID = Guid.Parse(cbbLoaiHD.Value.ToString());
                if (cbbHinhThucHD.Value != null)
                {
                    var lstinvoice1 = Utils.ListTT153Report.Where(t => t.InvoiceTypeID == Guid.Parse(cbbLoaiHD.Value.ToString()) && t.InvoiceForm == int.Parse(cbbHinhThucHD.Value.ToString())).ToList();
                    lstinvoice = (from a in lstinvoice1
                                  join b in Utils.ITT153PublishInvoiceDetailService.Query
                                       on a.ID equals b.TT153ReportID
                                  select a).Distinct().ToList();
                    cbbMauHD.DataSource = lstinvoice;
                    cbbMauHD.ValueMember = "ID";
                    cbbMauHD.DisplayMember = "InvoiceTemplate";
                    Utils.ConfigGrid(cbbMauHD, ConstDatabase.TT153Report_TableName);
                    _select.InvoiceForm = int.Parse(cbbHinhThucHD.Value.ToString());
                    if (lstinvoice.Count == 1)
                    {
                        var gt = lstinvoice.FirstOrDefault();
                        if (gt != null)
                        {
                            cbbMauHD.Value = gt.ID;
                            txtKyHieuHD.Text = gt.InvoiceSeries;
                            _select.InvoiceSeries = gt.InvoiceSeries;
                            _select.InvoiceTemplate = gt.InvoiceTemplate;
                            if (_statusForm == ConstFrm.optStatusForm.Add)
                            {
                                List<SAInvoice> lst = new List<SAInvoice>();
                                List<SAInvoice> lstsA = ISAInvoiceService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).ToList();
                                List<SAInvoice> lstpDr = IPPDiscountReturnService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                                    x => new SAInvoice
                                    {
                                        InvoiceTypeID = x.InvoiceTypeID,
                                        InvoiceForm = x.InvoiceForm,
                                        InvoiceTemplate = x.InvoiceTemplate,
                                        InvoiceNo = x.InvoiceNo
                                    }
                                    ).ToList();
                                List<SAInvoice> lstSABill = ISABillService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                                   x => new SAInvoice
                                   {
                                       InvoiceTypeID = x.InvoiceTypeID,
                                       InvoiceForm = x.InvoiceForm,
                                       InvoiceTemplate = x.InvoiceTemplate,
                                       InvoiceNo = x.InvoiceNo
                                   }
                                   ).ToList();
                                if (lstsA.Count > 0) lst.AddRange(lstsA);
                                if (lstpDr.Count > 0) lst.AddRange(lstpDr);
                                if (lstSABill.Count > 0) lst.AddRange(lstSABill);
                                List<SAInvoice> ls = lst.Where(x => x.InvoiceTypeID == _select.InvoiceTypeID && x.InvoiceForm == _select.InvoiceForm && x.InvoiceTemplate == _select.InvoiceTemplate && x.InvoiceNo != null && x.InvoiceNo != "").ToList();
                                if (ls.Count > 0)
                                {
                                    int maxno = ls.Select(n => int.Parse(n.InvoiceNo)).Max() + 1;
                                    txtSoHD.Text = maxno.ToString("0000000");
                                }
                                else
                                {
                                    txtSoHD.Text = "0000001";
                                }
                            }
                        }
                    }
                    else
                    {
                        if (_statusForm == ConstFrm.optStatusForm.Add)
                        {
                            cbbMauHD.Value = null;
                            txtKyHieuHD.Text = null;
                            txtSoHD.Text = null;
                        }
                        else
                        {
                            var templateInvoid = lstinvoice1.FirstOrDefault(t => t.InvoiceTemplate == _select.InvoiceTemplate);
                            if (templateInvoid != null)
                            {
                                cbbMauHD.Value = templateInvoid.ID;
                                txtKyHieuHD.Text = templateInvoid.InvoiceSeries;
                            }
                            else
                            {
                                cbbMauHD.Value = null;
                                txtKyHieuHD.Text = null;
                            }
                        }
                    }

                }
                else
                {
                    cbbMauHD.Value = null;
                    txtKyHieuHD.Text = null;
                }
            }
            if (cbb.Name == "cbbMauHD")
            {
                Guid guid = cbb.Value != null ? new Guid(cbb.Value.ToString()) : new Guid();
                //_select.InvoiceTemplate = cbb.Value.ToString();
                var lstinvoice1 = Utils.ListTT153Report.Where(t => t.ID == guid && t.InvoiceTypeID == Guid.Parse(cbbLoaiHD.Value.ToString()) && t.InvoiceForm == int.Parse(cbbHinhThucHD.Value.ToString())).ToList();
                if (lstinvoice1.Count == 1)
                {
                    var gt = lstinvoice1.FirstOrDefault();
                    if (gt != null)
                    {
                        _select.InvoiceTemplate = gt.InvoiceTemplate;
                        txtKyHieuHD.Text = gt.InvoiceSeries;
                        if (_statusForm == ConstFrm.optStatusForm.Add)
                        {
                            List<SAInvoice> lst = new List<SAInvoice>();
                            List<SAInvoice> lstsA = ISAInvoiceService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).ToList();
                            List<SAInvoice> lstpDr = IPPDiscountReturnService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                                x => new SAInvoice
                                {
                                    InvoiceTypeID = x.InvoiceTypeID,
                                    InvoiceForm = x.InvoiceForm,
                                    InvoiceTemplate = x.InvoiceTemplate,
                                    InvoiceNo = x.InvoiceNo
                                }
                                    ).ToList();
                            List<SAInvoice> lstSABill = ISABillService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                               x => new SAInvoice
                               {
                                   InvoiceTypeID = x.InvoiceTypeID,
                                   InvoiceForm = x.InvoiceForm,
                                   InvoiceTemplate = x.InvoiceTemplate,
                                   InvoiceNo = x.InvoiceNo
                               }
                               ).ToList();
                            if (lstsA.Count > 0) lst.AddRange(lstsA);
                            if (lstpDr.Count > 0) lst.AddRange(lstpDr);
                            if (lstSABill.Count > 0) lst.AddRange(lstSABill);
                            List<SAInvoice> ls = lst.Where(x => x.InvoiceTypeID == _select.InvoiceTypeID && x.InvoiceForm == _select.InvoiceForm && x.InvoiceTemplate == _select.InvoiceTemplate && x.InvoiceNo != null && x.InvoiceNo != "").ToList();
                            if (ls.Count > 0)
                            {
                                int maxno = ls.Select(n => int.Parse(n.InvoiceNo)).Max() + 1;
                                txtSoHD.Text = maxno.ToString("0000000");
                            }
                            else
                            {
                                txtSoHD.Text = "0000001";
                            }
                        }
                    }
                }
                else
                {
                    var gttemp = lstinvoice1.FirstOrDefault(t => t.ID == guid);
                    if (gttemp != null)
                    {
                        txtKyHieuHD.Text = gttemp.InvoiceSeries;
                        _select.InvoiceTemplate = gttemp.InvoiceTemplate;
                        if (_statusForm == ConstFrm.optStatusForm.Add)
                        {
                            List<SAInvoice> lst = new List<SAInvoice>();
                            List<SAInvoice> lstsA = ISAInvoiceService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).ToList();
                            List<SAInvoice> lstpDr = IPPDiscountReturnService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                                x => new SAInvoice
                                {
                                    InvoiceTypeID = x.InvoiceTypeID,
                                    InvoiceForm = x.InvoiceForm,
                                    InvoiceTemplate = x.InvoiceTemplate,
                                    InvoiceNo = x.InvoiceNo
                                }
                                    ).ToList();
                            List<SAInvoice> lstSABill = ISABillService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                               x => new SAInvoice
                               {
                                   InvoiceTypeID = x.InvoiceTypeID,
                                   InvoiceForm = x.InvoiceForm,
                                   InvoiceTemplate = x.InvoiceTemplate,
                                   InvoiceNo = x.InvoiceNo
                               }
                               ).ToList();
                            if (lstsA.Count > 0) lst.AddRange(lstsA);
                            if (lstpDr.Count > 0) lst.AddRange(lstpDr);
                            if (lstSABill.Count > 0) lst.AddRange(lstSABill);
                            List<SAInvoice> ls = lst.Where(x => x.InvoiceTypeID == _select.InvoiceTypeID && x.InvoiceForm == _select.InvoiceForm && x.InvoiceTemplate == _select.InvoiceTemplate && x.InvoiceNo != null && x.InvoiceNo != "").ToList();
                            if (ls.Count > 0)
                            {
                                int maxno = ls.Select(n => int.Parse(n.InvoiceNo)).Max() + 1;
                                txtSoHD.Text = maxno.ToString("0000000");
                            }
                            else
                            {
                                txtSoHD.Text = "0000001";
                            }
                        }
                    }
                }

            }
        }
        private void txtKyHieuHD_TextChansged(object sender, EventArgs e)
        {
            _select.InvoiceSeries = txtKyHieuHD.Text;
        }
        private void dteNgayHD_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (dteNgayHD.Value != null)
                    _select.InvoiceDate = Convert.ToDateTime(dteNgayHD.Value.ToString());
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void txtSoHD_TextChanged(object sender, EventArgs e)
        {
            _select.InvoiceNo = txtSoHD.Text;
        }
        private void AddData2(PPDiscountReturn input)
        {
            txtMauSoHDI.Text = input.InvoiceTemplate != null ? input.InvoiceTemplate : txtMauSoHDI.Text;
            txtKyHieuHDI.Text = input.InvoiceSeries != null ? input.InvoiceSeries : txtKyHieuHDI.Text;
            txtSoHDI.Text = input.InvoiceNo != null ? input.InvoiceNo : txtSoHDI.Text;
            dteNgayHDI.Value = input.InvoiceDate != null ? input.InvoiceDate : dteNgayHDI.Value;
        }
        protected override bool AdditionalFunc()
        {
            if (_select.TypeID == 230)
            {
                if (txtKyHieuHDI.Text != "" || txtMauSoHDI.Text != "" || txtSoHDI.Text != "" || dteNgayHDI.Value != null)
                {
                    if (txtKyHieuHDI.Text != "" && txtMauSoHDI.Text != "" && txtSoHDI.Text != "" && dteNgayHDI.Value != null)
                    {
                        if (txtSoHDI.Text.Length == 7)
                        {
                            _select.InvoiceTemplate = txtMauSoHDI.Text;
                            _select.InvoiceSeries = txtKyHieuHDI.Text;
                            _select.InvoiceNo = txtSoHDI.Text;
                            _select.InvoiceDate = Convert.ToDateTime(dteNgayHDI.Value.ToString());
                        }
                        else
                        {
                            MSG.Warning("Số hoá đơn phải là kiểu số và đúng 7 chữ số.");
                            return false;
                        }
                    }
                    else
                    {
                        MSG.Warning("Chưa nhập đủ thông tin hóa đơn ,vui lòng kiểm tra lại");
                        return false;
                    }
                }
                else
                {
                    _select.InvoiceTemplate = "";
                    _select.InvoiceSeries = "";
                    _select.InvoiceNo = "";
                    _select.InvoiceDate = null;
                }
            }
            return true;
        }
        protected void txtMauSoHDI_TextChanged(object sender, EventArgs e)
        {
            _select.InvoiceTemplate = txtMauSoHDI.Text;
        }
        protected void txtSoHDI_TextChanged(object sender, EventArgs e)
        {
            _select.InvoiceNo = txtSoHDI.Text;
        }
        protected void txtKyHieuHDI_TextChansged(object sender, EventArgs e)
        {
            _select.InvoiceSeries = txtKyHieuHDI.Text;
        }
        protected void dteNgayHDI_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (dteNgayHDI.Value != null)
                    _select.InvoiceDate = Convert.ToDateTime(dteNgayHDI.Value.ToString());
            }
            catch (Exception)
            {

                throw;
            }
        }
    }

    public class FppReturnDetailsStand : DetailBase<PPDiscountReturn> { }
}
