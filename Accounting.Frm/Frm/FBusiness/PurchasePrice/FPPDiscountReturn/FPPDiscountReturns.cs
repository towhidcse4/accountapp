﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FPPDiscountReturns : CustormForm
    {
        #region khai báo
        private readonly IPPDiscountReturnService _iPPDiscountReturnService;
        List<PPDiscountReturn> _dsPPDiscountReturn = new List<PPDiscountReturn>();

        #endregion

        #region khởi tạo
        public FPPDiscountReturns()
        {           
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            btnAdd.PopupItem = popUp;
            #endregion

            #region Thiết lập ban đầu cho Form
            _iPPDiscountReturnService = IoC.Resolve<IPPDiscountReturnService>();
            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();

            //chọn tự select dòng đầu tiên
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            #endregion
        }

        private void LoadDuLieu(bool configGrid = true)
        {
            #region Lấy dữ liệu từ CSDL
            //Dữ liệu lấy theo năm hoạch toán
            DateTime? dbStartDate = Utils.StringToDateTime(ConstFrm.DbStartDate);
            int year = (dbStartDate == null ? DateTime.Now.Year : dbStartDate.Value.Year);
            _dsPPDiscountReturn = _iPPDiscountReturnService.GetAll();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = _dsPPDiscountReturn;

            if (configGrid) ConfigGrid(uGrid);
            foreach (var item in uGrid.Rows)
            {
                if ((bool)item.Cells["Recorded"].Value)
                    item.Appearance.ForeColor = Color.Black;
                else if (!(bool) item.Cells["Recorded"].Value)
                    item.Appearance.ForeColor = Color.LimeGreen;
            }
            #endregion
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void TsmAddHbtlClick(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void TsmAddHbggClick(object sender, EventArgs e)
        {
            AddFunction(false);
        }

        private void TsmViewClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void TsmDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void TmsReLoadClick(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }
        #endregion


        #region Button
        private void BtnAddClick(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void BtnEditClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void BtnDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void UGridDoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunction();
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary> 
        void AddFunction(bool isHMTL = true)
        {
            PPDiscountReturn temp = new PPDiscountReturn();
            if (temp != null)
            {
                temp.TypeID = isHMTL ? 220 : 230;
                //new FPPDiscountDetails(temp, _dsPPDiscountReturn, ConstFrm.optStatusForm.Add).ShowDialog(this);
                new FPPDiscountReturnDetail(temp, _dsPPDiscountReturn, ConstFrm.optStatusForm.Add).ShowDialog(this);
            }
            if (!FPPDiscountReturnDetail.IsClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                PPDiscountReturn temp = uGrid.Selected.Rows[0].ListObject as PPDiscountReturn;
                new FPPDiscountReturnDetail(temp, _dsPPDiscountReturn, ConstFrm.optStatusForm.View).ShowDialog(this);
                if (!FPPDiscountReturnDetail.IsClose) LoadDuLieu();
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                PPDiscountReturn temp = uGrid.Selected.Rows[0].ListObject as PPDiscountReturn;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, temp.No)) == DialogResult.Yes)
                {
                    _iPPDiscountReturnService.BeginTran();
                    _iPPDiscountReturnService.Delete(temp);
                    _iPPDiscountReturnService.CommitTran();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion

        #region Utils
        void ConfigGrid(UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, ConstDatabase.PPDiscountReturn_TableName);
            utralGrid.DisplayLayout.Bands[0].Columns["Date"].Format = Constants.DdMMyyyy;
            utralGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Format = Constants.DdMMyyyy;
            utralGrid.DisplayLayout.Bands[0].Columns["Date"].CellActivation = Activation.NoEdit;
            utralGrid.DisplayLayout.Bands[0].Columns["PostedDate"].CellActivation = Activation.NoEdit;
        }

        static int GetTypeIdRef(int typeId)
        {//phiếu chi
            //if (typeID == 111) return 111568658;  //phiếu chi trả lương ([Tiền lương] -> [Trả lương])
            //else if (typeID == 112) return 111568658;  //phiếu chi trả lương ([Tiền lương] -> [Trả lương])....
            return typeId;
        }
        #endregion

        #region Event

        private void btnAdd_DroppingDown(object sender, CancelEventArgs e)
        {
            Control control = (Control)sender;
            var startPoint = control.PointToScreen(new Point(control.Location.X + control.Margin.Left + control.Padding.Left - btnAdd.ImageSize.Width, control.Location.Y + control.Size.Height + control.Margin.Top + control.Padding.Top - btnAdd.ImageSize.Height));
            cms4Button.Show(startPoint);
        }

        private void uGrid_AfterRowActivate(object sender, EventArgs e)
        {
            if (uGrid.Selected.Rows.Count < 1) return;
            object listObject = uGrid.Selected.Rows[0].ListObject;
            if (listObject == null) return;
            PPDiscountReturn PPDiscountReturn = _dsPPDiscountReturn[_dsPPDiscountReturn.IndexOf((PPDiscountReturn)listObject)];
            if (PPDiscountReturn == null) return;

            //Tab thông tin chung
            lblNo.Text = PPDiscountReturn.No;
            lblDate.Text = PPDiscountReturn.Date.ToString(Constants.DdMMyyyy);
            lblAccountingObjectName.Text = PPDiscountReturn.AccountingObjectName;
            lblAccountingObjectAddress.Text = PPDiscountReturn.AccountingObjectAddress;
            lblReason.Text = PPDiscountReturn.Reason;
            lblTotalPaymentAmountOriginal.Text = string.Format(PPDiscountReturn.TotalPaymentAmountOriginal == 0 ? "0" : "{0:0,0}", PPDiscountReturn.TotalPaymentAmountOriginal);  //SAReturn.TotalAmount.ToString("{0:0,0}");

            //Tab hoạch toán
            uGridPosted.DataSource = PPDiscountReturn;    //Utils.CloneObject(SAReturn.SAReturnDetails);

            #region Config Grid
            //hiển thị 1 band
            uGridPosted.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            //tắt lọc cột
            uGridPosted.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGridPosted.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGridPosted.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGridPosted.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;

            //Hiện những dòng trống?
            uGridPosted.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridPosted.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            //Fix Header
            uGridPosted.DisplayLayout.UseFixedHeaders = true;

            UltraGridBand band = uGridPosted.DisplayLayout.Bands[0];
            band.Summaries.Clear();

            Template mauGiaoDien = Utils.GetMauGiaoDien(PPDiscountReturn.TypeID, PPDiscountReturn.TemplateID, Utils.ListTemplate);

            Utils.ConfigGrid(uGridPosted, ConstDatabase.PPDiscountReturnDetail_TableName, mauGiaoDien == null ? new List<TemplateColumn>() : mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 0).TemplateColumns, 0);
            //Thêm tổng số ở cột "Số tiền"
            //SummarySettings summary = band.Summaries.Add("SumAmount", SummaryType.Sum, band.Columns["Amount"]);
            //summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            //summary.DisplayFormat = "{0:N0}";
            //summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            #endregion
        }

        private void HMMTLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddFunction(true);
        }

        private void HMTLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddFunction(false);
        }
        #endregion

    }
}
