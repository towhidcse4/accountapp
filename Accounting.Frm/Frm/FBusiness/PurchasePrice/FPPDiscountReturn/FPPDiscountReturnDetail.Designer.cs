﻿using Accounting.Core.Domain;

namespace Accounting
{
    partial class FPPDiscountReturnDetail
    {

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem8 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            this.ultraTabPageHoaDon = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBoxHoaDon = new Infragistics.Win.Misc.UltraGroupBox();
            this.dteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnVouchersDiscount = new Infragistics.Win.Misc.UltraButton();
            this.txtNoBK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.chkBangke = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.txtCompanyTaxCodeHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtMatHang = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectNameHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectHD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtReasonReturn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtSContactNameHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPagePhieuXuatKho = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBoxPhieuXuatKho = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtCompanyTaxCodeXK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblCompanyTaxCode = new Infragistics.Win.Misc.UltraLabel();
            this.lblChungTuGoc = new Infragistics.Win.Misc.UltraLabel();
            this.txtOriginalNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblNumberAttach = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectNameXK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectXK = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtSReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblSReason = new Infragistics.Win.Misc.UltraLabel();
            this.txtSContactNameXK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblSContactName = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressXK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDiaChi = new Infragistics.Win.Misc.UltraLabel();
            this.lblKhachHang = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel40 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbLoaiHD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel41 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbHinhThucHD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtSoHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtKyHieuHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel42 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel43 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel44 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel45 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbMauHD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dteNgayHD = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtAccountingBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObjectBankAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel33 = new Infragistics.Win.Misc.UltraLabel();
            this.txtThanhToan = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.dteDate1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtNoBK1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.chkBangke1 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.txtCompanyTaxCodeHD1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.txtMatHang1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectNameHD1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectHD1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtReasonReturn1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.txtSContactNameHD1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressHD1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.uGridControl = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.palFill = new System.Windows.Forms.Panel();
            this.palGrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.splitter = new Infragistics.Win.Misc.UltraSplitter();
            this.palThongTinChung = new System.Windows.Forms.Panel();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtMauSoHDI = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSoHDI = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtKyHieuHDI = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel29 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel30 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel34 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel35 = new Infragistics.Win.Misc.UltraLabel();
            this.dteNgayHDI = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uGroupBoxStand = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtCompanyTaxCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObject = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtReasonDiscount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSContactName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.btnVouchersReturn = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxEx = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.palTop = new System.Windows.Forms.Panel();
            this.ultraGroupBoxTop = new Infragistics.Win.Misc.UltraGroupBox();
            this.optPhieuXuatKho = new Accounting.UltraOptionSet_Ex();
            this.optHoaDon = new Accounting.UltraOptionSet_Ex();
            this.palTopSctNhtNctPXK = new Infragistics.Win.Misc.UltraPanel();
            this.palTopSctNhtNctHoaDon = new Infragistics.Win.Misc.UltraPanel();
            this.palBottom = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtOldInvSeries = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.txtOldInvTemplate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.txtOldInvDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.txtOldInvNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblTttt = new Infragistics.Win.Misc.UltraLabel();
            this.btnSelectBill = new Infragistics.Win.Misc.UltraButton();
            this.cbBill = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.lblTotalPaymentAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalVATAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalPaymentAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalVATAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.lbl2 = new Infragistics.Win.Misc.UltraLabel();
            this.lbl3 = new Infragistics.Win.Misc.UltraLabel();
            this.lbl1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraToolTipManager1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            this.ultraTabPageHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxHoaDon)).BeginInit();
            this.ultraGroupBoxHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoBK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBangke)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodeHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMatHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSContactNameHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressHD)).BeginInit();
            this.ultraTabPagePhieuXuatKho.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxPhieuXuatKho)).BeginInit();
            this.ultraGroupBoxPhieuXuatKho.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodeXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSContactNameXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressXK)).BeginInit();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbLoaiHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbHinhThucHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMauHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThanhToan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoBK1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBangke1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodeHD1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMatHang1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameHD1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectHD1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonReturn1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSContactNameHD1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressHD1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).BeginInit();
            this.palFill.SuspendLayout();
            this.palGrid.ClientArea.SuspendLayout();
            this.palGrid.SuspendLayout();
            this.palThongTinChung.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxStand)).BeginInit();
            this.uGroupBoxStand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonDiscount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSContactName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxEx)).BeginInit();
            this.uGroupBoxEx.SuspendLayout();
            this.palTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxTop)).BeginInit();
            this.ultraGroupBoxTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optPhieuXuatKho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optHoaDon)).BeginInit();
            this.palTopSctNhtNctPXK.SuspendLayout();
            this.palTopSctNhtNctHoaDon.SuspendLayout();
            this.palBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvSeries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBill)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageHoaDon
            // 
            this.ultraTabPageHoaDon.Controls.Add(this.ultraGroupBoxHoaDon);
            this.ultraTabPageHoaDon.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageHoaDon.Name = "ultraTabPageHoaDon";
            this.ultraTabPageHoaDon.Size = new System.Drawing.Size(934, 156);
            // 
            // ultraGroupBoxHoaDon
            // 
            this.ultraGroupBoxHoaDon.Controls.Add(this.dteDate);
            this.ultraGroupBoxHoaDon.Controls.Add(this.btnVouchersDiscount);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtNoBK);
            this.ultraGroupBoxHoaDon.Controls.Add(this.ultraLabel9);
            this.ultraGroupBoxHoaDon.Controls.Add(this.ultraLabel8);
            this.ultraGroupBoxHoaDon.Controls.Add(this.chkBangke);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtCompanyTaxCodeHD);
            this.ultraGroupBoxHoaDon.Controls.Add(this.ultraLabel1);
            this.ultraGroupBoxHoaDon.Controls.Add(this.ultraLabel2);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtMatHang);
            this.ultraGroupBoxHoaDon.Controls.Add(this.ultraLabel3);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtAccountingObjectNameHD);
            this.ultraGroupBoxHoaDon.Controls.Add(this.cbbAccountingObjectHD);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtReasonReturn);
            this.ultraGroupBoxHoaDon.Controls.Add(this.ultraLabel4);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtSContactNameHD);
            this.ultraGroupBoxHoaDon.Controls.Add(this.ultraLabel5);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtAccountingObjectAddressHD);
            this.ultraGroupBoxHoaDon.Controls.Add(this.ultraLabel6);
            this.ultraGroupBoxHoaDon.Controls.Add(this.ultraLabel7);
            this.ultraGroupBoxHoaDon.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance19.FontData.BoldAsString = "True";
            appearance19.FontData.SizeInPoints = 10F;
            this.ultraGroupBoxHoaDon.HeaderAppearance = appearance19;
            this.ultraGroupBoxHoaDon.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxHoaDon.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxHoaDon.Name = "ultraGroupBoxHoaDon";
            this.ultraGroupBoxHoaDon.Size = new System.Drawing.Size(934, 156);
            this.ultraGroupBoxHoaDon.TabIndex = 26;
            this.ultraGroupBoxHoaDon.Text = "Thông tin chung";
            this.ultraGroupBoxHoaDon.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // dteDate
            // 
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.dteDate.Appearance = appearance1;
            this.dteDate.AutoSize = false;
            this.dteDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.dteDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteDate.Location = new System.Drawing.Point(344, 121);
            this.dteDate.MaskInput = "";
            this.dteDate.Name = "dteDate";
            this.dteDate.Size = new System.Drawing.Size(124, 22);
            this.dteDate.TabIndex = 75;
            this.dteDate.Value = null;
            // 
            // btnVouchersDiscount
            // 
            this.btnVouchersDiscount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVouchersDiscount.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnVouchersDiscount.Location = new System.Drawing.Point(738, 25);
            this.btnVouchersDiscount.Name = "btnVouchersDiscount";
            this.btnVouchersDiscount.Size = new System.Drawing.Size(187, 22);
            this.btnVouchersDiscount.TabIndex = 74;
            this.btnVouchersDiscount.Text = "Chọn chứng từ mua hàng";
            this.btnVouchersDiscount.Click += new System.EventHandler(this.btnVouchers_Click);
            // 
            // txtNoBK
            // 
            appearance2.TextVAlignAsString = "Middle";
            this.txtNoBK.Appearance = appearance2;
            this.txtNoBK.AutoSize = false;
            this.txtNoBK.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtNoBK.Location = new System.Drawing.Point(201, 121);
            this.txtNoBK.Name = "txtNoBK";
            this.txtNoBK.Size = new System.Drawing.Size(89, 22);
            this.txtNoBK.TabIndex = 72;
            // 
            // ultraLabel9
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance3;
            this.ultraLabel9.Location = new System.Drawing.Point(296, 121);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(42, 21);
            this.ultraLabel9.TabIndex = 71;
            this.ultraLabel9.Text = "Ngày";
            // 
            // ultraLabel8
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance4;
            this.ultraLabel8.Location = new System.Drawing.Point(170, 121);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(25, 22);
            this.ultraLabel8.TabIndex = 70;
            this.ultraLabel8.Text = "Số";
            // 
            // chkBangke
            // 
            this.chkBangke.BackColor = System.Drawing.Color.Transparent;
            this.chkBangke.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkBangke.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.chkBangke.Location = new System.Drawing.Point(134, 121);
            this.chkBangke.Name = "chkBangke";
            this.chkBangke.Size = new System.Drawing.Size(19, 22);
            this.chkBangke.TabIndex = 69;
            this.chkBangke.CheckedChanged += new System.EventHandler(this.chkBangke_CheckedChanged);
            // 
            // txtCompanyTaxCodeHD
            // 
            appearance5.TextVAlignAsString = "Middle";
            this.txtCompanyTaxCodeHD.Appearance = appearance5;
            this.txtCompanyTaxCodeHD.AutoSize = false;
            this.txtCompanyTaxCodeHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtCompanyTaxCodeHD.Location = new System.Drawing.Point(134, 73);
            this.txtCompanyTaxCodeHD.Name = "txtCompanyTaxCodeHD";
            this.txtCompanyTaxCodeHD.Size = new System.Drawing.Size(156, 22);
            this.txtCompanyTaxCodeHD.TabIndex = 68;
            // 
            // ultraLabel1
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance6;
            this.ultraLabel1.Location = new System.Drawing.Point(12, 73);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(115, 21);
            this.ultraLabel1.TabIndex = 67;
            this.ultraLabel1.Text = "Mã số thuế";
            // 
            // ultraLabel2
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance7;
            this.ultraLabel2.Location = new System.Drawing.Point(474, 121);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(62, 19);
            this.ultraLabel2.TabIndex = 66;
            this.ultraLabel2.Text = "Mặt hàng";
            // 
            // txtMatHang
            // 
            this.txtMatHang.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.TextVAlignAsString = "Middle";
            this.txtMatHang.Appearance = appearance8;
            this.txtMatHang.AutoSize = false;
            this.txtMatHang.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtMatHang.Location = new System.Drawing.Point(542, 121);
            this.txtMatHang.Name = "txtMatHang";
            this.txtMatHang.Size = new System.Drawing.Size(383, 22);
            this.txtMatHang.TabIndex = 65;
            // 
            // ultraLabel3
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance9;
            this.ultraLabel3.Location = new System.Drawing.Point(12, 121);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(115, 22);
            this.ultraLabel3.TabIndex = 64;
            this.ultraLabel3.Text = "Bảng kê";
            // 
            // txtAccountingObjectNameHD
            // 
            this.txtAccountingObjectNameHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance10.TextVAlignAsString = "Middle";
            this.txtAccountingObjectNameHD.Appearance = appearance10;
            this.txtAccountingObjectNameHD.AutoSize = false;
            this.txtAccountingObjectNameHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectNameHD.Location = new System.Drawing.Point(296, 25);
            this.txtAccountingObjectNameHD.Name = "txtAccountingObjectNameHD";
            this.txtAccountingObjectNameHD.Size = new System.Drawing.Size(436, 22);
            this.txtAccountingObjectNameHD.TabIndex = 63;
            this.txtAccountingObjectNameHD.TextChanged += new System.EventHandler(this.txtAccountingObjectNameHD_TextChanged);
            // 
            // cbbAccountingObjectHD
            // 
            appearance11.TextVAlignAsString = "Middle";
            this.cbbAccountingObjectHD.Appearance = appearance11;
            this.cbbAccountingObjectHD.AutoSize = false;
            this.cbbAccountingObjectHD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjectHD.Location = new System.Drawing.Point(134, 25);
            this.cbbAccountingObjectHD.Name = "cbbAccountingObjectHD";
            this.cbbAccountingObjectHD.Size = new System.Drawing.Size(156, 22);
            this.cbbAccountingObjectHD.TabIndex = 62;
            // 
            // txtReasonReturn
            // 
            this.txtReasonReturn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance12.TextVAlignAsString = "Middle";
            this.txtReasonReturn.Appearance = appearance12;
            this.txtReasonReturn.AutoSize = false;
            this.txtReasonReturn.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReasonReturn.Location = new System.Drawing.Point(134, 97);
            this.txtReasonReturn.Name = "txtReasonReturn";
            this.txtReasonReturn.Size = new System.Drawing.Size(791, 22);
            this.txtReasonReturn.TabIndex = 61;
            // 
            // ultraLabel4
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance13;
            this.ultraLabel4.Location = new System.Drawing.Point(12, 97);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(115, 20);
            this.ultraLabel4.TabIndex = 60;
            this.ultraLabel4.Text = "Diễn giải";
            // 
            // txtSContactNameHD
            // 
            this.txtSContactNameHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance14.TextVAlignAsString = "Middle";
            this.txtSContactNameHD.Appearance = appearance14;
            this.txtSContactNameHD.AutoSize = false;
            this.txtSContactNameHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtSContactNameHD.Location = new System.Drawing.Point(474, 73);
            this.txtSContactNameHD.Name = "txtSContactNameHD";
            this.txtSContactNameHD.Size = new System.Drawing.Size(451, 22);
            this.txtSContactNameHD.TabIndex = 59;
            // 
            // ultraLabel5
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance15;
            this.ultraLabel5.Location = new System.Drawing.Point(296, 73);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(159, 21);
            this.ultraLabel5.TabIndex = 58;
            this.ultraLabel5.Text = "Họ tên người nhận hàng";
            // 
            // txtAccountingObjectAddressHD
            // 
            this.txtAccountingObjectAddressHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance16.TextVAlignAsString = "Middle";
            this.txtAccountingObjectAddressHD.Appearance = appearance16;
            this.txtAccountingObjectAddressHD.AutoSize = false;
            this.txtAccountingObjectAddressHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectAddressHD.Location = new System.Drawing.Point(134, 49);
            this.txtAccountingObjectAddressHD.Name = "txtAccountingObjectAddressHD";
            this.txtAccountingObjectAddressHD.Size = new System.Drawing.Size(791, 22);
            this.txtAccountingObjectAddressHD.TabIndex = 57;
            this.txtAccountingObjectAddressHD.TextChanged += new System.EventHandler(this.txtAccountingObjectNameHD_TextChanged);
            // 
            // ultraLabel6
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance17;
            this.ultraLabel6.Location = new System.Drawing.Point(12, 49);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(115, 21);
            this.ultraLabel6.TabIndex = 56;
            this.ultraLabel6.Text = "Địa chỉ";
            // 
            // ultraLabel7
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance18;
            this.ultraLabel7.Location = new System.Drawing.Point(12, 25);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(115, 22);
            this.ultraLabel7.TabIndex = 55;
            this.ultraLabel7.Text = "Nhà cung cấp";
            // 
            // ultraTabPagePhieuXuatKho
            // 
            this.ultraTabPagePhieuXuatKho.Controls.Add(this.ultraGroupBoxPhieuXuatKho);
            this.ultraTabPagePhieuXuatKho.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPagePhieuXuatKho.Name = "ultraTabPagePhieuXuatKho";
            this.ultraTabPagePhieuXuatKho.Size = new System.Drawing.Size(934, 156);
            // 
            // ultraGroupBoxPhieuXuatKho
            // 
            appearance20.TextVAlignAsString = "Middle";
            this.ultraGroupBoxPhieuXuatKho.Appearance = appearance20;
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtCompanyTaxCodeXK);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblCompanyTaxCode);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblChungTuGoc);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtOriginalNo);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblNumberAttach);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtAccountingObjectNameXK);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.cbbAccountingObjectXK);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtSReason);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblSReason);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtSContactNameXK);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblSContactName);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtAccountingObjectAddressXK);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblDiaChi);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblKhachHang);
            this.ultraGroupBoxPhieuXuatKho.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance36.FontData.BoldAsString = "True";
            appearance36.FontData.SizeInPoints = 10F;
            this.ultraGroupBoxPhieuXuatKho.HeaderAppearance = appearance36;
            this.ultraGroupBoxPhieuXuatKho.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxPhieuXuatKho.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxPhieuXuatKho.Name = "ultraGroupBoxPhieuXuatKho";
            this.ultraGroupBoxPhieuXuatKho.Size = new System.Drawing.Size(934, 156);
            this.ultraGroupBoxPhieuXuatKho.TabIndex = 27;
            this.ultraGroupBoxPhieuXuatKho.Text = "Thông tin chung";
            this.ultraGroupBoxPhieuXuatKho.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtCompanyTaxCodeXK
            // 
            appearance21.TextVAlignAsString = "Middle";
            this.txtCompanyTaxCodeXK.Appearance = appearance21;
            this.txtCompanyTaxCodeXK.AutoSize = false;
            this.txtCompanyTaxCodeXK.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtCompanyTaxCodeXK.Location = new System.Drawing.Point(132, 73);
            this.txtCompanyTaxCodeXK.Name = "txtCompanyTaxCodeXK";
            this.txtCompanyTaxCodeXK.Size = new System.Drawing.Size(158, 22);
            this.txtCompanyTaxCodeXK.TabIndex = 68;
            // 
            // lblCompanyTaxCode
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            appearance22.TextVAlignAsString = "Middle";
            this.lblCompanyTaxCode.Appearance = appearance22;
            this.lblCompanyTaxCode.Location = new System.Drawing.Point(10, 73);
            this.lblCompanyTaxCode.Name = "lblCompanyTaxCode";
            this.lblCompanyTaxCode.Size = new System.Drawing.Size(76, 21);
            this.lblCompanyTaxCode.TabIndex = 67;
            this.lblCompanyTaxCode.Text = "Mã số thuế";
            // 
            // lblChungTuGoc
            // 
            this.lblChungTuGoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance23.BackColor = System.Drawing.Color.Transparent;
            appearance23.TextHAlignAsString = "Center";
            appearance23.TextVAlignAsString = "Middle";
            this.lblChungTuGoc.Appearance = appearance23;
            appearance24.TextHAlignAsString = "Center";
            appearance24.TextVAlignAsString = "Middle";
            this.lblChungTuGoc.HotTrackAppearance = appearance24;
            this.lblChungTuGoc.Location = new System.Drawing.Point(786, 121);
            this.lblChungTuGoc.Name = "lblChungTuGoc";
            this.lblChungTuGoc.Size = new System.Drawing.Size(139, 22);
            this.lblChungTuGoc.TabIndex = 66;
            this.lblChungTuGoc.Text = "Chứng từ gốc";
            // 
            // txtOriginalNo
            // 
            this.txtOriginalNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance25.TextVAlignAsString = "Middle";
            this.txtOriginalNo.Appearance = appearance25;
            this.txtOriginalNo.AutoSize = false;
            this.txtOriginalNo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtOriginalNo.Location = new System.Drawing.Point(132, 121);
            this.txtOriginalNo.Name = "txtOriginalNo";
            this.txtOriginalNo.Size = new System.Drawing.Size(648, 22);
            this.txtOriginalNo.TabIndex = 65;
            // 
            // lblNumberAttach
            // 
            appearance26.BackColor = System.Drawing.Color.Transparent;
            appearance26.TextVAlignAsString = "Middle";
            this.lblNumberAttach.Appearance = appearance26;
            this.lblNumberAttach.Location = new System.Drawing.Point(10, 121);
            this.lblNumberAttach.Name = "lblNumberAttach";
            this.lblNumberAttach.Size = new System.Drawing.Size(76, 21);
            this.lblNumberAttach.TabIndex = 64;
            this.lblNumberAttach.Text = "Kèm theo";
            // 
            // txtAccountingObjectNameXK
            // 
            this.txtAccountingObjectNameXK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance27.TextVAlignAsString = "Middle";
            this.txtAccountingObjectNameXK.Appearance = appearance27;
            this.txtAccountingObjectNameXK.AutoSize = false;
            this.txtAccountingObjectNameXK.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectNameXK.Location = new System.Drawing.Point(296, 25);
            this.txtAccountingObjectNameXK.Name = "txtAccountingObjectNameXK";
            this.txtAccountingObjectNameXK.Size = new System.Drawing.Size(629, 22);
            this.txtAccountingObjectNameXK.TabIndex = 63;
            this.txtAccountingObjectNameXK.TextChanged += new System.EventHandler(this.txtAccountingObjectNameHD_TextChanged);
            // 
            // cbbAccountingObjectXK
            // 
            appearance28.TextVAlignAsString = "Middle";
            this.cbbAccountingObjectXK.Appearance = appearance28;
            this.cbbAccountingObjectXK.AutoSize = false;
            this.cbbAccountingObjectXK.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectXK.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjectXK.Location = new System.Drawing.Point(132, 25);
            this.cbbAccountingObjectXK.Name = "cbbAccountingObjectXK";
            this.cbbAccountingObjectXK.Size = new System.Drawing.Size(158, 22);
            this.cbbAccountingObjectXK.TabIndex = 62;
            // 
            // txtSReason
            // 
            this.txtSReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance29.TextVAlignAsString = "Middle";
            this.txtSReason.Appearance = appearance29;
            this.txtSReason.AutoSize = false;
            this.txtSReason.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtSReason.Location = new System.Drawing.Point(132, 97);
            this.txtSReason.Name = "txtSReason";
            this.txtSReason.Size = new System.Drawing.Size(793, 22);
            this.txtSReason.TabIndex = 61;
            // 
            // lblSReason
            // 
            appearance30.BackColor = System.Drawing.Color.Transparent;
            appearance30.TextVAlignAsString = "Middle";
            this.lblSReason.Appearance = appearance30;
            this.lblSReason.Location = new System.Drawing.Point(10, 97);
            this.lblSReason.Name = "lblSReason";
            this.lblSReason.Size = new System.Drawing.Size(76, 21);
            this.lblSReason.TabIndex = 60;
            this.lblSReason.Text = "Lý do xuất";
            // 
            // txtSContactNameXK
            // 
            this.txtSContactNameXK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance31.TextVAlignAsString = "Middle";
            this.txtSContactNameXK.Appearance = appearance31;
            this.txtSContactNameXK.AutoSize = false;
            this.txtSContactNameXK.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtSContactNameXK.Location = new System.Drawing.Point(474, 73);
            this.txtSContactNameXK.Name = "txtSContactNameXK";
            this.txtSContactNameXK.Size = new System.Drawing.Size(451, 22);
            this.txtSContactNameXK.TabIndex = 59;
            // 
            // lblSContactName
            // 
            appearance32.BackColor = System.Drawing.Color.Transparent;
            appearance32.TextVAlignAsString = "Middle";
            this.lblSContactName.Appearance = appearance32;
            this.lblSContactName.Location = new System.Drawing.Point(296, 73);
            this.lblSContactName.Name = "lblSContactName";
            this.lblSContactName.Size = new System.Drawing.Size(135, 21);
            this.lblSContactName.TabIndex = 58;
            this.lblSContactName.Text = "Họ tên người nhận hàng";
            // 
            // txtAccountingObjectAddressXK
            // 
            this.txtAccountingObjectAddressXK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance33.TextVAlignAsString = "Middle";
            this.txtAccountingObjectAddressXK.Appearance = appearance33;
            this.txtAccountingObjectAddressXK.AutoSize = false;
            this.txtAccountingObjectAddressXK.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectAddressXK.Location = new System.Drawing.Point(132, 49);
            this.txtAccountingObjectAddressXK.Name = "txtAccountingObjectAddressXK";
            this.txtAccountingObjectAddressXK.Size = new System.Drawing.Size(793, 22);
            this.txtAccountingObjectAddressXK.TabIndex = 57;
            this.txtAccountingObjectAddressXK.TextChanged += new System.EventHandler(this.txtAccountingObjectNameHD_TextChanged);
            // 
            // lblDiaChi
            // 
            appearance34.BackColor = System.Drawing.Color.Transparent;
            appearance34.TextVAlignAsString = "Middle";
            this.lblDiaChi.Appearance = appearance34;
            this.lblDiaChi.Location = new System.Drawing.Point(10, 49);
            this.lblDiaChi.Name = "lblDiaChi";
            this.lblDiaChi.Size = new System.Drawing.Size(76, 21);
            this.lblDiaChi.TabIndex = 56;
            this.lblDiaChi.Text = "Địa chỉ";
            // 
            // lblKhachHang
            // 
            appearance35.BackColor = System.Drawing.Color.Transparent;
            appearance35.TextVAlignAsString = "Middle";
            this.lblKhachHang.Appearance = appearance35;
            this.lblKhachHang.Location = new System.Drawing.Point(10, 24);
            this.lblKhachHang.Name = "lblKhachHang";
            this.lblKhachHang.Size = new System.Drawing.Size(76, 22);
            this.lblKhachHang.TabIndex = 55;
            this.lblKhachHang.Text = "Nhà cung cấp";
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox4);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(934, 156);
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox4.Controls.Add(this.ultraLabel40);
            this.ultraGroupBox4.Controls.Add(this.cbbLoaiHD);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel41);
            this.ultraGroupBox4.Controls.Add(this.cbbHinhThucHD);
            this.ultraGroupBox4.Controls.Add(this.txtSoHD);
            this.ultraGroupBox4.Controls.Add(this.txtKyHieuHD);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel42);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel43);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel44);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel45);
            this.ultraGroupBox4.Controls.Add(this.cbbMauHD);
            this.ultraGroupBox4.Controls.Add(this.dteNgayHD);
            this.ultraGroupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox4.Location = new System.Drawing.Point(648, 1);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(287, 148);
            this.ultraGroupBox4.TabIndex = 32;
            this.ultraGroupBox4.Text = "Hóa đơn";
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel40
            // 
            this.ultraLabel40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance37.BackColor = System.Drawing.Color.Transparent;
            appearance37.TextVAlignAsString = "Middle";
            this.ultraLabel40.Appearance = appearance37;
            this.ultraLabel40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel40.Location = new System.Drawing.Point(6, 44);
            this.ultraLabel40.Name = "ultraLabel40";
            this.ultraLabel40.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel40.TabIndex = 75;
            this.ultraLabel40.Text = "Loại HĐ";
            // 
            // cbbLoaiHD
            // 
            this.cbbLoaiHD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbLoaiHD.AutoSize = false;
            this.cbbLoaiHD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbLoaiHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbLoaiHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbLoaiHD.Location = new System.Drawing.Point(76, 44);
            this.cbbLoaiHD.Name = "cbbLoaiHD";
            this.cbbLoaiHD.Size = new System.Drawing.Size(205, 22);
            this.cbbLoaiHD.TabIndex = 15;
            // 
            // ultraLabel41
            // 
            this.ultraLabel41.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance38.BackColor = System.Drawing.Color.Transparent;
            appearance38.TextVAlignAsString = "Middle";
            this.ultraLabel41.Appearance = appearance38;
            this.ultraLabel41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel41.Location = new System.Drawing.Point(6, 20);
            this.ultraLabel41.Name = "ultraLabel41";
            this.ultraLabel41.Size = new System.Drawing.Size(68, 22);
            this.ultraLabel41.TabIndex = 73;
            this.ultraLabel41.Text = "Hình thứcHĐ";
            // 
            // cbbHinhThucHD
            // 
            this.cbbHinhThucHD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbHinhThucHD.AutoSize = false;
            this.cbbHinhThucHD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbHinhThucHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbHinhThucHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbHinhThucHD.Location = new System.Drawing.Point(76, 20);
            this.cbbHinhThucHD.Name = "cbbHinhThucHD";
            this.cbbHinhThucHD.Size = new System.Drawing.Size(205, 22);
            this.cbbHinhThucHD.TabIndex = 14;
            // 
            // txtSoHD
            // 
            this.txtSoHD.AutoSize = false;
            this.txtSoHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHD.Location = new System.Drawing.Point(74, 118);
            this.txtSoHD.Name = "txtSoHD";
            this.txtSoHD.Size = new System.Drawing.Size(71, 22);
            this.txtSoHD.TabIndex = 18;
            this.txtSoHD.TextChanged += new System.EventHandler(this.txtSoHD_TextChanged);
            // 
            // txtKyHieuHD
            // 
            this.txtKyHieuHD.AutoSize = false;
            this.txtKyHieuHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuHD.Location = new System.Drawing.Point(76, 93);
            this.txtKyHieuHD.Name = "txtKyHieuHD";
            this.txtKyHieuHD.Size = new System.Drawing.Size(205, 22);
            this.txtKyHieuHD.TabIndex = 17;
            this.txtKyHieuHD.TextChanged += new System.EventHandler(this.txtKyHieuHD_TextChansged);
            // 
            // ultraLabel42
            // 
            this.ultraLabel42.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance39.BackColor = System.Drawing.Color.Transparent;
            appearance39.TextVAlignAsString = "Middle";
            this.ultraLabel42.Appearance = appearance39;
            this.ultraLabel42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel42.Location = new System.Drawing.Point(144, 119);
            this.ultraLabel42.Name = "ultraLabel42";
            this.ultraLabel42.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel42.TabIndex = 69;
            this.ultraLabel42.Text = "Ngày HĐ";
            // 
            // ultraLabel43
            // 
            this.ultraLabel43.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance40.BackColor = System.Drawing.Color.Transparent;
            appearance40.TextVAlignAsString = "Middle";
            this.ultraLabel43.Appearance = appearance40;
            this.ultraLabel43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel43.Location = new System.Drawing.Point(5, 118);
            this.ultraLabel43.Name = "ultraLabel43";
            this.ultraLabel43.Size = new System.Drawing.Size(40, 22);
            this.ultraLabel43.TabIndex = 68;
            this.ultraLabel43.Text = "Số HĐ";
            // 
            // ultraLabel44
            // 
            this.ultraLabel44.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance41.BackColor = System.Drawing.Color.Transparent;
            appearance41.TextVAlignAsString = "Middle";
            this.ultraLabel44.Appearance = appearance41;
            this.ultraLabel44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel44.Location = new System.Drawing.Point(5, 93);
            this.ultraLabel44.Name = "ultraLabel44";
            this.ultraLabel44.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel44.TabIndex = 67;
            this.ultraLabel44.Text = "Ký hiệu HĐ";
            // 
            // ultraLabel45
            // 
            this.ultraLabel45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance42.BackColor = System.Drawing.Color.Transparent;
            appearance42.TextVAlignAsString = "Middle";
            this.ultraLabel45.Appearance = appearance42;
            this.ultraLabel45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel45.Location = new System.Drawing.Point(7, 72);
            this.ultraLabel45.Name = "ultraLabel45";
            this.ultraLabel45.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel45.TabIndex = 66;
            this.ultraLabel45.Text = "Mẫu số HĐ";
            // 
            // cbbMauHD
            // 
            this.cbbMauHD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbMauHD.AutoSize = false;
            this.cbbMauHD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbMauHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbMauHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbMauHD.Location = new System.Drawing.Point(76, 69);
            this.cbbMauHD.Name = "cbbMauHD";
            this.cbbMauHD.Size = new System.Drawing.Size(205, 22);
            this.cbbMauHD.TabIndex = 16;
            // 
            // dteNgayHD
            // 
            appearance43.TextHAlignAsString = "Center";
            appearance43.TextVAlignAsString = "Middle";
            this.dteNgayHD.Appearance = appearance43;
            this.dteNgayHD.AutoSize = false;
            this.dteNgayHD.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteNgayHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dteNgayHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteNgayHD.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteNgayHD.Location = new System.Drawing.Point(197, 119);
            this.dteNgayHD.MaskInput = "";
            this.dteNgayHD.Name = "dteNgayHD";
            this.dteNgayHD.Size = new System.Drawing.Size(84, 22);
            this.dteNgayHD.TabIndex = 19;
            this.dteNgayHD.Value = null;
            this.dteNgayHD.ValueChanged += new System.EventHandler(this.dteNgayHD_ValueChanged);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox1.Controls.Add(this.txtAccountingBankName);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel28);
            this.ultraGroupBox1.Controls.Add(this.cbbAccountingObjectBankAccount);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel33);
            this.ultraGroupBox1.Controls.Add(this.txtThanhToan);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel24);
            this.ultraGroupBox1.Controls.Add(this.dteDate1);
            this.ultraGroupBox1.Controls.Add(this.txtNoBK1);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel16);
            this.ultraGroupBox1.Controls.Add(this.chkBangke1);
            this.ultraGroupBox1.Controls.Add(this.txtCompanyTaxCodeHD1);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel17);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel18);
            this.ultraGroupBox1.Controls.Add(this.txtMatHang1);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel19);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectNameHD1);
            this.ultraGroupBox1.Controls.Add(this.cbbAccountingObjectHD1);
            this.ultraGroupBox1.Controls.Add(this.txtReasonReturn1);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel20);
            this.ultraGroupBox1.Controls.Add(this.txtSContactNameHD1);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel21);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectAddressHD1);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel22);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel23);
            appearance65.FontData.BoldAsString = "True";
            appearance65.FontData.SizeInPoints = 10F;
            this.ultraGroupBox1.HeaderAppearance = appearance65;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(648, 148);
            this.ultraGroupBox1.TabIndex = 27;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtAccountingBankName
            // 
            this.txtAccountingBankName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingBankName.AutoSize = false;
            this.txtAccountingBankName.Location = new System.Drawing.Point(495, 73);
            this.txtAccountingBankName.Name = "txtAccountingBankName";
            this.txtAccountingBankName.Size = new System.Drawing.Size(147, 22);
            this.txtAccountingBankName.TabIndex = 86;
            // 
            // ultraLabel28
            // 
            this.ultraLabel28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance44.BackColor = System.Drawing.Color.Transparent;
            appearance44.TextVAlignAsString = "Middle";
            this.ultraLabel28.Appearance = appearance44;
            this.ultraLabel28.Location = new System.Drawing.Point(381, 73);
            this.ultraLabel28.Name = "ultraLabel28";
            this.ultraLabel28.Size = new System.Drawing.Size(93, 22);
            this.ultraLabel28.TabIndex = 87;
            this.ultraLabel28.Text = "Tên ngân hàng";
            // 
            // cbbAccountingObjectBankAccount
            // 
            this.cbbAccountingObjectBankAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbAccountingObjectBankAccount.AutoSize = false;
            this.cbbAccountingObjectBankAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectBankAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjectBankAccount.Location = new System.Drawing.Point(281, 73);
            this.cbbAccountingObjectBankAccount.Name = "cbbAccountingObjectBankAccount";
            this.cbbAccountingObjectBankAccount.Size = new System.Drawing.Size(95, 22);
            this.cbbAccountingObjectBankAccount.TabIndex = 79;
            // 
            // ultraLabel33
            // 
            this.ultraLabel33.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance45.BackColor = System.Drawing.Color.Transparent;
            appearance45.TextVAlignAsString = "Middle";
            this.ultraLabel33.Appearance = appearance45;
            this.ultraLabel33.Location = new System.Drawing.Point(184, 73);
            this.ultraLabel33.Name = "ultraLabel33";
            this.ultraLabel33.Size = new System.Drawing.Size(79, 21);
            this.ultraLabel33.TabIndex = 78;
            this.ultraLabel33.Text = "TK ngân hàng";
            // 
            // txtThanhToan
            // 
            this.txtThanhToan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtThanhToan.AutoSize = false;
            this.txtThanhToan.Location = new System.Drawing.Point(495, 97);
            this.txtThanhToan.Name = "txtThanhToan";
            this.txtThanhToan.Size = new System.Drawing.Size(147, 22);
            this.txtThanhToan.TabIndex = 77;
            this.txtThanhToan.TextChanged += new System.EventHandler(this.txtThanhToan_TextChanged);
            // 
            // ultraLabel24
            // 
            this.ultraLabel24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance46.BackColor = System.Drawing.Color.Transparent;
            appearance46.TextVAlignAsString = "Middle";
            this.ultraLabel24.Appearance = appearance46;
            this.ultraLabel24.Location = new System.Drawing.Point(382, 97);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(109, 22);
            this.ultraLabel24.TabIndex = 76;
            this.ultraLabel24.Text = "Hình thức thanh toán";
            // 
            // dteDate1
            // 
            appearance47.TextHAlignAsString = "Center";
            appearance47.TextVAlignAsString = "Middle";
            this.dteDate1.Appearance = appearance47;
            this.dteDate1.AutoSize = false;
            this.dteDate1.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteDate1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.dteDate1.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteDate1.Location = new System.Drawing.Point(294, 121);
            this.dteDate1.MaskInput = "";
            this.dteDate1.Name = "dteDate1";
            this.dteDate1.Size = new System.Drawing.Size(82, 22);
            this.dteDate1.TabIndex = 75;
            this.dteDate1.Value = null;
            // 
            // txtNoBK1
            // 
            appearance48.TextVAlignAsString = "Middle";
            this.txtNoBK1.Appearance = appearance48;
            this.txtNoBK1.AutoSize = false;
            this.txtNoBK1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtNoBK1.Location = new System.Drawing.Point(187, 121);
            this.txtNoBK1.Name = "txtNoBK1";
            this.txtNoBK1.Size = new System.Drawing.Size(56, 22);
            this.txtNoBK1.TabIndex = 72;
            // 
            // ultraLabel10
            // 
            appearance49.BackColor = System.Drawing.Color.Transparent;
            appearance49.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance49;
            this.ultraLabel10.Location = new System.Drawing.Point(249, 121);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(42, 21);
            this.ultraLabel10.TabIndex = 71;
            this.ultraLabel10.Text = "Ngày";
            // 
            // ultraLabel16
            // 
            appearance50.BackColor = System.Drawing.Color.Transparent;
            appearance50.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance50;
            this.ultraLabel16.Location = new System.Drawing.Point(156, 121);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(25, 22);
            this.ultraLabel16.TabIndex = 70;
            this.ultraLabel16.Text = "Số";
            // 
            // chkBangke1
            // 
            this.chkBangke1.BackColor = System.Drawing.Color.Transparent;
            this.chkBangke1.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkBangke1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.chkBangke1.Location = new System.Drawing.Point(134, 121);
            this.chkBangke1.Name = "chkBangke1";
            this.chkBangke1.Size = new System.Drawing.Size(19, 22);
            this.chkBangke1.TabIndex = 69;
            // 
            // txtCompanyTaxCodeHD1
            // 
            this.txtCompanyTaxCodeHD1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance51.TextVAlignAsString = "Middle";
            this.txtCompanyTaxCodeHD1.Appearance = appearance51;
            this.txtCompanyTaxCodeHD1.AutoSize = false;
            this.txtCompanyTaxCodeHD1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtCompanyTaxCodeHD1.Location = new System.Drawing.Point(495, 49);
            this.txtCompanyTaxCodeHD1.Name = "txtCompanyTaxCodeHD1";
            this.txtCompanyTaxCodeHD1.Size = new System.Drawing.Size(147, 22);
            this.txtCompanyTaxCodeHD1.TabIndex = 68;
            // 
            // ultraLabel17
            // 
            this.ultraLabel17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance52.BackColor = System.Drawing.Color.Transparent;
            appearance52.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance52;
            this.ultraLabel17.Location = new System.Drawing.Point(383, 49);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(74, 21);
            this.ultraLabel17.TabIndex = 67;
            this.ultraLabel17.Text = "Mã số thuế";
            // 
            // ultraLabel18
            // 
            appearance53.BackColor = System.Drawing.Color.Transparent;
            appearance53.TextVAlignAsString = "Middle";
            this.ultraLabel18.Appearance = appearance53;
            this.ultraLabel18.Location = new System.Drawing.Point(383, 123);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(62, 19);
            this.ultraLabel18.TabIndex = 66;
            this.ultraLabel18.Text = "Mặt hàng";
            // 
            // txtMatHang1
            // 
            this.txtMatHang1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance54.TextVAlignAsString = "Middle";
            this.txtMatHang1.Appearance = appearance54;
            this.txtMatHang1.AutoSize = false;
            this.txtMatHang1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtMatHang1.Location = new System.Drawing.Point(495, 121);
            this.txtMatHang1.Name = "txtMatHang1";
            this.txtMatHang1.Size = new System.Drawing.Size(147, 22);
            this.txtMatHang1.TabIndex = 65;
            // 
            // ultraLabel19
            // 
            appearance55.BackColor = System.Drawing.Color.Transparent;
            appearance55.TextVAlignAsString = "Middle";
            this.ultraLabel19.Appearance = appearance55;
            this.ultraLabel19.Location = new System.Drawing.Point(4, 121);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(115, 22);
            this.ultraLabel19.TabIndex = 64;
            this.ultraLabel19.Text = "Bảng kê";
            // 
            // txtAccountingObjectNameHD1
            // 
            this.txtAccountingObjectNameHD1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance56.TextVAlignAsString = "Middle";
            this.txtAccountingObjectNameHD1.Appearance = appearance56;
            this.txtAccountingObjectNameHD1.AutoSize = false;
            this.txtAccountingObjectNameHD1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectNameHD1.Location = new System.Drawing.Point(267, 25);
            this.txtAccountingObjectNameHD1.Name = "txtAccountingObjectNameHD1";
            this.txtAccountingObjectNameHD1.Size = new System.Drawing.Size(375, 22);
            this.txtAccountingObjectNameHD1.TabIndex = 63;
            this.txtAccountingObjectNameHD1.TextChanged += new System.EventHandler(this.txtAccountingObjectNameHD_TextChanged);
            // 
            // cbbAccountingObjectHD1
            // 
            appearance57.TextVAlignAsString = "Middle";
            this.cbbAccountingObjectHD1.Appearance = appearance57;
            this.cbbAccountingObjectHD1.AutoSize = false;
            this.cbbAccountingObjectHD1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectHD1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjectHD1.Location = new System.Drawing.Point(134, 25);
            this.cbbAccountingObjectHD1.Name = "cbbAccountingObjectHD1";
            this.cbbAccountingObjectHD1.Size = new System.Drawing.Size(127, 22);
            this.cbbAccountingObjectHD1.TabIndex = 62;
            // 
            // txtReasonReturn1
            // 
            this.txtReasonReturn1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance58.TextVAlignAsString = "Middle";
            this.txtReasonReturn1.Appearance = appearance58;
            this.txtReasonReturn1.AutoSize = false;
            this.txtReasonReturn1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReasonReturn1.Location = new System.Drawing.Point(134, 97);
            this.txtReasonReturn1.Name = "txtReasonReturn1";
            this.txtReasonReturn1.Size = new System.Drawing.Size(242, 22);
            this.txtReasonReturn1.TabIndex = 61;
            // 
            // ultraLabel20
            // 
            appearance59.BackColor = System.Drawing.Color.Transparent;
            appearance59.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance59;
            this.ultraLabel20.Location = new System.Drawing.Point(4, 97);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(115, 20);
            this.ultraLabel20.TabIndex = 60;
            this.ultraLabel20.Text = "Diễn giải";
            // 
            // txtSContactNameHD1
            // 
            this.txtSContactNameHD1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance60.TextVAlignAsString = "Middle";
            this.txtSContactNameHD1.Appearance = appearance60;
            this.txtSContactNameHD1.AutoSize = false;
            this.txtSContactNameHD1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtSContactNameHD1.Location = new System.Drawing.Point(134, 73);
            this.txtSContactNameHD1.Name = "txtSContactNameHD1";
            this.txtSContactNameHD1.Size = new System.Drawing.Size(47, 22);
            this.txtSContactNameHD1.TabIndex = 59;
            // 
            // ultraLabel21
            // 
            appearance61.BackColor = System.Drawing.Color.Transparent;
            appearance61.TextVAlignAsString = "Middle";
            this.ultraLabel21.Appearance = appearance61;
            this.ultraLabel21.Location = new System.Drawing.Point(4, 73);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(125, 21);
            this.ultraLabel21.TabIndex = 58;
            this.ultraLabel21.Text = "Họ tên người nhận hàng";
            // 
            // txtAccountingObjectAddressHD1
            // 
            this.txtAccountingObjectAddressHD1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance62.TextVAlignAsString = "Middle";
            this.txtAccountingObjectAddressHD1.Appearance = appearance62;
            this.txtAccountingObjectAddressHD1.AutoSize = false;
            this.txtAccountingObjectAddressHD1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectAddressHD1.Location = new System.Drawing.Point(134, 49);
            this.txtAccountingObjectAddressHD1.Name = "txtAccountingObjectAddressHD1";
            this.txtAccountingObjectAddressHD1.Size = new System.Drawing.Size(242, 22);
            this.txtAccountingObjectAddressHD1.TabIndex = 57;
            this.txtAccountingObjectAddressHD1.TextChanged += new System.EventHandler(this.txtAccountingObjectNameHD_TextChanged);
            // 
            // ultraLabel22
            // 
            appearance63.BackColor = System.Drawing.Color.Transparent;
            appearance63.TextVAlignAsString = "Middle";
            this.ultraLabel22.Appearance = appearance63;
            this.ultraLabel22.Location = new System.Drawing.Point(4, 49);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(115, 21);
            this.ultraLabel22.TabIndex = 56;
            this.ultraLabel22.Text = "Địa chỉ";
            // 
            // ultraLabel23
            // 
            appearance64.BackColor = System.Drawing.Color.Transparent;
            appearance64.TextVAlignAsString = "Middle";
            this.ultraLabel23.Appearance = appearance64;
            this.ultraLabel23.Location = new System.Drawing.Point(4, 25);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(115, 22);
            this.ultraLabel23.TabIndex = 55;
            this.ultraLabel23.Text = "Nhà cung cấp";
            // 
            // uGridControl
            // 
            this.uGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridControl.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridControl.Location = new System.Drawing.Point(449, 98);
            this.uGridControl.Name = "uGridControl";
            this.uGridControl.Size = new System.Drawing.Size(487, 43);
            this.uGridControl.TabIndex = 1;
            this.uGridControl.Text = "ultraGrid1";
            // 
            // palFill
            // 
            this.palFill.AutoSize = true;
            this.palFill.BackColor = System.Drawing.Color.Transparent;
            this.palFill.Controls.Add(this.palGrid);
            this.palFill.Controls.Add(this.splitter);
            this.palFill.Controls.Add(this.palThongTinChung);
            this.palFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palFill.Location = new System.Drawing.Point(0, 68);
            this.palFill.Name = "palFill";
            this.palFill.Size = new System.Drawing.Size(936, 501);
            this.palFill.TabIndex = 31;
            // 
            // palGrid
            // 
            // 
            // palGrid.ClientArea
            // 
            this.palGrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.palGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palGrid.Location = new System.Drawing.Point(0, 189);
            this.palGrid.Name = "palGrid";
            this.palGrid.Size = new System.Drawing.Size(936, 312);
            this.palGrid.TabIndex = 28;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance66.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance66;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(842, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 6;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // splitter
            // 
            this.splitter.BackColor = System.Drawing.SystemColors.Control;
            this.splitter.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter.Location = new System.Drawing.Point(0, 179);
            this.splitter.Name = "splitter";
            this.splitter.RestoreExtent = 174;
            this.splitter.Size = new System.Drawing.Size(936, 10);
            this.splitter.TabIndex = 1;
            // 
            // palThongTinChung
            // 
            this.palThongTinChung.Controls.Add(this.ultraPanel1);
            this.palThongTinChung.Controls.Add(this.uGroupBoxEx);
            this.palThongTinChung.Dock = System.Windows.Forms.DockStyle.Top;
            this.palThongTinChung.Location = new System.Drawing.Point(0, 0);
            this.palThongTinChung.Name = "palThongTinChung";
            this.palThongTinChung.Size = new System.Drawing.Size(936, 179);
            this.palThongTinChung.TabIndex = 27;
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox2);
            this.ultraPanel1.ClientArea.Controls.Add(this.uGroupBoxStand);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 179);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(936, 134);
            this.ultraPanel1.TabIndex = 29;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox2.Controls.Add(this.txtMauSoHDI);
            this.ultraGroupBox2.Controls.Add(this.txtSoHDI);
            this.ultraGroupBox2.Controls.Add(this.txtKyHieuHDI);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel29);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel30);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel34);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel35);
            this.ultraGroupBox2.Controls.Add(this.dteNgayHDI);
            this.ultraGroupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox2.Location = new System.Drawing.Point(672, 1);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(263, 129);
            this.ultraGroupBox2.TabIndex = 35;
            this.ultraGroupBox2.Text = "Hóa đơn";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtMauSoHDI
            // 
            this.txtMauSoHDI.AutoSize = false;
            this.txtMauSoHDI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMauSoHDI.Location = new System.Drawing.Point(77, 21);
            this.txtMauSoHDI.Name = "txtMauSoHDI";
            this.txtMauSoHDI.Size = new System.Drawing.Size(166, 22);
            this.txtMauSoHDI.TabIndex = 76;
            // 
            // txtSoHDI
            // 
            this.txtSoHDI.AutoSize = false;
            this.txtSoHDI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHDI.Location = new System.Drawing.Point(77, 72);
            this.txtSoHDI.Name = "txtSoHDI";
            this.txtSoHDI.Size = new System.Drawing.Size(166, 22);
            this.txtSoHDI.TabIndex = 18;
            // 
            // txtKyHieuHDI
            // 
            this.txtKyHieuHDI.AutoSize = false;
            this.txtKyHieuHDI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuHDI.Location = new System.Drawing.Point(77, 46);
            this.txtKyHieuHDI.Name = "txtKyHieuHDI";
            this.txtKyHieuHDI.Size = new System.Drawing.Size(166, 22);
            this.txtKyHieuHDI.TabIndex = 17;
            // 
            // ultraLabel29
            // 
            this.ultraLabel29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance67.BackColor = System.Drawing.Color.Transparent;
            appearance67.TextVAlignAsString = "Middle";
            this.ultraLabel29.Appearance = appearance67;
            this.ultraLabel29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel29.Location = new System.Drawing.Point(10, 99);
            this.ultraLabel29.Name = "ultraLabel29";
            this.ultraLabel29.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel29.TabIndex = 69;
            this.ultraLabel29.Text = "Ngày HĐ";
            // 
            // ultraLabel30
            // 
            this.ultraLabel30.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance68.BackColor = System.Drawing.Color.Transparent;
            appearance68.TextVAlignAsString = "Middle";
            this.ultraLabel30.Appearance = appearance68;
            this.ultraLabel30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel30.Location = new System.Drawing.Point(9, 73);
            this.ultraLabel30.Name = "ultraLabel30";
            this.ultraLabel30.Size = new System.Drawing.Size(40, 22);
            this.ultraLabel30.TabIndex = 68;
            this.ultraLabel30.Text = "Số HĐ";
            // 
            // ultraLabel34
            // 
            this.ultraLabel34.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance69.BackColor = System.Drawing.Color.Transparent;
            appearance69.TextVAlignAsString = "Middle";
            this.ultraLabel34.Appearance = appearance69;
            this.ultraLabel34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel34.Location = new System.Drawing.Point(9, 48);
            this.ultraLabel34.Name = "ultraLabel34";
            this.ultraLabel34.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel34.TabIndex = 67;
            this.ultraLabel34.Text = "Ký hiệu HĐ";
            // 
            // ultraLabel35
            // 
            this.ultraLabel35.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance70.BackColor = System.Drawing.Color.Transparent;
            appearance70.TextVAlignAsString = "Middle";
            this.ultraLabel35.Appearance = appearance70;
            this.ultraLabel35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel35.Location = new System.Drawing.Point(10, 24);
            this.ultraLabel35.Name = "ultraLabel35";
            this.ultraLabel35.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel35.TabIndex = 66;
            this.ultraLabel35.Text = "Mẫu số HĐ";
            // 
            // dteNgayHDI
            // 
            appearance71.TextHAlignAsString = "Center";
            appearance71.TextVAlignAsString = "Middle";
            this.dteNgayHDI.Appearance = appearance71;
            this.dteNgayHDI.AutoSize = false;
            this.dteNgayHDI.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteNgayHDI.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dteNgayHDI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteNgayHDI.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteNgayHDI.Location = new System.Drawing.Point(77, 98);
            this.dteNgayHDI.MaskInput = "";
            this.dteNgayHDI.Name = "dteNgayHDI";
            this.dteNgayHDI.Size = new System.Drawing.Size(100, 22);
            this.dteNgayHDI.TabIndex = 19;
            this.dteNgayHDI.Value = null;
            // 
            // uGroupBoxStand
            // 
            this.uGroupBoxStand.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxStand.Controls.Add(this.txtCompanyTaxCode);
            this.uGroupBoxStand.Controls.Add(this.txtAccountingObjectName);
            this.uGroupBoxStand.Controls.Add(this.cbbAccountingObject);
            this.uGroupBoxStand.Controls.Add(this.txtReasonDiscount);
            this.uGroupBoxStand.Controls.Add(this.txtSContactName);
            this.uGroupBoxStand.Controls.Add(this.txtAccountingObjectAddress);
            this.uGroupBoxStand.Controls.Add(this.btnVouchersReturn);
            this.uGroupBoxStand.Controls.Add(this.ultraLabel11);
            this.uGroupBoxStand.Controls.Add(this.ultraLabel12);
            this.uGroupBoxStand.Controls.Add(this.ultraLabel13);
            this.uGroupBoxStand.Controls.Add(this.ultraLabel14);
            this.uGroupBoxStand.Controls.Add(this.ultraLabel15);
            appearance82.FontData.BoldAsString = "True";
            appearance82.FontData.SizeInPoints = 10F;
            this.uGroupBoxStand.HeaderAppearance = appearance82;
            this.uGroupBoxStand.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.uGroupBoxStand.Location = new System.Drawing.Point(0, 1);
            this.uGroupBoxStand.Name = "uGroupBoxStand";
            this.uGroupBoxStand.Size = new System.Drawing.Size(672, 129);
            this.uGroupBoxStand.TabIndex = 29;
            this.uGroupBoxStand.Text = "Thông tin chung";
            this.uGroupBoxStand.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtCompanyTaxCode
            // 
            appearance72.TextVAlignAsString = "Middle";
            this.txtCompanyTaxCode.Appearance = appearance72;
            this.txtCompanyTaxCode.AutoSize = false;
            this.txtCompanyTaxCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtCompanyTaxCode.Location = new System.Drawing.Point(135, 74);
            this.txtCompanyTaxCode.Name = "txtCompanyTaxCode";
            this.txtCompanyTaxCode.Size = new System.Drawing.Size(156, 22);
            this.txtCompanyTaxCode.TabIndex = 81;
            // 
            // txtAccountingObjectName
            // 
            this.txtAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance73.TextVAlignAsString = "Middle";
            this.txtAccountingObjectName.Appearance = appearance73;
            this.txtAccountingObjectName.AutoSize = false;
            this.txtAccountingObjectName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectName.Location = new System.Drawing.Point(297, 26);
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(217, 22);
            this.txtAccountingObjectName.TabIndex = 80;
            // 
            // cbbAccountingObject
            // 
            appearance74.TextVAlignAsString = "Middle";
            this.cbbAccountingObject.Appearance = appearance74;
            this.cbbAccountingObject.AutoSize = false;
            this.cbbAccountingObject.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObject.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObject.Location = new System.Drawing.Point(135, 26);
            this.cbbAccountingObject.Name = "cbbAccountingObject";
            this.cbbAccountingObject.Size = new System.Drawing.Size(156, 22);
            this.cbbAccountingObject.TabIndex = 79;
            // 
            // txtReasonDiscount
            // 
            this.txtReasonDiscount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance75.TextVAlignAsString = "Middle";
            this.txtReasonDiscount.Appearance = appearance75;
            this.txtReasonDiscount.AutoSize = false;
            this.txtReasonDiscount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReasonDiscount.Location = new System.Drawing.Point(135, 98);
            this.txtReasonDiscount.Name = "txtReasonDiscount";
            this.txtReasonDiscount.Size = new System.Drawing.Size(527, 22);
            this.txtReasonDiscount.TabIndex = 78;
            // 
            // txtSContactName
            // 
            this.txtSContactName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSContactName.AutoSize = false;
            this.txtSContactName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtSContactName.Location = new System.Drawing.Point(433, 74);
            this.txtSContactName.Name = "txtSContactName";
            this.txtSContactName.Size = new System.Drawing.Size(229, 22);
            this.txtSContactName.TabIndex = 77;
            // 
            // txtAccountingObjectAddress
            // 
            this.txtAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance76.TextVAlignAsString = "Middle";
            this.txtAccountingObjectAddress.Appearance = appearance76;
            this.txtAccountingObjectAddress.AutoSize = false;
            this.txtAccountingObjectAddress.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectAddress.Location = new System.Drawing.Point(135, 50);
            this.txtAccountingObjectAddress.Name = "txtAccountingObjectAddress";
            this.txtAccountingObjectAddress.Size = new System.Drawing.Size(527, 22);
            this.txtAccountingObjectAddress.TabIndex = 76;
            // 
            // btnVouchersReturn
            // 
            this.btnVouchersReturn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVouchersReturn.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnVouchersReturn.Location = new System.Drawing.Point(520, 26);
            this.btnVouchersReturn.Name = "btnVouchersReturn";
            this.btnVouchersReturn.Size = new System.Drawing.Size(142, 22);
            this.btnVouchersReturn.TabIndex = 75;
            this.btnVouchersReturn.Text = "Chọn chứng từ mua hàng";
            this.btnVouchersReturn.Click += new System.EventHandler(this.btnVouchers_Click);
            // 
            // ultraLabel11
            // 
            appearance77.BackColor = System.Drawing.Color.Transparent;
            appearance77.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance77;
            this.ultraLabel11.Location = new System.Drawing.Point(12, 74);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(115, 21);
            this.ultraLabel11.TabIndex = 67;
            this.ultraLabel11.Text = "Mã số thuế";
            // 
            // ultraLabel12
            // 
            appearance78.BackColor = System.Drawing.Color.Transparent;
            appearance78.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance78;
            this.ultraLabel12.Location = new System.Drawing.Point(12, 98);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(115, 20);
            this.ultraLabel12.TabIndex = 60;
            this.ultraLabel12.Text = "Diễn giải";
            // 
            // ultraLabel13
            // 
            appearance79.BackColor = System.Drawing.Color.Transparent;
            appearance79.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance79;
            this.ultraLabel13.Location = new System.Drawing.Point(297, 74);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(130, 21);
            this.ultraLabel13.TabIndex = 58;
            this.ultraLabel13.Text = "Họ tên người nhận hàng";
            // 
            // ultraLabel14
            // 
            appearance80.BackColor = System.Drawing.Color.Transparent;
            appearance80.TextVAlignAsString = "Middle";
            this.ultraLabel14.Appearance = appearance80;
            this.ultraLabel14.Location = new System.Drawing.Point(12, 50);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(115, 21);
            this.ultraLabel14.TabIndex = 56;
            this.ultraLabel14.Text = "Địa chỉ";
            // 
            // ultraLabel15
            // 
            appearance81.BackColor = System.Drawing.Color.Transparent;
            appearance81.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance81;
            this.ultraLabel15.Location = new System.Drawing.Point(12, 26);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(115, 22);
            this.ultraLabel15.TabIndex = 55;
            this.ultraLabel15.Text = "Đối tượng";
            // 
            // uGroupBoxEx
            // 
            this.uGroupBoxEx.Controls.Add(this.ultraTabSharedControlsPage2);
            this.uGroupBoxEx.Controls.Add(this.ultraTabPageHoaDon);
            this.uGroupBoxEx.Controls.Add(this.ultraTabPagePhieuXuatKho);
            this.uGroupBoxEx.Controls.Add(this.ultraTabPageControl1);
            this.uGroupBoxEx.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupBoxEx.Location = new System.Drawing.Point(0, 0);
            this.uGroupBoxEx.Name = "uGroupBoxEx";
            this.uGroupBoxEx.SharedControlsPage = this.ultraTabSharedControlsPage2;
            this.uGroupBoxEx.Size = new System.Drawing.Size(936, 179);
            this.uGroupBoxEx.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon;
            this.uGroupBoxEx.TabIndex = 27;
            ultraTab4.Key = "tabHoaDon";
            ultraTab4.TabPage = this.ultraTabPageHoaDon;
            ultraTab4.Text = "Chứng từ";
            ultraTab5.Key = "tabPhieuXuatKho";
            ultraTab5.TabPage = this.ultraTabPagePhieuXuatKho;
            ultraTab5.Text = "Phiếu xuất kho";
            ultraTab2.Key = "tabHoaDon1";
            ultraTab2.TabPage = this.ultraTabPageControl1;
            ultraTab2.Text = "Hóa đơn";
            this.uGroupBoxEx.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab4,
            ultraTab5,
            ultraTab2});
            this.uGroupBoxEx.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            this.uGroupBoxEx.ActiveTabChanged += new Infragistics.Win.UltraWinTabControl.ActiveTabChangedEventHandler(this.ultraTabControlThongTinChung_ActiveTabChanged);
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(934, 156);
            // 
            // palTop
            // 
            this.palTop.Controls.Add(this.ultraGroupBoxTop);
            this.palTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTop.Location = new System.Drawing.Point(0, 0);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(936, 68);
            this.palTop.TabIndex = 29;
            // 
            // ultraGroupBoxTop
            // 
            this.ultraGroupBoxTop.Controls.Add(this.optPhieuXuatKho);
            this.ultraGroupBoxTop.Controls.Add(this.optHoaDon);
            this.ultraGroupBoxTop.Controls.Add(this.palTopSctNhtNctPXK);
            this.ultraGroupBoxTop.Controls.Add(this.palTopSctNhtNctHoaDon);
            this.ultraGroupBoxTop.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance87.FontData.BoldAsString = "True";
            appearance87.FontData.SizeInPoints = 13F;
            this.ultraGroupBoxTop.HeaderAppearance = appearance87;
            this.ultraGroupBoxTop.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxTop.Name = "ultraGroupBoxTop";
            this.ultraGroupBoxTop.Size = new System.Drawing.Size(936, 68);
            this.ultraGroupBoxTop.TabIndex = 31;
            this.ultraGroupBoxTop.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // optPhieuXuatKho
            // 
            this.optPhieuXuatKho.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance83.TextHAlignAsString = "Left";
            appearance83.TextVAlignAsString = "Middle";
            this.optPhieuXuatKho.Appearance = appearance83;
            this.optPhieuXuatKho.BackColor = System.Drawing.Color.Transparent;
            this.optPhieuXuatKho.BackColorInternal = System.Drawing.Color.Transparent;
            this.optPhieuXuatKho.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem3.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem3.DataValue = true;
            valueListItem3.DisplayText = "Kiêm phiếu xuất kho";
            valueListItem3.Tag = "KPXK";
            valueListItem4.DataValue = "ValueListItem1";
            valueListItem4.DisplayText = "Không kiêm phiếu xuất kho";
            valueListItem4.Tag = "KKPXK";
            this.optPhieuXuatKho.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem4});
            this.optPhieuXuatKho.ItemSpacingHorizontal = 1;
            this.optPhieuXuatKho.Location = new System.Drawing.Point(642, 9);
            this.optPhieuXuatKho.Name = "optPhieuXuatKho";
            this.optPhieuXuatKho.ReadOnly = false;
            this.optPhieuXuatKho.Size = new System.Drawing.Size(288, 17);
            this.optPhieuXuatKho.TabIndex = 74;
            this.optPhieuXuatKho.Visible = false;
            this.optPhieuXuatKho.ValueChanged += new System.EventHandler(this.OptPhieuXuatKhoValueChangedStand);
            // 
            // optHoaDon
            // 
            this.optHoaDon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance84.TextHAlignAsString = "Left";
            appearance84.TextVAlignAsString = "Middle";
            this.optHoaDon.Appearance = appearance84;
            this.optHoaDon.BackColor = System.Drawing.Color.Transparent;
            this.optHoaDon.BackColorInternal = System.Drawing.Color.Transparent;
            this.optHoaDon.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem7.DataValue = true;
            valueListItem7.DisplayText = "Lập kèm hóa đơn";
            valueListItem7.Tag = "KHD";
            valueListItem8.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem8.DataValue = false;
            valueListItem8.DisplayText = "Không kèm hóa đơn";
            valueListItem8.Tag = "KKHD";
            this.optHoaDon.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem7,
            valueListItem8});
            this.optHoaDon.ItemSpacingHorizontal = 17;
            this.optHoaDon.Location = new System.Drawing.Point(642, 32);
            this.optHoaDon.Name = "optHoaDon";
            this.optHoaDon.ReadOnly = false;
            this.optHoaDon.Size = new System.Drawing.Size(288, 17);
            this.optHoaDon.TabIndex = 73;
            this.optHoaDon.Visible = false;
            this.optHoaDon.ValueChanged += new System.EventHandler(this.optHoaDon_ValueChanged);
            // 
            // palTopSctNhtNctPXK
            // 
            appearance85.BackColor = System.Drawing.Color.Transparent;
            appearance85.BackColor2 = System.Drawing.Color.Transparent;
            this.palTopSctNhtNctPXK.Appearance = appearance85;
            this.palTopSctNhtNctPXK.AutoSize = true;
            this.palTopSctNhtNctPXK.Location = new System.Drawing.Point(12, 13);
            this.palTopSctNhtNctPXK.Name = "palTopSctNhtNctPXK";
            this.palTopSctNhtNctPXK.Size = new System.Drawing.Size(391, 36);
            this.palTopSctNhtNctPXK.TabIndex = 69;
            // 
            // palTopSctNhtNctHoaDon
            // 
            appearance86.BackColor = System.Drawing.Color.Transparent;
            appearance86.BackColor2 = System.Drawing.Color.Transparent;
            this.palTopSctNhtNctHoaDon.Appearance = appearance86;
            this.palTopSctNhtNctHoaDon.AutoSize = true;
            this.palTopSctNhtNctHoaDon.Location = new System.Drawing.Point(13, 13);
            this.palTopSctNhtNctHoaDon.Name = "palTopSctNhtNctHoaDon";
            this.palTopSctNhtNctHoaDon.Size = new System.Drawing.Size(390, 36);
            this.palTopSctNhtNctHoaDon.TabIndex = 68;
            // 
            // palBottom
            // 
            this.palBottom.Controls.Add(this.panel1);
            this.palBottom.Controls.Add(this.btnSelectBill);
            this.palBottom.Controls.Add(this.cbBill);
            this.palBottom.Controls.Add(this.lblTotalPaymentAmount);
            this.palBottom.Controls.Add(this.lblTotalVATAmount);
            this.palBottom.Controls.Add(this.lblTotalAmount);
            this.palBottom.Controls.Add(this.lblTotalPaymentAmountOriginal);
            this.palBottom.Controls.Add(this.lblTotalVATAmountOriginal);
            this.palBottom.Controls.Add(this.lblTotalAmountOriginal);
            this.palBottom.Controls.Add(this.lbl2);
            this.palBottom.Controls.Add(this.lbl3);
            this.palBottom.Controls.Add(this.lbl1);
            this.palBottom.Controls.Add(this.uGridControl);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 569);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(936, 145);
            this.palBottom.TabIndex = 30;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtOldInvSeries);
            this.panel1.Controls.Add(this.ultraLabel25);
            this.panel1.Controls.Add(this.txtOldInvTemplate);
            this.panel1.Controls.Add(this.ultraLabel26);
            this.panel1.Controls.Add(this.txtOldInvDate);
            this.panel1.Controls.Add(this.ultraLabel27);
            this.panel1.Controls.Add(this.txtOldInvNo);
            this.panel1.Controls.Add(this.lblTttt);
            this.panel1.Location = new System.Drawing.Point(0, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(656, 34);
            this.panel1.TabIndex = 71;
            // 
            // txtOldInvSeries
            // 
            this.txtOldInvSeries.AutoSize = false;
            this.txtOldInvSeries.Location = new System.Drawing.Point(538, 1);
            this.txtOldInvSeries.Name = "txtOldInvSeries";
            this.txtOldInvSeries.ReadOnly = true;
            this.txtOldInvSeries.Size = new System.Drawing.Size(111, 22);
            this.txtOldInvSeries.TabIndex = 49;
            // 
            // ultraLabel25
            // 
            appearance88.BackColor = System.Drawing.Color.Transparent;
            appearance88.TextVAlignAsString = "Middle";
            this.ultraLabel25.Appearance = appearance88;
            this.ultraLabel25.Location = new System.Drawing.Point(488, 3);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(43, 19);
            this.ultraLabel25.TabIndex = 48;
            this.ultraLabel25.Text = "Ký hiệu";
            // 
            // txtOldInvTemplate
            // 
            this.txtOldInvTemplate.AutoSize = false;
            this.txtOldInvTemplate.Location = new System.Drawing.Point(362, 0);
            this.txtOldInvTemplate.Name = "txtOldInvTemplate";
            this.txtOldInvTemplate.ReadOnly = true;
            this.txtOldInvTemplate.Size = new System.Drawing.Size(111, 22);
            this.txtOldInvTemplate.TabIndex = 47;
            // 
            // ultraLabel26
            // 
            appearance89.BackColor = System.Drawing.Color.Transparent;
            appearance89.TextVAlignAsString = "Middle";
            this.ultraLabel26.Appearance = appearance89;
            this.ultraLabel26.Location = new System.Drawing.Point(315, 3);
            this.ultraLabel26.Name = "ultraLabel26";
            this.ultraLabel26.Size = new System.Drawing.Size(44, 19);
            this.ultraLabel26.TabIndex = 46;
            this.ultraLabel26.Text = "Mẫu số";
            // 
            // txtOldInvDate
            // 
            this.txtOldInvDate.AutoSize = false;
            this.txtOldInvDate.Location = new System.Drawing.Point(197, 1);
            this.txtOldInvDate.Name = "txtOldInvDate";
            this.txtOldInvDate.ReadOnly = true;
            this.txtOldInvDate.Size = new System.Drawing.Size(111, 22);
            this.txtOldInvDate.TabIndex = 45;
            // 
            // ultraLabel27
            // 
            appearance90.BackColor = System.Drawing.Color.Transparent;
            appearance90.TextVAlignAsString = "Middle";
            this.ultraLabel27.Appearance = appearance90;
            this.ultraLabel27.Location = new System.Drawing.Point(153, 4);
            this.ultraLabel27.Name = "ultraLabel27";
            this.ultraLabel27.Size = new System.Drawing.Size(37, 19);
            this.ultraLabel27.TabIndex = 44;
            this.ultraLabel27.Text = "Ngày";
            // 
            // txtOldInvNo
            // 
            this.txtOldInvNo.AutoSize = false;
            this.txtOldInvNo.Location = new System.Drawing.Point(31, 1);
            this.txtOldInvNo.Name = "txtOldInvNo";
            this.txtOldInvNo.ReadOnly = true;
            this.txtOldInvNo.Size = new System.Drawing.Size(111, 22);
            this.txtOldInvNo.TabIndex = 43;
            // 
            // lblTttt
            // 
            appearance91.BackColor = System.Drawing.Color.Transparent;
            appearance91.TextVAlignAsString = "Middle";
            this.lblTttt.Appearance = appearance91;
            this.lblTttt.Location = new System.Drawing.Point(3, 3);
            this.lblTttt.Name = "lblTttt";
            this.lblTttt.Size = new System.Drawing.Size(30, 19);
            this.lblTttt.TabIndex = 26;
            this.lblTttt.Text = "Số";
            // 
            // btnSelectBill
            // 
            this.btnSelectBill.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnSelectBill.Enabled = false;
            this.btnSelectBill.Location = new System.Drawing.Point(165, 6);
            this.btnSelectBill.Name = "btnSelectBill";
            this.btnSelectBill.Size = new System.Drawing.Size(85, 23);
            this.btnSelectBill.TabIndex = 70;
            this.btnSelectBill.Text = "Chọn HĐ";
            this.btnSelectBill.Click += new System.EventHandler(this.btnSelectBill_Click);
            // 
            // cbBill
            // 
            this.cbBill.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbBill.Location = new System.Drawing.Point(3, 6);
            this.cbBill.Name = "cbBill";
            this.cbBill.Size = new System.Drawing.Size(156, 22);
            this.cbBill.TabIndex = 69;
            this.cbBill.Text = "Đã lập hóa đơn";
            this.cbBill.CheckStateChanged += new System.EventHandler(this.cbBill_CheckStateChanged);
            // 
            // lblTotalPaymentAmount
            // 
            this.lblTotalPaymentAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance92.BackColor = System.Drawing.Color.Transparent;
            appearance92.TextHAlignAsString = "Right";
            appearance92.TextVAlignAsString = "Middle";
            this.lblTotalPaymentAmount.Appearance = appearance92;
            this.lblTotalPaymentAmount.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalPaymentAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalPaymentAmount.Location = new System.Drawing.Point(757, 65);
            this.lblTotalPaymentAmount.Name = "lblTotalPaymentAmount";
            this.lblTotalPaymentAmount.Size = new System.Drawing.Size(169, 21);
            this.lblTotalPaymentAmount.TabIndex = 68;
            // 
            // lblTotalVATAmount
            // 
            this.lblTotalVATAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance93.BackColor = System.Drawing.Color.Transparent;
            appearance93.TextHAlignAsString = "Right";
            appearance93.TextVAlignAsString = "Middle";
            this.lblTotalVATAmount.Appearance = appearance93;
            this.lblTotalVATAmount.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalVATAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalVATAmount.Location = new System.Drawing.Point(757, 41);
            this.lblTotalVATAmount.Name = "lblTotalVATAmount";
            this.lblTotalVATAmount.Size = new System.Drawing.Size(169, 21);
            this.lblTotalVATAmount.TabIndex = 67;
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance94.BackColor = System.Drawing.Color.Transparent;
            appearance94.TextHAlignAsString = "Right";
            appearance94.TextVAlignAsString = "Middle";
            this.lblTotalAmount.Appearance = appearance94;
            this.lblTotalAmount.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmount.Location = new System.Drawing.Point(757, 15);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(169, 21);
            this.lblTotalAmount.TabIndex = 66;
            // 
            // lblTotalPaymentAmountOriginal
            // 
            this.lblTotalPaymentAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance95.BackColor = System.Drawing.Color.Transparent;
            appearance95.TextHAlignAsString = "Right";
            appearance95.TextVAlignAsString = "Middle";
            this.lblTotalPaymentAmountOriginal.Appearance = appearance95;
            this.lblTotalPaymentAmountOriginal.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalPaymentAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalPaymentAmountOriginal.Location = new System.Drawing.Point(572, 65);
            this.lblTotalPaymentAmountOriginal.Name = "lblTotalPaymentAmountOriginal";
            this.lblTotalPaymentAmountOriginal.Size = new System.Drawing.Size(169, 21);
            this.lblTotalPaymentAmountOriginal.TabIndex = 65;
            // 
            // lblTotalVATAmountOriginal
            // 
            this.lblTotalVATAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance96.BackColor = System.Drawing.Color.Transparent;
            appearance96.TextHAlignAsString = "Right";
            appearance96.TextVAlignAsString = "Middle";
            this.lblTotalVATAmountOriginal.Appearance = appearance96;
            this.lblTotalVATAmountOriginal.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalVATAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalVATAmountOriginal.Location = new System.Drawing.Point(572, 41);
            this.lblTotalVATAmountOriginal.Name = "lblTotalVATAmountOriginal";
            this.lblTotalVATAmountOriginal.Size = new System.Drawing.Size(169, 21);
            this.lblTotalVATAmountOriginal.TabIndex = 64;
            // 
            // lblTotalAmountOriginal
            // 
            this.lblTotalAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance97.BackColor = System.Drawing.Color.Transparent;
            appearance97.TextHAlignAsString = "Right";
            appearance97.TextVAlignAsString = "Middle";
            this.lblTotalAmountOriginal.Appearance = appearance97;
            this.lblTotalAmountOriginal.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmountOriginal.Location = new System.Drawing.Point(572, 15);
            this.lblTotalAmountOriginal.Name = "lblTotalAmountOriginal";
            this.lblTotalAmountOriginal.Size = new System.Drawing.Size(169, 21);
            this.lblTotalAmountOriginal.TabIndex = 63;
            // 
            // lbl2
            // 
            this.lbl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance98.BackColor = System.Drawing.Color.Transparent;
            appearance98.TextVAlignAsString = "Middle";
            this.lbl2.Appearance = appearance98;
            this.lbl2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl2.Location = new System.Drawing.Point(439, 43);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(115, 19);
            this.lbl2.TabIndex = 58;
            this.lbl2.Text = "Tổng thuế GTGT";
            // 
            // lbl3
            // 
            this.lbl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance99.BackColor = System.Drawing.Color.Transparent;
            appearance99.TextVAlignAsString = "Middle";
            this.lbl3.Appearance = appearance99;
            this.lbl3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl3.Location = new System.Drawing.Point(439, 66);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(115, 19);
            this.lbl3.TabIndex = 56;
            this.lbl3.Text = "Tổng tiền thanh toán";
            // 
            // lbl1
            // 
            this.lbl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance100.BackColor = System.Drawing.Color.Transparent;
            appearance100.TextVAlignAsString = "Middle";
            this.lbl1.Appearance = appearance100;
            this.lbl1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1.Location = new System.Drawing.Point(439, 17);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(115, 19);
            this.lbl1.TabIndex = 54;
            this.lbl1.Text = "Tổng tiền hàng";
            // 
            // ultraToolTipManager1
            // 
            this.ultraToolTipManager1.ContainingControl = this;
            // 
            // FPPDiscountReturnDetail
            // 
            this.ClientSize = new System.Drawing.Size(936, 714);
            this.Controls.Add(this.palFill);
            this.Controls.Add(this.palTop);
            this.Controls.Add(this.palBottom);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FPPDiscountReturnDetail";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FPPDiscountReturnDetail_FormClosing);
            this.ultraTabPageHoaDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxHoaDon)).EndInit();
            this.ultraGroupBoxHoaDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoBK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBangke)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodeHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMatHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSContactNameHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressHD)).EndInit();
            this.ultraTabPagePhieuXuatKho.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxPhieuXuatKho)).EndInit();
            this.ultraGroupBoxPhieuXuatKho.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodeXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSContactNameXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressXK)).EndInit();
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbLoaiHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbHinhThucHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMauHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThanhToan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoBK1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBangke1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodeHD1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMatHang1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameHD1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectHD1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonReturn1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSContactNameHD1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressHD1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).EndInit();
            this.palFill.ResumeLayout(false);
            this.palGrid.ClientArea.ResumeLayout(false);
            this.palGrid.ResumeLayout(false);
            this.palThongTinChung.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxStand)).EndInit();
            this.uGroupBoxStand.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonDiscount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSContactName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxEx)).EndInit();
            this.uGroupBoxEx.ResumeLayout(false);
            this.palTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxTop)).EndInit();
            this.ultraGroupBoxTop.ResumeLayout(false);
            this.ultraGroupBoxTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optPhieuXuatKho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optHoaDon)).EndInit();
            this.palTopSctNhtNctPXK.ResumeLayout(false);
            this.palTopSctNhtNctPXK.PerformLayout();
            this.palTopSctNhtNctHoaDon.ResumeLayout(false);
            this.palTopSctNhtNctHoaDon.PerformLayout();
            this.palBottom.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvSeries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBill)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid uGridControl;
        private System.Windows.Forms.Panel palFill;
        private System.Windows.Forms.Panel palTop;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxTop;
        private System.Windows.Forms.Panel palBottom;
        private Infragistics.Win.UltraWinToolTip.UltraToolTipManager ultraToolTipManager1;
        private System.ComponentModel.IContainer components;
        private Infragistics.Win.Misc.UltraLabel lbl2;
        private Infragistics.Win.Misc.UltraLabel lbl3;
        private Infragistics.Win.Misc.UltraLabel lbl1;
        private Infragistics.Win.Misc.UltraPanel palGrid;
        private Infragistics.Win.Misc.UltraPanel palTopSctNhtNctHoaDon;
        private Infragistics.Win.Misc.UltraPanel palTopSctNhtNctPXK;
        private Infragistics.Win.Misc.UltraLabel lblTotalAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel lblTotalPaymentAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel lblTotalVATAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel lblTotalPaymentAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalVATAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalAmount;
        private Infragistics.Win.Misc.UltraSplitter splitter;
        private Infragistics.Win.Misc.UltraButton btnSelectBill;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor cbBill;
        private UltraOptionSet_Ex optHoaDon;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOldInvSeries;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOldInvTemplate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel26;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOldInvDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel27;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOldInvNo;
        private Infragistics.Win.Misc.UltraLabel lblTttt;
        private UltraOptionSet_Ex optPhieuXuatKho;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
        private System.Windows.Forms.Panel palThongTinChung;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uGroupBoxEx;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageHoaDon;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxHoaDon;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDate;
        private Infragistics.Win.Misc.UltraButton btnVouchersDiscount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNoBK;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkBangke;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxCodeHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMatHang;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameHD;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReasonReturn;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSContactNameHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPagePhieuXuatKho;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxPhieuXuatKho;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxCodeXK;
        private Infragistics.Win.Misc.UltraLabel lblCompanyTaxCode;
        private Infragistics.Win.Misc.UltraLabel lblChungTuGoc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOriginalNo;
        private Infragistics.Win.Misc.UltraLabel lblNumberAttach;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameXK;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectXK;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSReason;
        private Infragistics.Win.Misc.UltraLabel lblSReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSContactNameXK;
        private Infragistics.Win.Misc.UltraLabel lblSContactName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressXK;
        private Infragistics.Win.Misc.UltraLabel lblDiaChi;
        private Infragistics.Win.Misc.UltraLabel lblKhachHang;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingBankName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel28;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectBankAccount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel33;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtThanhToan;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDate1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNoBK1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkBangke1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxCodeHD1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMatHang1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameHD1;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectHD1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReasonReturn1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSContactNameHD1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressHD1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxStand;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObject;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReasonDiscount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSContactName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraButton btnVouchersReturn;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMauSoHDI;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSoHDI;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtKyHieuHDI;
        private Infragistics.Win.Misc.UltraLabel ultraLabel29;
        private Infragistics.Win.Misc.UltraLabel ultraLabel30;
        private Infragistics.Win.Misc.UltraLabel ultraLabel34;
        private Infragistics.Win.Misc.UltraLabel ultraLabel35;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteNgayHDI;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel40;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbLoaiHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel41;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbHinhThucHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSoHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtKyHieuHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel42;
        private Infragistics.Win.Misc.UltraLabel ultraLabel43;
        private Infragistics.Win.Misc.UltraLabel ultraLabel44;
        private Infragistics.Win.Misc.UltraLabel ultraLabel45;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbMauHD;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteNgayHD;
    }
}