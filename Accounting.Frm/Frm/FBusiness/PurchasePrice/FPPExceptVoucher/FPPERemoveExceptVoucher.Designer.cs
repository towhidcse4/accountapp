﻿namespace Accounting
{
    partial class FPPERemoveExceptVoucher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.lblAccount = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObject = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblAccountingObject = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.CkNo = new Infragistics.Win.Misc.UltraPanel();
            this.uGridDsachDtru = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnKetThuc = new Infragistics.Win.Misc.UltraButton();
            this.bntThuHien = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccount)).BeginInit();
            this.CkNo.ClientArea.SuspendLayout();
            this.CkNo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDsachDtru)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.lblAccount);
            this.ultraGroupBox2.Controls.Add(this.cbbAccountingObject);
            this.ultraGroupBox2.Controls.Add(this.lblAccountingObject);
            this.ultraGroupBox2.Controls.Add(this.cbbAccount);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            appearance27.FontData.BoldAsString = "True";
            appearance27.FontData.SizeInPoints = 13F;
            this.ultraGroupBox2.HeaderAppearance = appearance27;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(1095, 46);
            this.ultraGroupBox2.TabIndex = 43;
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // lblAccount
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextHAlignAsString = "Left";
            appearance1.TextVAlignAsString = "Middle";
            this.lblAccount.Appearance = appearance1;
            this.lblAccount.Location = new System.Drawing.Point(387, 17);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(84, 18);
            this.lblAccount.TabIndex = 3;
            this.lblAccount.Text = "TK phải trả :";
            // 
            // cbbAccountingObject
            // 
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountingObject.DisplayLayout.Appearance = appearance2;
            this.cbbAccountingObject.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountingObject.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.cbbAccountingObject.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountingObject.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountingObject.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountingObject.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.cbbAccountingObject.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountingObject.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountingObject.DisplayLayout.Override.CellAppearance = appearance9;
            this.cbbAccountingObject.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountingObject.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.cbbAccountingObject.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.cbbAccountingObject.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountingObject.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountingObject.DisplayLayout.Override.RowAppearance = appearance12;
            this.cbbAccountingObject.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountingObject.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.cbbAccountingObject.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObject.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountingObject.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountingObject.Enabled = false;
            this.cbbAccountingObject.Location = new System.Drawing.Point(114, 13);
            this.cbbAccountingObject.Name = "cbbAccountingObject";
            this.cbbAccountingObject.Size = new System.Drawing.Size(263, 22);
            this.cbbAccountingObject.TabIndex = 0;
            // 
            // lblAccountingObject
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextHAlignAsString = "Left";
            appearance14.TextVAlignAsString = "Middle";
            this.lblAccountingObject.Appearance = appearance14;
            this.lblAccountingObject.Location = new System.Drawing.Point(24, 13);
            this.lblAccountingObject.Name = "lblAccountingObject";
            this.lblAccountingObject.Size = new System.Drawing.Size(84, 22);
            this.lblAccountingObject.TabIndex = 1;
            this.lblAccountingObject.Text = "Nhà cung cấp :";
            // 
            // cbbAccount
            // 
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccount.DisplayLayout.Appearance = appearance15;
            this.cbbAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccount.DisplayLayout.GroupByBox.Appearance = appearance16;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance17;
            this.cbbAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.cbbAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccount.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccount.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.cbbAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccount.DisplayLayout.Override.CardAreaAppearance = appearance21;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            appearance22.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccount.DisplayLayout.Override.CellAppearance = appearance22;
            this.cbbAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccount.DisplayLayout.Override.CellPadding = 0;
            appearance23.BackColor = System.Drawing.SystemColors.Control;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccount.DisplayLayout.Override.GroupByRowAppearance = appearance23;
            appearance24.TextHAlignAsString = "Left";
            this.cbbAccount.DisplayLayout.Override.HeaderAppearance = appearance24;
            this.cbbAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccount.DisplayLayout.Override.RowAppearance = appearance25;
            this.cbbAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance26;
            this.cbbAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccount.Enabled = false;
            this.cbbAccount.Location = new System.Drawing.Point(477, 13);
            this.cbbAccount.Name = "cbbAccount";
            this.cbbAccount.Size = new System.Drawing.Size(122, 22);
            this.cbbAccount.TabIndex = 2;
            // 
            // CkNo
            // 
            // 
            // CkNo.ClientArea
            // 
            this.CkNo.ClientArea.Controls.Add(this.uGridDsachDtru);
            this.CkNo.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.CkNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CkNo.Location = new System.Drawing.Point(0, 46);
            this.CkNo.Name = "CkNo";
            this.CkNo.Size = new System.Drawing.Size(1095, 432);
            this.CkNo.TabIndex = 44;
            // 
            // uGridDsachDtru
            // 
            this.uGridDsachDtru.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridDsachDtru.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridDsachDtru.Location = new System.Drawing.Point(0, 23);
            this.uGridDsachDtru.Name = "uGridDsachDtru";
            this.uGridDsachDtru.Size = new System.Drawing.Size(1095, 409);
            this.uGridDsachDtru.TabIndex = 41;
            this.uGridDsachDtru.Text = "ultraGrid1";
            this.uGridDsachDtru.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uGridDsachDtru.AfterHeaderCheckStateChanged += new Infragistics.Win.UltraWinGrid.AfterHeaderCheckStateChangedEventHandler(this.uGridDsachDtru_AfterHeaderCheckStateChanged);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            appearance28.FontData.BoldAsString = "True";
            appearance28.FontData.SizeInPoints = 10F;
            this.ultraGroupBox1.HeaderAppearance = appearance28;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1095, 23);
            this.ultraGroupBox1.TabIndex = 43;
            this.ultraGroupBox1.Text = "Danh sách đối trừ";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.btnKetThuc);
            this.ultraGroupBox4.Controls.Add(this.bntThuHien);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            appearance31.FontData.BoldAsString = "True";
            appearance31.FontData.SizeInPoints = 10F;
            this.ultraGroupBox4.HeaderAppearance = appearance31;
            this.ultraGroupBox4.Location = new System.Drawing.Point(0, 439);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(1095, 39);
            this.ultraGroupBox4.TabIndex = 45;
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnKetThuc
            // 
            this.btnKetThuc.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance29.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnKetThuc.Appearance = appearance29;
            this.btnKetThuc.Location = new System.Drawing.Point(988, 6);
            this.btnKetThuc.Name = "btnKetThuc";
            this.btnKetThuc.Size = new System.Drawing.Size(101, 27);
            this.btnKetThuc.TabIndex = 47;
            this.btnKetThuc.Text = "Kết thúc";
            this.btnKetThuc.Click += new System.EventHandler(this.btnKetThuc_Click);
            // 
            // bntThuHien
            // 
            this.bntThuHien.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance30.Image = global::Accounting.Properties.Resources.apply_32;
            this.bntThuHien.Appearance = appearance30;
            this.bntThuHien.Location = new System.Drawing.Point(870, 6);
            this.bntThuHien.Name = "bntThuHien";
            this.bntThuHien.Size = new System.Drawing.Size(101, 27);
            this.bntThuHien.TabIndex = 46;
            this.bntThuHien.Text = "Thực hiện";
            this.bntThuHien.Click += new System.EventHandler(this.bntThuHien_Click);
            // 
            // FPPERemoveExceptVoucher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1095, 478);
            this.Controls.Add(this.ultraGroupBox4);
            this.Controls.Add(this.CkNo);
            this.Controls.Add(this.ultraGroupBox2);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FPPERemoveExceptVoucher";
            this.Text = "Bỏ đối trừ";
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccount)).EndInit();
            this.CkNo.ClientArea.ResumeLayout(false);
            this.CkNo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDsachDtru)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraLabel lblAccount;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObject;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObject;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccount;
        private Infragistics.Win.Misc.UltraPanel CkNo;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDsachDtru;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.Misc.UltraButton bntThuHien;
        private Infragistics.Win.Misc.UltraButton btnKetThuc;
    }
}