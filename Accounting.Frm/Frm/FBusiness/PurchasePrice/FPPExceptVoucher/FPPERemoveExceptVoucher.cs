﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Confront;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FPPERemoveExceptVoucher : CustormForm
    {
        #region Khai báo
        private readonly IExceptVoucherService IexceptVoucherService;
        private readonly IAccountingObjectService IaccountingObjectService;
        //add by cuongpv
        private readonly IGOtherVoucherService IGOtherVoucherService;
        private readonly IGOtherVoucherDetailService IGOtherVoucherDetailService;
        private readonly IGOtherVoucherDetailExceptService IGOtherVoucherDetailExceptService;
        private readonly IGeneralLedgerService IGeneralLedgerService;
        //end add by cuongpv
        public static bool IsClose = true;
        public static bool IsClose1 = false;
        List<RemoveExpectVoucher> removeLstExceptVoucher = new List<RemoveExpectVoucher>();

        Guid accountingObjectId1;
        string debitAccount1;
        #endregion

        #region Khởi tạo
        public FPPERemoveExceptVoucher(Guid accountingObjectId, string debitAccount)
        {
            IexceptVoucherService = IoC.Resolve<IExceptVoucherService>();
            IaccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            //add by cuongpv
            IGOtherVoucherService = IoC.Resolve<IGOtherVoucherService>();
            IGOtherVoucherDetailService = IoC.Resolve<IGOtherVoucherDetailService>();
            IGOtherVoucherDetailExceptService = IoC.Resolve<IGOtherVoucherDetailExceptService>();
            IGeneralLedgerService = IoC.Resolve<IGeneralLedgerService>();
            //end add by cuonpgv
            InitializeComponent();
            accountingObjectId1 = accountingObjectId;
            debitAccount1 = debitAccount;
            LoadDuLieu(accountingObjectId, debitAccount);
        }
        #endregion

        #region Nghiệp vụ

        void LoadDuLieu(Guid accountingObjectId, string debitAccount)
        {
            AccountingObject model = IaccountingObjectService.Getbykey(accountingObjectId);
            cbbAccountingObject.Text = model.AccountingObjectCode;
            cbbAccount.Text = debitAccount;
            removeLstExceptVoucher = IexceptVoucherService.RemovExceptVouchers(accountingObjectId, debitAccount).ToList();
            ViewDsach(uGridDsachDtru, IexceptVoucherService.RemovExceptVouchers(accountingObjectId, debitAccount));
        }

        #endregion

        #region Xử lý Gridview
        private List<TemplateColumn> MauDanhSach()
        {
            return new List<TemplateColumn>
                       {
                           new TemplateColumn
                               {
                                   ColumnName = "Status",
                                   ColumnCaption = "",
                                   ColumnWidth = 30,
                                   ColumnMaxWidth = 60,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 1
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "Date",
                                   ColumnCaption = "Ngày CT trả tiền",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 2,
                                   IsReadOnly = true,
                                   
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "ExceptNo",
                                   ColumnCaption = "Số CT trả tiền",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 3,
                                   IsReadOnly = true,
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "No",
                                   ColumnCaption = "Số CT còn nợ",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 4,
                                   IsReadOnly = true,
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "TenNhanVien",
                                   ColumnCaption = "Nhân viên trả tiền",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 5,
                                   IsReadOnly = true,
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "AmountOriginal",
                                   ColumnCaption = "Số tiền",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 6,
                                   IsReadOnly = true,
                               },
                               new TemplateColumn
                               {
                                   ColumnName = "Amount",
                                   ColumnCaption = "Số tiền quy đổi",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 7,
                                   IsReadOnly = true,
                               }
                       };
        }
        private void ViewDsach(UltraGrid uGrid, List<RemoveExpectVoucher> model)
        {
            uGrid.DataSource = model;
            Utils.ConfigGrid(uGrid, "", MauDanhSach());
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            // xét style cho các cột có dạng combobox
            UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["AmountOriginal"], ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["Amount"], ConstDatabase.Format_TienVND);
            ugc.SetHeaderCheckedState(uGrid.Rows, false);
        }
        private void uGridDsachDtru_AfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {
            if (removeLstExceptVoucher.Count > 0)
            {
                uGridDsachDtru.SetDataBinding(removeLstExceptVoucher, "");
            }
        }
        #endregion

        #region Events
        private void bntThuHien_Click(object sender, EventArgs e)
        {
            try
            {
                uGridDsachDtru.UpdateData();
                if (removeLstExceptVoucher.All(c => c.Status == false))
                {
                    MSG.Warning(resSystem.PPExceptVoucher10);
                    IsClose1 = false;
                }
                else
                {
                    IexceptVoucherService.BeginTran();
                    foreach (var item in removeLstExceptVoucher.Where(c => c.Status))
                    {
                        #region add by cuongpv: them code bo doi tru thi xoa du lieu tai cac bang lien quan
                        var lstGotherVoucherDetailExcept = IGOtherVoucherDetailExceptService.GetAll().Where(x => x.PayRefID == item.GLVoucherExceptID && x.DebtRefID == item.GLVoucherID).ToList();
                        if (lstGotherVoucherDetailExcept.Count > 0)
                        {
                            foreach (var itemGOtherDetailExcept in lstGotherVoucherDetailExcept)
                            {
                                var lstGotherVoucher = IGOtherVoucherService.GetAll().Where(c => c.ID == itemGOtherDetailExcept.GOtherVoucherID).ToList();
                                var lstGotherVoucherDetail = IGOtherVoucherDetailService.GetAll().Where(x => x.GOtherVoucherID == itemGOtherDetailExcept.GOtherVoucherID).ToList();
                                var lstGL = IGeneralLedgerService.GetAll().Where(x => x.ReferenceID == itemGOtherDetailExcept.GOtherVoucherID).ToList();

                                if (lstGL.Count > 0)
                                {
                                    foreach (var itemlstGL in lstGL)
                                    {
                                        IGeneralLedgerService.Delete(itemlstGL);
                                    }
                                }

                                if (lstGotherVoucherDetail.Count > 0)
                                {
                                    foreach (var itemlstGotherVoucherDetail in lstGotherVoucherDetail)
                                    {
                                        IGOtherVoucherDetailService.Delete(itemlstGotherVoucherDetail);
                                    }
                                }

                                if (lstGotherVoucher.Count > 0)
                                {
                                    foreach (var itemlstGotherVoucher in lstGotherVoucher)
                                    {
                                        IGOtherVoucherService.Delete(itemlstGotherVoucher.ID);
                                    }
                                }

                                IGOtherVoucherDetailExceptService.Delete(itemGOtherDetailExcept);
                            }
                        }
                        #endregion

                        IexceptVoucherService.Delete(item.ID);
                        IexceptVoucherService.CommitChanges();
                    }
                    IexceptVoucherService.CommitTran();
                    LoadDuLieu(accountingObjectId1, debitAccount1);
                    MSG.MessageBoxStand(resSystem.PPExceptVoucher8);
                    IsClose1 = true;
                }
            }
            catch (Exception ex)
            {
                IexceptVoucherService.RolbackTran();
                MSG.MessageBoxStand(resSystem.PPExceptVoucher9);

            }
        }
        private void btnKetThuc_Click(object sender, EventArgs e)
        {
            this.Close();
            IsClose = true;
        }
        #endregion
    }
}
