﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Accounting.Core.IService;
using FX.Core;

namespace Accounting
{
    public partial class FCostAlLocation : CustormForm
    {
        private IMaterialGoodsService _IMaterialGoodsService { get { return IoC.Resolve<IMaterialGoodsService>(); } }
        public PPInvoice PPInvoice { get; private set; }
        private List<PPServices> _ppServices = new List<PPServices>();
        private List<PPInvoiceDetailCost> _ppInvoiceDetailCosts = new List<PPInvoiceDetailCost>();
        int statusForm;
        int Lamtron = 0;
        public FCostAlLocation(PPInvoice input, int _statusForm)
        {
            InitializeComponent();
            statusForm = _statusForm;
            PPInvoice = input;
            Lamtron = int.Parse(Utils.ListSystemOption.FirstOrDefault(x => x.Code == "DDSo_TienVND").Data);
            var lstPpInvoiceDetails = input.PPInvoiceDetails.Where(x => x.MaterialGoodsID != null).ToList();
            decimal sumFreightAmount = lstPpInvoiceDetails.Sum(x => x.FreightAmount);
            //_ppServices = input.PPServices.ToList();
            _ppInvoiceDetailCosts = input.PPInvoiceDetailCosts.ToList();
            if (_ppInvoiceDetailCosts.Count > 0)
            {
                txtTotalFreightAmount.Value = _ppInvoiceDetailCosts.Where(x => x.CostType == false).Sum(x => x.AmountPB);
                btnAllocation.PerformClick();
            }
            List<CostAllocation> listInput = lstPpInvoiceDetails.Select(ppInvoiceDetail => new CostAllocation { MaterialGoodsID = (Guid)ppInvoiceDetail.MaterialGoodsID, Description = ppInvoiceDetail.Description, Quantity = ppInvoiceDetail.Quantity ?? 0, Amount = ppInvoiceDetail.AmountOriginal, FreightAmount = ppInvoiceDetail.FreightAmount, FreightRate = (decimal)sumFreightAmount == 0 ? (decimal)0 : ((ppInvoiceDetail.FreightAmountOriginal / (decimal)sumFreightAmount) * 100), PPInvoiceDetail = ppInvoiceDetail }).ToList();
            uGrid.DataSource = listInput;
            uGrid.ConfigGrid(ConstDatabase.CostAlLocation_KeyName, null, true, false);
            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid);
            }
            uGrid.ConfigSummaryOfGrid();
            uGrid.DisplayLayout.Override.HeaderAppearance.TextHAlign = HAlign.Center;
            txtTotalFreightAmount.FormatNumberic(input.CurrencyID.Equals("VND") ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            btnAllocation.Enabled = uGrid.Rows.Count > 0;
            btnApply.Enabled = uGrid.Rows.Count > 0;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAllocation_Click(object sender, EventArgs e)
        {
            var p = optSort.CheckedIndex == 0 ? ((decimal?)uGrid.Rows.SummaryValues["SumQuantity"].Value ?? 0) : ((decimal?)uGrid.Rows.SummaryValues["SumAmount"].Value ?? 0);
            foreach (var row in uGrid.Rows)
            {
                //var a = txtTotalFreightAmount.Text == "" ? (decimal)0 : Convert.ToDecimal(txtTotalFreightAmount.Value ?? 0);
                var a = _ppServices.Sum(x => x.FreightAmount);
                var b = optSort.CheckedIndex == 0 ? ((decimal)(row.Cells["Quantity"].Value ?? 0)) : ((decimal)(row.Cells["Amount"].Value ?? 0));
                var c = (a == 0 || b == 0) ? 0 : (b / p * 100);
                var d = (a == 0 || b == 0) ? 0 : (a * c / 100);
                row.Cells["FreightRate"].Value = c;
                row.Cells["FreightAmount"].Value = Math.Round((d ?? 0), Lamtron, MidpointRounding.AwayFromZero);
            }
        }

        private void btnSelectPPService_Click(object sender, EventArgs e)
        {
            try
            {
                var f = new FSelectPPServices(PPInvoice, statusForm, false);
                f.FormClosed += new FormClosedEventHandler(fSelectPPServices_FormClosed);
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                return;
            }
        }

        private void fSelectPPServices_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason != CloseReason.UserClosing) return;
            var f = (FSelectPPServices)sender;
            if (f.DialogResult == DialogResult.OK)
            {
                _ppServices = f.SelectPPServices;
                txtTotalFreightAmount.Value = _ppServices.Sum(x => x.FreightAmount);
                txtTotalFreightAmount.ReadOnly = _ppServices.Count > 1;
                for (int i = 0; i < _ppInvoiceDetailCosts.Count; i++)
                {
                    if (_ppServices.Any(x => x.ID != _ppInvoiceDetailCosts[i].PPServiceID && _ppInvoiceDetailCosts[i].CostType == false)) _ppInvoiceDetailCosts.RemoveAt(i);
                }
                foreach (var pps in _ppServices)
                {
                    if (_ppInvoiceDetailCosts.Any(x => x.PPServiceID == pps.ID && x.CostType == false))
                    {
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].TotalFreightAmount = pps.TotalAmount;
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].TotalFreightAmountOriginal = pps.TotalAmountOriginal;
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AmountPB = (pps.FreightAmount);
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AmountPBOriginal = pps.FreightAmount / PPInvoice.ExchangeRate;
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AccumulatedAllocateAmount = (_ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AmountPB + (pps.AccumulatedAllocateAmount));
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AccumulatedAllocateAmountOriginal = (_ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AmountPBOriginal + (pps.AccumulatedAllocateAmount));
                    }
                    else
                    {
                        PPInvoiceDetailCost ppi = new PPInvoiceDetailCost();
                        ppi.AccountingObjectID = pps.AccountingObjectID;
                        ppi.AccountingObjectName = pps.AccountingObjectName;
                        ppi.Date = pps.Date;
                        ppi.PostedDate = pps.PostedDate;
                        ppi.No = pps.No;
                        ppi.CostType = false;
                        ppi.TypeID = 210;
                        ppi.PPServiceID = pps.ID;
                        ppi.TotalFreightAmount = pps.TotalAmount;
                        ppi.TotalFreightAmountOriginal = pps.TotalAmountOriginal;
                        ppi.AmountPB = pps.FreightAmount;
                        ppi.AmountPBOriginal = pps.FreightAmount / PPInvoice.ExchangeRate;
                        ppi.AccumulatedAllocateAmount = (ppi.AmountPB + (pps.AccumulatedAllocateAmount));
                        ppi.AccumulatedAllocateAmountOriginal = (ppi.AmountPBOriginal + (pps.AccumulatedAllocateAmount));
                        ppi.ID = Guid.NewGuid();
                        _ppInvoiceDetailCosts.Add(ppi);
                    }
                }
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                PPInvoice.PPInvoiceDetailCosts = _ppInvoiceDetailCosts;
                SaveAndClose();
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                return;
            }
        }

        void SaveAndClose()
        {
            var listInput = (List<CostAllocation>)uGrid.DataSource;
            foreach (var ppInvoiceDetail in PPInvoice.PPInvoiceDetails)
            {
                var costAllocation = listInput.FirstOrDefault(x => x.PPInvoiceDetail.ID == ppInvoiceDetail.ID);
                if (costAllocation != null)
                {
                    ppInvoiceDetail.FreightAmountOriginal = costAllocation.FreightAmount / PPInvoice.ExchangeRate ?? 1;
                    ppInvoiceDetail.FreightAmount = costAllocation.FreightAmount;
                }
            }
            DialogResult = DialogResult.OK;
            Close();
        }

        private void uGrid_AfterCellUpdate(object sender, CellEventArgs e)
        {
        }
    }
}