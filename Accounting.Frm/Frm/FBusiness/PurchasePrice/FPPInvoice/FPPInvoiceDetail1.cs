﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Frm.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;

namespace Accounting.Frm
{
    public partial class FPPInvoiceDetail1 : FPPInvoiceDetailTmp1
    {
        protected Dictionary<string, List<Account>> _getAccountDefaultByTypeId = new Dictionary<string, List<Account>>(); //Chứa các tài khoản mặc định
        private MBCreditCardDetail _MBCreditCardDetail;
        private MCPaymentDetail _MCPaymentDetail;
        private MBTellerPaperDetail _MBTellerPaperDetail;

        #region khởi tạo
        public FPPInvoiceDetail1(PPInvoice temp, List<PPInvoice> listObject, int statusForm)
        {
            InitializeComponent();
            base.InitializeComponent();

            #region Thiết lập ban đầu cho Form
            _typeID = 210; _statusForm = statusForm;
            if (statusForm == ConstFrm.optStatusForm.Add) _select.TypeID = _typeID; else _select = temp;
            _listSelects.AddRange(listObject);
            InitializeGUI(_select); //khởi tạo và hiển thị giá trị ban đầu cho các control
            ReloadToolbar(_select, listObject, statusForm);  //Change Menu Top
            #endregion
        }
        #endregion

        #region override
        public override void InitializeGUI(PPInvoice input)
        {

            ultraTabControl2.Tabs[1].Visible = false;
            ultraTabControl2.Tabs[2].Visible = false;
            ultraTabControl2.Tabs[3].Visible = false;
            ultraTabControl2.Tabs[4].Visible = false;
            ultraTabControl2.Tabs[5].Visible = false;

            //if (_select.MBTellerPaperDetail == null)
            //{
            //    _select.MBTellerPaperDetail = new MBTellerPaperDetail();
            //    _select.MBTellerPaperDetail.ID = Guid.NewGuid();
            //}
            //if (_select.MBCreditCardDetail == null)
            //{
            //    _select.MBCreditCardDetail = new MBCreditCardDetail();
            //    _select.MBCreditCardDetail.ID = Guid.NewGuid();
            //}
            //if (_select.MCPaymentDetail == null)
            //{
            //    _select.MCPaymentDetail = new MCPaymentDetail();
            //    _select.MCPaymentDetail.ID = Guid.NewGuid();
            //}

            //if (_select.MBTellerPaperDetail.MBTellerPaper == null)
            //{
            //    _select.MBTellerPaperDetail.MBTellerPaper = new MBTellerPaper();
            //    _select.MBTellerPaperDetail.MBTellerPaper.ID = Guid.NewGuid();
            //}
            //if (_select.MBCreditCardDetail.MBCreditCard == null)
            //{
            //    _select.MBCreditCardDetail.MBCreditCard = new MBCreditCard();
            //    _select.MBCreditCardDetail.MBCreditCard.ID = Guid.NewGuid();
            //}
            //if (_select.MCPaymentDetail.MCPayment == null)
            //{
            //    _select.MCPaymentDetail.MCPayment = new MCPayment();
            //    _select.MCPaymentDetail.MCPayment.ID = Guid.NewGuid();
            //}

            #region Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            Template mauGiaoDien = new Template();
            //Template mauGiaoDien = Utils.GetTemplateUIfromDatabase(input.TypeID, _statusForm == ConstFrm.optStatusForm.Add, input.TemplateID);
            //_select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;
            #endregion

            //lấy danh sách những tài khoản mặc định của chứng từ
            _getAccountDefaultByTypeId = Utils.GetAccountDefaultByTypeID(input.TypeID);
            #endregion

            #region Thiết lập dữ liệu và Fill dữ liệu Obj vào control (nếu đang sửa)
            List<BankAccountDetail> lstBankAccountDetails = IoC.Resolve<IBankAccountDetailService>().GetAll();
            cbbBankAccountObject2.DataSource = cbbBankAccountObject3.DataSource = cbbBankAccountObject4.DataSource = lstBankAccountDetails;
            cbbBankAccountObject2.DisplayMember = cbbBankAccountObject3.DisplayMember = cbbBankAccountObject4.DisplayMember = "BankAccount";
            cbbBankAccountObject2.ValueMember = cbbBankAccountObject3.ValueMember = cbbBankAccountObject4.ValueMember = "BankAccount";
            Utils.ConfigGrid(cbbBankAccountObject2, ConstDatabase.BankAccountDetail_TableName);
            Utils.ConfigGrid(cbbBankAccountObject3, ConstDatabase.BankAccountDetail_TableName);
            Utils.ConfigGrid(cbbBankAccountObject4, ConstDatabase.BankAccountDetail_TableName);

            List<AccountingObjectBankAccount> lKhachHang = IAccountingObjectBankAccountService.GetAll();
            cbbBankAccountingObject2.DataSource = cbbBankAccountingObject3.DataSource = cbbBankAccountingObject5.DataSource = lKhachHang;
            cbbBankAccountingObject2.DisplayMember = cbbBankAccountingObject3.DisplayMember = cbbBankAccountingObject5.DisplayMember = "BankAccount";
            cbbBankAccountingObject2.ValueMember = cbbBankAccountingObject3.ValueMember = cbbBankAccountingObject5.ValueMember = "BankAccount";
            Utils.ConfigGrid(cbbBankAccountingObject2, ConstDatabase.AccountingObjectBankAccount_TableName);
            Utils.ConfigGrid(cbbBankAccountingObject3, ConstDatabase.AccountingObjectBankAccount_TableName);
            Utils.ConfigGrid(cbbBankAccountingObject5, ConstDatabase.AccountingObjectBankAccount_TableName);

            cbbCreditCardNumber5.DataSource = ICreditCardService.GetAll();
            cbbCreditCardNumber5.DisplayMember = "CreditCardNumber";
            cbbCreditCardNumber5.ValueMember = "CreditCardNumber";
            Utils.ConfigGrid(cbbCreditCardNumber5, ConstDatabase.CreditCard_TableName);

            #region khởi tạo đối tượng cho optAccountingObjectType
            //lấy khách hàng
            List<AccountingObject> lKhachHang2 = IAccountingObjectService.GetAll();
            cbbAccountingObjectID5.DataSource = cbbAccountingObjectID4.DataSource = cbbAccountingObjectID3.DataSource = cbbAccountingObjectID2.DataSource = cbbAccountingObjectID1.DataSource = cbbAccountingObjectID.DataSource = lKhachHang2;
            cbbAccountingObjectID5.DisplayMember = cbbAccountingObjectID4.DisplayMember = cbbAccountingObjectID3.DisplayMember = cbbAccountingObjectID2.DisplayMember = cbbAccountingObjectID1.DisplayMember = cbbAccountingObjectID.DisplayMember = "AccountingObjectCode";
            cbbAccountingObjectID5.ValueMember = cbbAccountingObjectID4.ValueMember = cbbAccountingObjectID3.ValueMember = cbbAccountingObjectID2.ValueMember = cbbAccountingObjectID1.ValueMember = cbbAccountingObjectID.ValueMember = "ID";
            Utils.ConfigGrid(cbbAccountingObjectID5, ConstDatabase.AccountingObject_TableName);
            Utils.ConfigGrid(cbbAccountingObjectID4, ConstDatabase.AccountingObject_TableName);
            Utils.ConfigGrid(cbbAccountingObjectID3, ConstDatabase.AccountingObject_TableName);
            Utils.ConfigGrid(cbbAccountingObjectID2, ConstDatabase.AccountingObject_TableName);
            Utils.ConfigGrid(cbbAccountingObjectID1, ConstDatabase.AccountingObject_TableName);
            Utils.ConfigGrid(cbbAccountingObjectID, ConstDatabase.AccountingObject_TableName);
            #endregion

            BindingList<PPInvoiceDetail> commonBinding = new BindingList<PPInvoiceDetail>(input.PPInvoiceDetails);
            uGridHangTien.DataSource = commonBinding;
            uGridTax.DataSource = commonBinding;
            uGridThongKe.DataSource = commonBinding;
            uGridChungTuChiPhi.DataSource = commonBinding;
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                //lấy dữ liệu chi tiết
                _select.TotalAmountOriginal = 0;
                _select.TotalAmount = 0;
                _select.CurrencyID = "VND";

                _select.No = Utils.TaoMaChungTu(IGenCodeService.getGenCode(ConstFrm.TypeGroup_PPInvoice));
                //Thực hiện set ngày Hàng Tiền và ngày đơn hàng (lấy từ cài đặt chung) <hiện đang tạm thời set là ngày hiện tại để test>
                _select.Date = DateTime.Now;
                _select.PostedDate = DateTime.Now;
                //_select.MCPaymentDetail.MCPayment.No = Utils.TaoMaChungTu(IGenCodeService.getGenCode(ConstFrm.TypeGroupMCPayment));
                //_select.MBCreditCardDetail.MBCreditCard.No = Utils.TaoMaChungTu(IGenCodeService.getGenCode(ConstFrm.TypeGroup_MBCreditCard));
                //_select.MBTellerPaperDetail.MBTellerPaper.No = Utils.TaoMaChungTu(IGenCodeService.getGenCode(ConstFrm.TypeGruop_MBTellerPaper));

                //_select.MCPaymentDetail.MCPayment.Date = DateTime.Now;
                //_select.MBCreditCardDetail.MBCreditCard.Date = DateTime.Now;
                //_select.MBTellerPaperDetail.MBTellerPaper.Date = DateTime.Now;

                //_select.MCPaymentDetail.MCPayment.PostedDate = DateTime.Now;
                //_select.MBCreditCardDetail.MBCreditCard.PostedDate = DateTime.Now;
                //_select.MBTellerPaperDetail.MBTellerPaper.PostedDate = DateTime.Now;
            }
            List<PPInvoice> ltemp = new List<PPInvoice> { _select };
            uGridControl.DataSource = ltemp;

            this.ConfigTopVouchersNo<PPDiscountReturn>(palTopSctNhtNctPXK, "No", "PostedDate", "Date");
            this.ConfigTopVouchersNo<PPDiscountReturn>(palTopSctNhtNctPXK, "No", "PostedDate", "Date");
            this.ConfigTopVouchersNo<PPDiscountReturn>(palTopSctNhtNctPXK, "No", "PostedDate", "Date");
            this.ConfigTopVouchersNo<PPDiscountReturn>(palTopSctNhtNctPXK, "No", "PostedDate", "Date");
            this.ConfigTopVouchersNo<PPDiscountReturn>(palTopSctNhtNctPXK, "No", "PostedDate", "Date");
            ///
            ///Bind giao dien vao data
            ///
            //tab 1 
            cbbAccountingObjectID.DataBindings.Add("Value", _select, "AccountingObjectID", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtNo.DataBindings.Add("Text", _select, "No");
            txtAccountingObjectAddress.DataBindings.Add("Text", _select, "AccountingObjectAddress");
            txtAccoutingObjectName.DataBindings.Add("Text", _select, "AccountingObjectName");
            txtReason.DataBindings.Add("Text", _select, "Reason");
            txtContactName.DataBindings.Add("Text", _select, "ContactName");
            txtNumberAttach.DataBindings.Add("Text", _select, "NumberAttach");
            //dteDate.DataBindings.Add("Text", _select, "Date");
            //postedDate.DataBindings.Add("Text", _select, "PostedDate");
            //tab 2
            cbbAccountingObjectID1.DataBindings.Add("Value", _select, "AccountingObjectID", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtNo1.DataBindings.Add("Text", _select.MCPaymentDetail.MCPayment, "No");
            txtAccountingObjectAddress1.DataBindings.Add("Text", _select, "AccountingObjectAddress");
            txtAccoutingObjectName1.DataBindings.Add("Text", _select, "AccountingObjectName");
            //dteDate1.DataBindings.Add("Text", _select.MCPaymentDetail.MCPayment, "Date");
            //postedDate1.DataBindings.Add("Text", _select.MCPaymentDetail.MCPayment, "PostedDate");
            //txtReason1.DataBindings.Add("Text", _select.MCPaymentDetail.MCPayment, "Reason");
            //txtReceiver.DataBindings.Add("Text", _select.MCPaymentDetail.MCPayment, "Receiver");
            //tab 3
            cbbAccountingObjectID2.DataBindings.Add("Value", _select, "AccountingObjectID", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtNo2.DataBindings.Add("Text", _select.MBTellerPaperDetail.MBTellerPaper, "No");
            txtAccountingObjectAddress2.DataBindings.Add("Text", _select, "AccountingObjectAddress");
            txtAccoutingObjectName2.DataBindings.Add("Text", _select, "AccountingObjectName");
            //dteDate2.DataBindings.Add("Text", _select.MBTellerPaperDetail.MBTellerPaper, "Date");
            //postedDate2.DataBindings.Add("Text", _select.MBTellerPaperDetail.MBTellerPaper, "PostedDate");
            //cbbBankAccountObject2.DataBindings.Add("Value", _select.MBTellerPaperDetail.MBTellerPaper, "BankAccount", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtBankAccountObjectName2.DataBindings.Add("Text", _select.MBTellerPaperDetail.MBTellerPaper, "BankName");
            //cbbBankAccountingObject2.DataBindings.Add("Value", _select.MBTellerPaperDetail.MBTellerPaper, "AccountingObjectBankAccount", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtReason2.DataBindings.Add("Text", _select.MBTellerPaperDetail.MBTellerPaper, "Reason");
            //txtBankAccountingObjectName2.DataBindings.Add("Text", _select.MBTellerPaperDetail.MBTellerPaper, "AccountingObjectBankName");
            //tab 4
            cbbAccountingObjectID3.DataBindings.Add("Value", _select, "AccountingObjectID", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtNo3.DataBindings.Add("Text", _select.MBTellerPaperDetail.MBTellerPaper, "No");
            txtAccountingObjectAddress3.DataBindings.Add("Text", _select, "AccountingObjectAddress");
            txtAccoutingObjectName3.DataBindings.Add("Text", _select, "AccountingObjectName");
            //dteDate3.DataBindings.Add("Text", _select.MBTellerPaperDetail.MBTellerPaper, "Date");
            //postedDate3.DataBindings.Add("Text", _select.MBTellerPaperDetail.MBTellerPaper, "PostedDate");
            //cbbBankAccountObject3.DataBindings.Add("Value", _select.MBTellerPaperDetail.MBTellerPaper, "BankAccount", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtBankAccountObjectName3.DataBindings.Add("Text", _select.MBTellerPaperDetail.MBTellerPaper, "BankName");
            //cbbBankAccountingObject3.DataBindings.Add("Value", _select.MBTellerPaperDetail.MBTellerPaper, "AccountingObjectBankAccount", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtReason3.DataBindings.Add("Text", _select.MBTellerPaperDetail.MBTellerPaper, "Reason");
            //txtBankAccountingObjectName3.DataBindings.Add("Text", _select.MBTellerPaperDetail.MBTellerPaper, "AccountingObjectBankName");
            //tab 5            
            cbbAccountingObjectID4.DataBindings.Add("Value", _select, "AccountingObjectID", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtIdentificationNo4.DataBindings.Add("Value", _select.MBTellerPaperDetail.MBTellerPaper, "BankAccount", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtNo4.DataBindings.Add("Text", _select.MBTellerPaperDetail.MBTellerPaper, "No");
            txtAccountingObjectAddress4.DataBindings.Add("Text", _select, "AccountingObjectAddress");
            txtAccoutingObjectName4.DataBindings.Add("Text", _select, "AccountingObjectName");
            //dteDate4.DataBindings.Add("Text", _select.MBTellerPaperDetail.MBTellerPaper, "Date");
            //postedDate4.DataBindings.Add("Text", _select.MBTellerPaperDetail.MBTellerPaper, "PostedDate");
            //txtIssueBy4.DataBindings.Add("Text", _select.MBTellerPaperDetail.MBTellerPaper, "IssueBy");
            //dteIssueDate4.DataBindings.Add("Text", _select.MBTellerPaperDetail.MBTellerPaper, "IssueDate");
            //txtReason4.DataBindings.Add("Text", _select.MBTellerPaperDetail.MBTellerPaper, "Reason");
            //cbbBankAccountObject4.DataBindings.Add("Value", _select.MBTellerPaperDetail.MBTellerPaper, "BankAccount", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtBankAccountObjectName4.DataBindings.Add("Text", _select.MBTellerPaperDetail.MBTellerPaper, "BankName");

            //tab 6            
            cbbAccountingObjectID5.DataBindings.Add("Value", _select, "AccountingObjectID", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtNo5.DataBindings.Add("Text", _select.MBCreditCardDetail.MBCreditCard, "No");
            txtAccountingObjectAddress5.DataBindings.Add("Text", _select, "AccountingObjectAddress");
            txtAccoutingObjectName5.DataBindings.Add("Text", _select, "AccountingObjectName");
            //dteDate5.DataBindings.Add("Text", _select.MBCreditCardDetail.MBCreditCard, "Date");
            //postedDate5.DataBindings.Add("Text", _select.MBCreditCardDetail.MBCreditCard, "PostedDate");
            //txtReason5.DataBindings.Add("Text", _select.MBCreditCardDetail.MBCreditCard, "Reason");
            //txtBankAccountingObjectName5.DataBindings.Add("Text", _select.MBCreditCardDetail.MBCreditCard, "AccountingObjectBankName");
            //cbbBankAccountingObject5.DataBindings.Add("Value", _select.MBCreditCardDetail.MBCreditCard, "AccountingObjectBankAccount", true, DataSourceUpdateMode.OnPropertyChanged);
            //cbbCreditCardNumber5.DataBindings.Add("Value", _select.MBCreditCardDetail.MBCreditCard, "CreditCardNumber", true, DataSourceUpdateMode.OnPropertyChanged);

            ///
            ///Bind giao dien voi nhau
            ///


            ConfigGridDong(mauGiaoDien);
            ConfigGridHangTien(mauGiaoDien);
            ConfigGridThue(mauGiaoDien);
            ConfigGridThongKe(mauGiaoDien);
            ConfigGridChungTuChiPhi(mauGiaoDien);


            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                SetOrGetGuiObject(input, false);
            }

            #endregion
        }

        private void ConfigGridDong(Template mauGiaoDien)
        {
            #region Grid động
            Utils.SetConfigDynamicUltraGrid(uGridControl);

            Infragistics.Win.UltraWinGrid.UltraGridBand band = uGridControl.DisplayLayout.Bands[0];
            band.Summaries.Clear();
            if (mauGiaoDien != null)
                Utils.ConfigGrid(uGridControl, ConstDatabase.PPInvoice_TableName, mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 100).TemplateColumns, 1);

            //bỏ dòng tổng số
            uGridControl.DisplayLayout.Bands[0].Summaries.Clear();
            uGridControl.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Default;

            foreach (var item in band.Columns)
            {
                if (item.Key.Equals("TotalAmountOriginal"))
                    item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                else if (item.Key.Equals("CurrencyID"))
                {
                    List<Currency> Currencys = ICurrencyService.GetAll();
                    item.Editor = this.CreateCombobox(Currencys, ConstDatabase.Currency_TableName, "ID", "ID");
                }
                else if (item.Key.Equals("EmployeeID"))
                {

                    List<AccountingObject> AccountingObjects = IAccountingObjectService.getAccountingObjects(2);
                    item.Editor = this.CreateCombobox(AccountingObjects, ConstDatabase.AccountingObject_TableName, "ID", "AccountingObjectName");
                }
                else if (item.Key.Equals("PaymentClauseID"))
                {
                    List<PaymentClause> AccountingObjects = IPaymentClauseService.GetAll();
                    item.Editor = this.CreateCombobox(AccountingObjects, ConstDatabase.PaymentClause_TableName, "ID", "PaymentClauseCode");
                }
                else if (item.Key.Equals("TranspotMethodID"))
                {
                    List<TransportMethod> AccountingObjects = ITransportMethodService.GetAll();
                    item.Editor = this.CreateCombobox(AccountingObjects, ConstDatabase.TransportMethod_TableName, "ID", "TransportMethodCode");
                }
            }
            #endregion
        }

        private void ConfigGridHangTien(Template mauGiaoDien)
        {

            #region Grid Hàng Tiền
            Utils.SetConfigNormalUltraGrid(uGridHangTien);
            //ConfigGrid(uGridHachToan, ConstDatabase.PPInvoiceDetail_TableName, mauGiaoDien);
            uGridHangTien.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;

            Infragistics.Win.UltraWinGrid.UltraGridBand band = uGridHangTien.DisplayLayout.Bands[0];
            band.Summaries.Clear();

            if (mauGiaoDien != null)
                Utils.ConfigGrid(uGridHangTien, ConstDatabase.PPInvoiceDetail_TableName, mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 0).TemplateColumns, 0);

            //Thêm tổng số ở cột "Số tiền"
            band.CreateSummary("SumAmountOriginal", "AmountOriginal");
            foreach (var item in band.Columns)
            {
                //diễn giải, tài khoản nợ, tài khoản có, tk ngân hàng, số tiền, đối tượng, khoản mục chi phí, phòng ban, đối tượng tập hợp chi phí
                ConfigColumn(item);
            }
            #endregion
        }
        private void ConfigGridThue(Template mauGiaoDien)
        {
            #region Grid Thuế
            Utils.SetConfigNormalUltraGrid(uGridTax);
            //ConfigGrid(uGridTax, ConstDatabase.PPInvoiceDetail_TableName, mauGiaoDien);
            //==> Tab Hàng Tiền
            // Turn on all of the Cut, Copy, and Paste functionality. 
            uGridTax.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;
            Infragistics.Win.UltraWinGrid.UltraGridBand band = uGridTax.DisplayLayout.Bands[0];
            band.Summaries.Clear();

            if (mauGiaoDien != null)
                Utils.ConfigGrid(uGridTax, ConstDatabase.PPInvoice_TableName, mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 1).TemplateColumns, 0);

            //Thêm tổng số ở cột "Số tiền"
            band.CreateSummary("SumVATAmountOriginal", "VATAmountOriginal");

            foreach (var item in band.Columns)
            {
                //diễn giải, Tk thuế GTGT, giá tính thuế, thuế suất, tiền thuế, Loại HĐ, Ngày HĐ, Ký hiệu HĐ, Số HĐ, Đối tượng, MST, Địa chỉ
                ConfigColumn(item);
            }
            #endregion
        }
        private void ConfigGridThongKe(Template mauGiaoDien)
        {
            #region Grid Thong ke
            Utils.SetConfigNormalUltraGrid(uGridThongKe);
            //ConfigGrid(uGridHachToan, ConstDatabase.PPInvoiceDetail_TableName, mauGiaoDien);
            uGridThongKe.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;

            Infragistics.Win.UltraWinGrid.UltraGridBand band = uGridThongKe.DisplayLayout.Bands[0];
            band.Summaries.Clear();

            if (mauGiaoDien != null)
                Utils.ConfigGrid(uGridThongKe, ConstDatabase.PPInvoiceDetail_TableName, mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 2).TemplateColumns, 0);

            //Thêm tổng số ở cột "Số tiền"
            foreach (var item in band.Columns)
            {
                ConfigColumn(item);
            }
            #endregion
        }
        private void ConfigGridChungTuChiPhi(Template mauGiaoDien)
        {
            #region Grid Chung Tu Chi phi
            Utils.SetConfigNormalUltraGrid(uGridChungTuChiPhi);
            //ConfigGrid(uGridHachToan, ConstDatabase.PPInvoiceDetail_TableName, mauGiaoDien);
            uGridChungTuChiPhi.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;

            Infragistics.Win.UltraWinGrid.UltraGridBand band = uGridChungTuChiPhi.DisplayLayout.Bands[0];
            band.Summaries.Clear();

            if (mauGiaoDien != null)
                Utils.ConfigGrid(uGridChungTuChiPhi, ConstDatabase.PPInvoiceDetail_TableName, mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 3).TemplateColumns, 0);

            //Thêm tổng số ở cột "Số tiền"
            foreach (var item in band.Columns)
            {
                ConfigColumn(item);
            }
            #endregion
        }

        private void ConfigColumn(UltraGridColumn item)
        {

            if (item.Key.Equals("DebitAccount") || item.Key.Equals("CreditAccount"))
            {   //tài khoản nợ, tài khoản có
                if (_getAccountDefaultByTypeId.Count == 0 || !_getAccountDefaultByTypeId.Keys.Contains(item.Key))
                {
                    item.Editor = this.CreateCombobox(IAccountService.GetAll(), ConstDatabase.Account_TableName, "AccountNumber", "AccountNumber");
                }
                else
                {
                    item.Editor = this.CreateCombobox(_getAccountDefaultByTypeId[item.Key], ConstDatabase.Account_TableName, "AccountNumber", "AccountNumber");
                }
            }
            else if (item.Key.Equals("AccountingObjectID"))
            {
                List<AccountingObject> AccountingObjects = IAccountingObjectService.GetAll();
                item.Editor = this.CreateCombobox(AccountingObjects, ConstDatabase.AccountingObject_TableName, "ID", "AccountingObjectName");
            }

            else if (item.Key.Equals("RepositoryID"))
            {
                List<Repository> MaterialGoods = IRepositoryService.GetAll();
                item.Editor = this.CreateCombobox(MaterialGoods, ConstDatabase.Repository_TableName, "ID", "RepositoryCode");
            }
            else if (item.Key.Equals("MaterialGoodsID"))
            {
                List<MaterialGoods> MaterialGoods = IMaterialGoodsService.GetAll();
                item.Editor = this.CreateCombobox(MaterialGoods, ConstDatabase.MaterialGoods_TableName, "ID", "MaterialGoodsCode");
            }
            else if (item.Key.Equals("VATRate"))
            {

                DefaultEditorOwnerSettings editorSettings = new DefaultEditorOwnerSettings { DataType = typeof(int) };
                ValueList valueList = new ValueList();
                valueList.ValueListItems.Add(0, "0%");
                valueList.ValueListItems.Add(5, "5%");
                valueList.ValueListItems.Add(10, "10%");
                valueList.ValueListItems.Add(-1, "không chịu thuế");
                editorSettings.ValueList = valueList;
                item.Editor = new EditorWithCombo(new DefaultEditorOwner(editorSettings));
                //item.Editor = GetEditorColumInGrid(ConstFrm.ValueList_in_Form_FPPInvoice.VATRateType.ToString());
            }
            else if (item.Key.Equals("ExpenseItemID"))
            {
                List<ExpenseItem> ExpenseItems = IExpenseItemService.GetAll();
                item.Editor = this.CreateCombobox(ExpenseItems, ConstDatabase.ExpenseItem_TableName, "ID", "ExpenseItemCode");
            }
            else if (item.Key.Equals("CostSetID"))
            {
                List<CostSet> CostSets = ICostSetService.GetAll();
                item.Editor = this.CreateCombobox(CostSets, ConstDatabase.CostSet_TableName, "ID", "CostSetCode");
            }
            else if (item.Key.Equals("StatisticsCodeID"))
            {
                List<StatisticsCode> StatisticsCodes = IStatisticsCodeService.GetAll();
                item.Editor = this.CreateCombobox(StatisticsCodes, ConstDatabase.StatisticsCode_TableName, "ID", "StatisticsCode_");
            }
            else if (item.Key.Equals("BudgetItemID"))
            {
                List<BudgetItem> BudgetItems = IBudgetItemService.GetAll();
                item.Editor = this.CreateCombobox(BudgetItems, ConstDatabase.BudgetItem_TableName, "ID", "BudgetItemCode");
            }
            else if (item.Key.Equals("DepartmentID"))
            {
                List<Department> Departments = IDepartmentService.GetAll();
                DefaultEditorOwnerSettings editorSettings = new DefaultEditorOwnerSettings();
                Infragistics.Win.UltraWinGrid.UltraDropDown dropDown_Departments = new Infragistics.Win.UltraWinGrid.UltraDropDown();
                dropDown_Departments.Visible = false;
                this.Controls.Add(dropDown_Departments);
                dropDown_Departments.DataSource = Departments;
                dropDown_Departments.Tag = Departments;
                dropDown_Departments.ValueMember = "ID";
                dropDown_Departments.DisplayMember = "DepartmentCode";
                Utils.ConfigGrid(dropDown_Departments, ConstDatabase.Department_TableName);
                editorSettings.ValueList = dropDown_Departments;
                editorSettings.DataType = typeof(int);
                item.Editor = new EditorWithCombo(new DefaultEditorOwner(editorSettings));
            }
            else if (item.Key.Equals("ContractID"))
            {
                //List<CostSet> StatisticsCodes = IContra.GetAll();
                //item.Editor = this.CreateCombobox(ICostSetService, ConstDatabase.CostSet_TableName, "ID", "CostSetCode");                    
            }
            else if (item.Key.Equals("VATAmountOriginal") || item.Key.Equals("TotalAmountOriginal") ||
                item.Key.Equals("Amount") || item.Key.Equals("UnitPrice") ||
                item.Key.Equals("PretaxAmountOriginal"))
            {
                item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            }
        }

        protected override bool Add()
        {
            //Gui to object
            #region Fill dữ liệu control vào obj
            PPInvoice temp = _select;
            #endregion

            #region Thao tác CSDL
            try
            {
                //_MBTellerPaperDetail = temp.MBTellerPaperDetail;
                //_MBCreditCardDetail = temp.MBCreditCardDetail;
                //_MCPaymentDetail = temp.MCPaymentDetail;

                IPPInvoiceService.BeginTran();

                if (ultraComboEditor1.Enabled && ultraComboEditor1.SelectedIndex == 0)
                {
                    //mua hang tien mat - 117
                    //temp.MBTellerPaperDetail = null;
                    //temp.MBCreditCardDetail = null;

                    //temp.MCPaymentDetail.MCPayment.AccountingObjectAddress = temp.AccountingObjectAddress;
                    //temp.MCPaymentDetail.MCPayment.AccountingObjectID = temp.AccountingObjectID;
                    //temp.MCPaymentDetail.MCPayment.AccountingObjectName = temp.AccountingObjectName;

                    //temp.MCPaymentDetail.MCPayment.CustomProperty1 = temp.CustomProperty1;
                    //temp.MCPaymentDetail.MCPayment.CustomProperty2 = temp.CustomProperty2;
                    //temp.MCPaymentDetail.MCPayment.CustomProperty3 = temp.CustomProperty3;
                    //temp.MCPaymentDetail.MCPayment.CurrencyID = temp.CurrencyID;
                    //temp.MCPaymentDetail.MCPayment.EmployeeID = temp.EmployeeID;
                    ////temp.MCPaymentDetail.MCPayment.IsImportPurchase = temp.IsImportPurchase;
                    //temp.MCPaymentDetail.MCPayment.Recorded = temp.Recorded;
                    //temp.MCPaymentDetail.MCPayment.TotalAmount = temp.TotalAmount;
                    //temp.MCPaymentDetail.MCPayment.TotalAmountOriginal = temp.TotalAmountOriginal;
                    //temp.MCPaymentDetail.MCPayment.TotalVATAmount = temp.TotalVATAmount;
                    //temp.MCPaymentDetail.MCPayment.TotalVATAmountOriginal = temp.TotalVATAmountOriginal;
                    //temp.MCPaymentDetail.MCPayment.TransportMethodID = temp.TransportMethodID;
                    //temp.MCPaymentDetail.MCPayment.TypeID = 117;
                    //temp.MCPaymentDetail.MCPayment.CurrencyID = temp.CurrencyID;
                    //IMCPaymentService.CreateNew(temp.MCPaymentDetail.MCPayment);
                    for (int i = 0; i < temp.PPInvoiceDetails.Count; i++)
                    {
                        PPInvoiceDetail temp2 = temp.PPInvoiceDetails[i];
                        //temp.MCPaymentDetail.Amount = temp2.Amount;
                        //temp.MCPaymentDetail.AmountOriginal = temp2.AmountOriginal;
                        //temp.MCPaymentDetail.CustomProperty1 = temp2.CustomProperty1;
                        //temp.MCPaymentDetail.CustomProperty2 = temp2.CustomProperty2;
                        //temp.MCPaymentDetail.CustomProperty3 = temp2.CustomProperty3;
                        //temp.MCPaymentDetail.DebitAccount = temp2.CreditAccount;
                        //temp.MCPaymentDetail.AccountingObjectID = temp2.AccountingObjectID;
                        //temp.MCPaymentDetail.OrderPriority = temp2.OrderPriority;

                        //IMCPaymentDetailService.CreateNew(temp.MCPaymentDetail);

                    }

                }
                else if (ultraComboEditor1.Enabled && ultraComboEditor1.SelectedIndex == 4)
                {
                    //mua hang the tin dung - 171
                    //temp.MBTellerPaperDetail = null;
                    //temp.MCPaymentDetail = null;

                    //temp.MBCreditCardDetail.MBCreditCardID = temp.MBCreditCardDetail.MBCreditCard.ID;
                    //temp.MBCreditCardDetail.MBCreditCard.AccountingObjectAddress = temp.AccountingObjectAddress;
                    //temp.MBCreditCardDetail.MBCreditCard.AccountingObjectID = temp.AccountingObjectID;
                    //temp.MBCreditCardDetail.MBCreditCard.AccountingObjectName = temp.AccountingObjectName;

                    //temp.MBCreditCardDetail.MBCreditCard.CustomProperty1 = temp.CustomProperty1;
                    //temp.MBCreditCardDetail.MBCreditCard.CustomProperty2 = temp.CustomProperty2;
                    //temp.MBCreditCardDetail.MBCreditCard.CustomProperty3 = temp.CustomProperty3;
                    //temp.MBCreditCardDetail.MBCreditCard.CurrencyID = temp.CurrencyID;
                    //temp.MBCreditCardDetail.MBCreditCard.EmployeeID = temp.EmployeeID;
                    //temp.MBCreditCardDetail.MBCreditCard.IsImportPurchase = temp.IsImportPurchase;
                    //temp.MBCreditCardDetail.MBCreditCard.IsMatch = temp.IsMatch;
                    //temp.MBCreditCardDetail.MBCreditCard.MatchDate = temp.MatchDate;
                    //temp.MBCreditCardDetail.MBCreditCard.Recorded = temp.Recorded;
                    //temp.MBCreditCardDetail.MBCreditCard.TotalAmount = temp.TotalAmount;
                    //temp.MBCreditCardDetail.MBCreditCard.TotalAmountOriginal = temp.TotalAmountOriginal;
                    //temp.MBCreditCardDetail.MBCreditCard.TotalVATAmount = temp.TotalVATAmount;
                    //temp.MBCreditCardDetail.MBCreditCard.TotalVATAmountOriginal = temp.TotalVATAmountOriginal;
                    //temp.MBCreditCardDetail.MBCreditCard.TransportMethodID = temp.TransportMethodID;
                    //temp.MBCreditCardDetail.MBCreditCard.TypeID = 171;
                    //temp.MBCreditCardDetail.MBCreditCard.CurrencyID = temp.CurrencyID;
                    //IMBCreditCardService.CreateNew(temp.MBCreditCardDetail.MBCreditCard);

                    for (int i = 0; i < temp.PPInvoiceDetails.Count; i++)
                    {
                        PPInvoiceDetail temp2 = temp.PPInvoiceDetails[i];
                        //temp.MBTellerPaperDetail = null;
                        //temp.MCPaymentDetail = null;
                        //temp.MBCreditCardDetail.Amount = temp2.Amount;
                        //temp.MBCreditCardDetail.AmountOriginal = temp2.AmountOriginal;
                        ////temp.MBCreditCardDetail.DiscountAmount = temp2.DiscountAmount;
                        ////temp.MBCreditCardDetail.DiscountAmountOriginal = temp2.DiscountAmountOriginal;
                        //temp.MBCreditCardDetail.CustomProperty1 = temp2.CustomProperty1;
                        //temp.MBCreditCardDetail.CustomProperty2 = temp2.CustomProperty2;
                        //temp.MBCreditCardDetail.CustomProperty3 = temp2.CustomProperty3;
                        ////temp.MBCreditCardDetail.EmployeeID = temp.EmployeeID;
                        ////temp.MBCreditCardDetail.InvoiceNo = temp2.InvoiceNo;
                        ////temp.MBCreditCardDetail.InvoiceDate = temp2.InvoiceDate;
                        //temp.MBCreditCardDetail.DebitAccount = temp2.CreditAccount;
                        //temp.MBCreditCardDetail.AccountingObjectID = temp2.AccountingObjectID;
                        //temp.MBCreditCardDetail.OrderPriority = temp2.OrderPriority;

                        ////temp.MBCreditCardDetail.PPInvoiceID = temp.ID;
                        ////temp.MBCreditCardDetail.DueDate = temp.DueDate;
                        //IMBCreditCardDetailService.CreateNew(temp.MBCreditCardDetail);
                    }

                }
                else if (ultraComboEditor1.Enabled)
                {
                    //mua hang uy nhiem chi - 127
                    int typeTmp = 0;
                    if (ultraComboEditor1.SelectedIndex == 1) typeTmp = 127;
                    //mua sec chuyen khoan - 131
                    if (ultraComboEditor1.SelectedIndex == 2) typeTmp = 131;
                    //mua hang sectienmat  - 141
                    if (ultraComboEditor1.SelectedIndex == 3) typeTmp = 141;
                    //temp.MBCreditCardDetail = null;
                    //temp.MCPaymentDetail = null;

                    //temp.MBTellerPaperDetail.MBTellerPaper.AccountingObjectAddress = temp.AccountingObjectAddress;
                    //temp.MBTellerPaperDetail.MBTellerPaper.AccountingObjectID = temp.AccountingObjectID;
                    //temp.MBTellerPaperDetail.MBTellerPaper.AccountingObjectName = temp.AccountingObjectName;

                    //temp.MBTellerPaperDetail.MBTellerPaper.CustomProperty1 = temp.CustomProperty1;
                    //temp.MBTellerPaperDetail.MBTellerPaper.CustomProperty2 = temp.CustomProperty2;
                    //temp.MBTellerPaperDetail.MBTellerPaper.CustomProperty3 = temp.CustomProperty3;
                    //temp.MBTellerPaperDetail.MBTellerPaper.CurrencyID = temp.CurrencyID;
                    //temp.MBTellerPaperDetail.MBTellerPaper.EmployeeID = temp.EmployeeID;
                    //temp.MBTellerPaperDetail.MBTellerPaper.IsImportPurchase = temp.IsImportPurchase;
                    //temp.MBTellerPaperDetail.MBTellerPaper.Recorded = temp.Recorded;
                    //temp.MBTellerPaperDetail.MBTellerPaper.TotalAmount = temp.TotalAmount;
                    //temp.MBTellerPaperDetail.MBTellerPaper.TotalAmountOriginal = temp.TotalAmountOriginal;
                    //temp.MBTellerPaperDetail.MBTellerPaper.TotalVATAmount = temp.TotalVATAmount;
                    //temp.MBTellerPaperDetail.MBTellerPaper.TotalVATAmountOriginal = temp.TotalVATAmountOriginal;
                    //temp.MBTellerPaperDetail.MBTellerPaper.TransportMethodID = temp.TransportMethodID;
                    //temp.MBTellerPaperDetail.MBTellerPaper.TypeID = typeTmp;
                    //temp.MBTellerPaperDetail.MBTellerPaper.CurrencyID = temp.CurrencyID;
                    //IMBTellerPaperService.CreateNew(temp.MBTellerPaperDetail.MBTellerPaper);
                    ////temp.MBTellerPaperDetail.DueDate = temp.DueDate;
                    //for (int i = 0; i < temp.PPInvoiceDetails.Count; i++)
                    //{
                    //    PPInvoiceDetail temp2 = temp.PPInvoiceDetails[i];
                    //    temp.MBTellerPaperDetail.Amount = temp2.Amount;
                    //    temp.MBTellerPaperDetail.AmountOriginal = temp2.AmountOriginal;
                    //    //temp.MBTellerPaperDetail.DiscountAmount = temp2.DiscountAmount;
                    //    //temp.MBTellerPaperDetail.DiscountAmountOriginal = temp2.DiscountAmountOriginal;
                    //    temp.MBTellerPaperDetail.CustomProperty1 = temp2.CustomProperty1;
                    //    temp.MBTellerPaperDetail.CustomProperty2 = temp2.CustomProperty2;
                    //    temp.MBTellerPaperDetail.CustomProperty3 = temp2.CustomProperty3;
                    //    //temp.MBTellerPaperDetail.InvoiceNo = temp2.InvoiceNo;
                    //    //temp.MBTellerPaperDetail.InvoiceDate = temp2.InvoiceDate;
                    //    temp.MBTellerPaperDetail.DebitAccount = temp2.CreditAccount;
                    //    temp.MBTellerPaperDetail.AccountingObjectID = temp2.AccountingObjectID;
                    //    temp.MBTellerPaperDetail.OrderPriority = temp2.OrderPriority;
                    //    IMBTellerPaperDetailService.CreateNew(temp.MBTellerPaperDetail);

                    //}
                }
                else
                {
                    //temp.MBTellerPaperDetail = null;
                    //temp.MBCreditCardDetail = null;
                    //temp.MCPaymentDetail = null;
                }

                IPPInvoiceService.CreateNew(temp);

                #region Up bảng gencode sinh đơn hàng tiếp theo
                GenCode gencode = IGenCodeService.getGenCode(ConstFrm.TypeGroup_PPInvoice);
                Dictionary<string, string> dicNo = Utils.CheckNo(_select.No);
                if (dicNo != null)
                {
                    gencode.Prefix = dicNo["Prefix"];
                    decimal _decimal = 0;
                    decimal.TryParse(dicNo["Value"], out _decimal);
                    gencode.CurrentValue = _decimal + 1;
                    gencode.Suffix = dicNo["Suffix"];
                    IGenCodeService.Update(gencode);
                }
                #endregion

                IPPInvoiceService.CommitTran();
            }
            catch (Exception)
            {
                IPPInvoiceService.RolbackTran();
            }
            finally
            {
                //temp.MBTellerPaperDetail = _MBTellerPaperDetail;
                //temp.MBCreditCardDetail = _MBCreditCardDetail;
                //temp.MCPaymentDetail = _MCPaymentDetail;
            }
            #endregion
            return true;
        }

        protected override bool Saveleged()
        {
            // luu d? li?u phi?u thu vào các b?ng s? cái và c?p nh?t tr?ng thái c?a phi?u thu
            // 1. Luu d? li?u vào b?ng s? cái GenegedLeged
            // 2. C?p nh?t tr?ng thái c?a phi?u thu
            #region th?c hi?n set giá tr? post = true
            try
            {
                //PPInvoice temp = IPPInvoiceService.Query.SingleOrDefault(p => p.No.Equals(txtNo.Text));
                PPInvoice temp = IPPInvoiceService.GetPPInvoicebyNo(_select.No);
                if (temp == null)
                {
                    return true;
                }
                //List<PPInvoiceDetail> listTempDetail =IPPInvoiceDetailService.Query.Where(p => p.PPInvoiceID == temp.ID).ToList();
                List<PPInvoiceDetail> listTempDetail = IPPInvoiceDetailService.GetListPPInvoiceDetailbyID(temp.ID);
                List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
                for (int i = 0; i < listTempDetail.Count; i++)
                {
                    GeneralLedger genTemp = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = temp.ID,
                        TypeID = temp.TypeID,
                        Date = temp.Date,
                        PostedDate = temp.PostedDate,
                        No = temp.No,
                        Account = listTempDetail[i].DebitAccount,
                        AccountCorresponding = listTempDetail[i].CreditAccount,
                        CurrencyID = temp.CurrencyID,
                        ExchangeRate = temp.ExchangeRate,
                        DebitAmount = listTempDetail[i].Amount,
                        DebitAmountOriginal = listTempDetail[i].AmountOriginal,
                        CreditAmount = 0,
                        CreditAmountOriginal = 0,
                        Reason = temp.Reason,
                        Description = listTempDetail[i].Description,
                        AccountingObjectID = temp.AccountingObjectID,
                        EmployeeID = temp.EmployeeID,
                        BudgetItemID = listTempDetail[i].BudgetItemID,
                        CostSetID = listTempDetail[i].CostSetID,
                        ContractID = listTempDetail[i].ContractID,
                        ContactName = temp.ContactName,
                        DetailID = listTempDetail[i].ID,
                        RefNo = temp.No,
                        RefDate = temp.Date,
                        DepartmentID = listTempDetail[i].DepartmentID,
                        ExpenseItemID = listTempDetail[i].ExpenseItemID,
                        IsIrrationalCost = listTempDetail[i].IsIrrationalCost
                    };
                    if (((listTempDetail[i].CreditAccount.StartsWith("133")) || listTempDetail[i].CreditAccount.StartsWith("33311")))
                    {
                        genTemp.VATDescription = listTempDetail[i].Description;
                    }
                    listGenTemp.Add(genTemp);
                    GeneralLedger genTempCorresponding = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = temp.ID,
                        TypeID = temp.TypeID,
                        Date = temp.Date,
                        PostedDate = temp.PostedDate,
                        No = temp.No,
                        Account = listTempDetail[i].CreditAccount,
                        AccountCorresponding = listTempDetail[i].DebitAccount,
                        CurrencyID = temp.CurrencyID,
                        ExchangeRate = temp.ExchangeRate,
                        DebitAmount = 0,
                        DebitAmountOriginal = 0,
                        CreditAmount = listTempDetail[i].Amount,
                        CreditAmountOriginal = listTempDetail[i].AmountOriginal,
                        Reason = temp.Reason,
                        Description = listTempDetail[i].Description,
                        AccountingObjectID = temp.AccountingObjectID,
                        EmployeeID = temp.EmployeeID,
                        BudgetItemID = listTempDetail[i].BudgetItemID,
                        CostSetID = listTempDetail[i].CostSetID,
                        ContractID = listTempDetail[i].ContractID,
                        ContactName = temp.ContactName,
                        DetailID = listTempDetail[i].ID,
                        RefNo = temp.No,
                        RefDate = temp.Date,
                        DepartmentID = listTempDetail[i].DepartmentID,
                        ExpenseItemID = listTempDetail[i].ExpenseItemID,
                        IsIrrationalCost = listTempDetail[i].IsIrrationalCost
                    };
                    if (((listTempDetail[i].CreditAccount.StartsWith("133")) || listTempDetail[i].CreditAccount.StartsWith("33311")))
                    {
                        genTempCorresponding.VATDescription = listTempDetail[i].Description;
                    }
                    listGenTemp.Add(genTempCorresponding);
                }
                IPPInvoiceService.BeginTran();
                foreach (var generalLedger in listGenTemp)
                {
                    IGeneralLedgerService.CreateNew(generalLedger);
                }
                temp.Recorded = true;
                IPPInvoiceService.Update(temp);
                IPPInvoiceService.CommitTran();
            }
            catch { IPPInvoiceService.RolbackTran(); }

            #endregion
            return true;
        }

        protected override bool Removeleged()
        {
            // Xóa dữ liệu phiếu thu ? các bảng sổ cái và cập nhật trạng thái của phiếu thu
            // 1. Xóa dữ liệu trong các bảng sổ cái GenegedLeged
            // 2. Cập nhật trạng thái của phiếu thu
            try
            {
                IPPInvoiceService.BeginTran();
                // 1. Xóa dữ liệu vào các bảng sổ cái GenegedLeged
                _select.Recorded = false;
                //List<GeneralLedger> listGenTemp = IGeneralLedgerService.Query.Where(p => p.ReferenceID == _select.ID).ToList();
                List<GeneralLedger> listGenTemp = IGeneralLedgerService.GetListByReferenceID(_select.ID);
                foreach (var generalLedger in listGenTemp)
                {
                    IGeneralLedgerService.Delete(generalLedger);
                }
                IPPInvoiceService.Update(_select);
                IPPInvoiceService.CommitTran();
            }
            catch (Exceptions)
            {
                IPPInvoiceService.RolbackTran();
            }
            return true;
        }

        protected override bool Edit()
        {
            //Gui to object
            #region Fill dữ liệu control vào obj
            PPInvoice temp = _select;
            #endregion

            #region Thao tác CSDL
            IPPInvoiceService.Update(temp);
            #endregion
            return true;
        }
        protected override bool Delete()
        {
            IPPInvoiceService.Delete(_select);
            IPPInvoiceService.CommitChanges();
            return true;
        }
        #endregion

        #region Utils

        /// <summary>
        /// Kiểm tra tên một AccountingObject đã có trong danh sách các AccountingObject chưa?
        /// </summary>
        /// <param name="strText">tên đối tượng AccountingObject</param>
        /// <param name="dsAccountingObject">danh sách AccountingObject</param>
        /// <returns>TRUE: đã có trong danh sách, FALSE: chưa có trong danh sách</returns>
        bool checkObjinCbbAccountingObjectID(string strText, List<AccountingObject> dsAccountingObject)
        {
            //check xem đối tượng có chính xác hay không?
            return dsAccountingObject.Any(item => item.AccountingObjectName.Equals(strText));
        }

        /// <summary>
        /// Reset giá trị của Form về trống hết
        /// </summary>
        protected override void ResetControlForm()
        {
            Guid tempGuid = _select.ID;
            Utils.Copy(typeof(PPInvoice), new PPInvoice(), typeof(PPInvoice), _select);
            //_select.No = string.Empty;
            //_select.Date = DateTime.Now;
            //_select.PostedDate = DateTime.Now;
            //_select.AccountingObjectID = null;
            //_select.ContactName = string.Empty;
            //_select.AccountingObjectAddress = string.Empty;
            //_select.Reason = string.Empty;
            //_select.TotalAmountOriginal = 0; 
            //_select.TotalAmount = 0; 
            _select.CurrencyID = "VND";

            _select.PPInvoiceDetails.Clear();
        }

        /// <summary>
        /// DeActive hoặc Active control khi ở trạng thái xem hoặc không xem
        /// </summary>
        /// <param name="isEnable"></param>
        public override void ChangeStatusControl(bool isEnable)
        {
            ultraGroupBox2.Enabled = isEnable;
            ultraGroupBox1.Enabled = isEnable;
            ultraTabControl1.Enabled = isEnable;
            uGridControl.Enabled = isEnable;
        }
        #endregion

        #region Event

        private void btnNewAccountingObjectID_Click(object sender, EventArgs e)
        {
            MSG.MessageBoxStand("Thêm mới đối tượng kế toán", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }



        #region Event uGridHachToan
        private void uGridHangTien_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {//[Grid Hàng Tiền uGridHachToan] sau khi giá trị của Cell được cập nhật

            #region những danh mục cha con không được Hàng Tiền vào nút con
            if (e.Cell.Column.Editor.GetType().Name.Equals("EditorWithCombo"))
            {
                //mục thu/chi, phòng ban, khoản mục chi phí, mã thống kê, đối tượng tập hợp chi phí
                EditorWithCombo editor = (EditorWithCombo)e.Cell.Column.Editor;
                bool kiemtra = false;
                if (e.Cell.Column.Key.Equals("BudgetItemID"))
                    kiemtra = ((List<BudgetItem>)editor.Tag)[editor.ValueList.SelectedItemIndex].IsParentNode;
                else if (e.Cell.Column.Key.Equals("DepartmentID"))
                    kiemtra = ((List<Department>)editor.Tag)[editor.ValueList.SelectedItemIndex].IsParentNode;
                else if (e.Cell.Column.Key.Equals("ExpenseItemID"))
                    kiemtra = ((List<ExpenseItem>)editor.Tag)[editor.ValueList.SelectedItemIndex].IsParentNode;
                else if (e.Cell.Column.Key.Equals("StatisticsCodeID"))
                    kiemtra = ((List<StatisticsCode>)editor.Tag)[editor.ValueList.SelectedItemIndex].IsParentNode;
                else if (e.Cell.Column.Key.Equals("CostSetID"))
                    kiemtra = ((List<CostSet>)editor.Tag)[editor.ValueList.SelectedItemIndex].IsParentNode;
                if (!kiemtra)
                {//test xét error
                    //uGridHachToan.Rows[0].Cells["ExpenseItemID"]
                }
            }
            #endregion

        }

        #endregion





        #region Event Test

        private void uGridHangTien_KeyUp(object sender, KeyEventArgs e)
        {//khi ấn delete thì tự xóa trống cell --> chưa hoàn thiện lắm
            if (e.KeyCode == Keys.Delete)
            {
                if (uGridHangTien.ActiveCell.Column.DataType == typeof(Guid))
                {
                    return;
                }
                else if (uGridHangTien.ActiveCell.Column.DataType == typeof(string))
                {
                    uGridHangTien.ActiveCell.SetValue(string.Empty, true);
                }
            }
        }
        #endregion


        #endregion

        private void uGridHangTien_AfterExitEditMode(object sender, EventArgs e)
        {

            RecalculateFormula(sender);
        }

        private void uGridTax_AfterExitEditMode(object sender, EventArgs e)
        {
            RecalculateFormula(sender);
        }

        private void uGridThongKe_AfterExitEditMode(object sender, EventArgs e)
        {
            RecalculateFormula(sender);
        }

        private void RecalculateFormula(object sender)
        {
            int index = ((Infragistics.Win.UltraWinGrid.UltraGrid)sender).ActiveCell.Row.Index;
            if (uGridTax.Rows.Count <= index)
            {
                var row = uGridTax.DisplayLayout.Bands[0].AddNew();
            }
            if (uGridThongKe.Rows.Count <= index)
            {
                var row = uGridThongKe.DisplayLayout.Bands[0].AddNew();
            }
            if (uGridHangTien.Rows.Count <= index)
            {
                var row = uGridHangTien.DisplayLayout.Bands[0].AddNew();
            }
            if (uGridChungTuChiPhi.Rows.Count <= index)
            {
                var row = uGridChungTuChiPhi.DisplayLayout.Bands[0].AddNew();
            }

            double quantity = Convert.ToDouble(uGridHangTien.Rows[index].Cells["Quantity"].Value);
            double convertRate = Convert.ToDouble(uGridHangTien.Rows[index].Cells["ConvertRate"].Value);
            double unitPrice = Convert.ToDouble(uGridHangTien.Rows[index].Cells["UnitPriceOriginal"].Value);
            double vatRate = Convert.ToDouble(uGridTax.Rows[index].Cells["VATRate"].Value);
            double discountRate = Convert.ToDouble(uGridTax.Rows[index].Cells["DiscountRate"].Value);
            double importTaxRate = Convert.ToDouble(uGridTax.Rows[index].Cells["ImportTaxRate"].Value);
            double specialConsumeTaxRate = Convert.ToDouble(uGridTax.Rows[index].Cells["SpecialConsumeTaxRate"].Value);

            //so luong chuyen doi = so luong * ty le chuyen doi
            uGridHangTien.Rows[index].Cells["QuantityConvert"].Value = (quantity * convertRate).ToString();
            //don gia chuyen doi = don gia / ty le chuyen doi
            uGridHangTien.Rows[index].Cells["UnitPriceConvert"].Value = convertRate == 0 ? "0" : (unitPrice / convertRate).ToString();
            //don gia chuyen doi sau thue = (don gia / ty le chuyen doi) * (1 + vat rate)

            //thanh tien = so luong * don gia
            uGridHangTien.Rows[index].Cells["AmountOriginal"].Value = (unitPrice * quantity).ToString();
            //thanh tien sau thue = (so luong * don gia) * (1 + vat rate)
            uGridHangTien.Rows[index].Cells["AmountAfterTaxOriginal"].Value = (unitPrice * quantity * (vatRate / 100 + 1)).ToString();
            //tien ck sau thue = tien ck * (1 + vat rate)
            uGridTax.Rows[index].Cells["DiscountAmountAfterTaxOriginal"].Value = (unitPrice * quantity * discountRate * (vatRate / 100 + 1)).ToString();
            //thue gtgt = (thanh tien - tien ck ) * vat rate
            uGridTax.Rows[index].Cells["VATAmountOriginal"].Value = ((vatRate / 100) * (unitPrice * quantity) * (1 - discountRate / 100)).ToString();
            //tien ck = thanh tien * ty le chiet khau
            uGridHangTien.Rows[index].Cells["DiscountAmountOriginal"].Value = (quantity * unitPrice * discountRate / 100).ToString();
            //tien thue nhap khau = thue xuat nhap khau *  thanh tien
            uGridTax.Rows[index].Cells["ImportTaxAmountOriginal"].Value = (importTaxRate * (unitPrice * quantity)).ToString();
            //tien thue ttdb = Tỷ suất thuế tiêu thụ đặc biệt *  thanh tien
            uGridTax.Rows[index].Cells["SpecialConsumeTaxAmountOriginal"].Value = (importTaxRate * (unitPrice * quantity) * (importTaxRate + 1)).ToString();

            //tong tien chiet khau = tong cot tien chiet khau 
            var totalDiscountAmountOriginal = (from n in uGridHangTien.Rows select (decimal?)n.Cells["DiscountAmountOriginal"].Value).Sum() ?? 0;
            lbTotalDiscountAmountOriginal.Text = totalDiscountAmountOriginal.ToString();
            //tong tien hang = tong cot thanh tien
            var totalAmountOriginal = (from n in uGridHangTien.Rows select (decimal?)n.Cells["AmountOriginal"].Value).Sum() ?? 0;
            lbTotalAmountOriginal.Text = totalAmountOriginal.ToString();
            //tong tien thue gtgt = tong cot thue gtgt
            var totalVATAmountOriginal = (from n in uGridTax.Rows select (decimal?)n.Cells["VATAmountOriginal"].Value).Sum() ?? 0;
            lbTotalVatAmountOriginal.Text = totalVATAmountOriginal.ToString();
            //tong tien thue nhập khẩu = tong cot thue nhập khẩu
            var totalImportTaxAmountOriginal = (from n in uGridTax.Rows select (decimal?)n.Cells["ImportTaxAmountOriginal"].Value).Sum() ?? 0;
            lbTotalImportTaxAmountOriginal.Text = totalImportTaxAmountOriginal.ToString();
            //tong tien thue ttdb = tong cot thue ttdb
            var totalSpecialConsumeTaxAmountOriginal = (from n in uGridTax.Rows select (decimal?)n.Cells["SpecialConsumeTaxAmountOriginal"].Value).Sum() ?? 0;
            lbTotalSpecialConsumeTaxAmountOriginal.Text = totalSpecialConsumeTaxAmountOriginal.ToString();

            //tong tien thanh toan = tong tien hang - tong tien ck + tong tien thue GTGT    
            lbTotalPaymentAmoutOriginal.Text = (totalAmountOriginal - totalDiscountAmountOriginal + totalVATAmountOriginal).ToString();
        }

        private void uButtonPPOrder_Click(object sender, EventArgs e)
        {
            FPPOrderDialog fPPOrderDialog = new FPPOrderDialog(IPPOrderService.GetAll());
            fPPOrderDialog.ShowDialog();
            if (fPPOrderDialog.PPOrder != null)
            {
                Utils.Copy(typeof(PPOrder), (object)fPPOrderDialog.PPOrder, typeof(PPInvoice), (object)_select);
                //uGridControl.DataSource = ltemp;
                List<PPInvoice> ltemp = new List<PPInvoice> { _select };
                uGridControl.DataSource = ltemp;
                uGridControl.Refresh();
            };
        }
        UserControl userControl;
        private void ultraComboEditor1_ValueChanged(object sender, EventArgs e)
        {
            ultraTabControl2.Tabs[1].Visible = false;
            ultraTabControl2.Tabs[2].Visible = false;
            ultraTabControl2.Tabs[3].Visible = false;
            ultraTabControl2.Tabs[4].Visible = false;
            ultraTabControl2.Tabs[5].Visible = false;
            ultraTabControl2.Tabs[ultraComboEditor1.SelectedIndex + 1].Visible = true;
        }

        private void uInfoTabControl_ActiveTabChanged(object sender, ActiveTabChangedEventArgs e)
        {
            panel5.Size = (e.Tab.Index == 0) ? new System.Drawing.Size(929, 230) : new System.Drawing.Size(929, 280);
        }

        private void ultraOptionSet1_ValueChanged(object sender, EventArgs e)
        {
            ultraComboEditor1.Enabled = (Convert.ToInt32(ultraOptionSet1.Value) == 1) ? true : false;
        }

        private void cbbAccountingObjectID_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            MessageBox.Show("Không tồn tại trong list!");
        }

        private void cbbAccountingObjectID_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAccountingObjectID.SelectedRow == null) return;
            txtAccoutingObjectName.Text = ((AccountingObject)cbbAccountingObjectID.SelectedRow.ListObject).AccountingObjectName;
        }

        private void cbbAccountingObjectID1_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAccountingObjectID1.SelectedRow == null) return;
            txtAccoutingObjectName1.Text = ((AccountingObject)cbbAccountingObjectID1.SelectedRow.ListObject).AccountingObjectName;
        }

        private void cbbAccountingObjectID2_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAccountingObjectID2.SelectedRow == null) return;
            txtAccoutingObjectName2.Text = ((AccountingObject)cbbAccountingObjectID2.SelectedRow.ListObject).AccountingObjectName;
        }

        private void cbbAccountingObjectID3_ValueChanged(object sender, EventArgs e)
        {

            if (cbbAccountingObjectID3.SelectedRow == null) return;
            txtAccoutingObjectName3.Text = ((AccountingObject)cbbAccountingObjectID3.SelectedRow.ListObject).AccountingObjectName;
        }

        private void cbbAccountingObjectID4_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAccountingObjectID4.SelectedRow == null) return;
            AccountingObject creditCard = ((AccountingObject)cbbAccountingObjectID4.SelectedRow.ListObject);
            txtAccoutingObjectName4.Text = creditCard.AccountingObjectName;
            txtIdentificationNo4.Text = creditCard.IdentificationNo;
            dteIssueDate4.DateTime = (creditCard.IssueDate == null) ? new DateTime() : creditCard.IssueDate.Value;
            txtIssueBy4.Text = creditCard.IssueBy;
        }

        private void cbbAccountingObjectID5_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAccountingObjectID5.SelectedRow == null) return;
            txtAccoutingObjectName5.Text = ((AccountingObject)cbbAccountingObjectID5.SelectedRow.ListObject).AccountingObjectName;
        }

        private void cbbBankAccountObject2_ValueChanged(object sender, EventArgs e)
        {
            if (cbbBankAccountObject2.SelectedRow == null) return;
            txtBankAccountObjectName2.Text = ((BankAccountDetail)cbbBankAccountObject2.SelectedRow.ListObject).BankName;
        }

        private void cbbBankAccountObject3_ValueChanged(object sender, EventArgs e)
        {
            if (cbbBankAccountObject3.SelectedRow == null) return;
            txtBankAccountObjectName3.Text = ((BankAccountDetail)cbbBankAccountObject3.SelectedRow.ListObject).BankName;
        }

        private void cbbBankAccountObject4_ValueChanged(object sender, EventArgs e)
        {
            if (cbbBankAccountObject4.SelectedRow == null) return;
            txtBankAccountObjectName4.Text = ((BankAccountDetail)cbbBankAccountObject4.SelectedRow.ListObject).BankName;
        }

        private void cbbBankAccountingObject2_ValueChanged(object sender, EventArgs e)
        {
            if (cbbBankAccountingObject2.SelectedRow == null) return;
            txtBankAccountingObjectName2.Text = ((AccountingObjectBankAccount)cbbBankAccountingObject2.SelectedRow.ListObject).BankName;
        }

        private void cbbBankAccountingObject3_ValueChanged(object sender, EventArgs e)
        {
            if (cbbBankAccountingObject3.SelectedRow == null) return;
            txtBankAccountingObjectName3.Text = ((AccountingObjectBankAccount)cbbBankAccountingObject3.SelectedRow.ListObject).BankName;
        }

        private void cbbBankAccountingObject5_ValueChanged(object sender, EventArgs e)
        {
            if (cbbBankAccountingObject5.SelectedRow == null) return;
            txtBankAccountingObjectName5.Text = ((AccountingObjectBankAccount)cbbBankAccountingObject5.SelectedRow.ListObject).BankName;
        }
    }

    public class FPPInvoiceDetailTmp1 : DetailBase<PPInvoice> { }
}
