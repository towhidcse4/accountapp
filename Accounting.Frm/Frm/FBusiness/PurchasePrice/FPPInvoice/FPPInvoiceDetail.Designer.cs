﻿namespace Accounting
{
    sealed partial class FPPInvoiceDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton6 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton7 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance116 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance123 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance127 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance129 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance130 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance131 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance132 = new Infragistics.Win.Appearance();
            this.ultraTabPageHoaDon = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.grHoaDon = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtMauSoHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSoHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtKyHieuHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel37 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel38 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel39 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel40 = new Infragistics.Win.Misc.UltraLabel();
            this.dteNgayHD = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraGroupBoxHoaDon = new Infragistics.Win.Misc.UltraGroupBox();
            this.lblChungTuGoc = new Infragistics.Win.Misc.UltraLabel();
            this.btnCongNo = new Infragistics.Win.Misc.UltraButton();
            this.btnPPOrder = new Infragistics.Win.Misc.UltraButton();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtContactName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblContactNameCt = new Infragistics.Win.Misc.UltraLabel();
            this.txtOriginalNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblOriginalNoCt = new Infragistics.Win.Misc.UltraLabel();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblReasonCt = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectAddress = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObjectID = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPagePhieuXuatKho = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBoxPhieuXuatKho = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel29 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
            this.txtAccountingObjectNameTm = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectIDTm = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtMContactNameTm = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNumberAttachTm = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtMReasonPayTm = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressTm = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPagePhieuThu = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObjectIDUnc = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccountingObjectAddressUnc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectNameUnc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbMAccountingObjectBankAccountDetailIDUnc = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtMAccountingObjectBankNameUnc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraButton3 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton4 = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbBankAccountDetailIDUnc = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtBankNameUnc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtMReasonPayUnc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTabPageGiayBaoCo = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObjectIDSck = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccountingObjectAddressSck = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectNameSck = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbMAccountingObjectBankAccountDetailIDSnk = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtMAccountingObjectBankNameSck = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraButton5 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton6 = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbBankAccountDetailIDSck = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtBankNameSck = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtMReasonPaySck = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox7 = new Infragistics.Win.Misc.UltraGroupBox();
            this.dteIssueDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel31 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel30 = new Infragistics.Win.Misc.UltraLabel();
            this.txtIdentificationNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObjectIDStm = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtMContactNameStm = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectNameStm = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtIssueBy = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraGroupBox8 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraButton7 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton8 = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbBankAccountDetailIDStm = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtBankNameStm = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtMReasonPayStm = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObjectIDTtd = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccountingObjectAddressTtd = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectNameTtd = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbMAccountingObjectBankAccountDetailIDTtd = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtMAccountingObjectBankNameTtd = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraGroupBox6 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraButton9 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton10 = new Infragistics.Win.Misc.UltraButton();
            this.txtCreditCardType = new Infragistics.Win.Misc.UltraLabel();
            this.picCreditCardType = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.cbbCreditCardNumber = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtOwnerCard = new System.Windows.Forms.Label();
            this.lblOwnerCard = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.txtMReasonPayTtd = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.palTop = new System.Windows.Forms.Panel();
            this.ultraGroupBoxTop = new Infragistics.Win.Misc.UltraGroupBox();
            this.palVouchersNoHd = new Infragistics.Win.Misc.UltraPanel();
            this.palVouchersNoTm = new Infragistics.Win.Misc.UltraPanel();
            this.palChung = new System.Windows.Forms.Panel();
            this.optThanhToan = new Accounting.UltraOptionSet_Ex();
            this.cbbChonThanhToan = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.palFill = new System.Windows.Forms.Panel();
            this.palGrid = new System.Windows.Forms.Panel();
            this.pnlUgrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.btnApportionTaxes = new Infragistics.Win.Misc.UltraButton();
            this.btnApportion = new Infragistics.Win.Misc.UltraButton();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.palThongTinChung = new System.Windows.Forms.Panel();
            this.ultraTabControlThongTinChung = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.chkBillReceived = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.lblTotalSpecialConsumeTaxAmount = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel35 = new Infragistics.Win.Misc.UltraLabel();
            this.uGridControl = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.lblBillReceived = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalInwardAmount = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel32 = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalImportTaxAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalDiscountAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAllOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalDiscountAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalVATAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblChangeByForm = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAll = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel36 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDon)).BeginInit();
            this.grHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxHoaDon)).BeginInit();
            this.ultraGroupBoxHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).BeginInit();
            this.ultraTabPagePhieuXuatKho.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxPhieuXuatKho)).BeginInit();
            this.ultraGroupBoxPhieuXuatKho.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameTm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDTm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMContactNameTm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberAttachTm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMReasonPayTm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressTm)).BeginInit();
            this.ultraTabPagePhieuThu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDUnc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressUnc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameUnc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMAccountingObjectBankAccountDetailIDUnc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAccountingObjectBankNameUnc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.ultraGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetailIDUnc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankNameUnc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMReasonPayUnc)).BeginInit();
            this.ultraTabPageGiayBaoCo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDSck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressSck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameSck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMAccountingObjectBankAccountDetailIDSnk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAccountingObjectBankNameSck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetailIDSck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankNameSck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMReasonPaySck)).BeginInit();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).BeginInit();
            this.ultraGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteIssueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificationNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDStm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMContactNameStm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameStm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox8)).BeginInit();
            this.ultraGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetailIDStm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankNameStm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMReasonPayStm)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDTtd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressTtd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameTtd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMAccountingObjectBankAccountDetailIDTtd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAccountingObjectBankNameTtd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).BeginInit();
            this.ultraGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMReasonPayTtd)).BeginInit();
            this.palTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxTop)).BeginInit();
            this.ultraGroupBoxTop.SuspendLayout();
            this.palVouchersNoHd.SuspendLayout();
            this.palVouchersNoTm.SuspendLayout();
            this.palChung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optThanhToan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChonThanhToan)).BeginInit();
            this.palFill.SuspendLayout();
            this.palGrid.SuspendLayout();
            this.pnlUgrid.ClientArea.SuspendLayout();
            this.pnlUgrid.SuspendLayout();
            this.palThongTinChung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControlThongTinChung)).BeginInit();
            this.ultraTabControlThongTinChung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBillReceived)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageHoaDon
            // 
            this.ultraTabPageHoaDon.Controls.Add(this.grHoaDon);
            this.ultraTabPageHoaDon.Controls.Add(this.ultraGroupBoxHoaDon);
            this.ultraTabPageHoaDon.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageHoaDon.Name = "ultraTabPageHoaDon";
            this.ultraTabPageHoaDon.Size = new System.Drawing.Size(971, 177);
            // 
            // grHoaDon
            // 
            this.grHoaDon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grHoaDon.Controls.Add(this.txtMauSoHD);
            this.grHoaDon.Controls.Add(this.txtSoHD);
            this.grHoaDon.Controls.Add(this.txtKyHieuHD);
            this.grHoaDon.Controls.Add(this.ultraLabel37);
            this.grHoaDon.Controls.Add(this.ultraLabel38);
            this.grHoaDon.Controls.Add(this.ultraLabel39);
            this.grHoaDon.Controls.Add(this.ultraLabel40);
            this.grHoaDon.Controls.Add(this.dteNgayHD);
            this.grHoaDon.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grHoaDon.Location = new System.Drawing.Point(657, 0);
            this.grHoaDon.Name = "grHoaDon";
            this.grHoaDon.Size = new System.Drawing.Size(314, 178);
            this.grHoaDon.TabIndex = 30;
            this.grHoaDon.Text = "Hóa đơn";
            this.grHoaDon.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtMauSoHD
            // 
            this.txtMauSoHD.AutoSize = false;
            this.txtMauSoHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMauSoHD.Location = new System.Drawing.Point(81, 21);
            this.txtMauSoHD.Name = "txtMauSoHD";
            this.txtMauSoHD.Size = new System.Drawing.Size(228, 22);
            this.txtMauSoHD.TabIndex = 76;
            this.txtMauSoHD.TextChanged += new System.EventHandler(this.txtMauSoHD_TextChanged);
            // 
            // txtSoHD
            // 
            this.txtSoHD.AutoSize = false;
            this.txtSoHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHD.Location = new System.Drawing.Point(81, 72);
            this.txtSoHD.Name = "txtSoHD";
            this.txtSoHD.Size = new System.Drawing.Size(228, 22);
            this.txtSoHD.TabIndex = 18;
            this.txtSoHD.TextChanged += new System.EventHandler(this.txtSoHD_TextChanged);
            // 
            // txtKyHieuHD
            // 
            this.txtKyHieuHD.AutoSize = false;
            this.txtKyHieuHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuHD.Location = new System.Drawing.Point(81, 46);
            this.txtKyHieuHD.Name = "txtKyHieuHD";
            this.txtKyHieuHD.Size = new System.Drawing.Size(228, 22);
            this.txtKyHieuHD.TabIndex = 17;
            this.txtKyHieuHD.TextChanged += new System.EventHandler(this.txtKyHieuHD_TextChanged);
            // 
            // ultraLabel37
            // 
            this.ultraLabel37.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel37.Appearance = appearance1;
            this.ultraLabel37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel37.Location = new System.Drawing.Point(11, 99);
            this.ultraLabel37.Name = "ultraLabel37";
            this.ultraLabel37.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel37.TabIndex = 69;
            this.ultraLabel37.Text = "Ngày HĐ";
            // 
            // ultraLabel38
            // 
            this.ultraLabel38.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel38.Appearance = appearance2;
            this.ultraLabel38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel38.Location = new System.Drawing.Point(9, 73);
            this.ultraLabel38.Name = "ultraLabel38";
            this.ultraLabel38.Size = new System.Drawing.Size(40, 22);
            this.ultraLabel38.TabIndex = 68;
            this.ultraLabel38.Text = "Số HĐ";
            // 
            // ultraLabel39
            // 
            this.ultraLabel39.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel39.Appearance = appearance3;
            this.ultraLabel39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel39.Location = new System.Drawing.Point(9, 48);
            this.ultraLabel39.Name = "ultraLabel39";
            this.ultraLabel39.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel39.TabIndex = 67;
            this.ultraLabel39.Text = "Ký hiệu HĐ";
            // 
            // ultraLabel40
            // 
            this.ultraLabel40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel40.Appearance = appearance4;
            this.ultraLabel40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel40.Location = new System.Drawing.Point(11, 24);
            this.ultraLabel40.Name = "ultraLabel40";
            this.ultraLabel40.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel40.TabIndex = 66;
            this.ultraLabel40.Text = "Mẫu số HĐ";
            // 
            // dteNgayHD
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.dteNgayHD.Appearance = appearance5;
            this.dteNgayHD.AutoSize = false;
            this.dteNgayHD.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteNgayHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dteNgayHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteNgayHD.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteNgayHD.Location = new System.Drawing.Point(81, 99);
            this.dteNgayHD.MaskInput = "";
            this.dteNgayHD.Name = "dteNgayHD";
            this.dteNgayHD.Size = new System.Drawing.Size(100, 22);
            this.dteNgayHD.TabIndex = 19;
            this.dteNgayHD.Value = null;
            this.dteNgayHD.ValueChanged += new System.EventHandler(this.dteNgayHD_ValueChanged);
            // 
            // ultraGroupBoxHoaDon
            // 
            this.ultraGroupBoxHoaDon.Controls.Add(this.lblChungTuGoc);
            this.ultraGroupBoxHoaDon.Controls.Add(this.btnCongNo);
            this.ultraGroupBoxHoaDon.Controls.Add(this.btnPPOrder);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtAccountingObjectName);
            this.ultraGroupBoxHoaDon.Controls.Add(this.cbbAccountingObjectID);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtContactName);
            this.ultraGroupBoxHoaDon.Controls.Add(this.lblContactNameCt);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtOriginalNo);
            this.ultraGroupBoxHoaDon.Controls.Add(this.lblOriginalNoCt);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtReason);
            this.ultraGroupBoxHoaDon.Controls.Add(this.lblReasonCt);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtAccountingObjectAddress);
            this.ultraGroupBoxHoaDon.Controls.Add(this.lblAccountingObjectAddress);
            this.ultraGroupBoxHoaDon.Controls.Add(this.lblAccountingObjectID);
            appearance20.FontData.BoldAsString = "True";
            appearance20.FontData.SizeInPoints = 10F;
            this.ultraGroupBoxHoaDon.HeaderAppearance = appearance20;
            this.ultraGroupBoxHoaDon.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxHoaDon.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxHoaDon.Name = "ultraGroupBoxHoaDon";
            this.ultraGroupBoxHoaDon.Size = new System.Drawing.Size(660, 177);
            this.ultraGroupBoxHoaDon.TabIndex = 26;
            this.ultraGroupBoxHoaDon.Text = "Thông tin chung";
            this.ultraGroupBoxHoaDon.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // lblChungTuGoc
            // 
            this.lblChungTuGoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.lblChungTuGoc.Appearance = appearance6;
            appearance7.TextHAlignAsString = "Center";
            appearance7.TextVAlignAsString = "Middle";
            this.lblChungTuGoc.HotTrackAppearance = appearance7;
            this.lblChungTuGoc.Location = new System.Drawing.Point(568, 119);
            this.lblChungTuGoc.Name = "lblChungTuGoc";
            this.lblChungTuGoc.Size = new System.Drawing.Size(85, 22);
            this.lblChungTuGoc.TabIndex = 67;
            this.lblChungTuGoc.Text = "Chứng từ gốc";
            // 
            // btnCongNo
            // 
            this.btnCongNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCongNo.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnCongNo.Location = new System.Drawing.Point(568, 23);
            this.btnCongNo.Name = "btnCongNo";
            this.btnCongNo.Size = new System.Drawing.Size(85, 23);
            this.btnCongNo.TabIndex = 34;
            this.btnCongNo.Text = "Công nợ";
            this.btnCongNo.Click += new System.EventHandler(this.btnCongNo_Click);
            // 
            // btnPPOrder
            // 
            this.btnPPOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPPOrder.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnPPOrder.Location = new System.Drawing.Point(457, 23);
            this.btnPPOrder.Name = "btnPPOrder";
            this.btnPPOrder.Size = new System.Drawing.Size(105, 23);
            this.btnPPOrder.TabIndex = 33;
            this.btnPPOrder.Text = "Đơn mua hàng";
            this.btnPPOrder.Click += new System.EventHandler(this.btnPPOrder_Click);
            // 
            // txtAccountingObjectName
            // 
            this.txtAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.TextVAlignAsString = "Middle";
            this.txtAccountingObjectName.Appearance = appearance8;
            this.txtAccountingObjectName.AutoSize = false;
            this.txtAccountingObjectName.Location = new System.Drawing.Point(290, 23);
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(161, 22);
            this.txtAccountingObjectName.TabIndex = 32;
            this.txtAccountingObjectName.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // cbbAccountingObjectID
            // 
            appearance9.TextVAlignAsString = "Middle";
            this.cbbAccountingObjectID.Appearance = appearance9;
            this.cbbAccountingObjectID.AutoSize = false;
            appearance10.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton1.Appearance = appearance10;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectID.ButtonsRight.Add(editorButton1);
            this.cbbAccountingObjectID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectID.Location = new System.Drawing.Point(128, 23);
            this.cbbAccountingObjectID.Name = "cbbAccountingObjectID";
            this.cbbAccountingObjectID.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID.Size = new System.Drawing.Size(156, 22);
            this.cbbAccountingObjectID.TabIndex = 31;
            this.cbbAccountingObjectID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccountingObjectID_RowSelected);
            this.cbbAccountingObjectID.ValueChanged += new System.EventHandler(this.cbbAccountingObjectID_ValueChanged);
            // 
            // txtContactName
            // 
            this.txtContactName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance11.TextVAlignAsString = "Middle";
            this.txtContactName.Appearance = appearance11;
            this.txtContactName.AutoSize = false;
            this.txtContactName.Location = new System.Drawing.Point(128, 71);
            this.txtContactName.Name = "txtContactName";
            this.txtContactName.Size = new System.Drawing.Size(525, 22);
            this.txtContactName.TabIndex = 29;
            // 
            // lblContactNameCt
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.TextHAlignAsString = "Left";
            appearance12.TextVAlignAsString = "Middle";
            this.lblContactNameCt.Appearance = appearance12;
            this.lblContactNameCt.Location = new System.Drawing.Point(6, 71);
            this.lblContactNameCt.Name = "lblContactNameCt";
            this.lblContactNameCt.Size = new System.Drawing.Size(115, 21);
            this.lblContactNameCt.TabIndex = 28;
            this.lblContactNameCt.Text = "Họ tên người giao";
            // 
            // txtOriginalNo
            // 
            this.txtOriginalNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance13.TextVAlignAsString = "Middle";
            this.txtOriginalNo.Appearance = appearance13;
            this.txtOriginalNo.AutoSize = false;
            this.txtOriginalNo.Location = new System.Drawing.Point(128, 119);
            this.txtOriginalNo.Name = "txtOriginalNo";
            this.txtOriginalNo.Size = new System.Drawing.Size(434, 22);
            this.txtOriginalNo.TabIndex = 27;
            // 
            // lblOriginalNoCt
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextHAlignAsString = "Left";
            appearance14.TextVAlignAsString = "Middle";
            this.lblOriginalNoCt.Appearance = appearance14;
            this.lblOriginalNoCt.Location = new System.Drawing.Point(6, 119);
            this.lblOriginalNoCt.Name = "lblOriginalNoCt";
            this.lblOriginalNoCt.Size = new System.Drawing.Size(115, 22);
            this.lblOriginalNoCt.TabIndex = 26;
            this.lblOriginalNoCt.Text = "Kèm theo";
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance15.TextVAlignAsString = "Middle";
            this.txtReason.Appearance = appearance15;
            this.txtReason.AutoSize = false;
            this.txtReason.Location = new System.Drawing.Point(128, 95);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(525, 22);
            this.txtReason.TabIndex = 25;
            // 
            // lblReasonCt
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.TextHAlignAsString = "Left";
            appearance16.TextVAlignAsString = "Middle";
            this.lblReasonCt.Appearance = appearance16;
            this.lblReasonCt.Location = new System.Drawing.Point(6, 95);
            this.lblReasonCt.Name = "lblReasonCt";
            this.lblReasonCt.Size = new System.Drawing.Size(116, 22);
            this.lblReasonCt.TabIndex = 24;
            this.lblReasonCt.Text = "Diễn giải";
            // 
            // txtAccountingObjectAddress
            // 
            this.txtAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance17.TextVAlignAsString = "Middle";
            this.txtAccountingObjectAddress.Appearance = appearance17;
            this.txtAccountingObjectAddress.AutoSize = false;
            this.txtAccountingObjectAddress.Location = new System.Drawing.Point(128, 47);
            this.txtAccountingObjectAddress.Name = "txtAccountingObjectAddress";
            this.txtAccountingObjectAddress.Size = new System.Drawing.Size(525, 22);
            this.txtAccountingObjectAddress.TabIndex = 23;
            this.txtAccountingObjectAddress.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // lblAccountingObjectAddress
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextHAlignAsString = "Left";
            appearance18.TextVAlignAsString = "Middle";
            this.lblAccountingObjectAddress.Appearance = appearance18;
            this.lblAccountingObjectAddress.Location = new System.Drawing.Point(6, 47);
            this.lblAccountingObjectAddress.Name = "lblAccountingObjectAddress";
            this.lblAccountingObjectAddress.Size = new System.Drawing.Size(115, 22);
            this.lblAccountingObjectAddress.TabIndex = 22;
            this.lblAccountingObjectAddress.Text = "Địa chỉ";
            // 
            // lblAccountingObjectID
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.TextHAlignAsString = "Left";
            appearance19.TextVAlignAsString = "Middle";
            this.lblAccountingObjectID.Appearance = appearance19;
            this.lblAccountingObjectID.Location = new System.Drawing.Point(6, 23);
            this.lblAccountingObjectID.Name = "lblAccountingObjectID";
            this.lblAccountingObjectID.Size = new System.Drawing.Size(115, 22);
            this.lblAccountingObjectID.TabIndex = 0;
            this.lblAccountingObjectID.Text = "Nhà cung cấp (*)";
            // 
            // ultraTabPagePhieuXuatKho
            // 
            this.ultraTabPagePhieuXuatKho.Controls.Add(this.ultraGroupBoxPhieuXuatKho);
            this.ultraTabPagePhieuXuatKho.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPagePhieuXuatKho.Name = "ultraTabPagePhieuXuatKho";
            this.ultraTabPagePhieuXuatKho.Size = new System.Drawing.Size(971, 177);
            // 
            // ultraGroupBoxPhieuXuatKho
            // 
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.ultraLabel29);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.ultraButton1);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.ultraButton2);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtAccountingObjectNameTm);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.cbbAccountingObjectIDTm);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtMContactNameTm);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.ultraLabel1);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtNumberAttachTm);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.ultraLabel2);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtMReasonPayTm);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.ultraLabel3);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtAccountingObjectAddressTm);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.ultraLabel4);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.ultraLabel5);
            this.ultraGroupBoxPhieuXuatKho.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance35.FontData.BoldAsString = "True";
            appearance35.FontData.SizeInPoints = 10F;
            this.ultraGroupBoxPhieuXuatKho.HeaderAppearance = appearance35;
            this.ultraGroupBoxPhieuXuatKho.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxPhieuXuatKho.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxPhieuXuatKho.Name = "ultraGroupBoxPhieuXuatKho";
            this.ultraGroupBoxPhieuXuatKho.Size = new System.Drawing.Size(971, 177);
            this.ultraGroupBoxPhieuXuatKho.TabIndex = 27;
            this.ultraGroupBoxPhieuXuatKho.Text = "Thông tin chung";
            this.ultraGroupBoxPhieuXuatKho.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel29
            // 
            this.ultraLabel29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance21.BackColor = System.Drawing.Color.Transparent;
            appearance21.TextHAlignAsString = "Center";
            appearance21.TextVAlignAsString = "Middle";
            this.ultraLabel29.Appearance = appearance21;
            appearance22.TextHAlignAsString = "Center";
            appearance22.TextVAlignAsString = "Middle";
            this.ultraLabel29.HotTrackAppearance = appearance22;
            this.ultraLabel29.Location = new System.Drawing.Point(879, 119);
            this.ultraLabel29.Name = "ultraLabel29";
            this.ultraLabel29.Size = new System.Drawing.Size(85, 22);
            this.ultraLabel29.TabIndex = 68;
            this.ultraLabel29.Text = "Chứng từ gốc";
            // 
            // ultraButton1
            // 
            this.ultraButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton1.Location = new System.Drawing.Point(879, 23);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(85, 23);
            this.ultraButton1.TabIndex = 47;
            this.ultraButton1.Text = "Công nợ";
            this.ultraButton1.Click += new System.EventHandler(this.btnCongNo_Click);
            // 
            // ultraButton2
            // 
            this.ultraButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton2.Location = new System.Drawing.Point(768, 23);
            this.ultraButton2.Name = "ultraButton2";
            this.ultraButton2.Size = new System.Drawing.Size(105, 23);
            this.ultraButton2.TabIndex = 46;
            this.ultraButton2.Text = "Đơn mua hàng";
            this.ultraButton2.Click += new System.EventHandler(this.btnPPOrder_Click);
            // 
            // txtAccountingObjectNameTm
            // 
            this.txtAccountingObjectNameTm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance23.TextVAlignAsString = "Middle";
            this.txtAccountingObjectNameTm.Appearance = appearance23;
            this.txtAccountingObjectNameTm.AutoSize = false;
            this.txtAccountingObjectNameTm.Location = new System.Drawing.Point(294, 23);
            this.txtAccountingObjectNameTm.Name = "txtAccountingObjectNameTm";
            this.txtAccountingObjectNameTm.Size = new System.Drawing.Size(468, 22);
            this.txtAccountingObjectNameTm.TabIndex = 45;
            this.txtAccountingObjectNameTm.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // cbbAccountingObjectIDTm
            // 
            appearance24.TextVAlignAsString = "Middle";
            this.cbbAccountingObjectIDTm.Appearance = appearance24;
            this.cbbAccountingObjectIDTm.AutoSize = false;
            appearance25.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton2.Appearance = appearance25;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectIDTm.ButtonsRight.Add(editorButton2);
            this.cbbAccountingObjectIDTm.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectIDTm.Location = new System.Drawing.Point(136, 23);
            this.cbbAccountingObjectIDTm.Name = "cbbAccountingObjectIDTm";
            this.cbbAccountingObjectIDTm.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectIDTm.Size = new System.Drawing.Size(152, 22);
            this.cbbAccountingObjectIDTm.TabIndex = 44;
            this.cbbAccountingObjectIDTm.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccountingObjectIDTm_RowSelected);
            this.cbbAccountingObjectIDTm.ValueChanged += new System.EventHandler(this.cbbAccountingObjectIDTm_ValueChanged);
            // 
            // txtMContactNameTm
            // 
            this.txtMContactNameTm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance26.TextVAlignAsString = "Middle";
            this.txtMContactNameTm.Appearance = appearance26;
            this.txtMContactNameTm.AutoSize = false;
            this.txtMContactNameTm.Location = new System.Drawing.Point(136, 71);
            this.txtMContactNameTm.Name = "txtMContactNameTm";
            this.txtMContactNameTm.Size = new System.Drawing.Size(828, 22);
            this.txtMContactNameTm.TabIndex = 43;
            // 
            // ultraLabel1
            // 
            appearance27.BackColor = System.Drawing.Color.Transparent;
            appearance27.TextHAlignAsString = "Left";
            appearance27.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance27;
            this.ultraLabel1.Location = new System.Drawing.Point(6, 73);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(124, 19);
            this.ultraLabel1.TabIndex = 42;
            this.ultraLabel1.Text = "Họ tên người nhận tiền";
            // 
            // txtNumberAttachTm
            // 
            this.txtNumberAttachTm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance28.TextVAlignAsString = "Middle";
            this.txtNumberAttachTm.Appearance = appearance28;
            this.txtNumberAttachTm.AutoSize = false;
            this.txtNumberAttachTm.Location = new System.Drawing.Point(136, 119);
            this.txtNumberAttachTm.Name = "txtNumberAttachTm";
            this.txtNumberAttachTm.Size = new System.Drawing.Size(737, 22);
            this.txtNumberAttachTm.TabIndex = 41;
            // 
            // ultraLabel2
            // 
            appearance29.BackColor = System.Drawing.Color.Transparent;
            appearance29.TextHAlignAsString = "Left";
            appearance29.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance29;
            this.ultraLabel2.Location = new System.Drawing.Point(6, 120);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(124, 19);
            this.ultraLabel2.TabIndex = 40;
            this.ultraLabel2.Text = "Kèm theo";
            // 
            // txtMReasonPayTm
            // 
            this.txtMReasonPayTm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance30.TextVAlignAsString = "Middle";
            this.txtMReasonPayTm.Appearance = appearance30;
            this.txtMReasonPayTm.AutoSize = false;
            this.txtMReasonPayTm.Location = new System.Drawing.Point(136, 95);
            this.txtMReasonPayTm.Name = "txtMReasonPayTm";
            this.txtMReasonPayTm.Size = new System.Drawing.Size(828, 22);
            this.txtMReasonPayTm.TabIndex = 39;
            // 
            // ultraLabel3
            // 
            appearance31.BackColor = System.Drawing.Color.Transparent;
            appearance31.TextHAlignAsString = "Left";
            appearance31.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance31;
            this.ultraLabel3.Location = new System.Drawing.Point(6, 95);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(125, 19);
            this.ultraLabel3.TabIndex = 38;
            this.ultraLabel3.Text = "Lý do chi";
            // 
            // txtAccountingObjectAddressTm
            // 
            this.txtAccountingObjectAddressTm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance32.TextVAlignAsString = "Middle";
            this.txtAccountingObjectAddressTm.Appearance = appearance32;
            this.txtAccountingObjectAddressTm.AutoSize = false;
            this.txtAccountingObjectAddressTm.Location = new System.Drawing.Point(136, 47);
            this.txtAccountingObjectAddressTm.Name = "txtAccountingObjectAddressTm";
            this.txtAccountingObjectAddressTm.Size = new System.Drawing.Size(828, 22);
            this.txtAccountingObjectAddressTm.TabIndex = 37;
            this.txtAccountingObjectAddressTm.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // ultraLabel4
            // 
            appearance33.BackColor = System.Drawing.Color.Transparent;
            appearance33.TextHAlignAsString = "Left";
            appearance33.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance33;
            this.ultraLabel4.Location = new System.Drawing.Point(6, 49);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(124, 19);
            this.ultraLabel4.TabIndex = 36;
            this.ultraLabel4.Text = "Địa chỉ";
            // 
            // ultraLabel5
            // 
            appearance34.BackColor = System.Drawing.Color.Transparent;
            appearance34.TextHAlignAsString = "Left";
            appearance34.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance34;
            this.ultraLabel5.Location = new System.Drawing.Point(6, 25);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(124, 19);
            this.ultraLabel5.TabIndex = 35;
            this.ultraLabel5.Text = "Nhà cung cấp (*)";
            // 
            // ultraTabPagePhieuThu
            // 
            this.ultraTabPagePhieuThu.Controls.Add(this.ultraGroupBox4);
            this.ultraTabPagePhieuThu.Controls.Add(this.ultraGroupBox5);
            this.ultraTabPagePhieuThu.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPagePhieuThu.Name = "ultraTabPagePhieuThu";
            this.ultraTabPagePhieuThu.Size = new System.Drawing.Size(971, 177);
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox4.Controls.Add(this.cbbAccountingObjectIDUnc);
            this.ultraGroupBox4.Controls.Add(this.txtAccountingObjectAddressUnc);
            this.ultraGroupBox4.Controls.Add(this.txtAccountingObjectNameUnc);
            this.ultraGroupBox4.Controls.Add(this.cbbMAccountingObjectBankAccountDetailIDUnc);
            this.ultraGroupBox4.Controls.Add(this.txtMAccountingObjectBankNameUnc);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance45.FontData.BoldAsString = "True";
            appearance45.FontData.SizeInPoints = 10F;
            this.ultraGroupBox4.HeaderAppearance = appearance45;
            this.ultraGroupBox4.Location = new System.Drawing.Point(0, 77);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(971, 100);
            this.ultraGroupBox4.TabIndex = 66;
            this.ultraGroupBox4.Text = "Đơn vị nhận tiền";
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel10
            // 
            appearance36.BackColor = System.Drawing.Color.Transparent;
            appearance36.TextHAlignAsString = "Left";
            appearance36.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance36;
            this.ultraLabel10.Location = new System.Drawing.Point(6, 73);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(82, 19);
            this.ultraLabel10.TabIndex = 45;
            this.ultraLabel10.Text = "Tài khoản";
            // 
            // ultraLabel9
            // 
            appearance37.BackColor = System.Drawing.Color.Transparent;
            appearance37.TextHAlignAsString = "Left";
            appearance37.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance37;
            this.ultraLabel9.Location = new System.Drawing.Point(6, 23);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(82, 19);
            this.ultraLabel9.TabIndex = 44;
            this.ultraLabel9.Text = "Nhà cung cấp";
            // 
            // ultraLabel8
            // 
            appearance38.BackColor = System.Drawing.Color.Transparent;
            appearance38.TextHAlignAsString = "Left";
            appearance38.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance38;
            this.ultraLabel8.Location = new System.Drawing.Point(6, 48);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(82, 19);
            this.ultraLabel8.TabIndex = 43;
            this.ultraLabel8.Text = "Địa chỉ";
            // 
            // cbbAccountingObjectIDUnc
            // 
            appearance39.TextVAlignAsString = "Middle";
            this.cbbAccountingObjectIDUnc.Appearance = appearance39;
            this.cbbAccountingObjectIDUnc.AutoSize = false;
            appearance40.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton3.Appearance = appearance40;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectIDUnc.ButtonsRight.Add(editorButton3);
            this.cbbAccountingObjectIDUnc.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectIDUnc.LimitToList = true;
            this.cbbAccountingObjectIDUnc.Location = new System.Drawing.Point(94, 23);
            this.cbbAccountingObjectIDUnc.Name = "cbbAccountingObjectIDUnc";
            this.cbbAccountingObjectIDUnc.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectIDUnc.Size = new System.Drawing.Size(158, 22);
            this.cbbAccountingObjectIDUnc.TabIndex = 42;
            this.cbbAccountingObjectIDUnc.ValueChanged += new System.EventHandler(this.cbbAccountingObjectIDUnc_ValueChanged);
            // 
            // txtAccountingObjectAddressUnc
            // 
            this.txtAccountingObjectAddressUnc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance41.TextVAlignAsString = "Middle";
            this.txtAccountingObjectAddressUnc.Appearance = appearance41;
            this.txtAccountingObjectAddressUnc.AutoSize = false;
            this.txtAccountingObjectAddressUnc.Location = new System.Drawing.Point(94, 47);
            this.txtAccountingObjectAddressUnc.Name = "txtAccountingObjectAddressUnc";
            this.txtAccountingObjectAddressUnc.Size = new System.Drawing.Size(870, 22);
            this.txtAccountingObjectAddressUnc.TabIndex = 23;
            this.txtAccountingObjectAddressUnc.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // txtAccountingObjectNameUnc
            // 
            this.txtAccountingObjectNameUnc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance42.TextVAlignAsString = "Middle";
            this.txtAccountingObjectNameUnc.Appearance = appearance42;
            this.txtAccountingObjectNameUnc.AutoSize = false;
            this.txtAccountingObjectNameUnc.Location = new System.Drawing.Point(258, 23);
            this.txtAccountingObjectNameUnc.Name = "txtAccountingObjectNameUnc";
            this.txtAccountingObjectNameUnc.Size = new System.Drawing.Size(706, 22);
            this.txtAccountingObjectNameUnc.TabIndex = 25;
            this.txtAccountingObjectNameUnc.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // cbbMAccountingObjectBankAccountDetailIDUnc
            // 
            appearance43.TextVAlignAsString = "Middle";
            this.cbbMAccountingObjectBankAccountDetailIDUnc.Appearance = appearance43;
            this.cbbMAccountingObjectBankAccountDetailIDUnc.AutoSize = false;
            this.cbbMAccountingObjectBankAccountDetailIDUnc.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbMAccountingObjectBankAccountDetailIDUnc.Location = new System.Drawing.Point(94, 71);
            this.cbbMAccountingObjectBankAccountDetailIDUnc.Name = "cbbMAccountingObjectBankAccountDetailIDUnc";
            this.cbbMAccountingObjectBankAccountDetailIDUnc.NullText = "<chọn dữ liệu>";
            this.cbbMAccountingObjectBankAccountDetailIDUnc.Size = new System.Drawing.Size(158, 22);
            this.cbbMAccountingObjectBankAccountDetailIDUnc.TabIndex = 31;
            // 
            // txtMAccountingObjectBankNameUnc
            // 
            this.txtMAccountingObjectBankNameUnc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance44.TextVAlignAsString = "Middle";
            this.txtMAccountingObjectBankNameUnc.Appearance = appearance44;
            this.txtMAccountingObjectBankNameUnc.AutoSize = false;
            this.txtMAccountingObjectBankNameUnc.Location = new System.Drawing.Point(258, 71);
            this.txtMAccountingObjectBankNameUnc.Name = "txtMAccountingObjectBankNameUnc";
            this.txtMAccountingObjectBankNameUnc.Size = new System.Drawing.Size(706, 22);
            this.txtMAccountingObjectBankNameUnc.TabIndex = 23;
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.Controls.Add(this.ultraButton3);
            this.ultraGroupBox5.Controls.Add(this.ultraButton4);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox5.Controls.Add(this.cbbBankAccountDetailIDUnc);
            this.ultraGroupBox5.Controls.Add(this.txtBankNameUnc);
            this.ultraGroupBox5.Controls.Add(this.txtMReasonPayUnc);
            this.ultraGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            appearance51.FontData.BoldAsString = "True";
            appearance51.FontData.SizeInPoints = 10F;
            this.ultraGroupBox5.HeaderAppearance = appearance51;
            this.ultraGroupBox5.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox5.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(971, 77);
            this.ultraGroupBox5.TabIndex = 65;
            this.ultraGroupBox5.Text = "Đơn vị trả tiền";
            this.ultraGroupBox5.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraButton3
            // 
            this.ultraButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton3.Location = new System.Drawing.Point(879, 23);
            this.ultraButton3.Name = "ultraButton3";
            this.ultraButton3.Size = new System.Drawing.Size(85, 23);
            this.ultraButton3.TabIndex = 39;
            this.ultraButton3.Text = "Công nợ";
            this.ultraButton3.Click += new System.EventHandler(this.btnCongNo_Click);
            // 
            // ultraButton4
            // 
            this.ultraButton4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton4.Location = new System.Drawing.Point(768, 23);
            this.ultraButton4.Name = "ultraButton4";
            this.ultraButton4.Size = new System.Drawing.Size(105, 23);
            this.ultraButton4.TabIndex = 38;
            this.ultraButton4.Text = "Đơn mua hàng";
            this.ultraButton4.Click += new System.EventHandler(this.btnPPOrder_Click);
            // 
            // ultraLabel7
            // 
            appearance46.BackColor = System.Drawing.Color.Transparent;
            appearance46.TextHAlignAsString = "Left";
            appearance46.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance46;
            this.ultraLabel7.Location = new System.Drawing.Point(6, 48);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(82, 19);
            this.ultraLabel7.TabIndex = 37;
            this.ultraLabel7.Text = "Nội dung TT";
            // 
            // ultraLabel6
            // 
            appearance47.BackColor = System.Drawing.Color.Transparent;
            appearance47.TextHAlignAsString = "Left";
            appearance47.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance47;
            this.ultraLabel6.Location = new System.Drawing.Point(6, 25);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(82, 19);
            this.ultraLabel6.TabIndex = 36;
            this.ultraLabel6.Text = "Tài khoản";
            // 
            // cbbBankAccountDetailIDUnc
            // 
            appearance48.TextVAlignAsString = "Middle";
            this.cbbBankAccountDetailIDUnc.Appearance = appearance48;
            this.cbbBankAccountDetailIDUnc.AutoSize = false;
            this.cbbBankAccountDetailIDUnc.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbBankAccountDetailIDUnc.Location = new System.Drawing.Point(94, 23);
            this.cbbBankAccountDetailIDUnc.Name = "cbbBankAccountDetailIDUnc";
            this.cbbBankAccountDetailIDUnc.NullText = "<chọn dữ liệu>";
            this.cbbBankAccountDetailIDUnc.Size = new System.Drawing.Size(158, 22);
            this.cbbBankAccountDetailIDUnc.TabIndex = 31;
            // 
            // txtBankNameUnc
            // 
            this.txtBankNameUnc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance49.TextVAlignAsString = "Middle";
            this.txtBankNameUnc.Appearance = appearance49;
            this.txtBankNameUnc.AutoSize = false;
            this.txtBankNameUnc.Location = new System.Drawing.Point(258, 23);
            this.txtBankNameUnc.Name = "txtBankNameUnc";
            this.txtBankNameUnc.Size = new System.Drawing.Size(504, 22);
            this.txtBankNameUnc.TabIndex = 23;
            // 
            // txtMReasonPayUnc
            // 
            this.txtMReasonPayUnc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance50.TextVAlignAsString = "Middle";
            this.txtMReasonPayUnc.Appearance = appearance50;
            this.txtMReasonPayUnc.AutoSize = false;
            this.txtMReasonPayUnc.Location = new System.Drawing.Point(94, 47);
            this.txtMReasonPayUnc.Name = "txtMReasonPayUnc";
            this.txtMReasonPayUnc.Size = new System.Drawing.Size(870, 22);
            this.txtMReasonPayUnc.TabIndex = 23;
            // 
            // ultraTabPageGiayBaoCo
            // 
            this.ultraTabPageGiayBaoCo.Controls.Add(this.ultraGroupBox1);
            this.ultraTabPageGiayBaoCo.Controls.Add(this.ultraGroupBox2);
            this.ultraTabPageGiayBaoCo.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageGiayBaoCo.Name = "ultraTabPageGiayBaoCo";
            this.ultraTabPageGiayBaoCo.Size = new System.Drawing.Size(971, 177);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.ultraLabel11);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel12);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel13);
            this.ultraGroupBox1.Controls.Add(this.cbbAccountingObjectIDSck);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectAddressSck);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectNameSck);
            this.ultraGroupBox1.Controls.Add(this.cbbMAccountingObjectBankAccountDetailIDSnk);
            this.ultraGroupBox1.Controls.Add(this.txtMAccountingObjectBankNameSck);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance61.FontData.BoldAsString = "True";
            appearance61.FontData.SizeInPoints = 10F;
            this.ultraGroupBox1.HeaderAppearance = appearance61;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 77);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(971, 100);
            this.ultraGroupBox1.TabIndex = 68;
            this.ultraGroupBox1.Text = "Đơn vị nhận tiền";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel11
            // 
            appearance52.BackColor = System.Drawing.Color.Transparent;
            appearance52.TextHAlignAsString = "Left";
            appearance52.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance52;
            this.ultraLabel11.Location = new System.Drawing.Point(6, 73);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(82, 19);
            this.ultraLabel11.TabIndex = 45;
            this.ultraLabel11.Text = "Tài khoản";
            // 
            // ultraLabel12
            // 
            appearance53.BackColor = System.Drawing.Color.Transparent;
            appearance53.TextHAlignAsString = "Left";
            appearance53.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance53;
            this.ultraLabel12.Location = new System.Drawing.Point(6, 23);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(82, 19);
            this.ultraLabel12.TabIndex = 44;
            this.ultraLabel12.Text = "Nhà cung cấp";
            // 
            // ultraLabel13
            // 
            appearance54.BackColor = System.Drawing.Color.Transparent;
            appearance54.TextHAlignAsString = "Left";
            appearance54.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance54;
            this.ultraLabel13.Location = new System.Drawing.Point(6, 48);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(82, 19);
            this.ultraLabel13.TabIndex = 43;
            this.ultraLabel13.Text = "Địa chỉ";
            // 
            // cbbAccountingObjectIDSck
            // 
            appearance55.TextVAlignAsString = "Middle";
            this.cbbAccountingObjectIDSck.Appearance = appearance55;
            this.cbbAccountingObjectIDSck.AutoSize = false;
            appearance56.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton4.Appearance = appearance56;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectIDSck.ButtonsRight.Add(editorButton4);
            this.cbbAccountingObjectIDSck.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectIDSck.LimitToList = true;
            this.cbbAccountingObjectIDSck.Location = new System.Drawing.Point(94, 23);
            this.cbbAccountingObjectIDSck.Name = "cbbAccountingObjectIDSck";
            this.cbbAccountingObjectIDSck.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectIDSck.Size = new System.Drawing.Size(158, 22);
            this.cbbAccountingObjectIDSck.TabIndex = 42;
            this.cbbAccountingObjectIDSck.ValueChanged += new System.EventHandler(this.cbbAccountingObjectIDSck_ValueChanged);
            // 
            // txtAccountingObjectAddressSck
            // 
            this.txtAccountingObjectAddressSck.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance57.TextVAlignAsString = "Middle";
            this.txtAccountingObjectAddressSck.Appearance = appearance57;
            this.txtAccountingObjectAddressSck.AutoSize = false;
            this.txtAccountingObjectAddressSck.Location = new System.Drawing.Point(94, 47);
            this.txtAccountingObjectAddressSck.Name = "txtAccountingObjectAddressSck";
            this.txtAccountingObjectAddressSck.Size = new System.Drawing.Size(870, 22);
            this.txtAccountingObjectAddressSck.TabIndex = 23;
            this.txtAccountingObjectAddressSck.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // txtAccountingObjectNameSck
            // 
            this.txtAccountingObjectNameSck.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance58.TextVAlignAsString = "Middle";
            this.txtAccountingObjectNameSck.Appearance = appearance58;
            this.txtAccountingObjectNameSck.AutoSize = false;
            this.txtAccountingObjectNameSck.Location = new System.Drawing.Point(258, 23);
            this.txtAccountingObjectNameSck.Name = "txtAccountingObjectNameSck";
            this.txtAccountingObjectNameSck.Size = new System.Drawing.Size(706, 22);
            this.txtAccountingObjectNameSck.TabIndex = 25;
            this.txtAccountingObjectNameSck.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // cbbMAccountingObjectBankAccountDetailIDSnk
            // 
            appearance59.TextVAlignAsString = "Middle";
            this.cbbMAccountingObjectBankAccountDetailIDSnk.Appearance = appearance59;
            this.cbbMAccountingObjectBankAccountDetailIDSnk.AutoSize = false;
            this.cbbMAccountingObjectBankAccountDetailIDSnk.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbMAccountingObjectBankAccountDetailIDSnk.Location = new System.Drawing.Point(94, 71);
            this.cbbMAccountingObjectBankAccountDetailIDSnk.Name = "cbbMAccountingObjectBankAccountDetailIDSnk";
            this.cbbMAccountingObjectBankAccountDetailIDSnk.NullText = "<chọn dữ liệu>";
            this.cbbMAccountingObjectBankAccountDetailIDSnk.Size = new System.Drawing.Size(158, 22);
            this.cbbMAccountingObjectBankAccountDetailIDSnk.TabIndex = 31;
            // 
            // txtMAccountingObjectBankNameSck
            // 
            this.txtMAccountingObjectBankNameSck.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance60.TextVAlignAsString = "Middle";
            this.txtMAccountingObjectBankNameSck.Appearance = appearance60;
            this.txtMAccountingObjectBankNameSck.AutoSize = false;
            this.txtMAccountingObjectBankNameSck.Location = new System.Drawing.Point(258, 71);
            this.txtMAccountingObjectBankNameSck.Name = "txtMAccountingObjectBankNameSck";
            this.txtMAccountingObjectBankNameSck.Size = new System.Drawing.Size(706, 22);
            this.txtMAccountingObjectBankNameSck.TabIndex = 23;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.ultraButton5);
            this.ultraGroupBox2.Controls.Add(this.ultraButton6);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel14);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel15);
            this.ultraGroupBox2.Controls.Add(this.cbbBankAccountDetailIDSck);
            this.ultraGroupBox2.Controls.Add(this.txtBankNameSck);
            this.ultraGroupBox2.Controls.Add(this.txtMReasonPaySck);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            appearance67.FontData.BoldAsString = "True";
            appearance67.FontData.SizeInPoints = 10F;
            this.ultraGroupBox2.HeaderAppearance = appearance67;
            this.ultraGroupBox2.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(971, 77);
            this.ultraGroupBox2.TabIndex = 67;
            this.ultraGroupBox2.Text = "Đơn vị trả tiền";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraButton5
            // 
            this.ultraButton5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton5.Location = new System.Drawing.Point(879, 23);
            this.ultraButton5.Name = "ultraButton5";
            this.ultraButton5.Size = new System.Drawing.Size(85, 23);
            this.ultraButton5.TabIndex = 39;
            this.ultraButton5.Text = "Công nợ";
            this.ultraButton5.Click += new System.EventHandler(this.btnCongNo_Click);
            // 
            // ultraButton6
            // 
            this.ultraButton6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton6.Location = new System.Drawing.Point(768, 23);
            this.ultraButton6.Name = "ultraButton6";
            this.ultraButton6.Size = new System.Drawing.Size(105, 23);
            this.ultraButton6.TabIndex = 38;
            this.ultraButton6.Text = "Đơn mua hàng";
            this.ultraButton6.Click += new System.EventHandler(this.btnPPOrder_Click);
            // 
            // ultraLabel14
            // 
            appearance62.BackColor = System.Drawing.Color.Transparent;
            appearance62.TextHAlignAsString = "Left";
            appearance62.TextVAlignAsString = "Middle";
            this.ultraLabel14.Appearance = appearance62;
            this.ultraLabel14.Location = new System.Drawing.Point(6, 48);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(82, 19);
            this.ultraLabel14.TabIndex = 37;
            this.ultraLabel14.Text = "Nội dung TT";
            // 
            // ultraLabel15
            // 
            appearance63.BackColor = System.Drawing.Color.Transparent;
            appearance63.TextHAlignAsString = "Left";
            appearance63.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance63;
            this.ultraLabel15.Location = new System.Drawing.Point(6, 25);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(82, 19);
            this.ultraLabel15.TabIndex = 36;
            this.ultraLabel15.Text = "Tài khoản";
            // 
            // cbbBankAccountDetailIDSck
            // 
            appearance64.TextVAlignAsString = "Middle";
            this.cbbBankAccountDetailIDSck.Appearance = appearance64;
            this.cbbBankAccountDetailIDSck.AutoSize = false;
            this.cbbBankAccountDetailIDSck.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbBankAccountDetailIDSck.Location = new System.Drawing.Point(94, 23);
            this.cbbBankAccountDetailIDSck.Name = "cbbBankAccountDetailIDSck";
            this.cbbBankAccountDetailIDSck.NullText = "<chọn dữ liệu>";
            this.cbbBankAccountDetailIDSck.Size = new System.Drawing.Size(158, 22);
            this.cbbBankAccountDetailIDSck.TabIndex = 31;
            // 
            // txtBankNameSck
            // 
            this.txtBankNameSck.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance65.TextVAlignAsString = "Middle";
            this.txtBankNameSck.Appearance = appearance65;
            this.txtBankNameSck.AutoSize = false;
            this.txtBankNameSck.Location = new System.Drawing.Point(258, 23);
            this.txtBankNameSck.Name = "txtBankNameSck";
            this.txtBankNameSck.Size = new System.Drawing.Size(504, 22);
            this.txtBankNameSck.TabIndex = 23;
            // 
            // txtMReasonPaySck
            // 
            this.txtMReasonPaySck.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance66.TextVAlignAsString = "Middle";
            this.txtMReasonPaySck.Appearance = appearance66;
            this.txtMReasonPaySck.AutoSize = false;
            this.txtMReasonPaySck.Location = new System.Drawing.Point(94, 47);
            this.txtMReasonPaySck.Name = "txtMReasonPaySck";
            this.txtMReasonPaySck.Size = new System.Drawing.Size(870, 22);
            this.txtMReasonPaySck.TabIndex = 23;
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox7);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox8);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(971, 177);
            // 
            // ultraGroupBox7
            // 
            this.ultraGroupBox7.Controls.Add(this.dteIssueDate);
            this.ultraGroupBox7.Controls.Add(this.ultraLabel31);
            this.ultraGroupBox7.Controls.Add(this.ultraLabel30);
            this.ultraGroupBox7.Controls.Add(this.txtIdentificationNo);
            this.ultraGroupBox7.Controls.Add(this.ultraLabel24);
            this.ultraGroupBox7.Controls.Add(this.ultraLabel25);
            this.ultraGroupBox7.Controls.Add(this.ultraLabel26);
            this.ultraGroupBox7.Controls.Add(this.cbbAccountingObjectIDStm);
            this.ultraGroupBox7.Controls.Add(this.txtMContactNameStm);
            this.ultraGroupBox7.Controls.Add(this.txtAccountingObjectNameStm);
            this.ultraGroupBox7.Controls.Add(this.txtIssueBy);
            this.ultraGroupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance80.FontData.BoldAsString = "True";
            appearance80.FontData.SizeInPoints = 10F;
            this.ultraGroupBox7.HeaderAppearance = appearance80;
            this.ultraGroupBox7.Location = new System.Drawing.Point(0, 77);
            this.ultraGroupBox7.Name = "ultraGroupBox7";
            this.ultraGroupBox7.Size = new System.Drawing.Size(971, 100);
            this.ultraGroupBox7.TabIndex = 70;
            this.ultraGroupBox7.Text = "Đơn vị nhận tiền";
            this.ultraGroupBox7.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // dteIssueDate
            // 
            appearance68.TextHAlignAsString = "Center";
            appearance68.TextVAlignAsString = "Middle";
            this.dteIssueDate.Appearance = appearance68;
            this.dteIssueDate.AutoSize = false;
            this.dteIssueDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteIssueDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.dteIssueDate.FormatProvider = new System.Globalization.CultureInfo("");
            this.dteIssueDate.Location = new System.Drawing.Point(368, 71);
            this.dteIssueDate.MaskInput = "";
            this.dteIssueDate.Name = "dteIssueDate";
            this.dteIssueDate.Size = new System.Drawing.Size(98, 22);
            this.dteIssueDate.TabIndex = 86;
            this.dteIssueDate.Value = null;
            // 
            // ultraLabel31
            // 
            appearance69.BackColor = System.Drawing.Color.Transparent;
            appearance69.TextHAlignAsString = "Left";
            appearance69.TextVAlignAsString = "Middle";
            this.ultraLabel31.Appearance = appearance69;
            this.ultraLabel31.Location = new System.Drawing.Point(495, 72);
            this.ultraLabel31.Name = "ultraLabel31";
            this.ultraLabel31.Size = new System.Drawing.Size(43, 19);
            this.ultraLabel31.TabIndex = 85;
            this.ultraLabel31.Text = "Nơi cấp";
            // 
            // ultraLabel30
            // 
            appearance70.BackColor = System.Drawing.Color.Transparent;
            appearance70.TextHAlignAsString = "Left";
            appearance70.TextVAlignAsString = "Middle";
            this.ultraLabel30.Appearance = appearance70;
            this.ultraLabel30.Location = new System.Drawing.Point(290, 72);
            this.ultraLabel30.Name = "ultraLabel30";
            this.ultraLabel30.Size = new System.Drawing.Size(57, 19);
            this.ultraLabel30.TabIndex = 84;
            this.ultraLabel30.Text = "Ngày cấp";
            // 
            // txtIdentificationNo
            // 
            appearance71.TextVAlignAsString = "Middle";
            this.txtIdentificationNo.Appearance = appearance71;
            this.txtIdentificationNo.AutoSize = false;
            this.txtIdentificationNo.Location = new System.Drawing.Point(127, 71);
            this.txtIdentificationNo.Name = "txtIdentificationNo";
            this.txtIdentificationNo.Size = new System.Drawing.Size(157, 22);
            this.txtIdentificationNo.TabIndex = 83;
            // 
            // ultraLabel24
            // 
            appearance72.BackColor = System.Drawing.Color.Transparent;
            appearance72.TextHAlignAsString = "Left";
            appearance72.TextVAlignAsString = "Middle";
            this.ultraLabel24.Appearance = appearance72;
            this.ultraLabel24.Location = new System.Drawing.Point(6, 71);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(115, 19);
            this.ultraLabel24.TabIndex = 45;
            this.ultraLabel24.Text = "Số CMND";
            // 
            // ultraLabel25
            // 
            appearance73.BackColor = System.Drawing.Color.Transparent;
            appearance73.TextHAlignAsString = "Left";
            appearance73.TextVAlignAsString = "Middle";
            this.ultraLabel25.Appearance = appearance73;
            this.ultraLabel25.Location = new System.Drawing.Point(6, 23);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(115, 19);
            this.ultraLabel25.TabIndex = 44;
            this.ultraLabel25.Text = "Nhà cung cấp";
            // 
            // ultraLabel26
            // 
            appearance74.BackColor = System.Drawing.Color.Transparent;
            appearance74.TextHAlignAsString = "Left";
            appearance74.TextVAlignAsString = "Middle";
            this.ultraLabel26.Appearance = appearance74;
            this.ultraLabel26.Location = new System.Drawing.Point(6, 48);
            this.ultraLabel26.Name = "ultraLabel26";
            this.ultraLabel26.Size = new System.Drawing.Size(115, 19);
            this.ultraLabel26.TabIndex = 43;
            this.ultraLabel26.Text = "Họ tên người lĩnh tiền";
            // 
            // cbbAccountingObjectIDStm
            // 
            appearance75.TextVAlignAsString = "Middle";
            this.cbbAccountingObjectIDStm.Appearance = appearance75;
            this.cbbAccountingObjectIDStm.AutoSize = false;
            appearance76.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton5.Appearance = appearance76;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectIDStm.ButtonsRight.Add(editorButton5);
            this.cbbAccountingObjectIDStm.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectIDStm.LimitToList = true;
            this.cbbAccountingObjectIDStm.Location = new System.Drawing.Point(127, 23);
            this.cbbAccountingObjectIDStm.Name = "cbbAccountingObjectIDStm";
            this.cbbAccountingObjectIDStm.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectIDStm.Size = new System.Drawing.Size(158, 22);
            this.cbbAccountingObjectIDStm.TabIndex = 42;
            this.cbbAccountingObjectIDStm.ValueChanged += new System.EventHandler(this.cbbAccountingObjectIDStm_ValueChanged);
            // 
            // txtMContactNameStm
            // 
            this.txtMContactNameStm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance77.TextVAlignAsString = "Middle";
            this.txtMContactNameStm.Appearance = appearance77;
            this.txtMContactNameStm.AutoSize = false;
            this.txtMContactNameStm.Location = new System.Drawing.Point(127, 47);
            this.txtMContactNameStm.Name = "txtMContactNameStm";
            this.txtMContactNameStm.Size = new System.Drawing.Size(837, 22);
            this.txtMContactNameStm.TabIndex = 23;
            this.txtMContactNameStm.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // txtAccountingObjectNameStm
            // 
            this.txtAccountingObjectNameStm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance78.TextVAlignAsString = "Middle";
            this.txtAccountingObjectNameStm.Appearance = appearance78;
            this.txtAccountingObjectNameStm.AutoSize = false;
            this.txtAccountingObjectNameStm.Location = new System.Drawing.Point(291, 23);
            this.txtAccountingObjectNameStm.Name = "txtAccountingObjectNameStm";
            this.txtAccountingObjectNameStm.Size = new System.Drawing.Size(673, 22);
            this.txtAccountingObjectNameStm.TabIndex = 25;
            this.txtAccountingObjectNameStm.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // txtIssueBy
            // 
            this.txtIssueBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance79.TextVAlignAsString = "Middle";
            this.txtIssueBy.Appearance = appearance79;
            this.txtIssueBy.AutoSize = false;
            this.txtIssueBy.Location = new System.Drawing.Point(554, 71);
            this.txtIssueBy.Name = "txtIssueBy";
            this.txtIssueBy.Size = new System.Drawing.Size(410, 22);
            this.txtIssueBy.TabIndex = 23;
            // 
            // ultraGroupBox8
            // 
            this.ultraGroupBox8.Controls.Add(this.ultraButton7);
            this.ultraGroupBox8.Controls.Add(this.ultraButton8);
            this.ultraGroupBox8.Controls.Add(this.ultraLabel27);
            this.ultraGroupBox8.Controls.Add(this.ultraLabel28);
            this.ultraGroupBox8.Controls.Add(this.cbbBankAccountDetailIDStm);
            this.ultraGroupBox8.Controls.Add(this.txtBankNameStm);
            this.ultraGroupBox8.Controls.Add(this.txtMReasonPayStm);
            this.ultraGroupBox8.Dock = System.Windows.Forms.DockStyle.Top;
            appearance86.FontData.BoldAsString = "True";
            appearance86.FontData.SizeInPoints = 10F;
            this.ultraGroupBox8.HeaderAppearance = appearance86;
            this.ultraGroupBox8.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox8.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox8.Name = "ultraGroupBox8";
            this.ultraGroupBox8.Size = new System.Drawing.Size(971, 77);
            this.ultraGroupBox8.TabIndex = 69;
            this.ultraGroupBox8.Text = "Đơn vị trả tiền";
            this.ultraGroupBox8.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraButton7
            // 
            this.ultraButton7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton7.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton7.Location = new System.Drawing.Point(879, 23);
            this.ultraButton7.Name = "ultraButton7";
            this.ultraButton7.Size = new System.Drawing.Size(85, 23);
            this.ultraButton7.TabIndex = 39;
            this.ultraButton7.Text = "Công nợ";
            this.ultraButton7.Click += new System.EventHandler(this.btnCongNo_Click);
            // 
            // ultraButton8
            // 
            this.ultraButton8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton8.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton8.Location = new System.Drawing.Point(768, 23);
            this.ultraButton8.Name = "ultraButton8";
            this.ultraButton8.Size = new System.Drawing.Size(105, 23);
            this.ultraButton8.TabIndex = 38;
            this.ultraButton8.Text = "Đơn mua hàng";
            this.ultraButton8.Click += new System.EventHandler(this.btnPPOrder_Click);
            // 
            // ultraLabel27
            // 
            appearance81.BackColor = System.Drawing.Color.Transparent;
            appearance81.TextHAlignAsString = "Left";
            appearance81.TextVAlignAsString = "Middle";
            this.ultraLabel27.Appearance = appearance81;
            this.ultraLabel27.Location = new System.Drawing.Point(6, 48);
            this.ultraLabel27.Name = "ultraLabel27";
            this.ultraLabel27.Size = new System.Drawing.Size(115, 19);
            this.ultraLabel27.TabIndex = 37;
            this.ultraLabel27.Text = "Nội dung TT";
            // 
            // ultraLabel28
            // 
            appearance82.BackColor = System.Drawing.Color.Transparent;
            appearance82.TextHAlignAsString = "Left";
            appearance82.TextVAlignAsString = "Middle";
            this.ultraLabel28.Appearance = appearance82;
            this.ultraLabel28.Location = new System.Drawing.Point(6, 26);
            this.ultraLabel28.Name = "ultraLabel28";
            this.ultraLabel28.Size = new System.Drawing.Size(115, 19);
            this.ultraLabel28.TabIndex = 36;
            this.ultraLabel28.Text = "Tài khoản";
            // 
            // cbbBankAccountDetailIDStm
            // 
            appearance83.TextVAlignAsString = "Middle";
            this.cbbBankAccountDetailIDStm.Appearance = appearance83;
            this.cbbBankAccountDetailIDStm.AutoSize = false;
            this.cbbBankAccountDetailIDStm.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbBankAccountDetailIDStm.Location = new System.Drawing.Point(127, 23);
            this.cbbBankAccountDetailIDStm.Name = "cbbBankAccountDetailIDStm";
            this.cbbBankAccountDetailIDStm.NullText = "<chọn dữ liệu>";
            this.cbbBankAccountDetailIDStm.Size = new System.Drawing.Size(158, 22);
            this.cbbBankAccountDetailIDStm.TabIndex = 31;
            // 
            // txtBankNameStm
            // 
            this.txtBankNameStm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance84.TextVAlignAsString = "Middle";
            this.txtBankNameStm.Appearance = appearance84;
            this.txtBankNameStm.AutoSize = false;
            this.txtBankNameStm.Location = new System.Drawing.Point(290, 23);
            this.txtBankNameStm.Name = "txtBankNameStm";
            this.txtBankNameStm.Size = new System.Drawing.Size(472, 22);
            this.txtBankNameStm.TabIndex = 23;
            // 
            // txtMReasonPayStm
            // 
            this.txtMReasonPayStm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance85.TextVAlignAsString = "Middle";
            this.txtMReasonPayStm.Appearance = appearance85;
            this.txtMReasonPayStm.AutoSize = false;
            this.txtMReasonPayStm.Location = new System.Drawing.Point(127, 47);
            this.txtMReasonPayStm.Name = "txtMReasonPayStm";
            this.txtMReasonPayStm.Size = new System.Drawing.Size(837, 22);
            this.txtMReasonPayStm.TabIndex = 23;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.ultraGroupBox3);
            this.ultraTabPageControl2.Controls.Add(this.ultraGroupBox6);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(971, 177);
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.ultraLabel16);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel17);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel18);
            this.ultraGroupBox3.Controls.Add(this.cbbAccountingObjectIDTtd);
            this.ultraGroupBox3.Controls.Add(this.txtAccountingObjectAddressTtd);
            this.ultraGroupBox3.Controls.Add(this.txtAccountingObjectNameTtd);
            this.ultraGroupBox3.Controls.Add(this.cbbMAccountingObjectBankAccountDetailIDTtd);
            this.ultraGroupBox3.Controls.Add(this.txtMAccountingObjectBankNameTtd);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance96.FontData.BoldAsString = "True";
            appearance96.FontData.SizeInPoints = 10F;
            this.ultraGroupBox3.HeaderAppearance = appearance96;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 77);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(971, 100);
            this.ultraGroupBox3.TabIndex = 70;
            this.ultraGroupBox3.Text = "Đơn vị nhận tiền";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel16
            // 
            appearance87.BackColor = System.Drawing.Color.Transparent;
            appearance87.TextHAlignAsString = "Left";
            appearance87.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance87;
            this.ultraLabel16.Location = new System.Drawing.Point(6, 73);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(82, 19);
            this.ultraLabel16.TabIndex = 45;
            this.ultraLabel16.Text = "Tài khoản";
            // 
            // ultraLabel17
            // 
            appearance88.BackColor = System.Drawing.Color.Transparent;
            appearance88.TextHAlignAsString = "Left";
            appearance88.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance88;
            this.ultraLabel17.Location = new System.Drawing.Point(6, 23);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(82, 19);
            this.ultraLabel17.TabIndex = 44;
            this.ultraLabel17.Text = "Nhà cung cấp";
            // 
            // ultraLabel18
            // 
            appearance89.BackColor = System.Drawing.Color.Transparent;
            appearance89.TextHAlignAsString = "Left";
            appearance89.TextVAlignAsString = "Middle";
            this.ultraLabel18.Appearance = appearance89;
            this.ultraLabel18.Location = new System.Drawing.Point(6, 48);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(82, 19);
            this.ultraLabel18.TabIndex = 43;
            this.ultraLabel18.Text = "Địa chỉ";
            // 
            // cbbAccountingObjectIDTtd
            // 
            appearance90.TextVAlignAsString = "Middle";
            this.cbbAccountingObjectIDTtd.Appearance = appearance90;
            this.cbbAccountingObjectIDTtd.AutoSize = false;
            appearance91.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton6.Appearance = appearance91;
            editorButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectIDTtd.ButtonsRight.Add(editorButton6);
            this.cbbAccountingObjectIDTtd.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectIDTtd.LimitToList = true;
            this.cbbAccountingObjectIDTtd.Location = new System.Drawing.Point(94, 23);
            this.cbbAccountingObjectIDTtd.Name = "cbbAccountingObjectIDTtd";
            this.cbbAccountingObjectIDTtd.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectIDTtd.Size = new System.Drawing.Size(158, 22);
            this.cbbAccountingObjectIDTtd.TabIndex = 42;
            this.cbbAccountingObjectIDTtd.ValueChanged += new System.EventHandler(this.cbbAccountingObjectIDTtd_ValueChanged);
            // 
            // txtAccountingObjectAddressTtd
            // 
            this.txtAccountingObjectAddressTtd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance92.TextVAlignAsString = "Middle";
            this.txtAccountingObjectAddressTtd.Appearance = appearance92;
            this.txtAccountingObjectAddressTtd.AutoSize = false;
            this.txtAccountingObjectAddressTtd.Location = new System.Drawing.Point(94, 47);
            this.txtAccountingObjectAddressTtd.Name = "txtAccountingObjectAddressTtd";
            this.txtAccountingObjectAddressTtd.Size = new System.Drawing.Size(868, 22);
            this.txtAccountingObjectAddressTtd.TabIndex = 23;
            this.txtAccountingObjectAddressTtd.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // txtAccountingObjectNameTtd
            // 
            this.txtAccountingObjectNameTtd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance93.TextVAlignAsString = "Middle";
            this.txtAccountingObjectNameTtd.Appearance = appearance93;
            this.txtAccountingObjectNameTtd.AutoSize = false;
            this.txtAccountingObjectNameTtd.Location = new System.Drawing.Point(258, 23);
            this.txtAccountingObjectNameTtd.Name = "txtAccountingObjectNameTtd";
            this.txtAccountingObjectNameTtd.Size = new System.Drawing.Size(704, 22);
            this.txtAccountingObjectNameTtd.TabIndex = 25;
            this.txtAccountingObjectNameTtd.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // cbbMAccountingObjectBankAccountDetailIDTtd
            // 
            appearance94.TextVAlignAsString = "Middle";
            this.cbbMAccountingObjectBankAccountDetailIDTtd.Appearance = appearance94;
            this.cbbMAccountingObjectBankAccountDetailIDTtd.AutoSize = false;
            this.cbbMAccountingObjectBankAccountDetailIDTtd.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbMAccountingObjectBankAccountDetailIDTtd.Location = new System.Drawing.Point(94, 71);
            this.cbbMAccountingObjectBankAccountDetailIDTtd.Name = "cbbMAccountingObjectBankAccountDetailIDTtd";
            this.cbbMAccountingObjectBankAccountDetailIDTtd.NullText = "<chọn dữ liệu>";
            this.cbbMAccountingObjectBankAccountDetailIDTtd.Size = new System.Drawing.Size(158, 22);
            this.cbbMAccountingObjectBankAccountDetailIDTtd.TabIndex = 31;
            // 
            // txtMAccountingObjectBankNameTtd
            // 
            this.txtMAccountingObjectBankNameTtd.AcceptsReturn = true;
            this.txtMAccountingObjectBankNameTtd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance95.TextVAlignAsString = "Middle";
            this.txtMAccountingObjectBankNameTtd.Appearance = appearance95;
            this.txtMAccountingObjectBankNameTtd.AutoSize = false;
            this.txtMAccountingObjectBankNameTtd.Location = new System.Drawing.Point(258, 71);
            this.txtMAccountingObjectBankNameTtd.Name = "txtMAccountingObjectBankNameTtd";
            this.txtMAccountingObjectBankNameTtd.Size = new System.Drawing.Size(704, 22);
            this.txtMAccountingObjectBankNameTtd.TabIndex = 23;
            // 
            // ultraGroupBox6
            // 
            this.ultraGroupBox6.Controls.Add(this.ultraButton9);
            this.ultraGroupBox6.Controls.Add(this.ultraButton10);
            this.ultraGroupBox6.Controls.Add(this.txtCreditCardType);
            this.ultraGroupBox6.Controls.Add(this.picCreditCardType);
            this.ultraGroupBox6.Controls.Add(this.cbbCreditCardNumber);
            this.ultraGroupBox6.Controls.Add(this.txtOwnerCard);
            this.ultraGroupBox6.Controls.Add(this.lblOwnerCard);
            this.ultraGroupBox6.Controls.Add(this.ultraLabel19);
            this.ultraGroupBox6.Controls.Add(this.ultraLabel20);
            this.ultraGroupBox6.Controls.Add(this.txtMReasonPayTtd);
            this.ultraGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            appearance105.FontData.BoldAsString = "True";
            appearance105.FontData.SizeInPoints = 10F;
            this.ultraGroupBox6.HeaderAppearance = appearance105;
            this.ultraGroupBox6.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox6.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox6.Name = "ultraGroupBox6";
            this.ultraGroupBox6.Size = new System.Drawing.Size(971, 77);
            this.ultraGroupBox6.TabIndex = 69;
            this.ultraGroupBox6.Text = "Đơn vị trả tiền";
            this.ultraGroupBox6.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraButton9
            // 
            this.ultraButton9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton9.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton9.Location = new System.Drawing.Point(879, 23);
            this.ultraButton9.Name = "ultraButton9";
            this.ultraButton9.Size = new System.Drawing.Size(85, 23);
            this.ultraButton9.TabIndex = 44;
            this.ultraButton9.Text = "Công nợ";
            this.ultraButton9.Click += new System.EventHandler(this.btnCongNo_Click);
            // 
            // ultraButton10
            // 
            this.ultraButton10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton10.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton10.Location = new System.Drawing.Point(768, 23);
            this.ultraButton10.Name = "ultraButton10";
            this.ultraButton10.Size = new System.Drawing.Size(105, 23);
            this.ultraButton10.TabIndex = 43;
            this.ultraButton10.Text = "Đơn mua hàng";
            this.ultraButton10.Click += new System.EventHandler(this.btnPPOrder_Click);
            // 
            // txtCreditCardType
            // 
            appearance97.BackColor = System.Drawing.Color.Transparent;
            appearance97.TextHAlignAsString = "Center";
            appearance97.TextVAlignAsString = "Middle";
            this.txtCreditCardType.Appearance = appearance97;
            this.txtCreditCardType.Location = new System.Drawing.Point(311, 24);
            this.txtCreditCardType.Name = "txtCreditCardType";
            this.txtCreditCardType.Size = new System.Drawing.Size(135, 21);
            this.txtCreditCardType.TabIndex = 42;
            // 
            // picCreditCardType
            // 
            appearance98.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance98.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance98.TextHAlignAsString = "Center";
            appearance98.TextVAlignAsString = "Middle";
            this.picCreditCardType.Appearance = appearance98;
            this.picCreditCardType.BackColor = System.Drawing.Color.Transparent;
            this.picCreditCardType.BorderShadowColor = System.Drawing.Color.Transparent;
            this.picCreditCardType.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.picCreditCardType.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.picCreditCardType.Location = new System.Drawing.Point(260, 24);
            this.picCreditCardType.Name = "picCreditCardType";
            this.picCreditCardType.Size = new System.Drawing.Size(43, 21);
            this.picCreditCardType.TabIndex = 41;
            // 
            // cbbCreditCardNumber
            // 
            appearance99.TextVAlignAsString = "Middle";
            this.cbbCreditCardNumber.Appearance = appearance99;
            this.cbbCreditCardNumber.AutoSize = false;
            appearance100.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton7.Appearance = appearance100;
            editorButton7.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbCreditCardNumber.ButtonsRight.Add(editorButton7);
            this.cbbCreditCardNumber.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCreditCardNumber.Location = new System.Drawing.Point(94, 23);
            this.cbbCreditCardNumber.Name = "cbbCreditCardNumber";
            this.cbbCreditCardNumber.NullText = "<chọn dữ liệu>";
            this.cbbCreditCardNumber.Size = new System.Drawing.Size(158, 22);
            this.cbbCreditCardNumber.TabIndex = 40;
            // 
            // txtOwnerCard
            // 
            this.txtOwnerCard.BackColor = System.Drawing.Color.Transparent;
            this.txtOwnerCard.Location = new System.Drawing.Point(505, 24);
            this.txtOwnerCard.Name = "txtOwnerCard";
            this.txtOwnerCard.Size = new System.Drawing.Size(213, 21);
            this.txtOwnerCard.TabIndex = 39;
            this.txtOwnerCard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblOwnerCard
            // 
            appearance101.BackColor = System.Drawing.Color.Transparent;
            appearance101.TextVAlignAsString = "Middle";
            this.lblOwnerCard.Appearance = appearance101;
            this.lblOwnerCard.Location = new System.Drawing.Point(452, 24);
            this.lblOwnerCard.Name = "lblOwnerCard";
            this.lblOwnerCard.Size = new System.Drawing.Size(47, 21);
            this.lblOwnerCard.TabIndex = 38;
            this.lblOwnerCard.Text = "Chủ thẻ:";
            // 
            // ultraLabel19
            // 
            appearance102.BackColor = System.Drawing.Color.Transparent;
            appearance102.TextHAlignAsString = "Left";
            appearance102.TextVAlignAsString = "Middle";
            this.ultraLabel19.Appearance = appearance102;
            this.ultraLabel19.Location = new System.Drawing.Point(6, 49);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(82, 19);
            this.ultraLabel19.TabIndex = 37;
            this.ultraLabel19.Text = "Nội dung TT";
            // 
            // ultraLabel20
            // 
            appearance103.BackColor = System.Drawing.Color.Transparent;
            appearance103.TextHAlignAsString = "Left";
            appearance103.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance103;
            this.ultraLabel20.Location = new System.Drawing.Point(6, 26);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(82, 19);
            this.ultraLabel20.TabIndex = 36;
            this.ultraLabel20.Text = "Số thẻ";
            // 
            // txtMReasonPayTtd
            // 
            this.txtMReasonPayTtd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance104.TextVAlignAsString = "Middle";
            this.txtMReasonPayTtd.Appearance = appearance104;
            this.txtMReasonPayTtd.AutoSize = false;
            this.txtMReasonPayTtd.Location = new System.Drawing.Point(94, 47);
            this.txtMReasonPayTtd.Name = "txtMReasonPayTtd";
            this.txtMReasonPayTtd.Size = new System.Drawing.Size(870, 22);
            this.txtMReasonPayTtd.TabIndex = 23;
            // 
            // palTop
            // 
            this.palTop.Controls.Add(this.ultraGroupBoxTop);
            this.palTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTop.Location = new System.Drawing.Point(0, 0);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(973, 68);
            this.palTop.TabIndex = 27;
            // 
            // ultraGroupBoxTop
            // 
            this.ultraGroupBoxTop.Controls.Add(this.palVouchersNoHd);
            this.ultraGroupBoxTop.Controls.Add(this.palVouchersNoTm);
            this.ultraGroupBoxTop.Controls.Add(this.palChung);
            this.ultraGroupBoxTop.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance109.FontData.BoldAsString = "True";
            appearance109.FontData.SizeInPoints = 13F;
            this.ultraGroupBoxTop.HeaderAppearance = appearance109;
            this.ultraGroupBoxTop.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxTop.Name = "ultraGroupBoxTop";
            this.ultraGroupBoxTop.Size = new System.Drawing.Size(973, 68);
            this.ultraGroupBoxTop.TabIndex = 31;
            this.ultraGroupBoxTop.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // palVouchersNoHd
            // 
            appearance106.BackColor = System.Drawing.Color.Transparent;
            this.palVouchersNoHd.Appearance = appearance106;
            this.palVouchersNoHd.Location = new System.Drawing.Point(12, 13);
            this.palVouchersNoHd.Name = "palVouchersNoHd";
            this.palVouchersNoHd.Size = new System.Drawing.Size(316, 50);
            this.palVouchersNoHd.TabIndex = 40;
            // 
            // palVouchersNoTm
            // 
            appearance107.BackColor = System.Drawing.Color.Transparent;
            this.palVouchersNoTm.Appearance = appearance107;
            this.palVouchersNoTm.Location = new System.Drawing.Point(12, 13);
            this.palVouchersNoTm.Name = "palVouchersNoTm";
            this.palVouchersNoTm.Size = new System.Drawing.Size(316, 50);
            this.palVouchersNoTm.TabIndex = 43;
            // 
            // palChung
            // 
            this.palChung.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.palChung.BackColor = System.Drawing.Color.Transparent;
            this.palChung.Controls.Add(this.optThanhToan);
            this.palChung.Controls.Add(this.cbbChonThanhToan);
            this.palChung.Location = new System.Drawing.Point(545, 13);
            this.palChung.Name = "palChung";
            this.palChung.Size = new System.Drawing.Size(420, 33);
            this.palChung.TabIndex = 39;
            // 
            // optThanhToan
            // 
            this.optThanhToan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance108.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance108.TextVAlignAsString = "Middle";
            this.optThanhToan.Appearance = appearance108;
            this.optThanhToan.BackColor = System.Drawing.Color.Transparent;
            this.optThanhToan.BackColorInternal = System.Drawing.Color.Transparent;
            this.optThanhToan.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem1.DataValue = false;
            valueListItem1.DisplayText = "Chưa thanh toán";
            valueListItem2.DataValue = true;
            valueListItem2.DisplayText = "Thanh toán ngay";
            this.optThanhToan.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.optThanhToan.Location = new System.Drawing.Point(10, 10);
            this.optThanhToan.Name = "optThanhToan";
            this.optThanhToan.ReadOnly = false;
            this.optThanhToan.Size = new System.Drawing.Size(210, 19);
            this.optThanhToan.TabIndex = 46;
            this.optThanhToan.ValueChanged += new System.EventHandler(this.OptThanhToanValueChanged);
            // 
            // cbbChonThanhToan
            // 
            this.cbbChonThanhToan.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbChonThanhToan.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbChonThanhToan.Location = new System.Drawing.Point(262, 6);
            this.cbbChonThanhToan.Name = "cbbChonThanhToan";
            this.cbbChonThanhToan.Size = new System.Drawing.Size(153, 21);
            this.cbbChonThanhToan.TabIndex = 36;
            this.cbbChonThanhToan.ValueChanged += new System.EventHandler(this.CbbChonThanhToanValueChanged);
            // 
            // palFill
            // 
            this.palFill.Controls.Add(this.palGrid);
            this.palFill.Controls.Add(this.ultraSplitter1);
            this.palFill.Controls.Add(this.palThongTinChung);
            this.palFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palFill.Location = new System.Drawing.Point(0, 68);
            this.palFill.Name = "palFill";
            this.palFill.Size = new System.Drawing.Size(973, 492);
            this.palFill.TabIndex = 28;
            // 
            // palGrid
            // 
            this.palGrid.Controls.Add(this.pnlUgrid);
            this.palGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palGrid.Location = new System.Drawing.Point(0, 210);
            this.palGrid.Name = "palGrid";
            this.palGrid.Size = new System.Drawing.Size(973, 282);
            this.palGrid.TabIndex = 28;
            // 
            // pnlUgrid
            // 
            // 
            // pnlUgrid.ClientArea
            // 
            this.pnlUgrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.pnlUgrid.ClientArea.Controls.Add(this.btnApportionTaxes);
            this.pnlUgrid.ClientArea.Controls.Add(this.btnApportion);
            this.pnlUgrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlUgrid.Location = new System.Drawing.Point(0, 0);
            this.pnlUgrid.Name = "pnlUgrid";
            this.pnlUgrid.Size = new System.Drawing.Size(973, 282);
            this.pnlUgrid.TabIndex = 0;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance110.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance110;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(575, 2);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 6;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // btnApportionTaxes
            // 
            this.btnApportionTaxes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance111.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnApportionTaxes.HotTrackAppearance = appearance111;
            this.btnApportionTaxes.Location = new System.Drawing.Point(667, 2);
            this.btnApportionTaxes.Name = "btnApportionTaxes";
            this.btnApportionTaxes.Size = new System.Drawing.Size(177, 22);
            this.btnApportionTaxes.TabIndex = 1;
            this.btnApportionTaxes.Text = "Phân &bổ chi phí trước hải quan";
            this.btnApportionTaxes.Click += new System.EventHandler(this.btnApportionTaxes_Click);
            // 
            // btnApportion
            // 
            this.btnApportion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApportion.Location = new System.Drawing.Point(850, 2);
            this.btnApportion.Name = "btnApportion";
            this.btnApportion.Size = new System.Drawing.Size(115, 22);
            this.btnApportion.TabIndex = 0;
            this.btnApportion.Text = "&Phân bổ chi phí...";
            this.btnApportion.Click += new System.EventHandler(this.btnApportion_Click);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 200);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 200;
            this.ultraSplitter1.Size = new System.Drawing.Size(973, 10);
            this.ultraSplitter1.TabIndex = 2;
            // 
            // palThongTinChung
            // 
            this.palThongTinChung.Controls.Add(this.ultraTabControlThongTinChung);
            this.palThongTinChung.Dock = System.Windows.Forms.DockStyle.Top;
            this.palThongTinChung.Location = new System.Drawing.Point(0, 0);
            this.palThongTinChung.Name = "palThongTinChung";
            this.palThongTinChung.Size = new System.Drawing.Size(973, 200);
            this.palThongTinChung.TabIndex = 27;
            // 
            // ultraTabControlThongTinChung
            // 
            appearance112.BackColor = System.Drawing.Color.Transparent;
            appearance112.TextHAlignAsString = "Left";
            appearance112.TextVAlignAsString = "Middle";
            this.ultraTabControlThongTinChung.Appearance = appearance112;
            this.ultraTabControlThongTinChung.Controls.Add(this.ultraTabSharedControlsPage2);
            this.ultraTabControlThongTinChung.Controls.Add(this.ultraTabPageHoaDon);
            this.ultraTabControlThongTinChung.Controls.Add(this.ultraTabPagePhieuXuatKho);
            this.ultraTabControlThongTinChung.Controls.Add(this.ultraTabPagePhieuThu);
            this.ultraTabControlThongTinChung.Controls.Add(this.ultraTabPageGiayBaoCo);
            this.ultraTabControlThongTinChung.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControlThongTinChung.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControlThongTinChung.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControlThongTinChung.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControlThongTinChung.Name = "ultraTabControlThongTinChung";
            this.ultraTabControlThongTinChung.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ultraTabControlThongTinChung.SharedControlsPage = this.ultraTabSharedControlsPage2;
            this.ultraTabControlThongTinChung.Size = new System.Drawing.Size(973, 200);
            this.ultraTabControlThongTinChung.TabIndex = 27;
            ultraTab4.Key = "tabChungTu";
            ultraTab4.TabPage = this.ultraTabPageHoaDon;
            ultraTab4.Text = "Chứng từ";
            ultraTab5.Key = "tabTienMat";
            ultraTab5.TabPage = this.ultraTabPagePhieuXuatKho;
            ultraTab5.Text = "Phiếu chi";
            ultraTab6.Key = "tabUyNhiemChi";
            ultraTab6.TabPage = this.ultraTabPagePhieuThu;
            ultraTab6.Text = "Ủy nhiệm chi";
            ultraTab6.Visible = false;
            ultraTab7.Key = "tabSecChuyenKhoan";
            ultraTab7.TabPage = this.ultraTabPageGiayBaoCo;
            ultraTab7.Text = "Séc chuyển khoản";
            ultraTab7.Visible = false;
            ultraTab1.Key = "tabSecTienMat";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Séc tiền mặt";
            ultraTab2.Key = "tabTheTinDung";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Thẻ tín dụng";
            this.ultraTabControlThongTinChung.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab4,
            ultraTab5,
            ultraTab6,
            ultraTab7,
            ultraTab1,
            ultraTab2});
            this.ultraTabControlThongTinChung.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            this.ultraTabControlThongTinChung.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.ultraTabControlThongTinChung_SelectedTabChanged);
            this.ultraTabControlThongTinChung.Leave += new System.EventHandler(this.ultraTabControlThongTinChung_Leave);
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(971, 177);
            // 
            // chkBillReceived
            // 
            appearance113.BackColor = System.Drawing.Color.Transparent;
            appearance113.BackColor2 = System.Drawing.Color.Transparent;
            appearance113.ImageAlpha = Infragistics.Win.Alpha.Transparent;
            this.chkBillReceived.Appearance = appearance113;
            this.chkBillReceived.BackColor = System.Drawing.Color.Transparent;
            this.chkBillReceived.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkBillReceived.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkBillReceived.Location = new System.Drawing.Point(7, 6);
            this.chkBillReceived.Name = "chkBillReceived";
            this.chkBillReceived.Size = new System.Drawing.Size(116, 22);
            this.chkBillReceived.TabIndex = 43;
            this.chkBillReceived.Text = "Đã nhận hóa đơn";
            this.chkBillReceived.Visible = false;
            this.chkBillReceived.CheckStateChanged += new System.EventHandler(this.chkBillReceived_CheckStateChanged);
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraPanel2);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 560);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(973, 135);
            this.ultraPanel1.TabIndex = 29;
            // 
            // ultraPanel2
            // 
            appearance114.TextHAlignAsString = "Left";
            appearance114.TextVAlignAsString = "Middle";
            this.ultraPanel2.Appearance = appearance114;
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalSpecialConsumeTaxAmount);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel35);
            this.ultraPanel2.ClientArea.Controls.Add(this.uGridControl);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblBillReceived);
            this.ultraPanel2.ClientArea.Controls.Add(this.chkBillReceived);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel21);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel22);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalInwardAmount);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel23);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel32);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalImportTaxAmount);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalDiscountAmountOriginal);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalAmountOriginal);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalAllOriginal);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalDiscountAmount);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalAmount);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalVATAmount);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblChangeByForm);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalAll);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel36);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(973, 135);
            this.ultraPanel2.TabIndex = 25;
            // 
            // lblTotalSpecialConsumeTaxAmount
            // 
            this.lblTotalSpecialConsumeTaxAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance115.TextHAlignAsString = "Right";
            appearance115.TextVAlignAsString = "Middle";
            this.lblTotalSpecialConsumeTaxAmount.Appearance = appearance115;
            this.lblTotalSpecialConsumeTaxAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalSpecialConsumeTaxAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalSpecialConsumeTaxAmount.Location = new System.Drawing.Point(850, 39);
            this.lblTotalSpecialConsumeTaxAmount.Name = "lblTotalSpecialConsumeTaxAmount";
            this.lblTotalSpecialConsumeTaxAmount.Size = new System.Drawing.Size(120, 17);
            this.lblTotalSpecialConsumeTaxAmount.TabIndex = 48;
            this.lblTotalSpecialConsumeTaxAmount.Text = "0";
            this.lblTotalSpecialConsumeTaxAmount.WrapText = false;
            // 
            // ultraLabel35
            // 
            this.ultraLabel35.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance116.TextHAlignAsString = "Left";
            appearance116.TextVAlignAsString = "Middle";
            this.ultraLabel35.Appearance = appearance116;
            this.ultraLabel35.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel35.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel35.Location = new System.Drawing.Point(710, 39);
            this.ultraLabel35.Name = "ultraLabel35";
            this.ultraLabel35.Size = new System.Drawing.Size(129, 17);
            this.ultraLabel35.TabIndex = 46;
            this.ultraLabel35.Text = "Tiền thuế TTĐB";
            // 
            // uGridControl
            // 
            this.uGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridControl.Location = new System.Drawing.Point(611, 87);
            this.uGridControl.Name = "uGridControl";
            this.uGridControl.Size = new System.Drawing.Size(361, 48);
            this.uGridControl.TabIndex = 45;
            this.uGridControl.Text = "uGridControl";
            // 
            // lblBillReceived
            // 
            appearance117.BackColor = System.Drawing.Color.Transparent;
            appearance117.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance117.BorderColor = System.Drawing.Color.DarkRed;
            appearance117.BorderColor2 = System.Drawing.Color.DarkRed;
            appearance117.BorderColor3DBase = System.Drawing.Color.DarkRed;
            appearance117.FontData.BoldAsString = "True";
            appearance117.ForeColor = System.Drawing.Color.DarkRed;
            appearance117.ForeColorDisabled = System.Drawing.Color.IndianRed;
            appearance117.TextHAlignAsString = "Center";
            appearance117.TextVAlignAsString = "Middle";
            this.lblBillReceived.Appearance = appearance117;
            this.lblBillReceived.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.TwoColor;
            this.lblBillReceived.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.TwoColor;
            this.lblBillReceived.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBillReceived.Location = new System.Drawing.Point(7, 40);
            this.lblBillReceived.Name = "lblBillReceived";
            this.lblBillReceived.Size = new System.Drawing.Size(163, 38);
            this.lblBillReceived.TabIndex = 44;
            this.lblBillReceived.Text = "ĐÃ NHẬN HÓA ĐƠN";
            this.lblBillReceived.Visible = false;
            // 
            // ultraLabel21
            // 
            this.ultraLabel21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance118.TextHAlignAsString = "Left";
            appearance118.TextVAlignAsString = "Middle";
            this.ultraLabel21.Appearance = appearance118;
            this.ultraLabel21.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel21.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel21.Location = new System.Drawing.Point(307, 22);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(131, 17);
            this.ultraLabel21.TabIndex = 1;
            this.ultraLabel21.Text = "Tiền chiết khấu";
            // 
            // ultraLabel22
            // 
            this.ultraLabel22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance119.TextHAlignAsString = "Left";
            appearance119.TextVAlignAsString = "Middle";
            this.ultraLabel22.Appearance = appearance119;
            this.ultraLabel22.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel22.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel22.Location = new System.Drawing.Point(307, 4);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(131, 17);
            this.ultraLabel22.TabIndex = 0;
            this.ultraLabel22.Text = "Tổng tiền hàng";
            // 
            // lblTotalInwardAmount
            // 
            this.lblTotalInwardAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance120.TextHAlignAsString = "Right";
            appearance120.TextVAlignAsString = "Middle";
            this.lblTotalInwardAmount.Appearance = appearance120;
            this.lblTotalInwardAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalInwardAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalInwardAmount.Location = new System.Drawing.Point(850, 56);
            this.lblTotalInwardAmount.Name = "lblTotalInwardAmount";
            this.lblTotalInwardAmount.Size = new System.Drawing.Size(120, 17);
            this.lblTotalInwardAmount.TabIndex = 23;
            this.lblTotalInwardAmount.Text = "0";
            this.lblTotalInwardAmount.WrapText = false;
            // 
            // ultraLabel23
            // 
            this.ultraLabel23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance121.TextHAlignAsString = "Left";
            appearance121.TextVAlignAsString = "Middle";
            this.ultraLabel23.Appearance = appearance121;
            this.ultraLabel23.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel23.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel23.Location = new System.Drawing.Point(711, 4);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(131, 17);
            this.ultraLabel23.TabIndex = 2;
            this.ultraLabel23.Text = "Tiền thuế GTGT";
            // 
            // ultraLabel32
            // 
            this.ultraLabel32.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance122.TextHAlignAsString = "Left";
            appearance122.TextVAlignAsString = "Middle";
            this.ultraLabel32.Appearance = appearance122;
            this.ultraLabel32.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel32.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel32.Location = new System.Drawing.Point(307, 39);
            this.ultraLabel32.Name = "ultraLabel32";
            this.ultraLabel32.Size = new System.Drawing.Size(131, 17);
            this.ultraLabel32.TabIndex = 3;
            this.ultraLabel32.Text = "Tổng tiền thanh toán";
            // 
            // lblTotalImportTaxAmount
            // 
            this.lblTotalImportTaxAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance123.TextHAlignAsString = "Right";
            appearance123.TextVAlignAsString = "Middle";
            this.lblTotalImportTaxAmount.Appearance = appearance123;
            this.lblTotalImportTaxAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalImportTaxAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalImportTaxAmount.Location = new System.Drawing.Point(850, 21);
            this.lblTotalImportTaxAmount.Name = "lblTotalImportTaxAmount";
            this.lblTotalImportTaxAmount.Size = new System.Drawing.Size(120, 17);
            this.lblTotalImportTaxAmount.TabIndex = 20;
            this.lblTotalImportTaxAmount.Text = "0";
            this.lblTotalImportTaxAmount.WrapText = false;
            // 
            // lblTotalDiscountAmountOriginal
            // 
            this.lblTotalDiscountAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance124.TextHAlignAsString = "Right";
            appearance124.TextVAlignAsString = "Middle";
            this.lblTotalDiscountAmountOriginal.Appearance = appearance124;
            this.lblTotalDiscountAmountOriginal.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalDiscountAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalDiscountAmountOriginal.Location = new System.Drawing.Point(444, 22);
            this.lblTotalDiscountAmountOriginal.Name = "lblTotalDiscountAmountOriginal";
            this.lblTotalDiscountAmountOriginal.Size = new System.Drawing.Size(120, 17);
            this.lblTotalDiscountAmountOriginal.TabIndex = 5;
            this.lblTotalDiscountAmountOriginal.Text = "0";
            this.lblTotalDiscountAmountOriginal.WrapText = false;
            // 
            // lblTotalAmountOriginal
            // 
            this.lblTotalAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance125.TextHAlignAsString = "Right";
            appearance125.TextVAlignAsString = "Middle";
            this.lblTotalAmountOriginal.Appearance = appearance125;
            this.lblTotalAmountOriginal.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmountOriginal.Location = new System.Drawing.Point(444, 4);
            this.lblTotalAmountOriginal.Name = "lblTotalAmountOriginal";
            this.lblTotalAmountOriginal.Size = new System.Drawing.Size(120, 17);
            this.lblTotalAmountOriginal.TabIndex = 4;
            this.lblTotalAmountOriginal.Text = "0";
            this.lblTotalAmountOriginal.WrapText = false;
            // 
            // lblTotalAllOriginal
            // 
            this.lblTotalAllOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance126.TextHAlignAsString = "Right";
            appearance126.TextVAlignAsString = "Middle";
            this.lblTotalAllOriginal.Appearance = appearance126;
            this.lblTotalAllOriginal.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalAllOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAllOriginal.Location = new System.Drawing.Point(444, 39);
            this.lblTotalAllOriginal.Name = "lblTotalAllOriginal";
            this.lblTotalAllOriginal.Size = new System.Drawing.Size(120, 17);
            this.lblTotalAllOriginal.TabIndex = 7;
            this.lblTotalAllOriginal.Text = "0";
            this.lblTotalAllOriginal.WrapText = false;
            // 
            // lblTotalDiscountAmount
            // 
            this.lblTotalDiscountAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance127.TextHAlignAsString = "Right";
            appearance127.TextVAlignAsString = "Middle";
            this.lblTotalDiscountAmount.Appearance = appearance127;
            this.lblTotalDiscountAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalDiscountAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalDiscountAmount.Location = new System.Drawing.Point(570, 22);
            this.lblTotalDiscountAmount.Name = "lblTotalDiscountAmount";
            this.lblTotalDiscountAmount.Size = new System.Drawing.Size(120, 17);
            this.lblTotalDiscountAmount.TabIndex = 9;
            this.lblTotalDiscountAmount.Text = "0";
            this.lblTotalDiscountAmount.WrapText = false;
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance128.TextHAlignAsString = "Right";
            appearance128.TextVAlignAsString = "Middle";
            this.lblTotalAmount.Appearance = appearance128;
            this.lblTotalAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmount.Location = new System.Drawing.Point(570, 4);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(120, 17);
            this.lblTotalAmount.TabIndex = 8;
            this.lblTotalAmount.Text = "0";
            this.lblTotalAmount.WrapText = false;
            // 
            // lblTotalVATAmount
            // 
            this.lblTotalVATAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance129.TextHAlignAsString = "Right";
            appearance129.TextVAlignAsString = "Middle";
            this.lblTotalVATAmount.Appearance = appearance129;
            this.lblTotalVATAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalVATAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalVATAmount.Location = new System.Drawing.Point(850, 4);
            this.lblTotalVATAmount.Name = "lblTotalVATAmount";
            this.lblTotalVATAmount.Size = new System.Drawing.Size(120, 17);
            this.lblTotalVATAmount.TabIndex = 10;
            this.lblTotalVATAmount.Text = "0";
            this.lblTotalVATAmount.WrapText = false;
            // 
            // lblChangeByForm
            // 
            this.lblChangeByForm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance130.TextHAlignAsString = "Left";
            appearance130.TextVAlignAsString = "Middle";
            this.lblChangeByForm.Appearance = appearance130;
            this.lblChangeByForm.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblChangeByForm.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChangeByForm.Location = new System.Drawing.Point(710, 56);
            this.lblChangeByForm.Name = "lblChangeByForm";
            this.lblChangeByForm.Size = new System.Drawing.Size(129, 17);
            this.lblChangeByForm.TabIndex = 15;
            this.lblChangeByForm.Text = "Tổng giá trị";
            // 
            // lblTotalAll
            // 
            this.lblTotalAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance131.TextHAlignAsString = "Right";
            appearance131.TextVAlignAsString = "Middle";
            this.lblTotalAll.Appearance = appearance131;
            this.lblTotalAll.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalAll.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAll.Location = new System.Drawing.Point(570, 39);
            this.lblTotalAll.Name = "lblTotalAll";
            this.lblTotalAll.Size = new System.Drawing.Size(120, 17);
            this.lblTotalAll.TabIndex = 11;
            this.lblTotalAll.Text = "0";
            this.lblTotalAll.WrapText = false;
            // 
            // ultraLabel36
            // 
            this.ultraLabel36.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance132.TextHAlignAsString = "Left";
            appearance132.TextVAlignAsString = "Middle";
            this.ultraLabel36.Appearance = appearance132;
            this.ultraLabel36.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel36.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel36.Location = new System.Drawing.Point(710, 21);
            this.ultraLabel36.Name = "ultraLabel36";
            this.ultraLabel36.Size = new System.Drawing.Size(129, 17);
            this.ultraLabel36.TabIndex = 12;
            this.ultraLabel36.Text = "Tiền thuế nhập khẩu";
            // 
            // FPPInvoiceDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 695);
            this.Controls.Add(this.palFill);
            this.Controls.Add(this.palTop);
            this.Controls.Add(this.ultraPanel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MinimumSize = new System.Drawing.Size(981, 722);
            this.Name = "FPPInvoiceDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FPPInvoiceDetail_FormClosing);
            this.Leave += new System.EventHandler(this.FPPInvoiceDetail_Leave);
            this.ultraTabPageHoaDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDon)).EndInit();
            this.grHoaDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxHoaDon)).EndInit();
            this.ultraGroupBoxHoaDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).EndInit();
            this.ultraTabPagePhieuXuatKho.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxPhieuXuatKho)).EndInit();
            this.ultraGroupBoxPhieuXuatKho.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameTm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDTm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMContactNameTm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberAttachTm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMReasonPayTm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressTm)).EndInit();
            this.ultraTabPagePhieuThu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDUnc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressUnc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameUnc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMAccountingObjectBankAccountDetailIDUnc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAccountingObjectBankNameUnc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.ultraGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetailIDUnc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankNameUnc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMReasonPayUnc)).EndInit();
            this.ultraTabPageGiayBaoCo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDSck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressSck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameSck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMAccountingObjectBankAccountDetailIDSnk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAccountingObjectBankNameSck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetailIDSck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankNameSck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMReasonPaySck)).EndInit();
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).EndInit();
            this.ultraGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dteIssueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificationNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDStm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMContactNameStm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameStm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox8)).EndInit();
            this.ultraGroupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetailIDStm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankNameStm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMReasonPayStm)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDTtd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressTtd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameTtd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMAccountingObjectBankAccountDetailIDTtd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMAccountingObjectBankNameTtd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).EndInit();
            this.ultraGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMReasonPayTtd)).EndInit();
            this.palTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxTop)).EndInit();
            this.ultraGroupBoxTop.ResumeLayout(false);
            this.palVouchersNoHd.ResumeLayout(false);
            this.palVouchersNoTm.ResumeLayout(false);
            this.palChung.ResumeLayout(false);
            this.palChung.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optThanhToan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChonThanhToan)).EndInit();
            this.palFill.ResumeLayout(false);
            this.palGrid.ResumeLayout(false);
            this.pnlUgrid.ClientArea.ResumeLayout(false);
            this.pnlUgrid.ResumeLayout(false);
            this.palThongTinChung.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControlThongTinChung)).EndInit();
            this.ultraTabControlThongTinChung.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkBillReceived)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel palTop;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxTop;
        private System.Windows.Forms.Panel palFill;
        private System.Windows.Forms.Panel palGrid;
        private System.Windows.Forms.Panel palThongTinChung;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbChonThanhToan;
        private System.Windows.Forms.Panel palChung;
        private Infragistics.Win.Misc.UltraPanel pnlUgrid;
        private Infragistics.Win.Misc.UltraPanel palVouchersNoHd;
        private Infragistics.Win.Misc.UltraPanel palVouchersNoTm;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkBillReceived;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.Misc.UltraLabel lblTotalInwardAmount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.Misc.UltraLabel ultraLabel32;
        private Infragistics.Win.Misc.UltraLabel lblTotalImportTaxAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalDiscountAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel lblTotalAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel lblTotalAllOriginal;
        private Infragistics.Win.Misc.UltraLabel lblTotalDiscountAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalVATAmount;
        private Infragistics.Win.Misc.UltraLabel lblChangeByForm;
        private Infragistics.Win.Misc.UltraLabel lblTotalAll;
        private Infragistics.Win.Misc.UltraLabel ultraLabel36;
        private UltraOptionSet_Ex optThanhToan;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControlThongTinChung;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageHoaDon;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxHoaDon;
        private Infragistics.Win.Misc.UltraButton btnCongNo;
        private Infragistics.Win.Misc.UltraButton btnPPOrder;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactName;
        private Infragistics.Win.Misc.UltraLabel lblContactNameCt;
        private Infragistics.Win.Misc.UltraLabel lblOriginalNoCt;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.Misc.UltraLabel lblReasonCt;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectID;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPagePhieuXuatKho;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxPhieuXuatKho;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameTm;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDTm;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMContactNameTm;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNumberAttachTm;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMReasonPayTm;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressTm;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPagePhieuThu;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDUnc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressUnc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameUnc;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbMAccountingObjectBankAccountDetailIDUnc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMAccountingObjectBankNameUnc;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccountDetailIDUnc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankNameUnc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMReasonPayUnc;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageGiayBaoCo;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDSck;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressSck;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameSck;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbMAccountingObjectBankAccountDetailIDSnk;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMAccountingObjectBankNameSck;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccountDetailIDSck;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankNameSck;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMReasonPaySck;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox7;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteIssueDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel31;
        private Infragistics.Win.Misc.UltraLabel ultraLabel30;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIdentificationNo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.Misc.UltraLabel ultraLabel26;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDStm;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMContactNameStm;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameStm;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIssueBy;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel27;
        private Infragistics.Win.Misc.UltraLabel ultraLabel28;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccountDetailIDStm;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankNameStm;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMReasonPayStm;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDTtd;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressTtd;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameTtd;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbMAccountingObjectBankAccountDetailIDTtd;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMAccountingObjectBankNameTtd;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox6;
        private Infragistics.Win.Misc.UltraLabel txtCreditCardType;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox picCreditCardType;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCreditCardNumber;
        private System.Windows.Forms.Label txtOwnerCard;
        private Infragistics.Win.Misc.UltraLabel lblOwnerCard;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMReasonPayTtd;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOriginalNo;
        private Infragistics.Win.Misc.UltraButton btnApportionTaxes;
        private Infragistics.Win.Misc.UltraButton btnApportion;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.Misc.UltraButton ultraButton2;
        private Infragistics.Win.Misc.UltraButton ultraButton3;
        private Infragistics.Win.Misc.UltraButton ultraButton4;
        private Infragistics.Win.Misc.UltraButton ultraButton5;
        private Infragistics.Win.Misc.UltraButton ultraButton6;
        private Infragistics.Win.Misc.UltraButton ultraButton7;
        private Infragistics.Win.Misc.UltraButton ultraButton8;
        private Infragistics.Win.Misc.UltraButton ultraButton9;
        private Infragistics.Win.Misc.UltraButton ultraButton10;
        private Infragistics.Win.Misc.UltraLabel lblChungTuGoc;
        private Infragistics.Win.Misc.UltraLabel ultraLabel29;
        private Infragistics.Win.Misc.UltraLabel lblBillReceived;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridControl;
        private Infragistics.Win.Misc.UltraLabel lblTotalSpecialConsumeTaxAmount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel35;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
        private Infragistics.Win.Misc.UltraGroupBox grHoaDon;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSoHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtKyHieuHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel37;
        private Infragistics.Win.Misc.UltraLabel ultraLabel38;
        private Infragistics.Win.Misc.UltraLabel ultraLabel39;
        private Infragistics.Win.Misc.UltraLabel ultraLabel40;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteNgayHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMauSoHD;
    }
}
