﻿namespace Accounting.Frm
{
    partial class FPPInvoiceDetail1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton6 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab8 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab9 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab10 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridHangTien = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridTax = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridThongKe = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridChungTuChiPhi = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl5 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.palTopSctNhtNctPXK = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.uButtonPPOrder = new Infragistics.Win.Misc.UltraButton();
            this.txtNumberAttach = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.lblReason = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblCompanyTaxCode = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccoutingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectAddress = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObjectID = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl6 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.palTopSctNhtNctPXK1 = new Infragistics.Win.Misc.UltraPanel();
            this.cbbAccountingObjectID1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.ultraTextEditor2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.txtReason1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.txtReceiver = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccoutingObjectName1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectAddress1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl7 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.palTopSctNhtNctPXK2 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbAccountingObjectID2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccountingObjectAddress2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccoutingObjectName2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
            this.cbbBankAccountingObject2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtBankAccountingObjectName2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbBankAccountObject2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtBankAccountObjectName2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtReason2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl8 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.palTopSctNhtNctPXK3 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox6 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbAccountingObjectID3 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccountingObjectAddress3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccoutingObjectName3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraButton4 = new Infragistics.Win.Misc.UltraButton();
            this.cbbBankAccountingObject3 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtBankAccountingObjectName3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox7 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbBankAccountObject3 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtBankAccountObjectName3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtReason3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl9 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.palTopSctNhtNctPXK4 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox8 = new Infragistics.Win.Misc.UltraGroupBox();
            this.dteIssueDate4 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.cbbAccountingObjectID4 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccountingObjectAddress4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel32 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel33 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccoutingObjectName4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraButton6 = new Infragistics.Win.Misc.UltraButton();
            this.txtIdentificationNo4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtIssueBy4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel46 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel45 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel34 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox9 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbBankAccountObject4 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtBankAccountObjectName4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtReason4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel35 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel36 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl10 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.palTopSctNhtNctPXK5 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox10 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtOwnerCard = new System.Windows.Forms.Label();
            this.lblOwnerCard = new Infragistics.Win.Misc.UltraLabel();
            this.cbbCreditCardNumber5 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtReason5 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel37 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel38 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox11 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbAccountingObjectID5 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccountingObjectAddress5 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel41 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel42 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccoutingObjectName5 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraButton8 = new Infragistics.Win.Misc.UltraButton();
            this.cbbBankAccountingObject5 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtBankAccountingObjectName5 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel43 = new Infragistics.Win.Misc.UltraLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraComboEditor1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraOptionSet1 = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.ultraTabControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.palBottom = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lbTotalPaymentAmount = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lbTotalPaymentAmoutOriginal = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lbTotalVatAmount = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbTotalVatAmountOriginal = new System.Windows.Forms.Label();
            this.lbTotalImportTaxAmount = new System.Windows.Forms.Label();
            this.lbTotalAmount = new System.Windows.Forms.Label();
            this.lbTotalSpecialConsumeTaxAmount = new System.Windows.Forms.Label();
            this.lbTotalDiscountAmount = new System.Windows.Forms.Label();
            this.lbTotalSpecialConsumeTaxAmountOriginal = new System.Windows.Forms.Label();
            this.lbTotalDiscountAmountOriginal = new System.Windows.Forms.Label();
            this.lbTotalImportTaxAmountOriginal = new System.Windows.Forms.Label();
            this.lbTotalAmountOriginal = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.uGridControl = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraToolTipManager1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHangTien)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridTax)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridThongKe)).BeginInit();
            this.ultraTabPageControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridChungTuChiPhi)).BeginInit();
            this.ultraTabPageControl5.SuspendLayout();
            this.palTopSctNhtNctPXK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberAttach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccoutingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).BeginInit();
            this.ultraTabPageControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            this.palTopSctNhtNctPXK1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccoutingObjectName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress1)).BeginInit();
            this.ultraTabPageControl7.SuspendLayout();
            this.palTopSctNhtNctPXK2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccoutingObjectName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountingObject2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccountingObjectName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.ultraGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountObject2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccountObjectName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason2)).BeginInit();
            this.ultraTabPageControl8.SuspendLayout();
            this.palTopSctNhtNctPXK3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).BeginInit();
            this.ultraGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccoutingObjectName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountingObject3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccountingObjectName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).BeginInit();
            this.ultraGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountObject3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccountObjectName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason3)).BeginInit();
            this.ultraTabPageControl9.SuspendLayout();
            this.palTopSctNhtNctPXK4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox8)).BeginInit();
            this.ultraGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteIssueDate4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccoutingObjectName4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificationNo4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueBy4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox9)).BeginInit();
            this.ultraGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountObject4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccountObjectName4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason4)).BeginInit();
            this.ultraTabPageControl10.SuspendLayout();
            this.palTopSctNhtNctPXK5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox10)).BeginInit();
            this.ultraGroupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardNumber5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox11)).BeginInit();
            this.ultraGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccoutingObjectName5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountingObject5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccountingObjectName5)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl2)).BeginInit();
            this.ultraTabControl2.SuspendLayout();
            this.palBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGridHangTien);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(927, 259);
            // 
            // uGridHangTien
            // 
            this.uGridHangTien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridHangTien.Location = new System.Drawing.Point(0, 0);
            this.uGridHangTien.Name = "uGridHangTien";
            this.uGridHangTien.Size = new System.Drawing.Size(927, 259);
            this.uGridHangTien.TabIndex = 0;
            this.uGridHangTien.Text = "ultraGrid1";
            this.uGridHangTien.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uGridHangTien.AfterExitEditMode += new System.EventHandler(this.uGridHangTien_AfterExitEditMode);
            this.uGridHangTien.KeyUp += new System.Windows.Forms.KeyEventHandler(this.uGridHangTien_KeyUp);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridTax);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(927, 269);
            // 
            // uGridTax
            // 
            this.uGridTax.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridTax.Location = new System.Drawing.Point(0, 0);
            this.uGridTax.Name = "uGridTax";
            this.uGridTax.Size = new System.Drawing.Size(927, 269);
            this.uGridTax.TabIndex = 0;
            this.uGridTax.Text = "ultraGrid1";
            this.uGridTax.AfterExitEditMode += new System.EventHandler(this.uGridTax_AfterExitEditMode);
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.uGridThongKe);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(927, 269);
            // 
            // uGridThongKe
            // 
            this.uGridThongKe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridThongKe.Location = new System.Drawing.Point(0, 0);
            this.uGridThongKe.Name = "uGridThongKe";
            this.uGridThongKe.Size = new System.Drawing.Size(927, 269);
            this.uGridThongKe.TabIndex = 1;
            this.uGridThongKe.Text = "ultraGrid1";
            this.uGridThongKe.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uGridThongKe.AfterExitEditMode += new System.EventHandler(this.uGridThongKe_AfterExitEditMode);
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.uGridChungTuChiPhi);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(927, 269);
            // 
            // uGridChungTuChiPhi
            // 
            this.uGridChungTuChiPhi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridChungTuChiPhi.Location = new System.Drawing.Point(0, 0);
            this.uGridChungTuChiPhi.Name = "uGridChungTuChiPhi";
            this.uGridChungTuChiPhi.Size = new System.Drawing.Size(927, 269);
            this.uGridChungTuChiPhi.TabIndex = 2;
            this.uGridChungTuChiPhi.Text = "ultraGrid1";
            this.uGridChungTuChiPhi.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // ultraTabPageControl5
            // 
            this.ultraTabPageControl5.Controls.Add(this.palTopSctNhtNctPXK);
            this.ultraTabPageControl5.Controls.Add(this.ultraGroupBox1);
            this.ultraTabPageControl5.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl5.Name = "ultraTabPageControl5";
            this.ultraTabPageControl5.Size = new System.Drawing.Size(925, 280);
            // 
            // palTopSctNhtNctPXK
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.BackColor2 = System.Drawing.Color.Transparent;
            this.palTopSctNhtNctPXK.Appearance = appearance1;
            this.palTopSctNhtNctPXK.AutoSize = true;
            this.palTopSctNhtNctPXK.Location = new System.Drawing.Point(11, 7);
            this.palTopSctNhtNctPXK.Name = "palTopSctNhtNctPXK";
            this.palTopSctNhtNctPXK.Size = new System.Drawing.Size(391, 36);
            this.palTopSctNhtNctPXK.TabIndex = 71;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox1.Controls.Add(this.cbbAccountingObjectID);
            this.ultraGroupBox1.Controls.Add(this.uButtonPPOrder);
            this.ultraGroupBox1.Controls.Add(this.txtNumberAttach);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.txtReason);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox1.Controls.Add(this.lblReason);
            this.ultraGroupBox1.Controls.Add(this.txtContactName);
            this.ultraGroupBox1.Controls.Add(this.lblCompanyTaxCode);
            this.ultraGroupBox1.Controls.Add(this.txtAccoutingObjectName);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectAddress);
            this.ultraGroupBox1.Controls.Add(this.lblAccountingObjectAddress);
            this.ultraGroupBox1.Controls.Add(this.lblAccountingObjectID);
            appearance9.FontData.BoldAsString = "True";
            appearance9.FontData.SizeInPoints = 10F;
            this.ultraGroupBox1.HeaderAppearance = appearance9;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(5, 59);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(917, 144);
            this.ultraGroupBox1.TabIndex = 26;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbAccountingObjectID
            // 
            this.cbbAccountingObjectID.AllowNull = Infragistics.Win.DefaultableBoolean.True;
            this.cbbAccountingObjectID.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            appearance2.Image = global::Accounting.Frm.Properties.Resources.ubtnAdd4;
            editorButton1.Appearance = appearance2;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectID.ButtonsRight.Add(editorButton1);
            this.cbbAccountingObjectID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectID.LimitToList = true;
            this.cbbAccountingObjectID.Location = new System.Drawing.Point(83, 27);
            this.cbbAccountingObjectID.Name = "cbbAccountingObjectID";
            this.cbbAccountingObjectID.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID.Size = new System.Drawing.Size(202, 22);
            this.cbbAccountingObjectID.TabIndex = 41;
            this.cbbAccountingObjectID.ValueChanged += new System.EventHandler(this.cbbAccountingObjectID_ValueChanged);
            this.cbbAccountingObjectID.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbAccountingObjectID_ItemNotInList);
            // 
            // uButtonPPOrder
            // 
            this.uButtonPPOrder.Location = new System.Drawing.Point(510, 26);
            this.uButtonPPOrder.Name = "uButtonPPOrder";
            this.uButtonPPOrder.Size = new System.Drawing.Size(75, 23);
            this.uButtonPPOrder.TabIndex = 33;
            this.uButtonPPOrder.Text = "Chọn";
            this.uButtonPPOrder.Click += new System.EventHandler(this.uButtonPPOrder_Click);
            // 
            // txtNumberAttach
            // 
            this.txtNumberAttach.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNumberAttach.Location = new System.Drawing.Point(83, 80);
            this.txtNumberAttach.Name = "txtNumberAttach";
            this.txtNumberAttach.Size = new System.Drawing.Size(688, 21);
            this.txtNumberAttach.TabIndex = 27;
            // 
            // ultraLabel2
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel2.Appearance = appearance3;
            this.ultraLabel2.Location = new System.Drawing.Point(12, 82);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(104, 19);
            this.ultraLabel2.TabIndex = 26;
            this.ultraLabel2.Text = "Kèm theo";
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason.Location = new System.Drawing.Point(670, 58);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(226, 21);
            this.txtReason.TabIndex = 27;
            // 
            // ultraLabel3
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel3.Appearance = appearance4;
            this.ultraLabel3.Location = new System.Drawing.Point(789, 84);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(102, 19);
            this.ultraLabel3.TabIndex = 26;
            this.ultraLabel3.Text = "Chứng từ gốc";
            // 
            // lblReason
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            this.lblReason.Appearance = appearance5;
            this.lblReason.Location = new System.Drawing.Point(605, 62);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(68, 19);
            this.lblReason.TabIndex = 26;
            this.lblReason.Text = "Diễn giải";
            // 
            // txtContactName
            // 
            this.txtContactName.Location = new System.Drawing.Point(83, 54);
            this.txtContactName.Name = "txtContactName";
            this.txtContactName.Size = new System.Drawing.Size(516, 21);
            this.txtContactName.TabIndex = 25;
            // 
            // lblCompanyTaxCode
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            this.lblCompanyTaxCode.Appearance = appearance6;
            this.lblCompanyTaxCode.Location = new System.Drawing.Point(9, 57);
            this.lblCompanyTaxCode.Name = "lblCompanyTaxCode";
            this.lblCompanyTaxCode.Size = new System.Drawing.Size(67, 19);
            this.lblCompanyTaxCode.TabIndex = 24;
            this.lblCompanyTaxCode.Text = "Người giao";
            // 
            // txtAccoutingObjectName
            // 
            this.txtAccoutingObjectName.Location = new System.Drawing.Point(291, 28);
            this.txtAccoutingObjectName.Name = "txtAccoutingObjectName";
            this.txtAccoutingObjectName.Size = new System.Drawing.Size(213, 21);
            this.txtAccoutingObjectName.TabIndex = 23;
            // 
            // txtAccountingObjectAddress
            // 
            this.txtAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddress.Location = new System.Drawing.Point(670, 29);
            this.txtAccountingObjectAddress.Name = "txtAccountingObjectAddress";
            this.txtAccountingObjectAddress.Size = new System.Drawing.Size(226, 21);
            this.txtAccountingObjectAddress.TabIndex = 23;
            // 
            // lblAccountingObjectAddress
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectAddress.Appearance = appearance7;
            this.lblAccountingObjectAddress.Location = new System.Drawing.Point(605, 29);
            this.lblAccountingObjectAddress.Name = "lblAccountingObjectAddress";
            this.lblAccountingObjectAddress.Size = new System.Drawing.Size(68, 19);
            this.lblAccountingObjectAddress.TabIndex = 22;
            this.lblAccountingObjectAddress.Text = "Địa chỉ";
            // 
            // lblAccountingObjectID
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectID.Appearance = appearance8;
            this.lblAccountingObjectID.Location = new System.Drawing.Point(9, 30);
            this.lblAccountingObjectID.Name = "lblAccountingObjectID";
            this.lblAccountingObjectID.Size = new System.Drawing.Size(68, 19);
            this.lblAccountingObjectID.TabIndex = 0;
            this.lblAccountingObjectID.Text = "Đối tượng";
            // 
            // ultraTabPageControl6
            // 
            this.ultraTabPageControl6.Controls.Add(this.ultraGroupBox3);
            this.ultraTabPageControl6.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl6.Name = "ultraTabPageControl6";
            this.ultraTabPageControl6.Size = new System.Drawing.Size(925, 280);
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.palTopSctNhtNctPXK1);
            this.ultraGroupBox3.Controls.Add(this.cbbAccountingObjectID1);
            this.ultraGroupBox3.Controls.Add(this.ultraButton1);
            this.ultraGroupBox3.Controls.Add(this.ultraTextEditor2);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox3.Controls.Add(this.txtReason1);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox3.Controls.Add(this.txtReceiver);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox3.Controls.Add(this.txtAccoutingObjectName1);
            this.ultraGroupBox3.Controls.Add(this.txtAccountingObjectAddress1);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel11);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel12);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance18.FontData.BoldAsString = "True";
            appearance18.FontData.SizeInPoints = 10F;
            this.ultraGroupBox3.HeaderAppearance = appearance18;
            this.ultraGroupBox3.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(925, 280);
            this.ultraGroupBox3.TabIndex = 28;
            this.ultraGroupBox3.Text = "Thông tin chung";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // palTopSctNhtNctPXK1
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.BackColor2 = System.Drawing.Color.Transparent;
            this.palTopSctNhtNctPXK1.Appearance = appearance10;
            this.palTopSctNhtNctPXK1.AutoSize = true;
            this.palTopSctNhtNctPXK1.Location = new System.Drawing.Point(14, 33);
            this.palTopSctNhtNctPXK1.Name = "palTopSctNhtNctPXK1";
            this.palTopSctNhtNctPXK1.Size = new System.Drawing.Size(391, 36);
            this.palTopSctNhtNctPXK1.TabIndex = 71;
            // 
            // cbbAccountingObjectID1
            // 
            appearance11.Image = global::Accounting.Frm.Properties.Resources.ubtnAdd4;
            editorButton2.Appearance = appearance11;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectID1.ButtonsRight.Add(editorButton2);
            this.cbbAccountingObjectID1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectID1.LimitToList = true;
            this.cbbAccountingObjectID1.Location = new System.Drawing.Point(85, 94);
            this.cbbAccountingObjectID1.Name = "cbbAccountingObjectID1";
            this.cbbAccountingObjectID1.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID1.Size = new System.Drawing.Size(298, 22);
            this.cbbAccountingObjectID1.TabIndex = 40;
            this.cbbAccountingObjectID1.ValueChanged += new System.EventHandler(this.cbbAccountingObjectID1_ValueChanged);
            // 
            // ultraButton1
            // 
            this.ultraButton1.Location = new System.Drawing.Point(608, 94);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(75, 23);
            this.ultraButton1.TabIndex = 33;
            this.ultraButton1.Text = "Chọn";
            // 
            // ultraTextEditor2
            // 
            this.ultraTextEditor2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraTextEditor2.Location = new System.Drawing.Point(85, 228);
            this.ultraTextEditor2.Name = "ultraTextEditor2";
            this.ultraTextEditor2.Size = new System.Drawing.Size(604, 21);
            this.ultraTextEditor2.TabIndex = 27;
            // 
            // ultraLabel7
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel7.Appearance = appearance12;
            this.ultraLabel7.Location = new System.Drawing.Point(14, 229);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(104, 19);
            this.ultraLabel7.TabIndex = 26;
            this.ultraLabel7.Text = "Kèm theo";
            // 
            // txtReason1
            // 
            this.txtReason1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason1.Location = new System.Drawing.Point(85, 194);
            this.txtReason1.Name = "txtReason1";
            this.txtReason1.Size = new System.Drawing.Size(713, 21);
            this.txtReason1.TabIndex = 27;
            // 
            // ultraLabel8
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel8.Appearance = appearance13;
            this.ultraLabel8.Location = new System.Drawing.Point(582, 233);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(102, 19);
            this.ultraLabel8.TabIndex = 26;
            this.ultraLabel8.Text = "Chứng từ gốc";
            // 
            // ultraLabel9
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel9.Appearance = appearance14;
            this.ultraLabel9.Location = new System.Drawing.Point(14, 195);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel9.TabIndex = 26;
            this.ultraLabel9.Text = "Lý do chi";
            // 
            // txtReceiver
            // 
            this.txtReceiver.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReceiver.Location = new System.Drawing.Point(85, 160);
            this.txtReceiver.Name = "txtReceiver";
            this.txtReceiver.Size = new System.Drawing.Size(713, 21);
            this.txtReceiver.TabIndex = 25;
            // 
            // ultraLabel10
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel10.Appearance = appearance15;
            this.ultraLabel10.Location = new System.Drawing.Point(11, 161);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(67, 19);
            this.ultraLabel10.TabIndex = 24;
            this.ultraLabel10.Text = "Người nhận";
            // 
            // txtAccoutingObjectName1
            // 
            this.txtAccoutingObjectName1.Location = new System.Drawing.Point(389, 95);
            this.txtAccoutingObjectName1.Name = "txtAccoutingObjectName1";
            this.txtAccoutingObjectName1.Size = new System.Drawing.Size(213, 21);
            this.txtAccoutingObjectName1.TabIndex = 23;
            // 
            // txtAccountingObjectAddress1
            // 
            this.txtAccountingObjectAddress1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddress1.Location = new System.Drawing.Point(85, 125);
            this.txtAccountingObjectAddress1.Name = "txtAccountingObjectAddress1";
            this.txtAccountingObjectAddress1.Size = new System.Drawing.Size(713, 21);
            this.txtAccountingObjectAddress1.TabIndex = 23;
            // 
            // ultraLabel11
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel11.Appearance = appearance16;
            this.ultraLabel11.Location = new System.Drawing.Point(14, 126);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel11.TabIndex = 22;
            this.ultraLabel11.Text = "Địa chỉ";
            // 
            // ultraLabel12
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel12.Appearance = appearance17;
            this.ultraLabel12.Location = new System.Drawing.Point(11, 97);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel12.TabIndex = 0;
            this.ultraLabel12.Text = "Đối tượng";
            // 
            // ultraTabPageControl7
            // 
            this.ultraTabPageControl7.Controls.Add(this.palTopSctNhtNctPXK2);
            this.ultraTabPageControl7.Controls.Add(this.ultraGroupBox4);
            this.ultraTabPageControl7.Controls.Add(this.ultraGroupBox5);
            this.ultraTabPageControl7.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl7.Name = "ultraTabPageControl7";
            this.ultraTabPageControl7.Size = new System.Drawing.Size(925, 280);
            // 
            // palTopSctNhtNctPXK2
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.BackColor2 = System.Drawing.Color.Transparent;
            this.palTopSctNhtNctPXK2.Appearance = appearance19;
            this.palTopSctNhtNctPXK2.AutoSize = true;
            this.palTopSctNhtNctPXK2.Location = new System.Drawing.Point(7, 8);
            this.palTopSctNhtNctPXK2.Name = "palTopSctNhtNctPXK2";
            this.palTopSctNhtNctPXK2.Size = new System.Drawing.Size(391, 36);
            this.palTopSctNhtNctPXK2.TabIndex = 71;
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox4.Controls.Add(this.cbbAccountingObjectID2);
            this.ultraGroupBox4.Controls.Add(this.txtAccountingObjectAddress2);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel16);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel17);
            this.ultraGroupBox4.Controls.Add(this.txtAccoutingObjectName2);
            this.ultraGroupBox4.Controls.Add(this.ultraButton2);
            this.ultraGroupBox4.Controls.Add(this.cbbBankAccountingObject2);
            this.ultraGroupBox4.Controls.Add(this.txtBankAccountingObjectName2);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel18);
            appearance24.FontData.BoldAsString = "True";
            appearance24.FontData.SizeInPoints = 10F;
            this.ultraGroupBox4.HeaderAppearance = appearance24;
            this.ultraGroupBox4.Location = new System.Drawing.Point(4, 153);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(918, 112);
            this.ultraGroupBox4.TabIndex = 64;
            this.ultraGroupBox4.Text = "Đối tượng nhận tiền";
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbAccountingObjectID2
            // 
            appearance20.Image = global::Accounting.Frm.Properties.Resources.ubtnAdd4;
            editorButton3.Appearance = appearance20;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectID2.ButtonsRight.Add(editorButton3);
            this.cbbAccountingObjectID2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectID2.LimitToList = true;
            this.cbbAccountingObjectID2.Location = new System.Drawing.Point(94, 19);
            this.cbbAccountingObjectID2.Name = "cbbAccountingObjectID2";
            this.cbbAccountingObjectID2.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID2.Size = new System.Drawing.Size(202, 22);
            this.cbbAccountingObjectID2.TabIndex = 42;
            this.cbbAccountingObjectID2.ValueChanged += new System.EventHandler(this.cbbAccountingObjectID2_ValueChanged);
            // 
            // txtAccountingObjectAddress2
            // 
            this.txtAccountingObjectAddress2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddress2.Location = new System.Drawing.Point(94, 47);
            this.txtAccountingObjectAddress2.Name = "txtAccountingObjectAddress2";
            this.txtAccountingObjectAddress2.Size = new System.Drawing.Size(811, 21);
            this.txtAccountingObjectAddress2.TabIndex = 23;
            // 
            // ultraLabel16
            // 
            appearance21.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel16.Appearance = appearance21;
            this.ultraLabel16.Location = new System.Drawing.Point(23, 23);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel16.TabIndex = 0;
            this.ultraLabel16.Text = "Đối tượng";
            // 
            // ultraLabel17
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel17.Appearance = appearance22;
            this.ultraLabel17.Location = new System.Drawing.Point(23, 48);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel17.TabIndex = 22;
            this.ultraLabel17.Text = "Địa chỉ";
            // 
            // txtAccoutingObjectName2
            // 
            this.txtAccoutingObjectName2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccoutingObjectName2.Location = new System.Drawing.Point(305, 22);
            this.txtAccoutingObjectName2.Name = "txtAccoutingObjectName2";
            this.txtAccoutingObjectName2.Size = new System.Drawing.Size(516, 21);
            this.txtAccoutingObjectName2.TabIndex = 25;
            // 
            // ultraButton2
            // 
            this.ultraButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton2.Location = new System.Drawing.Point(830, 22);
            this.ultraButton2.Name = "ultraButton2";
            this.ultraButton2.Size = new System.Drawing.Size(75, 23);
            this.ultraButton2.TabIndex = 33;
            this.ultraButton2.Text = "Chọn";
            // 
            // cbbBankAccountingObject2
            // 
            this.cbbBankAccountingObject2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbBankAccountingObject2.Location = new System.Drawing.Point(94, 74);
            this.cbbBankAccountingObject2.Name = "cbbBankAccountingObject2";
            this.cbbBankAccountingObject2.NullText = "<chọn dữ liệu>";
            this.cbbBankAccountingObject2.Size = new System.Drawing.Size(158, 22);
            this.cbbBankAccountingObject2.TabIndex = 31;
            this.cbbBankAccountingObject2.ValueChanged += new System.EventHandler(this.cbbBankAccountingObject2_ValueChanged);
            // 
            // txtBankAccountingObjectName2
            // 
            this.txtBankAccountingObjectName2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBankAccountingObjectName2.Location = new System.Drawing.Point(270, 75);
            this.txtBankAccountingObjectName2.Name = "txtBankAccountingObjectName2";
            this.txtBankAccountingObjectName2.Size = new System.Drawing.Size(635, 21);
            this.txtBankAccountingObjectName2.TabIndex = 23;
            // 
            // ultraLabel18
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel18.Appearance = appearance23;
            this.ultraLabel18.Location = new System.Drawing.Point(23, 76);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel18.TabIndex = 0;
            this.ultraLabel18.Text = "Tài khoản";
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox5.Controls.Add(this.cbbBankAccountObject2);
            this.ultraGroupBox5.Controls.Add(this.txtBankAccountObjectName2);
            this.ultraGroupBox5.Controls.Add(this.txtReason2);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel19);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel20);
            appearance27.FontData.BoldAsString = "True";
            appearance27.FontData.SizeInPoints = 10F;
            this.ultraGroupBox5.HeaderAppearance = appearance27;
            this.ultraGroupBox5.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox5.Location = new System.Drawing.Point(1, 55);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(921, 95);
            this.ultraGroupBox5.TabIndex = 57;
            this.ultraGroupBox5.Text = "Đơn vị trả tiền";
            this.ultraGroupBox5.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbBankAccountObject2
            // 
            this.cbbBankAccountObject2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbBankAccountObject2.Location = new System.Drawing.Point(94, 29);
            this.cbbBankAccountObject2.Name = "cbbBankAccountObject2";
            this.cbbBankAccountObject2.NullText = "<chọn dữ liệu>";
            this.cbbBankAccountObject2.Size = new System.Drawing.Size(158, 22);
            this.cbbBankAccountObject2.TabIndex = 31;
            this.cbbBankAccountObject2.ValueChanged += new System.EventHandler(this.cbbBankAccountObject2_ValueChanged);
            // 
            // txtBankAccountObjectName2
            // 
            this.txtBankAccountObjectName2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBankAccountObjectName2.Location = new System.Drawing.Point(270, 30);
            this.txtBankAccountObjectName2.Name = "txtBankAccountObjectName2";
            this.txtBankAccountObjectName2.Size = new System.Drawing.Size(635, 21);
            this.txtBankAccountObjectName2.TabIndex = 23;
            // 
            // txtReason2
            // 
            this.txtReason2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason2.Location = new System.Drawing.Point(94, 59);
            this.txtReason2.Name = "txtReason2";
            this.txtReason2.Size = new System.Drawing.Size(811, 21);
            this.txtReason2.TabIndex = 23;
            // 
            // ultraLabel19
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel19.Appearance = appearance25;
            this.ultraLabel19.Location = new System.Drawing.Point(23, 60);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel19.TabIndex = 22;
            this.ultraLabel19.Text = "Nội dung TT";
            // 
            // ultraLabel20
            // 
            appearance26.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel20.Appearance = appearance26;
            this.ultraLabel20.Location = new System.Drawing.Point(23, 31);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel20.TabIndex = 0;
            this.ultraLabel20.Text = "Tài khoản";
            // 
            // ultraTabPageControl8
            // 
            this.ultraTabPageControl8.Controls.Add(this.palTopSctNhtNctPXK3);
            this.ultraTabPageControl8.Controls.Add(this.ultraGroupBox6);
            this.ultraTabPageControl8.Controls.Add(this.ultraGroupBox7);
            this.ultraTabPageControl8.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl8.Name = "ultraTabPageControl8";
            this.ultraTabPageControl8.Size = new System.Drawing.Size(925, 280);
            // 
            // palTopSctNhtNctPXK3
            // 
            appearance28.BackColor = System.Drawing.Color.Transparent;
            appearance28.BackColor2 = System.Drawing.Color.Transparent;
            this.palTopSctNhtNctPXK3.Appearance = appearance28;
            this.palTopSctNhtNctPXK3.AutoSize = true;
            this.palTopSctNhtNctPXK3.Location = new System.Drawing.Point(6, 5);
            this.palTopSctNhtNctPXK3.Name = "palTopSctNhtNctPXK3";
            this.palTopSctNhtNctPXK3.Size = new System.Drawing.Size(391, 36);
            this.palTopSctNhtNctPXK3.TabIndex = 73;
            // 
            // ultraGroupBox6
            // 
            this.ultraGroupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox6.Controls.Add(this.cbbAccountingObjectID3);
            this.ultraGroupBox6.Controls.Add(this.txtAccountingObjectAddress3);
            this.ultraGroupBox6.Controls.Add(this.ultraLabel24);
            this.ultraGroupBox6.Controls.Add(this.ultraLabel25);
            this.ultraGroupBox6.Controls.Add(this.txtAccoutingObjectName3);
            this.ultraGroupBox6.Controls.Add(this.ultraButton4);
            this.ultraGroupBox6.Controls.Add(this.cbbBankAccountingObject3);
            this.ultraGroupBox6.Controls.Add(this.txtBankAccountingObjectName3);
            this.ultraGroupBox6.Controls.Add(this.ultraLabel26);
            appearance33.FontData.BoldAsString = "True";
            appearance33.FontData.SizeInPoints = 10F;
            this.ultraGroupBox6.HeaderAppearance = appearance33;
            this.ultraGroupBox6.Location = new System.Drawing.Point(6, 154);
            this.ultraGroupBox6.Name = "ultraGroupBox6";
            this.ultraGroupBox6.Size = new System.Drawing.Size(916, 112);
            this.ultraGroupBox6.TabIndex = 72;
            this.ultraGroupBox6.Text = "Đối tượng nhận tiền";
            this.ultraGroupBox6.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbAccountingObjectID3
            // 
            appearance29.Image = global::Accounting.Frm.Properties.Resources.ubtnAdd4;
            editorButton4.Appearance = appearance29;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectID3.ButtonsRight.Add(editorButton4);
            this.cbbAccountingObjectID3.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectID3.LimitToList = true;
            this.cbbAccountingObjectID3.Location = new System.Drawing.Point(94, 21);
            this.cbbAccountingObjectID3.Name = "cbbAccountingObjectID3";
            this.cbbAccountingObjectID3.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID3.Size = new System.Drawing.Size(202, 22);
            this.cbbAccountingObjectID3.TabIndex = 43;
            this.cbbAccountingObjectID3.ValueChanged += new System.EventHandler(this.cbbAccountingObjectID3_ValueChanged);
            // 
            // txtAccountingObjectAddress3
            // 
            this.txtAccountingObjectAddress3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddress3.Location = new System.Drawing.Point(94, 47);
            this.txtAccountingObjectAddress3.Name = "txtAccountingObjectAddress3";
            this.txtAccountingObjectAddress3.Size = new System.Drawing.Size(809, 21);
            this.txtAccountingObjectAddress3.TabIndex = 23;
            // 
            // ultraLabel24
            // 
            appearance30.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel24.Appearance = appearance30;
            this.ultraLabel24.Location = new System.Drawing.Point(23, 23);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel24.TabIndex = 0;
            this.ultraLabel24.Text = "Đối tượng";
            // 
            // ultraLabel25
            // 
            appearance31.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel25.Appearance = appearance31;
            this.ultraLabel25.Location = new System.Drawing.Point(23, 48);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel25.TabIndex = 22;
            this.ultraLabel25.Text = "Địa chỉ";
            // 
            // txtAccoutingObjectName3
            // 
            this.txtAccoutingObjectName3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccoutingObjectName3.Location = new System.Drawing.Point(305, 22);
            this.txtAccoutingObjectName3.Name = "txtAccoutingObjectName3";
            this.txtAccoutingObjectName3.Size = new System.Drawing.Size(514, 21);
            this.txtAccoutingObjectName3.TabIndex = 25;
            // 
            // ultraButton4
            // 
            this.ultraButton4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton4.Location = new System.Drawing.Point(828, 22);
            this.ultraButton4.Name = "ultraButton4";
            this.ultraButton4.Size = new System.Drawing.Size(75, 23);
            this.ultraButton4.TabIndex = 33;
            this.ultraButton4.Text = "Chọn";
            // 
            // cbbBankAccountingObject3
            // 
            this.cbbBankAccountingObject3.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbBankAccountingObject3.Location = new System.Drawing.Point(94, 74);
            this.cbbBankAccountingObject3.Name = "cbbBankAccountingObject3";
            this.cbbBankAccountingObject3.NullText = "<chọn dữ liệu>";
            this.cbbBankAccountingObject3.Size = new System.Drawing.Size(158, 22);
            this.cbbBankAccountingObject3.TabIndex = 31;
            this.cbbBankAccountingObject3.ValueChanged += new System.EventHandler(this.cbbBankAccountingObject3_ValueChanged);
            // 
            // txtBankAccountingObjectName3
            // 
            this.txtBankAccountingObjectName3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBankAccountingObjectName3.Location = new System.Drawing.Point(270, 75);
            this.txtBankAccountingObjectName3.Name = "txtBankAccountingObjectName3";
            this.txtBankAccountingObjectName3.Size = new System.Drawing.Size(633, 21);
            this.txtBankAccountingObjectName3.TabIndex = 23;
            // 
            // ultraLabel26
            // 
            appearance32.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel26.Appearance = appearance32;
            this.ultraLabel26.Location = new System.Drawing.Point(23, 76);
            this.ultraLabel26.Name = "ultraLabel26";
            this.ultraLabel26.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel26.TabIndex = 0;
            this.ultraLabel26.Text = "Tài khoản";
            // 
            // ultraGroupBox7
            // 
            this.ultraGroupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox7.Controls.Add(this.cbbBankAccountObject3);
            this.ultraGroupBox7.Controls.Add(this.txtBankAccountObjectName3);
            this.ultraGroupBox7.Controls.Add(this.txtReason3);
            this.ultraGroupBox7.Controls.Add(this.ultraLabel27);
            this.ultraGroupBox7.Controls.Add(this.ultraLabel28);
            appearance36.FontData.BoldAsString = "True";
            appearance36.FontData.SizeInPoints = 10F;
            this.ultraGroupBox7.HeaderAppearance = appearance36;
            this.ultraGroupBox7.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox7.Location = new System.Drawing.Point(3, 56);
            this.ultraGroupBox7.Name = "ultraGroupBox7";
            this.ultraGroupBox7.Size = new System.Drawing.Size(919, 95);
            this.ultraGroupBox7.TabIndex = 65;
            this.ultraGroupBox7.Text = "Đơn vị trả tiền";
            this.ultraGroupBox7.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbBankAccountObject3
            // 
            this.cbbBankAccountObject3.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbBankAccountObject3.Location = new System.Drawing.Point(94, 29);
            this.cbbBankAccountObject3.Name = "cbbBankAccountObject3";
            this.cbbBankAccountObject3.NullText = "<chọn dữ liệu>";
            this.cbbBankAccountObject3.Size = new System.Drawing.Size(158, 22);
            this.cbbBankAccountObject3.TabIndex = 31;
            this.cbbBankAccountObject3.ValueChanged += new System.EventHandler(this.cbbBankAccountObject3_ValueChanged);
            // 
            // txtBankAccountObjectName3
            // 
            this.txtBankAccountObjectName3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBankAccountObjectName3.Location = new System.Drawing.Point(270, 30);
            this.txtBankAccountObjectName3.Name = "txtBankAccountObjectName3";
            this.txtBankAccountObjectName3.Size = new System.Drawing.Size(633, 21);
            this.txtBankAccountObjectName3.TabIndex = 23;
            // 
            // txtReason3
            // 
            this.txtReason3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason3.Location = new System.Drawing.Point(94, 59);
            this.txtReason3.Name = "txtReason3";
            this.txtReason3.Size = new System.Drawing.Size(809, 21);
            this.txtReason3.TabIndex = 23;
            // 
            // ultraLabel27
            // 
            appearance34.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel27.Appearance = appearance34;
            this.ultraLabel27.Location = new System.Drawing.Point(23, 60);
            this.ultraLabel27.Name = "ultraLabel27";
            this.ultraLabel27.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel27.TabIndex = 22;
            this.ultraLabel27.Text = "Nội dung TT";
            // 
            // ultraLabel28
            // 
            appearance35.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel28.Appearance = appearance35;
            this.ultraLabel28.Location = new System.Drawing.Point(23, 31);
            this.ultraLabel28.Name = "ultraLabel28";
            this.ultraLabel28.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel28.TabIndex = 0;
            this.ultraLabel28.Text = "Tài khoản";
            // 
            // ultraTabPageControl9
            // 
            this.ultraTabPageControl9.Controls.Add(this.palTopSctNhtNctPXK4);
            this.ultraTabPageControl9.Controls.Add(this.ultraGroupBox8);
            this.ultraTabPageControl9.Controls.Add(this.ultraGroupBox9);
            this.ultraTabPageControl9.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl9.Name = "ultraTabPageControl9";
            this.ultraTabPageControl9.Size = new System.Drawing.Size(925, 280);
            // 
            // palTopSctNhtNctPXK4
            // 
            appearance37.BackColor = System.Drawing.Color.Transparent;
            appearance37.BackColor2 = System.Drawing.Color.Transparent;
            this.palTopSctNhtNctPXK4.Appearance = appearance37;
            this.palTopSctNhtNctPXK4.AutoSize = true;
            this.palTopSctNhtNctPXK4.Location = new System.Drawing.Point(7, 7);
            this.palTopSctNhtNctPXK4.Name = "palTopSctNhtNctPXK4";
            this.palTopSctNhtNctPXK4.Size = new System.Drawing.Size(391, 36);
            this.palTopSctNhtNctPXK4.TabIndex = 81;
            // 
            // ultraGroupBox8
            // 
            this.ultraGroupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox8.Controls.Add(this.dteIssueDate4);
            this.ultraGroupBox8.Controls.Add(this.cbbAccountingObjectID4);
            this.ultraGroupBox8.Controls.Add(this.txtAccountingObjectAddress4);
            this.ultraGroupBox8.Controls.Add(this.ultraLabel32);
            this.ultraGroupBox8.Controls.Add(this.ultraLabel33);
            this.ultraGroupBox8.Controls.Add(this.txtAccoutingObjectName4);
            this.ultraGroupBox8.Controls.Add(this.ultraButton6);
            this.ultraGroupBox8.Controls.Add(this.txtIdentificationNo4);
            this.ultraGroupBox8.Controls.Add(this.txtIssueBy4);
            this.ultraGroupBox8.Controls.Add(this.ultraLabel46);
            this.ultraGroupBox8.Controls.Add(this.ultraLabel45);
            this.ultraGroupBox8.Controls.Add(this.ultraLabel34);
            appearance45.FontData.BoldAsString = "True";
            appearance45.FontData.SizeInPoints = 10F;
            this.ultraGroupBox8.HeaderAppearance = appearance45;
            this.ultraGroupBox8.Location = new System.Drawing.Point(3, 152);
            this.ultraGroupBox8.Name = "ultraGroupBox8";
            this.ultraGroupBox8.Size = new System.Drawing.Size(919, 112);
            this.ultraGroupBox8.TabIndex = 80;
            this.ultraGroupBox8.Text = "Đối tượng nhận tiền";
            this.ultraGroupBox8.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // dteIssueDate4
            // 
            appearance38.TextHAlignAsString = "Center";
            appearance38.TextVAlignAsString = "Middle";
            this.dteIssueDate4.Appearance = appearance38;
            this.dteIssueDate4.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteIssueDate4.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteIssueDate4.Location = new System.Drawing.Point(315, 74);
            this.dteIssueDate4.MaskInput = "";
            this.dteIssueDate4.Name = "dteIssueDate4";
            this.dteIssueDate4.Size = new System.Drawing.Size(98, 21);
            this.dteIssueDate4.TabIndex = 78;
            this.dteIssueDate4.Value = null;
            // 
            // cbbAccountingObjectID4
            // 
            appearance39.Image = global::Accounting.Frm.Properties.Resources.ubtnAdd4;
            editorButton5.Appearance = appearance39;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectID4.ButtonsRight.Add(editorButton5);
            this.cbbAccountingObjectID4.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectID4.LimitToList = true;
            this.cbbAccountingObjectID4.Location = new System.Drawing.Point(94, 22);
            this.cbbAccountingObjectID4.Name = "cbbAccountingObjectID4";
            this.cbbAccountingObjectID4.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID4.Size = new System.Drawing.Size(202, 22);
            this.cbbAccountingObjectID4.TabIndex = 43;
            this.cbbAccountingObjectID4.ValueChanged += new System.EventHandler(this.cbbAccountingObjectID4_ValueChanged);
            // 
            // txtAccountingObjectAddress4
            // 
            this.txtAccountingObjectAddress4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddress4.Location = new System.Drawing.Point(94, 47);
            this.txtAccountingObjectAddress4.Name = "txtAccountingObjectAddress4";
            this.txtAccountingObjectAddress4.Size = new System.Drawing.Size(812, 21);
            this.txtAccountingObjectAddress4.TabIndex = 23;
            // 
            // ultraLabel32
            // 
            appearance40.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel32.Appearance = appearance40;
            this.ultraLabel32.Location = new System.Drawing.Point(23, 23);
            this.ultraLabel32.Name = "ultraLabel32";
            this.ultraLabel32.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel32.TabIndex = 0;
            this.ultraLabel32.Text = "Đối tượng";
            // 
            // ultraLabel33
            // 
            appearance41.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel33.Appearance = appearance41;
            this.ultraLabel33.Location = new System.Drawing.Point(23, 48);
            this.ultraLabel33.Name = "ultraLabel33";
            this.ultraLabel33.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel33.TabIndex = 22;
            this.ultraLabel33.Text = "Địa chỉ";
            // 
            // txtAccoutingObjectName4
            // 
            this.txtAccoutingObjectName4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccoutingObjectName4.Location = new System.Drawing.Point(305, 22);
            this.txtAccoutingObjectName4.Name = "txtAccoutingObjectName4";
            this.txtAccoutingObjectName4.Size = new System.Drawing.Size(517, 21);
            this.txtAccoutingObjectName4.TabIndex = 25;
            // 
            // ultraButton6
            // 
            this.ultraButton6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton6.Location = new System.Drawing.Point(831, 22);
            this.ultraButton6.Name = "ultraButton6";
            this.ultraButton6.Size = new System.Drawing.Size(75, 23);
            this.ultraButton6.TabIndex = 33;
            this.ultraButton6.Text = "Chọn";
            // 
            // txtIdentificationNo4
            // 
            this.txtIdentificationNo4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIdentificationNo4.Location = new System.Drawing.Point(94, 75);
            this.txtIdentificationNo4.Name = "txtIdentificationNo4";
            this.txtIdentificationNo4.Size = new System.Drawing.Size(157, 21);
            this.txtIdentificationNo4.TabIndex = 23;
            // 
            // txtIssueBy4
            // 
            this.txtIssueBy4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIssueBy4.Location = new System.Drawing.Point(521, 75);
            this.txtIssueBy4.Name = "txtIssueBy4";
            this.txtIssueBy4.Size = new System.Drawing.Size(385, 21);
            this.txtIssueBy4.TabIndex = 23;
            // 
            // ultraLabel46
            // 
            appearance42.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel46.Appearance = appearance42;
            this.ultraLabel46.Location = new System.Drawing.Point(420, 77);
            this.ultraLabel46.Name = "ultraLabel46";
            this.ultraLabel46.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel46.TabIndex = 0;
            this.ultraLabel46.Text = "Nơi cấp";
            // 
            // ultraLabel45
            // 
            appearance43.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel45.Appearance = appearance43;
            this.ultraLabel45.Location = new System.Drawing.Point(257, 78);
            this.ultraLabel45.Name = "ultraLabel45";
            this.ultraLabel45.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel45.TabIndex = 0;
            this.ultraLabel45.Text = "Ngày cấp";
            // 
            // ultraLabel34
            // 
            appearance44.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel34.Appearance = appearance44;
            this.ultraLabel34.Location = new System.Drawing.Point(23, 76);
            this.ultraLabel34.Name = "ultraLabel34";
            this.ultraLabel34.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel34.TabIndex = 0;
            this.ultraLabel34.Text = "Số CMND";
            // 
            // ultraGroupBox9
            // 
            this.ultraGroupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox9.Controls.Add(this.cbbBankAccountObject4);
            this.ultraGroupBox9.Controls.Add(this.txtBankAccountObjectName4);
            this.ultraGroupBox9.Controls.Add(this.txtReason4);
            this.ultraGroupBox9.Controls.Add(this.ultraLabel35);
            this.ultraGroupBox9.Controls.Add(this.ultraLabel36);
            appearance48.FontData.BoldAsString = "True";
            appearance48.FontData.SizeInPoints = 10F;
            this.ultraGroupBox9.HeaderAppearance = appearance48;
            this.ultraGroupBox9.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox9.Location = new System.Drawing.Point(0, 54);
            this.ultraGroupBox9.Name = "ultraGroupBox9";
            this.ultraGroupBox9.Size = new System.Drawing.Size(922, 95);
            this.ultraGroupBox9.TabIndex = 73;
            this.ultraGroupBox9.Text = "Đơn vị trả tiền";
            this.ultraGroupBox9.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbBankAccountObject4
            // 
            this.cbbBankAccountObject4.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbBankAccountObject4.Location = new System.Drawing.Point(94, 29);
            this.cbbBankAccountObject4.Name = "cbbBankAccountObject4";
            this.cbbBankAccountObject4.NullText = "<chọn dữ liệu>";
            this.cbbBankAccountObject4.Size = new System.Drawing.Size(158, 22);
            this.cbbBankAccountObject4.TabIndex = 31;
            this.cbbBankAccountObject4.ValueChanged += new System.EventHandler(this.cbbBankAccountObject4_ValueChanged);
            // 
            // txtBankAccountObjectName4
            // 
            this.txtBankAccountObjectName4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBankAccountObjectName4.Location = new System.Drawing.Point(270, 30);
            this.txtBankAccountObjectName4.Name = "txtBankAccountObjectName4";
            this.txtBankAccountObjectName4.Size = new System.Drawing.Size(636, 21);
            this.txtBankAccountObjectName4.TabIndex = 23;
            // 
            // txtReason4
            // 
            this.txtReason4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason4.Location = new System.Drawing.Point(94, 59);
            this.txtReason4.Name = "txtReason4";
            this.txtReason4.Size = new System.Drawing.Size(812, 21);
            this.txtReason4.TabIndex = 23;
            // 
            // ultraLabel35
            // 
            appearance46.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel35.Appearance = appearance46;
            this.ultraLabel35.Location = new System.Drawing.Point(23, 60);
            this.ultraLabel35.Name = "ultraLabel35";
            this.ultraLabel35.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel35.TabIndex = 22;
            this.ultraLabel35.Text = "Nội dung TT";
            // 
            // ultraLabel36
            // 
            appearance47.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel36.Appearance = appearance47;
            this.ultraLabel36.Location = new System.Drawing.Point(23, 31);
            this.ultraLabel36.Name = "ultraLabel36";
            this.ultraLabel36.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel36.TabIndex = 0;
            this.ultraLabel36.Text = "Tài khoản";
            // 
            // ultraTabPageControl10
            // 
            this.ultraTabPageControl10.Controls.Add(this.palTopSctNhtNctPXK5);
            this.ultraTabPageControl10.Controls.Add(this.ultraGroupBox10);
            this.ultraTabPageControl10.Controls.Add(this.ultraGroupBox11);
            this.ultraTabPageControl10.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl10.Name = "ultraTabPageControl10";
            this.ultraTabPageControl10.Size = new System.Drawing.Size(925, 280);
            // 
            // palTopSctNhtNctPXK5
            // 
            appearance49.BackColor = System.Drawing.Color.Transparent;
            appearance49.BackColor2 = System.Drawing.Color.Transparent;
            this.palTopSctNhtNctPXK5.Appearance = appearance49;
            this.palTopSctNhtNctPXK5.AutoSize = true;
            this.palTopSctNhtNctPXK5.Location = new System.Drawing.Point(6, 7);
            this.palTopSctNhtNctPXK5.Name = "palTopSctNhtNctPXK5";
            this.palTopSctNhtNctPXK5.Size = new System.Drawing.Size(391, 36);
            this.palTopSctNhtNctPXK5.TabIndex = 71;
            // 
            // ultraGroupBox10
            // 
            this.ultraGroupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox10.Controls.Add(this.txtOwnerCard);
            this.ultraGroupBox10.Controls.Add(this.lblOwnerCard);
            this.ultraGroupBox10.Controls.Add(this.cbbCreditCardNumber5);
            this.ultraGroupBox10.Controls.Add(this.txtReason5);
            this.ultraGroupBox10.Controls.Add(this.ultraLabel37);
            this.ultraGroupBox10.Controls.Add(this.ultraLabel38);
            appearance53.FontData.BoldAsString = "True";
            appearance53.FontData.SizeInPoints = 10F;
            this.ultraGroupBox10.HeaderAppearance = appearance53;
            this.ultraGroupBox10.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox10.Location = new System.Drawing.Point(3, 54);
            this.ultraGroupBox10.Name = "ultraGroupBox10";
            this.ultraGroupBox10.Size = new System.Drawing.Size(919, 95);
            this.ultraGroupBox10.TabIndex = 49;
            this.ultraGroupBox10.Text = "Thông tin chung";
            this.ultraGroupBox10.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtOwnerCard
            // 
            this.txtOwnerCard.AutoSize = true;
            this.txtOwnerCard.BackColor = System.Drawing.Color.Transparent;
            this.txtOwnerCard.Location = new System.Drawing.Point(354, 34);
            this.txtOwnerCard.Name = "txtOwnerCard";
            this.txtOwnerCard.Size = new System.Drawing.Size(10, 13);
            this.txtOwnerCard.TabIndex = 35;
            this.txtOwnerCard.Text = ":";
            // 
            // lblOwnerCard
            // 
            appearance50.BackColor = System.Drawing.Color.Transparent;
            this.lblOwnerCard.Appearance = appearance50;
            this.lblOwnerCard.Location = new System.Drawing.Point(308, 34);
            this.lblOwnerCard.Name = "lblOwnerCard";
            this.lblOwnerCard.Size = new System.Drawing.Size(56, 19);
            this.lblOwnerCard.TabIndex = 34;
            this.lblOwnerCard.Text = "Chủ thẻ";
            // 
            // cbbCreditCardNumber5
            // 
            this.cbbCreditCardNumber5.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCreditCardNumber5.Location = new System.Drawing.Point(97, 32);
            this.cbbCreditCardNumber5.Name = "cbbCreditCardNumber5";
            this.cbbCreditCardNumber5.NullText = "<chọn dữ liệu>";
            this.cbbCreditCardNumber5.Size = new System.Drawing.Size(158, 22);
            this.cbbCreditCardNumber5.TabIndex = 31;
            // 
            // txtReason5
            // 
            this.txtReason5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason5.Location = new System.Drawing.Point(97, 59);
            this.txtReason5.Name = "txtReason5";
            this.txtReason5.Size = new System.Drawing.Size(806, 21);
            this.txtReason5.TabIndex = 23;
            // 
            // ultraLabel37
            // 
            appearance51.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel37.Appearance = appearance51;
            this.ultraLabel37.Location = new System.Drawing.Point(24, 60);
            this.ultraLabel37.Name = "ultraLabel37";
            this.ultraLabel37.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel37.TabIndex = 22;
            this.ultraLabel37.Text = "Nội dung TT";
            // 
            // ultraLabel38
            // 
            appearance52.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel38.Appearance = appearance52;
            this.ultraLabel38.Location = new System.Drawing.Point(24, 34);
            this.ultraLabel38.Name = "ultraLabel38";
            this.ultraLabel38.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel38.TabIndex = 0;
            this.ultraLabel38.Text = "Số thẻ";
            // 
            // ultraGroupBox11
            // 
            this.ultraGroupBox11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox11.Controls.Add(this.cbbAccountingObjectID5);
            this.ultraGroupBox11.Controls.Add(this.txtAccountingObjectAddress5);
            this.ultraGroupBox11.Controls.Add(this.ultraLabel41);
            this.ultraGroupBox11.Controls.Add(this.ultraLabel42);
            this.ultraGroupBox11.Controls.Add(this.txtAccoutingObjectName5);
            this.ultraGroupBox11.Controls.Add(this.ultraButton8);
            this.ultraGroupBox11.Controls.Add(this.cbbBankAccountingObject5);
            this.ultraGroupBox11.Controls.Add(this.txtBankAccountingObjectName5);
            this.ultraGroupBox11.Controls.Add(this.ultraLabel43);
            appearance58.FontData.BoldAsString = "True";
            appearance58.FontData.SizeInPoints = 10F;
            this.ultraGroupBox11.HeaderAppearance = appearance58;
            this.ultraGroupBox11.Location = new System.Drawing.Point(6, 152);
            this.ultraGroupBox11.Name = "ultraGroupBox11";
            this.ultraGroupBox11.Size = new System.Drawing.Size(916, 112);
            this.ultraGroupBox11.TabIndex = 56;
            this.ultraGroupBox11.Text = "Đối tượng nhận tiền";
            this.ultraGroupBox11.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbAccountingObjectID5
            // 
            this.cbbAccountingObjectID5.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            appearance54.Image = global::Accounting.Frm.Properties.Resources.ubtnAdd4;
            editorButton6.Appearance = appearance54;
            editorButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectID5.ButtonsRight.Add(editorButton6);
            this.cbbAccountingObjectID5.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectID5.LimitToList = true;
            this.cbbAccountingObjectID5.Location = new System.Drawing.Point(97, 22);
            this.cbbAccountingObjectID5.Name = "cbbAccountingObjectID5";
            this.cbbAccountingObjectID5.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID5.Size = new System.Drawing.Size(202, 22);
            this.cbbAccountingObjectID5.TabIndex = 43;
            this.cbbAccountingObjectID5.ValueChanged += new System.EventHandler(this.cbbAccountingObjectID5_ValueChanged);
            // 
            // txtAccountingObjectAddress5
            // 
            this.txtAccountingObjectAddress5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddress5.Location = new System.Drawing.Point(97, 47);
            this.txtAccountingObjectAddress5.Name = "txtAccountingObjectAddress5";
            this.txtAccountingObjectAddress5.Size = new System.Drawing.Size(809, 21);
            this.txtAccountingObjectAddress5.TabIndex = 23;
            // 
            // ultraLabel41
            // 
            appearance55.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel41.Appearance = appearance55;
            this.ultraLabel41.Location = new System.Drawing.Point(23, 23);
            this.ultraLabel41.Name = "ultraLabel41";
            this.ultraLabel41.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel41.TabIndex = 0;
            this.ultraLabel41.Text = "Đối tượng";
            // 
            // ultraLabel42
            // 
            appearance56.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel42.Appearance = appearance56;
            this.ultraLabel42.Location = new System.Drawing.Point(24, 48);
            this.ultraLabel42.Name = "ultraLabel42";
            this.ultraLabel42.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel42.TabIndex = 22;
            this.ultraLabel42.Text = "Địa chỉ";
            // 
            // txtAccoutingObjectName5
            // 
            this.txtAccoutingObjectName5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccoutingObjectName5.Location = new System.Drawing.Point(305, 22);
            this.txtAccoutingObjectName5.Name = "txtAccoutingObjectName5";
            this.txtAccoutingObjectName5.Size = new System.Drawing.Size(517, 21);
            this.txtAccoutingObjectName5.TabIndex = 25;
            // 
            // ultraButton8
            // 
            this.ultraButton8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton8.Location = new System.Drawing.Point(828, 21);
            this.ultraButton8.Name = "ultraButton8";
            this.ultraButton8.Size = new System.Drawing.Size(75, 23);
            this.ultraButton8.TabIndex = 33;
            this.ultraButton8.Text = "Chọn";
            // 
            // cbbBankAccountingObject5
            // 
            this.cbbBankAccountingObject5.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbBankAccountingObject5.Location = new System.Drawing.Point(97, 74);
            this.cbbBankAccountingObject5.Name = "cbbBankAccountingObject5";
            this.cbbBankAccountingObject5.NullText = "<chọn dữ liệu>";
            this.cbbBankAccountingObject5.Size = new System.Drawing.Size(158, 22);
            this.cbbBankAccountingObject5.TabIndex = 31;
            this.cbbBankAccountingObject5.ValueChanged += new System.EventHandler(this.cbbBankAccountingObject5_ValueChanged);
            // 
            // txtBankAccountingObjectName5
            // 
            this.txtBankAccountingObjectName5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBankAccountingObjectName5.Location = new System.Drawing.Point(270, 75);
            this.txtBankAccountingObjectName5.Name = "txtBankAccountingObjectName5";
            this.txtBankAccountingObjectName5.Size = new System.Drawing.Size(636, 21);
            this.txtBankAccountingObjectName5.TabIndex = 23;
            // 
            // ultraLabel43
            // 
            appearance57.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel43.Appearance = appearance57;
            this.ultraLabel43.Location = new System.Drawing.Point(20, 76);
            this.ultraLabel43.Name = "ultraLabel43";
            this.ultraLabel43.Size = new System.Drawing.Size(68, 19);
            this.ultraLabel43.TabIndex = 0;
            this.ultraLabel43.Text = "Tài khoản";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ultraGroupBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(929, 51);
            this.panel1.TabIndex = 27;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.ultraComboEditor1);
            this.ultraGroupBox2.Controls.Add(this.ultraOptionSet1);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance60.FontData.BoldAsString = "True";
            appearance60.FontData.SizeInPoints = 13F;
            this.ultraGroupBox2.HeaderAppearance = appearance60;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(929, 51);
            this.ultraGroupBox2.TabIndex = 31;
            this.ultraGroupBox2.Text = "MUA HÀNG";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraComboEditor1
            // 
            this.ultraComboEditor1.Enabled = false;
            valueListItem3.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem3.DataValue = "ValueListItem0";
            valueListItem3.DisplayText = "Tiền mặt";
            valueListItem4.DataValue = "ValueListItem1";
            valueListItem4.DisplayText = "Ủy nhiệm chi";
            valueListItem5.DataValue = "ValueListItem2";
            valueListItem5.DisplayText = "Séc chuyển khoản";
            valueListItem6.DataValue = "ValueListItem3";
            valueListItem6.DisplayText = "Séc tiền mặt";
            valueListItem7.DataValue = "ValueListItem4";
            valueListItem7.DisplayText = "Thẻ tín dụng";
            this.ultraComboEditor1.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem4,
            valueListItem5,
            valueListItem6,
            valueListItem7});
            this.ultraComboEditor1.Location = new System.Drawing.Point(249, 25);
            this.ultraComboEditor1.Name = "ultraComboEditor1";
            this.ultraComboEditor1.Size = new System.Drawing.Size(144, 21);
            this.ultraComboEditor1.TabIndex = 31;
            this.ultraComboEditor1.ValueChanged += new System.EventHandler(this.ultraComboEditor1_ValueChanged);
            // 
            // ultraOptionSet1
            // 
            appearance59.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance59.TextHAlignAsString = "Center";
            appearance59.TextVAlignAsString = "Middle";
            this.ultraOptionSet1.Appearance = appearance59;
            this.ultraOptionSet1.BackColor = System.Drawing.Color.Transparent;
            this.ultraOptionSet1.BackColorInternal = System.Drawing.Color.Transparent;
            this.ultraOptionSet1.CheckedIndex = 0;
            valueListItem1.DataValue = "0";
            valueListItem1.DisplayText = "Chưa thanh toán";
            valueListItem2.DataValue = "1";
            valueListItem2.DisplayText = "Thanh toán ngay";
            this.ultraOptionSet1.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.ultraOptionSet1.Location = new System.Drawing.Point(11, 27);
            this.ultraOptionSet1.Name = "ultraOptionSet1";
            this.ultraOptionSet1.Size = new System.Drawing.Size(216, 19);
            this.ultraOptionSet1.TabIndex = 30;
            this.ultraOptionSet1.Text = "Chưa thanh toán";
            this.ultraOptionSet1.ValueChanged += new System.EventHandler(this.ultraOptionSet1_ValueChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.palBottom);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 51);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(929, 690);
            this.panel2.TabIndex = 28;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(929, 588);
            this.panel4.TabIndex = 28;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.ultraTabControl1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 306);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(929, 282);
            this.panel6.TabIndex = 28;
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl3);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl4);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(929, 282);
            this.ultraTabControl1.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon;
            this.ultraTabControl1.TabIndex = 4;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "1. Hàng tiền";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "2. Thuế";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "3. Thống kê";
            ultraTab4.TabPage = this.ultraTabPageControl4;
            ultraTab4.Text = "4. Chứng từ chi phí";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3,
            ultraTab4});
            this.ultraTabControl1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(927, 259);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.ultraTabControl2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(929, 306);
            this.panel5.TabIndex = 27;
            // 
            // ultraTabControl2
            // 
            this.ultraTabControl2.Controls.Add(this.ultraTabSharedControlsPage2);
            this.ultraTabControl2.Controls.Add(this.ultraTabPageControl5);
            this.ultraTabControl2.Controls.Add(this.ultraTabPageControl6);
            this.ultraTabControl2.Controls.Add(this.ultraTabPageControl7);
            this.ultraTabControl2.Controls.Add(this.ultraTabPageControl8);
            this.ultraTabControl2.Controls.Add(this.ultraTabPageControl9);
            this.ultraTabControl2.Controls.Add(this.ultraTabPageControl10);
            this.ultraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControl2.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControl2.Name = "ultraTabControl2";
            this.ultraTabControl2.SharedControlsPage = this.ultraTabSharedControlsPage2;
            this.ultraTabControl2.Size = new System.Drawing.Size(929, 306);
            this.ultraTabControl2.TabIndex = 34;
            ultraTab5.TabPage = this.ultraTabPageControl5;
            ultraTab5.Text = "Phiếu nhập";
            ultraTab6.TabPage = this.ultraTabPageControl6;
            ultraTab6.Text = "Tiền mặt";
            ultraTab7.TabPage = this.ultraTabPageControl7;
            ultraTab7.Text = "Ủy nhiệm chi";
            ultraTab8.TabPage = this.ultraTabPageControl8;
            ultraTab8.Text = "Séc chuyển khoản";
            ultraTab9.TabPage = this.ultraTabPageControl9;
            ultraTab9.Text = "Séc tiền mặt";
            ultraTab10.TabPage = this.ultraTabPageControl10;
            ultraTab10.Text = "Thẻ tín dụng";
            this.ultraTabControl2.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab5,
            ultraTab6,
            ultraTab7,
            ultraTab8,
            ultraTab9,
            ultraTab10});
            this.ultraTabControl2.ActiveTabChanged += new Infragistics.Win.UltraWinTabControl.ActiveTabChangedEventHandler(this.uInfoTabControl_ActiveTabChanged);
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(925, 280);
            // 
            // palBottom
            // 
            this.palBottom.Controls.Add(this.label16);
            this.palBottom.Controls.Add(this.label4);
            this.palBottom.Controls.Add(this.label15);
            this.palBottom.Controls.Add(this.label3);
            this.palBottom.Controls.Add(this.label14);
            this.palBottom.Controls.Add(this.label2);
            this.palBottom.Controls.Add(this.label13);
            this.palBottom.Controls.Add(this.lbTotalPaymentAmount);
            this.palBottom.Controls.Add(this.label12);
            this.palBottom.Controls.Add(this.lbTotalPaymentAmoutOriginal);
            this.palBottom.Controls.Add(this.label11);
            this.palBottom.Controls.Add(this.lbTotalVatAmount);
            this.palBottom.Controls.Add(this.label10);
            this.palBottom.Controls.Add(this.lbTotalVatAmountOriginal);
            this.palBottom.Controls.Add(this.lbTotalImportTaxAmount);
            this.palBottom.Controls.Add(this.lbTotalAmount);
            this.palBottom.Controls.Add(this.lbTotalSpecialConsumeTaxAmount);
            this.palBottom.Controls.Add(this.lbTotalDiscountAmount);
            this.palBottom.Controls.Add(this.lbTotalSpecialConsumeTaxAmountOriginal);
            this.palBottom.Controls.Add(this.lbTotalDiscountAmountOriginal);
            this.palBottom.Controls.Add(this.lbTotalImportTaxAmountOriginal);
            this.palBottom.Controls.Add(this.lbTotalAmountOriginal);
            this.palBottom.Controls.Add(this.label5);
            this.palBottom.Controls.Add(this.label1);
            this.palBottom.Controls.Add(this.uGridControl);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 588);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(929, 102);
            this.palBottom.TabIndex = 27;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(701, 80);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(82, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Giá trị nhập kho";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(472, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Tổng tiền thanh toán";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(701, 58);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Chi phí mua hàng";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(472, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tiền thuế GTGT";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(701, 36);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(84, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Tiền thuế TTĐB";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(472, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tổng tiền CK";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(878, 80);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "0";
            // 
            // lbTotalPaymentAmount
            // 
            this.lbTotalPaymentAmount.AutoSize = true;
            this.lbTotalPaymentAmount.Location = new System.Drawing.Point(649, 80);
            this.lbTotalPaymentAmount.Name = "lbTotalPaymentAmount";
            this.lbTotalPaymentAmount.Size = new System.Drawing.Size(13, 13);
            this.lbTotalPaymentAmount.TabIndex = 2;
            this.lbTotalPaymentAmount.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(818, 80);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "0";
            // 
            // lbTotalPaymentAmoutOriginal
            // 
            this.lbTotalPaymentAmoutOriginal.AutoSize = true;
            this.lbTotalPaymentAmoutOriginal.Location = new System.Drawing.Point(589, 80);
            this.lbTotalPaymentAmoutOriginal.Name = "lbTotalPaymentAmoutOriginal";
            this.lbTotalPaymentAmoutOriginal.Size = new System.Drawing.Size(13, 13);
            this.lbTotalPaymentAmoutOriginal.TabIndex = 2;
            this.lbTotalPaymentAmoutOriginal.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(878, 58);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "0";
            // 
            // lbTotalVatAmount
            // 
            this.lbTotalVatAmount.AutoSize = true;
            this.lbTotalVatAmount.Location = new System.Drawing.Point(649, 58);
            this.lbTotalVatAmount.Name = "lbTotalVatAmount";
            this.lbTotalVatAmount.Size = new System.Drawing.Size(13, 13);
            this.lbTotalVatAmount.TabIndex = 2;
            this.lbTotalVatAmount.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(818, 58);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "0";
            // 
            // lbTotalVatAmountOriginal
            // 
            this.lbTotalVatAmountOriginal.AutoSize = true;
            this.lbTotalVatAmountOriginal.Location = new System.Drawing.Point(589, 58);
            this.lbTotalVatAmountOriginal.Name = "lbTotalVatAmountOriginal";
            this.lbTotalVatAmountOriginal.Size = new System.Drawing.Size(13, 13);
            this.lbTotalVatAmountOriginal.TabIndex = 2;
            this.lbTotalVatAmountOriginal.Text = "0";
            // 
            // lbTotalImportTaxAmount
            // 
            this.lbTotalImportTaxAmount.AutoSize = true;
            this.lbTotalImportTaxAmount.Location = new System.Drawing.Point(878, 13);
            this.lbTotalImportTaxAmount.Name = "lbTotalImportTaxAmount";
            this.lbTotalImportTaxAmount.Size = new System.Drawing.Size(13, 13);
            this.lbTotalImportTaxAmount.TabIndex = 2;
            this.lbTotalImportTaxAmount.Text = "0";
            // 
            // lbTotalAmount
            // 
            this.lbTotalAmount.AutoSize = true;
            this.lbTotalAmount.Location = new System.Drawing.Point(649, 13);
            this.lbTotalAmount.Name = "lbTotalAmount";
            this.lbTotalAmount.Size = new System.Drawing.Size(13, 13);
            this.lbTotalAmount.TabIndex = 2;
            this.lbTotalAmount.Text = "0";
            // 
            // lbTotalSpecialConsumeTaxAmount
            // 
            this.lbTotalSpecialConsumeTaxAmount.AutoSize = true;
            this.lbTotalSpecialConsumeTaxAmount.Location = new System.Drawing.Point(878, 36);
            this.lbTotalSpecialConsumeTaxAmount.Name = "lbTotalSpecialConsumeTaxAmount";
            this.lbTotalSpecialConsumeTaxAmount.Size = new System.Drawing.Size(13, 13);
            this.lbTotalSpecialConsumeTaxAmount.TabIndex = 2;
            this.lbTotalSpecialConsumeTaxAmount.Text = "0";
            // 
            // lbTotalDiscountAmount
            // 
            this.lbTotalDiscountAmount.AutoSize = true;
            this.lbTotalDiscountAmount.Location = new System.Drawing.Point(649, 36);
            this.lbTotalDiscountAmount.Name = "lbTotalDiscountAmount";
            this.lbTotalDiscountAmount.Size = new System.Drawing.Size(13, 13);
            this.lbTotalDiscountAmount.TabIndex = 2;
            this.lbTotalDiscountAmount.Text = "0";
            // 
            // lbTotalSpecialConsumeTaxAmountOriginal
            // 
            this.lbTotalSpecialConsumeTaxAmountOriginal.AutoSize = true;
            this.lbTotalSpecialConsumeTaxAmountOriginal.Location = new System.Drawing.Point(818, 36);
            this.lbTotalSpecialConsumeTaxAmountOriginal.Name = "lbTotalSpecialConsumeTaxAmountOriginal";
            this.lbTotalSpecialConsumeTaxAmountOriginal.Size = new System.Drawing.Size(13, 13);
            this.lbTotalSpecialConsumeTaxAmountOriginal.TabIndex = 2;
            this.lbTotalSpecialConsumeTaxAmountOriginal.Text = "0";
            // 
            // lbTotalDiscountAmountOriginal
            // 
            this.lbTotalDiscountAmountOriginal.AutoSize = true;
            this.lbTotalDiscountAmountOriginal.Location = new System.Drawing.Point(589, 36);
            this.lbTotalDiscountAmountOriginal.Name = "lbTotalDiscountAmountOriginal";
            this.lbTotalDiscountAmountOriginal.Size = new System.Drawing.Size(13, 13);
            this.lbTotalDiscountAmountOriginal.TabIndex = 2;
            this.lbTotalDiscountAmountOriginal.Text = "0";
            // 
            // lbTotalImportTaxAmountOriginal
            // 
            this.lbTotalImportTaxAmountOriginal.AutoSize = true;
            this.lbTotalImportTaxAmountOriginal.Location = new System.Drawing.Point(818, 13);
            this.lbTotalImportTaxAmountOriginal.Name = "lbTotalImportTaxAmountOriginal";
            this.lbTotalImportTaxAmountOriginal.Size = new System.Drawing.Size(13, 13);
            this.lbTotalImportTaxAmountOriginal.TabIndex = 2;
            this.lbTotalImportTaxAmountOriginal.Text = "0";
            // 
            // lbTotalAmountOriginal
            // 
            this.lbTotalAmountOriginal.AutoSize = true;
            this.lbTotalAmountOriginal.Location = new System.Drawing.Point(589, 13);
            this.lbTotalAmountOriginal.Name = "lbTotalAmountOriginal";
            this.lbTotalAmountOriginal.Size = new System.Drawing.Size(13, 13);
            this.lbTotalAmountOriginal.TabIndex = 2;
            this.lbTotalAmountOriginal.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(701, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Tiền thuế NK";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(472, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Tổng tiền hàng";
            // 
            // uGridControl
            // 
            this.uGridControl.Location = new System.Drawing.Point(0, 0);
            this.uGridControl.Name = "uGridControl";
            this.uGridControl.Size = new System.Drawing.Size(466, 60);
            this.uGridControl.TabIndex = 1;
            this.uGridControl.Text = "ultraGrid1";
            // 
            // ultraToolTipManager1
            // 
            this.ultraToolTipManager1.ContainingControl = this;
            // 
            // FPPInvoiceDetail1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(929, 741);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FPPInvoiceDetail1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridHangTien)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridTax)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridThongKe)).EndInit();
            this.ultraTabPageControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridChungTuChiPhi)).EndInit();
            this.ultraTabPageControl5.ResumeLayout(false);
            this.ultraTabPageControl5.PerformLayout();
            this.palTopSctNhtNctPXK.ResumeLayout(false);
            this.palTopSctNhtNctPXK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberAttach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccoutingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).EndInit();
            this.ultraTabPageControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ultraGroupBox3.PerformLayout();
            this.palTopSctNhtNctPXK1.ResumeLayout(false);
            this.palTopSctNhtNctPXK1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccoutingObjectName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress1)).EndInit();
            this.ultraTabPageControl7.ResumeLayout(false);
            this.ultraTabPageControl7.PerformLayout();
            this.palTopSctNhtNctPXK2.ResumeLayout(false);
            this.palTopSctNhtNctPXK2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            this.ultraGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccoutingObjectName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountingObject2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccountingObjectName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.ultraGroupBox5.ResumeLayout(false);
            this.ultraGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountObject2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccountObjectName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason2)).EndInit();
            this.ultraTabPageControl8.ResumeLayout(false);
            this.ultraTabPageControl8.PerformLayout();
            this.palTopSctNhtNctPXK3.ResumeLayout(false);
            this.palTopSctNhtNctPXK3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).EndInit();
            this.ultraGroupBox6.ResumeLayout(false);
            this.ultraGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccoutingObjectName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountingObject3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccountingObjectName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).EndInit();
            this.ultraGroupBox7.ResumeLayout(false);
            this.ultraGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountObject3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccountObjectName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason3)).EndInit();
            this.ultraTabPageControl9.ResumeLayout(false);
            this.ultraTabPageControl9.PerformLayout();
            this.palTopSctNhtNctPXK4.ResumeLayout(false);
            this.palTopSctNhtNctPXK4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox8)).EndInit();
            this.ultraGroupBox8.ResumeLayout(false);
            this.ultraGroupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteIssueDate4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccoutingObjectName4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificationNo4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueBy4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox9)).EndInit();
            this.ultraGroupBox9.ResumeLayout(false);
            this.ultraGroupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountObject4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccountObjectName4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason4)).EndInit();
            this.ultraTabPageControl10.ResumeLayout(false);
            this.ultraTabPageControl10.PerformLayout();
            this.palTopSctNhtNctPXK5.ResumeLayout(false);
            this.palTopSctNhtNctPXK5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox10)).EndInit();
            this.ultraGroupBox10.ResumeLayout(false);
            this.ultraGroupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardNumber5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox11)).EndInit();
            this.ultraGroupBox11.ResumeLayout(false);
            this.ultraGroupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccoutingObjectName5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountingObject5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccountingObjectName5)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl2)).EndInit();
            this.ultraTabControl2.ResumeLayout(false);
            this.palBottom.ResumeLayout(false);
            this.palBottom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel palBottom;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridTax;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridControl;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridHangTien;
        private Infragistics.Win.UltraWinToolTip.UltraToolTipManager ultraToolTipManager1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridThongKe;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbTotalAmountOriginal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbTotalPaymentAmount;
        private System.Windows.Forms.Label lbTotalPaymentAmoutOriginal;
        private System.Windows.Forms.Label lbTotalVatAmount;
        private System.Windows.Forms.Label lbTotalVatAmountOriginal;
        private System.Windows.Forms.Label lbTotalAmount;
        private System.Windows.Forms.Label lbTotalDiscountAmount;
        private System.Windows.Forms.Label lbTotalDiscountAmountOriginal;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridChungTuChiPhi;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbTotalImportTaxAmount;
        private System.Windows.Forms.Label lbTotalSpecialConsumeTaxAmount;
        private System.Windows.Forms.Label lbTotalSpecialConsumeTaxAmountOriginal;
        private System.Windows.Forms.Label lbTotalImportTaxAmountOriginal;
        private System.Windows.Forms.Label label5;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl2;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl5;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor1;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet ultraOptionSet1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl6;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraButton uButtonPPOrder;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNumberAttach;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel lblReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactName;
        private Infragistics.Win.Misc.UltraLabel lblCompanyTaxCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccoutingObjectName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectID;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl7;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl8;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl9;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl10;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID1;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReceiver;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccoutingObjectName1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccoutingObjectName2;
        private Infragistics.Win.Misc.UltraButton ultraButton2;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccountingObject2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankAccountingObjectName2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccountObject2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankAccountObjectName2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccoutingObjectName3;
        private Infragistics.Win.Misc.UltraButton ultraButton4;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccountingObject3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankAccountingObjectName3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel26;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox7;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccountObject3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankAccountObjectName3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel27;
        private Infragistics.Win.Misc.UltraLabel ultraLabel28;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel32;
        private Infragistics.Win.Misc.UltraLabel ultraLabel33;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccoutingObjectName4;
        private Infragistics.Win.Misc.UltraButton ultraButton6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIssueBy4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel34;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox9;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccountObject4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankAccountObjectName4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel35;
        private Infragistics.Win.Misc.UltraLabel ultraLabel36;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID2;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID3;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID4;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteIssueDate4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel46;
        private Infragistics.Win.Misc.UltraLabel ultraLabel45;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIdentificationNo4;
        private Infragistics.Win.Misc.UltraPanel palTopSctNhtNctPXK;
        private Infragistics.Win.Misc.UltraPanel palTopSctNhtNctPXK1;
        private Infragistics.Win.Misc.UltraPanel palTopSctNhtNctPXK2;
        private Infragistics.Win.Misc.UltraPanel palTopSctNhtNctPXK3;
        private Infragistics.Win.Misc.UltraPanel palTopSctNhtNctPXK4;
        private Infragistics.Win.Misc.UltraPanel palTopSctNhtNctPXK5;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox10;
        private System.Windows.Forms.Label txtOwnerCard;
        private Infragistics.Win.Misc.UltraLabel lblOwnerCard;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCreditCardNumber5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel37;
        private Infragistics.Win.Misc.UltraLabel ultraLabel38;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox11;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel41;
        private Infragistics.Win.Misc.UltraLabel ultraLabel42;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccoutingObjectName5;
        private Infragistics.Win.Misc.UltraButton ultraButton8;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccountingObject5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankAccountingObjectName5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel43;
    }
}
