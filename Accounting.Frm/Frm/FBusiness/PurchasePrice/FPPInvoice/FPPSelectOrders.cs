﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FPPSelectOrders<T> : CustormForm
    {

        readonly BindingList<PPSelectOrder> _lstSelectOrder = new BindingList<PPSelectOrder>();
        public List<PPSelectOrder> SelectOrders { get; private set; }

        public FPPSelectOrders(T input, Guid? accountingObjectId, DateTime date)
        {
            InitializeComponent();
            ConfigControl(input, accountingObjectId, date);
            if (uGrid.Rows.Count > 0) uGrid.ActiveRow = uGrid.Rows[0];
            btnViewPpOrder.Enabled = (uGrid.ActiveRow != null || uGrid.Selected.Rows.Count > 0);
            btnApply.Enabled = uGrid.Rows.Count > 0;
        }

        private void ConfigControl(T input, Guid? accountingObjectId, DateTime date)
        {
            if (accountingObjectId == null)
            {
                ConfigGrid(new BindingList<PPSelectOrder>());
                return;
            }
            var result = Utils.IPPOrderService.Query.Where(x => x.AccountingObjectID == (Guid)accountingObjectId && x.Date <= date).OrderByDescending(s => s.Date).ThenByDescending(s => s.No).ToList();
            Utils.IPPOrderService.UnbindSession(result);
            result = Utils.IPPOrderService.Query.Where(x => x.AccountingObjectID == (Guid)accountingObjectId && x.Date <= date).OrderByDescending(s => s.Date).ThenByDescending(s => s.No).ToList();
            foreach (var ppOrder in result)
            {
                foreach (var ppOrderDetail in ppOrder.PPOrderDetails.Where(x => ((x.Quantity ?? 0) - (x.QuantityReceipt ?? 0)) > 0).OrderBy(a=>a.OrderPriority))//edit by cuongpv: add them order by OrderPriority
                {
                    var selectOrder = new PPSelectOrder
                    {
                        ID = ppOrder.ID,
                        No = ppOrder.No,
                        Date = ppOrder.Date,
                        Reason = ppOrder.Reason,
                        MaterialGoodsID = ppOrderDetail.MaterialGoodsID,
                        QuantityRemaining = (ppOrderDetail.Quantity ?? 0) - (ppOrderDetail.QuantityReceipt ?? 0),
                        QuantityInward = (ppOrderDetail.Quantity ?? 0) - (ppOrderDetail.QuantityReceipt ?? 0),
                        UnitPrice = ppOrderDetail.UnitPrice,
                        //Amount = Math.Abs((ppOrderDetail.Quantity ?? 0) - (ppOrderDetail.QuantityReceipt ?? 0) * ppOrderDetail.UnitPrice),
                        Amount = ppOrderDetail.Amount,
                        AccountingObjectID = ppOrder.AccountingObjectID,
                        CurrencyID = ppOrder.CurrencyID,
                        ExchangeRate = ppOrder.ExchangeRate,
                        OrderPriority = ppOrderDetail.OrderPriority,//add by cuongpv
                        PpOrderDetail = ppOrderDetail
                    };
                    var materialGoodsCustom = Utils.ListMaterialGoodsCustom.FirstOrDefault(x => x.ID == ppOrderDetail.MaterialGoodsID);
                    if (materialGoodsCustom != null)
                        selectOrder.MaterialGoodsName =
                            materialGoodsCustom.
                                MaterialGoodsName;
                    var ppInvoice = input as PPInvoice;
                    if (ppInvoice != null && ppInvoice.PPInvoiceDetails != null && ppInvoice.PPInvoiceDetails.Count > 0)
                    {
                        var selected = ppInvoice.PPInvoiceDetails.FirstOrDefault(x => x.PPOrderID == selectOrder.ID && x.MaterialGoodsID == selectOrder.MaterialGoodsID);
                        if (selected != null)
                        {
                            selectOrder.CheckColumn = true;
                            selectOrder.QuantityInward = selected.Quantity;
                        }
                    }
                    _lstSelectOrder.Add(selectOrder);
                }
            }
            ConfigGrid(_lstSelectOrder);
        }

        private void ConfigGrid(BindingList<PPSelectOrder> input)
        {
            uGrid.DataSource = input;
            uGrid.ConfigGrid(ConstDatabase.PPSelectOrder_KeyName, false);
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.UseFixedHeaders = false;
            //lọc cột
            uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            uGrid.DisplayLayout.Override.FilterClearButtonLocation = FilterClearButtonLocation.Row;
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;

            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid);
            }
            UltraGridColumn ugc = uGrid.DisplayLayout.Bands[0].Columns["CheckColumn"];
            ugc.Header.VisiblePosition = 0;
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnViewPpOrder_Click(object sender, EventArgs e)
        {
            ViewPpOrder();
        }

        private void ViewPpOrder()
        {
            //try
            //{
            //    if (uGrid.ActiveRow == null) return;
            //    var selectOrder = uGrid.ActiveRow.ListObject as PPSelectOrder;
            //    if (selectOrder == null) return;
            //    var ppOder = Utils.IPPOrderService.Query.FirstOrDefault(x => x.ID == selectOrder.ID);
            //    var f = new FPPOrderDetail(ppOder, new List<PPOrder>() { ppOder }, ConstFrm.optStatusForm.View);
            //    f.ShowDialog(this);
            //}
            //catch (Exception ex)
            //{
            //    MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            //}
        }

        private void uGrid_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            // edit by tungnt
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                PPSelectOrder temp = (PPSelectOrder)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                var ppOder = Utils.IPPOrderService.Query.FirstOrDefault(x => x.ID == temp.ID);
                var f = new FPPOrderDetail(ppOder, new List<PPOrder>() { ppOder }, ConstFrm.optStatusForm.View);
                f.ShowDialog(this);
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            var lstPpOrder = from u in _lstSelectOrder where u.CheckColumn select u;
            if (lstPpOrder.Count() == 0)
            {
                MSG.Warning(resSystem.MSG_Error_19);
                return;
            }
            SelectOrders = lstPpOrder.ToList();
            DialogResult = DialogResult.OK;
            Close();
        }

        private void uGrid_ClickCell(object sender, Infragistics.Win.UltraWinGrid.ClickCellEventArgs e)
        {
            btnViewPpOrder.Enabled = (uGrid.ActiveRow != null || uGrid.Selected.Rows.Count > 0);
        }

        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            if (!e.Cell.Row.Cells[e.Cell.Column.Key].IsFilterRowCell)
            {
                if (e.Cell.Column.Key == "CheckColumn")
                {
                    var gid = (Guid?)e.Cell.Row.Cells["ID"].Value;
                    e.Cell.Row.Cells["CheckColumn"].Value = !(bool)e.Cell.Value;
                }
                uGrid.UpdateDataGrid();
            }
        }
    }
}
