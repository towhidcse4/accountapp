﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using System.Linq;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;
using Accounting.Core.IService;
using FX.Core;

namespace Accounting
{
    public sealed partial class FPPInvoiceDetail : FppInvoiceDetailStand
    {
        #region Khai báo
        public int TypeFrm;
        private bool _isFirst = true;
        private UltraGrid _uGrid0;
        private UltraGrid _uGrid1;
        private UltraGrid _uGrid2;
        private UltraGrid _uGrid3;
        UltraGrid ugrid2 = null;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        private IMBCreditCardDetailVendorService _iMBCreditCardDetailVendorService { get { return IoC.Resolve<IMBCreditCardDetailVendorService>(); } }
        private IMBTellerPaperDetailVendorService _iMBTellerPaperDetailVendorService { get { return IoC.Resolve<IMBTellerPaperDetailVendorService>(); } }
        private IMCPaymentDetailVendorService _iMCPaymentDetailVendorService { get { return IoC.Resolve<IMCPaymentDetailVendorService>(); } }
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        int stsform;
        #endregion

        #region khởi tạo
        public FPPInvoiceDetail(PPInvoice temp, List<PPInvoice> listObject, int statusForm, int loaiFrm = 1)   //0: Mua hàng qua kho, 1: mua hàng không qua kho
        {
            InitializeComponent();
            base.InitializeComponent1();

            #region Thiết lập ban đầu
            //temp.TypeID = 210; //402 phiếu nhập kho từ mua hàng, 210 phiếu mua hàng chưa thanh toán
            TypeFrm = loaiFrm;
            _typeID = temp.TypeID; _statusForm = statusForm;
            stsform = statusForm;
            if (statusForm == ConstFrm.optStatusForm.Add) _select.TypeID = 210; else _select = temp;

            _select.StoredInRepository = loaiFrm == 0;
            _listSelects.AddRange(listObject);
            InitializeGUI(_select); //khởi tạo và hiển thị giá trị ban đầu cho các control
            if (statusForm == ConstFrm.optStatusForm.Add) _select.BillReceived = false;
            ReloadToolbar(_select, listObject, statusForm);  //Change Menu Top


            #endregion
        }
        #endregion

        #region override
        public override void InitializeGUI(PPInvoice input)
        {
            input.PcMcPayment = input.PcMcPayment ?? new MCPayment();   //GuidID móc sẽ trùng với Guid chứng từ gốc
            input.UncMbTellerPaper = input.UncMbTellerPaper ?? new MBTellerPaper();
            input.SckMbTellerPaper = input.SckMbTellerPaper ?? new MBTellerPaper();
            input.StmMbTellerPaper = input.StmMbTellerPaper ?? new MBTellerPaper();
            input.TtdMbCreditCard = input.TtdMbCreditCard ?? new MBCreditCard();
            input.BillReceived = _statusForm == ConstFrm.optStatusForm.Add || input.BillReceived;
            ////Config GUI
            //ConfigByType(input);

            #region Top
            //config số đơn hàng, ngày đơn hàng
            if (TypeFrm == 0)    //Mua hàng qua kho
            {
                this.ConfigTopVouchersNo<PPInvoice>(palVouchersNoHd, "InwardNo", "PostedDate", "Date", ConstFrm.TypeGroup_RSInwardOutward, resetNo: _statusForm == ConstFrm.optStatusForm.Add);
                _select.StoredInRepository = true;
            }
            else   //Mua hàng không qua kho
            {
                this.ConfigTopVouchersNo<PPInvoice>(palVouchersNoHd, "No", "PostedDate", "Date", resetNo: _statusForm == ConstFrm.optStatusForm.Add);
                _select.StoredInRepository = false;
            }
            List<int> lstId = new List<int> { 260, 261, 262, 264, 263 };
            List<string> lstType = new List<string> { "Tiền mặt", "Ủy nhiệm chi", "Séc chuyển khoản", "Séc tiền mặt", "Thẻ tín dụng" };
            cbbChonThanhToan.Items.Clear();
            for (int i = 0; i < lstId.Count; i++)
            {
                cbbChonThanhToan.Items.Add(lstId[i], lstType[i]);
            }
            _backSelect.TypeID = _select.TypeID.CloneObject();
            _backSelect.No = _select.No.CloneObject();
            if (_statusForm == ConstFrm.optStatusForm.Add) optThanhToan.Value = false;
            else
            {
                if (_select.TypeID == 210) optThanhToan.Value = false;
                else
                {
                    int typeId = _select.TypeID;
                    optThanhToan.Value = true;
                    _select.TypeID = typeId;
                    cbbChonThanhToan.SelectedItem = cbbChonThanhToan.Items.ValueList.FindByDataValue(_select.TypeID);
                }
            }
            #endregion

            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
            }

            ultraTabControlThongTinChung.Tabs[0].Text = TypeFrm == 1 ? "Hóa đơn mua hàng" : "Phiếu nhập";
            //Config Grid
            _listObjectInput.Clear();
            _listObjectInput.Add(new BindingList<PPInvoiceDetail>(input.PPInvoiceDetails));
            _listObjectInput.Add(new BindingList<PPInvoiceDetailCost>(input.PPInvoiceDetailCosts));

            ConfigGridBase(input, uGridControl, pnlUgrid);
            //config combo
            Utils.isPPInvoice = true;
            this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 1);
            this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectIDTm, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 1);
            this.ConfigCombo(input, Utils.ListAccountingObject, cbbAccountingObjectIDUnc, "" +
                                                                                          "AccountingObjectID", cbbMAccountingObjectBankAccountDetailIDUnc, "MAccountingObjectBankAccountDetailID", accountingObjectType: 1);
            this.ConfigCombo(input, Utils.ListAccountingObject, cbbAccountingObjectIDSck, "AccountingObjectID", cbbMAccountingObjectBankAccountDetailIDSnk, "MAccountingObjectBankAccountDetailID", accountingObjectType: 1);
            this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectIDStm, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 1);
            this.ConfigCombo(input, Utils.ListAccountingObject, cbbAccountingObjectIDTtd, "AccountingObjectID", cbbMAccountingObjectBankAccountDetailIDTtd, "MAccountingObjectBankAccountDetailID", accountingObjectType: 1);
            this.ConfigCombo(Utils.ListBankAccountDetail, cbbBankAccountDetailIDUnc, "BankAccount", "ID", input, "BankAccountDetailID");
            this.ConfigCombo(Utils.ListBankAccountDetail, cbbBankAccountDetailIDSck, "BankAccount", "ID", input, "BankAccountDetailID");
            this.ConfigCombo(Utils.ListBankAccountDetail, cbbBankAccountDetailIDStm, "BankAccount", "ID", input, "BankAccountDetailID");
            this.ConfigCombo(input, "CreditCardNumber", Utils.ListCreditCard, cbbCreditCardNumber, picCreditCardType, txtCreditCardType, txtOwnerCard);

            //Databinding
            DataBinding(new List<Control> { txtAccountingObjectName, txtAccountingObjectAddress, txtContactName, txtReason, txtOriginalNo }, "AccountingObjectName,AccountingObjectAddress,ContactName,Reason,OriginalNo");
            DataBinding(new List<Control> { txtAccountingObjectNameTm, txtAccountingObjectAddressTm, txtMContactNameTm, txtMReasonPayTm, txtNumberAttachTm }, "AccountingObjectName,AccountingObjectAddress,MContactName,MReasonPay,NumberAttach");
            DataBinding(new List<Control> { txtBankNameUnc, txtMReasonPayUnc, txtAccountingObjectNameUnc, txtAccountingObjectAddressUnc, txtMAccountingObjectBankNameUnc }, "BankName,MReasonPay,AccountingObjectName,AccountingObjectAddress,MAccountingObjectBankName");
            DataBinding(new List<Control> { txtBankNameSck, txtMReasonPaySck, txtAccountingObjectNameSck, txtAccountingObjectAddressSck, txtMAccountingObjectBankNameSck }, "BankName,MReasonPay,AccountingObjectName,AccountingObjectAddress,MAccountingObjectBankName");
            DataBinding(new List<Control> { txtBankNameStm, txtMReasonPayStm, txtAccountingObjectNameStm, txtMContactNameStm, txtIdentificationNo, dteIssueDate, txtIssueBy }, "BankName,MReasonPay,AccountingObjectName,MContactName,IdentificationNo,IssueDate,IssueBy", "0,0,0,0,0,2,0");
            DataBinding(new List<Control> { txtMReasonPayTtd, txtAccountingObjectNameTtd, txtAccountingObjectAddressTtd, txtMAccountingObjectBankNameTtd }, "MReasonPay,AccountingObjectName,AccountingObjectAddress,MAccountingObjectBankName");

            DataBinding(new List<Control> { chkBillReceived }, "BillReceived", "1");
            ugrid2 = Controls.Find("uGrid4", true).FirstOrDefault() as UltraGrid;
            ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            #region Phần cuối
            lblTotalAmountOriginal.DataBindings.Clear();
            lblTotalAmountOriginal.BindControl("Text", input, "TotalAmountOriginal", ConstDatabase.Format_TienVND, ConstDatabase.Format_ForeignCurrency);
            lblTotalAmount.DataBindings.Clear();
            lblTotalAmount.BindControl("Text", input, "TotalAmount", ConstDatabase.Format_TienVND);
            lblTotalDiscountAmountOriginal.DataBindings.Clear();
            lblTotalDiscountAmountOriginal.BindControl("Text", input, "TotalDiscountAmountOriginal", ConstDatabase.Format_TienVND, ConstDatabase.Format_ForeignCurrency);
            lblTotalDiscountAmount.DataBindings.Clear();
            lblTotalDiscountAmount.BindControl("Text", input, "TotalDiscountAmount", ConstDatabase.Format_TienVND);
            lblTotalAllOriginal.DataBindings.Clear();
            lblTotalAllOriginal.BindControl("Text", input, input.IsImportPurchase == false ? "TotalAllOriginal" : "TotalAllNoVatOriginal", ConstDatabase.Format_TienVND, ConstDatabase.Format_ForeignCurrency);
            lblTotalAll.DataBindings.Clear();
            lblTotalAll.BindControl("Text", input, input.IsImportPurchase == false ? "TotalAll" : "TotalAllNoVat", ConstDatabase.Format_TienVND);
            lblTotalImportTaxAmount.DataBindings.Clear();
            lblTotalImportTaxAmount.BindControl("Text", input, "TotalImportTaxAmount", ConstDatabase.Format_TienVND);
            lblTotalSpecialConsumeTaxAmount.DataBindings.Clear();
            lblTotalSpecialConsumeTaxAmount.BindControl("Text", input, "TotalSpecialConsumeTaxAmount", ConstDatabase.Format_TienVND);
            lblTotalVATAmount.DataBindings.Clear();
            lblTotalVATAmount.BindControl("Text", input, "TotalVATAmount", ConstDatabase.Format_TienVND);
            lblTotalInwardAmount.DataBindings.Clear();
            lblTotalInwardAmount.BindControl("Text", input, TypeFrm == 1 ? "TotalAllValue" : "TotalInwardAmount", ConstDatabase.Format_TienVND);
            lblChangeByForm.Text = TypeFrm == 0 ? "Giá nhập kho" : "Tổng giá trị";
            #endregion

            //ConfigGroup(false);

            var uTabControl = Controls.Find("ultraTabControl", true)[0] as UltraTabControl;
            if (uTabControl != null)
                uTabControl.TabPadding = new Size(1, 4);
            //Quuân edit
            uTabControl.SelectedTabChanged += (s, e) => ultraTabControl_SelectedTabChanged(s, e);

            //var gridControl = Controls.Find("uGridControl", true)[0] as UltraGrid;
            if (uGridControl != null)
            {
                uGridControl.CellChange += new CellEventHandler(uGridControl_CellChange);
            }
            _uGrid0 = (UltraGrid)Controls.Find("uGrid0", true).FirstOrDefault();
            if (_uGrid0 != null)
            {
                _uGrid0.DisplayLayout.Bands[0].Columns["RepositoryID"].Hidden = TypeFrm == 1;
                _uGrid0.DisplayLayout.Bands[0].Columns["InwardAmount"].Hidden = TypeFrm == 1;
                _uGrid0.DisplayLayout.Bands[0].Columns["LotNo"].Hidden = !_select.StoredInRepository ?? false;
                _uGrid0.DisplayLayout.Bands[0].Columns["ExpiryDate"].Hidden = !_select.StoredInRepository ?? false;
                _uGrid0.DisplayLayout.Bands[0].Columns["InwardAmount"].CellActivation = Activation.NoEdit;// edit by tungnt: khóa cột giá trị nhập kho
            }
            _uGrid1 = Controls.Find("uGrid1", true).FirstOrDefault() as UltraGrid;
            _uGrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
            _uGrid3 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
            if (input.TypeID.Equals(210))
            {
                _uGrid0.AfterRowsDeleted += new EventHandler(uGrid_AfterRowsDeleted);
                _uGrid1.AfterRowsDeleted += new EventHandler(uGrid_AfterRowsDeleted);
                _uGrid2.AfterRowsDeleted += new EventHandler(uGrid_AfterRowsDeleted);
            }
            if (_uGrid3 != null)
            {
                //uGrid3.DisplayLayout.Override.TemplateAddRowPrompt = "abc";
                _uGrid3.DisplayLayout.Override.ResetAllowAddNew();
                _uGrid3.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;
                foreach (var column in _uGrid3.DisplayLayout.Bands[0].Columns)
                {
                    column.CellActivation = Activation.NoEdit;
                    column.CellClickAction = CellClickAction.CellSelect;
                }
                //uGrid3.BeforeRowActivate -= this.uGrid_BeforeRowActivate<PPInvoice>;              
                _uGrid3.AfterRowsDeleted += new EventHandler(uGrid3_AfterRowsDeleted);

            }
            btnApportionTaxes.EnabledControl(_select.IsImportPurchase ?? false);
            lblBillReceived.Visible = chkBillReceived.CheckState == CheckState.Checked;
            if (_statusForm != ConstFrm.optStatusForm.View)
            {
                if (chkBillReceived.CheckState == CheckState.Checked)
                {
                    {
                        _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].CellActivation = Activation.AllowEdit;
                        _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].CellActivation = Activation.AllowEdit;
                        _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellActivation = Activation.AllowEdit;
                        _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].CellActivation = Activation.AllowEdit;
                    }
                }
                else
                {
                    {
                        _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].CellActivation = Activation.Disabled;
                        _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].CellActivation = Activation.Disabled;
                        _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellActivation = Activation.Disabled;
                        _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].CellActivation = Activation.Disabled;
                    }

                }
            }
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 210)//trungnq thêm vào để  làm task 6234
                {
                    txtReason.Value = "Nhập kho từ mua hàng";
                    txtReason.Text = "Nhập kho từ mua hàng";
                    _select.Reason = "Nhập kho từ mua hàng";
                };
            }
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 210)//trungnq thêm vào để  làm task 6234
                {
                    if (ultraTabControlThongTinChung.Tabs[0].Text == "Hóa đơn mua hàng")
                    {
                        txtReason.Value = "Mua hàng không qua kho";
                        txtReason.Text = "Mua hàng không qua kho";
                        _select.Reason = "Mua hàng không qua kho";
                    }
                };
            }
            // Utils.ConfigDeductionDebitAccountColumn(this, input.TypeID);
        }
        //Quuân edit
        private void ultraTabControl_SelectedTabChanged(object sender, SelectedTabChangedEventArgs e)
        {
            grHoaDon.Visible = false;
            ultraGroupBoxHoaDon.Dock = DockStyle.Fill;
            var ppInvoid = new PPInvoiceDetail();
            var ppInvoiddb = _select.PPInvoiceDetails.FirstOrDefault();
            if (ppInvoiddb != null)
            {
                ppInvoid = ppInvoiddb;
            }
            if (ppInvoid.InvoiceTemplate != "" || ppInvoid.InvoiceSeries != "" || ppInvoid.InvoiceNo != "" || ppInvoid.InvoiceDate != null)
                chkBillReceived.CheckState = CheckState.Checked;
            if (e.Tab.Key == "uTab1")
            {
                _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].CellActivation = Activation.NoEdit;
                _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].CellActivation = Activation.NoEdit;
                _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellActivation = Activation.NoEdit;
                _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].CellActivation = Activation.NoEdit;

                _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
                _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
                _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
                _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;


                txtMauSoHD.Text = ppInvoid.InvoiceTemplate != null ? ppInvoid.InvoiceTemplate : txtMauSoHD.Text;
                txtKyHieuHD.Text = ppInvoid.InvoiceSeries != null ? ppInvoid.InvoiceSeries : txtKyHieuHD.Text;
                txtSoHD.Text = ppInvoid.InvoiceNo != null ? ppInvoid.InvoiceNo : txtSoHD.Text;
                dteNgayHD.Value = ppInvoid.InvoiceDate != null ? ppInvoid.InvoiceDate : dteNgayHD.Value;
                if (txtMauSoHD.Text != "" || txtKyHieuHD.Text != "" || txtSoHD.Text != "" || dteNgayHD.Value != null)
                    chkBillReceived.CheckState = CheckState.Checked;
                grHoaDon.Visible = true;
                grHoaDon.Anchor = AnchorStyles.Right;
                grHoaDon.Anchor = AnchorStyles.Top;
                ultraGroupBoxHoaDon.Anchor = AnchorStyles.Left;
                ultraGroupBoxHoaDon.Anchor = AnchorStyles.Right;
                ultraGroupBoxHoaDon.Anchor = AnchorStyles.Top;
                ultraGroupBoxHoaDon.Width = ultraTabControlThongTinChung.Width - grHoaDon.Width;
                //ultraGroupBoxHoaDon.Dock = DockStyle.None;
            }
        }
        protected override bool AdditionalFunc()
        {
            //if (lblBillReceived.Visible == true)
            //{
            //    UltraGrid uGridCheck;
            //    uGridCheck = Controls.Find("uGrid1", true).FirstOrDefault() as UltraGrid;
            //    int u = uGridCheck.Rows.Count;
            //    foreach (UltraGridRow row in uGridCheck.Rows)
            //    {
            //        //        UltraGridRow row = uGridCheck.ActiveRow;
            //        if ((row.Cells["InvoiceNo"].Text.IsNullOrEmpty()) || (row.Cells["InvoiceDate"].Text.IsNullOrEmpty()) || (row.Cells["InvoiceSeries"].Text.IsNullOrEmpty()) || (row.Cells["InvoiceTemplate"].Text.IsNullOrEmpty()))
            //        {
            //            MSG.Warning("Chưa nhập đủ thông tin hóa đơn ,vui lòng kiểm tra lại");
            //            return false;
            //        }
            //    }
            //}
            //Quuân edit            
            if (txtKyHieuHD.Text != "" || txtMauSoHD.Text != "" || txtSoHD.Text != "" || dteNgayHD.Value != null)
            {
                if (txtKyHieuHD.Text != "" && txtMauSoHD.Text != "" && txtSoHD.Text != "" && dteNgayHD.Value != null)
                {
                    if (txtSoHD.Text.Length == 7)
                    {
                        foreach (var item in _select.PPInvoiceDetails)
                        {
                            item.InvoiceTemplate = txtMauSoHD.Text;
                            item.InvoiceSeries = txtKyHieuHD.Text;
                            item.InvoiceNo = txtSoHD.Text;
                            item.InvoiceDate = Convert.ToDateTime(dteNgayHD.Value.ToString());
                        }
                        _select.BillReceived = true;
                    }
                    else
                    {
                        MSG.Warning("Số hoá đơn phải là kiểu số và đúng 7 chữ số.");
                        return false;
                    }
                }
                else
                {
                    MSG.Warning("Chưa nhập đủ thông tin hóa đơn ,vui lòng kiểm tra lại");
                    return false;
                }
            }
            else
            {
                foreach (var item in _select.PPInvoiceDetails)
                {
                    item.InvoiceTemplate = "";
                    item.InvoiceSeries = "";
                    item.InvoiceNo = "";
                    item.InvoiceDate = null;
                }
                _select.BillReceived = false;
            }
            #region Bổ sung cập nhật InwardNo sang No cho TH Mua hàng qua kho
            if (TypeFrm == 0 && _select.TypeID == 210 && _select.No != _select.InwardNo)
                _select.No = _select.InwardNo;
            #endregion
            return true;
        }
        #endregion

        #region Event
        private void OptThanhToanValueChanged(object sender, EventArgs e)
        {
            if (_statusForm == ConstFrm.optStatusForm.Edit)
            {


                var opt1 = (UltraOptionSet_Ex)sender;
                if (opt1.Value == null) return;
                if (!(bool)opt1.Value)
                {
                    _select.TypeID = 210;
                    cbbChonThanhToan.Enabled = false;
                    bool status = true;
                    if (_statusForm != ConstFrm.optStatusForm.Add)
                        status = !(_select.TypeID == _backSelect.TypeID);
                    ConfigGroup(status);
                    if (TypeFrm == 0)
                        _select.No = _select.InwardNo;
                    else
                        _select.InwardNo = null;
                }
                else
                {
                    //_iMBCreditCardDetailVendorService.Query
                    if ((Utils.ListMCPaymentDetailVendor.Where(n => n.PPInvoiceID == _select.ID).ToList().Count != 0) || (Utils.ListMBTellerPaperDetailVendor.Where(n => n.PPInvoiceID == _select.ID).ToList().Count != 0) || (Utils.ListMBCreditCardDetailVendor.Where(n => n.PPInvoiceID == _select.ID).ToList().Count != 0))
                    {
                        MSG.Warning("Không thể thực hiện thao tác này! Chứng từ đã phát sinh nghiệp vụ trả tiền nhà cung cấp");
                        opt1.Value = false;
                    }
                    else
                    {
                        cbbChonThanhToan.Enabled = true;
                        if (cbbChonThanhToan.SelectedItem == null)
                        {
                            cbbChonThanhToan.SelectedItem = cbbChonThanhToan.Items.ValueList.FindByDataValue(260);
                            return;
                        }
                        _select.TypeID = (int)cbbChonThanhToan.Value;
                        bool isResetNo = true;
                        if (_statusForm != ConstFrm.optStatusForm.Add)
                            isResetNo = !(_select.TypeID == _backSelect.TypeID);
                        ConfigGroup(isResetNo);
                    }
                }
                var hienthi = !(new List<int> { 260, 261, 262, 263, 264 }.Any(x => x == _select.TypeID) && _select.CurrencyID != "VND");
                if (_uGrid0 != null)
                {
                    var band = _uGrid0.DisplayLayout.Bands[0];
                    band.Columns["CashOutExchangeRate"].Hidden = hienthi;
                    band.Columns["CashOutAmount"].Hidden = hienthi;
                    band.Columns["CashOutDifferAmount"].Hidden = hienthi;
                    band.Columns["CashOutDifferAccount"].Hidden = hienthi;
                }
                if (_uGrid1 != null)
                {
                    var band = _uGrid1.DisplayLayout.Bands[0];
                    band.Columns["CashOutVATAmount"].Hidden = hienthi;
                    band.Columns["CashOutDifferVATAmount"].Hidden = hienthi;
                }
            }
            else
            {
                var opt = (UltraOptionSet_Ex)sender;
                if (opt.Value == null) return;
                if (!(bool)opt.Value)
                {
                    _select.TypeID = 210;
                    cbbChonThanhToan.Enabled = false;
                    bool status = true;
                    if (_statusForm != ConstFrm.optStatusForm.Add)
                        status = !(_select.TypeID == _backSelect.TypeID);
                    ConfigGroup(status);
                    if (TypeFrm == 0)
                        _select.No = _select.InwardNo;
                    else
                        _select.InwardNo = null;
                }
                else
                {
                    cbbChonThanhToan.Enabled = true;
                    if (cbbChonThanhToan.SelectedItem == null)
                    {
                        cbbChonThanhToan.SelectedItem = cbbChonThanhToan.Items.ValueList.FindByDataValue(260);
                        return;
                    }
                    _select.TypeID = (int)cbbChonThanhToan.Value;
                    bool isResetNo = true;
                    if (_statusForm != ConstFrm.optStatusForm.Add)
                        isResetNo = !(_select.TypeID == _backSelect.TypeID);
                    ConfigGroup(isResetNo);
                }
                var hienthi = !(new List<int> { 260, 261, 262, 263, 264 }.Any(x => x == _select.TypeID) && _select.CurrencyID != "VND");
                if (_uGrid0 != null)
                {
                    var band = _uGrid0.DisplayLayout.Bands[0];
                    band.Columns["CashOutExchangeRate"].Hidden = hienthi;
                    band.Columns["CashOutAmount"].Hidden = hienthi;
                    band.Columns["CashOutDifferAmount"].Hidden = hienthi;
                    band.Columns["CashOutDifferAccount"].Hidden = hienthi;
                }
                if (_uGrid1 != null)
                {
                    var band = _uGrid1.DisplayLayout.Bands[0];
                    band.Columns["CashOutVATAmount"].Hidden = hienthi;
                    band.Columns["CashOutDifferVATAmount"].Hidden = hienthi;
                }
            }
            UltraGrid ultraGrid = (UltraGrid)this.Controls.Find("uGrid1", true).FirstOrDefault();
            if (ultraGrid != null && ultraGrid.DisplayLayout.Bands[0].Columns.Exists("DeductionDebitAccount"))//trungnq thêm để làm bug 6453
            {
                if (ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"].Hidden == false)
                {
                    Utils.ConfigAccountColumnInGrid<PPInvoice>(this, _select.TypeID, ultraGrid, ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"], isImportPurchase: _select.IsImportPurchase);
                }
            }
        }

        private void CbbChonThanhToanValueChanged(object sender, EventArgs e)
        {
            var cbb = (UltraComboEditor)sender;
            if (cbb.SelectedItem == null) return;
            _select.TypeID = (int)cbb.Value;
            bool status = true;
            if (_statusForm != ConstFrm.optStatusForm.Add)
                status = !(_select.TypeID == _backSelect.TypeID);
            ConfigGroup(status);
            var hienthi = (_select.CurrencyID == "VND");
            if (_uGrid0 != null)
            {
                var band = _uGrid0.DisplayLayout.Bands[0];
                band.Columns["CashOutExchangeRate"].Hidden = hienthi;
                band.Columns["CashOutAmount"].Hidden = hienthi;
                band.Columns["CashOutDifferAmount"].Hidden = hienthi;
                band.Columns["CashOutDifferAccount"].Hidden = hienthi;
            }
            if (_uGrid1 != null)
            {
                var band = _uGrid1.DisplayLayout.Bands[0];
                band.Columns["CashOutVATAmount"].Hidden = hienthi;
                band.Columns["CashOutDifferVATAmount"].Hidden = hienthi;
            }
            UltraGrid ultraGrid = (UltraGrid)this.Controls.Find("uGrid1", true).FirstOrDefault();
            if (ultraGrid != null && ultraGrid.DisplayLayout.Bands[0].Columns.Exists("DeductionDebitAccount"))//trungnq thêm để làm bug 6453
            {
                if (ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"].Hidden == false)
                {
                    Utils.ConfigAccountColumnInGrid<PPInvoice>(this, _select.TypeID, ultraGrid, ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"], isImportPurchase: _select.IsImportPurchase);
                }
            }
        }
        void uGridControl_CellChange(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key == "IsImportPurchase")
            {
                btnApportionTaxes.EnabledControl(e.Cell.Text == "Nhập khẩu");
                if (e.Cell.Text == "Trong nước")
                {
                    foreach (var row in _uGrid2.Rows)
                    {
                        row.Cells["ImportTaxRate"].Value = (decimal)0;
                        row.Cells["ImportTaxAmountOriginal"].Value = (decimal)0;
                    }
                    lblTotalImportTaxAmount.Text = "0";
                    lblTotalAllOriginal.DataBindings.Clear();
                    lblTotalAllOriginal.BindControl("Text", _select, "TotalAllOriginal", ConstDatabase.Format_TienVND, ConstDatabase.Format_ForeignCurrency);
                    lblTotalAll.DataBindings.Clear();
                    lblTotalAll.BindControl("Text", _select, "TotalAll", ConstDatabase.Format_TienVND);
                }
                else
                {
                    lblTotalAllOriginal.DataBindings.Clear();
                    lblTotalAllOriginal.BindControl("Text", _select, "TotalAllNoVatOriginal", ConstDatabase.Format_TienVND, ConstDatabase.Format_ForeignCurrency);
                    lblTotalAll.DataBindings.Clear();
                    lblTotalAll.BindControl("Text", _select, "TotalAllNoVat", ConstDatabase.Format_TienVND);
                }
                UltraGrid ultraGrid = (UltraGrid)this.Controls.Find("uGrid1", true).FirstOrDefault();
                if (ultraGrid != null && ultraGrid.DisplayLayout.Bands[0].Columns.Exists("DeductionDebitAccount"))//trungnq thêm để làm bug 6453
                {
                    if (ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"].Hidden == false)
                    {
                        if (e.Cell.Text == "Nhập khẩu")
                            Utils.ConfigAccountColumnInGrid<PPInvoice>(this, _select.TypeID, ultraGrid, ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"], isImportPurchase: true);
                        else
                            Utils.ConfigAccountColumnInGrid<PPInvoice>(this, _select.TypeID, ultraGrid, ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"], isImportPurchase: false);

                    }
                }
            }
        }

        #endregion

        #region Utils

        public void ConfigGridBase(PPInvoice input, UltraGrid uGridControlInput, UltraPanel pnlUgrid, bool haveUgridControl = true)
        {
            #region Config Grid
            if (!input.HasProperty("TypeID") || !input.HasProperty("TemplateID")) return;
            int typeIdInput = input.GetProperty<PPInvoice, int>("TypeID");
            Guid? templateIdInput = input.GetProperty<PPInvoice, Guid?>("TemplateID");
            //Load config hiển thị dữ liệu từ Database
            Template mauGiaoDien = Utils.GetMauGiaoDien(typeIdInput, templateIdInput, Utils.ListTemplate);
            _select.SetProperty("TemplateID", _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : templateIdInput);
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                bdlRefVoucher = new BindingList<RefVoucher>(input.RefVouchers);
            }
            _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
            this.ConfigGridByTemplete_General<PPInvoice>(pnlUgrid, mauGiaoDien, new[] { typeof(BindingList<PPInvoiceDetailCost>) });
            //this.ConfigGridByTemplete<PPInvoice>(typeIdInput, mauGiaoDien, true, _listObjectInput, blackStandList: new[] { typeof(BindingList<PPInvoiceDetailCost>) });
            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { true, true, true, true, false };
            this.ConfigGridByManyTemplete<PPInvoice>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString(), blackStandList: new[] { typeof(BindingList<PPInvoiceDetailCost>) });

            if (haveUgridControl && input.HasProperty("TotalAmountOriginal") && input.HasProperty("TotalAmount") && input.HasProperty("CurrencyID"))
            {
                decimal totalAmountOriginalInput = input.GetProperty<PPInvoice, decimal>("TotalAmountOriginal");
                decimal totalAmountInput = input.GetProperty<PPInvoice, decimal>("TotalAmount");
                string currencyIdInput = input.GetProperty<PPInvoice, string>("CurrencyID");
                //config Grid
                _select.SetProperty("TotalAmountOriginal", _statusForm == ConstFrm.optStatusForm.Add ? 0 : totalAmountOriginalInput);
                _select.SetProperty("TotalAmount", _statusForm == ConstFrm.optStatusForm.Add ? 0 : totalAmountInput);
                _select.SetProperty("CurrencyID", _statusForm == ConstFrm.optStatusForm.Add ? "VND" : currencyIdInput);
                _select.IsImportPurchase = _statusForm == ConstFrm.optStatusForm.Add ? false : input.IsImportPurchase;
                this.ConfigGridCurrencyByTemplate<PPInvoice>(typeIdInput, new BindingList<IList> { new List<PPInvoice> { _select } }, uGridControlInput, mauGiaoDien);
            }

            #endregion
        }
        void ConfigByType(PPInvoice input, bool status)
        {
            switch (input.TypeID)
            {
                case 210: //Mua hàng chưa thanh toán
                    #region Mua hàng chưa thanh toán
                    {
                        //optThanhToan.CheckedIndex = 0;  //tự động chọn "chưa thanh toán"
                        cbbChonThanhToan.Enabled = false;
                        ultraTabControlThongTinChung.Tabs[0].Visible = true;
                        ultraTabControlThongTinChung.Tabs[1].Visible = false;
                        ultraTabControlThongTinChung.Tabs[2].Visible = false;
                        ultraTabControlThongTinChung.Tabs[3].Visible = false;
                        ultraTabControlThongTinChung.Tabs[4].Visible = false;
                        ultraTabControlThongTinChung.Tabs[5].Visible = false;
                        palVouchersNoHd.Visible = true;
                        palVouchersNoTm.Visible = false;
                        if (TypeFrm == 1 && !_isFirst && status)
                            this.ConfigTopVouchersNo<PPInvoice>(palVouchersNoHd, "No", "PostedDate", "Date", resetNo: status);

                    }
                    #endregion
                    break;
                case 260: //mua hàng thanh toán ngay tiền mặt
                    #region mua hàng thanh toán ngay tiền mặt
                    {
                        //optThanhToan.CheckedIndex = 1;  //tự động chọn "thanh toán ngay"
                        cbbChonThanhToan.Enabled = true;    //hiện cbb cho chọn thanh toán ngay bằng "tiền mặt" hay "chuyển khoản"
                        //cbbChonThanhToan.SelectedIndex = 0; //mặc định là tiền mặt
                        ultraTabControlThongTinChung.Tabs[0].Visible = true;
                        ultraTabControlThongTinChung.Tabs[1].Visible = true;
                        ultraTabControlThongTinChung.Tabs[2].Visible = false;
                        ultraTabControlThongTinChung.Tabs[3].Visible = false;
                        ultraTabControlThongTinChung.Tabs[4].Visible = false;
                        ultraTabControlThongTinChung.Tabs[5].Visible = false;
                        palVouchersNoHd.Visible = false;
                        palVouchersNoTm.Visible = true;
                        if (TypeFrm == 1)
                            ultraTabControlThongTinChung.Tabs[0].Visible = false;
                        this.ConfigTopVouchersNo<PPInvoice>(palVouchersNoTm, "No", "MPostedDate", "MDate", ConstFrm.TypeGroupMCPayment, objectDataSource: _select, resetNo: status);
                        if (_statusForm == ConstFrm.optStatusForm.Add)
                        {
                            if (input.TypeID == 260)//trungnq thêm vào để  làm task 6234
                            {
                                txtMReasonPayTm.Value = "Chi tiền mua hàng bằng TM";
                                txtMReasonPayTm.Text = "Chi tiền mua hàng bằng TM";
                                _select.MReasonPay = "Chi tiền mua hàng bằng TM";
                            };
                        }
                    }
                    #endregion
                    break;
                case 261: //mua hàng thanh toán ngay UNC
                    #region mua hàng thanh toán ngay UNC
                    {
                        //optThanhToan.CheckedIndex = 1;  //tự động chọn "thanh toán ngay"
                        cbbChonThanhToan.Enabled = true;    //hiện cbb cho chọn thanh toán ngay bằng "tiền mặt" hay "chuyển khoản"
                        //cbbChonThanhToan.SelectedIndex = _statusForm == ConstFrm.optStatusForm.Add ? 0 : 1; //mặc định là tiền mặt
                        ultraTabControlThongTinChung.Tabs[0].Visible = true;
                        ultraTabControlThongTinChung.Tabs[1].Visible = false;
                        ultraTabControlThongTinChung.Tabs[2].Visible = true;
                        ultraTabControlThongTinChung.Tabs[3].Visible = false;
                        ultraTabControlThongTinChung.Tabs[4].Visible = false;
                        ultraTabControlThongTinChung.Tabs[5].Visible = false;
                        palVouchersNoHd.Visible = false;
                        palVouchersNoTm.Visible = true;
                        if (TypeFrm == 1)
                            ultraTabControlThongTinChung.Tabs[0].Visible = false;
                        this.ConfigTopVouchersNo<PPInvoice>(palVouchersNoTm, "No", "MPostedDate", "MDate", ConstFrm.TypeGruop_MBTellerPaper, objectDataSource: _select, resetNo: status);
                        if (_statusForm == ConstFrm.optStatusForm.Add)
                        {
                            if (input.TypeID == 261)//trungnq thêm vào để  làm task 6234
                            {
                                txtMReasonPayUnc.Value = "Chi tiền mua hàng bằng TGNH";
                                txtMReasonPayUnc.Text = "Chi tiền mua hàng bằng TGNH";
                                _select.MReasonPay = "Chi tiền mua hàng bằng TGNH";
                            };
                        }
                    }
                    #endregion
                    break;
                case 262: //mua hàng thanh toán ngay chuyển khoản
                    #region mua hàng thanh toán ngay séc chuyển khoản
                    {
                        cbbChonThanhToan.Enabled = true;    //hiện cbb cho chọn thanh toán ngay bằng "tiền mặt" hay "chuyển khoản"
                        ultraTabControlThongTinChung.Tabs[0].Visible = true;
                        ultraTabControlThongTinChung.Tabs[1].Visible = false;
                        ultraTabControlThongTinChung.Tabs[2].Visible = false;
                        ultraTabControlThongTinChung.Tabs[3].Visible = true;
                        ultraTabControlThongTinChung.Tabs[4].Visible = false;
                        ultraTabControlThongTinChung.Tabs[5].Visible = false;
                        palVouchersNoHd.Visible = false;
                        palVouchersNoTm.Visible = true;
                        if (TypeFrm == 1)
                            ultraTabControlThongTinChung.Tabs[0].Visible = false;
                        this.ConfigTopVouchersNo<PPInvoice>(palVouchersNoTm, "No", "MPostedDate", "MDate", 13, objectDataSource: _select, resetNo: status);
                        if (_statusForm == ConstFrm.optStatusForm.Add)
                        {
                            if (input.TypeID == 262)//trungnq thêm vào để  làm task 6234
                            {
                                txtMReasonPayUnc.Value = "Chi tiền mua hàng bằng TGNH";
                                txtMReasonPayUnc.Text = "Chi tiền mua hàng bằng TGNH";
                                _select.MReasonPay = "Chi tiền mua hàng bằng TGNH";
                            };
                        }
                    }
                    #endregion
                    break;
                case 263: //mua hàng thanh toán ngay TTD
                    #region mua hàng thanh toán ngay TTD
                    {
                        //optThanhToan.CheckedIndex = 1;  //tự động chọn "thanh toán ngay"
                        cbbChonThanhToan.Enabled = true;    //hiện cbb cho chọn thanh toán ngay bằng "tiền mặt" hay "chuyển khoản"
                        //cbbChonThanhToan.SelectedIndex = _statusForm == ConstFrm.optStatusForm.Add ? 0 : 1; //mặc định là tiền mặt
                        ultraTabControlThongTinChung.Tabs[0].Visible = true;
                        ultraTabControlThongTinChung.Tabs[1].Visible = false;
                        ultraTabControlThongTinChung.Tabs[2].Visible = false;
                        ultraTabControlThongTinChung.Tabs[3].Visible = false;
                        ultraTabControlThongTinChung.Tabs[4].Visible = false;
                        ultraTabControlThongTinChung.Tabs[5].Visible = true;
                        palVouchersNoHd.Visible = false;
                        palVouchersNoTm.Visible = true;
                        if (TypeFrm == 1)
                            ultraTabControlThongTinChung.Tabs[0].Visible = false;
                        this.ConfigTopVouchersNo<PPInvoice>(palVouchersNoTm, "No", "MPostedDate", "MDate", ConstFrm.TypeGroup_MBCreditCard, objectDataSource: _select, resetNo: status);
                        if (_statusForm == ConstFrm.optStatusForm.Add)
                        {
                            if (input.TypeID == 263)//trungnq thêm vào để  làm task 6234
                            {
                                txtMReasonPayUnc.Value = "Chi tiền mua hàng bằng TGNH";
                                txtMReasonPayUnc.Text = "Chi tiền mua hàng bằng TGNH";
                                _select.MReasonPay = "Chi tiền mua hàng bằng TGNH";
                            };
                        }
                    }
                    #endregion
                    break;
                case 264: //mua hàng thanh toán ngay STM
                    #region mua hàng thanh toán ngay STM
                    {
                        //optThanhToan.CheckedIndex = 1;  //tự động chọn "thanh toán ngay"
                        cbbChonThanhToan.Enabled = true;    //hiện cbb cho chọn thanh toán ngay bằng "tiền mặt" hay "chuyển khoản"
                        //cbbChonThanhToan.SelectedIndex = _statusForm == ConstFrm.optStatusForm.Add ? 0 : 1; //mặc định là tiền mặt
                        ultraTabControlThongTinChung.Tabs[0].Visible = true;
                        ultraTabControlThongTinChung.Tabs[1].Visible = false;
                        ultraTabControlThongTinChung.Tabs[2].Visible = false;
                        ultraTabControlThongTinChung.Tabs[3].Visible = false;
                        ultraTabControlThongTinChung.Tabs[4].Visible = true;
                        ultraTabControlThongTinChung.Tabs[5].Visible = false;
                        palVouchersNoHd.Visible = false;
                        palVouchersNoTm.Visible = true;
                        if (TypeFrm == 1)
                            ultraTabControlThongTinChung.Tabs[0].Visible = false;
                        this.ConfigTopVouchersNo<PPInvoice>(palVouchersNoTm, "No", "MPostedDate", "MDate", 14, objectDataSource: _select, resetNo: status);
                        if (_statusForm == ConstFrm.optStatusForm.Add)
                        {
                            if (input.TypeID == 264)//trungnq thêm vào để  làm task 6234
                            {
                                txtMReasonPayUnc.Value = "Chi tiền mua hàng bằng TGNH";
                                txtMReasonPayUnc.Text = "Chi tiền mua hàng bằng TGNH";
                                _select.MReasonPay = "Chi tiền mua hàng bằng TGNH";
                            };
                        }
                    }
                    #endregion
                    break;
                default:
                    {
                        ultraTabControlThongTinChung.Tabs[0].Visible = true;
                        ultraTabControlThongTinChung.Tabs[1].Visible = false;
                        ultraTabControlThongTinChung.Tabs[2].Visible = false;
                        ultraTabControlThongTinChung.Tabs[3].Visible = false;
                        ultraTabControlThongTinChung.Tabs[4].Visible = false;
                        ultraTabControlThongTinChung.Tabs[5].Visible = false;
                        palVouchersNoHd.Visible = false;
                        palVouchersNoTm.Visible = false;
                        if (TypeFrm == 0)
                            _select.No = _select.InwardNo;
                    }
                    break;
            }
            if (!status && !_isFirst && _select.TypeID == _backSelect.TypeID)
            {
                if (_backSelect.TypeID == 210)
                {
                    if (TypeFrm == 0)
                        _select.InwardNo = _backSelect.InwardNo.CloneObject();
                    else
                        _select.No = _backSelect.No.CloneObject();
                    var topvoucher1 = palVouchersNoHd.GetTopVouchers<PPInvoice>();
                    if (topvoucher1 != null)
                    {
                        topvoucher1.No = TypeFrm == 0 ? _select.InwardNo : _select.No;
                    }
                }
                _select.No = _backSelect.No.CloneObject();
                var topvoucher2 = palVouchersNoTm.GetTopVouchers<PPInvoice>();
                if (topvoucher2 != null)
                {
                    topvoucher2.No = _select.No;
                }
            }
        }
        private void ConfigGroup(bool status = true)
        {
            ConfigByType(_select, status);
            ConfigTopByTabIndex(ultraTabControlThongTinChung.ActiveTab == null ? 0 : ultraTabControlThongTinChung.ActiveTab.Index);
            if (TypeGroup == null) return;
            this.ReconfigAccountColumn<PPInvoice>(_select.TypeID);
            if (_select.TemplateID == null) return;
            //this.ReloadTemplate<PPInvoice>((Guid)_select.TemplateID, false, true, blackStandList: new[] { typeof(BindingList<PPInvoiceDetailCost>) });
            this.ConfigTemplateEdit<PPInvoice>(utmDetailBaseToolBar, TypeID, _statusForm, _select.TemplateID);
            _isFirst = false;
            if (_uGrid0 != null) _uGrid0.DisplayLayout.Bands[0].Columns["RepositoryID"].Hidden = TypeFrm == 1;
        }
        void ConfigTopByTabIndex(int index)
        {
            palVouchersNoHd.Visible = false;
            palVouchersNoTm.Visible = false;
            switch (index)
            {
                case 0:
                    palVouchersNoHd.Visible = true; break;
                case 1:
                    palVouchersNoTm.Visible = true; break;
                case 2:
                    palVouchersNoTm.Visible = true; break;
                case 3:
                    palVouchersNoTm.Visible = true; break;
                case 4:
                    palVouchersNoTm.Visible = true; break;
                case 5:
                    palVouchersNoTm.Visible = true; break;
            }
        }
        #endregion
        private void ultraTabControlThongTinChung_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            ConfigTopByTabIndex(e.Tab.Index);

        }

        private void btnPPOrder_Click(object sender, EventArgs e)
        {
            var f = new FPPSelectOrders<PPInvoice>(_select, _select.AccountingObjectID, _select.PostedDate);
            f.FormClosed += new FormClosedEventHandler(fSelectOrders_FormClosed);
            try
            {
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }

        private void fSelectOrders_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (FPPSelectOrders<PPInvoice>)sender;
            if (e.CloseReason != CloseReason.UserClosing) return;
            if (frm.DialogResult == DialogResult.OK)
            {
                var selectOrders = frm.SelectOrders;
                //var uGrid1 = (UltraGrid)Controls.Find("uGrid0", true).FirstOrDefault();
                if (_uGrid0 != null)
                {
                    var source = (BindingList<PPInvoiceDetail>)_uGrid0.DataSource;
                    if (selectOrders.Count > 0) source.Clear();
                    foreach (var selectOrder in selectOrders)
                    {
                        _uGrid0.AddNewRow4Grid();
                        var row = _uGrid0.Rows[_uGrid0.Rows.Count - 1];
                        row.Cells["ID"].Value = Guid.NewGuid();
                        row.Cells["MaterialGoodsID"].Value = selectOrder.MaterialGoodsID;
                        row.UpdateData();
                        _uGrid0.UpdateData();
                        row.Cells["Description"].Value = selectOrder.MaterialGoodsName;
                        row.Cells["RepositoryID"].Value = selectOrder.PpOrderDetail.RepositoryID;
                        row.Cells["DebitAccount"].Value = Utils.ListMaterialGoodsCustom.FirstOrDefault(x => x.ID == selectOrder.MaterialGoodsID).ReponsitoryAccount;
                        row.Cells["CreditAccount"].Value = selectOrder.PpOrderDetail.CreditAccount;
                        row.Cells["Unit"].Value = selectOrder.PpOrderDetail.Unit;
                        row.Cells["UnitConvert"].Value = selectOrder.PpOrderDetail.UnitConvert;
                        row.Cells["UnitPrice"].Value = selectOrder.PpOrderDetail.UnitPrice;
                        row.Cells["UnitPriceOriginal"].Value = selectOrder.PpOrderDetail.UnitPriceOriginal;
                        row.Cells["UnitPriceConvert"].Value = selectOrder.PpOrderDetail.UnitPriceConvert;
                        row.Cells["UnitPriceConvertOriginal"].Value = selectOrder.PpOrderDetail.UnitPriceConvertOriginal;
                        row.Cells["Quantity"].Value = selectOrder.QuantityInward;
                        row.Cells["Amount"].Value = selectOrder.PpOrderDetail.UnitPrice *
                                                    (selectOrder.QuantityInward ?? 0);
                        row.Cells["AmountOriginal"].Value = selectOrder.PpOrderDetail.UnitPriceOriginal *
                                                            (selectOrder.QuantityInward ?? 0);
                        row.Cells["AccountingObjectID"].Value = selectOrder.AccountingObjectID;
                        row.Cells["PPOrderID"].Value = selectOrder.ID;
                        row.Cells["PPOrderNo"].Value = selectOrder.No;
                        row.Cells["DetailID"].Value = selectOrder.PpOrderDetail.ID;
                        row.Cells["QuantityConvert"].Value = selectOrder.PpOrderDetail.QuantityConvert;
                        row.Cells["VATDescription"].Value = selectOrder.PpOrderDetail.VATDescription;
                        row.Cells["VATRate"].Value = selectOrder.PpOrderDetail.VATRate;
                        //row.Cells["VATAmount"].Value = selectOrder.PpOrderDetail.VATAmount;
                        row.Cells["VATAmountOriginal"].Value = selectOrder.PpOrderDetail.VATAmountOriginal;
                        row.Cells["VATAccount"].Value = selectOrder.PpOrderDetail.VATAccount;
                        row.Cells["CostSetID"].Value = selectOrder.PpOrderDetail.CostSetID;
                        row.Cells["ContractID"].Value = selectOrder.PpOrderDetail.ContractID;
                        row.Cells["StatisticsCodeID"].Value = selectOrder.PpOrderDetail.StatisticsCodeID;
                        row.Cells["DepartmentID"].Value = selectOrder.PpOrderDetail.DepartmentID;
                        row.Cells["ExpenseItemID"].Value = selectOrder.PpOrderDetail.ExpenseItemID;
                        row.Cells["BudgetItemID"].Value = selectOrder.PpOrderDetail.BudgetItemID;
                        row.Cells["ExpiryDate"].Value = selectOrder.PpOrderDetail.ExpiryDate;
                        row.Cells["LotNo"].Value = selectOrder.PpOrderDetail.LotNo;
                        row.Cells["UnitConvert"].Value = selectOrder.PpOrderDetail.UnitConvert;
                        row.Cells["ConvertRate"].Value = selectOrder.PpOrderDetail.ConvertRate;
                        row.Cells["UnitPriceAfterTax"].Value = selectOrder.PpOrderDetail.UnitPriceAfterTax;
                        row.Cells["UnitPriceAfterTaxOriginal"].Value = selectOrder.PpOrderDetail.UnitPriceAfterTaxOriginal;
                        row.Cells["AmountAfterTax"].Value = selectOrder.PpOrderDetail.AmountAfterTax;
                        row.Cells["AmountAfterTaxOriginal"].Value = selectOrder.PpOrderDetail.AmountAfterTaxOriginal;
                        row.Cells["DiscountRate"].Value = selectOrder.PpOrderDetail.DiscountRate;
                        row.Cells["DiscountAmount"].Value = selectOrder.PpOrderDetail.DiscountAmount;
                        row.Cells["DiscountAmountOriginal"].Value = selectOrder.PpOrderDetail.DiscountAmountOriginal;
                        row.Cells["DiscountAmountAfterTax"].Value = selectOrder.PpOrderDetail.DiscountAmountAfterTax;
                        row.Cells["DiscountAmountAfterTaxOriginal"].Value = selectOrder.PpOrderDetail.DiscountAmountAfterTaxOriginal;
                        row.UpdateData();
                        //foreach (var rw in _uGrid0.Rows)
                        //{
                        //    foreach (var cell in rw.Cells)
                        //    {
                        _uGrid0.ActiveCell = row.Cells["Quantity"];
                        this.DoEverythingInGrid<PPInvoice>(_uGrid0);
                        //    }
                        //}
                    }
                }
                var grpCurrency = (from u in selectOrders group u by u.CurrencyID into g select new { CurrencyID = g.Key, ExchangeRate = g.Select(p => p.ExchangeRate) });
                if (grpCurrency.Count() == 1)
                {
                    var f = grpCurrency.FirstOrDefault();
                    if (f != null)
                    {
                        _select.CurrencyID = f.CurrencyID;
                        var currency = Utils.ListCurrency.FirstOrDefault(c => c.ID == _select.CurrencyID);
                        var grpEx = f.ExchangeRate.GroupBy(x => x).Select(x => x.Key).ToList();
                        decimal? exrate = grpEx.Count == 1 ? grpEx.FirstOrDefault() : (decimal?)null;
                        this.AutoExchangeByCurrencySelected(currency, exrate);
                        if (uGridControl != null && uGridControl.DisplayLayout.Bands[0].Columns.Exists("CurrencyID"))
                        {
                            this.ConfigGridCurrencyByCurrency(uGridControl, currency.ID, exrate ?? 1);
                        }
                    }
                }
                var orderDates = selectOrders.GroupBy(k => k.Date).Select(k => k.Key).ToList();
                if (orderDates.Count == 1)
                {
                    _select.DueDate = orderDates.FirstOrDefault();
                    uGridControl.Rows[0].Refresh(RefreshRow.RefreshDisplay);
                    uGridControl.Rows[0].Refresh(RefreshRow.ReloadData);
                }

                //THOHD - 14/11/2017 - issue SDSACC-16 - lấy TK Có khi chọn hình thức thanh toán trước
                ConfigGroup(false);
                if (_uGrid0.Rows.Count > 0)
                    foreach (var row in _uGrid0.Rows)
                        foreach (var cell in row.Cells)
                            Utils.CheckErrorInCell<PPInvoice>(this, _uGrid0, cell);
            }
        }

        private void btnApportion_Click(object sender, EventArgs e)
        {
            if (_statusForm == ConstFrm.optStatusForm.Add)
                foreach (var detail in _select.PPInvoiceDetails)
                {
                    detail.ID = Guid.NewGuid();
                }
            var f = new FCostAlLocation(_select, _statusForm);
            f.FormClosed += new FormClosedEventHandler(fCostAllocation_FormClosed);
            try
            {
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }

        private void fCostAllocation_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FCostAlLocation)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var ppInvoice = f.PPInvoice;
                    if (_uGrid0 == null) return;
                    foreach (var row in _uGrid0.Rows)
                    {
                        var detail = ppInvoice.PPInvoiceDetails.FirstOrDefault(x => x.ID == (Guid)row.Cells["ID"].Value);
                        if (detail == null) continue;
                        row.Cells["FreightAmount"].Value = detail.FreightAmount;
                        row.Cells["FreightAmountOriginal"].Value = detail.FreightAmountOriginal;
                        row.CalculationImportTaxAmount(this);
                        if (TypeFrm == 0)
                        {
                            row.CalculationInwardAmount(this);
                            this.UpdateSelectFromSummaryValue<PPInvoice>(row.Cells["InwardAmount"]);
                            this.UpdateSelectFromSummaryValue<PPInvoice>(row.Cells["InwardAmountOriginal"]);

                        }
                        this.UpdateSelectFromSummaryValue<PPInvoice>(row.Cells["FreightAmount"]);
                        this.UpdateSelectFromSummaryValue<PPInvoice>(row.Cells["FreightAmountOriginal"]);
                    }
                    var a = _listObjectInput.FirstOrDefault(x => (x is BindingList<PPInvoiceDetailCost>)) as BindingList<PPInvoiceDetailCost>;
                    if (a != null)
                    {
                        while (a.Count != 0)
                        {
                            a = _listObjectInput.FirstOrDefault(x => (x is BindingList<PPInvoiceDetailCost>)) as BindingList<PPInvoiceDetailCost>;
                            a.Clear();
                        }
                        _uGrid3.Update();
                        ppInvoice.PPInvoiceDetailCosts.AddToBindingList(a);
                        _uGrid3.Update();
                    }
                }
            }
        }

        private void btnApportionTaxes_Click(object sender, EventArgs e)
        {
            if (_statusForm == ConstFrm.optStatusForm.Add)
                foreach (var detail in _select.PPInvoiceDetails)
                {
                    detail.ID = Guid.NewGuid();
                }
            var f = new FAllocationImportTax(_select, _statusForm);
            f.FormClosed += new FormClosedEventHandler(fAllocationImportTax_FormClosed);
            try
            {
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }

        private void fAllocationImportTax_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FAllocationImportTax)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var ppInvoice = f.PPInvoice;
                    if (_uGrid0 == null) return;
                    foreach (var row in _uGrid0.Rows)
                    {
                        var detail = ppInvoice.PPInvoiceDetails.FirstOrDefault(x => x.ID == (Guid)row.Cells["ID"].Value);
                        if (detail == null) continue;
                        row.Cells["ImportTaxExpenseAmount"].Value = detail.ImportTaxExpenseAmount;
                        row.Cells["ImportTaxExpenseAmountOriginal"].Value = detail.ImportTaxExpenseAmountOriginal;
                        row.UpdateData();
                        row.CalculationImportTaxAmount(this);
                        if (TypeFrm == 0) row.CalculationInwardAmount(this);
                        row.CalculationVatAmountSpecialCase(this);
                        this.UpdateSelectFromSummaryValue<PPInvoice>(row.Cells["ImportTaxAmount"]);
                        this.UpdateSelectFromSummaryValue<PPInvoice>(row.Cells["ImportTaxAmountOriginal"]);
                        this.UpdateSelectFromSummaryValue<PPInvoice>(row.Cells["ImportTaxExpenseAmount"]);
                        this.UpdateSelectFromSummaryValue<PPInvoice>(row.Cells["ImportTaxExpenseAmountOriginal"]);
                        if (TypeFrm == 0)
                        {
                            this.UpdateSelectFromSummaryValue<PPInvoice>(row.Cells["InwardAmount"]);
                            this.UpdateSelectFromSummaryValue<PPInvoice>(row.Cells["InwardAmountOriginal"]);
                        }
                        this.UpdateSelectFromSummaryValue<PPInvoice>(row.Cells["VATAmount"]);
                        this.UpdateSelectFromSummaryValue<PPInvoice>(row.Cells["VATAmountOriginal"]);
                        var a = _listObjectInput.FirstOrDefault(x => (x is BindingList<PPInvoiceDetailCost>)) as BindingList<PPInvoiceDetailCost>;
                        if (a != null)
                        {
                            while (a.Count != 0)
                            {
                                a = _listObjectInput.FirstOrDefault(x => (x is BindingList<PPInvoiceDetailCost>)) as BindingList<PPInvoiceDetailCost>;
                                a.Clear();
                            }
                            _uGrid3.Update();
                            ppInvoice.PPInvoiceDetailCosts.AddToBindingList(a);
                            //foreach (var x in ppInvoice.PPInvoiceDetailCosts) if (x.CostType == true) _select.PPInvoiceDetailCosts.Add(x);
                            _uGrid3.Update();
                        }
                    }
                }
            }
        }

        private void btnCongNo_Click(object sender, EventArgs e)
        {
            decimal debt = Utils.IGeneralLedgerService.GetCredCustomer(_select.AccountingObjectID ?? Guid.Empty, _select.PostedDate);
            MSG.MessageBoxStand(debt.ToStringNumbericFormat(ConstDatabase.Format_TienVND), MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        //Quuân edit
        private void chkBillReceived_CheckStateChanged(object sender, EventArgs e)
        {
            lblBillReceived.Visible = false;

            //if (_statusForm != ConstFrm.optStatusForm.View)
            //{
            //    if (chkBillReceived.CheckState == CheckState.Checked)
            //    {
            //        _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].CellActivation = Activation.AllowEdit;
            //        _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].CellActivation = Activation.AllowEdit;
            //        _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellActivation = Activation.AllowEdit;
            //        _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].CellActivation = Activation.AllowEdit;
            //    }
            //    else
            //    {
            //        _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].CellActivation = Activation.NoEdit;
            //        _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].CellActivation = Activation.NoEdit;
            //        _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellActivation = Activation.NoEdit;
            //        _uGrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].CellActivation = Activation.NoEdit;
            //        foreach (var x in _select.PPInvoiceDetails)
            //        {
            //            x.InvoiceSeries = null;
            //            x.InvoiceTemplate = null;
            //            x.InvoiceNo = null;
            //            x.InvoiceDate = null;
            //        }
            //    }
            //}
        }

        private void FPPInvoiceDetail_Leave(object sender, EventArgs e)
        {
        }

        private void ultraTabControlThongTinChung_Leave(object sender, EventArgs e)
        {
            //if (ultraTabControlThongTinChung.TabIndex.Equals("tabTheTinDung"))
            //{
            //    //cbbMAccountingObjectBankAccountDetailIDTtd.DataBindings.Add("Value", _select, "MAccountingObjectBankAccountDetailID", true, DataSourceUpdateMode.OnPropertyChanged);
            //}
        }
        private void uGrid3_AfterRowsDeleted(object sender, EventArgs e)
        {
            var list = _listObjectInput.FirstOrDefault(x => (x is BindingList<PPInvoiceDetailCost>)) as BindingList<PPInvoiceDetailCost>;
            var ppdc = new PPInvoiceDetailCost();
            for (int i = 0; i < _select.PPInvoiceDetailCosts.Count; i++)
            {
                if (!list.Any(x => x.ID == _select.PPInvoiceDetailCosts[i].ID))
                {
                    ppdc = _select.PPInvoiceDetailCosts[i];
                    _select.PPInvoiceDetailCosts.RemoveAt(i);
                }
            }
            if (list.Any(x => x.ID != ppdc.ID && x.PPServiceID == ppdc.PPServiceID))
            {
                var i = list.FirstOrDefault(x => x.ID != ppdc.ID && x.PPServiceID == ppdc.PPServiceID);
                if (i.AccumulatedAllocateAmountOriginal > ppdc.AccumulatedAllocateAmountOriginal)
                {
                    i.AccumulatedAllocateAmountOriginal = i.AccumulatedAllocateAmountOriginal - ppdc.AmountPBOriginal;
                    i.AccumulatedAllocateAmount = i.AccumulatedAllocateAmount - ppdc.AmountPB;
                }
            }
            var p = ((decimal?)_uGrid0.Rows.SummaryValues["SumAmountOriginal"].Value ?? 0);
            if (p > 0)
                foreach (var row in _uGrid0.Rows)
                {
                    var a = _select.PPInvoiceDetailCosts.Where(x => x.CostType == false).Sum(x => x.AmountPB);
                    var b = ((decimal)(row.Cells["AmountOriginal"].Value ?? 0));
                    var c = (a == 0 || b == 0) ? 0 : (b / p * 100);
                    var d = (a == 0 || b == 0) ? 0 : (a * c / 100);
                    row.Cells["FreightAmount"].Value = d;
                    row.Cells["FreightAmountOriginal"].Value = _select.CurrencyID == "VND" ? d : d / _select.ExchangeRate;
                    var f = _select.PPInvoiceDetailCosts.Where(x => x.CostType == true).Sum(x => x.AmountPB);
                    var g = ((decimal)(row.Cells["AmountOriginal"].Value ?? 0));
                    var h = (f == 0 || g == 0) ? 0 : (g / p * 100);
                    var i = (f == 0 || g == 0) ? 0 : (f * h / 100);
                    row.Cells["ImportTaxExpenseAmount"].Value = i;
                    row.Cells["ImportTaxExpenseAmountOriginal"].Value = _select.CurrencyID == "VND" ? i : i / _select.ExchangeRate;
                    row.CalculationImportTaxAmount(this);
                    if (TypeFrm == 0) row.CalculationInwardAmount(this);
                    if (TypeFrm == 0)
                    {
                        this.UpdateSelectFromSummaryValue<PPInvoice>(row.Cells["InwardAmount"]);
                        this.UpdateSelectFromSummaryValue<PPInvoice>(row.Cells["InwardAmountOriginal"]);
                    }
                }
        }
        private void uGrid_AfterRowsDeleted(object sender, EventArgs e)
        {
            var list = _listObjectInput.FirstOrDefault(x => (x is BindingList<PPInvoiceDetailCost>)) as BindingList<PPInvoiceDetailCost>;
            if (list.Count == 0) return;
            var p = ((decimal?)_uGrid0.Rows.SummaryValues["SumAmountOriginal"].Value ?? 0);
            if (p > 0)
                foreach (var row in _uGrid0.Rows)
                {
                    var a = _select.PPInvoiceDetailCosts.Where(x => x.CostType == false).Sum(x => x.AmountPB);
                    var b = ((decimal)(row.Cells["AmountOriginal"].Value ?? 0));
                    var c = (a == 0 || b == 0) ? 0 : (b / p * 100);
                    var d = (a == 0 || b == 0) ? 0 : (a * c / 100);
                    row.Cells["FreightAmount"].Value = d;
                    row.Cells["FreightAmountOriginal"].Value = _select.CurrencyID == "VND" ? d : d / _select.ExchangeRate;
                    var f = _select.PPInvoiceDetailCosts.Where(x => x.CostType == true).Sum(x => x.AmountPB);
                    var g = ((decimal)(row.Cells["AmountOriginal"].Value ?? 0));
                    var h = (f == 0 || g == 0) ? 0 : (g / p * 100);
                    var i = (f == 0 || g == 0) ? 0 : (f * h / 100);
                    row.Cells["ImportTaxExpenseAmount"].Value = i;
                    row.Cells["ImportTaxExpenseAmountOriginal"].Value = _select.CurrencyID == "VND" ? i : i / _select.ExchangeRate;
                }
        }

        private void cbbAccountingObjectID_RowSelected(object sender, RowSelectedEventArgs e)
        {

            if (Utils.ListAccountingObject.Any(x => x.AccountingObjectCode == cbbAccountingObjectID.Text))
            {
                var model = (AccountingObject)e.Row.ListObject;
                List<AccountingObjectBankAccount> listAccountingObjectBank = IAccountingObjectBankAccountService.GetByAccountingObjectID(model.ID) ?? new List<AccountingObjectBankAccount>();
                if (_typeID == 261 || _typeID == 210) cbbMAccountingObjectBankAccountDetailIDUnc.DataSource = listAccountingObjectBank;
                else if (_typeID == 262) cbbMAccountingObjectBankAccountDetailIDSnk.DataSource = listAccountingObjectBank;
                else if (_typeID == 263) cbbMAccountingObjectBankAccountDetailIDTtd.DataSource = listAccountingObjectBank;
            }

        }

        private void cbbAccountingObjectID_ValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cbbAccountingObjectID.Text))
            {
                DataBinding(new List<Control> { txtAccountingObjectName }, "AccountingObjectName");
            }
        }

        private void cbbAccountingObjectIDTm_ValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cbbAccountingObjectIDTm.Text))
            {
                DataBinding(new List<Control> { txtAccountingObjectNameTm }, "AccountingObjectName");
            }
        }

        private void cbbAccountingObjectIDUnc_ValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cbbAccountingObjectIDUnc.Text))
            {
                DataBinding(new List<Control> { txtAccountingObjectNameUnc }, "AccountingObjectName");
            }
        }

        private void cbbAccountingObjectIDSck_ValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cbbAccountingObjectIDSck.Text))
            {
                DataBinding(new List<Control> { txtAccountingObjectNameSck }, "AccountingObjectName");
            }
        }

        private void cbbAccountingObjectIDStm_ValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cbbAccountingObjectIDStm.Text))
            {
                DataBinding(new List<Control> { txtAccountingObjectNameStm }, "AccountingObjectName");
            }
        }

        private void cbbAccountingObjectIDTtd_ValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cbbAccountingObjectIDTtd.Text))
            {
                DataBinding(new List<Control> { txtAccountingObjectNameTtd }, "AccountingObjectName");
            }
        }

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {

                BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                if (datasource == null)
                    datasource = new BindingList<RefVoucher>();
                var f = new FViewVoucherOriginal(_select.CurrencyID, datasource);
                f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                try
                {
                    f.ShowDialog(this);
                }
                catch (Exception ex)
                {
                    MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                }

            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void cbbAccountingObjectIDTm_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (Utils.ListAccountingObject.Any(x => x.AccountingObjectCode == cbbAccountingObjectID.Text))
            {
                var model = (AccountingObject)e.Row.ListObject;
                List<AccountingObjectBankAccount> listAccountingObjectBank = IAccountingObjectBankAccountService.GetByAccountingObjectID(model.ID) ?? new List<AccountingObjectBankAccount>();
                if (_typeID == 261) cbbMAccountingObjectBankAccountDetailIDUnc.DataSource = listAccountingObjectBank;
                else if (_typeID == 262) cbbMAccountingObjectBankAccountDetailIDSnk.DataSource = listAccountingObjectBank;
                else if (_typeID == 263) cbbMAccountingObjectBankAccountDetailIDTtd.DataSource = listAccountingObjectBank;
            }
        }

        private void FPPInvoiceDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        private void txtAccountingObjectName_TextChanged(object sender, EventArgs e)
        {
            UltraTextEditor txt = (UltraTextEditor)sender;
            txt.Text = txt.Text.Replace("\r\n", "");
            if (txt.Text == "")
                txt.Multiline = true;
            else
                txt.Multiline = false;
        }
        //Quuân edit
        private void txtMauSoHD_TextChanged(object sender, EventArgs e)
        {
            if (txtMauSoHD.Text != "")
            {
                if (chkBillReceived.CheckState != CheckState.Checked)
                    chkBillReceived.CheckState = CheckState.Checked;
            }
            else
            {
                if (dteNgayHD.Value == null && txtKyHieuHD.Text == "" && txtSoHD.Text == "")
                {
                    chkBillReceived.CheckState = CheckState.Unchecked;
                }
            }
        }
        //Quuân edit
        private void txtKyHieuHD_TextChanged(object sender, EventArgs e)
        {
            if (txtKyHieuHD.Text != "")
            {
                if (chkBillReceived.CheckState != CheckState.Checked)
                    chkBillReceived.CheckState = CheckState.Checked;
            }
            else
            {
                if (dteNgayHD.Value == null && txtMauSoHD.Text == "" && txtSoHD.Text == "")
                {
                    chkBillReceived.CheckState = CheckState.Unchecked;
                }
            }
        }
        //Quuân edit
        private void txtSoHD_TextChanged(object sender, EventArgs e)
        {
            if (txtSoHD.Text != "")
            {
                if (chkBillReceived.CheckState != CheckState.Checked)
                    chkBillReceived.CheckState = CheckState.Checked;
            }
            else
            {
                if (txtKyHieuHD.Text == "" && txtMauSoHD.Text == "" && dteNgayHD.Value == null)
                {
                    chkBillReceived.CheckState = CheckState.Unchecked;
                }
            }
        }
        //Quuân edit
        private void dteNgayHD_ValueChanged(object sender, EventArgs e)
        {
            if (dteNgayHD.Value != null)
            {
                if (chkBillReceived.CheckState != CheckState.Checked)
                    chkBillReceived.CheckState = CheckState.Checked;
            }
            else
            {
                if (txtKyHieuHD.Text == "" && txtMauSoHD.Text == "" && txtSoHD.Text == "")
                {
                    chkBillReceived.CheckState = CheckState.Unchecked;
                }
            }

        }
    }
    public class FppInvoiceDetailStand : DetailBase<PPInvoice> { }
}