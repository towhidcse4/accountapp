﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FPPGetInvoices : CustormForm
    {
        private readonly IPPInvoiceService _ppInvoiceSrv;
        private readonly IAccountingObjectService _accountingObjectSrv;
        private readonly IInvoiceTypeService _invoiceTypeSrv;
        private readonly IGoodsServicePurchaseService _goodsServicePurchaseSrv;
        private readonly IGeneralLedgerService _generalLedgerService;
        private readonly Dictionary<int, string> _lstImportPurchase = new Dictionary<int, string> { { 0, "Trong nước" }, { 1, "Nhập khẩu" } };
        private readonly List<AccountingObject> _lstAccountingObject;
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        readonly UltraCombo cbbAccountingObjectTaxID = new UltraCombo();
        // ngày hoạch toán
        readonly DateTime ngayHoachToan = DateTime.ParseExact(ConstFrm.DbStartDate, "dd/MM/yyyy",
                                                     CultureInfo.InvariantCulture);
        List<PPInvoice> _lstPpInvoice = new List<PPInvoice>();
        List<PPService> _lstPPService = new List<PPService>();
        public FPPGetInvoices()
        {
            WaitingFrm.StartWaiting();
            _ppInvoiceSrv = IoC.Resolve<IPPInvoiceService>();
            _accountingObjectSrv = IoC.Resolve<IAccountingObjectService>();
            _invoiceTypeSrv = IoC.Resolve<IInvoiceTypeService>();
            _goodsServicePurchaseSrv = IoC.Resolve<IGoodsServicePurchaseService>();
            _generalLedgerService = IoC.Resolve<IGeneralLedgerService>();
            _lstAccountingObject = _accountingObjectSrv.GetAccountingObjectsGetInvoice(true); // danh sách khách hàng là nhà cung cấp 

            InitializeComponent();
            // load ngày bắt đầu và ngày kết thúc (note: mặc định ban đầu load theo ngày hoạch toán)
            dtEndDate.DateTime = dtBeginDate.DateTime = ngayHoachToan;
            // load dữ liệu cho Combobox Hình thức mua hàng
            cbbImportPurchase.DataSource = _lstImportPurchase.ToList();
            // load dữ liệu cho Combobox Chọn thời gian
            cbbDateTime.DataSource = _lstItems;
            cbbDateTime.DisplayMember = "Name";
            cbbDateTime.SelectedRow = cbbDateTime.Rows[4];
            Utils.ConfigGrid(cbbDateTime, ConstDatabase.ConfigXML_TableName);
            // load dữ liệu cho Grid Mẫu thuế
            uGridMauThue.DataSource = new List<PPInvoiceDetail> {new PPInvoiceDetail
                                                                    {
                                                                        VATPostedDate = DateTime.Now,
                                                                        InvoiceDate = DateTime.Now,
                                                                        VATAccount = "1331",
                                                                        InvoiceType = 2,
                                                                        GoodsServicePurchaseID = Utils.ListGoodsServicePurchase.FirstOrDefault(x=>x.GoodsServicePurchaseCode == Utils.ListSystemOption.FirstOrDefault(c=>c.Code == "VTHH_NhomHHDV").Data).ID

                                                                    }
                                                                };
            ViewMauThue(true);
            // load dữ liệu cho Grid Danh sách
            ViewDanhSach(DsMuaHangChuaLayHoaDon(new List<PPInvoice>()));// load dữ liệu Combobox Đối tượng. Note: đối tượng là nhà cung cấp
            cbbAccountingObjectID.DataSource = _lstAccountingObject;
            cbbAccountingObjectID.DisplayMember = "AccountingObjectCode";
            cbbAccountingObjectID.SelectedRow = cbbAccountingObjectID.Rows.Count > 0 ? cbbAccountingObjectID.Rows[0] : null;
            Utils.ConfigGrid(cbbAccountingObjectID, ConstDatabase.AccountingObject_TableName);

            ConfigGrid2(DsMuaHangChuaLayHoaDon(new List<PPInvoice>()));
            WaitingFrm.StopWaiting();
        }




        private void btnGetData_Click(object sender, EventArgs e)
        {
            if (ultraComboEditor1.Value.ToString() == "Hóa đơn mua hàng")
            {
                getAndViewData();
            }
            else
            {
                getAndViewData_MDV();
            }
        }

        private void getAndViewData()
        {
            var model = new AccountingObject();
            if (cbbAccountingObjectID.SelectedRow == null)
            {
                MSG.Warning("Đối tượng không được để trống!");
                return;
            }
            model = cbbAccountingObjectID.SelectedRow.ListObject as AccountingObject;
            bool? isImport = null;

            if (cbbImportPurchase.Value != null)
            {
                int checkIsImport = 0;
                int.TryParse(cbbImportPurchase.Value.ToString(), out checkIsImport);

                isImport = checkIsImport == 0 ? false : true;
            }

            _lstPpInvoice = _ppInvoiceSrv.GetByBillReceived(false, model.ID, true, dtBeginDate.DateTime,
                                                                            dtEndDate.DateTime, isImport);
            List<InvoiceReceive> _lstInvoiceReceive = DsMuaHangChuaLayHoaDon(_lstPpInvoice);
            ViewDanhSach(_lstInvoiceReceive);
        }

        private void getAndViewData_MDV()
        {
            var model = new AccountingObject();
            if (cbbAccountingObjectID.SelectedRow == null)
            {
                MSG.Warning("Đối tượng không được để trống!");
                return;
            }
            model = cbbAccountingObjectID.SelectedRow.ListObject as AccountingObject;
            _lstPPService = Utils.IPPServiceService.GetByBillReceived(false, model.ID, true, dtBeginDate.DateTime,
                                                                            dtEndDate.DateTime);
            List<InvoiceReceive> _lstInvoiceReceive = DsMuaHangChuaLayHoaDon(_lstPPService);
            ConfigGrid2(_lstInvoiceReceive);
        }
        #region Xử lý sự kiện trên form

        private void cbbDateTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbDateTime.SelectedRow != null)
            {
                var model = cbbDateTime.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(ngayHoachToan.Year,ngayHoachToan, model, out dtBegin, out dtEnd);
                dtBeginDate.DateTime = dtBegin;
                dtEndDate.DateTime = dtEnd;
            }

        }

        private void cbbAccountingObjectID_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAccountingObjectID.SelectedRow != null)
            {
                var model = cbbAccountingObjectID.SelectedRow.ListObject as AccountingObject;
                uGridMauThue.Rows[0].Cells["AccountingObjectTaxCode"].Value = model.AccountingObjectCode;
                uGridMauThue.Rows[0].Cells["AccountingObjectTaxName"].Value = model.AccountingObjectName;
                uGridMauThue.Rows[0].Cells["CompanyTaxCode"].Value = model.TaxCode;
            }


        }

        private void cbbAccountingObjectTaxID_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAccountingObjectTaxID.SelectedRow != null)
            {
                var model = cbbAccountingObjectTaxID.SelectedRow.ListObject as AccountingObject;
                uGridMauThue.Rows[0].Cells["AccountingObjectTaxName"].Value = model.AccountingObjectName;
                uGridMauThue.Rows[0].Cells["CompanyTaxCode"].Value = model.TaxCode;
            }
        }

        private void cbbImportPurchase_ValueChanged(object sender, EventArgs e)
        {
            uGridMauThue.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"].Hidden = true;
            if (ReferenceEquals(cbbImportPurchase.Value, _lstImportPurchase[1]))
            {
                foreach (var item in uGridMauThue.DisplayLayout.Bands[0].Columns)
                {
                    if (item.Key.Contains("DeductionDebitAccount"))
                        item.Header.VisiblePosition = 2;
                }
                // hiển thị cột TKĐƯ
                uGridMauThue.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"].Hidden = false;
                // cập nhật lại danh sách TK thuế GTGT
                ValueList valueList = new ValueList();
                valueList.ValueListItems.Add(0, "33312");
                uGridMauThue.DisplayLayout.Bands[0].Columns["VATAccount"].ValueList = valueList;
            }
            else
            {
                ValueList valueList = new ValueList();
                valueList.ValueListItems.Add(0, "1331");
                valueList.ValueListItems.Add(1, "1332");
                uGridMauThue.DisplayLayout.Bands[0].Columns["VATAccount"].ValueList = valueList;
            }
        }

        private void cbbAccountingObjectID_TextChanged(object sender, EventArgs e)
        {
            UltraCombo cbbTemp = (UltraCombo)sender;
            Utils.AutoComplateForUltraCombo(cbbAccountingObjectID, cbbTemp.Text);
        }

        private void cbbAccountingObjectTaxID_TextChanged(object sender, EventArgs e)
        {
            UltraCombo cbbTemp = (UltraCombo)sender;
            Utils.AutoComplateForUltraCombo(cbbAccountingObjectTaxID, cbbTemp.Text);
        }

        #endregion

        #region Hàm xử lý riêng của Form
        // Danh sách hàng tiền của mua hàng chưa nhận hóa đơn
        private List<InvoiceReceive> DsMuaHangChuaLayHoaDon(List<PPInvoice> model)
        {
            List<InvoiceReceive> lst = new List<InvoiceReceive>();
            for (int i = 0; i < model.Count; i++)
            {
                foreach (var item in model[i].PPInvoiceDetails)
                {
                    InvoiceReceive objInvoiceReceive = new InvoiceReceive();
                    objInvoiceReceive.TrangThai = false;
                    objInvoiceReceive.NgayHachToan = model[i].PostedDate;
                    objInvoiceReceive.NgayChungTu = model[i].Date;
                    objInvoiceReceive.SoChungTu = model[i].No;
                    objInvoiceReceive.DienGiai = item.Description;
                    objInvoiceReceive.SoTien = (item.AmountOriginal + item.FreightAmountOriginal + item.ImportTaxAmountOriginal + item.SpecialConsumeTaxAmountOriginal - item.DiscountAmountOriginal);
                    objInvoiceReceive.ID = model[i].ID;
                    objInvoiceReceive.Type = model[i].TypeID;
                    lst.Add(objInvoiceReceive);
                }
            }
            return lst.OrderByDescending(x => x.NgayHachToan).ToList();
        }
        private List<InvoiceReceive> DsMuaHangChuaLayHoaDon(List<PPService> model)
        {
            List<InvoiceReceive> lst = new List<InvoiceReceive>();
            for (int i = 0; i < model.Count; i++)
            {
                foreach (var item in model[i].PPServiceDetails)
                {
                    InvoiceReceive objInvoiceReceive = new InvoiceReceive();
                    objInvoiceReceive.TrangThai = false;
                    objInvoiceReceive.NgayHachToan = model[i].PostedDate;
                    objInvoiceReceive.NgayChungTu = model[i].Date;
                    objInvoiceReceive.SoChungTu = model[i].No;
                    objInvoiceReceive.DienGiai = item.Description;
                    objInvoiceReceive.SoTien = (item.AmountOriginal ?? 0 /*+ item.FreightAmountOriginal + item.ImportTaxAmountOriginal + item.SpecialConsumeTaxAmountOriginal*/ - item.DiscountAmountOriginal ?? 0);
                    objInvoiceReceive.ID = model[i].ID;
                    objInvoiceReceive.Type = model[i].TypeID;
                    lst.Add(objInvoiceReceive);
                }
            }
            return lst.OrderByDescending(x => x.NgayHachToan).ToList();
        }
        // Mẫu giao diện Grid Danh sách hàng tiền của mua hàng chưa nhận hóa đơn
        private List<TemplateColumn> MauDanhSach()
        {
            return new List<TemplateColumn>
                       {
                           new TemplateColumn
                               {
                                   ColumnName = "TrangThai",
                                   ColumnCaption = "",
                                   ColumnWidth = 60,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 1
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "NgayHachToan",
                                   ColumnCaption = "Ngày hạch toán",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 2
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "NgayChungTu",
                                   ColumnCaption = "Ngày chứng từ",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 3
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "SoChungTu",
                                   ColumnCaption = "Số chứng từ",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 4
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "DienGiai",
                                   ColumnCaption = "Diễn giải",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 5
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "SoTien",
                                   ColumnCaption = "Số tiền",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 6
                               }
                       };
        }

        // Hiển thị vùng thông tin mẫu thuế
        private void ViewMauThue(bool isImportPurchase)
        {
            Utils.ConfigGrid(uGridMauThue, ConstDatabase.PPInvoiceDetail_TableName_FPPGetInvoices, false);
            uGridMauThue.DisplayLayout.Bands[0].Summaries.Clear();
            uGridMauThue.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            // xét style cho các cột có dạng combobox
            UltraGridBand band = uGridMauThue.DisplayLayout.Bands[0];
            foreach (var item in band.Columns)
            {
                // % thuế GTGT

                if (item.Key.Equals("VATRate"))
                {
                    ValueList valueList = new ValueList();
                    valueList.ValueListItems.Add(-1, " ");
                    valueList.ValueListItems.Add(0, "0%");
                    valueList.ValueListItems.Add(5, "5%");
                    valueList.ValueListItems.Add(10, "10%");               
                    item.ValueList = valueList;
                }
                if (item.Key.Equals("DeductionDebitAccount"))
                {
                    item.Hidden = isImportPurchase;
                    ValueList valueList = new ValueList();
                    valueList.ValueListItems.Add(1331, "1331");
                    valueList.ValueListItems.Add(1338, "1338");
                    valueList.ValueListItems.Add(3388, "3388");
                    item.ValueList = valueList;
                }
                // xử lý dữ liệu combobox của TK thuế GTGT (Note: nếu hình thức mua hàng là nhập khẩu -> TK thuế GTGT: 33312, nếu ko TK thuế GTGT: 1331,1332)
                if (item.Key.Equals("VATAccount"))
                {
                    ValueList valueList = new ValueList();
                    valueList.ValueListItems.Add(0, "1331");
                    valueList.ValueListItems.Add(1, "1332");
                    band.Columns["VATAccount"].ValueList = valueList;
                }
                else if (item.Key.Equals("InvoiceNo")) // Hautv Sửa config số hóa đơn
                {
                    item.MaxLength = 25;
                }
                // loại hóa đơn
                //if (item.Key.Equals("InvoiceType"))
                //{
                //    ValueList valueList = new ValueList();
                //    valueList.ValueListItems.Add(0, "không có hóa đơn");
                //    valueList.ValueListItems.Add(1, "Hóa đơn thông thường");
                //    valueList.ValueListItems.Add(2, "Hóa đơn GTGT");
                //    item.ValueList = valueList;
                //}
                //// Mấu số hóa đơn
                //if (item.Key.Equals("InvoiceTypeID"))
                //    item.Editor = Utils.GetEmbeddableEditorBase(Controls, _invoiceTypeSrv.GetAll(), "ID", "InvoiceTypeCode", ConstDatabase.InvoiceType_TableName);
                // Hàng hóa dịch vụ
                if (item.Key.Equals("GoodsServicePurchaseID"))
                {
                    item.Editor = Utils.GetEmbeddableEditorBase(Controls, _goodsServicePurchaseSrv.GetAll_OrderBy(), "ID", "GoodsServicePurchaseCode", ConstDatabase.GoodsServicePurchase_TableName);
                }
                // đối tượng
                if (item.Key.Equals("AccountingObjectTaxCode"))
                {
                    cbbAccountingObjectTaxID.DataSource = _lstAccountingObject;
                    cbbAccountingObjectTaxID.DisplayMember = "AccountingObjectCode";
                    Utils.ConfigGrid(cbbAccountingObjectTaxID, ConstDatabase.AccountingObject_TableName);
                    cbbAccountingObjectTaxID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(cbbAccountingObjectTaxID_ValueChanged);
                    cbbAccountingObjectTaxID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(cbbAccountingObjectTaxID_TextChanged);
                    band.Columns["AccountingObjectTaxCode"].ValueList = cbbAccountingObjectTaxID;
                }
            }
        }
        private void ConfigGrid2(List<InvoiceReceive> model)
        {
            ultraGridPPSV.DataSource = model;
            //Utils.ConfigGrid(ultraGridPPSV, ConstDatabase.PPService_TableName, false);
            Utils.ConfigGrid(ultraGridPPSV, "", MauDanhSach());
            ultraGridPPSV.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            ultraGridPPSV.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            // xét style cho các cột có dạng combobox
            UltraGridBand band = ultraGridPPSV.DisplayLayout.Bands[0];
            band.Columns["SoTien"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
            band.Columns["NgayHachToan"].Format = "dd/MM/yyyy";
            UltraGridColumn ugc = band.Columns["TrangThai"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;
            ugc.SetHeaderCheckedState(ultraGridPPSV.Rows, false);
            foreach (var col in band.Columns)
            {
                if (col.Key != "TrangThai") col.CellActivation = Activation.NoEdit;
            }
        }
        // Hiển thị vùng thông tin danh sách
        private void ViewDanhSach(List<InvoiceReceive> model)
        {
            uGridDanhSach.DataSource = model;
            Utils.ConfigGrid(uGridDanhSach, "", MauDanhSach());
            uGridDanhSach.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGridDanhSach.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGridMauThue.DisplayLayout.Bands[0].Summaries.Clear();
            uGridMauThue.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            // xét style cho các cột có dạng combobox
            UltraGridBand band = uGridDanhSach.DisplayLayout.Bands[0];
            band.Columns["SoTien"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
            band.Columns["NgayHachToan"].Format = "dd/MM/yyyy";
            UltraGridColumn ugc = band.Columns["TrangThai"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;
            ugc.SetHeaderCheckedState(uGridDanhSach.Rows, false);
            foreach (var col in band.Columns)
            {
                if (col.Key != "TrangThai") col.CellActivation = Activation.NoEdit;
            }
        }
        #endregion

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            UltraGridRow row = uGridMauThue.ActiveRow;
            if ((row.Cells["InvoiceNo"].Text.IsNullOrEmpty()) || (row.Cells["InvoiceDate"].Text.IsNullOrEmpty()) || (row.Cells["InvoiceSeries"].Text.IsNullOrEmpty()) || (row.Cells["InvoiceTemplate"].Text.IsNullOrEmpty()))
            {
                MSG.Warning("Chưa nhập đủ thông tin hóa đơn ,vui lòng kiểm tra lại");
                return;
            }
            else
            {
                if (ultraComboEditor1.Value.ToString() == "Hóa đơn mua hàng")
                {
                    if (uGridDanhSach.ActiveRow == null)
                    {
                        MSG.Error("Bạn cần chọn chứng từ trước");
                        return;
                    }
                    uGridDanhSach.UpdateData();
                    uGridMauThue.UpdateData();
                    List<PPInvoiceDetail> ppInvoiceDetail = uGridMauThue.DataSource as List<PPInvoiceDetail>;
                    List<InvoiceReceive> listInvoiceReceives = uGridDanhSach.DataSource as List<InvoiceReceive>;
                    List<InvoiceReceive> listCheck = new List<InvoiceReceive>();
                    List<PPInvoice> listBillReceived = new List<PPInvoice>();
                    if (listInvoiceReceives != null)
                    {
                        listCheck = listInvoiceReceives.Where(p => p.TrangThai).ToList();

                        if (listCheck.Count == 0)
                        {
                            MSG.Error("Bạn cần chọn chứng từ trước");
                            return;
                        }

                        foreach (var item in listCheck)
                        {
                            PPInvoice temp = _lstPpInvoice.SingleOrDefault(p => p.No == item.SoChungTu);
                            listBillReceived.Add(temp);
                        }
                        ExecuteBillVoucher(listBillReceived, ppInvoiceDetail[0]);
                        //var model = cbbAccountingObjectID.SelectedRow.ListObject as AccountingObject;
                        //_lstPpInvoice = _ppInvoiceSrv.GetByBillReceived(false, model.ID, false, dtBeginDate.DateTime,
                        //                                                                dtEndDate.DateTime);
                        //List<InvoiceReceive> _lstInvoiceReceive = DsMuaHangChuaLayHoaDon(_lstPpInvoice);
                        //ViewDanhSach(_lstInvoiceReceive);

                        getAndViewData();
                        uGridMauThue.DataSource = new List<PPInvoiceDetail> {new PPInvoiceDetail
                                                                    {
                                                                        VATPostedDate = DateTime.Now,
                                                                        InvoiceDate = DateTime.Now,
                                                                        VATAccount = "1331",
                                                                        //VATRate = 
                                                                        
                                                                    }
                                                                };
                        ViewMauThue(true);
                        MSG.Information("Thực hiện nhận hóa đơn thành công!");
                    }
                }
                else
                {
                    if (ultraGridPPSV.ActiveRow == null)
                    {
                        MSG.Error("Bạn cần chọn chứng từ trước");
                        return;
                    }
                    ultraGridPPSV.UpdateData();
                    uGridMauThue.UpdateData();
                    List<PPInvoiceDetail> ppInvoiceDetail = uGridMauThue.DataSource as List<PPInvoiceDetail>;
                    List<InvoiceReceive> listInvoiceReceives = ultraGridPPSV.DataSource as List<InvoiceReceive>;
                    List<InvoiceReceive> listCheck = new List<InvoiceReceive>();
                    List<PPService> listBillReceived = new List<PPService>();
                    if (listInvoiceReceives != null)
                    {
                        listCheck = listInvoiceReceives.Where(p => p.TrangThai).ToList();

                        if (listCheck.Count == 0)
                        {
                            MSG.Error("Bạn cần chọn chứng từ trước");
                            return;
                        }

                        foreach (var item in listCheck)
                        {
                            PPService temp = _lstPPService.SingleOrDefault(p => p.No == item.SoChungTu);
                            listBillReceived.Add(temp);
                        }
                        ExecuteBillVoucher(listBillReceived, ppInvoiceDetail[0]);
                        //var model = cbbAccountingObjectID.SelectedRow.ListObject as AccountingObject;
                        //_lstPpInvoice = _ppInvoiceSrv.GetByBillReceived(false, model.ID, false, dtBeginDate.DateTime,
                        //                                                                dtEndDate.DateTime);
                        //List<InvoiceReceive> _lstInvoiceReceive = DsMuaHangChuaLayHoaDon(_lstPpInvoice);
                        //ViewDanhSach(_lstInvoiceReceive);

                        getAndViewData_MDV();
                        uGridMauThue.DataSource = new List<PPInvoiceDetail> {new PPInvoiceDetail
                                                                    {
                                                                        VATPostedDate = DateTime.Now,
                                                                        InvoiceDate = DateTime.Now,
                                                                        VATAccount = "1331",
                                                                        //VATRate = 
                                                                        
                                                                    }
                                                                };
                        ViewMauThue(true);
                        MSG.Information("Thực hiện nhận hóa đơn thành công!");
                    }
                }

            }
            //ViewVoucherSelected(voucher);

        }

        private void btnView_Click(object sender, EventArgs e)
        {
            InvoiceReceive data = new InvoiceReceive();
            WaitingFrm.StartWaiting();
            if (ultraComboEditor1.Value.ToString() == "Hóa đơn mua hàng")
            {
                if (uGridDanhSach.ActiveRow != null)
                {
                    data = uGridDanhSach.ActiveRow.ListObject as InvoiceReceive;
                    Utils.ViewVoucherSelected(data.ID, data.Type);
                    WaitingFrm.StopWaiting();
                }
                else
                {
                    WaitingFrm.StopWaiting();
                    MSG.Error("Bạn cần chọn chứng từ trước");
                }
            }
            else
            {
                if (ultraGridPPSV.ActiveRow != null)
                {
                    data = ultraGridPPSV.ActiveRow.ListObject as InvoiceReceive;
                    Utils.ViewVoucherSelected(data.ID, data.Type);
                    WaitingFrm.StopWaiting();
                }
                else
                {
                    WaitingFrm.StopWaiting();
                    MSG.Error("Bạn cần chọn chứng từ trước");
                }
            }

        }

        /// <summary>
        /// Hàm xử lý việc nhận hóa đơn
        /// </summary>
        /// <param name="listPpInvoice"></param>
        /// <param name="ppInvoiceDetail"></param>
        public void ExecuteBillVoucher(List<PPInvoice> listPpInvoice, PPInvoiceDetail ppInvoiceDetail)
        {
            List<object> lstUnRecord = new List<object>();
            List<object> lstRecord = new List<object>();
            List<PPInvoice> lstPP = new List<PPInvoice>();
            foreach (var ppInvoice in listPpInvoice)
            {
                try
                {
                    //_ppInvoiceSrv.BeginTran();
                    //B1.Update chứng từ mua hàng
                    ppInvoice.BillReceived = true;
                    foreach (var detail in ppInvoice.PPInvoiceDetails)
                    {
                        detail.VATRate = ppInvoiceDetail.VATRate;
                        if (ppInvoiceDetail.VATRate != null)
                        {
                            detail.VATAmount = (decimal)((detail.Amount * ((detail.VATRate == -1) ? 0 : detail.VATRate)) / 100);
                            detail.VATAmountOriginal = (decimal)(detail.AmountOriginal * ((detail.VATRate == -1) ? 0 : detail.VATRate)) / 100;
                        }

                        detail.VATAccount = ppInvoiceDetail.VATAccount;
                        detail.InvoiceTemplate = ppInvoiceDetail.InvoiceTemplate;
                        detail.InvoiceDate = ppInvoiceDetail.InvoiceDate;
                        detail.InvoiceSeries = ppInvoiceDetail.InvoiceSeries;
                        detail.InvoiceNo = ppInvoiceDetail.InvoiceNo;
                        detail.VATPostedDate = ppInvoiceDetail.VATPostedDate;
                        if (!string.IsNullOrEmpty(ppInvoiceDetail.AccountingObjectTaxCode))
                        {
                            AccountingObject acc =
                                _accountingObjectSrv.GetAccountingObjectByCode(ppInvoiceDetail.AccountingObjectTaxCode);
                            detail.AccountingObjectTaxID = acc.ID;
                            detail.AccountingObjectTaxName = ppInvoiceDetail.AccountingObjectTaxName;
                        }

                        detail.InvoiceTypeID = ppInvoiceDetail.InvoiceTypeID;
                        detail.GoodsServicePurchaseID = ppInvoiceDetail.GoodsServicePurchaseID;
                        ppInvoice.TotalVATAmount += detail.VATAmount;
                        ppInvoice.TotalVATAmountOriginal += detail.VATAmountOriginal;
                    }
                    //_ppInvoiceSrv.Update(ppInvoice);
                    ////B2.Delete chứng từ trong sổ cái
                    //Utils.RemoveLedgerNoTran(ppInvoice);
                    ////B3.Insert lại vào sổ cái
                    //Utils.SaveLedgerNoTran(ppInvoice);
                    //_ppInvoiceSrv.CommitTran();
                    lstPP.Add(ppInvoice);

                }
                catch (Exception)
                {
                    //_ppInvoiceSrv.RolbackTran();
                }

            }
            //Lọc những dòng hàng hóa cùng 1 chứng từ
            for (int i = lstPP.Count - 1; i >= 0; i--)
            {
                if (lstPP.Where(c => c.ID == lstPP[i].ID).ToList().Count > 1) lstPP.RemoveAt(i);
            }
            // Tối ưu tốc độ by Hautv
            #region Bỏ ghi sổ
            foreach (var item in lstPP)
            {
                List<Object> lst = Utils.IGeneralLedgerService.ProcessRemoveLedger_XLCT<PPInvoice>(item);
                lstUnRecord.AddRange(lst);
            }
            #endregion
            #region Ghi sổ lại
            XuLyChungTu(lstPP.Cast<Object>().ToList(), lstUnRecord);
            #endregion

        }
        public void ExecuteBillVoucher(List<PPService> listPPService, PPInvoiceDetail ppInvoiceDetail)
        {
            List<object> lstUnRecord = new List<object>();
            List<object> lstRecord = new List<object>();
            List<PPService> lstPP = new List<PPService>();
            foreach (var item in listPPService)
            {
                try
                {
                    //_ppInvoiceSrv.BeginTran();
                    //B1.Update chứng từ mua hàng dịch vụ
                    item.BillReceived = true;
                    foreach (var detail in item.PPServiceDetails)
                    {
                        detail.VATRate = ppInvoiceDetail.VATRate;
                        if (ppInvoiceDetail.VATRate != null)
                        {
                            detail.VATAmount = (decimal)((detail.Amount * ((detail.VATRate == -1) ? 0 : detail.VATRate)) / 100);
                            detail.VATAmountOriginal = (decimal)(detail.AmountOriginal * ((detail.VATRate == -1) ? 0 : detail.VATRate)) / 100;
                        }

                        detail.VATAccount = ppInvoiceDetail.VATAccount;
                        detail.InvoiceTemplate = ppInvoiceDetail.InvoiceTemplate;
                        detail.InvoiceDate = ppInvoiceDetail.InvoiceDate;
                        detail.InvoiceSeries = ppInvoiceDetail.InvoiceSeries;
                        detail.InvoiceNo = ppInvoiceDetail.InvoiceNo;
                        detail.VATPostedDate = ppInvoiceDetail.VATPostedDate;
                        if (!string.IsNullOrEmpty(ppInvoiceDetail.AccountingObjectTaxCode))
                        {
                            AccountingObject acc =
                                _accountingObjectSrv.GetAccountingObjectByCode(ppInvoiceDetail.AccountingObjectTaxCode);
                            detail.AccountingObjectTaxID = acc.ID;
                            detail.AccountingObjectTaxName = ppInvoiceDetail.AccountingObjectTaxName;
                        }

                        detail.InvoiceTypeID = ppInvoiceDetail.InvoiceTypeID;
                        detail.GoodsServicePurchaseID = ppInvoiceDetail.GoodsServicePurchaseID;
                        item.TotalVATAmount += detail.VATAmount ?? 0;
                        item.TotalVATAmountOriginal += detail.VATAmountOriginal ?? 0;
                    }
                    //_ppInvoiceSrv.Update(ppInvoice);
                    ////B2.Delete chứng từ trong sổ cái
                    //Utils.RemoveLedgerNoTran(ppInvoice);
                    ////B3.Insert lại vào sổ cái
                    //Utils.SaveLedgerNoTran(ppInvoice);
                    //_ppInvoiceSrv.CommitTran();
                    lstPP.Add(item);

                }
                catch (Exception)
                {
                    //_ppInvoiceSrv.RolbackTran();
                }

            }
            // Tối ưu tốc độ by Hautv
            #region Bỏ ghi sổ
            foreach (var item in lstPP)
            {
                List<Object> lst = Utils.IGeneralLedgerService.ProcessRemoveLedger_XLCT<PPService>(item);
                lstUnRecord.AddRange(lst);
            }
            #endregion
            #region Ghi sổ lại
            if (lstPP.Count > 0)
                XuLyChungTu(lstPP.Cast<Object>().ToList(), lstUnRecord);
            #endregion

        }
        private void XuLyChungTu(List<Object> lstObject, List<Object> lstunRecord)
        {
            List<object> listObj = new List<object>();
            Dictionary<bool, List<object>> listObjErr = new Dictionary<bool, List<object>>();
            listObjErr.Add(true, new List<object>());
            listObjErr.Add(false, new List<object>());
            Dictionary<object, string> msg = new Dictionary<object, string>();
            List<RepositoryLedger> listRepositoryLedger = new List<RepositoryLedger>();
            listRepositoryLedger = Utils.IRepositoryLedgerService.GetAll().Where(n => !lstunRecord.Where(r => r.GetType() == typeof(RepositoryLedger)).Any(r => r.GetProperty("ID").ToString() == n.ID.ToString())).ToList();
            List<ViewGLPayExceedCash> listViewGLPayExceedCash = new List<ViewGLPayExceedCash>();
            List<GeneralLedger> lstGeneralLedger = new List<GeneralLedger>();
            lstGeneralLedger = Utils.IGeneralLedgerService.GetAll().Where(r => r.GetType() == typeof(GeneralLedger)).Where(n => !lstunRecord.Any(r => r.GetProperty("ID").ToString() == n.ID.ToString())).ToList();
            listViewGLPayExceedCash = GenVGLP(lstGeneralLedger);
            string query = "";
            if (lstObject.First() is PPInvoice)
            {
                List<PPInvoice> lstPPInvoice = lstObject.Cast<PPInvoice>().ToList();
                foreach (var item in lstPPInvoice)
                {
                    string msg_ = "";
                    if (!Utils.Saveleged_XuLyChungTu<PPInvoice>(item, item.TypeID, listRepositoryLedger, ref msg_))
                    {
                        listObjErr[false].Add(item);
                        msg.Add(item, msg_);
                    }
                    else
                    {

                        List<object> lst_ = Utils.IGeneralLedgerService.GetListObject_Xulychungtu<PPInvoice>(item, ref msg_, listViewGLPayExceedCash);
                        if (msg_.IsNullOrEmpty())
                        {
                            listObjErr[true].Add(item);
                            listObj.AddRange(lst_);
                            query += GendUpdateObj(item);
                            foreach (var itemRpLG in lst_.Where(n => n.GetType() == typeof(RepositoryLedger)).ToList())
                            {
                                listRepositoryLedger.Add((RepositoryLedger)itemRpLG);
                            }
                            foreach (var itemRpLG in lst_.Where(n => n.GetType() == typeof(GeneralLedger)).ToList())
                            {
                                lstGeneralLedger.Add((GeneralLedger)itemRpLG);
                            }
                            listViewGLPayExceedCash = GenVGLP(lstGeneralLedger);
                        }
                        else
                        {
                            listObjErr[false].Add(item);
                            msg.Add(item, msg_);
                        }

                    }
                }
            }
            else if (lstObject.First() is PPService)
            {
                List<PPService> lstPPService = lstObject.Cast<PPService>().ToList();
                foreach (var item in lstPPService)
                {
                    string msg_ = "";
                    if (!Utils.Saveleged_XuLyChungTu<PPService>(item, item.TypeID, listRepositoryLedger, ref msg_))
                    {
                        listObjErr[false].Add(item);
                        msg.Add(item, msg_);
                    }
                    else
                    {

                        List<object> lst_ = Utils.IGeneralLedgerService.GetListObject_Xulychungtu<PPService>(item, ref msg_, listViewGLPayExceedCash);
                        if (msg_.IsNullOrEmpty())
                        {
                            listObjErr[true].Add(item);
                            listObj.AddRange(lst_);
                            query += GendUpdateObj(item);
                            foreach (var itemRpLG in lst_.Where(n => n.GetType() == typeof(RepositoryLedger)).ToList())
                            {
                                listRepositoryLedger.Add((RepositoryLedger)itemRpLG);
                            }
                            foreach (var itemRpLG in lst_.Where(n => n.GetType() == typeof(GeneralLedger)).ToList())
                            {
                                lstGeneralLedger.Add((GeneralLedger)itemRpLG);
                            }
                            listViewGLPayExceedCash = GenVGLP(lstGeneralLedger);
                        }
                        else
                        {
                            listObjErr[false].Add(item);
                            msg.Add(item, msg_);
                        }

                    }
                }
            }


            // Bỏ ghi sổ
            foreach (var item in lstunRecord)
            {
                if (item.GetType() == typeof(GeneralLedger))
                {
                    query += " " + string.Format("DELETE FROM GeneralLedger WHERE ID ='{0}'", item.GetProperty("ID").ToString());
                }
                else if (item.GetType() == typeof(RepositoryLedger))
                {
                    query += " " + string.Format("DELETE FROM RepositoryLedger WHERE ID ='{0}'", item.GetProperty("ID").ToString());
                }
                else if (item.GetType() == typeof(EMContractDetailMG))
                {
                    EMContractDetailMG eMContractDetailMG = item as EMContractDetailMG;
                    query += " " + string.Format("UPDATE EMContractDetailMG SET QuantityReceipt ={0},AmountReceipt={1},AmountReceiptOriginal={2} WHERE ID={3};"
                        , eMContractDetailMG.QuantityReceipt == null ? "NULL" : "'" + eMContractDetailMG.QuantityReceipt.ToString().Replace(',', '.') + "'"
                        , eMContractDetailMG.AmountReceipt == null ? "NULL" : "'" + eMContractDetailMG.AmountReceipt.ToString().Replace(',', '.') + "'"
                        , eMContractDetailMG.AmountReceiptOriginal == null ? "NULL" : "'" + eMContractDetailMG.AmountReceiptOriginal.ToString().Replace(',', '.') + "'"
                         , "'" + eMContractDetailMG.ID.ToString() + "'"
                        );
                }
                else if (item.GetType() == typeof(MBTellerPaper))
                {
                    MBTellerPaper mBTellerPaper = item as MBTellerPaper;
                    query += " " + string.Format("UPDATE MBTellerPaper SET Recorded = 0 where ID = '{0}';", mBTellerPaper.ID.ToString());
                }
            }
            //Ghi sổ
            foreach (var item in listObj)
            {
                if (item.GetType() == typeof(GeneralLedger))
                {
                    GeneralLedger generalLedger = item as GeneralLedger;
                    query += " " + string.Format("INSERT INTO GeneralLedger(ID,BranchID,ReferenceID,TypeID,Date,PostedDate,No,InvoiceDate,InvoiceNo,Account,AccountCorresponding,BankAccountDetailID,CurrencyID,ExchangeRate,DebitAmount,DebitAmountOriginal,CreditAmount,CreditAmountOriginal,Reason,Description,VATDescription,AccountingObjectID,EmployeeID,BudgetItemID,CostSetID,ContractID,StatisticsCodeID,InvoiceSeries,ContactName,DetailID,RefNo,RefDate,DepartmentID,ExpenseItemID,IsIrrationalCost) VALUES ({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34});"
                        , "'" + generalLedger.ID.ToString() + "'"
                        , generalLedger.BranchID == null ? "NULL" : "'" + generalLedger.BranchID.ToString() + "'"
                        , "'" + generalLedger.ReferenceID + "'"
                        , "'" + generalLedger.TypeID + "'"
                        , "'" + generalLedger.Date.ToString() + "'"
                        , "'" + generalLedger.PostedDate.ToString() + "'"
                        , "N'" + generalLedger.No + "'"
                        , generalLedger.InvoiceDate == null ? "NULL" : "'" + generalLedger.InvoiceDate.ToString() + "'"
                        , generalLedger.InvoiceNo.IsNullOrEmpty() ? "NULL" : "N'" + generalLedger.InvoiceNo + "'"
                        , generalLedger.Account.IsNullOrEmpty() ? "NULL" : "N'" + generalLedger.Account + "'"
                        , generalLedger.AccountCorresponding.IsNullOrEmpty() ? "NULL" : "N'" + generalLedger.AccountCorresponding + "'"
                        , generalLedger.BankAccountDetailID == null ? "NULL" : "'" + generalLedger.BankAccountDetailID.ToString() + "'"
                        , generalLedger.CurrencyID.IsNullOrEmpty() ? "NULL" : "N'" + generalLedger.CurrencyID + "'"
                        , generalLedger.ExchangeRate == null ? "NULL" : "'" + generalLedger.ExchangeRate.ToString().Replace(',', '.') + "'"
                         , "'" + generalLedger.DebitAmount.ToString().Replace(',', '.') + "'"
                         , "'" + generalLedger.DebitAmountOriginal.ToString().Replace(',', '.') + "'"
                         , "'" + generalLedger.CreditAmount.ToString().Replace(',', '.') + "'"
                         , "'" + generalLedger.CreditAmountOriginal.ToString().Replace(',', '.') + "'"
                        , generalLedger.Reason.IsNullOrEmpty() ? "NULL" : "N'" + generalLedger.Reason + "'"
                        , generalLedger.Description.IsNullOrEmpty() ? "NULL" : "N'" + generalLedger.Description + "'"
                        , generalLedger.VATDescription.IsNullOrEmpty() ? "NULL" : "N'" + generalLedger.VATDescription + "'"
                        , generalLedger.AccountingObjectID == null ? "NULL" : "'" + generalLedger.AccountingObjectID.ToString() + "'"
                        , generalLedger.EmployeeID == null ? "NULL" : "'" + generalLedger.EmployeeID.ToString() + "'"
                        , generalLedger.BudgetItemID == null ? "NULL" : "'" + generalLedger.BudgetItemID.ToString() + "'"
                        , generalLedger.CostSetID == null ? "NULL" : "'" + generalLedger.CostSetID.ToString() + "'"
                        , generalLedger.ContractID == null ? "NULL" : "'" + generalLedger.ContractID.ToString() + "'"
                        , generalLedger.StatisticsCodeID == null ? "NULL" : "'" + generalLedger.StatisticsCodeID.ToString() + "'"
                         , generalLedger.InvoiceSeries.IsNullOrEmpty() ? "NULL" : "N'" + generalLedger.InvoiceSeries + "'"
                         , generalLedger.ContactName.IsNullOrEmpty() ? "NULL" : "N'" + generalLedger.ContactName + "'"
                         , generalLedger.DetailID == null ? "NULL" : "'" + generalLedger.DetailID.ToString() + "'"
                         , generalLedger.RefNo.IsNullOrEmpty() ? "NULL" : "N'" + generalLedger.RefNo + "'"
                         , generalLedger.RefDate == null ? "NULL" : "'" + generalLedger.RefDate.ToString() + "'"
                         , generalLedger.DepartmentID == null ? "NULL" : "'" + generalLedger.DepartmentID.ToString() + "'"
                         , generalLedger.ExpenseItemID == null ? "NULL" : "'" + generalLedger.ExpenseItemID.ToString() + "'"
                         , generalLedger.IsIrrationalCost == true ? "1" : "0"
                        );
                }
                else if (item.GetType() == typeof(RepositoryLedger))
                {
                    RepositoryLedger repositoryLedger = item as RepositoryLedger;
                    query += " " + string.Format("INSERT INTO RepositoryLedger(ID,BranchID,ReferenceID,TypeID,Date,PostedDate,No,Account,AccountCorresponding,RepositoryID,MaterialGoodsID,Unit,UnitPrice,IWQuantity,OWQuantity,IWAmount,OWAmount,IWQuantityBalance,IWAmountBalance,Reason,Description,OWPurpose,ExpiryDate,LotNo,CostSetID,UnitConvert,UnitPriceConvert,IWQuantityConvert,OWQuantityConvert,StatisticsCodeID,ConvertRate,RefDateTime,DetailID) VALUES ({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32});"
                        , "'" + repositoryLedger.ID.ToString()/*Guid.NewGuid().ToString()*/ + "'"
                        , repositoryLedger.BranchID == null ? "NULL" : "'" + repositoryLedger.BranchID.ToString() + "'"
                        , repositoryLedger.ReferenceID == null ? "NULL" : "'" + repositoryLedger.ReferenceID.ToString() + "'"
                        , "'" + repositoryLedger.TypeID + "'"
                       , repositoryLedger.Date == null ? "NULL" : "'" + repositoryLedger.Date.ToString() + "'"
                       , repositoryLedger.PostedDate == null ? "NULL" : "'" + repositoryLedger.PostedDate.ToString() + "'"
                        , repositoryLedger.No.IsNullOrEmpty() ? "NULL" : "N'" + repositoryLedger.No + "'"
                        , repositoryLedger.Account.IsNullOrEmpty() ? "NULL" : "N'" + repositoryLedger.Account + "'"
                        , repositoryLedger.AccountCorresponding.IsNullOrEmpty() ? "NULL" : "N'" + repositoryLedger.AccountCorresponding + "'"
                        , "'" + repositoryLedger.RepositoryID.ToString() + "'"
                        , "'" + repositoryLedger.MaterialGoodsID.ToString() + "'"
                        , repositoryLedger.Unit.IsNullOrEmpty() ? "NULL" : "N'" + repositoryLedger.Unit + "'"
                        , "'" + repositoryLedger.UnitPrice.ToString().Replace(',', '.') + "'"
                        , repositoryLedger.IWQuantity == null ? "NULL" : "'" + repositoryLedger.IWQuantity.ToString().Replace(',', '.') + "'"
                        , repositoryLedger.OWQuantity == null ? "NULL" : "'" + repositoryLedger.OWQuantity.ToString().Replace(',', '.') + "'"
                        , repositoryLedger.IWAmount == null ? "NULL" : "'" + repositoryLedger.IWAmount.ToString().Replace(',', '.') + "'"
                        , repositoryLedger.OWAmount == null ? "NULL" : "'" + repositoryLedger.OWAmount.ToString().Replace(',', '.') + "'"
                        , repositoryLedger.IWQuantityBalance == null ? "NULL" : "'" + repositoryLedger.IWQuantityBalance.ToString().Replace(',', '.') + "'"
                        , repositoryLedger.IWAmountBalance == null ? "NULL" : "'" + repositoryLedger.IWAmountBalance.ToString().Replace(',', '.') + "'"
                        , repositoryLedger.Reason.IsNullOrEmpty() ? "NULL" : "N'" + repositoryLedger.Reason + "'"
                        , repositoryLedger.Description.IsNullOrEmpty() ? "NULL" : "N'" + repositoryLedger.Description + "'"
                        , repositoryLedger.OWPurpose == null ? "NULL" : "'" + repositoryLedger.OWPurpose.ToString() + "'"
                        , repositoryLedger.ExpiryDate == null ? "NULL" : "'" + repositoryLedger.ExpiryDate.ToString() + "'"
                        , repositoryLedger.LotNo.IsNullOrEmpty() ? "NULL" : "N'" + repositoryLedger.LotNo + "'"
                         , repositoryLedger.CostSetID == null ? "NULL" : "'" + repositoryLedger.CostSetID.ToString() + "'"
                         , repositoryLedger.UnitConvert.IsNullOrEmpty() ? "NULL" : "N'" + repositoryLedger.UnitConvert + "'"
                         , repositoryLedger.UnitPriceConvert == null ? "NULL" : "'" + repositoryLedger.UnitPriceConvert.ToString().Replace(',', '.') + "'"
                         , repositoryLedger.IWQuantityConvert == null ? "NULL" : "'" + repositoryLedger.IWQuantityConvert.ToString().Replace(',', '.') + "'"
                         , repositoryLedger.OWQuantityConvert == null ? "NULL" : "'" + repositoryLedger.OWQuantityConvert.ToString().Replace(',', '.') + "'"
                          , repositoryLedger.StatisticsCodeID == null ? "NULL" : "'" + repositoryLedger.StatisticsCodeID.ToString() + "'"
                           , repositoryLedger.ConvertRate == null ? "NULL" : "'" + repositoryLedger.ConvertRate.ToString().Replace(',', '.') + "'"
                           , repositoryLedger.RefDateTime == null ? "NULL" : "'" + repositoryLedger.RefDateTime.ToString() + "'"
                            , repositoryLedger.DetailID == null ? "NULL" : "'" + repositoryLedger.DetailID.ToString() + "'"
                        );
                }
                else if (item.GetType() == typeof(EMContractDetailMG))
                {
                    EMContractDetailMG eMContractDetailMG = item as EMContractDetailMG;
                    query += " " + string.Format("UPDATE EMContractDetailMG SET QuantityReceipt ={0},AmountReceipt={1},AmountReceiptOriginal={2} WHERE ID={3};"
                        , eMContractDetailMG.QuantityReceipt == null ? "NULL" : "'" + eMContractDetailMG.QuantityReceipt.ToString().Replace(',', '.') + "'"
                        , eMContractDetailMG.AmountReceipt == null ? "NULL" : "'" + eMContractDetailMG.AmountReceipt.ToString().Replace(',', '.') + "'"
                        , eMContractDetailMG.AmountReceiptOriginal == null ? "NULL" : "'" + eMContractDetailMG.AmountReceiptOriginal.ToString().Replace(',', '.') + "'"
                         , "'" + eMContractDetailMG.ID.ToString() + "'"
                        );
                }
                else if (item.GetType() == typeof(MBTellerPaper))
                {
                    MBTellerPaper mBTellerPaper = item as MBTellerPaper;
                    query += " " + string.Format("UPDATE MBTellerPaper SET Recorded = 1 where ID = '{0}';", mBTellerPaper.ID.ToString());
                }
            }
            // Update PPinvoice
            try
            {
                if (!ExecQueries(query))
                {
                    ClearCache();
                    //return;
                    //MSG.Error("Ghi sổ không thành công");
                    return;
                }
                ClearCache();
            }
            catch (Exception ex)
            {
                MSG.Error(ex.Message);
            }

        }
        private string GendUpdateObj(Object object_)
        {
            string query = " ";
            if (object_ is PPInvoice)
            {
                PPInvoice pPInvoice = object_ as PPInvoice;
                query += " " + string.Format("UPDATE PPInvoice SET Recorded ={0},BillReceived = {4} ,TotalVATAmount ={1},TotalVATAmountOriginal ={2} WHERE ID = {3};"
                , pPInvoice.Recorded == true ? "1" : "0"
                , pPInvoice.TotalVATAmount == null ? "NULL" : "'" + pPInvoice.TotalVATAmount.ToString().Replace(',', '.') + "'"
                , pPInvoice.TotalVATAmountOriginal == null ? "NULL" : "'" + pPInvoice.TotalVATAmountOriginal.ToString().Replace(',', '.') + "'"
                , "'" + pPInvoice.ID.ToString() + "'"
                , pPInvoice.BillReceived == true ? "1" : "0"
                );
                foreach (var item in pPInvoice.PPInvoiceDetails)
                {
                    query += " " + string.Format("UPDATE PPInvoiceDetail SET VATRate ={0},VATAmount ={1},VATAmountOriginal ={2},VATAccount ={3},InvoiceTemplate ={4},InvoiceDate ={5},InvoiceSeries ={6},InvoiceNo ={7},VATPostedDate ={8},AccountingObjectTaxID ={9},AccountingObjectTaxName ={10},InvoiceTypeID ={11},GoodsServicePurchaseID ={12} WHERE ID ={13};"
                    , item.VATRate == null ? "NULL" : "'" + item.VATRate.ToString().Replace(',', '.') + "'"
                    , item.VATAmount == null ? "NULL" : "'" + item.VATAmount.ToString().Replace(',', '.') + "'"
                    , item.VATAmountOriginal == null ? "NULL" : "'" + item.VATAmountOriginal.ToString().Replace(',', '.') + "'"
                    , item.VATAccount == null ? "NULL" : "'" + item.VATAccount.ToString().Replace(',', '.') + "'"
                    , item.InvoiceTemplate.IsNullOrEmpty() ? "NULL" : "N'" + item.InvoiceTemplate + "'"
                    , item.InvoiceDate == null ? "NULL" : "'" + item.InvoiceDate.ToString() + "'"
                    , item.InvoiceSeries.IsNullOrEmpty() ? "NULL" : "N'" + item.InvoiceSeries + "'"
                    , item.InvoiceNo.IsNullOrEmpty() ? "NULL" : "N'" + item.InvoiceNo + "'"
                    , item.VATPostedDate == null ? "NULL" : "'" + item.VATPostedDate.ToString() + "'"
                     , item.AccountingObjectTaxID == null ? "NULL" : "'" + item.AccountingObjectTaxID.ToString() + "'"
                     , item.AccountingObjectTaxName.IsNullOrEmpty() ? "NULL" : "N'" + item.AccountingObjectTaxName + "'"
                     , item.InvoiceTypeID == null ? "NULL" : "'" + item.InvoiceTypeID.ToString() + "'"
                     , item.GoodsServicePurchaseID == null ? "NULL" : "'" + item.GoodsServicePurchaseID.ToString() + "'"
                     , "'" + item.ID.ToString() + "'"
                    );
                }
            }
            else if (object_ is PPService)
            {
                PPService pPService = object_ as PPService;
                query += " " + string.Format("UPDATE PPService SET Recorded ={0},BillReceived = {4} ,TotalVATAmount ={1},TotalVATAmountOriginal ={2} WHERE ID = {3};"
                , pPService.Recorded == true ? "1" : "0"
                , pPService.TotalVATAmount == null ? "NULL" : "'" + pPService.TotalVATAmount.ToString().Replace(',', '.') + "'"
                , pPService.TotalVATAmountOriginal == null ? "NULL" : "'" + pPService.TotalVATAmountOriginal.ToString().Replace(',', '.') + "'"
                , "'" + pPService.ID.ToString() + "'"
                , pPService.BillReceived == true ? "1" : "0"
                );
                foreach (var item in pPService.PPServiceDetails)
                {
                    query += " " + string.Format("UPDATE PPServiceDetail SET VATRate ={0},VATAmount ={1},VATAmountOriginal ={2},VATAccount ={3},InvoiceTemplate ={4},InvoiceDate ={5},InvoiceSeries ={6},InvoiceNo ={7},VATPostedDate ={8},AccountingObjectTaxID ={9},AccountingObjectTaxName ={10},InvoiceTypeID ={11},GoodsServicePurchaseID ={12} WHERE ID ={13};"
                    , item.VATRate == null ? "NULL" : "'" + item.VATRate.ToString().Replace(',', '.') + "'"
                    , item.VATAmount == null ? "NULL" : "'" + item.VATAmount.ToString().Replace(',', '.') + "'"
                    , item.VATAmountOriginal == null ? "NULL" : "'" + item.VATAmountOriginal.ToString().Replace(',', '.') + "'"
                    , item.VATAccount == null ? "NULL" : "'" + item.VATAccount.ToString().Replace(',', '.') + "'"
                    , item.InvoiceTemplate.IsNullOrEmpty() ? "NULL" : "N'" + item.InvoiceTemplate + "'"
                    , item.InvoiceDate == null ? "NULL" : "'" + item.InvoiceDate.ToString() + "'"
                    , item.InvoiceSeries.IsNullOrEmpty() ? "NULL" : "N'" + item.InvoiceSeries + "'"
                    , item.InvoiceNo.IsNullOrEmpty() ? "NULL" : "N'" + item.InvoiceNo + "'"
                    , item.VATPostedDate == null ? "NULL" : "'" + item.VATPostedDate.ToString() + "'"
                     , item.AccountingObjectTaxID == null ? "NULL" : "'" + item.AccountingObjectTaxID.ToString() + "'"
                     , item.AccountingObjectTaxName.IsNullOrEmpty() ? "NULL" : "N'" + item.AccountingObjectTaxName + "'"
                     , item.InvoiceTypeID == null ? "NULL" : "'" + item.InvoiceTypeID.ToString() + "'"
                     , item.GoodsServicePurchaseID == null ? "NULL" : "'" + item.GoodsServicePurchaseID.ToString() + "'"
                     , "'" + item.ID.ToString() + "'"
                    );
                }
            }
            return query;
        }
        private void ClearCache()
        {
            Utils.ClearCacheByType<GeneralLedger>();
            Utils.IGeneralLedgerService.RolbackTran();
            Utils.ClearCacheByType<RepositoryLedger>();
            Utils.IRepositoryLedgerService.RolbackTran();
            Utils.ClearCacheByType<PPInvoice>();
            Utils.IPPInvoiceService.RolbackTran();
            Utils.ClearCacheByType<PPService>();
            Utils.IPPServiceService.RolbackTran();
            Utils.ClearCacheByType<EMContractDetailMG>();
            Utils.IEMContractDetailMGService.RolbackTran();
            Utils.ClearCacheByType<MBTellerPaper>();
            Utils.IMBTellerPaperService.RolbackTran();
        }
        private bool ExecQueries(string query)
        {
            string serverName = Properties.Settings.Default.ServerName;
            string database = Properties.Settings.Default.DatabaseName;
            string user = Properties.Settings.Default.UserName;
            string pass = Properties.Settings.Default.Password;

            if (serverName == null || database == null || user == null || pass == null)
            {
                return false;
            }
            SqlConnection sqlConnection = new SqlConnection("");
            if (!string.IsNullOrEmpty(serverName) && !string.IsNullOrEmpty(database) && !string.IsNullOrEmpty(pass) && !serverName.Equals("14.225.3.208") && !serverName.Equals("14.225.3.218"))
            {
                string _ConnectionString = "Data Source=" + serverName + ";Initial Catalog=" + database + ";User ID=" + user + ";Password=" + pass + "";

                sqlConnection = new SqlConnection(_ConnectionString);

                SqlTransaction transaction;
                try
                {
                    sqlConnection.Open();
                }
                catch (Exception)
                {
                    WaitingFrm.StopWaiting();
                    MSG.Warning("Máy chủ SQL không tồn tại. Vui lòng chọn máy chủ SQL khác.");
                    return false;
                }
                transaction = sqlConnection.BeginTransaction();
                try
                {
                    //string q = "";
                    //foreach (var query in sqlqueries)
                    //{
                    //    q += query + " ";
                    //}
                    new SqlCommand(query, sqlConnection, transaction).ExecuteNonQuery();
                    transaction.Commit();
                    transaction.Dispose();
                    //Utils.ISABillService.UnbindSession(Utils.ListSABill);
                    //Utils.ClearCacheByType<SABill>();
                    //Utils.ISABillService.UnbindSession(Utils.ListSABillDetail);
                    //Utils.ClearCacheByType<SABillDetail>();
                    //UnbindSess
                }
                catch (SqlException sqlError)
                {
                    WaitingFrm.StopWaiting();
                    MSG.Warning(sqlError.ToString());
                    transaction.Rollback();
                    return false;
                }
                try
                {
                    sqlConnection.Close();
                    sqlConnection.Dispose();
                }
                catch
                {
                }
            }
            return true;
        }
        private List<ViewGLPayExceedCash> GenVGLP(List<GeneralLedger> lstGL)
        {
            //List<GeneralLedger> lst = lstGL.Where(n=>n.ReferenceID.ToString() == "bfd5b0d6-f4a3-4900-a8f6-1d90b0e7caa0").ToList();
            return (from g in lstGL.Where(N => N.Account != null)
                    where g.Account.StartsWith("111") || g.Account.StartsWith("112")
                    group g by new { g.Account } into c
                    select new ViewGLPayExceedCash
                    {
                        ID = Guid.NewGuid(),
                        Account = c.Key.Account,
                        DebitAmount = c.Sum(n => n.DebitAmount),
                        DebitAmountOriginal = c.Sum(n => n.DebitAmountOriginal),
                        CreditAmount = c.Sum(n => n.CreditAmount),
                        CreditAmountOriginal = c.Sum(n => n.CreditAmountOriginal),
                    }).ToList();
        }
        //Hautv sửa
        private void uGridMauThue_AfterExitEditMode(object sender, EventArgs e)
        {
            //var uGrid = (UltraGrid)sender;
            //UltraGridCell cell = uGrid.ActiveCell;
            //if (cell.Column.Key.Equals("InvoiceNo")) //formmat số hóa đơn
            //{
            //    if (cell.Value != null && cell.Value != "")
            //    {
            //        string t = cell.Value.ToString();
            //        cell.Value = t.PadLeft(7, '0');
            //    }
            //}
        }
        private void uGridMauThue_Error(object sender, ErrorEventArgs e)
        {
            e.Cancel = true;
            UltraGridCell cell = uGridMauThue.ActiveCell;
            if (uGridMauThue.ActiveCell == null) return;
            if (cell.Column.Key.Equals("GoodsServicePurchaseID"))
            {
                int count = _goodsServicePurchaseSrv.GetAll_OrderBy().Count(x => x.GoodsServicePurchaseCode == cell.Text);
                if (count == 0)
                {
                    MSG.Error("Dữ liệu không có trong danh mục");
                    return;
                }
            }
            if (cell.Column.Key.Equals("InvoiceTypeID"))
            {
                int count = _invoiceTypeSrv.GetAll().Count(x => x.InvoiceTypeCode == cell.Text);
                if (count == 0)
                {
                    MSG.Error("Dữ liệu không có trong danh mục");
                    return;
                }
            }

        }

        public class InvoiceReceive
        {
            public Guid ID { get; set; }
            public bool TrangThai { get; set; }
            public DateTime NgayHachToan { get; set; }
            public DateTime NgayChungTu { get; set; }
            public string SoChungTu { get; set; }
            public string DienGiai { get; set; }
            public decimal SoTien { get; set; }
            public int Type { get; set; }
        }

        private void ultraComboEditor1_SelectionChanged(object sender, EventArgs e)
        {
            if (ultraComboEditor1.Value.ToString() == "Hóa đơn mua hàng")
            {
                ultraGridPPSV.Visible = false;
                uGridDanhSach.Visible = true;
                cbbImportPurchase.Enabled = true;
                cbbImportPurchase.SelectedIndex = 0;
            }
            else
            {
                ultraGridPPSV.Visible = true;
                uGridDanhSach.Visible = true;
                cbbImportPurchase.Enabled = false;
                cbbImportPurchase.Text = "";
            }

        }

        private void FPPGetInvoices_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGridDanhSach.ResetText();
            uGridDanhSach.ResetUpdateMode();
            uGridDanhSach.ResetExitEditModeOnLeave();
            uGridDanhSach.ResetRowUpdateCancelAction();
            uGridDanhSach.DataSource = null;
            uGridDanhSach.Layouts.Clear();
            uGridDanhSach.ResetLayouts();
            uGridDanhSach.ResetDisplayLayout();
            uGridDanhSach.Refresh();
            uGridDanhSach.ClearUndoHistory();
            uGridDanhSach.ClearXsdConstraints();

            uGridMauThue.ResetText();
            uGridMauThue.ResetUpdateMode();
            uGridMauThue.ResetExitEditModeOnLeave();
            uGridMauThue.ResetRowUpdateCancelAction();
            uGridMauThue.DataSource = null;
            uGridMauThue.Layouts.Clear();
            uGridMauThue.ResetLayouts();
            uGridMauThue.ResetDisplayLayout();
            uGridMauThue.Refresh();
            uGridMauThue.ClearUndoHistory();
            uGridMauThue.ClearXsdConstraints();
        }

        private void FPPGetInvoices_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
