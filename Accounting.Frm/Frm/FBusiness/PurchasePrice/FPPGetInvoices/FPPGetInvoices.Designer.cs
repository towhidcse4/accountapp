﻿namespace Accounting
{
    partial class FPPGetInvoices
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.palBody2 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridMauThue = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.palBody1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGridPPSV = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridDanhSach = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.palFooter = new Infragistics.Win.Misc.UltraPanel();
            this.btnView = new Infragistics.Win.Misc.UltraButton();
            this.btnOk = new Infragistics.Win.Misc.UltraButton();
            this.btnExit = new Infragistics.Win.Misc.UltraButton();
            this.pafHeader = new Infragistics.Win.Misc.UltraPanel();
            this.grbInfomation = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraComboEditor1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.cbbImportPurchase = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.lblAccountingObject = new Infragistics.Win.Misc.UltraLabel();
            this.lblDateTime = new Infragistics.Win.Misc.UltraLabel();
            this.lblImportPurchase = new Infragistics.Win.Misc.UltraLabel();
            this.lblBeginDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblEndDate = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.btnGetData = new Infragistics.Win.Misc.UltraButton();
            this.cbbDateTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dtBeginDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.palBody2.ClientArea.SuspendLayout();
            this.palBody2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMauThue)).BeginInit();
            this.palBody1.ClientArea.SuspendLayout();
            this.palBody1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridPPSV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDanhSach)).BeginInit();
            this.palFooter.ClientArea.SuspendLayout();
            this.palFooter.SuspendLayout();
            this.pafHeader.ClientArea.SuspendLayout();
            this.pafHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbInfomation)).BeginInit();
            this.grbInfomation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbImportPurchase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.palBody2);
            this.ultraPanel1.ClientArea.Controls.Add(this.palBody1);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 73);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(787, 348);
            this.ultraPanel1.TabIndex = 47;
            // 
            // palBody2
            // 
            // 
            // palBody2.ClientArea
            // 
            this.palBody2.ClientArea.Controls.Add(this.uGridMauThue);
            this.palBody2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palBody2.Location = new System.Drawing.Point(0, 277);
            this.palBody2.Name = "palBody2";
            this.palBody2.Size = new System.Drawing.Size(787, 71);
            this.palBody2.TabIndex = 45;
            // 
            // uGridMauThue
            // 
            this.uGridMauThue.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridMauThue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridMauThue.Location = new System.Drawing.Point(0, 0);
            this.uGridMauThue.Name = "uGridMauThue";
            this.uGridMauThue.Size = new System.Drawing.Size(787, 71);
            this.uGridMauThue.TabIndex = 38;
            this.uGridMauThue.Text = "ultraGrid1";
            this.uGridMauThue.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uGridMauThue.AfterExitEditMode += new System.EventHandler(this.uGridMauThue_AfterExitEditMode);
            this.uGridMauThue.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.uGridMauThue_Error);
            // 
            // palBody1
            // 
            // 
            // palBody1.ClientArea
            // 
            this.palBody1.ClientArea.Controls.Add(this.ultraGridPPSV);
            this.palBody1.ClientArea.Controls.Add(this.uGridDanhSach);
            this.palBody1.Dock = System.Windows.Forms.DockStyle.Top;
            this.palBody1.Location = new System.Drawing.Point(0, 0);
            this.palBody1.Name = "palBody1";
            this.palBody1.Size = new System.Drawing.Size(787, 277);
            this.palBody1.TabIndex = 44;
            // 
            // ultraGridPPSV
            // 
            this.ultraGridPPSV.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ultraGridPPSV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGridPPSV.Location = new System.Drawing.Point(0, 0);
            this.ultraGridPPSV.Name = "ultraGridPPSV";
            this.ultraGridPPSV.Size = new System.Drawing.Size(787, 277);
            this.ultraGridPPSV.TabIndex = 40;
            this.ultraGridPPSV.Text = "ultraGrid1";
            this.ultraGridPPSV.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.ultraGridPPSV.Visible = false;
            // 
            // uGridDanhSach
            // 
            this.uGridDanhSach.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridDanhSach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridDanhSach.Location = new System.Drawing.Point(0, 0);
            this.uGridDanhSach.Name = "uGridDanhSach";
            this.uGridDanhSach.Size = new System.Drawing.Size(787, 277);
            this.uGridDanhSach.TabIndex = 39;
            this.uGridDanhSach.Text = "ultraGrid11";
            this.uGridDanhSach.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // palFooter
            // 
            // 
            // palFooter.ClientArea
            // 
            this.palFooter.ClientArea.Controls.Add(this.btnView);
            this.palFooter.ClientArea.Controls.Add(this.btnOk);
            this.palFooter.ClientArea.Controls.Add(this.btnExit);
            this.palFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palFooter.Location = new System.Drawing.Point(0, 421);
            this.palFooter.Name = "palFooter";
            this.palFooter.Size = new System.Drawing.Size(787, 45);
            this.palFooter.TabIndex = 46;
            // 
            // btnView
            // 
            this.btnView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance12.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.btnView.Appearance = appearance12;
            this.btnView.Location = new System.Drawing.Point(501, 10);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(112, 23);
            this.btnView.TabIndex = 40;
            this.btnView.Text = "Xem chứng từ";
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance13.Image = global::Accounting.Properties.Resources.apply_32;
            this.btnOk.Appearance = appearance13;
            this.btnOk.Location = new System.Drawing.Point(619, 10);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 41;
            this.btnOk.Text = "Đồng ý";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance14.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnExit.Appearance = appearance14;
            this.btnExit.Location = new System.Drawing.Point(700, 10);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 42;
            this.btnExit.Text = "Đóng";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pafHeader
            // 
            // 
            // pafHeader.ClientArea
            // 
            this.pafHeader.ClientArea.Controls.Add(this.grbInfomation);
            this.pafHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pafHeader.Location = new System.Drawing.Point(0, 0);
            this.pafHeader.Name = "pafHeader";
            this.pafHeader.Size = new System.Drawing.Size(787, 73);
            this.pafHeader.TabIndex = 43;
            // 
            // grbInfomation
            // 
            this.grbInfomation.Controls.Add(this.ultraLabel1);
            this.grbInfomation.Controls.Add(this.ultraComboEditor1);
            this.grbInfomation.Controls.Add(this.cbbImportPurchase);
            this.grbInfomation.Controls.Add(this.lblAccountingObject);
            this.grbInfomation.Controls.Add(this.lblDateTime);
            this.grbInfomation.Controls.Add(this.lblImportPurchase);
            this.grbInfomation.Controls.Add(this.lblBeginDate);
            this.grbInfomation.Controls.Add(this.lblEndDate);
            this.grbInfomation.Controls.Add(this.cbbAccountingObjectID);
            this.grbInfomation.Controls.Add(this.btnGetData);
            this.grbInfomation.Controls.Add(this.cbbDateTime);
            this.grbInfomation.Controls.Add(this.dtBeginDate);
            this.grbInfomation.Controls.Add(this.dtEndDate);
            this.grbInfomation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbInfomation.Location = new System.Drawing.Point(0, 0);
            this.grbInfomation.Name = "grbInfomation";
            this.grbInfomation.Size = new System.Drawing.Size(787, 73);
            this.grbInfomation.TabIndex = 38;
            // 
            // ultraLabel1
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextHAlignAsString = "Left";
            appearance15.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance15;
            this.ultraLabel1.Location = new System.Drawing.Point(471, 8);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(49, 21);
            this.ultraLabel1.TabIndex = 40;
            this.ultraLabel1.Text = "Loại";
            // 
            // ultraComboEditor1
            // 
            this.ultraComboEditor1.DisplayMember = "Value";
            valueListItem1.DataValue = "Hóa đơn mua hàng";
            valueListItem1.DisplayText = "";
            valueListItem2.DataValue = "Hóa đơn mua dịch vụ";
            this.ultraComboEditor1.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.ultraComboEditor1.LimitToList = true;
            this.ultraComboEditor1.Location = new System.Drawing.Point(526, 8);
            this.ultraComboEditor1.Name = "ultraComboEditor1";
            this.ultraComboEditor1.Size = new System.Drawing.Size(130, 21);
            this.ultraComboEditor1.TabIndex = 39;
            this.ultraComboEditor1.Text = "Hóa đơn mua hàng";
            this.ultraComboEditor1.ValueMember = "Key";
            this.ultraComboEditor1.SelectionChanged += new System.EventHandler(this.ultraComboEditor1_SelectionChanged);
            // 
            // cbbImportPurchase
            // 
            this.cbbImportPurchase.DisplayMember = "Value";
            this.cbbImportPurchase.LimitToList = true;
            this.cbbImportPurchase.Location = new System.Drawing.Point(352, 7);
            this.cbbImportPurchase.Name = "cbbImportPurchase";
            this.cbbImportPurchase.Size = new System.Drawing.Size(113, 21);
            this.cbbImportPurchase.TabIndex = 38;
            this.cbbImportPurchase.Text = "Trong nước";
            this.cbbImportPurchase.ValueMember = "Key";
            this.cbbImportPurchase.ValueChanged += new System.EventHandler(this.cbbImportPurchase_ValueChanged);
            // 
            // lblAccountingObject
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.TextHAlignAsString = "Left";
            appearance16.TextVAlignAsString = "Middle";
            this.lblAccountingObject.Appearance = appearance16;
            this.lblAccountingObject.Location = new System.Drawing.Point(6, 8);
            this.lblAccountingObject.Name = "lblAccountingObject";
            this.lblAccountingObject.Size = new System.Drawing.Size(55, 21);
            this.lblAccountingObject.TabIndex = 0;
            this.lblAccountingObject.Text = "Đối tượng";
            // 
            // lblDateTime
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextHAlignAsString = "Left";
            appearance17.TextVAlignAsString = "Middle";
            this.lblDateTime.Appearance = appearance17;
            this.lblDateTime.Location = new System.Drawing.Point(6, 37);
            this.lblDateTime.Name = "lblDateTime";
            this.lblDateTime.Size = new System.Drawing.Size(79, 21);
            this.lblDateTime.TabIndex = 1;
            this.lblDateTime.Text = "Chọn thời gian";
            // 
            // lblImportPurchase
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextHAlignAsString = "Left";
            appearance18.TextVAlignAsString = "Middle";
            this.lblImportPurchase.Appearance = appearance18;
            this.lblImportPurchase.Location = new System.Drawing.Point(238, 8);
            this.lblImportPurchase.Name = "lblImportPurchase";
            this.lblImportPurchase.Size = new System.Drawing.Size(108, 21);
            this.lblImportPurchase.TabIndex = 2;
            this.lblImportPurchase.Text = "Hình thức mua hàng";
            // 
            // lblBeginDate
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.TextHAlignAsString = "Left";
            appearance19.TextVAlignAsString = "Middle";
            this.lblBeginDate.Appearance = appearance19;
            this.lblBeginDate.Location = new System.Drawing.Point(319, 37);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.Size = new System.Drawing.Size(27, 21);
            this.lblBeginDate.TabIndex = 3;
            this.lblBeginDate.Text = "Từ";
            // 
            // lblEndDate
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            appearance20.TextHAlignAsString = "Left";
            appearance20.TextVAlignAsString = "Middle";
            this.lblEndDate.Appearance = appearance20;
            this.lblEndDate.Location = new System.Drawing.Point(471, 36);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(49, 21);
            this.lblEndDate.TabIndex = 4;
            this.lblEndDate.Text = "Đến";
            // 
            // cbbAccountingObjectID
            // 
            this.cbbAccountingObjectID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectID.Location = new System.Drawing.Point(94, 7);
            this.cbbAccountingObjectID.Name = "cbbAccountingObjectID";
            this.cbbAccountingObjectID.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID.Size = new System.Drawing.Size(138, 22);
            this.cbbAccountingObjectID.TabIndex = 32;
            this.cbbAccountingObjectID.ValueChanged += new System.EventHandler(this.cbbAccountingObjectID_ValueChanged);
            this.cbbAccountingObjectID.TextChanged += new System.EventHandler(this.cbbAccountingObjectID_TextChanged);
            // 
            // btnGetData
            // 
            this.btnGetData.Location = new System.Drawing.Point(678, 34);
            this.btnGetData.Name = "btnGetData";
            this.btnGetData.Size = new System.Drawing.Size(75, 23);
            this.btnGetData.TabIndex = 37;
            this.btnGetData.Text = "Lấy dữ liệu";
            this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
            // 
            // cbbDateTime
            // 
            this.cbbDateTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDateTime.Location = new System.Drawing.Point(94, 36);
            this.cbbDateTime.Name = "cbbDateTime";
            this.cbbDateTime.NullText = "<chọn dữ liệu>";
            this.cbbDateTime.Size = new System.Drawing.Size(138, 22);
            this.cbbDateTime.TabIndex = 33;
            this.cbbDateTime.ValueChanged += new System.EventHandler(this.cbbDateTime_ValueChanged);
            // 
            // dtBeginDate
            // 
            appearance21.TextHAlignAsString = "Center";
            appearance21.TextVAlignAsString = "Middle";
            this.dtBeginDate.Appearance = appearance21;
            this.dtBeginDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtBeginDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtBeginDate.Location = new System.Drawing.Point(352, 36);
            this.dtBeginDate.MaskInput = "";
            this.dtBeginDate.Name = "dtBeginDate";
            this.dtBeginDate.Size = new System.Drawing.Size(113, 21);
            this.dtBeginDate.TabIndex = 36;
            this.dtBeginDate.Value = null;
            // 
            // dtEndDate
            // 
            appearance22.TextHAlignAsString = "Center";
            appearance22.TextVAlignAsString = "Middle";
            this.dtEndDate.Appearance = appearance22;
            this.dtEndDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtEndDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtEndDate.Location = new System.Drawing.Point(526, 37);
            this.dtEndDate.MaskInput = "";
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.Size = new System.Drawing.Size(130, 21);
            this.dtEndDate.TabIndex = 35;
            this.dtEndDate.Value = null;
            // 
            // FPPGetInvoices
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(787, 466);
            this.Controls.Add(this.ultraPanel1);
            this.Controls.Add(this.palFooter);
            this.Controls.Add(this.pafHeader);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FPPGetInvoices";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nhận hóa đơn";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FPPGetInvoices_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FPPGetInvoices_FormClosed);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.palBody2.ClientArea.ResumeLayout(false);
            this.palBody2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridMauThue)).EndInit();
            this.palBody1.ClientArea.ResumeLayout(false);
            this.palBody1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridPPSV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDanhSach)).EndInit();
            this.palFooter.ClientArea.ResumeLayout(false);
            this.palFooter.ResumeLayout(false);
            this.pafHeader.ClientArea.ResumeLayout(false);
            this.pafHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbInfomation)).EndInit();
            this.grbInfomation.ResumeLayout(false);
            this.grbInfomation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbImportPurchase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel lblAccountingObject;
        private Infragistics.Win.Misc.UltraLabel lblDateTime;
        private Infragistics.Win.Misc.UltraLabel lblImportPurchase;
        private Infragistics.Win.Misc.UltraLabel lblBeginDate;
        private Infragistics.Win.Misc.UltraLabel lblEndDate;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDateTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtEndDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtBeginDate;
        private Infragistics.Win.Misc.UltraButton btnGetData;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMauThue;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDanhSach;
        private Infragistics.Win.Misc.UltraButton btnView;
        private Infragistics.Win.Misc.UltraButton btnOk;
        private Infragistics.Win.Misc.UltraButton btnExit;
        private Infragistics.Win.Misc.UltraPanel pafHeader;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraPanel palBody2;
        private Infragistics.Win.Misc.UltraPanel palBody1;
        private Infragistics.Win.Misc.UltraPanel palFooter;
        private Infragistics.Win.Misc.UltraGroupBox grbInfomation;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbImportPurchase;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGridPPSV;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor1;
    }
}