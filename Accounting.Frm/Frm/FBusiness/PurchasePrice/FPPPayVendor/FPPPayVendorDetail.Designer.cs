﻿

namespace Accounting
{
    partial class FPPPayVendorDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance116 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance123 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance127 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance129 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance130 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance131 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance132 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance133 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance134 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance135 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance136 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance137 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance138 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance139 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance140 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance141 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance142 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance143 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance144 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FPPPayVendorDetail));
            Infragistics.Win.Appearance appearance151 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance145 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance146 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance147 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance148 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance149 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance150 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance152 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridBill = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridAccounting = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmbtnAddRow = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbtnDelRow = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.palCurrency = new System.Windows.Forms.Panel();
            this.uGridCurrency = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pnMBCreditCard = new System.Windows.Forms.Panel();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtCreditCardAccountingObjectBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbCreditCardAccountingObjectBankAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label27 = new System.Windows.Forms.Label();
            this.txtCreditCardAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbCreditCardAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtCreditCardAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.ultraGroupBox6 = new Infragistics.Win.Misc.UltraGroupBox();
            this.picCreditCardType = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.lblCreditCardOwnerCard = new System.Windows.Forms.Label();
            this.lblCreditCardType = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.cbbCreditCardNumber = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtCreditCardReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.pnMBTellerPaper_SecCK = new System.Windows.Forms.Panel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtSecCKAccountingObjectBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbSecCKAccountingObjectBankAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label14 = new System.Windows.Forms.Label();
            this.txtSecCKAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbSecCKAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtSecCKAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtSecCKReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSecCKBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbSecCKBankAccountDetailID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.pnMBTellerPaper_Sec = new System.Windows.Forms.Panel();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.dtSecIssueDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtSecIdentificationNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSecIssueBy = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.label20 = new System.Windows.Forms.Label();
            this.txtSecAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbSecAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtSecReceiver = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.ultraGroupBox7 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtSecReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSecBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbSecBankAccountDetailID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.pnMBTellerPaper_PayOrder = new System.Windows.Forms.Panel();
            this.gpAccountingObject = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtAccreditativeAccountingObjectBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccreditativeAccountingObjectBankAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label13 = new System.Windows.Forms.Label();
            this.txtAccreditativeAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccreditativeAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccreditativeAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.label17 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.gpPaymentOrder = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtPaymentOrderReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtPaymentOrderBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbPaymentOrderBankAccountDetailID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.gpMCPayment = new Infragistics.Win.Misc.UltraGroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtAccreditativeAttach = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtReceiver = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtPaymentAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbPaymentAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtPaymentAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.D = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnApply = new Infragistics.Win.Misc.UltraButton();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.lblPostedDate = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.dteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.postDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label1 = new System.Windows.Forms.Label();
            this.cbbPaymentMethod = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraToolTipManager1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridBill)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAccounting)).BeginInit();
            this.cms4Grid.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.palCurrency.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCurrency)).BeginInit();
            this.panel5.SuspendLayout();
            this.pnMBCreditCard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.ultraGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditCardAccountingObjectBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardAccountingObjectBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditCardAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditCardAccountingObjectAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).BeginInit();
            this.ultraGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditCardReason)).BeginInit();
            this.pnMBTellerPaper_SecCK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKAccountingObjectBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecCKAccountingObjectBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecCKAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKAccountingObjectAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecCKBankAccountDetailID)).BeginInit();
            this.pnMBTellerPaper_Sec.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtSecIssueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecIdentificationNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecIssueBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecReceiver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).BeginInit();
            this.ultraGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecBankAccountDetailID)).BeginInit();
            this.pnMBTellerPaper_PayOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gpAccountingObject)).BeginInit();
            this.gpAccountingObject.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccreditativeAccountingObjectBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccreditativeAccountingObjectBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccreditativeAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccreditativeAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccreditativeAccountingObjectAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpPaymentOrder)).BeginInit();
            this.gpPaymentOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentOrderReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentOrderBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPaymentOrderBankAccountDetailID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpMCPayment)).BeginInit();
            this.gpMCPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccreditativeAttach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPaymentAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentAccountingObjectAddress)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.postDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPaymentMethod)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGridBill);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(753, 256);
            // 
            // uGridBill
            // 
            this.uGridBill.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance1.FontData.BoldAsString = "False";
            this.uGridBill.DisplayLayout.Override.SummaryFooterAppearance = appearance1;
            appearance2.FontData.BoldAsString = "False";
            this.uGridBill.DisplayLayout.Override.SummaryFooterCaptionAppearance = appearance2;
            appearance3.FontData.BoldAsString = "False";
            this.uGridBill.DisplayLayout.Override.SummaryValueAppearance = appearance3;
            this.uGridBill.DisplayLayout.UseFixedHeaders = true;
            this.uGridBill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridBill.Location = new System.Drawing.Point(0, 0);
            this.uGridBill.Name = "uGridBill";
            this.uGridBill.Size = new System.Drawing.Size(753, 256);
            this.uGridBill.TabIndex = 0;
            this.uGridBill.Text = "grdBill";
            this.uGridBill.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uGridBill.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridBill_AfterCellUpdate);
            this.uGridBill.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.uGridBill_InitializeRow);
            this.uGridBill.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridBill_CellChange);
            this.uGridBill.SummaryValueChanged += new Infragistics.Win.UltraWinGrid.SummaryValueChangedEventHandler(this.uGridBill_SummaryValueChanged);
            this.uGridBill.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridBill_DoubleClickRow);
            this.uGridBill.AfterHeaderCheckStateChanged += new Infragistics.Win.UltraWinGrid.AfterHeaderCheckStateChangedEventHandler(this.uGridBill_AfterHeaderCheckStateChanged);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridAccounting);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(753, 256);
            // 
            // uGridAccounting
            // 
            this.uGridAccounting.ContextMenuStrip = this.cms4Grid;
            this.uGridAccounting.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            appearance4.FontData.BoldAsString = "False";
            this.uGridAccounting.DisplayLayout.Override.GroupBySummaryValueAppearance = appearance4;
            this.uGridAccounting.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance5.FontData.BoldAsString = "False";
            this.uGridAccounting.DisplayLayout.Override.SummaryFooterAppearance = appearance5;
            appearance6.FontData.BoldAsString = "False";
            this.uGridAccounting.DisplayLayout.Override.SummaryFooterCaptionAppearance = appearance6;
            appearance7.FontData.BoldAsString = "False";
            this.uGridAccounting.DisplayLayout.Override.SummaryValueAppearance = appearance7;
            this.uGridAccounting.DisplayLayout.UseFixedHeaders = true;
            this.uGridAccounting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridAccounting.Location = new System.Drawing.Point(0, 0);
            this.uGridAccounting.Name = "uGridAccounting";
            this.uGridAccounting.Size = new System.Drawing.Size(753, 256);
            this.uGridAccounting.TabIndex = 0;
            this.uGridAccounting.Text = "ultraGrid1";
            this.uGridAccounting.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAccounting_AfterCellUpdate);
            this.uGridAccounting.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.uGridAccounting_InitializeRow);
            this.uGridAccounting.BeforeRowActivate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.uGridAccounting_BeforeRowActivate);
            this.uGridAccounting.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAccounting_CellChange);
            // 
            // cms4Grid
            // 
            this.cms4Grid.ImageScalingSize = new System.Drawing.Size(15, 15);
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmbtnAddRow,
            this.cmbtnDelRow});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(248, 48);
            // 
            // cmbtnAddRow
            // 
            this.cmbtnAddRow.Image = global::Accounting.Properties.Resources._1437136799_add;
            this.cmbtnAddRow.Name = "cmbtnAddRow";
            this.cmbtnAddRow.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Insert)));
            this.cmbtnAddRow.ShowShortcutKeys = false;
            this.cmbtnAddRow.Size = new System.Drawing.Size(247, 22);
            this.cmbtnAddRow.Text = "Thêm dòng                 Ctrl + Insert";
            this.cmbtnAddRow.Click += new System.EventHandler(this.cmbtnAddRow_Click);
            // 
            // cmbtnDelRow
            // 
            this.cmbtnDelRow.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.cmbtnDelRow.Name = "cmbtnDelRow";
            this.cmbtnDelRow.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this.cmbtnDelRow.ShowShortcutKeys = false;
            this.cmbtnDelRow.Size = new System.Drawing.Size(247, 22);
            this.cmbtnDelRow.Text = "Xóa dòng                     Ctrl + Delete";
            this.cmbtnDelRow.Click += new System.EventHandler(this.cmbtnDelRow_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ultraTabControl1);
            this.panel1.Controls.Add(this.ultraSplitter1);
            this.panel1.Controls.Add(this.palCurrency);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.ultraGroupBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(755, 662);
            this.panel1.TabIndex = 29;
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 291);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(755, 279);
            this.ultraTabControl1.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon;
            this.ultraTabControl1.TabIndex = 4;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "1. Hóa đơn";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "2. Hạch toán";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            this.ultraTabControl1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(753, 256);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 281);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 0;
            this.ultraSplitter1.Size = new System.Drawing.Size(755, 10);
            this.ultraSplitter1.TabIndex = 16;
            // 
            // palCurrency
            // 
            this.palCurrency.AutoSize = true;
            this.palCurrency.Controls.Add(this.uGridCurrency);
            this.palCurrency.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palCurrency.Location = new System.Drawing.Point(0, 570);
            this.palCurrency.Name = "palCurrency";
            this.palCurrency.Size = new System.Drawing.Size(755, 47);
            this.palCurrency.TabIndex = 27;
            // 
            // uGridCurrency
            // 
            this.uGridCurrency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridCurrency.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;
            this.uGridCurrency.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.None;
            this.uGridCurrency.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
            this.uGridCurrency.DisplayLayout.Override.AllowRowSummaries = Infragistics.Win.UltraWinGrid.AllowRowSummaries.False;
            this.uGridCurrency.DisplayLayout.Override.GroupBySummaryDisplayStyle = Infragistics.Win.UltraWinGrid.GroupBySummaryDisplayStyle.Text;
            appearance8.TextHAlignAsString = "Right";
            this.uGridCurrency.DisplayLayout.Override.RowAppearance = appearance8;
            this.uGridCurrency.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridCurrency.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGridCurrency.Location = new System.Drawing.Point(537, 0);
            this.uGridCurrency.Name = "uGridCurrency";
            this.uGridCurrency.Size = new System.Drawing.Size(218, 44);
            this.uGridCurrency.TabIndex = 1;
            this.uGridCurrency.Text = "ultraGrid1";
            this.uGridCurrency.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridCurrency_AfterCellUpdate);
            this.uGridCurrency.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.uGridCurrency_InitializeRow);
            this.uGridCurrency.AfterExitEditMode += new System.EventHandler(this.uGridCurrency_AfterExitEditMode);
            this.uGridCurrency.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridCurrency_CellChange);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.Controls.Add(this.pnMBCreditCard);
            this.panel5.Controls.Add(this.pnMBTellerPaper_SecCK);
            this.panel5.Controls.Add(this.pnMBTellerPaper_Sec);
            this.panel5.Controls.Add(this.pnMBTellerPaper_PayOrder);
            this.panel5.Controls.Add(this.gpMCPayment);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 75);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(755, 206);
            this.panel5.TabIndex = 27;
            // 
            // pnMBCreditCard
            // 
            this.pnMBCreditCard.BackColor = System.Drawing.Color.Transparent;
            this.pnMBCreditCard.Controls.Add(this.ultraGroupBox5);
            this.pnMBCreditCard.Controls.Add(this.ultraGroupBox6);
            this.pnMBCreditCard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMBCreditCard.Location = new System.Drawing.Point(0, 0);
            this.pnMBCreditCard.Name = "pnMBCreditCard";
            this.pnMBCreditCard.Size = new System.Drawing.Size(755, 206);
            this.pnMBCreditCard.TabIndex = 4;
            this.pnMBCreditCard.Visible = false;
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.Controls.Add(this.txtCreditCardAccountingObjectBankName);
            this.ultraGroupBox5.Controls.Add(this.cbbCreditCardAccountingObjectBankAccount);
            this.ultraGroupBox5.Controls.Add(this.label27);
            this.ultraGroupBox5.Controls.Add(this.txtCreditCardAccountingObjectName);
            this.ultraGroupBox5.Controls.Add(this.cbbCreditCardAccountingObjectID);
            this.ultraGroupBox5.Controls.Add(this.txtCreditCardAccountingObjectAddress);
            this.ultraGroupBox5.Controls.Add(this.label28);
            this.ultraGroupBox5.Controls.Add(this.label29);
            this.ultraGroupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox5.Location = new System.Drawing.Point(0, 75);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(755, 131);
            this.ultraGroupBox5.TabIndex = 5;
            this.ultraGroupBox5.Text = "Đối tượng nhận tiền";
            this.ultraGroupBox5.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtCreditCardAccountingObjectBankName
            // 
            this.txtCreditCardAccountingObjectBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreditCardAccountingObjectBankName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtCreditCardAccountingObjectBankName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditCardAccountingObjectBankName.Location = new System.Drawing.Point(271, 77);
            this.txtCreditCardAccountingObjectBankName.Name = "txtCreditCardAccountingObjectBankName";
            this.txtCreditCardAccountingObjectBankName.Size = new System.Drawing.Size(482, 21);
            this.txtCreditCardAccountingObjectBankName.TabIndex = 15;
            // 
            // cbbCreditCardAccountingObjectBankAccount
            // 
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Appearance = appearance9;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance10.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.GroupByBox.Appearance = appearance10;
            appearance11.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance11;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance12.BackColor2 = System.Drawing.SystemColors.Control;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance12;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance14.BackColor = System.Drawing.SystemColors.Highlight;
            appearance14.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.ActiveRowAppearance = appearance14;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.CardAreaAppearance = appearance15;
            appearance16.BorderColor = System.Drawing.Color.Silver;
            appearance16.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.CellAppearance = appearance16;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.CellPadding = 0;
            appearance17.BackColor = System.Drawing.SystemColors.Control;
            appearance17.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance17.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance17.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.GroupByRowAppearance = appearance17;
            appearance18.TextHAlignAsString = "Left";
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.HeaderAppearance = appearance18;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.BorderColor = System.Drawing.Color.Silver;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.RowAppearance = appearance19;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance20.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance20;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbCreditCardAccountingObjectBankAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCreditCardAccountingObjectBankAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCreditCardAccountingObjectBankAccount.Location = new System.Drawing.Point(90, 76);
            this.cbbCreditCardAccountingObjectBankAccount.Name = "cbbCreditCardAccountingObjectBankAccount";
            this.cbbCreditCardAccountingObjectBankAccount.Size = new System.Drawing.Size(175, 22);
            this.cbbCreditCardAccountingObjectBankAccount.TabIndex = 14;
            this.cbbCreditCardAccountingObjectBankAccount.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbCreditCardAccountingObjectBankAccount_RowSelected);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(15, 83);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(55, 13);
            this.label27.TabIndex = 13;
            this.label27.Text = "Tài khoản";
            // 
            // txtCreditCardAccountingObjectName
            // 
            this.txtCreditCardAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreditCardAccountingObjectName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtCreditCardAccountingObjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditCardAccountingObjectName.Location = new System.Drawing.Point(271, 28);
            this.txtCreditCardAccountingObjectName.Name = "txtCreditCardAccountingObjectName";
            this.txtCreditCardAccountingObjectName.Size = new System.Drawing.Size(482, 21);
            this.txtCreditCardAccountingObjectName.TabIndex = 12;
            this.txtCreditCardAccountingObjectName.TextChanged += new System.EventHandler(this.txtCreditCardAccountingObjectName_TextChanged);
            // 
            // cbbCreditCardAccountingObjectID
            // 
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Appearance = appearance21;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance22.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.GroupByBox.Appearance = appearance22;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance23;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance24.BackColor2 = System.Drawing.SystemColors.Control;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.GroupByBox.PromptAppearance = appearance24;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.ActiveCellAppearance = appearance25;
            appearance26.BackColor = System.Drawing.SystemColors.Highlight;
            appearance26.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.ActiveRowAppearance = appearance26;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.CardAreaAppearance = appearance27;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            appearance28.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.CellAppearance = appearance28;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.CellPadding = 0;
            appearance29.BackColor = System.Drawing.SystemColors.Control;
            appearance29.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance29.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.GroupByRowAppearance = appearance29;
            appearance30.TextHAlignAsString = "Left";
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.HeaderAppearance = appearance30;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.RowAppearance = appearance31;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.Override.TemplateAddRowAppearance = appearance32;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbCreditCardAccountingObjectID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbCreditCardAccountingObjectID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCreditCardAccountingObjectID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCreditCardAccountingObjectID.Location = new System.Drawing.Point(90, 27);
            this.cbbCreditCardAccountingObjectID.Name = "cbbCreditCardAccountingObjectID";
            this.cbbCreditCardAccountingObjectID.Size = new System.Drawing.Size(175, 22);
            this.cbbCreditCardAccountingObjectID.TabIndex = 11;
            this.cbbCreditCardAccountingObjectID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbCreditCardAccountingObjectID_RowSelected);
            this.cbbCreditCardAccountingObjectID.TextChanged += new System.EventHandler(this.cbbAccountingObjectID_TextChanged);
            this.cbbCreditCardAccountingObjectID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbAccountingObjectID_KeyPress);
            // 
            // txtCreditCardAccountingObjectAddress
            // 
            this.txtCreditCardAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreditCardAccountingObjectAddress.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtCreditCardAccountingObjectAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditCardAccountingObjectAddress.Location = new System.Drawing.Point(90, 52);
            this.txtCreditCardAccountingObjectAddress.Name = "txtCreditCardAccountingObjectAddress";
            this.txtCreditCardAccountingObjectAddress.Size = new System.Drawing.Size(663, 21);
            this.txtCreditCardAccountingObjectAddress.TabIndex = 10;
            this.txtCreditCardAccountingObjectAddress.TextChanged += new System.EventHandler(this.txtCreditCardAccountingObjectName_TextChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(15, 58);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(40, 13);
            this.label28.TabIndex = 9;
            this.label28.Text = "Địa chỉ";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(15, 34);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(53, 13);
            this.label29.TabIndex = 8;
            this.label29.Text = "Đối tượng";
            // 
            // ultraGroupBox6
            // 
            this.ultraGroupBox6.Controls.Add(this.picCreditCardType);
            this.ultraGroupBox6.Controls.Add(this.lblCreditCardOwnerCard);
            this.ultraGroupBox6.Controls.Add(this.lblCreditCardType);
            this.ultraGroupBox6.Controls.Add(this.label32);
            this.ultraGroupBox6.Controls.Add(this.cbbCreditCardNumber);
            this.ultraGroupBox6.Controls.Add(this.txtCreditCardReason);
            this.ultraGroupBox6.Controls.Add(this.label30);
            this.ultraGroupBox6.Controls.Add(this.label31);
            this.ultraGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox6.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox6.Name = "ultraGroupBox6";
            this.ultraGroupBox6.Size = new System.Drawing.Size(755, 75);
            this.ultraGroupBox6.TabIndex = 4;
            this.ultraGroupBox6.Text = "Thẻ tín dụng";
            this.ultraGroupBox6.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // picCreditCardType
            // 
            appearance33.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            appearance33.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance33.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance33.TextHAlignAsString = "Center";
            appearance33.TextVAlignAsString = "Middle";
            this.picCreditCardType.Appearance = appearance33;
            this.picCreditCardType.BackColor = System.Drawing.Color.Transparent;
            this.picCreditCardType.BorderShadowColor = System.Drawing.Color.Transparent;
            this.picCreditCardType.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.picCreditCardType.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.picCreditCardType.Location = new System.Drawing.Point(274, 17);
            this.picCreditCardType.Name = "picCreditCardType";
            this.picCreditCardType.Size = new System.Drawing.Size(57, 22);
            this.picCreditCardType.TabIndex = 37;
            // 
            // lblCreditCardOwnerCard
            // 
            this.lblCreditCardOwnerCard.AutoSize = true;
            this.lblCreditCardOwnerCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreditCardOwnerCard.Location = new System.Drawing.Point(522, 23);
            this.lblCreditCardOwnerCard.Name = "lblCreditCardOwnerCard";
            this.lblCreditCardOwnerCard.Size = new System.Drawing.Size(0, 13);
            this.lblCreditCardOwnerCard.TabIndex = 36;
            // 
            // lblCreditCardType
            // 
            this.lblCreditCardType.AutoSize = true;
            this.lblCreditCardType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreditCardType.Location = new System.Drawing.Point(332, 23);
            this.lblCreditCardType.Name = "lblCreditCardType";
            this.lblCreditCardType.Size = new System.Drawing.Size(0, 13);
            this.lblCreditCardType.TabIndex = 34;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(456, 23);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(50, 13);
            this.label32.TabIndex = 33;
            this.label32.Text = "Chủ thẻ: ";
            // 
            // cbbCreditCardNumber
            // 
            appearance34.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton1.Appearance = appearance34;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbCreditCardNumber.ButtonsRight.Add(editorButton1);
            this.cbbCreditCardNumber.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCreditCardNumber.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbCreditCardNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCreditCardNumber.Location = new System.Drawing.Point(90, 17);
            this.cbbCreditCardNumber.Name = "cbbCreditCardNumber";
            this.cbbCreditCardNumber.Size = new System.Drawing.Size(175, 22);
            this.cbbCreditCardNumber.TabIndex = 32;
            this.cbbCreditCardNumber.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbCreditCardNumber_RowSelected);
            // 
            // txtCreditCardReason
            // 
            this.txtCreditCardReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreditCardReason.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtCreditCardReason.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditCardReason.Location = new System.Drawing.Point(90, 42);
            this.txtCreditCardReason.Name = "txtCreditCardReason";
            this.txtCreditCardReason.Size = new System.Drawing.Size(663, 21);
            this.txtCreditCardReason.TabIndex = 4;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(15, 44);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(67, 13);
            this.label30.TabIndex = 1;
            this.label30.Text = "Nội dung TT";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(15, 23);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(38, 13);
            this.label31.TabIndex = 0;
            this.label31.Text = "Số thẻ";
            // 
            // pnMBTellerPaper_SecCK
            // 
            this.pnMBTellerPaper_SecCK.BackColor = System.Drawing.Color.Transparent;
            this.pnMBTellerPaper_SecCK.Controls.Add(this.ultraGroupBox1);
            this.pnMBTellerPaper_SecCK.Controls.Add(this.ultraGroupBox3);
            this.pnMBTellerPaper_SecCK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMBTellerPaper_SecCK.Location = new System.Drawing.Point(0, 0);
            this.pnMBTellerPaper_SecCK.Name = "pnMBTellerPaper_SecCK";
            this.pnMBTellerPaper_SecCK.Size = new System.Drawing.Size(755, 206);
            this.pnMBTellerPaper_SecCK.TabIndex = 2;
            this.pnMBTellerPaper_SecCK.Visible = false;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.txtSecCKAccountingObjectBankName);
            this.ultraGroupBox1.Controls.Add(this.cbbSecCKAccountingObjectBankAccount);
            this.ultraGroupBox1.Controls.Add(this.label14);
            this.ultraGroupBox1.Controls.Add(this.txtSecCKAccountingObjectName);
            this.ultraGroupBox1.Controls.Add(this.cbbSecCKAccountingObjectID);
            this.ultraGroupBox1.Controls.Add(this.txtSecCKAccountingObjectAddress);
            this.ultraGroupBox1.Controls.Add(this.label15);
            this.ultraGroupBox1.Controls.Add(this.label16);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 75);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(755, 131);
            this.ultraGroupBox1.TabIndex = 3;
            this.ultraGroupBox1.Text = "Đối tượng nhận tiền";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtSecCKAccountingObjectBankName
            // 
            this.txtSecCKAccountingObjectBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecCKAccountingObjectBankName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtSecCKAccountingObjectBankName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecCKAccountingObjectBankName.Location = new System.Drawing.Point(271, 77);
            this.txtSecCKAccountingObjectBankName.Name = "txtSecCKAccountingObjectBankName";
            this.txtSecCKAccountingObjectBankName.Size = new System.Drawing.Size(471, 21);
            this.txtSecCKAccountingObjectBankName.TabIndex = 15;
            // 
            // cbbSecCKAccountingObjectBankAccount
            // 
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Appearance = appearance35;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance36.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance36.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance36.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.GroupByBox.Appearance = appearance36;
            appearance37.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance37;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance38.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance38.BackColor2 = System.Drawing.SystemColors.Control;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance38.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance38;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance39.BackColor = System.Drawing.SystemColors.Window;
            appearance39.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.ActiveCellAppearance = appearance39;
            appearance40.BackColor = System.Drawing.SystemColors.Highlight;
            appearance40.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.ActiveRowAppearance = appearance40;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.CardAreaAppearance = appearance41;
            appearance42.BorderColor = System.Drawing.Color.Silver;
            appearance42.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.CellAppearance = appearance42;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.CellPadding = 0;
            appearance43.BackColor = System.Drawing.SystemColors.Control;
            appearance43.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance43.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance43.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.GroupByRowAppearance = appearance43;
            appearance44.TextHAlignAsString = "Left";
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.HeaderAppearance = appearance44;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            appearance45.BorderColor = System.Drawing.Color.Silver;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.RowAppearance = appearance45;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance46.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance46;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbSecCKAccountingObjectBankAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbSecCKAccountingObjectBankAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbSecCKAccountingObjectBankAccount.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbSecCKAccountingObjectBankAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbSecCKAccountingObjectBankAccount.Location = new System.Drawing.Point(90, 76);
            this.cbbSecCKAccountingObjectBankAccount.Name = "cbbSecCKAccountingObjectBankAccount";
            this.cbbSecCKAccountingObjectBankAccount.Size = new System.Drawing.Size(175, 22);
            this.cbbSecCKAccountingObjectBankAccount.TabIndex = 14;
            this.cbbSecCKAccountingObjectBankAccount.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbSecCKAccountingObjectBankAccount_RowSelected);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(15, 83);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "Tài khoản";
            // 
            // txtSecCKAccountingObjectName
            // 
            this.txtSecCKAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecCKAccountingObjectName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtSecCKAccountingObjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecCKAccountingObjectName.Location = new System.Drawing.Point(271, 28);
            this.txtSecCKAccountingObjectName.Name = "txtSecCKAccountingObjectName";
            this.txtSecCKAccountingObjectName.Size = new System.Drawing.Size(471, 21);
            this.txtSecCKAccountingObjectName.TabIndex = 12;
            // 
            // cbbSecCKAccountingObjectID
            // 
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Appearance = appearance47;
            this.cbbSecCKAccountingObjectID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbSecCKAccountingObjectID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.cbbSecCKAccountingObjectID.DisplayLayout.GroupByBox.Appearance = appearance33;
            appearance48.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecCKAccountingObjectID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance48;
            this.cbbSecCKAccountingObjectID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance49.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance49.BackColor2 = System.Drawing.SystemColors.Control;
            appearance49.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance49.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecCKAccountingObjectID.DisplayLayout.GroupByBox.PromptAppearance = appearance49;
            this.cbbSecCKAccountingObjectID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbSecCKAccountingObjectID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance50.BackColor = System.Drawing.SystemColors.Window;
            appearance50.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.ActiveCellAppearance = appearance50;
            appearance51.BackColor = System.Drawing.SystemColors.Highlight;
            appearance51.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.ActiveRowAppearance = appearance51;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance52.BackColor = System.Drawing.SystemColors.Window;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.CardAreaAppearance = appearance52;
            appearance53.BorderColor = System.Drawing.Color.Silver;
            appearance53.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.CellAppearance = appearance53;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.CellPadding = 0;
            appearance54.BackColor = System.Drawing.SystemColors.Control;
            appearance54.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance54.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance54.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance54.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.GroupByRowAppearance = appearance54;
            appearance55.TextHAlignAsString = "Left";
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.HeaderAppearance = appearance55;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance56.BackColor = System.Drawing.SystemColors.Window;
            appearance56.BorderColor = System.Drawing.Color.Silver;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.RowAppearance = appearance56;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance57.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbSecCKAccountingObjectID.DisplayLayout.Override.TemplateAddRowAppearance = appearance57;
            this.cbbSecCKAccountingObjectID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbSecCKAccountingObjectID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbSecCKAccountingObjectID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbSecCKAccountingObjectID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbSecCKAccountingObjectID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbSecCKAccountingObjectID.Location = new System.Drawing.Point(90, 27);
            this.cbbSecCKAccountingObjectID.Name = "cbbSecCKAccountingObjectID";
            this.cbbSecCKAccountingObjectID.Size = new System.Drawing.Size(175, 22);
            this.cbbSecCKAccountingObjectID.TabIndex = 11;
            this.cbbSecCKAccountingObjectID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbSecCKAccountingObjectID_RowSelected);
            this.cbbSecCKAccountingObjectID.TextChanged += new System.EventHandler(this.cbbAccountingObjectID_TextChanged);
            this.cbbSecCKAccountingObjectID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbAccountingObjectID_KeyPress);
            // 
            // txtSecCKAccountingObjectAddress
            // 
            this.txtSecCKAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecCKAccountingObjectAddress.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtSecCKAccountingObjectAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecCKAccountingObjectAddress.Location = new System.Drawing.Point(90, 52);
            this.txtSecCKAccountingObjectAddress.Name = "txtSecCKAccountingObjectAddress";
            this.txtSecCKAccountingObjectAddress.Size = new System.Drawing.Size(652, 21);
            this.txtSecCKAccountingObjectAddress.TabIndex = 10;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(15, 58);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 13);
            this.label15.TabIndex = 9;
            this.label15.Text = "Địa chỉ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(15, 34);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "Đối tượng";
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.txtSecCKReason);
            this.ultraGroupBox3.Controls.Add(this.txtSecCKBankName);
            this.ultraGroupBox3.Controls.Add(this.cbbSecCKBankAccountDetailID);
            this.ultraGroupBox3.Controls.Add(this.label18);
            this.ultraGroupBox3.Controls.Add(this.label19);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(755, 75);
            this.ultraGroupBox3.TabIndex = 2;
            this.ultraGroupBox3.Text = "Đơn vị trả tiền";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtSecCKReason
            // 
            this.txtSecCKReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecCKReason.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtSecCKReason.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecCKReason.Location = new System.Drawing.Point(90, 42);
            this.txtSecCKReason.Name = "txtSecCKReason";
            this.txtSecCKReason.Size = new System.Drawing.Size(652, 21);
            this.txtSecCKReason.TabIndex = 4;
            // 
            // txtSecCKBankName
            // 
            this.txtSecCKBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecCKBankName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtSecCKBankName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecCKBankName.Location = new System.Drawing.Point(271, 18);
            this.txtSecCKBankName.Name = "txtSecCKBankName";
            this.txtSecCKBankName.Size = new System.Drawing.Size(471, 21);
            this.txtSecCKBankName.TabIndex = 3;
            // 
            // cbbSecCKBankAccountDetailID
            // 
            appearance58.BackColor = System.Drawing.SystemColors.Window;
            appearance58.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Appearance = appearance58;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance59.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance59.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance59.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance59.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.GroupByBox.Appearance = appearance59;
            appearance60.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance60;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance61.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance61.BackColor2 = System.Drawing.SystemColors.Control;
            appearance61.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance61.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.GroupByBox.PromptAppearance = appearance61;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance62.BackColor = System.Drawing.SystemColors.Window;
            appearance62.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.ActiveCellAppearance = appearance62;
            appearance63.BackColor = System.Drawing.SystemColors.Highlight;
            appearance63.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.ActiveRowAppearance = appearance63;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance64.BackColor = System.Drawing.SystemColors.Window;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.CardAreaAppearance = appearance64;
            appearance65.BorderColor = System.Drawing.Color.Silver;
            appearance65.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.CellAppearance = appearance65;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.CellPadding = 0;
            appearance66.BackColor = System.Drawing.SystemColors.Control;
            appearance66.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance66.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance66.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance66.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.GroupByRowAppearance = appearance66;
            appearance67.TextHAlignAsString = "Left";
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.HeaderAppearance = appearance67;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance68.BackColor = System.Drawing.SystemColors.Window;
            appearance68.BorderColor = System.Drawing.Color.Silver;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.RowAppearance = appearance68;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance69.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.Override.TemplateAddRowAppearance = appearance69;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbSecCKBankAccountDetailID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbSecCKBankAccountDetailID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbSecCKBankAccountDetailID.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbSecCKBankAccountDetailID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbSecCKBankAccountDetailID.Location = new System.Drawing.Point(90, 17);
            this.cbbSecCKBankAccountDetailID.Name = "cbbSecCKBankAccountDetailID";
            this.cbbSecCKBankAccountDetailID.Size = new System.Drawing.Size(175, 22);
            this.cbbSecCKBankAccountDetailID.TabIndex = 2;
            this.cbbSecCKBankAccountDetailID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbSecCKBankAccountDetailID_RowSelected);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(15, 44);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(67, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Nội dung TT";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(15, 23);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Tài khoản";
            // 
            // pnMBTellerPaper_Sec
            // 
            this.pnMBTellerPaper_Sec.BackColor = System.Drawing.Color.Transparent;
            this.pnMBTellerPaper_Sec.Controls.Add(this.ultraGroupBox4);
            this.pnMBTellerPaper_Sec.Controls.Add(this.ultraGroupBox7);
            this.pnMBTellerPaper_Sec.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMBTellerPaper_Sec.Location = new System.Drawing.Point(0, 0);
            this.pnMBTellerPaper_Sec.Name = "pnMBTellerPaper_Sec";
            this.pnMBTellerPaper_Sec.Size = new System.Drawing.Size(755, 206);
            this.pnMBTellerPaper_Sec.TabIndex = 3;
            this.pnMBTellerPaper_Sec.Visible = false;
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.dtSecIssueDate);
            this.ultraGroupBox4.Controls.Add(this.label26);
            this.ultraGroupBox4.Controls.Add(this.label25);
            this.ultraGroupBox4.Controls.Add(this.txtSecIdentificationNo);
            this.ultraGroupBox4.Controls.Add(this.txtSecIssueBy);
            this.ultraGroupBox4.Controls.Add(this.label20);
            this.ultraGroupBox4.Controls.Add(this.txtSecAccountingObjectName);
            this.ultraGroupBox4.Controls.Add(this.cbbSecAccountingObjectID);
            this.ultraGroupBox4.Controls.Add(this.txtSecReceiver);
            this.ultraGroupBox4.Controls.Add(this.label21);
            this.ultraGroupBox4.Controls.Add(this.label22);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox4.Location = new System.Drawing.Point(0, 75);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(755, 131);
            this.ultraGroupBox4.TabIndex = 3;
            this.ultraGroupBox4.Text = "Đối tượng nhận tiền";
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // dtSecIssueDate
            // 
            appearance70.TextHAlignAsString = "Center";
            appearance70.TextVAlignAsString = "Middle";
            this.dtSecIssueDate.Appearance = appearance70;
            this.dtSecIssueDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtSecIssueDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.dtSecIssueDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtSecIssueDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtSecIssueDate.Location = new System.Drawing.Point(332, 77);
            this.dtSecIssueDate.MaskInput = "";
            this.dtSecIssueDate.Name = "dtSecIssueDate";
            this.dtSecIssueDate.Size = new System.Drawing.Size(98, 21);
            this.dtSecIssueDate.TabIndex = 28;
            this.dtSecIssueDate.Value = null;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(438, 83);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(44, 13);
            this.label26.TabIndex = 18;
            this.label26.Text = "Nơi cấp";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(275, 83);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 13);
            this.label25.TabIndex = 17;
            this.label25.Text = "Ngày cấp";
            // 
            // txtSecIdentificationNo
            // 
            this.txtSecIdentificationNo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtSecIdentificationNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecIdentificationNo.Location = new System.Drawing.Point(90, 76);
            this.txtSecIdentificationNo.Name = "txtSecIdentificationNo";
            this.txtSecIdentificationNo.Size = new System.Drawing.Size(175, 21);
            this.txtSecIdentificationNo.TabIndex = 16;
            // 
            // txtSecIssueBy
            // 
            this.txtSecIssueBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecIssueBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecIssueBy.Location = new System.Drawing.Point(484, 77);
            this.txtSecIssueBy.Name = "txtSecIssueBy";
            this.txtSecIssueBy.Size = new System.Drawing.Size(248, 21);
            this.txtSecIssueBy.TabIndex = 15;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(15, 83);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 13);
            this.label20.TabIndex = 13;
            this.label20.Text = "Số CMND";
            // 
            // txtSecAccountingObjectName
            // 
            this.txtSecAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecAccountingObjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecAccountingObjectName.Location = new System.Drawing.Point(271, 28);
            this.txtSecAccountingObjectName.Name = "txtSecAccountingObjectName";
            this.txtSecAccountingObjectName.Size = new System.Drawing.Size(461, 21);
            this.txtSecAccountingObjectName.TabIndex = 12;
            // 
            // cbbSecAccountingObjectID
            // 
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            appearance71.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbSecAccountingObjectID.DisplayLayout.Appearance = appearance71;
            this.cbbSecAccountingObjectID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbSecAccountingObjectID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance72.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance72.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance72.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance72.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSecAccountingObjectID.DisplayLayout.GroupByBox.Appearance = appearance72;
            appearance73.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecAccountingObjectID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance73;
            this.cbbSecAccountingObjectID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance74.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance74.BackColor2 = System.Drawing.SystemColors.Control;
            appearance74.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance74.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecAccountingObjectID.DisplayLayout.GroupByBox.PromptAppearance = appearance74;
            this.cbbSecAccountingObjectID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbSecAccountingObjectID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance75.BackColor = System.Drawing.SystemColors.Window;
            appearance75.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.ActiveCellAppearance = appearance75;
            appearance76.BackColor = System.Drawing.SystemColors.Highlight;
            appearance76.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.ActiveRowAppearance = appearance76;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance77.BackColor = System.Drawing.SystemColors.Window;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.CardAreaAppearance = appearance77;
            appearance78.BorderColor = System.Drawing.Color.Silver;
            appearance78.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.CellAppearance = appearance78;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.CellPadding = 0;
            appearance79.BackColor = System.Drawing.SystemColors.Control;
            appearance79.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance79.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance79.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance79.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.GroupByRowAppearance = appearance79;
            appearance80.TextHAlignAsString = "Left";
            this.cbbSecAccountingObjectID.DisplayLayout.Override.HeaderAppearance = appearance80;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance81.BackColor = System.Drawing.SystemColors.Window;
            appearance81.BorderColor = System.Drawing.Color.Silver;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.RowAppearance = appearance81;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance82.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbSecAccountingObjectID.DisplayLayout.Override.TemplateAddRowAppearance = appearance82;
            this.cbbSecAccountingObjectID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbSecAccountingObjectID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbSecAccountingObjectID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbSecAccountingObjectID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbSecAccountingObjectID.Location = new System.Drawing.Point(90, 27);
            this.cbbSecAccountingObjectID.Name = "cbbSecAccountingObjectID";
            this.cbbSecAccountingObjectID.Size = new System.Drawing.Size(175, 22);
            this.cbbSecAccountingObjectID.TabIndex = 11;
            this.cbbSecAccountingObjectID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbSecAccountingObjectID_RowSelected);
            this.cbbSecAccountingObjectID.TextChanged += new System.EventHandler(this.cbbAccountingObjectID_TextChanged);
            this.cbbSecAccountingObjectID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbAccountingObjectID_KeyPress);
            // 
            // txtSecReceiver
            // 
            this.txtSecReceiver.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecReceiver.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecReceiver.Location = new System.Drawing.Point(90, 52);
            this.txtSecReceiver.Name = "txtSecReceiver";
            this.txtSecReceiver.Size = new System.Drawing.Size(642, 21);
            this.txtSecReceiver.TabIndex = 10;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(15, 58);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(77, 13);
            this.label21.TabIndex = 9;
            this.label21.Text = "Người lĩnh tiền";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(15, 34);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 13);
            this.label22.TabIndex = 8;
            this.label22.Text = "Đối tượng";
            // 
            // ultraGroupBox7
            // 
            this.ultraGroupBox7.Controls.Add(this.txtSecReason);
            this.ultraGroupBox7.Controls.Add(this.txtSecBankName);
            this.ultraGroupBox7.Controls.Add(this.cbbSecBankAccountDetailID);
            this.ultraGroupBox7.Controls.Add(this.label23);
            this.ultraGroupBox7.Controls.Add(this.label24);
            this.ultraGroupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox7.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox7.Name = "ultraGroupBox7";
            this.ultraGroupBox7.Size = new System.Drawing.Size(755, 75);
            this.ultraGroupBox7.TabIndex = 2;
            this.ultraGroupBox7.Text = "Đơn vị trả tiền";
            this.ultraGroupBox7.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtSecReason
            // 
            this.txtSecReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecReason.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecReason.Location = new System.Drawing.Point(90, 42);
            this.txtSecReason.Name = "txtSecReason";
            this.txtSecReason.Size = new System.Drawing.Size(642, 21);
            this.txtSecReason.TabIndex = 4;
            // 
            // txtSecBankName
            // 
            this.txtSecBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecBankName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecBankName.Location = new System.Drawing.Point(271, 18);
            this.txtSecBankName.Name = "txtSecBankName";
            this.txtSecBankName.Size = new System.Drawing.Size(461, 21);
            this.txtSecBankName.TabIndex = 3;
            // 
            // cbbSecBankAccountDetailID
            // 
            appearance83.BackColor = System.Drawing.SystemColors.Window;
            appearance83.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbSecBankAccountDetailID.DisplayLayout.Appearance = appearance83;
            this.cbbSecBankAccountDetailID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbSecBankAccountDetailID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance84.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance84.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance84.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance84.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSecBankAccountDetailID.DisplayLayout.GroupByBox.Appearance = appearance84;
            appearance85.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecBankAccountDetailID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance85;
            this.cbbSecBankAccountDetailID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance86.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance86.BackColor2 = System.Drawing.SystemColors.Control;
            appearance86.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance86.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSecBankAccountDetailID.DisplayLayout.GroupByBox.PromptAppearance = appearance86;
            this.cbbSecBankAccountDetailID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbSecBankAccountDetailID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance87.BackColor = System.Drawing.SystemColors.Window;
            appearance87.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.ActiveCellAppearance = appearance87;
            appearance88.BackColor = System.Drawing.SystemColors.Highlight;
            appearance88.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.ActiveRowAppearance = appearance88;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance89.BackColor = System.Drawing.SystemColors.Window;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.CardAreaAppearance = appearance89;
            appearance90.BorderColor = System.Drawing.Color.Silver;
            appearance90.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.CellAppearance = appearance90;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.CellPadding = 0;
            appearance91.BackColor = System.Drawing.SystemColors.Control;
            appearance91.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance91.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance91.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance91.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.GroupByRowAppearance = appearance91;
            appearance92.TextHAlignAsString = "Left";
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.HeaderAppearance = appearance92;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance93.BackColor = System.Drawing.SystemColors.Window;
            appearance93.BorderColor = System.Drawing.Color.Silver;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.RowAppearance = appearance93;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance94.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbSecBankAccountDetailID.DisplayLayout.Override.TemplateAddRowAppearance = appearance94;
            this.cbbSecBankAccountDetailID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbSecBankAccountDetailID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbSecBankAccountDetailID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbSecBankAccountDetailID.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbSecBankAccountDetailID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbSecBankAccountDetailID.Location = new System.Drawing.Point(90, 17);
            this.cbbSecBankAccountDetailID.Name = "cbbSecBankAccountDetailID";
            this.cbbSecBankAccountDetailID.Size = new System.Drawing.Size(175, 22);
            this.cbbSecBankAccountDetailID.TabIndex = 2;
            this.cbbSecBankAccountDetailID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbSecBankAccountDetailID_RowSelected);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(15, 44);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(67, 13);
            this.label23.TabIndex = 1;
            this.label23.Text = "Nội dung TT";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(15, 23);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Tài khoản";
            // 
            // pnMBTellerPaper_PayOrder
            // 
            this.pnMBTellerPaper_PayOrder.BackColor = System.Drawing.Color.Transparent;
            this.pnMBTellerPaper_PayOrder.Controls.Add(this.gpAccountingObject);
            this.pnMBTellerPaper_PayOrder.Controls.Add(this.gpPaymentOrder);
            this.pnMBTellerPaper_PayOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMBTellerPaper_PayOrder.Location = new System.Drawing.Point(0, 0);
            this.pnMBTellerPaper_PayOrder.Name = "pnMBTellerPaper_PayOrder";
            this.pnMBTellerPaper_PayOrder.Size = new System.Drawing.Size(755, 206);
            this.pnMBTellerPaper_PayOrder.TabIndex = 1;
            this.pnMBTellerPaper_PayOrder.Visible = false;
            // 
            // gpAccountingObject
            // 
            this.gpAccountingObject.Controls.Add(this.txtAccreditativeAccountingObjectBankName);
            this.gpAccountingObject.Controls.Add(this.cbbAccreditativeAccountingObjectBankAccount);
            this.gpAccountingObject.Controls.Add(this.label13);
            this.gpAccountingObject.Controls.Add(this.txtAccreditativeAccountingObjectName);
            this.gpAccountingObject.Controls.Add(this.cbbAccreditativeAccountingObjectID);
            this.gpAccountingObject.Controls.Add(this.txtAccreditativeAccountingObjectAddress);
            this.gpAccountingObject.Controls.Add(this.label17);
            this.gpAccountingObject.Controls.Add(this.label12);
            this.gpAccountingObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gpAccountingObject.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpAccountingObject.Location = new System.Drawing.Point(0, 75);
            this.gpAccountingObject.Name = "gpAccountingObject";
            this.gpAccountingObject.Size = new System.Drawing.Size(755, 131);
            this.gpAccountingObject.TabIndex = 1;
            this.gpAccountingObject.Text = "Đối tượng nhận tiền";
            this.gpAccountingObject.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtAccreditativeAccountingObjectBankName
            // 
            this.txtAccreditativeAccountingObjectBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccreditativeAccountingObjectBankName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccreditativeAccountingObjectBankName.Location = new System.Drawing.Point(271, 77);
            this.txtAccreditativeAccountingObjectBankName.Name = "txtAccreditativeAccountingObjectBankName";
            this.txtAccreditativeAccountingObjectBankName.Size = new System.Drawing.Size(461, 21);
            this.txtAccreditativeAccountingObjectBankName.TabIndex = 15;
            // 
            // cbbAccreditativeAccountingObjectBankAccount
            // 
            appearance95.BackColor = System.Drawing.SystemColors.Window;
            appearance95.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Appearance = appearance95;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance96.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance96.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance96.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance96.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.GroupByBox.Appearance = appearance96;
            appearance97.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance97;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance98.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance98.BackColor2 = System.Drawing.SystemColors.Control;
            appearance98.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance98.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance98;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance99.BackColor = System.Drawing.SystemColors.Window;
            appearance99.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.ActiveCellAppearance = appearance99;
            appearance100.BackColor = System.Drawing.SystemColors.Highlight;
            appearance100.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.ActiveRowAppearance = appearance100;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance101.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.CardAreaAppearance = appearance101;
            appearance102.BorderColor = System.Drawing.Color.Silver;
            appearance102.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.CellAppearance = appearance102;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.CellPadding = 0;
            appearance103.BackColor = System.Drawing.SystemColors.Control;
            appearance103.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance103.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance103.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance103.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.GroupByRowAppearance = appearance103;
            appearance104.TextHAlignAsString = "Left";
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.HeaderAppearance = appearance104;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance105.BackColor = System.Drawing.SystemColors.Window;
            appearance105.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.RowAppearance = appearance105;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance106.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance106;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccreditativeAccountingObjectBankAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccreditativeAccountingObjectBankAccount.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbAccreditativeAccountingObjectBankAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbAccreditativeAccountingObjectBankAccount.Location = new System.Drawing.Point(90, 76);
            this.cbbAccreditativeAccountingObjectBankAccount.Name = "cbbAccreditativeAccountingObjectBankAccount";
            this.cbbAccreditativeAccountingObjectBankAccount.Size = new System.Drawing.Size(175, 22);
            this.cbbAccreditativeAccountingObjectBankAccount.TabIndex = 14;
            this.cbbAccreditativeAccountingObjectBankAccount.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccreditativeAccountingObjectBankAccount_RowSelected);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(15, 83);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "Tài khoản";
            // 
            // txtAccreditativeAccountingObjectName
            // 
            this.txtAccreditativeAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccreditativeAccountingObjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccreditativeAccountingObjectName.Location = new System.Drawing.Point(271, 28);
            this.txtAccreditativeAccountingObjectName.Name = "txtAccreditativeAccountingObjectName";
            this.txtAccreditativeAccountingObjectName.Size = new System.Drawing.Size(461, 21);
            this.txtAccreditativeAccountingObjectName.TabIndex = 12;
            // 
            // cbbAccreditativeAccountingObjectID
            // 
            appearance107.BackColor = System.Drawing.SystemColors.Window;
            appearance107.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Appearance = appearance107;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance108.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance108.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance108.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance108.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.GroupByBox.Appearance = appearance108;
            appearance109.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance109;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance110.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance110.BackColor2 = System.Drawing.SystemColors.Control;
            appearance110.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance110.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.GroupByBox.PromptAppearance = appearance110;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance111.BackColor = System.Drawing.SystemColors.Window;
            appearance111.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.ActiveCellAppearance = appearance111;
            appearance112.BackColor = System.Drawing.SystemColors.Highlight;
            appearance112.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.ActiveRowAppearance = appearance112;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance113.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.CardAreaAppearance = appearance113;
            appearance114.BorderColor = System.Drawing.Color.Silver;
            appearance114.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.CellAppearance = appearance114;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.CellPadding = 0;
            appearance115.BackColor = System.Drawing.SystemColors.Control;
            appearance115.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance115.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance115.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance115.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.GroupByRowAppearance = appearance115;
            appearance116.TextHAlignAsString = "Left";
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.HeaderAppearance = appearance116;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance117.BackColor = System.Drawing.SystemColors.Window;
            appearance117.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.RowAppearance = appearance117;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance118.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.Override.TemplateAddRowAppearance = appearance118;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccreditativeAccountingObjectID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccreditativeAccountingObjectID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbAccreditativeAccountingObjectID.Location = new System.Drawing.Point(90, 27);
            this.cbbAccreditativeAccountingObjectID.Name = "cbbAccreditativeAccountingObjectID";
            this.cbbAccreditativeAccountingObjectID.Size = new System.Drawing.Size(175, 22);
            this.cbbAccreditativeAccountingObjectID.TabIndex = 11;
            this.cbbAccreditativeAccountingObjectID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccreditativeAccountingObjectID_RowSelected);
            this.cbbAccreditativeAccountingObjectID.TextChanged += new System.EventHandler(this.cbbAccountingObjectID_TextChanged);
            this.cbbAccreditativeAccountingObjectID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbAccountingObjectID_KeyPress);
            // 
            // txtAccreditativeAccountingObjectAddress
            // 
            this.txtAccreditativeAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccreditativeAccountingObjectAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccreditativeAccountingObjectAddress.Location = new System.Drawing.Point(90, 52);
            this.txtAccreditativeAccountingObjectAddress.Name = "txtAccreditativeAccountingObjectAddress";
            this.txtAccreditativeAccountingObjectAddress.Size = new System.Drawing.Size(642, 21);
            this.txtAccreditativeAccountingObjectAddress.TabIndex = 10;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(15, 58);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(40, 13);
            this.label17.TabIndex = 9;
            this.label17.Text = "Địa chỉ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(15, 34);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Đối tượng";
            // 
            // gpPaymentOrder
            // 
            this.gpPaymentOrder.Controls.Add(this.txtPaymentOrderReason);
            this.gpPaymentOrder.Controls.Add(this.txtPaymentOrderBankName);
            this.gpPaymentOrder.Controls.Add(this.cbbPaymentOrderBankAccountDetailID);
            this.gpPaymentOrder.Controls.Add(this.label11);
            this.gpPaymentOrder.Controls.Add(this.label10);
            this.gpPaymentOrder.Dock = System.Windows.Forms.DockStyle.Top;
            this.gpPaymentOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpPaymentOrder.Location = new System.Drawing.Point(0, 0);
            this.gpPaymentOrder.Name = "gpPaymentOrder";
            this.gpPaymentOrder.Size = new System.Drawing.Size(755, 75);
            this.gpPaymentOrder.TabIndex = 0;
            this.gpPaymentOrder.Text = "Đơn vị trả tiền";
            this.gpPaymentOrder.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtPaymentOrderReason
            // 
            this.txtPaymentOrderReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPaymentOrderReason.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaymentOrderReason.Location = new System.Drawing.Point(90, 42);
            this.txtPaymentOrderReason.Name = "txtPaymentOrderReason";
            this.txtPaymentOrderReason.Size = new System.Drawing.Size(642, 21);
            this.txtPaymentOrderReason.TabIndex = 4;
            // 
            // txtPaymentOrderBankName
            // 
            this.txtPaymentOrderBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPaymentOrderBankName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaymentOrderBankName.Location = new System.Drawing.Point(271, 18);
            this.txtPaymentOrderBankName.Name = "txtPaymentOrderBankName";
            this.txtPaymentOrderBankName.Size = new System.Drawing.Size(461, 21);
            this.txtPaymentOrderBankName.TabIndex = 3;
            // 
            // cbbPaymentOrderBankAccountDetailID
            // 
            appearance119.BackColor = System.Drawing.SystemColors.Window;
            appearance119.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Appearance = appearance119;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance120.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance120.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance120.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance120.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.GroupByBox.Appearance = appearance120;
            appearance121.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance121;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance122.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance122.BackColor2 = System.Drawing.SystemColors.Control;
            appearance122.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance122.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.GroupByBox.PromptAppearance = appearance122;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance123.BackColor = System.Drawing.SystemColors.Window;
            appearance123.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.ActiveCellAppearance = appearance123;
            appearance124.BackColor = System.Drawing.SystemColors.Highlight;
            appearance124.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.ActiveRowAppearance = appearance124;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance125.BackColor = System.Drawing.SystemColors.Window;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.CardAreaAppearance = appearance125;
            appearance126.BorderColor = System.Drawing.Color.Silver;
            appearance126.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.CellAppearance = appearance126;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.CellPadding = 0;
            appearance127.BackColor = System.Drawing.SystemColors.Control;
            appearance127.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance127.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance127.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance127.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.GroupByRowAppearance = appearance127;
            appearance128.TextHAlignAsString = "Left";
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.HeaderAppearance = appearance128;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance129.BackColor = System.Drawing.SystemColors.Window;
            appearance129.BorderColor = System.Drawing.Color.Silver;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.RowAppearance = appearance129;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance130.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.Override.TemplateAddRowAppearance = appearance130;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbPaymentOrderBankAccountDetailID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbPaymentOrderBankAccountDetailID.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbPaymentOrderBankAccountDetailID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPaymentOrderBankAccountDetailID.Location = new System.Drawing.Point(90, 17);
            this.cbbPaymentOrderBankAccountDetailID.Name = "cbbPaymentOrderBankAccountDetailID";
            this.cbbPaymentOrderBankAccountDetailID.Size = new System.Drawing.Size(175, 22);
            this.cbbPaymentOrderBankAccountDetailID.TabIndex = 2;
            this.cbbPaymentOrderBankAccountDetailID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbPaymentOrderBankAccountDetailID_RowSelected);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(15, 44);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Nội dung TT";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(15, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Tài khoản";
            // 
            // gpMCPayment
            // 
            this.gpMCPayment.Controls.Add(this.label9);
            this.gpMCPayment.Controls.Add(this.txtAccreditativeAttach);
            this.gpMCPayment.Controls.Add(this.txtReason);
            this.gpMCPayment.Controls.Add(this.txtReceiver);
            this.gpMCPayment.Controls.Add(this.txtPaymentAccountingObjectName);
            this.gpMCPayment.Controls.Add(this.cbbPaymentAccountingObjectID);
            this.gpMCPayment.Controls.Add(this.txtPaymentAccountingObjectAddress);
            this.gpMCPayment.Controls.Add(this.label8);
            this.gpMCPayment.Controls.Add(this.label7);
            this.gpMCPayment.Controls.Add(this.label6);
            this.gpMCPayment.Controls.Add(this.label5);
            this.gpMCPayment.Controls.Add(this.D);
            this.gpMCPayment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gpMCPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpMCPayment.Location = new System.Drawing.Point(0, 0);
            this.gpMCPayment.Name = "gpMCPayment";
            this.gpMCPayment.Size = new System.Drawing.Size(755, 206);
            this.gpMCPayment.TabIndex = 0;
            this.gpMCPayment.Text = "THÔNG TIN CHUNG";
            this.gpMCPayment.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(658, 125);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Chứng từ gốc";
            // 
            // txtAccreditativeAttach
            // 
            this.txtAccreditativeAttach.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccreditativeAttach.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccreditativeAttach.Location = new System.Drawing.Point(87, 121);
            this.txtAccreditativeAttach.Name = "txtAccreditativeAttach";
            this.txtAccreditativeAttach.Size = new System.Drawing.Size(565, 21);
            this.txtAccreditativeAttach.TabIndex = 10;
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReason.Location = new System.Drawing.Point(87, 97);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(642, 21);
            this.txtReason.TabIndex = 9;
            // 
            // txtReceiver
            // 
            this.txtReceiver.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReceiver.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReceiver.Location = new System.Drawing.Point(87, 73);
            this.txtReceiver.Name = "txtReceiver";
            this.txtReceiver.Size = new System.Drawing.Size(642, 21);
            this.txtReceiver.TabIndex = 8;
            // 
            // txtPaymentAccountingObjectName
            // 
            this.txtPaymentAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPaymentAccountingObjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaymentAccountingObjectName.Location = new System.Drawing.Point(268, 25);
            this.txtPaymentAccountingObjectName.Name = "txtPaymentAccountingObjectName";
            this.txtPaymentAccountingObjectName.Size = new System.Drawing.Size(461, 21);
            this.txtPaymentAccountingObjectName.TabIndex = 7;
            // 
            // cbbPaymentAccountingObjectID
            // 
            this.cbbPaymentAccountingObjectID.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.None;
            appearance131.BackColor = System.Drawing.SystemColors.Window;
            appearance131.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Appearance = appearance131;
            this.cbbPaymentAccountingObjectID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbPaymentAccountingObjectID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance132.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance132.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance132.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance132.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbPaymentAccountingObjectID.DisplayLayout.GroupByBox.Appearance = appearance132;
            appearance133.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbPaymentAccountingObjectID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance133;
            this.cbbPaymentAccountingObjectID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance134.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance134.BackColor2 = System.Drawing.SystemColors.Control;
            appearance134.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance134.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbPaymentAccountingObjectID.DisplayLayout.GroupByBox.PromptAppearance = appearance134;
            this.cbbPaymentAccountingObjectID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbPaymentAccountingObjectID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance135.BackColor = System.Drawing.SystemColors.Window;
            appearance135.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.ActiveCellAppearance = appearance135;
            appearance136.BackColor = System.Drawing.SystemColors.Highlight;
            appearance136.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.ActiveRowAppearance = appearance136;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance137.BackColor = System.Drawing.SystemColors.Window;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.CardAreaAppearance = appearance137;
            appearance138.BorderColor = System.Drawing.Color.Silver;
            appearance138.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.CellAppearance = appearance138;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.CellPadding = 0;
            appearance139.BackColor = System.Drawing.SystemColors.Control;
            appearance139.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance139.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance139.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance139.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.GroupByRowAppearance = appearance139;
            appearance140.TextHAlignAsString = "Left";
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.HeaderAppearance = appearance140;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance141.BackColor = System.Drawing.SystemColors.Window;
            appearance141.BorderColor = System.Drawing.Color.Silver;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.RowAppearance = appearance141;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance142.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbPaymentAccountingObjectID.DisplayLayout.Override.TemplateAddRowAppearance = appearance142;
            this.cbbPaymentAccountingObjectID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbPaymentAccountingObjectID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbPaymentAccountingObjectID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbPaymentAccountingObjectID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPaymentAccountingObjectID.Location = new System.Drawing.Point(87, 24);
            this.cbbPaymentAccountingObjectID.Name = "cbbPaymentAccountingObjectID";
            this.cbbPaymentAccountingObjectID.Size = new System.Drawing.Size(175, 22);
            this.cbbPaymentAccountingObjectID.TabIndex = 6;
            this.cbbPaymentAccountingObjectID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbPaymentAccountingObjectID_RowSelected);
            this.cbbPaymentAccountingObjectID.TextChanged += new System.EventHandler(this.cbbAccountingObjectID_TextChanged);
            this.cbbPaymentAccountingObjectID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbAccountingObjectID_KeyPress);
            // 
            // txtPaymentAccountingObjectAddress
            // 
            this.txtPaymentAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPaymentAccountingObjectAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaymentAccountingObjectAddress.Location = new System.Drawing.Point(87, 49);
            this.txtPaymentAccountingObjectAddress.Name = "txtPaymentAccountingObjectAddress";
            this.txtPaymentAccountingObjectAddress.Size = new System.Drawing.Size(642, 21);
            this.txtPaymentAccountingObjectAddress.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 126);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Kèm theo";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 102);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Lý do chi";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Người nhận";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Địa chỉ";
            // 
            // D
            // 
            this.D.AutoSize = true;
            this.D.BackColor = System.Drawing.Color.Transparent;
            this.D.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.D.Location = new System.Drawing.Point(12, 31);
            this.D.Name = "D";
            this.D.Size = new System.Drawing.Size(53, 13);
            this.D.TabIndex = 0;
            this.D.Text = "Đối tượng";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.btnApply);
            this.panel3.Controls.Add(this.btnCancel);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 617);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(755, 45);
            this.panel3.TabIndex = 29;
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance143.Image = global::Accounting.Properties.Resources.apply_32;
            this.btnApply.Appearance = appearance143;
            this.btnApply.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnApply.Location = new System.Drawing.Point(563, 8);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(89, 31);
            this.btnApply.TabIndex = 2;
            this.btnApply.Text = "Thực hiện";
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance144.Image = ((object)(resources.GetObject("appearance144.Image")));
            this.btnCancel.Appearance = appearance144;
            this.btnCancel.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnCancel.Location = new System.Drawing.Point(658, 8);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(89, 31);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.lblPostedDate);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox2.Controls.Add(this.txtNo);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox2.Controls.Add(this.dteDate);
            this.ultraGroupBox2.Controls.Add(this.postDate);
            this.ultraGroupBox2.Controls.Add(this.label1);
            this.ultraGroupBox2.Controls.Add(this.cbbPaymentMethod);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            appearance151.FontData.BoldAsString = "True";
            appearance151.FontData.SizeInPoints = 13F;
            this.ultraGroupBox2.HeaderAppearance = appearance151;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(755, 75);
            this.ultraGroupBox2.TabIndex = 31;
            this.ultraGroupBox2.Text = "TRẢ TIỀN NHÀ CUNG CẤP";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // lblPostedDate
            // 
            appearance145.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(104)))), ((int)(((byte)(189)))));
            appearance145.BackColor2 = System.Drawing.Color.Transparent;
            appearance145.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance145.ForeColor = System.Drawing.Color.White;
            appearance145.TextHAlignAsString = "Center";
            appearance145.TextVAlignAsString = "Middle";
            this.lblPostedDate.Appearance = appearance145;
            this.lblPostedDate.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblPostedDate.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.lblPostedDate.Location = new System.Drawing.Point(106, 25);
            this.lblPostedDate.Name = "lblPostedDate";
            this.lblPostedDate.Size = new System.Drawing.Size(100, 24);
            this.lblPostedDate.TabIndex = 35;
            this.lblPostedDate.Text = "Ngày hạch toán";
            // 
            // ultraLabel2
            // 
            appearance146.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(104)))), ((int)(((byte)(189)))));
            appearance146.BackColor2 = System.Drawing.Color.Transparent;
            appearance146.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance146.ForeColor = System.Drawing.Color.White;
            appearance146.TextHAlignAsString = "Center";
            appearance146.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance146;
            this.ultraLabel2.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel2.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.ultraLabel2.Location = new System.Drawing.Point(7, 25);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(100, 24);
            this.ultraLabel2.TabIndex = 30;
            this.ultraLabel2.Text = "Số chứng từ";
            this.ultraLabel2.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // txtNo
            // 
            appearance147.TextHAlignAsString = "Center";
            this.txtNo.Appearance = appearance147;
            this.txtNo.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.txtNo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtNo.Location = new System.Drawing.Point(7, 49);
            this.txtNo.MaxLength = 25;
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(100, 19);
            this.txtNo.TabIndex = 31;
            // 
            // ultraLabel3
            // 
            appearance148.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(104)))), ((int)(((byte)(189)))));
            appearance148.BackColor2 = System.Drawing.Color.Transparent;
            appearance148.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance148.ForeColor = System.Drawing.Color.White;
            appearance148.TextHAlignAsString = "Center";
            appearance148.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance148;
            this.ultraLabel3.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel3.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.ultraLabel3.Location = new System.Drawing.Point(205, 25);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(100, 24);
            this.ultraLabel3.TabIndex = 34;
            this.ultraLabel3.Text = "Ngày chứng từ";
            this.ultraLabel3.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // dteDate
            // 
            appearance149.TextHAlignAsString = "Center";
            appearance149.TextVAlignAsString = "Middle";
            this.dteDate.Appearance = appearance149;
            this.dteDate.AutoSize = false;
            this.dteDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.dteDate.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.dteDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.dteDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteDate.Location = new System.Drawing.Point(205, 49);
            this.dteDate.MaskInput = "dd/mm/yyyy";
            this.dteDate.Name = "dteDate";
            this.dteDate.Size = new System.Drawing.Size(100, 19);
            this.dteDate.TabIndex = 33;
            // 
            // postDate
            // 
            appearance150.TextHAlignAsString = "Center";
            appearance150.TextVAlignAsString = "Middle";
            this.postDate.Appearance = appearance150;
            this.postDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.postDate.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.postDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.postDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.postDate.Location = new System.Drawing.Point(106, 49);
            this.postDate.MaskInput = "dd/mm/yyyy";
            this.postDate.Name = "postDate";
            this.postDate.Size = new System.Drawing.Size(100, 19);
            this.postDate.TabIndex = 32;
            this.postDate.ValueChanged += new System.EventHandler(this.postDate_ValueChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(470, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "Phương thức thanh toán";
            // 
            // cbbPaymentMethod
            // 
            this.cbbPaymentMethod.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbPaymentMethod.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbPaymentMethod.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem1.DataValue = "Tiền mặt ";
            valueListItem2.DataValue = "Ủy nhiệm chi";
            valueListItem3.DataValue = "Séc chuyển khoản";
            valueListItem4.DataValue = "Séc tiền mặt";
            valueListItem5.DataValue = "Thẻ tín dụng";
            this.cbbPaymentMethod.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3,
            valueListItem4,
            valueListItem5});
            this.cbbPaymentMethod.Location = new System.Drawing.Point(598, 28);
            this.cbbPaymentMethod.Name = "cbbPaymentMethod";
            this.cbbPaymentMethod.Size = new System.Drawing.Size(144, 21);
            this.cbbPaymentMethod.TabIndex = 28;
            this.cbbPaymentMethod.SelectionChanged += new System.EventHandler(this.cbbPaymentMethod_SelectionChanged);
            // 
            // ultraToolTipManager1
            // 
            this.ultraToolTipManager1.AutoPopDelay = 10000;
            this.ultraToolTipManager1.ContainingControl = this;
            this.ultraToolTipManager1.InitialDelay = 2000;
            this.ultraToolTipManager1.ToolTipImage = Infragistics.Win.ToolTipImage.Error;
            appearance152.BackColor = System.Drawing.Color.Red;
            this.ultraToolTipManager1.ToolTipTitleAppearance = appearance152;
            // 
            // FPPPayVendorDetail
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(755, 662);
            this.Controls.Add(this.panel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FPPPayVendorDetail";
            this.Text = "Trả tiền nhà cung cấp";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridBill)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridAccounting)).EndInit();
            this.cms4Grid.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.palCurrency.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridCurrency)).EndInit();
            this.panel5.ResumeLayout(false);
            this.pnMBCreditCard.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.ultraGroupBox5.ResumeLayout(false);
            this.ultraGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditCardAccountingObjectBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardAccountingObjectBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditCardAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditCardAccountingObjectAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).EndInit();
            this.ultraGroupBox6.ResumeLayout(false);
            this.ultraGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditCardReason)).EndInit();
            this.pnMBTellerPaper_SecCK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKAccountingObjectBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecCKAccountingObjectBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecCKAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKAccountingObjectAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ultraGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecCKBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecCKBankAccountDetailID)).EndInit();
            this.pnMBTellerPaper_Sec.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            this.ultraGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtSecIssueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecIdentificationNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecIssueBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecReceiver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).EndInit();
            this.ultraGroupBox7.ResumeLayout(false);
            this.ultraGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSecBankAccountDetailID)).EndInit();
            this.pnMBTellerPaper_PayOrder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gpAccountingObject)).EndInit();
            this.gpAccountingObject.ResumeLayout(false);
            this.gpAccountingObject.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccreditativeAccountingObjectBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccreditativeAccountingObjectBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccreditativeAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccreditativeAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccreditativeAccountingObjectAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpPaymentOrder)).EndInit();
            this.gpPaymentOrder.ResumeLayout(false);
            this.gpPaymentOrder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentOrderReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentOrderBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPaymentOrderBankAccountDetailID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpMCPayment)).EndInit();
            this.gpMCPayment.ResumeLayout(false);
            this.gpMCPayment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccreditativeAttach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPaymentAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentAccountingObjectAddress)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.postDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPaymentMethod)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinToolTip.UltraToolTipManager ultraToolTipManager1;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private System.Windows.Forms.Panel panel5;
        private Infragistics.Win.Misc.UltraGroupBox gpMCPayment;
        private System.Windows.Forms.Label label9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccreditativeAttach;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReceiver;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPaymentAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbPaymentAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPaymentAccountingObjectAddress;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label D;
        private System.Windows.Forms.Panel pnMBCreditCard;
        private System.Windows.Forms.Panel pnMBTellerPaper_Sec;
        private System.Windows.Forms.Panel pnMBTellerPaper_PayOrder;
        private Infragistics.Win.Misc.UltraGroupBox gpAccountingObject;
        private Infragistics.Win.Misc.UltraGroupBox gpPaymentOrder;
        private System.Windows.Forms.Panel pnMBTellerPaper_SecCK;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccreditativeAccountingObjectBankName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccreditativeAccountingObjectBankAccount;
        private System.Windows.Forms.Label label13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccreditativeAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccreditativeAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccreditativeAccountingObjectAddress;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label12;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPaymentOrderReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPaymentOrderBankName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbPaymentOrderBankAccountDetailID;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecCKAccountingObjectBankName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbSecCKAccountingObjectBankAccount;
        private System.Windows.Forms.Label label14;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecCKAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbSecCKAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecCKAccountingObjectAddress;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecCKReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecCKBankName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbSecCKBankAccountDetailID;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtSecIssueDate;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecIdentificationNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecIssueBy;
        private System.Windows.Forms.Label label20;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbSecAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecReceiver;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSecBankName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbSecBankAccountDetailID;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCreditCardAccountingObjectBankName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCreditCardAccountingObjectBankAccount;
        private System.Windows.Forms.Label label27;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCreditCardAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCreditCardAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCreditCardAccountingObjectAddress;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCreditCardReason;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label lblCreditCardType;
        private System.Windows.Forms.Label label32;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCreditCardNumber;
        private System.Windows.Forms.Label lblCreditCardOwnerCard;
        //private System.Windows.Forms.Label lblImageCreditCardType;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox picCreditCardType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbPaymentMethod;
        private Infragistics.Win.Misc.UltraButton btnApply;
        private Infragistics.Win.Misc.UltraButton btnCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridBill;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridAccounting;
        private System.Windows.Forms.Panel palCurrency;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCurrency;
        private Infragistics.Win.Misc.UltraLabel lblPostedDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        public Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDate;
        public Infragistics.Win.UltraWinEditors.UltraDateTimeEditor postDate;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        public System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripMenuItem cmbtnAddRow;
        private System.Windows.Forms.ToolStripMenuItem cmbtnDelRow;
    }
}