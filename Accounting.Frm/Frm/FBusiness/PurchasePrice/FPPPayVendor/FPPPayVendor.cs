﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;

namespace Accounting
{
    public partial class FPPPayVendor : CustormForm
    {

        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FPPPayVendor()
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();

            List<Item> lstItems = Utils.ObjConstValue.SelectTimes;
            // load dữ liệu cho Combobox Chọn thời gian
            cbbDateTime.DataSource = lstItems;
            cbbDateTime.DisplayMember = "Name";
            Utils.ConfigGrid(cbbDateTime, ConstDatabase.ConfigXML_TableName);
            cbbDateTime.ValueChanged += cbbDateTime_ValueChanged;
            cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode("FPPPayVendor", Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                //dateTimeCacheHistory = new DateTimeCacheHistory();
                //dateTimeCacheHistory.ID = Guid.NewGuid();
                //dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                //dateTimeCacheHistory.SubSystemCode = "FPPPayVendor";
                //dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                //dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                //dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                //_IDateTimeCacheHistoryService.BeginTran();
                //_IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                //_IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

            LoadDuLieu(true, dtBeginDate.DateTime, dtEndDate.DateTime);
            btnReLoad.Visible = false;

            

            WaitingFrm.StopWaiting();

            
        }

        #region Sự kiện
        private void uGridIndex_AfterSelectChange(object sender, Infragistics.Win.UltraWinGrid.AfterSelectChangeEventArgs e)
        {
            // lấy đối tượng được chọn
            if (uGridIndex.Selected.Rows.Count > 0)
            {
                PayVendor model = uGridIndex.Selected.Rows[0].ListObject as PayVendor;
                ViewTabInfomation(model);
            }

        }

        private void cbbDateTime_ValueChanged(object sender, EventArgs e)
        {
            var cbb = (UltraCombo)sender;
            if (cbb.SelectedRow != null)
            {
                var model = cbb.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(DateTime.Now.Year, DateTime.Now, model, out dtBegin, out dtEnd);
                dtBeginDate.Value = dtBegin;
                dtEndDate.Value = dtEnd;
            }
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction(false);
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction(false);
        }
        private void btnReLoad_Click(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }
        private void uGridIndex_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGridIndex, cms4Grid);
        }
        private void uGridIndex_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            AddFunction(true);
        }
        #endregion

        #region Xử lý
        private void LoadDuLieu(bool configGrid = true, DateTime? beginDate = null, DateTime? endDate = null)
        {
            #region hiển thị và xử lý hiển thị
            try
            {
                ViewTabIndex(Utils.IAccountingObjectService.GetListPayVendor(Utils.StringToDateTime(Utils.GetDbStartDate()) ?? DateTime.Now, beginDate, endDate), configGrid);
            }
            catch(Exception ex)
            { }
            #endregion
            if (uGridIndex.Rows.Count > 0) uGridIndex.Rows[0].Selected = true;
        }
        /// <summary>
        /// Dùng view danh sách khách hàng có phát sinh
        /// </summary>
        /// <param name="lstCustomerDebit">Danh sách khách hàng có phát sinh</param>
        /// <param name="configGrid"></param>
        public void ViewTabIndex(List<PayVendor> lstCustomerDebit, bool configGrid = true)
        {
            uGridIndex.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            uGridIndex.DataSource = lstCustomerDebit;
            uGridIndex.DisplayLayout.Bands[0].Summaries.Clear();
            if (configGrid) Utils.ConfigGrid(uGridIndex, ConstDatabase.PayVendor_TableName);
            //Tổng số hàng
            uGridIndex.DisplayLayout.Override.AllowRowSummaries = AllowRowSummaries.Default; //Ẩn ký hiệu tổng trên Header Caption
            UltraGridBand band = uGridIndex.DisplayLayout.Bands[0];
            Utils.FormatNumberic(band.Columns["SoDuDauNam"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(band.Columns["SoPhatSinh"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(band.Columns["SoDaTra"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(band.Columns["SoConPhaiTra"], ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridIndex, "SoDuDauNam", false);
            Utils.AddSumColumn(uGridIndex, "SoPhatSinh", false);
            Utils.AddSumColumn(uGridIndex, "SoDaTra", false);
            Utils.AddSumColumn(uGridIndex, "SoConPhaiTra", false);
        }
        public void ViewTabInfomation(PayVendor model)
        {
            // thực hiện hiển thị thông tin lên tab thông tin chung
            txtCustomerCode.Text = model.AccountingObjectCode;
            txtCustomerName.Text = model.AccountingObjectName;
            txtAddress.Text = model.AccountingObjectAddress;
            txtMST.Text = model.TaxCode;
            txtNLH.Text = model.ContactName;
            Utils.FormatNumberic(txtSoDuDauNam, model.SoDuDauNam, ConstDatabase.Format_TienVND);
            ViewTabDetails((List<BillPPPayVendorDetail>)model.Bill.OrderByDescending(c=>c.Date).ToList());
        }
        public void ViewTabDetails(List<BillPPPayVendorDetail> voucherVendor)
        {
            uGridCT.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            uGridCT.DataSource = voucherVendor;
            Utils.ConfigGrid(uGridCT, ConstDatabase.BillPPPayVendorDetail_TableName);
            uGridCT.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            //Tổng số hàng
            uGridCT.DisplayLayout.Override.AllowRowSummaries = AllowRowSummaries.Default; //Ẩn ký hiệu tổng trên Header Caption
            UltraGridBand band = uGridCT.DisplayLayout.Bands[0];
            Utils.FormatNumberic(band.Columns["TotalDebit"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(band.Columns["PaymentAmount"], ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCT, "PaymentAmount", false);
            Utils.AddSumColumn(uGridCT, "TotalDebit", false);
        }
        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        void AddFunction(bool edit)
        {
            if(edit == true)
            {
                PayVendor temp = uGridIndex.Selected.Rows.Count <= 0 ? new PayVendor() : uGridIndex.Selected.Rows[0].ListObject as PayVendor;
                //new FPPPayVendorDetail(temp, (IList<PayVendor>)uGridIndex.DataSource, ConstFrm.optStatusForm.Edit).ShowDialog(this);
                //HUYPD Edit Show Form
                new FPPPayVendorDetail(temp, (IList<PayVendor>)uGridIndex.DataSource, ConstFrm.optStatusForm.Edit).Show(this);
            }
            else
            {
                PayVendor temp = new PayVendor();
                //new FPPPayVendorDetail(temp, (IList<PayVendor>)uGridIndex.DataSource, ConstFrm.optStatusForm.Add).ShowDialog(this);
                //HUYPD Add Show Form
                new FPPPayVendorDetail(temp, (IList<PayVendor>)uGridIndex.DataSource, ConstFrm.optStatusForm.Add).Show(this);
            }

            if (!FPPPayVendorDetail.IsClose) LoadDuLieu(false, dtBeginDate.DateTime, dtEndDate.DateTime);
        }
        #endregion

        private void btnLoc_Click(object sender, EventArgs e)
        {

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode("FPPPayVendor", Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = "FPPPayVendor";
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            LoadDuLieu(false, dtBeginDate.DateTime, dtEndDate.DateTime);
        }

        private void FPPPayVendor_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGridIndex.ResetText();
            uGridIndex.ResetUpdateMode();
            uGridIndex.ResetExitEditModeOnLeave();
            uGridIndex.ResetRowUpdateCancelAction();
            uGridIndex.DataSource = null;
            uGridIndex.Layouts.Clear();
            uGridIndex.ResetLayouts();
            uGridIndex.ResetDisplayLayout();
            uGridIndex.Refresh();
            uGridIndex.ClearUndoHistory();
            uGridIndex.ClearXsdConstraints();

            uGridCT.ResetText();
            uGridCT.ResetUpdateMode();
            uGridCT.ResetExitEditModeOnLeave();
            uGridCT.ResetRowUpdateCancelAction();
            uGridCT.DataSource = null;
            uGridCT.Layouts.Clear();
            uGridCT.ResetLayouts();
            uGridCT.ResetDisplayLayout();
            uGridCT.Refresh();
            uGridCT.ClearUndoHistory();
            uGridCT.ClearXsdConstraints();
        }

        private void FPPPayVendor_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
