﻿namespace Accounting
{
    partial class FPPPayVendor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.txtMST = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSoDuDauNam = new System.Windows.Forms.Label();
            this.txtNLH = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.Label();
            this.txtCustomerName = new System.Windows.Forms.Label();
            this.txtCustomerCode = new System.Windows.Forms.Label();
            this.lblSoDuDauNam = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblCustomerName = new System.Windows.Forms.Label();
            this.lblCustomerCode = new System.Windows.Forms.Label();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridCT = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.uGridIndex = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnLoc = new Infragistics.Win.Misc.UltraButton();
            this.lblBeginDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblEndDate = new Infragistics.Win.Misc.UltraLabel();
            this.dtBeginDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblKyKeToan = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDateTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.btnReLoad = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabPageControl1.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCT)).BeginInit();
            this.cms4Grid.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridIndex)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.txtMST);
            this.ultraTabPageControl1.Controls.Add(this.label1);
            this.ultraTabPageControl1.Controls.Add(this.txtSoDuDauNam);
            this.ultraTabPageControl1.Controls.Add(this.txtNLH);
            this.ultraTabPageControl1.Controls.Add(this.txtAddress);
            this.ultraTabPageControl1.Controls.Add(this.txtCustomerName);
            this.ultraTabPageControl1.Controls.Add(this.txtCustomerCode);
            this.ultraTabPageControl1.Controls.Add(this.lblSoDuDauNam);
            this.ultraTabPageControl1.Controls.Add(this.lblEmail);
            this.ultraTabPageControl1.Controls.Add(this.lblAddress);
            this.ultraTabPageControl1.Controls.Add(this.lblCustomerName);
            this.ultraTabPageControl1.Controls.Add(this.lblCustomerCode);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(837, 232);
            // 
            // txtMST
            // 
            this.txtMST.AutoSize = true;
            this.txtMST.BackColor = System.Drawing.Color.Transparent;
            this.txtMST.Location = new System.Drawing.Point(118, 80);
            this.txtMST.Name = "txtMST";
            this.txtMST.Size = new System.Drawing.Size(10, 13);
            this.txtMST.TabIndex = 36;
            this.txtMST.Text = ":";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 35;
            this.label1.Text = "Mã số thuế";
            // 
            // txtSoDuDauNam
            // 
            this.txtSoDuDauNam.AutoSize = true;
            this.txtSoDuDauNam.BackColor = System.Drawing.Color.Transparent;
            this.txtSoDuDauNam.Location = new System.Drawing.Point(118, 134);
            this.txtSoDuDauNam.Name = "txtSoDuDauNam";
            this.txtSoDuDauNam.Size = new System.Drawing.Size(10, 13);
            this.txtSoDuDauNam.TabIndex = 34;
            this.txtSoDuDauNam.Text = ":";
            // 
            // txtNLH
            // 
            this.txtNLH.AutoSize = true;
            this.txtNLH.BackColor = System.Drawing.Color.Transparent;
            this.txtNLH.Location = new System.Drawing.Point(118, 108);
            this.txtNLH.Name = "txtNLH";
            this.txtNLH.Size = new System.Drawing.Size(10, 13);
            this.txtNLH.TabIndex = 33;
            this.txtNLH.Text = ":";
            // 
            // txtAddress
            // 
            this.txtAddress.AutoSize = true;
            this.txtAddress.BackColor = System.Drawing.Color.Transparent;
            this.txtAddress.Location = new System.Drawing.Point(118, 56);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(10, 13);
            this.txtAddress.TabIndex = 31;
            this.txtAddress.Text = ":";
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.AutoSize = true;
            this.txtCustomerName.BackColor = System.Drawing.Color.Transparent;
            this.txtCustomerName.Location = new System.Drawing.Point(118, 30);
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new System.Drawing.Size(10, 13);
            this.txtCustomerName.TabIndex = 30;
            this.txtCustomerName.Text = ":";
            // 
            // txtCustomerCode
            // 
            this.txtCustomerCode.AutoSize = true;
            this.txtCustomerCode.BackColor = System.Drawing.Color.Transparent;
            this.txtCustomerCode.Location = new System.Drawing.Point(118, 4);
            this.txtCustomerCode.Name = "txtCustomerCode";
            this.txtCustomerCode.Size = new System.Drawing.Size(10, 13);
            this.txtCustomerCode.TabIndex = 29;
            this.txtCustomerCode.Text = ":";
            // 
            // lblSoDuDauNam
            // 
            this.lblSoDuDauNam.AutoSize = true;
            this.lblSoDuDauNam.Location = new System.Drawing.Point(11, 133);
            this.lblSoDuDauNam.Name = "lblSoDuDauNam";
            this.lblSoDuDauNam.Size = new System.Drawing.Size(80, 13);
            this.lblSoDuDauNam.TabIndex = 25;
            this.lblSoDuDauNam.Text = "Số dư đầu năm";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(11, 107);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(69, 13);
            this.lblEmail.TabIndex = 24;
            this.lblEmail.Text = "Người liên hệ";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(11, 55);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(40, 13);
            this.lblAddress.TabIndex = 22;
            this.lblAddress.Text = "Địa chỉ";
            // 
            // lblCustomerName
            // 
            this.lblCustomerName.AutoSize = true;
            this.lblCustomerName.Location = new System.Drawing.Point(11, 29);
            this.lblCustomerName.Name = "lblCustomerName";
            this.lblCustomerName.Size = new System.Drawing.Size(74, 13);
            this.lblCustomerName.TabIndex = 21;
            this.lblCustomerName.Text = "Tên đối tượng";
            // 
            // lblCustomerCode
            // 
            this.lblCustomerCode.AutoSize = true;
            this.lblCustomerCode.Location = new System.Drawing.Point(11, 3);
            this.lblCustomerCode.Name = "lblCustomerCode";
            this.lblCustomerCode.Size = new System.Drawing.Size(70, 13);
            this.lblCustomerCode.TabIndex = 20;
            this.lblCustomerCode.Text = "Mã đối tượng";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridCT);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(837, 232);
            // 
            // uGridCT
            // 
            this.uGridCT.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridCT.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            this.uGridCT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCT.Location = new System.Drawing.Point(0, 0);
            this.uGridCT.Name = "uGridCT";
            this.uGridCT.Size = new System.Drawing.Size(837, 232);
            this.uGridCT.TabIndex = 1;
            this.uGridCT.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // cms4Grid
            // 
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tsmAdd,
            this.tmsReLoad});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(150, 54);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(146, 6);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.Size = new System.Drawing.Size(149, 22);
            this.tsmAdd.Text = "Thêm";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tmsReLoad
            // 
            this.tmsReLoad.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tmsReLoad.Name = "tmsReLoad";
            this.tmsReLoad.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.tmsReLoad.Size = new System.Drawing.Size(149, 22);
            this.tmsReLoad.Text = "Tải Lại";
            this.tmsReLoad.Click += new System.EventHandler(this.tmsReLoad_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.uGridIndex);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 49);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(841, 183);
            this.panel3.TabIndex = 4;
            // 
            // uGridIndex
            // 
            this.uGridIndex.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridIndex.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridIndex.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridIndex.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridIndex.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            this.uGridIndex.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGridIndex.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGridIndex.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGridIndex.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridIndex.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridIndex.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridIndex.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridIndex.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGridIndex.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGridIndex.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGridIndex.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridIndex.Location = new System.Drawing.Point(0, 0);
            this.uGridIndex.Name = "uGridIndex";
            this.uGridIndex.Size = new System.Drawing.Size(841, 183);
            this.uGridIndex.TabIndex = 1;
            this.uGridIndex.Text = "uGrid";
            this.uGridIndex.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.uGridIndex_AfterSelectChange);
            this.uGridIndex.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridIndex_DoubleClickRow);
            this.uGridIndex.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uGridIndex_MouseDown);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 232);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 258;
            this.ultraSplitter1.Size = new System.Drawing.Size(841, 10);
            this.ultraSplitter1.TabIndex = 21;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ultraTabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 242);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(841, 258);
            this.panel2.TabIndex = 6;
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(841, 258);
            this.ultraTabControl1.TabIndex = 20;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Thông tin chung";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Chi tiết";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(837, 232);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnLoc);
            this.panel1.Controls.Add(this.lblBeginDate);
            this.panel1.Controls.Add(this.lblEndDate);
            this.panel1.Controls.Add(this.dtBeginDate);
            this.panel1.Controls.Add(this.dtEndDate);
            this.panel1.Controls.Add(this.lblKyKeToan);
            this.panel1.Controls.Add(this.cbbDateTime);
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.btnReLoad);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(841, 49);
            this.panel1.TabIndex = 5;
            // 
            // btnLoc
            // 
            this.btnLoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance11.BackColor = System.Drawing.Color.DarkRed;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance11.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance11.Image = global::Accounting.Properties.Resources.filter;
            this.btnLoc.Appearance = appearance11;
            this.btnLoc.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance12.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.btnLoc.HotTrackAppearance = appearance12;
            this.btnLoc.Location = new System.Drawing.Point(757, 10);
            this.btnLoc.Name = "btnLoc";
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.btnLoc.PressedAppearance = appearance13;
            this.btnLoc.Size = new System.Drawing.Size(63, 30);
            this.btnLoc.TabIndex = 68;
            this.btnLoc.Text = "Lọc";
            this.btnLoc.Click += new System.EventHandler(this.btnLoc_Click);
            // 
            // lblBeginDate
            // 
            this.lblBeginDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextHAlignAsString = "Left";
            appearance14.TextVAlignAsString = "Bottom";
            this.lblBeginDate.Appearance = appearance14;
            this.lblBeginDate.Location = new System.Drawing.Point(516, 19);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.Size = new System.Drawing.Size(24, 17);
            this.lblBeginDate.TabIndex = 64;
            this.lblBeginDate.Text = "Từ:";
            // 
            // lblEndDate
            // 
            this.lblEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextHAlignAsString = "Left";
            appearance15.TextVAlignAsString = "Bottom";
            this.lblEndDate.Appearance = appearance15;
            this.lblEndDate.Location = new System.Drawing.Point(633, 19);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(32, 17);
            this.lblEndDate.TabIndex = 65;
            this.lblEndDate.Text = "Đến:";
            // 
            // dtBeginDate
            // 
            this.dtBeginDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance16.TextHAlignAsString = "Center";
            appearance16.TextVAlignAsString = "Middle";
            this.dtBeginDate.Appearance = appearance16;
            this.dtBeginDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtBeginDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtBeginDate.Location = new System.Drawing.Point(543, 17);
            this.dtBeginDate.MaskInput = "";
            this.dtBeginDate.Name = "dtBeginDate";
            this.dtBeginDate.Size = new System.Drawing.Size(85, 21);
            this.dtBeginDate.TabIndex = 66;
            this.dtBeginDate.Value = null;
            // 
            // dtEndDate
            // 
            this.dtEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance17.TextHAlignAsString = "Center";
            appearance17.TextVAlignAsString = "Middle";
            this.dtEndDate.Appearance = appearance17;
            this.dtEndDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtEndDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtEndDate.Location = new System.Drawing.Point(669, 17);
            this.dtEndDate.MaskInput = "";
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.Size = new System.Drawing.Size(83, 21);
            this.dtEndDate.TabIndex = 67;
            this.dtEndDate.Value = null;
            // 
            // lblKyKeToan
            // 
            this.lblKyKeToan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextHAlignAsString = "Left";
            appearance18.TextVAlignAsString = "Bottom";
            this.lblKyKeToan.Appearance = appearance18;
            this.lblKyKeToan.Location = new System.Drawing.Point(347, 19);
            this.lblKyKeToan.Name = "lblKyKeToan";
            this.lblKyKeToan.Size = new System.Drawing.Size(29, 17);
            this.lblKyKeToan.TabIndex = 63;
            this.lblKyKeToan.Text = "Kỳ:";
            // 
            // cbbDateTime
            // 
            this.cbbDateTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbDateTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDateTime.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbDateTime.Location = new System.Drawing.Point(382, 17);
            this.cbbDateTime.Name = "cbbDateTime";
            this.cbbDateTime.NullText = "<chọn dữ liệu>";
            this.cbbDateTime.Size = new System.Drawing.Size(128, 22);
            this.cbbDateTime.TabIndex = 62;
            // 
            // btnAdd
            // 
            appearance19.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.btnAdd.Appearance = appearance19;
            this.btnAdd.Location = new System.Drawing.Point(12, 10);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 30);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnReLoad
            // 
            appearance20.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.btnReLoad.Appearance = appearance20;
            this.btnReLoad.Location = new System.Drawing.Point(85, 10);
            this.btnReLoad.Name = "btnReLoad";
            this.btnReLoad.Size = new System.Drawing.Size(75, 30);
            this.btnReLoad.TabIndex = 6;
            this.btnReLoad.Text = "Nạp";
            this.btnReLoad.Click += new System.EventHandler(this.btnReLoad_Click);
            // 
            // FPPPayVendor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 500);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.ultraSplitter1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FPPPayVendor";
            this.Text = "Danh sách trả tiền nhà cung cấp";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FPPPayVendor_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FPPPayVendor_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl1.PerformLayout();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridCT)).EndInit();
            this.cms4Grid.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridIndex)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridIndex;
        private System.Windows.Forms.Panel panel2;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCT;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.Misc.UltraButton btnAdd;
        private Infragistics.Win.Misc.UltraButton btnReLoad;
        private System.Windows.Forms.Label txtSoDuDauNam;
        private System.Windows.Forms.Label txtNLH;
        private System.Windows.Forms.Label txtAddress;
        private System.Windows.Forms.Label txtCustomerName;
        private System.Windows.Forms.Label txtCustomerCode;
        private System.Windows.Forms.Label lblSoDuDauNam;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblCustomerName;
        private System.Windows.Forms.Label lblCustomerCode;
        private System.Windows.Forms.Label txtMST;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tmsReLoad;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        public Infragistics.Win.Misc.UltraButton btnLoc;
        private Infragistics.Win.Misc.UltraLabel lblBeginDate;
        private Infragistics.Win.Misc.UltraLabel lblEndDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtBeginDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtEndDate;
        private Infragistics.Win.Misc.UltraLabel lblKyKeToan;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDateTime;
    }
}