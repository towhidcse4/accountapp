﻿using System.Collections;
using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using FX.Data;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.UltraWinToolTip;

namespace Accounting
{
    public partial class FPPPayVendorDetail : CustormForm
    {
        #region Khai báo
        List<AccountingObject> accObjectVendor = new List<AccountingObject>();
        List<BankAccountDetail> bankAcc = new List<BankAccountDetail>();
        List<AccountingObjectBankAccount> accObjectBank = new List<AccountingObjectBankAccount>();
        private readonly IAccountDefaultService _IAccountDefaultService = Utils.IAccountDefaultService;
        List<PayVendor> lstPVendor = new List<PayVendor>();
        PayVendor payVendor = new PayVendor();
        List<BudgetItem> lstBudgetItemTree = new List<BudgetItem>();
        List<ExpenseItem> lstExpenseItemTree = new List<ExpenseItem>();
        List<Department> lstDepartmentTree = new List<Department>();
        List<CostSet> lstCostSetTree = new List<CostSet>();
        List<StatisticsCode> lstStatisticsCodeTree = new List<StatisticsCode>();
        Dictionary<string, string> dicAccountDefault = new Dictionary<string, string>();
        private readonly IMCPaymentService _IMCPaymentService = IoC.Resolve<IMCPaymentService>();
        private readonly IMCPaymentDetailService _IMCPaymentDetailService = IoC.Resolve<IMCPaymentDetailService>();
        private readonly IMCPaymentDetailVendorService _IMCPaymentDetailVendorService = IoC.Resolve<IMCPaymentDetailVendorService>();
        private readonly IMBTellerPaperService _IMBTellerPaperService = IoC.Resolve<IMBTellerPaperService>();
        private readonly IMBTellerPaperDetailService _IMBTellerPaperDetailService = IoC.Resolve<IMBTellerPaperDetailService>();
        private readonly IMBTellerPaperDetailVendorService _IMBTellerPaperDetailVendorService = IoC.Resolve<IMBTellerPaperDetailVendorService>();
        private readonly IMBCreditCardService _IMBCreditCardService = IoC.Resolve<IMBCreditCardService>();
        private readonly IMBCreditCardDetailService _IMBCreditCardDetailService = IoC.Resolve<IMBCreditCardDetailService>();
        private readonly IMBCreditCardDetailVendorService _IMBCreditCardDetailVendorService = IoC.Resolve<IMBCreditCardDetailVendorService>();
        private readonly IGeneralLedgerService _IGeneralLedgerService = IoC.Resolve<IGeneralLedgerService>();
        private readonly IGenCodeService _IGenCodeService = IoC.Resolve<IGenCodeService>();
        public IAccountingObjectService _IAccountingObjectService { get { return IoC.Resolve<IAccountingObjectService>(); } }
        bool isBQCK = Utils.ListSystemOption.FirstOrDefault(x => x.ID == 89).Data == "Bình quân cuối kỳ";
        int _statusForm;
        bool _statusTemp = false;
        bool _statusSmart = false;
        string _rollBackAccObj;
        bool isRun = false;
        Point p = new Point();
        private List<PayVendor> _listSelect;
        private int type = 118;
        public List<IList> ListObjectInput = new List<IList>();
        int TypeGroup = 11;
        int LamTron = 0;
        int LamTron1 = 2;
        string currencyID = "";
        #endregion

        #region Khởi tạo
        public FPPPayVendorDetail(PayVendor temp, IList<PayVendor> list, int statusForm)
        {
            #region Khởi tạo giá trị mặc định của Form

            InitializeComponent();
            Utils.SortFormControls(this);

            #endregion

            #region Thiết lập ban đầu cho Form
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            WaitingFrm.StartWaiting();
            uGridBill.AfterExitEditMode += uGridBill_AfterExitEditMode;
            _statusForm = statusForm;
            _listSelect = list.ToList();
            LamTron = int.Parse(Utils.ListSystemOption.FirstOrDefault(x => x.Code == "DDSo_TienVND").Data);
            LamTron1 = int.Parse(Utils.ListSystemOption.FirstOrDefault(x => x.Code == "DDSo_NgoaiTe").Data);
            if (temp == null)
            {
                payVendor = new PayVendor();
                gpMCPayment.Visible = true;
            }
            else
            {
                payVendor = temp;
                cbbPaymentAccountingObjectID.Value = payVendor.AccountingObjectID;
            }
            payVendor.Accounting = new List<AccountingPPPayVendorDetail>();
            InitializeGUI(payVendor);
            //=====//
            //this.WindowState = FormWindowState.Maximized;
            //Utils.ClearCacheByType<SystemOption>();

            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;
                this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
                this.StartPosition = FormStartPosition.CenterScreen;


            }
            WaitingFrm.StopWaiting();
            #endregion
        }

        //Thiết lập định dang mặc định cho Vùng tiền tệ
        private void uGridCurrency_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            #region DuyTN sửa lại cho đúng với load các loại tiền chung
            Utils.FormatNumberic(e.Row.Cells["ExchangeRate"].Column, ConstDatabase.Format_Rate);
            Utils.FormatNumberic(e.Row.Cells["TotalAmountOriginal"].Column, (string)e.Row.Cells["CurrencyID"].Value == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(e.Row.Cells["TotalAmount"].Column, ConstDatabase.Format_TienVND);
            //e.Row.Cells["TotalAmount"].Value = ((decimal)e.Row.Cells["TotalAmountOriginal"].Value) * e.Row.Cells["ExchangeRate"].GetValueFromTextAndNotNull();
            e.Row.Update();
            uGridCurrency.UpdateData();
            #endregion
        }
        private void uGridAccounting_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            e.Row.Cells["TotalAmountOriginal"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            e.Row.Cells["TotalAmount"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;

        }
        private void uGridBill_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (e.Row.Cells["AmountOriginal"].Value != null)
            {
                e.Row.Cells["DifferAmount"].Value = Math.Round((((decimal)e.Row.Cells["AmountOriginal"].Value * (decimal)uGridCurrency.Rows[0].Cells["ExchangeRate"].Value)) - ((decimal)e.Row.Cells["LastExchangeRate"].Value == 0 ?
                  ((decimal)e.Row.Cells["AmountOriginal"].Value * (decimal)e.Row.Cells["RefVoucherExchangeRate"].Value) : ((decimal)e.Row.Cells["AmountOriginal"].Value * (decimal)e.Row.Cells["LastExchangeRate"].Value)), LamTron, MidpointRounding.AwayFromZero);
                if (isBQCK)
                {
                    e.Row.Cells["Amount"].Value = (decimal)e.Row.Cells["AmountOriginal"].Value * (decimal)uGridCurrency.Rows[0].Cells["ExchangeRate"].Value;
                    e.Row.Cells["AmountTT"].Value = (decimal)e.Row.Cells["DifferAmount"].Value <= 0 ? Math.Round(((decimal)e.Row.Cells["AmountOriginal"].Value * (decimal)uGridCurrency.Rows[0].Cells["ExchangeRate"].Value), LamTron, MidpointRounding.AwayFromZero) :
                 Math.Round(((decimal)e.Row.Cells["AmountOriginal"].Value * ((decimal)e.Row.Cells["LastExchangeRate"].Value == 0 ? (decimal)e.Row.Cells["RefVoucherExchangeRate"].Value : (decimal)e.Row.Cells["LastExchangeRate"].Value)), LamTron, MidpointRounding.AwayFromZero);
                }
                else
                {
                    e.Row.Cells["Amount"].Value = (decimal)e.Row.Cells["LastExchangeRate"].Value > 0 ? ((decimal)e.Row.Cells["AmountOriginal"].Value * (decimal)e.Row.Cells["LastExchangeRate"].Value)
                        : ((decimal)e.Row.Cells["AmountOriginal"].Value * (decimal)e.Row.Cells["RefVoucherExchangeRate"].Value);
                    e.Row.Cells["AmountTT"].Value = (decimal)e.Row.Cells["Amount"].Value;
                }
            }
            e.Row.Cells["TotalDebitOriginal"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            e.Row.Cells["TotalDebit"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            e.Row.Cells["DebitAmountOriginal"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            e.Row.Cells["DebitAmount"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            e.Row.Cells["AmountOriginal"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            e.Row.Cells["Amount"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            e.Row.Cells["DiscountRate"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DoubleNonNegative;
            e.Row.Cells["DiscountAmountOriginal"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            e.Row.Cells["DiscountAmount"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            e.Row.Cells["CheckColumn"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            var dicDefaultAccount = _IAccountDefaultService.DefaultAccount();
            if (dicDefaultAccount.Any(valuePair => valuePair.Key == string.Format("{0};{1}", 250, "DiscountAccount")))
            {
                var keyDefaultAccount =
                    dicDefaultAccount.FirstOrDefault(valuePair => valuePair.Key == string.Format("{0};{1}", 250, "DiscountAccount"));
                if (keyDefaultAccount.Value != null && !string.IsNullOrEmpty(keyDefaultAccount.Value))
                {
                    e.Row.Cells["DiscountAccount"].Value = keyDefaultAccount.Value;
                }
            }
            e.Row.Update();
            uGridBill.UpdateData();
        }
        #endregion

        #region overide
        private void InitializeGUI(PayVendor temp)
        {
            #region Thiết lập dữ liệu và Fill dữ liệu Obj vào control (nếu đang sửa)
            //Đặt phương thức thanh toán mặc định
            cbbPaymentMethod.SelectedIndex = 0;

            //Đặt giá trị mặc định vùng chứng từ
            dteDate.Value = DateTime.Now;
            postDate.Value = dteDate.Value;
            #region Lấy DS Đối tượng NCC
            bankAcc = Utils.IBankAccountDetailService.GetIsActive(true);
            accObjectVendor = Utils.IAccountingObjectService.GetAccountingObjects(1, true);
            accObjectBank = Utils.IAccountingObjectBankAccountService.GetAll();
            #endregion

            #region Thiết lập vùng tiền tệ
            //if(_statusForm == ConstFrm.optStatusForm.Add)
            //{
            //    temp.TotalAmountOriginal = 0;
            //    temp.TotalAmount = 0;
            //    temp.CurrencyID = "VND";
            //}
            var currencyTemp = new CurrencyPVendor() { TotalAmount = 0, TotalAmountOriginal = 0, CurrencyID = "VND", ExchangeRate = 1 };
            var currency = new List<CurrencyPVendor> { currencyTemp };
            ViewCurrency(currency);
            #endregion

            #region Thiết lập Tab Hóa đơn
            ViewTabBill(temp.Bill.Where(x => x.DebitAmountOriginal > 0).OrderByDescending(x => x.Date).ToList());

            #endregion

            #region Thiết lập Tab Hạch toán
            uGridAccounting.DataSource = new BindingList<AccountingPPPayVendorDetail>();
            ViewTabAccounting(false);
            #endregion

            #region Thiết lập thông tin mặc định cho phương thức TT tiền mặt
            ConfigComboAccObject(payVendor, cbbPaymentAccountingObjectID);
            //isRun = false;
            //cbbPaymentAccountingObjectID.SelectedRow = cbbPaymentAccountingObjectID.Rows[0];
            //isRun = true;
            //cbbPaymentAccountingObjectID.Show();
            //txtPaymentAccountingObjectName.DataBindings.Clear();
            //txtPaymentAccountingObjectName.DataBindings.Add("Text", payVendor, "AccountingObjectName", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtPaymentAccountingObjectAddress.DataBindings.Clear();
            //txtPaymentAccountingObjectAddress.DataBindings.Add("Text", payVendor, "AccountingObjectAddress", true, DataSourceUpdateMode.OnPropertyChanged);
            #endregion

            #region Thiết lập thông tin mặc định cho phương thức Ủy nhiệm chi
            ConfigComboBankDetail(payVendor, cbbPaymentOrderBankAccountDetailID);
            ConfigComboAccObject(payVendor, cbbAccreditativeAccountingObjectID);
            ConfigComboAccObjectBank(payVendor, cbbAccreditativeAccountingObjectBankAccount);
            //txtAccreditativeAccountingObjectName.DataBindings.Add("Text", payVendor, "AccountingObjectName", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtAccreditativeAccountingObjectAddress.DataBindings.Add("Text", payVendor, "AccountingObjectAddress", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtAccreditativeAccountingObjectBankName.DataBindings.Add("Text", payVendor, "AccountingObjectBankName", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtPaymentOrderBankName.DataBindings.Add("Text", payVendor, "BankName", true, DataSourceUpdateMode.OnPropertyChanged);
            #endregion

            #region Thiết lập thông tin mặc định cho phương thức Séc chuyển khoản
            ConfigComboBankDetail(payVendor, cbbSecCKBankAccountDetailID);
            ConfigComboAccObject(payVendor, cbbSecCKAccountingObjectID);
            ConfigComboAccObjectBank(payVendor, cbbSecCKAccountingObjectBankAccount);
            //txtSecCKAccountingObjectName.DataBindings.Add("Text", payVendor, "AccountingObjectName", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtSecCKAccountingObjectAddress.DataBindings.Add("Text", payVendor, "AccountingObjectAddress", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtSecCKAccountingObjectBankName.DataBindings.Add("Text", payVendor, "AccountingObjectBankName", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtSecCKBankName.DataBindings.Add("Text", payVendor, "BankName", true, DataSourceUpdateMode.OnPropertyChanged);
            #endregion

            #region Thiết lập thông tin mặc định cho phương thức Séc tiền mặt
            ConfigComboBankDetail(payVendor, cbbSecBankAccountDetailID);
            ConfigComboAccObject(payVendor, cbbSecAccountingObjectID);
            //txtSecAccountingObjectName.DataBindings.Add("Text", payVendor, "AccountingObjectName", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtSecBankName.DataBindings.Add("Text", payVendor, "BankName", true, DataSourceUpdateMode.OnPropertyChanged);
            #endregion

            #region Thiết lập thông tin mặc định cho phương thức Thẻ tín dụng
            //ConfigComboCreditCard(payVendor, cbbCreditCardNumber);
            //Cấu hình combobox Thẻ tín dụng
            this.ConfigCombo(payVendor, "CreditCardNumber", Utils.ICreditCardService.GetAll_IsActive(true), cbbCreditCardNumber, picCreditCardType,
                             lblCreditCardType, lblCreditCardOwnerCard);
            ConfigComboAccObject(payVendor, cbbCreditCardAccountingObjectID);
            ConfigComboAccObjectBank(payVendor, cbbCreditCardAccountingObjectBankAccount);
            //txtCreditCardAccountingObjectName.DataBindings.Add("Text", payVendor, "AccountingObjectName", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtCreditCardAccountingObjectAddress.DataBindings.Add("Text", payVendor, "AccountingObjectAddress", true, DataSourceUpdateMode.OnPropertyChanged);
            //txtCreditCardAccountingObjectBankName.DataBindings.Add("Value", payVendor, "AccountingObjectBankName", true, DataSourceUpdateMode.OnPropertyChanged);
            //lblCreditCardOwnerCard.DataBindings.Add("Text", payVendor, "OwnerCard", true, DataSourceUpdateMode.OnPropertyChanged);
            //lblCreditCardType.DataBindings.Add("Text", payVendor, "CreditCardType", true, DataSourceUpdateMode.OnPropertyChanged);
            #endregion

            if (_statusForm != 2 && _statusForm != 3)
            {
                btnApply.Enabled = false;
            }

            p = ultraTabControl1.Location;
            #endregion

            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                //trungnq thêm vào để  làm task 6234

                txtReason.Value = "Trả tiền nhà cung cấp bằng tiền mặt";
                txtReason.Text = "Trả tiền nhà cung cấp bằng tiền mặt";
                payVendor.Reason = "Trả tiền nhà cung cấp bằng tiền mặt";

            }
        }
        #endregion

        #region Utils
        #region Cấu hình các Cbb
        /// <summary>
        /// Nghiệp vụ Add CreditCard
        /// </summary> 
        void AddCreditCardFunction()
        {
            try
            {
                new FCreditCardDetail().ShowDialog(this);
                if (!FCreditCardDetail.IsClose) cbbCreditCardNumber.DataSource = Utils.ICreditCardService.GetAll();
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                return;
            }
        }
        //Cấu hình combobox NCC
        private void ConfigComboAccObject(PayVendor input, UltraCombo cbb)
        {
            cbb.DataSource = accObjectVendor;
            cbb.DisplayMember = "AccountingObjectCode";
            Utils.ConfigGrid(cbb, ConstDatabase.AccountingObject_TableName);
            cbb.DataBindings.Clear();
            cbb.DataBindings.Add("Value", input, "AccountingObjectCode", true, DataSourceUpdateMode.OnPropertyChanged);
        }
        //Cấu hình combobox TK ngân hàng bên gửi
        private void ConfigComboBankDetail(PayVendor input, UltraCombo cbb)
        {
            cbb.DataSource = bankAcc;
            cbb.DisplayMember = "BankAccount";
            Utils.ConfigGrid(cbb, ConstDatabase.BankAccountDetail_TableName);
            cbb.DataBindings.Clear();
            //cbb.DataBindings.Add("Value", input, "BankAccount", true, DataSourceUpdateMode.OnPropertyChanged);
        }
        //Cấu hình combobox TK ngân hàng NCC
        private void ConfigComboAccObjectBank(PayVendor input, UltraCombo cbb)
        {
            cbb.DataSource = accObjectBank;
            cbb.DisplayMember = "BankAccount";
            Utils.ConfigGrid(cbb, ConstDatabase.AccountingObjectBankAccount_TableName);
            cbb.DataBindings.Clear();
            //cbb.DataBindings.Add("Value", input, "AccountingObjectBankAccount", true, DataSourceUpdateMode.OnPropertyChanged);
        }
        #endregion
        //Cấu hình Vùng tiền tệ
        private void ViewCurrency(List<CurrencyPVendor> currency)
        {
            uGridCurrency.DataSource = currency;
            Template template = Utils.GetTemplateInDatabase(null, 250);
            Utils.ConfigGrid(uGridCurrency, "", template.TemplateDetails.Single(x => x.TabIndex == 100).TemplateColumns);
            uGridCurrency.DisplayLayout.Bands[0].Summaries.Clear();
            UltraCombo cbbCurrency = new UltraCombo
            {
                DataSource = Utils.ICurrencyService.GetIsActive(true),
                DisplayMember = "ID"
            };
            Utils.ConfigGrid(cbbCurrency, ConstDatabase.Currency_TableName);
            cbbCurrency.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(cbbCurrency_RowSelected);
            uGridCurrency.DisplayLayout.Bands[0].Columns["CurrencyID"].ValueList = cbbCurrency;
            uGridCurrency.DisplayLayout.Bands[0].Columns["TotalAmountOriginal"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridCurrency.DisplayLayout.Bands[0].Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            //uGridCurrency.Size = new Size(203, 44);
            uGridCurrency.ConfigSizeGridCurrency(this);
            //uGridCurrency.DisplayLayout.Bands[0].Columns["TotalAmountOriginal"].MaskInput = "{double:9.12}";
        }
        //Cấu hình Tab Hóa đơn
        private void ViewTabBill(List<BillPPPayVendorDetail> bill)
        {
            //uGridBill.DataSource = bill;
            uGridBill.SetDataBinding(bill, null);
            UltraGridBand band = uGridBill.DisplayLayout.Bands[0];
            Template temp = Utils.GetTemplateInDatabase(null, 250);
            Utils.ConfigGrid(uGridBill, "", temp.TemplateDetails.Single(x => x.TabIndex == 0).TemplateColumns, true, false);
            uGridBill.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
            uGridBill.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGridBill.DisplayLayout.AutoFitStyle = AutoFitStyle.None;
            uGridBill.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            this.uGridBill.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            UltraGridColumn ugc = band.Columns["CheckColumn"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            band.Columns["CheckColumn"].CellActivation = Activation.AllowEdit;
            //Format định dạng các cột tiền tệ
            band.Columns["TotalDebitOriginal"].FormatNumberic(ConstDatabase.Format_TienVND);
            band.Columns["TotalDebit"].FormatNumberic(ConstDatabase.Format_TienVND);
            band.Columns["DebitAmountOriginal"].FormatNumberic(ConstDatabase.Format_TienVND);
            band.Columns["DebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            band.Columns["AmountOriginal"].FormatNumberic(ConstDatabase.Format_TienVND);
            band.Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            band.Columns["Amount"].CellActivation = Activation.AllowEdit;
            band.Columns["DiscountAmountOriginal"].FormatNumberic(ConstDatabase.Format_TienVND);
            band.Columns["DiscountAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            band.Columns["DiscountRate"].FormatNumberic(ConstDatabase.Format_Coefficient);
            band.Columns["DiscountRate"].MaskInput = @"{double:3.4}";
            band.Columns["DiscountRate"].DefaultCellValue = 0.0;
            band.Columns["RefVoucherExchangeRate"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
            band.Columns["LastExchangeRate"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
            band.Columns["DifferAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            band.Summaries.Clear();
            //add combo
            foreach (UltraGridColumn column in uGridBill.DisplayLayout.Bands[0].Columns)
            {
                //Tài khoản chiết khấu
                if (column.Key.Contains("DiscountAccount"))
                {
                    Utils.ConfigCbbToGrid<PayVendor, Account>(this, uGridBill, column.Key, _IAccountDefaultService.GetAccountDefaultByTypeId(250, "DiscountAccount")
                        , "AccountNumber", "AccountNumber", ConstDatabase.Account_TableName, isThread: false);
                    column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                }
            }
            //UltraCombo cbbDiscountAccount = new UltraCombo,
            //{
            //    DataSource = _IAccountDefaultService.GetAccountDefaultByTypeId(250, "DiscountAccount")/*.Where(c=>c.AccountNumber == "515")*/,
            //    DisplayMember = "AccountNumber"
            //};
            //Utils.ConfigGrid(cbbDiscountAccount, ConstDatabase.Account_TableName);
            //band.Columns["DiscountAccount"].ValueList = cbbDiscountAccount;

            if (band.Summaries.Count != 0) band.Summaries.Clear();
            SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["CheckColumn"]);
            summary.DisplayFormat = "Số dòng = {0:N0}";
            summary.Appearance.FontData.Bold = DefaultableBoolean.True;
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            uGridBill.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False;   //ẩn
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            uGridBill.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            band.SummaryFooterCaption = @"Số dòng dữ liệu: ";
            uGridBill.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;

            //Thêm tổng số ở cột "Tổng Số tổng nợ"
            Utils.AddSumColumn(uGridBill, "TotalDebitOriginal", false, "", ConstDatabase.Format_TienVND, false);

            // thêm tổng số ở cột "Tổng Số tổng nợ QĐ"
            Utils.AddSumColumn(uGridBill, "TotalDebit", false, "", ConstDatabase.Format_TienVND, false);

            // Thêm tổng số ở cột "Tổng số còn nợ"
            Utils.AddSumColumn(uGridBill, "DebitAmountOriginal", false, "", ConstDatabase.Format_TienVND, false);

            // Thêm tổng số ở cột "Tổng số còn nợ QĐ"
            Utils.AddSumColumn(uGridBill, "DebitAmount", false, "", ConstDatabase.Format_TienVND, false);

            // Thêm tổng số ở cột "Tổng số trả"
            Utils.AddSumColumn(uGridBill, "AmountOriginal", false, "", ConstDatabase.Format_TienVND);

            // Thêm tổng số ở cột "Tổng số trả QĐ"
            Utils.AddSumColumn(uGridBill, "Amount", false, "", ConstDatabase.Format_TienVND, false);

            // Thêm tổng số ở cột "Tổng tiền chiết khấu"
            Utils.AddSumColumn(uGridBill, "DiscountAmountOriginal", false, "", ConstDatabase.Format_TienVND, false);

            // Thêm tổng số ở cột "Tổng tiền chiết khấu QĐ"            
            Utils.AddSumColumn(uGridBill, "DiscountAmount", false, "", ConstDatabase.Format_TienVND, false);
        }
        //Cấu hình Tab Hạch toán
        private void ViewTabAccounting(bool load = true)
        {
            UltraGridBand band = uGridAccounting.DisplayLayout.Bands[0];
            Template temp = Utils.GetTemplateInDatabase(null, 250);
            Utils.ConfigGrid(uGridAccounting, "", temp.TemplateDetails.Single(x => x.TabIndex == 1).TemplateColumns, true);
            uGridAccounting.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
            //Format định dạng các cột tiền tệ
            band.Columns["TotalAmountOriginal"].FormatNumberic(ConstDatabase.Format_TienVND);
            band.Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            if (currencyID != "VND" && currencyID != "") // edit by tungnt: hien thi cot tien QD
            {
                band.Columns["TotalAmount"].Hidden = false;
                band.Columns["TotalAmountOriginal"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
            }
            else
            {
                band.Columns["TotalAmount"].Hidden = true;
            }
            //Add combobox
            //============================Tài khoản nợ===========================================//
            UltraCombo cbbDebitAccount = new UltraCombo();
            switch (cbbPaymentMethod.SelectedIndex)
            {
                //Tiền mặt
                case 0:
                    type = 118;
                    TypeGroup = 11;
                    break;
                //Ủy nhiệm chi
                //Ủy nhiệm chi
                case 1:
                    type = 128;
                    TypeGroup = 12;
                    break;
                //Sec CK
                case 2:
                    type = 134;
                    TypeGroup = 13;
                    break;
                //Sec TM
                case 3:
                    type = 144;
                    TypeGroup = 14;
                    break;
                //Thẻ tín dụng
                case 4:
                    type = 174;
                    TypeGroup = 17;
                    break;
            }
            cbbDebitAccount.DataSource = _IAccountDefaultService.GetAccountDefaultByTypeId(type, "DebitAccount");
            cbbDebitAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbDebitAccount, ConstDatabase.Account_TableName);
            band.Columns["DebitAccount"].ValueList = cbbDebitAccount;
            if (cbbDebitAccount.Rows.Count > 0) cbbDebitAccount.Text = (string)cbbDebitAccount.Rows[0].Cells["AccountNumber"].Value;
            //=========================Tài khoản có===================================//
            UltraCombo cbbCreditAccount = new UltraCombo();
            switch (cbbPaymentMethod.SelectedIndex)
            {
                //Tiền mặt
                case 0:
                    type = 118;
                    TypeGroup = 11;
                    break;
                //Ủy nhiệm chi
                //Ủy nhiệm chi
                case 1:
                    type = 128;
                    TypeGroup = 12;
                    break;
                //Sec CK
                case 2:
                    type = 134;
                    TypeGroup = 13;
                    break;
                //Sec TM
                case 3:
                    type = 144;
                    TypeGroup = 14;
                    break;
                //Thẻ tín dụng
                case 4:
                    type = 174;
                    TypeGroup = 17;
                    break;
            }
            cbbCreditAccount.DataSource = _IAccountDefaultService.GetAccountDefaultByTypeId(type, "CreditAccount");
            cbbCreditAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbCreditAccount, ConstDatabase.Account_TableName);
            band.Columns["CreditAccount"].ValueList = cbbCreditAccount;
            if (cbbCreditAccount.Rows.Count > 0) cbbCreditAccount.Value = type == 118 ? (uGridCurrency.Rows[0].Cells["CurrencyID"].Value.ToString() == "VND" ? "1111" : "1112") : (uGridCurrency.Rows[0].Cells["CurrencyID"].Value.ToString() == "VND" ? "1121" : "1122");
            if (uGridAccounting.Rows.Count > 0)
                foreach (UltraGridRow row in uGridAccounting.Rows)
                {
                    row.Cells["CreditAccount"].Value = type == 118 ? (uGridCurrency.Rows[0].Cells["CurrencyID"].Value.ToString() == "VND" ? "1111" : "1112") : (uGridCurrency.Rows[0].Cells["CurrencyID"].Value.ToString() == "VND" ? "1121" : "1122");
                }
            #region listdata
            lstBudgetItemTree = Utils.IBudgetItemService.GetByActive_OrderByTreeIsParentNode(true);
            lstExpenseItemTree = Utils.IExpenseItemService.GetByActive_OrderByTreeIsParentNode(true);
            lstDepartmentTree = Utils.IDepartmentService.GetByActive_OrderByTreeIsParentNode(true);
            lstCostSetTree = Utils.ICostSetService.GetByActive_OrderByTreeIsParentNode(true);
            lstStatisticsCodeTree = Utils.IStatisticsCodeService.GetByActive_OrderByTreeIsParentNode(true);
            #endregion
            foreach (UltraGridColumn column in uGridAccounting.DisplayLayout.Bands[0].Columns)
            {
                //Mục thu/chi
                if (column.Key.Equals("BudgetItemCode"))
                {
                    Utils.ConfigCbbToGrid<PayVendor, BudgetItem>(this, uGridAccounting, column.Key, lstBudgetItemTree, "ID", "BudgetItemCode",
                                   ConstDatabase.BudgetItem_TableName, "ParentID", "IsParentNode", isThread: false);
                    column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                }
                //Khoản mục CP
                if (column.Key.Equals("ExpenseItemCode"))
                {
                    Utils.ConfigCbbToGrid<PayVendor, ExpenseItem>(this, uGridAccounting, column.Key, lstExpenseItemTree, "ID", "ExpenseItemCode",
                                    ConstDatabase.ExpenseItem_TableName, "ParentID", "IsParentNode", isThread: false);
                    column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                }
                //Phòng ban
                if (column.Key.Equals("DepartmentCode"))
                {
                    Utils.ConfigCbbToGrid<PayVendor, Department>(this, uGridAccounting, column.Key, lstDepartmentTree, "ID", "DepartmentCode",
                                    ConstDatabase.Department_TableName, "ParentID", "IsParentNode", isThread: false);
                    column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                }
                //Đối tượng tập hợp CP
                if (column.Key.Equals("CostSetCode"))
                {
                    Utils.ConfigCbbToGrid<PayVendor, CostSet>(this, uGridAccounting, column.Key, lstCostSetTree, "ID", "CostSetCode",
                                    ConstDatabase.CostSet_TableName, "ParentID", "IsParentNode", isThread: false);
                    column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                }
                //Hợp đồng
                if (column.Key.Equals("ContractCode"))
                {
                    Utils.ConfigCbbToGrid<PayVendor, EMContract>(this, uGridAccounting, column.Key, Utils.IEMContractService.GetAll(), "ID", "Code",
                                    ConstDatabase.EMContract_TableName, isThread: false);
                    column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                }
                //Mã thống kê
                if (column.Key.Equals("StatisticsCode"))
                {
                    Utils.ConfigCbbToGrid<PayVendor, StatisticsCode>(this, uGridAccounting, column.Key, lstStatisticsCodeTree, "ID", "StatisticsCode_",
                                    ConstDatabase.StatisticsCode_TableName, "ParentID", "IsParentNode", isThread: false);
                    column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                }
            }
            #region
            ////========================== Mục thu / chi =================================//
            //UltraCombo cbbBudgetItemCode = new UltraCombo();
            //lstBudgetItemTree = Utils.IBudgetItemService.GetByActive_OrderByTreeIsParentNode(true);
            //cbbBudgetItemCode.DataSource = lstBudgetItemTree;
            //cbbBudgetItemCode.DisplayMember = "BudgetItemCode";
            //this.ConfigGrid<BudgetItem>(cbbBudgetItemCode, ConstDatabase.BudgetItem_TableName, "ParentID", "IsParentNode");
            //band.Columns["BudgetItemCode"].ValueList = cbbBudgetItemCode;
            //if (cbbBudgetItemCode.Rows.Count > 0) cbbBudgetItemCode.SelectedRow = cbbBudgetItemCode.Rows[0];
            ////===========================Khoản mục CP==============================//
            //UltraCombo cbbExpenseItemCode = new UltraCombo();
            //lstExpenseItemTree = Utils.IExpenseItemService.GetByActive_OrderByTreeIsParentNode(true);
            //cbbExpenseItemCode.DataSource = lstExpenseItemTree;
            //cbbExpenseItemCode.DisplayMember = "ExpenseItemCode";
            //this.ConfigGrid<ExpenseItem>(cbbExpenseItemCode, ConstDatabase.ExpenseItem_TableName, "ParentID", "IsParentNode");
            //band.Columns["ExpenseItemCode"].ValueList = cbbExpenseItemCode;
            //if (cbbExpenseItemCode.Rows.Count > 0) cbbExpenseItemCode.SelectedRow = cbbExpenseItemCode.Rows[0];
            ////=============================Phòng ban==================================//
            //UltraCombo cbbDepartmentCode = new UltraCombo();
            //lstDepartmentTree = Utils.IDepartmentService.GetByActive_OrderByTreeIsParentNode(true);
            //cbbDepartmentCode.DataSource = lstDepartmentTree;
            //cbbDepartmentCode.DisplayMember = "DepartmentCode";
            //this.ConfigGrid<Department>(cbbDepartmentCode, ConstDatabase.Department_TableName, "ParentID", "IsParentNode");
            //band.Columns["DepartmentCode"].EditorComponent = cbbDepartmentCode;
            //if (cbbDepartmentCode.Rows.Count > 0) cbbDepartmentCode.SelectedRow = cbbDepartmentCode.Rows[0];
            ////==========================ĐT tập hợp CP=================================//
            //UltraCombo cbbCostSetCode = new UltraCombo();
            //lstCostSetTree = Utils.ICostSetService.GetByActive_OrderByTreeIsParentNode(true);
            //cbbCostSetCode.DataSource = lstCostSetTree;
            //cbbCostSetCode.DisplayMember = "CostSetCode";
            //this.ConfigGrid<CostSet>(cbbCostSetCode, ConstDatabase.CostSet_TableName, "ParentID", "IsParentNode");
            //band.Columns["CostSetCode"].ValueList = cbbCostSetCode;
            //if (cbbCostSetCode.Rows.Count > 0) cbbCostSetCode.SelectedRow = cbbCostSetCode.Rows[0];
            ////===============================Hợp đồng===================================//
            //UltraCombo cbbEmContractCode = new UltraCombo
            //{
            //    DataSource = Utils.IEMContractService.GetAll(),
            //    DisplayMember = "Code"
            //};
            //Utils.ConfigGrid(cbbEmContractCode, ConstDatabase.EMContract_TableName);
            //band.Columns["EmContractCode"].ValueList = cbbEmContractCode;
            //if (cbbEmContractCode.Rows.Count > 0) cbbEmContractCode.SelectedRow = cbbEmContractCode.Rows[0];
            ////===============================Mã thống kê=======================================//
            //UltraCombo cbbStatisticsCode = new UltraCombo();
            //lstStatisticsCodeTree = Utils.IStatisticsCodeService.GetByActive_OrderByTreeIsParentNode(true);
            //cbbStatisticsCode.DataSource = lstStatisticsCodeTree;
            //cbbStatisticsCode.DisplayMember = "StatisticsCode";
            //this.ConfigGrid<StatisticsCode>(cbbStatisticsCode, ConstDatabase.StatisticsCode_TableName, "ParentID", "IsParentNode");
            //band.Columns["StatisticsCode"].ValueList = cbbStatisticsCode;
            //if (cbbStatisticsCode.Rows.Count > 0) cbbStatisticsCode.SelectedRow = cbbStatisticsCode.Rows[0];
            ////---------------------------------------------------------------------------------//
            #endregion
            band.Summaries.Clear();
            if (band.Summaries.Count != 0) band.Summaries.Clear();
            SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["Description"]);
            summary.DisplayFormat = "Số dòng = {0:N0}";
            summary.Appearance.FontData.Bold = DefaultableBoolean.True;
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            uGridBill.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False;   //ẩn
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            uGridBill.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            band.SummaryFooterCaption = @"Số dòng dữ liệu: ";
            uGridBill.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
            // Thêm tổng số ở cột "Tổng số tiền"
            Utils.AddSumColumn(uGridAccounting, "TotalAmountOriginal", false, "", ConstDatabase.Format_TienVND);
            //summary = band.Summaries.Add("TongSoTra", SummaryType.Sum, band.Columns["TotalAmountOriginal"]);
            //summary.Appearance.FontData.Bold = DefaultableBoolean.True;
            //summary.Appearance.TextHAlign = HAlign.Right;
            //summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            //summary.DisplayFormat = "{0:N0}";
            //summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            // Thêm tổng số ở cột "Tổng số tiền QĐ"
            Utils.AddSumColumn(uGridAccounting, "TotalAmount", false, "", ConstDatabase.Format_TienVND);
            //summary = band.Summaries.Add("TongSoTraQD", SummaryType.Sum, band.Columns["TotalAmount"]);
            //summary.Appearance.FontData.Bold = DefaultableBoolean.True;
            //summary.Appearance.TextHAlign = HAlign.Right;
            //summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            //summary.DisplayFormat = "{0:N0}";
            //summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
        }
        private void ConfigGridFromAccObj(AccountingObject model)
        {
            if (model == null) return;
            PayVendor temp = _listSelect.FirstOrDefault(p => p.AccountingObjectID == model.ID);
            if (temp != null)
            {
                if (temp.BankAccount == null && payVendor.BankAccount != null)
                {
                    temp.BankAccount = payVendor.BankAccount;
                    temp.BankName = payVendor.BankName;
                }
                payVendor = temp;
            }
            uGridBill.SetDataBinding(payVendor != null ? payVendor.Bill.Where(x => x.DebitAmountOriginal > 0 && x.CurrencyID == (string)uGridCurrency.Rows[0].Cells["CurrencyID"].Value && x.TypeID != 601).OrderByDescending(x => x.Date).ToList() : new List<BillPPPayVendorDetail>(), null);
            uGridAccounting.DataSource = new BindingList<AccountingPPPayVendorDetail>();
        }
        #endregion

        #region Event
        #region Lựa chọn phương thức thanh toán

        private void cbbPaymentMethod_SelectionChanged(object sender, EventArgs e)
        {
            gpMCPayment.Visible = false;
            pnMBTellerPaper_PayOrder.Visible = false;
            pnMBTellerPaper_Sec.Visible = false;
            pnMBTellerPaper_SecCK.Visible = false;
            pnMBCreditCard.Visible = false;
            var combo = (UltraComboEditor)sender;
            switch (combo.SelectedIndex)
            {
                //Tiền mặt
                case 0:
                    type = 118;
                    txtNo.Text = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(ConstFrm.TypeGroupMCPayment));
                    gpMCPayment.Visible = true;
                    if (_statusForm == ConstFrm.optStatusForm.Add)
                    {
                        //trungnq thêm vào để  làm task 6234

                        txtReason.Value = "Trả tiền nhà cung cấp bằng tiền mặt";
                        txtReason.Text = "Trả tiền nhà cung cấp bằng tiền mặt";
                        payVendor.Reason = "Trả tiền nhà cung cấp bằng tiền mặt";

                    }
                    break;
                //Ủy nhiệm chi
                case 1:
                    type = 128;
                    txtNo.Text = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(ConstFrm.TypeGruop_MBTellerPaper));
                    pnMBTellerPaper_PayOrder.Visible = true;
                    //cbbAccreditativeAccountingObjectBankAccount.DataSource = accObjectBank.Where(x => x.AccountingObjectID == payVendor.AccountingObjectID).ToList();
                    //cbbAccreditativeAccountingObjectBankAccount.UpdateData();
                    if (_statusForm == ConstFrm.optStatusForm.Add)
                    {
                        //trungnq thêm vào để  làm task 6234

                        txtPaymentOrderReason.Value = "Trả tiền nhà cung cấp bằng tiền mặt";
                        txtPaymentOrderReason.Text = "Trả tiền nhà cung cấp bằng tiền mặt";
                        payVendor.Reason = "Trả tiền nhà cung cấp bằng tiền mặt";

                    }
                    break;
                //Sec CK
                case 2:
                    type = 134;
                    txtNo.Text = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(13));
                    pnMBTellerPaper_SecCK.Visible = true;
                    //cbbSecCKAccountingObjectBankAccount.DataSource = accObjectBank.Where(x => x.AccountingObjectID == payVendor.AccountingObjectID).ToList();
                    //cbbSecCKAccountingObjectBankAccount.UpdateData();
                    if (_statusForm == ConstFrm.optStatusForm.Add)
                    {
                        //trungnq thêm vào để  làm task 6234

                        txtSecCKReason.Value = "Trả tiền NCC bằng TGNH";
                        txtSecCKReason.Text = "Trả tiền NCC bằng TGNH";
                        payVendor.Reason = "Trả tiền NCC bằng TGNH";

                    }
                    break;
                //Sec TM
                case 3:
                    type = 144;
                    txtNo.Text = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(14));
                    pnMBTellerPaper_Sec.Visible = true;
                    if (_statusForm == ConstFrm.optStatusForm.Add)
                    {
                        //trungnq thêm vào để  làm task 6234

                        txtSecReason.Value = "Trả tiền NCC bằng TGNH";
                        txtSecReason.Text = "Trả tiền NCC bằng TGNH";
                        payVendor.Reason = "Trả tiền NCC bằng TGNH";

                    }
                    break;
                //Thẻ tín dụng
                case 4:
                    type = 174;
                    txtNo.Text = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(ConstFrm.TypeGroup_MBCreditCard));
                    pnMBCreditCard.Visible = true;
                    if (_statusForm == ConstFrm.optStatusForm.Add)
                    {
                        //trungnq thêm vào để  làm task 6234

                        txtCreditCardReason.Value = "Trả tiền NCC bằng TGNH";
                        txtCreditCardReason.Text = "Trả tiền NCC bằng TGNH";
                        payVendor.Reason = "Trả tiền NCC bằng TGNH";

                    }
                    //cbbCreditCardAccountingObjectBankAccount.DataSource = accObjectBank.Where(x => x.AccountingObjectID == payVendor.AccountingObjectID).ToList();
                    //cbbCreditCardAccountingObjectBankAccount.UpdateData();
                    break;
            }

            dicAccountDefault = Utils.IAccountDefaultService.DefaultAccount();
            if (payVendor != null && payVendor.Accounting.Count > 0)
            {
                uGridAccounting.DataSource = new BindingList<AccountingPPPayVendorDetail>(payVendor.Accounting);
                ViewTabAccounting();
                uGridAccounting.Refresh();
                uGridAccounting.Update();
                uGridAccounting.UpdateData();
                uGridAccounting.Refresh();
            }

        }
        /// <summary>
        /// Reset toolbar boxs
        /// </summary>
        private void ResetToolbar()
        {
            gpMCPayment.Visible = false;
            pnMBTellerPaper_PayOrder.Visible = false;
            pnMBTellerPaper_Sec.Visible = false;
            pnMBTellerPaper_SecCK.Visible = false;
            pnMBCreditCard.Visible = false;

            switch (cbbPaymentMethod.SelectedIndex)
            {
                //Tiền mặt
                case 0:
                    type = 118;
                    txtNo.Text = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(ConstFrm.TypeGroupMCPayment));
                    gpMCPayment.Visible = true;
                    break;
                //Ủy nhiệm chi
                case 1:
                    type = 128;
                    txtNo.Text = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(ConstFrm.TypeGruop_MBTellerPaper));
                    pnMBTellerPaper_PayOrder.Visible = true;
                    break;
                //Sec CK
                case 2:
                    type = 134;
                    txtNo.Text = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(13));
                    pnMBTellerPaper_SecCK.Visible = true;
                    break;
                //Sec TM
                case 3:
                    type = 144;
                    txtNo.Text = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(14));
                    pnMBTellerPaper_Sec.Visible = true;
                    break;
                //Thẻ tín dụng
                case 4:
                    type = 174;
                    txtNo.Text = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(ConstFrm.TypeGroup_MBCreditCard));
                    pnMBCreditCard.Visible = true;
                    break;
            }
        }
        #endregion

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region Lựa chọn đối tượng Nhà cung cấp        
        #region Tiền mặt
        private void cbbPaymentAccountingObjectID_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null) return;
            if (e.Row.Selected) isRun = false;
            if (!isRun)
            {
                var model = e.Row.ListObject as AccountingObject;
                if (model != null)
                {
                    //_rollBackAccObj = model.AccountingObjectCode;
                    if (String.IsNullOrEmpty(payVendor.AccountingObjectName) || payVendor.AccountingObjectName.Trim() != model.AccountingObjectName.Trim())
                        payVendor.AccountingObjectName = model.AccountingObjectName;
                    if (String.IsNullOrEmpty(payVendor.AccountingObjectAddress) || payVendor.AccountingObjectAddress.Trim() != model.Address.Trim())
                        payVendor.AccountingObjectAddress = model.Address;
                    txtPaymentAccountingObjectName.Text = payVendor.AccountingObjectName;
                    txtPaymentAccountingObjectAddress.Text = payVendor.AccountingObjectAddress;
                    ConfigGridFromAccObj(model);
                }
            }
            else
                isRun = false;
        }
        #endregion

        #region Thẻ tín dụng
        private void cbbCreditCardAccountingObjectID_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null) return;
            if (e.Row.Selected) isRun = false;
            if (!isRun)
            {
                var model = (AccountingObject)e.Row.ListObject;
                if (String.IsNullOrEmpty(payVendor.AccountingObjectName) || payVendor.AccountingObjectName.Trim() != model.AccountingObjectName.Trim())
                {
                    payVendor.AccountingObjectName = model.AccountingObjectName;
                    txtCreditCardAccountingObjectBankName.Text = null;
                    txtAccreditativeAccountingObjectBankName.Text = null;
                    txtSecCKAccountingObjectBankName.Text = null;
                }
                if (String.IsNullOrEmpty(payVendor.AccountingObjectAddress) || payVendor.AccountingObjectAddress.Trim() != model.Address.Trim())
                    payVendor.AccountingObjectAddress = model.Address;
                txtCreditCardAccountingObjectName.Text = payVendor.AccountingObjectName;
                txtCreditCardAccountingObjectAddress.Text = payVendor.AccountingObjectAddress;
                cbbCreditCardAccountingObjectBankAccount.DataSource = accObjectBank.Where(x => x.AccountingObjectID == model.ID).ToList();
                cbbCreditCardAccountingObjectBankAccount.Update();
                ConfigGridFromAccObj(model);

            }
            else
                isRun = false;
        }
        #endregion

        #region Ủy nhiệm chi
        private void cbbAccreditativeAccountingObjectID_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null) return;
            if (e.Row.Selected) isRun = false;
            if (!isRun)
            {
                var model = (AccountingObject)e.Row.ListObject;
                if (String.IsNullOrEmpty(payVendor.AccountingObjectName) || payVendor.AccountingObjectName.Trim() != model.AccountingObjectName.Trim())
                {
                    payVendor.AccountingObjectName = model.AccountingObjectName;
                    txtCreditCardAccountingObjectBankName.Text = null;
                    txtAccreditativeAccountingObjectBankName.Text = null;
                    txtSecCKAccountingObjectBankName.Text = null;
                }
                if (String.IsNullOrEmpty(payVendor.AccountingObjectAddress) || payVendor.AccountingObjectAddress.Trim() != model.Address.Trim())
                    payVendor.AccountingObjectAddress = model.Address;
                txtAccreditativeAccountingObjectName.Text = payVendor.AccountingObjectName;
                txtAccreditativeAccountingObjectAddress.Text = payVendor.AccountingObjectAddress;
                cbbAccreditativeAccountingObjectBankAccount.DataSource = accObjectBank.Where(x => x.AccountingObjectID == model.ID).ToList();
                cbbAccreditativeAccountingObjectBankAccount.Update();
                ConfigGridFromAccObj(model);
            }
            else
                isRun = false;
        }
        #endregion

        #region Sec tiền mặt
        private void cbbSecAccountingObjectID_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null) return;
            if (e.Row.Selected) isRun = false;
            if (!isRun)
            {
                var model = (AccountingObject)e.Row.ListObject;
                if (String.IsNullOrEmpty(payVendor.AccountingObjectName) || payVendor.AccountingObjectName.Trim() != model.AccountingObjectName.Trim())
                    payVendor.AccountingObjectName = model.AccountingObjectName;
                if (String.IsNullOrEmpty(payVendor.AccountingObjectAddress) || payVendor.AccountingObjectAddress.Trim() != model.Address.Trim())
                    payVendor.AccountingObjectAddress = model.Address;
                txtSecAccountingObjectName.Text = payVendor.AccountingObjectName;
                ConfigGridFromAccObj(model);
            }
            else
                isRun = false;
        }
        #endregion

        #region Sec chuyển khoản
        private void cbbSecCKAccountingObjectID_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null) return;
            if (e.Row.Selected) isRun = false;
            if (!isRun)
            {
                var model = (AccountingObject)e.Row.ListObject;
                if (String.IsNullOrEmpty(payVendor.AccountingObjectName) || payVendor.AccountingObjectName.Trim() != model.AccountingObjectName.Trim())
                {
                    payVendor.AccountingObjectName = model.AccountingObjectName;
                    txtCreditCardAccountingObjectBankName.Text = null;
                    txtAccreditativeAccountingObjectBankName.Text = null;
                    txtSecCKAccountingObjectBankName.Text = null;
                }
                if (String.IsNullOrEmpty(payVendor.AccountingObjectAddress) || payVendor.AccountingObjectAddress.Trim() != model.Address.Trim())
                    payVendor.AccountingObjectAddress = model.Address;
                txtSecCKAccountingObjectName.Text = payVendor.AccountingObjectName;
                txtSecCKAccountingObjectAddress.Text = payVendor.AccountingObjectAddress;
                cbbSecCKAccountingObjectBankAccount.DataSource = accObjectBank.Where(x => x.AccountingObjectID == model.ID).ToList();
                cbbSecCKAccountingObjectBankAccount.Update();
                ConfigGridFromAccObj(model);
            }
            else
                isRun = false;
        }
        #endregion

        #region Lựa chọn TK Ngân hàng của NCC
        //Thẻ tín dụng
        private void cbbCreditCardAccountingObjectBankAccount_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null)
            {
                payVendor.AccountingObjectBankAccount = null;
                payVendor.AccountingObjectBankName = null;
                return;
            }
            if (e.Row != null)
            {
                var model = (AccountingObjectBankAccount)e.Row.ListObject;
                if (String.IsNullOrEmpty(payVendor.AccountingObjectBankName) || payVendor.AccountingObjectBankName.Trim() != model.BankName.Trim())
                {
                    payVendor.AccountingObjectBankAccount = model.ID;
                    payVendor.AccountingObjectBankName = model.BankName;
                }
                //else
                //    payVendor.AccountingObjectBankName = "";
            }
            txtCreditCardAccountingObjectBankName.Text = payVendor.AccountingObjectBankName;
        }

        //Ủy nhiệm chi
        private void cbbAccreditativeAccountingObjectBankAccount_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null)
            {
                payVendor.AccountingObjectBankAccount = null;
                payVendor.AccountingObjectBankName = null;
                return;
            }
            var model = (AccountingObjectBankAccount)e.Row.ListObject;
            if (String.IsNullOrEmpty(payVendor.AccountingObjectBankName) || payVendor.AccountingObjectBankName.Trim() != model.BankName.Trim())
            {
                payVendor.AccountingObjectBankName = model.BankName;
                payVendor.AccountingObjectBankAccount = model.ID;
            }

            //else
            //    payVendor.AccountingObjectBankName = "";
            txtAccreditativeAccountingObjectBankName.Text = payVendor.AccountingObjectBankName;
        }

        //Sec chuyển khoản
        private void cbbSecCKAccountingObjectBankAccount_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null)
            {
                payVendor.AccountingObjectBankAccount = null;
                payVendor.AccountingObjectBankName = null;
                return;
            }
            var model = (AccountingObjectBankAccount)e.Row.ListObject;
            if (String.IsNullOrEmpty(payVendor.AccountingObjectBankName) || payVendor.AccountingObjectBankName.Trim() != model.BankName.Trim())
            {
                payVendor.AccountingObjectBankName = model.BankName;
                payVendor.AccountingObjectBankAccount = model.ID;
            }
            //else
            //    payVendor.AccountingObjectBankName = "";
            txtSecCKAccountingObjectBankName.Text = payVendor.AccountingObjectBankName;
        }
        #endregion

        #endregion

        #region Lựa chọn TK Ngân hàng bên gửi
        //Ủy nhiệm chi
        private void cbbPaymentOrderBankAccountDetailID_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null)
            {
                payVendor.BankName = null;
                payVendor.BankAccount = null;
                return;
            }
            var model = (BankAccountDetail)e.Row.ListObject;
            if (String.IsNullOrEmpty(payVendor.BankName) || payVendor.BankName.Trim() != model.BankName.Trim())
            {
                payVendor.BankName = model.BankName;
                payVendor.BankAccount = model.ID.ToString();
            }
            txtPaymentOrderBankName.Text = payVendor.BankName;
        }
        //Sec TM
        private void cbbSecBankAccountDetailID_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null)
            {
                payVendor.BankName = null;
                payVendor.BankAccount = null;
                return;
            }
            var model = (BankAccountDetail)e.Row.ListObject;
            if (String.IsNullOrEmpty(payVendor.BankName) || payVendor.BankName.Trim() != model.BankName.Trim())
            {
                payVendor.BankName = model.BankName;
                payVendor.BankAccount = model.ID.ToString();
            }
            txtSecBankName.Text = payVendor.BankName;
        }
        //Sec CK
        private void cbbSecCKBankAccountDetailID_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null)
            {
                payVendor.BankName = null;
                payVendor.BankAccount = null;
                return;
            }
            var model = (BankAccountDetail)e.Row.ListObject;
            if (String.IsNullOrEmpty(payVendor.BankName) || payVendor.BankName.Trim() != model.BankName.Trim())
            {
                payVendor.BankName = model.BankName;
                payVendor.BankAccount = model.ID.ToString();
            }
            txtSecCKBankName.Text = payVendor.BankName;
        }

        #endregion

        #region Lựa chọn thẻ tín dụng
        private void cbbCreditCardNumber_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null) return;
            var model = (CreditCard)e.Row.ListObject;
            if (String.IsNullOrEmpty(payVendor.CreditCardNumber) || payVendor.CreditCardNumber.Trim() != model.CreditCardNumber.Trim())
            {
                payVendor.CreditCardNumber = model.CreditCardNumber;
                payVendor.OwnerCard = model.OwnerCard;
                payVendor.CreditCardType = model.CreditCardType;
            }
            //lblCreditCardOwnerCard.Text = payVendor.OwnerCard;
            //lblCreditCardType.Text = payVendor.CreditCardType;

        }
        #endregion

        #region Lựa chọn loại tiền tệ
        private void uGridCurrency_CellChange(object sender, CellEventArgs e)
        {
            //uGridCurrency.UpdateData();
            //var need = Utils.GetValueFromTextAndNotNull(uGridCurrency.Rows[0].Cells["TotalAmountOriginal"]);
            //decimal total = 0;
            if (e.Cell.Column.Key == "CurrencyID")
            {
                UltraGridBand uGridCurrencyBand = uGridCurrency.DisplayLayout.Bands[0];
                UltraGridBand uGridBillBand = uGridBill.DisplayLayout.Bands[0];
                currencyID = e.Cell.Text;
                if (e.Cell.Text == "VND")
                {
                    uGridCurrencyBand.Columns["TotalAmount"].Hidden = true;
                    uGridCurrencyBand.Columns["ExchangeRate"].Hidden = true;
                    uGridCurrency.ConfigSizeGridCurrency(this, false);
                    uGridBillBand.Columns["DifferAmount"].Hidden = true;
                    uGridAccounting.DisplayLayout.Bands[0].Columns["TotalAmount"].Hidden = true;

                    uGridBill.DisplayLayout.Bands[0].Summaries.Clear();
                    Utils.AddSumColumn(uGridBill, "TotalDebitOriginal", false, "", ConstDatabase.Format_TienVND, false);
                    Utils.AddSumColumn(uGridBill, "TotalDebit", false, "", ConstDatabase.Format_TienVND, false);
                    Utils.AddSumColumn(uGridBill, "DebitAmountOriginal", false, "", ConstDatabase.Format_TienVND, false);
                    Utils.AddSumColumn(uGridBill, "DebitAmount", false, "", ConstDatabase.Format_TienVND, false);
                    Utils.AddSumColumn(uGridBill, "AmountOriginal", false, "", ConstDatabase.Format_TienVND);
                    Utils.AddSumColumn(uGridBill, "Amount", false, "", ConstDatabase.Format_TienVND, false);
                    Utils.AddSumColumn(uGridBill, "DiscountAmountOriginal", false, "", ConstDatabase.Format_TienVND, false);
                    Utils.AddSumColumn(uGridBill, "DiscountAmount", false, "", ConstDatabase.Format_TienVND, false);

                    uGridAccounting.DisplayLayout.Bands[0].Summaries.Clear();
                    Utils.AddSumColumn(uGridAccounting, "TotalAmountOriginal", false, "", ConstDatabase.Format_TienVND, false);
                    Utils.AddSumColumn(uGridAccounting, "TotalAmount", false, "", ConstDatabase.Format_TienVND, false);
                    //
                }
                else
                {
                    uGridCurrencyBand.Columns["TotalAmount"].Header.Caption = "Số tiền QĐ";
                    uGridCurrencyBand.Columns["ExchangeRate"].Hidden = !isBQCK;
                    uGridCurrencyBand.Columns["TotalAmount"].Hidden = false;
                    uGridCurrency.ConfigSizeGridCurrency(this, false);
                    uGridBillBand.Columns["DifferAmount"].Hidden = !isBQCK;
                    uGridAccounting.DisplayLayout.Bands[0].Columns["TotalAmount"].Hidden = false;

                    uGridBill.DisplayLayout.Bands[0].Summaries.Clear();
                    Utils.AddSumColumn(uGridBill, "TotalDebitOriginal", false, "", ConstDatabase.Format_ForeignCurrency, false);
                    Utils.AddSumColumn(uGridBill, "TotalDebit", false, "", ConstDatabase.Format_TienVND, false);
                    Utils.AddSumColumn(uGridBill, "DebitAmountOriginal", false, "", ConstDatabase.Format_ForeignCurrency, false);
                    Utils.AddSumColumn(uGridBill, "DebitAmount", false, "", ConstDatabase.Format_TienVND, false);
                    Utils.AddSumColumn(uGridBill, "AmountOriginal", false, "", ConstDatabase.Format_ForeignCurrency);
                    Utils.AddSumColumn(uGridBill, "Amount", false, "", ConstDatabase.Format_TienVND, false);
                    Utils.AddSumColumn(uGridBill, "DiscountAmountOriginal", false, "", ConstDatabase.Format_ForeignCurrency, false);
                    Utils.AddSumColumn(uGridBill, "DiscountAmount", false, "", ConstDatabase.Format_TienVND, false);

                    uGridAccounting.DisplayLayout.Bands[0].Summaries.Clear();
                    Utils.AddSumColumn(uGridAccounting, "TotalAmountOriginal", false, "", ConstDatabase.Format_ForeignCurrency, false);
                    Utils.AddSumColumn(uGridAccounting, "TotalAmount", false, "", ConstDatabase.Format_TienVND, false);
                }
                uGridBillBand.Columns["TotalDebit"].Hidden = e.Cell.Text == "VND";
                uGridBillBand.Columns["DebitAmount"].Hidden = e.Cell.Text == "VND";
                uGridBillBand.Columns["Amount"].Hidden = e.Cell.Text == "VND";
                uGridBillBand.Columns["DiscountAmount"].Hidden = e.Cell.Text == "VND";
                uGridBillBand.Columns["RefVoucherExchangeRate"].Hidden = e.Cell.Text == "VND";
                uGridBillBand.Columns["LastExchangeRate"].Hidden = e.Cell.Text == "VND";

            }


        }

        //Lấy tỷ giá
        private void cbbCurrency_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row != null)
            {
                var model = (Currency)e.Row.ListObject;
                uGridCurrency.Rows[0].Cells["ExchangeRate"].Value = model.ExchangeRate;
                //var value = model.ExchangeRate;
                //if (value != null)
                //{
                //    uGridCurrency.Rows[0].Cells["TotalAmount"].Value = (value * (decimal)uGridCurrency.Rows[0].Cells["TotalAmountOriginal"].Value);
                //}

                //foreach (UltraGridRow ugr in uGridBill.Rows)
                //{
                //    var o = uGridCurrency.Rows[0].Cells["ExchangeRate"].Value;
                //    if (o != null)
                //        ugr.Cells["Amount"].Value = Convert.ToDecimal(o.ToString()) * Convert.ToDecimal(ugr.Cells["AmountOriginal"].Value.ToString());
                //}
                foreach (var x in payVendor.Bill) x.CheckColumn = false;
                uGridBill.SetDataBinding(payVendor != null ? payVendor.Bill.Where(x => x.DebitAmountOriginal > 0 && x.CurrencyID == model.ID && x.TypeID != 601).OrderByDescending(x => x.Date).ToList() : new List<BillPPPayVendorDetail>(), null);
                uGridAccounting.DataSource = new BindingList<AccountingPPPayVendorDetail>();
                uGridBill.DisplayLayout.Bands[0].Columns["TotalDebitOriginal"].FormatNumberic(model.ID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
                uGridBill.DisplayLayout.Bands[0].Columns["DebitAmountOriginal"].FormatNumberic(model.ID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
                uGridBill.DisplayLayout.Bands[0].Columns["AmountOriginal"].FormatNumberic(model.ID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
                uGridBill.DisplayLayout.Bands[0].Columns["DiscountAmountOriginal"].FormatNumberic(model.ID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
                uGridBill.DisplayLayout.Bands[0].Columns["RefVoucherExchangeRate"].Hidden = model.ID == "VND";
                uGridBill.DisplayLayout.Bands[0].Columns["LastExchangeRate"].Hidden = model.ID == "VND";
                if (model.ID == "VND")
                {
                    uGridCurrency.DisplayLayout.Bands[0].Columns["ExchangeRate"].Hidden = true;
                    uGridBill.DisplayLayout.Bands[0].Columns["DifferAmount"].Hidden = true;
                }
                else
                {
                    uGridCurrency.DisplayLayout.Bands[0].Columns["ExchangeRate"].Hidden = !isBQCK;
                    uGridBill.DisplayLayout.Bands[0].Columns["DifferAmount"].Hidden = !isBQCK;
                }
                uGridAccounting.DisplayLayout.Bands[0].Columns["TotalAmountOriginal"].FormatNumberic(model.ID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
                uGridCurrency.DisplayLayout.Bands[0].Columns["TotalAmountOriginal"].FormatNumberic(model.ID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
                uGridBill.UpdateData();
                uGridBill.UpdateDataGrid();
                uGridCurrency.UpdateData();
            }
        }

        #endregion

        #region Chọn chứng từ hóa đơn
        private void CheckDiscount(UltraGridCell cell)
        {
            var rate = Utils.GetValueFromTextAndNotNull(cell);
            p = ultraTabControl1.Location;
            if (rate > 100)
                uGridBill.NotificationCell(cell, resSystem.MSG_System_39);
            else
            {
                if (cell.Row.Cells["AmountOriginal"].Value != null)
                {
                    if (currencyID == "VND")
                        cell.Row.Cells["DiscountAmountOriginal"].Value = Math.Round(((decimal)cell.Row.Cells["AmountOriginal"].Value * rate) / 100, LamTron, MidpointRounding.AwayFromZero);
                    else
                        cell.Row.Cells["DiscountAmountOriginal"].Value = ((decimal)cell.Row.Cells["AmountOriginal"].Value * rate) / 100;

                }
                if (cell.Row.Cells["Amount"].Value != null)
                    cell.Row.Cells["DiscountAmount"].Value = Math.Round((decimal)cell.Row.Cells["Amount"].Value * rate / 100, LamTron, MidpointRounding.AwayFromZero);
                uGridBill.RemoveNotificationCell(cell);
            }
        }
        private void uGridBill_AfterExitEditMode(object sender, EventArgs e)
        {
            AfterExitEditModeBill();
        }
        public void AfterExitEditModeBill()
        {
            UltraGridCell cell = uGridBill.ActiveCell;
            switch (cell.Column.Key)
            {
                case "DiscountRate":
                    CheckDiscount(cell);
                    uGridBill.Rows.Refresh(RefreshRow.FireInitializeRow);
                    break;
                case "AmountOriginal":
                    if (!cell.Row.Cells["AmountOriginal"].IsFilterRowCell)
                    {
                        if (cell.Row.Cells["AmountOriginal"].Value != null)
                        {
                            if ((decimal)cell.Row.Cells["AmountOriginal"].Value > (decimal)cell.Row.Cells["DebitAmountOriginal"].Value)
                            {
                                MSG.Warning("Số trả không được lớn hơn số còn nợ");
                            }
                            if (Utils.GetValueFromTextAndNotNull(cell) > 0)
                            {
                                if (Utils.GetValueFromTextAndNotNull(cell) > Utils.GetValueFromTextAndNotNull(cell.Row.Cells["DebitAmountOriginal"]))
                                    uGridBill.NotificationCell(cell, "Số trả phải <= Số còn nợ");
                                else
                                    uGridBill.RemoveNotificationCell(cell);
                                CheckDiscount(cell.Row.Cells["DiscountRate"]);
                                cell.Row.Cells["CheckColumn"].Value = true;

                                //cell.Row.Cells["Amount"].Value = Math.Round((decimal)cell.Row.Cells["AmountOriginal"].Value * (decimal)uGridCurrency.Rows[0].Cells["ExchangeRate"].Value, LamTron, MidpointRounding.AwayFromZero); ;
                                uGridBill.Rows.Refresh(RefreshRow.FireInitializeRow);
                            }
                            else
                            {
                                cell.Row.Cells["CheckColumn"].Value = false;
                                uGridBill.Rows.Refresh(RefreshRow.FireInitializeRow);
                            }
                        }
                    }

                    break;
                //case "Amount":
                //    uGridAccounting.Rows[0].Cells["TotalAmount"].Value = Convert.ToDecimal(uGridBill.Rows.SummaryValues["AmountSum"].Value.ToString()) - Convert.ToDecimal(uGridBill.Rows.SummaryValues["DiscountAmountSum"].Value.ToString());
                //    break;
                case "DiscountAccount":
                    if (!cell.Row.Cells["DiscountAccount"].IsFilterRowCell)
                    {
                        //edit by tungnt: không group các chứng từ tích chọn để trả tiền mà để mỗi chứng từ sinh ra 1 dòng bên tab hạch toán
                        List<AccountingPPPayVendorDetail> lstaccPayVender = (from c in payVendor.Bill.Where(d => d.CheckColumn).ToList()
                                                                                 //group c by c.Account
                                                                                 //into g
                                                                             select new AccountingPPPayVendorDetail()
                                                                             {
                                                                                 DebitAccount = c.Account,
                                                                                 CreditAccount = type == 118 ? (uGridCurrency.Rows[0].Cells["CurrencyID"].Value.ToString() == "VND" ? "1111" : "1112") : (uGridCurrency.Rows[0].Cells["CurrencyID"].Value.ToString() == "VND" ? "1121" : "1122")/*dicAccountDefault.FirstOrDefault(p => p.Key.StartsWith(string.Format("{0};{1}", type, "CreditAccount"))).Value*/,
                                                                                 TotalAmount = Math.Round(((c.AmountOriginal ?? 0M) - c.DiscountAmountOriginal) * (decimal)uGridCurrency.Rows[0].Cells["ExchangeRate"].Value, LamTron, MidpointRounding.AwayFromZero),
                                                                                 TotalAmountOriginal = (c.AmountOriginal ?? 0M) - c.DiscountAmountOriginal,
                                                                                 AccountingObjectCode = payVendor.AccountingObjectCode,
                                                                                 Description = "Trả tiền nhà cung cấp " + payVendor.AccountingObjectName
                                                                             }).ToList();
                        List<AccountingPPPayVendorDetail> lstaccPayVenderdis = (from c in payVendor.Bill.Where(d => d.CheckColumn && d.DiscountAccount != null).ToList()
                                                                                    //group c by new { c.Account, c.DiscountAccount } into g
                                                                                select new AccountingPPPayVendorDetail()
                                                                                {
                                                                                    DebitAccount = c.Account,
                                                                                    CreditAccount = c.DiscountAccount,
                                                                                    TotalAmount = c.DiscountAmount,
                                                                                    TotalAmountOriginal = c.DiscountAmountOriginal,
                                                                                    AccountingObjectCode = payVendor.AccountingObjectCode,
                                                                                    Description = "Chiết khấu thanh toán được hưởng "
                                                                                }).ToList();
                        payVendor.Accounting.Clear();
                        payVendor.Accounting.AddRange(lstaccPayVender);
                        payVendor.Accounting.AddRange(lstaccPayVenderdis);
                        if (isBQCK)
                        {
                            List<AccountingPPPayVendorDetail> lstClEx1 = (from c in payVendor.Bill.Where(d => d.CheckColumn && d.DifferAmount > 0).ToList()
                                                                              //group c by c.Account
                                                                              //   into g
                                                                          select new AccountingPPPayVendorDetail()
                                                                          {
                                                                              DebitAccount = Utils.ListSystemOption.FirstOrDefault(x => x.Code == "TCKHAC_TKCLechLo").Data,
                                                                              CreditAccount = type == 118 ? ("1112") : ("1122"),
                                                                              TotalAmount = c.DifferAmount,
                                                                              TotalAmountOriginal = 0,
                                                                              AccountingObjectCode = payVendor.AccountingObjectCode,
                                                                              Description = "Xử lý lỗ chênh lệch tỷ giá"
                                                                          }).ToList();
                            List<AccountingPPPayVendorDetail> lstClEx2 = (from c in payVendor.Bill.Where(d => d.CheckColumn && d.DifferAmount < 0).ToList()
                                                                              //group c by c.Account
                                                                              //       into g
                                                                          select new AccountingPPPayVendorDetail()
                                                                          {
                                                                              DebitAccount = c.Account,
                                                                              CreditAccount = Utils.ListSystemOption.FirstOrDefault(x => x.Code == "TCKHAC_TKCLechLai").Data,
                                                                              TotalAmount = Math.Abs(c.DifferAmount),
                                                                              TotalAmountOriginal = 0,
                                                                              AccountingObjectCode = payVendor.AccountingObjectCode,
                                                                              Description = "Xử lý lãi chênh lệch tỷ giá"
                                                                          }).ToList();
                            if (lstClEx1.Count > 0) payVendor.Accounting.AddRange(lstClEx1);
                            if (lstClEx2.Count > 0) payVendor.Accounting.AddRange(lstClEx2);
                        }
                        uGridAccounting.DataSource = new BindingList<AccountingPPPayVendorDetail>(payVendor.Accounting);
                        UltraGridBand band = uGridAccounting.DisplayLayout.Bands[0];
                        UltraCombo cbbCreditAccount = new UltraCombo();
                        cbbCreditAccount.DataSource = _IAccountDefaultService.GetAccountDefaultByTypeId(type, "CreditAccount");
                        cbbCreditAccount.DisplayMember = "AccountNumber";
                        Utils.ConfigGrid(cbbCreditAccount, ConstDatabase.Account_TableName);
                        band.Columns["CreditAccount"].ValueList = cbbCreditAccount;
                        if (cbbCreditAccount.Rows.Count > 0) cbbCreditAccount.SelectedRow = cbbCreditAccount.Rows[0];
                        uGridAccounting.UpdateData();
                        uGridAccounting.Refresh();
                    }
                    break;
            }
        }
        private void uGridBill_CellChange(object sender, CellEventArgs e)
        {
            if (!e.Cell.Row.Cells[e.Cell.Column.Key].IsFilterRowCell)
            {
                switch (e.Cell.Column.Key)
                {
                    case "CheckColumn":
                        if (Boolean.Parse(e.Cell.Row.Cells["CheckColumn"].Text))
                        {
                            e.Cell.Row.Cells["AmountOriginal"].Value = e.Cell.Row.Cells["DebitAmountOriginal"].Value.ToString();
                            uGridCurrency.Rows[0].Cells["TotalAmountOriginal"].Value = Convert.ToDecimal(uGridBill.Rows.SummaryValues["AmountOriginalSum"].Value.ToString()) - Convert.ToDecimal(uGridBill.Rows.SummaryValues["DiscountAmountOriginalSum"].Value.ToString());
                            uGridCurrency.Rows[0].Cells["TotalAmount"].Value = Convert.ToDecimal(uGridBill.Rows.SummaryValues["AmountSum"].Value.ToString()) - Convert.ToDecimal(uGridBill.Rows.SummaryValues["DiscountAmountSum"].Value.ToString());
                            //e.Cell.Row.Cells["Amount"].Value = (decimal)e.Cell.Row.Cells["AmountOriginal"].Value * (decimal)uGridCurrency.Rows[0].Cells["ExchangeRate"].Value;
                            uGridBill.Rows.Refresh(RefreshRow.FireInitializeRow);
                        }
                        else
                        {
                            e.Cell.Row.Cells["AmountOriginal"].Value = 0M;
                            e.Cell.Row.Cells["Amount"].Value = 0M;
                            e.Cell.Row.Cells["DiscountRate"].Value = 0;
                            uGridBill.Rows.Refresh(RefreshRow.FireInitializeRow);
                        }
                        CheckDiscount(e.Cell.Row.Cells["DiscountRate"]);
                        uGridBill.Rows.Refresh(RefreshRow.FireInitializeRow);
                        break;
                    default:
                        break;
                }
            }
        }
        private void uGridBill_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (!e.Cell.Row.Cells[e.Cell.Column.Key].IsFilterRowCell)
            {
                switch (e.Cell.Column.Key)
                {
                    case "DiscountRate":
                        CheckDiscount(e.Cell);
                        uGridBill.Rows.Refresh(RefreshRow.FireInitializeRow);
                        break;
                    case "AmountOriginal":
                        if (e.Cell.Value != null)
                        {
                            if (Utils.GetValueFromTextAndNotNull(e.Cell) > 0)
                            {
                                if (Utils.GetValueFromTextAndNotNull(e.Cell) > Utils.GetValueFromTextAndNotNull(e.Cell.Row.Cells["DebitAmountOriginal"]))
                                    uGridBill.NotificationCell(e.Cell, "Số trả phải <= Số còn nợ");
                                else
                                    uGridBill.RemoveNotificationCell(e.Cell);
                                CheckDiscount(e.Cell.Row.Cells["DiscountRate"]);
                                e.Cell.Row.Cells["CheckColumn"].Value = true;
                                uGridCurrency.Rows[0].Cells["TotalAmountOriginal"].Value = Convert.ToDecimal(uGridBill.Rows.SummaryValues["AmountOriginalSum"].Value.ToString()) - Convert.ToDecimal(uGridBill.Rows.SummaryValues["DiscountAmountOriginalSum"].Value.ToString());
                                //e.Cell.Row.Cells["Amount"].Value = Math.Round((decimal)e.Cell.Row.Cells["AmountOriginal"].Value * (decimal)uGridCurrency.Rows[0].Cells["ExchangeRate"].Value, LamTron, MidpointRounding.AwayFromZero);
                                uGridBill.Rows.Refresh(RefreshRow.FireInitializeRow);
                            }
                            else
                            {
                                e.Cell.Row.Cells["CheckColumn"].Value = false;
                                uGridCurrency.Rows[0].Cells["TotalAmountOriginal"].Value = Convert.ToDecimal(uGridBill.Rows.SummaryValues["AmountOriginalSum"].Value.ToString()) - Convert.ToDecimal(uGridBill.Rows.SummaryValues["DiscountAmountOriginalSum"].Value.ToString());
                                uGridBill.Rows.Refresh(RefreshRow.FireInitializeRow);
                            }
                        }
                        else
                        {
                            e.Cell.Row.Cells["CheckColumn"].Value = false;
                            uGridCurrency.Rows[0].Cells["TotalAmountOriginal"].Value = Convert.ToDecimal(uGridBill.Rows.SummaryValues["AmountOriginalSum"].Value.ToString()) - Convert.ToDecimal(uGridBill.Rows.SummaryValues["DiscountAmountOriginalSum"].Value.ToString());
                            uGridBill.Rows.Refresh(RefreshRow.FireInitializeRow);
                        }
                        break;
                    case "Amount":
                        uGridCurrency.Rows[0].Cells["TotalAmount"].Value = Convert.ToDecimal(uGridBill.Rows.SummaryValues["AmountSum"].Value.ToString()) - Convert.ToDecimal(uGridBill.Rows.SummaryValues["DiscountAmountSum"].Value.ToString());
                        uGridBill.Rows.Refresh(RefreshRow.FireInitializeRow);
                        break;
                    case "DiscountAmountOriginal":
                        uGridCurrency.Rows[0].Cells["TotalAmountOriginal"].Value = Convert.ToDecimal(uGridBill.Rows.SummaryValues["AmountOriginalSum"].Value.ToString()) - Convert.ToDecimal(uGridBill.Rows.SummaryValues["DiscountAmountOriginalSum"].Value.ToString());
                        uGridBill.Rows.Refresh(RefreshRow.FireInitializeRow);
                        break;
                    case "DiscountAmount":
                        uGridCurrency.Rows[0].Cells["TotalAmount"].Value = Convert.ToDecimal(uGridBill.Rows.SummaryValues["AmountSum"].Value.ToString()) - Convert.ToDecimal(uGridBill.Rows.SummaryValues["DiscountAmountSum"].Value.ToString());
                        uGridBill.Rows.Refresh(RefreshRow.FireInitializeRow);
                        break;
                    default:
                        break;
                }
            }
        }
        private void uGridBill_AfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {
            //uGridBill.DisplayLayout.Bands[0].Columns["CheckColumn"].GetHeaderCheckedState(uGridBill.ActiveRow.ChildBands[0].Rows);            
            if (e.Column.Key == "CheckColumn")
            {
                UltraGridColumn col = this.uGridBill.DisplayLayout.Bands[0].Columns["CheckColumn"];
                if (!_statusTemp)
                {
                    switch (col.GetHeaderCheckedState(e.Rows))
                    {

                        case CheckState.Checked:
                            foreach (UltraGridRow ugr in uGridBill.Rows)
                            {
                                if (!ugr.Cells["AmountOriginal"].IsFilterRowCell && (decimal)ugr.Cells["AmountOriginal"].Value == 0)
                                {
                                    ugr.Cells["AmountOriginal"].Value = ugr.Cells["DebitAmountOriginal"].Value;
                                }
                            }
                            uGridBill.Rows.Refresh(RefreshRow.FireInitializeRow);
                            if (uGridBill.DisplayLayout.Bands[0].Summaries.Exists("AmountOriginalSum"))
                            {
                                uGridBill.DisplayLayout.Bands[0].Summaries["AmountOriginalSum"].Refresh();
                            }
                            if (uGridBill.DisplayLayout.Bands[0].Summaries.Exists("AmountSum"))
                            {
                                uGridBill.DisplayLayout.Bands[0].Summaries["AmountSum"].Refresh();
                            }
                            uGridCurrency.Rows[0].Cells["TotalAmountOriginal"].Value = Convert.ToDecimal(uGridBill.Rows.SummaryValues["AmountOriginalSum"].Value.ToString()) - Convert.ToDecimal(uGridBill.Rows.SummaryValues["DiscountAmountOriginalSum"].Value.ToString());
                            uGridCurrency.Rows[0].Cells["TotalAmount"].Value = Convert.ToDecimal(uGridBill.Rows.SummaryValues["AmountSum"].Value.ToString()) - Convert.ToDecimal(uGridBill.Rows.SummaryValues["DiscountAmountSum"].Value.ToString());
                            break;
                        case CheckState.Unchecked:
                            foreach (UltraGridRow ugr in uGridBill.Rows)
                            {
                                if (!ugr.Cells["AmountOriginal"].IsFilterRowCell)
                                {
                                    ugr.Cells["AmountOriginal"].Value = 0M;
                                    ugr.Cells["Amount"].Value = 0M;
                                    ugr.Cells["DiscountRate"].Value = 0;

                                }
                            }
                            uGridCurrency.Rows[0].Cells["TotalAmountOriginal"].Value = 0;
                            uGridCurrency.Rows[0].Cells["TotalAmount"].Value = 0;
                            uGridBill.Rows.Refresh(RefreshRow.FireInitializeRow);
                            uGridCurrency.Rows.Refresh(RefreshRow.FireInitializeRow);
                            break;
                        default:
                            break;

                    }
                }
                else
                    _statusTemp = false;
            }
        }
        //Sinh chứng từ 
        private void uGridBill_SummaryValueChanged(object sender, SummaryValueChangedEventArgs e)
        {
            //edit by tungnt: không group các chứng từ tích chọn để trả tiền mà để mỗi chứng từ sinh ra 1 dòng bên tab hạch toán
            if (payVendor == null) return;
            if (Convert.ToDecimal(uGridBill.Rows.SummaryValues["AmountOriginalSum"].Value.ToString()) != 0)
            {
                //uGridCurrency.Rows[0].Cells["TotalAmountOriginal"].Value = Convert.ToDecimal(uGridBill.Rows.SummaryValues["AmountOriginalSum"].Value.ToString()) - Convert.ToDecimal(uGridBill.Rows.SummaryValues["DiscountAmountOriginalSum"].Value.ToString());
                //uGridCurrency.Rows[0].Cells["TotalAmount"].Value = Convert.ToDecimal(uGridBill.Rows.SummaryValues["AmountSum"].Value.ToString()) - Convert.ToDecimal(uGridBill.Rows.SummaryValues["DiscountAmountSum"].Value.ToString());
                dicAccountDefault = Utils.IAccountDefaultService.DefaultAccount();
                List<AccountingPPPayVendorDetail> lstaccPayVender = (from c in payVendor.Bill.Where(d => d.CheckColumn).ToList()
                                                                         //group c by c.Account
                                                                         //into g
                                                                     select new AccountingPPPayVendorDetail()
                                                                     {
                                                                         DebitAccount = c.Account,
                                                                         CreditAccount = type == 118 ? (uGridCurrency.Rows[0].Cells["CurrencyID"].Value.ToString() == "VND" ? "1111" : "1112") : (uGridCurrency.Rows[0].Cells["CurrencyID"].Value.ToString() == "VND" ? "1121" : "1122")/*dicAccountDefault.FirstOrDefault(p => p.Key.StartsWith(string.Format("{0};{1}", type, "CreditAccount"))).Value*/,
                                                                         TotalAmount = Math.Round(((c.AmountOriginal ?? 0M) - c.DiscountAmountOriginal) * (decimal)uGridCurrency.Rows[0].Cells["ExchangeRate"].Value, LamTron, MidpointRounding.AwayFromZero),
                                                                         TotalAmountOriginal = (c.AmountOriginal ?? 0M) - c.DiscountAmountOriginal,
                                                                         AccountingObjectCode = payVendor.AccountingObjectCode,
                                                                         Description = "Trả tiền nhà cung cấp " + payVendor.AccountingObjectName
                                                                     }).ToList();
                List<AccountingPPPayVendorDetail> lstaccPayVenderdis = (from c in payVendor.Bill.Where(d => d.CheckColumn && d.DiscountAccount != null).ToList()
                                                                            //group c by new { c.Account, c.DiscountAccount } into g
                                                                        select new AccountingPPPayVendorDetail()
                                                                        {
                                                                            DebitAccount = c.Account,
                                                                            CreditAccount = c.DiscountAccount/*dicAccountDefault.FirstOrDefault(p => p.Key.StartsWith(string.Format("{0};{1}", type, "CreditAccount"))).Value*/,
                                                                            TotalAmount = c.DiscountAmount,
                                                                            TotalAmountOriginal = c.DiscountAmountOriginal,
                                                                            AccountingObjectCode = payVendor.AccountingObjectCode,
                                                                            Description = "Chiết khấu thanh toán được hưởng "
                                                                        }).ToList();
                payVendor.Accounting.Clear();
                payVendor.Accounting.AddRange(lstaccPayVender);
                payVendor.Accounting.AddRange(lstaccPayVenderdis);
                if (isBQCK)
                {
                    List<AccountingPPPayVendorDetail> lstClEx1 = (from c in payVendor.Bill.Where(d => d.CheckColumn && d.DifferAmount > 0).ToList()
                                                                      //group c by c.Account
                                                                      //   into g
                                                                  select new AccountingPPPayVendorDetail()
                                                                  {
                                                                      DebitAccount = Utils.ListSystemOption.FirstOrDefault(x => x.Code == "TCKHAC_TKCLechLo").Data,
                                                                      CreditAccount = type == 118 ? ("1112") : ("1122"),
                                                                      TotalAmount = c.DifferAmount,
                                                                      TotalAmountOriginal = 0,
                                                                      AccountingObjectCode = payVendor.AccountingObjectCode,
                                                                      Description = "Xử lý lỗ chênh lệch tỷ giá"
                                                                  }).ToList();
                    List<AccountingPPPayVendorDetail> lstClEx2 = (from c in payVendor.Bill.Where(d => d.CheckColumn && d.DifferAmount < 0).ToList()
                                                                      //group c by c.Account
                                                                      //       into g
                                                                  select new AccountingPPPayVendorDetail()
                                                                  {
                                                                      DebitAccount = c.Account,
                                                                      CreditAccount = Utils.ListSystemOption.FirstOrDefault(x => x.Code == "TCKHAC_TKCLechLai").Data,
                                                                      TotalAmount = Math.Abs(c.DifferAmount),
                                                                      TotalAmountOriginal = 0,
                                                                      AccountingObjectCode = payVendor.AccountingObjectCode,
                                                                      Description = "Xử lý lãi chênh lệch tỷ giá"
                                                                  }).ToList();
                    if (lstClEx1.Count > 0) payVendor.Accounting.AddRange(lstClEx1);
                    if (lstClEx2.Count > 0) payVendor.Accounting.AddRange(lstClEx2);
                }
                uGridAccounting.DataSource = new BindingList<AccountingPPPayVendorDetail>(payVendor.Accounting);
                UltraGridBand band = uGridAccounting.DisplayLayout.Bands[0];
                UltraCombo cbbCreditAccount = new UltraCombo();
                cbbCreditAccount.DataSource = _IAccountDefaultService.GetAccountDefaultByTypeId(type, "CreditAccount");
                cbbCreditAccount.DisplayMember = "AccountNumber";
                Utils.ConfigGrid(cbbCreditAccount, ConstDatabase.Account_TableName);
                band.Columns["CreditAccount"].ValueList = cbbCreditAccount;
                if (cbbCreditAccount.Rows.Count > 0) cbbCreditAccount.SelectedRow = cbbCreditAccount.Rows[0];
                uGridAccounting.UpdateData();
                uGridAccounting.Refresh();

                uGridCurrency.Rows.Refresh(RefreshRow.FireInitializeRow);
                uGridCurrency.UpdateData();
                uGridCurrency.Refresh();
            }
            else if (payVendor.Bill.All(c => c.CheckColumn == false))
            {
                if (payVendor != null)
                {
                    payVendor.Accounting.Clear();
                    uGridAccounting.DataSource = new BindingList<AccountingPPPayVendorDetail>(payVendor.Accounting);
                    uGridAccounting.UpdateData();
                    uGridAccounting.Refresh();
                    uGridCurrency.Rows.Refresh(RefreshRow.FireInitializeRow);
                    uGridCurrency.UpdateData();
                    uGridCurrency.Refresh();
                }
            }
        }
        #endregion

        #region Chọn chứng từ Hạch toán
        private void uGridAccounting_CellChange(object sender, CellEventArgs e)
        {
            bool check;
            uGridAccounting.UpdateData();
            switch (e.Cell.Column.Key)
            {
                case "TotalAmountOriginal":
                    if (Convert.ToDecimal(e.Cell.Value.ToString()) < 0)
                    {
                        if (Utils.GetValueFromTextAndNotNull(e.Cell) >= 0)
                            uGridBill.NotificationCell(e.Cell, "Tiền chiết khấu không được > 0");
                        else
                            uGridBill.RemoveNotificationCell(e.Cell, ultraToolTipManager1, p);
                    }
                    else
                    {
                        if (Utils.GetValueFromTextAndNotNull(e.Cell) > Convert.ToDecimal(e.Cell.Value.ToString()))
                            uGridBill.NotificationCell(e.Cell, "Số tiền không được vượt quá " + e.Cell.Value);
                        else
                            uGridBill.RemoveNotificationCell(e.Cell, ultraToolTipManager1, p);
                    }
                    break;
                case "BudgetItemCode":
                    check = lstBudgetItemTree.Where(x => x.BudgetItemCode == e.Cell.Value.ToString()).Select(x => x.IsParentNode).FirstOrDefault();
                    CheckTabAccounting(check, uGridAccounting, e.Cell);
                    break;
                case "ExpenseItemCode":
                    check = lstExpenseItemTree.Where(x => x.ExpenseItemCode == e.Cell.Value.ToString()).Select(x => x.IsParentNode).FirstOrDefault();
                    CheckTabAccounting(check, uGridAccounting, e.Cell);
                    break;
                case "DepartmentCode":
                    check = lstDepartmentTree.Where(x => x.DepartmentCode == e.Cell.Value.ToString()).Select(x => x.IsParentNode).FirstOrDefault();
                    CheckTabAccounting(check, uGridAccounting, e.Cell);
                    break;
                case "CostSetCode":
                    check = lstCostSetTree.Where(x => x.CostSetCode == e.Cell.Value.ToString()).Select(x => x.IsParentNode).FirstOrDefault();
                    CheckTabAccounting(check, uGridAccounting, e.Cell);
                    break;
                case "StatisticsCode":
                    check = lstStatisticsCodeTree.Where(x => x.StatisticsCode_ == e.Cell.Value.ToString()).Select(x => x.IsParentNode).FirstOrDefault();
                    CheckTabAccounting(check, uGridAccounting, e.Cell);
                    break;

            }
        }

        private void CheckTabAccounting(bool check, UltraGrid ug, UltraGridCell cell)
        {
            if (check)
                ug.NotificationCell(cell, resSystem.MSG_System_29);
            else
                ug.RemoveNotificationCell(cell);
        }
        #endregion

        private void cbbAccountingObjectID_TextChanged(object sender, EventArgs e)
        {
            if (isRun)
            {
                UltraCombo cbbTemp = (UltraCombo)sender;
                Utils.AutoComplateUltraCombo(cbbTemp, accObjectVendor.Where(x => x.AccountingObjectCode.ToLower().Contains(cbbTemp.Text.ToLower()) || x.AccountingObjectName.ToLower().Contains(cbbTemp.Text.ToLower())).ToList());
            }
        }

        private void cbbAccountingObjectID_KeyPress(object sender, KeyPressEventArgs e)
        {
            isRun = true;
        }
        #endregion

        #region CheckErr and btnApply
        protected bool CheckErrorBase<T>(T @select)
        {
            List<ErrorObj> lstError = new List<ErrorObj>();     //danh sách lỗi
            string locationProperty = string.Empty;             //control bị lỗi (để focus đến control đó)
            int typeId = @select.GetProperty<T, int>("TypeID");

            try
            {
                #region Header

                List<string> blackList;        //danh sách bỏ qua (Black List)
                List<string> lstMsgProperty;                        //thông báo lỗi tiếng việt
                List<string> lstIdProperty;                         //key cần check
                string key;
                {
                    #region ngày chứng từ, ngày hạch toán không được phép rỗng (hiện tại chỉ check ngày ko check đến giờ phút giây)
                    blackList = "FADepreciation".Split(',').ToList();
                    if (!blackList.Contains(@select.GetType().Name))
                    {
                        //bổ sung thêm KEY vào lstIdProperty và lstMsgProperty tương ứng
                        lstIdProperty = new List<string> { "Date", "PostedDate", "ODate", "OPostedDate", "IWDate", "IWPostedDate" };
                        lstMsgProperty = new List<string> { resSystem.MSG_Error_01, resSystem.MSG_Error_02 };
                        for (int i = 0; i < lstIdProperty.Count; i++)
                        {
                            key = lstIdProperty[i];
                            if (!@select.HasProperty(key)) continue;
                            DateTime date = @select.GetProperty<T, DateTime>(key);
                            if (date == null || date.Date == Utils.GetDateTimeDefault().Date || date.Date == new DateTime().Date)
                                lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = lstMsgProperty[i] });
                        }
                    }
                    #endregion

                    #region ngày hoạch toán lớn hơn hoặc bằng ngày chứng từ (hiện tại chỉ check ngày ko check đến giờ phút giây)
                    blackList = "".Split(',').ToList();
                    if (!blackList.Contains(@select.GetType().Name))
                    {
                        //bổ sung thêm CẶP KEY vào lstIdProperty và lstMsgProperty tương ứng
                        lstIdProperty = new List<string> { "Date,PostedDate", "ODate,OPostedDate", "IWDate,IWPostedDate" };
                        lstMsgProperty = new List<string> { resSystem.MSG_Error_03 };
                        for (int i = 0; i < lstIdProperty.Count; i++)
                        {
                            string[] lstIdPropertySplit = lstIdProperty[i].Split(',');
                            if (!@select.HasProperty(lstIdPropertySplit[0]) ||
                                !@select.HasProperty(lstIdPropertySplit[1])) continue;
                            key = string.Format("{0} < {1}", lstIdPropertySplit[1], lstIdPropertySplit[0]);
                            DateTime date = @select.GetProperty<T, DateTime>(lstIdPropertySplit[0]);
                            DateTime postdate = @select.GetProperty<T, DateTime>(lstIdPropertySplit[1]);
                            if (postdate.Date < date.Date)
                                lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = lstMsgProperty[i] });
                        }
                    }
                    #endregion

                    #region check mã chứng từ tự sinh cho phép người dùng nhập có đúng chuẩn hay không?
                    blackList = "".Split(',').ToList();
                    if (!blackList.Contains(@select.GetType().Name))
                    {
                        List<int> lstIsOutwardNo = new List<int> { 320, 321, 322, 323, 324, 325 };  //check có kiêm phiếu xuất hay ko (IsDeliveryVoucher)
                        //bổ sung thêm KEY vào lstIdProperty và lstMsgProperty tương ứng
                        lstIdProperty = new List<string> { "No", "OutwardNo", "IWNo" };
                        lstMsgProperty = new List<string> { resSystem.MSG_Error_07, resSystem.MSG_Error_08, resSystem.MSG_Error_16 };
                        for (int i = 0; i < lstIdProperty.Count; i++)
                        {
                            key = lstIdProperty[i];
                            if (!@select.HasProperty(key)) continue;
                            string no = @select.GetProperty<T, string>(key);

                            //trường hợp bán hàng. nếu kiêm phiếu xuất kho thì không check OutwardNo
                            if (lstIsOutwardNo.Contains(typeId) && key.Equals("OutwardNo"))
                            {
                                if (!@select.HasProperty("IsDeliveryVoucher")) continue;
                                if (!@select.GetProperty<T, bool>("IsDeliveryVoucher")) continue;
                            }

                            if (string.IsNullOrEmpty(no) || (!string.IsNullOrEmpty(no) && Utils.CheckNo(no) == null))
                                lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = lstMsgProperty[i] });
                        }
                    }
                    #endregion
                }
                #endregion

                #region Thông tin chung
                #region Check Not Null
                #region không được phép để trống đối tượng
                blackList = "".Split(',').ToList();
                if (!blackList.Contains(@select.GetType().Name))
                {
                    key = "AccountingObjectID";
                    if (@select.HasProperty(key))
                    {
                        Guid? guid = @select.GetProperty<T, Guid?>(key);
                        if (guid == null || guid == Guid.Empty)
                            lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_Error_10 });
                    }
                }
                #endregion

                #region Nộp tiền tài khoản: bắt buộc phải nhập tài khoản ngân hàng
                List<int> allowList = new List<int> { 160, 161, 162, 163 };        //danh sách cho phép (Allow List)
                if (allowList.Contains(typeId))
                {
                    key = "BankAccountDetailID";
                    if (@select.HasProperty(key))
                    {
                        Guid? guid = @select.GetProperty<T, Guid?>(key);
                        if (guid == null || guid == Guid.Empty)
                            lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_Error_15 });
                    }
                }
                #endregion

                #region Thẻ tín dụng: bắt buộc phải nhập số thẻ tín dụng
                if (typeId.Equals(174))
                {
                    key = "CreditCardNumber";
                    if (@select.HasProperty(key))
                    {
                        string guid = @select.GetProperty<T, string>(key);
                        if (guid == null) lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_Error_17 });
                    }
                }
                #endregion

                #region Ủy nhiệm chi: "Tài khoản nhận và TK trả" không được để trống
                if (typeId.Equals(128))
                {
                    if (cbbPaymentOrderBankAccountDetailID.Text == "")
                    {
                        MSG.Warning(resSystem.MSG_Error_29);
                        cbbPaymentOrderBankAccountDetailID.Focus();
                        return false;
                    }
                }
                if (typeId.Equals(134))
                {
                    if (cbbSecCKBankAccountDetailID.Text == "")
                    {
                        MSG.Warning(resSystem.MSG_Error_29);
                        cbbSecCKBankAccountDetailID.Focus();
                        return false;
                    }
                }
                if (typeId.Equals(144))
                {
                    if (cbbSecBankAccountDetailID.Text == "")
                    {
                        MSG.Warning(resSystem.MSG_Error_29);
                        cbbSecBankAccountDetailID.Focus();
                        return false;
                    }
                }
                #endregion
                #endregion

                #region Nhập mã số thuế theo chuẩn
                blackList = "".Split(',').ToList();
                if (!blackList.Contains(@select.GetType().Name))
                {
                    key = "CompanyTaxCode";
                    if (@select.HasProperty(key))
                    {
                        string str = @select.GetProperty<T, string>(key);
                        if (!string.IsNullOrEmpty(str) && !Core.Utils.CheckMST(str))
                            lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_System_44 });
                    }
                }
                #endregion
                #endregion

                #region Grid

                #region TK nợ, có không được để trống. nếu bên Nợ, Có là TK đầu 0 thì ko bắt buộc nhập cả 2, khác không thì bắt buộc nhập cả 2
                blackList = "PPOrder,SAOrder,SAQuote".Split(',').ToList();
                if (!blackList.Contains(@select.GetType().Name))
                {
                    lstIdProperty = new List<string> { "DebitAccount", "CreditAccount" };
                    key = string.Format("{0}Detail", @select.GetType().Name);
                    string keys = string.Format("{0}s", key);
                    if (@select.HasProperty(keys))
                    {
                        IList lstT = ListObjectInput.FirstOrDefault(k =>
                        {
                            var fullName = k.GetType().FullName;
                            return fullName != null &&
                                   fullName.Contains(key);
                        });
                        if (lstT != null && lstT.Count > 0)
                        {
                            foreach (object item in lstT)
                            {
                                if (!item.HasProperty(lstIdProperty[0]) || !item.HasProperty(lstIdProperty[1])) continue;
                                string tkno = item.GetProperty<object, string>(lstIdProperty[0]);
                                string tkco = item.GetProperty<object, string>(lstIdProperty[1]);

                                //K nợ, có không được để trống. nếu bên Nợ, Có là TK đầu 0 thì ko bắt buộc nhập cả 2, khác không thì bắt buộc nhập cả 2
                                bool a = (string.IsNullOrEmpty(tkno) &&
                                          ((!string.IsNullOrEmpty(tkco)) && (tkco.Substring(0, 1).Equals("0"))))
                                         ||
                                         (string.IsNullOrEmpty(tkco) &&
                                          ((!string.IsNullOrEmpty(tkno)) && (tkno.Substring(0, 1).Equals("0"))));
                                bool b = (!string.IsNullOrEmpty(tkno) && !tkno.Substring(0, 1).Equals("0"))
                                         && (!string.IsNullOrEmpty(tkco) && !tkco.Substring(0, 1).Equals("0"));
                                if (!(a || b))
                                    lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = resSystem.MSG_Error_09 });

                                #region check chi tiết theo
                                Account tkNo = Utils.ListAccount.FirstOrDefault(k => k.AccountNumber.Equals(tkno));
                                Account tkCo = Utils.ListAccount.FirstOrDefault(k => k.AccountNumber.Equals(tkco));
                                //=> cùng chi tiết theo một cái
                                string msg;
                                if (!ReflectionUtils.CheckDetailTypeAccount(@select, item, tkNo, out msg))
                                    lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = msg });
                                if (!ReflectionUtils.CheckDetailTypeAccount(@select, item, tkCo, out msg))
                                    lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = msg });
                                #endregion
                            }
                        }
                    }
                }
                #endregion
                #endregion

                #region Other
                #region check chứng từ đã tồn tại
                blackList = "".Split(',').ToList();
                if (!blackList.Contains(@select.GetType().Name))
                {
                    //bổ sung thêm KEY vào lstIdProperty và lstMsgProperty tương ứng
                    lstIdProperty = new List<string> { "No", "OutwardNo", "IWNo" };
                    lstMsgProperty = new List<string> { resSystem.MSG_Error_05, resSystem.MSG_Error_06, resSystem.MSG_Error_05 };
                    for (int i = 0; i < lstIdProperty.Count; i++)
                    {
                        key = lstIdProperty[i];
                        if (!@select.HasProperty(key)) continue;

                        //trường hợp bán hàng. nếu kiêm phiếu xuất kho thì không check OutwardNo
                        if ((new List<int> { 320, 321, 322, 323, 324, 325 }).Contains(typeId) && key.Equals("OutwardNo"))
                        {
                            if (!@select.HasProperty("IsDeliveryVoucher")) continue;
                            if (!@select.GetProperty<T, bool>("IsDeliveryVoucher")) continue;
                        }

                        try
                        {
                            if (key.Equals("OutwardNo") || key.Equals("IWNo"))
                            {
                                RSInwardOutward @object =
                                    Utils.IRSInwardOutwardService.Query.FirstOrDefault(
                                        k =>
                                        k.ID == @select.GetProperty<T, Guid>(key) &&
                                        k.IsInwardOutwardType == (key.Equals("OutwardNo") ? 0 : 1));
                                if (@object != null)
                                    lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = string.Format(lstMsgProperty[i], @object.GetProperty<RSInwardOutward, string>(key)) });
                            }
                            else if (key.Equals("No"))
                            {
                                string temp = @select.GetProperty<T, string>(key);
                                BaseService<T, Guid> service = this.GetIService(@select);
                                List<T> tempp = service.Query.ToList();
                                T @object = (T)Activator.CreateInstance(typeof(T));
                                foreach (T item in tempp)
                                {
                                    string temppp = item.GetProperty<T, string>(key);
                                    if (!temppp.Equals(temp)) continue;
                                    @object = item;
                                    break;
                                }
                                // T @object = .FirstOrDefault(k => k.GetProperty<T, string>(key).Equals(test));
                                //if (@object != null && !string.IsNullOrEmpty(@object.GetProperty<T, string>(key)))
                                if (!string.IsNullOrEmpty(@object.GetProperty<T, string>(key)))
                                    lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = string.Format(lstMsgProperty[i], @object.GetProperty<T, string>(key)) });
                                if (!_statusForm.Equals(ConstFrm.optStatusForm.Add))
                                    //[H.A] Thêm UnbindSession cho TH Edit để tránh lỗi difference session
                                    service.UnbindSession(tempp);
                            }
                        }
                        catch (Exception ex)
                        {
                            //Mất mạng hay gì gì đó! bị lỗi hay bị catch gì đó....
                            string msgLocation = string.Format(resSystem.MSG_Error_11, @select.GetProperty<T, string>(key));
                            lstError.Add(new ErrorObj
                            {
                                Id = key,
                                Location = locationProperty,
                                Msg = msgLocation,
                                Exception = ex
                            });
                        }
                    }
                }
                #endregion
                #endregion

                #region Check Template
                //[Huy Anh]
                //if (this.CheckExistObject<Template>(((Guid)_templateID))) lstError.Add(new ErrorObj { Id = "Utils.IsTemplateError", Location = locationProperty, Msg = resSystem.MSG_System_46 });
                //if(Utils.Checked.Equals(false))  
                var lstErrorForm = this.CheckGridForm();
                if (lstErrorForm.Count > 0) lstError.Add(new ErrorObj { Id = "IsFormError", Location = locationProperty, Object = lstErrorForm.FirstOrDefault().Key, Msg = lstErrorForm.FirstOrDefault().Value });
                #endregion
            }
            catch (Exception exception)
            {
                MSG.Warning(string.Format("Có lỗi xảy ra khi check lỗi! \r\n Chi tiết lỗi: \r\n\r\n {0} \r\n\r\n {1}", exception.Message, exception.StackTrace));
                return false;
            }
            if (lstError.Count == 0)
                return true;
            MSG.Error(lstError[0].Msg);
            if (lstError[0].Object != null)
            {
                if (lstError[0].Object is UltraGridCell)
                {
                    var cell = (UltraGridCell)lstError[0].Object;
                    var grid = cell.Band.Layout.Grid as UltraGrid;
                    grid.Focus();
                    grid.ActiveCell = cell;
                    grid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                }
            }
            return false;
            //throw new NotImplementedException();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            payVendor.Bill = (uGridBill.DataSource as List<BillPPPayVendorDetail>).Where(c => c.AmountOriginal > 0 && c.CheckColumn).ToList();
            if (payVendor.Bill.Count <= 0)
            {
                MSG.Error("Bạn cần chọn chứng từ trước khi thực hiện trả tiền nhà cung cấp");
                return;
            }
            if (dteDate.Value == null || !OPNUntil.CheckDateTime(dteDate.Value.ToString()))
            {
                MSG.Error(resSystem.MSG_System_15);
                return;
            }
            if (postDate.Value == null || !OPNUntil.CheckDateTime(postDate.Value.ToString()))
            {
                MSG.Error(resSystem.MSG_Error_02);
                return;
            }
            if (Convert.ToDecimal(uGridBill.Rows.SummaryValues["AmountOriginalSum"].Value.ToString()) < Convert.ToDecimal(uGridAccounting.Rows.SummaryValues["TotalAmountOriginalSum"].Value.ToString()))
            {
                MSG.Warning("Tổng Số Tiền ở tab Hạch Toán phải bằng Tổng Số Trả ở tab Hóa Đơn. Vui lòng kiểm tra lại!");
                return;
            }
            if (Convert.ToDecimal(uGridBill.Rows.SummaryValues["AmountOriginalSum"].Value.ToString()) > Convert.ToDecimal(uGridAccounting.Rows.SummaryValues["TotalAmountOriginalSum"].Value.ToString()))
            {
                MSG.Warning("Tổng Số Tiền ở tab Hạch Toán phải bằng Tổng Số Trả ở tab Hóa Đơn. Vui lòng kiểm tra lại!");
                return;
            }
            if (Convert.ToDecimal(uGridBill.Rows.SummaryValues["AmountOriginalSum"].Value.ToString()) != 0 && Convert.ToDecimal(uGridAccounting.Rows.SummaryValues["TotalAmountOriginalSum"].Value.ToString()) == 0M)
            {
                MSG.Warning("Tổng Số Tiền ở tab Hạch Toán phải bằng Tổng Số Trả ở tab Hóa Đơn. Vui lòng kiểm tra lại!");
                return;
            }
            // edit by tungnt gán dữ liệu từ bindinglist -> list
            payVendor.Accounting = ((BindingList<AccountingPPPayVendorDetail>)uGridAccounting.DataSource).ToList();
            CurrencyPVendor currency = new CurrencyPVendor();
            var currencyPVendors = uGridCurrency.DataSource as List<CurrencyPVendor>;
            if (currencyPVendors != null) currency = currencyPVendors.FirstOrDefault();
            payVendor.Date = (DateTime)dteDate.Value;
            payVendor.PostedDate = (DateTime)postDate.Value;
            payVendor.No = txtNo.Text;
            payVendor.CreditCardNumber = (string)cbbCreditCardNumber.Value;

            switch (cbbPaymentMethod.SelectedIndex)
            {
                case 0: // Tiền mặt
                    try
                    {
                        if (currency != null)
                        {
                            MCPayment payment = new MCPayment
                            {
                                ID = Guid.NewGuid(),
                                TypeID = 118,
                                Date = payVendor.Date,
                                PostedDate = payVendor.PostedDate,
                                No = payVendor.No,
                                AccountingObjectID = payVendor.AccountingObjectID,
                                AccountingObjectName = payVendor.AccountingObjectName,
                                AccountingObjectAddress = payVendor.AccountingObjectAddress,
                                AccountingObjectType = 1,
                                Reason = txtReason.Text,
                                NumberAttach = txtAccreditativeAttach.Text,
                                TotalAmount = currency.TotalAmount,
                                TotalAmountOriginal = currency.TotalAmountOriginal,
                                CurrencyID = currency.CurrencyID,
                                ExchangeRate = isBQCK ? currency.ExchangeRate : (currency.TotalAmount / currency.TotalAmountOriginal),
                                Receiver = txtReceiver.Text,
                                TotalAllOriginal = currency.TotalAmountOriginal,
                                TotalAll = currency.TotalAmountOriginal * currency.ExchangeRate
                            };
                            foreach (BillPPPayVendorDetail bill in payVendor.Bill)
                            {
                                if (bill.AmountOriginal > bill.DebitAmountOriginal)
                                {
                                    MSG.Warning("Số trả không được lớn hơn số còn nợ");
                                    return;
                                }
                                MCPaymentDetailVendor billVendor = new MCPaymentDetailVendor();
                                billVendor.MCPaymentID = payment.ID;
                                billVendor.VoucherDate = bill.Date;
                                billVendor.VoucherPostedDate = bill.Date;
                                billVendor.VoucherNo = bill.No;
                                billVendor.DebitAccount = bill.Account;
                                billVendor.DueDate = bill.DueDate;
                                billVendor.PayableAmount = Convert.ToDecimal(bill.TotalDebit);
                                billVendor.PayableAmountOriginal = Convert.ToDecimal(bill.TotalDebitOriginal);
                                billVendor.RemainingAmount = Convert.ToDecimal(bill.DebitAmount);
                                billVendor.RemainingAmountOriginal = Convert.ToDecimal(bill.DebitAmountOriginal);
                                billVendor.Amount = Convert.ToDecimal(bill.Amount);
                                billVendor.AmountOriginal = Convert.ToDecimal(bill.AmountOriginal);
                                billVendor.DiscountRate = bill.DiscountRate;
                                billVendor.DiscountAccount = bill.DiscountAccount;
                                billVendor.DiscountAmount = Convert.ToDecimal(bill.DiscountAmount);
                                billVendor.DiscountAmountOriginal = Convert.ToDecimal(bill.DiscountAmountOriginal);
                                billVendor.DiscountRate = Convert.ToDecimal(bill.DiscountRate);
                                billVendor.AccountingObjectID = payVendor.AccountingObjectID;
                                billVendor.InvoiceNo = bill.InvoiceNo;
                                billVendor.PPInvoiceID = bill.ReferenceID;
                                payment.MCPaymentDetailVendors.Add(billVendor);
                            }
                            foreach (AccountingPPPayVendorDetail acct in payVendor.Accounting)
                            {
                                MCPaymentDetail paymentDetail = new MCPaymentDetail
                                {
                                    MCPaymentID = payment.ID,
                                    Description = acct.Description,
                                    DebitAccount = acct.DebitAccount,
                                    CreditAccount = acct.CreditAccount,
                                    Amount = Convert.ToDecimal(acct.TotalAmount),
                                    AmountOriginal = Convert.ToDecimal(acct.TotalAmountOriginal),
                                    BudgetItemID = acct.BudgetItemCode != null ? (acct.BudgetItemCode) : (Guid?)null,
                                    CostSetID = acct.CostSetCode != null ? (acct.CostSetCode) : (Guid?)null,
                                    ContractID = acct.EmContractCode != null ? (acct.EmContractCode) : (Guid?)null,
                                    StatisticsCodeID = acct.StatisticsCode != null ? (acct.StatisticsCode) : (Guid?)null,
                                    ExpenseItemID = acct.ExpenseItemCode != null ? (acct.ExpenseItemCode) : (Guid?)null,
                                    DepartmentID = acct.DepartmentCode != null ? (acct.DepartmentCode) : (Guid?)null,
                                    IsIrrationalCost = acct.IsIrrationalCost,
                                    AccountingObjectID = payVendor.AccountingObjectID
                                };
                                payment.MCPaymentDetails.Add(paymentDetail);
                            }
                            //if (!CheckErrorBase(payment)) return;
                            //_IMCPaymentService.BeginTran();
                            //_IMCPaymentService.CreateNew(payment);
                            //bool saveledger = Utils.SaveLedger<MCPayment>(payment);
                            //if (!saveledger)
                            //{
                            //    _IMCPaymentService.RolbackTran();
                            //    //WaitingFrm.StopWaiting();
                            //    return;
                            //}
                            //bool status = _IGenCodeService.UpdateGenCodeForm(TypeGroup, payment.GetProperty<MCPayment, string>("No"), payment.ID);
                            ////Logs.WriteTxt("Add fnc _ line 1683");
                            //if (!status)
                            //{
                            //    _IMCPaymentService.RolbackTran();
                            //    MSG.Warning(resSystem.MSG_System_52);
                            //    //WaitingFrm.StopWaiting();
                            //    return;
                            //}
                            //Logs.SaveBusiness(payment, type, Logs.Action.Add);
                            //_IMCPaymentService.CommitTran();
                            //if (
                            //MSG.Question("Thành công. Hệ thống đã sinh phiếu chi  " + payment.No +
                            //             " \nBạn có muốn xem chứng từ vừa tạo không?") == DialogResult.Yes)
                            //{
                            try
                            {
                                FMCPaymentDetail frm = new FMCPaymentDetail(payment, new List<MCPayment>() { payment }, ConstFrm.optStatusForm.Add);
                                frm.FormClosed += frm_FormClosed;
                                frm.ShowDialog(this);
                            }
                            catch (Exception ex)
                            {
                                MSG.Warning("Kết nối chứng từ không thành công. Xin vui lòng thử lại sau.");
                            }

                            //}
                            //else
                            //    Close();
                            //new FMCPaymentDetail(payment, new List<MCPayment> { payment }, ConstFrm.optStatusForm.View).ShowDialog();
                        }
                    }
                    catch (Exception)
                    {
                        _IMCPaymentService.RolbackTran();
                        MSG.Error(resSystem.MSG_System_48);
                    }
                    break;
                case 1: // Ủy nhiệm chi
                case 2: //Sec CK
                case 3: //Sec TM
                    try
                    {
                        MBTellerPaper teller = new MBTellerPaper
                        {
                            ID = Guid.NewGuid(),
                            Date = payVendor.Date,
                            PostedDate = payVendor.PostedDate,
                            No = payVendor.No,
                            BankAccountDetailID = payVendor.BankAccount != null ? Guid.Parse(payVendor.BankAccount) : (Guid?)null,
                            BankName = payVendor.BankName,
                            AccountingObjectID = payVendor.AccountingObjectID,
                            AccountingObjectName = payVendor.AccountingObjectName,
                            AccountingObjectAddress = payVendor.AccountingObjectAddress,
                            AccountingObjectBankAccount = payVendor.AccountingObjectBankAccount,
                            AccountingObjectBankName = payVendor.AccountingObjectBankName ?? "",
                            AccountingObjectType = 1,
                            IssueDate = payVendor.IssueDate,
                            TotalAmount = currency.TotalAmount,
                            TotalAmountOriginal = currency.TotalAmountOriginal,
                            TotalAll = currency.TotalAmount,
                            TotalAllOriginal = currency.TotalAmountOriginal,
                            ExchangeRate = isBQCK ? currency.ExchangeRate : (currency.TotalAmount / currency.TotalAmountOriginal),
                            CurrencyID = currency.CurrencyID
                        };
                        switch (cbbPaymentMethod.SelectedIndex)
                        {
                            //Ủy nhiệm chi
                            case 1:
                                teller.TypeID = 128;
                                teller.Reason = txtPaymentOrderReason.Text;
                                TypeGroup = 12;
                                break;
                            //Sec CK
                            case 2:
                                teller.TypeID = 134;
                                teller.Reason = txtSecCKReason.Text;
                                TypeGroup = 13;
                                break;
                            //Sec TM
                            case 3:
                                teller.TypeID = 144;
                                teller.Reason = txtSecReason.Text;
                                TypeGroup = 14;
                                break;
                        }
                        foreach (BillPPPayVendorDetail bill in payVendor.Bill)
                        {
                            if (bill.AmountOriginal > bill.DebitAmountOriginal)
                            {
                                MSG.Warning("Số trả không được lớn hơn số còn nợ");
                                return;
                            }
                            MBTellerPaperDetailVendor billVendor = new MBTellerPaperDetailVendor();
                            billVendor.MBTellerPaperID = teller.ID;
                            billVendor.DebitAccount = bill.Account;
                            billVendor.VoucherDate = bill.Date;
                            billVendor.VoucherNo = bill.No;
                            billVendor.VoucherPostedDate = payVendor.PostedDate;
                            billVendor.DueDate = bill.DueDate;
                            billVendor.PayableAmount = Convert.ToDecimal(bill.TotalDebit);
                            billVendor.PayableAmountOriginal = Convert.ToDecimal(bill.TotalDebitOriginal);
                            billVendor.RemainingAmount = Convert.ToDecimal(bill.DebitAmount);
                            billVendor.RemainingAmountOriginal = Convert.ToDecimal(bill.DebitAmountOriginal);
                            billVendor.Amount = Convert.ToDecimal(bill.Amount);
                            billVendor.AmountOriginal = Convert.ToDecimal(bill.AmountOriginal);
                            billVendor.DiscountAccount = bill.DiscountAccount;
                            billVendor.DiscountRate = bill.DiscountRate;
                            billVendor.DiscountAmount = Convert.ToDecimal(bill.DiscountAmount);
                            billVendor.DiscountAmountOriginal = Convert.ToDecimal(bill.DiscountAmountOriginal);
                            billVendor.DiscountRate = Convert.ToDecimal(bill.DiscountRate);
                            billVendor.AccountingObjectID = payVendor.AccountingObjectID;
                            billVendor.InvoiceNo = bill.InvoiceNo;
                            billVendor.PPInvoiceID = bill.ReferenceID;
                            teller.MBTellerPaperDetailVendors.Add(billVendor);
                        }

                        foreach (AccountingPPPayVendorDetail acct in payVendor.Accounting)
                        {
                            MBTellerPaperDetail tellerDetail = new MBTellerPaperDetail();
                            tellerDetail.MBTellerPaperID = teller.ID;
                            tellerDetail.Description = acct.Description;
                            tellerDetail.DebitAccount = acct.DebitAccount;
                            tellerDetail.CreditAccount = acct.CreditAccount;
                            tellerDetail.Amount = Convert.ToDecimal(acct.TotalAmount);
                            tellerDetail.AmountOriginal = Convert.ToDecimal(acct.TotalAmountOriginal);
                            tellerDetail.BudgetItemID = acct.BudgetItemCode != null ? (acct.BudgetItemCode) : (Guid?)null;
                            tellerDetail.CostSetID = acct.CostSetCode != null ? (acct.CostSetCode) : (Guid?)null;
                            tellerDetail.DepartmentID = acct.DepartmentCode != null ? (acct.DepartmentCode) : (Guid?)null;
                            tellerDetail.ContractID = acct.EmContractCode != null ? (acct.EmContractCode) : (Guid?)null;
                            tellerDetail.StatisticsCodeID = acct.StatisticsCode != null ? (acct.StatisticsCode) : (Guid?)null;
                            tellerDetail.ExpenseItemID = acct.ExpenseItemCode != null ? (acct.ExpenseItemCode) : (Guid?)null;
                            tellerDetail.AccountingObjectID = payVendor.AccountingObjectID;
                            teller.MBTellerPaperDetails.Add(tellerDetail);
                        }
                        //if (!CheckErrorBase(teller)) return;
                        //_IMBTellerPaperService.BeginTran();
                        //_IMBTellerPaperService.CreateNew(teller);

                        //bool saveledger = Utils.SaveLedger<MBTellerPaper>(teller);
                        //if (!saveledger)
                        //{
                        //    _IMBTellerPaperService.RolbackTran();
                        //    return;
                        //}

                        //bool status = _IGenCodeService.UpdateGenCodeForm(TypeGroup, teller.GetProperty<MBTellerPaper, string>("No"), teller.ID);
                        //if (!status)
                        //{
                        //    _IMBTellerPaperService.RolbackTran();
                        //    MSG.Warning(resSystem.MSG_System_52);
                        //    //WaitingFrm.StopWaiting();
                        //    return;
                        //}

                        //Logs.SaveBusiness(teller, teller.TypeID, Logs.Action.Add);

                        //_IMBTellerPaperService.CommitTran();

                        //if (MSG.Question("Thành công. Hệ thống đã sinh phiếu chi " + teller.No +
                        //                 " \nBạn có muốn xem chứng từ vừa tạo không?") == DialogResult.Yes)
                        //{
                        try
                        {
                            FMBTellerPaperDetail frm = new FMBTellerPaperDetail(teller, new List<MBTellerPaper>() { teller }, ConstFrm.optStatusForm.Add);
                            frm.FormClosed += frm_FormClosed;
                            frm.ShowDialog(this);
                        }
                        catch (Exception ex)
                        {
                            MSG.Warning("Kết nối chứng từ không thành công. Xin vui lòng thử lại sau.");
                        }
                        //}
                        //else
                        //{
                        //    Close();
                        //}
                        //new FMBTellerPaperDetail(teller, new List<MBTellerPaper> { teller },ConstFrm.optStatusForm.View).ShowDialog();
                        //MSG.MessageBoxStand(string.Format(resSystem.MSG_System_47, payVendor.No), MessageBoxButtons.OK,
                        //  MessageBoxIcon.Information);
                    }
                    catch (Exception)
                    {
                        _IMBTellerPaperService.RolbackTran();
                        MSG.Error(resSystem.MSG_System_48);
                    }
                    break;
                case 4: // Thẻ tín dụng
                    try
                    {

                        MBCreditCard creditCard = new MBCreditCard
                        {
                            ID = Guid.NewGuid(),
                            TypeID = 174,
                            Date = payVendor.Date,
                            No = payVendor.No,
                            PostedDate = payVendor.PostedDate,
                            AccountingObjectID = payVendor.AccountingObjectID,
                            AccountingObjectName = payVendor.AccountingObjectName,
                            AccountingObjectAddress = payVendor.AccountingObjectAddress,
                            AccountingObjectBankAccount = payVendor.AccountingObjectBankAccount,
                            AccountingObjectBankName = payVendor.AccountingObjectBankName,
                            Reason = txtCreditCardReason.Text,
                            AccountingObjectType = 1,
                            CreditCardNumber = payVendor.CreditCardNumber,
                            TotalAmount = currency.TotalAmount,
                            TotalAmountOriginal = currency.TotalAmountOriginal,
                            TotalAll = currency.TotalAmount,
                            TotalAllOriginal = currency.TotalAmountOriginal,
                            ExchangeRate = isBQCK ? currency.ExchangeRate : (currency.TotalAmount / currency.TotalAmountOriginal),
                            CurrencyID = currency.CurrencyID
                        };

                        foreach (BillPPPayVendorDetail bill in payVendor.Bill)
                        {
                            if (bill.AmountOriginal > bill.DebitAmountOriginal)
                            {
                                MSG.Warning("Số trả không được lớn hơn số còn nợ");
                                return;
                            }
                            MBCreditCardDetailVendor billVendor = new MBCreditCardDetailVendor
                            {
                                MBCreditCardID = creditCard.ID,
                                DebitAccount = bill.Account,
                                VoucherDate = payVendor.Date,
                                VoucherNo = payVendor.No,
                                VoucherPostedDate = payVendor.PostedDate,
                                DueDate = bill.DueDate,
                                PayableAmount = Convert.ToDecimal(bill.TotalDebit),
                                PayableAmountOriginal = Convert.ToDecimal(bill.TotalDebitOriginal),
                                RemainingAmount = Convert.ToDecimal(bill.DebitAmount),
                                RemainingAmountOriginal = Convert.ToDecimal(bill.DebitAmountOriginal),
                                DiscountAccount = bill.DiscountAccount,
                                DiscountAmount = Convert.ToDecimal(bill.DiscountAmount),
                                DiscountAmountOriginal = Convert.ToDecimal(bill.DiscountAmountOriginal),
                                DiscountRate = Convert.ToDecimal(bill.DiscountRate),
                                AccountingObjectID = payVendor.AccountingObjectID,
                                PPInvoiceID = bill.ReferenceID,
                                InvoiceNo = bill.InvoiceNo,
                                Amount = (decimal)bill.Amount,
                                AmountOriginal = (decimal)bill.AmountOriginal
                            };
                            creditCard.MBCreditCardDetailVendors.Add(billVendor);
                        }

                        foreach (AccountingPPPayVendorDetail acct in payVendor.Accounting)
                        {
                            MBCreditCardDetail creditCardDetail = new MBCreditCardDetail
                            {
                                MBCreditCardID = creditCard.ID,
                                Description = acct.Description,
                                DebitAccount = acct.DebitAccount,
                                CreditAccount = acct.CreditAccount,
                                Amount = Convert.ToDecimal(acct.TotalAmount),
                                AmountOriginal = Convert.ToDecimal(acct.TotalAmountOriginal),
                                BudgetItemID = acct.BudgetItemCode != null ? (acct.BudgetItemCode) : (Guid?)null,
                                CostSetID = acct.CostSetCode != null ? (acct.CostSetCode) : (Guid?)null,
                                ContractID = acct.EmContractCode != null ? (acct.EmContractCode) : (Guid?)null,
                                StatisticsCodeID = acct.StatisticsCode != null ? (acct.StatisticsCode) : (Guid?)null,
                                ExpenseItemID = acct.ExpenseItemCode != null ? (acct.ExpenseItemCode) : (Guid?)null,
                                DepartmentID = acct.DepartmentCode != null ? (acct.DepartmentCode) : (Guid?)null,
                                IsIrrationalCost = acct.IsIrrationalCost,
                                AccountingObjectID = payVendor.AccountingObjectID,

                            };
                            creditCard.MBCreditCardDetails.Add(creditCardDetail);
                        }
                        //if (!CheckErrorBase(creditCard)) return;
                        //_IMBCreditCardService.BeginTran();
                        //_IMBCreditCardService.CreateNew(creditCard);
                        //bool saveledger = Utils.SaveLedger<MBCreditCard>(creditCard);

                        //if (!saveledger)
                        //{
                        //    _IMCPaymentService.RolbackTran();
                        //    //WaitingFrm.StopWaiting();
                        //    return;
                        //}

                        //bool status = _IGenCodeService.UpdateGenCodeForm(TypeGroup, creditCard.GetProperty<MBCreditCard, string>("No"), creditCard.ID);
                        //if (!status)
                        //{
                        //    _IMBCreditCardService.RolbackTran();
                        //    MSG.Warning(resSystem.MSG_System_52);
                        //    //WaitingFrm.StopWaiting();
                        //    return;
                        //}
                        //Logs.SaveBusiness(creditCard, creditCard.TypeID, Logs.Action.Add);
                        //_IMBCreditCardService.CommitTran();
                        //if (
                        //    MSG.Question("Thành công. Hệ thống đã sinh phiếu chi " + creditCard.No +
                        //                 " \nBạn có muốn xem chứng từ vừa tạo không?") == DialogResult.Yes)
                        //{
                        try
                        {
                            FMBCreditCardDetail frm = new FMBCreditCardDetail(creditCard, new List<MBCreditCard>() { creditCard }, ConstFrm.optStatusForm.Add);
                            frm.FormClosed += frm_FormClosed;
                            frm.ShowDialog(this);
                        }
                        catch (Exception ex)
                        {
                            MSG.Warning("Kết nối chứng từ không thành công. Xin vui lòng thử lại sau.");
                        }
                        //}
                        //else
                        //{
                        //    Close();
                        //}
                        //new FMBCreditCardDetail(creditCard, new List<MBCreditCard> { creditCard },ConstFrm.optStatusForm.View).ShowDialog();
                        //MSG.MessageBoxStand(string.Format(resSystem.MSG_System_47, payVendor.No), MessageBoxButtons.OK,
                        //  MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        _IMBCreditCardService.RolbackTran();
                        viewException(ex);
                        MSG.Error(resSystem.MSG_System_48);
                    }
                    break;
            }

            //ResetToolbar();
            Utils.ClearCacheByType<PayVendor>();
            //btnApply.Enabled = false;
            //WaitingFrm.StopWaiting();
        }
        void frm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }
        #endregion
        public static bool IsClose { get; set; }

        private void viewException(Exception ex)
        {
            this.Close();
        }

        private void postDate_ValueChanged(object sender, EventArgs e)
        {
            if (postDate.Value != null)
            {
                dteDate.Value = postDate.Value;
            }
        }

        private void uGridBill_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                BillPPPayVendorDetail temp = (BillPPPayVendorDetail)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    Utils.ViewVoucherSelected(temp.ReferenceID, temp.TypeID);

            }
        }

        private void uGridCurrency_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key == "ExchangeRate")
            {
                if (e.Cell.Row.Cells["CurrencyID"].Value.ToString() != "VND")
                    e.Cell.Row.Cells["TotalAmount"].Value = Utils.GetValueFromTextAndNotNull(e.Cell.Row.Cells["ExchangeRate"]) * Utils.GetValueFromTextAndNotNull(e.Cell.Row.Cells["TotalAmountOriginal"]);
                if (e.Cell.Activated)
                {
                    _statusTemp = true;
                    foreach (UltraGridRow ugr in uGridBill.Rows)
                    {
                        uGridBill.Rows[0].Cells["DifferAmount"].Value = Math.Round((((decimal)uGridBill.Rows[0].Cells["AmountOriginal"].Value * (decimal)uGridCurrency.Rows[0].Cells["ExchangeRate"].Value)) - ((decimal)uGridBill.Rows[0].Cells["LastExchangeRate"].Value == 0 ?
                        ((decimal)uGridBill.Rows[0].Cells["AmountOriginal"].Value * (decimal)uGridBill.Rows[0].Cells["RefVoucherExchangeRate"].Value) : ((decimal)uGridBill.Rows[0].Cells["AmountOriginal"].Value * (decimal)uGridBill.Rows[0].Cells["LastExchangeRate"].Value)), LamTron, MidpointRounding.AwayFromZero);
                        ugr.Cells["Amount"].Value = Convert.ToDecimal(ugr.Cells["AmountOriginal"].Value.ToString()) * Utils.GetValueFromTextAndNotNull(e.Cell.Row.Cells["ExchangeRate"]);
                        CheckDiscount(ugr.Cells["DiscountRate"]);

                    }
                }
            }
        }

        private void uGridCurrency_AfterExitEditMode(object sender, EventArgs e)
        {
            uGridCurrency.UpdateData();
            var need = Utils.GetValueFromTextAndNotNull(uGridCurrency.Rows[0].Cells["TotalAmountOriginal"]);
            decimal total = 0;
            uGridCurrency.UpdateData();
            UltraGridCell cell = uGridCurrency.ActiveCell;
            if (cell.Column.Key == "TotalAmountOriginal")
            {
                if (cell.Row.Cells["CurrencyID"].Value.ToString() != "VND")
                    cell.Row.Cells["TotalAmount"].Value = Utils.GetValueFromTextAndNotNull(cell.Row.Cells["ExchangeRate"]) * Utils.GetValueFromTextAndNotNull(cell.Row.Cells["TotalAmountOriginal"]);
                if (cell.Activated)
                {
                    _statusTemp = true;
                    foreach (UltraGridRow ugr in uGridBill.Rows)
                    {
                        if (total < need)
                        {
                            decimal temp = Convert.ToDecimal(ugr.Cells["DebitAmountOriginal"].Value.ToString());
                            if ((temp + total) <= need)
                            {
                                ugr.Cells["CheckColumn"].Value = true;
                                ugr.Cells["AmountOriginal"].Value = ugr.Cells["DebitAmountOriginal"].Value;
                            }
                            else
                            {
                                ugr.Cells["CheckColumn"].Value = true;
                                ugr.Cells["AmountOriginal"].Value = need - total;
                            }
                        }
                        else
                        {
                            ugr.Cells["CheckColumn"].Value = false;
                            ugr.Cells["AmountOriginal"].Value = 0M;
                        }
                        total += Convert.ToDecimal(ugr.Cells["AmountOriginal"].Value.ToString());
                        ugr.Cells["Amount"].Value = Convert.ToDecimal(ugr.Cells["AmountOriginal"].Value.ToString()) * Utils.GetValueFromTextAndNotNull(cell.Row.Cells["ExchangeRate"]);
                        CheckDiscount(ugr.Cells["DiscountRate"]);

                    }
                }
            }
        }

        private void uGridAccounting_BeforeRowActivate(object sender, RowEventArgs e)
        {
            // add by tungnt: copy dữ liệu ở dòng trên xuống dòng dưới
            UltraGridRow row = e.Row;
            int index = row.Index;
            if (index > 0)
            {
                foreach (var cell in row.Cells)
                {
                    if (cell.Column.Key.Contains("TotalAmountOriginal") || cell.Column.Key.Contains("CreditAccount") || cell.Column.Key.Contains("DebitAccount") || cell.Column.Key.Contains("AccountingObjectCode") || cell.Column.Key.Contains("Description"))
                        if (Utils.GetIsCopyRow())
                            if (cell.Value != null && cell.Value.ToString() != "") { }
                            else
                                cell.Value = uGridAccounting.Rows[index - 1].Cells[cell.Column.Key].Value;

                }
                uGridAccounting.Rows[index].UpdateData();
            }
        }

        private void uGridAccounting_AfterCellUpdate(object sender, CellEventArgs e)
        {
            // add by tungnt: tính lại tiền quy đổi
            if (e.Cell.Column.Key == "TotalAmountOriginal")
            {
                if (uGridCurrency.Rows[0].Cells["CurrencyID"].Value.ToString() != "VND")
                    e.Cell.Row.Cells["TotalAmount"].Value = Convert.ToDecimal(e.Cell.Row.Cells["TotalAmountOriginal"].Value.ToString()) * Utils.GetValueFromTextAndNotNull(uGridCurrency.Rows[0].Cells["ExchangeRate"]);
            }
        }

        private void cmbtnAddRow_Click(object sender, EventArgs e)
        {
            // tungnt: thêm dòng bằng cms
            Utils.AddNewRow4Grid(uGridAccounting);
        }

        private void cmbtnDelRow_Click(object sender, EventArgs e)
        {
            // tungnt: xóa dòng bằng cms
            this.RemoveRow4Grid<FPPPayVendorDetail>(uGridAccounting);
        }

        private void txtCreditCardAccountingObjectName_TextChanged(object sender, EventArgs e)
        {
            UltraTextEditor txt = (UltraTextEditor)sender;
            txt.Text = txt.Text.Replace("\r\n", "");
            if (txt.Text == "")
                txt.Multiline = true;
            else
                txt.Multiline = false;
        }
    }

    public class FrmTemp : DetailBase<PayVendor>
    {
    }
}