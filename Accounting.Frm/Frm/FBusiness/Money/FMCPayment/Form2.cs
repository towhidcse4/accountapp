﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Frm.TextMessage;

namespace Accounting.Frm.Frm.FBusiness.FMCPayment
{
    public partial class Form2 : MyClass1
    {
        public Form2()
        {
            InitializeComponent();
        }

        public override void InitValueFrm()
        {
            base.InitValueFrm();
            NameTable = ConstDatabase.Account_TableName;
            IsGridShow = true;
        }
    }

    public class MyClass1 : BaseCatalog<Core.Domain.MCPayment>
    {

    }
}
