﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;
using Type = System.Type;
using Infragistics.Win.UltraWinTabControl;

namespace Accounting
{
    public sealed partial class FMCPaymentDetail : FmcPaymentDetailStand
    {
        #region khai báo
        UltraGrid uGrid0;
        UltraGrid uGrid2 = null;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private IRefVoucherRSInwardOutwardService _refVoucherRSInwardOutwardService { get { return IoC.Resolve<IRefVoucherRSInwardOutwardService>(); } }
        bool IsAdd = false;
        #endregion

        #region khởi tạo
        public FMCPaymentDetail(MCPayment mcPayment, IList<MCPayment> dsMcPayment, int statusForm)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();


            #endregion

            #region Thiết lập ban đầu cho Form
            _statusForm = statusForm;
            IsAdd = mcPayment.MCPaymentDetails.Count > 0;
            _select = statusForm == ConstFrm.optStatusForm.Add ? (new MCPayment { TypeID = 110 }) : mcPayment;   //100: phiếu thu, 101: phiếu thu bán hàng, 102: phiếu thu bán hàng đại lý.., 103: phiếu thu tiền khách hàng
            if (IsAdd) _select = mcPayment;
            _listSelects.AddRange(dsMcPayment);
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, _listSelects, statusForm);


            //this.ConfigGuiBase(utmDetailBaseToolBar);
            #endregion

        }
        #endregion

        #region override
        public override void InitializeGUI(MCPayment inputVoucher)
        {
            #region Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            Template mauGiaoDien = Utils.GetMauGiaoDien(inputVoucher.TypeID, inputVoucher.TemplateID, Utils.ListTemplate);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : inputVoucher.TemplateID;
            #endregion

            #endregion

            #region Thiết lập dữ liệu và Fill dữ liệu Obj vào control (nếu đang sửa)

            #region Phần đầu
            grpTop.Text = _select.Type == null ? Utils.ListType.FirstOrDefault(p => p.ID.Equals(_select.TypeID)).TypeName.ToUpper() : _select.Type.TypeName.ToUpper();
            this.ConfigTopVouchersNo<MCPayment>(palTopVouchers, "No", "PostedDate", "Date");
            #endregion
            if (_statusForm == ConstFrm.optStatusForm.Add)
                _select.ID = Guid.NewGuid();

            if (optAccountingObjectType.DataSource != null)
            {
                optAccountingObjectType.CheckedIndex = 0;
            }

            if (inputVoucher.TypeID.Equals(111))
            {
                BindingList<MCPaymentDetail> bdlMcPaymentDetail = new BindingList<MCPaymentDetail>();
                BindingList<MCPaymentDetailSalary> bdlMcPaymentDetailSalary = new BindingList<MCPaymentDetailSalary>();
                if (_statusForm != ConstFrm.optStatusForm.Add)
                {
                    bdlMcPaymentDetail = new BindingList<MCPaymentDetail>(inputVoucher.MCPaymentDetails);
                    bdlMcPaymentDetailSalary = new BindingList<MCPaymentDetailSalary>(inputVoucher.MCPaymentDetailSalarys);
                }
                _listObjectInput = new BindingList<System.Collections.IList> { bdlMcPaymentDetail, bdlMcPaymentDetailSalary };
                this.ConfigGridByTemplete_General<MCPayment>(palGrid, mauGiaoDien);
                this.ConfigGridByTemplete<MCPayment>(inputVoucher.TypeID, mauGiaoDien, true, _listObjectInput);
                optAccountingObjectType.CheckedIndex = 2;
                btnOriginalVoucher.Visible = false;
            }
            else if (inputVoucher.TypeID.Equals(112))
            {
                //BindingList<MCPaymentDetailInsurance> bdlMcPaymentDetailInsurance = new BindingList<MCPaymentDetailInsurance>();
                //if (_statusForm != ConstFrm.optStatusForm.Add)
                //{
                //    bdlMcPaymentDetailInsurance = new BindingList<MCPaymentDetailInsurance>(inputVoucher.MCPaymentDetailInsurances);
                //}
                //_listObjectInput = new BindingList<System.Collections.IList> { bdlMcPaymentDetailInsurance };
                //this.ConfigGridByTemplete_General<MCPayment>(palGrid, mauGiaoDien);
                //this.ConfigGridByTemplete<MCPayment>(inputVoucher.TypeID, mauGiaoDien, true, _listObjectInput);
                BindingList<MCPaymentDetail> bdlMcPaymentDetail = new BindingList<MCPaymentDetail>();//cuongpv sua 12.04.2019 cho phan nop thue
                if (_statusForm != ConstFrm.optStatusForm.Add)
                {
                    bdlMcPaymentDetail = new BindingList<MCPaymentDetail>(inputVoucher.MCPaymentDetails);
                }
                _listObjectInput = new BindingList<System.Collections.IList> { bdlMcPaymentDetail };
                this.ConfigGridByTemplete_General<MCPayment>(palGrid, mauGiaoDien);
                this.ConfigGridByTemplete<MCPayment>(inputVoucher.TypeID, mauGiaoDien, true, _listObjectInput);
                btnOriginalVoucher.Visible = false;

            }
            else if (inputVoucher.TypeID.Equals(113))
            {
                BindingList<MCPaymentDetail> bdlMcPaymentDetail = new BindingList<MCPaymentDetail>();
                BindingList<MCPaymentDetailVendor> bdlMcPaymentDetailVendor = new BindingList<MCPaymentDetailVendor>();
                if (_statusForm != ConstFrm.optStatusForm.Add)
                {
                    bdlMcPaymentDetail = new BindingList<MCPaymentDetail>(inputVoucher.MCPaymentDetails);
                    bdlMcPaymentDetailVendor = new BindingList<MCPaymentDetailVendor>(inputVoucher.MCPaymentDetailVendors);
                }
                _listObjectInput = new BindingList<System.Collections.IList> { bdlMcPaymentDetailVendor, bdlMcPaymentDetail};
                this.ConfigGridByTemplete_General<MCPayment>(palGrid, mauGiaoDien);
                this.ConfigGridByTemplete<MCPayment>(inputVoucher.TypeID, mauGiaoDien, true, _listObjectInput);
                btnOriginalVoucher.Visible = false;
            }
            else if (inputVoucher.TypeID.Equals(114))
            {
                BindingList<MCPaymentDetail> bdlMcPaymentDetail = new BindingList<MCPaymentDetail>();
                BindingList<MCPaymentPersonalIncome> bdlMcPaymentPersonalIncome = new BindingList<MCPaymentPersonalIncome>();
                if (_statusForm != ConstFrm.optStatusForm.Add)
                {
                    bdlMcPaymentDetail = new BindingList<MCPaymentDetail>(inputVoucher.MCPaymentDetails);
                    bdlMcPaymentPersonalIncome = new BindingList<MCPaymentPersonalIncome>(inputVoucher.MCPaymentPersonalIncomes);
                }
                _listObjectInput = new BindingList<System.Collections.IList> { bdlMcPaymentDetail, bdlMcPaymentPersonalIncome };
                this.ConfigGridByTemplete_General<MCPayment>(palGrid, mauGiaoDien);
                this.ConfigGridByTemplete<MCPayment>(inputVoucher.TypeID, mauGiaoDien, true, _listObjectInput);
                btnOriginalVoucher.Visible = false;

            }
            else if (inputVoucher.TypeID.Equals(115))
            {
                BindingList<MCPaymentDetailInsurance> bdlMcPaymentDetailInsurance = new BindingList<MCPaymentDetailInsurance>();
                if (_statusForm != ConstFrm.optStatusForm.Add)
                {
                    bdlMcPaymentDetailInsurance = new BindingList<MCPaymentDetailInsurance>(inputVoucher.MCPaymentDetailInsurances);
                }
                _listObjectInput = new BindingList<System.Collections.IList> { bdlMcPaymentDetailInsurance };
                this.ConfigGridByTemplete_General<MCPayment>(palGrid, mauGiaoDien);
                this.ConfigGridByTemplete<MCPayment>(inputVoucher.TypeID, mauGiaoDien, true, _listObjectInput);
                btnOriginalVoucher.Visible = false;
            }
            else if (inputVoucher.TypeID.Equals(116))
            {
                BindingList<MCPaymentDetail> bdlMcPaymentDetail = new BindingList<MCPaymentDetail>();
                BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
                if (_statusForm != ConstFrm.optStatusForm.Add)
                {
                    bdlMcPaymentDetail = new BindingList<MCPaymentDetail>(inputVoucher.MCPaymentDetails);

                }
                BindingList<PPServiceDetail> bdlPPServiceDetail = new BindingList<PPServiceDetail>();
                _selectJoin = new PPService();
                _selectJoin = IPPServiceService.Getbykey(inputVoucher.ID);
                IPPServiceService.UnbindSession(_selectJoin);
                _selectJoin = IPPServiceService.Getbykey(inputVoucher.ID);
                foreach (var x in ((PPService)_selectJoin).PPServiceDetails)
                {
                    x.Amount = x.Amount - x.DiscountAmount;
                    x.AmountOriginal = x.AmountOriginal - x.DiscountAmountOriginal;
                }
                if (_selectJoin != null)
                {
                    bdlPPServiceDetail = new BindingList<PPServiceDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                          ? new List<PPServiceDetail>()
                                                                          : (((PPService)_selectJoin).PPServiceDetails ?? new List<PPServiceDetail>()));
                    bdlRefVoucher = new BindingList<RefVoucher>(((PPService)_selectJoin).RefVouchers);

                    _listObjectInput = new BindingList<System.Collections.IList> { bdlPPServiceDetail, bdlMcPaymentDetail };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                    this.ConfigGridByTemplete_General<MCPayment>(palGrid, mauGiaoDien);
                    //this.ConfigGridByTemplete<MCPayment>(inputVoucher.TypeID, mauGiaoDien, true, _listObjectInput);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
                    this.ConfigGridByManyTemplete<MCPayment>(inputVoucher.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    uGrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
                    uGrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    uGrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    uGrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                }
                btnOriginalVoucher.Visible = false;
            }
            else if (inputVoucher.TypeID.Equals(117))
            {
                BindingList<MCPaymentDetail> bdlMcPaymentDetail = new BindingList<MCPaymentDetail>();
                BindingList<PPInvoiceDetail> bdlPPInvoiceDetail = new BindingList<PPInvoiceDetail>();
                BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
                if (_statusForm != ConstFrm.optStatusForm.Add)
                {
                    bdlMcPaymentDetail = new BindingList<MCPaymentDetail>(inputVoucher.MCPaymentDetails);

                }
                _selectJoin = new PPInvoice();
                _selectJoin = IPPInvoiceService.Getbykey(inputVoucher.ID);
                if (_selectJoin != null)
                {
                    bdlPPInvoiceDetail = new BindingList<PPInvoiceDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                          ? new List<PPInvoiceDetail>()
                                                                          : (((PPInvoice)_selectJoin).PPInvoiceDetails ?? new List<PPInvoiceDetail>()));
                    bdlRefVoucher = new BindingList<RefVoucher>(((PPInvoice)_selectJoin).RefVouchers);

                    _listObjectInput = new BindingList<System.Collections.IList> { bdlMcPaymentDetail, bdlPPInvoiceDetail };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                    this.ConfigGridByTemplete_General<MCPayment>(palGrid, mauGiaoDien);
                    //this.ConfigGridByTemplete<MCPayment>(inputVoucher.TypeID, mauGiaoDien, true, _listObjectInput);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
                    this.ConfigGridByManyTemplete<MCPayment>(inputVoucher.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    uGrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
                    uGrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    uGrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    uGrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                }
                btnOriginalVoucher.Visible = false;
            }
            else if (inputVoucher.TypeID.Equals(118))
            {
                BindingList<MCPaymentDetail> bdlMcPaymentDetail = new BindingList<MCPaymentDetail>();
                BindingList<MCPaymentDetailVendor> bdlMcPaymentDetailVendor = new BindingList<MCPaymentDetailVendor>();
                if (_statusForm != ConstFrm.optStatusForm.Add || IsAdd)
                {
                    bdlMcPaymentDetail = new BindingList<MCPaymentDetail>(inputVoucher.MCPaymentDetails);
                    bdlMcPaymentDetailVendor = new BindingList<MCPaymentDetailVendor>(inputVoucher.MCPaymentDetailVendors);
                }
                _listObjectInput = new BindingList<System.Collections.IList> { bdlMcPaymentDetailVendor, bdlMcPaymentDetail };
                this.ConfigGridByTemplete_General<MCPayment>(palGrid, mauGiaoDien);
                this.ConfigGridByTemplete<MCPayment>(inputVoucher.TypeID, mauGiaoDien, true, _listObjectInput, isForeignCurrency: (_select.CurrencyID == "VND" ? false : true));
                btnOriginalVoucher.Visible = false;

                uGrid0 = (UltraGrid)palGrid.Controls.Find("uGrid0", true).FirstOrDefault();
                UltraGrid uGrid1 = (UltraGrid)palGrid.Controls.Find("uGrid1", true).FirstOrDefault();
                uGrid0.SummaryValueChanged += uGrid0_SummaryValueChanged;
                optAccountingObjectType.CheckedIndex = 1;
                if (_select.CurrencyID != "VND")
                {
                    uGrid0.DisplayLayout.Bands[0].Columns["DiscountAmountOriginal"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                    uGrid0.DisplayLayout.Bands[0].Columns["PayableAmountOriginal"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                    uGrid0.DisplayLayout.Bands[0].Columns["RemainingAmountOriginal"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                    uGrid0.DisplayLayout.Bands[0].Columns["AmountOriginal"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                    uGrid1.DisplayLayout.Bands[0].Columns["AmountOriginal"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                }
            }
            else if (inputVoucher.TypeID.Equals(119))
            {
                BindingList<MCPaymentDetail> bdlMcPaymentDetail = new BindingList<MCPaymentDetail>();
                BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
                if (_statusForm != ConstFrm.optStatusForm.Add)
                {
                    bdlMcPaymentDetail = new BindingList<MCPaymentDetail>(inputVoucher.MCPaymentDetails);

                }
                BindingList<FAIncrementDetail> bdlFAIncrementDetail = new BindingList<FAIncrementDetail>();
                _selectJoin = new FAIncrement();
                _selectJoin = IFAIncrementService.Getbykey(inputVoucher.ID);
                if (_selectJoin != null)
                {
                    bdlFAIncrementDetail = new BindingList<FAIncrementDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                          ? new List<FAIncrementDetail>()
                                                                          : (((FAIncrement)_selectJoin).FAIncrementDetails ?? new List<FAIncrementDetail>()));
                    bdlRefVoucher = new BindingList<RefVoucher>(((FAIncrement)_selectJoin).RefVouchers);

                    _listObjectInput = new BindingList<System.Collections.IList> { bdlMcPaymentDetail, bdlFAIncrementDetail };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                    this.ConfigGridByTemplete_General<MCPayment>(palGrid, mauGiaoDien);
                    //this.ConfigGridByTemplete<MCPayment>(inputVoucher.TypeID, mauGiaoDien, true, _listObjectInput);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
                    this.ConfigGridByManyTemplete<MCPayment>(inputVoucher.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    uGrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
                    uGrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    uGrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    uGrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                }
                btnOriginalVoucher.Visible = false;
            }
            else if (inputVoucher.TypeID.Equals(902))
            {
                BindingList<MCPaymentDetail> bdlMcPaymentDetail = new BindingList<MCPaymentDetail>();
                BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
                if (_statusForm != ConstFrm.optStatusForm.Add)
                {
                    bdlMcPaymentDetail = new BindingList<MCPaymentDetail>(inputVoucher.MCPaymentDetails);

                }
                BindingList<TIIncrementDetail> bdlFAIncrementDetail = new BindingList<TIIncrementDetail>();
                _selectJoin = new TIIncrement();
                _selectJoin = ITIIncrementService.Getbykey(inputVoucher.ID);
                if (_selectJoin != null)
                {
                    bdlFAIncrementDetail = new BindingList<TIIncrementDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                          ? new List<TIIncrementDetail>()
                                                                          : (((TIIncrement)_selectJoin).TIIncrementDetails ?? new List<TIIncrementDetail>()));
                    bdlRefVoucher = new BindingList<RefVoucher>(((TIIncrement)_selectJoin).RefVouchers);

                    _listObjectInput = new BindingList<System.Collections.IList> { bdlMcPaymentDetail, bdlFAIncrementDetail };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                    this.ConfigGridByTemplete_General<MCPayment>(palGrid, mauGiaoDien);
                    //this.ConfigGridByTemplete<MCPayment>(inputVoucher.TypeID, mauGiaoDien, true, _listObjectInput);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, true, true, false };
                    this.ConfigGridByManyTemplete<MCPayment>(inputVoucher.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    uGrid2 = Controls.Find("uGrid4", true).FirstOrDefault() as UltraGrid;
                    uGrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    uGrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    uGrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                    var ultraTabControl = (UltraTabControl)palGrid.Controls.Find("ultraTabControl", true).FirstOrDefault();
                    ultraTabControl.Tabs[3].Visible = false;
                    ultraTabControl.Tabs[4].Text = "4. Chứng từ tham chiếu";
                }
                btnOriginalVoucher.Visible = false;
            }
            else
            {
                BindingList<MCPaymentDetail> bdlMcPaymentDetail = new BindingList<MCPaymentDetail>();
                BindingList<MCPaymentDetailTax> bdlMcPaymentDetailTax = new BindingList<MCPaymentDetailTax>();
                BindingList<RefVoucherRSInwardOutward> bdlRefVoucher = new BindingList<RefVoucherRSInwardOutward>();
                if (_statusForm != ConstFrm.optStatusForm.Add)
                {
                    bdlMcPaymentDetail = new BindingList<MCPaymentDetail>(inputVoucher.MCPaymentDetails);
                    bdlMcPaymentDetailTax = new BindingList<MCPaymentDetailTax>(inputVoucher.MCPaymentDetailTaxs);
                    bdlRefVoucher = new BindingList<RefVoucherRSInwardOutward>(inputVoucher.RefVoucherRSInwardOutwards);
                }
                _listObjectInput = new BindingList<System.Collections.IList> { bdlMcPaymentDetail, bdlMcPaymentDetailTax };
                _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                this.ConfigGridByTemplete_General<MCPayment>(palGrid, mauGiaoDien);
                //this.ConfigGridByTemplete<MCPayment>(inputVoucher.TypeID, mauGiaoDien, true, _listObjectInput);
                List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInputPost };
                List<Boolean> manyStandard = new List<Boolean>() { true, true, false };
                this.ConfigGridByManyTemplete<MCPayment>(inputVoucher.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                uGrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
                uGrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                uGrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                uGrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                btnOriginalVoucher.Visible = true;//add by cuongpv
            }
            if(_statusForm!=ConstFrm.optStatusForm.Add)
            {
                optAccountingObjectType.CheckedIndex = _select.AccountingObjectType??0;
            }
            if (uGrid2 != null)
                uGrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            //Cấu hình Grid động
            _select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : inputVoucher.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : inputVoucher.TotalAmount;
            _select.CurrencyID = (_statusForm == ConstFrm.optStatusForm.Add && inputVoucher.TypeID != 118) ? "VND" : inputVoucher.CurrencyID;



            List<MCPayment> inputCurrency = new List<MCPayment> { _select };
            this.ConfigGridCurrencyByTemplate<MCPayment>(_select.TypeID, new BindingList<System.Collections.IList> { inputCurrency },
                                              uGridControl, mauGiaoDien);
            if (_select.TypeID == 118)
            {
                if (_select.CurrencyID != "VND")
                {
                    uGridControl.DisplayLayout.Bands[0].Columns["TotalAllOriginal"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                    uGridControl.DisplayLayout.Bands[0].Columns["TotalAllOriginal"].Hidden = false;
                    uGridControl.DisplayLayout.Bands[0].Columns["TotalAll"].Hidden = false;
                    uGridControl.DisplayLayout.Bands[0].Columns["ExchangeRate"].Hidden = false;
                }
                else
                {
                    uGridControl.DisplayLayout.Bands[0].Columns["TotalAllOriginal"].FormatNumberic(ConstDatabase.Format_TienVND);
                    uGridControl.DisplayLayout.Bands[0].Columns["TotalAllOriginal"].Hidden = false;
                    uGridControl.DisplayLayout.Bands[0].Columns["TotalAll"].Hidden = true;
                    uGridControl.DisplayLayout.Bands[0].Columns["ExchangeRate"].Hidden = true;
                }
            }
            _select.AccountingObjectType = _statusForm.Equals(ConstFrm.optStatusForm.Add)
                                               ? 0
                                               : _select.AccountingObjectType;

            this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectCode", "ID", _select, "AccountingObjectID");



            var listAutoPrinciple = Utils.IAutoPrincipleService.getByTypeID(_select.TypeID);
            var bindinglistAutoPrinciple = new BindingList<AutoPrinciple>(listAutoPrinciple);
            this.ConfigCombo(bindinglistAutoPrinciple, cbbReason, "AutoPrincipleName", "ID", null);

            DataBinding(new List<Control> { optAccountingObjectType }, "AccountingObjectType", "4");

            GetDataComboByOption(optAccountingObjectType);

            this.ConfigReasonCombo<MCPayment>(_select.TypeID, _statusForm, cbbReason, txtReason, "AutoPrincipleName", "ID", "txtNumberAttach");

            DataBinding(new List<Control> { txtAccountingObjectAddress, txtAccountingObjectName, txtReceiver, txtReason, txtNumberAttach },
                "AccountingObjectAddress,AccountingObjectName,Receiver,Reason,NumberAttach", "0,0,0,0,0");
            Utils.ConfigColumnByCurrency(this, Utils.ListCurrency.FirstOrDefault(x => x.ID == inputVoucher.CurrencyID));

            #endregion
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (_select.TypeID == 110)//trungnq thêm vào để  làm task 6234
                {
                    txtReason.Value = "Chi tiền mặt";
                    txtReason.Text = "Chi tiền mặt";
                    _select.Reason = "Chi tiền mặt";
                };
            }
        }

        #endregion

        #region nghiệp vụ
        #endregion

        #region Event

        private void uGrid0_SummaryValueChanged(object sender, SummaryValueChangedEventArgs e)
        {
            var columns = uGrid0.DisplayLayout.Bands[0].Columns;
            if (columns.Exists("AmountOriginal") && columns.Exists("SumAmountOriginal") && columns.Exists("TotalAmountOriginal") && columns.Exists("SumDiscountAmountOriginal"))
            {
                if (Convert.ToDecimal(uGrid0.Rows.SummaryValues["SumAmountOriginal"].Value.ToString()) != 0)
                {
                    uGridControl.Rows[0].Cells["TotalAllOriginal"].Value =
                        Convert.ToDecimal(uGrid0.Rows.SummaryValues["SumAmountOriginal"].Value.ToString()) -
                        Convert.ToDecimal(uGrid0.Rows.SummaryValues["SumDiscountAmountOriginal"].Value.ToString());
                }
            }
        }
        #endregion
        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucherRSInwardOutward> datasource = (BindingList<RefVoucherRSInwardOutward>)uGrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucherRSInwardOutward>();
                    var f = new FViewVoucher(_select.CurrencyID, datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                

            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucher)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucherRSInwardOutward>)uGrid2.DataSource;
                    foreach (var item in f.RefVoucherRSInwardOutward)
                    {
                        source.Add(new RefVoucherRSInwardOutward
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                if (!new int[] { 116, 117, 119, 902 }.Contains(_select.TypeID))
                {
                    RefVoucherRSInwardOutward temp = (RefVoucherRSInwardOutward)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                    if (temp != null)
                        editFuntion(temp);
                }
                else
                {
                    RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                    if (temp != null)
                        editFuntion(temp);
                }

            }
        }

        private void editFuntion(RefVoucherRSInwardOutward temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }
        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }
        private void cbbAccountingObjectID_ValueChanged(object sender, EventArgs e)
        {
            //txtAccountingObjectName.Value = Utils.GetAccountingObjectName(cbbAccountingObjectID.Text);
            //txtAccountingObjectAddress.Value = Utils.GetAccountingObjectAddress(cbbAccountingObjectID.Text);
        }

        private void cbbAccountingObjectID_TextChanged(object sender, EventArgs e)
        {
            //Guid s = ((AccountingObject)Utils.getSelectCbbItem(cbbAccountingObjectID)).ID;
            //txtAccountingObjectName.Value = Utils.GetAccountingObjectName(s);
            //txtAccountingObjectAddress.Value = Utils.GetAccountingObjectAddress(s);
        }

        private void FMCPaymentDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        private void optAccountingObjectType_ValueChanged(object sender, EventArgs e)
        {
            if (_statusForm == ConstFrm.optStatusForm.Edit)
            {
                _select.AccountingObjectType = optAccountingObjectType.CheckedIndex;
            }
        }
    }

    public class FmcPaymentDetailStand : DetailBase<MCPayment>
    {
    }
}