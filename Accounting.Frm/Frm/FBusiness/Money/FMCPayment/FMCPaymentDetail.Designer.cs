﻿namespace Accounting
{
    sealed partial class FMCPaymentDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            this.ugbThongTinChung = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbReason = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtNumberAttach = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblNumberAttach = new Infragistics.Win.Misc.UltraLabel();
            this.lblReason = new Infragistics.Win.Misc.UltraLabel();
            this.txtReceiver = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblReceiver = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectAddress = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObjectID = new Infragistics.Win.Misc.UltraLabel();
            this.palTop = new System.Windows.Forms.Panel();
            this.grpTop = new Infragistics.Win.Misc.UltraGroupBox();
            this.optAccountingObjectType = new Accounting.UltraOptionSet_Ex();
            this.palTopVouchers = new Infragistics.Win.Misc.UltraPanel();
            this.palGrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.palThongTinChung = new Infragistics.Win.Misc.UltraPanel();
            this.palBottom = new System.Windows.Forms.Panel();
            this.uGridControl = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.palFill = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.ugbThongTinChung)).BeginInit();
            this.ugbThongTinChung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberAttach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).BeginInit();
            this.palTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpTop)).BeginInit();
            this.grpTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optAccountingObjectType)).BeginInit();
            this.palTopVouchers.SuspendLayout();
            this.palGrid.ClientArea.SuspendLayout();
            this.palGrid.SuspendLayout();
            this.palThongTinChung.ClientArea.SuspendLayout();
            this.palThongTinChung.SuspendLayout();
            this.palBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).BeginInit();
            this.palFill.SuspendLayout();
            this.SuspendLayout();
            // 
            // ugbThongTinChung
            // 
            this.ugbThongTinChung.Controls.Add(this.txtReason);
            this.ugbThongTinChung.Controls.Add(this.txtAccountingObjectName);
            this.ugbThongTinChung.Controls.Add(this.cbbReason);
            this.ugbThongTinChung.Controls.Add(this.ultraLabel1);
            this.ugbThongTinChung.Controls.Add(this.cbbAccountingObjectID);
            this.ugbThongTinChung.Controls.Add(this.txtNumberAttach);
            this.ugbThongTinChung.Controls.Add(this.lblNumberAttach);
            this.ugbThongTinChung.Controls.Add(this.lblReason);
            this.ugbThongTinChung.Controls.Add(this.txtReceiver);
            this.ugbThongTinChung.Controls.Add(this.lblReceiver);
            this.ugbThongTinChung.Controls.Add(this.txtAccountingObjectAddress);
            this.ugbThongTinChung.Controls.Add(this.lblAccountingObjectAddress);
            this.ugbThongTinChung.Controls.Add(this.lblAccountingObjectID);
            this.ugbThongTinChung.Dock = System.Windows.Forms.DockStyle.Top;
            appearance16.FontData.BoldAsString = "True";
            appearance16.FontData.SizeInPoints = 10F;
            this.ugbThongTinChung.HeaderAppearance = appearance16;
            this.ugbThongTinChung.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ugbThongTinChung.Location = new System.Drawing.Point(0, 0);
            this.ugbThongTinChung.Name = "ugbThongTinChung";
            this.ugbThongTinChung.Size = new System.Drawing.Size(685, 155);
            this.ugbThongTinChung.TabIndex = 26;
            this.ugbThongTinChung.Text = "Thông tin chung";
            this.ugbThongTinChung.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.TextVAlignAsString = "Middle";
            this.txtReason.Appearance = appearance1;
            this.txtReason.AutoSize = false;
            this.txtReason.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReason.Location = new System.Drawing.Point(262, 98);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(410, 22);
            this.txtReason.TabIndex = 3;
            // 
            // txtAccountingObjectName
            // 
            this.txtAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.TextVAlignAsString = "Middle";
            this.txtAccountingObjectName.Appearance = appearance2;
            this.txtAccountingObjectName.AutoSize = false;
            this.txtAccountingObjectName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectName.Location = new System.Drawing.Point(262, 26);
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(410, 22);
            this.txtAccountingObjectName.TabIndex = 3;
            // 
            // cbbReason
            // 
            appearance3.TextVAlignAsString = "Middle";
            this.cbbReason.Appearance = appearance3;
            this.cbbReason.AutoSize = false;
            this.cbbReason.Location = new System.Drawing.Point(85, 98);
            this.cbbReason.Name = "cbbReason";
            this.cbbReason.NullText = "<chọn dữ liệu>";
            this.cbbReason.Size = new System.Drawing.Size(171, 22);
            this.cbbReason.TabIndex = 2;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel1.Appearance = appearance4;
            this.ultraLabel1.Location = new System.Drawing.Point(596, 123);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(80, 19);
            this.ultraLabel1.TabIndex = 33;
            this.ultraLabel1.Text = "Chứng từ gốc";
            // 
            // cbbAccountingObjectID
            // 
            appearance5.TextVAlignAsString = "Middle";
            this.cbbAccountingObjectID.Appearance = appearance5;
            this.cbbAccountingObjectID.AutoSize = false;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance6.BackHatchStyle = Infragistics.Win.BackHatchStyle.None;
            appearance6.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance6.ImageBackgroundAlpha = Infragistics.Win.Alpha.Transparent;
            this.cbbAccountingObjectID.ButtonAppearance = appearance6;
            appearance7.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton1.Appearance = appearance7;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectID.ButtonsRight.Add(editorButton1);
            this.cbbAccountingObjectID.Location = new System.Drawing.Point(85, 26);
            this.cbbAccountingObjectID.Name = "cbbAccountingObjectID";
            this.cbbAccountingObjectID.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID.Size = new System.Drawing.Size(171, 22);
            this.cbbAccountingObjectID.TabIndex = 2;
            this.cbbAccountingObjectID.ValueChanged += new System.EventHandler(this.cbbAccountingObjectID_ValueChanged);
            this.cbbAccountingObjectID.TextChanged += new System.EventHandler(this.cbbAccountingObjectID_TextChanged);
            // 
            // txtNumberAttach
            // 
            this.txtNumberAttach.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.TextVAlignAsString = "Middle";
            this.txtNumberAttach.Appearance = appearance8;
            this.txtNumberAttach.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtNumberAttach.Location = new System.Drawing.Point(85, 122);
            this.txtNumberAttach.Name = "txtNumberAttach";
            this.txtNumberAttach.Size = new System.Drawing.Size(505, 21);
            this.txtNumberAttach.TabIndex = 7;
            // 
            // lblNumberAttach
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextHAlignAsString = "Left";
            appearance9.TextVAlignAsString = "Middle";
            this.lblNumberAttach.Appearance = appearance9;
            this.lblNumberAttach.Location = new System.Drawing.Point(11, 122);
            this.lblNumberAttach.Name = "lblNumberAttach";
            this.lblNumberAttach.Size = new System.Drawing.Size(68, 22);
            this.lblNumberAttach.TabIndex = 28;
            this.lblNumberAttach.Text = "Kèm theo";
            // 
            // lblReason
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextHAlignAsString = "Left";
            appearance10.TextVAlignAsString = "Middle";
            this.lblReason.Appearance = appearance10;
            this.lblReason.Location = new System.Drawing.Point(11, 98);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(68, 22);
            this.lblReason.TabIndex = 26;
            this.lblReason.Text = "Lý do chi";
            // 
            // txtReceiver
            // 
            this.txtReceiver.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance11.TextVAlignAsString = "Middle";
            this.txtReceiver.Appearance = appearance11;
            this.txtReceiver.AutoSize = false;
            this.txtReceiver.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReceiver.Location = new System.Drawing.Point(85, 74);
            this.txtReceiver.Name = "txtReceiver";
            this.txtReceiver.Size = new System.Drawing.Size(587, 22);
            this.txtReceiver.TabIndex = 5;
            // 
            // lblReceiver
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.TextHAlignAsString = "Left";
            appearance12.TextVAlignAsString = "Middle";
            this.lblReceiver.Appearance = appearance12;
            this.lblReceiver.Location = new System.Drawing.Point(11, 74);
            this.lblReceiver.Name = "lblReceiver";
            this.lblReceiver.Size = new System.Drawing.Size(68, 22);
            this.lblReceiver.TabIndex = 24;
            this.lblReceiver.Text = "Người nhận";
            // 
            // txtAccountingObjectAddress
            // 
            this.txtAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance13.TextVAlignAsString = "Middle";
            this.txtAccountingObjectAddress.Appearance = appearance13;
            this.txtAccountingObjectAddress.AutoSize = false;
            this.txtAccountingObjectAddress.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectAddress.Location = new System.Drawing.Point(85, 50);
            this.txtAccountingObjectAddress.Name = "txtAccountingObjectAddress";
            this.txtAccountingObjectAddress.Size = new System.Drawing.Size(587, 22);
            this.txtAccountingObjectAddress.TabIndex = 4;
            // 
            // lblAccountingObjectAddress
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextHAlignAsString = "Left";
            appearance14.TextVAlignAsString = "Middle";
            this.lblAccountingObjectAddress.Appearance = appearance14;
            this.lblAccountingObjectAddress.Location = new System.Drawing.Point(12, 50);
            this.lblAccountingObjectAddress.Name = "lblAccountingObjectAddress";
            this.lblAccountingObjectAddress.Size = new System.Drawing.Size(68, 22);
            this.lblAccountingObjectAddress.TabIndex = 22;
            this.lblAccountingObjectAddress.Text = "Địa chỉ";
            // 
            // lblAccountingObjectID
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextHAlignAsString = "Left";
            appearance15.TextVAlignAsString = "Middle";
            this.lblAccountingObjectID.Appearance = appearance15;
            this.lblAccountingObjectID.Location = new System.Drawing.Point(11, 26);
            this.lblAccountingObjectID.Name = "lblAccountingObjectID";
            this.lblAccountingObjectID.Size = new System.Drawing.Size(68, 22);
            this.lblAccountingObjectID.TabIndex = 0;
            this.lblAccountingObjectID.Text = "Đối tượng";
            // 
            // palTop
            // 
            this.palTop.Controls.Add(this.grpTop);
            this.palTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTop.Location = new System.Drawing.Point(0, 0);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(685, 75);
            this.palTop.TabIndex = 27;
            // 
            // grpTop
            // 
            this.grpTop.Controls.Add(this.optAccountingObjectType);
            this.grpTop.Controls.Add(this.palTopVouchers);
            this.grpTop.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance19.FontData.BoldAsString = "True";
            appearance19.FontData.SizeInPoints = 13F;
            this.grpTop.HeaderAppearance = appearance19;
            this.grpTop.Location = new System.Drawing.Point(0, 0);
            this.grpTop.Name = "grpTop";
            this.grpTop.Size = new System.Drawing.Size(685, 75);
            this.grpTop.TabIndex = 31;
            this.grpTop.Text = "PHIẾU CHI";
            this.grpTop.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // optAccountingObjectType
            // 
            this.optAccountingObjectType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextHAlignAsString = "Center";
            appearance17.TextVAlignAsString = "Middle";
            this.optAccountingObjectType.Appearance = appearance17;
            this.optAccountingObjectType.BackColor = System.Drawing.Color.Transparent;
            this.optAccountingObjectType.BackColorInternal = System.Drawing.Color.Transparent;
            this.optAccountingObjectType.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.optAccountingObjectType.CheckedIndex = 0;
            valueListItem3.DataValue = "Default Item";
            valueListItem3.DisplayText = "Khách hàng";
            valueListItem5.DataValue = "ValueListItem1";
            valueListItem5.DisplayText = "Nhà cung cấp";
            valueListItem6.DataValue = "ValueListItem2";
            valueListItem6.DisplayText = "Nhân viên";
            valueListItem1.DataValue = "ValueListItem3";
            valueListItem1.DisplayText = "Khác";
            this.optAccountingObjectType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem5,
            valueListItem6,
            valueListItem1});
            this.optAccountingObjectType.Location = new System.Drawing.Point(376, 38);
            this.optAccountingObjectType.Name = "optAccountingObjectType";
            this.optAccountingObjectType.ReadOnly = false;
            this.optAccountingObjectType.Size = new System.Drawing.Size(297, 22);
            this.optAccountingObjectType.TabIndex = 1;
            this.optAccountingObjectType.Text = "Khách hàng";
            this.optAccountingObjectType.ValueChanged += new System.EventHandler(this.optAccountingObjectType_ValueChanged);
            // 
            // palTopVouchers
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            this.palTopVouchers.Appearance = appearance18;
            this.palTopVouchers.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.palTopVouchers.Location = new System.Drawing.Point(11, 21);
            this.palTopVouchers.Name = "palTopVouchers";
            this.palTopVouchers.Size = new System.Drawing.Size(315, 42);
            this.palTopVouchers.TabIndex = 0;
            // 
            // palGrid
            // 
            // 
            // palGrid.ClientArea
            // 
            this.palGrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.palGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palGrid.Location = new System.Drawing.Point(0, 165);
            this.palGrid.Name = "palGrid";
            this.palGrid.Size = new System.Drawing.Size(685, 77);
            this.palGrid.TabIndex = 8;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance20.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance20;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(586, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 6;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 155);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 155;
            this.ultraSplitter1.Size = new System.Drawing.Size(685, 10);
            this.ultraSplitter1.TabIndex = 1;
            // 
            // palThongTinChung
            // 
            // 
            // palThongTinChung.ClientArea
            // 
            this.palThongTinChung.ClientArea.Controls.Add(this.ugbThongTinChung);
            this.palThongTinChung.Dock = System.Windows.Forms.DockStyle.Top;
            this.palThongTinChung.Location = new System.Drawing.Point(0, 0);
            this.palThongTinChung.Name = "palThongTinChung";
            this.palThongTinChung.Size = new System.Drawing.Size(685, 155);
            this.palThongTinChung.TabIndex = 0;
            // 
            // palBottom
            // 
            this.palBottom.Controls.Add(this.uGridControl);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 317);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(685, 73);
            this.palBottom.TabIndex = 27;
            // 
            // uGridControl
            // 
            this.uGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridControl.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uGridControl.Location = new System.Drawing.Point(376, 6);
            this.uGridControl.Name = "uGridControl";
            this.uGridControl.Size = new System.Drawing.Size(309, 64);
            this.uGridControl.TabIndex = 9;
            this.uGridControl.Text = "ultraGrid1";
            this.uGridControl.Visible = false;
            // 
            // palFill
            // 
            this.palFill.Controls.Add(this.palGrid);
            this.palFill.Controls.Add(this.ultraSplitter1);
            this.palFill.Controls.Add(this.palThongTinChung);
            this.palFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palFill.Location = new System.Drawing.Point(0, 75);
            this.palFill.Name = "palFill";
            this.palFill.Size = new System.Drawing.Size(685, 242);
            this.palFill.TabIndex = 28;
            // 
            // FMCPaymentDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(685, 390);
            this.Controls.Add(this.palFill);
            this.Controls.Add(this.palBottom);
            this.Controls.Add(this.palTop);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FMCPaymentDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FMCPaymentDetail_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ugbThongTinChung)).EndInit();
            this.ugbThongTinChung.ResumeLayout(false);
            this.ugbThongTinChung.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberAttach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).EndInit();
            this.palTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpTop)).EndInit();
            this.grpTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optAccountingObjectType)).EndInit();
            this.palTopVouchers.ResumeLayout(false);
            this.palGrid.ClientArea.ResumeLayout(false);
            this.palGrid.ResumeLayout(false);
            this.palThongTinChung.ClientArea.ResumeLayout(false);
            this.palThongTinChung.ResumeLayout(false);
            this.palBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).EndInit();
            this.palFill.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ugbThongTinChung;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectAddress;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNumberAttach;
        private Infragistics.Win.Misc.UltraLabel lblNumberAttach;
        private Infragistics.Win.Misc.UltraLabel lblReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReceiver;
        private Infragistics.Win.Misc.UltraLabel lblReceiver;
        private System.Windows.Forms.Panel palTop;
        private Infragistics.Win.Misc.UltraGroupBox grpTop;
        private System.Windows.Forms.Panel palBottom;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridControl;
        private Infragistics.Win.Misc.UltraPanel palGrid;
        private Infragistics.Win.Misc.UltraPanel palThongTinChung;
        private Infragistics.Win.Misc.UltraPanel palTopVouchers;
        private UltraOptionSet_Ex optAccountingObjectType;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private System.Windows.Forms.Panel palFill;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
    }
}
