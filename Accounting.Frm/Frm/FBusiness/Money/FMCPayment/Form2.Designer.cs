﻿namespace Accounting.Frm.Frm.FBusiness.FMCPayment
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPostedDate = new System.Windows.Forms.Label();
            this.lblTypeID = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNo = new System.Windows.Forms.Label();
            this.lblTotalAmount = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblNumberAttach = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblAccountingObjectName = new System.Windows.Forms.Label();
            this.lblReason = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblAccountingObjectAddress = new System.Windows.Forms.Label();
            this.lblPayers = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridPosted = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl1.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPosted)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.label1);
            this.ultraTabPageControl1.Controls.Add(this.lblPostedDate);
            this.ultraTabPageControl1.Controls.Add(this.lblTypeID);
            this.ultraTabPageControl1.Controls.Add(this.label10);
            this.ultraTabPageControl1.Controls.Add(this.label2);
            this.ultraTabPageControl1.Controls.Add(this.lblNo);
            this.ultraTabPageControl1.Controls.Add(this.lblTotalAmount);
            this.ultraTabPageControl1.Controls.Add(this.label7);
            this.ultraTabPageControl1.Controls.Add(this.label3);
            this.ultraTabPageControl1.Controls.Add(this.lblDate);
            this.ultraTabPageControl1.Controls.Add(this.lblNumberAttach);
            this.ultraTabPageControl1.Controls.Add(this.label8);
            this.ultraTabPageControl1.Controls.Add(this.label6);
            this.ultraTabPageControl1.Controls.Add(this.lblAccountingObjectName);
            this.ultraTabPageControl1.Controls.Add(this.lblReason);
            this.ultraTabPageControl1.Controls.Add(this.label9);
            this.ultraTabPageControl1.Controls.Add(this.label5);
            this.ultraTabPageControl1.Controls.Add(this.lblAccountingObjectAddress);
            this.ultraTabPageControl1.Controls.Add(this.lblPayers);
            this.ultraTabPageControl1.Controls.Add(this.label4);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(780, 112);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(9, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ngày hoạch toán";
            // 
            // lblPostedDate
            // 
            this.lblPostedDate.AutoSize = true;
            this.lblPostedDate.BackColor = System.Drawing.Color.Transparent;
            this.lblPostedDate.Location = new System.Drawing.Point(110, 7);
            this.lblPostedDate.Name = "lblPostedDate";
            this.lblPostedDate.Size = new System.Drawing.Size(10, 13);
            this.lblPostedDate.TabIndex = 10;
            this.lblPostedDate.Text = ":";
            // 
            // lblTypeID
            // 
            this.lblTypeID.AutoSize = true;
            this.lblTypeID.BackColor = System.Drawing.Color.Transparent;
            this.lblTypeID.Location = new System.Drawing.Point(110, 211);
            this.lblTypeID.Name = "lblTypeID";
            this.lblTypeID.Size = new System.Drawing.Size(10, 13);
            this.lblTypeID.TabIndex = 19;
            this.lblTypeID.Text = ":";
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(9, 211);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 17);
            this.label10.TabIndex = 9;
            this.label10.Text = "Loại phiếu chi";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(9, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Số chứng từ";
            // 
            // lblNo
            // 
            this.lblNo.AutoSize = true;
            this.lblNo.BackColor = System.Drawing.Color.Transparent;
            this.lblNo.Location = new System.Drawing.Point(110, 31);
            this.lblNo.Name = "lblNo";
            this.lblNo.Size = new System.Drawing.Size(10, 13);
            this.lblNo.TabIndex = 11;
            this.lblNo.Text = ":";
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.AutoSize = true;
            this.lblTotalAmount.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalAmount.Location = new System.Drawing.Point(111, 190);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(10, 13);
            this.lblTotalAmount.TabIndex = 18;
            this.lblTotalAmount.Text = ":";
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(10, 190);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 17);
            this.label7.TabIndex = 8;
            this.label7.Text = "Số tiền";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(10, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ngày chứng từ";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.Location = new System.Drawing.Point(111, 54);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(10, 13);
            this.lblDate.TabIndex = 12;
            this.lblDate.Text = ":";
            // 
            // lblNumberAttach
            // 
            this.lblNumberAttach.AutoSize = true;
            this.lblNumberAttach.BackColor = System.Drawing.Color.Transparent;
            this.lblNumberAttach.Location = new System.Drawing.Point(110, 169);
            this.lblNumberAttach.Name = "lblNumberAttach";
            this.lblNumberAttach.Size = new System.Drawing.Size(10, 13);
            this.lblNumberAttach.TabIndex = 17;
            this.lblNumberAttach.Text = ":";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(9, 169);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "Kèm theo";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(10, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 17);
            this.label6.TabIndex = 3;
            this.label6.Text = "Đối tượng";
            // 
            // lblAccountingObjectName
            // 
            this.lblAccountingObjectName.AutoSize = true;
            this.lblAccountingObjectName.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectName.Location = new System.Drawing.Point(111, 76);
            this.lblAccountingObjectName.Name = "lblAccountingObjectName";
            this.lblAccountingObjectName.Size = new System.Drawing.Size(10, 13);
            this.lblAccountingObjectName.TabIndex = 13;
            this.lblAccountingObjectName.Text = ":";
            // 
            // lblReason
            // 
            this.lblReason.AutoSize = true;
            this.lblReason.BackColor = System.Drawing.Color.Transparent;
            this.lblReason.Location = new System.Drawing.Point(110, 145);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(10, 13);
            this.lblReason.TabIndex = 16;
            this.lblReason.Text = ":";
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(9, 145);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 17);
            this.label9.TabIndex = 6;
            this.label9.Text = "Lý do chi";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(10, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Địa chỉ";
            // 
            // lblAccountingObjectAddress
            // 
            this.lblAccountingObjectAddress.AutoSize = true;
            this.lblAccountingObjectAddress.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectAddress.Location = new System.Drawing.Point(111, 100);
            this.lblAccountingObjectAddress.Name = "lblAccountingObjectAddress";
            this.lblAccountingObjectAddress.Size = new System.Drawing.Size(10, 13);
            this.lblAccountingObjectAddress.TabIndex = 14;
            this.lblAccountingObjectAddress.Text = ":";
            // 
            // lblPayers
            // 
            this.lblPayers.AutoSize = true;
            this.lblPayers.BackColor = System.Drawing.Color.Transparent;
            this.lblPayers.Location = new System.Drawing.Point(111, 122);
            this.lblPayers.Name = "lblPayers";
            this.lblPayers.Size = new System.Drawing.Size(10, 13);
            this.lblPayers.TabIndex = 15;
            this.lblPayers.Text = ":";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(10, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Người nhận";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridPosted);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(780, 112);
            // 
            // uGridPosted
            // 
            this.uGridPosted.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridPosted.Location = new System.Drawing.Point(0, 0);
            this.uGridPosted.Name = "uGridPosted";
            this.uGridPosted.Size = new System.Drawing.Size(780, 112);
            this.uGridPosted.TabIndex = 1;
            this.uGridPosted.Text = "ultraGrid1";
            this.uGridPosted.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.ultraTabControl1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 297);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(784, 138);
            this.panel3.TabIndex = 6;
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(784, 138);
            this.ultraTabControl1.TabIndex = 20;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Thông tin chung";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Chi tiết";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(780, 112);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 435);
            this.Controls.Add(this.panel3);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Controls.SetChildIndex(this.panel3, 0);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl1.PerformLayout();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridPosted)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPostedDate;
        private System.Windows.Forms.Label lblTypeID;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblNo;
        private System.Windows.Forms.Label lblTotalAmount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblNumberAttach;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblAccountingObjectName;
        private System.Windows.Forms.Label lblReason;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblAccountingObjectAddress;
        private System.Windows.Forms.Label lblPayers;
        private System.Windows.Forms.Label label4;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPosted;
    }
}