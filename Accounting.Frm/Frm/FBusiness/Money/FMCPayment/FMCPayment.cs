﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Frm.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting.Frm
{
    public partial class FMCPayment : CustormForm //UserControl
    {
        #region khai báo
        private readonly IMCPaymentService _iMcPaymentService;
        private readonly IGeneralLedgerService _iGeneralLedgerService;
        BindingList<MCPayment> _dsMcPayment = new BindingList<MCPayment>();

        // Chứa danh sách các template dựa theo TypeID (KEY type of string = TypeID@Guid, VALUES = Template của TypeID và TemplateID ứng với chứng từ đó)
        // - khi một chứng từ được load, nó kiểm tra Template nó cần có trong dsTemplate chưa, chưa có thì load trong csdl có rồi thì lấy luôn trong dsTemplate để tránh truy vấn nhiều
        // - dsTemplate sẽ được cập nhật lại khi có sự thay đổi giao diện
        //Note: nó có tác dụng tăng tốc độ của chương trình, tuy nhiên chú ý nó là hàm Static nên tồn tại ngay cả khi đóng form, chỉ mất đi khi đóng toàn bộ chương trình do đó cần chú ý set nó bằng null hoặc bằng new Dictionary<string, Template>() hoặc clear item trong dic cho giải phóng bộ nhớ
        public static Dictionary<string, Template> DsTemplate = new Dictionary<string, Template>();
        #endregion

        #region khởi tạo
        public FMCPayment()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _iMcPaymentService = IoC.Resolve<IMCPaymentService>();
            _iGeneralLedgerService = IoC.Resolve<IGeneralLedgerService>();
            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();

            //chọn tự select dòng đầu tiên
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            uGridPosted.DisplayLayout.Override.CellClickAction = CellClickAction.RowSelect;
            #endregion
        }

        private void LoadDuLieu(bool configGrid = true)
        {
            #region Lấy dữ liệu từ CSDL
            //Dữ liệu lấy theo năm hoạch toán
            DateTime? dbStartDate = Utils.StringToDateTime(ConstFrm.DbStartDate);
            int nam = (dbStartDate == null ? DateTime.Now.Year : dbStartDate.Value.Year);
            //_dsMcPayment = _iMcPaymentService.Query.Where(k => k.PostedDate.Year == nam).OrderByDescending(k => k.PostedDate).ToList();
            _dsMcPayment= new BindingList<MCPayment>(_iMcPaymentService.GetAll());
            //_iMcPaymentService.UnbindSession(_dsMcPayment);
            //_iMcPaymentService.OrderByPostedDate(nam).AddToBindingList(_dsMcPayment);
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = _dsMcPayment;

            uGridPosted.Visible = uGrid.Rows.Count > 0;

            if (configGrid) ConfigGrid(uGrid);
            #endregion
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void TsmAddClick(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void TsmEditClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void TsmDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void TmsReLoadClick(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void BtnAddClick(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void BtnEditClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void BtnDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void UGridMouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void UGridDoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunction();
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        void AddFunction()
        {
            MCPayment temp = uGrid.Selected.Rows.Count <= 0 ? new MCPayment() : uGrid.Selected.Rows[0].ListObject as MCPayment;
            FMCPaymentDetail frm = new FMCPaymentDetail(temp, _dsMcPayment, ConstFrm.optStatusForm.Add);
            frm.FormClosed += new FormClosedEventHandler(frm_FormClosed);
            frm.ShowDialog();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                MCPayment temp = uGrid.Selected.Rows[0].ListObject as MCPayment;
                FMCPaymentDetail frm = new FMCPaymentDetail(temp, _dsMcPayment, ConstFrm.optStatusForm.View);
                frm.FormClosed += new FormClosedEventHandler(frm_FormClosed);
                frm.ShowDialog();
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }

        void frm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (FMCPaymentDetail.IsClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                MCPayment temp = uGrid.Selected.Rows[0].ListObject as MCPayment;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, temp.No)) == DialogResult.Yes)
                {
                    _iMcPaymentService.BeginTran();
                    //List<GeneralLedger> listGenTemp = _iGeneralLedgerService.Query.Where(p => p.ReferenceID == temp.ID).ToList();
                    List<GeneralLedger> listGenTemp = _iGeneralLedgerService.GetByReferenceID(temp.ID);
                    foreach (var generalLedger in listGenTemp)
                    {
                        _iGeneralLedgerService.Delete(generalLedger);
                    }
                    _iMcPaymentService.Delete(temp);
                    _iMcPaymentService.CommitTran();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion

        #region Utils
        void ConfigGrid(UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, ConstDatabase.MCPayment_TableName);
            utralGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Format = Constants.DdMMyyyy;
            utralGrid.DisplayLayout.Bands[0].Columns["Date"].Format = Constants.DdMMyyyy;
            utralGrid.DisplayLayout.Bands[0].Columns["PostedDate"].CellAppearance.TextHAlign = HAlign.Center;
            utralGrid.DisplayLayout.Bands[0].Columns["Date"].CellAppearance.TextHAlign = HAlign.Center;
        }

        public static Template GetMauGiaoDien(int typeId, Guid? templateId, Dictionary<string, Template> dsTemplate)    //out string keyofdsTemplate
        {
            string keyofdsTemplate = string.Format("{0}@{1}", typeId, templateId);
            if (!dsTemplate.Keys.Contains(keyofdsTemplate))
            {
                //Note: các chứng từ được sinh tự động từ nghiệp vụ khác thì phải truyền TypeID của nghiệp vụ khác đó
                int typeIdTemp = GetTypeIdRef(typeId);
                Template template = Utils.GetTemplateInDatabase(templateId, typeIdTemp);
                dsTemplate.Add(keyofdsTemplate, template);
                return template;
            }
            //keyofdsTemplate = string.Empty;
            return dsTemplate[keyofdsTemplate];
        }

        static int GetTypeIdRef(int typeId)
        {//phiếu chi
            //if (typeID == 111) return 111568658;  //phiếu chi trả lương ([Tiền lương] -> [Trả lương])
            //else if (typeID == 112) return 111568658;  //phiếu chi trả lương ([Tiền lương] -> [Trả lương])....
            return typeId;
        }
        #endregion

        #region Event
        private void UGridAfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGrid.Selected.Rows.Count < 1) return;
            object listObject = uGrid.Selected.Rows[0].ListObject;
            if (listObject == null) return;
            MCPayment mcPayment = listObject as MCPayment;
            if (mcPayment == null) return;

            //Tab thông tin chung
            lblPostedDate.Text = mcPayment.PostedDate.ToString(Constants.DdMMyyyy);
            lblNo.Text = mcPayment.No;
            lblDate.Text = mcPayment.Date.ToString(Constants.DdMMyyyy);
            lblAccountingObjectName.Text = mcPayment.AccountingObjectName;
            lblAccountingObjectAddress.Text = mcPayment.AccountingObjectAddress;
            lblPayers.Text = mcPayment.Receiver;
            lblReason.Text = mcPayment.Reason;
            lblNumberAttach.Text = mcPayment.NumberAttach;
            lblTotalAmount.Text = string.Format(mcPayment.TotalAmountOriginal == 0 ? "0" : "{0:0,0}", mcPayment.TotalAmountOriginal);  //MCPayment.TotalAmount.ToString("{0:0,0}");
            lblTypeID.Text = mcPayment.Type != null ? mcPayment.Type.TypeName : string.Empty;

            //Tab hoạch toán
            if (mcPayment.MCPaymentDetails.Count == 0) mcPayment.MCPaymentDetails = new List<MCPaymentDetail>();
            uGridPosted.DataSource = mcPayment.MCPaymentDetails;    //Utils.CloneObject(mcPayment.MCPaymentDetails);

            #region Config Grid
            //hiển thị 1 band
            uGridPosted.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            //tắt lọc cột
            uGridPosted.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGridPosted.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGridPosted.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGridPosted.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;

            //Hiện những dòng trống?
            uGridPosted.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridPosted.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            //Fix Header
            uGridPosted.DisplayLayout.UseFixedHeaders = true;

            UltraGridBand band = uGridPosted.DisplayLayout.Bands[0];
            band.Summaries.Clear();

            Template mauGiaoDien = GetMauGiaoDien(mcPayment.TypeID, mcPayment.TemplateID, DsTemplate);

            Utils.ConfigGrid(uGridPosted, ConstDatabase.MCPaymentDetail_TableName, mauGiaoDien == null ? new List<TemplateColumn>() : mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 0).TemplateColumns, 0);
            //Thêm tổng số ở cột "Số tiền"
            SummarySettings summary = band.Summaries.Add("SumAmountOriginal", SummaryType.Sum, band.Columns["AmountOriginal"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            #endregion
        }
        private void FmcPaymentFormClosing(object sender, FormClosingEventArgs e)
        {
            //giải phóng biến static khi đóng form giúp giải phóng tài nguyên, tuy nhiên nếu để lại thì tốc độ truy cập lần sau sẽ tăng nếu mở form lại
            //DsTemplate = new Dictionary<string, Template>();
        }
        #endregion
    }
}
