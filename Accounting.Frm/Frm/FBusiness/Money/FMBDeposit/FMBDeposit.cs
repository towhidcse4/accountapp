﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FMBDeposit : CustormForm //UserControl
    {
        #region khai báo
        private readonly IMBDepositService _mbDepositSrv;
        private readonly IGeneralLedgerService _iGeneralLedgerService;
        private readonly ITypeService _typeSrv;
        List<MBDeposit> _dsMBDeposit = new List<MBDeposit>();

        // Chứa danh sách các template dựa theo TypeID (KEY type of string = TypeID@Guid, VALUES = Template của TypeID và TemplateID ứng với chứng từ đó)
        // - khi một chứng từ được load, nó kiểm tra Template nó cần có trong dsTemplate chưa, chưa có thì load trong csdl có rồi thì lấy luôn trong dsTemplate để tránh truy vấn nhiều
        // - dsTemplate sẽ được cập nhật lại khi có sự thay đổi giao diện
        //Note: nó có tác dụng tăng tốc độ của chương trình, tuy nhiên chú ý nó là hàm Static nên tồn tại ngay cả khi đóng form, chỉ mất đi khi đóng toàn bộ chương trình do đó cần chú ý set nó bằng null hoặc bằng new Dictionary<string, Template>() hoặc clear item trong dic cho giải phóng bộ nhớ
        public static Dictionary<string, Template> DsTemplate = new Dictionary<string, Template>();
        #endregion

        #region khởi tạo
        public FMBDeposit()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _mbDepositSrv = IoC.Resolve<IMBDepositService>();
            _iGeneralLedgerService = IoC.Resolve<IGeneralLedgerService>();
            _typeSrv = IoC.Resolve<ITypeService>();
            
            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu

            LoadDuLieu();

            //chọn tự select dòng đầu tiên
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            #endregion
        }

        private void LoadDuLieu(bool configGrid = true)
        {
            #region Lấy dữ liệu từ CSDL
            _dsMBDeposit = _mbDepositSrv.GetAll();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = _dsMBDeposit.ToArray();

            if (configGrid) ConfigGrid(uGrid);
            #endregion
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void TsmAddClick(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void TsmEditClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void TsmDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void TmsReLoadClick(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void BtnAddClick(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void BtnEditClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void BtnDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void UGridMouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void UGridDoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunction();
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        void AddFunction()
        {
            //MBDeposit temp = uGrid.Selected.Rows.Count <= 0 ? new MBDeposit { TypeID=160 } : uGrid.Selected.Rows[0].ListObject as MBDeposit;
            MBDeposit temp = new MBDeposit ();
            new FMBDepositDetail(temp, _dsMBDeposit, ConstFrm.optStatusForm.Add).ShowDialog(this);
            if (!FMBDepositDetail.IsClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                MBDeposit temp = uGrid.Selected.Rows[0].ListObject as MBDeposit;
                new FMBDepositDetail(temp, _dsMBDeposit, ConstFrm.optStatusForm.View).ShowDialog(this);
                if (!FMBDepositDetail.IsClose) LoadDuLieu();
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                MBDeposit temp = uGrid.Selected.Rows[0].ListObject as MBDeposit;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, temp.No)) == DialogResult.Yes)
                {
                    _mbDepositSrv.BeginTran();
                    //List<GeneralLedger> listGenTemp = _iGeneralLedgerService.Query.Where(p => p.ReferenceID == temp.ID).ToList();
                    List<GeneralLedger> listGenTemp = _iGeneralLedgerService.GetListByReferenceID(temp.ID);
                    foreach (var generalLedger in listGenTemp) _iGeneralLedgerService.Delete(generalLedger);
                    _mbDepositSrv.Delete(temp);
                    _mbDepositSrv.CommitTran();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion

        #region Utils

        static void ConfigGrid(UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, ConstDatabase.MBDeposit_TableName);

            utralGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Format = Constants.DdMMyyyy;
            utralGrid.DisplayLayout.Bands[0].Columns["Date"].Format = Constants.DdMMyyyy;
        }
        #endregion

        private void UGridAfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGrid.Selected.Rows.Count < 1) return;
            object listObject = uGrid.Selected.Rows[0].ListObject;
            if (listObject == null) return;
            MBDeposit model = listObject as MBDeposit;
            if (model == null) return;

            //Tab thông tin chung
            lblPostedDate.Text = model.PostedDate.ToString(Constants.DdMMyyyy);
            lblNo.Text = model.No;
            lblDate.Text = model.Date.ToString(Constants.DdMMyyyy); 
            lblAccountingObjectName.Text = model.AccountingObjectName;
            lblAddress.Text = model.AccountingObjectAddress;
            lblReason.Text = model.Reason;
            lblTotalAmount.Text = string.Format(model.TotalAmount == 0 ? "0" : "{0:0,0}", model.TotalAmount);

            lblTypeName.Text = _typeSrv.Getbykey(model.TypeID) == null ? "" : _typeSrv.Getbykey(model.TypeID).TypeName;
            uGridPosted.DataSource = model.MBDepositDetails;
            #region Config Grid
            //hiển thị 1 band
            uGridPosted.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            //tắt lọc cột
            uGridPosted.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGridPosted.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGridPosted.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGridPosted.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;

            //Hiện những dòng trống?
            uGridPosted.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridPosted.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            //Fix Header
            uGridPosted.DisplayLayout.UseFixedHeaders = true;

            UltraGridBand band = uGridPosted.DisplayLayout.Bands[0];
            band.Summaries.Clear();

            string keyofdsTemplate = string.Format("{0}@{1}", model.TypeID, model.TemplateID);
            Template mauGiaoDien;
            if (DsTemplate.Keys.Contains(keyofdsTemplate)) mauGiaoDien = DsTemplate[keyofdsTemplate];
            else
            {
                mauGiaoDien = Utils.GetTemplateUIfromDatabase(model.TypeID, false, model.TemplateID);
                DsTemplate.Add(keyofdsTemplate, mauGiaoDien);
            }
            Utils.ConfigGrid(uGridPosted, ConstDatabase.MBDepositDetail_TableName, mauGiaoDien == null ? new List<TemplateColumn>() : mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 0).TemplateColumns, 0);
            //Thêm tổng số ở cột "Số tiền"
            //SummarySettings summary = band.Summaries.Add("SumAmountOriginal", SummaryType.Sum, band.Columns["AmountOriginal"]);
            //summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            //summary.DisplayFormat = "{0:N0}";
            //summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            #endregion
        }
    }
}
