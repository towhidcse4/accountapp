﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;

namespace Accounting
{
    public partial class FMBDepositDetail : FrmMBD
    {
        #region Khai báo
        UltraGrid uGrid0;
        protected readonly List<MBDeposit> _dsMBDeposit = new List<MBDeposit>();
        readonly BindingList<AccountingObject> _accObject = Utils.ListAccountingObject;
        MBDeposit _mbDeposit = new MBDeposit();
        IMBDepositService _IMBDepositService = IoC.Resolve<IMBDepositService>();
        UltraGrid ugrid2 = null;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        private IRefVoucherRSInwardOutwardService _refVoucherRSInwardOutwardService { get { return IoC.Resolve<IRefVoucherRSInwardOutwardService>(); } }
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        bool IsAdd = false;
        #endregion

        #region khởi tạo

        public FMBDepositDetail(MBDeposit temp, List<MBDeposit> dsMBDeposit, int statusForm)
        {//Add -> thêm, Sửa -> sửa, Xem -> Sửa ở trạng thái Enable = False
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();
           
            #endregion

            #region Thiết lập ban đầu cho Form
            _statusForm = statusForm;
            IsAdd = temp.MBDepositDetails.Count > 0;
            
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.TypeID = 160;
            }

            else
            {
                _select = _IMBDepositService.Getbykey(temp.ID);
            }
            if (IsAdd) _select = temp;
            _listSelects.AddRange(dsMBDeposit);
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, dsMBDeposit, statusForm);
            // ChangeStatusControl(statusForm != ConstFrm.optStatusForm.View);
            //this.WindowState = FormWindowState.Maximized;
             
            #endregion
        }
        #endregion

        #region
        public override void InitializeGUI(MBDeposit inputVoucher)
        {
            // _mbDeposit = inputVoucher;
            Template mauGiaoDien = Utils.GetMauGiaoDien(inputVoucher.TypeID, inputVoucher.TemplateID, Utils.ListTemplate);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : inputVoucher.TemplateID;
            this.ConfigTopVouchersNo<MBDeposit>(pnlDateNo, "No", "PostedDate", "Date");
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
            }
            #region Phần chung
            #region Grid Thay đổi động

            _select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : inputVoucher.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : inputVoucher.TotalAmount;
            _select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add ? "VND" : inputVoucher.CurrencyID;

            List<MBDeposit> input = new List<MBDeposit> { _select };
            this.ConfigGridCurrencyByTemplate<MBDeposit>(_select.TypeID, new BindingList<System.Collections.IList> { input },
                                              uGridControl, mauGiaoDien);
            this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObject, "AccountingObjectCode", "ID",
                             inputVoucher, "AccountingObjectID");
            this.ConfigCombo(Utils.ListBankAccountDetail, cbbBankAccount, "BankAccount", "ID", inputVoucher, "BankAccountDetailID");
            #endregion
            if (inputVoucher.TypeID.Equals(161))
            {
                BindingList<MBDepositDetail> _bdlMbDepositDetails = new BindingList<MBDepositDetail>();
                BindingList<MBDepositDetailCustomer> _bdlMbDepositDetailCustomers = new BindingList<MBDepositDetailCustomer>();
                BindingList<RefVoucherRSInwardOutward> _bdlRefVoucher = new BindingList<RefVoucherRSInwardOutward>();
                if (_statusForm != ConstFrm.optStatusForm.Add || IsAdd)
                {
                    _bdlRefVoucher = new BindingList<RefVoucherRSInwardOutward>(inputVoucher.RefVoucherRSInwardOutwards);
                    _bdlMbDepositDetails = new BindingList<MBDepositDetail>(inputVoucher.MBDepositDetails);
                    _bdlMbDepositDetailCustomers = new BindingList<MBDepositDetailCustomer>(inputVoucher.MBDepositDetailCustomers);
                }
                    
                _listObjectInput = new BindingList<System.Collections.IList> { _bdlMbDepositDetailCustomers, _bdlMbDepositDetails };
                _listObjectInputPost = new BindingList<System.Collections.IList> { _bdlRefVoucher };
                this.ConfigGridByTemplete_General<MBDeposit>(palGrid, mauGiaoDien);
                List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInputPost };
                List<Boolean> manyStandard = new List<Boolean>() { true, true, false };
                this.ConfigGridByManyTemplete<MBDeposit>(inputVoucher.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                ugrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
                ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                uGrid0 = (UltraGrid)palGrid.Controls.Find("uGrid0", true).FirstOrDefault();
                uGrid0.SummaryValueChanged += uGrid0_SummaryValueChanged;
                var ultraTabControl = (UltraTabControl)palGrid.Controls.Find("ultraTabControl", true).FirstOrDefault();
                ultraTabControl.Tabs[2].Visible = false;
                btnOriginalVoucher.Visible = false;
            }
            else if (inputVoucher.TypeID.Equals(162) || inputVoucher.TypeID.Equals(163))
            {
                BindingList<SAInvoiceDetail> bdlSaInvoiceDetail = new BindingList<SAInvoiceDetail>();
                BindingList<RefVoucher> _bdlRefVoucher = new BindingList<RefVoucher>();

                _selectJoin = new SAInvoice();
                _selectJoin = ISAInvoiceService.Getbykey(inputVoucher.ID);
                if (_selectJoin != null)
                {
                    bdlSaInvoiceDetail = new BindingList<SAInvoiceDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                          ? new List<SAInvoiceDetail>()
                                                                          : (((SAInvoice)_selectJoin).SAInvoiceDetails ?? new List<SAInvoiceDetail>()));
                    _bdlRefVoucher = new BindingList<RefVoucher>(((SAInvoice)_selectJoin).RefVouchers);
                }

                _listObjectInput = new BindingList<System.Collections.IList> { bdlSaInvoiceDetail };
                _listObjectInputPost = new BindingList<System.Collections.IList> { _bdlRefVoucher };
                this.ConfigGridByTemplete_General<MBDeposit>(palGrid, mauGiaoDien);
                List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
                List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
                this.ConfigGridByManyTemplete<MBDeposit>(inputVoucher.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
                ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                btnOriginalVoucher.Visible = false;
            }
            else
            {
                _select.AccountingObjectType = _statusForm.Equals(ConstFrm.optStatusForm.Add)
                                                  ? 0
                                                  : _select.AccountingObjectType;
                DataBinding(new List<Control> { optAccountingObjectType }, "AccountingObjectType", "4");
                if (_statusForm.Equals(ConstFrm.optStatusForm.Add))
                {
                    optAccountingObjectType.CheckedIndex = 0;
                }
                GetDataComboByOption(optAccountingObjectType);
                BindingList<MBDepositDetail> _bdlMbDepositDetails = new BindingList<MBDepositDetail>();
                BindingList<MBDepositDetailTax> _bdlDepositDetailTaxs = new BindingList<MBDepositDetailTax>();
                BindingList<RefVoucherRSInwardOutward> _bdlRefVoucher = new BindingList<RefVoucherRSInwardOutward>();
                if (_statusForm != ConstFrm.optStatusForm.Add)
                    _bdlRefVoucher = new BindingList<RefVoucherRSInwardOutward>(inputVoucher.RefVoucherRSInwardOutwards);
                _bdlMbDepositDetails = new BindingList<MBDepositDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                           ? new List<MBDepositDetail>()
                                                                           : inputVoucher.MBDepositDetails);
                _bdlDepositDetailTaxs = new BindingList<MBDepositDetailTax>(_statusForm == ConstFrm.optStatusForm.Add
                                                               ? new List<MBDepositDetailTax>()
                                                               : inputVoucher.MBDepositDetailTaxs);
                _listObjectInput = new BindingList<System.Collections.IList> { _bdlMbDepositDetails, _bdlDepositDetailTaxs };
                _listObjectInputPost = new BindingList<System.Collections.IList> { _bdlRefVoucher };
                this.ConfigGridByTemplete_General<MBDeposit>(palGrid, mauGiaoDien);
                List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInputPost };
                List<Boolean> manyStandard = new List<Boolean>() { true, true, false };
                this.ConfigGridByManyTemplete<MBDeposit>(inputVoucher.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                ugrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
                ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                btnOriginalVoucher.Visible = true;//add by cuongpv
                if (inputVoucher.TypeID == 160)//trungnq thêm vào để ẩn tab thuế , làm task 6237
                {
                    UltraTabControl uTabControl = Controls.Find("ultraTabControl", true).FirstOrDefault() as UltraTabControl;
                    uTabControl.Tabs[1].Visible = false;
                    uTabControl.Tabs[2].Text = "&2. Chứng từ tham chiếu";
                    uTabControl.Tabs[2].ToolTipText = "&2. Chứng từ tham chiếu";
                }
            }
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            #endregion
            //this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObject, "AccountingObjectID", cbbBankAccount,
            //    "BankAccountDetailID");
            DataBinding(new List<Control> { txtAccountingObjectName, txtAccountingObjectAddress, txtBankName, txtdecreption },
                "AccountingObjectName,AccountingObjectAddress,BankName,Reason");

            #region Thiết lập dữ liệu và Fill dữ liệu Obj vào control (nếu đang sửa)
            Utils.ConfigColumnByCurrency(this, Utils.ListCurrency.FirstOrDefault(x => x.ID == inputVoucher.CurrencyID));
            #endregion
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (inputVoucher.TypeID == 160)//trungnq thêm vào để  làm task 6234
                {
                    txtdecreption.Value = "Thu qua ngân hàng";
                    txtdecreption.Text = "Thu qua ngân hàng";
                    _select.Reason = "Thu qua ngân hàng";
                };
            }
        }

        #endregion
        #region Utils
        #endregion
        #region Event
        // thay đổi tài khoản ngân hàng (chủ)

        private void uGrid0_SummaryValueChanged(object sender, SummaryValueChangedEventArgs e)
        {
            if (_statusForm != ConstFrm.optStatusForm.Add && _statusForm != ConstFrm.optStatusForm.Edit) return;
            var columns = uGrid0.DisplayLayout.Bands[0].Columns;
            if (columns.Exists("AmountOriginal") && columns.Exists("SumDiscountAmountOriginal") && columns.Exists("SumAmountOriginal") && columns.Exists("TotalAmountOriginal"))
            {
                if (Convert.ToDecimal(uGrid0.Rows.SummaryValues["SumAmountOriginal"].Value.ToString()) != 0)
                {
                    decimal sumAmount = Utils.decimalTryParse(uGrid0.Rows.SummaryValues["SumAmountOriginal"].Value.ToString());
                    decimal sumDiscount = Utils.decimalTryParse(uGrid0.Rows.SummaryValues["SumDiscountAmountOriginal"].Value.ToString());
                    uGridControl.Rows[0].Cells["TotalAmountOriginal"].Value = sumAmount - sumDiscount;
                }
            }
        }
        #endregion

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucherRSInwardOutward> datasource = (BindingList<RefVoucherRSInwardOutward>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucherRSInwardOutward>();
                    var f = new FViewVoucher(_select.CurrencyID, datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucher)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucherRSInwardOutward>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucherRSInwardOutward)
                    {
                        source.Add(new RefVoucherRSInwardOutward
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                if (_select.TypeID != 162 && _select.TypeID != 163)
                {
                    RefVoucherRSInwardOutward temp = (RefVoucherRSInwardOutward)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                    if (temp != null)
                        editFuntion(temp);
                }
                else
                {
                    RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                    if (temp != null)
                        editFuntion(temp);
                }

            }
        }

        private void editFuntion(RefVoucherRSInwardOutward temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }
        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void FMBDepositDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }
    public class FrmMBD : DetailBase<MBDeposit>
    { }
}