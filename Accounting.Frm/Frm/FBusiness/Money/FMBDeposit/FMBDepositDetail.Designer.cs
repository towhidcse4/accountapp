﻿namespace Accounting
{
    partial class FMBDepositDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            this.grbInfomation = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtdecreption = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectAddress = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObject = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblAccountingObject = new Infragistics.Win.Misc.UltraLabel();
            this.cbbBankAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblD = new Infragistics.Win.Misc.UltraLabel();
            this.grbHome = new Infragistics.Win.Misc.UltraGroupBox();
            this.optAccountingObjectType = new Accounting.UltraOptionSet_Ex();
            this.pnlDateNo = new Infragistics.Win.Misc.UltraPanel();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.ultraToolTipManager1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            this.uGridControl = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.palTop = new Infragistics.Win.Misc.UltraPanel();
            this.palFill = new Infragistics.Win.Misc.UltraPanel();
            this.palGrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.palThongTinChung = new Infragistics.Win.Misc.UltraPanel();
            this.palBottom = new Infragistics.Win.Misc.UltraPanel();
            ((System.ComponentModel.ISupportInitialize)(this.grbInfomation)).BeginInit();
            this.grbInfomation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtdecreption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbHome)).BeginInit();
            this.grbHome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optAccountingObjectType)).BeginInit();
            this.pnlDateNo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).BeginInit();
            this.palTop.ClientArea.SuspendLayout();
            this.palTop.SuspendLayout();
            this.palFill.ClientArea.SuspendLayout();
            this.palFill.SuspendLayout();
            this.palGrid.ClientArea.SuspendLayout();
            this.palGrid.SuspendLayout();
            this.palThongTinChung.ClientArea.SuspendLayout();
            this.palThongTinChung.SuspendLayout();
            this.palBottom.ClientArea.SuspendLayout();
            this.palBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbInfomation
            // 
            this.grbInfomation.Controls.Add(this.txtdecreption);
            this.grbInfomation.Controls.Add(this.ultraLabel1);
            this.grbInfomation.Controls.Add(this.txtAccountingObjectName);
            this.grbInfomation.Controls.Add(this.txtAccountingObjectAddress);
            this.grbInfomation.Controls.Add(this.lblAccountingObjectAddress);
            this.grbInfomation.Controls.Add(this.cbbAccountingObject);
            this.grbInfomation.Controls.Add(this.lblAccountingObject);
            this.grbInfomation.Controls.Add(this.cbbBankAccount);
            this.grbInfomation.Controls.Add(this.txtBankName);
            this.grbInfomation.Controls.Add(this.lblD);
            this.grbInfomation.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance5.FontData.BoldAsString = "True";
            appearance5.FontData.SizeInPoints = 10F;
            this.grbInfomation.HeaderAppearance = appearance5;
            this.grbInfomation.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.grbInfomation.Location = new System.Drawing.Point(0, 0);
            this.grbInfomation.Name = "grbInfomation";
            this.grbInfomation.Size = new System.Drawing.Size(935, 129);
            this.grbInfomation.TabIndex = 32;
            this.grbInfomation.Text = "Thông tin chung";
            this.grbInfomation.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtdecreption
            // 
            this.txtdecreption.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtdecreption.Location = new System.Drawing.Point(99, 96);
            this.txtdecreption.Multiline = true;
            this.txtdecreption.Name = "txtdecreption";
            this.txtdecreption.Size = new System.Drawing.Size(824, 22);
            this.txtdecreption.TabIndex = 33;
            // 
            // ultraLabel1
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance1;
            this.ultraLabel1.Location = new System.Drawing.Point(11, 96);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(69, 22);
            this.ultraLabel1.TabIndex = 32;
            this.ultraLabel1.Text = "Diễn Giải";
            // 
            // txtAccountingObjectName
            // 
            this.txtAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectName.AutoSize = false;
            this.txtAccountingObjectName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectName.Location = new System.Drawing.Point(279, 21);
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(644, 22);
            this.txtAccountingObjectName.TabIndex = 23;
            // 
            // txtAccountingObjectAddress
            // 
            this.txtAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddress.AutoSize = false;
            this.txtAccountingObjectAddress.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectAddress.Location = new System.Drawing.Point(99, 46);
            this.txtAccountingObjectAddress.Name = "txtAccountingObjectAddress";
            this.txtAccountingObjectAddress.Size = new System.Drawing.Size(824, 22);
            this.txtAccountingObjectAddress.TabIndex = 25;
            // 
            // lblAccountingObjectAddress
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.lblAccountingObjectAddress.Appearance = appearance2;
            this.lblAccountingObjectAddress.Location = new System.Drawing.Point(12, 46);
            this.lblAccountingObjectAddress.Name = "lblAccountingObjectAddress";
            this.lblAccountingObjectAddress.Size = new System.Drawing.Size(84, 22);
            this.lblAccountingObjectAddress.TabIndex = 22;
            this.lblAccountingObjectAddress.Text = "Địa chỉ";
            // 
            // cbbAccountingObject
            // 
            this.cbbAccountingObject.AutoSize = false;
            this.cbbAccountingObject.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObject.Location = new System.Drawing.Point(99, 21);
            this.cbbAccountingObject.Name = "cbbAccountingObject";
            this.cbbAccountingObject.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObject.Size = new System.Drawing.Size(174, 22);
            this.cbbAccountingObject.TabIndex = 31;
            // 
            // lblAccountingObject
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.lblAccountingObject.Appearance = appearance3;
            this.lblAccountingObject.Location = new System.Drawing.Point(12, 21);
            this.lblAccountingObject.Name = "lblAccountingObject";
            this.lblAccountingObject.Size = new System.Drawing.Size(68, 22);
            this.lblAccountingObject.TabIndex = 0;
            this.lblAccountingObject.Text = "Đối tượng";
            // 
            // cbbBankAccount
            // 
            this.cbbBankAccount.AutoSize = false;
            this.cbbBankAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbBankAccount.Location = new System.Drawing.Point(99, 71);
            this.cbbBankAccount.Name = "cbbBankAccount";
            this.cbbBankAccount.NullText = "<chọn dữ liệu>";
            this.cbbBankAccount.Size = new System.Drawing.Size(174, 22);
            this.cbbBankAccount.TabIndex = 31;
            // 
            // txtBankName
            // 
            this.txtBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBankName.AutoSize = false;
            this.txtBankName.Location = new System.Drawing.Point(279, 71);
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Size = new System.Drawing.Size(644, 22);
            this.txtBankName.TabIndex = 23;
            // 
            // lblD
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblD.Appearance = appearance4;
            this.lblD.Location = new System.Drawing.Point(11, 71);
            this.lblD.Name = "lblD";
            this.lblD.Size = new System.Drawing.Size(68, 22);
            this.lblD.TabIndex = 0;
            this.lblD.Text = "Nộp vào TK";
            // 
            // grbHome
            // 
            this.grbHome.Controls.Add(this.optAccountingObjectType);
            this.grbHome.Controls.Add(this.pnlDateNo);
            this.grbHome.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance8.FontData.BoldAsString = "True";
            appearance8.FontData.SizeInPoints = 13F;
            this.grbHome.HeaderAppearance = appearance8;
            this.grbHome.Location = new System.Drawing.Point(0, 0);
            this.grbHome.Name = "grbHome";
            this.grbHome.Size = new System.Drawing.Size(935, 88);
            this.grbHome.TabIndex = 31;
            this.grbHome.Text = "Thu qua ngân hàng";
            this.grbHome.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // optAccountingObjectType
            // 
            this.optAccountingObjectType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.BackColor2 = System.Drawing.Color.Transparent;
            this.optAccountingObjectType.Appearance = appearance6;
            this.optAccountingObjectType.BackColor = System.Drawing.Color.Transparent;
            this.optAccountingObjectType.BackColorInternal = System.Drawing.Color.Transparent;
            this.optAccountingObjectType.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.optAccountingObjectType.CheckedIndex = 0;
            valueListItem3.DataValue = "Default Item";
            valueListItem3.DisplayText = "Khách hàng";
            valueListItem5.DataValue = "ValueListItem1";
            valueListItem5.DisplayText = "Nhà cung cấp";
            valueListItem6.DataValue = "ValueListItem2";
            valueListItem6.DisplayText = "Nhân viên";
            valueListItem1.DataValue = "ValueListItem3";
            valueListItem1.DisplayText = "Khác";
            this.optAccountingObjectType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem5,
            valueListItem6,
            valueListItem1});
            this.optAccountingObjectType.Location = new System.Drawing.Point(623, 27);
            this.optAccountingObjectType.Name = "optAccountingObjectType";
            this.optAccountingObjectType.ReadOnly = false;
            this.optAccountingObjectType.Size = new System.Drawing.Size(300, 24);
            this.optAccountingObjectType.TabIndex = 30;
            this.optAccountingObjectType.Text = "Khách hàng";
            // 
            // pnlDateNo
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.ForeColor = System.Drawing.Color.Transparent;
            this.pnlDateNo.Appearance = appearance7;
            this.pnlDateNo.Location = new System.Drawing.Point(13, 27);
            this.pnlDateNo.Name = "pnlDateNo";
            this.pnlDateNo.Size = new System.Drawing.Size(326, 47);
            this.pnlDateNo.TabIndex = 29;
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Left
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Floating;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 0);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Left";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(935, 741);
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Right
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Floating;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(0, 0);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Right";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(935, 741);
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Top
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Floating;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Top";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(935, 741);
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Bottom
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Floating;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 0);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Bottom";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(935, 741);
            // 
            // ultraToolTipManager1
            // 
            this.ultraToolTipManager1.ContainingControl = this;
            // 
            // uGridControl
            // 
            this.uGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridControl.Location = new System.Drawing.Point(469, 3);
            this.uGridControl.Name = "uGridControl";
            this.uGridControl.Size = new System.Drawing.Size(466, 60);
            this.uGridControl.TabIndex = 1;
            this.uGridControl.Text = "ultraGrid1";
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(0, 0);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(0, 0);
            // 
            // palTop
            // 
            // 
            // palTop.ClientArea
            // 
            this.palTop.ClientArea.Controls.Add(this.grbHome);
            this.palTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTop.Location = new System.Drawing.Point(0, 0);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(935, 88);
            this.palTop.TabIndex = 39;
            // 
            // palFill
            // 
            this.palFill.AutoSize = true;
            // 
            // palFill.ClientArea
            // 
            this.palFill.ClientArea.Controls.Add(this.palGrid);
            this.palFill.ClientArea.Controls.Add(this.ultraSplitter1);
            this.palFill.ClientArea.Controls.Add(this.palThongTinChung);
            this.palFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palFill.Location = new System.Drawing.Point(0, 88);
            this.palFill.Name = "palFill";
            this.palFill.Size = new System.Drawing.Size(935, 587);
            this.palFill.TabIndex = 40;
            // 
            // palGrid
            // 
            this.palGrid.AutoSize = true;
            // 
            // palGrid.ClientArea
            // 
            this.palGrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.palGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palGrid.Location = new System.Drawing.Point(0, 139);
            this.palGrid.Name = "palGrid";
            this.palGrid.Size = new System.Drawing.Size(935, 448);
            this.palGrid.TabIndex = 33;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance9;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(837, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 7;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 129);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 129;
            this.ultraSplitter1.Size = new System.Drawing.Size(935, 10);
            this.ultraSplitter1.TabIndex = 1;
            // 
            // palThongTinChung
            // 
            // 
            // palThongTinChung.ClientArea
            // 
            this.palThongTinChung.ClientArea.Controls.Add(this.grbInfomation);
            this.palThongTinChung.Dock = System.Windows.Forms.DockStyle.Top;
            this.palThongTinChung.Location = new System.Drawing.Point(0, 0);
            this.palThongTinChung.Name = "palThongTinChung";
            this.palThongTinChung.Size = new System.Drawing.Size(935, 129);
            this.palThongTinChung.TabIndex = 34;
            // 
            // palBottom
            // 
            // 
            // palBottom.ClientArea
            // 
            this.palBottom.ClientArea.Controls.Add(this.uGridControl);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 675);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(935, 66);
            this.palBottom.TabIndex = 33;
            // 
            // FMBDepositDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(935, 741);
            this.Controls.Add(this.palFill);
            this.Controls.Add(this.palTop);
            this.Controls.Add(this.palBottom);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Left);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Right);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Top);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FMBDepositDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FMBDepositDetail_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.grbInfomation)).EndInit();
            this.grbInfomation.ResumeLayout(false);
            this.grbInfomation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtdecreption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbHome)).EndInit();
            this.grbHome.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optAccountingObjectType)).EndInit();
            this.pnlDateNo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).EndInit();
            this.palTop.ClientArea.ResumeLayout(false);
            this.palTop.ResumeLayout(false);
            this.palFill.ClientArea.ResumeLayout(false);
            this.palFill.ClientArea.PerformLayout();
            this.palFill.ResumeLayout(false);
            this.palFill.PerformLayout();
            this.palGrid.ClientArea.ResumeLayout(false);
            this.palGrid.ResumeLayout(false);
            this.palGrid.PerformLayout();
            this.palThongTinChung.ClientArea.ResumeLayout(false);
            this.palThongTinChung.ResumeLayout(false);
            this.palBottom.ClientArea.ResumeLayout(false);
            this.palBottom.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Left;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Right;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Bottom;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Top;
        private Infragistics.Win.Misc.UltraGroupBox grbHome;
        private Infragistics.Win.UltraWinToolTip.UltraToolTipManager ultraToolTipManager1;
        private Infragistics.Win.Misc.UltraGroupBox grbInfomation;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectAddress;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObject;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObject;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankName;
        private Infragistics.Win.Misc.UltraLabel lblD;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPosted;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridTax;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridControl;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private Infragistics.Win.Misc.UltraPanel palTop;
        private Infragistics.Win.Misc.UltraPanel palFill;
        private Infragistics.Win.Misc.UltraPanel palBottom;
        private Infragistics.Win.Misc.UltraPanel palGrid;
        private Infragistics.Win.Misc.UltraPanel palThongTinChung;
        private Infragistics.Win.Misc.UltraPanel pnlDateNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtdecreption;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private UltraOptionSet_Ex optAccountingObjectType;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
    }
}
