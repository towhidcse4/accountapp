﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;
using Type = System.Type;

namespace Accounting
{
    public sealed partial class FMCAuditDetail : FmcAuditDetailStand
    {
        #region khai bao
        private IMCAuditService _IMCAuditService { get { return IoC.Resolve<IMCAuditService>(); } }
        readonly Dictionary<int, Dictionary<string, List<Account>>> _dsAccountDefault = new Dictionary<int, Dictionary<string, List<Account>>>();
        //private readonly IMCAuditDetailService MCAuditDetailService = Utils.IMCAuditDetailService;
        private UltraGrid uGridDS;
        private UltraGrid uGridNV;
        //Tập các dữ liệu cần dùng
        #endregion
        public FMCAuditDetail(MCAudit temp, List<MCAudit> dsAudits, int statusForm)
        {
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                WaitingFrm.StopWaiting();
                var frm = new FSelectAuditDate(temp, dsAudits, statusForm);
                if (frm.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
                {
                    base.Close();
                    return;
                }
                WaitingFrm.StartWaiting();
                statusForm = frm._status;
                if (statusForm == ConstFrm.optStatusForm.View)
                    temp = dsAudits.FirstOrDefault(k => k.ID == frm.MCAudit.ID);
            }
            #region Khởi tạo giá trị mặc định của Form
            //base.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            //this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            InitializeComponent();
            base.InitializeComponent1();
            #endregion

            #region Thiết lập ban đầu cho Form
            _statusForm = statusForm;
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                temp.TypeID = 180;
                _select = temp;
            }
            else
            {
                _select = temp;
            }

            _listSelects.AddRange(dsAudits);

            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, dsAudits, statusForm);
            cbbCurrency.ReadOnly = true;
            txtBalanceAmount.ReadOnly = true;
            txtAuditAmount.ReadOnly = true;

            #endregion
            
            WaitingFrm.StopWaiting();
        }

        public override void InitializeGUI(MCAudit input)
        {

            cbbCurrency.Text = input.CurrencyID;

            if (input.MCAuditDetails.Count == 0) input.MCAuditDetails = new List<MCAuditDetail>();

            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)

            BindingList<MCAuditDetail> dsAuditDetails = new BindingList<MCAuditDetail>();
            BindingList<MCAuditDetailMember> dsAuditDetailGroup = new BindingList<MCAuditDetailMember>();
            //Template mauGiaoDien = Utils.GetTemplateUIfromDatabase(input.TypeID, _statusForm == ConstFrm.optStatusForm.Add, input.TemplateID);
            Template mauGiaoDien = Utils.GetMauGiaoDien(input.TypeID, input.TemplateID, Utils.ListTemplate);
            _select.TemplateID = _statusForm == 2 ? mauGiaoDien.ID : input.TemplateID;
            #endregion
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.Description = string.Format("Kiểm kê quỹ đến ngày {0}", _select.AuditDate.ToString("dd/MM/yyyy"));
                _select.TotalBalanceAmount = _IMCAuditService.SoDuSoQuy(_select.AuditDate, _select.CurrencyID);
                txtBalanceAmount.Text = _select.TotalBalanceAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            }
            else
            {
                txtBalanceAmount.Text = _select.TotalBalanceAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                dsAuditDetails = new BindingList<MCAuditDetail>(input.MCAuditDetails);
                dsAuditDetailGroup = new BindingList<MCAuditDetailMember>(input.MCAuditDetailMembers);                
            }
            _listObjectInput = new BindingList<System.Collections.IList> { dsAuditDetails, dsAuditDetailGroup };
            this.ConfigGridByTemplete_General<MCAudit>(palGrid, mauGiaoDien);
            this.ConfigGridByTemplete<MCAudit>(input.TypeID, mauGiaoDien, true, _listObjectInput);
            //_listObjectInput.Clear();



            //this.ConfigGridByTempleteWithCount<MCAudit>(input.TypeID, mauGiaoDien, true, _listObjectInput, _listObjectInputGroup, false, true);

            uGridDS = (UltraGrid)this.Controls.Find("uGrid0", true).FirstOrDefault();
            uGridDS.AfterCellUpdate += new CellEventHandler(uGridDS_AfterCellUpdate);
            uGridDS.SummaryValueChanged += uGridDS_SummaryValueChanged;
            uGridDS.DisplayLayout.Bands[0].Columns["ValueOfMoney"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridDS.DisplayLayout.Bands[0].Columns["Quantity"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridDS.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridNV = (UltraGrid)this.Controls.Find("uGrid1", true).FirstOrDefault();
            uGridNV.CellChange += new CellEventHandler(uGridNV_AfterCellUpdate);
            this.ConfigTopVouchersNo<MCAudit>(palTopVouchers, "No", "", "Date", null, false);
            DataBinding(new List<System.Windows.Forms.Control> { txtDescription }, "Description");
            DataBinding(new List<System.Windows.Forms.Control> { ultraTextEditor1 }, "Summary");

        }
        private void uGridDS_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            UltraGridRow row = uGridDS.ActiveRow;
            if (e.Cell.Column.Key.Equals("ValueOfMoney") || e.Cell.Column.Key.Equals("Quantity"))
                row.Cells["Amount"].Value = Convert.ToDecimal(row.Cells["ValueOfMoney"].Value.ToString()) * Convert.ToDecimal(row.Cells["Quantity"].Value.ToString());

        }
        private void uGridNV_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            //if (e.Cell.Column.Key.Equals("DepartmentID"))
            //{
            //    this.CheckErrorInCell<MCAudit>(uGridNV, e.Cell.Row.Cells["DepartmentID"]);
            //}

        }
        private void uGridDS_SummaryValueChanged(object sender, SummaryValueChangedEventArgs e)
        {
            var columns = uGridDS.DisplayLayout.Bands[0].Columns;
            if (columns.Exists("Amount"))
            {
                _select.TotalAuditAmount = Convert.ToDecimal(uGridDS.Rows.SummaryValues["SumAmount"].Value.ToString());
                _select.DifferAmount = _select.TotalAuditAmount - _select.TotalBalanceAmount;
                txtAuditAmount.Text = _select.TotalAuditAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                txtDifferAmount.Text = Math.Abs(_select.DifferAmount).FormatNumberic(ConstDatabase.Format_TienVND);
            }
        }
        public override void ShowPopup(MCAudit input, int statusForm)
        {
            WaitingFrm.StopWaiting();
            var frm = new FSelectAuditDate(input, _listSelects, statusForm);
            if (frm.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }
            WaitingFrm.StartWaiting();
            statusForm = frm._status;
            if (statusForm == ConstFrm.optStatusForm.View)
                input = _listSelects.FirstOrDefault(k => k.ID == frm.MCAudit.ID);
            else
            {
                input = frm.MCAudit;
            }

            _statusForm = statusForm;
            _select = input;

            InitializeGUI(_select);
        }

        private void FMCAuditDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }

    public class FmcAuditDetailStand : DetailBase<MCAudit>
    {
        public IGenCodeService IgenCodeService { get { return IoC.Resolve<IGenCodeService>(); } }
    }
}
