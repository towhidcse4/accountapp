﻿namespace Accounting
{
    partial class FSelectAuditDate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FSelectAuditDate));
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.btnApply = new Infragistics.Win.Misc.UltraButton();
            this.cbbCurrency = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.dteDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDateFrom)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraLabel1
            // 
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance1;
            this.ultraLabel1.Location = new System.Drawing.Point(13, 13);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel1.TabIndex = 65;
            this.ultraLabel1.Text = "Đến ngày";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
            this.btnCancel.Appearance = appearance2;
            this.btnCancel.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnCancel.Location = new System.Drawing.Point(208, 99);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(89, 31);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "&Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance3.Image = global::Accounting.Properties.Resources.apply_32;
            this.btnApply.Appearance = appearance3;
            this.btnApply.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnApply.Location = new System.Drawing.Point(113, 99);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(89, 31);
            this.btnApply.TabIndex = 10;
            this.btnApply.Text = "Đồ&ng ý";
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // cbbCurrency
            // 
            this.cbbCurrency.AutoSize = false;
            this.cbbCurrency.CausesValidation = false;
            this.cbbCurrency.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbCurrency.LimitToList = true;
            this.cbbCurrency.Location = new System.Drawing.Point(113, 54);
            this.cbbCurrency.Name = "cbbCurrency";
            this.cbbCurrency.Size = new System.Drawing.Size(184, 26);
            this.cbbCurrency.TabIndex = 68;
            this.cbbCurrency.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbCurrency_ItemNotInList);
            this.cbbCurrency.Leave += new System.EventHandler(this.cbbCurrency_Leave);
            // 
            // ultraLabel2
            // 
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance4;
            this.ultraLabel2.Location = new System.Drawing.Point(13, 54);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel2.TabIndex = 67;
            this.ultraLabel2.Text = "Loại tiền";
            // 
            // dteDateFrom
            // 
            this.dteDateFrom.AutoSize = false;
            this.dteDateFrom.CausesValidation = false;
            this.dteDateFrom.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.dteDateFrom.Location = new System.Drawing.Point(113, 13);
            this.dteDateFrom.MaskInput = "dd/mm/yyyy";
            this.dteDateFrom.Name = "dteDateFrom";
            this.dteDateFrom.Size = new System.Drawing.Size(184, 26);
            this.dteDateFrom.TabIndex = 69;
            // 
            // FSelectAuditDate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(328, 142);
            this.Controls.Add(this.dteDateFrom);
            this.Controls.Add(this.cbbCurrency);
            this.Controls.Add(this.ultraLabel2);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FSelectAuditDate";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chọn thời gian kiểm kê";
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDateFrom)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraButton btnCancel;
        private Infragistics.Win.Misc.UltraButton btnApply;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCurrency;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDateFrom;
    }
}