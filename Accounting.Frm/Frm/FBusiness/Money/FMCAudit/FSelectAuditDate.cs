﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;

namespace Accounting
{
    public partial class FSelectAuditDate : CustormForm
    {
        readonly List<MCAudit> _dsMCAudits;
        public MCAudit MCAudit;
        public int _status;
        public FSelectAuditDate(MCAudit temp, List<MCAudit> dsMCAudits, int statusForm)
        {
            MCAudit = temp;
            _dsMCAudits = dsMCAudits;           
            _status = statusForm;
            InitializeComponent();
            cbbCurrency.DataSource = Utils.ListCurrency;
            Utils.ConfigGrid(cbbCurrency, ConstDatabase.Currency_TableName);
            cbbCurrency.ValueMember = "ID";
            cbbCurrency.Value = "VND";
        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            if(cbbCurrency.Value == null || cbbCurrency.Value == "")
            {
                MSG.Warning("Loại tiền không được để trống!");
                return;
            }
            MCAudit.AuditDate = dteDateFrom.DateTime;
            MCAudit.Date = dteDateFrom.DateTime;
            MCAudit.CurrencyID = (string)cbbCurrency.Value;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbbCurrency_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {

        }

        private void cbbCurrency_Leave(object sender, EventArgs e)
        {
            if(cbbCurrency.Text != null && !string.IsNullOrEmpty(cbbCurrency.Text))
            {
                bool check = Utils.ListCurrency.Any(x => x.ID == cbbCurrency.Value.ToString());
                if (!check)
                {
                    MSG.Warning("Dữ liệu không có trong danh mục!");
                    cbbCurrency.Focus();
                    return;
                }
            }           
        }
    }
}
