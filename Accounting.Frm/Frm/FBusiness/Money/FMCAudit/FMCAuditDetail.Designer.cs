﻿namespace Accounting
{
    sealed partial class FMCAuditDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            this.palTop = new System.Windows.Forms.Panel();
            this.grpTop = new Infragistics.Win.Misc.UltraGroupBox();
            this.palTopVouchers = new Infragistics.Win.Misc.UltraPanel();
            this.palGrid = new Infragistics.Win.Misc.UltraPanel();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.palThongTinChung = new Infragistics.Win.Misc.UltraPanel();
            this.ugbThongTinChung = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbCurrency = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDifferAmount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDifferAmount = new Infragistics.Win.Misc.UltraLabel();
            this.txtAuditAmount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAuditAmount = new Infragistics.Win.Misc.UltraLabel();
            this.txtBalanceAmount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblBalanceAmount = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblSummary = new Infragistics.Win.Misc.UltraLabel();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblCurrency = new Infragistics.Win.Misc.UltraLabel();
            this.lblDescription = new Infragistics.Win.Misc.UltraLabel();
            this.palBottom = new System.Windows.Forms.Panel();
            this.palFill = new System.Windows.Forms.Panel();
            this.palTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpTop)).BeginInit();
            this.grpTop.SuspendLayout();
            this.palTopVouchers.SuspendLayout();
            this.palGrid.SuspendLayout();
            this.palThongTinChung.ClientArea.SuspendLayout();
            this.palThongTinChung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ugbThongTinChung)).BeginInit();
            this.ugbThongTinChung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDifferAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAuditAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBalanceAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            this.palFill.SuspendLayout();
            this.SuspendLayout();
            // 
            // palTop
            // 
            this.palTop.Controls.Add(this.grpTop);
            this.palTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTop.Location = new System.Drawing.Point(0, 0);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(954, 75);
            this.palTop.TabIndex = 27;
            // 
            // grpTop
            // 
            this.grpTop.Controls.Add(this.palTopVouchers);
            this.grpTop.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance2.FontData.BoldAsString = "True";
            appearance2.FontData.SizeInPoints = 13F;
            this.grpTop.HeaderAppearance = appearance2;
            this.grpTop.Location = new System.Drawing.Point(0, 0);
            this.grpTop.Name = "grpTop";
            this.grpTop.Size = new System.Drawing.Size(954, 75);
            this.grpTop.TabIndex = 31;
            this.grpTop.Text = "Bảng kiểm kê quỹ";
            this.grpTop.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // palTopVouchers
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.palTopVouchers.Appearance = appearance1;
            this.palTopVouchers.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.palTopVouchers.Location = new System.Drawing.Point(11, 28);
            this.palTopVouchers.Name = "palTopVouchers";
            this.palTopVouchers.Size = new System.Drawing.Size(315, 42);
            this.palTopVouchers.TabIndex = 0;
            // 
            // palGrid
            // 
            this.palGrid.AutoSize = true;
            this.palGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palGrid.Location = new System.Drawing.Point(0, 125);
            this.palGrid.Name = "palGrid";
            this.palGrid.Size = new System.Drawing.Size(954, 372);
            this.palGrid.TabIndex = 8;
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 115);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 155;
            this.ultraSplitter1.Size = new System.Drawing.Size(954, 10);
            this.ultraSplitter1.TabIndex = 1;
            // 
            // palThongTinChung
            // 
            // 
            // palThongTinChung.ClientArea
            // 
            this.palThongTinChung.ClientArea.Controls.Add(this.ugbThongTinChung);
            this.palThongTinChung.Dock = System.Windows.Forms.DockStyle.Top;
            this.palThongTinChung.Location = new System.Drawing.Point(0, 0);
            this.palThongTinChung.Name = "palThongTinChung";
            this.palThongTinChung.Size = new System.Drawing.Size(954, 115);
            this.palThongTinChung.TabIndex = 0;
            // 
            // ugbThongTinChung
            // 
            this.ugbThongTinChung.Controls.Add(this.cbbCurrency);
            this.ugbThongTinChung.Controls.Add(this.txtDifferAmount);
            this.ugbThongTinChung.Controls.Add(this.lblDifferAmount);
            this.ugbThongTinChung.Controls.Add(this.txtAuditAmount);
            this.ugbThongTinChung.Controls.Add(this.lblAuditAmount);
            this.ugbThongTinChung.Controls.Add(this.txtBalanceAmount);
            this.ugbThongTinChung.Controls.Add(this.lblBalanceAmount);
            this.ugbThongTinChung.Controls.Add(this.ultraTextEditor1);
            this.ugbThongTinChung.Controls.Add(this.lblSummary);
            this.ugbThongTinChung.Controls.Add(this.txtDescription);
            this.ugbThongTinChung.Controls.Add(this.lblCurrency);
            this.ugbThongTinChung.Controls.Add(this.lblDescription);
            this.ugbThongTinChung.Dock = System.Windows.Forms.DockStyle.Top;
            appearance15.FontData.BoldAsString = "True";
            appearance15.FontData.SizeInPoints = 10F;
            this.ugbThongTinChung.HeaderAppearance = appearance15;
            this.ugbThongTinChung.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ugbThongTinChung.Location = new System.Drawing.Point(0, 0);
            this.ugbThongTinChung.Name = "ugbThongTinChung";
            this.ugbThongTinChung.Size = new System.Drawing.Size(954, 114);
            this.ugbThongTinChung.TabIndex = 26;
            this.ugbThongTinChung.Text = "Thông tin chung";
            this.ugbThongTinChung.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbCurrency
            // 
            this.cbbCurrency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance3.TextVAlignAsString = "Middle";
            this.cbbCurrency.Appearance = appearance3;
            this.cbbCurrency.AutoSize = false;
            this.cbbCurrency.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbCurrency.Location = new System.Drawing.Point(553, 32);
            this.cbbCurrency.Name = "cbbCurrency";
            this.cbbCurrency.ReadOnly = true;
            this.cbbCurrency.Size = new System.Drawing.Size(123, 22);
            this.cbbCurrency.TabIndex = 35;
            // 
            // txtDifferAmount
            // 
            this.txtDifferAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.TextVAlignAsString = "Middle";
            this.txtDifferAmount.Appearance = appearance4;
            this.txtDifferAmount.AutoSize = false;
            this.txtDifferAmount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtDifferAmount.Location = new System.Drawing.Point(819, 72);
            this.txtDifferAmount.Name = "txtDifferAmount";
            this.txtDifferAmount.Size = new System.Drawing.Size(123, 22);
            this.txtDifferAmount.TabIndex = 34;
            // 
            // lblDifferAmount
            // 
            this.lblDifferAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextHAlignAsString = "Left";
            appearance5.TextVAlignAsString = "Middle";
            this.lblDifferAmount.Appearance = appearance5;
            this.lblDifferAmount.Location = new System.Drawing.Point(687, 72);
            this.lblDifferAmount.Name = "lblDifferAmount";
            this.lblDifferAmount.Size = new System.Drawing.Size(126, 22);
            this.lblDifferAmount.TabIndex = 33;
            this.lblDifferAmount.Text = "Số tiền chênh lệch";
            // 
            // txtAuditAmount
            // 
            this.txtAuditAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.TextVAlignAsString = "Middle";
            this.txtAuditAmount.Appearance = appearance6;
            this.txtAuditAmount.AutoSize = false;
            this.txtAuditAmount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAuditAmount.Location = new System.Drawing.Point(819, 32);
            this.txtAuditAmount.Name = "txtAuditAmount";
            this.txtAuditAmount.ReadOnly = true;
            this.txtAuditAmount.Size = new System.Drawing.Size(123, 22);
            this.txtAuditAmount.TabIndex = 32;
            // 
            // lblAuditAmount
            // 
            this.lblAuditAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Middle";
            this.lblAuditAmount.Appearance = appearance7;
            this.lblAuditAmount.Location = new System.Drawing.Point(687, 32);
            this.lblAuditAmount.Name = "lblAuditAmount";
            this.lblAuditAmount.Size = new System.Drawing.Size(126, 22);
            this.lblAuditAmount.TabIndex = 31;
            this.lblAuditAmount.Text = "Số tiền kiểm kê thực tế";
            // 
            // txtBalanceAmount
            // 
            this.txtBalanceAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.TextVAlignAsString = "Middle";
            this.txtBalanceAmount.Appearance = appearance8;
            this.txtBalanceAmount.AutoSize = false;
            this.txtBalanceAmount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtBalanceAmount.Location = new System.Drawing.Point(553, 72);
            this.txtBalanceAmount.Name = "txtBalanceAmount";
            this.txtBalanceAmount.ReadOnly = true;
            this.txtBalanceAmount.Size = new System.Drawing.Size(123, 22);
            this.txtBalanceAmount.TabIndex = 30;
            // 
            // lblBalanceAmount
            // 
            this.lblBalanceAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextHAlignAsString = "Left";
            appearance9.TextVAlignAsString = "Middle";
            this.lblBalanceAmount.Appearance = appearance9;
            this.lblBalanceAmount.Location = new System.Drawing.Point(449, 72);
            this.lblBalanceAmount.Name = "lblBalanceAmount";
            this.lblBalanceAmount.Size = new System.Drawing.Size(98, 22);
            this.lblBalanceAmount.TabIndex = 29;
            this.lblBalanceAmount.Text = "Số dư theo sổ quỹ";
            // 
            // ultraTextEditor1
            // 
            this.ultraTextEditor1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance10.TextVAlignAsString = "Middle";
            this.ultraTextEditor1.Appearance = appearance10;
            this.ultraTextEditor1.AutoSize = false;
            this.ultraTextEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.ultraTextEditor1.Location = new System.Drawing.Point(118, 72);
            this.ultraTextEditor1.Name = "ultraTextEditor1";
            this.ultraTextEditor1.Size = new System.Drawing.Size(313, 22);
            this.ultraTextEditor1.TabIndex = 28;
            // 
            // lblSummary
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextHAlignAsString = "Left";
            appearance11.TextVAlignAsString = "Middle";
            this.lblSummary.Appearance = appearance11;
            this.lblSummary.Location = new System.Drawing.Point(11, 72);
            this.lblSummary.Name = "lblSummary";
            this.lblSummary.Size = new System.Drawing.Size(98, 22);
            this.lblSummary.TabIndex = 27;
            this.lblSummary.Text = "Kết quả kiểm kê";
            // 
            // txtDescription
            // 
            this.txtDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance12.TextVAlignAsString = "Middle";
            this.txtDescription.Appearance = appearance12;
            this.txtDescription.AutoSize = false;
            this.txtDescription.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtDescription.Location = new System.Drawing.Point(118, 32);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(313, 22);
            this.txtDescription.TabIndex = 3;
            // 
            // lblCurrency
            // 
            this.lblCurrency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextHAlignAsString = "Left";
            appearance13.TextVAlignAsString = "Middle";
            this.lblCurrency.Appearance = appearance13;
            this.lblCurrency.Location = new System.Drawing.Point(449, 32);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(68, 22);
            this.lblCurrency.TabIndex = 26;
            this.lblCurrency.Text = "Loại tiền";
            // 
            // lblDescription
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextHAlignAsString = "Left";
            appearance14.TextVAlignAsString = "Middle";
            this.lblDescription.Appearance = appearance14;
            this.lblDescription.Location = new System.Drawing.Point(11, 32);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(68, 22);
            this.lblDescription.TabIndex = 0;
            this.lblDescription.Text = "Diễn giải";
            // 
            // palBottom
            // 
            this.palBottom.AutoSize = true;
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 572);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(954, 0);
            this.palBottom.TabIndex = 27;
            // 
            // palFill
            // 
            this.palFill.AutoSize = true;
            this.palFill.Controls.Add(this.palGrid);
            this.palFill.Controls.Add(this.ultraSplitter1);
            this.palFill.Controls.Add(this.palThongTinChung);
            this.palFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palFill.Location = new System.Drawing.Point(0, 75);
            this.palFill.Name = "palFill";
            this.palFill.Size = new System.Drawing.Size(954, 497);
            this.palFill.TabIndex = 28;
            // 
            // FMCAuditDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(954, 572);
            this.Controls.Add(this.palFill);
            this.Controls.Add(this.palBottom);
            this.Controls.Add(this.palTop);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FMCAuditDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FMCAuditDetail_FormClosing);
            this.palTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpTop)).EndInit();
            this.grpTop.ResumeLayout(false);
            this.palTopVouchers.ResumeLayout(false);
            this.palGrid.ResumeLayout(false);
            this.palGrid.PerformLayout();
            this.palThongTinChung.ClientArea.ResumeLayout(false);
            this.palThongTinChung.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ugbThongTinChung)).EndInit();
            this.ugbThongTinChung.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDifferAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAuditAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBalanceAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            this.palFill.ResumeLayout(false);
            this.palFill.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel palTop;
        private Infragistics.Win.Misc.UltraGroupBox grpTop;
        private System.Windows.Forms.Panel palBottom;
        private Infragistics.Win.Misc.UltraPanel palGrid;
        private Infragistics.Win.Misc.UltraPanel palThongTinChung;
        private Infragistics.Win.Misc.UltraPanel palTopVouchers;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private System.Windows.Forms.Panel palFill;
        private Infragistics.Win.Misc.UltraGroupBox ugbThongTinChung;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor cbbCurrency;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDifferAmount;
        private Infragistics.Win.Misc.UltraLabel lblDifferAmount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAuditAmount;
        private Infragistics.Win.Misc.UltraLabel lblAuditAmount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBalanceAmount;
        private Infragistics.Win.Misc.UltraLabel lblBalanceAmount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1;
        private Infragistics.Win.Misc.UltraLabel lblSummary;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.Misc.UltraLabel lblCurrency;
        private Infragistics.Win.Misc.UltraLabel lblDescription;
    }
}
