﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Frm.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win;

namespace Accounting.Frm
{
    public partial class FMBCreditCard : CustormForm //UserControl
    {
        #region khai báo
        private readonly IMBCreditCardService IMBCreditCardService;
        private readonly IAccountService IAccountService;
        private readonly IAccountingObjectService IAccountingObjectService;
        private readonly IGeneralLedgerService IGeneralLedgerService;
        private readonly IDepartmentService IDepartmentService;
        private readonly IExpenseItemService IExpenseItemService;
        private readonly ICostSetService ICostSetService;
        private readonly ITypeService ITypeService;
        List<MBCreditCard> _dsCreditCard = new List<MBCreditCard>();

        // Chứa danh sách các template dựa theo TypeID (KEY type of string = TypeID@Guid, VALUES = Template của TypeID và TemplateID ứng với chứng từ đó)
        // - khi một chứng từ được load, nó kiểm tra Template nó cần có trong dsTemplate chưa, chưa có thì load trong csdl có rồi thì lấy luôn trong dsTemplate để tránh truy vấn nhiều
        // - dsTemplate sẽ được cập nhật lại khi có sự thay đổi giao diện
        //Note: nó có tác dụng tăng tốc độ của chương trình, tuy nhiên chú ý nó là hàm Static nên tồn tại ngay cả khi đóng form, chỉ mất đi khi đóng toàn bộ chương trình do đó cần chú ý set nó bằng null hoặc bằng new Dictionary<string, Template>() hoặc clear item trong dic cho giải phóng bộ nhớ
        public static Dictionary<string, Template> DsTemplate = new Dictionary<string, Template>();
        #endregion

        #region khởi tạo
        public FMBCreditCard()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            IMBCreditCardService = IoC.Resolve<IMBCreditCardService>();
            IAccountService = IoC.Resolve<IAccountService>();
            IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            IGeneralLedgerService = IoC.Resolve<IGeneralLedgerService>();
            IDepartmentService = IoC.Resolve<IDepartmentService>();
            IExpenseItemService = IoC.Resolve<IExpenseItemService>();
            ICostSetService = IoC.Resolve<ICostSetService>();
            ITypeService = IoC.Resolve<ITypeService>();

            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu

            LoadDuLieu();

            //chọn tự select dòng đầu tiên
           // if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            #endregion
        }

        private void LoadDuLieu(bool configGrid = true)
        {
            #region Lấy dữ liệu từ CSDL
            _dsCreditCard = IMBCreditCardService.GetAll();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = _dsCreditCard.ToArray();

            if (configGrid) ConfigGrid(uGrid);
            #endregion
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void TsmAddClick(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void TsmEditClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void TsmDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void TmsReLoadClick(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void BtnAddClick(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void BtnEditClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void BtnDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void UGridMouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void UGridDoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunction();
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        void AddFunction()
        {
            //MBDeposit temp = uGrid.Selected.Rows.Count <= 0 ? new MBDeposit { TypeID=160 } : uGrid.Selected.Rows[0].ListObject as MBDeposit;
            MBCreditCard temp = new MBCreditCard();
            new FMBCreditCardDetail(temp, _dsCreditCard, ConstFrm.optStatusForm.Add).ShowDialog();
            if (!FMBCreditCardDetail.IsClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                MBCreditCard temp = uGrid.Selected.Rows[0].ListObject as MBCreditCard;
                new FMBCreditCardDetail(temp, _dsCreditCard, ConstFrm.optStatusForm.View).ShowDialog();
                if (!FMBCreditCardDetail.IsClose) LoadDuLieu();
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                MBCreditCard temp = uGrid.Selected.Rows[0].ListObject as MBCreditCard;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, temp.No)) == DialogResult.Yes)
                {
                    IMBCreditCardService.BeginTran();
                    //List<GeneralLedger> listGenTemp = IGeneralLedgerService.Query.Where(p => p.ReferenceID == temp.ID).ToList();
                    List<GeneralLedger> listGenTemp = IGeneralLedgerService.GetListByReferenceID(temp.ID);
                    foreach (var generalLedger in listGenTemp) IGeneralLedgerService.Delete(generalLedger);
                    IMBCreditCardService.Delete(temp);
                    IMBCreditCardService.CommitTran();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion

        #region Utils

        static void ConfigGrid(UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, ConstDatabase.MBCreditCard_TableName);

            utralGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Format = Constants.DdMMyyyy;
            utralGrid.DisplayLayout.Bands[0].Columns["Date"].Format = Constants.DdMMyyyy;
        }
        #endregion

        private void UGridAfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGrid.Selected.Rows.Count < 1) return;
            object listObject = uGrid.Selected.Rows[0].ListObject;
            if (listObject == null) return;
            MBCreditCard model = listObject as MBCreditCard;
            if (model == null) return;

            //Tab thông tin chung
            lblPostedDate.Text = model.PostedDate.ToString(Constants.DdMMyyyy);
            lblNo.Text = model.No;
            lblDate.Text = model.Date.ToString(Constants.DdMMyyyy);
            lblAccountingObjectName.Text = model.AccountingObjectName;
            lblAddress.Text = model.AccountingObjectAddress;
            lblReason.Text = model.Reason;
            lblTotalAmount.Text = string.Format(model.TotalAmount == 0 ? "0" : "{0:0,0}", model.TotalAmount);

            lblTypeName.Text = ITypeService.Getbykey(model.TypeID) == null ? "" : ITypeService.Getbykey(model.TypeID).TypeName;
            uGridPosted.DataSource = model.MBCreditCardDetails;
            #region Config Grid
            
            //hiển thị 1 band
            uGridPosted.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            //tắt lọc cột
            uGridPosted.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGridPosted.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGridPosted.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGridPosted.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;

            //Hiện những dòng trống?
            uGridPosted.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridPosted.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            //Fix Header
            uGridPosted.DisplayLayout.UseFixedHeaders = true;

            UltraGridBand band = uGridPosted.DisplayLayout.Bands[0];
            band.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            band.Summaries.Clear();

            string keyofdsTemplate = string.Format("{0}@{1}", model.TypeID, model.TemplateID);
            Template mauGiaoDien;
            if (DsTemplate.Keys.Contains(keyofdsTemplate)) mauGiaoDien = DsTemplate[keyofdsTemplate];
            else
            {
                mauGiaoDien = Utils.GetTemplateUIfromDatabase(model.TypeID, false, model.TemplateID);
                DsTemplate.Add(keyofdsTemplate, mauGiaoDien);
            }
            Utils.ConfigGrid(uGridPosted, ConstDatabase.MBCreditCardDetail_TableName, mauGiaoDien == null ? new List<TemplateColumn>() : mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 0).TemplateColumns, 0);
            //Thêm tổng số ở cột "Số tiền"
            //SummarySettings summary = band.Summaries.Add("SumAmountOriginal", SummaryType.Sum, band.Columns["AmountOriginal"]);
            //summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            //summary.DisplayFormat = "{0:N0}";
            //summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            foreach (var item in band.Columns)
            {
                item.CellActivation = Activation.NoEdit;
                if (item.Key.Equals("DebitAccount") || item.Key.Equals("CreditAccount"))
                {
                    item.Editor = this.CreateCombobox(IoC.Resolve<IAccountService>().GetAll().OrderBy(a => a.AccountNumber).ToList(), ConstDatabase.Account_TableName, "AccountNumber", "AccountNumber");
                }
                else if (item.Key.Equals("AmountOriginal"))
                {
                    item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                }
                else if (item.Key.Equals("AccountingObjectID"))
                {
                    List<AccountingObject> AccountingObjects = IAccountingObjectService.GetAll();
                    item.Editor = this.CreateCombobox(AccountingObjects, ConstDatabase.AccountingObject_TableName, "ID", "AccountingObjectName");
                    //item.Editor = getEditorColumInGrid(ConstFrm.ValueList_in_Form_FMCReceipt.AccountingObject.ToString());
                    //item.Editor = GetEditorColumInGrid(ConstFrm.ValueList_in_Form_FMCReceipt.AccountingObject.ToString(), true)[item.Key];
                }
                else if (item.Key.Equals("DepartmentID"))
                {
                    List<Department> Departments = IDepartmentService.GetAll();
                    DefaultEditorOwnerSettings editorSettings = new DefaultEditorOwnerSettings();
                    Infragistics.Win.UltraWinGrid.UltraDropDown dropDown_Departments = new Infragistics.Win.UltraWinGrid.UltraDropDown();
                    dropDown_Departments.Visible = false;
                    this.Controls.Add(dropDown_Departments);
                    dropDown_Departments.DataSource = Departments;
                    dropDown_Departments.Tag = Departments;
                    dropDown_Departments.ValueMember = "ID";
                    dropDown_Departments.DisplayMember = "DepartmentCode";
                    Utils.ConfigGrid(dropDown_Departments, ConstDatabase.Department_TableName);
                    editorSettings.ValueList = dropDown_Departments;
                    editorSettings.DataType = typeof(int);
                    item.Editor = new EditorWithCombo(new DefaultEditorOwner(editorSettings));
                    //item.Editor = GetEditorColumInGrid(ConstFrm.ValueList_in_Form_FMCReceipt.Department.ToString());
                }
                else if (item.Key.Equals("ExpenseItemID"))
                {
                    List<ExpenseItem> ExpenseItems = IExpenseItemService.GetAll();
                    item.Editor = this.CreateCombobox(ExpenseItems, ConstDatabase.ExpenseItem_TableName, "ID", "ExpenseItemCode");
                    //item.Editor = GetEditorColumInGrid(ConstFrm.ValueList_in_Form_FMCReceipt.ExpenseItem.ToString());
                }
                else if (item.Key.Equals("CostSetID"))
                {
                    List<CostSet> CostSets = ICostSetService.GetAll();
                    item.Editor = this.CreateCombobox(CostSets, ConstDatabase.CostSet_TableName, "ID", "CostSetCode");
                    //item.Editor = GetEditorColumInGrid(ConstFrm.ValueList_in_Form_FMCReceipt.CostSet.ToString());
                }
                
            }
            if (model.CurrencyID != "VND")
                uGridPosted.DisplayLayout.Bands[0].Columns["Amount"].Hidden = false;

            #endregion
        }
    }
}
