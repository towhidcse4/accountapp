﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Accounting;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;

namespace Accounting
{
    public partial class FMBCreditCardDetail : FMBCreditCardStand
    {

        #region khởi tạo
        protected Dictionary<string, List<Account>> _getAccountDefaultByTypeId = new Dictionary<string, List<Account>>(); //Chứa các tài khoản mặc định
        private UltraGrid uGridPosted;
        private UltraGrid uGridTax;
        private UltraGrid ugrid2 = null;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private IRefVoucherRSInwardOutwardService _refVoucherRSInwardOutwardService { get { return IoC.Resolve<IRefVoucherRSInwardOutwardService>(); } }
        bool IsAdd = false;

        public FMBCreditCardDetail(MBCreditCard temp, List<MBCreditCard> dsMBCreditCard, int statusForm)
        {//Add -> thêm, Sửa -> sửa, Xem -> Sửa ở trạng thái Enable = False

            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();


            #endregion


            #region Thiết lập ban đầu cho Form

            _statusForm = statusForm;
            IsAdd = temp.MBCreditCardDetails.Count > 0;
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.TypeID = 170;

            }
            else
            {
                _select = temp;
            }
            if (IsAdd) _select = temp;
            _listSelects.AddRange(dsMBCreditCard);
            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, _listSelects, statusForm);

            #endregion
        }

        #endregion
        #region override
        public override void InitializeGUI(MBCreditCard inputVoucher)
        {

            Template mauGiaoDien = Utils.GetMauGiaoDien(inputVoucher.TypeID, inputVoucher.TemplateID, Utils.ListTemplate);
            inputVoucher.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : inputVoucher.TemplateID;

            inputVoucher.AccountingObjectType = _statusForm.Equals(ConstFrm.optStatusForm.Add)
                                                  ? 1
                                                  : inputVoucher.AccountingObjectType;

            if (inputVoucher.TypeID.Equals(171))
            {
                BindingList<PPInvoiceDetail> bdlPPInvoiceDetail = new BindingList<PPInvoiceDetail>();
                BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
                _selectJoin = new PPInvoice();
                _selectJoin = IPPInvoiceService.Getbykey(inputVoucher.ID);
                if (_selectJoin != null)
                {
                    bdlPPInvoiceDetail = new BindingList<PPInvoiceDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                          ? new List<PPInvoiceDetail>()
                                                                          : (((PPInvoice)_selectJoin).PPInvoiceDetails ?? new List<PPInvoiceDetail>()));
                    bdlRefVoucher = new BindingList<RefVoucher>(((PPInvoice)_selectJoin).RefVouchers);

                    _listObjectInput = new BindingList<System.Collections.IList> { bdlPPInvoiceDetail };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                    this.ConfigGridByTemplete_General<MBCreditCard>(pnlGrid, mauGiaoDien);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
                    this.ConfigGridByManyTemplete<MBCreditCard>(inputVoucher.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
                    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                }
                btnOriginalVoucher.Visible = false;
            }
            else if (inputVoucher.TypeID.Equals(172))
            {
                BindingList<FAIncrementDetail> bdlFAIncrementDetail = new BindingList<FAIncrementDetail>();
                BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();

                _selectJoin = new FAIncrement();
                _selectJoin = IFAIncrementService.Getbykey(inputVoucher.ID);
                if (_selectJoin != null)
                {
                    bdlFAIncrementDetail = new BindingList<FAIncrementDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                          ? new List<FAIncrementDetail>()
                                                                          : (((FAIncrement)_selectJoin).FAIncrementDetails ?? new List<FAIncrementDetail>()));
                    bdlRefVoucher = new BindingList<RefVoucher>(((FAIncrement)_selectJoin).RefVouchers);

                    _listObjectInput = new BindingList<System.Collections.IList> { bdlFAIncrementDetail };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                    this.ConfigGridByTemplete_General<MBCreditCard>(pnlGrid, mauGiaoDien);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
                    this.ConfigGridByManyTemplete<MBCreditCard>(inputVoucher.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
                    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                }
                btnOriginalVoucher.Visible = false;
            }
            else if (inputVoucher.TypeID.Equals(173))
            {
                BindingList<PPServiceDetail> bdlPPServiceDetail = new BindingList<PPServiceDetail>();
                BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();

                _selectJoin = new PPService();
                _selectJoin = IPPServiceService.Getbykey(inputVoucher.ID);
                if (_selectJoin != null)
                {
                    bdlPPServiceDetail = new BindingList<PPServiceDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                          ? new List<PPServiceDetail>()
                                                                          : (((PPService)_selectJoin).PPServiceDetails ?? new List<PPServiceDetail>()));
                    bdlRefVoucher = new BindingList<RefVoucher>(((PPService)_selectJoin).RefVouchers);

                    _listObjectInput = new BindingList<System.Collections.IList> { bdlPPServiceDetail };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                    this.ConfigGridByTemplete_General<MBCreditCard>(pnlGrid, mauGiaoDien);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
                    this.ConfigGridByManyTemplete<MBCreditCard>(inputVoucher.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
                    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                }
                btnOriginalVoucher.Visible = false;
            }
            else if (inputVoucher.TypeID.Equals(174)) // add by tungnt: sửa form trả tiền ncc chưa lên tab hóa đơn
            {
                // Hoạch toán
                BindingList<MBCreditCardDetail> bindingMBCreditcard = new BindingList<MBCreditCardDetail>();
                BindingList<MBCreditCardDetailVendor> bindingMbCreditCardDetailTaxs = new BindingList<MBCreditCardDetailVendor>();

                bindingMBCreditcard = new BindingList<MBCreditCardDetail>(inputVoucher.MBCreditCardDetails);
                bindingMbCreditCardDetailTaxs = new BindingList<MBCreditCardDetailVendor>(inputVoucher.MBCreditCardDetailVendors);
                _listObjectInput = new BindingList<System.Collections.IList> { bindingMbCreditCardDetailTaxs, bindingMBCreditcard };
                this.ConfigGridByTemplete_General<MBCreditCard>(pnlGrid, mauGiaoDien);
                //this.ConfigGridByTemplete<MBCreditCard>(inputVoucher.TypeID, mauGiaoDien, true, _listObjectInput);
                List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput };
                List<Boolean> manyStandard = new List<Boolean>() { true, true, false };
                this.ConfigGridByManyTemplete<MBCreditCard>(inputVoucher.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                //ugrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
                //if (ugrid2 != null)
                //{
                //    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                //    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                //    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                //}
                btnOriginalVoucher.Visible = false;
            }
            else if (inputVoucher.TypeID.Equals(906))
            {
                BindingList<TIIncrementDetail> bdlTIIncrementDetail = new BindingList<TIIncrementDetail>();
                BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();

                _selectJoin = new TIIncrement();
                _selectJoin = ITIIncrementService.Getbykey(inputVoucher.ID);
                if (_selectJoin != null)
                {
                    bdlTIIncrementDetail = new BindingList<TIIncrementDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                          ? new List<TIIncrementDetail>()
                                                                          : (((TIIncrement)_selectJoin).TIIncrementDetails ?? new List<TIIncrementDetail>()));
                    bdlRefVoucher = new BindingList<RefVoucher>(((TIIncrement)_selectJoin).RefVouchers);

                    _listObjectInput = new BindingList<System.Collections.IList> { bdlTIIncrementDetail };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                    this.ConfigGridByTemplete_General<MBCreditCard>(pnlGrid, mauGiaoDien);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, true, true, false };
                    this.ConfigGridByManyTemplete<MBCreditCard>(inputVoucher.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    ugrid2 = Controls.Find("uGrid4", true).FirstOrDefault() as UltraGrid;
                    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                    var ultraTabControl = (UltraTabControl)pnlGrid.Controls.Find("ultraTabControl", true).FirstOrDefault();
                    ultraTabControl.Tabs[3].Visible = false;
                    ultraTabControl.Tabs[4].Text = "4. Chứng từ tham chiếu";
                }
                btnOriginalVoucher.Visible = false;
            }
            else
            {
                // Hoạch toán
                BindingList<MBCreditCardDetail> bindingMBCreditcard = new BindingList<MBCreditCardDetail>();
                BindingList<MBCreditCardDetailTax> bindingMbCreditCardDetailTaxs = new BindingList<MBCreditCardDetailTax>();
                BindingList<RefVoucherRSInwardOutward> bdlRefVoucher = new BindingList<RefVoucherRSInwardOutward>();

                bdlRefVoucher = new BindingList<RefVoucherRSInwardOutward>(inputVoucher.RefVoucherRSInwardOutwards);
                bindingMBCreditcard = new BindingList<MBCreditCardDetail>(inputVoucher.MBCreditCardDetails);
                bindingMbCreditCardDetailTaxs = new BindingList<MBCreditCardDetailTax>(inputVoucher.MBCreditCardDetailTaxs);
                _listObjectInput = new BindingList<System.Collections.IList> { bindingMBCreditcard, bindingMbCreditCardDetailTaxs };
                _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                this.ConfigGridByTemplete_General<MBCreditCard>(pnlGrid, mauGiaoDien);
                //this.ConfigGridByTemplete<MBCreditCard>(inputVoucher.TypeID, mauGiaoDien, true, _listObjectInput);
                List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInputPost };
                List<Boolean> manyStandard = new List<Boolean>() { true, true, false };
                this.ConfigGridByManyTemplete<MBCreditCard>(inputVoucher.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                ugrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
                if (ugrid2 != null)
                {
                    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                }
                btnOriginalVoucher.Visible = true;//add by cuongpv
            }
            if (ugrid2 != null)
                ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            List<MBCreditCard> inputCurrency = new List<MBCreditCard> { _select };
            this.ConfigGridCurrencyByTemplate<MBCreditCard>(inputVoucher.TypeID, new BindingList<System.Collections.IList> { inputCurrency },
                                                uGridControl, mauGiaoDien);

            this.ConfigTopVouchersNo<MBCreditCard>(pnlDateNo, "No", "PostedDate", "Date");

            // Thông tin chung: Số thẻ, tên chủ thẻ, nội dung thanh toán
            this.ConfigCombo(inputVoucher, "CreditCardNumber", Utils.ListCreditCard, cbbCreditCardNumber, picCreditCardType,
                             txtCreditCardType, txtOwnerCard);
            // Thông tin đối tượng nhận tiền: Mã đối tượng, tên đối tượng, địa chỉ, tài khoản ngân hàng, tên ngân hàng

            this.ConfigCombo(inputVoucher, Utils.ListAccountingObject, cbbAccountingObject, "AccountingObjectID", cbbAccountingObjBank,
                "AccountingObjectBankAccount", accountingObjectType: 3);

            DataBinding(new List<Control> { optAccountingObjectType }, "AccountingObjectType", "4");
            if (_statusForm.Equals(ConstFrm.optStatusForm.Add))
            {
                optAccountingObjectType.CheckedIndex = 1;
            }
            GetDataComboByOption(optAccountingObjectType);

            DataBinding(new List<Control> { txtAccountingObjectName, txtAccountingObjectAddress, txtReason, txtAccountingObjtBankName },
                "AccountingObjectName,AccountingObjectAddress,Reason,AccountingObjectBankName");
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
            }
            Utils.ConfigColumnByCurrency(this, Utils.ListCurrency.FirstOrDefault(x => x.ID == inputVoucher.CurrencyID));
            if (inputVoucher.TypeID.Equals(174) && _select.CurrencyID != "VND")
            {
                uGridControl.DisplayLayout.Bands[0].Columns["TotalAllOriginal"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGridControl.DisplayLayout.Bands[0].Columns["TotalAll"].Hidden = false;
                uGridControl.DisplayLayout.Bands[0].Columns["ExchangeRate"].Hidden = false;
            }
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (inputVoucher.TypeID == 170)//trungnq thêm vào để  làm task 6234
                {
                    txtReason.Value = "Chi tiền từ thẻ tín dụng";
                    txtReason.Text = "Chi tiền từ thẻ tín dụng";
                    _select.Reason = "Chi tiền từ thẻ tín dụng";
                };
            }
        }

        #endregion

        #region Utils
        #endregion

        #region Event
        #endregion

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucherRSInwardOutward> datasource = (BindingList<RefVoucherRSInwardOutward>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucherRSInwardOutward>();
                    var f = new FViewVoucher(_select.CurrencyID, datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucher)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucherRSInwardOutward>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucherRSInwardOutward)
                    {
                        source.Add(new RefVoucherRSInwardOutward
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                if (_select.TypeID != 171 && _select.TypeID != 172 && _select.TypeID != 173 && _select.TypeID != 906)
                {
                    RefVoucherRSInwardOutward temp = (RefVoucherRSInwardOutward)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                    if (temp != null)
                        editFuntion(temp);
                }
                else
                {
                    RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                    if (temp != null)
                        editFuntion(temp);
                }

            }
        }

        private void editFuntion(RefVoucherRSInwardOutward temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }
        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void ultraSplitter1_Click(object sender, EventArgs e)
        {

        }

        private void FMBCreditCardDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }
    public class FMBCreditCardStand : DetailBase<MBCreditCard>
    {

    }
}