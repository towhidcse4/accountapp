﻿namespace Accounting
{
    partial class FMBCreditCardDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            this.Header = new System.Windows.Forms.Panel();
            this.grbAccount = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtCreditCardType = new Infragistics.Win.Misc.UltraLabel();
            this.picCreditCardType = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.cbbCreditCardNumber = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtOwnerCard = new System.Windows.Forms.Label();
            this.lblOwnerCard = new Infragistics.Win.Misc.UltraLabel();
            this.lblCreditCardNumber = new Infragistics.Win.Misc.UltraLabel();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblReason = new Infragistics.Win.Misc.UltraLabel();
            this.grbAccounting = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbAccountingObjBank = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbAccountingObject = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccountingObjtBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectBankAccount = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectAddress = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObject = new Infragistics.Win.Misc.UltraLabel();
            this.grbHome = new Infragistics.Win.Misc.UltraGroupBox();
            this.optAccountingObjectType = new Accounting.UltraOptionSet_Ex();
            this.pnlDateNo = new Infragistics.Win.Misc.UltraPanel();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.ultraToolTipManager1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            this.Footer = new System.Windows.Forms.Panel();
            this.uGridControl = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.Body = new System.Windows.Forms.Panel();
            this.pnlGrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.Header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbAccount)).BeginInit();
            this.grbAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbAccounting)).BeginInit();
            this.grbAccounting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjBank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjtBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbHome)).BeginInit();
            this.grbHome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optAccountingObjectType)).BeginInit();
            this.pnlDateNo.SuspendLayout();
            this.Footer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).BeginInit();
            this.Body.SuspendLayout();
            this.pnlGrid.ClientArea.SuspendLayout();
            this.pnlGrid.SuspendLayout();
            this.SuspendLayout();
            // 
            // Header
            // 
            this.Header.Controls.Add(this.grbAccount);
            this.Header.Controls.Add(this.grbAccounting);
            this.Header.Controls.Add(this.grbHome);
            this.Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.Header.Location = new System.Drawing.Point(0, 0);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(935, 273);
            this.Header.TabIndex = 27;
            // 
            // grbAccount
            // 
            this.grbAccount.Controls.Add(this.txtCreditCardType);
            this.grbAccount.Controls.Add(this.picCreditCardType);
            this.grbAccount.Controls.Add(this.cbbCreditCardNumber);
            this.grbAccount.Controls.Add(this.txtOwnerCard);
            this.grbAccount.Controls.Add(this.lblOwnerCard);
            this.grbAccount.Controls.Add(this.lblCreditCardNumber);
            this.grbAccount.Controls.Add(this.txtReason);
            this.grbAccount.Controls.Add(this.lblReason);
            this.grbAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance7.FontData.BoldAsString = "True";
            appearance7.FontData.SizeInPoints = 10F;
            this.grbAccount.HeaderAppearance = appearance7;
            this.grbAccount.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.grbAccount.Location = new System.Drawing.Point(0, 83);
            this.grbAccount.Name = "grbAccount";
            this.grbAccount.Size = new System.Drawing.Size(935, 81);
            this.grbAccount.TabIndex = 32;
            this.grbAccount.Text = "Thông tin chung";
            this.grbAccount.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtCreditCardType
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.txtCreditCardType.Appearance = appearance1;
            this.txtCreditCardType.Location = new System.Drawing.Point(306, 25);
            this.txtCreditCardType.Name = "txtCreditCardType";
            this.txtCreditCardType.Size = new System.Drawing.Size(135, 22);
            this.txtCreditCardType.TabIndex = 37;
            // 
            // picCreditCardType
            // 
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance2.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.picCreditCardType.Appearance = appearance2;
            this.picCreditCardType.BackColor = System.Drawing.Color.Transparent;
            this.picCreditCardType.BorderShadowColor = System.Drawing.Color.Transparent;
            this.picCreditCardType.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.picCreditCardType.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.picCreditCardType.Location = new System.Drawing.Point(255, 25);
            this.picCreditCardType.Name = "picCreditCardType";
            this.picCreditCardType.Size = new System.Drawing.Size(43, 22);
            this.picCreditCardType.TabIndex = 36;
            // 
            // cbbCreditCardNumber
            // 
            this.cbbCreditCardNumber.AutoSize = false;
            appearance3.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton1.Appearance = appearance3;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbCreditCardNumber.ButtonsRight.Add(editorButton1);
            this.cbbCreditCardNumber.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCreditCardNumber.Location = new System.Drawing.Point(99, 25);
            this.cbbCreditCardNumber.Name = "cbbCreditCardNumber";
            this.cbbCreditCardNumber.NullText = "<chọn dữ liệu>";
            this.cbbCreditCardNumber.Size = new System.Drawing.Size(150, 22);
            this.cbbCreditCardNumber.TabIndex = 35;
            // 
            // txtOwnerCard
            // 
            this.txtOwnerCard.BackColor = System.Drawing.Color.Transparent;
            this.txtOwnerCard.Location = new System.Drawing.Point(500, 25);
            this.txtOwnerCard.Name = "txtOwnerCard";
            this.txtOwnerCard.Size = new System.Drawing.Size(227, 22);
            this.txtOwnerCard.TabIndex = 34;
            this.txtOwnerCard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblOwnerCard
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblOwnerCard.Appearance = appearance4;
            this.lblOwnerCard.Location = new System.Drawing.Point(447, 25);
            this.lblOwnerCard.Name = "lblOwnerCard";
            this.lblOwnerCard.Size = new System.Drawing.Size(47, 22);
            this.lblOwnerCard.TabIndex = 33;
            this.lblOwnerCard.Text = "Chủ thẻ:";
            // 
            // lblCreditCardNumber
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.lblCreditCardNumber.Appearance = appearance5;
            this.lblCreditCardNumber.Location = new System.Drawing.Point(13, 25);
            this.lblCreditCardNumber.Name = "lblCreditCardNumber";
            this.lblCreditCardNumber.Size = new System.Drawing.Size(68, 22);
            this.lblCreditCardNumber.TabIndex = 0;
            this.lblCreditCardNumber.Text = "Số thẻ";
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason.AutoSize = false;
            this.txtReason.Location = new System.Drawing.Point(99, 50);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(822, 22);
            this.txtReason.TabIndex = 25;
            // 
            // lblReason
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.lblReason.Appearance = appearance6;
            this.lblReason.Location = new System.Drawing.Point(13, 50);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(67, 22);
            this.lblReason.TabIndex = 24;
            this.lblReason.Text = "Nội dung TT";
            // 
            // grbAccounting
            // 
            this.grbAccounting.Controls.Add(this.cbbAccountingObjBank);
            this.grbAccounting.Controls.Add(this.cbbAccountingObject);
            this.grbAccounting.Controls.Add(this.txtAccountingObjtBankName);
            this.grbAccounting.Controls.Add(this.lblAccountingObjectBankAccount);
            this.grbAccounting.Controls.Add(this.txtAccountingObjectAddress);
            this.grbAccounting.Controls.Add(this.txtAccountingObjectName);
            this.grbAccounting.Controls.Add(this.lblAccountingObjectAddress);
            this.grbAccounting.Controls.Add(this.lblAccountingObject);
            this.grbAccounting.Dock = System.Windows.Forms.DockStyle.Bottom;
            appearance12.FontData.BoldAsString = "True";
            appearance12.FontData.SizeInPoints = 10F;
            this.grbAccounting.HeaderAppearance = appearance12;
            this.grbAccounting.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.grbAccounting.Location = new System.Drawing.Point(0, 164);
            this.grbAccounting.Name = "grbAccounting";
            this.grbAccounting.Size = new System.Drawing.Size(935, 109);
            this.grbAccounting.TabIndex = 33;
            this.grbAccounting.Text = "Đối tượng nhận tiền";
            this.grbAccounting.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbAccountingObjBank
            // 
            this.cbbAccountingObjBank.AutoSize = false;
            this.cbbAccountingObjBank.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjBank.Location = new System.Drawing.Point(98, 76);
            this.cbbAccountingObjBank.Name = "cbbAccountingObjBank";
            this.cbbAccountingObjBank.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjBank.Size = new System.Drawing.Size(150, 22);
            this.cbbAccountingObjBank.TabIndex = 37;
            // 
            // cbbAccountingObject
            // 
            this.cbbAccountingObject.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbAccountingObject.AutoSize = false;
            appearance8.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton2.Appearance = appearance8;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObject.ButtonsRight.Add(editorButton2);
            this.cbbAccountingObject.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObject.Location = new System.Drawing.Point(98, 26);
            this.cbbAccountingObject.Name = "cbbAccountingObject";
            this.cbbAccountingObject.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObject.Size = new System.Drawing.Size(150, 22);
            this.cbbAccountingObject.TabIndex = 36;
            // 
            // txtAccountingObjtBankName
            // 
            this.txtAccountingObjtBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjtBankName.AutoSize = false;
            this.txtAccountingObjtBankName.Location = new System.Drawing.Point(254, 76);
            this.txtAccountingObjtBankName.Name = "txtAccountingObjtBankName";
            this.txtAccountingObjtBankName.Size = new System.Drawing.Size(666, 22);
            this.txtAccountingObjtBankName.TabIndex = 33;
            // 
            // lblAccountingObjectBankAccount
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.lblAccountingObjectBankAccount.Appearance = appearance9;
            this.lblAccountingObjectBankAccount.Location = new System.Drawing.Point(10, 76);
            this.lblAccountingObjectBankAccount.Name = "lblAccountingObjectBankAccount";
            this.lblAccountingObjectBankAccount.Size = new System.Drawing.Size(68, 22);
            this.lblAccountingObjectBankAccount.TabIndex = 33;
            this.lblAccountingObjectBankAccount.Text = "Tài khoản";
            // 
            // txtAccountingObjectAddress
            // 
            this.txtAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddress.AutoSize = false;
            this.txtAccountingObjectAddress.Location = new System.Drawing.Point(98, 51);
            this.txtAccountingObjectAddress.Name = "txtAccountingObjectAddress";
            this.txtAccountingObjectAddress.Size = new System.Drawing.Size(822, 22);
            this.txtAccountingObjectAddress.TabIndex = 25;
            // 
            // txtAccountingObjectName
            // 
            this.txtAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectName.AutoSize = false;
            this.txtAccountingObjectName.Location = new System.Drawing.Point(254, 26);
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(666, 22);
            this.txtAccountingObjectName.TabIndex = 23;
            // 
            // lblAccountingObjectAddress
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextVAlignAsString = "Middle";
            this.lblAccountingObjectAddress.Appearance = appearance10;
            this.lblAccountingObjectAddress.Location = new System.Drawing.Point(10, 51);
            this.lblAccountingObjectAddress.Name = "lblAccountingObjectAddress";
            this.lblAccountingObjectAddress.Size = new System.Drawing.Size(84, 22);
            this.lblAccountingObjectAddress.TabIndex = 22;
            this.lblAccountingObjectAddress.Text = "Địa chỉ";
            // 
            // lblAccountingObject
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextVAlignAsString = "Middle";
            this.lblAccountingObject.Appearance = appearance11;
            this.lblAccountingObject.Location = new System.Drawing.Point(9, 26);
            this.lblAccountingObject.Name = "lblAccountingObject";
            this.lblAccountingObject.Size = new System.Drawing.Size(68, 22);
            this.lblAccountingObject.TabIndex = 0;
            this.lblAccountingObject.Text = "Đối tượng";
            // 
            // grbHome
            // 
            this.grbHome.Controls.Add(this.optAccountingObjectType);
            this.grbHome.Controls.Add(this.pnlDateNo);
            this.grbHome.Dock = System.Windows.Forms.DockStyle.Top;
            appearance15.FontData.BoldAsString = "True";
            appearance15.FontData.SizeInPoints = 13F;
            this.grbHome.HeaderAppearance = appearance15;
            this.grbHome.Location = new System.Drawing.Point(0, 0);
            this.grbHome.Name = "grbHome";
            this.grbHome.Size = new System.Drawing.Size(935, 83);
            this.grbHome.TabIndex = 31;
            this.grbHome.Text = "Thẻ tín dụng";
            this.grbHome.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // optAccountingObjectType
            // 
            this.optAccountingObjectType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.BackColor2 = System.Drawing.Color.Transparent;
            this.optAccountingObjectType.Appearance = appearance13;
            this.optAccountingObjectType.BackColor = System.Drawing.Color.Transparent;
            this.optAccountingObjectType.BackColorInternal = System.Drawing.Color.Transparent;
            this.optAccountingObjectType.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.optAccountingObjectType.CheckedIndex = 0;
            valueListItem3.DataValue = "Default Item";
            valueListItem3.DisplayText = "Khách hàng";
            valueListItem5.DataValue = "ValueListItem1";
            valueListItem5.DisplayText = "Nhà cung cấp";
            valueListItem6.DataValue = "ValueListItem2";
            valueListItem6.DisplayText = "Nhân viên";
            valueListItem1.DataValue = "3";
            valueListItem1.DisplayText = "Khác";
            this.optAccountingObjectType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem5,
            valueListItem6,
            valueListItem1});
            this.optAccountingObjectType.Location = new System.Drawing.Point(626, 41);
            this.optAccountingObjectType.Name = "optAccountingObjectType";
            this.optAccountingObjectType.ReadOnly = false;
            this.optAccountingObjectType.Size = new System.Drawing.Size(294, 24);
            this.optAccountingObjectType.TabIndex = 31;
            this.optAccountingObjectType.Text = "Khách hàng";
            // 
            // pnlDateNo
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            this.pnlDateNo.Appearance = appearance14;
            this.pnlDateNo.Location = new System.Drawing.Point(9, 26);
            this.pnlDateNo.Name = "pnlDateNo";
            this.pnlDateNo.Size = new System.Drawing.Size(340, 39);
            this.pnlDateNo.TabIndex = 29;
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Left
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Floating;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 0);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Left";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(935, 741);
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Right
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Floating;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(0, 0);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Right";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(935, 741);
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Top
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Floating;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Top";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(935, 741);
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Bottom
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Floating;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 0);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Bottom";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(935, 741);
            // 
            // ultraToolTipManager1
            // 
            this.ultraToolTipManager1.ContainingControl = this;
            // 
            // Footer
            // 
            this.Footer.Controls.Add(this.uGridControl);
            this.Footer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Footer.Location = new System.Drawing.Point(0, 678);
            this.Footer.Name = "Footer";
            this.Footer.Size = new System.Drawing.Size(935, 63);
            this.Footer.TabIndex = 46;
            // 
            // uGridControl
            // 
            this.uGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridControl.Location = new System.Drawing.Point(366, 0);
            this.uGridControl.Name = "uGridControl";
            this.uGridControl.Size = new System.Drawing.Size(569, 60);
            this.uGridControl.TabIndex = 1;
            this.uGridControl.Text = "ultraGrid1";
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(0, 0);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(0, 0);
            // 
            // Body
            // 
            this.Body.AutoSize = true;
            this.Body.Controls.Add(this.pnlGrid);
            this.Body.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Body.Location = new System.Drawing.Point(0, 283);
            this.Body.Name = "Body";
            this.Body.Size = new System.Drawing.Size(935, 395);
            this.Body.TabIndex = 34;
            // 
            // pnlGrid
            // 
            this.pnlGrid.AutoSize = true;
            // 
            // pnlGrid.ClientArea
            // 
            this.pnlGrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.pnlGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGrid.Location = new System.Drawing.Point(0, 0);
            this.pnlGrid.Name = "pnlGrid";
            this.pnlGrid.Size = new System.Drawing.Size(935, 395);
            this.pnlGrid.TabIndex = 0;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance16.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance16;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(835, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 7;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 273);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 273;
            this.ultraSplitter1.Size = new System.Drawing.Size(935, 10);
            this.ultraSplitter1.TabIndex = 1;
            this.ultraSplitter1.Click += new System.EventHandler(this.ultraSplitter1_Click);
            // 
            // FMBCreditCardDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(935, 741);
            this.Controls.Add(this.Body);
            this.Controls.Add(this.ultraSplitter1);
            this.Controls.Add(this.Header);
            this.Controls.Add(this.Footer);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Left);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Right);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Top);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FMBCreditCardDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FMBCreditCardDetail_FormClosing);
            this.Header.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbAccount)).EndInit();
            this.grbAccount.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbAccounting)).EndInit();
            this.grbAccounting.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjBank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjtBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbHome)).EndInit();
            this.grbHome.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optAccountingObjectType)).EndInit();
            this.pnlDateNo.ResumeLayout(false);
            this.Footer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).EndInit();
            this.Body.ResumeLayout(false);
            this.Body.PerformLayout();
            this.pnlGrid.ClientArea.ResumeLayout(false);
            this.pnlGrid.ResumeLayout(false);
            this.pnlGrid.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Header;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Left;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Right;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Bottom;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Top;
        private Infragistics.Win.Misc.UltraGroupBox grbHome;
        private Infragistics.Win.UltraWinToolTip.UltraToolTipManager ultraToolTipManager1;
        private Infragistics.Win.Misc.UltraGroupBox grbAccount;
        private Infragistics.Win.Misc.UltraLabel lblCreditCardNumber;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.Misc.UltraLabel lblReason;
        private System.Windows.Forms.Panel Body;
        private System.Windows.Forms.Panel Footer;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridControl;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private Infragistics.Win.Misc.UltraLabel lblOwnerCard;
        private System.Windows.Forms.Label txtOwnerCard;
        private Infragistics.Win.Misc.UltraGroupBox grbAccounting;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjtBankName;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectBankAccount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObject;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCreditCardNumber;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObject;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjBank;
        private Infragistics.Win.Misc.UltraPanel pnlGrid;
        private Infragistics.Win.Misc.UltraLabel txtCreditCardType;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox picCreditCardType;
        private Infragistics.Win.Misc.UltraPanel pnlDateNo;
        private UltraOptionSet_Ex optAccountingObjectType;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
    }
}
