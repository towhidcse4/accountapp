﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Accounting;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;

namespace Accounting
{
    public partial class FMCReceiptDetail : FrmTmp
    {
        #region khai báo
        UltraGrid uGrid0;
        UltraGrid ugrid2 = null;
        UltraGrid ugrid1 = null;
        private ISystemOptionService _ISystemOptionService { get { return IoC.Resolve<ISystemOptionService>(); } }
        private IMCReceiptService _IMCReceiptService { get { return IoC.Resolve<IMCReceiptService>(); } }
        private IMCPaymentService _IMCPaymentService { get { return IoC.Resolve<IMCPaymentService>(); } }
        private IMBTellerPaperService _IMBTellerPaperService { get { return IoC.Resolve<IMBTellerPaperService>(); } }
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private IRefVoucherRSInwardOutwardService _refVoucherRSInwardOutwardService { get { return IoC.Resolve<IRefVoucherRSInwardOutwardService>(); } }
        bool IsAdd = false;
        #endregion

        #region khởi tạo


        public FMCReceiptDetail(MCReceipt temp, List<MCReceipt> dsMcReceipt, int statusForm)
        {//Add -> thêm, Sửa -> sửa, Xem -> Sửa ở trạng thái Enable = False


            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();


            //set size của ugrid chi tiết là cố định
            //panel4.Dock = DockStyle.None;
            //int o = (int)(0.5769230769230769* Screen.PrimaryScreen.WorkingArea.Height);
            //pnlGrid.Dock = DockStyle.None;
            //     panel4.Location = new System.Drawing.Point(0, Screen.PrimaryScreen.WorkingArea.Height - (o + 66));
            //      panel4.Size = new System.Drawing.Size(Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height-(grpTop.Location.Y+ grpTop.Height+120+palBottom.Height));
            //     panel4.Location = new System.Drawing.Point(grpTop.Location.X, grpTop.Location.Y + grpTop.Height);
            // pnlGrid.Size = new System.Drawing.Size(Screen.PrimaryScreen.WorkingArea.Width, 255);

            #endregion


            #region Thiết lập ban đầu cho Form
            _statusForm = statusForm;
            IsAdd = temp.MCReceiptDetails.Count > 0;
            _select = statusForm == ConstFrm.optStatusForm.Add ? (new MCReceipt { TypeID = 100 }) : temp;   //100: phiếu thu, 101: phiếu thu bán hàng, 102: phiếu thu bán hàng đại lý.., 103: phiếu thu tiền khách hàng
            if (IsAdd) _select = temp;
            _listSelects.AddRange(dsMcReceipt);

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);

            //Change Menu Top
            ReloadToolbar(_select, _listSelects, statusForm);

            #endregion
        }

        #endregion
        #region override
        public override void InitializeGUI(MCReceipt input)
        {

            #region Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            Template mauGiaoDien = Utils.GetMauGiaoDien(input.TypeID, input.TemplateID, Utils.ListTemplate);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;
            #endregion

            #endregion

            #region Thiết lập dữ liệu và Fill dữ liệu Obj vào control (nếu đang sửa)

            #region Phần đầu
            grpTop.Text = _select.Type == null ? Utils.ListType.FirstOrDefault(p => p.ID.Equals(_select.TypeID)).TypeName.ToUpper() : _select.Type.TypeName.ToUpper();
            this.ConfigTopVouchersNo<MCReceipt>(palTopVouchers, "No", "PostedDate", "Date");
            #endregion

            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();



            }

            if (input.TypeID.Equals(101))
            {
                BindingList<MCReceiptDetail> bdlMcReceiptDetail = new BindingList<MCReceiptDetail>();
                BindingList<MCReceiptDetailCustomer> bdlMcReceiptDetailCustomer = new BindingList<MCReceiptDetailCustomer>();
                BindingList<RefVoucherRSInwardOutward> bdlRefVoucher = new BindingList<RefVoucherRSInwardOutward>();
                if (_statusForm != ConstFrm.optStatusForm.Add || IsAdd)
                {
                    bdlMcReceiptDetail = new BindingList<MCReceiptDetail>(input.MCReceiptDetails);
                    bdlMcReceiptDetailCustomer = new BindingList<MCReceiptDetailCustomer>(input.MCReceiptDetailCustomers);
                    bdlRefVoucher = new BindingList<RefVoucherRSInwardOutward>(input.RefVoucherRSInwardOutwards);
                }
                _listObjectInput = new BindingList<System.Collections.IList> { bdlMcReceiptDetailCustomer, bdlMcReceiptDetail };
                _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                this.ConfigGridByTemplete_General<MCReceipt>(pnlGrid, mauGiaoDien);
                List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInputPost };
                List<Boolean> manyStandard = new List<Boolean>() { true, true, false };
                this.ConfigGridByManyTemplete<MCReceipt>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString(), isForeignCurrency: (_select.CurrencyID == "VND" ? false : true));
                uGrid0 = (UltraGrid)pnlGrid.Controls.Find("uGrid0", true).FirstOrDefault();
                uGrid0.SummaryValueChanged += uGrid0_SummaryValueChanged;
                ugrid1 = (UltraGrid)pnlGrid.Controls.Find("uGrid1", true).FirstOrDefault();
                if (_select.CurrencyID != "VND")
                {
                    uGrid0.DisplayLayout.Bands[0].Columns["ReceipableAmountOriginal"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                    uGrid0.DisplayLayout.Bands[0].Columns["RemainingAmountOriginal"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                    uGrid0.DisplayLayout.Bands[0].Columns["AmountOriginal"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                    ugrid1.DisplayLayout.Bands[0].Columns["AmountOriginal"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);


                }
                ugrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
                ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                var ultraTabControl = (UltraTabControl)pnlGrid.Controls.Find("ultraTabControl", true).FirstOrDefault();
                ultraTabControl.Tabs[2].Visible = false;
                btnOriginalVoucher.Visible = false;
            }
            else if (input.TypeID.Equals(102))
            {
                BindingList<MCReceiptDetail> bdlMcReceiptDetail = new BindingList<MCReceiptDetail>();
                BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
                if (_statusForm != ConstFrm.optStatusForm.Add)
                {
                    bdlMcReceiptDetail = new BindingList<MCReceiptDetail>(input.MCReceiptDetails);

                }
                BindingList<SAInvoiceDetail> bdlSaInvoiceDetail = new BindingList<SAInvoiceDetail>();
                _selectJoin = new SAInvoice();
                _selectJoin = ISAInvoiceService.Getbykey(input.ID);
                if (_selectJoin != null)
                {
                    bdlSaInvoiceDetail = new BindingList<SAInvoiceDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                          ? new List<SAInvoiceDetail>()
                                                                          : (((SAInvoice)_selectJoin).SAInvoiceDetails ?? new List<SAInvoiceDetail>()));
                    bdlRefVoucher = new BindingList<RefVoucher>(((SAInvoice)_selectJoin).RefVouchers);

                    _listObjectInput = new BindingList<System.Collections.IList> { bdlMcReceiptDetail, bdlSaInvoiceDetail };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                    this.ConfigGridByTemplete_General<MCReceipt>(pnlGrid, mauGiaoDien);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
                    this.ConfigGridByManyTemplete<MCReceipt>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                }
                ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
                ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                btnOriginalVoucher.Visible = false;
            }
            else if (input.TypeID.Equals(103))
            {
                BindingList<SAInvoiceDetail> bdlSAInvoiceDetail = new BindingList<SAInvoiceDetail>();
                BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();

                _selectJoin = new SAInvoice();
                _selectJoin = ISAInvoiceService.Getbykey(input.ID);
                if (_selectJoin != null)
                {
                    bdlSAInvoiceDetail = new BindingList<SAInvoiceDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                          ? new List<SAInvoiceDetail>()
                                                                          : (((SAInvoice)_selectJoin).SAInvoiceDetails ?? new List<SAInvoiceDetail>()));
                    bdlRefVoucher = new BindingList<RefVoucher>(((SAInvoice)_selectJoin).RefVouchers);

                    _listObjectInput = new BindingList<System.Collections.IList> { bdlSAInvoiceDetail };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                    this.ConfigGridByTemplete_General<MCReceipt>(pnlGrid, mauGiaoDien);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
                    this.ConfigGridByManyTemplete<MCReceipt>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                }
                ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
                ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                btnOriginalVoucher.Visible = false;
            }
            else
            {

                BindingList<MCReceiptDetail> bdlMcReceiptDetail = new BindingList<MCReceiptDetail>();
                BindingList<MCReceiptDetailTax> bdlMcReceiptDetailTax = new BindingList<MCReceiptDetailTax>();
                BindingList<RefVoucherRSInwardOutward> bdlRefVoucher = new BindingList<RefVoucherRSInwardOutward>();
                if (_statusForm != ConstFrm.optStatusForm.Add)
                {
                    bdlMcReceiptDetail = new BindingList<MCReceiptDetail>(input.MCReceiptDetails);
                    bdlMcReceiptDetailTax = new BindingList<MCReceiptDetailTax>(input.MCReceiptDetailTaxs);
                    bdlRefVoucher = new BindingList<RefVoucherRSInwardOutward>(input.RefVoucherRSInwardOutwards);
                    
                }
                
                    _listObjectInput = new BindingList<System.Collections.IList> { bdlMcReceiptDetail, bdlMcReceiptDetailTax };
                _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                this.ConfigGridByTemplete_General<MCReceipt>(pnlGrid, mauGiaoDien);
                List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInputPost };
                List<Boolean> manyStandard = new List<Boolean>() { true, true, false };
                this.ConfigGridByManyTemplete<MCReceipt>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                ugrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
                ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                btnOriginalVoucher.Visible = true;//add by cuongpv
                if (input.TypeID == 100)//trungnq thêm vào để ẩn tab thuế , làm task 6237
                {
                    UltraTabControl uTabControl = Controls.Find("ultraTabControl", true).FirstOrDefault() as UltraTabControl;
                    uTabControl.Tabs[1].Visible = false;
                    uTabControl.Tabs[2].Text = "&2. Chứng từ tham chiếu";
                    uTabControl.Tabs[2].ToolTipText = "&2. Chứng từ tham chiếu";
                }
            }
            if (ugrid2 != null)
                ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            //Cấu hình Grid động
            _select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmount;
            _select.CurrencyID = (_statusForm == ConstFrm.optStatusForm.Add && input.TypeID != 101) ? "VND" : input.CurrencyID;
            List<MCReceipt> inputCurrency = new List<MCReceipt> { input };

            this.ConfigGridCurrencyByTemplate<MCReceipt>(input.TypeID, new BindingList<System.Collections.IList> { inputCurrency },
                                              uGridControl, mauGiaoDien);
            if (_select.CurrencyID != "VND")
                uGridControl.DisplayLayout.Bands[0].Columns["TotalAllOriginal"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
            else
                uGridControl.DisplayLayout.Bands[0].Columns["TotalAllOriginal"].FormatNumberic(ConstDatabase.Format_TienVND);
            var listAutoPrinciple = Utils.IAutoPrincipleService.getByTypeID(_select.TypeID);
            var bindinglistAutoPrinciple = new BindingList<AutoPrinciple>(listAutoPrinciple);
            this.ConfigCombo(bindinglistAutoPrinciple, cbbReason, "AutoPrincipleName", "ID", null);

            _select.AccountingObjectType = _statusForm.Equals(ConstFrm.optStatusForm.Add)
                                               ? 0
                                               : _select.AccountingObjectType;
            this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectCode", "ID", _select, "AccountingObjectID");
            DataBinding(new List<Control> { optAccountingObjectType }, "AccountingObjectType", "4");
            //lblTotalVATAmount.DataBindings.Clear();
            //lblTotalVATAmount.BindControl("Text", input, "TotalVATAmount", ConstDatabase.Format_TienVND);
            //lblTotalVATAmountOriginal.DataBindings.Clear();
            //lblTotalVATAmountOriginal.BindControl("Text", input, "TotalVATAmountOriginal", ConstDatabase.Format_TienVND, ConstDatabase.Format_ForeignCurrency);

            if (_statusForm.Equals(ConstFrm.optStatusForm.Add))
            {
                optAccountingObjectType.CheckedIndex = 0;
            }
            GetDataComboByOption(optAccountingObjectType);
            this.ConfigReasonCombo<MCReceipt>(_select.TypeID, _statusForm, cbbReason, txtReason, "AutoPrincipleName", "ID", "txtNumberAttach");
            DataBinding(new List<Control> { txtAccountingObjectAddress, txtAccountingObjectName, txtPayers, txtReason, txtNumberAttach },
                "AccountingObjectAddress,AccountingObjectName,Payers,Reason,NumberAttach");
            #endregion           
            Utils.ConfigColumnByCurrency(this, Utils.ListCurrency.FirstOrDefault(x => x.ID == input.CurrencyID));
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 100)//trungnq thêm vào để  làm task 6234
                {
                    txtReason.Value = "Thu tiền mặt";
                    txtReason.Text = "Thu tiền mặt";
                    _select.Reason= "Thu tiền mặt";
                };
            }
        }

        #endregion

        #region Event

        //private int optionCheckedIndex = 0;
        //private bool isClickOption = false;
        //private void optAccountingObjectType_ValueChanged(object sender, EventArgs e)
        //{
        //    if (optAccountingObjectType.CheckedIndex.Equals(optionCheckedIndex)) return;
        //    optionCheckedIndex = optAccountingObjectType.CheckedIndex;
        //    if (isClickOption)
        //    {
        //        if (!string.IsNullOrEmpty(cbbAccountingObjectID.Text.Trim()))
        //        {
        //            _select.AccountingObjectID = null; //_select.AccountingObjectID = null;
        //            cbbAccountingObjectID.UpdateData();
        //        }
        //        if (!string.IsNullOrEmpty(txtAccountingObjectName.Text.Trim()))
        //            _select.AccountingObjectName = "";
        //        if (!string.IsNullOrEmpty(txtAccountingObjectAddress.Text.Trim()))
        //            _select.AccountingObjectAddress = "";
        //        if (!string.IsNullOrEmpty(txtPayers.Text.Trim()))
        //            _select.Payers = "";
        //    }
        //    GetDataComboByOption();
        //    if (cbbAccountingObjectID.IsDroppedDown) cbbAccountingObjectID.ToggleDropdown();
        //    optAccountingObjectType.Focus();
        //}

        //void GetDataComboByOption()
        //{
        //    switch (optAccountingObjectType.CheckedIndex)
        //    {
        //        case 0:
        //            this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectCode", "ID", _select, "AccountingObjectID"); //, DataSourceUpdateMode.OnValidation
        //            break;
        //        case 1:
        //            this.ConfigCombo(_accObjectVendor, cbbAccountingObjectID, "AccountingObjectCode", "ID", _select, "AccountingObjectID");
        //            break;
        //        case 2:
        //            this.ConfigCombo(_lstEmployee, cbbAccountingObjectID, "AccountingObjectCode", "ID", _select, "AccountingObjectID");
        //            break;
        //        default:
        //            this.ConfigCombo(_accObjectCustomer, cbbAccountingObjectID, "AccountingObjectCode", "ID", _select, "AccountingObjectID");
        //            break;
        //    }
        //}

        //private void optAccountingObjectType_Leave(object sender, EventArgs e)
        //{
        //    isClickOption = false;
        //}

        //private void optAccountingObjectType_Click(object sender, EventArgs e)
        //{
        //    isClickOption = true;
        //}

        //private void optAccountingObjectType_Paint(object sender, PaintEventArgs e)
        //{
        //    if (!optAccountingObjectType.CheckedIndex.Equals(optionCheckedIndex))
        //    {
        //        optAccountingObjectType.CheckedIndex = optionCheckedIndex;
        //        optAccountingObjectType.DataBindings[0].WriteValue();
        //    }
        //}
        private void uGrid0_SummaryValueChanged(object sender, SummaryValueChangedEventArgs e)
        {
            var columns = uGrid0.DisplayLayout.Bands[0].Columns;
            if (columns.Exists("AmountOriginal") && columns.Exists("SumAmountOriginal") && columns.Exists("TotalAmountOriginal") && columns.Exists("SumDiscountAmountOriginal"))
            {
                if (Convert.ToDecimal(uGrid0.Rows.SummaryValues["SumAmountOriginal"].Value.ToString()) != 0)
                {
                    uGridControl.Rows[0].Cells["TotalAmountOriginal"].Value =
                        Convert.ToDecimal(uGrid0.Rows.SummaryValues["SumAmountOriginal"].Value.ToString()) -
                        Convert.ToDecimal(uGrid0.Rows.SummaryValues["SumDiscountAmountOriginal"].Value.ToString());
                }
            }
        }
        #endregion

        #region nghiệp vụ

        #endregion

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucherRSInwardOutward> datasource = (BindingList<RefVoucherRSInwardOutward>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucherRSInwardOutward>();
                    var f = new FViewVoucher(_select.CurrencyID, datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                
            
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucher)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucherRSInwardOutward>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucherRSInwardOutward)
                    {
                        source.Add(new RefVoucherRSInwardOutward
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                if (_select.TypeID != 102 && _select.TypeID != 103)
                {
                    RefVoucherRSInwardOutward temp = (RefVoucherRSInwardOutward)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                    if (temp != null)
                        editFuntion(temp);
                }
                else
                {
                    RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                    if (temp != null)
                        editFuntion(temp);
                }

            }
        }

        private void editFuntion(RefVoucherRSInwardOutward temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }
        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void FMCReceiptDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }

    public class FrmTmp : DetailBase<MCReceipt>
    {
    }

}