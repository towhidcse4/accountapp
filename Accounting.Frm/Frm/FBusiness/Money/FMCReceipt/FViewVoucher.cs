﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win;
using System.Drawing;
using System.ComponentModel;

namespace Accounting
{
    public partial class FViewVoucher : CustormForm
    {
        private IRSInwardOutwardService _IRSInwardOutwardService { get { return IoC.Resolve<IRSInwardOutwardService>(); } }
        private IMCReceiptService _IMCReceiptService { get { return IoC.Resolve<IMCReceiptService>(); } }
        private IMCPaymentService _IMCPaymentService { get { return IoC.Resolve<IMCPaymentService>(); } }
        private IMBTellerPaperService _IMBTellerPaperService { get { return IoC.Resolve<IMBTellerPaperService>(); } }
        private IGOtherVoucherService _IGOtherVoucherService { get { return IoC.Resolve<IGOtherVoucherService>(); } }
        private IViewVoucherInvisibleService _IViewVoucherInvisibleService { get { return IoC.Resolve<IViewVoucherInvisibleService>(); } }
        private IPPServiceService _IPPServiceService { get { return IoC.Resolve<IPPServiceService>(); } }
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        private DateTime _startDate;
        public List<RefVoucherRSInwardOutward> RefVoucherRSInwardOutward;
        public BindingList<RefVoucherRSInwardOutward> selected;
        public int type;
        String clone;
        private int typeGroupID;
        private GenCode selectedRow;
        public FViewVoucher(String CurrencyID, BindingList<RefVoucherRSInwardOutward> datasource)
        {
            InitializeComponent();

            btnGetData.Click += (s, e) => btnGetData_Click(s, e, CurrencyID);
            selected = datasource;
            ConfigControl(CurrencyID);
            clone = CurrencyID;

        }

        private void ConfigControl(String CurrencyID)
        {

            var stringToDateTime = Utils.StringToDateTime(ConstFrm.DbStartDate);
            if (stringToDateTime != null)
                _startDate = (DateTime)stringToDateTime;
            else _startDate = DateTime.Now;
            cbbAboutTime.ConfigComboSelectTime(Utils.ObjConstValue.SelectTimes, "Name");
            cbbAboutTime.SelectedRow = cbbAboutTime.Rows[4];

            var ListGenCode = Utils.IGenCodeService.Query.Where(o => o.IsReset == true).ToList();

            // config combobox 
            Utils.ConfigCombo<GenCode>(this, ListGenCode, cbbType, "TypeGroupName", "TypeGroupID");
            UltraGridBand band = cbbType.DisplayLayout.Bands[0];
            cbbType.BorderStyle = UIElementBorderStyle.Solid;
            cbbType.DisplayStyle = EmbeddableElementDisplayStyle.Default;
            cbbType.DisplayLayout.BorderStyle = UIElementBorderStyle.Solid;
            cbbType.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
            cbbType.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            cbbType.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;
            cbbType.DisplayLayout.ScrollBounds = ScrollBounds.ScrollToFill;
            cbbType.DisplayLayout.Scrollbars = Scrollbars.Automatic;
            band.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            band.Override.ActiveRowAppearance.ForeColor = Color.Black;
            band.Override.BorderStyleCell = UIElementBorderStyle.None;
            band.Override.BorderStyleRow = UIElementBorderStyle.None;
            band.Columns["Prefix"].Hidden = true;
            band.Columns["CurrentValue"].Hidden = true;
            //cbbType.SelectedRow = cbbType.Rows[1];
            // lấy dữ liệu theo khoảng thời gian
            //TIOriginalVoucher = _IRSInwardOutwardService.getOriginalVoucher(dteDateFrom.DateTime, dteDateTo.DateTime, CurrencyID);
            //TIOriginalVoucher.Where(x => selected.Any(y => y.No == x.No)).Select(z => { z.Check = true; return z; }).ToList();
            // cấu hình cho lưới
            if (RefVoucherRSInwardOutward == null)
                RefVoucherRSInwardOutward = new List<RefVoucherRSInwardOutward>();
            ConfigGrid(RefVoucherRSInwardOutward);
        }

        private void ConfigGrid(List<RefVoucherRSInwardOutward> input)
        {
            uGrid.DataSource = input;
            Utils.ConfigGrid(uGrid, ConstDatabase.RefVoucher_TableName, false);
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.UseFixedHeaders = false;
            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid, true);
            }
            uGrid.DisplayLayout.Bands[0].Columns["Check"].Header.VisiblePosition = 0;
            UltraGridColumn ugc = uGrid.DisplayLayout.Bands[0].Columns["Check"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnGetData_Click(object sender, EventArgs e, String CurrencyID)
        {
            try
            {
                var genCodeSelected = (GenCode)cbbType.SelectedRow.ListObject;
                // get voucher info
                typeGroupID = genCodeSelected.TypeGroupID;
                type = typeGroupID;
                selectedRow = genCodeSelected;
                // get list vouchers form db
                RefVoucherRSInwardOutward = _voucherInvisibleService.GetVoucher(typeGroupID, dteDateFrom.DateTime, dteDateTo.DateTime);
                //RefVoucherRSInwardOutward.Where(x => selected.Any(y => y.No == x.No)).Select(z => { z.Check = true; return z; }).ToList();
                ConfigGrid(RefVoucherRSInwardOutward);
            }
            catch(Exception ex)
            {
                MSG.Error("Bạn chưa chọn loại chứng từ");
                return;
            }
        }

        private void cbbAboutTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAboutTime.SelectedRow != null)
            {
                var model = cbbAboutTime.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(_startDate.Year,_startDate, model, out dtBegin, out dtEnd);
                dteDateFrom.DateTime = dtBegin;
                dteDateTo.DateTime = dtEnd;
            }
        }

        private void dteDateFrom_ValueChanged(object sender, EventArgs e)
        {
            if (ActiveControl != null && ActiveControl.Name == ((UltraDateTimeEditor)sender).Name)
                cbbAboutTime.SelectedRow = cbbAboutTime.Rows[34];
        }

        private void dteDateTo_ValueChanged(object sender, EventArgs e)
        {
            if (ActiveControl != null && ActiveControl.Name == ((UltraDateTimeEditor)sender).Name)
                cbbAboutTime.SelectedRow = cbbAboutTime.Rows[34];
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            RefVoucherRSInwardOutward = (List<RefVoucherRSInwardOutward>)uGrid.DataSource;
            //RefVoucherRSInwardOutward.AddRange(selected.Where(x => !RefVoucherRSInwardOutward.Any(y => y.No == x.No)).ToList());
            RefVoucherRSInwardOutward = RefVoucherRSInwardOutward.Where(a => a.Check).ToList();
            var lstError = new List<string>();
            foreach (var item in RefVoucherRSInwardOutward)
            {
                foreach (var item2 in selected)
                {
                    if (item2.RefID2 == item.RefID2)
                    {
                        lstError.Add(item.No);
                    }
                }
            }
            if (lstError.Count > 0)
            {
                string er = "";
                foreach (var itemE in lstError)
                {
                    er += itemE + ", ";
                }
                MSG.Error("Chứng từ " + er + "đã tồn tại trong bảng tham chiếu. Vui lòng kiểm tra lại.");
                return;
            }
            else
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void ultraLabel2_Click(object sender, EventArgs e)
        {

        }

        private void cbbType_ValueChanged(object sender, EventArgs e)
        {

        }

        private void cbbType_RowSelected(object sender, RowSelectedEventArgs e)
        {
            
            var genCodeSelected = (GenCode)cbbType.SelectedRow.ListObject;
            // get voucher info
            typeGroupID = genCodeSelected.TypeGroupID;
            type = typeGroupID;
            selectedRow = genCodeSelected;
            // get list vouchers form db
            RefVoucherRSInwardOutward = _voucherInvisibleService.GetVoucher(typeGroupID, dteDateFrom.DateTime, dteDateTo.DateTime);
            //RefVoucherRSInwardOutward.Where(x => selected.Any(y => y.No == x.No)).Select(z => { z.Check = true; return z; }).ToList();
            ConfigGrid(RefVoucherRSInwardOutward);
        }
    }
}