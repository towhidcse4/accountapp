﻿namespace Accounting
{
    partial class FMCReceiptDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton("btnAddAccountingObject");
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbReason = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtNumberAttach = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblNumberAttach = new Infragistics.Win.Misc.UltraLabel();
            this.lblReason = new Infragistics.Win.Misc.UltraLabel();
            this.txtPayers = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblPayers = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectAddress = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObjectID = new Infragistics.Win.Misc.UltraLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grpTop = new Infragistics.Win.Misc.UltraGroupBox();
            this.palTopVouchers = new Infragistics.Win.Misc.UltraPanel();
            this.optAccountingObjectType = new Accounting.UltraOptionSet_Ex();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pnlGrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.panel5 = new System.Windows.Forms.Panel();
            this.palBottom = new System.Windows.Forms.Panel();
            this.uGridControl = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraToolTipManager1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberAttach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpTop)).BeginInit();
            this.grpTop.SuspendLayout();
            this.palTopVouchers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optAccountingObjectType)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.pnlGrid.ClientArea.SuspendLayout();
            this.pnlGrid.SuspendLayout();
            this.panel5.SuspendLayout();
            this.palBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.txtReason);
            this.ultraGroupBox1.Controls.Add(this.cbbReason);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectName);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Controls.Add(this.cbbAccountingObjectID);
            this.ultraGroupBox1.Controls.Add(this.txtNumberAttach);
            this.ultraGroupBox1.Controls.Add(this.lblNumberAttach);
            this.ultraGroupBox1.Controls.Add(this.lblReason);
            this.ultraGroupBox1.Controls.Add(this.txtPayers);
            this.ultraGroupBox1.Controls.Add(this.lblPayers);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectAddress);
            this.ultraGroupBox1.Controls.Add(this.lblAccountingObjectAddress);
            this.ultraGroupBox1.Controls.Add(this.lblAccountingObjectID);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance15.FontData.BoldAsString = "True";
            appearance15.FontData.SizeInPoints = 10F;
            this.ultraGroupBox1.HeaderAppearance = appearance15;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(802, 155);
            this.ultraGroupBox1.TabIndex = 26;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.TextVAlignAsString = "Middle";
            this.txtReason.Appearance = appearance1;
            this.txtReason.AutoSize = false;
            this.txtReason.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReason.Location = new System.Drawing.Point(260, 97);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(530, 22);
            this.txtReason.TabIndex = 37;
            // 
            // cbbReason
            // 
            appearance2.TextVAlignAsString = "Middle";
            this.cbbReason.Appearance = appearance2;
            this.cbbReason.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Append;
            this.cbbReason.AutoSize = false;
            this.cbbReason.Location = new System.Drawing.Point(83, 97);
            this.cbbReason.Name = "cbbReason";
            this.cbbReason.NullText = "<chọn dữ liệu>";
            this.cbbReason.Size = new System.Drawing.Size(171, 22);
            this.cbbReason.TabIndex = 36;
            // 
            // txtAccountingObjectName
            // 
            this.txtAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance3.TextVAlignAsString = "Middle";
            this.txtAccountingObjectName.Appearance = appearance3;
            this.txtAccountingObjectName.AutoSize = false;
            this.txtAccountingObjectName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectName.Location = new System.Drawing.Point(260, 26);
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(530, 22);
            this.txtAccountingObjectName.TabIndex = 35;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel1.Appearance = appearance4;
            this.ultraLabel1.Location = new System.Drawing.Point(707, 127);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(80, 19);
            this.ultraLabel1.TabIndex = 33;
            this.ultraLabel1.Text = "Chứng từ gốc";
            // 
            // cbbAccountingObjectID
            // 
            appearance5.TextVAlignAsString = "Middle";
            this.cbbAccountingObjectID.Appearance = appearance5;
            this.cbbAccountingObjectID.AutoSize = false;
            editorButton1.AccessibleName = "btnAddAccountingObject";
            appearance6.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            appearance6.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance6.ImageVAlign = Infragistics.Win.VAlign.Middle;
            editorButton1.Appearance = appearance6;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            editorButton1.Key = "btnAddAccountingObject";
            this.cbbAccountingObjectID.ButtonsRight.Add(editorButton1);
            this.cbbAccountingObjectID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjectID.Location = new System.Drawing.Point(83, 26);
            this.cbbAccountingObjectID.Name = "cbbAccountingObjectID";
            this.cbbAccountingObjectID.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID.Size = new System.Drawing.Size(171, 22);
            this.cbbAccountingObjectID.TabIndex = 31;
            // 
            // txtNumberAttach
            // 
            this.txtNumberAttach.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.TextVAlignAsString = "Middle";
            this.txtNumberAttach.Appearance = appearance7;
            this.txtNumberAttach.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtNumberAttach.Location = new System.Drawing.Point(83, 122);
            this.txtNumberAttach.Name = "txtNumberAttach";
            this.txtNumberAttach.Size = new System.Drawing.Size(618, 21);
            this.txtNumberAttach.TabIndex = 29;
            // 
            // lblNumberAttach
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.lblNumberAttach.Appearance = appearance8;
            this.lblNumberAttach.Location = new System.Drawing.Point(9, 122);
            this.lblNumberAttach.Name = "lblNumberAttach";
            this.lblNumberAttach.Size = new System.Drawing.Size(68, 22);
            this.lblNumberAttach.TabIndex = 28;
            this.lblNumberAttach.Text = "Kèm theo";
            // 
            // lblReason
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.lblReason.Appearance = appearance9;
            this.lblReason.Location = new System.Drawing.Point(9, 98);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(68, 22);
            this.lblReason.TabIndex = 26;
            this.lblReason.Text = "Lý do nộp";
            // 
            // txtPayers
            // 
            this.txtPayers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance10.TextVAlignAsString = "Middle";
            this.txtPayers.Appearance = appearance10;
            this.txtPayers.AutoSize = false;
            this.txtPayers.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtPayers.Location = new System.Drawing.Point(83, 74);
            this.txtPayers.Name = "txtPayers";
            this.txtPayers.Size = new System.Drawing.Size(707, 22);
            this.txtPayers.TabIndex = 25;
            // 
            // lblPayers
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextVAlignAsString = "Middle";
            this.lblPayers.Appearance = appearance11;
            this.lblPayers.Location = new System.Drawing.Point(9, 74);
            this.lblPayers.Name = "lblPayers";
            this.lblPayers.Size = new System.Drawing.Size(67, 22);
            this.lblPayers.TabIndex = 24;
            this.lblPayers.Text = "Người nộp";
            // 
            // txtAccountingObjectAddress
            // 
            this.txtAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance12.TextVAlignAsString = "Middle";
            this.txtAccountingObjectAddress.Appearance = appearance12;
            this.txtAccountingObjectAddress.AutoSize = false;
            this.txtAccountingObjectAddress.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectAddress.Location = new System.Drawing.Point(83, 50);
            this.txtAccountingObjectAddress.Name = "txtAccountingObjectAddress";
            this.txtAccountingObjectAddress.Size = new System.Drawing.Size(707, 22);
            this.txtAccountingObjectAddress.TabIndex = 23;
            // 
            // lblAccountingObjectAddress
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextVAlignAsString = "Middle";
            this.lblAccountingObjectAddress.Appearance = appearance13;
            this.lblAccountingObjectAddress.Location = new System.Drawing.Point(9, 50);
            this.lblAccountingObjectAddress.Name = "lblAccountingObjectAddress";
            this.lblAccountingObjectAddress.Size = new System.Drawing.Size(68, 22);
            this.lblAccountingObjectAddress.TabIndex = 22;
            this.lblAccountingObjectAddress.Text = "Địa chỉ";
            // 
            // lblAccountingObjectID
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextVAlignAsString = "Middle";
            this.lblAccountingObjectID.Appearance = appearance14;
            this.lblAccountingObjectID.Location = new System.Drawing.Point(9, 26);
            this.lblAccountingObjectID.Name = "lblAccountingObjectID";
            this.lblAccountingObjectID.Size = new System.Drawing.Size(68, 22);
            this.lblAccountingObjectID.TabIndex = 0;
            this.lblAccountingObjectID.Text = "Đối tượng";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.grpTop);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(802, 79);
            this.panel1.TabIndex = 27;
            // 
            // grpTop
            // 
            this.grpTop.Controls.Add(this.palTopVouchers);
            this.grpTop.Controls.Add(this.optAccountingObjectType);
            this.grpTop.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance18.FontData.BoldAsString = "True";
            appearance18.FontData.SizeInPoints = 13F;
            this.grpTop.HeaderAppearance = appearance18;
            this.grpTop.Location = new System.Drawing.Point(0, 0);
            this.grpTop.Name = "grpTop";
            this.grpTop.Size = new System.Drawing.Size(802, 79);
            this.grpTop.TabIndex = 31;
            this.grpTop.Text = "PHIẾU THU";
            this.grpTop.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // palTopVouchers
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.BackColor2 = System.Drawing.Color.Transparent;
            this.palTopVouchers.Appearance = appearance16;
            this.palTopVouchers.AutoSize = true;
            this.palTopVouchers.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.palTopVouchers.Location = new System.Drawing.Point(6, 21);
            this.palTopVouchers.Name = "palTopVouchers";
            this.palTopVouchers.Size = new System.Drawing.Size(308, 43);
            this.palTopVouchers.TabIndex = 30;
            // 
            // optAccountingObjectType
            // 
            this.optAccountingObjectType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextHAlignAsString = "Center";
            appearance17.TextVAlignAsString = "Middle";
            this.optAccountingObjectType.Appearance = appearance17;
            this.optAccountingObjectType.BackColor = System.Drawing.Color.Transparent;
            this.optAccountingObjectType.BackColorInternal = System.Drawing.Color.Transparent;
            this.optAccountingObjectType.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.optAccountingObjectType.CheckedIndex = 0;
            valueListItem3.DataValue = "Default Item";
            valueListItem3.DisplayText = "Khách hàng";
            valueListItem5.DataValue = "ValueListItem1";
            valueListItem5.DisplayText = "Nhà cung cấp";
            valueListItem6.DataValue = "ValueListItem2";
            valueListItem6.DisplayText = "Nhân viên";
            valueListItem1.DataValue = "ValueListItem3";
            valueListItem1.DisplayText = "Khác";
            this.optAccountingObjectType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem5,
            valueListItem6,
            valueListItem1});
            this.optAccountingObjectType.Location = new System.Drawing.Point(489, 39);
            this.optAccountingObjectType.Name = "optAccountingObjectType";
            this.optAccountingObjectType.ReadOnly = false;
            this.optAccountingObjectType.Size = new System.Drawing.Size(298, 22);
            this.optAccountingObjectType.TabIndex = 29;
            this.optAccountingObjectType.Text = "Khách hàng";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.palBottom);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 79);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(802, 379);
            this.panel2.TabIndex = 28;
            // 
            // panel4
            // 
            this.panel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel4.Controls.Add(this.pnlGrid);
            this.panel4.Controls.Add(this.ultraSplitter1);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(802, 318);
            this.panel4.TabIndex = 28;
            // 
            // pnlGrid
            // 
            this.pnlGrid.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            // 
            // pnlGrid.ClientArea
            // 
            this.pnlGrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.pnlGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGrid.Location = new System.Drawing.Point(0, 165);
            this.pnlGrid.Name = "pnlGrid";
            this.pnlGrid.Size = new System.Drawing.Size(802, 153);
            this.pnlGrid.TabIndex = 28;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance19.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance19;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(704, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 5;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 155);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 155;
            this.ultraSplitter1.Size = new System.Drawing.Size(802, 10);
            this.ultraSplitter1.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.ultraGroupBox1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(802, 155);
            this.panel5.TabIndex = 27;
            // 
            // palBottom
            // 
            this.palBottom.Controls.Add(this.uGridControl);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 318);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(802, 61);
            this.palBottom.TabIndex = 27;
            // 
            // uGridControl
            // 
            this.uGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridControl.Location = new System.Drawing.Point(336, -2);
            this.uGridControl.Name = "uGridControl";
            this.uGridControl.Size = new System.Drawing.Size(466, 60);
            this.uGridControl.TabIndex = 1;
            this.uGridControl.Text = "ultraGrid1";
            // 
            // ultraToolTipManager1
            // 
            this.ultraToolTipManager1.ContainingControl = this;
            // 
            // FMCReceiptDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 458);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FMCReceiptDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FMCReceiptDetail_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberAttach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpTop)).EndInit();
            this.grpTop.ResumeLayout(false);
            this.grpTop.PerformLayout();
            this.palTopVouchers.ResumeLayout(false);
            this.palTopVouchers.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optAccountingObjectType)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.pnlGrid.ClientArea.ResumeLayout(false);
            this.pnlGrid.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.palBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectAddress;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNumberAttach;
        private Infragistics.Win.Misc.UltraLabel lblNumberAttach;
        private Infragistics.Win.Misc.UltraLabel lblReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPayers;
        private Infragistics.Win.Misc.UltraLabel lblPayers;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Infragistics.Win.Misc.UltraGroupBox grpTop;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel palBottom;
        private System.Windows.Forms.Panel panel5;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridControl;
        private Infragistics.Win.UltraWinToolTip.UltraToolTipManager ultraToolTipManager1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraPanel pnlGrid;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        private Accounting.UltraOptionSet_Ex optAccountingObjectType;
        private Infragistics.Win.Misc.UltraPanel palTopVouchers;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbReason;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
    }
}
