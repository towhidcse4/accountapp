﻿namespace Accounting
{
    partial class FMBInternalTransfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FMBInternalTransfer));
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.txtTotalAmount = new System.Windows.Forms.Label();
            this.txtReason = new System.Windows.Forms.Label();
            this.txtDate = new System.Windows.Forms.Label();
            this.txtNo = new System.Windows.Forms.Label();
            this.txtPostedDate = new System.Windows.Forms.Label();
            this.lblTotalAmount = new System.Windows.Forms.Label();
            this.lblReason = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblNo = new System.Windows.Forms.Label();
            this.lblPostedDate = new System.Windows.Forms.Label();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridCT = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tabInfomation = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.btnEdit = new Infragistics.Win.Misc.UltraButton();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.uGridIndex = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl1.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabInfomation)).BeginInit();
            this.tabInfomation.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridIndex)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.txtTotalAmount);
            this.ultraTabPageControl1.Controls.Add(this.txtReason);
            this.ultraTabPageControl1.Controls.Add(this.txtDate);
            this.ultraTabPageControl1.Controls.Add(this.txtNo);
            this.ultraTabPageControl1.Controls.Add(this.txtPostedDate);
            this.ultraTabPageControl1.Controls.Add(this.lblTotalAmount);
            this.ultraTabPageControl1.Controls.Add(this.lblReason);
            this.ultraTabPageControl1.Controls.Add(this.lblDate);
            this.ultraTabPageControl1.Controls.Add(this.lblNo);
            this.ultraTabPageControl1.Controls.Add(this.lblPostedDate);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(855, 250);
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.AutoSize = true;
            this.txtTotalAmount.BackColor = System.Drawing.Color.Transparent;
            this.txtTotalAmount.Location = new System.Drawing.Point(124, 122);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.Size = new System.Drawing.Size(10, 13);
            this.txtTotalAmount.TabIndex = 15;
            this.txtTotalAmount.Text = ":";
            // 
            // txtReason
            // 
            this.txtReason.AutoSize = true;
            this.txtReason.BackColor = System.Drawing.Color.Transparent;
            this.txtReason.Location = new System.Drawing.Point(124, 96);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(10, 13);
            this.txtReason.TabIndex = 14;
            this.txtReason.Text = ":";
            // 
            // txtDate
            // 
            this.txtDate.AutoSize = true;
            this.txtDate.BackColor = System.Drawing.Color.Transparent;
            this.txtDate.Location = new System.Drawing.Point(124, 70);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(10, 13);
            this.txtDate.TabIndex = 13;
            this.txtDate.Text = ":";
            // 
            // txtNo
            // 
            this.txtNo.AutoSize = true;
            this.txtNo.BackColor = System.Drawing.Color.Transparent;
            this.txtNo.Location = new System.Drawing.Point(124, 44);
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(10, 13);
            this.txtNo.TabIndex = 12;
            this.txtNo.Text = ":";
            // 
            // txtPostedDate
            // 
            this.txtPostedDate.AutoSize = true;
            this.txtPostedDate.BackColor = System.Drawing.Color.Transparent;
            this.txtPostedDate.Location = new System.Drawing.Point(124, 18);
            this.txtPostedDate.Name = "txtPostedDate";
            this.txtPostedDate.Size = new System.Drawing.Size(10, 13);
            this.txtPostedDate.TabIndex = 11;
            this.txtPostedDate.Text = ":";
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.AutoSize = true;
            this.lblTotalAmount.Location = new System.Drawing.Point(17, 121);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(45, 13);
            this.lblTotalAmount.TabIndex = 4;
            this.lblTotalAmount.Text = "Tiền gửi";
            // 
            // lblReason
            // 
            this.lblReason.AutoSize = true;
            this.lblReason.Location = new System.Drawing.Point(17, 95);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(48, 13);
            this.lblReason.TabIndex = 3;
            this.lblReason.Text = "Diễn giải";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(17, 69);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(77, 13);
            this.lblDate.TabIndex = 2;
            this.lblDate.Text = "Ngày chứng từ";
            // 
            // lblNo
            // 
            this.lblNo.AutoSize = true;
            this.lblNo.Location = new System.Drawing.Point(17, 43);
            this.lblNo.Name = "lblNo";
            this.lblNo.Size = new System.Drawing.Size(65, 13);
            this.lblNo.TabIndex = 1;
            this.lblNo.Text = "Số chứng từ";
            // 
            // lblPostedDate
            // 
            this.lblPostedDate.AutoSize = true;
            this.lblPostedDate.Location = new System.Drawing.Point(17, 17);
            this.lblPostedDate.Name = "lblPostedDate";
            this.lblPostedDate.Size = new System.Drawing.Size(89, 13);
            this.lblPostedDate.TabIndex = 0;
            this.lblPostedDate.Text = "Ngày hoạch toán";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridCT);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(855, 250);
            // 
            // uGridCT
            // 
            this.uGridCT.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridCT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCT.Location = new System.Drawing.Point(0, 0);
            this.uGridCT.Name = "uGridCT";
            this.uGridCT.Size = new System.Drawing.Size(855, 250);
            this.uGridCT.TabIndex = 0;
            this.uGridCT.Text = "ultraGrid1";
            // 
            // tabInfomation
            // 
            this.tabInfomation.Controls.Add(this.ultraTabSharedControlsPage1);
            this.tabInfomation.Controls.Add(this.ultraTabPageControl1);
            this.tabInfomation.Controls.Add(this.ultraTabPageControl2);
            this.tabInfomation.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabInfomation.Location = new System.Drawing.Point(0, 220);
            this.tabInfomation.Name = "tabInfomation";
            this.tabInfomation.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.tabInfomation.Size = new System.Drawing.Size(857, 273);
            this.tabInfomation.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon;
            this.tabInfomation.TabIndex = 5;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "1.Thông tin chung";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "2. Chi tiết";
            this.tabInfomation.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            this.tabInfomation.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(855, 250);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(857, 49);
            this.panel1.TabIndex = 6;
            // 
            // btnDelete
            // 
            appearance1.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.btnDelete.Appearance = appearance1;
            this.btnDelete.Location = new System.Drawing.Point(174, 10);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 11;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            appearance2.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.btnEdit.Appearance = appearance2;
            this.btnEdit.Location = new System.Drawing.Point(93, 10);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 30);
            this.btnEdit.TabIndex = 10;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            appearance3.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.btnAdd.Appearance = appearance3;
            this.btnAdd.Location = new System.Drawing.Point(12, 10);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 30);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // uGridIndex
            // 
            this.uGridIndex.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridIndex.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridIndex.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridIndex.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridIndex.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGridIndex.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGridIndex.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGridIndex.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridIndex.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridIndex.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridIndex.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridIndex.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGridIndex.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGridIndex.DisplayLayout.Scrollbars = Infragistics.Win.UltraWinGrid.Scrollbars.Vertical;
            this.uGridIndex.DisplayLayout.UseFixedHeaders = true;
            this.uGridIndex.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGridIndex.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridIndex.Location = new System.Drawing.Point(0, 49);
            this.uGridIndex.Name = "uGridIndex";
            this.uGridIndex.Size = new System.Drawing.Size(857, 171);
            this.uGridIndex.TabIndex = 7;
            this.uGridIndex.Text = "uGrid";
            this.uGridIndex.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.uGridIndex_AfterSelectChange);
            this.uGridIndex.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridIndex_DoubleClickRow);
            // 
            // FMBInternalTransfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(857, 493);
            this.Controls.Add(this.uGridIndex);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabInfomation);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FMBInternalTransfer";
            this.Text = "Thu tiền khách hàng";
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl1.PerformLayout();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridCT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabInfomation)).EndInit();
            this.tabInfomation.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridIndex)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinTabControl.UltraTabControl tabInfomation;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCT;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.Misc.UltraButton btnAdd;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridIndex;
        private System.Windows.Forms.Label lblPostedDate;
        private System.Windows.Forms.Label lblTotalAmount;
        private System.Windows.Forms.Label lblReason;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblNo;
        private System.Windows.Forms.Label txtReason;
        private System.Windows.Forms.Label txtDate;
        private System.Windows.Forms.Label txtNo;
        private System.Windows.Forms.Label txtPostedDate;
        private System.Windows.Forms.Label txtTotalAmount;
        private Infragistics.Win.Misc.UltraButton btnDelete;
        private Infragistics.Win.Misc.UltraButton btnEdit;
    }
}