﻿namespace Accounting
{
    partial class FMBInternalTransferDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            this.grHome = new Infragistics.Win.Misc.UltraGroupBox();
            this.pnlDateNo = new Infragistics.Win.Misc.UltraPanel();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblReason = new Infragistics.Win.Misc.UltraLabel();
            this.pnlUgrid = new System.Windows.Forms.Panel();
            this.palGrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.palBottom = new Infragistics.Win.Misc.UltraPanel();
            this.uGridControl = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.Header = new System.Windows.Forms.Panel();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.ultraToolTipManager1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            ((System.ComponentModel.ISupportInitialize)(this.grHome)).BeginInit();
            this.grHome.SuspendLayout();
            this.pnlDateNo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            this.pnlUgrid.SuspendLayout();
            this.palGrid.ClientArea.SuspendLayout();
            this.palGrid.SuspendLayout();
            this.palBottom.ClientArea.SuspendLayout();
            this.palBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).BeginInit();
            this.Header.SuspendLayout();
            this.SuspendLayout();
            // 
            // grHome
            // 
            this.grHome.Controls.Add(this.pnlDateNo);
            this.grHome.Controls.Add(this.txtReason);
            this.grHome.Controls.Add(this.lblReason);
            this.grHome.Dock = System.Windows.Forms.DockStyle.Top;
            appearance3.FontData.BoldAsString = "True";
            appearance3.FontData.SizeInPoints = 13F;
            this.grHome.HeaderAppearance = appearance3;
            this.grHome.Location = new System.Drawing.Point(0, 0);
            this.grHome.Name = "grHome";
            this.grHome.Size = new System.Drawing.Size(935, 131);
            this.grHome.TabIndex = 31;
            this.grHome.Text = "Chuyển tiền nội bộ";
            this.grHome.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // pnlDateNo
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.pnlDateNo.Appearance = appearance1;
            this.pnlDateNo.Location = new System.Drawing.Point(12, 28);
            this.pnlDateNo.Name = "pnlDateNo";
            this.pnlDateNo.Size = new System.Drawing.Size(380, 42);
            this.pnlDateNo.TabIndex = 26;
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason.AutoSize = false;
            this.txtReason.Location = new System.Drawing.Point(65, 78);
            this.txtReason.Multiline = true;
            this.txtReason.Name = "txtReason";
            this.txtReason.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtReason.Size = new System.Drawing.Size(858, 45);
            this.txtReason.TabIndex = 25;
            // 
            // lblReason
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.lblReason.Appearance = appearance2;
            this.lblReason.Location = new System.Drawing.Point(6, 78);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(67, 22);
            this.lblReason.TabIndex = 24;
            this.lblReason.Text = "Diễn giải";
            // 
            // pnlUgrid
            // 
            this.pnlUgrid.Controls.Add(this.palGrid);
            this.pnlUgrid.Controls.Add(this.palBottom);
            this.pnlUgrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlUgrid.Location = new System.Drawing.Point(0, 141);
            this.pnlUgrid.Name = "pnlUgrid";
            this.pnlUgrid.Size = new System.Drawing.Size(935, 600);
            this.pnlUgrid.TabIndex = 28;
            // 
            // palGrid
            // 
            this.palGrid.AutoSize = true;
            // 
            // palGrid.ClientArea
            // 
            this.palGrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.palGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palGrid.Location = new System.Drawing.Point(0, 0);
            this.palGrid.Name = "palGrid";
            this.palGrid.Size = new System.Drawing.Size(935, 534);
            this.palGrid.TabIndex = 35;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance4;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(837, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 7;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // palBottom
            // 
            // 
            // palBottom.ClientArea
            // 
            this.palBottom.ClientArea.Controls.Add(this.uGridControl);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 534);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(935, 66);
            this.palBottom.TabIndex = 34;
            // 
            // uGridControl
            // 
            this.uGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridControl.Location = new System.Drawing.Point(469, 3);
            this.uGridControl.Name = "uGridControl";
            this.uGridControl.Size = new System.Drawing.Size(466, 60);
            this.uGridControl.TabIndex = 1;
            this.uGridControl.Text = "ultraGrid1";
            // 
            // Header
            // 
            this.Header.Controls.Add(this.grHome);
            this.Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.Header.Location = new System.Drawing.Point(0, 0);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(935, 131);
            this.Header.TabIndex = 27;
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Left
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Floating;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 0);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Left";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(935, 741);
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Right
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Floating;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(0, 0);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Right";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(935, 741);
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Top
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Floating;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Top";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(935, 741);
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Bottom
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Floating;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 0);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Bottom";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(935, 741);
            // 
            // ultraToolTipManager1
            // 
            this.ultraToolTipManager1.ContainingControl = this;
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 131);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 131;
            this.ultraSplitter1.Size = new System.Drawing.Size(935, 10);
            this.ultraSplitter1.TabIndex = 1;
            this.ultraSplitter1.Click += new System.EventHandler(this.ultraSplitter1_Click);
            // 
            // FMBInternalTransferDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(935, 741);
            this.Controls.Add(this.pnlUgrid);
            this.Controls.Add(this.ultraSplitter1);
            this.Controls.Add(this.Header);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Left);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Right);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Top);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FMBInternalTransferDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FMBInternalTransferDetail_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.grHome)).EndInit();
            this.grHome.ResumeLayout(false);
            this.pnlDateNo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            this.pnlUgrid.ResumeLayout(false);
            this.pnlUgrid.PerformLayout();
            this.palGrid.ClientArea.ResumeLayout(false);
            this.palGrid.ResumeLayout(false);
            this.palGrid.PerformLayout();
            this.palBottom.ClientArea.ResumeLayout(false);
            this.palBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).EndInit();
            this.Header.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlUgrid;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Left;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Right;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Bottom;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Top;
        private Infragistics.Win.Misc.UltraGroupBox grHome;
        private System.Windows.Forms.Panel Header;
        private Infragistics.Win.UltraWinToolTip.UltraToolTipManager ultraToolTipManager1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.Misc.UltraLabel lblReason;
        private Infragistics.Win.Misc.UltraPanel pnlDateNo;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraPanel palBottom;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridControl;
        private Infragistics.Win.Misc.UltraPanel palGrid;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
    }
}
