﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.Model;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FMBInternalTransfer : CustormForm
    {
        private readonly int TypeId = 150;
        private readonly IMBInternalTransferService _internalTransferService;
        private List<MBInternalTransfer> lstMbInternalTransfer;
        private ITemplateColumnService _templateColumnSrv;
        private IAccountDefaultService _accountDefaultSrv;
        private IAccountService _accountSrv;
        List<BankAccountDetail> _dsBankAccountDetail = new List<BankAccountDetail>();
        List<BudgetItem> _dsBudgetItem = new List<BudgetItem>();    //Bảng BudgetItem
        List<ExpenseItem> _dsExpenseItem = new List<ExpenseItem>();    //Bảng ExpenseItem
        List<Department> _dsDepartment = new List<Department>();    //Bảng Department
        List<CostSet> _dsCostSet = new List<CostSet>();    //Bảng CostSet
        List<StatisticsCode> _dsStatisticsCode = new List<StatisticsCode>();    //Bảng StatisticsCode
        List<EMContract> _dsEMContract = new List<EMContract>();    //Bảng EMContract
        List<Account> lstAccount = new List<Account>();
        List<Currency> _dsCurrency = new List<Currency>();
        List<AccountingObject> _dsEmployee = new List<AccountingObject>();
        public FMBInternalTransfer()
        {
            _internalTransferService = IoC.Resolve<IMBInternalTransferService>();
            _templateColumnSrv = IoC.Resolve<ITemplateColumnService>();
            _accountDefaultSrv = IoC.Resolve<IAccountDefaultService>();
            _accountSrv = IoC.Resolve<IAccountService>();
            lstAccount = _accountSrv.GetListAccountIsActive(true);
            _dsBankAccountDetail = IoC.Resolve<IBankAccountDetailService>().GetIsActive(true);
            _dsBudgetItem = IoC.Resolve<IBudgetItemService>().GetByActive_OrderByTreeIsParentNode(true);
            _dsExpenseItem = IoC.Resolve<IExpenseItemService>().GetByActive_OrderByTreeIsParentNode(true);
            _dsDepartment = IoC.Resolve<IDepartmentService>().GetByActive_OrderByTreeIsParentNode(true);
            _dsCostSet = IoC.Resolve<ICostSetService>().GetByActive_OrderByTreeIsParentNode(true);
            _dsStatisticsCode = IoC.Resolve<IStatisticsCodeService>().GetByActive_OrderByTreeIsParentNode(true);
            _dsEMContract = IoC.Resolve<IEMContractService>().GetAll();
            _dsCurrency = IoC.Resolve<ICurrencyService>().GetIsActive(true);
            _dsEmployee = IoC.Resolve<IAccountingObjectService>().GetListAccountingObjectByEmployee(true);
            lstMbInternalTransfer = _internalTransferService.GetAll();
            InitializeComponent();
            // hiện thị danh sách thu tiền khách hàng
            ViewTabIndex(lstMbInternalTransfer);
            // mặc định hiển thị chi tiết thông tin khách hàng đầu tiên

            MBInternalTransfer model = lstMbInternalTransfer.Count > 0
                                           ? lstMbInternalTransfer[0]
                                           : new MBInternalTransfer();
            ViewTabInfomation(model);
            // tab chi tiết
            var c = _templateColumnSrv.GetTemplateDetailDefaultByTypeId(150);
            uGridCT.DataSource = model.MBInternalTransferDetails.Count > 0
                                     ? model.MBInternalTransferDetails
                                     : new List<MBInternalTransferDetail>();
            ViewTabDetails(uGridCT, c);
        }

        // thực hiện khi chọn một khách hàng trong danh sách khách hàng có phát sinh
        private void uGridIndex_AfterSelectChange(object sender, Infragistics.Win.UltraWinGrid.AfterSelectChangeEventArgs e)
        {
            // lấy đối tượng được chọn
            if (uGridIndex.Selected.Rows.Count > 0)
            {
                MBInternalTransfer model = uGridIndex.Selected.Rows[0].ListObject as MBInternalTransfer;
                ViewTabInfomation(model);
            }

        }

        public void ViewTabIndex(List<MBInternalTransfer> lstMbInternalTranfer)
        {
            uGridIndex.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            uGridIndex.DataSource = lstMbInternalTranfer;
            Utils.ConfigGrid(uGridIndex, ConstDatabase.FMBInternalTranfer_TableName);
            //Tổng số hàng
            uGridIndex.DisplayLayout.Override.AllowRowSummaries = AllowRowSummaries.Default; //Ẩn ký hiệu tổng trên Header Caption
            UltraGridBand band = uGridIndex.DisplayLayout.Bands[0];
            if (band.Summaries.Count != 0) band.Summaries.Clear();
            SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["Date"]);
            summary.DisplayFormat = "Số dòng = {0:N0}";
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            uGridIndex.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False;   //ẩn
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            uGridIndex.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            uGridIndex.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
            uGridIndex.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
            // Thêm tổng số ở cột "Số còn phải thu"
            summary = band.Summaries.Add("TotalAmount", SummaryType.Sum, band.Columns["TotalAmount"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
        }
        public void ViewTabInfomation(MBInternalTransfer model)
        {
            // thực hiện hiển thị thông tin lên tab thông tin chung
            txtPostedDate.Text = model.PostedDate.Year < 1970 ? "" : model.PostedDate.ToString("dd/MM/yyyy");
            txtNo.Text = string.IsNullOrEmpty(model.No) ? "" : model.No;
            txtDate.Text = model.Date.Year < 1970 ? "" : model.Date.ToString("dd/MM/yyyy");
            txtReason.Text = string.IsNullOrEmpty(model.Reason) ? "" : model.Reason;
            txtTotalAmount.Text = model.TotalAmount == 0 ? "" : model.TotalAmount.ToString();
            // thực hiện hiển thị thông tin lên tab chi tiết
            uGridCT.DataSource = model.MBInternalTransferDetails;
        }
        void ViewTabDetails(UltraGrid uGridInput, IList<TemplateColumn> mauGiaoDien)
        {
            Utils.ConfigGrid(uGridInput, "", mauGiaoDien, true);
            //Thêm tổng số ở cột "Số tiền"
            SummarySettings summary = uGridInput.DisplayLayout.Bands[0].Summaries.Add("SumAmountOriginal", SummaryType.Sum, uGridInput.DisplayLayout.Bands[0].Columns["AmountOriginal"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            //Thêm tổng số ở cột "Số tiền QĐ"
            summary = uGridInput.DisplayLayout.Bands[0].Summaries.Add("SumAmount", SummaryType.Sum, uGridInput.DisplayLayout.Bands[0].Columns["Amount"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            foreach (var item in uGridInput.DisplayLayout.Bands[0].Columns)
            {
                if (item.Key.Equals("DebitAccount") || item.Key.Equals("CreditAccount")) //tài khoản nợ, tài khoản có
                {
                    List<Account> lstTemp =
                        _accountDefaultSrv.GetAccountDefaultByTypeId(TypeId, item.Key).Count > 0
                            ? _accountDefaultSrv.GetAccountDefaultByTypeId(TypeId,
                                                                               item.Key)
                            : lstAccount.OrderBy(k => k.AccountName).ToList();
                    UltraCombo cbbDebitAccount = new UltraCombo();
                    cbbDebitAccount.DataSource = lstTemp;
                    cbbDebitAccount.DisplayMember = "AccountNumber";
                    Utils.ConfigGrid(cbbDebitAccount, ConstDatabase.Currency_TableName);
                    item.ValueList = cbbDebitAccount;
                    //cbbDebitAccount.ConfigCbbToGrid(uGridInput, item.Key, lstTemp, "AccountNumber", "AccountNumber", ConstDatabase.Account_TableName);
                }
                //TK Ngân hàng
                if (item.Key.Equals("FromBankAccountDetailID") || item.Key.Equals("ToBankAccountDetailID"))
                    this.ConfigCbbToGrid(uGridInput, item.Key, _dsBankAccountDetail, "ID", "BankAccount",
                                         ConstDatabase.BankAccountDetail_TableName);
                // Loại tiền
                if (item.Key.Equals("CurrencyID"))
                {
                    item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
                    UltraCombo cbbCurrency = new UltraCombo();
                    cbbCurrency.DataSource = _dsCurrency;
                    cbbCurrency.DisplayMember = "ID";
                    Utils.ConfigGrid(cbbCurrency, ConstDatabase.Currency_TableName);
                    item.ValueList = cbbCurrency;
                }
                //
                if (item.Key.Equals("Quantity") || item.Key.Equals("Amount") || item.Key.Equals("AmountOriginal"))
                {
                    item.Nullable = Infragistics.Win.UltraWinGrid.Nullable.Nothing;
                    item.NullText = "0";
                    item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                    if (item.Key.Equals("Quantity"))
                        Utils.FormatNumberic(item, ConstDatabase.Format_Quantity);
                    else
                        Utils.FormatNumberic(item, ConstDatabase.Format_TienVND);
                }
                // mục thu chi
                if (item.Key.Equals("BudgetItemID"))
                    this.ConfigCbbToGrid(uGridInput, item.Key, _dsBudgetItem, "ID", "BudgetItemCode",
                                         ConstDatabase.BudgetItem_TableName, "ParentID", "IsParentNode");
                item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                //Khoản mục CP
                if (item.Key.Equals("ExpenseItemID"))
                    this.ConfigCbbToGrid(uGridInput, item.Key, _dsExpenseItem, "ID", "ExpenseItemCode",
                                         ConstDatabase.ExpenseItem_TableName, "ParentID", "IsParentNode");
                item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                //Phòng ban
                if (item.Key.Equals("DepartmentID"))
                    this.ConfigCbbToGrid(uGridInput, item.Key, _dsDepartment, "ID", "DepartmentCode",
                                         ConstDatabase.Department_TableName, "ParentID", "IsParentNode");
                item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                //Đối tượng tập hợp CP
                if (item.Key.Equals("CostSetID"))
                    this.ConfigCbbToGrid(uGridInput, item.Key, _dsCostSet, "ID", "CostSetCode",
                                         ConstDatabase.CostSet_TableName, "ParentID", "IsParentNode");
                item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                //Hợp đồng
                if (item.Key.Equals("ContractID"))
                    this.ConfigCbbToGrid(uGridInput, item.Key, _dsEMContract, "ID", "Code",
                                         ConstDatabase.EMContract_TableName);
                item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                //Mã thống kê
                if (item.Key.Equals("StatisticsCodeID"))
                    this.ConfigCbbToGrid(uGridInput, item.Key, _dsStatisticsCode, "ID", "StatisticsCode_",
                                         ConstDatabase.StatisticsCode_TableName, "ParentID", "IsParentNode");
                item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                if (item.Key.Equals("EmployeeID"))
                    this.ConfigCbbToGrid(uGridInput, item.Key,
                                         _dsEmployee, "ID",
                                         "AccountingObjectCode", ConstDatabase.AccountingObject_TableName);
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            new FMBInternalTransferDetail(new MBInternalTransfer(), lstMbInternalTransfer, ConstFrm.optStatusForm.Add).ShowDialog(this);
            uGridIndex.DataSource = _internalTransferService.GetAll();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            // lấy đối tượng được chọn
            if (uGridIndex.Selected.Rows.Count > 0)
            {
                MBInternalTransfer model = uGridIndex.Selected.Rows[0].ListObject as MBInternalTransfer;
                if (model.Recorded)
                {
                    MessageBox.Show("Bạn không thể xóa chứng từ đã được ghi sổ");
                    return;
                }
                _internalTransferService.Delete(model);
                _internalTransferService.CommitChanges();
                uGridIndex.DataSource = lstMbInternalTransfer = _internalTransferService.GetAll();
                if (lstMbInternalTransfer.Count > 0)
                    ViewTabInfomation(lstMbInternalTransfer[0]);
                MessageBox.Show("Xóa thành công !");
            }
            MessageBox.Show("Danh sách trống !");
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (uGridIndex.Selected.Rows.Count > 0)
            {
                MBInternalTransfer model = uGridIndex.Selected.Rows[0].ListObject as MBInternalTransfer;
                new FMBInternalTransferDetail(model, lstMbInternalTransfer, 1).ShowDialog(this);
                uGridIndex.DataSource = _internalTransferService.GetAll();
            }
            MessageBox.Show("Bạn chưa chọn chứng từ cần sửa");

        }

        private void uGridIndex_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (uGridIndex.Selected.Rows.Count > 0)
            {
                MBInternalTransfer model = uGridIndex.Selected.Rows[0].ListObject as MBInternalTransfer;
                new FMBInternalTransferDetail(model, lstMbInternalTransfer, 1).ShowDialog(this);
                uGridIndex.DataSource = _internalTransferService.GetAll();
            }
            MSG.Error(resSystem.MSG_System_04);
        }
    }
}
