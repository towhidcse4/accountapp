﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinGrid;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;
using System.Windows.Forms;
using FX.Core;
using Infragistics.Win.UltraWinTabControl;

namespace Accounting
{
    public partial class FMBInternalTransferDetail : FMBInternalTransferBase
    {
        #region khởi tạo

        private readonly IMBInternalTransferService _internalTransferSrv;
        private readonly IAccountDefaultService _accountDefaultSrv;

        private UltraGrid uGridPosted;
        private UltraGrid ugrid2 = null;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        public FMBInternalTransferDetail(MBInternalTransfer temp, List<MBInternalTransfer> lstItems, int statusForm)
        {
            //Add -> thêm, Sửa -> sửa, Xem -> Sửa ở trạng thái Enable = False


            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();
           

            #endregion

            #region Thiết lập ban đầu cho Form
            _statusForm = statusForm;
            
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.TypeID = 150;

            }
            else
            {
                _select = temp;
            }

            _listSelects.AddRange(lstItems);
            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, _listSelects, statusForm);
            
            #endregion
        }
        #endregion
        #region override
        public override void InitializeGUI(MBInternalTransfer input)
        {
           
            //Template mauGiaoDien = input.TemplateID.HasValue ? ITemplateService.Getbykey(input.TemplateID.Value) : getMauChuan(input.TypeID);
            Template mauGiaoDien = Utils.GetMauGiaoDien(input.TypeID, input.TemplateID, Utils.ListTemplate);
            input.TemplateID = mauGiaoDien.ID;
            _select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmount;
            _select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add ? "VND" : input.CurrencyID;
            List<MBInternalTransfer> select = new List<MBInternalTransfer> { _select };
            

            #region Body
            BindingList<MBInternalTransferDetail> listMBInternalTransferDetail = new BindingList<MBInternalTransferDetail>();
            BindingList<MBInternalTransferTax> listMBInternalTransferTax = new BindingList<MBInternalTransferTax>();
            BindingList<RefVoucher> _bdlRefVoucher = new BindingList<RefVoucher>();

            _bdlRefVoucher = new BindingList<RefVoucher>(input.RefVouchers);
            listMBInternalTransferDetail = new BindingList<MBInternalTransferDetail>(input.MBInternalTransferDetails);
            listMBInternalTransferTax = new BindingList<MBInternalTransferTax>(input.MBInternalTransferTaxs);

            _listObjectInput = new BindingList<System.Collections.IList> { listMBInternalTransferDetail, listMBInternalTransferTax };
            _listObjectInputPost = new BindingList<System.Collections.IList> { _bdlRefVoucher };
            this.ConfigGridByTemplete_General<MBInternalTransfer>(palGrid, mauGiaoDien);
            //this.ConfigGridByTemplete<MBInternalTransfer>(input.TypeID, mauGiaoDien, true, _listObjectInput);
            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { true, true, false };
            this.ConfigGridByManyTemplete<MBInternalTransfer>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());

            this.ConfigGridCurrencyByTemplate<MBInternalTransfer>(_select.TypeID, new BindingList<System.Collections.IList> { select },
                                              uGridControl, mauGiaoDien);
            this.ConfigTopVouchersNo<MBInternalTransfer>(pnlDateNo, "No", "PostedDate", "Date");
           

            DataBinding(new List<Control> { txtReason }, "Reason");
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
            }


            #endregion
            ugrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
            ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 150)//trungnq thêm vào để  làm task 6234
                {
                    txtReason.Value = "Chuyển tiền nội bộ";
                    txtReason.Text = "Chuyển tiền nội bộ";
                    _select.Reason = "Chuyển tiền nội bộ";
                };
            }
        }
        #endregion

        #region Utils
        #endregion

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucher>();
                    var f = new FViewVoucherOriginal(_select.CurrencyID, datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void ultraSplitter1_Click(object sender, EventArgs e)
        {
            //palGrid.Dock = DockStyle.Fill;
           
        }

        private void FMBInternalTransferDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }
    public class FMBInternalTransferBase : DetailBase<MBInternalTransfer>
    {
    }
}