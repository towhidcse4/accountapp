﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using CellClickAction = Infragistics.Win.UltraWinTree.CellClickAction;

namespace Accounting
{
    public partial class GetMatchedVouchers : CustormForm
    {
        private  IAccountingObjectBankAccountService _IAccountingObjectBankAccountService;
        private  IBankAccountDetailService _IBankAccountDetailService;
        List<BankAccountDetail> _listBankAccountDetail = new List<BankAccountDetail>();
        public GetMatchedVouchers()
        {
           
            InitializeComponent();
            LoadDulieu();
            ButGiup.Visible = false;


        }

        public void LoadDulieu()
        {
            _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            _listBankAccountDetail = _IBankAccountDetailService.GetAll();
            List<ComparedVoucher> lst = new List<ComparedVoucher>();
           lst = _IAccountingObjectBankAccountService.GetComparedVoucher(_listBankAccountDetail.Select(c=>c.ID).ToList());
           
            ultraGridDSchungtuDaDoiChieu.DataSource = lst;
            Utils.ConfigGrid(ultraGridDSchungtuDaDoiChieu, ConstDatabase.ComparedVoucher_TableName);
            //tự thay đổi kích thước cột
            ultraGridDSchungtuDaDoiChieu.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
          
            Utils.AddSumColumn(ultraGridDSchungtuDaDoiChieu, "Amount", false, "", ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(ultraGridDSchungtuDaDoiChieu.DisplayLayout.Bands[0].Columns["Amount"], ConstDatabase.Format_TienVND);
        }
        private void ButDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
