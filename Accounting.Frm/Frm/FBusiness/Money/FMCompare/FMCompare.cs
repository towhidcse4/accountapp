﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using CellClickAction = Infragistics.Win.UltraWinTree.CellClickAction;

namespace Accounting
{
    public partial class FMCompare : CustormForm
    {
        #region Khởi tạo
        //Kết nối đến Iservice
        private readonly IAccountingObjectBankAccountService _IAccountingObjectBankAccountService;
        private readonly IBankAccountDetailService _IBankAccountDetailService;
        private readonly ICurrencyService _ICurrencyService;
        List<Compared> lst = new List<Compared>();
        List<Currency> lstcCurrencies = new List<Currency>(); 
        
        #endregion
        #region Hàm Main
        public FMCompare()
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            #region Khai Báo
            _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            _ICurrencyService = IoC.Resolve<ICurrencyService>();
            #endregion
            LoadDuLieu();
            cbbLoaiTien.SelectedRow = cbbLoaiTien.Rows[lstcCurrencies.FindIndex(c => c.ID == "VND")];
            WaitingFrm.StopWaiting();
        }
        #endregion
        #region Hàm Chung
        List<CollectionVoucher> lstphieuthu = new List<CollectionVoucher>();
        List<SpendingVoucher> lstphieuchi = new List<SpendingVoucher>();
        private void LoadDuLieu()
        {
            // Load dữ liệu combobox nhà cung cấp
            cbbTaiKhoanNganHang.DataSource = _IBankAccountDetailService.GetAll().OrderBy(c=>c.BankAccount).ToList();
            cbbTaiKhoanNganHang.DisplayMember = "BankAccount";
            Utils.ConfigGrid(cbbTaiKhoanNganHang, ConstDatabase.BankAccountDetail_TableName);            
            //Load Ngày đối chiếu với phần mềm
            dtNgayDoiChieu.DateTime = Utils.StringToDateTime(Utils.GetDbStartDate()) ?? DateTime.Now;
            dtTuNgayDoiChieu.DateTime = DateTime.Now;
            dtTuNgayDoiChieu.Enabled = false;
            //Load dữ liệu combobox loại tiền tệ
            lstcCurrencies = _ICurrencyService.Query.Where(x=>x.IsActive).ToList();
            cbbLoaiTien.DataSource = lstcCurrencies;
            cbbLoaiTien.DisplayMember = "ID";
            Utils.ConfigGrid(cbbLoaiTien, ConstDatabase.Currency_TableName);
            txtSoDuNgHang.Value = txtSoDuTaiNganHang.Value = 0;
            UgridDSchungtuthutien.SetDataBinding(lstphieuthu,"");
            //UgridDSchungtuthutien.DataSource = lstphieuthu;
            ViewDsach(UgridDSchungtuthutien, GetTemplate(true, true));
            uGridDSchungtuchitien.SetDataBinding(lstphieuchi, "");
           // uGridDSchungtuchitien.DataSource = lstphieuthu;
            ViewDsach(uGridDSchungtuchitien, GetTemplate(false, true));
            if (cbbTaiKhoanNganHang.Rows.Count > 0) cbbTaiKhoanNganHang.SelectedRow = cbbTaiKhoanNganHang.Rows[0];
        }

        public void LoadGrid()
        {
            txtSoDuNgHang.Value = txtSoDuTaiNganHang.Value = 0;
            txtChenhLech.Value = Convert.ToDecimal(txtDaKhopDoiChieu.Value) - Convert.ToDecimal(txtSoDuNgHang.Value);
            //var bankAccountDetail = cbbTaiKhoanNganHang.SelectedRow.ListObject as BankAccountDetail;
            //var currency = cbbLoaiTien.SelectedRow.ListObject as Currency;
            //UgridDSchungtuthutien.DataSource = _IAccountingObjectBankAccountService.GetCollectionVoucher(bankAccountDetail.ID, currency.ID, dtNgayDoiChieu.DateTime, false);
            //uGridDSchungtuchitien.DataSource = _IAccountingObjectBankAccountService.GetSpendingVoucher(bankAccountDetail.ID, currency.ID, dtNgayDoiChieu.DateTime, false);
            //txtDaKhop.Value = _IAccountingObjectBankAccountService.GetMatchedOverBalance(bankAccountDetail.ID, currency.ID, dtNgayDoiChieu.DateTime);
            foreach (var item in uGridDSchungtuchitien.Rows)
            {
                item.Cells["Status"].Value = false;//edit by cuongpv Check->Status
            }
            var lst = _IAccountingObjectBankAccountService.GetCompared(_IBankAccountDetailService.GetAll().Select(c => c.ID).ToList()).OrderByDescending(x => x.DateCompare).ToList();
            dtTuNgayDoiChieu.DateTime = lst.Count > 0 ? lst[0].DateCompare.AddDays(1) : (Utils.StringToDateTime(Utils.GetApplicationStartDate()) ?? DateTime.Now);
            dtNgayDoiChieu.DateTime = dtTuNgayDoiChieu.DateTime;
            loadData();
            
        }
        public void UpdateForm()
        {
            int countCollection = 0;
            int countSpending = 0;
            int countCollectionChecked = 0;
            int countSpendingChecked = 0;
            decimal totalCollection = 0;
            decimal totalSpending = 0;
            UgridDSchungtuthutien.UpdateData();
            uGridDSchungtuchitien.UpdateData();
            if (lstphieuthu.Count > 0)
            {
                countCollection = lstphieuthu.Count;
                totalCollection = lstphieuthu.Where(c => c.Status).Sum(c => c.AmountOriginal);
                countCollectionChecked = lstphieuthu.Count(c => c.Status);
            }
            if (lstphieuchi.Count > 0)
            {
                countSpending = lstphieuchi.Count;
                totalSpending = lstphieuchi.Where(c => c.Status).Sum(c => c.AmountOriginal);
                countSpendingChecked = lstphieuchi.Count(c => c.Status);
            }
            lblChungTuThu.Text = countCollectionChecked + "/" + countCollection + " Thu";
            lblChungTuChi.Text = countSpendingChecked + "/" + countSpending + " Chi";
            lblTongChungTuThu.FormatNumberic(totalCollection, 1);
            lblTongChungTuChi.FormatNumberic(totalSpending, 1);
            //Số dư cuối kỳ 
            //Đã khớp Đối chiếu
            txtDaKhopDoiChieu.Value = Convert.ToDecimal(string.IsNullOrEmpty(txtDaKhop.Text) ? 0 : txtDaKhop.Value) + totalCollection - totalSpending;
            //Chênh lệnh
            txtChenhLech.Value = Convert.ToDecimal(txtDaKhopDoiChieu.Value) - Convert.ToDecimal(txtSoDuNgHang.Value);

        }
        private void txtSoDuTaiNganHang_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtSoDuTaiNganHang.Text == "")
            {
                txtSoDuTaiNganHang.Text = "0";
            }
        }

        private void txtSoDuTaiNganHang_KeyUp(object sender, KeyEventArgs e)
        {
            if (txtSoDuTaiNganHang.Text == "")
            {
                txtSoDuTaiNganHang.Text = "0";
            }
        }
        #endregion
        #region Header
        private void cbbTaiKhoanNganHang_ValueChanged(object sender, EventArgs e)
        {
            if (cbbTaiKhoanNganHang.SelectedRow.ListObject != null)
            {
                var model = cbbTaiKhoanNganHang.SelectedRow.ListObject as BankAccountDetail;
                TxtTenNganHang.Text = model.BankName;
                List<Guid> lstID = new List<Guid> { model.ID };
                var lst = _IAccountingObjectBankAccountService.GetCompared(lstID).OrderByDescending(x => x.DateCompare).ToList();
                dtTuNgayDoiChieu.DateTime = lst.Count > 0 ? lst[0].DateCompare.AddDays(1) : (Utils.StringToDateTime(Utils.GetApplicationStartDate()) ?? DateTime.Now);
                if (dtNgayDoiChieu.DateTime < dtTuNgayDoiChieu.DateTime) dtNgayDoiChieu.DateTime = dtTuNgayDoiChieu.DateTime;
            }
        }
        private void cbbLoaiTien_ValueChanged(object sender, EventArgs e)
        {
            if (cbbLoaiTien.SelectedRow != null)
            {
                var model = cbbLoaiTien.SelectedRow.ListObject as Currency;
                if (model.ID == "VND") 
                {
                    ViewDsach(UgridDSchungtuthutien, GetTemplate(true, true));
                    ViewDsach(uGridDSchungtuchitien, GetTemplate(false, true));
                }
                else
                {
                    ViewDsach(UgridDSchungtuthutien, GetTemplate(true, false));
                    ViewDsach(uGridDSchungtuchitien, GetTemplate(false, false));
                }
            }
        }
        private void TxtTenNganHang_ValueChanged(object sender, EventArgs e)
        {
            if (txtSoDuTaiNganHang.Text != "")
            {
                txtSoDuNgHang.Value = txtSoDuTaiNganHang.Value;
            }
            UpdateForm();
        }

        private void dtNgayDoiChieu_Leave(object sender, EventArgs e)
        {
            if (dtNgayDoiChieu.DateTime < dtTuNgayDoiChieu.DateTime)
            {
                MSG.Warning("Đến ngày không được nhỏ hơn từ ngày!");
                dtNgayDoiChieu.Focus();
            }
        }
        private void ButLaySoLieu_Click(object sender, EventArgs e)
        {
            if (cbbTaiKhoanNganHang.SelectedRow == null)
            {
                MSG.Warning("Bạn cần chọn tài khoản ngân hàng");
                return;
            }
            if (cbbLoaiTien.SelectedRow == null)
            {
                MSG.Warning("Bạn chưa chọn loại tiền tệ");
                return;
            }
            loadData();
        }
        private void loadData()
        {
            WaitingFrm.StartWaiting();
            var bankAccountDetail = cbbTaiKhoanNganHang.SelectedRow.ListObject as BankAccountDetail;
            var currency = cbbLoaiTien.SelectedRow.ListObject as Currency;
            lstphieuthu = _IAccountingObjectBankAccountService.GetCollectionVoucher(bankAccountDetail.ID, currency.ID, dtNgayDoiChieu.DateTime, false);
            UgridDSchungtuthutien.SetDataBinding(lstphieuthu, "");
            lstphieuchi = _IAccountingObjectBankAccountService.GetSpendingVoucher(bankAccountDetail.ID, currency.ID, dtNgayDoiChieu.DateTime, false);
            uGridDSchungtuchitien.SetDataBinding(lstphieuchi, "");
            txtDaKhop.Value = _IAccountingObjectBankAccountService.GetMatchedOverBalance(bankAccountDetail.ID, currency.ID, dtTuNgayDoiChieu.DateTime.AddDays(-1));
            UpdateForm();
            WaitingFrm.StopWaiting();
        }
        #endregion
        #region Body
        public List<TemplateColumn> GetTemplate(bool isCollectionVoucher, bool isVnd)
        {
            string accountingObjectName = isCollectionVoucher ? "Đối tượng nộp" : "Đối tượng nhận";
            string amount = isVnd ? "Số tiền" : "Quy đổi";
            return
                new List<TemplateColumn>
                    {
                        new TemplateColumn
                            {
                                ColumnName = "Status",
                                ColumnCaption = "",
                                ColumnWidth = 30,
                                ColumnMaxWidth = 60,
                                IsVisible = true,
                                IsVisibleCbb = true,
                                VisiblePosition = 1,
                                
                                
                            },
                        new TemplateColumn
                            {
                                ColumnName = "Date",
                                ColumnCaption = "Ngày",
                                ColumnWidth = 100,
                                IsVisible = true,
                                IsVisibleCbb = true,
                                VisiblePosition = 2,
                                IsReadOnly = true
                            },
                        new TemplateColumn
                            {
                                ColumnName = "No",
                                ColumnCaption = "Số",
                                ColumnWidth = 100,
                                IsVisible = true,
                                IsVisibleCbb = true,
                                VisiblePosition = 3,
                                IsReadOnly = true
                            },
                        new TemplateColumn
                            {
                                ColumnName = "AccountingObjectName",
                                ColumnCaption = accountingObjectName,
                                ColumnWidth = 100,
                                IsVisible = true,
                                IsVisibleCbb = true,
                                VisiblePosition = 4,
                                IsReadOnly = true
                            },
                        new TemplateColumn
                            {
                                ColumnName = "Rate",
                                ColumnCaption = "Tỷ giá",
                                ColumnWidth = 100,
                                IsVisible = !isVnd,
                                IsVisibleCbb = true,
                                VisiblePosition = 5,
                                IsReadOnly = true
                            },
                        new TemplateColumn
                            {
                                ColumnName = "Amount",
                                ColumnCaption = amount,
                                ColumnWidth = 100,
                                IsVisible = !isVnd,
                                IsVisibleCbb = true,
                                VisiblePosition = 8,
                                IsReadOnly = true
                            },
                        new TemplateColumn
                            {
                                ColumnName = "AmountOriginal",
                                ColumnCaption = "Số tiền",
                                ColumnWidth = 100,
                                IsVisible = true,
                                IsVisibleCbb = true,
                                VisiblePosition = 7,
                                IsReadOnly = true
                            },
                        new TemplateColumn
                            {
                                ColumnName = "Description",
                                ColumnCaption = "Diễn giải",
                                ColumnWidth = 100,
                                IsVisible = true,
                                IsVisibleCbb = true,
                                VisiblePosition = 6,
                                IsReadOnly = true
                            }
                    };
        }
        private void uGridSpendingVoucher_CellChange(object sender, CellEventArgs e)
        {
            UpdateForm();
        }
        private void ViewDsach(UltraGrid uGrid, List<TemplateColumn> template)
        {
            Utils.ConfigGrid(uGrid, "", template);
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.Override.SelectedRowAppearance.BackColor = Color.White;
            uGrid.DisplayLayout.Override.HotTrackRowAppearance.ForeColor = Color.White;
            uGrid.DisplayLayout.Override.HotTrackRowSelectorAppearance.BackColor = Color.FromArgb(51, 153, 255);
            uGrid.DisplayLayout.Override.HotTrackRowCellAppearance.BackColor = Color.FromArgb(51, 153, 255);
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            // xét style cho các cột có dạng checkbox
            UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;
            ugc.SetHeaderCheckedState(uGrid.Rows, false);//Hàm dùng để bỏ tích trên cùng khi load form
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["Amount"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["Rate"], ConstDatabase.Format_Rate);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["AmountOriginal"],
                                 ConstDatabase.Format_ForeignCurrency);

        }
        private void UgridDSchungtuthutien_AfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {
            if (lstphieuthu.Count > 0)
            {
                UpdateForm();
            }
        }

        private void uGridDSchungtuchitien_AfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {
            if (lstphieuchi.Count > 0)
            {
                UpdateForm();
            }
        }

        #endregion
        #region Footer
        private void ButThucHienDoiChieu_Click(object sender, EventArgs e)
        {           
            if (Convert.ToDecimal(txtSoDuNgHang.Value) != 0 && Convert.ToDecimal(txtChenhLech.Value) == 0 && (lstphieuthu.Any(c => c.Status == true) || lstphieuchi.Any(c => c.Status == true)))
            {
                WaitingFrm.StartWaiting();
                List<Compared> lsCompareds =
                    lstphieuthu.Select(
                        c =>
                            new Compared()
                            {
                                Id = c.Id,
                                BankAccountID = c.BankAccountDetailId,
                                BankAccount = "",
                                BankName = "",
                                DateCompare = DateTime.MinValue,
                                Status = c.Status
                            }).ToList();
                List<Compared> lsComparedsp =
                    lstphieuchi.Select( 
                        c =>
                            new Compared()
                            {
                                Id = c.Id,
                                BankAccountID = c.BankAccountDetailId,
                                BankAccount = "",
                                BankName = "",
                                DateCompare = DateTime.MinValue,
                                Status = c.Status
                            }).ToList();
                _IAccountingObjectBankAccountService.ComparedV(lsCompareds.Where(c => c.Status == true).ToList(), (DateTime)dtNgayDoiChieu.Value);
                _IAccountingObjectBankAccountService.ComparedV(lsComparedsp.Where(c => c.Status == true).ToList(), (DateTime)dtNgayDoiChieu.Value);
                LoadGrid();
                //loadData();
                WaitingFrm.StopWaiting();
                MSG.Information("Đối chiếu thành công");

            }
            else
            {
                MSG.Warning("Đối chiếu không thành công, kiểm tra lại chứng từ");
            }

        }
        private void ButHuyBo_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void butXemChungTuDaDoiChieu_Click(object sender, EventArgs e)
        {
            GetMatchedVouchers gmGetMatchedVouchers = new GetMatchedVouchers();
            gmGetMatchedVouchers.Show();

        }

        private void ButBoDoiChieu_Click(object sender, EventArgs e)
        {
            BodoiChieu();
        }

        void BodoiChieu()
        {
            new RemoveMathVouchers().ShowDialog(this);
            if (cbbTaiKhoanNganHang.Text != "")
            {
                if (RemoveMathVouchers.IsClose && RemoveMathVouchers.IsClose1 == true) LoadGrid();
               
            }
            
            
        }

        #endregion

        
    }

}
