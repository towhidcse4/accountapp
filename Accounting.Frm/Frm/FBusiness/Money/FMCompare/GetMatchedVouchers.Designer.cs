﻿namespace Accounting
{
    partial class GetMatchedVouchers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            this.ultraGridDSchungtuDaDoiChieu = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ButDong = new Infragistics.Win.Misc.UltraButton();
            this.ButGiup = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridDSchungtuDaDoiChieu)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGridDSchungtuDaDoiChieu
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.Appearance = appearance1;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.Override.CellAppearance = appearance8;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.Override.RowAppearance = appearance11;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGridDSchungtuDaDoiChieu.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.ultraGridDSchungtuDaDoiChieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGridDSchungtuDaDoiChieu.Location = new System.Drawing.Point(0, 0);
            this.ultraGridDSchungtuDaDoiChieu.Name = "ultraGridDSchungtuDaDoiChieu";
            this.ultraGridDSchungtuDaDoiChieu.Size = new System.Drawing.Size(1019, 513);
            this.ultraGridDSchungtuDaDoiChieu.TabIndex = 0;
            this.ultraGridDSchungtuDaDoiChieu.Text = "ultraGrid1";
            // 
            // ButDong
            // 
            appearance13.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.ButDong.Appearance = appearance13;
            this.ButDong.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.ButDong.Location = new System.Drawing.Point(929, 517);
            this.ButDong.Name = "ButDong";
            this.ButDong.Size = new System.Drawing.Size(75, 30);
            this.ButDong.TabIndex = 51;
            this.ButDong.Text = "Đóng";
            this.ButDong.Click += new System.EventHandler(this.ButDong_Click);
            // 
            // ButGiup
            // 
            appearance14.Image = global::Accounting.Properties.Resources.ubtnHelp;
            this.ButGiup.Appearance = appearance14;
            this.ButGiup.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.ButGiup.Location = new System.Drawing.Point(0, 523);
            this.ButGiup.Name = "ButGiup";
            this.ButGiup.Size = new System.Drawing.Size(75, 30);
            this.ButGiup.TabIndex = 51;
            this.ButGiup.Text = "Giúp";
            // 
            // GetMatchedVouchers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 552);
            this.Controls.Add(this.ButGiup);
            this.Controls.Add(this.ButDong);
            this.Controls.Add(this.ultraGridDSchungtuDaDoiChieu);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1035, 591);
            this.MinimumSize = new System.Drawing.Size(1035, 591);
            this.Name = "GetMatchedVouchers";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Danh sách chứng từ đã đối chiếu với ngân hàng";
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridDSchungtuDaDoiChieu)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGridDSchungtuDaDoiChieu;
        private Infragistics.Win.Misc.UltraButton ButDong;
        private Infragistics.Win.Misc.UltraButton ButGiup;
    }
}