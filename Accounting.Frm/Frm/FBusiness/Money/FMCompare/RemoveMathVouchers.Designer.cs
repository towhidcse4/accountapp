﻿namespace Accounting
{
    partial class RemoveMathVouchers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RemoveMathVouchers));
            this.ButDongY = new Infragistics.Win.Misc.UltraButton();
            this.ButHuyBo = new Infragistics.Win.Misc.UltraButton();
            this.ultraGridDsDoiChieu = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridDsDoiChieu)).BeginInit();
            this.SuspendLayout();
            // 
            // ButDongY
            // 
            this.ButDongY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.Image = global::Accounting.Properties.Resources.apply_32;
            this.ButDongY.Appearance = appearance1;
            this.ButDongY.Location = new System.Drawing.Point(845, 510);
            this.ButDongY.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ButDongY.Name = "ButDongY";
            this.ButDongY.Size = new System.Drawing.Size(100, 37);
            this.ButDongY.TabIndex = 53;
            this.ButDongY.Text = "Đồng ý";
            this.ButDongY.Click += new System.EventHandler(this.ButDongY_Click);
            // 
            // ButHuyBo
            // 
            this.ButHuyBo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.Image = global::Accounting.Properties.Resources.cancel_16;
            this.ButHuyBo.Appearance = appearance2;
            this.ButHuyBo.Location = new System.Drawing.Point(953, 510);
            this.ButHuyBo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ButHuyBo.Name = "ButHuyBo";
            this.ButHuyBo.Size = new System.Drawing.Size(100, 37);
            this.ButHuyBo.TabIndex = 52;
            this.ButHuyBo.Text = "Hủy bỏ";
            this.ButHuyBo.Click += new System.EventHandler(this.ButHuyBo_Click);
            // 
            // ultraGridDsDoiChieu
            // 
            appearance3.BackColor = System.Drawing.SystemColors.Window;
            appearance3.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGridDsDoiChieu.DisplayLayout.Appearance = appearance3;
            this.ultraGridDsDoiChieu.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGridDsDoiChieu.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance4.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance4.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGridDsDoiChieu.DisplayLayout.GroupByBox.Appearance = appearance4;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGridDsDoiChieu.DisplayLayout.GroupByBox.BandLabelAppearance = appearance5;
            this.ultraGridDsDoiChieu.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance6.BackColor2 = System.Drawing.SystemColors.Control;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGridDsDoiChieu.DisplayLayout.GroupByBox.PromptAppearance = appearance6;
            this.ultraGridDsDoiChieu.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGridDsDoiChieu.DisplayLayout.MaxRowScrollRegions = 1;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGridDsDoiChieu.DisplayLayout.Override.ActiveCellAppearance = appearance7;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGridDsDoiChieu.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.ultraGridDsDoiChieu.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGridDsDoiChieu.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGridDsDoiChieu.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGridDsDoiChieu.DisplayLayout.Override.CellAppearance = appearance10;
            this.ultraGridDsDoiChieu.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGridDsDoiChieu.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGridDsDoiChieu.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance12.TextHAlignAsString = "Left";
            this.ultraGridDsDoiChieu.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.ultraGridDsDoiChieu.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGridDsDoiChieu.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.ultraGridDsDoiChieu.DisplayLayout.Override.RowAppearance = appearance13;
            this.ultraGridDsDoiChieu.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGridDsDoiChieu.DisplayLayout.Override.TemplateAddRowAppearance = appearance14;
            this.ultraGridDsDoiChieu.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGridDsDoiChieu.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGridDsDoiChieu.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.ultraGridDsDoiChieu.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGridDsDoiChieu.Location = new System.Drawing.Point(0, 0);
            this.ultraGridDsDoiChieu.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ultraGridDsDoiChieu.Name = "ultraGridDsDoiChieu";
            this.ultraGridDsDoiChieu.Size = new System.Drawing.Size(1051, 502);
            this.ultraGridDsDoiChieu.TabIndex = 54;
            this.ultraGridDsDoiChieu.Text = "ultraGrid1";
            // 
            // RemoveMathVouchers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1051, 538);
            this.Controls.Add(this.ultraGridDsDoiChieu);
            this.Controls.Add(this.ButDongY);
            this.Controls.Add(this.ButHuyBo);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1069, 585);
            this.MinimumSize = new System.Drawing.Size(1069, 585);
            this.Name = "RemoveMathVouchers";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bỏ đối chiếu ngân hàng";
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridDsDoiChieu)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton ButDongY;
        private Infragistics.Win.Misc.UltraButton ButHuyBo;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGridDsDoiChieu;
    }
}