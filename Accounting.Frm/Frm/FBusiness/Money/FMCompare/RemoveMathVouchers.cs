﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using CellClickAction = Infragistics.Win.UltraWinTree.CellClickAction;


namespace Accounting
{
    public partial class RemoveMathVouchers : CustormForm
    {
        private IAccountingObjectBankAccountService _IAccountingObjectBankAccountService;
        private IBankAccountDetailService _IBankAccountDetailService;
        List<BankAccountDetail> _listBankAccountDetail = new List<BankAccountDetail>();
        List<Compared> lst = new List<Compared>();
        public static bool IsClose = true;
        public static bool IsClose1 = false;
        public RemoveMathVouchers()
        {
            InitializeComponent();
            LoadDulieu();

        }
        public void LoadDulieu()
        {
            _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            _listBankAccountDetail = _IBankAccountDetailService.GetAll();
            lst= new List<Compared>();
            lst = _IAccountingObjectBankAccountService.GetCompared(_listBankAccountDetail.Select(c => c.ID).ToList());
            ultraGridDsDoiChieu.SetDataBinding(lst,"");
            Utils.ConfigGrid(ultraGridDsDoiChieu, ConstDatabase.Compared_TableName);
            //tự thay đổi kích thước cột
            ultraGridDsDoiChieu.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //Hàm xử lý cho edit cột muốn
            UltraGridBand band = ultraGridDsDoiChieu.DisplayLayout.Bands[0];
            band.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Default;
            foreach (UltraGridColumn column in band.Columns)
            {
                if (column.Key.Contains("Status"))
                    column.CellActivation = Activation.AllowEdit;
                else column.CellActivation = Activation.NoEdit;
            }
        }

        private void ButHuyBo_Click(object sender, EventArgs e)
        {
            IsClose = true;
            this.Close();
        }

        private void ButDongY_Click(object sender, EventArgs e)
        {
            if (lst.All(c=>c.Status == false))
            {
                MSG.Warning("Bạn chưa chọn tài khoản muốn bỏ đối chiếu");
                return;
                IsClose1 = false;

            }
            else if(MessageBox.Show("Bạn có chắc muốn hủy bỏ đối chiếu không", "Thực hiện hủy bỏ đối chiếu", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes && lst.Any(c => c.Status ==true))
            {
                
                _IAccountingObjectBankAccountService.RemoveList(lst.Where(c => c.Status==true).ToList());
                LoadDulieu();
                IsClose1 = true;
            }
           

        }
    }
}
