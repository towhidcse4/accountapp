﻿namespace Accounting
{
    partial class FMCompare
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.dtTuNgayDoiChieu = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.TxtTenNganHang = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ButLaySoLieu = new Infragistics.Win.Misc.UltraButton();
            this.txtSoDuTaiNganHang = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbTaiKhoanNganHang = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbLoaiTien = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.dtNgayDoiChieu = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblCreditCardNumber = new Infragistics.Win.Misc.UltraLabel();
            this.uGridDSchungtuchitien = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ButThucHienDoiChieu = new Infragistics.Win.Misc.UltraButton();
            this.ButHuyBo = new Infragistics.Win.Misc.UltraButton();
            this.ButBoDoiChieu = new Infragistics.Win.Misc.UltraButton();
            this.butXemChungTuDaDoiChieu = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.lblTongChungTuChi = new Infragistics.Win.Misc.UltraLabel();
            this.lblTongChungTuThu = new Infragistics.Win.Misc.UltraLabel();
            this.lblChungTuChi = new Infragistics.Win.Misc.UltraLabel();
            this.lblChungTuThu = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox8 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtChenhLech = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtSoDuNgHang = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtDaKhopDoiChieu = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox7 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtDaKhop = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.UgridDSchungtuthutien = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtTuNgayDoiChieu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTenNganHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTaiKhoanNganHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbLoaiTien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNgayDoiChieu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDSchungtuchitien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.ultraGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox8)).BeginInit();
            this.ultraGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).BeginInit();
            this.ultraGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UgridDSchungtuthutien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox1.Controls.Add(this.dtTuNgayDoiChieu);
            this.ultraGroupBox1.Controls.Add(this.TxtTenNganHang);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox1.Controls.Add(this.ButLaySoLieu);
            this.ultraGroupBox1.Controls.Add(this.txtSoDuTaiNganHang);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.cbbTaiKhoanNganHang);
            this.ultraGroupBox1.Controls.Add(this.cbbLoaiTien);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Controls.Add(this.dtNgayDoiChieu);
            this.ultraGroupBox1.Controls.Add(this.lblCreditCardNumber);
            this.ultraGroupBox1.Location = new System.Drawing.Point(6, 5);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(985, 78);
            this.ultraGroupBox1.TabIndex = 0;
            // 
            // ultraLabel3
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance1;
            this.ultraLabel3.AutoSize = true;
            this.ultraLabel3.Location = new System.Drawing.Point(259, 46);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(53, 14);
            this.ultraLabel3.TabIndex = 52;
            this.ultraLabel3.Text = "Đến ngày";
            // 
            // dtTuNgayDoiChieu
            // 
            appearance2.TextHAlignAsString = "Left";
            appearance2.TextVAlignAsString = "Middle";
            this.dtTuNgayDoiChieu.Appearance = appearance2;
            this.dtTuNgayDoiChieu.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtTuNgayDoiChieu.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtTuNgayDoiChieu.Location = new System.Drawing.Point(98, 42);
            this.dtTuNgayDoiChieu.MaskInput = "";
            this.dtTuNgayDoiChieu.Name = "dtTuNgayDoiChieu";
            this.dtTuNgayDoiChieu.Size = new System.Drawing.Size(133, 21);
            this.dtTuNgayDoiChieu.TabIndex = 51;
            this.dtTuNgayDoiChieu.Value = null;
            // 
            // TxtTenNganHang
            // 
            appearance3.ForeColorDisabled = System.Drawing.Color.Black;
            this.TxtTenNganHang.Appearance = appearance3;
            this.TxtTenNganHang.Enabled = false;
            this.TxtTenNganHang.Location = new System.Drawing.Point(318, 13);
            this.TxtTenNganHang.Name = "TxtTenNganHang";
            this.TxtTenNganHang.Size = new System.Drawing.Size(447, 21);
            this.TxtTenNganHang.TabIndex = 50;
            // 
            // ultraLabel4
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance4;
            this.ultraLabel4.AutoSize = true;
            this.ultraLabel4.Location = new System.Drawing.Point(786, 19);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(51, 14);
            this.ultraLabel4.TabIndex = 49;
            this.ultraLabel4.Text = "Loại Tiền";
            // 
            // ButLaySoLieu
            // 
            this.ButLaySoLieu.Location = new System.Drawing.Point(857, 40);
            this.ButLaySoLieu.Name = "ButLaySoLieu";
            this.ButLaySoLieu.Size = new System.Drawing.Size(110, 23);
            this.ButLaySoLieu.TabIndex = 48;
            this.ButLaySoLieu.Text = "Lấy số liệu";
            this.ButLaySoLieu.Click += new System.EventHandler(this.ButLaySoLieu_Click);
            // 
            // txtSoDuTaiNganHang
            // 
            appearance5.TextHAlignAsString = "Right";
            this.txtSoDuTaiNganHang.Appearance = appearance5;
            this.txtSoDuTaiNganHang.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtSoDuTaiNganHang.InputMask = "nn,nnn,nnn,nnn,nnn";
            this.txtSoDuTaiNganHang.Location = new System.Drawing.Point(613, 42);
            this.txtSoDuTaiNganHang.Name = "txtSoDuTaiNganHang";
            this.txtSoDuTaiNganHang.NullText = "0";
            this.txtSoDuTaiNganHang.PromptChar = ' ';
            this.txtSoDuTaiNganHang.Size = new System.Drawing.Size(231, 20);
            this.txtSoDuTaiNganHang.TabIndex = 47;
            this.txtSoDuTaiNganHang.ValueChanged += new System.EventHandler(this.TxtTenNganHang_ValueChanged);
            this.txtSoDuTaiNganHang.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSoDuTaiNganHang_KeyPress);
            this.txtSoDuTaiNganHang.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSoDuTaiNganHang_KeyUp);
            // 
            // ultraLabel2
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance6;
            this.ultraLabel2.AutoSize = true;
            this.ultraLabel2.Location = new System.Drawing.Point(496, 45);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(105, 14);
            this.ultraLabel2.TabIndex = 46;
            this.ultraLabel2.Text = "Số dư tại ngân hàng";
            // 
            // cbbTaiKhoanNganHang
            // 
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbTaiKhoanNganHang.DisplayLayout.Appearance = appearance7;
            this.cbbTaiKhoanNganHang.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbTaiKhoanNganHang.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance8.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance8.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbTaiKhoanNganHang.DisplayLayout.GroupByBox.Appearance = appearance8;
            appearance9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbTaiKhoanNganHang.DisplayLayout.GroupByBox.BandLabelAppearance = appearance9;
            this.cbbTaiKhoanNganHang.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance10.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance10.BackColor2 = System.Drawing.SystemColors.Control;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbTaiKhoanNganHang.DisplayLayout.GroupByBox.PromptAppearance = appearance10;
            this.cbbTaiKhoanNganHang.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbTaiKhoanNganHang.DisplayLayout.MaxRowScrollRegions = 1;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbTaiKhoanNganHang.DisplayLayout.Override.ActiveCellAppearance = appearance11;
            appearance12.BackColor = System.Drawing.SystemColors.Highlight;
            appearance12.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbTaiKhoanNganHang.DisplayLayout.Override.ActiveRowAppearance = appearance12;
            this.cbbTaiKhoanNganHang.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbTaiKhoanNganHang.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            this.cbbTaiKhoanNganHang.DisplayLayout.Override.CardAreaAppearance = appearance13;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            appearance14.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbTaiKhoanNganHang.DisplayLayout.Override.CellAppearance = appearance14;
            this.cbbTaiKhoanNganHang.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbTaiKhoanNganHang.DisplayLayout.Override.CellPadding = 0;
            appearance15.BackColor = System.Drawing.SystemColors.Control;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbTaiKhoanNganHang.DisplayLayout.Override.GroupByRowAppearance = appearance15;
            appearance16.TextHAlignAsString = "Left";
            this.cbbTaiKhoanNganHang.DisplayLayout.Override.HeaderAppearance = appearance16;
            this.cbbTaiKhoanNganHang.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbTaiKhoanNganHang.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            this.cbbTaiKhoanNganHang.DisplayLayout.Override.RowAppearance = appearance17;
            this.cbbTaiKhoanNganHang.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbTaiKhoanNganHang.DisplayLayout.Override.TemplateAddRowAppearance = appearance18;
            this.cbbTaiKhoanNganHang.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbTaiKhoanNganHang.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbTaiKhoanNganHang.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbTaiKhoanNganHang.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbTaiKhoanNganHang.Location = new System.Drawing.Point(98, 12);
            this.cbbTaiKhoanNganHang.Name = "cbbTaiKhoanNganHang";
            this.cbbTaiKhoanNganHang.Size = new System.Drawing.Size(214, 22);
            this.cbbTaiKhoanNganHang.TabIndex = 45;
            this.cbbTaiKhoanNganHang.ValueChanged += new System.EventHandler(this.cbbTaiKhoanNganHang_ValueChanged);
            // 
            // cbbLoaiTien
            // 
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbLoaiTien.DisplayLayout.Appearance = appearance19;
            this.cbbLoaiTien.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbLoaiTien.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance20.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance20.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance20.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbLoaiTien.DisplayLayout.GroupByBox.Appearance = appearance20;
            appearance21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbLoaiTien.DisplayLayout.GroupByBox.BandLabelAppearance = appearance21;
            this.cbbLoaiTien.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance22.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance22.BackColor2 = System.Drawing.SystemColors.Control;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbLoaiTien.DisplayLayout.GroupByBox.PromptAppearance = appearance22;
            this.cbbLoaiTien.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbLoaiTien.DisplayLayout.MaxRowScrollRegions = 1;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbLoaiTien.DisplayLayout.Override.ActiveCellAppearance = appearance23;
            appearance24.BackColor = System.Drawing.SystemColors.Highlight;
            appearance24.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbLoaiTien.DisplayLayout.Override.ActiveRowAppearance = appearance24;
            this.cbbLoaiTien.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbLoaiTien.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            this.cbbLoaiTien.DisplayLayout.Override.CardAreaAppearance = appearance25;
            appearance26.BorderColor = System.Drawing.Color.Silver;
            appearance26.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbLoaiTien.DisplayLayout.Override.CellAppearance = appearance26;
            this.cbbLoaiTien.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbLoaiTien.DisplayLayout.Override.CellPadding = 0;
            appearance27.BackColor = System.Drawing.SystemColors.Control;
            appearance27.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance27.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbLoaiTien.DisplayLayout.Override.GroupByRowAppearance = appearance27;
            appearance28.TextHAlignAsString = "Left";
            this.cbbLoaiTien.DisplayLayout.Override.HeaderAppearance = appearance28;
            this.cbbLoaiTien.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbLoaiTien.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            this.cbbLoaiTien.DisplayLayout.Override.RowAppearance = appearance29;
            this.cbbLoaiTien.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance30.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbLoaiTien.DisplayLayout.Override.TemplateAddRowAppearance = appearance30;
            this.cbbLoaiTien.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbLoaiTien.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbLoaiTien.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbLoaiTien.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbLoaiTien.Location = new System.Drawing.Point(857, 13);
            this.cbbLoaiTien.Name = "cbbLoaiTien";
            this.cbbLoaiTien.Size = new System.Drawing.Size(110, 22);
            this.cbbLoaiTien.TabIndex = 44;
            this.cbbLoaiTien.ValueChanged += new System.EventHandler(this.cbbLoaiTien_ValueChanged);
            // 
            // ultraLabel1
            // 
            appearance31.BackColor = System.Drawing.Color.Transparent;
            appearance31.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance31;
            this.ultraLabel1.AutoSize = true;
            this.ultraLabel1.Location = new System.Drawing.Point(11, 49);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(45, 14);
            this.ultraLabel1.TabIndex = 40;
            this.ultraLabel1.Text = "Từ ngày";
            // 
            // dtNgayDoiChieu
            // 
            appearance32.TextHAlignAsString = "Left";
            appearance32.TextVAlignAsString = "Middle";
            this.dtNgayDoiChieu.Appearance = appearance32;
            this.dtNgayDoiChieu.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtNgayDoiChieu.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtNgayDoiChieu.Location = new System.Drawing.Point(319, 42);
            this.dtNgayDoiChieu.MaskInput = "";
            this.dtNgayDoiChieu.Name = "dtNgayDoiChieu";
            this.dtNgayDoiChieu.Size = new System.Drawing.Size(133, 21);
            this.dtNgayDoiChieu.TabIndex = 39;
            this.dtNgayDoiChieu.Value = null;
            this.dtNgayDoiChieu.Leave += new System.EventHandler(this.dtNgayDoiChieu_Leave);
            // 
            // lblCreditCardNumber
            // 
            appearance33.BackColor = System.Drawing.Color.Transparent;
            appearance33.TextVAlignAsString = "Middle";
            this.lblCreditCardNumber.Appearance = appearance33;
            this.lblCreditCardNumber.Location = new System.Drawing.Point(11, 15);
            this.lblCreditCardNumber.Name = "lblCreditCardNumber";
            this.lblCreditCardNumber.Size = new System.Drawing.Size(81, 19);
            this.lblCreditCardNumber.TabIndex = 36;
            this.lblCreditCardNumber.Text = "TK Ngân hàng";
            // 
            // uGridDSchungtuchitien
            // 
            this.uGridDSchungtuchitien.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridDSchungtuchitien.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGridDSchungtuchitien.Location = new System.Drawing.Point(6, 23);
            this.uGridDSchungtuchitien.Name = "uGridDSchungtuchitien";
            this.uGridDSchungtuchitien.Size = new System.Drawing.Size(973, 140);
            this.uGridDSchungtuchitien.TabIndex = 43;
            this.uGridDSchungtuchitien.Text = "ultraGrid1";
            this.uGridDSchungtuchitien.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uGridDSchungtuchitien.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridSpendingVoucher_CellChange);
            this.uGridDSchungtuchitien.AfterHeaderCheckStateChanged += new Infragistics.Win.UltraWinGrid.AfterHeaderCheckStateChangedEventHandler(this.uGridDSchungtuchitien_AfterHeaderCheckStateChanged);
            // 
            // ButThucHienDoiChieu
            // 
            this.ButThucHienDoiChieu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance34.Image = global::Accounting.Properties.Resources.apply_32;
            this.ButThucHienDoiChieu.Appearance = appearance34;
            this.ButThucHienDoiChieu.Location = new System.Drawing.Point(752, 547);
            this.ButThucHienDoiChieu.Name = "ButThucHienDoiChieu";
            this.ButThucHienDoiChieu.Size = new System.Drawing.Size(143, 30);
            this.ButThucHienDoiChieu.TabIndex = 49;
            this.ButThucHienDoiChieu.Text = "Thực hiện đối chiếu";
            this.ButThucHienDoiChieu.Click += new System.EventHandler(this.ButThucHienDoiChieu_Click);
            // 
            // ButHuyBo
            // 
            this.ButHuyBo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance35.Image = global::Accounting.Properties.Resources.cancel_16;
            this.ButHuyBo.Appearance = appearance35;
            this.ButHuyBo.Location = new System.Drawing.Point(901, 547);
            this.ButHuyBo.Name = "ButHuyBo";
            this.ButHuyBo.Size = new System.Drawing.Size(85, 30);
            this.ButHuyBo.TabIndex = 50;
            this.ButHuyBo.Text = "Hủy bỏ";
            this.ButHuyBo.Click += new System.EventHandler(this.ButHuyBo_Click);
            // 
            // ButBoDoiChieu
            // 
            appearance36.Image = global::Accounting.Properties.Resources.edit_not_validated_icon;
            this.ButBoDoiChieu.Appearance = appearance36;
            this.ButBoDoiChieu.Location = new System.Drawing.Point(197, 547);
            this.ButBoDoiChieu.Name = "ButBoDoiChieu";
            this.ButBoDoiChieu.Size = new System.Drawing.Size(105, 30);
            this.ButBoDoiChieu.TabIndex = 52;
            this.ButBoDoiChieu.Text = "Bỏ đối chiếu";
            this.ButBoDoiChieu.Click += new System.EventHandler(this.ButBoDoiChieu_Click);
            // 
            // butXemChungTuDaDoiChieu
            // 
            appearance37.Image = global::Accounting.Properties.Resources.ubtnViewList;
            this.butXemChungTuDaDoiChieu.Appearance = appearance37;
            this.butXemChungTuDaDoiChieu.Location = new System.Drawing.Point(10, 547);
            this.butXemChungTuDaDoiChieu.Name = "butXemChungTuDaDoiChieu";
            this.butXemChungTuDaDoiChieu.Size = new System.Drawing.Size(181, 30);
            this.butXemChungTuDaDoiChieu.TabIndex = 53;
            this.butXemChungTuDaDoiChieu.Text = "Xem chứng từ đã đối chiếu";
            this.butXemChungTuDaDoiChieu.Click += new System.EventHandler(this.butXemChungTuDaDoiChieu_Click);
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rounded;
            this.ultraGroupBox5.Controls.Add(this.lblTongChungTuChi);
            this.ultraGroupBox5.Controls.Add(this.lblTongChungTuThu);
            this.ultraGroupBox5.Controls.Add(this.lblChungTuChi);
            this.ultraGroupBox5.Controls.Add(this.lblChungTuThu);
            this.ultraGroupBox5.HeaderBorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded4;
            this.ultraGroupBox5.Location = new System.Drawing.Point(4, 434);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(298, 107);
            this.ultraGroupBox5.TabIndex = 0;
            this.ultraGroupBox5.Text = "Chứng từ đã đối chiếu / chưa đối chiếu";
            // 
            // lblTongChungTuChi
            // 
            this.lblTongChungTuChi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance38.BackColor = System.Drawing.Color.Transparent;
            appearance38.TextHAlignAsString = "Right";
            appearance38.TextVAlignAsString = "Middle";
            this.lblTongChungTuChi.Appearance = appearance38;
            this.lblTongChungTuChi.Location = new System.Drawing.Point(130, 60);
            this.lblTongChungTuChi.Name = "lblTongChungTuChi";
            this.lblTongChungTuChi.Size = new System.Drawing.Size(152, 22);
            this.lblTongChungTuChi.TabIndex = 3;
            this.lblTongChungTuChi.Text = "0";
            // 
            // lblTongChungTuThu
            // 
            this.lblTongChungTuThu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance39.BackColor = System.Drawing.Color.Transparent;
            appearance39.TextHAlignAsString = "Right";
            appearance39.TextVAlignAsString = "Middle";
            this.lblTongChungTuThu.Appearance = appearance39;
            this.lblTongChungTuThu.Location = new System.Drawing.Point(130, 31);
            this.lblTongChungTuThu.Name = "lblTongChungTuThu";
            this.lblTongChungTuThu.Size = new System.Drawing.Size(152, 23);
            this.lblTongChungTuThu.TabIndex = 2;
            this.lblTongChungTuThu.Text = "0";
            // 
            // lblChungTuChi
            // 
            appearance40.BackColor = System.Drawing.Color.Transparent;
            appearance40.TextVAlignAsString = "Middle";
            this.lblChungTuChi.Appearance = appearance40;
            this.lblChungTuChi.Location = new System.Drawing.Point(11, 60);
            this.lblChungTuChi.Name = "lblChungTuChi";
            this.lblChungTuChi.Size = new System.Drawing.Size(69, 23);
            this.lblChungTuChi.TabIndex = 1;
            this.lblChungTuChi.Text = "0/0";
            // 
            // lblChungTuThu
            // 
            appearance41.BackColor = System.Drawing.Color.Transparent;
            appearance41.TextVAlignAsString = "Middle";
            this.lblChungTuThu.Appearance = appearance41;
            this.lblChungTuThu.Location = new System.Drawing.Point(11, 31);
            this.lblChungTuThu.Name = "lblChungTuThu";
            this.lblChungTuThu.Size = new System.Drawing.Size(69, 23);
            this.lblChungTuThu.TabIndex = 0;
            this.lblChungTuThu.Text = "0/0";
            // 
            // ultraGroupBox8
            // 
            this.ultraGroupBox8.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rounded;
            this.ultraGroupBox8.Controls.Add(this.txtChenhLech);
            this.ultraGroupBox8.Controls.Add(this.txtSoDuNgHang);
            this.ultraGroupBox8.Controls.Add(this.txtDaKhopDoiChieu);
            this.ultraGroupBox8.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox8.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox8.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox8.HeaderBorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded4;
            this.ultraGroupBox8.Location = new System.Drawing.Point(629, 434);
            this.ultraGroupBox8.Name = "ultraGroupBox8";
            this.ultraGroupBox8.Size = new System.Drawing.Size(362, 107);
            this.ultraGroupBox8.TabIndex = 3;
            this.ultraGroupBox8.Text = "Số dư cuối kỳ";
            // 
            // txtChenhLech
            // 
            appearance42.ForeColorDisabled = System.Drawing.Color.Black;
            appearance42.TextHAlignAsString = "Right";
            this.txtChenhLech.Appearance = appearance42;
            this.txtChenhLech.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtChenhLech.Enabled = false;
            this.txtChenhLech.InputMask = "-nn,nnn,nnn,nnn,nnn";
            this.txtChenhLech.Location = new System.Drawing.Point(85, 75);
            this.txtChenhLech.Name = "txtChenhLech";
            this.txtChenhLech.NullText = "0";
            this.txtChenhLech.PromptChar = ' ';
            this.txtChenhLech.Size = new System.Drawing.Size(271, 20);
            this.txtChenhLech.TabIndex = 51;
            // 
            // txtSoDuNgHang
            // 
            appearance43.ForeColorDisabled = System.Drawing.Color.Black;
            appearance43.TextHAlignAsString = "Right";
            this.txtSoDuNgHang.Appearance = appearance43;
            this.txtSoDuNgHang.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtSoDuNgHang.Enabled = false;
            this.txtSoDuNgHang.InputMask = "-nn,nnn,nnn,nnn,nnn";
            this.txtSoDuNgHang.Location = new System.Drawing.Point(85, 50);
            this.txtSoDuNgHang.Name = "txtSoDuNgHang";
            this.txtSoDuNgHang.NullText = "0";
            this.txtSoDuNgHang.PromptChar = ' ';
            this.txtSoDuNgHang.Size = new System.Drawing.Size(271, 20);
            this.txtSoDuNgHang.TabIndex = 50;
            // 
            // txtDaKhopDoiChieu
            // 
            appearance44.ForeColorDisabled = System.Drawing.Color.Black;
            appearance44.TextHAlignAsString = "Right";
            this.txtDaKhopDoiChieu.Appearance = appearance44;
            this.txtDaKhopDoiChieu.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtDaKhopDoiChieu.Enabled = false;
            this.txtDaKhopDoiChieu.InputMask = "-nn,nnn,nnn,nnn,nnn";
            this.txtDaKhopDoiChieu.Location = new System.Drawing.Point(85, 24);
            this.txtDaKhopDoiChieu.Name = "txtDaKhopDoiChieu";
            this.txtDaKhopDoiChieu.NullText = "0";
            this.txtDaKhopDoiChieu.PromptChar = ' ';
            this.txtDaKhopDoiChieu.Size = new System.Drawing.Size(271, 20);
            this.txtDaKhopDoiChieu.TabIndex = 49;
            // 
            // ultraLabel9
            // 
            appearance45.BackColor = System.Drawing.Color.Transparent;
            appearance45.TextHAlignAsString = "Left";
            appearance45.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance45;
            this.ultraLabel9.Location = new System.Drawing.Point(6, 71);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(89, 23);
            this.ultraLabel9.TabIndex = 8;
            this.ultraLabel9.Text = "Chênh lệch";
            // 
            // ultraLabel8
            // 
            appearance46.BackColor = System.Drawing.Color.Transparent;
            appearance46.TextHAlignAsString = "Left";
            appearance46.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance46;
            this.ultraLabel8.Location = new System.Drawing.Point(6, 46);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(89, 23);
            this.ultraLabel8.TabIndex = 7;
            this.ultraLabel8.Text = "Tiền ngân hàng";
            // 
            // ultraLabel7
            // 
            appearance47.BackColor = System.Drawing.Color.Transparent;
            appearance47.TextHAlignAsString = "Left";
            appearance47.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance47;
            this.ultraLabel7.Location = new System.Drawing.Point(6, 23);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(89, 23);
            this.ultraLabel7.TabIndex = 6;
            this.ultraLabel7.Text = "Đã khớp ĐC";
            // 
            // ultraGroupBox7
            // 
            this.ultraGroupBox7.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rounded;
            this.ultraGroupBox7.Controls.Add(this.txtDaKhop);
            this.ultraGroupBox7.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox7.HeaderBorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded4;
            this.ultraGroupBox7.Location = new System.Drawing.Point(308, 434);
            this.ultraGroupBox7.Name = "ultraGroupBox7";
            this.ultraGroupBox7.Size = new System.Drawing.Size(313, 107);
            this.ultraGroupBox7.TabIndex = 2;
            this.ultraGroupBox7.Text = "Số dư đầu kỳ";
            // 
            // txtDaKhop
            // 
            appearance48.ForeColorDisabled = System.Drawing.Color.Black;
            appearance48.TextHAlignAsString = "Right";
            this.txtDaKhop.Appearance = appearance48;
            this.txtDaKhop.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtDaKhop.Enabled = false;
            this.txtDaKhop.InputMask = "nn,nnn,nnn,nnn,nnn";
            this.txtDaKhop.Location = new System.Drawing.Point(69, 24);
            this.txtDaKhop.MaxValue = new decimal(new int[] {
            276447231,
            23283,
            0,
            0});
            this.txtDaKhop.Name = "txtDaKhop";
            this.txtDaKhop.NullText = "0";
            this.txtDaKhop.PromptChar = ' ';
            this.txtDaKhop.Size = new System.Drawing.Size(238, 20);
            this.txtDaKhop.TabIndex = 48;
            // 
            // ultraLabel6
            // 
            this.ultraLabel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance49.BackColor = System.Drawing.Color.Transparent;
            appearance49.TextHAlignAsString = "Center";
            appearance49.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance49;
            this.ultraLabel6.Location = new System.Drawing.Point(3, 23);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(76, 23);
            this.ultraLabel6.TabIndex = 5;
            this.ultraLabel6.Text = "Đã khớp";
            // 
            // UgridDSchungtuthutien
            // 
            this.UgridDSchungtuthutien.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.UgridDSchungtuthutien.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.UgridDSchungtuthutien.Location = new System.Drawing.Point(6, 19);
            this.UgridDSchungtuthutien.Name = "UgridDSchungtuthutien";
            this.UgridDSchungtuthutien.Size = new System.Drawing.Size(973, 140);
            this.UgridDSchungtuthutien.TabIndex = 43;
            this.UgridDSchungtuthutien.Text = "ultraGrid1";
            this.UgridDSchungtuthutien.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.UgridDSchungtuthutien.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridSpendingVoucher_CellChange);
            this.UgridDSchungtuthutien.AfterHeaderCheckStateChanged += new Infragistics.Win.UltraWinGrid.AfterHeaderCheckStateChangedEventHandler(this.UgridDSchungtuthutien_AfterHeaderCheckStateChanged);
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.uGridDSchungtuchitien);
            this.ultraGroupBox4.Location = new System.Drawing.Point(6, 258);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(985, 170);
            this.ultraGroupBox4.TabIndex = 3;
            this.ultraGroupBox4.Text = "Danh sách chứng từ chi tiền";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.UgridDSchungtuthutien);
            this.ultraGroupBox2.Location = new System.Drawing.Point(6, 89);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(985, 163);
            this.ultraGroupBox2.TabIndex = 1;
            this.ultraGroupBox2.Text = "Danh sách chứng từ thu tiền";
            // 
            // FMCompare
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 580);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.ultraGroupBox4);
            this.Controls.Add(this.butXemChungTuDaDoiChieu);
            this.Controls.Add(this.ButBoDoiChieu);
            this.Controls.Add(this.ultraGroupBox8);
            this.Controls.Add(this.ultraGroupBox7);
            this.Controls.Add(this.ButHuyBo);
            this.Controls.Add(this.ButThucHienDoiChieu);
            this.Controls.Add(this.ultraGroupBox5);
            this.Controls.Add(this.ultraGroupBox1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.Name = "FMCompare";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đối chiếu với ngân hàng";
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtTuNgayDoiChieu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTenNganHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTaiKhoanNganHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbLoaiTien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNgayDoiChieu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDSchungtuchitien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.ultraGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox8)).EndInit();
            this.ultraGroupBox8.ResumeLayout(false);
            this.ultraGroupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).EndInit();
            this.ultraGroupBox7.ResumeLayout(false);
            this.ultraGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UgridDSchungtuthutien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblCreditCardNumber;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtNgayDoiChieu;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbTaiKhoanNganHang;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtSoDuTaiNganHang;
        private Infragistics.Win.Misc.UltraButton ButLaySoLieu;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDSchungtuchitien;
        private Infragistics.Win.Misc.UltraButton ButThucHienDoiChieu;
        private Infragistics.Win.Misc.UltraButton ButHuyBo;
        private Infragistics.Win.Misc.UltraButton ButBoDoiChieu;
        private Infragistics.Win.Misc.UltraButton butXemChungTuDaDoiChieu;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.Misc.UltraLabel lblTongChungTuChi;
        private Infragistics.Win.Misc.UltraLabel lblTongChungTuThu;
        private Infragistics.Win.Misc.UltraLabel lblChungTuChi;
        private Infragistics.Win.Misc.UltraLabel lblChungTuThu;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox8;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtChenhLech;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtSoDuNgHang;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtDaKhopDoiChieu;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox7;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtDaKhop;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinGrid.UltraGrid UgridDSchungtuthutien;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor TxtTenNganHang;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtTuNgayDoiChieu;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbLoaiTien;
    }
}