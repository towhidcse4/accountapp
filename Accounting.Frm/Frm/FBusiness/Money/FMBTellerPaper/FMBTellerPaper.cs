﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Frm.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting.Frm
{
    public partial class FMBTellerPaper : CustormForm //UserControl
    {
        #region khai báo
        private readonly IMBTellerPaperService IMBTellerPaperService;
        private readonly IGeneralLedgerService IGeneralLedgerService;
        private readonly IBankAccountDetailService IBankAccountDetailService;
        private readonly IAccountingObjectService IAccountingObjectService;
        private readonly ITypeService ITypeService;
        private readonly ITemplateService ITemplateService;
        List<MBTellerPaper> _dsMBTellerPaper = new List<MBTellerPaper>();

        // Chứa danh sách các template dựa theo TypeID (KEY type of string = TypeID@Guid, VALUES = Template của TypeID và TemplateID ứng với chứng từ đó)
        // - khi một chứng từ được load, nó kiểm tra Template nó cần có trong dsTemplate chưa, chưa có thì load trong csdl có rồi thì lấy luôn trong dsTemplate để tránh truy vấn nhiều
        // - dsTemplate sẽ được cập nhật lại khi có sự thay đổi giao diện
        //Note: nó có tác dụng tăng tốc độ của chương trình, tuy nhiên chú ý nó là hàm Static nên tồn tại ngay cả khi đóng form, chỉ mất đi khi đóng toàn bộ chương trình do đó cần chú ý set nó bằng null hoặc bằng new Dictionary<string, Template>() hoặc clear item trong dic cho giải phóng bộ nhớ
        public static Dictionary<string, Template> DsTemplate = new Dictionary<string, Template>();
        #endregion

        #region khởi tạo
        public FMBTellerPaper()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            IMBTellerPaperService = IoC.Resolve<IMBTellerPaperService>();
            IGeneralLedgerService = IoC.Resolve<IGeneralLedgerService>();
            IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            ITypeService = IoC.Resolve<ITypeService>();
            ITemplateService = IoC.Resolve<ITemplateService>();
            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu

            LoadDuLieu();

            //chọn tự select dòng đầu tiên
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            #endregion
        }

        private void LoadDuLieu(bool configGrid = true)
        {
            #region Lấy dữ liệu từ CSDL
            _dsMBTellerPaper = IMBTellerPaperService.GetAll();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = _dsMBTellerPaper.ToArray();

            if (configGrid) ConfigGrid(uGrid);
            #endregion
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void TsmAddClick(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void TsmEditClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void TsmDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void TmsReLoadClick(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void BtnAddClick(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void BtnEditClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void BtnDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void UGridMouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void UGridDoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunction();
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        void AddFunction()
        {
            MBTellerPaper temp = new MBTellerPaper();
            temp.TypeID = 120;
            new FMBTellerPaperDetail(temp, _dsMBTellerPaper, ConstFrm.optStatusForm.Add).ShowDialog();
            if (!FMBTellerPaperDetail.IsClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                MBTellerPaper temp = uGrid.Selected.Rows[0].ListObject as MBTellerPaper;
                new FMBTellerPaperDetail(temp, _dsMBTellerPaper, ConstFrm.optStatusForm.View).ShowDialog();
                if (!FMBTellerPaperDetail.IsClose) LoadDuLieu();
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                MBTellerPaper temp = uGrid.Selected.Rows[0].ListObject as MBTellerPaper;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, temp.No)) == DialogResult.Yes)
                {
                    IMBTellerPaperService.BeginTran();
                    //List<GeneralLedger> listGenTemp = IGeneralLedgerService.Query.Where(p => p.ReferenceID == temp.ID).ToList();
                    List<GeneralLedger> listGenTemp = IGeneralLedgerService.GetListByReferenceID(temp.ID);
                    foreach (var generalLedger in listGenTemp) IGeneralLedgerService.Delete(generalLedger);
                    IMBTellerPaperService.Delete(temp);
                    IMBTellerPaperService.CommitTran();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion

        #region Utils

        static void ConfigGrid(UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, ConstDatabase.MBTellerPaper_TableName);

            utralGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Format = Constants.DdMMyyyy;
            utralGrid.DisplayLayout.Bands[0].Columns["Date"].Format = Constants.DdMMyyyy;
        }
        #endregion

        private void UGridAfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGrid.Selected.Rows.Count < 1) return;
            object listObject = uGrid.Selected.Rows[0].ListObject;
            if (listObject == null) return;
            MBTellerPaper objMBTellerPaper = listObject as MBTellerPaper;
            if (objMBTellerPaper == null) return;
            //Tab thông tin chung
            lblPostedDate.Text = objMBTellerPaper.PostedDate.ToString(Constants.DdMMyyyy); // ngày hoạch toán
            lblNo.Text = objMBTellerPaper.No; // số chứng từ
            lblDate.Text = objMBTellerPaper.Date.ToString(Constants.DdMMyyyy); // ngày chứng từ
            BankAccountDetail bankAccountDetail =
                Utils.ListBankAccountDetail.FirstOrDefault(p => p.ID.Equals(objMBTellerPaper.BankAccountDetailID));
            lblAccountBank.Text = bankAccountDetail != null ? bankAccountDetail.BankAccount : string.Empty;    
            lblReason.Text = objMBTellerPaper.Reason; // Nội dung thanh toán
            lblTotalAmount.Text = string.Format(objMBTellerPaper.TotalAmount == 0 ? "0" : "{0:0,0}", objMBTellerPaper.TotalAmount);
            if (objMBTellerPaper.TypeID !=null && objMBTellerPaper.TypeID!=0)
            {
                var model = ITypeService.Getbykey(objMBTellerPaper.TypeID);
                if (model != null)
                    lblTypeName.Text = model.TypeName;
            }// Tổng tiền
            lblBranchName.Text = "Chi nhánh"; // chi nhánh
            lblAccountingObjectName.Text = objMBTellerPaper.AccountingObjectName; // tên đối tượng nhận tiền
            lblAccountingObjectAddress.Text = objMBTellerPaper.AccountingObjectAddress; // địa chỉ đối tượng nhận tiền
            AccountingObjectBankAccount accountingObjectBankAccount =
                Utils.ListAccountingObjectBank.FirstOrDefault(
                    p => p.ID.Equals(objMBTellerPaper.AccountingObjectBankAccount));
            lblAccountingBankAccount.Text = accountingObjectBankAccount != null
                                                ? accountingObjectBankAccount.BankAccount
                                                : string.Empty;
            uGridPosted.DataSource = objMBTellerPaper.MBTellerPaperDetails;
            #region Config Grid
            //hiển thị 1 band
            uGridPosted.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            //tắt lọc cột
            uGridPosted.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGridPosted.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGridPosted.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGridPosted.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;

            //Hiện những dòng trống?
            uGridPosted.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridPosted.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            //Fix Header
            uGridPosted.DisplayLayout.UseFixedHeaders = true;

            UltraGridBand band = uGridPosted.DisplayLayout.Bands[0];
            band.Summaries.Clear();

            string keyofdsTemplate = string.Format("{0}@{1}", objMBTellerPaper.TypeID, objMBTellerPaper.TemplateID);
            //Template mauGiaoDien = objMBTellerPaper.TemplateID.HasValue ? ITemplateService.Getbykey(objMBTellerPaper.TemplateID.Value) : getMauChuan(objMBTellerPaper.TypeID);
            Template mauGiaoDien = objMBTellerPaper.TemplateID.HasValue
                                       ? ITemplateService.Getbykey(objMBTellerPaper.TemplateID.Value)
                                       : ITemplateService.getTemplateStandard(objMBTellerPaper.TypeID);
            Utils.ConfigGrid(uGridPosted, ConstDatabase.MBTellerPaperDetail_TableName, mauGiaoDien == null ? new List<TemplateColumn>() : mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 0).TemplateColumns, 0);
            //Thêm tổng số ở cột "Số tiền"
            //SummarySettings summary = band.Summaries.Add("SumAmountOriginal", SummaryType.Sum, band.Columns["AmountOriginal"]);
            //summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            //summary.DisplayFormat = "{0:N0}";
            //summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            #endregion
        }


        //Template getMauChuan(int typeId)
        //{
        //    return (from t in IoC.Resolve<ITemplateService>().Query
        //            join d in IoC.Resolve<ITemplateDetailService>().Query on t.ID equals d.TemplateID
        //            where t.IsDefault == false && t.TypeID == typeId
        //            select t).FirstOrDefault();
        //}
    }
}
