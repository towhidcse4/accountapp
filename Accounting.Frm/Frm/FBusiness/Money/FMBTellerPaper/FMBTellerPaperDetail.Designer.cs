﻿namespace Accounting
{
    partial class FMBTellerPaperDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton("btnNewAccountingObject");
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton("btnNewBankAccount");
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            this.grAccounting = new Infragistics.Win.Misc.UltraGroupBox();
            this.lblIdentificationNo = new Infragistics.Win.Misc.UltraLabel();
            this.lblReceiver = new Infragistics.Win.Misc.UltraLabel();
            this.txtReceiver = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtIssueBy = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblIssueBy = new Infragistics.Win.Misc.UltraLabel();
            this.txtIdentificationNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.dteIssueDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblIssuDate = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjtBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectBankAccount = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjBank = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblAccountingObjectAddress = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObject = new Infragistics.Win.Misc.UltraLabel();
            this.grpTop = new Infragistics.Win.Misc.UltraGroupBox();
            this.optAccountingObjectType = new Accounting.UltraOptionSet_Ex();
            this.palTopVouchers = new Infragistics.Win.Misc.UltraPanel();
            this.palTop = new System.Windows.Forms.Panel();
            this.palInfo = new Infragistics.Win.Misc.UltraPanel();
            this.grAccount = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbBankAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblReason = new Infragistics.Win.Misc.UltraLabel();
            this.txtBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblBankAccount = new Infragistics.Win.Misc.UltraLabel();
            this.palGrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.uGridControl = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.palBot = new System.Windows.Forms.Panel();
            this.palFill = new System.Windows.Forms.Panel();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            ((System.ComponentModel.ISupportInitialize)(this.grAccounting)).BeginInit();
            this.grAccounting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificationNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteIssueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjtBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjBank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTop)).BeginInit();
            this.grpTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optAccountingObjectType)).BeginInit();
            this.palTopVouchers.SuspendLayout();
            this.palTop.SuspendLayout();
            this.palInfo.ClientArea.SuspendLayout();
            this.palInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grAccount)).BeginInit();
            this.grAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).BeginInit();
            this.palGrid.ClientArea.SuspendLayout();
            this.palGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).BeginInit();
            this.palBot.SuspendLayout();
            this.palFill.SuspendLayout();
            this.SuspendLayout();
            // 
            // grAccounting
            // 
            this.grAccounting.Controls.Add(this.lblIdentificationNo);
            this.grAccounting.Controls.Add(this.lblReceiver);
            this.grAccounting.Controls.Add(this.txtReceiver);
            this.grAccounting.Controls.Add(this.txtIssueBy);
            this.grAccounting.Controls.Add(this.lblIssueBy);
            this.grAccounting.Controls.Add(this.txtIdentificationNo);
            this.grAccounting.Controls.Add(this.dteIssueDate);
            this.grAccounting.Controls.Add(this.lblIssuDate);
            this.grAccounting.Controls.Add(this.txtAccountingObjtBankName);
            this.grAccounting.Controls.Add(this.lblAccountingObjectBankAccount);
            this.grAccounting.Controls.Add(this.cbbAccountingObjectID);
            this.grAccounting.Controls.Add(this.txtAccountingObjectAddress);
            this.grAccounting.Controls.Add(this.txtAccountingObjectName);
            this.grAccounting.Controls.Add(this.cbbAccountingObjBank);
            this.grAccounting.Controls.Add(this.lblAccountingObjectAddress);
            this.grAccounting.Controls.Add(this.lblAccountingObject);
            this.grAccounting.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance10.FontData.BoldAsString = "True";
            appearance10.FontData.SizeInPoints = 10F;
            this.grAccounting.HeaderAppearance = appearance10;
            this.grAccounting.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.grAccounting.Location = new System.Drawing.Point(0, 80);
            this.grAccounting.Name = "grAccounting";
            this.grAccounting.Size = new System.Drawing.Size(935, 105);
            this.grAccounting.TabIndex = 26;
            this.grAccounting.Text = "Đối tượng nhận tiền";
            this.grAccounting.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // lblIdentificationNo
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.lblIdentificationNo.Appearance = appearance1;
            this.lblIdentificationNo.Location = new System.Drawing.Point(10, 75);
            this.lblIdentificationNo.Name = "lblIdentificationNo";
            this.lblIdentificationNo.Size = new System.Drawing.Size(68, 21);
            this.lblIdentificationNo.TabIndex = 36;
            this.lblIdentificationNo.Text = "Số CMT";
            this.lblIdentificationNo.Visible = false;
            // 
            // lblReceiver
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.lblReceiver.Appearance = appearance2;
            this.lblReceiver.Location = new System.Drawing.Point(9, 50);
            this.lblReceiver.Name = "lblReceiver";
            this.lblReceiver.Size = new System.Drawing.Size(84, 22);
            this.lblReceiver.TabIndex = 42;
            this.lblReceiver.Text = "Người lĩnh tiền";
            this.lblReceiver.Visible = false;
            // 
            // txtReceiver
            // 
            this.txtReceiver.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReceiver.AutoSize = false;
            this.txtReceiver.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReceiver.Location = new System.Drawing.Point(100, 50);
            this.txtReceiver.Name = "txtReceiver";
            this.txtReceiver.Size = new System.Drawing.Size(823, 22);
            this.txtReceiver.TabIndex = 41;
            this.txtReceiver.Visible = false;
            // 
            // txtIssueBy
            // 
            this.txtIssueBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIssueBy.AutoSize = false;
            this.txtIssueBy.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtIssueBy.Location = new System.Drawing.Point(493, 75);
            this.txtIssueBy.Name = "txtIssueBy";
            this.txtIssueBy.Size = new System.Drawing.Size(430, 22);
            this.txtIssueBy.TabIndex = 39;
            this.txtIssueBy.Visible = false;
            // 
            // lblIssueBy
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.lblIssueBy.Appearance = appearance3;
            this.lblIssueBy.Location = new System.Drawing.Point(446, 77);
            this.lblIssueBy.Name = "lblIssueBy";
            this.lblIssueBy.Size = new System.Drawing.Size(69, 19);
            this.lblIssueBy.TabIndex = 38;
            this.lblIssueBy.Text = "Nơi cấp";
            this.lblIssueBy.Visible = false;
            // 
            // txtIdentificationNo
            // 
            this.txtIdentificationNo.AutoSize = false;
            this.txtIdentificationNo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtIdentificationNo.Location = new System.Drawing.Point(100, 75);
            this.txtIdentificationNo.Name = "txtIdentificationNo";
            this.txtIdentificationNo.Size = new System.Drawing.Size(174, 22);
            this.txtIdentificationNo.TabIndex = 35;
            this.txtIdentificationNo.Visible = false;
            // 
            // dteIssueDate
            // 
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.dteIssueDate.Appearance = appearance4;
            this.dteIssueDate.AutoSize = false;
            this.dteIssueDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteIssueDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.dteIssueDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteIssueDate.Location = new System.Drawing.Point(345, 75);
            this.dteIssueDate.MaskInput = "";
            this.dteIssueDate.Name = "dteIssueDate";
            this.dteIssueDate.Size = new System.Drawing.Size(98, 22);
            this.dteIssueDate.TabIndex = 34;
            this.dteIssueDate.Value = null;
            this.dteIssueDate.Visible = false;
            // 
            // lblIssuDate
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.lblIssuDate.Appearance = appearance5;
            this.lblIssuDate.Location = new System.Drawing.Point(285, 77);
            this.lblIssuDate.Name = "lblIssuDate";
            this.lblIssuDate.Size = new System.Drawing.Size(58, 19);
            this.lblIssuDate.TabIndex = 37;
            this.lblIssuDate.Text = "Ngày cấp";
            this.lblIssuDate.Visible = false;
            this.lblIssuDate.Click += new System.EventHandler(this.lblIssuDate_Click);
            // 
            // txtAccountingObjtBankName
            // 
            this.txtAccountingObjtBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjtBankName.AutoSize = false;
            this.txtAccountingObjtBankName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjtBankName.Location = new System.Drawing.Point(280, 75);
            this.txtAccountingObjtBankName.Name = "txtAccountingObjtBankName";
            this.txtAccountingObjtBankName.Size = new System.Drawing.Size(643, 22);
            this.txtAccountingObjtBankName.TabIndex = 33;
            this.txtAccountingObjtBankName.Visible = false;
            // 
            // lblAccountingObjectBankAccount
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.lblAccountingObjectBankAccount.Appearance = appearance6;
            this.lblAccountingObjectBankAccount.Location = new System.Drawing.Point(10, 75);
            this.lblAccountingObjectBankAccount.Name = "lblAccountingObjectBankAccount";
            this.lblAccountingObjectBankAccount.Size = new System.Drawing.Size(68, 21);
            this.lblAccountingObjectBankAccount.TabIndex = 33;
            this.lblAccountingObjectBankAccount.Text = "Tài khoản";
            this.lblAccountingObjectBankAccount.Visible = false;
            // 
            // cbbAccountingObjectID
            // 
            this.cbbAccountingObjectID.AutoSize = false;
            editorButton1.AccessibleName = "btnNewAccountingObject";
            appearance7.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton1.Appearance = appearance7;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            editorButton1.Key = "btnNewAccountingObject";
            this.cbbAccountingObjectID.ButtonsRight.Add(editorButton1);
            this.cbbAccountingObjectID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjectID.Location = new System.Drawing.Point(100, 25);
            this.cbbAccountingObjectID.Name = "cbbAccountingObjectID";
            this.cbbAccountingObjectID.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID.Size = new System.Drawing.Size(174, 22);
            this.cbbAccountingObjectID.TabIndex = 31;
            this.cbbAccountingObjectID.TextChanged += new System.EventHandler(this.cbbAccountingObjectID_TextChanged);
            // 
            // txtAccountingObjectAddress
            // 
            this.txtAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddress.AutoSize = false;
            this.txtAccountingObjectAddress.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectAddress.Location = new System.Drawing.Point(100, 50);
            this.txtAccountingObjectAddress.Name = "txtAccountingObjectAddress";
            this.txtAccountingObjectAddress.Size = new System.Drawing.Size(823, 22);
            this.txtAccountingObjectAddress.TabIndex = 25;
            this.txtAccountingObjectAddress.Visible = false;
            // 
            // txtAccountingObjectName
            // 
            this.txtAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectName.AutoSize = false;
            this.txtAccountingObjectName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectName.Location = new System.Drawing.Point(280, 25);
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(643, 22);
            this.txtAccountingObjectName.TabIndex = 23;
            // 
            // cbbAccountingObjBank
            // 
            this.cbbAccountingObjBank.AutoSize = false;
            this.cbbAccountingObjBank.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjBank.Location = new System.Drawing.Point(100, 75);
            this.cbbAccountingObjBank.Name = "cbbAccountingObjBank";
            this.cbbAccountingObjBank.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjBank.Size = new System.Drawing.Size(174, 22);
            this.cbbAccountingObjBank.TabIndex = 40;
            this.cbbAccountingObjBank.Visible = false;
            this.cbbAccountingObjBank.TextChanged += new System.EventHandler(this.cbbAccountingObjBank_TextChanged);
            this.cbbAccountingObjBank.Leave += new System.EventHandler(this.cbbAccountingObjBank_Leave);
            // 
            // lblAccountingObjectAddress
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectAddress.Appearance = appearance8;
            this.lblAccountingObjectAddress.Location = new System.Drawing.Point(8, 55);
            this.lblAccountingObjectAddress.Name = "lblAccountingObjectAddress";
            this.lblAccountingObjectAddress.Size = new System.Drawing.Size(84, 19);
            this.lblAccountingObjectAddress.TabIndex = 22;
            this.lblAccountingObjectAddress.Text = "Địa chỉ";
            this.lblAccountingObjectAddress.Visible = false;
            // 
            // lblAccountingObject
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.lblAccountingObject.Appearance = appearance9;
            this.lblAccountingObject.Location = new System.Drawing.Point(9, 25);
            this.lblAccountingObject.Name = "lblAccountingObject";
            this.lblAccountingObject.Size = new System.Drawing.Size(68, 22);
            this.lblAccountingObject.TabIndex = 0;
            this.lblAccountingObject.Text = "Đối tượng";
            // 
            // grpTop
            // 
            this.grpTop.Controls.Add(this.optAccountingObjectType);
            this.grpTop.Controls.Add(this.palTopVouchers);
            this.grpTop.Dock = System.Windows.Forms.DockStyle.Top;
            appearance13.FontData.BoldAsString = "True";
            appearance13.FontData.SizeInPoints = 13F;
            this.grpTop.HeaderAppearance = appearance13;
            this.grpTop.Location = new System.Drawing.Point(0, 0);
            this.grpTop.Name = "grpTop";
            this.grpTop.Size = new System.Drawing.Size(935, 79);
            this.grpTop.TabIndex = 31;
            this.grpTop.Text = "Séc/Ủy nhiệm chi";
            this.grpTop.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // optAccountingObjectType
            // 
            this.optAccountingObjectType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextHAlignAsString = "Center";
            appearance11.TextVAlignAsString = "Middle";
            this.optAccountingObjectType.Appearance = appearance11;
            this.optAccountingObjectType.BackColor = System.Drawing.Color.Transparent;
            this.optAccountingObjectType.BackColorInternal = System.Drawing.Color.Transparent;
            this.optAccountingObjectType.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.optAccountingObjectType.CheckedIndex = 0;
            valueListItem3.DataValue = "Default Item";
            valueListItem3.DisplayText = "Khách hàng";
            valueListItem5.DataValue = "ValueListItem1";
            valueListItem5.DisplayText = "Nhà cung cấp";
            valueListItem6.DataValue = "ValueListItem2";
            valueListItem6.DisplayText = "Nhân viên";
            valueListItem1.DataValue = "ValueListItem3";
            valueListItem1.DisplayText = "Khác";
            this.optAccountingObjectType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem5,
            valueListItem6,
            valueListItem1});
            this.optAccountingObjectType.Location = new System.Drawing.Point(626, 37);
            this.optAccountingObjectType.Name = "optAccountingObjectType";
            this.optAccountingObjectType.ReadOnly = false;
            this.optAccountingObjectType.Size = new System.Drawing.Size(297, 22);
            this.optAccountingObjectType.TabIndex = 37;
            this.optAccountingObjectType.Text = "Khách hàng";
            // 
            // palTopVouchers
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            this.palTopVouchers.Appearance = appearance12;
            this.palTopVouchers.AutoSize = true;
            this.palTopVouchers.Location = new System.Drawing.Point(10, 26);
            this.palTopVouchers.Name = "palTopVouchers";
            this.palTopVouchers.Size = new System.Drawing.Size(453, 48);
            this.palTopVouchers.TabIndex = 36;
            // 
            // palTop
            // 
            this.palTop.Controls.Add(this.palInfo);
            this.palTop.Controls.Add(this.grpTop);
            this.palTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTop.Location = new System.Drawing.Point(0, 0);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(935, 264);
            this.palTop.TabIndex = 27;
            // 
            // palInfo
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            this.palInfo.Appearance = appearance14;
            // 
            // palInfo.ClientArea
            // 
            this.palInfo.ClientArea.Controls.Add(this.grAccounting);
            this.palInfo.ClientArea.Controls.Add(this.grAccount);
            this.palInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palInfo.Location = new System.Drawing.Point(0, 79);
            this.palInfo.Name = "palInfo";
            this.palInfo.Size = new System.Drawing.Size(935, 185);
            this.palInfo.TabIndex = 38;
            // 
            // grAccount
            // 
            this.grAccount.Controls.Add(this.cbbBankAccount);
            this.grAccount.Controls.Add(this.txtReason);
            this.grAccount.Controls.Add(this.lblReason);
            this.grAccount.Controls.Add(this.txtBankName);
            this.grAccount.Controls.Add(this.lblBankAccount);
            this.grAccount.Dock = System.Windows.Forms.DockStyle.Top;
            appearance18.FontData.BoldAsString = "True";
            appearance18.FontData.SizeInPoints = 10F;
            this.grAccount.HeaderAppearance = appearance18;
            this.grAccount.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.grAccount.Location = new System.Drawing.Point(0, 0);
            this.grAccount.Name = "grAccount";
            this.grAccount.Size = new System.Drawing.Size(935, 80);
            this.grAccount.TabIndex = 27;
            this.grAccount.Text = "Đơn vị trả tiền";
            this.grAccount.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbBankAccount
            // 
            this.cbbBankAccount.AutoSize = false;
            editorButton2.AccessibleName = "btnNewBankAccount";
            appearance15.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton2.Appearance = appearance15;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            editorButton2.Key = "btnNewBankAccount";
            this.cbbBankAccount.ButtonsRight.Add(editorButton2);
            this.cbbBankAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbBankAccount.Location = new System.Drawing.Point(100, 25);
            this.cbbBankAccount.Name = "cbbBankAccount";
            this.cbbBankAccount.NullText = "<chọn dữ liệu>";
            this.cbbBankAccount.Size = new System.Drawing.Size(174, 22);
            this.cbbBankAccount.TabIndex = 31;
            this.cbbBankAccount.TextChanged += new System.EventHandler(this.cbbBankAccount_TextChanged);
            this.cbbBankAccount.Validated += new System.EventHandler(this.cbbBankAccount_Validated);
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason.AutoSize = false;
            this.txtReason.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReason.Location = new System.Drawing.Point(100, 50);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(823, 22);
            this.txtReason.TabIndex = 25;
            // 
            // lblReason
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.TextVAlignAsString = "Middle";
            this.lblReason.Appearance = appearance16;
            this.lblReason.Location = new System.Drawing.Point(10, 50);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(67, 22);
            this.lblReason.TabIndex = 24;
            this.lblReason.Text = "Nội dung TT";
            // 
            // txtBankName
            // 
            this.txtBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBankName.AutoSize = false;
            this.txtBankName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtBankName.Location = new System.Drawing.Point(280, 25);
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Size = new System.Drawing.Size(643, 22);
            this.txtBankName.TabIndex = 23;
            // 
            // lblBankAccount
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextVAlignAsString = "Middle";
            this.lblBankAccount.Appearance = appearance17;
            this.lblBankAccount.Location = new System.Drawing.Point(10, 25);
            this.lblBankAccount.Name = "lblBankAccount";
            this.lblBankAccount.Size = new System.Drawing.Size(68, 22);
            this.lblBankAccount.TabIndex = 0;
            this.lblBankAccount.Text = "Tài khoản";
            // 
            // palGrid
            // 
            this.palGrid.AutoSize = true;
            // 
            // palGrid.ClientArea
            // 
            this.palGrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.palGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palGrid.Location = new System.Drawing.Point(0, 0);
            this.palGrid.Name = "palGrid";
            this.palGrid.Size = new System.Drawing.Size(935, 410);
            this.palGrid.TabIndex = 33;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance19.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance19;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(837, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 7;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // uGridControl
            // 
            this.uGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridControl.Location = new System.Drawing.Point(579, 0);
            this.uGridControl.Name = "uGridControl";
            this.uGridControl.Size = new System.Drawing.Size(356, 54);
            this.uGridControl.TabIndex = 1;
            this.uGridControl.Text = "uGridControl";
            // 
            // palBot
            // 
            this.palBot.Controls.Add(this.uGridControl);
            this.palBot.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBot.Location = new System.Drawing.Point(0, 410);
            this.palBot.Name = "palBot";
            this.palBot.Size = new System.Drawing.Size(935, 57);
            this.palBot.TabIndex = 27;
            // 
            // palFill
            // 
            this.palFill.Controls.Add(this.palGrid);
            this.palFill.Controls.Add(this.palBot);
            this.palFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palFill.Location = new System.Drawing.Point(0, 274);
            this.palFill.Name = "palFill";
            this.palFill.Size = new System.Drawing.Size(935, 467);
            this.palFill.TabIndex = 0;
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 264);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 264;
            this.ultraSplitter1.Size = new System.Drawing.Size(935, 10);
            this.ultraSplitter1.TabIndex = 28;
            this.ultraSplitter1.Click += new System.EventHandler(this.ultraSplitter1_Click);
            // 
            // FMBTellerPaperDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(935, 741);
            this.Controls.Add(this.palFill);
            this.Controls.Add(this.ultraSplitter1);
            this.Controls.Add(this.palTop);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FMBTellerPaperDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FMBTellerPaperDetail_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.grAccounting)).EndInit();
            this.grAccounting.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificationNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteIssueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjtBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjBank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTop)).EndInit();
            this.grpTop.ResumeLayout(false);
            this.grpTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optAccountingObjectType)).EndInit();
            this.palTopVouchers.ResumeLayout(false);
            this.palTopVouchers.PerformLayout();
            this.palTop.ResumeLayout(false);
            this.palInfo.ClientArea.ResumeLayout(false);
            this.palInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grAccount)).EndInit();
            this.grAccount.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).EndInit();
            this.palGrid.ClientArea.ResumeLayout(false);
            this.palGrid.ResumeLayout(false);
            this.palGrid.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).EndInit();
            this.palBot.ResumeLayout(false);
            this.palFill.ResumeLayout(false);
            this.palFill.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox grAccounting;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObject;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectAddress;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraGroupBox grpTop;
        private System.Windows.Forms.Panel palTop;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridTax;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPosted;
        private Infragistics.Win.Misc.UltraGroupBox grAccount;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.Misc.UltraLabel lblReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankName;
        private Infragistics.Win.Misc.UltraLabel lblBankAccount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjtBankName;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectBankAccount;
        private Infragistics.Win.Misc.UltraLabel lblIdentificationNo;
        private Infragistics.Win.Misc.UltraLabel lblIssueBy;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteIssueDate;
        private Infragistics.Win.Misc.UltraLabel lblIssuDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIssueBy;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjBank;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReceiver;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIdentificationNo;
        private Infragistics.Win.Misc.UltraLabel lblReceiver;
        private Infragistics.Win.Misc.UltraPanel palTopVouchers;
        private UltraOptionSet_Ex optAccountingObjectType;
        private System.Windows.Forms.Panel palFill;
        private Infragistics.Win.Misc.UltraPanel palGrid;
        private System.Windows.Forms.Panel palBot;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridControl;
        private Infragistics.Win.Misc.UltraPanel palInfo;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
    }
}
