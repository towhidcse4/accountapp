﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Accounting;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;

namespace Accounting
{
    public partial class FMBTellerPaperDetail : FrmMBTP
    {
        #region Khai báo
        private readonly BindingList<BankAccountDetail> _lstBankAccountDetails = Utils.ListBankAccountDetail;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private IRefVoucherRSInwardOutwardService _refVoucherRSInwardOutwardService { get { return IoC.Resolve<IRefVoucherRSInwardOutwardService>(); } }
        UltraGrid ugrid2 = null;
        bool IsAdd = false;
        #endregion
        #region khởi tạo


        public FMBTellerPaperDetail(MBTellerPaper temp, List<MBTellerPaper> dsMBTellerPaper, int statusForm)
        {//Add -> thêm, Sửa -> sửa, Xem -> Sửa ở trạng thái Enable = False

            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();


            #endregion


            #region Thiết lập ban đầu cho Form

            _statusForm = statusForm;
            IsAdd = temp.MBTellerPaperDetails.Count > 0;
            _select = statusForm == ConstFrm.optStatusForm.Add ? (new MBTellerPaper { TypeID = 120 }) : temp;
            if (IsAdd) _select = temp;
            _typeID = _select.TypeID;
            _listSelects.AddRange(dsMBTellerPaper);
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, _listSelects, statusForm);
            //this.WindowState = FormWindowState.Maximized;

            #endregion
        }

        #endregion

        #region override
        public override void InitializeGUI(MBTellerPaper input)
        {

            Template mauGiaoDien = Utils.GetMauGiaoDien(input.TypeID, input.TemplateID, Utils.ListTemplate);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;

            input.TemplateID = mauGiaoDien.ID;
            List<int> lstTypeParrent = new List<int>() { 120, 130, 140 };
            List<int> lst2CellsInfo = new List<int>() { 122, 123, 124, 125, 126, 127, 128, 129 };
            List<int> lstSingleCellInfo = new List<int>() { 121 };

            var firstOrDefault = Utils.ListType.FirstOrDefault(p => p.ID.Equals(_select.TypeID));
            if (firstOrDefault != null)
                grpTop.Text = firstOrDefault.TypeName.ToUpper();

            #region Body
            if (input.TypeID.Equals(127) || input.TypeID.Equals(131) || input.TypeID.Equals(141))
            {
                this.ConfigTopVouchersNo<MBTellerPaper>(palTopVouchers, "No", "PostedDate", "Date", null, true, true, lstTypeParrent.Any(p => p.Equals(input.TypeID) && _statusForm == ConstFrm.optStatusForm.Add), "TypeID");
                optAccountingObjectType.Visible = false;
                this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectID", cbbAccountingObjBank, "AccountingObjectBankAccount");
                BindingList<PPInvoiceDetail> bdlPPInvoiceDetail = new BindingList<PPInvoiceDetail>();
                BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();

                _selectJoin = new PPInvoice();
                _selectJoin = IPPInvoiceService.Getbykey(input.ID);
                if (_selectJoin != null)
                {
                    bdlPPInvoiceDetail = new BindingList<PPInvoiceDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                          ? new List<PPInvoiceDetail>()
                                                                          : (((PPInvoice)_selectJoin).PPInvoiceDetails ?? new List<PPInvoiceDetail>()));
                    bdlRefVoucher = new BindingList<RefVoucher>(((PPInvoice)_selectJoin).RefVouchers);

                    _listObjectInput = new BindingList<System.Collections.IList> { bdlPPInvoiceDetail };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                    this.ConfigGridByTemplete_General<MBTellerPaper>(palGrid, mauGiaoDien);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
                    this.ConfigGridByManyTemplete<MBTellerPaper>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
                    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                }
                btnOriginalVoucher.Visible = false;
            }
            else if (input.TypeID.Equals(129) || input.TypeID.Equals(132) || input.TypeID.Equals(142))
            {
                this.ConfigTopVouchersNo<MBTellerPaper>(palTopVouchers, "No", "PostedDate", "Date", null, true, true, lstTypeParrent.Any(p => p.Equals(input.TypeID) && _statusForm == ConstFrm.optStatusForm.Add), "TypeID");
                optAccountingObjectType.Visible = false;
                this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectID", cbbAccountingObjBank, "AccountingObjectBankAccount");
                BindingList<FAIncrementDetail> bdlFAIncrementDetail = new BindingList<FAIncrementDetail>();
                BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();

                _selectJoin = new FAIncrement();
                _selectJoin = IFAIncrementService.Getbykey(input.ID);
                if (_selectJoin != null)
                {
                    bdlFAIncrementDetail = new BindingList<FAIncrementDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                          ? new List<FAIncrementDetail>()
                                                                          : (((FAIncrement)_selectJoin).FAIncrementDetails ?? new List<FAIncrementDetail>()));
                    bdlRefVoucher = new BindingList<RefVoucher>(((FAIncrement)_selectJoin).RefVouchers);

                    _listObjectInput = new BindingList<System.Collections.IList> { bdlFAIncrementDetail };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                    this.ConfigGridByTemplete_General<MBTellerPaper>(palGrid, mauGiaoDien);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
                    this.ConfigGridByManyTemplete<MBTellerPaper>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
                    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                }
                btnOriginalVoucher.Visible = false;
            }
            else if (input.TypeID.Equals(903) || input.TypeID.Equals(904) || input.TypeID.Equals(905))
            {
                this.ConfigTopVouchersNo<MBTellerPaper>(palTopVouchers, "No", "PostedDate", "Date", null, true, true, lstTypeParrent.Any(p => p.Equals(input.TypeID) && _statusForm == ConstFrm.optStatusForm.Add), "TypeID");
                optAccountingObjectType.Visible = false;
                this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectID", cbbAccountingObjBank, "AccountingObjectBankAccount");
                BindingList<TIIncrementDetail> bdlTIIncrementDetail = new BindingList<TIIncrementDetail>();
                BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();

                _selectJoin = new TIIncrement();
                _selectJoin = ITIIncrementService.Getbykey(input.ID);
                if (_selectJoin != null)
                {
                    bdlTIIncrementDetail = new BindingList<TIIncrementDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                          ? new List<TIIncrementDetail>()
                                                                          : (((TIIncrement)_selectJoin).TIIncrementDetails ?? new List<TIIncrementDetail>()));
                    bdlRefVoucher = new BindingList<RefVoucher>(((TIIncrement)_selectJoin).RefVouchers);

                    _listObjectInput = new BindingList<System.Collections.IList> { bdlTIIncrementDetail };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                    this.ConfigGridByTemplete_General<MBTellerPaper>(palGrid, mauGiaoDien);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, true, true, false };
                    this.ConfigGridByManyTemplete<MBTellerPaper>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    ugrid2 = Controls.Find("uGrid4", true).FirstOrDefault() as UltraGrid;
                    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                    var ultraTabControl = (UltraTabControl)palGrid.Controls.Find("ultraTabControl", true).FirstOrDefault();
                    ultraTabControl.Tabs[3].Visible = false;
                    ultraTabControl.Tabs[4].Text = "&4. Chứng từ tham chiếu";
                }
                btnOriginalVoucher.Visible = false;
            }
            else if (input.TypeID.Equals(133) || input.TypeID.Equals(143) || input.TypeID.Equals(126))
            {
                this.ConfigTopVouchersNo<MBTellerPaper>(palTopVouchers, "No", "PostedDate", "Date", null, true, true, lstTypeParrent.Any(p => p.Equals(input.TypeID) && _statusForm == ConstFrm.optStatusForm.Add), "TypeID");
                optAccountingObjectType.Visible = false;
                this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectID", cbbAccountingObjBank, "AccountingObjectBankAccount");
                BindingList<PPServiceDetail> bdlPPServiceDetail = new BindingList<PPServiceDetail>();
                BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();

                _selectJoin = new PPService();
                _selectJoin = IPPServiceService.Getbykey(input.ID);
                if (_selectJoin != null)
                {
                    bdlPPServiceDetail = new BindingList<PPServiceDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                          ? new List<PPServiceDetail>()
                                                                          : (((PPService)_selectJoin).PPServiceDetails ?? new List<PPServiceDetail>()));
                    bdlRefVoucher = new BindingList<RefVoucher>(((PPService)_selectJoin).RefVouchers);

                    _listObjectInput = new BindingList<System.Collections.IList> { bdlPPServiceDetail };
                    _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                    this.ConfigGridByTemplete_General<MBTellerPaper>(palGrid, mauGiaoDien);
                    List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
                    List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
                    this.ConfigGridByManyTemplete<MBTellerPaper>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                    ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
                    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                }
                btnOriginalVoucher.Visible = false;
            }
            else if (input.TypeID.Equals(128) || input.TypeID.Equals(144) || input.TypeID.Equals(134))
            {
                this.ConfigTopVouchersNo<MBTellerPaper>(palTopVouchers, "No", "PostedDate", "Date", null, true, true, lstTypeParrent.Any(p => p.Equals(input.TypeID) && _statusForm == ConstFrm.optStatusForm.Add), "TypeID", cbbType_RowSelected);
                _select.AccountingObjectType = _statusForm.Equals(ConstFrm.optStatusForm.Add)
                                              ? 0
                                              : _select.AccountingObjectType;
                optAccountingObjectType.Visible = true;
                this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectID", cbbAccountingObjBank, "AccountingObjectBankAccount");

                DataBinding(new List<Control> { optAccountingObjectType }, "AccountingObjectType", "4");
                if (_statusForm.Equals(ConstFrm.optStatusForm.Add))
                {
                    optAccountingObjectType.CheckedIndex = 0;
                }
                GetDataComboByOption(optAccountingObjectType);

                BindingList<MBTellerPaperDetail> bdlMBTellerPaperDetail = new BindingList<MBTellerPaperDetail>();
                BindingList<MBTellerPaperDetailVendor> bdlMBTellerPaperDetailVendor = new BindingList<MBTellerPaperDetailVendor>();
                if (_statusForm == ConstFrm.optStatusForm.Add || IsAdd)
                {
                    bdlMBTellerPaperDetail = new BindingList<MBTellerPaperDetail>(input.MBTellerPaperDetails);
                    bdlMBTellerPaperDetailVendor = new BindingList<MBTellerPaperDetailVendor>(input.MBTellerPaperDetailVendors);
                }
                _listObjectInput = new BindingList<System.Collections.IList> { bdlMBTellerPaperDetailVendor, bdlMBTellerPaperDetail };
                this.ConfigGridByTemplete_General<MBTellerPaper>(palGrid, mauGiaoDien);
                this.ConfigGridByTemplete<MBTellerPaper>(input.TypeID, mauGiaoDien, true, _listObjectInput);
                btnOriginalVoucher.Visible = false;
            }
            else if (input.TypeID.Equals(125))
            {
                this.ConfigTopVouchersNo<MBTellerPaper>(palTopVouchers, "No", "PostedDate", "Date");
                this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectID", cbbAccountingObjBank, "AccountingObjectBankAccount");
                optAccountingObjectType.Visible = true;
                DataBinding(new List<Control> { optAccountingObjectType }, "AccountingObjectType", "4");
                if (_statusForm.Equals(ConstFrm.optStatusForm.Add))
                {
                    optAccountingObjectType.CheckedIndex = 0;
                }
                GetDataComboByOption(optAccountingObjectType);
                List<MBTellerPaperDetailInsurance> alst = input.MBTellerPaperDetailInsurances.ToList();
                BindingList<MBTellerPaperDetailInsurance> bdlMBTellerPaperDetail = new BindingList<MBTellerPaperDetailInsurance>(_statusForm == ConstFrm.optStatusForm.Add
                                                                           ? new List<MBTellerPaperDetailInsurance>()
                                                                           : alst);
                _listObjectInput = new BindingList<System.Collections.IList> { bdlMBTellerPaperDetail };
                this.ConfigGridByTemplete_General<MBTellerPaper>(palGrid, mauGiaoDien);
                this.ConfigGridByTemplete<MBTellerPaper>(input.TypeID, mauGiaoDien, true, _listObjectInput);
                btnOriginalVoucher.Visible = false;
            }
            else if (input.TypeID.Equals(121))
            {
                this.ConfigTopVouchersNo<MBTellerPaper>(palTopVouchers, "No", "PostedDate", "Date", null, true, true, lstTypeParrent.Any(p => p.Equals(input.TypeID) && _statusForm == ConstFrm.optStatusForm.Add), "TypeID", cbbType_RowSelected);
                _select.AccountingObjectType = _statusForm.Equals(ConstFrm.optStatusForm.Add)
                                              ? 0
                                              : _select.AccountingObjectType;
                this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectID", cbbAccountingObjBank, "AccountingObjectBankAccount");
                optAccountingObjectType.Visible = true;
                DataBinding(new List<Control> { optAccountingObjectType }, "AccountingObjectType", "4");
                if (_statusForm.Equals(ConstFrm.optStatusForm.Add))
                {
                    optAccountingObjectType.CheckedIndex = 0;
                }
                GetDataComboByOption(optAccountingObjectType);

                BindingList<MBTellerPaperDetail> bdlMBTellerPaperDetail = new BindingList<MBTellerPaperDetail>();
                BindingList<MBTellerPaperDetailSalary> MBTellerPaperDetailSalarys = new BindingList<MBTellerPaperDetailSalary>();
                bdlMBTellerPaperDetail = new BindingList<MBTellerPaperDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                           ? new List<MBTellerPaperDetail>()
                                                                           : (input.MBTellerPaperDetails ?? new List<MBTellerPaperDetail>()));
                MBTellerPaperDetailSalarys = new BindingList<MBTellerPaperDetailSalary>(_statusForm == ConstFrm.optStatusForm.Add
                                                                           ? new List<MBTellerPaperDetailSalary>()
                                                                           : (input.MBTellerPaperDetailSalarys ?? new List<MBTellerPaperDetailSalary>()));
                _listObjectInput = new BindingList<System.Collections.IList> { bdlMBTellerPaperDetail, MBTellerPaperDetailSalarys };
                this.ConfigGridByTemplete_General<MBTellerPaper>(palGrid, mauGiaoDien);
                this.ConfigGridByTemplete<MBTellerPaper>(input.TypeID, mauGiaoDien, true, _listObjectInput);
                btnOriginalVoucher.Visible = false;
            }
            else
            {
                this.ConfigTopVouchersNo<MBTellerPaper>(palTopVouchers, "No", "PostedDate", "Date", null, true, true, lstTypeParrent.Any(p => p.Equals(input.TypeID) && _statusForm == ConstFrm.optStatusForm.Add), "TypeID", cbbType_RowSelected);
                _select.AccountingObjectType = _statusForm.Equals(ConstFrm.optStatusForm.Add)
                                              ? 0
                                              : _select.AccountingObjectType;
                this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectID", cbbAccountingObjBank, "AccountingObjectBankAccount");
                optAccountingObjectType.Visible = true;
                DataBinding(new List<Control> { optAccountingObjectType }, "AccountingObjectType", "4");
                if (_statusForm.Equals(ConstFrm.optStatusForm.Add))
                {
                    optAccountingObjectType.CheckedIndex = 0;
                }
                GetDataComboByOption(optAccountingObjectType);

                BindingList<MBTellerPaperDetail> bdlMBTellerPaperDetail = new BindingList<MBTellerPaperDetail>();
                BindingList<MBTellerPaperDetailTax> bdlMBTellerPaperDetailTax = new BindingList<MBTellerPaperDetailTax>();
                BindingList<RefVoucherRSInwardOutward> bdlRefVoucher = new BindingList<RefVoucherRSInwardOutward>();
                if (_statusForm != ConstFrm.optStatusForm.Add)
                    bdlRefVoucher = new BindingList<RefVoucherRSInwardOutward>(input.RefVoucherRSInwardOutwards);
                bdlMBTellerPaperDetail = new BindingList<MBTellerPaperDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                           ? new List<MBTellerPaperDetail>()
                                                                           : (input.MBTellerPaperDetails ?? new List<MBTellerPaperDetail>()));
                bdlMBTellerPaperDetailTax = new BindingList<MBTellerPaperDetailTax>(_statusForm == ConstFrm.optStatusForm.Add
                                                                           ? new List<MBTellerPaperDetailTax>()
                                                                           : (input.MBTellerPaperDetailTaxs ?? new List<MBTellerPaperDetailTax>()));
                _listObjectInput = new BindingList<System.Collections.IList> { bdlMBTellerPaperDetail, bdlMBTellerPaperDetailTax };
                _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                this.ConfigGridByTemplete_General<MBTellerPaper>(palGrid, mauGiaoDien);
                List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInputPost };
                List<Boolean> manyStandard = new List<Boolean>() { true, true, false };
                this.ConfigGridByManyTemplete<MBTellerPaper>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                if (!input.TypeID.Equals(122) && !input.TypeID.Equals(123) && !input.TypeID.Equals(124))
                {
                    ugrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
                    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                }

                if (input.TypeID.Equals(122) || input.TypeID.Equals(123) || input.TypeID.Equals(124))
                {
                    //var ultraTabControl = (UltraTabControl)palGrid.Controls.Find("ultraTabControl", true).FirstOrDefault();
                    //ultraTabControl.Tabs[2].Visible = false;
                    btnOriginalVoucher.Visible = false;
                }
                else
                {
                    btnOriginalVoucher.Visible = true;//add by cuongpv
                }
            }
            if (ugrid2 != null)
                ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
            }
            #endregion
            #region Footer
            _select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmount;
            _select.CurrencyID = (_statusForm == ConstFrm.optStatusForm.Add && input.TypeID != 128 && input.TypeID != 144 && input.TypeID != 134) ? "VND" : input.CurrencyID;

            List<MBTellerPaper> inputCurrency = new List<MBTellerPaper> { _select };
            this.ConfigGridCurrencyByTemplate<MBTellerPaper>(_select.TypeID, new BindingList<System.Collections.IList> { inputCurrency },
                                              uGridControl, mauGiaoDien);
            this.ConfigCombo(_lstBankAccountDetails, cbbBankAccount, "BankAccount", "ID", input, "BankAccountDetailID");
            //if(input.TypeID)
            Utils.ConfigColumnByCurrency(this, Utils.ListCurrency.FirstOrDefault(x => x.ID == input.CurrencyID));
            if ((input.TypeID.Equals(128) || input.TypeID.Equals(144) || input.TypeID.Equals(134)) && _select.CurrencyID != "VND")
            {
                uGridControl.DisplayLayout.Bands[0].Columns["TotalAllOriginal"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGridControl.DisplayLayout.Bands[0].Columns["TotalAll"].Hidden = false;
                uGridControl.DisplayLayout.Bands[0].Columns["ExchangeRate"].Hidden = false;
            }
            #endregion
            #region Account - Trả tiền
            DataBinding(new List<Control> { txtBankName, txtReason }, "BankName,Reason");
            #endregion
            #region Accounting - Nhận tiền
            DataBinding(new List<Control> { txtAccountingObjectName, txtAccountingObjectAddress, txtAccountingObjtBankName, txtReceiver, txtIdentificationNo, txtIssueBy }, "AccountingObjectName,AccountingObjectAddress,AccountingObjectBankName,Receiver,IdentificationNo,IssueBy");
            DataBinding(new List<Control> { dteIssueDate }, "IssueDate", "2");
            txtIssueBy.Text = input.IssueBy;
            lblAccountingObjectAddress.Visible = txtAccountingObjectAddress.Visible = lblAccountingObjectBankAccount.Visible = cbbAccountingObjBank.Visible = txtAccountingObjtBankName.Visible = true;
            if (input.TypeID == 140)
            {
                lblAccountingObjectAddress.Visible = txtAccountingObjectAddress.Visible = lblAccountingObjectBankAccount.Visible = cbbAccountingObjBank.Visible = txtAccountingObjtBankName.Visible = false;
                lblReceiver.Visible = txtReceiver.Visible = lblIdentificationNo.Visible = txtIdentificationNo.Visible = lblIssuDate.Visible = dteIssueDate.Visible = lblIssueBy.Visible = txtIssueBy.Visible = true;
            }
            if (lstSingleCellInfo.Any(p => p.Equals(input.TypeID)))
            {
                grAccounting.Visible = false;
            }
            else
            {
                grAccounting.Visible = true;
            }
            ShowTemplateByType(_select.TypeID);

            #endregion
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 120)//trungnq thêm vào để  làm task 6234
                {
                    txtReason.Value = "Chi tiền từ TKNH";
                    txtReason.Text = "Chi tiền từ TKNH";
                    _select.Reason = "Chi tiền từ TKNH";
                };
            }
        }
        #endregion

        #region Utils

        #endregion

        #region Event
        // thay đổi loại giấy nợ
        private void cbbType_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            if (e.Row != null && e.Row.ListObject != null)
            {
                int typeId = (e.Row.ListObject as Accounting.Core.Domain.Type).ID;
                ShowTemplateByType(typeId);
            }
        }

        private void ShowTemplateByType(int typeID)
        {
            if (typeID == 140 || typeID == 141 || typeID == 142 || typeID == 143)
            {
                lblAccountingObject.Text = "Trả cho";
                lblAccountingObjectAddress.Text = "Người lĩnh tiền";
                // ẩn các box Địa chỉ, Tài khoản ngân hàng, Tên ngân hàng
                txtAccountingObjectAddress.Visible = false;
                lblAccountingObjectBankAccount.Visible = cbbAccountingObjBank.Visible = txtAccountingObjtBankName.Visible = false;
                // hiện các box Người lĩnh tiền, Chứng minh thư, ngày cấp, nơi cấp
                txtReceiver.Visible = true;
                lblIdentificationNo.Visible = txtIdentificationNo.Visible = true;
                lblIssuDate.Visible = dteIssueDate.Visible = true;
                lblIssueBy.Visible = txtIssueBy.Visible = true;
            }
            else
            {
                lblAccountingObject.Text = "Đối tượng";
                lblAccountingObjectAddress.Text = "Địa chỉ";
                //ẩn các box Người lĩnh tiền, Chứng minh thư, ngày cấp, nơi cấp
                lblIdentificationNo.Visible = lblIssuDate.Visible = lblIssueBy.Visible = false;
                txtReceiver.Visible = txtIdentificationNo.Visible = dteIssueDate.Visible = txtIssueBy.Visible = false;
                //hiện các box Địa chỉ đối tượng kế toán, tài khoản ngân hàng tên ngân hàng
                lblAccountingObjectBankAccount.Visible = true;
                txtAccountingObjectAddress.Visible = cbbAccountingObjBank.Visible = txtAccountingObjtBankName.Visible = true;
            }
            this.ReconfigAccountColumn<MBTellerPaper>(typeID);
            if (_select.TemplateID == null) return;
            //this.ReloadTemplate<MBTellerPaper>((Guid)_select.TemplateID, false);
            this.ConfigTemplateEdit<MBTellerPaper>(utmDetailBaseToolBar, TypeID, _statusForm, _select.TemplateID);
        }

        #endregion

        private void lblIssuDate_Click(object sender, EventArgs e)
        {

        }


        private void cbbBankAccount_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cbbBankAccount.Text))
            {
                txtBankName.Clear();
            }
        }

        private void cbbAccountingObjectID_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cbbAccountingObjectID.Text))
            {
                txtAccountingObjectName.Clear();
            }

            cbbAccountingObjBank.Value = null;
        }
        private void cbbAccountingObjBank_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cbbAccountingObjBank.Text))
            {
                txtAccountingObjtBankName.Clear();
            }
        }

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucherRSInwardOutward> datasource = (BindingList<RefVoucherRSInwardOutward>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucherRSInwardOutward>();
                    var f = new FViewVoucher(_select.CurrencyID, datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucher)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucherRSInwardOutward>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucherRSInwardOutward)
                    {
                        source.Add(new RefVoucherRSInwardOutward
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                if (!new int[] { 127, 131, 141, 129, 132, 142, 903, 904, 905, 133, 143, 126 }.Contains(_select.TypeID))
                {
                    RefVoucherRSInwardOutward temp = (RefVoucherRSInwardOutward)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                    if (temp != null)
                        editFuntion(temp);
                }
                else
                {
                    RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                    if (temp != null)
                        editFuntion(temp);
                }

            }
        }

        private void editFuntion(RefVoucherRSInwardOutward temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }
        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void ultraSplitter1_Click(object sender, EventArgs e)
        {

        }

        private void cbbBankAccount_Validated(object sender, EventArgs e)
        {
            //foreach(var item in _lstBankAccountDetails)
            //{
            //    if((cbbBankAccount.Text!= item.BankAccount)&&(cbbBankAccount.Text!=null) && (cbbBankAccount.Text != ""))
            //    { MSG.Warning("Dữ liệu không có trong danh mục");
            //        return;
            //    }
            //}
        }

        private void cbbAccountingObjBank_Leave(object sender, EventArgs e)
        {
            if (!((cbbAccountingObjBank.Value is Guid) || (cbbAccountingObjBank.Text.IsNullOrEmpty())))
            {
                if (txtAccountingObjectName.Text.IsNullOrEmpty())
                {
                    MSG.Warning("Xóa và chọn đối tượng");
                    return;
                }
                else
                {
                    MSG.Warning("Dữ liệu không có trong danh mục");
                    return;
                }
            }
        }

        private void FMBTellerPaperDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }


    public class FrmMBTP : DetailBase<MBTellerPaper>
    {
    }
}