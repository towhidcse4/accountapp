﻿namespace Accounting
{
    partial class UButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            this.ubtnBack = new Infragistics.Win.Misc.UltraButton();
            this.ubtnHelp = new Infragistics.Win.Misc.UltraButton();
            this.ubtnPrint = new Infragistics.Win.Misc.UltraButton();
            this.ubtnTemplate = new Infragistics.Win.Misc.UltraButton();
            this.ubtnUtilities = new Infragistics.Win.Misc.UltraButton();
            this.ubtnViewList = new Infragistics.Win.Misc.UltraButton();
            this.ubtnReset = new Infragistics.Win.Misc.UltraButton();
            this.ubtnPost = new Infragistics.Win.Misc.UltraButton();
            this.ubtnComeBack = new Infragistics.Win.Misc.UltraButton();
            this.ubtnDelete = new Infragistics.Win.Misc.UltraButton();
            this.ubtnSave = new Infragistics.Win.Misc.UltraButton();
            this.ubtnEdit = new Infragistics.Win.Misc.UltraButton();
            this.ubtnAdd = new Infragistics.Win.Misc.UltraButton();
            this.ubtnForward = new Infragistics.Win.Misc.UltraButton();
            this.ubtnClose = new Infragistics.Win.Misc.UltraButton();
            this.SuspendLayout();
            // 
            // ubtnBack
            // 
            appearance1.Image = global::Accounting.Properties.Resources.ubtnBack;
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Top;
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.ubtnBack.Appearance = appearance1;
            this.ubtnBack.Location = new System.Drawing.Point(3, 3);
            this.ubtnBack.Name = "ubtnBack";
            this.ubtnBack.Size = new System.Drawing.Size(46, 60);
            this.ubtnBack.TabIndex = 12;
            this.ubtnBack.Text = "Lùi";
            // 
            // ubtnHelp
            // 
            appearance2.Image = global::Accounting.Properties.Resources.ubtnHelp;
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance2.ImageVAlign = Infragistics.Win.VAlign.Top;
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ubtnHelp.Appearance = appearance2;
            this.ubtnHelp.Location = new System.Drawing.Point(679, 4);
            this.ubtnHelp.Name = "ubtnHelp";
            this.ubtnHelp.Size = new System.Drawing.Size(46, 60);
            this.ubtnHelp.TabIndex = 25;
            this.ubtnHelp.Text = "Giúp";
            // 
            // ubtnPrint
            // 
            appearance3.Image = global::Accounting.Properties.Resources.ubtnPrint;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Top;
            appearance3.TextHAlignAsString = "Center";
            appearance3.TextVAlignAsString = "Middle";
            this.ubtnPrint.Appearance = appearance3;
            this.ubtnPrint.Location = new System.Drawing.Point(627, 4);
            this.ubtnPrint.Name = "ubtnPrint";
            this.ubtnPrint.Size = new System.Drawing.Size(46, 60);
            this.ubtnPrint.TabIndex = 24;
            this.ubtnPrint.Text = "In";
            // 
            // ubtnTemplate
            // 
            appearance4.Image = global::Accounting.Properties.Resources.ubtnTemplate;
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance4.ImageVAlign = Infragistics.Win.VAlign.Top;
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.ubtnTemplate.Appearance = appearance4;
            this.ubtnTemplate.Location = new System.Drawing.Point(575, 4);
            this.ubtnTemplate.Name = "ubtnTemplate";
            this.ubtnTemplate.Size = new System.Drawing.Size(46, 60);
            this.ubtnTemplate.TabIndex = 23;
            this.ubtnTemplate.Text = "Mẫu";
            // 
            // ubtnUtilities
            // 
            appearance5.Image = global::Accounting.Properties.Resources.ubtnUtilities;
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Top;
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.ubtnUtilities.Appearance = appearance5;
            this.ubtnUtilities.Location = new System.Drawing.Point(523, 4);
            this.ubtnUtilities.Name = "ubtnUtilities";
            this.ubtnUtilities.Size = new System.Drawing.Size(46, 60);
            this.ubtnUtilities.TabIndex = 22;
            this.ubtnUtilities.Text = "Tiện ích";
            // 
            // ubtnViewList
            // 
            appearance6.Image = global::Accounting.Properties.Resources.ubtnViewList;
            appearance6.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance6.ImageVAlign = Infragistics.Win.VAlign.Top;
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.ubtnViewList.Appearance = appearance6;
            this.ubtnViewList.Location = new System.Drawing.Point(471, 4);
            this.ubtnViewList.Name = "ubtnViewList";
            this.ubtnViewList.Size = new System.Drawing.Size(46, 60);
            this.ubtnViewList.TabIndex = 21;
            this.ubtnViewList.Text = "Duyệt";
            // 
            // ubtnReset
            // 
            appearance7.Image = global::Accounting.Properties.Resources.ubtnReset;
            appearance7.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance7.ImageVAlign = Infragistics.Win.VAlign.Top;
            appearance7.TextHAlignAsString = "Center";
            appearance7.TextVAlignAsString = "Middle";
            this.ubtnReset.Appearance = appearance7;
            this.ubtnReset.Location = new System.Drawing.Point(419, 4);
            this.ubtnReset.Name = "ubtnReset";
            this.ubtnReset.Size = new System.Drawing.Size(46, 60);
            this.ubtnReset.TabIndex = 20;
            this.ubtnReset.Text = "Làm mới";
            // 
            // ubtnPost
            // 
            appearance8.Image = global::Accounting.Properties.Resources.ubtnPost;
            appearance8.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance8.ImageVAlign = Infragistics.Win.VAlign.Top;
            appearance8.TextHAlignAsString = "Center";
            appearance8.TextVAlignAsString = "Middle";
            this.ubtnPost.Appearance = appearance8;
            this.ubtnPost.Location = new System.Drawing.Point(367, 4);
            this.ubtnPost.Name = "ubtnPost";
            this.ubtnPost.Size = new System.Drawing.Size(46, 60);
            this.ubtnPost.TabIndex = 19;
            this.ubtnPost.Text = "Ghi sổ";
            // 
            // ubtnComeBack
            // 
            appearance9.Image = global::Accounting.Properties.Resources.ubtnComeBack;
            appearance9.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance9.ImageVAlign = Infragistics.Win.VAlign.Top;
            appearance9.TextHAlignAsString = "Center";
            appearance9.TextVAlignAsString = "Middle";
            this.ubtnComeBack.Appearance = appearance9;
            this.ubtnComeBack.Location = new System.Drawing.Point(315, 3);
            this.ubtnComeBack.Name = "ubtnComeBack";
            this.ubtnComeBack.Size = new System.Drawing.Size(46, 60);
            this.ubtnComeBack.TabIndex = 18;
            this.ubtnComeBack.Text = "Trở về";
            // 
            // ubtnDelete
            // 
            appearance10.Image = global::Accounting.Properties.Resources.ubtnDelete;
            appearance10.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance10.ImageVAlign = Infragistics.Win.VAlign.Top;
            appearance10.TextHAlignAsString = "Center";
            appearance10.TextVAlignAsString = "Middle";
            this.ubtnDelete.Appearance = appearance10;
            this.ubtnDelete.Location = new System.Drawing.Point(263, 3);
            this.ubtnDelete.Name = "ubtnDelete";
            this.ubtnDelete.Size = new System.Drawing.Size(46, 60);
            this.ubtnDelete.TabIndex = 17;
            this.ubtnDelete.Text = "Hủy bỏ";
            // 
            // ubtnSave
            // 
            appearance11.Image = global::Accounting.Properties.Resources.ubtnSave;
            appearance11.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance11.ImageVAlign = Infragistics.Win.VAlign.Top;
            appearance11.TextHAlignAsString = "Center";
            appearance11.TextVAlignAsString = "Middle";
            this.ubtnSave.Appearance = appearance11;
            this.ubtnSave.Location = new System.Drawing.Point(211, 3);
            this.ubtnSave.Name = "ubtnSave";
            this.ubtnSave.Size = new System.Drawing.Size(46, 60);
            this.ubtnSave.TabIndex = 16;
            this.ubtnSave.Text = "Lưu";
            // 
            // ubtnEdit
            // 
            appearance12.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            appearance12.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance12.ImageVAlign = Infragistics.Win.VAlign.Top;
            appearance12.TextHAlignAsString = "Center";
            appearance12.TextVAlignAsString = "Middle";
            this.ubtnEdit.Appearance = appearance12;
            this.ubtnEdit.Location = new System.Drawing.Point(159, 4);
            this.ubtnEdit.Name = "ubtnEdit";
            this.ubtnEdit.Size = new System.Drawing.Size(46, 60);
            this.ubtnEdit.TabIndex = 15;
            this.ubtnEdit.Text = "Sửa";
            // 
            // ubtnAdd
            // 
            appearance13.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            appearance13.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance13.ImageVAlign = Infragistics.Win.VAlign.Top;
            appearance13.TextHAlignAsString = "Center";
            appearance13.TextVAlignAsString = "Middle";
            this.ubtnAdd.Appearance = appearance13;
            this.ubtnAdd.Location = new System.Drawing.Point(107, 3);
            this.ubtnAdd.Name = "ubtnAdd";
            this.ubtnAdd.Size = new System.Drawing.Size(46, 60);
            this.ubtnAdd.TabIndex = 14;
            this.ubtnAdd.Text = "Thêm";
            // 
            // ubtnForward
            // 
            appearance14.Image = global::Accounting.Properties.Resources.ubtnForward;
            appearance14.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance14.ImageVAlign = Infragistics.Win.VAlign.Top;
            appearance14.TextHAlignAsString = "Center";
            appearance14.TextVAlignAsString = "Middle";
            this.ubtnForward.Appearance = appearance14;
            this.ubtnForward.Location = new System.Drawing.Point(55, 3);
            this.ubtnForward.Name = "ubtnForward";
            this.ubtnForward.Size = new System.Drawing.Size(46, 60);
            this.ubtnForward.TabIndex = 13;
            this.ubtnForward.Text = "Tiến";
            // 
            // ubtnClose
            // 
            appearance15.Image = global::Accounting.Properties.Resources.ubtnClose;
            appearance15.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance15.ImageVAlign = Infragistics.Win.VAlign.Top;
            appearance15.TextHAlignAsString = "Center";
            appearance15.TextVAlignAsString = "Middle";
            this.ubtnClose.Appearance = appearance15;
            this.ubtnClose.Location = new System.Drawing.Point(731, 4);
            this.ubtnClose.Name = "ubtnClose";
            this.ubtnClose.Size = new System.Drawing.Size(46, 60);
            this.ubtnClose.TabIndex = 26;
            this.ubtnClose.Text = "Đóng";
            // 
            // UButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ubtnClose);
            this.Controls.Add(this.ubtnHelp);
            this.Controls.Add(this.ubtnPrint);
            this.Controls.Add(this.ubtnTemplate);
            this.Controls.Add(this.ubtnUtilities);
            this.Controls.Add(this.ubtnViewList);
            this.Controls.Add(this.ubtnReset);
            this.Controls.Add(this.ubtnPost);
            this.Controls.Add(this.ubtnComeBack);
            this.Controls.Add(this.ubtnDelete);
            this.Controls.Add(this.ubtnSave);
            this.Controls.Add(this.ubtnEdit);
            this.Controls.Add(this.ubtnAdd);
            this.Controls.Add(this.ubtnForward);
            this.Controls.Add(this.ubtnBack);
            this.Name = "UButton";
            this.Size = new System.Drawing.Size(783, 68);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton ubtnBack;
        private Infragistics.Win.Misc.UltraButton ubtnHelp;
        private Infragistics.Win.Misc.UltraButton ubtnPrint;
        private Infragistics.Win.Misc.UltraButton ubtnTemplate;
        private Infragistics.Win.Misc.UltraButton ubtnUtilities;
        private Infragistics.Win.Misc.UltraButton ubtnViewList;
        private Infragistics.Win.Misc.UltraButton ubtnReset;
        private Infragistics.Win.Misc.UltraButton ubtnPost;
        private Infragistics.Win.Misc.UltraButton ubtnComeBack;
        private Infragistics.Win.Misc.UltraButton ubtnDelete;
        private Infragistics.Win.Misc.UltraButton ubtnSave;
        private Infragistics.Win.Misc.UltraButton ubtnEdit;
        private Infragistics.Win.Misc.UltraButton ubtnAdd;
        private Infragistics.Win.Misc.UltraButton ubtnForward;
        private Infragistics.Win.Misc.UltraButton ubtnClose;

    }
}
