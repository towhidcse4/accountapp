﻿using log4net;
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace Accounting
{
    public partial class WaitingFrm : Form
    {
        private PictureBox pictureBox1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private static Thread ThdWaiting;
        private Infragistics.Win.Misc.UltraPanel pnlMain;
        private Infragistics.Win.Misc.UltraLabel lblDot;
        static WaitingFrm waitingFrm;
        public WaitingFrm()
        {
            //switch (style)
            //{
            //    case 1: 
            InitializeComponent();
            //        break;
            //    case 2: InitializeComponent2(); break;
            //    case 3: InitializeComponent4(); break;
            //    case 4: InitializeComponent3(); break;//Skype
            //    default: InitializeComponent1(); break;
            //}
            Thread th = new Thread(new ThreadStart(RunningLabelDotAnimation));
            th.IsBackground = true;
            th.Start();
            this.Closing += new System.ComponentModel.CancelEventHandler((s, e) => WaitingFrm_Closing(s, e, th));
        }

        void WaitingFrm_Closing(object sender, System.ComponentModel.CancelEventArgs e, Thread thAnimation)
        {
            if (thAnimation.ThreadState == ThreadState.Running)
                thAnimation.Abort();
        }

        private void RunningLabelDotAnimation()
        {
            int i = 1;
            while (i <= 3)
            {
                var s = string.Empty;
                for (int j = 0; j < i; j++)
                {
                    if (j > 0) s += " ";
                    s += ".";
                }
                lblDot.InvokeAction(() => SetText(s));
                //lblDot.SetPropertyValue(a => a.Text, s);
                Thread.Sleep(1000);
                i++;
                if (i > 3) i = 1;
            }
        }

        void SetText(string text)
        {
            lblDot.Text = text;
        }

        static private void ShowFormThread()
        {
            waitingFrm = new WaitingFrm();
            Application.Run(waitingFrm);
        }
        public void CloseScreen()
        {
            if (InvokeRequired)
            {
                this.Invoke((Action)(() =>
                {
                    this.Close();
                    this.Dispose();
                }));
                return;
            }
            this.Close();
            this.Dispose();
            //this.InvokeAction(Close);
        }

        public static void StartWaiting()
        {
            try
            {
                if (waitingFrm != null) return;
                ThdWaiting = new Thread(new ThreadStart(() => ShowFormThread()));
                ThdWaiting.IsBackground = true;
                ThdWaiting.SetApartmentState(ApartmentState.STA);
                ThdWaiting.Start();
            }
            catch (Exception)
            {
            }
        }
        private static readonly ILog log = LogManager.GetLogger(typeof(Bootstrapper));
        public static void StopWaiting()
        {
            try
            {
                if (ThdWaiting == null) return;
                if (waitingFrm == null) goto END;
                try
                {
                    waitingFrm.CloseScreen();
                }
                catch { }
            END:
                try
                {
                    ThdWaiting.Abort();
                    ThdWaiting = null;
                    waitingFrm = null;
                }
                catch { }
            }
            catch (Exception)
            {
            }
        }
        //private void InitializeComponent1()
        //{
        //    Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
        //    this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
        //    this.pictureBox1 = new System.Windows.Forms.PictureBox();
        //    ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
        //    this.SuspendLayout();
        //    // 
        //    // ultraLabel1
        //    // 
        //    appearance1.BackColor = System.Drawing.Color.Transparent;
        //    appearance1.TextHAlignAsString = "Center";
        //    appearance1.TextVAlignAsString = "Middle";
        //    this.ultraLabel1.Appearance = appearance1;
        //    this.ultraLabel1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.ultraLabel1.Location = new System.Drawing.Point(0, 147);
        //    this.ultraLabel1.Name = "ultraLabel1";
        //    this.ultraLabel1.Size = new System.Drawing.Size(177, 23);
        //    this.ultraLabel1.TabIndex = 0;
        //    this.ultraLabel1.Text = "Đang xử lý";
        //    // 
        //    // pictureBox1
        //    // 
        //    this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
        //    this.pictureBox1.Image = global::Accounting.Properties.Resources._301;
        //    this.pictureBox1.Location = new System.Drawing.Point(24, 12);
        //    this.pictureBox1.Name = "pictureBox1";
        //    this.pictureBox1.Size = new System.Drawing.Size(128, 128);
        //    this.pictureBox1.TabIndex = 1;
        //    this.pictureBox1.TabStop = false;
        //    // 
        //    // WaitingFrm
        //    // 
        //    this.BackColor = System.Drawing.Color.White;
        //    this.ClientSize = new System.Drawing.Size(177, 177);
        //    this.Controls.Add(this.pictureBox1);
        //    this.Controls.Add(this.ultraLabel1);
        //    this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
        //    this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
        //    this.Name = "WaitingFrm";
        //    this.Opacity = 0.8D;
        //    this.ShowIcon = false;
        //    this.ShowInTaskbar = false;
        //    this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        //    this.TopMost = true;
        //    ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
        //    this.ResumeLayout(false);

        //}
        //private void InitializeComponent2()
        //{
        //    this.pictureBox1 = new System.Windows.Forms.PictureBox();
        //    ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
        //    this.SuspendLayout();
        //    // 
        //    // pictureBox1
        //    // 
        //    this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
        //    this.pictureBox1.Image = global::Accounting.Properties.Resources._294;
        //    this.pictureBox1.Location = new System.Drawing.Point(10, 10);
        //    this.pictureBox1.Name = "pictureBox1";
        //    this.pictureBox1.Size = new System.Drawing.Size(220, 20);
        //    this.pictureBox1.TabIndex = 0;
        //    this.pictureBox1.TabStop = false;
        //    // 
        //    // WaitingFrmLine
        //    // 
        //    this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
        //    this.BackColor = System.Drawing.Color.White;
        //    this.ClientSize = new System.Drawing.Size(240, 40);
        //    this.Controls.Add(this.pictureBox1);
        //    this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
        //    this.Name = "WaitingFrmLine";
        //    this.Opacity = 0.9D;
        //    this.ShowIcon = false;
        //    this.ShowInTaskbar = false;
        //    this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        //    this.Text = "WaitingFrmLine";
        //    this.TopMost = true;
        //    ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
        //    this.ResumeLayout(false);
        //}
        //private void InitializeComponent3()
        //{
        //    Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
        //    this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
        //    this.pictureBox1 = new System.Windows.Forms.PictureBox();
        //    ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
        //    this.SuspendLayout();
        //    // 
        //    // ultraLabel1
        //    // 
        //    appearance1.BackColor = System.Drawing.Color.Transparent;
        //    appearance1.TextHAlignAsString = "Center";
        //    appearance1.TextVAlignAsString = "Middle";
        //    appearance1.ForeColor = System.Drawing.Color.White;
        //    this.ultraLabel1.Appearance = appearance1;
        //    this.ultraLabel1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.ultraLabel1.Location = new System.Drawing.Point(0, 147);
        //    this.ultraLabel1.Name = "ultraLabel1";
        //    this.ultraLabel1.Size = new System.Drawing.Size(177, 23);
        //    this.ultraLabel1.TabIndex = 0;
        //    this.ultraLabel1.Text = "Đang xử lý";
        //    // 
        //    // pictureBox1
        //    // 
        //    this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
        //    this.pictureBox1.Image = global::Accounting.Properties.Resources._728;
        //    this.pictureBox1.Location = new System.Drawing.Point(58, 45);
        //    this.pictureBox1.Name = "pictureBox1";
        //    this.pictureBox1.Size = new System.Drawing.Size(60, 64);
        //    this.pictureBox1.TabIndex = 1;
        //    this.pictureBox1.TabStop = false;
        //    // 
        //    // WaitingFrm
        //    // 
        //    this.BackColor = System.Drawing.Color.FromArgb(0, 58, 130);
        //    this.ClientSize = new System.Drawing.Size(177, 177);
        //    this.Controls.Add(this.pictureBox1);
        //    this.Controls.Add(this.ultraLabel1);
        //    this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
        //    this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
        //    this.Name = "WaitingFrm";
        //    this.ShowIcon = false;
        //    this.ShowInTaskbar = false;
        //    this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        //    this.TopMost = true;
        //    ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
        //    this.ResumeLayout(false);

        //}
        //private void InitializeComponent4()
        //{
        //    Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
        //    this.pictureBox1 = new System.Windows.Forms.PictureBox();
        //    ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
        //    this.SuspendLayout();
        //    // 
        //    // pictureBox1
        //    // 
        //    this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
        //    this.pictureBox1.Image = global::Accounting.Properties.Resources._490;
        //    this.pictureBox1.Location = new System.Drawing.Point(10, 10);
        //    this.pictureBox1.Name = "pictureBox1";
        //    this.pictureBox1.Size = new System.Drawing.Size(128, 110);
        //    this.pictureBox1.TabIndex = 1;
        //    this.pictureBox1.TabStop = false;
        //    // 
        //    // WaitingFrm
        //    // 
        //    this.BackColor = System.Drawing.Color.White;
        //    this.ClientSize = new System.Drawing.Size(148, 130);
        //    this.Controls.Add(this.pictureBox1);
        //    this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
        //    this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
        //    this.Name = "WaitingFrm";
        //    //this.Opacity = 0.8D;
        //    this.ShowIcon = false;
        //    this.ShowInTaskbar = false;
        //    this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        //    this.TopMost = true;
        //    this.TransparencyKey = System.Drawing.Color.White;
        //    ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
        //    this.ResumeLayout(false);

        //}
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.pnlMain = new Infragistics.Win.Misc.UltraPanel();
            this.lblDot = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlMain.ClientArea.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Accounting.Properties.Resources._301x48_blue;
            this.pictureBox1.Location = new System.Drawing.Point(27, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(48, 48);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // ultraLabel1
            // 
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance1;
            this.ultraLabel1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel1.Location = new System.Drawing.Point(81, 13);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(89, 48);
            this.ultraLabel1.TabIndex = 1;
            this.ultraLabel1.Text = "Đang xử lý";
            // 
            // pnlMain
            // 
            appearance2.BorderColor = System.Drawing.Color.Gainsboro;
            this.pnlMain.Appearance = appearance2;
            this.pnlMain.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // pnlMain.ClientArea
            // 
            this.pnlMain.ClientArea.Controls.Add(this.lblDot);
            this.pnlMain.ClientArea.Controls.Add(this.pictureBox1);
            this.pnlMain.ClientArea.Controls.Add(this.ultraLabel1);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(207, 74);
            this.pnlMain.TabIndex = 2;
            this.pnlMain.UseAppStyling = false;
            // 
            // lblDot
            // 
            appearance3.TextVAlignAsString = "Middle";
            this.lblDot.Appearance = appearance3;
            this.lblDot.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDot.Location = new System.Drawing.Point(157, 13);
            this.lblDot.Name = "lblDot";
            this.lblDot.Size = new System.Drawing.Size(29, 48);
            this.lblDot.TabIndex = 2;
            this.lblDot.Text = ".";
            // 
            // WaitingFrm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(207, 74);
            this.Controls.Add(this.pnlMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "WaitingFrm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WaitingFrmLine";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.WaitingFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlMain.ClientArea.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void WaitingFrm_Load(object sender, EventArgs e)
        {
            System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
        }
    }
}
