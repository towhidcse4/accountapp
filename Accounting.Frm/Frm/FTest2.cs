﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FTest2 : Form
    {
        public FTest2()
        {
            InitializeComponent();
            //khởi tạo
            //foreach (Control c in this.Controls)
            //{
            //    PropertyInfo k = c.GetType().GetProperty("ReadOnly");
            //    if (k != null && k.CanWrite)
            //        k.SetValue(c, true, null);
            //    if (c.Controls.Count > 0)
            //    {
            //        int a = 0;
            //    }
            //}
            List<ForTest> forTests = GenData();

            cbbForTest.DataSource = forTests;
            cbbForTest.DisplayMember = "Code";
            Config(cbbForTest.DisplayLayout.Bands[0]);

            uGrid.DataSource = forTests;
            //Config(uGrid.DisplayLayout.Bands[0]);

            //hiển thị 1 band
            uGrid.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            //tắt lọc cột
            uGrid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGrid.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.True;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            //Hiển thị SupportDataErrorInfo
            uGrid.DisplayLayout.Override.SupportDataErrorInfo = SupportDataErrorInfo.CellsOnly;
            //Hiện những dòng trống?
            uGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGrid.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;

            uGrid.DisplayLayout.Bands[0].Summaries.Clear();

            uGrid.DisplayLayout.Override.AllowRowSummaries = AllowRowSummaries.Default; //Ẩn ký hiệu tổng trên Header Caption
            if (uGrid.DisplayLayout.Bands[0].Summaries.Count != 0) uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            SummarySettings summary = uGrid.DisplayLayout.Bands[0].Summaries.Add("Count", SummaryType.Count, uGrid.DisplayLayout.Bands[0].Columns["Code"]);
            summary.DisplayFormat = "Số dòng = {0:N0}";
            summary.SummaryPosition = SummaryPosition.Left;
            uGrid.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False;   //ẩn

            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            uGrid.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            uGrid.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
            uGrid.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
            ProcessControls(this);
        }

        private void FTest2_Load(object sender, EventArgs e)
        {
            // Do something
        }
        private void ProcessControls(Control ctrlContainer)
        {
            foreach (Control ctrl in ctrlContainer.Controls)
            {
                if (!ctrl.GetType().Name.Contains(typeof(UltraGrid).Name))
                {
                    PropertyInfo propInfo = ctrl.GetType().GetProperty("ReadOnly");
                    if (propInfo != null && propInfo.CanWrite)
                        propInfo.SetValue(ctrl, true, null);
                }
                else
                {
                    UltraGrid uGrid = (UltraGrid)ctrl;
                    foreach (UltraGridColumn column in uGrid.DisplayLayout.Bands[0].Columns)
                    {
                        if (column.CellActivation == Activation.AllowEdit)
                            column.CellActivation = Activation.ActivateOnly;
                    }
                }
                if (ctrl.HasChildren)
                    ProcessControls(ctrl);
            }
        }

        #region Utils
        private static void GenObject<T>(ICollection<ForTest> forTests, string code, string name)
        {//Đặt try catch để tránh form nào bị lỗi thì không ảnh hưởng đến các form còn lại
            try
            {
                forTests.Add(new ForTest { Code = code, Name = name, Form = (Form)Activator.CreateInstance(typeof(T)) });
            }
            catch (Exception exception)
            {
                forTests.Add(new ForTest { Code = code, Name = name, Form = null, Msg = exception, Description = "Bị lỗi" });
            }
        }

        static void Config(UltraGridBand band)
        {
            band.Override.CellClickAction = CellClickAction.RowSelect;
            band.Override.SelectedRowAppearance.BackColor = System.Drawing.Color.FromArgb(255, 192, 128);
            foreach (var item in band.Columns)
            {
                if (item.Key.Equals("Code"))
                {
                    item.Header.Caption = "Mã Form";
                    item.Hidden = false;
                    item.Width = 170;
                }
                else if (item.Key.Equals("Name"))
                {
                    item.Header.Caption = "Tên Form";
                    item.Hidden = false;
                    item.Width = 262;
                }
                else if (item.Key.Equals("Form"))
                {
                    item.Header.Caption = "Form";
                    item.Hidden = true;
                }
                else if (item.Key.Equals("Msg"))
                {
                    item.Header.Caption = "Error";
                    item.Hidden = true;
                }
                else if (item.Key.Equals("Description"))
                {
                    item.Header.Caption = "Trạng thái";
                    item.Hidden = false;
                }
            }
        }

        static List<ForTest> GenData()
        {
            List<ForTest> forTests = new List<ForTest>();
            GenObject<FAccount>(forTests, "FAccount", "Hệ thống tài khoản");
            GenObject<FAccountDefault>(forTests, "FAccountDefault", "Tài khoản ngầm định");
            GenObject<FAccountGroup>(forTests, "FAccountGroup", "Nhóm tài khoản");
            GenObject<FAccountingObjectCustomers>(forTests, "FAccountingObjectCustomers", "Khách hàng/ Nhà cung cấp");
            GenObject<FAccountingObjectEmployee>(forTests, "FAccountingObjectEmployee", "Nhân viên");
            GenObject<FAccountingObjectCategory>(forTests, "FAccountingObjectCategory", "Loại đối tượng khách hàng, NCC");
            GenObject<FAccountingObjectGroup>(forTests, "FAccountingObjectGroup", "Nhóm khách hàng, nhà cung cấp");
            GenObject<FAccountTransfer>(forTests, "FAccountTransfer", "Tài khoản kết chuyển");
            GenObject<FAutoPrinciple>(forTests, "FAutoPrinciple", "Định khoản tự động");
            GenObject<FBank>(forTests, "FBank", "Ngân hàng");
            GenObject<FBankAccountDetail>(forTests, "FBankAccountDetail", "Thông tin tài khoản NH");
            GenObject<FBudgetItem>(forTests, "FBudgetItem", "Mục thu/chi");
            GenObject<FContractState>(forTests, "FContractState", "Tình trạng hợp đồng");
            GenObject<FCostSet>(forTests, "FCostSet", "đối tượng tập hợp chi phí");
            GenObject<FCreditCard>(forTests, "FCreditCard", "Thẻ tín dụng");
            GenObject<FCurrency>(forTests, "FCurrency", "tiền tệ");
            GenObject<FDepartment>(forTests, "FDepartment", "Phòng ban");
            GenObject<FExpenseItem>(forTests, "FExpenseItem", "khoản mục chi phí");
            GenObject<FFixedAsset>(forTests, "FFixedAsset", "Tài sản cố định");
            GenObject<FFixedAssetCategory>(forTests, "FFixedAssetCategory", "loại TSCĐ");
            GenObject<FInvestor>(forTests, "FInvestor", "Nhà đầu tư");
            GenObject<FInvestorGroup>(forTests, "FInvestorGroup", "Nhóm nhà đầu tư");
            GenObject<FMaterialGoodsCategory>(forTests, "FMaterialGoodsCategory", "loại VTHH, CCDC");
            GenObject<FMaterialQuantum>(forTests, "FMaterialQuantum", "Định mức nguyên vật liệu");
            GenObject<FMaterialGoodsSpecialTaxGroup>(forTests, "FMaterialGoodsSpecialTaxGroup", "Nhóm hàng hóa chịu thuế TTĐB");
            GenObject<FPaymentClause>(forTests, "FPaymentClause", "Điều khoản thanh toán");
            GenObject<FPersonalSalaryTax>(forTests, "FPersonalSalaryTax", "Biểu tính thuế thu nhập cá nhân");
            GenObject<FRegistrationGroup>(forTests, "FRegistrationGroup", "Nhóm đăng ký(Đăng ký mua cổ phần)");
            GenObject<FRepository>(forTests, "FRepository", "Kho");
            GenObject<FShareHolderGroup>(forTests, "FShareHolderGroup", "Nhóm cổ đông");
            GenObject<FStaff>(forTests, "FStaff", "FStaff");
            GenObject<FStatisticsCode>(forTests, "FStatisticsCode", "Mã thống kê");
            GenObject<FStockCategory>(forTests, "FStockCategory", "cổ phần(loại cổ phần)");
            GenObject<FTimeSheetSymbols>(forTests, "FTimeSheetSymbols", "Ký hiệu chấm công");
            GenObject<FType>(forTests, "FType", "Loại chứng từ");
            GenObject<FTransportMethod>(forTests, "FTransportMethod", "Phương thức vận chuyển");
            GenObject<FGoodsServicePurchase>(forTests, "FGoodsServicePurchase", "Nhóm hàng hóa dịch vụ");
            return forTests;
        }
        #endregion

        private void UGridDoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index == -1) return;
            if (uGrid.ActiveCell != null)
            {
                ForTest forTest = uGrid.ActiveCell.Row.ListObject as ForTest;
                if (forTest == null) return;
                if (forTest.Form == null)
                {
                    bool checkMsgNull = forTest.Msg.InnerException == null;
                    MSG.Error("Form " + forTest.Code + "có lỗi không thể mở được!\r\n\r\n" + (checkMsgNull ? forTest.Msg.Message : forTest.Msg.InnerException.Message) + "\r\n\r\n" + (checkMsgNull ? forTest.Msg.Message : forTest.Msg.InnerException.StackTrace));
                    return;
                }
                try
                {
                    forTest.Form.Show();
                }
                catch (Exception exception)
                {
                    MSG.Error("Form " + forTest.Code + "có lỗi không thể mở được!\r\n\r\n" + exception.Message + "\r\n\r\n" + exception.StackTrace);
                }

            }
            else
                MSG.Error("Bạn chưa chọn đối tượng!");
        }

        private void BtnOpenClick(object sender, EventArgs e)
        {
            ForTest select = (ForTest)Utils.getSelectCbbItem(cbbForTest);
            if (select != null) select.Form.ShowDialog(this);
        }

        private void ultraDropDown1_RowSelected(object sender, RowSelectedEventArgs e)
        {

        }

        private void cbbForTest_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {

        }

    }

    public class ForTest
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public Form Form { get; set; }
        public Exception Msg { get; set; }
        public string Description { get; set; }

        public ForTest()
        {
            Form = new Form();
            Name = string.Empty;
            Code = string.Empty;
        }
    }
}
