﻿namespace Accounting
{
    partial class FImportData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FImportData));
            this.btnReadExcel = new Infragistics.Win.Misc.UltraButton();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.btnApply = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanelTop = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridSelect = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uLblTable = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uProcessBar = new Infragistics.Win.UltraWinProgressBar.UltraProgressBar();
            this.txtExcelFilePath = new Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanelTop.ClientArea.SuspendLayout();
            this.ultraPanelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSelect)).BeginInit();
            this.SuspendLayout();
            // 
            // btnReadExcel
            // 
            this.btnReadExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnReadExcel.Appearance = appearance1;
            this.btnReadExcel.Enabled = false;
            this.btnReadExcel.Location = new System.Drawing.Point(424, 332);
            this.btnReadExcel.Name = "btnReadExcel";
            this.btnReadExcel.Size = new System.Drawing.Size(125, 27);
            this.btnReadExcel.TabIndex = 7;
            this.btnReadExcel.Text = "Đọc Sheet Excel";
            this.btnReadExcel.Click += new System.EventHandler(this.btnReadExcel_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 326);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 45);
            this.splitter1.TabIndex = 5;
            this.splitter1.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnCancel.Appearance = appearance2;
            this.btnCancel.Location = new System.Drawing.Point(687, 332);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(83, 27);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance3.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnApply.Appearance = appearance3;
            this.btnApply.Enabled = false;
            this.btnApply.Location = new System.Drawing.Point(555, 332);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(126, 27);
            this.btnApply.TabIndex = 3;
            this.btnApply.Text = "Chuyển dữ liệu";
            this.btnApply.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // ultraPanelTop
            // 
            // 
            // ultraPanelTop.ClientArea
            // 
            this.ultraPanelTop.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.ultraPanelTop.ClientArea.Controls.Add(this.uLblTable);
            this.ultraPanelTop.ClientArea.Controls.Add(this.ultraLabel2);
            this.ultraPanelTop.ClientArea.Controls.Add(this.uProcessBar);
            this.ultraPanelTop.ClientArea.Controls.Add(this.txtExcelFilePath);
            this.ultraPanelTop.ClientArea.Controls.Add(this.ultraLabel1);
            this.ultraPanelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanelTop.Location = new System.Drawing.Point(0, 0);
            this.ultraPanelTop.Name = "ultraPanelTop";
            this.ultraPanelTop.Size = new System.Drawing.Size(773, 326);
            this.ultraPanelTop.TabIndex = 0;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.uGridSelect);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 100);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(773, 226);
            this.ultraGroupBox1.TabIndex = 9;
            this.ultraGroupBox1.Text = "Dữ liệu chọn";
            // 
            // uGridSelect
            // 
            this.uGridSelect.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridSelect.Location = new System.Drawing.Point(3, 16);
            this.uGridSelect.Name = "uGridSelect";
            this.uGridSelect.Size = new System.Drawing.Size(767, 207);
            this.uGridSelect.TabIndex = 1;
            this.uGridSelect.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridSelect_CellChange);
            // 
            // uLblTable
            // 
            this.uLblTable.Location = new System.Drawing.Point(151, 43);
            this.uLblTable.Name = "uLblTable";
            this.uLblTable.Size = new System.Drawing.Size(218, 23);
            this.uLblTable.TabIndex = 8;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(9, 43);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(148, 23);
            this.ultraLabel2.TabIndex = 7;
            this.ultraLabel2.Text = "Đang chuyển dữ liệu bảng:";
            // 
            // uProcessBar
            // 
            this.uProcessBar.Location = new System.Drawing.Point(3, 70);
            this.uProcessBar.Name = "uProcessBar";
            this.uProcessBar.Size = new System.Drawing.Size(767, 23);
            this.uProcessBar.TabIndex = 6;
            this.uProcessBar.Text = "[Formatted]";
            // 
            // txtExcelFilePath
            // 
            appearance4.Image = global::Accounting.Properties.Resources.Excel;
            appearance4.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance4.TextVAlignAsString = "Middle";
            this.txtExcelFilePath.Appearance = appearance4;
            appearance5.Image = global::Accounting.Properties.Resources.Excel;
            editorButton1.Appearance = appearance5;
            this.txtExcelFilePath.ButtonsLeft.Add(editorButton1);
            this.txtExcelFilePath.Location = new System.Drawing.Point(89, 12);
            this.txtExcelFilePath.Name = "txtExcelFilePath";
            this.txtExcelFilePath.Size = new System.Drawing.Size(383, 23);
            this.txtExcelFilePath.TabIndex = 6;
            this.txtExcelFilePath.Value = "Click để chọn file dữ liệu...";
            this.txtExcelFilePath.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtPdfFilePath_MouseClick);
            // 
            // ultraLabel1
            // 
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance6;
            this.ultraLabel1.Location = new System.Drawing.Point(12, 12);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(71, 23);
            this.ultraLabel1.TabIndex = 5;
            this.ultraLabel1.Text = "Tìm kiếm";
            // 
            // FImportData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(773, 371);
            this.Controls.Add(this.btnReadExcel);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.ultraPanelTop);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FImportData";
            this.Text = "FImportData";
            this.ultraPanelTop.ClientArea.ResumeLayout(false);
            this.ultraPanelTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridSelect)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanelTop;
        private Infragistics.Win.Misc.UltraButton btnApply;
        private Infragistics.Win.Misc.UltraButton btnCancel;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private System.Windows.Forms.Splitter splitter1;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor txtExcelFilePath;
        private Infragistics.Win.UltraWinProgressBar.UltraProgressBar uProcessBar;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel uLblTable;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraButton btnReadExcel;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSelect;
    }
}