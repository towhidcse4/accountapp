﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Linq.Dynamic;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Windows.Forms.VisualStyles;
using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using FX.Core;
using Infragistics.Win;
using Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using Microsoft.SqlServer.Server;
using System.Reflection;
using FX.Data;
using System.Globalization;
using Accounting.TextMessage;
using Accounting.Core.Domain.obj.AllOPAccount;


namespace Accounting
{
    public partial class FImportData : CustormForm
    {
        #region Khai báo mở Services
        public ISystemOptionService _ISystemOptionService { get { return IoC.Resolve<ISystemOptionService>(); } }
        public IAccountService _IAccountService { get { return IoC.Resolve<IAccountService>(); } }
        public IAccountGroupService _IAccountGroupService { get { return IoC.Resolve<IAccountGroupService>(); } }
        public IAccountingObjectGroupService _IAccountingObjectGroupService { get { return IoC.Resolve<IAccountingObjectGroupService>(); } }
        public IAccountingObjectCategoryService _IAccountingObjectCategoryService { get { return IoC.Resolve<IAccountingObjectCategoryService>(); } }
        public IAccountingObjectService _IAccountingObjectService { get { return IoC.Resolve<IAccountingObjectService>(); } }
        public IDepartmentService _IDepartmentService { get { return IoC.Resolve<IDepartmentService>(); } }
        public ICurrencyService _ICurrencyService { get { return IoC.Resolve<ICurrencyService>(); } }
        public IBankService _IBankService { get { return IoC.Resolve<IBankService>(); } }
        public IBankAccountDetailService _IBankAccountDetailService { get { return IoC.Resolve<IBankAccountDetailService>(); } }
        public IAccountTransferService _IAccountTransferService { get { return IoC.Resolve<IAccountTransferService>(); } }
        public IAccountDefaultService _IAccountDefaultService { get { return IoC.Resolve<IAccountDefaultService>(); } }
        public IPersonalSalaryTaxService _IPersonalSalaryTaxService { get { return IoC.Resolve<IPersonalSalaryTaxService>(); } }
        public ITimeSheetSymbolsService _ITimeSheetSymbolsService { get { return IoC.Resolve<ITimeSheetSymbolsService>(); } }
        public IRepositoryService _IRepositoryService { get { return IoC.Resolve<IRepositoryService>(); } }
        public IMaterialGoodsCategoryService _IMaterialGoodsCategoryService { get { return IoC.Resolve<IMaterialGoodsCategoryService>(); } }
        public IMaterialQuantumService _IMaterialQuantumService { get { return IoC.Resolve<IMaterialQuantumService>(); } }
        public IMaterialQuantumDetailService _IMaterialQuantumDetailService { get { return IoC.Resolve<IMaterialQuantumDetailService>(); } }
        public IFixedAssetCategoryService _IFixedAssetCategoryService { get { return IoC.Resolve<IFixedAssetCategoryService>(); } }
        public IFixedAssetService _IFixedAssetService { get { return IoC.Resolve<IFixedAssetService>(); } }
        public IFixedAssetDetailService _IFixedAssetDetailService { get { return IoC.Resolve<IFixedAssetDetailService>(); } }
        public IExpenseItemService _IExpenseItemService { get { return IoC.Resolve<IExpenseItemService>(); } }
        public ICostSetService _ICostSetService { get { return IoC.Resolve<ICostSetService>(); } }
        public IStockCategoryService _IStockCategoryService { get { return IoC.Resolve<IStockCategoryService>(); } }
        public IInvestorGroupService _IInvestorGroupService { get { return IoC.Resolve<IInvestorGroupService>(); } }
        public IInvestorService _IInvestorService { get { return IoC.Resolve<IInvestorService>(); } }
        public IRegistrationGroupService _IRegistrationGroupService { get { return IoC.Resolve<IRegistrationGroupService>(); } }
        public IShareHolderGroupService _IShareHolderGroupService { get { return IoC.Resolve<IShareHolderGroupService>(); } }
        public ICreditCardService _ICreditCardService { get { return IoC.Resolve<ICreditCardService>(); } }
        public IStatisticsCodeService _IStatisticsCodeService { get { return IoC.Resolve<IStatisticsCodeService>(); } }
        public IPaymentClauseService _IPaymentClauseService { get { return IoC.Resolve<IPaymentClauseService>(); } }
        public ITransportMethodService _ITransportMethodService { get { return IoC.Resolve<ITransportMethodService>(); } }
        public IBudgetItemService _IBudgetItemService { get { return IoC.Resolve<IBudgetItemService>(); } }
        public ISalePriceGroupService _ISalePriceGroupService { get { return IoC.Resolve<ISalePriceGroupService>(); } }
        public IGoodsServicePurchaseService _IGoodsServicePurchaseService { get { return IoC.Resolve<IGoodsServicePurchaseService>(); } }
        public ITypeService _ITypeService { get { return IoC.Resolve<ITypeService>(); } }
        public IContractStateService _IContractStateService { get { return IoC.Resolve<IContractStateService>(); } }
        public IOPAccountService _IOPAccountService { get { return IoC.Resolve<IOPAccountService>(); } }
        public IOPMaterialGoodsService _IOPMaterialGoodsService { get { return IoC.Resolve<IOPMaterialGoodsService>(); } }
        public ITypeGroupService _ITypeGroupService { get { return IoC.Resolve<ITypeGroupService>(); } }
        public IGenCodeService _IGenCodeService { get { return IoC.Resolve<IGenCodeService>(); } }
        public IInvoiceTypeService _IInvoiceTypeService { get { return IoC.Resolve<IInvoiceTypeService>(); } }
        public ISaleDiscountPolicyService _ISaleDiscountPolicyService { get { return IoC.Resolve<ISaleDiscountPolicyService>(); } }
        public IMaterialGoodsAssemblyService _IMaterialGoodsAssemblyService { get { return IoC.Resolve<IMaterialGoodsAssemblyService>(); } }
        public IAutoPrincipleService _IAutoPrincipleService { get { return IoC.Resolve<IAutoPrincipleService>(); } }
        public IMaterialGoodsSpecialTaxGroupService _IMaterialGoodsSpecialTaxGroupService { get { return IoC.Resolve<IMaterialGoodsSpecialTaxGroupService>(); } }
        public IMaterialGoodsService _IMaterialGoodsService { get { return IoC.Resolve<IMaterialGoodsService>(); } }
        #endregion

        List<ExcelSheets> listReturn = new List<ExcelSheets>();
        private String[] tempStrings = new String[100];
        public FImportData()
        {
            InitializeComponent();
            uProcessBar.Appearance.BackColor = Color.Gray;
            uProcessBar.Appearance.BackColor2 = Color.White;
            uProcessBar.Appearance.BackGradientStyle = GradientStyle.HorizontalBump;
            uProcessBar.Appearance.ForeColor = Color.DarkRed;

            uProcessBar.FillAppearance.BackColor = Color.SpringGreen;
            uProcessBar.FillAppearance.BackColor2 = Color.White;
            uProcessBar.FillAppearance.BackGradientStyle = GradientStyle.HorizontalBump;
            uProcessBar.FillAppearance.ForeColor = Color.DarkRed;
            uProcessBar.Minimum = 0;
            uProcessBar.Value = 0;
            uProcessBar.Step = 1;
            uProcessBar.PercentFormat = "P1";

            uLblTable.Text = "Waiting..";
            List<ExcelSheets> excelSheetses = new List<ExcelSheets>();
            uGridSelect.DataSource = excelSheetses;
            Utils.ConfigGrid(uGridSelect, ConstDatabase.ImportData_TableName, false);
        }

        private void btnReadExcel_Click(object sender, EventArgs e)
        {
            var xlApp = new Microsoft.Office.Interop.Excel.Application();
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Workbook excelBook = xlApp.Workbooks.Open(txtExcelFilePath.Tag.ToString());

            String[] excelSheets = new String[excelBook.Worksheets.Count];

            int i = 0;
            foreach (Worksheet wSheet in excelBook.Worksheets)
            {
                excelSheets[i] = wSheet.Name;
                Marshal.ReleaseComObject(wSheet);
                i++;
            }
            tempStrings = excelSheets;
            excelBook.Close(false, System.Type.Missing, System.Type.Missing);

            xlApp.Quit();
            GC.Collect();
            Marshal.ReleaseComObject(excelBook);
            Marshal.ReleaseComObject(xlApp);
            List<ExcelSheets> a = ConvertExcelSheets(excelSheets);
            uGridSelect.DataSource = a;
            Utils.ConfigGrid(uGridSelect, ConstDatabase.ImportData_TableName, false);
        }
        private void txtPdfFilePath_MouseClick(object sender, MouseEventArgs e)
        {
            FileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = @"Excel Files (*.xls, *.xlsx,... )|*.xls;*.xlsx;*.xlm;*.xlsm;*.xlsb;*.xlw";
            fileDialog.ShowDialog(this);
            if (!string.IsNullOrEmpty(fileDialog.FileName))
            {
                txtExcelFilePath.Text = fileDialog.FileName;
                txtExcelFilePath.Tag = fileDialog.FileName;
            }
            btnReadExcel.Enabled = true;
            btnApply.Enabled = true;
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void btnImport_Click(object sender, EventArgs e)
        {
            string connString;
            if (null == txtExcelFilePath.Tag)
                txtPdfFilePath_MouseClick(sender, null);
            var filePath = txtExcelFilePath.Tag.ToString().Replace("\\", "/");

            //cat duong dan
            var fileParts = filePath.Split('.');

            if (filePath.Length > 1 && fileParts[1] == "xls")//excel 2003
                connString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=Excel 8.0");
            else//excel 2007
            {
                connString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=Excel 12.0");
            }
            OleDbConnection oledbConn = new OleDbConnection(connString);

            //Mở kết nối
            oledbConn.Open();
            String[] excelSheets = new String[100];
            if (listReturn.Count > 0)
            {
                excelSheets = new String[listReturn.Count];

                int i = 0;
                foreach (var item in listReturn)
                {
                    excelSheets[i] = item.ExcelSheetsName;
                    i++;
                }
            }
            else
            {
                excelSheets = tempStrings;
            }
            //Duyệt các sheet và cập nhật dữ liệu
            for (int j = 0; j < excelSheets.Length; j++)
            {
                OleDbCommand cmd = new OleDbCommand("SELECT * FROM[" + excelSheets[j] + "$]", oledbConn);
                System.Data.DataTable results = new System.Data.DataTable();
                OleDbDataAdapter adapter = new OleDbDataAdapter(cmd);
                adapter.Fill(results);
                bool flag;

                #region Nhập dữ liệu vào DB

                switch (excelSheets[j])
                {

                    #region Account

                    case "Account":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<Account> listAcc = Convert_FromDataTable_ToList<Account>(results);
                        List<Account> listAccDb = _IAccountService.GetAll();

                        uProcessBar.Maximum = listAcc.Count;
                        foreach (var account in listAcc)
                        {
                            flag = listAccDb.Any(accountDb => accountDb.AccountNumber == account.AccountNumber);
                            UpdateData(account, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region AccountGroup

                    case "AccountGroup":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<AccountGroup> listAccGroup = Convert_FromDataTable_ToList<AccountGroup>(results);
                        List<AccountGroup> listAccGroupDb = _IAccountGroupService.GetAll();
                        uProcessBar.Maximum = listAccGroup.Count;
                        foreach (var accGroup in listAccGroup)
                        {
                            flag = listAccGroupDb.Any(accountgroupDb => accountgroupDb.ID == accGroup.ID);
                            UpdateData(accGroup, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region AccountingObjectGroup

                    case "AccountingObjectGroup":

                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<AccountingObjectGroup> listAccoutingObjectGroup =
                            Convert_FromDataTable_ToList<AccountingObjectGroup>(results);
                        List<AccountingObjectGroup> listAccoutingObjectGroupDb =
                            _IAccountingObjectGroupService.GetAll();
                        uProcessBar.Maximum = listAccoutingObjectGroup.Count;
                        foreach (var accountingObjectGroup in listAccoutingObjectGroup)
                        {
                            flag =
                                listAccoutingObjectGroupDb.Any(
                                    accountingObjGroup =>
                                        accountingObjGroup.AccountingObjectGroupCode ==
                                        accountingObjectGroup.AccountingObjectGroupCode);
                            UpdateData(accountingObjectGroup, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region AccountingObjectCategory

                    case "AccountingObjectCategory":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<AccountingObjectCategory> listAccoutingObjectCategory =
                            Convert_FromDataTable_ToList<AccountingObjectCategory>(results);
                        List<AccountingObjectCategory> listAccoutingObjectCategoryDb =
                            _IAccountingObjectCategoryService.GetAll();
                        uProcessBar.Maximum = listAccoutingObjectCategory.Count;
                        foreach (var accountingObjectCategory in listAccoutingObjectCategory)
                        {
                            flag =
                                listAccoutingObjectCategoryDb.Any(
                                    aoc =>
                                        aoc.AccountingObjectCategoryCode ==
                                        accountingObjectCategory.AccountingObjectCategoryCode);
                            UpdateData(accountingObjectCategory, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region AccountingObject

                    case "AccountingObject":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<AccountingObject> listAccountingObject =
                            Convert_FromDataTable_ToList<AccountingObject>(results);
                        List<AccountingObject> listAccountingObjectDb = _IAccountingObjectService.GetAll();
                        uProcessBar.Maximum = listAccountingObject.Count;
                        foreach (var accountingObject in listAccountingObject)
                        {
                            flag =
                                listAccountingObjectDb.Any(
                                    accObjDb =>
                                        accObjDb.AccountingObjectCode == accountingObject.AccountingObjectCode);
                            UpdateData(accountingObject, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region AccountTransfer

                    case "AccountTransfer":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<AccountTransfer> listAccountTransfer =
                            Convert_FromDataTable_ToList<AccountTransfer>(results);
                        List<AccountTransfer> listAccountTransferDb = _IAccountTransferService.GetAll();
                        uProcessBar.Maximum = listAccountTransfer.Count;
                        foreach (var accTransfer in listAccountTransfer)
                        {
                            flag =
                                listAccountTransferDb.Any(
                                    at => at.AccountTransferCode == accTransfer.AccountTransferCode);
                            UpdateData(accTransfer, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region AccountDefault

                    case "AccountDefault":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<AccountDefault> listAccountDefault =
                            Convert_FromDataTable_ToList<AccountDefault>(results);
                        List<AccountDefault> listAccountDefaultDb = _IAccountDefaultService.GetAll();
                        uProcessBar.Maximum = listAccountDefault.Count;
                        foreach (var accDefault in listAccountDefault)
                        {
                            flag = listAccountDefaultDb.Any(ad => ad.ID == accDefault.ID);
                            UpdateData(accDefault, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region AutoPrinciple

                    case "AutoPrinciple":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<AutoPrinciple> listAutoPrinciple = Convert_FromDataTable_ToList<AutoPrinciple>(results);
                        List<AutoPrinciple> listAutoPrincipleDb = _IAutoPrincipleService.GetAll();
                        uProcessBar.Maximum = listAutoPrinciple.Count;
                        foreach (var autoPrinciple in listAutoPrinciple)
                        {
                            flag =
                                listAutoPrincipleDb.Any(
                                    ap => ap.AutoPrincipleName == autoPrinciple.AutoPrincipleName);
                            UpdateData(autoPrinciple, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region Bank

                    case "Bank":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<Bank> listBank = Convert_FromDataTable_ToList<Bank>(results);
                        List<Bank> listBankDb = _IBankService.GetAll();
                        uProcessBar.Maximum = listBank.Count;
                        foreach (var bank in listBank)
                        {
                            flag = listBankDb.Any(ba => ba.BankCode == bank.BankCode);
                            UpdateData(bank, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region BankAccountDetail

                    case "BankAccountDetail":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<BankAccountDetail> listBankAccountDetail =
                            Convert_FromDataTable_ToList<BankAccountDetail>(results);
                        List<BankAccountDetail> listBankAccountDetailDb = _IBankAccountDetailService.GetAll();
                        uProcessBar.Maximum = listBankAccountDetail.Count;
                        foreach (var bankAccDetail in listBankAccountDetail)
                        {
                            flag = listBankAccountDetailDb.Any(baD => baD.BankAccount == bankAccDetail.BankAccount);
                            UpdateData(bankAccDetail, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region BudgetItem

                    case "BudgetItem":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<BudgetItem> listBudgetItem = Convert_FromDataTable_ToList<BudgetItem>(results);
                        List<BudgetItem> listBudgetItemDb = _IBudgetItemService.GetAll();
                        uProcessBar.Maximum = listBudgetItem.Count;
                        foreach (var budgetItem in listBudgetItem)
                        {
                            flag = listBudgetItemDb.Any(bi => bi.BudgetItemCode == budgetItem.BudgetItemCode);
                            UpdateData(budgetItem, flag);
                        }
                        break;

                    #endregion

                    #region CostSet

                    case "CostSet":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<CostSet> listCostSet = Convert_FromDataTable_ToList<CostSet>(results);
                        List<CostSet> listCostSetDb = _ICostSetService.GetAll();
                        uProcessBar.Maximum = listCostSet.Count;
                        foreach (var costSet in listCostSet)
                        {
                            flag = listCostSetDb.Any(cs => cs.CostSetCode == costSet.CostSetCode);
                            UpdateData(costSet, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region Currency

                    case "Currency":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<Currency> listCurrency = Convert_FromDataTable_ToList<Currency>(results);
                        List<Currency> listCurrencyDb = _ICurrencyService.GetAll();
                        uProcessBar.Maximum = listCurrency.Count;
                        foreach (var currency in listCurrency)
                        {
                            flag = listCurrencyDb.Any(cur => cur.ID == currency.ID);
                            UpdateData(currency, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region CreditCard

                    case "CreditCard":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<CreditCard> listCreditCard = Convert_FromDataTable_ToList<CreditCard>(results);
                        List<CreditCard> listCreditCardDb = _ICreditCardService.GetAll();
                        uProcessBar.Maximum = listCreditCard.Count;
                        foreach (var creditCard in listCreditCard)
                        {
                            flag = listCreditCardDb.Any(cc => cc.CreditCardNumber == creditCard.CreditCardNumber);
                            UpdateData(creditCard, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region ContractState

                    case "ContractState":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<ContractState> listContractState = Convert_FromDataTable_ToList<ContractState>(results);
                        ;
                        List<ContractState> listContractStateDb = _IContractStateService.GetAll();
                        uProcessBar.Maximum = listContractState.Count;
                        foreach (var contractState in listContractState)
                        {
                            flag =
                                listContractStateDb.Any(
                                    cs => cs.ContractStateCode == contractState.ContractStateCode);
                            UpdateData(contractState, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region Department

                    case "Department":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<Department> listDepart = Convert_FromDataTable_ToList<Department>(results);
                        List<Department> listDepartDb = _IDepartmentService.GetAll();
                        uProcessBar.Maximum = listDepart.Count;
                        foreach (var department in listDepart)
                        {
                            flag = listDepartDb.Any(depart => depart.DepartmentCode == department.DepartmentCode);
                            UpdateData(department, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region ExpenseItem

                    case "ExpenseItem":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<ExpenseItem> listExpenseItem = Convert_FromDataTable_ToList<ExpenseItem>(results);
                        List<ExpenseItem> listExpenseItemDb = _IExpenseItemService.GetAll();
                        uProcessBar.Maximum = listExpenseItem.Count;
                        foreach (var expenseItem in listExpenseItem)
                        {
                            flag = listExpenseItemDb.Any(ex => ex.ExpenseItemCode == expenseItem.ExpenseItemCode);
                            UpdateData(expenseItem, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region FixedAssetCategory

                    case "FixedAssetCategory":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<FixedAssetCategory> listFixedAssetCategory =
                            Convert_FromDataTable_ToList<FixedAssetCategory>(results);
                        List<FixedAssetCategory> listFixedAssetCategoryDb = _IFixedAssetCategoryService.GetAll();
                        uProcessBar.Maximum = listFixedAssetCategory.Count;
                        foreach (var fixedAssetCategory in listFixedAssetCategory)
                        {
                            flag =
                                listFixedAssetCategoryDb.Any(
                                    fc => fc.FixedAssetCategoryCode == fixedAssetCategory.FixedAssetCategoryCode);
                            UpdateData(fixedAssetCategory, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region FixedAsset

                    case "FixedAsset":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<FixedAsset> listFixedAsset = Convert_FromDataTable_ToList<FixedAsset>(results);
                        List<FixedAsset> listFixedAssetDb = _IFixedAssetService.GetAll();
                        uProcessBar.Maximum = listFixedAsset.Count;
                        foreach (var fixedAsset in listFixedAsset)
                        {
                            flag = listFixedAssetDb.Any(fa => fa.FixedAssetCode == fixedAsset.FixedAssetCode);
                            UpdateData(fixedAsset, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region FixedAssetDetail

                    case "FixedAssetDetail":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<FixedAssetDetail> listFixedAssetDetail =
                            Convert_FromDataTable_ToList<FixedAssetDetail>(results);
                        List<FixedAssetDetail> listFixedAssetDetailDb = _IFixedAssetDetailService.GetAll();
                        uProcessBar.Maximum = listFixedAssetDetail.Count;
                        foreach (var fixedAssetDetail in listFixedAssetDetail)
                        {
                            flag = listFixedAssetDetailDb.Any(fad => fad.ID == fixedAssetDetail.ID);
                            UpdateData(fixedAssetDetail, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region GoodsServicePurchase

                    case "GoodsServicePurchase":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<GoodsServicePurchase> listGoodsServicePurchase =
                            Convert_FromDataTable_ToList<GoodsServicePurchase>(results);
                        List<GoodsServicePurchase> listGoodsServicePurchaseDb =
                            _IGoodsServicePurchaseService.GetAll();
                        uProcessBar.Maximum = listGoodsServicePurchase.Count;
                        foreach (var goodsServicePurchase in listGoodsServicePurchase)
                        {
                            flag =
                                listGoodsServicePurchaseDb.Any(
                                    gp =>
                                        gp.GoodsServicePurchaseCode == goodsServicePurchase.GoodsServicePurchaseCode);
                            UpdateData(goodsServicePurchase, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region Gencode

                    case "Gencode":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<GenCode> listGencode = Convert_FromDataTable_ToList<GenCode>(results);
                        List<GenCode> listGencodeDb = _IGenCodeService.GetAll();
                        uProcessBar.Maximum = listGencode.Count;
                        foreach (var genCode in listGencode)
                        {
                            flag = listGencodeDb.Any(gc => gc.TypeGroupName == genCode.TypeGroupName);
                            UpdateData(genCode, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region InvestorGroup

                    case "InvestorGroup":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<InvestorGroup> listInvestorGroup = Convert_FromDataTable_ToList<InvestorGroup>(results);
                        List<InvestorGroup> listInvestorGroupDb = _IInvestorGroupService.GetAll();
                        uProcessBar.Maximum = listInvestorGroup.Count;
                        foreach (var investorGroup in listInvestorGroup)
                        {
                            flag =
                                listInvestorGroupDb.Any(
                                    ig => ig.InvestorGroupCode == investorGroup.InvestorGroupCode);
                            UpdateData(investorGroup, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region Investor

                    case "Investor":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<Investor> listInvestor = Convert_FromDataTable_ToList<Investor>(results);
                        List<Investor> listInvestorDb = _IInvestorService.GetAll();
                        uProcessBar.Maximum = listInvestor.Count;
                        foreach (var investor in listInvestor)
                        {
                            flag = listInvestorDb.Any(iv => iv.InvestorCode == investor.InvestorCode);
                            UpdateData(investor, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region InvoiceType

                    case "InvoiceType":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<InvoiceType> listInvoiceType = Convert_FromDataTable_ToList<InvoiceType>(results);
                        List<InvoiceType> listInvoiceTypeDb = _IInvoiceTypeService.GetAll();
                        uProcessBar.Maximum = listInvoiceType.Count;
                        foreach (var invoiceType in listInvoiceType)
                        {
                            flag = listInvoiceTypeDb.Any(it => it.InvoiceTypeCode == invoiceType.InvoiceTypeCode);
                            UpdateData(invoiceType, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region MaterialGoodsCategory

                    case "MaterialGoodsCategory":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<MaterialGoodsCategory> listMaterialGoodsCategory =
                            Convert_FromDataTable_ToList<MaterialGoodsCategory>(results);
                        List<MaterialGoodsCategory> listMaterialGoodsCategoryDb =
                            _IMaterialGoodsCategoryService.GetAll();
                        uProcessBar.Maximum = listMaterialGoodsCategory.Count;
                        foreach (var materialGoodsCategory in listMaterialGoodsCategory)
                        {
                            flag =
                                listMaterialGoodsCategoryDb.Any(
                                    mg =>
                                        mg.MaterialGoodsCategoryCode ==
                                        materialGoodsCategory.MaterialGoodsCategoryCode);
                            UpdateData(materialGoodsCategory, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region MaterialQuantum

                    case "MaterialQuantum":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<MaterialQuantum> listMaterialQuantum =
                            Convert_FromDataTable_ToList<MaterialQuantum>(results);
                        List<MaterialQuantum> listMaterialQuantumDb = _IMaterialQuantumService.GetAll();
                        uProcessBar.Maximum = listMaterialQuantum.Count;
                        foreach (var materialQuantum in listMaterialQuantum)
                        {
                            flag =
                                listMaterialQuantumDb.Any(
                                    mq => mq.MaterialQuantumCode == materialQuantum.MaterialQuantumCode);
                            UpdateData(materialQuantum, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region MaterialQuantumDetail

                    case "MaterialQuantumDetail":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<MaterialQuantumDetail> listMaterialQuantumDetail =
                            Convert_FromDataTable_ToList<MaterialQuantumDetail>(results);
                        List<MaterialQuantumDetail> listMaterialQuantumDetailDb =
                            _IMaterialQuantumDetailService.GetAll();
                        uProcessBar.Maximum = listMaterialQuantumDetail.Count;
                        foreach (var materialQuantumDetail in listMaterialQuantumDetail)
                        {
                            flag = listMaterialQuantumDetailDb.Any(mq => mq.ID == materialQuantumDetail.ID);
                            UpdateData(materialQuantumDetail, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region MaterialGoodsAssembly

                    case "MaterialGoodsAssembly":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<MaterialGoodsAssembly> listMaterialGoodsAssembly =
                            Convert_FromDataTable_ToList<MaterialGoodsAssembly>(results);
                        List<MaterialGoodsAssembly> listMaterialGoodsAssemblyDb =
                            _IMaterialGoodsAssemblyService.Query.ToList();
                        uProcessBar.Maximum = listMaterialGoodsAssembly.Count;
                        foreach (var materialGoodsAssembly in listMaterialGoodsAssembly)
                        {
                            flag = listMaterialGoodsAssemblyDb.Any(mg => mg.ID == materialGoodsAssembly.ID);
                            UpdateData(materialGoodsAssembly, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region MaterialGoodsSpecialTaxGroup

                    case "MaterialGoodsSpecialTaxGroup":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<MaterialGoodsSpecialTaxGroup> listMaterialGoodsSpecialTaxGroup =
                            Convert_FromDataTable_ToList<MaterialGoodsSpecialTaxGroup>(results);
                        List<MaterialGoodsSpecialTaxGroup> listMaterialGoodsSpecialTaxGroupDb =
                            _IMaterialGoodsSpecialTaxGroupService.GetAll();
                        uProcessBar.Maximum = listMaterialGoodsSpecialTaxGroup.Count;
                        foreach (var materialGoodsSpecialTaxGroup in listMaterialGoodsSpecialTaxGroup)
                        {
                            flag =
                                listMaterialGoodsSpecialTaxGroupDb.Any(
                                    mg =>
                                        mg.MaterialGoodsSpecialTaxGroupCode ==
                                        materialGoodsSpecialTaxGroup.MaterialGoodsSpecialTaxGroupCode);
                            UpdateData(materialGoodsSpecialTaxGroup, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region MaterialGoods

                    case "MaterialGoods":

                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<MaterialGoods> listMaterialGoods = Convert_FromDataTable_ToList<MaterialGoods>(results);
                        List<MaterialGoods> listMaterialGoodsDb = _IMaterialGoodsService.GetAll();
                        uProcessBar.Maximum = listMaterialGoods.Count;
                        foreach (var materialGoods in listMaterialGoods)
                        {
                            flag =
                                listMaterialGoodsDb.Any(
                                    mg => mg.MaterialGoodsCode == materialGoods.MaterialGoodsCode);
                            UpdateData(materialGoods, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region OPAccount

                    case "OPAccount":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<OPAccount> listOPAccount = Convert_FromDataTable_ToList<OPAccount>(results);
                        List<OPAccount> listOPAccountDb = _IOPAccountService.GetAll();
                        uProcessBar.Maximum = listOPAccount.Count;
                        foreach (var opAccount in listOPAccount)
                        {
                            flag = listOPAccountDb.Any(op => op.ID == opAccount.ID);
                            UpdateData(opAccount, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    //Gen Guid bằng tay

                    #region OPMaterialGoods

                    case "OPMaterialGoods":

                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<OPMaterialGoods> listOpMaterialGood =
                            Convert_FromDataTable_ToList<OPMaterialGoods>(results);
                        List<OPMaterialGoods> listOpMaterialGoodDb = _IOPMaterialGoodsService.GetAll();
                        uProcessBar.Maximum = listOpMaterialGood.Count;
                        foreach (var opMaterialGood in listOpMaterialGood)
                        {
                            flag = listOpMaterialGoodDb.Any(opM => opM.ID == opMaterialGood.ID);
                            UpdateData(opMaterialGood, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region PersonalSalaryTax

                    case "PersonalSalaryTax":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<PersonalSalaryTax> listPersonalSalaryTax =
                            Convert_FromDataTable_ToList<PersonalSalaryTax>(results);
                        List<PersonalSalaryTax> listPersonalSalaryTaxDb = _IPersonalSalaryTaxService.GetAll();
                        uProcessBar.Maximum = listPersonalSalaryTax.Count;
                        foreach (var perSaTax in listPersonalSalaryTax)
                        {
                            flag = listPersonalSalaryTaxDb.Any(ps => ps.ID == perSaTax.ID);
                            UpdateData(perSaTax, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region PaymentClause

                    case "PaymentClause":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<PaymentClause> listPaymentClause = Convert_FromDataTable_ToList<PaymentClause>(results);
                        List<PaymentClause> listPaymentClauseDb = _IPaymentClauseService.GetAll();
                        uProcessBar.Maximum = listPaymentClause.Count;
                        foreach (var paymentClause in listPaymentClause)
                        {
                            flag =
                                listPaymentClauseDb.Any(
                                    pc => pc.PaymentClauseCode == paymentClause.PaymentClauseCode);
                            UpdateData(paymentClause, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region Repository

                    case "Repository":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<Repository> ListRepository = Convert_FromDataTable_ToList<Repository>(results);
                        List<Repository> ListRepositoryDb = _IRepositoryService.GetAll();
                        uProcessBar.Maximum = ListRepository.Count;
                        foreach (var repository in ListRepository)
                        {
                            flag = ListRepositoryDb.Any(rs => rs.RepositoryCode == repository.RepositoryCode);
                            UpdateData(repository, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region RegistrationGroup

                    case "RegistrationGroup":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<RegistrationGroup> listRegistrationGroup =
                            Convert_FromDataTable_ToList<RegistrationGroup>(results);
                        List<RegistrationGroup> listRegistrationGroupDb = _IRegistrationGroupService.GetAll();
                        uProcessBar.Maximum = listRegistrationGroup.Count;
                        foreach (var registrationGroup in listRegistrationGroup)
                        {
                            flag =
                                listRegistrationGroupDb.Any(
                                    rg => rg.RegistrationGroupCode == registrationGroup.RegistrationGroupCode);
                            UpdateData(registrationGroup, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region StockCategory

                    case "StockCategory":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<StockCategory> listStockCategory = Convert_FromDataTable_ToList<StockCategory>(results);
                        List<StockCategory> listStockCategoryDb = _IStockCategoryService.GetAll();
                        uProcessBar.Maximum = listStockCategory.Count;
                        foreach (var stockCategory in listStockCategory)
                        {
                            flag =
                                listStockCategoryDb.Any(
                                    sc => sc.StockCategoryCode == stockCategory.StockCategoryCode);
                            UpdateData(stockCategory, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region ShareHolderGroup

                    case "ShareHolderGroup":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<ShareHolderGroup> listShareHolderGroup =
                            Convert_FromDataTable_ToList<ShareHolderGroup>(results);
                        List<ShareHolderGroup> listShareHolderGroupDb = _IShareHolderGroupService.GetAll();
                        uProcessBar.Maximum = listShareHolderGroup.Count;
                        foreach (var shareHolderGroup in listShareHolderGroup)
                        {
                            flag =
                                listShareHolderGroupDb.Any(
                                    sh => sh.ShareHolderGroupCode == shareHolderGroup.ShareHolderGroupCode);
                            UpdateData(shareHolderGroup, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region StatisticsCode

                    case "StatisticsCode":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<StatisticsCode> listStatisticsCode =
                            Convert_FromDataTable_ToList<StatisticsCode>(results);
                        List<StatisticsCode> listStatisticsCodeDb = _IStatisticsCodeService.GetAll();
                        uProcessBar.Maximum = listStatisticsCode.Count;
                        foreach (var statisticsCode in listStatisticsCode)
                        {
                            flag =
                                listStatisticsCodeDb.Any(sc => sc.StatisticsCode_ == statisticsCode.StatisticsCode_);
                            UpdateData(statisticsCode, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region SalePriceGroup

                    case "SalePriceGroup":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<SalePriceGroup> listSalePriceGroup =
                            Convert_FromDataTable_ToList<SalePriceGroup>(results);
                        List<SalePriceGroup> listSalePriceGroupDb = _ISalePriceGroupService.GetAll();
                        uProcessBar.Maximum = listSalePriceGroup.Count;
                        foreach (var salePriceGroup in listSalePriceGroup)
                        {
                            flag =
                                listSalePriceGroupDb.Any(
                                    sp => sp.SalePriceGroupCode == salePriceGroup.SalePriceGroupCode);
                            UpdateData(salePriceGroup, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region SaleDiscountPolicy

                    case "SaleDiscountPolicy":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<SaleDiscountPolicy> listSaleDiscountPolicy =
                            Convert_FromDataTable_ToList<SaleDiscountPolicy>(results);
                        List<SaleDiscountPolicy> listSaleDiscountPolicyDb = _ISaleDiscountPolicyService.GetAll();
                        uProcessBar.Maximum = listSaleDiscountPolicy.Count;
                        foreach (var saleDiscountPolicy in listSaleDiscountPolicy)
                        {
                            flag = listSaleDiscountPolicyDb.Any(sd => sd.ID == saleDiscountPolicy.ID);
                            UpdateData(saleDiscountPolicy, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region TimeSheetSymbols

                    case "TimeSheetSymbols":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<TimeSheetSymbols> ListTimeSheetSymbols =
                            Convert_FromDataTable_ToList<TimeSheetSymbols>(results);
                        List<TimeSheetSymbols> ListTimeSheetSymbolsDb = _ITimeSheetSymbolsService.GetAll();
                        uProcessBar.Maximum = ListTimeSheetSymbols.Count;
                        foreach (var timeSheetSymbols in ListTimeSheetSymbols)
                        {
                            flag =
                                ListTimeSheetSymbolsDb.Any(
                                    ts => ts.TimeSheetSymbolsCode == timeSheetSymbols.TimeSheetSymbolsCode);
                            UpdateData(timeSheetSymbols, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region TransportMethod

                    case "TransportMethod":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<TransportMethod> listTransportMethod =
                            Convert_FromDataTable_ToList<TransportMethod>(results);
                        List<TransportMethod> listTransportMethodDb = _ITransportMethodService.GetAll();
                        uProcessBar.Maximum = listTransportMethod.Count;
                        foreach (var transportMethod in listTransportMethod)
                        {
                            flag =
                                listTransportMethodDb.Any(
                                    tm => tm.TransportMethodCode == transportMethod.TransportMethodCode);
                            UpdateData(transportMethod, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    //Dữ liệu trong Excel bị thiếu

                    #region Type

                    case "Type":

                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<Accounting.Core.Domain.Type> listType =
                            Convert_FromDataTable_ToList<Accounting.Core.Domain.Type>(results);
                        ;
                        List<Accounting.Core.Domain.Type> listTypeDb = _ITypeService.GetAll();
                        uProcessBar.Maximum = listType.Count;
                        foreach (var type in listType)
                        {
                            flag = listTypeDb.Any(t => t.ID == type.ID);
                            UpdateData(type, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                    #region TypeGroup

                    case "TypeGroup":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<TypeGroup> listTypeGroup = Convert_FromDataTable_ToList<TypeGroup>(results);
                        List<TypeGroup> listTypeGroupDb = _ITypeGroupService.GetAll();
                        uProcessBar.Maximum = listTypeGroup.Count;
                        foreach (var typeGroup in listTypeGroup)
                        {
                            flag = listTypeGroupDb.Any(tg => tg.ID == typeGroup.ID);
                            UpdateData(typeGroup, flag);
                            uProcessBar.PerformStep();

                        }
                        break;

                    #endregion

                    #region TuyChon

                    case "TuyChon":
                        uLblTable.Text = excelSheets[j];
                        uLblTable.Refresh();
                        uProcessBar.Value = 0;
                        List<SystemOption> listSystemOptions = Convert_FromDataTable_ToList<SystemOption>(results);
                        foreach (var sysItem in listSystemOptions)
                        {
                            if (string.IsNullOrEmpty(sysItem.Data))
                            {
                                sysItem.Data = "";
                            }
                            if (string.IsNullOrEmpty(sysItem.DefaultData))
                            {
                                sysItem.DefaultData = "";
                            }
                            if(string.IsNullOrEmpty(sysItem.Name))
                            {
                                sysItem.Name = "";
                            }
                        }
                        List<SystemOption> listSystemOptionsDb = _ISystemOptionService.GetAll();
                        uProcessBar.Maximum = listSystemOptions.Count;
                        foreach (var systemOption in listSystemOptions)
                        {
                            flag = listSystemOptionsDb.Any(optionDb => optionDb.ID == systemOption.ID);
                            UpdateData(systemOption, flag);
                            uProcessBar.PerformStep();
                        }
                        break;

                    #endregion

                }

                #endregion

            }
            oledbConn.Close();
            MessageBox.Show("Chúc mừng bạn đã chuyển dữ liệu thành công", "Chúc mừng", MessageBoxButtons.OK,
                MessageBoxIcon.Information);

        }

        #region Utils
        private static List<ExcelSheets> ConvertExcelSheets(string[] input)
        {
            var listTemp = new List<ExcelSheets>();
            foreach (var s in input)
            {
                ExcelSheets temp = new ExcelSheets();
                temp.ExcelSheetsName = s;
                listTemp.Add(temp);
            }
            return listTemp;
        }
        private static Guid? FormatDataGuidIdAlowNull(string inputStr)
        {
            if (string.IsNullOrEmpty(inputStr))
                return null;
            return new Guid(inputStr);
        }
        private static int? FormatDataIntAlowNull(string inputStr)
        {
            if (string.IsNullOrEmpty(inputStr))
                return null;
            return Convert.ToInt32(inputStr);
        }
        private static bool? FormatDataBoolAlowNull(string inputStr)
        {
            if (string.IsNullOrEmpty(inputStr))
                return null;
            return inputStr == "1";
        }
        private static DateTime? FormatDataDateTimeAlowNull(string inputStr)
        {
            if (!string.IsNullOrEmpty(inputStr))
            {
                if (CheckValidDateTime(inputStr))
                {
                    return Convert.ToDateTime(inputStr);
                }
                return null;
            }
            //return Convert.ToDateTime(inputStr);
            return null;
        }
        private static decimal? FormatDataDecimalAlowNull(string inputStr)
        {
            if (string.IsNullOrEmpty(inputStr))
                return null;
            return Convert.ToDecimal(inputStr);
        }
        public static bool CheckValidDateTime(string inputStr)
        {
            string[] formats = {"M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt", 
                   "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss", 
                   "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt", 
                   "M/d/yyyy h:mm", "M/d/yyyy h:mm", 
                   "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm"};
            DateTime dateValue;
            if (DateTime.TryParseExact(inputStr, formats, new CultureInfo("en-US"), DateTimeStyles.None, out dateValue))
                return true;
            return false;
        }
        public static List<T> Convert_FromDataTable_ToList<T>(System.Data.DataTable dt)
        {
            List<T> lst = new List<T>();
            System.Type elementType = typeof(T);
            PropertyInfo[] propertyInfo = elementType.GetProperties();
            List<DataColumn> dc = dt.Columns.Cast<DataColumn>().ToList();
            foreach (DataRow drRow in dt.Rows)
            {
                if (drRow.ItemArray[0].ToString() != "" || drRow.ItemArray[1].ToString() != "" || drRow.ItemArray[2].ToString() != "")
                {
                    T cn = (T)Activator.CreateInstance(elementType);
                    foreach (PropertyInfo propItem in propertyInfo)
                    {
                        if (dc.Any(c => c.ColumnName == propItem.Name))
                        {
                            DataColumn dcColumn = dc.Find(c => c.ColumnName == propItem.Name);
                            if (dcColumn != null && !string.IsNullOrEmpty(drRow[propItem.Name].ToString()))
                            {
                                System.Type t = System.Type.GetType(propItem.PropertyType.FullName);
                                if (t != null)
                                {
                                    if (t == typeof(Guid) || (t == typeof(Guid?)))
                                    {
                                        propItem.SetValue(cn, FormatDataGuidIdAlowNull(drRow[propItem.Name].ToString()), null);
                                    }
                                    else if (t == typeof(Decimal) || t == typeof(Decimal?))
                                    {
                                        propItem.SetValue(cn, FormatDataDecimalAlowNull(drRow[propItem.Name].ToString()), null);
                                    }
                                    else if (t == typeof(Boolean) || t == typeof(Boolean?))
                                    {
                                        propItem.SetValue(cn, FormatDataBoolAlowNull(drRow[propItem.Name].ToString()), null);
                                    }
                                    else if (t == typeof(DateTime) || t == typeof(DateTime?))
                                    {
                                        propItem.SetValue(cn, FormatDataDateTimeAlowNull(drRow[propItem.Name].ToString()), null);
                                    }
                                    else if (t == typeof(Int32) || t == typeof(Int32?))
                                    {
                                        propItem.SetValue(cn, FormatDataIntAlowNull(drRow[propItem.Name].ToString()), null);
                                    }
                                    else
                                    {
                                        propItem.SetValue(cn, Convert.ChangeType(drRow[propItem.Name], t), null);
                                    }

                                }

                            }
                        }
                    }
                    lst.Add(cn);
                }
            }
            return lst;
        }
        public T GetAgainObject<T>(T objectInput) where T : new()
        {
            T temp = new T();
            PropertyInfo propRecord = objectInput.GetType()
                    .GetProperty("ID", BindingFlags.Public | BindingFlags.Instance);
            if (null != propRecord && propRecord.CanWrite && propRecord.PropertyType.Name == "Guid")
            {
                Guid tempGuid = (Guid)propRecord.GetValue(objectInput, null);
                BaseService<T, Guid> testInvoke = this.GetIService((T)Activator.CreateInstance(typeof(T)));
                temp = testInvoke.Getbykey(tempGuid);
            }
            else if (null != propRecord && propRecord.CanWrite && propRecord.PropertyType.Name == "String")
            {
                String tempString = (String)propRecord.GetValue(objectInput, null);
                BaseService<T, String> testInvoke = this.GetIServiceString((T)Activator.CreateInstance(typeof(T)));
                temp = testInvoke.Getbykey(tempString);
            }
            else if (null != propRecord && propRecord.CanWrite && propRecord.PropertyType.Name == "Int32")
            {
                int tempString = (int)propRecord.GetValue(objectInput, null);
                BaseService<T, int> testInvoke = this.GetIServiceInt((T)Activator.CreateInstance(typeof(T)));
                temp = testInvoke.Getbykey(tempString);
            }
            System.Type elementType = typeof(T);
            PropertyInfo[] propertyInfo = elementType.GetProperties();
            foreach (PropertyInfo propItem in propertyInfo)
            {
                PropertyInfo propRecordTemp = objectInput.GetType().GetProperty(propItem.Name, BindingFlags.Public | BindingFlags.Instance);
                if (null != propRecord && propRecord.CanWrite)
                {
                    try
                    {
                        propItem.SetValue(temp, propRecordTemp.GetValue(objectInput, null), null);
                    }
                    catch (Exceptions ex)
                    {
                        throw ex;
                    }

                }
            }
            return temp;
        }
        public void UpdateData<T>(T objectInput, bool flag) where T : new()
        {
            T temp = new T();
            PropertyInfo propRecord = objectInput.GetType()
                    .GetProperty("ID", BindingFlags.Public | BindingFlags.Instance);
            if (null != propRecord && propRecord.CanWrite && propRecord.PropertyType.Name == "Guid")
            {
                BaseService<T, Guid> testInvoke = this.GetIService((T)Activator.CreateInstance(typeof(T)));
                try
                {
                    testInvoke.BeginTran();
                    if (flag)
                    {
                        T objectUpdate = GetAgainObject(objectInput);
                        testInvoke.Update(objectUpdate);
                    }
                    else
                    {
                        testInvoke.CreateNew(objectInput);
                    }
                }
                catch (Exception exception)
                {
                    testInvoke.RolbackTran();
                    MessageBox.Show(exception.ToString(), "Thông báo lỗi cập nhật dữ liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                testInvoke.CommitTran();
            }
            else if (null != propRecord && propRecord.CanWrite && propRecord.PropertyType.Name == "String")
            {
                BaseService<T, String> testInvoke = this.GetIServiceString((T)Activator.CreateInstance(typeof(T)));
                try
                {
                    testInvoke.BeginTran();
                    if (flag)
                    {
                        T objectUpdate = GetAgainObject(objectInput);
                        testInvoke.Update(objectUpdate);
                    }
                    else
                    {
                        testInvoke.CreateNew(objectInput);
                    }
                }
                catch (Exception exception)
                {
                    testInvoke.RolbackTran();
                    MessageBox.Show(exception.ToString(), "Thông báo lỗi cập nhật dữ liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                testInvoke.CommitTran();
            }
            else if (null != propRecord && propRecord.CanWrite && propRecord.PropertyType.Name == "Int32")
            {
                BaseService<T, int> testInvoke = this.GetIServiceInt((T)Activator.CreateInstance(typeof(T)));
                try
                {
                    testInvoke.BeginTran();
                    if (flag)
                    {
                        T objectUpdate = GetAgainObject(objectInput);
                        testInvoke.Update(objectUpdate);
                    }
                    else
                    {
                        testInvoke.CreateNew(objectInput);
                    }
                }
                catch (Exception exception)
                {
                    testInvoke.RolbackTran();
                    MessageBox.Show(exception.ToString(), "Thông báo lỗi cập nhật dữ liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                testInvoke.CommitTran();
            }

        }
        #endregion

        private void uGridSelect_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            uGridSelect.UpdateData();
            //uGridSelect.Update();
            var lstNo = uGridSelect.DataSource as List<ExcelSheets>;
            if (lstNo != null) listReturn = lstNo.Where(t => t.Checkbox).ToList();
        }
    }

    public class ExcelSheets
    {
        private bool checkbox;
        private string excelSheetsName;
        private string excelSheetsNameVietNamese;

        public virtual bool Checkbox
        {
            get { return checkbox; }
            set { checkbox = value; }
        }
        public virtual string ExcelSheetsName
        {
            get { return excelSheetsName; }
            set { excelSheetsName = value; }
        }
        public virtual string ExcelSheetsNameVietNamese
        {
            get { return excelSheetsNameVietNamese; }
            set { excelSheetsNameVietNamese = value; }
        }
    }
}
