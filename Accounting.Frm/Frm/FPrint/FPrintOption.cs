﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

#endregion

namespace Accounting
{
    public partial class FPrintOption : Form
    {
        private List<VoucherPatternsReport> _lstVoucherPatternsReports1 = new List<VoucherPatternsReport>();
        private VoucherPatternsReport vc= new VoucherPatternsReport();
        public VoucherPatternsReport VcRa
        {
            get { return vc; }
        }
        public bool isClose = false;
        public FPrintOption(int typeID)
        {
            InitializeComponent();
            _lstVoucherPatternsReports1 = IVoucherPatternsReportService.Query.Where(r=>r.ListTypeID.Contains(typeID.ToString())).ToList();
            try
            {
                if (_lstVoucherPatternsReports1.Count < 0) return;
                uGrid_DanhSachChungTuIn.SetDataBinding(_lstVoucherPatternsReports1, "");
                Utils.ConfigGrid(uGrid_DanhSachChungTuIn,ConstDatabase.VoucherPatternsReport_TableName);
                uGrid_DanhSachChungTuIn.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            }
            catch (Exception ex)
            {
                string s = ex.StackTrace;
                if (s == "") throw;
            }
        }

        public static IVoucherPatternsReportService IVoucherPatternsReportService
        {
            get { return IoC.Resolve<IVoucherPatternsReportService>(); }
        }

        private void uGrid_DanhSachChungTuIn_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                vc = (VoucherPatternsReport)uGrid_DanhSachChungTuIn.Selected.Rows[0].ListObject;
                isClose = true;
                Close();
            }
            catch(Exception ex)
            {
                MSG.Information("Không có mẫu chứng từ.");
            }
        }

        private void btDong_Click(object sender, EventArgs e)
        {
            isClose = false;
            Close();
        }

        private void btThuHien_Click(object sender, EventArgs e)
        {
            if(uGrid_DanhSachChungTuIn.Selected.Rows.Count > 0)
            {
                vc = (VoucherPatternsReport)uGrid_DanhSachChungTuIn.Selected.Rows[0].ListObject;
            }
            isClose = true;
            Close();
        }
    }
}