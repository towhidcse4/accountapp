﻿namespace Accounting
{
    partial class FPrintOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uGrid_DanhSachChungTuIn = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.btThuHien = new Infragistics.Win.Misc.UltraButton();
            this.btDong = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid_DanhSachChungTuIn)).BeginInit();
            this.SuspendLayout();
            // 
            // uGrid_DanhSachChungTuIn
            // 
            this.uGrid_DanhSachChungTuIn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uGrid_DanhSachChungTuIn.Location = new System.Drawing.Point(12, 12);
            this.uGrid_DanhSachChungTuIn.Name = "uGrid_DanhSachChungTuIn";
            this.uGrid_DanhSachChungTuIn.Size = new System.Drawing.Size(415, 288);
            this.uGrid_DanhSachChungTuIn.TabIndex = 7;
            this.uGrid_DanhSachChungTuIn.Text = "Chọn mẫu chứng từ cần in";
            this.uGrid_DanhSachChungTuIn.DoubleClick += new System.EventHandler(this.uGrid_DanhSachChungTuIn_DoubleClick);
            // 
            // btThuHien
            // 
            this.btThuHien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btThuHien.Location = new System.Drawing.Point(217, 306);
            this.btThuHien.Name = "btThuHien";
            this.btThuHien.Size = new System.Drawing.Size(129, 23);
            this.btThuHien.TabIndex = 13;
            this.btThuHien.Text = "Xem bản in";
            this.btThuHien.Click += new System.EventHandler(this.btThuHien_Click);
            // 
            // btDong
            // 
            this.btDong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btDong.Location = new System.Drawing.Point(352, 306);
            this.btDong.MaximumSize = new System.Drawing.Size(75, 23);
            this.btDong.MinimumSize = new System.Drawing.Size(75, 23);
            this.btDong.Name = "btDong";
            this.btDong.Size = new System.Drawing.Size(75, 23);
            this.btDong.TabIndex = 14;
            this.btDong.Text = "Đóng";
            this.btDong.Click += new System.EventHandler(this.btDong_Click);
            // 
            // FPrintfOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 341);
            this.Controls.Add(this.uGrid_DanhSachChungTuIn);
            this.Controls.Add(this.btDong);
            this.Controls.Add(this.btThuHien);
            this.MaximumSize = new System.Drawing.Size(455, 380);
            this.MinimumSize = new System.Drawing.Size(455, 380);
            this.Name = "FPrintfOption";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tùy chọn mẫu chứng từ cần in";
            ((System.ComponentModel.ISupportInitialize)(this.uGrid_DanhSachChungTuIn)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btThuHien;
        private Infragistics.Win.Misc.UltraButton btDong;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid_DanhSachChungTuIn;

    }
}