﻿namespace Accounting
{
    partial class FTest3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinToolbars.RibbonTab ribbonTab1 = new Infragistics.Win.UltraWinToolbars.RibbonTab("ribbon1");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup1 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool1 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ButtonTool1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool2 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ButtonTool1");
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridGeneralLedger = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridRepositoryLedger = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridFixedAssetLedger = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGridTypeIDandGeneralLedger = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraOptionSet_Ex1 = new Accounting.UltraOptionSet_Ex();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnRefresh = new Infragistics.Win.Misc.UltraButton();
            this._FTest3_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.ultraToolbarsManager1 = new Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(this.components);
            this._FTest3_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FTest3_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FTest3_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridGeneralLedger)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridRepositoryLedger)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridFixedAssetLedger)).BeginInit();
            this.ultraTabPageControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridTypeIDandGeneralLedger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet_Ex1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGridGeneralLedger);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(830, 386);
            // 
            // uGridGeneralLedger
            // 
            this.uGridGeneralLedger.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.WithinBand;
            this.uGridGeneralLedger.DisplayLayout.Override.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Contains;
            this.uGridGeneralLedger.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridGeneralLedger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridGeneralLedger.Location = new System.Drawing.Point(0, 0);
            this.uGridGeneralLedger.Name = "uGridGeneralLedger";
            this.uGridGeneralLedger.Size = new System.Drawing.Size(830, 386);
            this.uGridGeneralLedger.TabIndex = 0;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridRepositoryLedger);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(814, 216);
            // 
            // uGridRepositoryLedger
            // 
            this.uGridRepositoryLedger.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.WithinBand;
            this.uGridRepositoryLedger.DisplayLayout.Override.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Contains;
            this.uGridRepositoryLedger.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridRepositoryLedger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridRepositoryLedger.Location = new System.Drawing.Point(0, 0);
            this.uGridRepositoryLedger.Name = "uGridRepositoryLedger";
            this.uGridRepositoryLedger.Size = new System.Drawing.Size(814, 216);
            this.uGridRepositoryLedger.TabIndex = 0;
            this.uGridRepositoryLedger.Text = "ultraGrid2";
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.uGridFixedAssetLedger);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(814, 216);
            // 
            // uGridFixedAssetLedger
            // 
            this.uGridFixedAssetLedger.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.WithinBand;
            this.uGridFixedAssetLedger.DisplayLayout.Override.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Contains;
            this.uGridFixedAssetLedger.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridFixedAssetLedger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridFixedAssetLedger.Location = new System.Drawing.Point(0, 0);
            this.uGridFixedAssetLedger.Name = "uGridFixedAssetLedger";
            this.uGridFixedAssetLedger.Size = new System.Drawing.Size(814, 216);
            this.uGridFixedAssetLedger.TabIndex = 1;
            this.uGridFixedAssetLedger.Text = "ultraGrid2";
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.ultraGridTypeIDandGeneralLedger);
            this.ultraTabPageControl4.Controls.Add(this.ultraGroupBox2);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(814, 216);
            // 
            // ultraGridTypeIDandGeneralLedger
            // 
            this.ultraGridTypeIDandGeneralLedger.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.WithinBand;
            this.ultraGridTypeIDandGeneralLedger.DisplayLayout.Override.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Contains;
            this.ultraGridTypeIDandGeneralLedger.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.ultraGridTypeIDandGeneralLedger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGridTypeIDandGeneralLedger.Location = new System.Drawing.Point(0, 0);
            this.ultraGridTypeIDandGeneralLedger.Name = "ultraGridTypeIDandGeneralLedger";
            this.ultraGridTypeIDandGeneralLedger.Size = new System.Drawing.Size(659, 216);
            this.ultraGridTypeIDandGeneralLedger.TabIndex = 1;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.ultraOptionSet_Ex1);
            this.ultraGroupBox2.Controls.Add(this.ultraButton1);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.ultraGroupBox2.Location = new System.Drawing.Point(659, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(155, 216);
            this.ultraGroupBox2.TabIndex = 2;
            this.ultraGroupBox2.Text = "Hỗ trợ";
            // 
            // ultraOptionSet_Ex1
            // 
            valueListItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem1.DataValue = "0";
            valueListItem1.DisplayText = "Mở hết";
            valueListItem2.DataValue = "1";
            valueListItem2.DisplayText = "Đóng hết";
            this.ultraOptionSet_Ex1.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.ultraOptionSet_Ex1.Location = new System.Drawing.Point(6, 68);
            this.ultraOptionSet_Ex1.Name = "ultraOptionSet_Ex1";
            this.ultraOptionSet_Ex1.ReadOnly = false;
            this.ultraOptionSet_Ex1.Size = new System.Drawing.Size(138, 36);
            this.ultraOptionSet_Ex1.TabIndex = 1;
            this.ultraOptionSet_Ex1.ValueChanged += new System.EventHandler(this.ultraOptionSet_Ex1_ValueChanged);
            // 
            // ultraButton1
            // 
            this.ultraButton1.Location = new System.Drawing.Point(6, 19);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(143, 43);
            this.ultraButton1.TabIndex = 0;
            this.ultraButton1.Text = "Chỉ xem loại chứng từ có phát sinh ghi sổ";
            this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(830, 386);
            // 
            // ultraTabControl1
            // 
            appearance1.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            appearance1.BorderColor = System.Drawing.Color.DodgerBlue;
            appearance1.FontData.BoldAsString = "True";
            this.ultraTabControl1.Appearance = appearance1;
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl3);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl4);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 49);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(832, 409);
            this.ultraTabControl1.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon;
            this.ultraTabControl1.TabIndex = 12;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Sổ cái";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Sổ kho";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "Sổ tài sản cố định";
            ultraTab4.TabPage = this.ultraTabPageControl4;
            ultraTab4.Text = "Sổ cái kèm chứng từ";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3,
            ultraTab4});
            this.ultraTabControl1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.btnRefresh);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(832, 49);
            this.ultraGroupBox1.TabIndex = 1;
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnRefresh
            // 
            this.btnRefresh.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnRefresh.Location = new System.Drawing.Point(6, 9);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(182, 32);
            this.btnRefresh.TabIndex = 0;
            this.btnRefresh.Text = "Làm mới";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // _FTest3_Toolbars_Dock_Area_Right
            // 
            this._FTest3_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FTest3_Toolbars_Dock_Area_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._FTest3_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right;
            this._FTest3_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.SystemColors.ControlText;
            this._FTest3_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(832, 0);
            this._FTest3_Toolbars_Dock_Area_Right.Name = "_FTest3_Toolbars_Dock_Area_Right";
            this._FTest3_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(0, 458);
            this._FTest3_Toolbars_Dock_Area_Right.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // ultraToolbarsManager1
            // 
            this.ultraToolbarsManager1.DesignerFlags = 1;
            this.ultraToolbarsManager1.DockWithinContainer = this;
            this.ultraToolbarsManager1.DockWithinContainerBaseType = typeof(System.Windows.Forms.Form);
            ribbonTab1.Caption = "ribbon1";
            ribbonGroup1.Caption = "ribbonGroup1";
            ribbonGroup1.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool1});
            ribbonTab1.Groups.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonGroup[] {
            ribbonGroup1});
            this.ultraToolbarsManager1.Ribbon.NonInheritedRibbonTabs.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonTab[] {
            ribbonTab1});
            this.ultraToolbarsManager1.Ribbon.Visible = true;
            this.ultraToolbarsManager1.SettingsKey = "";
            buttonTool2.SharedPropsInternal.Caption = "ButtonTool1";
            this.ultraToolbarsManager1.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool2});
            // 
            // _FTest3_Toolbars_Dock_Area_Left
            // 
            this._FTest3_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FTest3_Toolbars_Dock_Area_Left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._FTest3_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
            this._FTest3_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.SystemColors.ControlText;
            this._FTest3_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 0);
            this._FTest3_Toolbars_Dock_Area_Left.Name = "_FTest3_Toolbars_Dock_Area_Left";
            this._FTest3_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(0, 458);
            this._FTest3_Toolbars_Dock_Area_Left.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // _FTest3_Toolbars_Dock_Area_Bottom
            // 
            this._FTest3_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FTest3_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._FTest3_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom;
            this._FTest3_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.SystemColors.ControlText;
            this._FTest3_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 458);
            this._FTest3_Toolbars_Dock_Area_Bottom.Name = "_FTest3_Toolbars_Dock_Area_Bottom";
            this._FTest3_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(832, 0);
            this._FTest3_Toolbars_Dock_Area_Bottom.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // _FTest3_Toolbars_Dock_Area_Top
            // 
            this._FTest3_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FTest3_Toolbars_Dock_Area_Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._FTest3_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top;
            this._FTest3_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.SystemColors.ControlText;
            this._FTest3_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
            this._FTest3_Toolbars_Dock_Area_Top.Name = "_FTest3_Toolbars_Dock_Area_Top";
            this._FTest3_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(832, 0);
            this._FTest3_Toolbars_Dock_Area_Top.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // FTest3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(832, 458);
            this.Controls.Add(this.ultraTabControl1);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this._FTest3_Toolbars_Dock_Area_Left);
            this.Controls.Add(this._FTest3_Toolbars_Dock_Area_Right);
            this.Controls.Add(this._FTest3_Toolbars_Dock_Area_Bottom);
            this.Controls.Add(this._FTest3_Toolbars_Dock_Area_Top);
            this.Name = "FTest3";
            this.Text = "FTest3";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridGeneralLedger)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridRepositoryLedger)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridFixedAssetLedger)).EndInit();
            this.ultraTabPageControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridTypeIDandGeneralLedger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet_Ex1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridRepositoryLedger;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridFixedAssetLedger;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraButton btnRefresh;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridGeneralLedger;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGridTypeIDandGeneralLedger;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private UltraOptionSet_Ex ultraOptionSet_Ex1;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FTest3_Toolbars_Dock_Area_Right;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsManager ultraToolbarsManager1;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FTest3_Toolbars_Dock_Area_Left;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FTest3_Toolbars_Dock_Area_Bottom;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FTest3_Toolbars_Dock_Area_Top;

    }
}