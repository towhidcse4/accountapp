﻿namespace Accounting
{
    partial class FTest4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup ultraExplorerBarGroup1 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup();
            this.ultraExplorerBar1 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar();
            ((System.ComponentModel.ISupportInitialize)(this.ultraExplorerBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraExplorerBar1
            // 
            this.ultraExplorerBar1.Dock = System.Windows.Forms.DockStyle.Left;
            ultraExplorerBarGroup1.Text = "New Group";
            this.ultraExplorerBar1.Groups.AddRange(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup[] {
            ultraExplorerBarGroup1});
            this.ultraExplorerBar1.Location = new System.Drawing.Point(0, 0);
            this.ultraExplorerBar1.Name = "ultraExplorerBar1";
            this.ultraExplorerBar1.NavigationPaneExpandedState = Infragistics.Win.UltraWinExplorerBar.NavigationPaneExpandedState.Collapsed;
            this.ultraExplorerBar1.NavigationPaneExpansionMode = ((Infragistics.Win.UltraWinExplorerBar.NavigationPaneExpansionMode)((Infragistics.Win.UltraWinExplorerBar.NavigationPaneExpansionMode.OnButtonClick | Infragistics.Win.UltraWinExplorerBar.NavigationPaneExpansionMode.OnSizeChanged)));
            this.ultraExplorerBar1.Size = new System.Drawing.Size(40, 350);
            this.ultraExplorerBar1.Style = Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarStyle.OutlookNavigationPane;
            this.ultraExplorerBar1.TabIndex = 0;
            // 
            // FTest4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 350);
            this.Controls.Add(this.ultraExplorerBar1);
            this.Name = "FTest4";
            this.Text = "FTest4";
            ((System.ComponentModel.ISupportInitialize)(this.ultraExplorerBar1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar ultraExplorerBar1;
    }
}