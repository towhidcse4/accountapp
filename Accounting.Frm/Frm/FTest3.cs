﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using ColumnStyle = System.Windows.Forms.ColumnStyle;
using Type = Accounting.Core.Domain.Type;

namespace Accounting
{
    public partial class FTest3 : Form
    {
        List<GeneralLedger> generalLedger = new List<GeneralLedger>();
        private List<Core.Domain.Type> listType = new List<Type>();
        List<RepositoryLedger> repositoryLedger = new List<RepositoryLedger>();
        List<FixedAssetLedger> fixedAssetLedger = new List<FixedAssetLedger>();
        public FTest3()
        {
            InitializeComponent();
            generalLedger = Utils.IGeneralLedgerService.GetAll();
            repositoryLedger = Utils.IRepositoryLedgerService.GetAll();
            fixedAssetLedger = Utils.IFixedAssetLedgerService.GetAll();

            uGridGeneralLedger.DataSource = generalLedger;
            uGridRepositoryLedger.DataSource = repositoryLedger;
            uGridFixedAssetLedger.DataSource = fixedAssetLedger;

            listType = Utils.ITypeService.GetAll();
            DataSet ds = new DataSet();
            ds = Core.Utils.Convert_Parent_And_Child_List_To_DataSet(listType, generalLedger, "ID", "TypeID");
            ultraGridTypeIDandGeneralLedger.DataSource = ds;
            ultraOptionSet_Ex1.CheckedItem = ultraOptionSet_Ex1.Items[0];
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            generalLedger = Utils.IGeneralLedgerService.GetAll();
            Utils.IGeneralLedgerService.UnbindSession(generalLedger);
            generalLedger = Utils.IGeneralLedgerService.GetAll();
            repositoryLedger = Utils.IRepositoryLedgerService.GetAll();
            Utils.IRepositoryLedgerService.UnbindSession(repositoryLedger);
            repositoryLedger = Utils.IRepositoryLedgerService.GetAll();
            fixedAssetLedger = Utils.IFixedAssetLedgerService.GetAll();
            Utils.IFixedAssetLedgerService.UnbindSession(fixedAssetLedger);
            fixedAssetLedger = Utils.IFixedAssetLedgerService.GetAll();
            Utils.ITypeService.UnbindSession(listType);
            listType = Utils.ITypeService.GetAll();

            DataSet ds = new DataSet();
            ds = Core.Utils.Convert_Parent_And_Child_List_To_DataSet(listType, generalLedger, "ID", "TypeID");
            ultraGridTypeIDandGeneralLedger.DataSource = ds;
            ConfigGrid(ultraGridTypeIDandGeneralLedger, ConstDatabase.Type_TableName, null, 0);
            ConfigGrid(ultraGridTypeIDandGeneralLedger, ConstDatabase.GeneralLedger_TableName, null, 1);
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            var t = (from ledger in generalLedger group ledger by ledger.TypeID into g select new { g.Key }).ToList();
            listType = listType.Where(c => t.Any(d => d.Key == c.ID)).ToList();
            DataSet ds = new DataSet();
            ds = Core.Utils.Convert_Parent_And_Child_List_To_DataSet(listType, generalLedger, "ID", "TypeID");
            ultraGridTypeIDandGeneralLedger.DataSource = ds;
            ConfigGrid(ultraGridTypeIDandGeneralLedger, ConstDatabase.Type_TableName, null, 0);
            ConfigGrid(ultraGridTypeIDandGeneralLedger, ConstDatabase.GeneralLedger_TableName, null, 1);

        }

        private void ultraOptionSet_Ex1_ValueChanged(object sender, EventArgs e)
        {
            if (ultraOptionSet_Ex1.CheckedIndex == 0)
            {
                ultraGridTypeIDandGeneralLedger.Rows.ExpandAll(true);
            }
            else if (ultraOptionSet_Ex1.CheckedIndex == 1)
            {
                ultraGridTypeIDandGeneralLedger.Rows.CollapseAll(true);
            }
        }

        public void ConfigGrid(UltraGrid utralGrid, string nameTable, IList<TemplateColumn> configGui, int i)  //, bool isRowSelect = false
        {
            configGui = configGui ?? new List<TemplateColumn>();

            //Convert configGUI to dic
            var dicconfigGui = new Dictionary<string, TemplateColumn>();
            if (configGui.Count > 0)
            {
                foreach (var item in configGui)
                {
                    if (!dicconfigGui.ContainsKey(item.ColumnName)) dicconfigGui.Add(item.ColumnName, item);
                    else
                    {
                        Clipboard.SetText(item.ColumnName);
                        throw new Exception(string.Format("Tên cột {0} bị trùng khi cấu hình [ {1} ] Đề nghị xem lại", item.ColumnName, utralGrid.Name));
                    }
                }
            }

            #region setting ResourceCustomizer
            Infragistics.Shared.ResourceCustomizer rc = Infragistics.Win.UltraWinGrid.Resources.Customizer;
            rc.SetCustomizedString("RowFilterDropDownCustomItem", "(Tùy chọn)");
            rc.SetCustomizedString("RowFilterDropDownBlanksItem", "(Trống)");
            rc.SetCustomizedString("RowFilterDropDownNonBlanksItem", "(Không trống)");
            rc.SetCustomizedString("RowFilterDropDownEquals", "Bằng");
            rc.SetCustomizedString("RowFilterDropDownNotEquals", "Khác");
            rc.SetCustomizedString("RowFilterDropDownLessThan", "Nhỏ hơn");
            rc.SetCustomizedString("RowFilterDropDownLessThanOrEqualTo", "Nhỏ hơn hoặc bằng");
            rc.SetCustomizedString("RowFilterDropDownGreaterThan", "Lớn hơn");
            rc.SetCustomizedString("RowFilterDropDownGreaterThanOrEqualTo", "Lớn hơn hoặc bằng");
            rc.SetCustomizedString("RowFilterDropDownMatch", "Thỏa mãn biểu thức");
            rc.SetCustomizedString("RowFilterDropDownLike", "Giống");
            rc.SetCustomizedString("RowFilterDropDown_Operator_StartsWith", "Bắt đầu với");
            rc.SetCustomizedString("RowFilterDropDown_Operator_Contains", "Chứa");
            rc.SetCustomizedString("RowFilterDropDown_Operator_EndsWith", "Kết thúc với");
            rc.SetCustomizedString("RowFilterDropDown_Operator_DoesNotStartWith", "Không bắt đầu với");
            rc.SetCustomizedString("RowFilterDropDown_Operator_DoesNotContain", "Không chứa");
            rc.SetCustomizedString("RowFilterDropDown_Operator_DoesNotEndWith", "Không kết thúc với");
            rc.SetCustomizedString("RowFilterDropDown_Operator_DoesNotMatch", "Không chứa(chuỗi)");
            rc.SetCustomizedString("RowFilterDropDown_Operator_NotLike", "Không giống");
            #endregion

            //Tiêu đề
            utralGrid.Text = nameTable;
            utralGrid.DisplayLayout.UseFixedHeaders = true;
            utralGrid.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;
            utralGrid.DisplayLayout.Scrollbars = Scrollbars.Automatic;
            utralGrid.DisplayLayout.MaxColScrollRegions = 1;
            utralGrid.DisplayLayout.MaxRowScrollRegions = 1;
            utralGrid.DisplayLayout.ViewStyle = ViewStyle.MultiBand;
            UltraGridBand band = utralGrid.DisplayLayout.Bands[i];

            //Border các ô các dòng
            //band.Override.BorderStyleRow = UIElementBorderStyle.WindowsVista;
            //band.Override.BorderStyleCell = UIElementBorderStyle.WindowsVista;


            ////Tiêu đề cột
            string headerCaptionFirst = string.Empty;
            var dicVisiblePosition = new Dictionary<int, int>();   //[vị trí trong list band.Columns] - [vị trí VisiblePosition]
            var templateColumns = new List<TemplateColumn>();
            foreach (var item in band.Columns)
            {
                //Set màu Header
                item.Header.Appearance.BorderAlpha = Alpha.Transparent;
                if (dicconfigGui.Count > 0 ? dicconfigGui.ContainsKey(item.Key) : ConstDatabase.TablesInDatabase[nameTable].ContainsKey(item.Key))
                {
                    TemplateColumn temp = dicconfigGui.Count > 0 ? dicconfigGui[item.Key] : ConstDatabase.TablesInDatabase[nameTable][item.Key];
                    templateColumns.Add(temp);
                    item.Header.Caption = temp.ColumnCaption;
                    item.Header.ToolTipText = temp.ColumnToolTip;
                    //item.Header.Enabled = !temp.IsReadOnly;
                    item.Header.Fixed = temp.IsColumnHeader;
                    item.CellActivation = temp.IsReadOnly ? Activation.NoEdit : Activation.AllowEdit;
                    item.Width = temp.ColumnWidth == -1 ? item.Width : temp.ColumnWidth;
                    item.MaxWidth = temp.ColumnMaxWidth == -1 ? item.MaxWidth : temp.ColumnMaxWidth;
                    item.MinWidth = temp.ColumnMinWidth == -1 ? item.MinWidth : temp.ColumnMinWidth;
                    item.Hidden = !temp.IsVisible;
                    if (item.Hidden) continue; //(đến đoạn xét vị trí thì bỏ qua nếu colum đó là colum ẩn => chỉ xét vị trí cho những colum hiển thị)
                    int vitri = temp.VisiblePosition == -1 ? item.Header.VisiblePosition : temp.VisiblePosition;
                    dicVisiblePosition.Add(vitri, item.Index);
                }
                else
                    item.Hidden = true;
                if (item.Key.Contains("Date") && !item.Key.Equals("FinalDate"))
                {
                    item.Format = Constants.DdMMyyyy;
                    item.MaskInput = @"{LOC}dd/mm/yyyy";
                    item.CellAppearance.TextHAlign = HAlign.Center;
                    item.CellAppearance.TextVAlign = VAlign.Middle;
                }
            }
            //xét vị trí xuất hiện cho các cột hiển thị (Việc gán vị trí phải được lần lượt từ vị trí đầu tiên đến vị trí cuối cùng do đó ta phải sắp xếp lại vị trí hiển thị rồi mới set lần lượt)
            foreach (var item in (from pair in dicVisiblePosition orderby pair.Key ascending select pair)) band.Columns[item.Value].Header.VisiblePosition = item.Key;


            //Tính tổng hàng
            //List<TemplateColumn> templateColumns = dicconfigGui.Count > 0 ? dicconfigGui.Values.ToList() : ConstDatabase.TablesInDatabase[nameTable].Values.ToList();
            var list = templateColumns.Where(p => p.IsVisible).OrderBy(p => p.VisiblePosition).ToList();
            if (list.Count > 0)
            {
                var firstOrDefault = list.FirstOrDefault();
                if (firstOrDefault != null) headerCaptionFirst = firstOrDefault.ColumnName;
                utralGrid.DisplayLayout.Override.AllowRowSummaries = AllowRowSummaries.Default; //Ẩn ký hiệu tổng trên Header Caption
                if (band.Summaries.Count != 0) band.Summaries.Clear();
            }

            utralGrid.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            utralGrid.DisplayLayout.Bands[0].SummaryFooterCaption = @"Số dòng dữ liệu: ";
            utralGrid.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
            utralGrid.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False;   //ẩn
            utralGrid.DisplayLayout.Override.CellAppearance.TextTrimming = TextTrimming.EllipsisWord;
        }

    }
}
