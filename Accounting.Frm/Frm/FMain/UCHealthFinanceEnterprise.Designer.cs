﻿namespace Accounting.Frm.FMain
{
    partial class UCHealthFinanceEnterprise
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            this.ultraPanelHealthFELeft = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanelHealthFETop = new Infragistics.Win.Misc.UltraPanel();
            this.btnOk = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.lblBeginDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblEndDate = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDateTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dtBeginDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanelHealthFERight = new Infragistics.Win.Misc.UltraPanel();
            this.cbbKyVeBieuDo = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.chartHealthFinance = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.ultraPanelHealthFERightBottom = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.txtTySoNo = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtTySoTuTaiTro = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtVonLuanChuyen = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtHSThanhToanNganHan = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtHSThanhToanNhanh = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtHSThanhToanTucThoi = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtHSThanhToanChung = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtHSQuayVongHangTonKho = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtHSLoiNhuanVonKD = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtHSLoiNhuanDoanhThuThuan = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtHSLoiNhuanVonChuSoHuu = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtTienMat = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTienGui = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDoanhThu = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtChiPhi = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtLoiNhuanTruocThue = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtPhaiThu = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtPhaiTra = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtHangTonKho = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraPanelHealthFELeft.ClientArea.SuspendLayout();
            this.ultraPanelHealthFELeft.SuspendLayout();
            this.ultraPanelHealthFETop.ClientArea.SuspendLayout();
            this.ultraPanelHealthFETop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).BeginInit();
            this.ultraPanelHealthFERight.ClientArea.SuspendLayout();
            this.ultraPanelHealthFERight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbKyVeBieuDo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartHealthFinance)).BeginInit();
            this.ultraPanelHealthFERightBottom.ClientArea.SuspendLayout();
            this.ultraPanelHealthFERightBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVonLuanChuyen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienMat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienGui)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDoanhThu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiPhi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoiNhuanTruocThue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhaiThu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhaiTra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHangTonKho)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraPanelHealthFELeft
            // 
            // 
            // ultraPanelHealthFELeft.ClientArea
            // 
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.txtVonLuanChuyen);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.txtHSLoiNhuanVonChuSoHuu);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.txtHSLoiNhuanDoanhThuThuan);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.txtHSLoiNhuanVonKD);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.txtHSQuayVongHangTonKho);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.txtHSThanhToanChung);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.txtHSThanhToanTucThoi);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.txtHSThanhToanNhanh);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.txtHSThanhToanNganHan);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.txtTySoTuTaiTro);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.txtTySoNo);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.ultraPanelHealthFETop);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.ultraLabel12);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.ultraLabel11);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.ultraLabel10);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.ultraLabel9);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.ultraLabel8);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.ultraLabel7);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.ultraLabel6);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.ultraLabel5);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.ultraLabel4);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.ultraLabel3);
            this.ultraPanelHealthFELeft.ClientArea.Controls.Add(this.ultraLabel2);
            this.ultraPanelHealthFELeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.ultraPanelHealthFELeft.Location = new System.Drawing.Point(0, 0);
            this.ultraPanelHealthFELeft.Name = "ultraPanelHealthFELeft";
            this.ultraPanelHealthFELeft.Size = new System.Drawing.Size(411, 416);
            this.ultraPanelHealthFELeft.TabIndex = 1;
            // 
            // ultraPanelHealthFETop
            // 
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.Control;
            this.ultraPanelHealthFETop.Appearance = appearance12;
            // 
            // ultraPanelHealthFETop.ClientArea
            // 
            this.ultraPanelHealthFETop.ClientArea.Controls.Add(this.btnOk);
            this.ultraPanelHealthFETop.ClientArea.Controls.Add(this.ultraLabel1);
            this.ultraPanelHealthFETop.ClientArea.Controls.Add(this.lblBeginDate);
            this.ultraPanelHealthFETop.ClientArea.Controls.Add(this.lblEndDate);
            this.ultraPanelHealthFETop.ClientArea.Controls.Add(this.cbbDateTime);
            this.ultraPanelHealthFETop.ClientArea.Controls.Add(this.dtBeginDate);
            this.ultraPanelHealthFETop.ClientArea.Controls.Add(this.dtEndDate);
            this.ultraPanelHealthFETop.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanelHealthFETop.Location = new System.Drawing.Point(0, 0);
            this.ultraPanelHealthFETop.Name = "ultraPanelHealthFETop";
            this.ultraPanelHealthFETop.Size = new System.Drawing.Size(411, 60);
            this.ultraPanelHealthFETop.TabIndex = 58;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(326, 27);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(79, 23);
            this.btnOk.TabIndex = 58;
            this.btnOk.Text = "Lấy dữ liệu";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // ultraLabel1
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextHAlignAsString = "Left";
            appearance13.TextVAlignAsString = "Bottom";
            this.ultraLabel1.Appearance = appearance13;
            this.ultraLabel1.Location = new System.Drawing.Point(9, 6);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(32, 17);
            this.ultraLabel1.TabIndex = 54;
            this.ultraLabel1.Text = "Kỳ";
            // 
            // lblBeginDate
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextHAlignAsString = "Left";
            appearance14.TextVAlignAsString = "Bottom";
            this.lblBeginDate.Appearance = appearance14;
            this.lblBeginDate.Location = new System.Drawing.Point(109, 6);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.Size = new System.Drawing.Size(55, 17);
            this.lblBeginDate.TabIndex = 49;
            this.lblBeginDate.Text = "Từ ngày";
            // 
            // lblEndDate
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextHAlignAsString = "Left";
            appearance15.TextVAlignAsString = "Bottom";
            this.lblEndDate.Appearance = appearance15;
            this.lblEndDate.Location = new System.Drawing.Point(221, 4);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(61, 17);
            this.lblEndDate.TabIndex = 50;
            this.lblEndDate.Text = "Đến ngày";
            // 
            // cbbDateTime
            // 
            this.cbbDateTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDateTime.Location = new System.Drawing.Point(9, 29);
            this.cbbDateTime.Name = "cbbDateTime";
            this.cbbDateTime.NullText = "<chọn dữ liệu>";
            this.cbbDateTime.Size = new System.Drawing.Size(85, 22);
            this.cbbDateTime.TabIndex = 51;
            // 
            // dtBeginDate
            // 
            appearance16.TextHAlignAsString = "Center";
            appearance16.TextVAlignAsString = "Middle";
            this.dtBeginDate.Appearance = appearance16;
            this.dtBeginDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtBeginDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtBeginDate.Location = new System.Drawing.Point(109, 29);
            this.dtBeginDate.MaskInput = "";
            this.dtBeginDate.Name = "dtBeginDate";
            this.dtBeginDate.Size = new System.Drawing.Size(99, 21);
            this.dtBeginDate.TabIndex = 52;
            this.dtBeginDate.Value = null;
            // 
            // dtEndDate
            // 
            appearance17.TextHAlignAsString = "Center";
            appearance17.TextVAlignAsString = "Middle";
            this.dtEndDate.Appearance = appearance17;
            this.dtEndDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtEndDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtEndDate.Location = new System.Drawing.Point(221, 29);
            this.dtEndDate.MaskInput = "";
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.Size = new System.Drawing.Size(99, 21);
            this.dtEndDate.TabIndex = 53;
            this.dtEndDate.Value = null;
            // 
            // ultraLabel12
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextHAlignAsString = "Left";
            appearance18.TextVAlignAsString = "Bottom";
            this.ultraLabel12.Appearance = appearance18;
            this.ultraLabel12.Location = new System.Drawing.Point(13, 391);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(224, 17);
            this.ultraLabel12.TabIndex = 56;
            this.ultraLabel12.Text = "Hệ số lợi nhuận / Vốn chủ sở hữu";
            // 
            // ultraLabel11
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.TextHAlignAsString = "Left";
            appearance19.TextVAlignAsString = "Bottom";
            this.ultraLabel11.Appearance = appearance19;
            this.ultraLabel11.Location = new System.Drawing.Point(13, 359);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(224, 17);
            this.ultraLabel11.TabIndex = 56;
            this.ultraLabel11.Text = "Hệ số lợi nhuận / Doanh thu thuần";
            // 
            // ultraLabel10
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            appearance20.TextHAlignAsString = "Left";
            appearance20.TextVAlignAsString = "Bottom";
            this.ultraLabel10.Appearance = appearance20;
            this.ultraLabel10.Location = new System.Drawing.Point(13, 327);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(224, 17);
            this.ultraLabel10.TabIndex = 56;
            this.ultraLabel10.Text = "Hệ số lợi nhuận / Vốn kinh doanh";
            // 
            // ultraLabel9
            // 
            appearance21.BackColor = System.Drawing.Color.Transparent;
            appearance21.TextHAlignAsString = "Left";
            appearance21.TextVAlignAsString = "Bottom";
            this.ultraLabel9.Appearance = appearance21;
            this.ultraLabel9.Location = new System.Drawing.Point(13, 295);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(224, 17);
            this.ultraLabel9.TabIndex = 56;
            this.ultraLabel9.Text = "Hệ số quay vòng hàng tồn kho";
            // 
            // ultraLabel8
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            appearance22.TextHAlignAsString = "Left";
            appearance22.TextVAlignAsString = "Bottom";
            this.ultraLabel8.Appearance = appearance22;
            this.ultraLabel8.Location = new System.Drawing.Point(13, 263);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(224, 17);
            this.ultraLabel8.TabIndex = 56;
            this.ultraLabel8.Text = "Hệ số thanh toán chung";
            // 
            // ultraLabel7
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            appearance23.TextHAlignAsString = "Left";
            appearance23.TextVAlignAsString = "Bottom";
            this.ultraLabel7.Appearance = appearance23;
            this.ultraLabel7.Location = new System.Drawing.Point(13, 231);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(224, 17);
            this.ultraLabel7.TabIndex = 56;
            this.ultraLabel7.Text = "Hệ số thanh toán tức thời";
            // 
            // ultraLabel6
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            appearance24.TextHAlignAsString = "Left";
            appearance24.TextVAlignAsString = "Bottom";
            this.ultraLabel6.Appearance = appearance24;
            this.ultraLabel6.Location = new System.Drawing.Point(13, 199);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(224, 17);
            this.ultraLabel6.TabIndex = 56;
            this.ultraLabel6.Text = "Hệ số thanh toán nhanh";
            // 
            // ultraLabel5
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            appearance25.TextHAlignAsString = "Left";
            appearance25.TextVAlignAsString = "Bottom";
            this.ultraLabel5.Appearance = appearance25;
            this.ultraLabel5.Location = new System.Drawing.Point(13, 167);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(224, 17);
            this.ultraLabel5.TabIndex = 56;
            this.ultraLabel5.Text = "Hệ số thanh toán ngắn hạn";
            // 
            // ultraLabel4
            // 
            appearance26.BackColor = System.Drawing.Color.Transparent;
            appearance26.TextHAlignAsString = "Left";
            appearance26.TextVAlignAsString = "Bottom";
            this.ultraLabel4.Appearance = appearance26;
            this.ultraLabel4.Location = new System.Drawing.Point(13, 135);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(224, 17);
            this.ultraLabel4.TabIndex = 56;
            this.ultraLabel4.Text = "Vốn luân chuyển";
            // 
            // ultraLabel3
            // 
            appearance27.BackColor = System.Drawing.Color.Transparent;
            appearance27.TextHAlignAsString = "Left";
            appearance27.TextVAlignAsString = "Bottom";
            this.ultraLabel3.Appearance = appearance27;
            this.ultraLabel3.Location = new System.Drawing.Point(13, 103);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(224, 17);
            this.ultraLabel3.TabIndex = 56;
            this.ultraLabel3.Text = "Tỷ số tự tài trợ";
            // 
            // ultraLabel2
            // 
            appearance28.BackColor = System.Drawing.Color.Transparent;
            appearance28.TextHAlignAsString = "Left";
            appearance28.TextVAlignAsString = "Bottom";
            this.ultraLabel2.Appearance = appearance28;
            this.ultraLabel2.Location = new System.Drawing.Point(13, 71);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(175, 17);
            this.ultraLabel2.TabIndex = 55;
            this.ultraLabel2.Text = "Tỷ số nợ";
            // 
            // ultraPanelHealthFERight
            // 
            appearance29.BackColor = System.Drawing.Color.Transparent;
            appearance29.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraPanelHealthFERight.Appearance = appearance29;
            // 
            // ultraPanelHealthFERight.ClientArea
            // 
            this.ultraPanelHealthFERight.ClientArea.Controls.Add(this.cbbKyVeBieuDo);
            this.ultraPanelHealthFERight.ClientArea.Controls.Add(this.ultraLabel21);
            this.ultraPanelHealthFERight.ClientArea.Controls.Add(this.chartHealthFinance);
            this.ultraPanelHealthFERight.ClientArea.Controls.Add(this.ultraPanelHealthFERightBottom);
            this.ultraPanelHealthFERight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanelHealthFERight.Location = new System.Drawing.Point(411, 0);
            this.ultraPanelHealthFERight.Name = "ultraPanelHealthFERight";
            this.ultraPanelHealthFERight.Size = new System.Drawing.Size(482, 416);
            this.ultraPanelHealthFERight.TabIndex = 2;
            // 
            // cbbKyVeBieuDo
            // 
            this.cbbKyVeBieuDo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbKyVeBieuDo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbKyVeBieuDo.Location = new System.Drawing.Point(374, 29);
            this.cbbKyVeBieuDo.Name = "cbbKyVeBieuDo";
            this.cbbKyVeBieuDo.NullText = "<chọn dữ liệu>";
            this.cbbKyVeBieuDo.Size = new System.Drawing.Size(98, 22);
            this.cbbKyVeBieuDo.TabIndex = 62;
            // 
            // ultraLabel21
            // 
            this.ultraLabel21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance30.BackColor = System.Drawing.Color.White;
            appearance30.TextHAlignAsString = "Left";
            appearance30.TextVAlignAsString = "Bottom";
            this.ultraLabel21.Appearance = appearance30;
            this.ultraLabel21.Location = new System.Drawing.Point(374, 6);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(78, 17);
            this.ultraLabel21.TabIndex = 61;
            this.ultraLabel21.Text = "Kỳ vẽ biểu đồ";
            // 
            // chartHealthFinance
            // 
            chartArea1.AxisX.MajorGrid.Enabled = false;
            chartArea1.AxisY.MajorGrid.Enabled = false;
            chartArea1.Name = "ChartArea1";
            this.chartHealthFinance.ChartAreas.Add(chartArea1);
            this.chartHealthFinance.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Alignment = System.Drawing.StringAlignment.Center;
            legend1.Name = "Legend1";
            this.chartHealthFinance.Legends.Add(legend1);
            this.chartHealthFinance.Location = new System.Drawing.Point(0, 0);
            this.chartHealthFinance.Name = "chartHealthFinance";
            series1.ChartArea = "ChartArea1";
            series1.IsValueShownAsLabel = true;
            series1.Legend = "Legend1";
            series1.Name = "Doanh Thu";
            series1.XValueMember = "PostedMonthYear";
            series1.YValueMembers = "TurnoverTotal";
            series2.ChartArea = "ChartArea1";
            series2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            series2.IsValueShownAsLabel = true;
            series2.Legend = "Legend1";
            series2.Name = "Chi Phí";
            series2.XValueMember = "PostedMonthYear";
            series2.YValueMembers = "CostsTotal";
            this.chartHealthFinance.Series.Add(series1);
            this.chartHealthFinance.Series.Add(series2);
            this.chartHealthFinance.Size = new System.Drawing.Size(482, 261);
            this.chartHealthFinance.TabIndex = 2;
            // 
            // ultraPanelHealthFERightBottom
            // 
            // 
            // ultraPanelHealthFERightBottom.ClientArea
            // 
            this.ultraPanelHealthFERightBottom.ClientArea.Controls.Add(this.txtHangTonKho);
            this.ultraPanelHealthFERightBottom.ClientArea.Controls.Add(this.txtChiPhi);
            this.ultraPanelHealthFERightBottom.ClientArea.Controls.Add(this.txtPhaiTra);
            this.ultraPanelHealthFERightBottom.ClientArea.Controls.Add(this.txtDoanhThu);
            this.ultraPanelHealthFERightBottom.ClientArea.Controls.Add(this.txtPhaiThu);
            this.ultraPanelHealthFERightBottom.ClientArea.Controls.Add(this.txtTienGui);
            this.ultraPanelHealthFERightBottom.ClientArea.Controls.Add(this.txtLoiNhuanTruocThue);
            this.ultraPanelHealthFERightBottom.ClientArea.Controls.Add(this.txtTienMat);
            this.ultraPanelHealthFERightBottom.ClientArea.Controls.Add(this.ultraLabel20);
            this.ultraPanelHealthFERightBottom.ClientArea.Controls.Add(this.ultraLabel19);
            this.ultraPanelHealthFERightBottom.ClientArea.Controls.Add(this.ultraLabel18);
            this.ultraPanelHealthFERightBottom.ClientArea.Controls.Add(this.ultraLabel15);
            this.ultraPanelHealthFERightBottom.ClientArea.Controls.Add(this.ultraLabel17);
            this.ultraPanelHealthFERightBottom.ClientArea.Controls.Add(this.ultraLabel14);
            this.ultraPanelHealthFERightBottom.ClientArea.Controls.Add(this.ultraLabel16);
            this.ultraPanelHealthFERightBottom.ClientArea.Controls.Add(this.ultraLabel13);
            this.ultraPanelHealthFERightBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanelHealthFERightBottom.Location = new System.Drawing.Point(0, 261);
            this.ultraPanelHealthFERightBottom.Name = "ultraPanelHealthFERightBottom";
            this.ultraPanelHealthFERightBottom.Size = new System.Drawing.Size(482, 155);
            this.ultraPanelHealthFERightBottom.TabIndex = 0;
            // 
            // ultraLabel20
            // 
            appearance39.BackColor = System.Drawing.Color.Transparent;
            appearance39.TextHAlignAsString = "Left";
            appearance39.TextVAlignAsString = "Bottom";
            this.ultraLabel20.Appearance = appearance39;
            this.ultraLabel20.Location = new System.Drawing.Point(232, 93);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(114, 17);
            this.ultraLabel20.TabIndex = 58;
            this.ultraLabel20.Text = "Hàng tồn kho";
            // 
            // ultraLabel19
            // 
            appearance40.BackColor = System.Drawing.Color.Transparent;
            appearance40.TextHAlignAsString = "Left";
            appearance40.TextVAlignAsString = "Bottom";
            this.ultraLabel19.Appearance = appearance40;
            this.ultraLabel19.Location = new System.Drawing.Point(16, 93);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(52, 17);
            this.ultraLabel19.TabIndex = 58;
            this.ultraLabel19.Text = "Chi phí";
            // 
            // ultraLabel18
            // 
            appearance41.BackColor = System.Drawing.Color.Transparent;
            appearance41.TextHAlignAsString = "Left";
            appearance41.TextVAlignAsString = "Bottom";
            this.ultraLabel18.Appearance = appearance41;
            this.ultraLabel18.Location = new System.Drawing.Point(232, 65);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(114, 17);
            this.ultraLabel18.TabIndex = 58;
            this.ultraLabel18.Text = "Phải trả";
            // 
            // ultraLabel15
            // 
            appearance42.BackColor = System.Drawing.Color.Transparent;
            appearance42.TextHAlignAsString = "Left";
            appearance42.TextVAlignAsString = "Bottom";
            this.ultraLabel15.Appearance = appearance42;
            this.ultraLabel15.Location = new System.Drawing.Point(16, 64);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(71, 17);
            this.ultraLabel15.TabIndex = 58;
            this.ultraLabel15.Text = "Doanh thu";
            // 
            // ultraLabel17
            // 
            appearance43.BackColor = System.Drawing.Color.Transparent;
            appearance43.TextHAlignAsString = "Left";
            appearance43.TextVAlignAsString = "Bottom";
            this.ultraLabel17.Appearance = appearance43;
            this.ultraLabel17.Location = new System.Drawing.Point(232, 37);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(114, 17);
            this.ultraLabel17.TabIndex = 58;
            this.ultraLabel17.Text = "Phải thu";
            // 
            // ultraLabel14
            // 
            appearance44.BackColor = System.Drawing.Color.Transparent;
            appearance44.TextHAlignAsString = "Left";
            appearance44.TextVAlignAsString = "Bottom";
            this.ultraLabel14.Appearance = appearance44;
            this.ultraLabel14.Location = new System.Drawing.Point(16, 34);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(71, 17);
            this.ultraLabel14.TabIndex = 58;
            this.ultraLabel14.Text = "Tiền gửi";
            // 
            // ultraLabel16
            // 
            appearance45.BackColor = System.Drawing.Color.Transparent;
            appearance45.TextHAlignAsString = "Left";
            appearance45.TextVAlignAsString = "Bottom";
            this.ultraLabel16.Appearance = appearance45;
            this.ultraLabel16.Location = new System.Drawing.Point(231, 9);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(114, 17);
            this.ultraLabel16.TabIndex = 58;
            this.ultraLabel16.Text = "Lợi nhuận trước thuế";
            // 
            // ultraLabel13
            // 
            appearance46.BackColor = System.Drawing.Color.Transparent;
            appearance46.TextHAlignAsString = "Left";
            appearance46.TextVAlignAsString = "Bottom";
            this.ultraLabel13.Appearance = appearance46;
            this.ultraLabel13.Location = new System.Drawing.Point(15, 9);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(72, 17);
            this.ultraLabel13.TabIndex = 58;
            this.ultraLabel13.Text = "Tiền mặt";
            // 
            // txtTySoNo
            // 
            appearance11.BackColor = System.Drawing.Color.White;
            appearance11.TextHAlignAsString = "Right";
            this.txtTySoNo.Appearance = appearance11;
            this.txtTySoNo.AutoSize = false;
            this.txtTySoNo.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtTySoNo.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtTySoNo.InputMask = "-n,nnn,nnn,nnn,nnn,nnn.nn";
            this.txtTySoNo.Location = new System.Drawing.Point(248, 68);
            this.txtTySoNo.Margin = new System.Windows.Forms.Padding(0);
            this.txtTySoNo.Name = "txtTySoNo";
            this.txtTySoNo.NullText = "";
            this.txtTySoNo.PromptChar = ' ';
            this.txtTySoNo.ReadOnly = true;
            this.txtTySoNo.Size = new System.Drawing.Size(157, 22);
            this.txtTySoNo.TabIndex = 241;
            // 
            // txtTySoTuTaiTro
            // 
            appearance10.BackColor = System.Drawing.Color.White;
            appearance10.TextHAlignAsString = "Right";
            this.txtTySoTuTaiTro.Appearance = appearance10;
            this.txtTySoTuTaiTro.AutoSize = false;
            this.txtTySoTuTaiTro.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtTySoTuTaiTro.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtTySoTuTaiTro.InputMask = "-n,nnn,nnn,nnn,nnn,nnn.nn";
            this.txtTySoTuTaiTro.Location = new System.Drawing.Point(248, 99);
            this.txtTySoTuTaiTro.Margin = new System.Windows.Forms.Padding(0);
            this.txtTySoTuTaiTro.Name = "txtTySoTuTaiTro";
            this.txtTySoTuTaiTro.NullText = "";
            this.txtTySoTuTaiTro.PromptChar = ' ';
            this.txtTySoTuTaiTro.ReadOnly = true;
            this.txtTySoTuTaiTro.Size = new System.Drawing.Size(157, 22);
            this.txtTySoTuTaiTro.TabIndex = 241;
            // 
            // txtVonLuanChuyen
            // 
            appearance1.BackColor = System.Drawing.Color.White;
            appearance1.TextHAlignAsString = "Right";
            this.txtVonLuanChuyen.Appearance = appearance1;
            this.txtVonLuanChuyen.AutoSize = false;
            this.txtVonLuanChuyen.BackColor = System.Drawing.Color.White;
            this.txtVonLuanChuyen.Location = new System.Drawing.Point(248, 131);
            this.txtVonLuanChuyen.Name = "txtVonLuanChuyen";
            this.txtVonLuanChuyen.ReadOnly = true;
            this.txtVonLuanChuyen.Size = new System.Drawing.Size(157, 22);
            this.txtVonLuanChuyen.TabIndex = 242;
            // 
            // txtHSThanhToanNganHan
            // 
            appearance9.BackColor = System.Drawing.Color.White;
            appearance9.TextHAlignAsString = "Right";
            this.txtHSThanhToanNganHan.Appearance = appearance9;
            this.txtHSThanhToanNganHan.AutoSize = false;
            this.txtHSThanhToanNganHan.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtHSThanhToanNganHan.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtHSThanhToanNganHan.InputMask = "-n,nnn,nnn,nnn,nnn,nnn.nn";
            this.txtHSThanhToanNganHan.Location = new System.Drawing.Point(248, 162);
            this.txtHSThanhToanNganHan.Margin = new System.Windows.Forms.Padding(0);
            this.txtHSThanhToanNganHan.Name = "txtHSThanhToanNganHan";
            this.txtHSThanhToanNganHan.NullText = "";
            this.txtHSThanhToanNganHan.PromptChar = ' ';
            this.txtHSThanhToanNganHan.ReadOnly = true;
            this.txtHSThanhToanNganHan.Size = new System.Drawing.Size(157, 22);
            this.txtHSThanhToanNganHan.TabIndex = 241;
            // 
            // txtHSThanhToanNhanh
            // 
            appearance8.BackColor = System.Drawing.Color.White;
            appearance8.TextHAlignAsString = "Right";
            this.txtHSThanhToanNhanh.Appearance = appearance8;
            this.txtHSThanhToanNhanh.AutoSize = false;
            this.txtHSThanhToanNhanh.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtHSThanhToanNhanh.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtHSThanhToanNhanh.InputMask = "-n,nnn,nnn,nnn,nnn,nnn.nn";
            this.txtHSThanhToanNhanh.Location = new System.Drawing.Point(248, 194);
            this.txtHSThanhToanNhanh.Margin = new System.Windows.Forms.Padding(0);
            this.txtHSThanhToanNhanh.Name = "txtHSThanhToanNhanh";
            this.txtHSThanhToanNhanh.NullText = "";
            this.txtHSThanhToanNhanh.PromptChar = ' ';
            this.txtHSThanhToanNhanh.ReadOnly = true;
            this.txtHSThanhToanNhanh.Size = new System.Drawing.Size(157, 22);
            this.txtHSThanhToanNhanh.TabIndex = 241;
            // 
            // txtHSThanhToanTucThoi
            // 
            appearance7.BackColor = System.Drawing.Color.White;
            appearance7.TextHAlignAsString = "Right";
            this.txtHSThanhToanTucThoi.Appearance = appearance7;
            this.txtHSThanhToanTucThoi.AutoSize = false;
            this.txtHSThanhToanTucThoi.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtHSThanhToanTucThoi.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtHSThanhToanTucThoi.InputMask = "-n,nnn,nnn,nnn,nnn,nnn.nn";
            this.txtHSThanhToanTucThoi.Location = new System.Drawing.Point(248, 226);
            this.txtHSThanhToanTucThoi.Margin = new System.Windows.Forms.Padding(0);
            this.txtHSThanhToanTucThoi.Name = "txtHSThanhToanTucThoi";
            this.txtHSThanhToanTucThoi.NullText = "";
            this.txtHSThanhToanTucThoi.PromptChar = ' ';
            this.txtHSThanhToanTucThoi.ReadOnly = true;
            this.txtHSThanhToanTucThoi.Size = new System.Drawing.Size(157, 22);
            this.txtHSThanhToanTucThoi.TabIndex = 241;
            // 
            // txtHSThanhToanChung
            // 
            appearance6.BackColor = System.Drawing.Color.White;
            appearance6.TextHAlignAsString = "Right";
            this.txtHSThanhToanChung.Appearance = appearance6;
            this.txtHSThanhToanChung.AutoSize = false;
            this.txtHSThanhToanChung.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtHSThanhToanChung.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtHSThanhToanChung.InputMask = "-n,nnn,nnn,nnn,nnn,nnn.nn";
            this.txtHSThanhToanChung.Location = new System.Drawing.Point(248, 258);
            this.txtHSThanhToanChung.Margin = new System.Windows.Forms.Padding(0);
            this.txtHSThanhToanChung.Name = "txtHSThanhToanChung";
            this.txtHSThanhToanChung.NullText = "";
            this.txtHSThanhToanChung.PromptChar = ' ';
            this.txtHSThanhToanChung.ReadOnly = true;
            this.txtHSThanhToanChung.Size = new System.Drawing.Size(157, 22);
            this.txtHSThanhToanChung.TabIndex = 241;
            // 
            // txtHSQuayVongHangTonKho
            // 
            appearance5.BackColor = System.Drawing.Color.White;
            appearance5.TextHAlignAsString = "Right";
            this.txtHSQuayVongHangTonKho.Appearance = appearance5;
            this.txtHSQuayVongHangTonKho.AutoSize = false;
            this.txtHSQuayVongHangTonKho.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtHSQuayVongHangTonKho.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtHSQuayVongHangTonKho.InputMask = "-n,nnn,nnn,nnn,nnn,nnn.nn";
            this.txtHSQuayVongHangTonKho.Location = new System.Drawing.Point(248, 290);
            this.txtHSQuayVongHangTonKho.Margin = new System.Windows.Forms.Padding(0);
            this.txtHSQuayVongHangTonKho.Name = "txtHSQuayVongHangTonKho";
            this.txtHSQuayVongHangTonKho.NullText = "";
            this.txtHSQuayVongHangTonKho.PromptChar = ' ';
            this.txtHSQuayVongHangTonKho.ReadOnly = true;
            this.txtHSQuayVongHangTonKho.Size = new System.Drawing.Size(157, 22);
            this.txtHSQuayVongHangTonKho.TabIndex = 241;
            // 
            // txtHSLoiNhuanVonKD
            // 
            appearance4.BackColor = System.Drawing.Color.White;
            appearance4.TextHAlignAsString = "Right";
            this.txtHSLoiNhuanVonKD.Appearance = appearance4;
            this.txtHSLoiNhuanVonKD.AutoSize = false;
            this.txtHSLoiNhuanVonKD.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtHSLoiNhuanVonKD.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtHSLoiNhuanVonKD.InputMask = "-n,nnn,nnn,nnn,nnn,nnn.nn";
            this.txtHSLoiNhuanVonKD.Location = new System.Drawing.Point(248, 322);
            this.txtHSLoiNhuanVonKD.Margin = new System.Windows.Forms.Padding(0);
            this.txtHSLoiNhuanVonKD.Name = "txtHSLoiNhuanVonKD";
            this.txtHSLoiNhuanVonKD.NullText = "";
            this.txtHSLoiNhuanVonKD.PromptChar = ' ';
            this.txtHSLoiNhuanVonKD.ReadOnly = true;
            this.txtHSLoiNhuanVonKD.Size = new System.Drawing.Size(157, 22);
            this.txtHSLoiNhuanVonKD.TabIndex = 241;
            // 
            // txtHSLoiNhuanDoanhThuThuan
            // 
            appearance3.BackColor = System.Drawing.Color.White;
            appearance3.TextHAlignAsString = "Right";
            this.txtHSLoiNhuanDoanhThuThuan.Appearance = appearance3;
            this.txtHSLoiNhuanDoanhThuThuan.AutoSize = false;
            this.txtHSLoiNhuanDoanhThuThuan.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtHSLoiNhuanDoanhThuThuan.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtHSLoiNhuanDoanhThuThuan.InputMask = "-n,nnn,nnn,nnn,nnn,nnn.nn";
            this.txtHSLoiNhuanDoanhThuThuan.Location = new System.Drawing.Point(248, 354);
            this.txtHSLoiNhuanDoanhThuThuan.Margin = new System.Windows.Forms.Padding(0);
            this.txtHSLoiNhuanDoanhThuThuan.Name = "txtHSLoiNhuanDoanhThuThuan";
            this.txtHSLoiNhuanDoanhThuThuan.NullText = "";
            this.txtHSLoiNhuanDoanhThuThuan.PromptChar = ' ';
            this.txtHSLoiNhuanDoanhThuThuan.ReadOnly = true;
            this.txtHSLoiNhuanDoanhThuThuan.Size = new System.Drawing.Size(157, 22);
            this.txtHSLoiNhuanDoanhThuThuan.TabIndex = 241;
            // 
            // txtHSLoiNhuanVonChuSoHuu
            // 
            appearance2.BackColor = System.Drawing.Color.White;
            appearance2.TextHAlignAsString = "Right";
            this.txtHSLoiNhuanVonChuSoHuu.Appearance = appearance2;
            this.txtHSLoiNhuanVonChuSoHuu.AutoSize = false;
            this.txtHSLoiNhuanVonChuSoHuu.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtHSLoiNhuanVonChuSoHuu.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.txtHSLoiNhuanVonChuSoHuu.InputMask = "-n,nnn,nnn,nnn,nnn,nnn.nn";
            this.txtHSLoiNhuanVonChuSoHuu.Location = new System.Drawing.Point(248, 386);
            this.txtHSLoiNhuanVonChuSoHuu.Margin = new System.Windows.Forms.Padding(0);
            this.txtHSLoiNhuanVonChuSoHuu.Name = "txtHSLoiNhuanVonChuSoHuu";
            this.txtHSLoiNhuanVonChuSoHuu.NullText = "";
            this.txtHSLoiNhuanVonChuSoHuu.PromptChar = ' ';
            this.txtHSLoiNhuanVonChuSoHuu.ReadOnly = true;
            this.txtHSLoiNhuanVonChuSoHuu.Size = new System.Drawing.Size(157, 22);
            this.txtHSLoiNhuanVonChuSoHuu.TabIndex = 241;
            // 
            // txtTienMat
            // 
            appearance38.BackColor = System.Drawing.Color.White;
            appearance38.TextHAlignAsString = "Right";
            this.txtTienMat.Appearance = appearance38;
            this.txtTienMat.AutoSize = false;
            this.txtTienMat.BackColor = System.Drawing.Color.White;
            this.txtTienMat.Location = new System.Drawing.Point(93, 6);
            this.txtTienMat.Name = "txtTienMat";
            this.txtTienMat.ReadOnly = true;
            this.txtTienMat.Size = new System.Drawing.Size(129, 22);
            this.txtTienMat.TabIndex = 242;
            // 
            // txtTienGui
            // 
            appearance36.BackColor = System.Drawing.Color.White;
            appearance36.TextHAlignAsString = "Right";
            this.txtTienGui.Appearance = appearance36;
            this.txtTienGui.AutoSize = false;
            this.txtTienGui.BackColor = System.Drawing.Color.White;
            this.txtTienGui.Location = new System.Drawing.Point(93, 34);
            this.txtTienGui.Name = "txtTienGui";
            this.txtTienGui.ReadOnly = true;
            this.txtTienGui.Size = new System.Drawing.Size(129, 22);
            this.txtTienGui.TabIndex = 242;
            // 
            // txtDoanhThu
            // 
            appearance34.BackColor = System.Drawing.Color.White;
            appearance34.TextHAlignAsString = "Right";
            this.txtDoanhThu.Appearance = appearance34;
            this.txtDoanhThu.AutoSize = false;
            this.txtDoanhThu.BackColor = System.Drawing.Color.White;
            this.txtDoanhThu.Location = new System.Drawing.Point(93, 62);
            this.txtDoanhThu.Name = "txtDoanhThu";
            this.txtDoanhThu.ReadOnly = true;
            this.txtDoanhThu.Size = new System.Drawing.Size(129, 22);
            this.txtDoanhThu.TabIndex = 242;
            // 
            // txtChiPhi
            // 
            appearance32.BackColor = System.Drawing.Color.White;
            appearance32.TextHAlignAsString = "Right";
            this.txtChiPhi.Appearance = appearance32;
            this.txtChiPhi.AutoSize = false;
            this.txtChiPhi.BackColor = System.Drawing.Color.White;
            this.txtChiPhi.Location = new System.Drawing.Point(93, 90);
            this.txtChiPhi.Name = "txtChiPhi";
            this.txtChiPhi.ReadOnly = true;
            this.txtChiPhi.Size = new System.Drawing.Size(129, 22);
            this.txtChiPhi.TabIndex = 242;
            // 
            // txtLoiNhuanTruocThue
            // 
            appearance37.BackColor = System.Drawing.Color.White;
            appearance37.TextHAlignAsString = "Right";
            this.txtLoiNhuanTruocThue.Appearance = appearance37;
            this.txtLoiNhuanTruocThue.AutoSize = false;
            this.txtLoiNhuanTruocThue.BackColor = System.Drawing.Color.White;
            this.txtLoiNhuanTruocThue.Location = new System.Drawing.Point(351, 7);
            this.txtLoiNhuanTruocThue.Name = "txtLoiNhuanTruocThue";
            this.txtLoiNhuanTruocThue.ReadOnly = true;
            this.txtLoiNhuanTruocThue.Size = new System.Drawing.Size(121, 22);
            this.txtLoiNhuanTruocThue.TabIndex = 242;
            // 
            // txtPhaiThu
            // 
            appearance35.BackColor = System.Drawing.Color.White;
            appearance35.TextHAlignAsString = "Right";
            this.txtPhaiThu.Appearance = appearance35;
            this.txtPhaiThu.AutoSize = false;
            this.txtPhaiThu.BackColor = System.Drawing.Color.White;
            this.txtPhaiThu.Location = new System.Drawing.Point(351, 35);
            this.txtPhaiThu.Name = "txtPhaiThu";
            this.txtPhaiThu.ReadOnly = true;
            this.txtPhaiThu.Size = new System.Drawing.Size(121, 22);
            this.txtPhaiThu.TabIndex = 242;
            // 
            // txtPhaiTra
            // 
            appearance33.BackColor = System.Drawing.Color.White;
            appearance33.TextHAlignAsString = "Right";
            this.txtPhaiTra.Appearance = appearance33;
            this.txtPhaiTra.AutoSize = false;
            this.txtPhaiTra.BackColor = System.Drawing.Color.White;
            this.txtPhaiTra.Location = new System.Drawing.Point(351, 63);
            this.txtPhaiTra.Name = "txtPhaiTra";
            this.txtPhaiTra.ReadOnly = true;
            this.txtPhaiTra.Size = new System.Drawing.Size(121, 22);
            this.txtPhaiTra.TabIndex = 242;
            // 
            // txtHangTonKho
            // 
            appearance31.BackColor = System.Drawing.Color.White;
            appearance31.TextHAlignAsString = "Right";
            this.txtHangTonKho.Appearance = appearance31;
            this.txtHangTonKho.AutoSize = false;
            this.txtHangTonKho.BackColor = System.Drawing.Color.White;
            this.txtHangTonKho.Location = new System.Drawing.Point(351, 91);
            this.txtHangTonKho.Name = "txtHangTonKho";
            this.txtHangTonKho.ReadOnly = true;
            this.txtHangTonKho.Size = new System.Drawing.Size(121, 22);
            this.txtHangTonKho.TabIndex = 242;
            // 
            // UCHealthFinanceEnterprise
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.ultraPanelHealthFERight);
            this.Controls.Add(this.ultraPanelHealthFELeft);
            this.Name = "UCHealthFinanceEnterprise";
            this.Size = new System.Drawing.Size(893, 416);
            this.ultraPanelHealthFELeft.ClientArea.ResumeLayout(false);
            this.ultraPanelHealthFELeft.ResumeLayout(false);
            this.ultraPanelHealthFETop.ClientArea.ResumeLayout(false);
            this.ultraPanelHealthFETop.ClientArea.PerformLayout();
            this.ultraPanelHealthFETop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).EndInit();
            this.ultraPanelHealthFERight.ClientArea.ResumeLayout(false);
            this.ultraPanelHealthFERight.ClientArea.PerformLayout();
            this.ultraPanelHealthFERight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbKyVeBieuDo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartHealthFinance)).EndInit();
            this.ultraPanelHealthFERightBottom.ClientArea.ResumeLayout(false);
            this.ultraPanelHealthFERightBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtVonLuanChuyen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienMat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienGui)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDoanhThu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChiPhi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoiNhuanTruocThue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhaiThu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhaiTra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHangTonKho)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Infragistics.Win.Misc.UltraPanel ultraPanelHealthFELeft;
        private Infragistics.Win.Misc.UltraPanel ultraPanelHealthFERight;
        private Infragistics.Win.Misc.UltraPanel ultraPanelHealthFERightBottom;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartHealthFinance;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbKyVeBieuDo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.Misc.UltraPanel ultraPanelHealthFETop;
        private Infragistics.Win.Misc.UltraButton btnOk;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel lblBeginDate;
        private Infragistics.Win.Misc.UltraLabel lblEndDate;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDateTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtBeginDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtEndDate;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtTySoNo;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtTySoTuTaiTro;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtVonLuanChuyen;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtHSLoiNhuanVonChuSoHuu;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtHSLoiNhuanDoanhThuThuan;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtHSLoiNhuanVonKD;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtHSQuayVongHangTonKho;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtHSThanhToanChung;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtHSThanhToanTucThoi;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtHSThanhToanNhanh;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtHSThanhToanNganHan;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtHangTonKho;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtChiPhi;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPhaiTra;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDoanhThu;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPhaiThu;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTienGui;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtLoiNhuanTruocThue;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTienMat;
    }
}
