﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Data;
using Infragistics.Win;
using Type = System.Type;
using InvoiceClient;
using Newtonsoft.Json;
using Accounting.Frm;
using Newtonsoft.Json.Linq;

namespace Accounting
{
    public class BaseForm : CustormForm
    {

        #region Service
        #region Danh mục
        public IMaterialGoodsService IMaterialGoodsService
        {
            get { return Utils.IMaterialGoodsService; }
        }
        public IPaymentClauseService IPaymentClauseService
        {
            get { return Utils.IPaymentClauseService; }
        }
        public ITransportMethodService ITransportMethodService
        {
            get { return Utils.ITransportMethodService; }
        }
        public IRepositoryService IRepositoryService
        {
            get { return Utils.IRepositoryService; }
        }
        public IAccountService IAccountService
        {
            get { return Utils.IAccountService; }
        }
        public IInvoiceTypeService IInvoiceTypeService
        {
            get { return Utils.IInvoiceTypeService; }
        }
        public IRepositoryLedgerService IRepositoryLedgerService
        {
            get { return Utils.IRepositoryLedgerService; }
        }
        public IAccountingObjectService IAccountingObjectService
        {
            get { return Utils.IAccountingObjectService; }
        }
        public IBankAccountDetailService IBankAccountDetailService
        {
            get { return Utils.IBankAccountDetailService; }
        }
        public IExpenseItemService IExpenseItemService
        {
            get { return Utils.IExpenseItemService; }
        }
        public IDepartmentService IDepartmentService
        {
            get { return Utils.IDepartmentService; }
        }
        public IBudgetItemService IBudgetItemService
        {
            get { return Utils.IBudgetItemService; }
        }
        public IStatisticsCodeService IStatisticsCodeService
        {
            get { return Utils.IStatisticsCodeService; }
        }
        public ICostSetService ICostSetService
        {
            get { return Utils.ICostSetService; }
        }
        public ICurrencyService ICurrencyService
        {
            get { return Utils.ICurrencyService; }
        }
        public IGenCodeService IGenCodeService
        {
            get { return Utils.IGenCodeService; }
        }
        public IGeneralLedgerService IGeneralLedgerService
        {
            get { return Utils.IGeneralLedgerService; }
        }
        public IFixedAssetLedgerService IFixedAssetLedgerService
        {
            get { return Utils.IFixedAssetLedgerService; }
        }
        public IAccountingObjectBankAccountService IAccountingObjectBankAccountService
        {
            get { return Utils.IAccountingObjectBankAccountService; }
        }
        public ICreditCardService ICreditCardService
        {
            get { return Utils.ICreditCardService; }
        }
        public ITypeService ITypeService
        {
            get { return Utils.ITypeService; }
        }
        public ITemplateService ITemplateService
        {
            get { return Utils.ITemplateService; }
        }
        public IAccountDefaultService IAccountDefaultService
        {
            get { return Utils.IAccountDefaultService; }
        }
        public IAccountTransferService IAccountTransferService
        {
            get { return Utils.IAccountTransferService; }
        }
        public IAccountGroupService IAccountGroupService
        {
            get { return Utils.IAccountGroupService; }
        }
        public IAutoPrincipleService IAutoPrincipleService
        {
            get { return Utils.IAutoPrincipleService; }
        }
        public IAccountingObjectCategoryService IAccountingObjectCategoryService
        {
            get { return Utils.IAccountingObjectCategoryService; }
        }
        public IAccountingObjectGroupService IAccountingObjectGroupService
        {
            get { return Utils.IAccountingObjectGroupService; }
        }
        public IPersonalSalaryTaxService IPersonalSalaryTaxService
        {
            get { return Utils.IPersonalSalaryTaxService; }
        }
        public ITimeSheetSymbolsService ITimeSheetSymbolsService
        {
            get { return Utils.ITimeSheetSymbolsService; }
        }
        public IMaterialGoodsCategoryService IMaterialGoodsCategoryService
        {
            get { return Utils.IMaterialGoodsCategoryService; }
        }
        public IMaterialQuantumService IMaterialQuantumService
        {
            get { return Utils.IMaterialQuantumService; }
        }
        public IMaterialQuantumDetailService IMaterialQuantumDetailService
        {
            get { return Utils.IMaterialQuantumDetailService; }
        }
        public IFixedAssetCategoryService IFixedAssetCategoryService
        {
            get { return Utils.IFixedAssetCategoryService; }
        }
        public IFixedAssetService IFixedAssetService
        {
            get { return Utils.IFixedAssetService; }
        }
        public IPSSalarySheetService IPSSalarySheetService
        {
            get { return Utils.IPSSalarySheetService; }
        }
        public IFixedAssetDetailService IFixedAssetDetailService
        {
            get { return Utils.IFixedAssetDetailService; }
        }
        public IStockCategoryService IStockCategoryService
        {
            get { return Utils.IStockCategoryService; }
        }
        public IInvestorGroupService IInvestorGroupService
        {
            get { return Utils.IInvestorGroupService; }
        }
        public IInvestorService IInvestorService
        {
            get { return Utils.IInvestorService; }
        }
        public IRegistrationGroupService IRegistrationGroupService
        {
            get { return Utils.IRegistrationGroupService; }
        }
        public IShareHolderGroupService IShareHolderGroupService
        {
            get { return Utils.IShareHolderGroupService; }
        }
        public IBankService IBankService
        {
            get { return Utils.IBankService; }
        }
        public IContractStateService IContractStateService
        {
            get { return Utils.IContractStateService; }
        }
        public IToolLedgerService IToolLedgerService
        {
            get { return Utils.IToolLedgerService; }
        }
        #endregion

        #region MC Module
        public IMCPaymentService IMCPaymentService
        {
            get { return Utils.IMCPaymentService; }
        }
        public IMCPaymentDetailVendorService IMCPaymentDetailVendorService
        {
            get { return Utils.IMCPaymentDetailVendorService; }
        }
        public IMCPaymentDetailService IMCPaymentDetailService
        {
            get { return Utils.IMCPaymentDetailService; }
        }
        public IMCPaymentDetailTaxService IMCPaymentDetailTaxService
        {
            get { return Utils.IMCPaymentDetailTaxService; }
        }
        public IMCReceiptService IMCReceiptService
        {
            get { return Utils.IMCReceiptService; }
        }
        public IMCReceiptDetailService IMCReceiptDetailService
        {
            get { return Utils.IMCReceiptDetailService; }
        }
        public IMCReceiptDetailCustomerService IMCReceiptDetailCustomerService
        {
            get { return Utils.IMCReceiptDetailCustomerService; }
        }
        public IMCReceiptDetailTaxService IMCReceiptDetailTaxService
        {
            get { return Utils.IMCReceiptDetailTaxService; }
        }
        #endregion

        #region MB Module
        public IMBCreditCardService IMBCreditCardService
        {
            get { return Utils.IMBCreditCardService; }
        }
        public IMBCreditCardDetailTaxService IMBCreditCardDetailTaxService
        {
            get { return Utils.IMBCreditCardDetailTaxService; }
        }
        public IMBCreditCardDetailService IMBCreditCardDetailService
        {
            get { return Utils.IMBCreditCardDetailService; }
        }
        public IMBTellerPaperDetailVendorService IMBTellerPaperDetailVendorService
        {
            get { return Utils.IMBTellerPaperDetailVendorService; }
        }
        public IMBTellerPaperDetailService IMBTellerPaperDetailService
        {
            get { return Utils.IMBTellerPaperDetailService; }
        }
        public IMBTellerPaperDetailTaxService IMBTellerPaperDetailTaxService
        {
            get { return Utils.IMBTellerPaperDetailTaxService; }
        }
        public IMBTellerPaperService IMBTellerPaperService
        {
            get { return Utils.IMBTellerPaperService; }
        }
        public IMBTellerPaperDetailService IMBTellerPaperDetailSerivce
        {
            get { return Utils.IMBTellerPaperDetailService; }
        }
        public IMBDepositService IMBDepositService
        {
            get { return Utils.IMBDepositService; }
        }
        public IMBDepositDetailTaxService IMBDepositDetailTaxService
        {
            get { return Utils.IMBDepositDetailTaxService; }
        }
        public IMBInternalTransferService IMBInternalTransferService
        {
            get { return Utils.IMBInternalTransferService; }
        }
        public IMBInternalTransferDetailService IMBInternalTransferDetailService
        {
            get { return Utils.IMBInternalTransferDetailService; }
        }
        public IMBInternalTransferTaxService IMBInternalTransferTaxService
        {
            get { return Utils.IMBInternalTransferTaxService; }
        }

        #endregion

        #region PP Module
        public IPPDiscountReturnService IPPDiscountReturnService
        {
            get { return Utils.IPPDiscountReturnService; }
        }
        public IPPDiscountReturnDetailService IPPDiscountReturnDetailService
        {
            get { return Utils.IPPDiscountReturnDetailService; }
        }
        public IPPServiceService IPPServiceService
        {
            get { return Utils.IPPServiceService; }
        }
        public IPPServiceDetailService IPPServiceDetailService
        {
            get { return Utils.IPPServiceDetailService; }
        }
        public IPPOrderService IPPOrderService
        {
            get { return Utils.IPPOrderService; }
        }
        public IPPOrderDetailService IPPOrderDetailService
        {
            get { return Utils.IPPOrderDetailService; }
        }
        public IPPInvoiceDetailService IPPInvoiceDetailService
        {
            get { return Utils.IPPInvoiceDetailService; }
        }
        public IPPInvoiceDetailCostService IPPInvoiceDetailCostService
        {
            get { return Utils.IPPInvoiceDetailCostService; }
        }
        public IPPInvoiceService IPPInvoiceService
        {
            get { return Utils.IPPInvoiceService; }
        }
        #endregion

        #region RS Module
        public IRSInwardOutwardService IRSInwardOutwardService
        {
            get { return Utils.IRSInwardOutwardService; }
        }
        public IRSInwardOutwardDetailService IRSInwardOutwardDetailService
        {
            get { return Utils.IRSInwardOutwardDetailService; }
        }
        public IRSTransferService IRSTransferService
        {
            get { return Utils.IRSTransferService; }
        }
        public IRSTransferDetailService IRSTransferDetailService
        {
            get { return Utils.IRSTransferDetailService; }
        }
        public IRSAssemblyDismantlementService IRSAssemblyDismantlementService
        {
            get { return Utils.IRSAssemblyDismantlementService; }
        }
        public IRSAssemblyDismantlementDetailService IRSAssemblyDismantlementDetailService
        {
            get { return Utils.IRSAssemblyDismantlementDetailService; }
        }
        #endregion

        #region EM Module
        public IEMContractService IEMContractService
        {
            get { return Utils.IEMContractService; }
        }
        public IEMContractDetailMGService IEMContractDetailMGService
        {
            get { return Utils.IEMContractDetailMGService; }
        }
        public IEMContractDetailPaymentService IEMContractDetailPaymentService
        {
            get { return Utils.IEMContractDetailPaymentService; }
        }
        public IEMContractDetailRevenueService IEMContractDetailRevenueService
        {
            get { return Utils.IEMContractDetailRevenueService; }
        }
        #endregion

        #region SA Module
        public ISAQuoteService ISAQuoteService
        {
            get { return Utils.ISAQuoteService; }
        }
        public ISAQuoteDetailService ISAQuoteDetailService
        {
            get { return Utils.ISAQuoteDetailService; }
        }
        public ISAOrderService ISAOrderService
        {
            get { return Utils.ISAOrderService; }
        }
        public ISAOrderDetailService ISAOrderDetailService
        {
            get { return Utils.ISAOrderDetailService; }
        }
        public ISAReturnService ISAReturnService
        {
            get { return Utils.ISAReturnService; }
        }
        public ISAReturnDetailService ISAReturnDetailService
        {
            get { return Utils.ISAReturnDetailService; }
        }
        public ISAReturnDetailCustomerService ISAReturnDetailCustomerService
        {
            get { return Utils.ISAReturnDetailCustomerService; }
        }
        public ISAInvoiceService ISAInvoiceService
        {
            get { return Utils.ISAInvoiceService; }
        }
        public ISAInvoiceDetailService ISAInvoiceDetailService
        {
            get { return Utils.ISAInvoiceDetailService; }
        }
        public ISABillService ISABillService
        {
            get { return Utils.ISABillService; }
        }
        #endregion
        #region FA Module
        public IFADecrementService IFADecrementService
        {
            get { return Utils.IFADecrementService; }
        }

        public IFAIncrementService IFAIncrementService
        {
            get { return Utils.IFAIncrementService; }
        }
        public IFAIncrementDetailService IFAIncrementDetailService
        {
            get { return Utils.IFAIncrementDetailService; }
        }

        public IFADecrementDetailService IFADecrementDetailService
        {
            get { return Utils.IFADecrementDetailService; }
        }
        public IFAAdjustmentService IFAAdjustmentService
        {
            get { return Utils.IFAAdjustmentService; }
        }
        public IFAAdjustmentDetailService IFAAdjustmentDetailService
        {
            get { return Utils.IFAAdjustmentDetailService; }
        }
        public IFATransferService IFATransferService
        {
            get { return Utils.IFATransferService; }
        }
        public IFATransferDetailService IFATransferDetailService
        {
            get { return Utils.IFATransferDetailService; }
        }
        public IFADepreciationService IFADepreciationService
        {
            get { return Utils.IFADepreciationService; }
        }
        public IFADepreciationDetailService IFADepreciationDetailService
        {
            get { return Utils.IFADepreciationDetailService; }
        }
        public IFAAuditService IFAAuditService
        {
            get { return Utils.IFAAuditService; }
        }
        public IFAAuditDetailService IFAAuditDetailService
        {
            get { return Utils.IFAAuditDetailService; }
        }
        public IFAAuditMemberDetailService IFAAuditMemberDetailService
        {
            get { return Utils.IFAAuditMemberDetailService; }
        }
        public IFAInitService IFAInitService
        {
            get { return Utils.IFAInitService; }
        }
        #endregion

        #region TI Module
        public ITIIncrementService ITIIncrementService
        {
            get { return Utils.ITIIncrementService; }
        }
        public ITIIncrementDetailService ITIIncrementDetailService
        {
            get { return Utils.ITIIncrementDetailService; }
        }
        public ITIDecrementService ITIDecrementService
        {
            get { return Utils.ITIDecrementService; }
        }
        public ITIDecrementDetailService ITIDecrementDetailService
        {
            get { return Utils.ITIDecrementDetailService; }
        }
        public ITIAllocationService ITIAllocationService
        {
            get { return Utils.ITIAllocationService; }
        }
        public ITIAllocationDetailService ITIAllocationDetailService
        {
            get { return Utils.ITIAllocationDetailService; }
        }
        public ITITransferService ITITransferService
        {
            get { return Utils.ITITransferService; }
        }
        public ITITransferDetailService ITITransferDetailService
        {
            get { return Utils.ITITransferDetailService; }
        }
        public ITILostService ITILostService
        {
            get { return Utils.ITILostService; }
        }
        public ITILostDetailService ITILostDetailService
        {
            get { return Utils.ITILostDetailService; }
        }
        public ITIAdjustmentService ITIAdjustmentService
        {
            get { return Utils.ITIAdjustmentService; }
        }
        public ITIAdjustmentDetailService ITIAdjustmentDetailService
        {
            get { return Utils.ITIAdjustmentDetailService; }
        }
        public ITIInitService ITIInitService
        {
            get { return Utils.ITIInitService; }
        }
        #endregion

        #region GO Module
        public IGOtherVoucherService IGOtherVoucherService
        {
            get { return Utils.IGOtherVoucherService; }
        }
        public IGOtherVoucherDetailService IGOtherVoucherDetailService
        {
            get { return Utils.IGOtherVoucherDetailService; }
        }
        public IPrepaidExpenseService IPrepaidExpenseService
        {
            get { return Utils.IPrepaidExpenseService; }
        }
        public IGOtherVoucherDetailExpenseService IGOtherVoucherDetailExpenseService
        {
            get { return Utils.IGOtherVoucherDetailExpenseService; }
        }
        #endregion

        #region CP Module
        public ICPAcceptanceService ICPAcceptanceService
        {
            get { return Utils.ICPAcceptanceService; }
        }
        #endregion

        #region EI Module
        public ISupplierServiceService ISupplierServiceService
        {
            get { return Utils.ISupplierServiceService; }
        }
        #endregion

        #region RefVoucher Module

        public IRefVoucherRSInwardOutwardService IRefVoucherRSInwardOutwardService
        {
            get { return Utils.IRefVoucherRSInwardOutwardService; }
        }

        public IRefVoucherService IRefVoucherService
        {
            get { return Utils.IRefVoucherService; }
        }

        #endregion
        #endregion

        /// <summary>
        /// Cập nhật Số lượng nhận vào Đơn mua hàng được chọn trong phần Mua hàng
        /// </summary>
        /// <param name="input"></param>
        /// <param name="back"> </param>
        /// <returns></returns>
        public bool UpdateQuantityReceipt(PPInvoice input, PPInvoice back = null)
        {
            try
            {
                if (back != null)
                {
                    if (!RemoveQuantityReceipt(back))
                        return false;
                }
                foreach (var ppInvoiceDetail in input.PPInvoiceDetails.Where(x => x.PPOrderID.HasValue && x.DetailID.HasValue))
                {
                    var ppOrderDetail = IPPOrderDetailService.Getbykey(ppInvoiceDetail.DetailID.Value);
                    if (ppOrderDetail == null) continue;
                    ppOrderDetail.QuantityReceipt = (ppOrderDetail.QuantityReceipt ?? 0) + ppInvoiceDetail.Quantity;
                    IPPOrderDetailService.Update(ppOrderDetail);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// Cập nhật Số lượng nhận vào Đơn đặt hàng được chọn trong phần Bán hàng
        /// </summary>
        /// <param name="input"></param>
        /// <param name="back"> </param>
        /// <returns></returns>
        public bool UpdateQuantityReceipt(SAInvoice input, SAInvoice back = null)
        {
            try
            {
                if (back != null)
                {
                    if (!RemoveQuantityReceipt(back))
                        return false;
                }
                foreach (var saInvoiceDetail in input.SAInvoiceDetails.Where(x => x.SAOrderID.HasValue && x.DetailID.HasValue))
                {
                    var saOrderDetail = ISAOrderDetailService.Getbykey(saInvoiceDetail.DetailID.Value);
                    if (saOrderDetail == null) continue;
                    saOrderDetail.QuantityReceipt = (saOrderDetail.QuantityReceipt ?? 0) + saInvoiceDetail.Quantity;
                    ISAOrderDetailService.Update(saOrderDetail);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// Cập nhật Số lượng nhận vào Đơn đặt hàng được chọn trong phần Bán hàng
        /// </summary>
        /// <param name="input"></param>
        /// <param name="back"> </param>
        /// <returns></returns>
        public bool UpdateQuantityReceipt(RSInwardOutward input, RSInwardOutward back = null)
        {
            try
            {
                if (back != null)
                {
                    if (!RemoveQuantityReceipt(back))
                        return false;
                }
                foreach (var rsInwardOutwardDetail in input.RSInwardOutwardDetails.Where(x => x.DetailID.HasValue))
                {
                    var saOrderDetail = ISAOrderDetailService.Getbykey(rsInwardOutwardDetail.DetailID.Value);
                    if (saOrderDetail == null) continue;
                    saOrderDetail.QuantityReceipt = (saOrderDetail.QuantityReceipt ?? 0) + rsInwardOutwardDetail.Quantity;
                    ISAOrderDetailService.Update(saOrderDetail);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Xóa Số lượng nhận vào Đơn đặt hàng/Đơn mua hàng được chọn trong phần Bán hàng/Mua hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public bool RemoveQuantityReceipt<T>(T input)
        {
            try
            {
                if (typeof(T) == typeof(PPInvoice))
                {
                    if (((input as PPInvoice).PPInvoiceDetails != null))
                        foreach (var ppInvoiceDetail in (input as PPInvoice).PPInvoiceDetails.Where(x => x.PPOrderID.HasValue && x.DetailID.HasValue))
                        {
                            var ppOrderDetail = IPPOrderDetailService.Getbykey(ppInvoiceDetail.DetailID.Value);
                            if (ppOrderDetail == null) continue;
                            ppOrderDetail.QuantityReceipt -= ppInvoiceDetail.Quantity;
                            IPPOrderDetailService.Update(ppOrderDetail);
                        }
                }
                else if (typeof(T) == typeof(SAInvoice))
                {
                    if ((input as SAInvoice).SAInvoiceDetails != null)
                        foreach (var saInvoiceDetail in (input as SAInvoice).SAInvoiceDetails.Where(x => x.SAOrderID.HasValue && x.DetailID.HasValue))
                        {
                            var saOrderDetail = ISAOrderDetailService.Getbykey(saInvoiceDetail.DetailID.Value);
                            if (saOrderDetail == null) continue;
                            saOrderDetail.QuantityReceipt -= saInvoiceDetail.Quantity;
                            ISAOrderDetailService.Update(saOrderDetail);
                        }
                }
                else if (typeof(T) == typeof(RSInwardOutward) && (new int[] { 400, 401, 404 }.Contains((input as RSInwardOutward).TypeID) || new int[] { 410, 411, 414 }.Contains((input as RSInwardOutward).TypeID)))
                {

                    if ((input as RSInwardOutward).RSInwardOutwardDetails != null)
                        foreach (var rsInwardOutwardDetail in (input as RSInwardOutward).RSInwardOutwardDetails.Where(x => x.DetailID.HasValue))
                        {
                            var saOrderDetail = ISAOrderDetailService.Getbykey(rsInwardOutwardDetail.DetailID.Value);
                            if (saOrderDetail == null) continue;
                            saOrderDetail.QuantityReceipt -= rsInwardOutwardDetail.Quantity;
                            ISAOrderDetailService.Update(saOrderDetail);
                        }
                }
                return true;
            }
            catch (Exception Ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Lưu móc trong móc 3
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TL"></typeparam>
        /// <param name="select"></param>
        /// <param name="id"></param>
        /// <param name="remove"></param>
        /// <returns></returns>
        public bool SaveLeftJoinSelect<T, TL>(T @select, Guid? id, bool remove) where TL : class
        {
            if (id == null) return false;
            TL leftJoin = (TL)Activator.CreateInstance(typeof(TL));
            BaseService<TL, Guid> services = this.GetIService(leftJoin);
            leftJoin = services.Getbykey(id.Value);
            if (remove)
            {
                //var temp = services.Getbykey(id.Value);
                if (leftJoin != null)
                    services.Delete(leftJoin);
                return true;
            }
            if (leftJoin != null)
            {
                PropertyInfo propAccountingObjectId = @select.GetType().GetProperty("AccountingObjectID");
                if (propAccountingObjectId != null && propAccountingObjectId.CanWrite &&
                    propAccountingObjectId.CanRead)
                {
                    PropertyInfo propLjAccountingObjectId = leftJoin.GetType().GetProperty("AccountingObjectID");
                    if (propLjAccountingObjectId != null && propLjAccountingObjectId.CanWrite)
                        propLjAccountingObjectId.SetValue(leftJoin,
                                                          (Guid)propAccountingObjectId.GetValue(@select, null),
                                                          null);
                }
                PropertyInfo propAccountingObjectName = @select.GetType().GetProperty("AccountingObjectName");
                if (propAccountingObjectName != null && propAccountingObjectName.CanWrite &&
                    propAccountingObjectName.CanRead)
                {
                    PropertyInfo propLjAccountingObjectName = leftJoin.GetType().GetProperty("AccountingObjectName");
                    propLjAccountingObjectName.SetValue(leftJoin,
                                                        (string)propAccountingObjectName.GetValue(@select, null),
                                                        null);
                }
                PropertyInfo propAccountingObjectAddress = @select.GetType().GetProperty("AccountingObjectAddress");
                if (propAccountingObjectAddress != null && propAccountingObjectAddress.CanWrite &&
                    propAccountingObjectAddress.CanRead)
                {
                    PropertyInfo propLjAccountingObjectAddress =
                        leftJoin.GetType().GetProperty("AccountingObjectAddress");
                    propLjAccountingObjectAddress.SetValue(leftJoin,
                                                           (string)
                                                           propAccountingObjectAddress.GetValue(@select, null),
                                                           null);
                }
                services.Update(leftJoin);
            }
            return true;
        }


        /// <summary>
        /// Map and Gen Vocher Other Base
        /// </summary>
        /// <typeparam name="TK">Kiểu đối tượng móc (nhập kho, xuất kho, phiếu thu, phiếu chi, Ủy nhiệm chi,....)</typeparam>
        /// <typeparam name="T"> </typeparam>
        /// <param name="statusForm"> </param>
        /// <param name="keyPair">cặp TypeID(A,B) trong đó A là chứng từ chính, B là chứng từ con</param>
        /// <param name="strLstKeys">danh sách các properties của chứng từ móc</param>
        /// <param name="strLstOtherKeys">danh sách các properties của _SELECT mà map tương ứng</param>
        /// <param name="remove">Chỉ xóa chứng từ móc</param>
        /// <param name="select"> </param>
        public void GenVocherOtherStand<T, TK>(T @select, int statusForm, Dictionary<int, int> keyPair, string strLstKeys, string strLstOtherKeys, bool remove)
        {
            Guid guid = @select.GetProperty<T, Guid>("ID");
            int typeid = @select.GetProperty<T, int>("TypeID");
            if (!keyPair.Keys.Contains(typeid)) return; //typeID tự sinh

            TK temp = (TK)Activator.CreateInstance(typeof(TK));
            BaseService<TK, Guid> baseService = this.GetIService(temp);

            if ((statusForm == ConstFrm.optStatusForm.Edit && baseService.Getbykey(guid) != null)
                || (statusForm == ConstFrm.optStatusForm.Delete && baseService.Getbykey(guid) != null)
                || (remove && statusForm == ConstFrm.optStatusForm.View)) baseService.Delete(guid);
            if (remove) return;//Nếu là xóa chứng từ thì ko tạo mới
            List<string> lstKeys = strLstKeys.Split(',').ToList();
            List<string> lstOtherKeys = strLstOtherKeys.Split('@').ToList();

            Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>();
            for (int i = 0; i < lstKeys.Count; i++)
                dictionary.Add(lstKeys[i].Trim(), lstOtherKeys[i].Split(',').ToList());
            //----------------------------------
            temp.SetProperty("ID", guid);
            temp.SetProperty("TypeID", keyPair[typeid]);
            foreach (string keyOfTemp in dictionary.Keys)
            {
                foreach (string str in dictionary[keyOfTemp].Where(str => @select.HasProperty(str.Trim())))
                {
                    temp.SetProperty(keyOfTemp, @select.GetProperty(str.Trim()));
                    break;
                }
            }
            #region Lưu loại đối tượng kế toán là Khách hàng
            var keys = new Dictionary<int, int>
                                                   {
                                                       { 321, 102 }, //(Hóa đơn bán hàng thu tiền ngay - Tiền mặt) - Phiếu thu từ bán hàng
                                                       { 324, 103 }, //(Hóa đơn bán hàng đại lý bán đúng giá, nhận ủy thác  XNK - Tiền mặt) - Phiếu thu từ bán hàng đại lý bán đúng giá, nhận ủy thác XNK
                                                   };
            //List<string> lstType = new List<string>
            //                           {
            //                               string.Format("{0};{1}",typeof(SAInvoice).Name, typeof(MCReceipt).Name),//Bán hàng - Phiếu thu
            //                           };
            foreach (var key in keys)
            {
                //var keys = item.Split(';').ToList();
                if (!keyPair.Contains(key)) continue;
                if (temp.HasProperty("AccountingObjectType"))
                    temp.SetProperty("AccountingObjectType", 0);
            }
            #endregion
            #region Lưu loại đối tượng kế toán là NCC
            keys = new Dictionary<int, int>
                                                   {
                                                       { 260, 117 }, //Mua hàng - phiếu chi từ mua hàng
                                                       { 119, 119 }, //Mua TSCĐ - Phiếu chi mua TSCĐ
                                                       { 116, 116 }, //Mua DV - phiếu chi mua dv
                                                       { 261, 127 }, //Mua hàng - ủy nhiệm chi từ mua hàng
                                                       { 129, 129 }, //Mua TSCĐ - Mua TSCĐ bằng Ủy nhiệm chi
                                                       { 126, 126 }, //Mua DV - Mua DV bằng Ủy nhiệm chi
                                                       { 262, 131 }, //Mua hàng - séc chuyển khoản từ mua hàng
                                                       { 132, 132 }, //Mua TSCĐ - Mua TSCĐ bằng Séc chuyển khoản
                                                       { 133, 133 }, //Mua DV - Mua dV bằng Séc chuyển khoản
                                                       { 263, 141 }, //Mua hàng - séc tiền mặt từ mua hàng
                                                       { 142, 142 }, //Mua TSCĐ - Mua TSCĐ bằng Séc TM
                                                       { 143, 143 }, //Mua DV - Mua DV bằng Séc TM
                                                       { 264, 171 }, //Mua hàng - thẻ tín dụng từ mua hàng
                                                       { 172, 172 }, //Mua TSCĐ - Mua TSCĐ bằng thẻ tín dụng
                                                       { 173, 173 }, //Mua DV - Mua DV bằng thẻ tín dụng
                                                       { 902, 902 }, //Mua CCDC - Phiếu chi mua CCDC
                                                       { 903, 903 }, //Mua CCDC - Mua CCDC bằng ủy nhiệm chi
                                                       { 904, 904 }, //Mua CCDC - Mua CCDC bằng Séc chuyển khoản
                                                       { 905, 905 }, //Mua CCDC - Mua CCDC bằng Séc tiền mặt
                                                       { 906, 906 }, //Mua CCDC - Mua CCDC bằng Thẻ tín dụng
                                                   };
            //lstType = new List<string> {
            //                               string.Format("{0};{1}",typeof(PPInvoice).Name,typeof (MCPayment).Name),//Mua hàng - Phiếu chi
            //                               string.Format("{0};{1}",typeof(FAIncrement).Name,typeof (MCPayment).Name),//Mua TSCĐ - Phiếu chi
            //                               string.Format("{0};{1}",typeof(PPInvoice).Name,typeof (MBTellerPaper).Name),//Mua hàng - Séc/UNC
            //                               string.Format("{0};{1}",typeof(FAIncrement).Name,typeof (MBTellerPaper).Name),//Mua TSCĐ - Séc/UNC
            //                               string.Format("{0};{1}",typeof(PPInvoice).Name,typeof (MBCreditCard).Name),//Mua hàng - Thẻ TD
            //                               string.Format("{0};{1}",typeof(FAIncrement).Name,typeof (MBCreditCard).Name),//Mua TSCĐ - Thẻ TD
            //                           };
            foreach (var key in keys)
            {
                //var keys = item.Split(';').ToList();
                if (!keyPair.Contains(key)) continue;
                if (temp.HasProperty("AccountingObjectType"))
                    temp.SetProperty("AccountingObjectType", 1);
            }
            #endregion

            baseService.CreateNew(temp);
            if (statusForm == ConstFrm.optStatusForm.Add) IGenCodeService.UpdateGenCodeForm((int)Utils.ListType.FirstOrDefault(p => p.ID.Equals(temp.GetProperty<TK, int>("TypeID"))).TypeGroupID, temp.GetProperty<TK, string>("No"), temp.GetProperty<TK, Guid>("ID"));
        }

        public bool GenVoucherOther<T>(T @select, T backSelect, int statusForm, bool remove = false)
        {
            bool status = true;
            try
            {
                #region check chứng từ móc
                if (statusForm == ConstFrm.optStatusForm.Add)
                {//Add chứng từ móc
                 //[Check key nhập kho, xuất kho]
                 //- có key và key có value thì tạo chứng từ và createnew

                    //[check typeID PC,UNC,SCK,STM,TTD]
                    //- nếu typeID là loại có kèm chứng từ móc thì tạo chứng từ móc và createnew

                    //[Check TH Kiểm kê quỹ]   
                    //[check TH bán hàng không kèm phiếu xuất kho]
                    #region update tỷ giá xuất quỹ
                    if (@select is GOtherVoucher && @select.GetProperty<T, int>("TypeID") == 660)
                    {
                        GOtherVoucher model = @select as GOtherVoucher;
                        if (model.GOtherVoucherDetails.Count > 0)
                        {
                            string accountTkcLechLai = Utils.ListSystemOption.FirstOrDefault(x => x.Code == "TCKHAC_TKCLechLai").Data;
                            string accountTkcLechLo = Utils.ListSystemOption.FirstOrDefault(x => x.Code == "TCKHAC_TKCLechLo").Data;
                            var fromdate = new DateTime(model.PostedDate.Year, model.PostedDate.Month, 1);
                            var lstUpdate = IGeneralLedgerService.Query.Where(c => (c.Account.StartsWith("1112") || c.Account.StartsWith("1122")) && c.PostedDate <= model.PostedDate && c.PostedDate >= fromdate).ToList();
                            foreach (var x in model.GOtherVoucherDetails)
                            {
                                #region PPInvoice
                                if (new int[] { 117, 127, 131, 141, 171, 260, 261, 262, 263, 264 }.Any(a => a == x.RefTypeID))
                                {
                                    var ppInvoice = IPPInvoiceService.Getbykey(x.RefExchangeID ?? Guid.Empty);
                                    if (ppInvoice != null)
                                    {
                                        foreach (var ppInvoiceDetail in ppInvoice.PPInvoiceDetails.ToList().Where(d => lstUpdate.Any(c => c.DetailID == d.ID)).ToList())
                                        {
                                            ppInvoiceDetail.CashOutExchangeRate = x.ExchangeRate;
                                            ppInvoiceDetail.CashOutAmount = (ppInvoiceDetail.AmountOriginal * x.ExchangeRate);
                                            ppInvoiceDetail.CashOutDifferAmount = Math.Abs(ppInvoiceDetail.Amount - ppInvoiceDetail.CashOutAmount);
                                            ppInvoiceDetail.CashOutDifferAccount = (ppInvoiceDetail.Amount > ppInvoiceDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                                            ppInvoiceDetail.CashOutVATAmount = (ppInvoiceDetail.VATAmountOriginal * x.ExchangeRate);
                                            ppInvoiceDetail.CashOutDifferVATAmount = Math.Abs(ppInvoiceDetail.VATAmount - ppInvoiceDetail.CashOutVATAmount);
                                            IPPInvoiceDetailService.Update(ppInvoiceDetail);
                                        }
                                    }
                                }
                                #endregion
                                #region PPService
                                if (new int[] { 116, 126, 133, 143, 173 }.Any(a => a == x.RefTypeID))
                                {
                                    var ppService = IPPServiceService.Getbykey(x.RefExchangeID ?? Guid.Empty);
                                    if (ppService != null)
                                    {
                                        foreach (var ppServiceDetail in ppService.PPServiceDetails.ToList().Where(d => lstUpdate.Any(c => c.DetailID == d.ID)).ToList())
                                        {
                                            ppServiceDetail.CashOutExchangeRate = x.ExchangeRate;
                                            ppServiceDetail.CashOutAmount = (ppServiceDetail.AmountOriginal ?? 0 * x.ExchangeRate);
                                            ppServiceDetail.CashOutDifferAmount = Math.Abs(ppServiceDetail.Amount ?? 0 - ppServiceDetail.CashOutAmount);
                                            ppServiceDetail.CashOutDifferAccount = (ppServiceDetail.Amount > ppServiceDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                                            ppServiceDetail.CashOutVATAmount = (ppServiceDetail.VATAmountOriginal ?? 0 * x.ExchangeRate);
                                            ppServiceDetail.CashOutDifferVATAmount = Math.Abs(ppServiceDetail.VATAmount ?? 0 - ppServiceDetail.CashOutVATAmount);
                                            IPPServiceDetailService.Update(ppServiceDetail);
                                        }
                                    }
                                }
                                #endregion
                                #region FAIncrement
                                if (new int[] { 119, 129, 132, 142, 172 }.Any(a => a == x.RefTypeID))
                                {
                                    var faIncrement = IFAIncrementService.Getbykey(x.RefExchangeID ?? Guid.Empty);
                                    if (faIncrement != null)
                                    {
                                        foreach (var faIncrementDetail in faIncrement.FAIncrementDetails.ToList().Where(d => lstUpdate.Any(c => c.DetailID == d.ID)).ToList())
                                        {
                                            faIncrementDetail.CashOutExchangeRate = x.ExchangeRate;
                                            faIncrementDetail.CashOutAmount = (faIncrementDetail.AmountOriginal * x.ExchangeRate);
                                            faIncrementDetail.CashOutDifferAmount = Math.Abs(faIncrementDetail.Amount - faIncrementDetail.CashOutAmount);
                                            faIncrementDetail.CashOutDifferAccount = (faIncrementDetail.Amount > faIncrementDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                                            faIncrementDetail.CashOutVATAmount = (faIncrementDetail.VATAmountOriginal * x.ExchangeRate);
                                            faIncrementDetail.CashOutDifferVATAmount = Math.Abs(faIncrementDetail.VATAmount - faIncrementDetail.CashOutVATAmount);
                                            IFAIncrementDetailService.Update(faIncrementDetail);
                                        }
                                    }
                                }
                                #endregion
                                #region TIIncrement
                                if (new int[] { 902, 903, 904, 905, 906 }.Any(a => a == x.RefTypeID))
                                {
                                    var tiIncrement = ITIIncrementService.Getbykey(x.RefExchangeID ?? Guid.Empty);
                                    if (tiIncrement != null)
                                    {
                                        foreach (var tiIncrementDetail in tiIncrement.TIIncrementDetails.ToList().Where(d => lstUpdate.Any(c => c.DetailID == d.ID)).ToList())
                                        {
                                            tiIncrementDetail.CashOutExchangeRate = x.ExchangeRate;
                                            tiIncrementDetail.CashOutAmount = (tiIncrementDetail.AmountOriginal * x.ExchangeRate);
                                            tiIncrementDetail.CashOutDifferAmount = Math.Abs(tiIncrementDetail.Amount - tiIncrementDetail.CashOutAmount);
                                            tiIncrementDetail.CashOutDifferAccount = (tiIncrementDetail.Amount > tiIncrementDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                                            tiIncrementDetail.CashOutVATAmount = (tiIncrementDetail.VATAmountOriginal * x.ExchangeRate);
                                            tiIncrementDetail.CashOutDifferVATAmount = Math.Abs(tiIncrementDetail.VATAmount - tiIncrementDetail.CashOutVATAmount);
                                            ITIIncrementDetailService.Update(tiIncrementDetail);
                                        }
                                    }
                                }
                                #endregion
                                #region SAReturn
                                if (new int[] { 330, 340 }.Any(a => a == x.RefTypeID))
                                {
                                    var saReturn = ISAReturnService.Getbykey(x.RefExchangeID ?? Guid.Empty);
                                    if (saReturn != null)
                                    {
                                        foreach (var saReturnDetail in saReturn.SAReturnDetails.ToList().Where(d => lstUpdate.Any(c => c.DetailID == d.ID)).ToList())
                                        {
                                            saReturnDetail.CashOutExchangeRate = x.ExchangeRate;
                                            saReturnDetail.CashOutAmount = (saReturnDetail.AmountOriginal * x.ExchangeRate);
                                            saReturnDetail.CashOutDifferAmount = Math.Abs(saReturnDetail.Amount - saReturnDetail.CashOutAmount);
                                            saReturnDetail.CashOutDifferAccount = (saReturnDetail.Amount > saReturnDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                                            saReturnDetail.CashOutVATAmount = (saReturnDetail.VATAmountOriginal * x.ExchangeRate);
                                            saReturnDetail.CashOutDifferVATAmount = Math.Abs(saReturnDetail.VATAmount - saReturnDetail.CashOutVATAmount);
                                            ISAReturnDetailService.Update(saReturnDetail);
                                        }
                                    }
                                }
                                #endregion
                                #region MCPayment
                                if (new int[] { 110 }.Any(a => a == x.RefTypeID))
                                {
                                    var mcPayment = IMCPaymentService.Getbykey(x.RefExchangeID ?? Guid.Empty);
                                    if (mcPayment != null)
                                    {
                                        foreach (var mcPaymentDetail in mcPayment.MCPaymentDetails.ToList().Where(d => lstUpdate.Any(c => c.DetailID == d.ID)).ToList())
                                        {
                                            mcPaymentDetail.CashOutExchangeRate = x.ExchangeRate;
                                            mcPaymentDetail.CashOutAmount = (mcPaymentDetail.AmountOriginal * x.ExchangeRate);
                                            mcPaymentDetail.CashOutDifferAmount = Math.Abs(mcPaymentDetail.Amount - mcPaymentDetail.CashOutAmount);
                                            mcPaymentDetail.CashOutDifferAccount = (mcPaymentDetail.Amount > mcPaymentDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                                            IMCPaymentDetailService.Update(mcPaymentDetail);
                                        }
                                    }
                                }
                                #endregion
                                #region MBTellerPaper
                                if (new int[] { 120, 130, 140 }.Any(a => a == x.RefTypeID))
                                {
                                    var mbTellerPaper = IMBTellerPaperService.Getbykey(x.RefExchangeID ?? Guid.Empty);
                                    if (mbTellerPaper != null)
                                    {
                                        foreach (var mbTellerPaperDetail in mbTellerPaper.MBTellerPaperDetails.ToList().Where(d => lstUpdate.Any(c => c.DetailID == d.ID)).ToList())
                                        {
                                            mbTellerPaperDetail.CashOutExchangeRate = x.ExchangeRate;
                                            mbTellerPaperDetail.CashOutAmount = (mbTellerPaperDetail.AmountOriginal * x.ExchangeRate);
                                            mbTellerPaperDetail.CashOutDifferAmount = Math.Abs(mbTellerPaperDetail.Amount - mbTellerPaperDetail.CashOutAmount);
                                            mbTellerPaperDetail.CashOutDifferAccount = (mbTellerPaperDetail.Amount > mbTellerPaperDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                                            IMBTellerPaperDetailService.Update(mbTellerPaperDetail);
                                        }
                                    }
                                }
                                #endregion
                                #region MBCreditCard
                                if (new int[] { 170 }.Any(a => a == x.RefTypeID))
                                {
                                    var mbCreditCard = IMBCreditCardService.Getbykey(x.RefExchangeID ?? Guid.Empty);
                                    if (mbCreditCard != null)
                                    {
                                        foreach (var mbCreditCardDetail in mbCreditCard.MBCreditCardDetails.ToList().Where(d => lstUpdate.Any(c => c.DetailID == d.ID)).ToList())
                                        {
                                            mbCreditCardDetail.CashOutExchangeRate = x.ExchangeRate;
                                            mbCreditCardDetail.CashOutAmount = (mbCreditCardDetail.AmountOriginal * x.ExchangeRate);
                                            mbCreditCardDetail.CashOutDifferAmount = Math.Abs(mbCreditCardDetail.Amount - mbCreditCardDetail.CashOutAmount);
                                            mbCreditCardDetail.CashOutDifferAccount = (mbCreditCardDetail.Amount > mbCreditCardDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                                            IMBCreditCardDetailService.Update(mbCreditCardDetail);
                                        }
                                    }
                                }
                                #endregion
                                #region MBInternalTransfer
                                if (new int[] { 150 }.Any(a => a == x.RefTypeID))
                                {
                                    var mbInternalTransfer = IMBInternalTransferService.Getbykey(x.RefExchangeID ?? Guid.Empty);
                                    if (mbInternalTransfer != null)
                                    {
                                        foreach (var mbInternalTransferDetail in mbInternalTransfer.MBInternalTransferDetails.ToList().Where(d => lstUpdate.Any(c => c.DetailID == d.ID)).ToList())
                                        {
                                            mbInternalTransferDetail.CashOutExchangeRate = x.ExchangeRate;
                                            mbInternalTransferDetail.CashOutAmount = (mbInternalTransferDetail.AmountOriginal * x.ExchangeRate);
                                            mbInternalTransferDetail.CashOutDifferAmount = Math.Abs(mbInternalTransferDetail.Amount - mbInternalTransferDetail.CashOutAmount);
                                            mbInternalTransferDetail.CashOutDifferAccount = (mbInternalTransferDetail.Amount > mbInternalTransferDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                                            IMBInternalTransferDetailService.Update(mbInternalTransferDetail);
                                        }
                                    }
                                }
                                #endregion
                                #region GOtherVoucher
                                if (new int[] { 600 }.Any(a => a == x.RefTypeID))
                                {
                                    var gotherVoucher = IGOtherVoucherService.Getbykey(x.RefExchangeID ?? Guid.Empty);
                                    if (gotherVoucher != null)
                                    {
                                        foreach (var gotherVoucherDetail in gotherVoucher.GOtherVoucherDetails.ToList().Where(d => lstUpdate.Any(c => c.DetailID == d.ID)).ToList())
                                        {
                                            gotherVoucherDetail.CashOutExchangeRate = x.ExchangeRate;
                                            gotherVoucherDetail.CashOutAmount = (gotherVoucherDetail.AmountOriginal * x.ExchangeRate);
                                            gotherVoucherDetail.CashOutDifferAmount = Math.Abs(gotherVoucherDetail.Amount - gotherVoucherDetail.CashOutAmount);
                                            gotherVoucherDetail.CashOutDifferAccount = (gotherVoucherDetail.Amount > gotherVoucherDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                                            IGOtherVoucherDetailService.Update(gotherVoucherDetail);
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                    #endregion
                    #region update chứng từ liên quan
                    if (@select is SAInvoice)
                    {
                        if (@select.HasProperty("ID") && @select.HasProperty("OutwardNo"))
                        {
                            var id = @select.GetProperty<T, Guid>("ID");
                            if (string.IsNullOrEmpty(@select.GetProperty<T, string>("OutwardNo")))
                            {
                                if (@select.HasProperty("ObjRsInwardOutwardIDs"))
                                {
                                    var guids = @select.GetProperty<T, List<Guid>>("ObjRsInwardOutwardIDs");
                                    foreach (var gId in guids)
                                    {
                                        var temp = IRSInwardOutwardService.Getbykey(gId);
                                        if (temp == null) continue;
                                        temp.SAInvoiceID = id;
                                        IRSInwardOutwardService.Update(temp);
                                    }
                                }
                            }
                        }
                        #region Tạo hóa đơn
                        //    var systemOption = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "EI_IDNhaCungCapDichVu");
                        //    if ((@select.GetProperty<T, bool>("IsBill") || @select.GetProperty<T, bool>("IsAttachListBill") && @select.GetProperty<T, bool>("Recorded")) && !string.IsNullOrEmpty(systemOption.Data))
                        //    {
                        //        SupplierService supplierService = ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                        //        if (supplierService.SupplierServiceCode == "VTE")
                        //        {
                        //            try
                        //            {
                        //                string UserPass = Utils.GetUserName() + ":" + Core.ITripleDes.TripleDesDefaultDecryption(Utils.GetPassWords());
                        //                string codeTax = Utils.GetCompanyTaxCode();
                        //                string apiLink = Utils.GetPathAccess()+ ":8443" + @"/InvoiceAPI/InvoiceWS/createOrUpdateInvoiceDraft/" + codeTax;
                        //                string autStr = CreateRequest.Base64Encode(UserPass);
                        //                string contentType = "application/json";

                        //                SAInvoice sAInvoice = select as SAInvoice;
                        //                AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sAInvoice.AccountingObjectID);
                        //                BankAccountDetail bankAccountDetail = Utils.ListBankAccountDetail.FirstOrDefault(n => n.ID == sAInvoice.BankAccountDetailID);


                        //                InvoiceInfo objInvoice = new InvoiceInfo();
                        //                objInvoice.uuId = sAInvoice.ID.ToString();
                        //                objInvoice.invoiceType = sAInvoice.InvoiceType.ToString();
                        //                objInvoice.templateCode = sAInvoice.InvoiceTemplate.ToString();
                        //                objInvoice.invoiceSeries = sAInvoice.InvoiceSeries.ToString();
                        //                if (sAInvoice.InvoiceDate != null)
                        //                {
                        //                    DateTime dt = sAInvoice.InvoiceDate ?? DateTime.Now;
                        //                    objInvoice.invoiceIssuedDate = dt.ToString("dd/MM/yyyy");
                        //                }
                        //                else
                        //                {
                        //                    objInvoice.invoiceIssuedDate = "";
                        //                }
                        //                objInvoice.currencyCode = sAInvoice.CurrencyID;
                        //                objInvoice.adjustmentType = "1";
                        //                objInvoice.paymentStatus = "true";
                        //                objInvoice.paymentType = sAInvoice.PaymentMethod;
                        //                objInvoice.paymentTypeName = sAInvoice.PaymentMethod;
                        //                objInvoice.cusGetInvoiceRight = "true";
                        //                objInvoice.buyerIdNo = "12320351130";
                        //                objInvoice.buyerIdType = "3";

                        //                BuyerInfo objBuyer = new BuyerInfo();
                        //                objBuyer.buyerAddressLine = sAInvoice.AccountingObjectAddress;
                        //                objBuyer.buyerIdNo = objInvoice.buyerIdNo;
                        //                objBuyer.buyerIdType = objInvoice.buyerIdType;
                        //                objBuyer.buyerName = sAInvoice.ContactName;
                        //                objBuyer.buyerPhoneNumber = accountingObject.ContactMobile;
                        //                objBuyer.buyerEmail = "tung996@gmail.com";
                        //                objBuyer.buyerBankName = "BIDV";
                        //                objBuyer.buyerTaxCode = "102698494633";
                        //                objBuyer.buyerBankAccount = ReportUtils.GetAccountingObjectBankAccount(sAInvoice.AccountingObjectID.ToString());

                        //                SellerInfo objSeller = new SellerInfo();
                        //                objSeller.sellerAddressLine = Utils.GetCompanyAddress();
                        //                objSeller.sellerBankAccount = Utils.GetBankAccount();
                        //                objSeller.sellerBankName = Utils.GetBankName();
                        //                objSeller.sellerEmail = "abc@gmail.com";
                        //                objSeller.sellerLegalName = "Quỳnh";
                        //                objSeller.sellerPhoneNumber = Utils.GetCompanyPhoneNumber();
                        //                objSeller.sellerTaxCode = codeTax;
                        //                string paymentMethodName = sAInvoice.PaymentMethod;

                        //                List<ItemInfo> lstItem = new List<ItemInfo>();
                        //                int lineNumber = 0;
                        //                foreach (SAInvoiceDetail itemD in sAInvoice.SAInvoiceDetails.CloneObject())
                        //                {
                        //                    ItemInfo item = new ItemInfo();
                        //                    MaterialGoods mg = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == itemD.MaterialGoodsID);
                        //                    item.discount = itemD.DiscountRate.ToString();
                        //                    item.itemCode = mg.MaterialGoodsCode;
                        //                    item.itemName = mg.MaterialGoodsName;
                        //                    item.itemTotalAmountWithoutTax = itemD.AmountOriginal.ToString();
                        //                    int Line = lineNumber += 1;
                        //                    item.lineNumber = Line.ToString();
                        //                    item.quantity = itemD.Quantity.ToString();
                        //                    item.taxAmount = itemD.VATAmountOriginal.ToString();
                        //                    item.taxPercentage = itemD.VATRate.ToString();
                        //                    item.unitName = itemD.Unit;
                        //                    item.unitPrice = itemD.UnitPrice.ToString();
                        //                    item.itemDiscount = itemD.itemDiscount.ToString();
                        //                    lstItem.Add(item);

                        //                }

                        //                SummarizeInfo objSummary = new SummarizeInfo();
                        //                objSummary.discountAmount = sAInvoice.TotalDiscountAmountOriginal.ToString();
                        //                objSummary.totalTaxAmount = sAInvoice.TotalVATAmountOriginal.ToString();
                        //                objSummary.taxPercentage = Convert.ToInt32(sAInvoice.MaxVATRate);
                        //                decimal t1 = (sAInvoice.TotalDiscountAmountOriginal * (decimal)objSummary.taxPercentage) / 100;
                        //                objSummary.settlementDiscountAmount = t1.ToString();
                        //                objSummary.sumOfTotalLineAmountWithoutTax = sAInvoice.TotalAmountOriginal.ToString();
                        //                decimal t2 = decimal.Parse(objSummary.sumOfTotalLineAmountWithoutTax) - decimal.Parse(objSummary.discountAmount);
                        //                objSummary.totalAmountWithoutTax = t2.ToString();
                        //                decimal t3 = decimal.Parse(objSummary.totalAmountWithoutTax) + sAInvoice.TotalVATAmountOriginal;
                        //                objSummary.totalAmountWithTax = t3.ToString();
                        //                if (objInvoice.currencyCode == "VND" || objInvoice.currencyCode == "JPY")
                        //                    objSummary.totalAmountWithTaxInWords = NumberToWord.GetAmountInWords(decimal.Parse(objSummary.totalAmountWithTax), objInvoice.currencyCode);


                        //                string request = @"{    ""generalInvoiceInfo"": " +
                        //@"{       ""invoiceType"":""" + objInvoice.invoiceType +
                        //@""",       ""templateCode"":""" + objInvoice.templateCode +
                        //@""", 	""invoiceSeries"":""" + objInvoice.invoiceSeries +
                        //@""",       ""transactionUuid"": """ + objInvoice.uuId +
                        //@""",       ""invoiceIssuedDate"":" + string.Format("\"{0}\"", objInvoice.invoiceIssuedDate) +
                        //@",       ""currencyCode"":""" + objInvoice.currencyCode + @""", 
                        //""adjustmentType"":""" + objInvoice.adjustmentType + @""",  
                        //""paymentStatus"":" + objInvoice.paymentStatus + @",       
                        //""paymentType"":""" + objInvoice.paymentType + @""",      
                        //""paymentTypeName"":""" + objInvoice.paymentTypeName + @""",       
                        //""cusGetInvoiceRight"":" + objInvoice.cusGetInvoiceRight + @",      
                        //""buyerIdNo"":""" + objInvoice.buyerIdNo + @""",        
                        //""buyerIdType"":""" + objInvoice.buyerIdType + @"""    },    
                        //""buyerInfo"":{       
                        //""buyerName"":""" + objBuyer.buyerName + @""",       
                        //""buyerLegalName"":""" + objBuyer.buyerLegalName + @""",       
                        //""buyerTaxCode"":""" + objBuyer.buyerTaxCode + @""",      
                        //""buyerAddressLine"":""" + objBuyer.buyerAddressLine + @""",       
                        //""buyerPhoneNumber"":""" + objBuyer.buyerPhoneNumber + @""",      
                        //""buyerEmail"":""" + objBuyer.buyerEmail + @""",      
                        //""buyerIdNo"":""" + objBuyer.buyerIdNo + @""",        
                        //""buyerIdType"":""" + objBuyer.buyerIdType + @"""    },    
                        //""sellerInfo"":{       
                        //""sellerLegalName"":""" + objSeller.sellerLegalName + @""",       
                        //""sellerTaxCode"":""" + objSeller.sellerTaxCode + @""",        
                        //""sellerAddressLine"":""" + objSeller.sellerAddressLine + @""",          
                        //""sellerPhoneNumber"":""" + objSeller.sellerPhoneNumber + @""",             
                        //""sellerEmail"":""" + objSeller.sellerEmail + @""",              
                        //""sellerBankName"":""" + objSeller.sellerBankName + @""",             
                        //""sellerBankAccount"":""" + objSeller.sellerBankAccount + @"""   },    
                        //""extAttribute"":[     ],    
                        //""payments"":[       {
                        //""paymentMethodName"":""" + paymentMethodName + @"""    }    ],    
                        //""deliveryInfo"":{     },    
                        //""itemInfo"":[       ";
                        //                int indexItem = 1;
                        //                foreach (var itemInfo in lstItem)
                        //                {
                        //                    request += @"{""lineNumber"":" + itemInfo.lineNumber + @",          
                        //""itemCode"":""" + itemInfo.itemCode + @""",    
                        //""itemName"":""" + itemInfo.itemName + @""",          
                        //""unitName"":""" + itemInfo.unitName + @""",     
                        //""unitPrice"":" + string.Format("\"{0}\"", itemInfo.unitPrice) + @",             
                        //""quantity"":" + string.Format("\"{0}\"", itemInfo.quantity) + @",               
                        //""itemTotalAmountWithoutTax"":" + string.Format("\"{0}\"", itemInfo.itemTotalAmountWithoutTax) + @",               
                        //""taxPercentage"":" + string.Format("\"{0}\"", itemInfo.taxPercentage) + @",              
                        //""taxAmount"":" + string.Format("\"{0}\"", itemInfo.taxAmount) + @",              
                        //""discount"":" + string.Format("\"{0}\"", itemInfo.discount) + @",                
                        //""itemDiscount"":" + string.Format("\"{0}\"", itemInfo.itemDiscount) + @"     }";
                        //                    if (indexItem > 1)
                        //                        request += " , ";
                        //                    indexItem++;
                        //                }
                        //                request += @"],    
                        //""discountItemInfo"":[     ],   
                        //""meterReading"": [{             
                        //""previousIndex"": ""5454"",             
                        //""currentIndex"": ""244"",             
                        //""factor"": ""22"",             
                        //""amount"": ""2""           },           
                        //{             ""previousIndex"": ""44"",             
                        //""currentIndex"": ""44"",             
                        //""factor"": ""33"",             
                        //""amount"": ""3""           }],    
                        //""summarizeInfo"":{       
                        //""sumOfTotalLineAmountWithoutTax"":" +  string.Format("\"{0}\"",objSummary.sumOfTotalLineAmountWithoutTax.ToString()) + @",          
                        //""totalAmountWithoutTax"":" + string.Format("\"{0}\"", objSummary.totalAmountWithoutTax.ToString()) + @",   
                        //""totalTaxAmount"":" + string.Format("\"{0}\"", objSummary.totalTaxAmount.ToString()) + @",  
                        //""totalAmountWithTax"":" + string.Format("\"{0}\"", objSummary.totalAmountWithTax.ToString()) + @", 
                        //""totalAmountWithTaxInWords"":""" + objSummary.totalAmountWithTaxInWords.ToString() + @""",       
                        //""discountAmount"":" + string.Format("\"{0}\"", objSummary.discountAmount.ToString()) + @",     
                        //""settlementDiscountAmount"":" + string.Format("\"{0}\"", objSummary.settlementDiscountAmount.ToString()) + @",    
                        //""taxPercentage"":" + objSummary.taxPercentage.ToString() + @"  },    
                        //""taxBreakdowns"":[       {          
                        //""taxPercentage"":10.0,          
                        //""taxableAmount"":250000,          
                        //""taxAmount"":25000.0       }    ] } ";
                        //                string result = CreateRequest.webRequest(apiLink, request, autStr, "POST", contentType);
                        //                MessageBox.Show("OK " + result);
                        //            }
                        //            catch (Exception ex)
                        //            {
                        //                MessageBox.Show("NOK " + ex.Message);
                        //            }

                        //        }

                        //    }
                        #endregion

                    }
                    //[check TH kho có kèm phiếu bán hàng không kèm phiếu xuất kho]
                    if (@select is RSInwardOutward)
                    {
                        if (@select.HasProperty("ID") && @select.HasProperty("IsOutwardSAInvoice"))
                        {
                            var id = @select.GetProperty<T, Guid>("ID");
                            if (@select.GetProperty<T, bool?>("IsOutwardSAInvoice") == true)
                            {
                                if (@select.HasProperty("SaInvoiceCustormIds"))
                                {
                                    var guids = @select.GetProperty<T, List<Guid>>("SaInvoiceCustormIds");
                                    foreach (var gId in guids)
                                    {
                                        var temp = ISAInvoiceService.Getbykey(gId);
                                        if (temp == null) continue;
                                        temp.OutwardRefID = id;
                                        ISAInvoiceService.Update(temp);
                                    }
                                }
                            }
                        }
                    }
                    if (@select is SABill)
                    {
                        if (@select.HasProperty("ID") && @select.HasProperty("SAIDs"))
                        {
                            var id = @select.GetProperty<T, Guid>("ID");

                            var guids = @select.GetProperty<T, List<Guid>>("SAIDs");
                            foreach (var gId in guids)
                            {
                                var temp = ISAInvoiceService.Getbykey(gId);
                                if (temp == null) continue;
                                temp.BillRefID = id;
                                temp.IsAttachListBill = true;
                                //temp.InvoiceForm = @select.GetProperty<T, int>("InvoiceForm");
                                //temp.InvoiceTypeID = @select.GetProperty<T, Guid>("InvoiceTypeID");
                                //temp.InvoiceTemplate = @select.GetProperty<T, string>("InvoiceTemplate");
                                //temp.InvoiceNo = @select.GetProperty<T, string>("InvoiceNo");
                                //temp.InvoiceSeries = @select.GetProperty<T, string>("InvoiceSeries");
                                //temp.InvoiceDate = @select.GetProperty<T, DateTime>("InvoiceDate");
                                //temp.PaymentMethod = @select.GetProperty<T, string>("PaymentMethod");
                                ISAInvoiceService.Update(temp);
                            }
                        }
                        #region Tạo hóa đơn
                        //var systemOption = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "EI_IDNhaCungCapDichVu");
                        //if (!string.IsNullOrEmpty(systemOption.Data))
                        //{
                        //    SupplierService supplierService = ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                        //    if (supplierService.SupplierServiceCode == "SDS")
                        //    {
                        //        RestApiService client = new RestApiService(Utils.GetPathAccess(), Utils.GetUserName(), Core.ITripleDes.TripleDesDefaultDecryption(Utils.GetPassWords()));
                        //        SABill sAInvoice = select as SABill;
                        //        var request = new Request();
                        //        request.Pattern = sAInvoice.InvoiceTemplate;
                        //        request.Serial = sAInvoice.InvoiceSeries;

                        //        EInvoices invoices = new EInvoices();
                        //        AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sAInvoice.AccountingObjectID);
                        //        BankAccountDetail bankAccountDetail = Utils.ListBankAccountDetail.FirstOrDefault(n => n.ID == sAInvoice.BankAccountDetailID);
                        //        Inv inv = new Inv();
                        //        inv.Key = sAInvoice.ID.ToString();
                        //        inv.Invoice.InvNo = "";
                        //        inv.Invoice.CusCode = accountingObject.AccountingObjectCode;
                        //        inv.Invoice.Buyer = sAInvoice.ContactName;
                        //        inv.Invoice.CusName = accountingObject.AccountingObjectName;
                        //        inv.Invoice.CusAddress = sAInvoice.AccountingObjectAddress;
                        //        inv.Invoice.CusBankName = bankAccountDetail != null ? bankAccountDetail.BankName : "";
                        //        inv.Invoice.CusBankNo = bankAccountDetail != null ? bankAccountDetail.BankAccount : "";
                        //        inv.Invoice.CusPhone = accountingObject.ContactMobile;
                        //        inv.Invoice.PaymentMethod = sAInvoice.PaymentMethod;
                        //        if (sAInvoice.InvoiceDate != null)
                        //        {
                        //            DateTime dt = sAInvoice.InvoiceDate ?? DateTime.Now;
                        //            inv.Invoice.ArisingDate = dt.ToString("dd/MM/yyyy");
                        //        }
                        //        else
                        //        {
                        //            inv.Invoice.ArisingDate = "";
                        //        }
                        //        inv.Invoice.ExchangeRate = sAInvoice.ExchangeRate;
                        //        inv.Invoice.CurrencyUnit = sAInvoice.CurrencyID;
                        //        foreach (var itemD in sAInvoice.SABillDetails.CloneObject())
                        //        {
                        //            Product prd = new Product();
                        //            MaterialGoods mg = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == itemD.MaterialGoodsID);
                        //            prd.Code = mg.MaterialGoodsCode;
                        //            prd.ProdName = mg.MaterialGoodsName;
                        //            prd.ProdUnit = itemD.Unit;
                        //            prd.ProdQuantity = itemD.Quantity != null ? Convert.ToInt32(itemD.Quantity) : 0;
                        //            prd.ProdPrice = itemD.UnitPriceOriginal;
                        //            prd.Total = itemD.AmountOriginal;
                        //            prd.VATRate = Convert.ToInt32(itemD.VATRate);
                        //            prd.VATAmount = (prd.Total * (decimal)prd.VATRate) / 100;
                        //            prd.Amount = itemD.AmountOriginal;
                        //            prd.Extra = "";

                        //            Product prdCK = new Product();
                        //            if (request.Pattern.Contains("02GTTT0"))
                        //            {
                        //                prd.Amount = prd.Total + prd.VATAmount;
                        //                prdCK.ProdName = "Chiết khấu";
                        //                prdCK.VATRate = Convert.ToInt32(itemD.VATRate);
                        //                prdCK.Total = -itemD.DiscountAmountOriginal;
                        //                prdCK.VATAmount = -(-prdCK.Total * (decimal)prdCK.VATRate) / 100;
                        //                prdCK.Amount = prdCK.Total + prdCK.VATAmount;

                        //            }
                        //            else
                        //            {
                        //                prdCK.ProdName = "Chiết khấu";
                        //                prdCK.VATRate = Convert.ToInt32(itemD.VATRate);
                        //                prdCK.Total = -itemD.DiscountAmountOriginal;
                        //                prdCK.VATAmount = -(-prdCK.Total * (decimal)prdCK.VATRate) / 100;
                        //                prdCK.Amount = prdCK.Total;
                        //            }
                        //            inv.Invoice.Products.Add(prd);
                        //            if (prdCK.Total != 0)
                        //            {
                        //                inv.Invoice.Products.Add(prdCK);
                        //            }
                        //        }
                        //        inv.Invoice.VATRate = inv.Invoice.Products.Count > 0 ? Convert.ToInt32(inv.Invoice.Products.First().VATRate).ToString() : "";
                        //        inv.Invoice.Amount = sAInvoice.TotalPaymentAmountOriginal;
                        //        inv.Invoice.AmountInWords = Utils.So_chu(sAInvoice.TotalPaymentAmountOriginal, sAInvoice.CurrencyID);
                        //        inv.Invoice.Total = sAInvoice.TotalAmountOriginal - sAInvoice.TotalDiscountAmountOriginal;
                        //        inv.Invoice.VATAmount = inv.Invoice.Products.Sum(n => n.VATAmount);
                        //        invoices.Invoices.Add(inv);

                        //        var xmldata = Utils.Serialize<EInvoices>(invoices);
                        //        XmlDocument xml = new XmlDocument();
                        //        xml.LoadXml(xmldata);
                        //        XmlNode xmldt = xml.SelectSingleNode("/EInvoices/Invoices");
                        //        request.XmlData = xmldt.OuterXml;

                        //        var response = client.Post(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "TaoHoaDonChuaKiSo").ApiPath, request);
                        //        if (response.Status != 2)
                        //        {
                        //            return false;
                        //        }
                        //    }
                        //}
                        #endregion
                    }
                    if (@select is SAReturn)
                    {
                        #region Tạo hóa đơn
                        //SAReturn sAInvoice = select as SAReturn;
                        //var systemOption = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "EI_IDNhaCungCapDichVu");
                        //if (!string.IsNullOrEmpty(systemOption.Data) && sAInvoice.InvoiceTemplate != null && !string.IsNullOrEmpty(sAInvoice.InvoiceTemplate) && sAInvoice.InvoiceSeries != null && !string.IsNullOrEmpty(sAInvoice.InvoiceSeries))
                        //{
                        //    SupplierService supplierService = ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                        //    if (supplierService.SupplierServiceCode == "SDS")
                        //    {
                        //        RestApiService client = new RestApiService(Utils.GetPathAccess(), Utils.GetUserName(), Core.ITripleDes.TripleDesDefaultDecryption(Utils.GetPassWords()));
                        //        var request = new Request();
                        //        request.Pattern = sAInvoice.InvoiceTemplate;
                        //        request.Serial = sAInvoice.InvoiceSeries;
                        //        EInvoices invoices = new EInvoices();
                        //        AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sAInvoice.AccountingObjectID);
                        //        BankAccountDetail bankAccountDetail = /*Utils.ListBankAccountDetail.FirstOrDefault(n => n.ID == sAInvoice.BankAccountDetailID)*/null;
                        //        Inv inv = new Inv();
                        //        inv.Key = sAInvoice.ID.ToString();
                        //        inv.Invoice.InvNo = "";
                        //        inv.Invoice.CusCode = accountingObject.AccountingObjectCode;
                        //        inv.Invoice.Buyer = sAInvoice.ContactName;
                        //        inv.Invoice.CusName = accountingObject.AccountingObjectName;
                        //        inv.Invoice.CusAddress = sAInvoice.AccountingObjectAddress;
                        //        inv.Invoice.CusBankName = bankAccountDetail != null ? bankAccountDetail.BankName : "";
                        //        inv.Invoice.CusBankNo = bankAccountDetail != null ? bankAccountDetail.BankAccount : "";
                        //        inv.Invoice.CusPhone = accountingObject.ContactMobile;
                        //        inv.Invoice.PaymentMethod = sAInvoice.PaymentMethod;
                        //        if (sAInvoice.InvoiceDate != null)
                        //        {
                        //            DateTime dt = sAInvoice.InvoiceDate ?? DateTime.Now;
                        //            inv.Invoice.ArisingDate = dt.ToString("dd/MM/yyyy");
                        //        }
                        //        else
                        //        {
                        //            inv.Invoice.ArisingDate = "";
                        //        }
                        //        inv.Invoice.ExchangeRate = sAInvoice.ExchangeRate;
                        //        inv.Invoice.CurrencyUnit = sAInvoice.CurrencyID;
                        //        foreach (var itemD in sAInvoice.SAReturnDetails.CloneObject())
                        //        {
                        //            Product prd = new Product();
                        //            MaterialGoods mg = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == itemD.MaterialGoodsID);
                        //            prd.Code = mg.MaterialGoodsCode;
                        //            prd.ProdName = mg.MaterialGoodsName;
                        //            prd.ProdUnit = itemD.Unit;
                        //            prd.ProdQuantity = itemD.Quantity != null ? Convert.ToInt32(itemD.Quantity) : 0;
                        //            prd.ProdPrice = itemD.UnitPriceOriginal;
                        //            prd.Total = itemD.AmountOriginal;
                        //            prd.VATRate = Convert.ToInt32(itemD.VATRate);
                        //            prd.VATAmount = (prd.Total * (decimal)prd.VATRate) / 100;
                        //            prd.Amount = itemD.AmountOriginal;
                        //            prd.Extra = "";

                        //            Product prdCK = new Product();
                        //            if (request.Pattern.Contains("02GTTT0"))
                        //            {
                        //                prd.Amount = prd.Total + prd.VATAmount;
                        //                prdCK.ProdName = "Chiết khấu";
                        //                prdCK.VATRate = Convert.ToInt32(itemD.VATRate);
                        //                prdCK.Total = -itemD.DiscountAmountOriginal;
                        //                prdCK.VATAmount = -(-prdCK.Total * (decimal)prdCK.VATRate) / 100;
                        //                prdCK.Amount = prdCK.Total + prdCK.VATAmount;

                        //            }
                        //            else
                        //            {
                        //                prdCK.ProdName = "Chiết khấu";
                        //                prdCK.VATRate = Convert.ToInt32(itemD.VATRate);
                        //                prdCK.Total = -itemD.DiscountAmountOriginal;
                        //                prdCK.VATAmount = -(-prdCK.Total * (decimal)prdCK.VATRate) / 100;
                        //                prdCK.Amount = prdCK.Total;
                        //            }
                        //            inv.Invoice.Products.Add(prd);
                        //            if (prdCK.Total != 0)
                        //            {
                        //                inv.Invoice.Products.Add(prdCK);
                        //            }
                        //        }
                        //        inv.Invoice.VATRate = inv.Invoice.Products.Count > 0 ? Convert.ToInt32(inv.Invoice.Products.First().VATRate).ToString() : "";
                        //        inv.Invoice.Amount = sAInvoice.TotalPaymentAmountOriginal;
                        //        inv.Invoice.AmountInWords = Utils.So_chu(sAInvoice.TotalPaymentAmountOriginal, sAInvoice.CurrencyID);
                        //        inv.Invoice.Total = sAInvoice.TotalAmountOriginal - sAInvoice.TotalDiscountAmountOriginal;
                        //        inv.Invoice.VATAmount = inv.Invoice.Products.Sum(n => n.VATAmount);
                        //        invoices.Invoices.Add(inv);

                        //        var xmldata = Utils.Serialize<EInvoices>(invoices);
                        //        XmlDocument xml = new XmlDocument();
                        //        xml.LoadXml(xmldata);
                        //        XmlNode xmldt = xml.SelectSingleNode("/EInvoices/Invoices");
                        //        request.XmlData = xmldt.OuterXml;

                        //        var response = client.Post(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "TaoHoaDonChuaKiSo").ApiPath, request);
                        //        if (response.Status != 2)
                        //        {
                        //            return false;
                        //        }
                        //    }
                        //}
                        #endregion
                    }
                    if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "HDDT").Data == "1")
                    {
                        SystemOption systemOption = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "EI_IDNhaCungCapDichVu");
                        if (@select.HasProperty("StatusInvoice") && @select.HasProperty("InvoiceForm"))
                            if (@select.GetProperty<T, int>("TypeID") == 326 || (@select.HasProperty("IsBill") && @select.GetProperty<T, bool>("IsBill")))
                            {
                                if ((@select.GetProperty<T, int>("StatusInvoice") == 0 || @select.GetProperty<T, int>("StatusInvoice") == 7 || @select.GetProperty<T, int>("StatusInvoice") == 8) && @select.GetProperty<T, int>("InvoiceForm") == 2)
                                    if (!string.IsNullOrEmpty(systemOption.Data))
                                    {
                                        SupplierService supplierService = Utils.ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                                        if (supplierService != null)
                                        {
                                            #region Tạo hóa đơn MIV
                                            if (supplierService.SupplierServiceCode == "MIV")
                                            {
                                                //request.Pattern = item.Key.GetProperty("Pattern").ToString();
                                                //request.Serial = item.Key.GetProperty("Serial").ToString();
                                                bool check = true;
                                                EInvoices invoices = new EInvoices();
                                                Invoice_MIV invoice_MIV = new Invoice_MIV();
                                                List<EInvoiceVietTel> lstEVT = new List<EInvoiceVietTel>();
                                                DataInvoice_MIV dataInvoice_MIV = new DataInvoice_MIV();
                                                SAInvoice sAInvoice = @select as SAInvoice;
                                                SABill sABill = @select as SABill;
                                                PPDiscountReturn pPDiscountReturn = @select as PPDiscountReturn;
                                                SAReturn sAReturn = @select as SAReturn;
                                                if (sAInvoice == null)
                                                {
                                                    //sABill = Utils.ListSABill.FirstOrDefault(n => n.ID == Eitem.ID);
                                                    if (sABill == null)
                                                    {

                                                        //sAReturn = Utils.ListSAReturn.FirstOrDefault(n => n.ID == Eitem.ID);
                                                        if (sAReturn == null)
                                                        {
                                                            //pPDiscountReturn = Utils.ListPPDiscountReturn.FirstOrDefault(n => n.ID == Eitem.ID);
                                                            AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == pPDiscountReturn.AccountingObjectID);
                                                            InvoiceType invoiceType = Utils.ListInvoiceType.FirstOrDefault(n => n.ID == pPDiscountReturn.InvoiceTypeID);
                                                            //inv.Invoice.InvNo = "";

                                                            invoice_MIV.editmode = "1";
                                                            if (invoiceType.InvoiceTypeCode.Equals("01GTKT"))
                                                            {
                                                                invoice_MIV.windowid = "WIN00187";
                                                            }
                                                            else if (invoiceType.InvoiceTypeCode.Equals("02GTTT"))
                                                            {
                                                                invoice_MIV.windowid = "WIN00189";
                                                            }
                                                            //dataInvoice_MIV.inv_invoiceSeries = item.Key.GetProperty("Serial").ToString();
                                                            dataInvoice_MIV.inv_invoiceSeries = pPDiscountReturn.InvoiceSeries;
                                                            if (pPDiscountReturn.InvoiceDate != null)
                                                            {
                                                                DateTime dt = pPDiscountReturn.InvoiceDate ?? DateTime.Now;
                                                                dataInvoice_MIV.inv_invoiceIssuedDate = dt.Date.ToString("yyyy-MM-dd");
                                                            }
                                                            else
                                                            {
                                                                dataInvoice_MIV.inv_invoiceIssuedDate = "";
                                                            }
                                                            dataInvoice_MIV.inv_currencyCode = pPDiscountReturn.CurrencyID;
                                                            if (dataInvoice_MIV.inv_currencyCode == "VND")
                                                            {
                                                                dataInvoice_MIV.inv_exchangeRate = 1;
                                                            }
                                                            else
                                                            {
                                                                dataInvoice_MIV.inv_exchangeRate = pPDiscountReturn.ExchangeRate ?? 0;
                                                            }
                                                            dataInvoice_MIV.inv_buyerDisplayName = pPDiscountReturn.OContactName ?? "";
                                                            dataInvoice_MIV.ma_dt = accountingObject.AccountingObjectCode;
                                                            dataInvoice_MIV.inv_buyerLegalName = pPDiscountReturn.AccountingObjectName ?? "";
                                                            dataInvoice_MIV.inv_buyerTaxCode = pPDiscountReturn.CompanyTaxCode;
                                                            dataInvoice_MIV.inv_buyerAddressLine = pPDiscountReturn.AccountingObjectAddress ?? "";
                                                            dataInvoice_MIV.inv_buyerEmail = accountingObject.Email;
                                                            dataInvoice_MIV.inv_buyerBankAccount = pPDiscountReturn.AccountingObjectBankAccount ?? "";
                                                            dataInvoice_MIV.inv_buyerBankName = pPDiscountReturn.AccountingObjectBankName ?? "";
                                                            dataInvoice_MIV.inv_paymentMethodName = pPDiscountReturn.PaymentMethod;
                                                            // dataInvoice_MIV.inv_sellerBankAccount = Utils.GetBankAccount();
                                                            //dataInvoice_MIV.inv_sellerBankName = Utils.GetBankName();
                                                            dataInvoice_MIV.mau_hd = pPDiscountReturn.InvoiceTemplate;

                                                            Details_MIV details_MIV = new Details_MIV();
                                                            int i = 0;
                                                            foreach (PPDiscountReturnDetail itemD in pPDiscountReturn.PPDiscountReturnDetails.OrderBy(n => n.OrderPriority).ToList())
                                                            {
                                                                i++;
                                                                MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == itemD.MaterialGoodsID);
                                                                if (invoiceType.InvoiceTypeCode.Equals("01GTKT"))
                                                                {
                                                                    details_MIV.tab_id = "TAB00188";
                                                                }
                                                                else if (invoiceType.InvoiceTypeCode.Equals("02GTTT"))
                                                                {
                                                                    details_MIV.tab_id = "TAB00192";
                                                                }
                                                                // thông tin hàng hóa
                                                                DataDetails_MIV dataDetails_MIV = new DataDetails_MIV();
                                                                dataDetails_MIV.stt_rec0 = i.ToString().PadLeft(8, '0');
                                                                dataDetails_MIV.inv_itemCode = materialGoods.MaterialGoodsCode ?? "";
                                                                dataDetails_MIV.inv_itemName = itemD.Description ?? "";
                                                                dataDetails_MIV.inv_unitName = itemD.Unit;
                                                                dataDetails_MIV.inv_unitCode = itemD.Unit;
                                                                if (dataInvoice_MIV.inv_currencyCode == "VND")
                                                                {
                                                                    dataDetails_MIV.inv_unitPrice = Math.Round(itemD.UnitPriceOriginal ?? 0, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaQuyDoi), MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    dataDetails_MIV.inv_unitPrice = Math.Round(itemD.UnitPriceOriginal ?? 0, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaNgoaiTe), MidpointRounding.AwayFromZero);
                                                                }
                                                                dataDetails_MIV.inv_quantity = itemD.Quantity ?? 0;
                                                                dataDetails_MIV.inv_TotalAmountWithoutVat = itemD.AmountOriginal ?? 0;
                                                                dataDetails_MIV.inv_vatAmount = itemD.VATAmountOriginal ?? 0;
                                                                dataDetails_MIV.inv_TotalAmount = itemD.AmountOriginal ?? 0 + itemD.VATAmountOriginal ?? 0;
                                                                dataDetails_MIV.ma_thue = itemD.VATRate != -2 ? itemD.VATRate ?? 0 : -1;
                                                                //dataDetails_MIV.inv_discountPercentage = itemD.d
                                                                details_MIV.data.Add(dataDetails_MIV);
                                                            }
                                                            dataInvoice_MIV.inv_vatAmount = pPDiscountReturn.TotalVATAmountOriginal;
                                                            dataInvoice_MIV.inv_discountAmount = pPDiscountReturn.TotalDiscountAmountOriginal;
                                                            dataInvoice_MIV.inv_TotalAmountWithoutVat = pPDiscountReturn.TotalAmountOriginal;
                                                            dataInvoice_MIV.inv_TotalAmount = pPDiscountReturn.TotalPaymentAmountOriginal;
                                                            dataInvoice_MIV.details.Add(details_MIV);
                                                        }
                                                        else
                                                        {
                                                            //sAReturn = Utils.ListSAReturn.FirstOrDefault(n => n.ID == Eitem.ID);
                                                            AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sAReturn.AccountingObjectID);
                                                            InvoiceType invoiceType = Utils.ListInvoiceType.FirstOrDefault(n => n.ID == sAReturn.InvoiceTypeID);
                                                            //inv.Invoice.InvNo = "";

                                                            invoice_MIV.editmode = "1";
                                                            if (invoiceType.InvoiceTypeCode.Equals("01GTKT"))
                                                            {
                                                                invoice_MIV.windowid = "WIN00187";
                                                            }
                                                            else if (invoiceType.InvoiceTypeCode.Equals("02GTTT"))
                                                            {
                                                                invoice_MIV.windowid = "WIN00189";
                                                            }

                                                            //dataInvoice_MIV.inv_invoiceSeries = item.Key.GetProperty("Serial").ToString();
                                                            dataInvoice_MIV.inv_invoiceSeries = sAReturn.InvoiceSeries;
                                                            if (sAReturn.InvoiceDate != null)
                                                            {
                                                                DateTime dt = sAReturn.InvoiceDate ?? DateTime.Now;
                                                                dataInvoice_MIV.inv_invoiceIssuedDate = dt.Date.ToString("yyyy-MM-dd");
                                                            }
                                                            else
                                                            {
                                                                dataInvoice_MIV.inv_invoiceIssuedDate = "";
                                                            }
                                                            dataInvoice_MIV.inv_currencyCode = sAReturn.CurrencyID;
                                                            if (dataInvoice_MIV.inv_currencyCode == "VND")
                                                            {
                                                                dataInvoice_MIV.inv_exchangeRate = 1;
                                                            }
                                                            else
                                                            {
                                                                dataInvoice_MIV.inv_exchangeRate = sAReturn.ExchangeRate ?? 0;
                                                            }
                                                            dataInvoice_MIV.inv_buyerDisplayName = sAReturn.ContactName;
                                                            dataInvoice_MIV.ma_dt = accountingObject.AccountingObjectCode;
                                                            dataInvoice_MIV.inv_buyerLegalName = sAReturn.AccountingObjectName ?? "";
                                                            dataInvoice_MIV.inv_buyerTaxCode = sAReturn.CompanyTaxCode;
                                                            dataInvoice_MIV.inv_buyerAddressLine = sAReturn.AccountingObjectAddress ?? "";
                                                            dataInvoice_MIV.inv_buyerEmail = accountingObject.Email;
                                                            dataInvoice_MIV.inv_buyerBankAccount = sAReturn.AccountingObjectBankAccount ?? "";
                                                            dataInvoice_MIV.inv_buyerBankName = sAReturn.AccountingObjectBankName ?? "";
                                                            dataInvoice_MIV.inv_paymentMethodName = sAReturn.PaymentMethod;
                                                            // dataInvoice_MIV.inv_sellerBankAccount = Utils.GetBankAccount();
                                                            //dataInvoice_MIV.inv_sellerBankName = Utils.GetBankName();
                                                            dataInvoice_MIV.mau_hd = sAReturn.InvoiceTemplate;

                                                            Details_MIV details_MIV = new Details_MIV();
                                                            int i = 0;
                                                            foreach (SAReturnDetail itemD in sAReturn.SAReturnDetails.OrderBy(n => n.OrderPriority).ToList())
                                                            {
                                                                i++;
                                                                MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == itemD.MaterialGoodsID);
                                                                if (invoiceType.InvoiceTypeCode.Equals("01GTKT"))
                                                                {
                                                                    details_MIV.tab_id = "TAB00188";
                                                                }
                                                                else if (invoiceType.InvoiceTypeCode.Equals("02GTTT"))
                                                                {
                                                                    details_MIV.tab_id = "TAB00192";
                                                                }
                                                                // thông tin hàng hóa
                                                                DataDetails_MIV dataDetails_MIV = new DataDetails_MIV();
                                                                dataDetails_MIV.stt_rec0 = i.ToString().PadLeft(8, '0');
                                                                dataDetails_MIV.inv_itemCode = materialGoods.MaterialGoodsCode ?? "";
                                                                dataDetails_MIV.inv_itemName = itemD.Description ?? "";
                                                                dataDetails_MIV.inv_unitName = itemD.Unit;
                                                                dataDetails_MIV.inv_unitCode = itemD.Unit;
                                                                if (dataInvoice_MIV.inv_currencyCode == "VND")
                                                                {
                                                                    dataDetails_MIV.inv_unitPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaQuyDoi), MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    dataDetails_MIV.inv_unitPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaNgoaiTe), MidpointRounding.AwayFromZero);
                                                                }
                                                                dataDetails_MIV.inv_quantity = itemD.Quantity ?? 0;
                                                                dataDetails_MIV.inv_TotalAmountWithoutVat = itemD.AmountOriginal;
                                                                dataDetails_MIV.inv_vatAmount = itemD.VATAmountOriginal;
                                                                dataDetails_MIV.inv_discountAmount = itemD.DiscountAmount;
                                                                dataDetails_MIV.inv_discountPercentage = itemD.DiscountRate ?? 0;
                                                                dataDetails_MIV.inv_TotalAmount = itemD.AmountOriginal + itemD.VATAmountOriginal - itemD.DiscountAmount;
                                                                dataDetails_MIV.ma_thue = itemD.VATRate != -2 ? itemD.VATRate ?? 0 : -1;
                                                                //dataDetails_MIV.inv_discountPercentage
                                                                details_MIV.data.Add(dataDetails_MIV);
                                                            }
                                                            dataInvoice_MIV.inv_vatAmount = sAReturn.TotalVATAmountOriginal;
                                                            dataInvoice_MIV.inv_discountAmount = sAReturn.TotalDiscountAmountOriginal;
                                                            dataInvoice_MIV.inv_TotalAmountWithoutVat = sAReturn.TotalAmountOriginal;
                                                            dataInvoice_MIV.inv_TotalAmount = sAReturn.TotalPaymentAmountOriginal;
                                                            dataInvoice_MIV.details.Add(details_MIV);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (sABill.Type == 2 && sABill.StatusInvoice == 8) // điều chỉnh tăng
                                                        {
                                                            check = false;
                                                        }
                                                        else if (sABill.Type == 3 && sABill.StatusInvoice == 8) // điều chỉnh giảm
                                                        {
                                                            check = false;
                                                        }
                                                        else if (sABill.Type == 4 && sABill.StatusInvoice == 8) // điều chỉnh thông tin
                                                        {
                                                            check = false;
                                                        }
                                                        else if (sABill.Type == 0 && sABill.StatusInvoice == 7) //Thay thế hóa đơn
                                                        {
                                                            check = false;
                                                        }
                                                        else
                                                        {
                                                            AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sABill.AccountingObjectID);

                                                            //sABill = Utils.ListSABill.FirstOrDefault(n => n.ID == Eitem.ID);
                                                            InvoiceType invoiceType = Utils.ListInvoiceType.FirstOrDefault(n => n.ID == sABill.InvoiceTypeID);
                                                            //inv.Invoice.InvNo = "";

                                                            invoice_MIV.editmode = "1";
                                                            if (invoiceType.InvoiceTypeCode.Equals("01GTKT"))
                                                            {
                                                                invoice_MIV.windowid = "WIN00187";
                                                            }
                                                            else if (invoiceType.InvoiceTypeCode.Equals("02GTTT"))
                                                            {
                                                                invoice_MIV.windowid = "WIN00189";
                                                            }

                                                            dataInvoice_MIV.inv_invoiceSeries = sABill.InvoiceSeries;
                                                            if (sABill.InvoiceDate != null)
                                                            {
                                                                DateTime dt = sABill.InvoiceDate ?? DateTime.Now;
                                                                dataInvoice_MIV.inv_invoiceIssuedDate = dt.Date.ToString("yyyy-MM-dd");
                                                            }
                                                            else
                                                            {
                                                                dataInvoice_MIV.inv_invoiceIssuedDate = "";
                                                            }
                                                            dataInvoice_MIV.inv_currencyCode = sABill.CurrencyID;
                                                            if (dataInvoice_MIV.inv_currencyCode == "VND")
                                                            {
                                                                dataInvoice_MIV.inv_exchangeRate = 1;
                                                            }
                                                            else
                                                            {
                                                                dataInvoice_MIV.inv_exchangeRate = sABill.ExchangeRate;
                                                            }
                                                            dataInvoice_MIV.inv_buyerDisplayName = sABill.ContactName ?? "";
                                                            dataInvoice_MIV.ma_dt = accountingObject.AccountingObjectCode;
                                                            dataInvoice_MIV.inv_buyerLegalName = sABill.AccountingObjectName ?? "";
                                                            dataInvoice_MIV.inv_buyerTaxCode = sABill.CompanyTaxCode;
                                                            dataInvoice_MIV.inv_buyerAddressLine = sABill.AccountingObjectAddress ?? "";
                                                            dataInvoice_MIV.inv_buyerEmail = accountingObject.Email;
                                                            dataInvoice_MIV.inv_buyerBankAccount = sABill.AccountingObjectBankAccount ?? "";
                                                            dataInvoice_MIV.inv_buyerBankName = sABill.AccountingObjectBankName ?? "";
                                                            dataInvoice_MIV.inv_paymentMethodName = sABill.PaymentMethod;
                                                            // dataInvoice_MIV.inv_sellerBankAccount = Utils.GetBankAccount();
                                                            //dataInvoice_MIV.inv_sellerBankName = Utils.GetBankName();
                                                            dataInvoice_MIV.mau_hd = sABill.InvoiceTemplate;

                                                            Details_MIV details_MIV = new Details_MIV();
                                                            int i = 0;
                                                            foreach (SABillDetail itemD in sABill.SABillDetails.OrderBy(n => n.OrderPriority).ToList())
                                                            {
                                                                i++;
                                                                MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == itemD.MaterialGoodsID);
                                                                if (invoiceType.InvoiceTypeCode.Equals("01GTKT"))
                                                                {
                                                                    details_MIV.tab_id = "TAB00188";
                                                                }
                                                                else if (invoiceType.InvoiceTypeCode.Equals("02GTTT"))
                                                                {
                                                                    details_MIV.tab_id = "TAB00192";
                                                                }
                                                                // thông tin hàng hóa
                                                                DataDetails_MIV dataDetails_MIV = new DataDetails_MIV();
                                                                dataDetails_MIV.stt_rec0 = i.ToString().PadLeft(8, '0');
                                                                dataDetails_MIV.inv_itemCode = materialGoods.MaterialGoodsCode ?? "";
                                                                dataDetails_MIV.inv_itemName = itemD.Description ?? "";
                                                                dataDetails_MIV.inv_unitName = itemD.Unit;
                                                                dataDetails_MIV.inv_unitCode = itemD.Unit;
                                                                if (dataInvoice_MIV.inv_currencyCode == "VND")
                                                                {
                                                                    dataDetails_MIV.inv_unitPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaQuyDoi), MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    dataDetails_MIV.inv_unitPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaNgoaiTe), MidpointRounding.AwayFromZero);
                                                                }
                                                                dataDetails_MIV.inv_quantity = itemD.Quantity;
                                                                dataDetails_MIV.inv_TotalAmountWithoutVat = itemD.AmountOriginal;
                                                                dataDetails_MIV.inv_vatAmount = itemD.VATAmountOriginal;
                                                                dataDetails_MIV.inv_discountAmount = itemD.DiscountAmount;
                                                                dataDetails_MIV.inv_discountPercentage = itemD.DiscountRate ?? 0;
                                                                dataDetails_MIV.inv_TotalAmount = itemD.AmountOriginal + itemD.VATAmountOriginal - itemD.DiscountAmount;
                                                                dataDetails_MIV.ma_thue = itemD.VATRate != -2 ? itemD.VATRate ?? 0 : -1;
                                                                //dataDetails_MIV.inv_discountPercentage
                                                                details_MIV.data.Add(dataDetails_MIV);
                                                            }
                                                            dataInvoice_MIV.inv_vatAmount = sABill.TotalVATAmountOriginal;
                                                            dataInvoice_MIV.inv_discountAmount = sABill.TotalDiscountAmountOriginal;
                                                            dataInvoice_MIV.inv_TotalAmountWithoutVat = sABill.TotalAmountOriginal;
                                                            dataInvoice_MIV.inv_TotalAmount = sABill.TotalPaymentAmountOriginal;
                                                            dataInvoice_MIV.details.Add(details_MIV);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sAInvoice.AccountingObjectID);

                                                    //sAInvoice = Utils.ListSAInvoice.FirstOrDefault(n => n.ID == Eitem.ID);
                                                    InvoiceType invoiceType = Utils.ListInvoiceType.FirstOrDefault(n => n.ID == sAInvoice.InvoiceTypeID);
                                                    //inv.Invoice.InvNo = "";

                                                    invoice_MIV.editmode = "1";
                                                    if (invoiceType.InvoiceTypeCode.Equals("01GTKT"))
                                                    {
                                                        invoice_MIV.windowid = "WIN00187";
                                                    }
                                                    else if (invoiceType.InvoiceTypeCode.Equals("02GTTT"))
                                                    {
                                                        invoice_MIV.windowid = "WIN00189";
                                                    }

                                                    dataInvoice_MIV.inv_invoiceSeries = sAInvoice.InvoiceSeries;
                                                    if (sAInvoice.InvoiceDate != null)
                                                    {
                                                        DateTime dt = sAInvoice.InvoiceDate ?? DateTime.Now;
                                                        dataInvoice_MIV.inv_invoiceIssuedDate = dt.Date.ToString("yyyy-MM-dd");
                                                    }
                                                    else
                                                    {
                                                        dataInvoice_MIV.inv_invoiceIssuedDate = "";
                                                    }
                                                    dataInvoice_MIV.inv_currencyCode = sAInvoice.CurrencyID;
                                                    if (dataInvoice_MIV.inv_currencyCode == "VND")
                                                    {
                                                        dataInvoice_MIV.inv_exchangeRate = 1;
                                                    }
                                                    else
                                                    {
                                                        dataInvoice_MIV.inv_exchangeRate = sAInvoice.ExchangeRate ?? 0;
                                                    }
                                                    dataInvoice_MIV.inv_buyerDisplayName = sAInvoice.ContactName ?? "";
                                                    dataInvoice_MIV.ma_dt = accountingObject.AccountingObjectCode;
                                                    dataInvoice_MIV.inv_buyerLegalName = sAInvoice.AccountingObjectName ?? "";
                                                    dataInvoice_MIV.inv_buyerTaxCode = sAInvoice.CompanyTaxCode;
                                                    dataInvoice_MIV.inv_buyerAddressLine = sAInvoice.AccountingObjectAddress ?? "";
                                                    dataInvoice_MIV.inv_buyerEmail = accountingObject.Email;
                                                    dataInvoice_MIV.inv_buyerBankAccount = sAInvoice.AccountingObjectBankAccount ?? "";
                                                    dataInvoice_MIV.inv_buyerBankName = sAInvoice.AccountingObjectBankName ?? "";
                                                    dataInvoice_MIV.inv_paymentMethodName = sAInvoice.PaymentMethod;
                                                    // dataInvoice_MIV.inv_sellerBankAccount = Utils.GetBankAccount();
                                                    //dataInvoice_MIV.inv_sellerBankName = Utils.GetBankName();
                                                    dataInvoice_MIV.mau_hd = sAInvoice.InvoiceTemplate;

                                                    Details_MIV details_MIV = new Details_MIV();
                                                    int i = 0;
                                                    foreach (SAInvoiceDetail itemD in sAInvoice.SAInvoiceDetails.OrderBy(n => n.OrderPriority).ToList())
                                                    {
                                                        i++;
                                                        MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == itemD.MaterialGoodsID);
                                                        if (invoiceType.InvoiceTypeCode.Equals("01GTKT"))
                                                        {
                                                            details_MIV.tab_id = "TAB00188";
                                                        }
                                                        else if (invoiceType.InvoiceTypeCode.Equals("02GTTT"))
                                                        {
                                                            details_MIV.tab_id = "TAB00192";
                                                        }
                                                        // thông tin hàng hóa
                                                        DataDetails_MIV dataDetails_MIV = new DataDetails_MIV();
                                                        dataDetails_MIV.stt_rec0 = i.ToString().PadLeft(8, '0');
                                                        dataDetails_MIV.inv_itemCode = materialGoods.MaterialGoodsCode ?? "";
                                                        dataDetails_MIV.inv_itemName = itemD.Description ?? "";
                                                        dataDetails_MIV.inv_unitName = itemD.Unit;
                                                        dataDetails_MIV.inv_unitCode = itemD.Unit;
                                                        if (dataInvoice_MIV.inv_currencyCode == "VND")
                                                        {
                                                            dataDetails_MIV.inv_unitPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaQuyDoi), MidpointRounding.AwayFromZero);
                                                        }
                                                        else
                                                        {
                                                            dataDetails_MIV.inv_unitPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaNgoaiTe), MidpointRounding.AwayFromZero);
                                                        }
                                                        dataDetails_MIV.inv_quantity = itemD.Quantity ?? 0;
                                                        dataDetails_MIV.inv_TotalAmountWithoutVat = itemD.AmountOriginal;
                                                        dataDetails_MIV.inv_vatAmount = itemD.VATAmountOriginal;
                                                        dataDetails_MIV.inv_discountAmount = itemD.DiscountAmount;
                                                        dataDetails_MIV.inv_discountPercentage = itemD.DiscountRate ?? 0;
                                                        dataDetails_MIV.inv_TotalAmount = itemD.AmountOriginal + itemD.VATAmountOriginal - itemD.DiscountAmount;
                                                        dataDetails_MIV.ma_thue = itemD.VATRate != -2 ? itemD.VATRate ?? 0 : -1;
                                                        //dataDetails_MIV.inv_discountPercentage
                                                        details_MIV.data.Add(dataDetails_MIV);
                                                    }

                                                    dataInvoice_MIV.inv_vatAmount = sAInvoice.TotalVATAmountOriginal;
                                                    dataInvoice_MIV.inv_discountAmount = sAInvoice.TotalDiscountAmountOriginal;
                                                    dataInvoice_MIV.inv_TotalAmountWithoutVat = sAInvoice.TotalAmountOriginal;
                                                    dataInvoice_MIV.inv_TotalAmount = sAInvoice.TotalPaymentAmountOriginal;
                                                    dataInvoice_MIV.details.Add(details_MIV);
                                                }
                                                if (check)
                                                {
                                                    invoice_MIV.data.Add(dataInvoice_MIV);
                                                    string jsonRequest = JsonConvert.SerializeObject(invoice_MIV, Newtonsoft.Json.Formatting.None,
                                                                                                        new JsonSerializerSettings
                                                                                                        {
                                                                                                            NullValueHandling = NullValueHandling.Ignore
                                                                                                        });
                                                    string contentType = "application/json";
                                                    string apiLink = Utils.GetPathAccess() + supplierService.ApiService.Where(n => n.SupplierServiceCode == "MIV").FirstOrDefault(n => n.ApiName == "TaoHoaDon").ApiPath;
                                                    string autStr = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "Token_MIV").Data + ";VP";
                                                    try
                                                    {
                                                        string result = CreateRequest.webRequest_MIV(apiLink, jsonRequest, autStr, "POST", contentType);
                                                        JObject jObject = JObject.Parse(result);
                                                        Respone_MIV respone_MIV = JsonConvert.DeserializeObject<Respone_MIV>(result);
                                                        if (respone_MIV.ok)
                                                        {
                                                            select.SetProperty("ID_MIV", respone_MIV.data.First().inv_InvoiceAuth_id);
                                                            select.SetProperty("InvoiceNo", respone_MIV.data.First().inv_invoiceNumber);
                                                            //string token = (string)jObject["token"];
                                                            //string error = (string)jObject["error"];
                                                        }
                                                        else
                                                        {
                                                            MSG.Error(respone_MIV.error);
                                                        }

                                                    }
                                                    catch (Exception ex)
                                                    {

                                                    }
                                                }
                                                else
                                                {
                                                }
                                            }
                                            #endregion
                                            #region Tạo hóa đơn SDS
                                            else if (supplierService.SupplierServiceCode == "SDS")
                                            {
                                                var request = new Request();
                                                request.Pattern = @select.GetProperty("InvoiceTemplate").ToString();
                                                request.Serial = @select.GetProperty("InvoiceSeries").ToString();

                                                EInvoices invoices = new EInvoices();

                                                Inv inv = new Inv();
                                                int check = 0;
                                                SAInvoice sAInvoice = @select as SAInvoice;
                                                SABill sABill = @select as SABill;
                                                PPDiscountReturn pPDiscountReturn = @select as PPDiscountReturn;
                                                SAReturn sAReturn = @select as SAReturn;
                                                if (sAInvoice == null)
                                                {
                                                    if (sABill == null)
                                                    {

                                                        if (sAReturn == null)
                                                        {
                                                            check = 3;
                                                            AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == pPDiscountReturn.AccountingObjectID);

                                                            inv.Key = pPDiscountReturn.ID.ToString();
                                                            inv.Invoice.InvNo = "";
                                                            inv.Invoice.CusCode = accountingObject.AccountingObjectCode ?? "";
                                                            inv.Invoice.Buyer = pPDiscountReturn.OContactName ?? "";
                                                            inv.Invoice.CusName = pPDiscountReturn.AccountingObjectName ?? "";
                                                            inv.Invoice.Email = accountingObject.Email ?? "";
                                                            inv.Invoice.CusAddress = pPDiscountReturn.AccountingObjectAddress ?? "";
                                                            inv.Invoice.CusBankName = pPDiscountReturn.AccountingObjectBankName ?? "";
                                                            inv.Invoice.CusBankNo = pPDiscountReturn.AccountingObjectBankAccount ?? "";
                                                            inv.Invoice.CusPhone = accountingObject.ContactMobile;
                                                            inv.Invoice.PaymentMethod = pPDiscountReturn.PaymentMethod;
                                                            inv.Invoice.CusTaxCode = pPDiscountReturn.CompanyTaxCode;
                                                            if (pPDiscountReturn.InvoiceDate != null)
                                                            {
                                                                DateTime dt = pPDiscountReturn.InvoiceDate ?? DateTime.Now;
                                                                inv.Invoice.ArisingDate = dt.Date.ToString("dd/MM/yyyy");
                                                            }
                                                            else
                                                            {
                                                                inv.Invoice.ArisingDate = "";
                                                            }
                                                            inv.Invoice.ExchangeRate = pPDiscountReturn.ExchangeRate;
                                                            inv.Invoice.CurrencyUnit = pPDiscountReturn.CurrencyID;
                                                            //inv.Invoice.Extra = Eitem.Extra;
                                                            foreach (PPDiscountReturnDetail itemD in pPDiscountReturn.PPDiscountReturnDetails.OrderBy(n => n.OrderPriority).ToList())
                                                            {
                                                                MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == itemD.MaterialGoodsID);
                                                                Product prd = new Product();
                                                                prd.Code = materialGoods.MaterialGoodsCode ?? "";
                                                                prd.ProdName = itemD.Description ?? "";
                                                                prd.ProdUnit = itemD.Unit;
                                                                prd.ProdQuantity = itemD.Quantity ?? 0;
                                                                //prd.ProdPrice = itemD.UnitPrice;
                                                                //prd.Total = itemD.Amount;
                                                                if (inv.Invoice.CurrencyUnit == "VND")
                                                                {
                                                                    prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal ?? 0, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaQuyDoi), MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal ?? 0, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaNgoaiTe), MidpointRounding.AwayFromZero);
                                                                }
                                                                prd.Total = itemD.AmountOriginal ?? 0;

                                                                prd.VATRate = itemD.VATRate != -2 ? Convert.ToInt32(itemD.VATRate) : -1;
                                                                if (prd.VATRate == -1)
                                                                {
                                                                    prd.VATAmount = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                    {
                                                                        prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                    }
                                                                    else
                                                                    {
                                                                        prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                    }
                                                                }
                                                                prd.Amount = prd.Total + prd.VATAmount;
                                                                if (!string.IsNullOrEmpty(itemD.LotNo) || itemD.ExpiryDate != null)
                                                                {
                                                                    prd.Extra = @"{""Lot"":""";
                                                                    prd.Extra += itemD.LotNo != null ? itemD.LotNo : "";
                                                                    if (itemD.ExpiryDate != null)
                                                                    {
                                                                        DateTime date = itemD.ExpiryDate ?? DateTime.Now;
                                                                        prd.Extra += @""",""ExpireDate"":""" + date.ToString("dd/MM/yyyy") + @"""}";
                                                                    }
                                                                    else
                                                                    {
                                                                        prd.Extra += @""",""ExpireDate"":""" + @"""}";
                                                                    }

                                                                }
                                                                else
                                                                {
                                                                    prd.Extra = "";
                                                                }

                                                                decimal sub = 0M;
                                                                var VATAmount = 0M;
                                                                if (prd.VATRate == -1)
                                                                {
                                                                    prd.VATAmount = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                    {
                                                                        VATAmount = Math.Round(((prd.Total) * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                    }
                                                                    else
                                                                    {
                                                                        VATAmount = Math.Round(((prd.Total) * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                    }
                                                                }
                                                                if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                {
                                                                    sub = Math.Round(itemD.VATAmountOriginal ?? 0, MidpointRounding.AwayFromZero) - VATAmount;
                                                                }
                                                                else
                                                                {
                                                                    sub = Math.Round(itemD.VATAmountOriginal ?? 0, 2, MidpointRounding.AwayFromZero) - VATAmount;
                                                                }


                                                                if (Math.Abs(sub) > 0)
                                                                {
                                                                    prd.VATAmount = prd.VATAmount + sub;

                                                                }

                                                                prd.Amount = prd.Total + prd.VATAmount;
                                                                if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                {
                                                                    prd.Total = Math.Round(itemD.AmountOriginal ?? 0, MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    prd.Total = itemD.AmountOriginal ?? 0;
                                                                }
                                                                inv.Invoice.Products.Add(prd);

                                                                //Product prdCK = new Product();
                                                                //prdCK.ProdName = "Chiết khấu";
                                                                //prdCK.VATRate = Convert.ToInt32(itemD.VATRate);
                                                                ////prdCK.Total = -itemD.DiscountAmount;
                                                                //prdCK.Total = -itemD.DiscountAmountOriginal??0;
                                                                //prdCK.VATAmount = -(-prdCK.Total * (decimal)prdCK.VATRate) / 100;
                                                                //prdCK.Amount = prdCK.Total + prdCK.VATAmount;


                                                                //if (prdCK.Total != 0)
                                                                //{
                                                                //    inv.Invoice.Products.Add(prdCK);
                                                                //}
                                                            }
                                                        }
                                                        else
                                                        {
                                                            check = 2;
                                                            AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sAReturn.AccountingObjectID);

                                                            inv.Key = sAReturn.ID.ToString();
                                                            inv.Invoice.InvNo = "";
                                                            inv.Invoice.CusCode = accountingObject.AccountingObjectCode ?? "";
                                                            inv.Invoice.Buyer = sAReturn.ContactName ?? "";
                                                            inv.Invoice.CusName = sAReturn.AccountingObjectName ?? "";
                                                            inv.Invoice.Email = accountingObject.Email ?? "";
                                                            inv.Invoice.CusAddress = sAReturn.AccountingObjectAddress ?? "";
                                                            inv.Invoice.CusBankName = sAReturn.AccountingObjectBankName ?? "";
                                                            inv.Invoice.CusBankNo = sAReturn.AccountingObjectBankAccount ?? "";
                                                            inv.Invoice.CusPhone = accountingObject.ContactMobile;
                                                            inv.Invoice.PaymentMethod = sAReturn.PaymentMethod;
                                                            inv.Invoice.CusTaxCode = sAReturn.CompanyTaxCode;
                                                            if (sAReturn.InvoiceDate != null)
                                                            {
                                                                DateTime dt = sAReturn.InvoiceDate ?? DateTime.Now;
                                                                inv.Invoice.ArisingDate = dt.Date.ToString("dd/MM/yyyy");
                                                            }
                                                            else
                                                            {
                                                                inv.Invoice.ArisingDate = "";
                                                            }
                                                            inv.Invoice.ExchangeRate = sAReturn.ExchangeRate;
                                                            inv.Invoice.CurrencyUnit = sAReturn.CurrencyID;
                                                            //inv.Invoice.Extra = Eitem.Extra;
                                                            foreach (SAReturnDetail itemD in sAReturn.SAReturnDetails.OrderBy(n => n.OrderPriority).ToList())
                                                            {
                                                                MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == itemD.MaterialGoodsID);
                                                                Product prd = new Product();
                                                                prd.Code = materialGoods.MaterialGoodsCode ?? "";
                                                                prd.ProdName = itemD.Description ?? "";
                                                                if (itemD.IsPromotion != null)
                                                                {
                                                                    if (itemD.IsPromotion ?? false)
                                                                    {
                                                                        prd.ProdName += " (Hàng khuyến mại)";
                                                                    }
                                                                }
                                                                prd.ProdUnit = itemD.Unit;
                                                                prd.ProdQuantity = itemD.Quantity ?? 0;
                                                                //prd.ProdPrice = itemD.UnitPrice;
                                                                //prd.Total = itemD.Amount;
                                                                if (inv.Invoice.CurrencyUnit == "VND")
                                                                {
                                                                    prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaQuyDoi), MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaNgoaiTe), MidpointRounding.AwayFromZero);
                                                                }
                                                                if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                {
                                                                    prd.Total = Math.Round(itemD.AmountOriginal, MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    prd.Total = itemD.AmountOriginal;
                                                                }

                                                                prd.VATRate = itemD.VATRate != -2 ? Convert.ToInt32(itemD.VATRate) : -1;
                                                                if (prd.VATRate == -1)
                                                                {
                                                                    prd.VATAmount = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                    {
                                                                        prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                    }
                                                                    else
                                                                    {
                                                                        prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                    }
                                                                }
                                                                prd.Amount = prd.Total + prd.VATAmount;
                                                                if (!string.IsNullOrEmpty(itemD.LotNo) || itemD.ExpiryDate != null)
                                                                {
                                                                    prd.Extra = @"{""Lot"":""";
                                                                    prd.Extra += itemD.LotNo != null ? itemD.LotNo : "";
                                                                    if (itemD.ExpiryDate != null)
                                                                    {
                                                                        DateTime date = itemD.ExpiryDate ?? DateTime.Now;
                                                                        prd.Extra += @""",""ExpireDate"":""" + date.ToString("dd/MM/yyyy") + @"""}";
                                                                    }
                                                                    else
                                                                    {
                                                                        prd.Extra += @""",""ExpireDate"":""" + @"""}";
                                                                    }

                                                                }
                                                                else
                                                                {
                                                                    prd.Extra = "";
                                                                }

                                                                Product prdCK = new Product();
                                                                prdCK.ProdName = "Chiết khấu";
                                                                prdCK.VATRate = itemD.VATRate != -2 ? Convert.ToInt32(itemD.VATRate) : -1;
                                                                //prdCK.Total = -itemD.DiscountAmount;
                                                                if (itemD.DiscountRate != null)
                                                                {
                                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                    {
                                                                        prdCK.Total = -Math.Round((prd.Total * itemD.DiscountRate ?? 0) / 100, MidpointRounding.AwayFromZero);
                                                                    }
                                                                    else
                                                                    {
                                                                        prdCK.Total = -Math.Round((prd.Total * itemD.DiscountRate ?? 0) / 100, 2, MidpointRounding.AwayFromZero);
                                                                    }
                                                                }
                                                                if (prdCK.VATRate == -1)
                                                                {
                                                                    prdCK.VATAmount = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                    {
                                                                        prdCK.VATAmount = Math.Round((prdCK.Total * (decimal)prdCK.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                    }
                                                                    else
                                                                    {
                                                                        prdCK.VATAmount = Math.Round((prdCK.Total * (decimal)prdCK.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                    }
                                                                }
                                                                prdCK.Amount = prdCK.Total + prdCK.VATAmount;

                                                                decimal sub = 0M;
                                                                if (itemD.DiscountAmountOriginal != 0M)
                                                                {
                                                                    var VATAmount = 0M;
                                                                    if (prd.VATRate == -1)
                                                                    {
                                                                        prd.VATAmount = 0;
                                                                    }
                                                                    else
                                                                    {
                                                                        if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                        {
                                                                            VATAmount = Math.Round(((prd.Total + prdCK.Total) * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                        }
                                                                        else
                                                                        {
                                                                            VATAmount = Math.Round(((prd.Total + prdCK.Total) * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                        }
                                                                    }
                                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                    {
                                                                        sub = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - VATAmount;
                                                                    }
                                                                    else
                                                                    {
                                                                        sub = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - VATAmount;
                                                                    }

                                                                }
                                                                else
                                                                {
                                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                    {
                                                                        sub = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                    }
                                                                    else
                                                                    {
                                                                        sub = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                    }
                                                                }

                                                                if (Math.Abs(sub) > 0)
                                                                {
                                                                    prd.VATAmount = prd.VATAmount + sub;

                                                                }
                                                                if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                {
                                                                    prdCK.VATAmount = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                }
                                                                else
                                                                {
                                                                    prdCK.VATAmount = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                }
                                                                prdCK.Amount = prdCK.Total + prdCK.VATAmount;
                                                                prd.Amount = prd.Total + prd.VATAmount;

                                                                inv.Invoice.Products.Add(prd);
                                                                if (prdCK.Total != 0)
                                                                {
                                                                    inv.Invoice.Products.Add(prdCK);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        check = 1;
                                                        AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sABill.AccountingObjectID);

                                                        inv.Key = sABill.ID.ToString();
                                                        inv.Invoice.InvNo = "";
                                                        inv.Invoice.CusCode = accountingObject.AccountingObjectCode ?? "";
                                                        inv.Invoice.Buyer = sABill.ContactName ?? "";
                                                        inv.Invoice.CusName = sABill.AccountingObjectName ?? "";
                                                        inv.Invoice.Email = accountingObject.Email ?? "";
                                                        inv.Invoice.CusAddress = sABill.AccountingObjectAddress;
                                                        inv.Invoice.CusBankName = sABill.AccountingObjectBankName ?? "";
                                                        inv.Invoice.CusBankNo = sABill.AccountingObjectBankAccount ?? "";
                                                        inv.Invoice.CusPhone = accountingObject.ContactMobile;
                                                        inv.Invoice.PaymentMethod = sABill.PaymentMethod;
                                                        inv.Invoice.CusTaxCode = sABill.CompanyTaxCode;
                                                        if (sABill.InvoiceDate != null)
                                                        {
                                                            DateTime dt = sABill.InvoiceDate ?? DateTime.Now;
                                                            inv.Invoice.ArisingDate = dt.Date.ToString("dd/MM/yyyy");
                                                        }
                                                        else
                                                        {
                                                            inv.Invoice.ArisingDate = "";
                                                        }
                                                        inv.Invoice.ExchangeRate = sABill.ExchangeRate;
                                                        inv.Invoice.CurrencyUnit = sABill.CurrencyID;
                                                        //inv.Invoice.Extra = Eitem.Extra;
                                                        foreach (SABillDetail itemD in sABill.SABillDetails.OrderBy(n => n.OrderPriority).ToList())
                                                        {
                                                            MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == itemD.MaterialGoodsID);
                                                            Product prd = new Product();
                                                            prd.Code = materialGoods.MaterialGoodsCode;
                                                            prd.ProdName = itemD.Description;
                                                            if (itemD.IsPromotion != null)
                                                            {
                                                                if (itemD.IsPromotion ?? false)
                                                                {
                                                                    prd.ProdName += " (Hàng khuyến mại)";
                                                                }
                                                            }
                                                            prd.ProdUnit = itemD.Unit;
                                                            prd.ProdQuantity = itemD.Quantity;
                                                            if (inv.Invoice.CurrencyUnit == "VND")
                                                            {
                                                                prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaQuyDoi), MidpointRounding.AwayFromZero);
                                                            }
                                                            else
                                                            {
                                                                prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaNgoaiTe), MidpointRounding.AwayFromZero);
                                                            }

                                                            prd.VATRate = itemD.VATRate != -2 ? Convert.ToInt32(itemD.VATRate) : -1;
                                                            //prd.VATRate = itemD.VATRate;
                                                            if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                            {
                                                                prd.Total = Math.Round(itemD.AmountOriginal, MidpointRounding.AwayFromZero);
                                                            }
                                                            else
                                                            {
                                                                prd.Total = itemD.AmountOriginal;
                                                            }

                                                            if (prd.VATRate == -1)
                                                            {
                                                                prd.VATAmount = 0;
                                                            }
                                                            else
                                                            {
                                                                if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                {
                                                                    prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                }
                                                            }
                                                            prd.Amount = prd.Total + prd.VATAmount;
                                                            if (!string.IsNullOrEmpty(itemD.LotNo) || itemD.ExpiryDate != null)
                                                            {
                                                                prd.Extra = @"{""Lot"":""";
                                                                prd.Extra += itemD.LotNo != null ? itemD.LotNo : "";
                                                                if (itemD.ExpiryDate != null)
                                                                {
                                                                    DateTime date = itemD.ExpiryDate ?? DateTime.Now;
                                                                    prd.Extra += @""",""ExpireDate"":""" + date.ToString("dd/MM/yyyy") + @"""}";
                                                                }
                                                                else
                                                                {
                                                                    prd.Extra += @""",""ExpireDate"":""" + @"""}";
                                                                }

                                                            }
                                                            else
                                                            {
                                                                prd.Extra = "";
                                                            }

                                                            Product prdCK = new Product();
                                                            prdCK.ProdName = "Chiết khấu";
                                                            prdCK.VATRate = itemD.VATRate != -2 ? Convert.ToInt32(itemD.VATRate) : -1;
                                                            //prdCK.Total = -itemD.DiscountAmount;
                                                            if (itemD.DiscountRate != null)
                                                            {
                                                                if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                {
                                                                    prdCK.Total = -Math.Round((prd.Total * (decimal)itemD.DiscountRate) / 100, MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    prdCK.Total = -Math.Round((prd.Total * (decimal)itemD.DiscountRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                }
                                                            }
                                                            if (prdCK.VATRate == -1)
                                                            {
                                                                prdCK.VATAmount = 0;
                                                            }
                                                            else
                                                            {
                                                                if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                {
                                                                    prdCK.VATAmount = Math.Round((prdCK.Total * (decimal)prdCK.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    prdCK.VATAmount = Math.Round((prdCK.Total * (decimal)prdCK.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                }

                                                            }
                                                            prdCK.Amount = prdCK.Total + prdCK.VATAmount;

                                                            decimal sub = 0M;
                                                            if (itemD.DiscountAmountOriginal != 0M)
                                                            {
                                                                var VATAmount = 0M;
                                                                if (prd.VATRate == -1)
                                                                {
                                                                    prd.VATAmount = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                    {
                                                                        VATAmount = Math.Round(((prd.Total + prdCK.Total) * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                    }
                                                                    else
                                                                    {
                                                                        VATAmount = Math.Round(((prd.Total + prdCK.Total) * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                    }
                                                                }
                                                                if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                {
                                                                    sub = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - VATAmount;
                                                                }
                                                                else
                                                                {
                                                                    sub = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - VATAmount;
                                                                }

                                                            }
                                                            else
                                                            {
                                                                if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                {
                                                                    sub = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                }
                                                                else
                                                                {
                                                                    sub = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                }
                                                            }

                                                            if (Math.Abs(sub) > 0)
                                                            {
                                                                prd.VATAmount = prd.VATAmount + sub;

                                                            }
                                                            if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                            {
                                                                prdCK.VATAmount = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                            }
                                                            else
                                                            {
                                                                prdCK.VATAmount = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                            }
                                                            prdCK.Amount = prdCK.Total + prdCK.VATAmount;
                                                            prd.Amount = prd.Total + prd.VATAmount;

                                                            inv.Invoice.Products.Add(prd);
                                                            if (prdCK.Total != 0)
                                                            {
                                                                inv.Invoice.Products.Add(prdCK);
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    check = 0;
                                                    AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sAInvoice.AccountingObjectID);

                                                    inv.Key = sAInvoice.ID.ToString();
                                                    inv.Invoice.InvNo = "";
                                                    inv.Invoice.CusCode = accountingObject.AccountingObjectCode ?? "";
                                                    inv.Invoice.Buyer = sAInvoice.ContactName ?? "";
                                                    inv.Invoice.CusName = sAInvoice.AccountingObjectName ?? "";
                                                    inv.Invoice.Email = accountingObject.Email ?? "";
                                                    inv.Invoice.CusAddress = sAInvoice.AccountingObjectAddress ?? "";
                                                    inv.Invoice.CusBankName = sAInvoice.AccountingObjectBankName ?? "";
                                                    inv.Invoice.CusBankNo = sAInvoice.AccountingObjectBankAccount ?? "";
                                                    inv.Invoice.CusPhone = accountingObject.ContactMobile;
                                                    inv.Invoice.CusTaxCode = sAInvoice.CompanyTaxCode;
                                                    inv.Invoice.PaymentMethod = sAInvoice.PaymentMethod;
                                                    if (sAInvoice.InvoiceDate != null)
                                                    {
                                                        DateTime dt = sAInvoice.InvoiceDate ?? DateTime.Now;
                                                        inv.Invoice.ArisingDate = dt.Date.ToString("dd/MM/yyyy");
                                                    }
                                                    else
                                                    {
                                                        inv.Invoice.ArisingDate = "";
                                                    }
                                                    inv.Invoice.ExchangeRate = sAInvoice.ExchangeRate;
                                                    inv.Invoice.CurrencyUnit = sAInvoice.CurrencyID;
                                                    //inv.Invoice.Extra = Eitem.Extra;
                                                    foreach (SAInvoiceDetail itemD in sAInvoice.SAInvoiceDetails.OrderBy(n => n.OrderPriority).ToList())
                                                    {
                                                        MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == itemD.MaterialGoodsID);
                                                        Product prd = new Product();
                                                        prd.Code = materialGoods.MaterialGoodsCode ?? "";
                                                        prd.ProdName = itemD.Description ?? "";
                                                        if (itemD.IsPromotion != null)
                                                        {
                                                            if (itemD.IsPromotion ?? false)
                                                            {
                                                                prd.ProdName += " (Hàng khuyến mại)";
                                                            }
                                                        }
                                                        prd.ProdUnit = itemD.Unit;
                                                        prd.ProdQuantity = itemD.Quantity ?? 0;
                                                        if (inv.Invoice.CurrencyUnit == "VND")
                                                        {
                                                            prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaQuyDoi), MidpointRounding.AwayFromZero);
                                                        }
                                                        else
                                                        {
                                                            prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaNgoaiTe), MidpointRounding.AwayFromZero);
                                                        }
                                                        if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                        {
                                                            prd.Total = Math.Round(itemD.AmountOriginal, MidpointRounding.AwayFromZero);
                                                        }
                                                        else
                                                        {
                                                            prd.Total = itemD.AmountOriginal;
                                                        }

                                                        prd.VATRate = itemD.VATRate != -2 ? Convert.ToInt32(itemD.VATRate) : -1;
                                                        if (prd.VATRate == -1)
                                                        {
                                                            prd.VATAmount = 0;
                                                        }
                                                        else
                                                        {
                                                            if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                            {
                                                                prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                            }
                                                            else
                                                            {
                                                                prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                            }

                                                        }
                                                        prd.Amount = prd.Total + prd.VATAmount;
                                                        if (!string.IsNullOrEmpty(itemD.LotNo) || itemD.ExpiryDate != null)
                                                        {
                                                            prd.Extra = @"{""Lot"":""";
                                                            prd.Extra += itemD.LotNo != null ? itemD.LotNo : "";
                                                            if (itemD.ExpiryDate != null)
                                                            {
                                                                DateTime date = itemD.ExpiryDate ?? DateTime.Now;
                                                                prd.Extra += @""",""ExpireDate"":""" + date.ToString("dd/MM/yyyy") + @"""}";
                                                            }
                                                            else
                                                            {
                                                                prd.Extra += @""",""ExpireDate"":""" + @"""}";
                                                            }

                                                        }
                                                        else
                                                        {
                                                            prd.Extra = "";
                                                        }
                                                        // Chiết khấu
                                                        Product prdCK = new Product();
                                                        prdCK.ProdName = "Chiết khấu";
                                                        prdCK.VATRate = itemD.VATRate != -2 ? Convert.ToInt32(itemD.VATRate) : -1;
                                                        if (itemD.DiscountRate != null)
                                                        {
                                                            if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                            {
                                                                prdCK.Total = -Math.Round((prd.Total * itemD.DiscountRate ?? 0) / 100, MidpointRounding.AwayFromZero);
                                                            }
                                                            else
                                                            {
                                                                prdCK.Total = -Math.Round((prd.Total * itemD.DiscountRate ?? 0) / 100, 2, MidpointRounding.AwayFromZero);
                                                            }
                                                        }

                                                        if (prdCK.VATRate == -1)
                                                        {
                                                            prdCK.VATAmount = 0;
                                                        }
                                                        else
                                                        {
                                                            if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                            {
                                                                prdCK.VATAmount = Math.Round((prdCK.Total * (decimal)prdCK.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                            }
                                                            else
                                                            {
                                                                prdCK.VATAmount = Math.Round((prdCK.Total * (decimal)prdCK.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                            }

                                                        }
                                                        prdCK.Amount = prdCK.Total + prdCK.VATAmount;

                                                        decimal sub = 0M;
                                                        if (itemD.DiscountAmountOriginal != 0M)
                                                        {
                                                            var VATAmount = 0M;
                                                            if (prd.VATRate == -1)
                                                            {
                                                                prd.VATAmount = 0;
                                                            }
                                                            else
                                                            {
                                                                if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                {
                                                                    VATAmount = Math.Round(((prd.Total + prdCK.Total) * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    VATAmount = Math.Round(((prd.Total + prdCK.Total) * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                }
                                                            }
                                                            if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                            {
                                                                sub = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - VATAmount;
                                                            }
                                                            else
                                                            {
                                                                sub = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - VATAmount;
                                                            }

                                                        }
                                                        else
                                                        {
                                                            if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                            {
                                                                sub = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                            }
                                                            else
                                                            {
                                                                sub = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                            }
                                                        }

                                                        if (Math.Abs(sub) > 0)
                                                        {
                                                            prd.VATAmount = prd.VATAmount + sub;

                                                        }
                                                        if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                        {
                                                            prdCK.VATAmount = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                        }
                                                        else
                                                        {
                                                            prdCK.VATAmount = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                        }
                                                        prdCK.Amount = prdCK.Total + prdCK.VATAmount;
                                                        prd.Amount = prd.Total + prd.VATAmount;
                                                        //prdCK.Total = -itemD.DiscountAmount;

                                                        inv.Invoice.Products.Add(prd);
                                                        if (prdCK.Total != 0)
                                                        {
                                                            inv.Invoice.Products.Add(prdCK);
                                                        }
                                                    }

                                                }
                                                #region code mới
                                                if (check == 0)
                                                {
                                                    inv.Invoice.VATRate = inv.Invoice.Products.Count > 0 ? Convert.ToInt32(inv.Invoice.Products.First().VATRate).ToString() : "-1";

                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                    {
                                                        inv.Invoice.Amount = Math.Round(inv.Invoice.Products.Sum(n => n.Amount), MidpointRounding.AwayFromZero);
                                                        inv.Invoice.AmountInWords = NumberToWord.GetAmountInWords(inv.Invoice.Amount, sAInvoice.CurrencyID);
                                                        inv.Invoice.Total = Math.Round(inv.Invoice.Products.Sum(n => n.Total), MidpointRounding.AwayFromZero);
                                                        inv.Invoice.VATAmount = Math.Round(inv.Invoice.Products.Sum(n => n.VATAmount), MidpointRounding.AwayFromZero);
                                                    }
                                                    else
                                                    {
                                                        inv.Invoice.Amount = Math.Round(inv.Invoice.Products.Sum(n => n.Amount), 2, MidpointRounding.AwayFromZero);
                                                        if (inv.Invoice.CurrencyUnit == "USD" || inv.Invoice.CurrencyUnit == "EUR" || inv.Invoice.CurrencyUnit == "GBP" || inv.Invoice.CurrencyUnit == "SGD")
                                                            inv.Invoice.AmountInWords = NumberToWord.GetAmountInWords(inv.Invoice.Amount, sAInvoice.CurrencyID);
                                                        else
                                                            inv.Invoice.AmountInWords = Utils.So_chu(inv.Invoice.Amount, sAInvoice.CurrencyID);
                                                        inv.Invoice.Total = Math.Round(inv.Invoice.Products.Sum(n => n.Total), 2, MidpointRounding.AwayFromZero);
                                                        inv.Invoice.VATAmount = Math.Round(inv.Invoice.Products.Sum(n => n.VATAmount), 2, MidpointRounding.AwayFromZero);
                                                    }
                                                    invoices.Invoices.Add(inv);
                                                }
                                                else if (check == 1)
                                                {
                                                    inv.Invoice.VATRate = inv.Invoice.Products.Count > 0 ? Convert.ToInt32(inv.Invoice.Products.First().VATRate).ToString() : "-1";
                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                    {
                                                        inv.Invoice.Amount = Math.Round(inv.Invoice.Products.Sum(n => n.Amount), MidpointRounding.AwayFromZero);
                                                        inv.Invoice.AmountInWords = NumberToWord.GetAmountInWords(Math.Round(inv.Invoice.Amount, MidpointRounding.AwayFromZero), sABill.CurrencyID);
                                                        inv.Invoice.Total = Math.Round(inv.Invoice.Products.Sum(n => n.Total), MidpointRounding.AwayFromZero);
                                                        inv.Invoice.VATAmount = Math.Round(inv.Invoice.Products.Sum(n => n.VATAmount), MidpointRounding.AwayFromZero);
                                                    }
                                                    else
                                                    {
                                                        inv.Invoice.Amount = Math.Round(inv.Invoice.Products.Sum(n => n.Amount), 2, MidpointRounding.AwayFromZero);
                                                        if (inv.Invoice.CurrencyUnit == "USD" || inv.Invoice.CurrencyUnit == "EUR" || inv.Invoice.CurrencyUnit == "GBP" || inv.Invoice.CurrencyUnit == "SGD")
                                                            inv.Invoice.AmountInWords = NumberToWord.GetAmountInWords(Math.Round(inv.Invoice.Amount, 2, MidpointRounding.AwayFromZero), sABill.CurrencyID);
                                                        else
                                                            inv.Invoice.AmountInWords = Utils.So_chu(Math.Round(inv.Invoice.Amount, 2, MidpointRounding.AwayFromZero), sABill.CurrencyID);
                                                        inv.Invoice.Total = Math.Round(inv.Invoice.Products.Sum(n => n.Total), 2, MidpointRounding.AwayFromZero);
                                                        inv.Invoice.VATAmount = Math.Round(inv.Invoice.Products.Sum(n => n.VATAmount), 2, MidpointRounding.AwayFromZero);
                                                    }
                                                    invoices.Invoices.Add(inv);
                                                }
                                                else if (check == 2)
                                                {
                                                    inv.Invoice.VATRate = inv.Invoice.Products.Count > 0 ? Convert.ToInt32(inv.Invoice.Products.First().VATRate).ToString() : "-1";

                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                    {
                                                        inv.Invoice.Amount = Math.Round(inv.Invoice.Products.Sum(n => n.Amount), MidpointRounding.AwayFromZero);
                                                        inv.Invoice.AmountInWords = NumberToWord.GetAmountInWords(inv.Invoice.Amount, sAReturn.CurrencyID);
                                                        inv.Invoice.Total = Math.Round(inv.Invoice.Products.Sum(n => n.Total), MidpointRounding.AwayFromZero);
                                                        inv.Invoice.VATAmount = Math.Round(inv.Invoice.Products.Sum(n => n.VATAmount), MidpointRounding.AwayFromZero);
                                                    }
                                                    else
                                                    {
                                                        inv.Invoice.Amount = Math.Round(inv.Invoice.Products.Sum(n => n.Amount), 2, MidpointRounding.AwayFromZero);
                                                        if (inv.Invoice.CurrencyUnit == "USD" || inv.Invoice.CurrencyUnit == "EUR" || inv.Invoice.CurrencyUnit == "GBP" || inv.Invoice.CurrencyUnit == "SGD")
                                                            inv.Invoice.AmountInWords = NumberToWord.GetAmountInWords(inv.Invoice.Amount, sAReturn.CurrencyID);
                                                        else
                                                            inv.Invoice.AmountInWords = Utils.So_chu(inv.Invoice.Amount, sAReturn.CurrencyID);
                                                        inv.Invoice.Total = Math.Round(inv.Invoice.Products.Sum(n => n.Total), 2, MidpointRounding.AwayFromZero);
                                                        inv.Invoice.VATAmount = Math.Round(inv.Invoice.Products.Sum(n => n.VATAmount), 2, MidpointRounding.AwayFromZero);
                                                    }
                                                    invoices.Invoices.Add(inv);
                                                }
                                                else if (check == 3)
                                                {
                                                    inv.Invoice.VATRate = inv.Invoice.Products.Count > 0 ? Convert.ToInt32(inv.Invoice.Products.First().VATRate).ToString() : "-1";
                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                    {
                                                        inv.Invoice.Amount = Math.Round(inv.Invoice.Products.Sum(n => n.Amount), MidpointRounding.AwayFromZero);
                                                        inv.Invoice.AmountInWords = NumberToWord.GetAmountInWords(Math.Round(inv.Invoice.Amount, MidpointRounding.AwayFromZero), pPDiscountReturn.CurrencyID);
                                                        inv.Invoice.Total = Math.Round(inv.Invoice.Products.Sum(n => n.Total), MidpointRounding.AwayFromZero);
                                                        inv.Invoice.VATAmount = Math.Round(inv.Invoice.Products.Sum(n => n.VATAmount), MidpointRounding.AwayFromZero);
                                                    }
                                                    else
                                                    {
                                                        inv.Invoice.Amount = Math.Round(inv.Invoice.Products.Sum(n => n.Amount), 2, MidpointRounding.AwayFromZero);
                                                        if (inv.Invoice.CurrencyUnit == "USD" || inv.Invoice.CurrencyUnit == "EUR" || inv.Invoice.CurrencyUnit == "GBP" || inv.Invoice.CurrencyUnit == "SGD")
                                                            inv.Invoice.AmountInWords = NumberToWord.GetAmountInWords(inv.Invoice.Amount, pPDiscountReturn.CurrencyID);
                                                        else
                                                            inv.Invoice.AmountInWords = Utils.So_chu(inv.Invoice.Amount, pPDiscountReturn.CurrencyID);
                                                        inv.Invoice.Total = Math.Round(inv.Invoice.Products.Sum(n => n.Total), 2, MidpointRounding.AwayFromZero);
                                                        inv.Invoice.VATAmount = Math.Round(inv.Invoice.Products.Sum(n => n.VATAmount), 2, MidpointRounding.AwayFromZero);
                                                    }
                                                    invoices.Invoices.Add(inv);
                                                }
                                                #endregion
                                                var xmldata = Utils.Serialize<EInvoices>(invoices);
                                                XmlDocument xml = new XmlDocument();
                                                xml.LoadXml(xmldata);
                                                XmlNode xmldt = xml.SelectSingleNode("/EInvoices/Invoices");
                                                request.XmlData = xmldt.OuterXml;
                                                RestApiService client = new RestApiService(Utils.GetPathAccess(), Utils.GetUserName(), Core.ITripleDes.TripleDesDefaultDecryption(Utils.GetPassWords()));
                                                Response response = client.Post(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "TaoHoaDonChuaKiSo").ApiPath, request);
                                                if (response.Status != 2)
                                                {
                                                    MSG.Error(response.Message);
                                                }
                                            }
                                            #endregion
                                        }
                                    }
                            }
                    }

                    #endregion
                }
                else if (statusForm == ConstFrm.optStatusForm.Edit)
                {
                    //[check TH bán hàng không kèm phiếu xuất kho]
                    if (@select is SAInvoice)
                    {
                        if (@select.HasProperty("ID") && @select.HasProperty("OutwardNo"))
                        {
                            var id = @select.GetProperty<T, Guid>("ID");
                            //Step 1: Xóa hết các chứng từ xuất nhập kho đang đc gắn vs chứng từ bán hàng hiện tại
                            foreach (var del in IRSInwardOutwardService.Query.Where(x => x.SAInvoiceID == id))
                            {
                                del.SAInvoiceID = null;
                                IRSInwardOutwardService.Update(del);
                            }
                            //Step 2: gắn các ID chứng từ bán hàng hiện tại vào xuất nhập kho nếu là bán hàng không kèm phiếu xuất kho
                            if (string.IsNullOrEmpty(@select.GetProperty<T, string>("OutwardNo")))
                            {
                                if (@select.HasProperty("ObjRsInwardOutwardIDs"))
                                {
                                    var guids = @select.GetProperty<T, List<Guid>>("ObjRsInwardOutwardIDs");
                                    foreach (var gId in guids)
                                    {
                                        var temp = IRSInwardOutwardService.Getbykey(gId);
                                        if (temp == null) continue;
                                        temp.SAInvoiceID = id;
                                        IRSInwardOutwardService.Update(temp);
                                    }
                                }
                            }
                        }
                    }
                    //[check TH kho có kèm phiếu bán hàng không kèm phiếu xuất kho]
                    if (@select is RSInwardOutward)
                    {
                        if (@select.HasProperty("ID") && @select.HasProperty("IsOutwardSAInvoice"))
                        {
                            var id = @select.GetProperty<T, Guid>("ID");
                            foreach (var del in ISAInvoiceService.Query.Where(x => x.OutwardRefID == id))
                            {
                                del.OutwardRefID = null;
                                ISAInvoiceService.Update(del);
                            }
                            if (@select.GetProperty<T, bool?>("IsOutwardSAInvoice") == true)
                            {
                                if (@select.HasProperty("SaInvoiceCustormIds"))
                                {
                                    var guids = @select.GetProperty<T, List<Guid>>("SaInvoiceCustormIds");
                                    foreach (var gId in guids)
                                    {
                                        var temp = ISAInvoiceService.Getbykey(gId);
                                        if (temp == null) continue;
                                        temp.OutwardRefID = id;
                                        ISAInvoiceService.Update(temp);
                                    }
                                }
                            }
                        }
                    }
                    if (@select is SABill)
                    {
                        if (@select.HasProperty("ID") && @select.HasProperty("SAIDs"))
                        {
                            var id = @select.GetProperty<T, Guid>("ID");

                            var guids = @select.GetProperty<T, List<Guid>>("SAIDs");
                            foreach (var gId in guids)
                            {
                                var temp = ISAInvoiceService.Getbykey(gId);
                                if (temp == null) continue;
                                temp.BillRefID = id;
                                temp.IsAttachListBill = true;
                                temp.InvoiceForm = null;
                                temp.InvoiceTypeID = null;
                                temp.InvoiceTemplate = null;
                                temp.InvoiceNo = null;
                                temp.InvoiceSeries = null;
                                temp.InvoiceDate = null;
                                temp.PaymentMethod = null;
                                ISAInvoiceService.Update(temp);
                            }
                        }
                    }
                    if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "HDDT").Data == "1")
                    {
                        SystemOption systemOption = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "EI_IDNhaCungCapDichVu");
                        if (@select.HasProperty("StatusInvoice") && @select.HasProperty("InvoiceForm") && @select.HasProperty("TypeID"))
                            if (@select.GetProperty<T, int>("TypeID") == 326 || (@select.HasProperty("IsBill") && @select.GetProperty<T, bool>("IsBill")))
                            {
                                if ((@select.GetProperty<T, int>("StatusInvoice") == 0 || @select.GetProperty<T, int>("StatusInvoice") == 7 || @select.GetProperty<T, int>("StatusInvoice") == 8 || @select.GetProperty<T, int>("StatusInvoice") == 6) && @select.GetProperty<T, int>("InvoiceForm") == 2)
                                    if (!string.IsNullOrEmpty(systemOption.Data))
                                    {
                                        SupplierService supplierService = Utils.ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                                        if (supplierService != null)
                                        {
                                            #region Tạo hóa đơn MIV
                                            if (supplierService.SupplierServiceCode == "MIV" && @select.GetProperty<T, int>("StatusInvoice") != 6)
                                            {
                                                //request.Pattern = item.Key.GetProperty("Pattern").ToString();
                                                //request.Serial = item.Key.GetProperty("Serial").ToString();
                                                int check = 0;
                                                EInvoices invoices = new EInvoices();
                                                Invoice_MIV invoice_MIV = new Invoice_MIV();
                                                List<EInvoiceVietTel> lstEVT = new List<EInvoiceVietTel>();
                                                DataInvoice_MIV dataInvoice_MIV = new DataInvoice_MIV();
                                                SAInvoice sAInvoice = @select as SAInvoice;
                                                SABill sABill = @select as SABill;
                                                PPDiscountReturn pPDiscountReturn = @select as PPDiscountReturn;
                                                SAReturn sAReturn = @select as SAReturn;
                                                string jsonRequest = "";
                                                if (sAInvoice == null)
                                                {
                                                    //sABill = Utils.ListSABill.FirstOrDefault(n => n.ID == Eitem.ID);
                                                    if (sABill == null)
                                                    {

                                                        //sAReturn = Utils.ListSAReturn.FirstOrDefault(n => n.ID == Eitem.ID);
                                                        if (sAReturn == null)
                                                        {
                                                            //pPDiscountReturn = Utils.ListPPDiscountReturn.FirstOrDefault(n => n.ID == Eitem.ID);
                                                            AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == pPDiscountReturn.AccountingObjectID);
                                                            InvoiceType invoiceType = Utils.ListInvoiceType.FirstOrDefault(n => n.ID == pPDiscountReturn.InvoiceTypeID);
                                                            //inv.Invoice.InvNo = "";
                                                            if (pPDiscountReturn.InvoiceNo.IsNullOrEmpty())
                                                            {
                                                                invoice_MIV.editmode = "1";
                                                            }
                                                            else
                                                            {
                                                                invoice_MIV.editmode = "2";
                                                            }
                                                            if (invoiceType.InvoiceTypeCode.Equals("01GTKT"))
                                                            {
                                                                invoice_MIV.windowid = "WIN00187";
                                                            }
                                                            else if (invoiceType.InvoiceTypeCode.Equals("02GTTT"))
                                                            {
                                                                invoice_MIV.windowid = "WIN00189";
                                                            }
                                                            //dataInvoice_MIV.inv_invoiceSeries = item.Key.GetProperty("Serial").ToString();
                                                            dataInvoice_MIV.inv_invoiceSeries = pPDiscountReturn.InvoiceSeries;
                                                            dataInvoice_MIV.inv_invoiceNumber = pPDiscountReturn.InvoiceNo;
                                                            if (pPDiscountReturn.InvoiceDate != null)
                                                            {
                                                                DateTime dt = pPDiscountReturn.InvoiceDate ?? DateTime.Now;
                                                                dataInvoice_MIV.inv_invoiceIssuedDate = dt.Date.ToString("yyyy-MM-dd");
                                                            }
                                                            else
                                                            {
                                                                dataInvoice_MIV.inv_invoiceIssuedDate = "";
                                                            }
                                                            dataInvoice_MIV.inv_currencyCode = pPDiscountReturn.CurrencyID;
                                                            if (dataInvoice_MIV.inv_currencyCode == "VND")
                                                            {
                                                                dataInvoice_MIV.inv_exchangeRate = 1;
                                                            }
                                                            else
                                                            {
                                                                dataInvoice_MIV.inv_exchangeRate = pPDiscountReturn.ExchangeRate ?? 0;
                                                            }
                                                            dataInvoice_MIV.inv_buyerDisplayName = pPDiscountReturn.OContactName ?? "";
                                                            dataInvoice_MIV.ma_dt = accountingObject.AccountingObjectCode;
                                                            dataInvoice_MIV.inv_buyerLegalName = pPDiscountReturn.AccountingObjectName ?? "";
                                                            dataInvoice_MIV.inv_buyerTaxCode = pPDiscountReturn.CompanyTaxCode;
                                                            dataInvoice_MIV.inv_buyerAddressLine = pPDiscountReturn.AccountingObjectAddress ?? "";
                                                            dataInvoice_MIV.inv_buyerEmail = accountingObject.Email;
                                                            dataInvoice_MIV.inv_buyerBankAccount = pPDiscountReturn.AccountingObjectBankAccount ?? "";
                                                            dataInvoice_MIV.inv_buyerBankName = pPDiscountReturn.AccountingObjectBankName ?? "";
                                                            dataInvoice_MIV.inv_paymentMethodName = pPDiscountReturn.PaymentMethod;
                                                            // dataInvoice_MIV.inv_sellerBankAccount = Utils.GetBankAccount();
                                                            //dataInvoice_MIV.inv_sellerBankName = Utils.GetBankName();
                                                            dataInvoice_MIV.mau_hd = pPDiscountReturn.InvoiceTemplate;

                                                            Details_MIV details_MIV = new Details_MIV();
                                                            int i = 0;
                                                            foreach (PPDiscountReturnDetail itemD in pPDiscountReturn.PPDiscountReturnDetails.OrderBy(n => n.OrderPriority).ToList())
                                                            {
                                                                i++;
                                                                MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == itemD.MaterialGoodsID);
                                                                if (invoiceType.InvoiceTypeCode.Equals("01GTKT"))
                                                                {
                                                                    details_MIV.tab_id = "TAB00188";
                                                                }
                                                                else if (invoiceType.InvoiceTypeCode.Equals("02GTTT"))
                                                                {
                                                                    details_MIV.tab_id = "TAB00192";
                                                                }
                                                                // thông tin hàng hóa
                                                                DataDetails_MIV dataDetails_MIV = new DataDetails_MIV();
                                                                dataDetails_MIV.stt_rec0 = i.ToString().PadLeft(8, '0');
                                                                dataDetails_MIV.inv_itemCode = materialGoods.MaterialGoodsCode ?? "";
                                                                dataDetails_MIV.inv_itemName = itemD.Description ?? "";
                                                                dataDetails_MIV.inv_unitName = itemD.Unit;
                                                                dataDetails_MIV.inv_unitCode = itemD.Unit;
                                                                dataDetails_MIV.inv_unitPrice = itemD.UnitPriceOriginal ?? 0;
                                                                dataDetails_MIV.inv_quantity = itemD.Quantity ?? 0;
                                                                dataDetails_MIV.inv_TotalAmountWithoutVat = itemD.AmountOriginal ?? 0;
                                                                dataDetails_MIV.inv_vatAmount = itemD.VATAmountOriginal ?? 0;
                                                                dataDetails_MIV.inv_TotalAmount = itemD.AmountOriginal ?? 0 + itemD.VATAmountOriginal ?? 0;
                                                                dataDetails_MIV.ma_thue = itemD.VATRate != -2 ? itemD.VATRate ?? 0 : -1;
                                                                //dataDetails_MIV.inv_discountPercentage
                                                                details_MIV.data.Add(dataDetails_MIV);
                                                            }
                                                            dataInvoice_MIV.inv_vatAmount = pPDiscountReturn.TotalVATAmountOriginal;
                                                            dataInvoice_MIV.inv_discountAmount = pPDiscountReturn.TotalDiscountAmountOriginal;
                                                            dataInvoice_MIV.inv_TotalAmountWithoutVat = pPDiscountReturn.TotalAmountOriginal;
                                                            dataInvoice_MIV.inv_TotalAmount = pPDiscountReturn.TotalPaymentAmountOriginal;
                                                            dataInvoice_MIV.details.Add(details_MIV);
                                                        }
                                                        else
                                                        {
                                                            //sAReturn = Utils.ListSAReturn.FirstOrDefault(n => n.ID == Eitem.ID);
                                                            AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sAReturn.AccountingObjectID);
                                                            InvoiceType invoiceType = Utils.ListInvoiceType.FirstOrDefault(n => n.ID == sAReturn.InvoiceTypeID);
                                                            //inv.Invoice.InvNo = "";

                                                            if (sAReturn.InvoiceNo.IsNullOrEmpty())
                                                            {
                                                                invoice_MIV.editmode = "1";
                                                            }
                                                            else
                                                            {
                                                                invoice_MIV.editmode = "2";
                                                            }
                                                            if (invoiceType.InvoiceTypeCode.Equals("01GTKT"))
                                                            {
                                                                invoice_MIV.windowid = "WIN00187";
                                                            }
                                                            else if (invoiceType.InvoiceTypeCode.Equals("02GTTT"))
                                                            {
                                                                invoice_MIV.windowid = "WIN00189";
                                                            }

                                                            //dataInvoice_MIV.inv_invoiceSeries = item.Key.GetProperty("Serial").ToString();
                                                            dataInvoice_MIV.inv_invoiceSeries = sAReturn.InvoiceSeries;
                                                            dataInvoice_MIV.inv_invoiceNumber = sAReturn.InvoiceNo;
                                                            if (sAReturn.InvoiceDate != null)
                                                            {
                                                                DateTime dt = sAReturn.InvoiceDate ?? DateTime.Now;
                                                                dataInvoice_MIV.inv_invoiceIssuedDate = dt.Date.ToString("yyyy-MM-dd");
                                                            }
                                                            else
                                                            {
                                                                dataInvoice_MIV.inv_invoiceIssuedDate = "";
                                                            }
                                                            dataInvoice_MIV.inv_currencyCode = sAReturn.CurrencyID;
                                                            if (dataInvoice_MIV.inv_currencyCode == "VND")
                                                            {
                                                                dataInvoice_MIV.inv_exchangeRate = 1;
                                                            }
                                                            else
                                                            {
                                                                dataInvoice_MIV.inv_exchangeRate = sAReturn.ExchangeRate ?? 0;
                                                            }
                                                            dataInvoice_MIV.inv_buyerDisplayName = sAReturn.ContactName;
                                                            dataInvoice_MIV.ma_dt = accountingObject.AccountingObjectCode;
                                                            dataInvoice_MIV.inv_buyerLegalName = sAReturn.AccountingObjectName ?? "";
                                                            dataInvoice_MIV.inv_buyerTaxCode = sAReturn.CompanyTaxCode;
                                                            dataInvoice_MIV.inv_buyerAddressLine = sAReturn.AccountingObjectAddress ?? "";
                                                            dataInvoice_MIV.inv_buyerEmail = accountingObject.Email;
                                                            dataInvoice_MIV.inv_buyerBankAccount = sAReturn.AccountingObjectBankAccount ?? "";
                                                            dataInvoice_MIV.inv_buyerBankName = sAReturn.AccountingObjectBankName ?? "";
                                                            dataInvoice_MIV.inv_paymentMethodName = sAReturn.PaymentMethod;
                                                            // dataInvoice_MIV.inv_sellerBankAccount = Utils.GetBankAccount();
                                                            //dataInvoice_MIV.inv_sellerBankName = Utils.GetBankName();
                                                            dataInvoice_MIV.mau_hd = sAReturn.InvoiceTemplate;

                                                            Details_MIV details_MIV = new Details_MIV();
                                                            int i = 0;
                                                            foreach (SAReturnDetail itemD in sAReturn.SAReturnDetails.OrderBy(n => n.OrderPriority).ToList())
                                                            {
                                                                i++;
                                                                MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == itemD.MaterialGoodsID);
                                                                if (invoiceType.InvoiceTypeCode.Equals("01GTKT"))
                                                                {
                                                                    details_MIV.tab_id = "TAB00188";
                                                                }
                                                                else if (invoiceType.InvoiceTypeCode.Equals("02GTTT"))
                                                                {
                                                                    details_MIV.tab_id = "TAB00192";
                                                                }
                                                                // thông tin hàng hóa
                                                                DataDetails_MIV dataDetails_MIV = new DataDetails_MIV();
                                                                dataDetails_MIV.stt_rec0 = i.ToString().PadLeft(8, '0');
                                                                dataDetails_MIV.inv_itemCode = materialGoods.MaterialGoodsCode ?? "";
                                                                dataDetails_MIV.inv_itemName = itemD.Description;
                                                                dataDetails_MIV.inv_unitName = itemD.Unit;
                                                                dataDetails_MIV.inv_unitCode = itemD.Unit;
                                                                dataDetails_MIV.inv_unitPrice = itemD.UnitPriceOriginal;
                                                                dataDetails_MIV.inv_quantity = itemD.Quantity ?? 0;
                                                                dataDetails_MIV.inv_TotalAmountWithoutVat = itemD.AmountOriginal;
                                                                dataDetails_MIV.inv_vatAmount = itemD.VATAmountOriginal;
                                                                dataDetails_MIV.inv_discountAmount = itemD.DiscountAmount;
                                                                dataDetails_MIV.inv_discountPercentage = itemD.DiscountRate ?? 0;
                                                                dataDetails_MIV.inv_TotalAmount = itemD.AmountOriginal + itemD.VATAmountOriginal - itemD.DiscountAmount;
                                                                dataDetails_MIV.ma_thue = itemD.VATRate != -2 ? itemD.VATRate ?? 0 : -1;
                                                                //dataDetails_MIV.inv_discountPercentage
                                                                details_MIV.data.Add(dataDetails_MIV);
                                                            }
                                                            dataInvoice_MIV.inv_vatAmount = sAReturn.TotalVATAmountOriginal;
                                                            dataInvoice_MIV.inv_discountAmount = sAReturn.TotalDiscountAmountOriginal;
                                                            dataInvoice_MIV.inv_TotalAmountWithoutVat = sAReturn.TotalAmountOriginal;
                                                            dataInvoice_MIV.inv_TotalAmount = sAReturn.TotalPaymentAmountOriginal;
                                                            dataInvoice_MIV.details.Add(details_MIV);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (sABill.Type == 2 && sABill.StatusInvoice == 8) // điều chỉnh tăng
                                                        {
                                                            check = 1;
                                                            AdjustIncrease_MIV adjustIncrease_MIV = new AdjustIncrease_MIV();
                                                            adjustIncrease_MIV.inv_InvoiceAuth_id = sABill.ID_MIVAdjust;
                                                            adjustIncrease_MIV.inv_invoiceIssuedDate = (sABill.InvoiceDate ?? DateTime.Now).Date.ToString("yyyy-MM-dd");
                                                            adjustIncrease_MIV.sovb = sABill.DocumentNo;
                                                            adjustIncrease_MIV.ngayvb = (sABill.DocumentDate ?? DateTime.Now).Date.ToString("yyyy-MM-dd");
                                                            adjustIncrease_MIV.ghi_chu = sABill.DocumentNote;

                                                            foreach (var item in sABill.SABillDetails.OrderBy(n => n.OrderPriority).ToList())
                                                            {
                                                                DataAdjust dataAdjust = new DataAdjust();

                                                                dataAdjust.inv_itemCode = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == item.MaterialGoodsID).MaterialGoodsCode;
                                                                dataAdjust.inv_quantity = item.Quantity;
                                                                dataAdjust.inv_unitPrice = item.UnitPriceOriginal;
                                                                dataAdjust.nv_discountPercentage = item.DiscountRate;
                                                                dataAdjust.inv_discountAmount = item.DiscountAmountOriginal;
                                                                dataAdjust.ma_thue = item.VATRate != -2 ? item.VATRate ?? 0 : -1;

                                                                SAInvoice sA = Utils.ListSAInvoice.FirstOrDefault(n => n.ID == sABill.IDAdjustInv);
                                                                SAReturn sR = Utils.ListSAReturn.FirstOrDefault(n => n.ID == sABill.IDAdjustInv);
                                                                SABill sAB = Utils.ListSABill.FirstOrDefault(n => n.ID == sABill.IDAdjustInv);
                                                                PPDiscountReturn sPP = Utils.ListPPDiscountReturn.FirstOrDefault(n => n.ID == sABill.IDAdjustInv);
                                                                string tieuthuc = "";
                                                                if (sA != null)
                                                                {
                                                                    if (!sA.SAInvoiceDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.Quantity == item.Quantity))
                                                                    {
                                                                        tieuthuc += "Số lượng";
                                                                    }
                                                                    if (!sA.SAInvoiceDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.UnitPrice == item.UnitPrice))
                                                                    {
                                                                        if (tieuthuc.Contains("Số lượng"))
                                                                        {
                                                                            tieuthuc += ", đơn giá";
                                                                        }
                                                                        else
                                                                        {
                                                                            tieuthuc += "Đơn giá";
                                                                        }
                                                                    }
                                                                    if (!sA.SAInvoiceDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.VATRate == item.VATRate))
                                                                    {
                                                                        if (tieuthuc.Contains("Số lượng") || tieuthuc.Contains("Đơn giá") || tieuthuc.Contains("đơn giá"))
                                                                        {
                                                                            tieuthuc += ", thuế suất";
                                                                        }
                                                                        else
                                                                        {
                                                                            tieuthuc += "Thuế suất";
                                                                        }
                                                                    }
                                                                }
                                                                else if (sR != null)
                                                                {
                                                                    if (!sR.SAReturnDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.Quantity == item.Quantity))
                                                                    {
                                                                        tieuthuc += "Số lượng";
                                                                    }
                                                                    if (!sR.SAReturnDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.UnitPrice == item.UnitPrice))
                                                                    {
                                                                        if (tieuthuc.Contains("Số lượng"))
                                                                        {
                                                                            tieuthuc += ", đơn giá";
                                                                        }
                                                                        else
                                                                        {
                                                                            tieuthuc += "Đơn giá";
                                                                        }
                                                                    }
                                                                    if (!sR.SAReturnDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.VATRate == item.VATRate))
                                                                    {
                                                                        if (tieuthuc.Contains("Số lượng") || tieuthuc.Contains("Đơn giá") || tieuthuc.Contains("đơn giá"))
                                                                        {
                                                                            tieuthuc += ", thuế suất";
                                                                        }
                                                                        else
                                                                        {
                                                                            tieuthuc += "Thuế suất";
                                                                        }
                                                                    }
                                                                }
                                                                else if (sAB != null)
                                                                {
                                                                    if (!sAB.SABillDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.Quantity == item.Quantity))
                                                                    {
                                                                        tieuthuc += "Số lượng";
                                                                    }
                                                                    if (!sAB.SABillDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.UnitPrice == item.UnitPrice))
                                                                    {
                                                                        if (tieuthuc.Contains("Số lượng"))
                                                                        {
                                                                            tieuthuc += ", đơn giá";
                                                                        }
                                                                        else
                                                                        {
                                                                            tieuthuc += "Đơn giá";
                                                                        }
                                                                    }
                                                                    if (!sAB.SABillDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.VATRate == item.VATRate))
                                                                    {
                                                                        if (tieuthuc.Contains("Số lượng") || tieuthuc.Contains("Đơn giá") || tieuthuc.Contains("đơn giá"))
                                                                        {
                                                                            tieuthuc += ", thuế suất";
                                                                        }
                                                                        else
                                                                        {
                                                                            tieuthuc += "Thuế suất";
                                                                        }
                                                                    }
                                                                }
                                                                else if (sPP != null)
                                                                {
                                                                    if (!sPP.PPDiscountReturnDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.Quantity == item.Quantity))
                                                                    {
                                                                        tieuthuc += "Số lượng";
                                                                    }
                                                                    if (!sPP.PPDiscountReturnDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.UnitPrice == item.UnitPrice))
                                                                    {
                                                                        if (tieuthuc.Contains("Số lượng"))
                                                                        {
                                                                            tieuthuc += ", đơn giá";
                                                                        }
                                                                        else
                                                                        {
                                                                            tieuthuc += "Đơn giá";
                                                                        }
                                                                    }
                                                                    if (!sPP.PPDiscountReturnDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.VATRate == item.VATRate))
                                                                    {
                                                                        if (tieuthuc.Contains("Số lượng") || tieuthuc.Contains("Đơn giá") || tieuthuc.Contains("đơn giá"))
                                                                        {
                                                                            tieuthuc += ", thuế suất";
                                                                        }
                                                                        else
                                                                        {
                                                                            tieuthuc += "Thuế suất";
                                                                        }
                                                                    }
                                                                }
                                                                dataAdjust.tieu_thuc = tieuthuc;
                                                                if (!dataAdjust.tieu_thuc.ToLower().Contains("số lượng"))
                                                                {
                                                                    dataAdjust.inv_quantity = null;
                                                                }
                                                                if (!dataAdjust.tieu_thuc.ToLower().Contains("đơn giá"))
                                                                {
                                                                    dataAdjust.inv_unitPrice = null;
                                                                }
                                                                if (!dataAdjust.tieu_thuc.ToLower().Contains("thuế suất"))
                                                                {
                                                                    dataAdjust.ma_thue = null;
                                                                }
                                                                adjustIncrease_MIV.data.Add(dataAdjust);

                                                            }
                                                            jsonRequest = JsonConvert.SerializeObject(adjustIncrease_MIV, Newtonsoft.Json.Formatting.None,
                                                                                                        new JsonSerializerSettings
                                                                                                        {
                                                                                                            NullValueHandling = NullValueHandling.Ignore
                                                                                                        });

                                                        }
                                                        else if (sABill.Type == 3 && sABill.StatusInvoice == 8) // điều chỉnh giảm
                                                        {
                                                            check = 2;
                                                            AdjustReduction_MIV adjustReduction_MIV = new AdjustReduction_MIV();
                                                            adjustReduction_MIV.inv_InvoiceAuth_id = sABill.ID_MIVAdjust;
                                                            adjustReduction_MIV.inv_invoiceIssuedDate = (sABill.InvoiceDate ?? DateTime.Now).Date.ToString("");
                                                            adjustReduction_MIV.sovb = sABill.DocumentNo;
                                                            adjustReduction_MIV.ngayvb = (sABill.DocumentDate ?? DateTime.Now).Date.ToString("yyyy-MM-dd");
                                                            adjustReduction_MIV.ghi_chu = sABill.DocumentNote;

                                                            foreach (var item in sABill.SABillDetails.OrderBy(n => n.OrderPriority).ToList())
                                                            {
                                                                DataAdjust dataAdjust = new DataAdjust();

                                                                dataAdjust.inv_itemCode = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == item.MaterialGoodsID).MaterialGoodsCode;
                                                                dataAdjust.inv_quantity = item.Quantity;
                                                                dataAdjust.inv_unitPrice = item.UnitPriceOriginal;
                                                                dataAdjust.ma_thue = item.VATRate != -2 ? item.VATRate : -1;
                                                                dataAdjust.nv_discountPercentage = item.DiscountRate;
                                                                dataAdjust.inv_discountAmount = item.DiscountAmountOriginal;

                                                                SAInvoice sA = Utils.ListSAInvoice.FirstOrDefault(n => n.ID == sABill.IDAdjustInv);
                                                                SAReturn sR = Utils.ListSAReturn.FirstOrDefault(n => n.ID == sABill.IDAdjustInv);
                                                                SABill sAB = Utils.ListSABill.FirstOrDefault(n => n.ID == sABill.IDAdjustInv);
                                                                PPDiscountReturn sPP = Utils.ListPPDiscountReturn.FirstOrDefault(n => n.ID == sABill.IDAdjustInv);
                                                                string tieuthuc = "";
                                                                if (sA != null)
                                                                {
                                                                    if (!sA.SAInvoiceDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.Quantity == item.Quantity))
                                                                    {
                                                                        tieuthuc += "Số lượng";
                                                                    }
                                                                    if (!sA.SAInvoiceDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.UnitPrice == item.UnitPrice))
                                                                    {
                                                                        if (tieuthuc.Contains("Số lượng"))
                                                                        {
                                                                            tieuthuc += ", đơn giá";
                                                                        }
                                                                        else
                                                                        {
                                                                            tieuthuc += "Đơn giá";
                                                                        }
                                                                    }
                                                                    if (!sA.SAInvoiceDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.VATRate == item.VATRate))
                                                                    {
                                                                        if (tieuthuc.Contains("Số lượng") || tieuthuc.Contains("Đơn giá") || tieuthuc.Contains("đơn giá"))
                                                                        {
                                                                            tieuthuc += ", thuế suất";
                                                                        }
                                                                        else
                                                                        {
                                                                            tieuthuc += "Thuế suất";
                                                                        }
                                                                    }
                                                                }
                                                                else if (sR != null)
                                                                {
                                                                    if (!sR.SAReturnDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.Quantity == item.Quantity))
                                                                    {
                                                                        tieuthuc += "Số lượng";
                                                                    }
                                                                    if (!sR.SAReturnDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.UnitPrice == item.UnitPrice))
                                                                    {
                                                                        if (tieuthuc.Contains("Số lượng"))
                                                                        {
                                                                            tieuthuc += ", đơn giá";
                                                                        }
                                                                        else
                                                                        {
                                                                            tieuthuc += "Đơn giá";
                                                                        }
                                                                    }
                                                                    if (!sR.SAReturnDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.VATRate == item.VATRate))
                                                                    {
                                                                        if (tieuthuc.Contains("Số lượng") || tieuthuc.Contains("Đơn giá") || tieuthuc.Contains("đơn giá"))
                                                                        {
                                                                            tieuthuc += ", thuế suất";
                                                                        }
                                                                        else
                                                                        {
                                                                            tieuthuc += "Thuế suất";
                                                                        }
                                                                    }
                                                                }
                                                                else if (sAB != null)
                                                                {
                                                                    if (!sAB.SABillDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.Quantity == item.Quantity))
                                                                    {
                                                                        tieuthuc += "Số lượng";
                                                                    }
                                                                    if (!sAB.SABillDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.UnitPrice == item.UnitPrice))
                                                                    {
                                                                        if (tieuthuc.Contains("Số lượng"))
                                                                        {
                                                                            tieuthuc += ", đơn giá";
                                                                        }
                                                                        else
                                                                        {
                                                                            tieuthuc += "Đơn giá";
                                                                        }
                                                                    }
                                                                    if (!sAB.SABillDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.VATRate == item.VATRate))
                                                                    {
                                                                        if (tieuthuc.Contains("Số lượng") || tieuthuc.Contains("Đơn giá") || tieuthuc.Contains("đơn giá"))
                                                                        {
                                                                            tieuthuc += ", thuế suất";
                                                                        }
                                                                        else
                                                                        {
                                                                            tieuthuc += "Thuế suất";
                                                                        }
                                                                    }
                                                                }
                                                                else if (sPP != null)
                                                                {
                                                                    if (!sPP.PPDiscountReturnDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.Quantity == item.Quantity))
                                                                    {
                                                                        tieuthuc += "Số lượng";
                                                                    }
                                                                    if (!sPP.PPDiscountReturnDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.UnitPrice == item.UnitPrice))
                                                                    {
                                                                        if (tieuthuc.Contains("Số lượng"))
                                                                        {
                                                                            tieuthuc += ", đơn giá";
                                                                        }
                                                                        else
                                                                        {
                                                                            tieuthuc += "Đơn giá";
                                                                        }
                                                                    }
                                                                    if (!sPP.PPDiscountReturnDetails.Any(n => n.MaterialGoodsID == item.MaterialGoodsID && n.VATRate == item.VATRate))
                                                                    {
                                                                        if (tieuthuc.Contains("Số lượng") || tieuthuc.Contains("Đơn giá") || tieuthuc.Contains("đơn giá"))
                                                                        {
                                                                            tieuthuc += ", thuế suất";
                                                                        }
                                                                        else
                                                                        {
                                                                            tieuthuc += "Thuế suất";
                                                                        }
                                                                    }
                                                                }
                                                                dataAdjust.tieu_thuc = tieuthuc;
                                                                if (!dataAdjust.tieu_thuc.ToLower().Contains("số lượng"))
                                                                {
                                                                    dataAdjust.inv_quantity = null;
                                                                }
                                                                if (!dataAdjust.tieu_thuc.ToLower().Contains("đơn giá"))
                                                                {
                                                                    dataAdjust.inv_unitPrice = null;
                                                                }
                                                                if (!dataAdjust.tieu_thuc.ToLower().Contains("thuế suất"))
                                                                {
                                                                    dataAdjust.ma_thue = null;
                                                                }
                                                                adjustReduction_MIV.data.Add(dataAdjust);
                                                            }
                                                            jsonRequest = JsonConvert.SerializeObject(adjustReduction_MIV, Newtonsoft.Json.Formatting.None,
                                                                                                        new JsonSerializerSettings
                                                                                                        {
                                                                                                            NullValueHandling = NullValueHandling.Ignore
                                                                                                        });

                                                        }
                                                        else if (sABill.Type == 4 && sABill.StatusInvoice == 8) // điều chỉnh thông tin
                                                        {
                                                            check = 3;
                                                            AdjustInformation_MIV adjustInformation_MIV = new AdjustInformation_MIV();
                                                            adjustInformation_MIV.inv_InvoiceAuth_id = sABill.ID_MIVAdjust;
                                                            adjustInformation_MIV.inv_invoiceIssuedDate = (sABill.InvoiceDate ?? DateTime.Now).Date.ToString("yyyy-MM-dd");
                                                            adjustInformation_MIV.sovb = sABill.DocumentNo;
                                                            adjustInformation_MIV.ngayvb = (sABill.DocumentDate ?? DateTime.Now).Date.ToString("yyyy-MM-dd");
                                                            adjustInformation_MIV.ghi_chu = sABill.DocumentNote;
                                                            SAInvoice sA = Utils.ListSAInvoice.FirstOrDefault(n => n.ID == sABill.IDAdjustInv);
                                                            SAReturn sR = Utils.ListSAReturn.FirstOrDefault(n => n.ID == sABill.IDAdjustInv);
                                                            SABill sAB = Utils.ListSABill.FirstOrDefault(n => n.ID == sABill.IDAdjustInv);
                                                            PPDiscountReturn sPP = Utils.ListPPDiscountReturn.FirstOrDefault(n => n.ID == sABill.IDAdjustInv);
                                                            if (sA != null)
                                                            {
                                                                AccountingObject acc = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sA.AccountingObjectID);
                                                                AccountingObject acc_ = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sABill.AccountingObjectID);
                                                                if (!sABill.ContactName.Equals(sA.ContactName))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerDisplayName = sABill.ContactName;
                                                                }
                                                                if (!acc_.AccountingObjectCode.Equals(acc.AccountingObjectCode))
                                                                {
                                                                    adjustInformation_MIV.ma_dt = acc_.AccountingObjectCode;
                                                                }
                                                                if (!sABill.AccountingObjectName.Equals(sA.AccountingObjectName))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerLegalName = sABill.AccountingObjectName;
                                                                }
                                                                if (!sABill.AccountingObjectAddress.Equals(sA.AccountingObjectAddress))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerAddressLine = sABill.AccountingObjectAddress;
                                                                }
                                                                if (!acc_.Email.Equals(acc.Email))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerEmail = acc_.Email;
                                                                }
                                                                if (!sABill.AccountingObjectBankAccount.Equals(sA.AccountingObjectBankAccount))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerBankAccount = sABill.AccountingObjectBankAccount;
                                                                }
                                                                if (!sABill.AccountingObjectBankName.Equals(sA.AccountingObjectBankName))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerBankName = sABill.AccountingObjectBankName;
                                                                }
                                                            }
                                                            else if (sR != null)
                                                            {
                                                                AccountingObject acc = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sR.AccountingObjectID);
                                                                AccountingObject acc_ = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sABill.AccountingObjectID);
                                                                if (!sABill.ContactName.Equals(sR.ContactName))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerDisplayName = sABill.ContactName;
                                                                }
                                                                if (!acc_.AccountingObjectCode.Equals(acc.AccountingObjectCode))
                                                                {
                                                                    adjustInformation_MIV.ma_dt = acc_.AccountingObjectCode;
                                                                }
                                                                if (!sABill.AccountingObjectName.Equals(sR.AccountingObjectName))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerLegalName = sABill.AccountingObjectName;
                                                                }
                                                                if (!sABill.AccountingObjectAddress.Equals(sR.AccountingObjectAddress))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerAddressLine = sABill.AccountingObjectAddress;
                                                                }
                                                                if (!acc_.Email.Equals(acc.Email))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerEmail = acc_.Email;
                                                                }
                                                                if (!sABill.AccountingObjectBankAccount.Equals(sR.AccountingObjectBankAccount))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerBankAccount = sABill.AccountingObjectBankAccount;
                                                                }
                                                                if (!sABill.AccountingObjectBankName.Equals(sR.AccountingObjectBankName))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerBankName = sABill.AccountingObjectBankName;
                                                                }
                                                            }
                                                            else if (sAB != null)
                                                            {
                                                                AccountingObject acc = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sAB.AccountingObjectID);
                                                                AccountingObject acc_ = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sABill.AccountingObjectID);
                                                                if (!sABill.ContactName.Equals(sAB.ContactName))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerDisplayName = sABill.ContactName;
                                                                }
                                                                if (!acc_.AccountingObjectCode.Equals(acc.AccountingObjectCode))
                                                                {
                                                                    adjustInformation_MIV.ma_dt = acc_.AccountingObjectCode;
                                                                }
                                                                if (!sABill.AccountingObjectName.Equals(sAB.AccountingObjectName))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerLegalName = sABill.AccountingObjectName;
                                                                }
                                                                if (!sABill.AccountingObjectAddress.Equals(sAB.AccountingObjectAddress))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerAddressLine = sABill.AccountingObjectAddress;
                                                                }
                                                                if (!acc_.Email.Equals(acc.Email))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerEmail = acc_.Email;
                                                                }
                                                                if (!sABill.AccountingObjectBankAccount.Equals(sAB.AccountingObjectBankAccount))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerBankAccount = sABill.AccountingObjectBankAccount;
                                                                }
                                                                if (!sABill.AccountingObjectBankName.Equals(sAB.AccountingObjectBankName))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerBankName = sABill.AccountingObjectBankName;
                                                                }
                                                            }
                                                            else if (sPP != null)
                                                            {
                                                                AccountingObject acc = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sPP.AccountingObjectID);
                                                                AccountingObject acc_ = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sABill.AccountingObjectID);
                                                                if (!sABill.ContactName.Equals(sPP.OContactName))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerDisplayName = sABill.ContactName;
                                                                }
                                                                if (!acc_.AccountingObjectCode.Equals(acc.AccountingObjectCode))
                                                                {
                                                                    adjustInformation_MIV.ma_dt = acc_.AccountingObjectCode;
                                                                }
                                                                if (!sABill.AccountingObjectName.Equals(sPP.AccountingObjectName))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerLegalName = sABill.AccountingObjectName;
                                                                }
                                                                if (!sABill.AccountingObjectAddress.Equals(sPP.AccountingObjectAddress))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerAddressLine = sABill.AccountingObjectAddress;
                                                                }
                                                                if (!acc_.Email.Equals(acc.Email))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerEmail = acc_.Email;
                                                                }
                                                                if (!sABill.AccountingObjectBankAccount.Equals(sPP.AccountingObjectBankAccount))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerBankAccount = sABill.AccountingObjectBankAccount;
                                                                }
                                                                if (!sABill.AccountingObjectBankName.Equals(sPP.AccountingObjectBankName))
                                                                {
                                                                    adjustInformation_MIV.inv_buyerBankName = sABill.AccountingObjectBankName;
                                                                }
                                                            }
                                                            jsonRequest = JsonConvert.SerializeObject(adjustInformation_MIV, Newtonsoft.Json.Formatting.None,
                                                                                                        new JsonSerializerSettings
                                                                                                        {
                                                                                                            NullValueHandling = NullValueHandling.Ignore
                                                                                                        });

                                                        }
                                                        else if (sABill.Type == 0 && sABill.StatusInvoice == 7) //Thay thế hóa đơn
                                                        {
                                                            check = 4;

                                                        }
                                                        else
                                                        {
                                                            AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sABill.AccountingObjectID);

                                                            //sABill = Utils.ListSABill.FirstOrDefault(n => n.ID == Eitem.ID);
                                                            InvoiceType invoiceType = Utils.ListInvoiceType.FirstOrDefault(n => n.ID == sABill.InvoiceTypeID);
                                                            //inv.Invoice.InvNo = "";

                                                            if (sABill.InvoiceNo.IsNullOrEmpty())
                                                            {
                                                                invoice_MIV.editmode = "1";
                                                            }
                                                            else
                                                            {
                                                                invoice_MIV.editmode = "2";
                                                            }
                                                            if (invoiceType.InvoiceTypeCode.Equals("01GTKT"))
                                                            {
                                                                invoice_MIV.windowid = "WIN00187";
                                                            }
                                                            else if (invoiceType.InvoiceTypeCode.Equals("02GTTT"))
                                                            {
                                                                invoice_MIV.windowid = "WIN00189";
                                                            }

                                                            dataInvoice_MIV.inv_invoiceSeries = sABill.InvoiceSeries;
                                                            dataInvoice_MIV.inv_invoiceNumber = sABill.InvoiceNo;
                                                            if (sABill.InvoiceDate != null)
                                                            {
                                                                DateTime dt = sABill.InvoiceDate ?? DateTime.Now;
                                                                dataInvoice_MIV.inv_invoiceIssuedDate = dt.Date.ToString("yyyy-MM-dd");
                                                            }
                                                            else
                                                            {
                                                                dataInvoice_MIV.inv_invoiceIssuedDate = "";
                                                            }
                                                            dataInvoice_MIV.inv_currencyCode = sABill.CurrencyID;
                                                            if (dataInvoice_MIV.inv_currencyCode == "VND")
                                                            {
                                                                dataInvoice_MIV.inv_exchangeRate = 1;
                                                            }
                                                            else
                                                            {
                                                                dataInvoice_MIV.inv_exchangeRate = sABill.ExchangeRate;
                                                            }
                                                            dataInvoice_MIV.inv_buyerDisplayName = sABill.ContactName ?? "";
                                                            dataInvoice_MIV.ma_dt = accountingObject.AccountingObjectCode;
                                                            dataInvoice_MIV.inv_buyerLegalName = sABill.AccountingObjectName ?? "";
                                                            dataInvoice_MIV.inv_buyerTaxCode = sABill.CompanyTaxCode;
                                                            dataInvoice_MIV.inv_buyerAddressLine = sABill.AccountingObjectAddress ?? "";
                                                            dataInvoice_MIV.inv_buyerEmail = accountingObject.Email;
                                                            dataInvoice_MIV.inv_buyerBankAccount = sABill.AccountingObjectBankAccount ?? "";
                                                            dataInvoice_MIV.inv_buyerBankName = sABill.AccountingObjectBankName ?? "";
                                                            dataInvoice_MIV.inv_paymentMethodName = sABill.PaymentMethod;
                                                            // dataInvoice_MIV.inv_sellerBankAccount = Utils.GetBankAccount();
                                                            //dataInvoice_MIV.inv_sellerBankName = Utils.GetBankName();
                                                            dataInvoice_MIV.mau_hd = sABill.InvoiceTemplate;

                                                            Details_MIV details_MIV = new Details_MIV();
                                                            int i = 0;
                                                            foreach (SABillDetail itemD in sABill.SABillDetails.OrderBy(n => n.OrderPriority).ToList())
                                                            {
                                                                i++;
                                                                MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == itemD.MaterialGoodsID);
                                                                if (invoiceType.InvoiceTypeCode.Equals("01GTKT"))
                                                                {
                                                                    details_MIV.tab_id = "TAB00188";
                                                                }
                                                                else if (invoiceType.InvoiceTypeCode.Equals("02GTTT"))
                                                                {
                                                                    details_MIV.tab_id = "TAB00192";
                                                                }
                                                                // thông tin hàng hóa
                                                                DataDetails_MIV dataDetails_MIV = new DataDetails_MIV();
                                                                dataDetails_MIV.stt_rec0 = i.ToString().PadLeft(8, '0');
                                                                dataDetails_MIV.inv_itemCode = materialGoods.MaterialGoodsCode ?? "";
                                                                dataDetails_MIV.inv_itemName = itemD.Description ?? "";
                                                                dataDetails_MIV.inv_unitName = itemD.Unit;
                                                                dataDetails_MIV.inv_unitCode = itemD.Unit;
                                                                dataDetails_MIV.inv_unitPrice = itemD.UnitPriceOriginal;
                                                                dataDetails_MIV.inv_quantity = itemD.Quantity;
                                                                dataDetails_MIV.inv_TotalAmountWithoutVat = itemD.AmountOriginal;
                                                                dataDetails_MIV.inv_vatAmount = itemD.VATAmountOriginal;
                                                                dataDetails_MIV.inv_discountAmount = itemD.DiscountAmount;
                                                                dataDetails_MIV.inv_discountPercentage = itemD.DiscountRate ?? 0;
                                                                dataDetails_MIV.inv_TotalAmount = itemD.AmountOriginal + itemD.VATAmountOriginal - itemD.DiscountAmount;
                                                                dataDetails_MIV.ma_thue = itemD.VATRate != -2 ? itemD.VATRate ?? 0 : -1;
                                                                //dataDetails_MIV.inv_discountPercentage
                                                                details_MIV.data.Add(dataDetails_MIV);
                                                            }
                                                            dataInvoice_MIV.inv_vatAmount = sABill.TotalVATAmountOriginal;
                                                            dataInvoice_MIV.inv_discountAmount = sABill.TotalDiscountAmountOriginal;
                                                            dataInvoice_MIV.inv_TotalAmountWithoutVat = sABill.TotalAmountOriginal;
                                                            dataInvoice_MIV.inv_TotalAmount = sABill.TotalPaymentAmountOriginal;
                                                            dataInvoice_MIV.details.Add(details_MIV);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sAInvoice.AccountingObjectID);

                                                    //sAInvoice = Utils.ListSAInvoice.FirstOrDefault(n => n.ID == Eitem.ID);
                                                    InvoiceType invoiceType = Utils.ListInvoiceType.FirstOrDefault(n => n.ID == sAInvoice.InvoiceTypeID);
                                                    //inv.Invoice.InvNo = "";

                                                    if (sAInvoice.InvoiceNo.IsNullOrEmpty())
                                                    {
                                                        invoice_MIV.editmode = "1";
                                                    }
                                                    else
                                                    {
                                                        invoice_MIV.editmode = "2";
                                                    }
                                                    if (invoiceType.InvoiceTypeCode.Equals("01GTKT"))
                                                    {
                                                        invoice_MIV.windowid = "WIN00187";
                                                    }
                                                    else if (invoiceType.InvoiceTypeCode.Equals("02GTTT"))
                                                    {
                                                        invoice_MIV.windowid = "WIN00189";
                                                    }

                                                    dataInvoice_MIV.inv_invoiceSeries = sAInvoice.InvoiceSeries;
                                                    dataInvoice_MIV.inv_invoiceNumber = sAInvoice.InvoiceNo;
                                                    if (sAInvoice.InvoiceDate != null)
                                                    {
                                                        DateTime dt = sAInvoice.InvoiceDate ?? DateTime.Now;
                                                        dataInvoice_MIV.inv_invoiceIssuedDate = dt.Date.ToString("yyyy-MM-dd");
                                                    }
                                                    else
                                                    {
                                                        dataInvoice_MIV.inv_invoiceIssuedDate = "";
                                                    }
                                                    dataInvoice_MIV.inv_currencyCode = sAInvoice.CurrencyID;
                                                    if (dataInvoice_MIV.inv_currencyCode == "VND")
                                                    {
                                                        dataInvoice_MIV.inv_exchangeRate = 1;
                                                    }
                                                    else
                                                    {
                                                        dataInvoice_MIV.inv_exchangeRate = sAInvoice.ExchangeRate ?? 0;
                                                    }
                                                    dataInvoice_MIV.inv_buyerDisplayName = sAInvoice.ContactName ?? "";
                                                    dataInvoice_MIV.ma_dt = accountingObject.AccountingObjectCode;
                                                    dataInvoice_MIV.inv_buyerLegalName = sAInvoice.AccountingObjectName ?? "";
                                                    dataInvoice_MIV.inv_buyerTaxCode = sAInvoice.CompanyTaxCode;
                                                    dataInvoice_MIV.inv_buyerAddressLine = sAInvoice.AccountingObjectAddress ?? "";
                                                    dataInvoice_MIV.inv_buyerEmail = accountingObject.Email;
                                                    dataInvoice_MIV.inv_buyerBankAccount = sAInvoice.AccountingObjectBankAccount ?? "";
                                                    dataInvoice_MIV.inv_buyerBankName = sAInvoice.AccountingObjectBankName ?? "";
                                                    dataInvoice_MIV.inv_paymentMethodName = sAInvoice.PaymentMethod;
                                                    // dataInvoice_MIV.inv_sellerBankAccount = Utils.GetBankAccount();
                                                    //dataInvoice_MIV.inv_sellerBankName = Utils.GetBankName();
                                                    dataInvoice_MIV.mau_hd = sAInvoice.InvoiceTemplate;

                                                    Details_MIV details_MIV = new Details_MIV();
                                                    int i = 0;
                                                    foreach (SAInvoiceDetail itemD in sAInvoice.SAInvoiceDetails.OrderBy(n => n.OrderPriority).ToList())
                                                    {
                                                        i++;
                                                        MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == itemD.MaterialGoodsID);
                                                        if (invoiceType.InvoiceTypeCode.Equals("01GTKT"))
                                                        {
                                                            details_MIV.tab_id = "TAB00188";
                                                        }
                                                        else if (invoiceType.InvoiceTypeCode.Equals("02GTTT"))
                                                        {
                                                            details_MIV.tab_id = "TAB00192";
                                                        }
                                                        // thông tin hàng hóa
                                                        DataDetails_MIV dataDetails_MIV = new DataDetails_MIV();
                                                        dataDetails_MIV.stt_rec0 = i.ToString().PadLeft(8, '0');
                                                        dataDetails_MIV.inv_itemCode = materialGoods.MaterialGoodsCode ?? "";
                                                        dataDetails_MIV.inv_itemName = itemD.Description ?? "";
                                                        dataDetails_MIV.inv_unitName = itemD.Unit;
                                                        dataDetails_MIV.inv_unitCode = itemD.Unit;
                                                        dataDetails_MIV.inv_unitPrice = itemD.UnitPriceOriginal;
                                                        dataDetails_MIV.inv_quantity = itemD.Quantity ?? 0;
                                                        dataDetails_MIV.inv_TotalAmountWithoutVat = itemD.AmountOriginal;
                                                        dataDetails_MIV.inv_vatAmount = itemD.VATAmountOriginal;
                                                        dataDetails_MIV.inv_discountAmount = itemD.DiscountAmount;
                                                        dataDetails_MIV.inv_discountPercentage = itemD.DiscountRate ?? 0;
                                                        dataDetails_MIV.inv_TotalAmount = itemD.AmountOriginal + itemD.VATAmountOriginal - itemD.DiscountAmount;
                                                        dataDetails_MIV.ma_thue = itemD.VATRate != -2 ? itemD.VATRate ?? 0 : -1;
                                                        //dataDetails_MIV.inv_discountPercentage
                                                        details_MIV.data.Add(dataDetails_MIV);
                                                    }

                                                    dataInvoice_MIV.inv_vatAmount = sAInvoice.TotalVATAmountOriginal;
                                                    dataInvoice_MIV.inv_discountAmount = sAInvoice.TotalDiscountAmountOriginal;
                                                    dataInvoice_MIV.inv_TotalAmountWithoutVat = sAInvoice.TotalAmountOriginal;
                                                    dataInvoice_MIV.inv_TotalAmount = sAInvoice.TotalPaymentAmountOriginal;
                                                    dataInvoice_MIV.details.Add(details_MIV);
                                                }
                                                if (check == 0)
                                                {
                                                    invoice_MIV.data.Add(dataInvoice_MIV);
                                                    jsonRequest = JsonConvert.SerializeObject(invoice_MIV, Newtonsoft.Json.Formatting.None,
                                                                                                        new JsonSerializerSettings
                                                                                                        {
                                                                                                            NullValueHandling = NullValueHandling.Ignore
                                                                                                        });
                                                    string contentType = "application/json";
                                                    string apiLink = Utils.GetPathAccess() + supplierService.ApiService.Where(n => n.SupplierServiceCode == "MIV").FirstOrDefault(n => n.ApiName == "TaoHoaDon").ApiPath;
                                                    string autStr = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "Token_MIV").Data + ";VP";
                                                    try
                                                    {
                                                        string result = CreateRequest.webRequest_MIV(apiLink, jsonRequest, autStr, "POST", contentType);
                                                        JObject jObject = JObject.Parse(result);
                                                        Respone_MIV respone_MIV = JsonConvert.DeserializeObject<Respone_MIV>(result);
                                                        if (respone_MIV.ok)
                                                        {
                                                            select.SetProperty("ID_MIV", respone_MIV.data.First().inv_InvoiceAuth_id);
                                                            select.SetProperty("InvoiceNo", respone_MIV.data.First().inv_invoiceNumber);
                                                            //string token = (string)jObject["token"];
                                                            //string error = (string)jObject["error"];
                                                        }
                                                        else
                                                        {
                                                            MSG.Error(respone_MIV.error);
                                                        }

                                                    }
                                                    catch (Exception ex)
                                                    {

                                                    }
                                                }
                                                else if (check == 1 && (select.GetProperty("InvoiceNo") == null || select.GetProperty("InvoiceNo") == "")) // Điều chỉnh tăng
                                                {
                                                    string contentType = "application/json";
                                                    string apiLink = Utils.GetPathAccess() + supplierService.ApiService.Where(n => n.SupplierServiceCode == "MIV").FirstOrDefault(n => n.ApiName == "HDDCTang").ApiPath;
                                                    string autStr = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "Token_MIV").Data + ";VP";
                                                    try
                                                    {
                                                        string result = CreateRequest.webRequest_MIV(apiLink, jsonRequest, autStr, "POST", contentType);
                                                        ResponeAdjust responeAdjust = JsonConvert.DeserializeObject<ResponeAdjust>(result);
                                                        if (responeAdjust.error.IsNullOrEmpty())
                                                        {
                                                            select.SetProperty("ID_MIV", responeAdjust.ok.inv_InvoiceAuth_id);
                                                            select.SetProperty("InvoiceNo", responeAdjust.ok.inv_invoiceNumber);
                                                            //string token = (string)jObject["token"];
                                                            //string error = (string)jObject["error"];
                                                        }
                                                        else
                                                        {
                                                            MSG.Error(responeAdjust.error);
                                                        }

                                                    }
                                                    catch (Exception ex)
                                                    {

                                                    }
                                                }
                                                else if (check == 2 && (select.GetProperty("InvoiceNo") == null || select.GetProperty("InvoiceNo") == "")) //Điều chỉnh giảm
                                                {
                                                    string contentType = "application/json";
                                                    string apiLink = Utils.GetPathAccess() + supplierService.ApiService.Where(n => n.SupplierServiceCode == "MIV").FirstOrDefault(n => n.ApiName == "HDDCGiam").ApiPath;
                                                    string autStr = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "Token_MIV").Data + ";VP";
                                                    try
                                                    {
                                                        string result = CreateRequest.webRequest_MIV(apiLink, jsonRequest, autStr, "POST", contentType);
                                                        ResponeAdjust responeAdjust = JsonConvert.DeserializeObject<ResponeAdjust>(result);
                                                        if (responeAdjust.error.IsNullOrEmpty())
                                                        {
                                                            select.SetProperty("ID_MIV", responeAdjust.ok.inv_InvoiceAuth_id);
                                                            select.SetProperty("InvoiceNo", responeAdjust.ok.inv_invoiceNumber);
                                                            //string token = (string)jObject["token"];
                                                            //string error = (string)jObject["error"];
                                                        }
                                                        else
                                                        {
                                                            MSG.Error(responeAdjust.error);
                                                        }

                                                    }
                                                    catch (Exception ex)
                                                    {

                                                    }
                                                }
                                                else if (check == 3 && (select.GetProperty("InvoiceNo") == null || select.GetProperty("InvoiceNo") == "")) //Điều chỉnh thông tin
                                                {
                                                    string contentType = "application/json";
                                                    string apiLink = Utils.GetPathAccess() + supplierService.ApiService.Where(n => n.SupplierServiceCode == "MIV").FirstOrDefault(n => n.ApiName == "HDDCDinhDanh").ApiPath;
                                                    string autStr = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "Token_MIV").Data + ";VP";
                                                    try
                                                    {
                                                        string result = CreateRequest.webRequest_MIV(apiLink, jsonRequest, autStr, "POST", contentType);
                                                        ResponeAdjust responeAdjust = JsonConvert.DeserializeObject<ResponeAdjust>(result);
                                                        if (responeAdjust.error.IsNullOrEmpty())
                                                        {
                                                            select.SetProperty("ID_MIV", responeAdjust.ok.inv_InvoiceAuth_id);
                                                            select.SetProperty("InvoiceNo", responeAdjust.ok.inv_invoiceNumber);
                                                            //string token = (string)jObject["token"];
                                                            //string error = (string)jObject["error"];
                                                        }
                                                        else
                                                        {
                                                            MSG.Error(responeAdjust.error);
                                                        }

                                                    }
                                                    catch (Exception ex)
                                                    {

                                                    }
                                                }
                                            }
                                            #endregion
                                            #region Tạo hóa đơn SDS
                                            else if (supplierService.SupplierServiceCode == "SDS")
                                            {
                                                // Hóa đơn mới tạo lập StatusInvoice = 0
                                                if (@select.GetProperty<T, int>("StatusInvoice") == 0 || @select.GetProperty<T, int>("StatusInvoice") == 6)
                                                {
                                                    var request = new Request();
                                                    request.Pattern = @select.GetProperty("InvoiceTemplate").ToString();
                                                    request.Serial = @select.GetProperty("InvoiceSeries").ToString();

                                                    EInvoices invoices = new EInvoices();

                                                    Inv inv = new Inv();
                                                    int check = 0;
                                                    SAInvoice sAInvoice = @select as SAInvoice;
                                                    SABill sABill = @select as SABill;
                                                    PPDiscountReturn pPDiscountReturn = @select as PPDiscountReturn;
                                                    SAReturn sAReturn = @select as SAReturn;
                                                    if (sAInvoice == null)
                                                    {
                                                        if (sABill == null)
                                                        {

                                                            if (sAReturn == null)
                                                            {
                                                                check = 3;
                                                                AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == pPDiscountReturn.AccountingObjectID);

                                                                inv.Key = pPDiscountReturn.ID.ToString();
                                                                inv.Invoice.InvNo = pPDiscountReturn.InvoiceNo ?? "";
                                                                inv.Invoice.CusCode = accountingObject.AccountingObjectCode ?? "";
                                                                inv.Invoice.Buyer = pPDiscountReturn.OContactName ?? "";
                                                                inv.Invoice.CusName = pPDiscountReturn.AccountingObjectName ?? "";
                                                                inv.Invoice.Email = accountingObject.Email ?? "";
                                                                inv.Invoice.CusAddress = pPDiscountReturn.AccountingObjectAddress ?? "";
                                                                inv.Invoice.CusBankName = pPDiscountReturn.AccountingObjectBankName ?? "";
                                                                inv.Invoice.CusBankNo = pPDiscountReturn.AccountingObjectBankAccount ?? "";
                                                                inv.Invoice.CusPhone = accountingObject.ContactMobile;
                                                                inv.Invoice.PaymentMethod = pPDiscountReturn.PaymentMethod;
                                                                inv.Invoice.CusTaxCode = pPDiscountReturn.CompanyTaxCode;
                                                                if (pPDiscountReturn.InvoiceDate != null)
                                                                {
                                                                    DateTime dt = pPDiscountReturn.InvoiceDate ?? DateTime.Now;
                                                                    inv.Invoice.ArisingDate = dt.Date.ToString("dd/MM/yyyy");
                                                                }
                                                                else
                                                                {
                                                                    inv.Invoice.ArisingDate = "";
                                                                }
                                                                inv.Invoice.ExchangeRate = pPDiscountReturn.ExchangeRate;
                                                                inv.Invoice.CurrencyUnit = pPDiscountReturn.CurrencyID;
                                                                //inv.Invoice.Extra = Eitem.Extra;
                                                                foreach (PPDiscountReturnDetail itemD in pPDiscountReturn.PPDiscountReturnDetails.OrderBy(n => n.OrderPriority).ToList())
                                                                {
                                                                    MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == itemD.MaterialGoodsID);
                                                                    Product prd = new Product();
                                                                    prd.Code = materialGoods.MaterialGoodsCode ?? "";
                                                                    prd.ProdName = itemD.Description ?? "";
                                                                    prd.ProdUnit = itemD.Unit;
                                                                    prd.ProdQuantity = itemD.Quantity ?? 0;
                                                                    //prd.ProdPrice = itemD.UnitPrice;
                                                                    //prd.Total = itemD.Amount;
                                                                    if (inv.Invoice.CurrencyUnit == "VND")
                                                                    {
                                                                        prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal ?? 0, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaQuyDoi), MidpointRounding.AwayFromZero);
                                                                    }
                                                                    else
                                                                    {
                                                                        prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal ?? 0, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaNgoaiTe), MidpointRounding.AwayFromZero);
                                                                    }
                                                                    prd.Total = itemD.AmountOriginal ?? 0;

                                                                    prd.VATRate = itemD.VATRate != -2 ? Convert.ToInt32(itemD.VATRate) : -1;
                                                                    if (prd.VATRate == -1)
                                                                    {
                                                                        prd.VATAmount = 0;
                                                                    }
                                                                    else
                                                                    {
                                                                        if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                        {
                                                                            prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                        }
                                                                        else
                                                                        {
                                                                            prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                        }
                                                                    }
                                                                    prd.Amount = prd.Total + prd.VATAmount;
                                                                    if (!string.IsNullOrEmpty(itemD.LotNo) || itemD.ExpiryDate != null)
                                                                    {
                                                                        prd.Extra = @"{""Lot"":""";
                                                                        prd.Extra += itemD.LotNo != null ? itemD.LotNo : "";
                                                                        if (itemD.ExpiryDate != null)
                                                                        {
                                                                            DateTime date = itemD.ExpiryDate ?? DateTime.Now;
                                                                            prd.Extra += @""",""ExpireDate"":""" + date.ToString("dd/MM/yyyy") + @"""}";
                                                                        }
                                                                        else
                                                                        {
                                                                            prd.Extra += @""",""ExpireDate"":""" + @"""}";
                                                                        }

                                                                    }
                                                                    else
                                                                    {
                                                                        prd.Extra = "";
                                                                    }

                                                                    decimal sub = 0M;
                                                                    var VATAmount = 0M;
                                                                    if (prd.VATRate == -1)
                                                                    {
                                                                        prd.VATAmount = 0;
                                                                    }
                                                                    else
                                                                    {
                                                                        if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                        {
                                                                            VATAmount = Math.Round(((prd.Total) * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                        }
                                                                        else
                                                                        {
                                                                            VATAmount = Math.Round(((prd.Total) * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                        }
                                                                    }
                                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                    {
                                                                        sub = Math.Round(itemD.VATAmountOriginal ?? 0, MidpointRounding.AwayFromZero) - VATAmount;
                                                                    }
                                                                    else
                                                                    {
                                                                        sub = Math.Round(itemD.VATAmountOriginal ?? 0, 2, MidpointRounding.AwayFromZero) - VATAmount;
                                                                    }


                                                                    if (Math.Abs(sub) > 0)
                                                                    {
                                                                        prd.VATAmount = prd.VATAmount + sub;

                                                                    }

                                                                    prd.Amount = prd.Total + prd.VATAmount;
                                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                    {
                                                                        prd.Total = Math.Round(itemD.AmountOriginal ?? 0, MidpointRounding.AwayFromZero);
                                                                    }
                                                                    else
                                                                    {
                                                                        prd.Total = itemD.AmountOriginal ?? 0;
                                                                    }
                                                                    inv.Invoice.Products.Add(prd);

                                                                    //Product prdCK = new Product();
                                                                    //prdCK.ProdName = "Chiết khấu";
                                                                    //prdCK.VATRate = Convert.ToInt32(itemD.VATRate);
                                                                    ////prdCK.Total = -itemD.DiscountAmount;
                                                                    //prdCK.Total = -itemD.DiscountAmountOriginal??0;
                                                                    //prdCK.VATAmount = -(-prdCK.Total * (decimal)prdCK.VATRate) / 100;
                                                                    //prdCK.Amount = prdCK.Total + prdCK.VATAmount;


                                                                    //if (prdCK.Total != 0)
                                                                    //{
                                                                    //    inv.Invoice.Products.Add(prdCK);
                                                                    //}
                                                                }
                                                            }
                                                            else
                                                            {
                                                                check = 2;
                                                                AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sAReturn.AccountingObjectID);

                                                                inv.Key = sAReturn.ID.ToString();
                                                                inv.Invoice.InvNo = sAReturn.InvoiceNo ?? "";
                                                                inv.Invoice.CusCode = accountingObject.AccountingObjectCode ?? "";
                                                                inv.Invoice.Buyer = sAReturn.ContactName ?? "";
                                                                inv.Invoice.CusName = sAReturn.AccountingObjectName ?? "";
                                                                inv.Invoice.Email = accountingObject.Email ?? "";
                                                                inv.Invoice.CusAddress = sAReturn.AccountingObjectAddress ?? "";
                                                                inv.Invoice.CusBankName = sAReturn.AccountingObjectBankName ?? "";
                                                                inv.Invoice.CusBankNo = sAReturn.AccountingObjectBankAccount ?? "";
                                                                inv.Invoice.CusPhone = accountingObject.ContactMobile;
                                                                inv.Invoice.PaymentMethod = sAReturn.PaymentMethod;
                                                                inv.Invoice.CusTaxCode = sAReturn.CompanyTaxCode;
                                                                if (sAReturn.InvoiceDate != null)
                                                                {
                                                                    DateTime dt = sAReturn.InvoiceDate ?? DateTime.Now;
                                                                    inv.Invoice.ArisingDate = dt.Date.ToString("dd/MM/yyyy");
                                                                }
                                                                else
                                                                {
                                                                    inv.Invoice.ArisingDate = "";
                                                                }
                                                                inv.Invoice.ExchangeRate = sAReturn.ExchangeRate;
                                                                inv.Invoice.CurrencyUnit = sAReturn.CurrencyID;
                                                                //inv.Invoice.Extra = Eitem.Extra;
                                                                foreach (SAReturnDetail itemD in sAReturn.SAReturnDetails.OrderBy(n => n.OrderPriority).ToList())
                                                                {
                                                                    MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == itemD.MaterialGoodsID);
                                                                    Product prd = new Product();
                                                                    prd.Code = materialGoods.MaterialGoodsCode ?? "";
                                                                    prd.ProdName = itemD.Description ?? "";
                                                                    if (itemD.IsPromotion != null)
                                                                    {
                                                                        if (itemD.IsPromotion ?? false)
                                                                        {
                                                                            prd.ProdName += " (Hàng khuyến mại)";
                                                                        }
                                                                    }
                                                                    prd.ProdUnit = itemD.Unit;
                                                                    prd.ProdQuantity = itemD.Quantity ?? 0;
                                                                    //prd.ProdPrice = itemD.UnitPrice;
                                                                    //prd.Total = itemD.Amount;
                                                                    if (inv.Invoice.CurrencyUnit == "VND")
                                                                    {
                                                                        prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaQuyDoi), MidpointRounding.AwayFromZero);
                                                                    }
                                                                    else
                                                                    {
                                                                        prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaNgoaiTe), MidpointRounding.AwayFromZero);
                                                                    }
                                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                    {
                                                                        prd.Total = Math.Round(itemD.AmountOriginal, MidpointRounding.AwayFromZero);
                                                                    }
                                                                    else
                                                                    {
                                                                        prd.Total = itemD.AmountOriginal;
                                                                    }

                                                                    prd.VATRate = itemD.VATRate != -2 ? Convert.ToInt32(itemD.VATRate) : -1;
                                                                    if (prd.VATRate == -1)
                                                                    {
                                                                        prd.VATAmount = 0;
                                                                    }
                                                                    else
                                                                    {
                                                                        if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                        {
                                                                            prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                        }
                                                                        else
                                                                        {
                                                                            prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                        }
                                                                    }
                                                                    prd.Amount = prd.Total + prd.VATAmount;
                                                                    if (!string.IsNullOrEmpty(itemD.LotNo) || itemD.ExpiryDate != null)
                                                                    {
                                                                        prd.Extra = @"{""Lot"":""";
                                                                        prd.Extra += itemD.LotNo != null ? itemD.LotNo : "";
                                                                        if (itemD.ExpiryDate != null)
                                                                        {
                                                                            DateTime date = itemD.ExpiryDate ?? DateTime.Now;
                                                                            prd.Extra += @""",""ExpireDate"":""" + date.ToString("dd/MM/yyyy") + @"""}";
                                                                        }
                                                                        else
                                                                        {
                                                                            prd.Extra += @""",""ExpireDate"":""" + @"""}";
                                                                        }

                                                                    }
                                                                    else
                                                                    {
                                                                        prd.Extra = "";
                                                                    }

                                                                    Product prdCK = new Product();
                                                                    prdCK.ProdName = "Chiết khấu";
                                                                    prdCK.VATRate = itemD.VATRate != -2 ? Convert.ToInt32(itemD.VATRate) : -1;
                                                                    //prdCK.Total = -itemD.DiscountAmount;
                                                                    if (itemD.DiscountRate != null)
                                                                    {
                                                                        if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                        {
                                                                            prdCK.Total = -Math.Round((prd.Total * itemD.DiscountRate ?? 0) / 100, MidpointRounding.AwayFromZero);
                                                                        }
                                                                        else
                                                                        {
                                                                            prdCK.Total = -Math.Round((prd.Total * itemD.DiscountRate ?? 0) / 100, 2, MidpointRounding.AwayFromZero);
                                                                        }
                                                                    }
                                                                    if (prdCK.VATRate == -1)
                                                                    {
                                                                        prdCK.VATAmount = 0;
                                                                    }
                                                                    else
                                                                    {
                                                                        if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                        {
                                                                            prdCK.VATAmount = Math.Round((prdCK.Total * (decimal)prdCK.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                        }
                                                                        else
                                                                        {
                                                                            prdCK.VATAmount = Math.Round((prdCK.Total * (decimal)prdCK.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                        }
                                                                    }
                                                                    prdCK.Amount = prdCK.Total + prdCK.VATAmount;

                                                                    decimal sub = 0M;
                                                                    if (itemD.DiscountAmountOriginal != 0M)
                                                                    {
                                                                        var VATAmount = 0M;
                                                                        if (prd.VATRate == -1)
                                                                        {
                                                                            prd.VATAmount = 0;
                                                                        }
                                                                        else
                                                                        {
                                                                            if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                            {
                                                                                VATAmount = Math.Round(((prd.Total + prdCK.Total) * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                            }
                                                                            else
                                                                            {
                                                                                VATAmount = Math.Round(((prd.Total + prdCK.Total) * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                            }
                                                                        }
                                                                        if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                        {
                                                                            sub = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - VATAmount;
                                                                        }
                                                                        else
                                                                        {
                                                                            sub = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - VATAmount;
                                                                        }

                                                                    }
                                                                    else
                                                                    {
                                                                        if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                        {
                                                                            sub = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                        }
                                                                        else
                                                                        {
                                                                            sub = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                        }
                                                                    }

                                                                    if (Math.Abs(sub) > 0)
                                                                    {
                                                                        prd.VATAmount = prd.VATAmount + sub;

                                                                    }
                                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                    {
                                                                        prdCK.VATAmount = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                    }
                                                                    else
                                                                    {
                                                                        prdCK.VATAmount = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                    }
                                                                    prdCK.Amount = prdCK.Total + prdCK.VATAmount;
                                                                    prd.Amount = prd.Total + prd.VATAmount;

                                                                    inv.Invoice.Products.Add(prd);
                                                                    if (prdCK.Total != 0)
                                                                    {
                                                                        inv.Invoice.Products.Add(prdCK);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            check = 1;
                                                            AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sABill.AccountingObjectID);

                                                            inv.Key = sABill.ID.ToString();
                                                            inv.Invoice.InvNo = sABill.InvoiceNo ?? "";
                                                            inv.Invoice.CusCode = accountingObject.AccountingObjectCode ?? "";
                                                            inv.Invoice.Buyer = sABill.ContactName ?? "";
                                                            inv.Invoice.CusName = sABill.AccountingObjectName ?? "";
                                                            inv.Invoice.Email = accountingObject.Email ?? "";
                                                            inv.Invoice.CusAddress = sABill.AccountingObjectAddress;
                                                            inv.Invoice.CusBankName = sABill.AccountingObjectBankName ?? "";
                                                            inv.Invoice.CusBankNo = sABill.AccountingObjectBankAccount ?? "";
                                                            inv.Invoice.CusPhone = accountingObject.ContactMobile;
                                                            inv.Invoice.PaymentMethod = sABill.PaymentMethod;
                                                            inv.Invoice.CusTaxCode = sABill.CompanyTaxCode;
                                                            if (sABill.InvoiceDate != null)
                                                            {
                                                                DateTime dt = sABill.InvoiceDate ?? DateTime.Now;
                                                                inv.Invoice.ArisingDate = dt.Date.ToString("dd/MM/yyyy");
                                                            }
                                                            else
                                                            {
                                                                inv.Invoice.ArisingDate = "";
                                                            }
                                                            inv.Invoice.ExchangeRate = sABill.ExchangeRate;
                                                            inv.Invoice.CurrencyUnit = sABill.CurrencyID;
                                                            //inv.Invoice.Extra = Eitem.Extra;
                                                            foreach (SABillDetail itemD in sABill.SABillDetails.OrderBy(n => n.OrderPriority).ToList())
                                                            {
                                                                MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == itemD.MaterialGoodsID);
                                                                Product prd = new Product();
                                                                prd.Code = materialGoods.MaterialGoodsCode;
                                                                prd.ProdName = itemD.Description;
                                                                if (itemD.IsPromotion != null)
                                                                {
                                                                    if (itemD.IsPromotion ?? false)
                                                                    {
                                                                        prd.ProdName += " (Hàng khuyến mại)";
                                                                    }
                                                                }
                                                                prd.ProdUnit = itemD.Unit;
                                                                prd.ProdQuantity = itemD.Quantity;
                                                                if (inv.Invoice.CurrencyUnit == "VND")
                                                                {
                                                                    prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaQuyDoi), MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaNgoaiTe), MidpointRounding.AwayFromZero);
                                                                }

                                                                prd.VATRate = itemD.VATRate != -2 ? Convert.ToInt32(itemD.VATRate) : -1;
                                                                //prd.VATRate = itemD.VATRate;
                                                                if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                {
                                                                    prd.Total = Math.Round(itemD.AmountOriginal, MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    prd.Total = itemD.AmountOriginal;
                                                                }

                                                                if (prd.VATRate == -1)
                                                                {
                                                                    prd.VATAmount = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                    {
                                                                        prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                    }
                                                                    else
                                                                    {
                                                                        prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                    }
                                                                }
                                                                prd.Amount = prd.Total + prd.VATAmount;
                                                                if (!string.IsNullOrEmpty(itemD.LotNo) || itemD.ExpiryDate != null)
                                                                {
                                                                    prd.Extra = @"{""Lot"":""";
                                                                    prd.Extra += itemD.LotNo != null ? itemD.LotNo : "";
                                                                    if (itemD.ExpiryDate != null)
                                                                    {
                                                                        DateTime date = itemD.ExpiryDate ?? DateTime.Now;
                                                                        prd.Extra += @""",""ExpireDate"":""" + date.ToString("dd/MM/yyyy") + @"""}";
                                                                    }
                                                                    else
                                                                    {
                                                                        prd.Extra += @""",""ExpireDate"":""" + @"""}";
                                                                    }

                                                                }
                                                                else
                                                                {
                                                                    prd.Extra = "";
                                                                }

                                                                Product prdCK = new Product();
                                                                prdCK.ProdName = "Chiết khấu";
                                                                prdCK.VATRate = itemD.VATRate != -2 ? Convert.ToInt32(itemD.VATRate) : -1;
                                                                //prdCK.Total = -itemD.DiscountAmount;
                                                                if (itemD.DiscountRate != null)
                                                                {
                                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                    {
                                                                        prdCK.Total = -Math.Round((prd.Total * (decimal)itemD.DiscountRate) / 100, MidpointRounding.AwayFromZero);
                                                                    }
                                                                    else
                                                                    {
                                                                        prdCK.Total = -Math.Round((prd.Total * (decimal)itemD.DiscountRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                    }
                                                                }
                                                                if (prdCK.VATRate == -1)
                                                                {
                                                                    prdCK.VATAmount = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                    {
                                                                        prdCK.VATAmount = Math.Round((prdCK.Total * (decimal)prdCK.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                    }
                                                                    else
                                                                    {
                                                                        prdCK.VATAmount = Math.Round((prdCK.Total * (decimal)prdCK.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                    }

                                                                }
                                                                prdCK.Amount = prdCK.Total + prdCK.VATAmount;

                                                                decimal sub = 0M;
                                                                if (itemD.DiscountAmountOriginal != 0M)
                                                                {
                                                                    var VATAmount = 0M;
                                                                    if (prd.VATRate == -1)
                                                                    {
                                                                        prd.VATAmount = 0;
                                                                    }
                                                                    else
                                                                    {
                                                                        if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                        {
                                                                            VATAmount = Math.Round(((prd.Total + prdCK.Total) * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                        }
                                                                        else
                                                                        {
                                                                            VATAmount = Math.Round(((prd.Total + prdCK.Total) * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                        }
                                                                    }
                                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                    {
                                                                        sub = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - VATAmount;
                                                                    }
                                                                    else
                                                                    {
                                                                        sub = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - VATAmount;
                                                                    }

                                                                }
                                                                else
                                                                {
                                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                    {
                                                                        sub = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                    }
                                                                    else
                                                                    {
                                                                        sub = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                    }
                                                                }

                                                                if (Math.Abs(sub) > 0)
                                                                {
                                                                    prd.VATAmount = prd.VATAmount + sub;

                                                                }
                                                                if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                {
                                                                    prdCK.VATAmount = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                }
                                                                else
                                                                {
                                                                    prdCK.VATAmount = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                }
                                                                prdCK.Amount = prdCK.Total + prdCK.VATAmount;
                                                                prd.Amount = prd.Total + prd.VATAmount;

                                                                inv.Invoice.Products.Add(prd);
                                                                if (prdCK.Total != 0)
                                                                {
                                                                    inv.Invoice.Products.Add(prdCK);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        check = 0;
                                                        AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sAInvoice.AccountingObjectID);

                                                        inv.Key = sAInvoice.ID.ToString();
                                                        inv.Invoice.InvNo = sAInvoice.InvoiceNo ?? "";
                                                        inv.Invoice.CusCode = accountingObject.AccountingObjectCode ?? "";
                                                        inv.Invoice.Buyer = sAInvoice.ContactName ?? "";
                                                        inv.Invoice.CusName = sAInvoice.AccountingObjectName ?? "";
                                                        inv.Invoice.Email = accountingObject.Email ?? "";
                                                        inv.Invoice.CusAddress = sAInvoice.AccountingObjectAddress ?? "";
                                                        inv.Invoice.CusBankName = sAInvoice.AccountingObjectBankName ?? "";
                                                        inv.Invoice.CusBankNo = sAInvoice.AccountingObjectBankAccount ?? "";
                                                        inv.Invoice.CusPhone = accountingObject.ContactMobile;
                                                        inv.Invoice.CusTaxCode = sAInvoice.CompanyTaxCode;
                                                        inv.Invoice.PaymentMethod = sAInvoice.PaymentMethod;
                                                        if (sAInvoice.InvoiceDate != null)
                                                        {
                                                            DateTime dt = sAInvoice.InvoiceDate ?? DateTime.Now;
                                                            inv.Invoice.ArisingDate = dt.Date.ToString("dd/MM/yyyy");
                                                        }
                                                        else
                                                        {
                                                            inv.Invoice.ArisingDate = "";
                                                        }
                                                        inv.Invoice.ExchangeRate = sAInvoice.ExchangeRate;
                                                        inv.Invoice.CurrencyUnit = sAInvoice.CurrencyID;
                                                        //inv.Invoice.Extra = Eitem.Extra;
                                                        foreach (SAInvoiceDetail itemD in sAInvoice.SAInvoiceDetails.OrderBy(n => n.OrderPriority).ToList())
                                                        {
                                                            MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == itemD.MaterialGoodsID);
                                                            Product prd = new Product();
                                                            prd.Code = materialGoods.MaterialGoodsCode ?? "";
                                                            prd.ProdName = itemD.Description ?? "";
                                                            if (itemD.IsPromotion != null)
                                                            {
                                                                if (itemD.IsPromotion ?? false)
                                                                {
                                                                    prd.ProdName += " (Hàng khuyến mại)";
                                                                }
                                                            }
                                                            prd.ProdUnit = itemD.Unit;
                                                            prd.ProdQuantity = itemD.Quantity ?? 0;
                                                            if (inv.Invoice.CurrencyUnit == "VND")
                                                            {
                                                                prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaQuyDoi), MidpointRounding.AwayFromZero);
                                                            }
                                                            else
                                                            {
                                                                prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaNgoaiTe), MidpointRounding.AwayFromZero);
                                                            }
                                                            if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                            {
                                                                prd.Total = Math.Round(itemD.AmountOriginal, MidpointRounding.AwayFromZero);
                                                            }
                                                            else
                                                            {
                                                                prd.Total = itemD.AmountOriginal;
                                                            }

                                                            prd.VATRate = itemD.VATRate != -2 ? Convert.ToInt32(itemD.VATRate) : -1;
                                                            if (prd.VATRate == -1)
                                                            {
                                                                prd.VATAmount = 0;
                                                            }
                                                            else
                                                            {
                                                                if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                {
                                                                    prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                }

                                                            }
                                                            prd.Amount = prd.Total + prd.VATAmount;
                                                            if (!string.IsNullOrEmpty(itemD.LotNo) || itemD.ExpiryDate != null)
                                                            {
                                                                prd.Extra = @"{""Lot"":""";
                                                                prd.Extra += itemD.LotNo != null ? itemD.LotNo : "";
                                                                if (itemD.ExpiryDate != null)
                                                                {
                                                                    DateTime date = itemD.ExpiryDate ?? DateTime.Now;
                                                                    prd.Extra += @""",""ExpireDate"":""" + date.ToString("dd/MM/yyyy") + @"""}";
                                                                }
                                                                else
                                                                {
                                                                    prd.Extra += @""",""ExpireDate"":""" + @"""}";
                                                                }

                                                            }
                                                            else
                                                            {
                                                                prd.Extra = "";
                                                            }
                                                            // Chiết khấu
                                                            Product prdCK = new Product();
                                                            prdCK.ProdName = "Chiết khấu";
                                                            prdCK.VATRate = itemD.VATRate != -2 ? Convert.ToInt32(itemD.VATRate) : -1;
                                                            if (itemD.DiscountRate != null)
                                                            {
                                                                if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                {
                                                                    prdCK.Total = -Math.Round((prd.Total * itemD.DiscountRate ?? 0) / 100, MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    prdCK.Total = -Math.Round((prd.Total * itemD.DiscountRate ?? 0) / 100, 2, MidpointRounding.AwayFromZero);
                                                                }
                                                            }

                                                            if (prdCK.VATRate == -1)
                                                            {
                                                                prdCK.VATAmount = 0;
                                                            }
                                                            else
                                                            {
                                                                if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                {
                                                                    prdCK.VATAmount = Math.Round((prdCK.Total * (decimal)prdCK.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    prdCK.VATAmount = Math.Round((prdCK.Total * (decimal)prdCK.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                }

                                                            }
                                                            prdCK.Amount = prdCK.Total + prdCK.VATAmount;

                                                            decimal sub = 0M;
                                                            if (itemD.DiscountAmountOriginal != 0M)
                                                            {
                                                                var VATAmount = 0M;
                                                                if (prd.VATRate == -1)
                                                                {
                                                                    prd.VATAmount = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                    {
                                                                        VATAmount = Math.Round(((prd.Total + prdCK.Total) * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                    }
                                                                    else
                                                                    {
                                                                        VATAmount = Math.Round(((prd.Total + prdCK.Total) * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                    }
                                                                }
                                                                if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                {
                                                                    sub = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - VATAmount;
                                                                }
                                                                else
                                                                {
                                                                    sub = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - VATAmount;
                                                                }

                                                            }
                                                            else
                                                            {
                                                                if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                                {
                                                                    sub = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                }
                                                                else
                                                                {
                                                                    sub = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                }
                                                            }

                                                            if (Math.Abs(sub) > 0)
                                                            {
                                                                prd.VATAmount = prd.VATAmount + sub;

                                                            }
                                                            if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                            {
                                                                prdCK.VATAmount = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                            }
                                                            else
                                                            {
                                                                prdCK.VATAmount = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                            }
                                                            prdCK.Amount = prdCK.Total + prdCK.VATAmount;
                                                            prd.Amount = prd.Total + prd.VATAmount;
                                                            //prdCK.Total = -itemD.DiscountAmount;

                                                            inv.Invoice.Products.Add(prd);
                                                            if (prdCK.Total != 0)
                                                            {
                                                                inv.Invoice.Products.Add(prdCK);
                                                            }
                                                        }

                                                    }
                                                    #region code mới
                                                    if (check == 0)
                                                    {
                                                        inv.Invoice.VATRate = inv.Invoice.Products.Count > 0 ? Convert.ToInt32(inv.Invoice.Products.First().VATRate).ToString() : "-1";

                                                        if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                        {
                                                            inv.Invoice.Amount = Math.Round(inv.Invoice.Products.Sum(n => n.Amount), MidpointRounding.AwayFromZero);
                                                            inv.Invoice.AmountInWords = NumberToWord.GetAmountInWords(inv.Invoice.Amount, sAInvoice.CurrencyID);
                                                            inv.Invoice.Total = Math.Round(inv.Invoice.Products.Sum(n => n.Total), MidpointRounding.AwayFromZero);
                                                            inv.Invoice.VATAmount = Math.Round(inv.Invoice.Products.Sum(n => n.VATAmount), MidpointRounding.AwayFromZero);
                                                        }
                                                        else
                                                        {
                                                            inv.Invoice.Amount = Math.Round(inv.Invoice.Products.Sum(n => n.Amount), 2, MidpointRounding.AwayFromZero);
                                                            if (inv.Invoice.CurrencyUnit == "USD" || inv.Invoice.CurrencyUnit == "EUR" || inv.Invoice.CurrencyUnit == "GBP" || inv.Invoice.CurrencyUnit == "SGD")
                                                                inv.Invoice.AmountInWords = NumberToWord.GetAmountInWords(inv.Invoice.Amount, sAInvoice.CurrencyID);
                                                            else
                                                                inv.Invoice.AmountInWords = Utils.So_chu(inv.Invoice.Amount, sAInvoice.CurrencyID);
                                                            inv.Invoice.Total = Math.Round(inv.Invoice.Products.Sum(n => n.Total), 2, MidpointRounding.AwayFromZero);
                                                            inv.Invoice.VATAmount = Math.Round(inv.Invoice.Products.Sum(n => n.VATAmount), 2, MidpointRounding.AwayFromZero);
                                                        }
                                                        invoices.Invoices.Add(inv);
                                                    }
                                                    else if (check == 1)
                                                    {
                                                        inv.Invoice.VATRate = inv.Invoice.Products.Count > 0 ? Convert.ToInt32(inv.Invoice.Products.First().VATRate).ToString() : "-1";
                                                        if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                        {
                                                            inv.Invoice.Amount = Math.Round(inv.Invoice.Products.Sum(n => n.Amount), MidpointRounding.AwayFromZero);
                                                            inv.Invoice.AmountInWords = NumberToWord.GetAmountInWords(Math.Round(inv.Invoice.Amount, MidpointRounding.AwayFromZero), sABill.CurrencyID);
                                                            inv.Invoice.Total = Math.Round(inv.Invoice.Products.Sum(n => n.Total), MidpointRounding.AwayFromZero);
                                                            inv.Invoice.VATAmount = Math.Round(inv.Invoice.Products.Sum(n => n.VATAmount), MidpointRounding.AwayFromZero);
                                                        }
                                                        else
                                                        {
                                                            inv.Invoice.Amount = Math.Round(inv.Invoice.Products.Sum(n => n.Amount), 2, MidpointRounding.AwayFromZero);
                                                            if (inv.Invoice.CurrencyUnit == "USD" || inv.Invoice.CurrencyUnit == "EUR" || inv.Invoice.CurrencyUnit == "GBP" || inv.Invoice.CurrencyUnit == "SGD")
                                                                inv.Invoice.AmountInWords = NumberToWord.GetAmountInWords(Math.Round(inv.Invoice.Amount, 2, MidpointRounding.AwayFromZero), sABill.CurrencyID);
                                                            else
                                                                inv.Invoice.AmountInWords = Utils.So_chu(Math.Round(inv.Invoice.Amount, 2, MidpointRounding.AwayFromZero), sABill.CurrencyID);
                                                            inv.Invoice.Total = Math.Round(inv.Invoice.Products.Sum(n => n.Total), 2, MidpointRounding.AwayFromZero);
                                                            inv.Invoice.VATAmount = Math.Round(inv.Invoice.Products.Sum(n => n.VATAmount), 2, MidpointRounding.AwayFromZero);
                                                        }
                                                        invoices.Invoices.Add(inv);
                                                    }
                                                    else if (check == 2)
                                                    {
                                                        inv.Invoice.VATRate = inv.Invoice.Products.Count > 0 ? Convert.ToInt32(inv.Invoice.Products.First().VATRate).ToString() : "-1";

                                                        if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                        {
                                                            inv.Invoice.Amount = Math.Round(inv.Invoice.Products.Sum(n => n.Amount), MidpointRounding.AwayFromZero);
                                                            inv.Invoice.AmountInWords = NumberToWord.GetAmountInWords(inv.Invoice.Amount, sAReturn.CurrencyID);
                                                            inv.Invoice.Total = Math.Round(inv.Invoice.Products.Sum(n => n.Total), MidpointRounding.AwayFromZero);
                                                            inv.Invoice.VATAmount = Math.Round(inv.Invoice.Products.Sum(n => n.VATAmount), MidpointRounding.AwayFromZero);
                                                        }
                                                        else
                                                        {
                                                            inv.Invoice.Amount = Math.Round(inv.Invoice.Products.Sum(n => n.Amount), 2, MidpointRounding.AwayFromZero);
                                                            if (inv.Invoice.CurrencyUnit == "USD" || inv.Invoice.CurrencyUnit == "EUR" || inv.Invoice.CurrencyUnit == "GBP" || inv.Invoice.CurrencyUnit == "SGD")
                                                                inv.Invoice.AmountInWords = NumberToWord.GetAmountInWords(inv.Invoice.Amount, sAReturn.CurrencyID);
                                                            else
                                                                inv.Invoice.AmountInWords = Utils.So_chu(inv.Invoice.Amount, sAReturn.CurrencyID);
                                                            inv.Invoice.Total = Math.Round(inv.Invoice.Products.Sum(n => n.Total), 2, MidpointRounding.AwayFromZero);
                                                            inv.Invoice.VATAmount = Math.Round(inv.Invoice.Products.Sum(n => n.VATAmount), 2, MidpointRounding.AwayFromZero);
                                                        }
                                                        invoices.Invoices.Add(inv);
                                                    }
                                                    else if (check == 3)
                                                    {
                                                        inv.Invoice.VATRate = inv.Invoice.Products.Count > 0 ? Convert.ToInt32(inv.Invoice.Products.First().VATRate).ToString() : "-1";
                                                        if (inv.Invoice.CurrencyUnit == "VND" || inv.Invoice.CurrencyUnit == "JPY")
                                                        {
                                                            inv.Invoice.Amount = Math.Round(inv.Invoice.Products.Sum(n => n.Amount), MidpointRounding.AwayFromZero);
                                                            inv.Invoice.AmountInWords = NumberToWord.GetAmountInWords(Math.Round(inv.Invoice.Amount, MidpointRounding.AwayFromZero), pPDiscountReturn.CurrencyID);
                                                            inv.Invoice.Total = Math.Round(inv.Invoice.Products.Sum(n => n.Total), MidpointRounding.AwayFromZero);
                                                            inv.Invoice.VATAmount = Math.Round(inv.Invoice.Products.Sum(n => n.VATAmount), MidpointRounding.AwayFromZero);
                                                        }
                                                        else
                                                        {
                                                            inv.Invoice.Amount = Math.Round(inv.Invoice.Products.Sum(n => n.Amount), 2, MidpointRounding.AwayFromZero);
                                                            if (inv.Invoice.CurrencyUnit == "USD" || inv.Invoice.CurrencyUnit == "EUR" || inv.Invoice.CurrencyUnit == "GBP" || inv.Invoice.CurrencyUnit == "SGD")
                                                                inv.Invoice.AmountInWords = NumberToWord.GetAmountInWords(inv.Invoice.Amount, pPDiscountReturn.CurrencyID);
                                                            else
                                                                inv.Invoice.AmountInWords = Utils.So_chu(inv.Invoice.Amount, pPDiscountReturn.CurrencyID);
                                                            inv.Invoice.Total = Math.Round(inv.Invoice.Products.Sum(n => n.Total), 2, MidpointRounding.AwayFromZero);
                                                            inv.Invoice.VATAmount = Math.Round(inv.Invoice.Products.Sum(n => n.VATAmount), 2, MidpointRounding.AwayFromZero);
                                                        }
                                                        invoices.Invoices.Add(inv);
                                                    }
                                                    #endregion
                                                    string API_Path = "";
                                                    if (@select.GetProperty<T, int>("StatusInvoice") == 6)
                                                    {
                                                        API_Path = "TaoHoaDonChuaKiSo_Cho";
                                                    }
                                                    else
                                                    {
                                                        API_Path = "TaoHoaDonChuaKiSo";
                                                    }
                                                    var xmldata = Utils.Serialize<EInvoices>(invoices);
                                                    XmlDocument xml = new XmlDocument();
                                                    xml.LoadXml(xmldata);
                                                    XmlNode xmldt = xml.SelectSingleNode("/EInvoices/Invoices");
                                                    request.XmlData = xmldt.OuterXml;
                                                    RestApiService client = new RestApiService(Utils.GetPathAccess(), Utils.GetUserName(), Core.ITripleDes.TripleDesDefaultDecryption(Utils.GetPassWords()));
                                                    Response response = client.Post(supplierService.ApiService.FirstOrDefault(n => n.ApiName == API_Path).ApiPath, request);
                                                    if (response != null && response.Status != 2)
                                                    {
                                                        MSG.Error(response.Message);
                                                    }
                                                }
                                                // Hóa đơn mới tạo lập điều chỉnh StatusInvoice = 7
                                                else if (@select.GetProperty<T, int>("StatusInvoice") == 8)
                                                {
                                                    var request = new Request();
                                                    AdjustInv AdjInv = new AdjustInv();

                                                    request.Pattern = @select.GetProperty("InvoiceTemplate").ToString();
                                                    request.Serial = @select.GetProperty("InvoiceSeries").ToString();

                                                    SABill sABill = @select as SABill;
                                                    if (sABill == null)
                                                    {
                                                    }
                                                    else
                                                    {
                                                        request.Ikey = sABill.IDAdjustInv.ToString();
                                                        AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sABill.AccountingObjectID);

                                                        AdjInv.Key = sABill.ID.ToString();
                                                        AdjInv.CusCode = accountingObject.AccountingObjectCode;
                                                        AdjInv.Buyer = sABill.ContactName;
                                                        AdjInv.CusName = sABill.AccountingObjectName;
                                                        AdjInv.Email = accountingObject.Email ?? "";
                                                        AdjInv.CusAddress = sABill.AccountingObjectAddress;
                                                        AdjInv.CusBankName = sABill.AccountingObjectBankName ?? "";
                                                        AdjInv.CusBankNo = sABill.AccountingObjectBankAccount ?? "";
                                                        AdjInv.CusPhone = accountingObject.ContactMobile;
                                                        AdjInv.CusTaxCode = sABill.CompanyTaxCode;
                                                        AdjInv.PaymentMethod = sABill.PaymentMethod;
                                                        AdjInv.Type = sABill.Type ?? 2;
                                                        if (sABill.InvoiceDate != null)
                                                        {
                                                            DateTime dt = sABill.InvoiceDate ?? DateTime.Now;
                                                            AdjInv.ArisingDate = dt.Date.Date.ToString("dd/MM/yyyy");
                                                        }
                                                        else
                                                        {
                                                            AdjInv.ArisingDate = "";
                                                        }
                                                        AdjInv.ExchangeRate = sABill.ExchangeRate;
                                                        AdjInv.CurrencyUnit = sABill.CurrencyID;
                                                        //AdjInv.Extra = Eitem.Extra;
                                                        foreach (SABillDetail itemD in sABill.SABillDetails.OrderBy(n => n.OrderPriority).ToList())
                                                        {
                                                            MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == itemD.MaterialGoodsID);
                                                            Product prd = new Product();
                                                            prd.Code = materialGoods.MaterialGoodsCode;
                                                            prd.ProdName = itemD.Description;
                                                            if (itemD.IsPromotion != null)
                                                            {
                                                                if (itemD.IsPromotion ?? false)
                                                                {
                                                                    prd.ProdName += " (Hàng khuyến mại)";
                                                                }
                                                            }
                                                            prd.ProdUnit = itemD.Unit;
                                                            prd.ProdQuantity = itemD.Quantity;
                                                            //prd.ProdPrice = itemD.UnitPrice;
                                                            //prd.Total = itemD.Amount;
                                                            if (AdjInv.CurrencyUnit == "VND")
                                                            {
                                                                prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaQuyDoi), MidpointRounding.AwayFromZero);
                                                            }
                                                            else
                                                            {
                                                                prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaNgoaiTe), MidpointRounding.AwayFromZero);
                                                            }
                                                            if (AdjInv.CurrencyUnit == "VND" || AdjInv.CurrencyUnit == "JPY")
                                                            {
                                                                prd.Total = Math.Round(itemD.AmountOriginal, MidpointRounding.AwayFromZero);
                                                            }
                                                            else
                                                            {
                                                                prd.Total = itemD.AmountOriginal;
                                                            }
                                                            prd.VATRate = itemD.VATRate != -2 ? Convert.ToInt32(itemD.VATRate) : -1;
                                                            if (prd.VATRate == -1)
                                                            {
                                                                prd.VATAmount = 0;
                                                            }
                                                            else
                                                            {
                                                                if (AdjInv.CurrencyUnit == "VND" || AdjInv.CurrencyUnit == "JPY")
                                                                {
                                                                    prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                }
                                                            }
                                                            //prd.VATRate = itemD.VATRate;
                                                            prd.Amount = prd.Total + prd.VATAmount;
                                                            if (!string.IsNullOrEmpty(itemD.LotNo) || itemD.ExpiryDate != null)
                                                            {
                                                                prd.Extra = @"{""Lot"":""";
                                                                prd.Extra += itemD.LotNo != null ? itemD.LotNo : "";
                                                                if (itemD.ExpiryDate != null)
                                                                {
                                                                    DateTime date = itemD.ExpiryDate ?? DateTime.Now;
                                                                    prd.Extra += @""",""ExpireDate"":""" + date.ToString("dd/MM/yyyy") + @"""}";
                                                                }
                                                                else
                                                                {
                                                                    prd.Extra += @""",""ExpireDate"":""" + @"""}";
                                                                }

                                                            }
                                                            else
                                                            {
                                                                prd.Extra = "";
                                                            }

                                                            Product prdCK = new Product();
                                                            prdCK.ProdName = "Chiết khấu";
                                                            prdCK.VATRate = itemD.VATRate != -2 ? Convert.ToInt32(itemD.VATRate) : -1;
                                                            //prdCK.Total = -itemD.DiscountAmount;
                                                            if (itemD.DiscountRate != null)
                                                            {
                                                                if (AdjInv.CurrencyUnit == "VND" || AdjInv.CurrencyUnit == "JPY")
                                                                {
                                                                    prdCK.Total = -Math.Round((prd.Total * (decimal)itemD.DiscountRate) / 100, MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    prdCK.Total = -Math.Round((prd.Total * (decimal)itemD.DiscountRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                }
                                                            }
                                                            if (prdCK.VATRate == -1)
                                                            {
                                                                prdCK.VATAmount = 0;
                                                            }
                                                            else
                                                            {
                                                                if (AdjInv.CurrencyUnit == "VND" || AdjInv.CurrencyUnit == "JPY")
                                                                {
                                                                    prdCK.VATAmount = Math.Round((prdCK.Total * (decimal)prdCK.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    prdCK.VATAmount = Math.Round((prdCK.Total * (decimal)prdCK.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                }
                                                            }
                                                            prdCK.Amount = prdCK.Total + prdCK.VATAmount;

                                                            decimal sub = 0M;
                                                            if (itemD.DiscountAmountOriginal != 0M)
                                                            {
                                                                var VATAmount = 0M;
                                                                if (prd.VATRate == -1)
                                                                {
                                                                    prd.VATAmount = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (AdjInv.CurrencyUnit == "VND" || AdjInv.CurrencyUnit == "JPY")
                                                                    {
                                                                        VATAmount = Math.Round(((prd.Total + prdCK.Total) * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                    }
                                                                    else
                                                                    {
                                                                        VATAmount = Math.Round(((prd.Total + prdCK.Total) * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                    }
                                                                }
                                                                if (AdjInv.CurrencyUnit == "VND" || AdjInv.CurrencyUnit == "JPY")
                                                                {
                                                                    sub = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - VATAmount;
                                                                }
                                                                else
                                                                {
                                                                    sub = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - VATAmount;
                                                                }

                                                            }
                                                            else
                                                            {
                                                                if (AdjInv.CurrencyUnit == "VND" || AdjInv.CurrencyUnit == "JPY")
                                                                {
                                                                    sub = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                }
                                                                else
                                                                {
                                                                    sub = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                }
                                                            }

                                                            if (Math.Abs(sub) > 0)
                                                            {
                                                                prd.VATAmount = prd.VATAmount + sub;

                                                            }
                                                            if (AdjInv.CurrencyUnit == "VND" || AdjInv.CurrencyUnit == "JPY")
                                                            {
                                                                prdCK.VATAmount = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                            }
                                                            else
                                                            {
                                                                prdCK.VATAmount = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                            }
                                                            prdCK.Amount = prdCK.Total + prdCK.VATAmount;
                                                            prd.Amount = prd.Total + prd.VATAmount;

                                                            AdjInv.Products.Add(prd);
                                                            if (prdCK.Total != 0)
                                                            {
                                                                AdjInv.Products.Add(prdCK);
                                                            }
                                                        }
                                                    }
                                                    #region code mới
                                                    AdjInv.VATRate = AdjInv.Products.Count > 0 ? Convert.ToInt32(AdjInv.Products.First().VATRate).ToString() : "-1";

                                                    if (AdjInv.CurrencyUnit == "VND" || AdjInv.CurrencyUnit == "JPY")
                                                    {
                                                        AdjInv.Amount = Math.Round(AdjInv.Products.Sum(n => n.Amount), MidpointRounding.AwayFromZero);
                                                        AdjInv.AmountInWords = NumberToWord.GetAmountInWords(AdjInv.Amount, sABill.CurrencyID);
                                                        AdjInv.Total = Math.Round(AdjInv.Products.Sum(n => n.Total), MidpointRounding.AwayFromZero);
                                                        AdjInv.VATAmount = Math.Round(AdjInv.Products.Sum(n => n.VATAmount), MidpointRounding.AwayFromZero);
                                                    }
                                                    else
                                                    {
                                                        AdjInv.Amount = Math.Round(AdjInv.Products.Sum(n => n.Amount), 2, MidpointRounding.AwayFromZero);
                                                        if (AdjInv.CurrencyUnit == "USD" || AdjInv.CurrencyUnit == "EUR" || AdjInv.CurrencyUnit == "GBP" || AdjInv.CurrencyUnit == "SGD")
                                                            AdjInv.AmountInWords = NumberToWord.GetAmountInWords(AdjInv.Amount, sABill.CurrencyID);
                                                        else
                                                            AdjInv.AmountInWords = Utils.So_chu(AdjInv.Amount, sABill.CurrencyID);
                                                        AdjInv.Total = Math.Round(AdjInv.Products.Sum(n => n.Total), 2, MidpointRounding.AwayFromZero);
                                                        AdjInv.VATAmount = Math.Round(AdjInv.Products.Sum(n => n.VATAmount), 2, MidpointRounding.AwayFromZero);
                                                    }
                                                    #endregion
                                                    #region Code cũ
                                                    //AdjInv.VATRate = AdjInv.Products.Count > 0 ? Convert.ToInt32(AdjInv.Products.First().VATRate).ToString() : "-1";
                                                    //AdjInv.Amount = sABill.TotalPaymentAmountOriginal;
                                                    //AdjInv.AmountInWords = Utils.So_chu(Math.Round(sABill.TotalPaymentAmountOriginal, 2), sABill.CurrencyID);
                                                    //AdjInv.Total = sABill.TotalAmountOriginal - sABill.TotalDiscountAmountOriginal;
                                                    //AdjInv.VATAmount = AdjInv.Products.Sum(n => n.VATAmount);
                                                    #endregion
                                                    var xmldata = Utils.Serialize<AdjustInv>(AdjInv);
                                                    XmlDocument xml = new XmlDocument();
                                                    xml.LoadXml(xmldata);
                                                    //XmlNode xmldt = xml.SelectSingleNode("/EInvoices/Invoices");
                                                    //request.XmlData = xmldt.OuterXml;
                                                    request.XmlData = xml.OuterXml;

                                                    RestApiService client = new RestApiService(Utils.GetPathAccess(), Utils.GetUserName(), Core.ITripleDes.TripleDesDefaultDecryption(Utils.GetPassWords()));
                                                    Response response = client.Post(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "TaoHoaDonChuaKiSo_DieuChinh").ApiPath, request);
                                                    if (response != null && response.Status != 2)
                                                    {
                                                        MSG.Error(response.Message);
                                                    }
                                                }
                                                // Hóa đơn mới tạo lập thay thế StatusInvoice = 0
                                                else if (@select.GetProperty<T, int>("StatusInvoice") == 7)
                                                {
                                                    var request = new Request();
                                                    ReplaceInv replaceInv = new ReplaceInv();


                                                    request.Pattern = @select.GetProperty("InvoiceTemplate").ToString();
                                                    request.Serial = @select.GetProperty("InvoiceSeries").ToString();

                                                    SABill sABill = @select as SABill;
                                                    int check = 0;
                                                    if (sABill == null)
                                                    {
                                                    }
                                                    else
                                                    {
                                                        request.Ikey = sABill.IDReplaceInv.ToString();
                                                        AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.ID == sABill.AccountingObjectID);

                                                        replaceInv.Key = sABill.ID.ToString();
                                                        replaceInv.CusCode = accountingObject.AccountingObjectCode ?? "";
                                                        replaceInv.Buyer = sABill.ContactName ?? "";
                                                        replaceInv.CusName = sABill.AccountingObjectName ?? "";
                                                        replaceInv.Email = accountingObject.Email ?? "";
                                                        replaceInv.CusAddress = sABill.AccountingObjectAddress ?? "";
                                                        replaceInv.CusBankName = sABill.AccountingObjectBankName ?? "";
                                                        replaceInv.CusBankNo = sABill.AccountingObjectBankAccount ?? "";
                                                        replaceInv.CusPhone = accountingObject.ContactMobile ?? "";
                                                        replaceInv.CusTaxCode = sABill.CompanyTaxCode;
                                                        replaceInv.PaymentMethod = sABill.PaymentMethod;
                                                        if (sABill.InvoiceDate != null)
                                                        {
                                                            DateTime dt = sABill.InvoiceDate ?? DateTime.Now;
                                                            replaceInv.ArisingDate = dt.Date.ToString("dd/MM/yyyy");
                                                        }
                                                        else
                                                        {
                                                            replaceInv.ArisingDate = "";
                                                        }
                                                        replaceInv.ExchangeRate = sABill.ExchangeRate;
                                                        replaceInv.CurrencyUnit = sABill.CurrencyID;
                                                        //replaceInv.Extra = Eitem.Extra;
                                                        foreach (SABillDetail itemD in sABill.SABillDetails.OrderBy(n => n.OrderPriority).ToList())
                                                        {
                                                            MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.ID == itemD.MaterialGoodsID);
                                                            Product prd = new Product();
                                                            prd.Code = materialGoods.MaterialGoodsCode ?? "";
                                                            prd.ProdName = itemD.Description ?? "";
                                                            if (itemD.IsPromotion != null)
                                                            {
                                                                if (itemD.IsPromotion ?? false)
                                                                {
                                                                    prd.ProdName = itemD.Description ?? "" + " (Hàng khuyến mại)";
                                                                }
                                                            }
                                                            prd.ProdUnit = itemD.Unit;
                                                            prd.ProdQuantity = itemD.Quantity;
                                                            if (replaceInv.CurrencyUnit == "VND")
                                                            {
                                                                prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaQuyDoi), MidpointRounding.AwayFromZero);
                                                            }
                                                            else
                                                            {
                                                                prd.ProdPrice = Math.Round(itemD.UnitPriceOriginal, Utils.GetFormatDecimalPlaces(ConstDatabase.Format_DonGiaNgoaiTe), MidpointRounding.AwayFromZero);
                                                            }

                                                            prd.VATRate = itemD.VATRate != -2 ? Convert.ToInt32(itemD.VATRate) : -1;
                                                            if (replaceInv.CurrencyUnit == "VND" || replaceInv.CurrencyUnit == "JPY")
                                                            {
                                                                prd.Total = Math.Round(itemD.AmountOriginal, MidpointRounding.AwayFromZero);
                                                            }
                                                            else
                                                            {
                                                                prd.Total = itemD.AmountOriginal;
                                                            }
                                                            if (prd.VATRate == -1)
                                                            {
                                                                prd.VATAmount = 0;
                                                            }
                                                            else
                                                            {
                                                                if (replaceInv.CurrencyUnit == "VND" || replaceInv.CurrencyUnit == "JPY")
                                                                {
                                                                    prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    prd.VATAmount = Math.Round((prd.Total * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                }
                                                            }
                                                            prd.Amount = prd.Total + prd.VATAmount;
                                                            if (!string.IsNullOrEmpty(itemD.LotNo) || itemD.ExpiryDate != null)
                                                            {
                                                                prd.Extra = @"{""Lot"":""";
                                                                prd.Extra += itemD.LotNo != null ? itemD.LotNo : "";
                                                                if (itemD.ExpiryDate != null)
                                                                {
                                                                    DateTime date = itemD.ExpiryDate ?? DateTime.Now;
                                                                    prd.Extra += @""",""ExpireDate"":""" + date.ToString("dd/MM/yyyy") + @"""}";
                                                                }
                                                                else
                                                                {
                                                                    prd.Extra += @""",""ExpireDate"":""" + @"""}";
                                                                }

                                                            }
                                                            else
                                                            {
                                                                prd.Extra = "";
                                                            }


                                                            Product prdCK = new Product();
                                                            prdCK.ProdName = "Chiết khấu";
                                                            prdCK.VATRate = itemD.VATRate != -2 ? Convert.ToInt32(itemD.VATRate) : -1;
                                                            //prdCK.Total = -itemD.DiscountAmount;
                                                            if (itemD.DiscountRate != null)
                                                            {
                                                                if (replaceInv.CurrencyUnit == "VND" || replaceInv.CurrencyUnit == "JPY")
                                                                {
                                                                    prdCK.Total = -Math.Round((prd.Total * (decimal)itemD.DiscountRate) / 100, MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    prdCK.Total = -Math.Round((prd.Total * (decimal)itemD.DiscountRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                }
                                                            }
                                                            if (prdCK.VATRate == -1)
                                                            {
                                                                prdCK.VATAmount = 0;
                                                            }
                                                            else
                                                            {
                                                                if (replaceInv.CurrencyUnit == "VND" || replaceInv.CurrencyUnit == "JPY")
                                                                {
                                                                    prdCK.VATAmount = Math.Round((prdCK.Total * (decimal)prdCK.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                }
                                                                else
                                                                {
                                                                    prdCK.VATAmount = Math.Round((prdCK.Total * (decimal)prdCK.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                }
                                                            }
                                                            prdCK.Amount = prdCK.Total + prdCK.VATAmount;

                                                            decimal sub = 0M;
                                                            if (itemD.DiscountAmountOriginal != 0M)
                                                            {
                                                                var VATAmount = 0M;
                                                                if (prd.VATRate == -1)
                                                                {
                                                                    prd.VATAmount = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (replaceInv.CurrencyUnit == "VND" || replaceInv.CurrencyUnit == "JPY")
                                                                    {
                                                                        VATAmount = Math.Round(((prd.Total + prdCK.Total) * (decimal)prd.VATRate) / 100, MidpointRounding.AwayFromZero);
                                                                    }
                                                                    else
                                                                    {
                                                                        VATAmount = Math.Round(((prd.Total + prdCK.Total) * (decimal)prd.VATRate) / 100, 2, MidpointRounding.AwayFromZero);
                                                                    }
                                                                }
                                                                if (replaceInv.CurrencyUnit == "VND" || replaceInv.CurrencyUnit == "JPY")
                                                                {
                                                                    sub = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - VATAmount;
                                                                }
                                                                else
                                                                {
                                                                    sub = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - VATAmount;
                                                                }

                                                            }
                                                            else
                                                            {
                                                                if (replaceInv.CurrencyUnit == "VND" || replaceInv.CurrencyUnit == "JPY")
                                                                {
                                                                    sub = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                }
                                                                else
                                                                {
                                                                    sub = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                                }
                                                            }

                                                            if (Math.Abs(sub) > 0)
                                                            {
                                                                prd.VATAmount = prd.VATAmount + sub;

                                                            }
                                                            if (replaceInv.CurrencyUnit == "VND" || replaceInv.CurrencyUnit == "JPY")
                                                            {
                                                                prdCK.VATAmount = Math.Round(itemD.VATAmountOriginal, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                            }
                                                            else
                                                            {
                                                                prdCK.VATAmount = Math.Round(itemD.VATAmountOriginal, 2, MidpointRounding.AwayFromZero) - prd.VATAmount;
                                                            }
                                                            prdCK.Amount = prdCK.Total + prdCK.VATAmount;
                                                            prd.Amount = prd.Total + prd.VATAmount;

                                                            replaceInv.Products.Add(prd);
                                                            if (prdCK.Total != 0)
                                                            {
                                                                replaceInv.Products.Add(prdCK);
                                                            }
                                                        }
                                                    }
                                                    #region code mới
                                                    replaceInv.VATRate = replaceInv.Products.Count > 0 ? Convert.ToInt32(replaceInv.Products.First().VATRate).ToString() : "-1";

                                                    if (replaceInv.CurrencyUnit == "VND" || replaceInv.CurrencyUnit == "JPY")
                                                    {
                                                        replaceInv.Amount = Math.Round(replaceInv.Products.Sum(n => n.Amount), MidpointRounding.AwayFromZero);
                                                        replaceInv.AmountInWords = NumberToWord.GetAmountInWords(replaceInv.Amount, sABill.CurrencyID);
                                                        replaceInv.Total = Math.Round(replaceInv.Products.Sum(n => n.Total), MidpointRounding.AwayFromZero);
                                                        replaceInv.VATAmount = Math.Round(replaceInv.Products.Sum(n => n.VATAmount), MidpointRounding.AwayFromZero);
                                                    }
                                                    else
                                                    {
                                                        replaceInv.Amount = Math.Round(replaceInv.Products.Sum(n => n.Amount), 2, MidpointRounding.AwayFromZero);
                                                        if (replaceInv.CurrencyUnit == "USD" || replaceInv.CurrencyUnit == "EUR" || replaceInv.CurrencyUnit == "GBP" || replaceInv.CurrencyUnit == "SGD")
                                                            replaceInv.AmountInWords = NumberToWord.GetAmountInWords(Math.Round(replaceInv.Amount, 2, MidpointRounding.AwayFromZero), sABill.CurrencyID);
                                                        else
                                                            replaceInv.AmountInWords = Utils.So_chu(Math.Round(replaceInv.Amount, 2, MidpointRounding.AwayFromZero), sABill.CurrencyID);
                                                        replaceInv.Total = Math.Round(replaceInv.Products.Sum(n => n.Total), 2, MidpointRounding.AwayFromZero);
                                                        replaceInv.VATAmount = Math.Round(replaceInv.Products.Sum(n => n.VATAmount), 2, MidpointRounding.AwayFromZero);
                                                    }
                                                    #endregion
                                                    #region Code cũ
                                                    //replaceInv.VATRate = replaceInv.Products.Count > 0 ? Convert.ToInt32(replaceInv.Products.First().VATRate).ToString() : "-1";
                                                    //replaceInv.Amount = sABill.TotalPaymentAmountOriginal;
                                                    //replaceInv.AmountInWords = Utils.So_chu(Math.Round(sABill.TotalPaymentAmountOriginal, 2), sABill.CurrencyID);
                                                    //replaceInv.Total = sABill.TotalAmountOriginal - sABill.TotalDiscountAmountOriginal;
                                                    //replaceInv.VATAmount = replaceInv.Products.Sum(n => n.VATAmount);
                                                    #endregion
                                                    var xmldata = Utils.Serialize<ReplaceInv>(replaceInv);
                                                    XmlDocument xml = new XmlDocument();
                                                    xml.LoadXml(xmldata);
                                                    //XmlNode xmldt = xml.SelectSingleNode("/EInvoices/Invoices");
                                                    //request.XmlData = xmldt.OuterXml;
                                                    request.XmlData = xml.OuterXml;
                                                    RestApiService client = new RestApiService(Utils.GetPathAccess(), Utils.GetUserName(), Core.ITripleDes.TripleDesDefaultDecryption(Utils.GetPassWords()));
                                                    Response response = client.Post(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "TaoHoaDonChuaKiSo_ThayThe").ApiPath, request);
                                                    if (response != null && response.Status != 2)
                                                    {
                                                        MSG.Error(response.Message);
                                                    }
                                                }
                                            }
                                            #endregion
                                        }
                                    }
                            }
                    }


                }
                else if (statusForm == ConstFrm.optStatusForm.Delete)
                {
                    //[check TH bán hàng không kèm phiếu xuất kho]
                    if (@select is SAInvoice)
                    {
                        if (@select.HasProperty("ID") && @select.HasProperty("OutwardNo"))
                        {
                            var id = @select.GetProperty<T, Guid>("ID");
                            if (string.IsNullOrEmpty(@select.GetProperty<T, string>("OutwardNo")))
                            {
                                foreach (var del in IRSInwardOutwardService.Query.Where(x => x.SAInvoiceID == id))
                                {
                                    del.SAInvoiceID = null;
                                    IRSInwardOutwardService.Update(del);
                                }
                            }
                        }
                    }
                    //[check TH kho có kèm phiếu bán hàng không kèm phiếu xuất kho]
                    if (@select is RSInwardOutward)
                    {
                        if (@select.HasProperty("ID") && @select.HasProperty("IsOutwardSAInvoice"))
                        {
                            var id = @select.GetProperty<T, Guid>("ID");
                            if (@select.GetProperty<T, bool?>("IsOutwardSAInvoice") == true)
                            {
                                foreach (var del in ISAInvoiceService.Query.Where(x => x.OutwardRefID == id))
                                {
                                    del.OutwardRefID = null;
                                    ISAInvoiceService.Update(del);
                                }
                            }
                        }
                    }
                    if (@select is SABill)
                    {
                        if (@select.HasProperty("ID") && @select.HasProperty("SAIDs"))
                        {
                            var id = @select.GetProperty<T, Guid>("ID");
                            var guids = ISAInvoiceService.Query.Where(x => x.BillRefID == id);
                            if (guids != null)
                            {
                                foreach (var temp in guids)
                                {
                                    temp.BillRefID = null;
                                    temp.IsAttachListBill = false;
                                    temp.InvoiceForm = null;
                                    temp.InvoiceTypeID = null;
                                    temp.InvoiceTemplate = null;
                                    temp.InvoiceNo = null;
                                    temp.InvoiceSeries = null;
                                    temp.InvoiceDate = null;
                                    ISAInvoiceService.Update(temp);
                                }
                            }
                            //if(Utils.ListSAInvoice.Where(x => x.SABillID == id).ToList().Count > 0)
                            //{
                            //    foreach(var x in Utils.ListSAInvoice.Where(x => x.SABillID == id).ToList())
                            //    {
                            //        x.SABillID = null;
                            //        ISAInvoiceService.Update(x);
                            //    }
                            //}
                        }
                    }
                }

                //có key và key có value
                if (!@select.HasProperty("ID")) return true;
                Guid guid = @select.GetProperty<T, Guid>("ID");
                if (!@select.HasProperty("TypeID")) return true;
                int typeid = @select.GetProperty<T, int>("TypeID");

                //[Huy Anh] Lọc ra TH Lắp ráp/Tháo dỡ

                if (typeid.Equals(900) || typeid.Equals(901))
                {

                }
                #endregion
                else
                {
                    #region Nhập kho - xuất kho
                    string key = "OutwardNo";
                    bool IsOutwardNo = @select.HasProperty(key) && !string.IsNullOrEmpty(@select.GetProperty<T, string>(key));
                    key = "InwardNo,IWNo";
                    bool IsInwardNo = key.Split(',').ToList().Any(k => @select.HasProperty(k) && !string.IsNullOrEmpty(@select.GetProperty<T, string>(k)));
                    if (IsOutwardNo || IsInwardNo)
                    {
                        //cặp TypeID(A,B) trong đó A là chứng từ chính, B là chứng từ con
                        Dictionary<int, int> keyPair = new Dictionary<int, int>
                                                   {
                                                       { 320, 412 }, //Bán hàng - Xuất kho từ bán hàng
                                                       { 321, 412 }, //Bán hàng - Xuất kho từ bán hàng
                                                       { 322, 412 }, //Bán hàng - Xuất kho từ bán hàng
                                                       { 323, 412 }, //Bán hàng - Xuất kho từ bán hàng
                                                       { 324, 412 }, //Bán hàng - Xuất kho từ bán hàng
                                                       { 325, 412 }, //Bán hàng - Xuất kho từ bán hàng
                                                       { 220, 413 }, //Hàng mua trả lại - Xuất kho từ hàng mua trả lại
                                                       { 210, 402 }, //Mua hàng - Nhập kho từ mua hàng
                                                       { 260, 402 }, //Mua hàng - Nhập kho từ mua hàng
                                                       { 261, 402 }, //Mua hàng - Nhập kho từ mua hàng
                                                       { 262, 402 }, //Mua hàng - Nhập kho từ mua hàng
                                                       { 263, 402 }, //Mua hàng - Nhập kho từ mua hàng
                                                       { 264, 402 }, //Mua hàng - Nhập kho từ mua hàng
                                                       { 330, 403 }  //Hàng bán trả lại - Nhập kho từ hàng bán trả lại
                                                   };

                        //Tạo một phiếu xuất kho, nhập kho
                        //danh sách các properties của NHẬP KHO, XUẤT KHO
                        string strLstKeys = "ID,BranchID"
                                                  + ",Date,PostedDate,No"
                                                  + ",AccountingObjectID,AccountingObjectName,AccountingObjectAddress,ContactName,Reason,OriginalNo"
                                                  + ",CurrencyID,ExchangeRate,EmployeeID,TransportMethodID,TotalAmount,TotalAmountOriginal,Recorded,Exported,RefDateTime"
                                                  + (@select.HasProperty("IsImportPurchase") ? ",IsImportPurchase" : "");
                        //danh sách các properties của _SELECT mà map tương ứng
                        string strLstOtherKeys = "ID@BranchID"
                                                       + (IsOutwardNo ? "@SDate,ODate,OWDate" : "@IWDate,Date")
                                                       + (IsOutwardNo ? "@SPostedDate,OPostedDate,OWPostedDate" : "@IWPostedDate,PostedDate")
                                                       + (IsOutwardNo ? "@OWNo,OutwardNo" : "@IWNo,InwardNo")
                                                       + (IsOutwardNo ? "@AccountingObjectID,OWAccountingObjectID" : "@AccountingObjectID,IWAccountingObjectID")
                                                       + (IsOutwardNo ? "@AccountingObjectName,OWAccountingObjectName" : "@AccountingObjectName,IWAccountingObjectName")
                                                       + "@AccountingObjectAddress"
                                                       + (IsOutwardNo ? "@OContactName,SContactName" : "@IWContactName,ContactName")
                                                       + (IsOutwardNo ? "@OReason,SReason" : "@IWReason,Reason")
                                                       + (IsOutwardNo ? "@OriginalNo,NumberAttach" : "@IWNumberAttach,NumberAttach")
                                                       + "@CurrencyID@ExchangeRate@EmployeeID@TransportMethodID"
                                                       + (IsOutwardNo ? (@select.GetProperty<T, int>("TypeID") == 220 ? "@TotalAmount@TotalAmountOriginal" : "@TotalCapitalAmount@TotalCapitalAmountOriginal") : (@select.GetProperty<T, int>("TypeID") == 330 ? "@TotalOWAmount@TotalOWAmountOriginal" : (@select.HasProperty("TotalInwardAmount") ? "@TotalInwardAmount@TotalInwardAmountOriginal" : "@TotalPaymentAmount@TotalPaymentAmountOriginal")))
                                                       + "@Recorded@Exported@RefDateTime"
                                                       + (@select.HasProperty("IsImportPurchase") ? "@IsImportPurchase" : "");
                        GenVocherOtherStand<T, RSInwardOutward>(@select, statusForm, keyPair, strLstKeys, strLstOtherKeys, remove);
                    }
                    #endregion

                    #region Tạo chứng từ móc kép
                    #region DS type chứng từ
                    List<int> lstVocherPc = new List<int> { 260, 119, 116, 902 };//Phiều chi
                    List<int> lstVocherPt = new List<int> { 321, 324 };//Phiều thu
                    List<int> lstVocherUnc = new List<int> { 261, 129, 126, 903 };//Ủy nhiệm chi
                    List<int> lstVocherSck = new List<int> { 262, 132, 133, 904 };//Séc chuyển khoản
                    List<int> lstVocherStm = new List<int> { 264, 142, 143, 905 };//Séc tiền mặt
                    List<int> lstVocherTtd = new List<int> { 263, 172, 173, 906 };//Thẻ tín dụng
                    List<int> lstVocherNttk = new List<int> { 322, 325 };//Nộp tiền vào TK
                    #endregion

                    #region Xóa chứng từ móc kép cũ nếu sửa loại chứng từ
                    if (statusForm == ConstFrm.optStatusForm.Edit)
                    {
                        if (backSelect.HasProperty("TypeID"))
                        {
                            var oldTypeId = backSelect.GetProperty<T, int>("TypeID");
                            if (oldTypeId != typeid)
                                try
                                {
                                    #region Phiếu thu
                                    if (lstVocherPt.Contains(oldTypeId))
                                        Utils.IMCReceiptService.Delete(guid);
                                    #endregion

                                    #region Phiếu chi
                                    if (lstVocherPc.Contains(oldTypeId))
                                        Utils.IMCPaymentService.Delete(guid);
                                    #endregion

                                    #region Ủy nhiệm chi/Séc chuyển khoản/Séc tiền mặt
                                    if (lstVocherUnc.Contains(oldTypeId) || lstVocherSck.Contains(oldTypeId) || lstVocherStm.Contains(oldTypeId))
                                        Utils.IMBTellerPaperService.Delete(guid);
                                    #endregion

                                    #region Thẻ tín dụng
                                    if (lstVocherTtd.Contains(oldTypeId))
                                        Utils.IMBCreditCardService.Delete(guid);
                                    #endregion

                                    #region Nộp tiền TK
                                    if (lstVocherNttk.Contains(oldTypeId))
                                        Utils.IMBDepositService.Delete(guid);
                                    #endregion
                                }
                                catch (Exception)
                                {
                                    return false;
                                }
                        }
                    }
                    #endregion

                    #region Phiếu thu
                    if (lstVocherPt.Contains(typeid))
                    {
                        Dictionary<int, int> keyPair = new Dictionary<int, int>
                                                   {
                                                       { 321, 102 }, //(Hóa đơn bán hàng thu tiền ngay - Tiền mặt) - Phiếu thu từ bán hàng
                                                       { 324, 103 }, //(Hóa đơn bán hàng đại lý bán đúng giá, nhận ủy thác  XNK - Tiền mặt) - Phiếu thu từ bán hàng đại lý bán đúng giá, nhận ủy thác XNK
                                                   };
                        //danh sách các properties của chứng từ móc
                        const string strLstKeys = "BranchID,Date,PostedDate,No,AccountingObjectID,AccountingObjectName"
                                                  + ",AccountingObjectAddress,Payers,Reason,NumberAttach,CurrencyID,ExchangeRate,SAQuoteID,SAOrderID,PaymentClauseID,EmployeeID,TransportMethodID"
                                                  + ",TotalAmount,TotalAmountOriginal,TotalVATAmount,TotalVATAmountOriginal,Recorded,Exported,InvoiceTypeID,InvoiceDate,InvoiceTemplate,InvoiceNo,InvoiceSeries,TotalAll,TotalAllOriginal";
                        //danh sách các properties của _SELECT mà map tương ứng
                        const string strLstOtherKeys = "BranchID"
                                                       + "@MDate"
                                                       + "@MPostedDate"
                                                       + "@MNo"
                                                       + "@AccountingObjectID"
                                                       + "@AccountingObjectName"
                                                       + "@AccountingObjectAddress"
                                                       + "@MContactName"
                                                       + "@MReasonPay"
                                                       + "@NumberAttach@CurrencyID@ExchangeRate@SAQuoteID@SAOrderID@PaymentClauseID@EmployeeID@TransportMethodID@TotalPaymentAmountStand@TotalPaymentAmountOriginalStand@TotalVATAmount@TotalVATAmountOriginal@Recorded@Exported@InvoiceTypeID@InvoiceDate@InvoiceTemplate@InvoiceNo@InvoiceSeries@TotalPaymentAmount@TotalPaymentAmountOriginal";
                        GenVocherOtherStand<T, MCReceipt>(@select, statusForm, keyPair, strLstKeys, strLstOtherKeys, remove);
                    }
                    #endregion

                    #region Phiếu chi
                    if (lstVocherPc.Contains(typeid))
                    {
                        Dictionary<int, Type> dicCheck = new Dictionary<int, Type>
                                                                    {
                                                                        {119, typeof (FAIncrement)},//Nếu là Type của chứng từ móc Ghi tăng TSCĐ nhưng Form hiện tại không phải là Form TSCĐ thì ko xử lý
                                                                        {116, typeof (PPService)},//Nếu là Type của chứng từ móc Mua DV nhưng Form hiện tại không phải là Form Mua DV thì ko xử lý
                                                                        {902, typeof (TIIncrement)}
                                                                    };
                        if (dicCheck.Any(x => x.Key == typeid) && dicCheck.FirstOrDefault(x => x.Key == typeid).Value != typeof(T)) return true;
                        Dictionary<int, int> keyPair = new Dictionary<int, int>
                                                   {
                                                       { 260, 117 }, //Mua hàng - phiếu chi từ mua hàng
                                                       { 119, 119 }, //Mua TSCĐ - Phiếu chi mua TSCĐ
                                                       { 116, 116 },//Mua DV - phiếu chi mua dv
                                                       { 902, 902 },//Mua CCDC - Phiếu chi mua CCDC
                                                   };
                        //danh sách các properties của chứng từ móc
                        const string strLstKeys = "ID,BranchID,Date,PostedDate,No,AccountingObjectID,AccountingObjectName"
                                                  + ",AccountingObjectAddress,Receiver,Reason,NumberAttach,CurrencyID,ExchangeRate,IsImportPurchase,PaymentClauseID,EmployeeID,TransportMethodID"
                                                  + ",TotalAmount,TotalAmountOriginal,TotalVATAmount,TotalVATAmountOriginal,Recorded,Exported,TotalAll,TotalAllOriginal";
                        //danh sách các properties của _SELECT mà map tương ứng
                        const string strLstOtherKeys = "ID@BranchID"
                                                       + "@MDate,Date"
                                                       + "@MPostedDate,PostedDate"
                                                       + "@MNo,No"
                                                       + "@AccountingObjectID"
                                                       + "@AccountingObjectName"
                                                       + "@AccountingObjectAddress"
                                                       + "@MContactName,ContactName,Receiver"
                                                       + "@MReasonPay,Reason"
                                                       + "@NumberAttach@CurrencyID@ExchangeRate@IsImportPurchase@PaymentClauseID@EmployeeID@TransportMethodID@TotalAmount@TotalAmountOriginal@TotalVATAmount@TotalVATAmountOriginal@Recorded@Exported@TotalAll@TotalAllOriginal";
                        GenVocherOtherStand<T, MCPayment>(@select, statusForm, keyPair, strLstKeys, strLstOtherKeys, remove);
                    }
                    #endregion

                    #region Ủy nhiệm chi
                    if (lstVocherUnc.Contains(typeid))
                    {
                        Dictionary<int, Type> dicCheck = new Dictionary<int, Type>
                                                                    {
                                                                        {129, typeof (FAIncrement)},//Nếu là Type của chứng từ móc Ghi tăng TSCĐ nhưng Form hiện tại không phải là Form TSCĐ thì ko xử lý
                                                                        {126, typeof (PPService)},//Nếu là Type của chứng từ móc Mua DV nhưng Form hiện tại không phải là Form Mua DV thì ko xử lý
                                                                        {903, typeof (TIIncrement)},
                                                                    };
                        if (dicCheck.Any(x => x.Key == typeid) && dicCheck.FirstOrDefault(x => x.Key == typeid).Value != typeof(T)) return true;
                        Dictionary<int, int> keyPair = new Dictionary<int, int>
                                                   {
                                                       { 261, 127 }, //Mua hàng - ủy nhiệm chi từ mua hàng
                                                       { 129, 129 }, //Mua TSCĐ - Mua TSCĐ bằng Ủy nhiệm chi
                                                       { 126, 126 }, //Mua DV - Mua DV bằng Ủy nhiệm chi
                                                       { 903, 903 },//Mua CCDC - Phiếu chi mua CCDC
                                                   };
                        //danh sách các properties của chứng từ móc
                        const string strLstKeys = "BranchID,Date,PostedDate,No,AccountingObjectID,AccountingObjectName,AccountingObjectAddress,Reason"
                                                  + ",BankAccountDetailID,BankName,AccountingObjectBankAccount,AccountingObjectBankName"
                                                  + ",Receiver"
                                                  + ",IdentificationNo,IssueDate,IssueBy"
                                                  + ",CurrencyID,ExchangeRate,IsImportPurchase,PaymentClauseID,EmployeeID,TransportMethodID"
                                                  + ",TotalAmount,TotalAmountOriginal,TotalVATAmount,TotalVATAmountOriginal,Recorded,Exported,TotalAll,TotalAllOriginal";
                        //danh sách các properties của _SELECT mà map tương ứng
                        const string strLstOtherKeys = "BranchID"
                                                       + "@MDate,Date"
                                                       + "@MPostedDate,PostedDate"
                                                       + "@MNo,No"
                                                       + "@AccountingObjectID@AccountingObjectName@AccountingObjectAddress@MReasonPay,Reason"
                                                       + "@BankAccountDetailID@BankName@MAccountingObjectBankAccount,AccountingObjectBankAccount,AccountingObjectBankAccountDetailID"
                                                       + "@MAccountingObjectBankName,AccountingObjectBankName"
                                                       + "@MContactName,Receiver,ContactName"
                                                       + "@IdentificationNo@IssueDate@IssueBy"
                                                       + "@CurrencyID@ExchangeRate@IsImportPurchase@PaymentClauseID@EmployeeID@TransportMethodID"
                                                       + "@TotalAmount@TotalAmountOriginal@TotalVATAmount@TotalVATAmountOriginal@Recorded@Exported@TotalAll@TotalAllOriginal";
                        GenVocherOtherStand<T, MBTellerPaper>(@select, statusForm, keyPair, strLstKeys, strLstOtherKeys, remove);
                    }
                    #endregion

                    #region Séc chuyển khoản
                    if (lstVocherSck.Contains(typeid))
                    {
                        Dictionary<int, Type> dicCheck = new Dictionary<int, Type>
                                                                    {
                                                                        {132, typeof (FAIncrement)},//Nếu là Type của chứng từ móc Ghi tăng TSCĐ nhưng Form hiện tại không phải là Form TSCĐ thì ko xử lý
                                                                        {133, typeof (PPService)},//Nếu là Type của chứng từ móc Mua DV nhưng Form hiện tại không phải là Form Mua DV thì ko xử lý
                                                                        {904, typeof (TIIncrement)},
                                                                    };
                        if (dicCheck.Any(x => x.Key == typeid) && dicCheck.FirstOrDefault(x => x.Key == typeid).Value != typeof(T)) return true;
                        Dictionary<int, int> keyPair = new Dictionary<int, int>
                                                   {
                                                       { 262, 131 }, //Mua hàng - séc chuyển khoản từ mua hàng
                                                       { 132, 132 }, //Mua TSCĐ - Mua TSCĐ bằng Séc chuyển khoản
                                                       { 133, 133 }, //Mua DV - Mua dV bằng Séc chuyển khoản
                                                       { 904, 904 },//Mua CCDC - Phiếu chi mua CCDC
                                                   };
                        //danh sách các properties của chứng từ móc
                        const string strLstKeys = "BranchID,Date,PostedDate,No,AccountingObjectID,AccountingObjectName,AccountingObjectAddress,Reason"
                                                  + ",BankAccountDetailID,BankName,AccountingObjectBankAccount,AccountingObjectBankName"
                                                  + ",Receiver"
                                                  + ",IdentificationNo,IssueDate,IssueBy"
                                                  + ",CurrencyID,ExchangeRate,IsImportPurchase,PaymentClauseID,EmployeeID,TransportMethodID"
                                                  + ",TotalAmount,TotalAmountOriginal,TotalVATAmount,TotalVATAmountOriginal,Recorded,Exported,TotalAll,TotalAllOriginal";
                        //danh sách các properties của _SELECT mà map tương ứng
                        const string strLstOtherKeys = "BranchID@MDate,Date@MPostedDate,PostedDate@MNo,No@AccountingObjectID@AccountingObjectName@AccountingObjectAddress@MReasonPay,Reason"
                                                       + "@BankAccountDetailID@BankName@MAccountingObjectBankAccount,AccountingObjectBankAccount,MAccountingObjectBankAccountDetailID,AccountingObjectBankAccountDetailID@MAccountingObjectBankName,AccountingObjectBankName"
                                                       + "@MContactName,Receiver"
                                                       + "@IdentificationNo@IssueDate@IssueBy"
                                                       + "@CurrencyID@ExchangeRate@IsImportPurchase@PaymentClauseID@EmployeeID@TransportMethodID"
                                                       + "@TotalAmount@TotalAmountOriginal@TotalVATAmount@TotalVATAmountOriginal@Recorded@Exported@TotalAll@TotalAllOriginal";
                        GenVocherOtherStand<T, MBTellerPaper>(@select, statusForm, keyPair, strLstKeys, strLstOtherKeys, remove);
                    }
                    #endregion

                    #region Séc tiền mặt
                    if (lstVocherStm.Contains(typeid))
                    {
                        Dictionary<int, Type> dicCheck = new Dictionary<int, Type>
                                                                    {
                                                                        {142, typeof (FAIncrement)},//Nếu là Type của chứng từ móc Ghi tăng TSCĐ nhưng Form hiện tại không phải là Form TSCĐ thì ko xử lý
                                                                        {143, typeof (PPService)},//Nếu là Type của chứng từ móc Mua DV nhưng Form hiện tại không phải là Form Mua DV thì ko xử lý
                                                                        {905, typeof (TIIncrement)},
                                                                    };
                        if (dicCheck.Any(x => x.Key == typeid) && dicCheck.FirstOrDefault(x => x.Key == typeid).Value != typeof(T)) return true;
                        Dictionary<int, int> keyPair = new Dictionary<int, int>
                                                   {
                                                       { 264, 141 }, //Mua hàng - séc tiền mặt từ mua hàng
                                                       { 142, 142 }, //Mua TSCĐ - Mua TSCĐ bằng Séc TM
                                                       { 143, 143 }, //Mua DV - Mua DV bằng Séc TM
                                                       { 905, 905 },//Mua CCDC - Phiếu chi mua CCDC
                                                   };
                        //danh sách các properties của chứng từ móc
                        const string strLstKeys = "BranchID,Date,PostedDate,No,AccountingObjectID,AccountingObjectName,AccountingObjectAddress,Reason"
                                                  + ",BankAccountDetailID,BankName,AccountingObjectBankAccount,AccountingObjectBankName"
                                                  + ",Receiver"
                                                  + ",IdentificationNo,IssueDate,IssueBy"
                                                  + ",CurrencyID,ExchangeRate,IsImportPurchase,PaymentClauseID,EmployeeID,TransportMethodID"
                                                  + ",TotalAmount,TotalAmountOriginal,TotalVATAmount,TotalVATAmountOriginal,Recorded,Exported,TotalAll,TotalAllOriginal";
                        //danh sách các properties của _SELECT mà map tương ứng
                        const string strLstOtherKeys = "BranchID@MDate,Date@MPostedDate,PostedDate@MNo,No@AccountingObjectID@AccountingObjectName@AccountingObjectAddress@MReasonPay,Reason"
                                                       + "@BankAccountDetailID@BankName@MAccountingObjectBankAccount,AccountingObjectBankAccount,MAccountingObjectBankAccountDetailID,AccountingObjectBankAccountDetailID@MAccountingObjectBankName,AccountingObjectBankName"
                                                       + "@MContactName,Receiver,ContactName"
                                                       + "@IdentificationNo@IssueDate@IssueBy"
                                                       + "@CurrencyID@ExchangeRate@IsImportPurchase@PaymentClauseID@EmployeeID@TransportMethodID"
                                                       + "@TotalAmount@TotalAmountOriginal@TotalVATAmount@TotalVATAmountOriginal@Recorded@Exported@TotalAll@TotalAllOriginal";
                        GenVocherOtherStand<T, MBTellerPaper>(@select, statusForm, keyPair, strLstKeys, strLstOtherKeys, remove);
                    }
                    #endregion

                    #region Thẻ tín dụng
                    if (lstVocherTtd.Contains(typeid))
                    {
                        Dictionary<int, Type> dicCheck = new Dictionary<int, Type>
                                                                    {
                                                                        {172, typeof (FAIncrement)},//Nếu là Type của chứng từ móc Ghi tăng TSCĐ nhưng Form hiện tại không phải là Form TSCĐ thì ko xử lý
                                                                        {173, typeof (PPService)},//Nếu là Type của chứng từ móc Mua DV nhưng Form hiện tại không phải là Form Mua DV thì ko xử lý
                                                                        {906, typeof (TIIncrement)},
                                                                    };
                        if (dicCheck.Any(x => x.Key == typeid) && dicCheck.FirstOrDefault(x => x.Key == typeid).Value != typeof(T)) return true;
                        Dictionary<int, int> keyPair = new Dictionary<int, int>
                                                   {
                                                       { 263, 171 }, //Mua hàng - thẻ tín dụng từ mua hàng
                                                       { 172, 172 }, //Mua TSCĐ - Mua TSCĐ bằng thẻ tín dụng
                                                       { 173, 173 }, //Mua DV - Mua DV bằng thẻ tín dụng
                                                       { 906, 906 },//Mua CCDC - Phiếu chi mua CCDC
                                                   };
                        //danh sách các properties của chứng từ móc
                        const string strLstKeys = "BranchID,Date,PostedDate,No,CreditCardNumber,AccountingObjectID,AccountingObjectName"
                                                  + ",AccountingObjectAddress,Reason,CurrencyID,ExchangeRate,IsImportPurchase,PaymentClauseID,EmployeeID,TransportMethodID"
                                                  + ",TotalAmount,TotalAmountOriginal,TotalVATAmount,TotalVATAmountOriginal,Recorded,Exported,AccountingObjectBankAccount,AccountingObjectBankName,TotalAll,TotalAllOriginal";
                        //danh sách các properties của _SELECT mà map tương ứng
                        const string strLstOtherKeys = "BranchID"
                                                       + "@MDate,Date"
                                                       + "@MPostedDate,PostedDate"
                                                       + "@MNo,No"
                                                       + "@CreditCardNumber@AccountingObjectID"
                                                       + "@AccountingObjectName"
                                                       + "@AccountingObjectAddress"
                                                       + "@MReasonPay,Reason"
                                                       + "@CurrencyID@ExchangeRate@IsImportPurchase@PaymentClauseID@EmployeeID@TransportMethodID@TotalAmount@TotalAmountOriginal@TotalVATAmount@TotalVATAmountOriginal@Recorded@Exported"
                                                       + "@MAccountingObjectBankAccount,AccountingObjectBankAccount,MAccountingObjectBankAccountDetailID,AccountingObjectBankAccountDetailID"
                                                       + "@MAccountingObjectBankName,AccountingObjectBankName@TotalAll@TotalAllOriginal";
                        GenVocherOtherStand<T, MBCreditCard>(@select, statusForm, keyPair, strLstKeys, strLstOtherKeys, remove);
                    }
                    #endregion

                    #region Nộp tiền vào TK
                    if (lstVocherNttk.Contains(typeid))
                    {
                        Dictionary<int, Type> dicCheck = new Dictionary<int, Type>
                        {
                        };
                        if (dicCheck.Any(x => x.Key == typeid) && dicCheck.FirstOrDefault(x => x.Key == typeid).Value != typeof(T)) return true;
                        Dictionary<int, int> keyPair = new Dictionary<int, int>
                                                   {
                                                       { 322, 162 }, //Bán hàng - Nộp tiền từ bán hàng
                                                       { 325, 162 }, //Bán hàng - Nộp tiền từ bán hàng
                                                   };
                        //danh sách các properties của chứng từ móc
                        const string strLstKeys = "BranchID,Date,PostedDate,No,AccountingObjectID,AccountingObjectName"
                                                  + ",AccountingObjectAddress,BankAccountDetailID,BankName,Reason,CurrencyID,ExchangeRate,SAQuoteID,SAOrderID,PaymentClauseID,EmployeeID,TransportMethodID,InvoiceTemplate,InvoiceDate,InvoiceNo,InvoiceSeries"
                                                  + ",TotalAmount,TotalAmountOriginal,TotalVATAmount,TotalVATAmountOriginal,Recorded,Exported,AccountingObjectBankAccount,AccountingObjectBankName,InvoiceTypeID,IsMatch,MatchDate,TotalAll,TotalAllOriginal";
                        //danh sách các properties của _SELECT mà map tương ứng
                        const string strLstOtherKeys = "BranchID"
                                                       + "@MDate,Date"
                                                       + "@MPostedDate,PostedDate"
                                                       + "@MNo,No"
                                                       + "@AccountingObjectID"
                                                       + "@AccountingObjectName"
                                                       + "@AccountingObjectAddress"
                                                       + "@BankAccountDetailID@BankName"
                                                       + "@MReasonPay,Reason"
                                                       + "@CurrencyID@ExchangeRate@SAQuoteID@SAOrderID@PaymentClauseID@EmployeeID@TransportMethodID@InvoiceTemplate@InvoiceDate@InvoiceNo@InvoiceSeries"
                                                       + "@TotalAmount@TotalAmountOriginal@TotalVATAmount@TotalVATAmountOriginal@Recorded@Exported@MAccountingObjectBankAccount,AccountingObjectBankAccount,MAccountingObjectBankAccountDetailID,AccountingObjectBankAccountDetailID"
                                                       + "@MAccountingObjectBankName,AccountingObjectBankName@InvoiceTypeID@IsMatch@MatchDate@TotalPaymentAmount@TotalPaymentAmountOriginal";
                        GenVocherOtherStand<T, MBDeposit>(@select, statusForm, keyPair, strLstKeys, strLstOtherKeys, remove);
                    }
                    #endregion
                    #endregion

                }
                #region Cập nhật lại Số lượng nhận vào Đơn mua hàng nếu là Mua hàng trả lại
                if (@select is PPDiscountReturn)
                {
                    if (@select.HasProperty("TypeID") && @select.GetProperty<T, int>("TypeID") == 220)
                    {
                        if (@select.HasProperty("PPDiscountReturnDetails"))
                        {
                            var lstdetail = (@select as PPDiscountReturn).PPDiscountReturnDetails;
                            if (lstdetail != null)
                            {
                                foreach (var detail in lstdetail)
                                {
                                    if (detail.ConfrontDetailID != null && detail.ConfrontDetailID != Guid.Empty)
                                    {
                                        var ppdetail = IPPInvoiceDetailService.Getbykey(detail.ConfrontDetailID ?? Guid.Empty);
                                        if (ppdetail != null && ppdetail.DetailID != null && ppdetail.DetailID != Guid.Empty)
                                        {
                                            var OrderDetail = IPPOrderDetailService.Getbykey(ppdetail.DetailID ?? Guid.Empty);
                                            if (OrderDetail != null)
                                            {
                                                bool xoa = statusForm == ConstFrm.optStatusForm.Delete;
                                                if (xoa) OrderDetail.QuantityReceipt = OrderDetail.QuantityReceipt + detail.Quantity;
                                                else OrderDetail.QuantityReceipt = OrderDetail.QuantityReceipt - detail.Quantity;
                                                IPPOrderDetailService.Update(OrderDetail);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
                #region Cập nhật lại Số lượng nhận vào Đơn mua hàng nếu là Mua hàng
                if (typeof(T) == typeof(PPInvoice))
                    status = statusForm == ConstFrm.optStatusForm.Delete
                                 ? RemoveQuantityReceipt(@select as PPInvoice)
                                 : UpdateQuantityReceipt(@select as PPInvoice, statusForm == ConstFrm.optStatusForm.Edit ? backSelect as PPInvoice : null);
                #endregion
                #region Cập nhật lại Số lượng nhận vào Đơn đặt hàng nếu là Hàng bán trả lại
                if (@select is SAReturn)
                {
                    if (@select.HasProperty("TypeID") && @select.GetProperty<T, int>("TypeID") == 330)
                    {
                        if (@select.HasProperty("SAReturnDetails"))
                        {
                            var lstdetail = (@select as SAReturn).SAReturnDetails;
                            if (lstdetail != null)
                            {
                                foreach (var detail in lstdetail)
                                {
                                    if (detail.SAInvoiceDetailID != null && detail.SAInvoiceDetailID != Guid.Empty)
                                    {
                                        var sadetail = ISAInvoiceDetailService.Getbykey(detail.SAInvoiceDetailID ?? Guid.Empty);
                                        if (sadetail != null && sadetail.DetailID != null && sadetail.DetailID != Guid.Empty)
                                        {
                                            var OrderDetail = ISAOrderDetailService.Getbykey(sadetail.DetailID ?? Guid.Empty);
                                            if (OrderDetail != null)
                                            {
                                                bool xoa = statusForm == ConstFrm.optStatusForm.Delete;
                                                if (xoa) OrderDetail.QuantityReceipt = OrderDetail.QuantityReceipt + detail.Quantity;
                                                else OrderDetail.QuantityReceipt = OrderDetail.QuantityReceipt - detail.Quantity;
                                                ISAOrderDetailService.Update(OrderDetail);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
                #region Cập nhật lại Số lượng nhận vào Đơn đặt hàng nếu là Bán hàng
                if (typeof(T) == typeof(SAInvoice))
                    status = statusForm == ConstFrm.optStatusForm.Delete
                                 ? RemoveQuantityReceipt(@select as SAInvoice)
                                 : UpdateQuantityReceipt(@select as SAInvoice, statusForm == ConstFrm.optStatusForm.Edit ? backSelect as SAInvoice : null);
                #endregion
                #region Cập nhật lại Số lượng nhận vào Đơn đặt hàng nếu là Xuất kho cho đơn đặt hàng
                if (typeof(T) == typeof(RSInwardOutward))
                    status = statusForm == ConstFrm.optStatusForm.Delete
                                 ? RemoveQuantityReceipt(@select as RSInwardOutward)
                                 : UpdateQuantityReceipt(@select as RSInwardOutward, statusForm == ConstFrm.optStatusForm.Edit ? backSelect as RSInwardOutward : null);
                #endregion

                if (statusForm != ConstFrm.optStatusForm.Delete && typeid == 500 && @select.HasProperty("ID") && @select.HasProperty("PostedDate"))
                {
                    var id = @select.GetProperty<T, Guid>("ID");
                    var fixedAsset = IFixedAssetService.Getbykey(id);
                    IFixedAssetService.UnbindSession(fixedAsset);
                    fixedAsset = IFixedAssetService.Getbykey(id);
                    if (fixedAsset != null)
                    {
                        fixedAsset.DepreciationDate = @select.GetProperty<T, DateTime>("PostedDate");
                        IFixedAssetService.Update(fixedAsset);
                    }
                }
                else if (typeid == 840 && @select.HasProperty("PSSalarySheetID"))
                {
                    var id = @select.GetProperty<T, Guid>("PSSalarySheetID");
                    var PSSalarySheet = IPSSalarySheetService.Getbykey(id);
                    IFixedAssetService.UnbindSession(PSSalarySheet);
                    PSSalarySheet = IPSSalarySheetService.Getbykey(id);
                    if (PSSalarySheet != null)
                    {
                        if (statusForm != ConstFrm.optStatusForm.Delete)
                            PSSalarySheet.GOtherVoucherID = @select.GetProperty<T, Guid>("ID");
                        else PSSalarySheet.GOtherVoucherID = null;
                        IPSSalarySheetService.Update(PSSalarySheet);
                    }

                }
            }
            catch (Exception ex)
            {
                status = false;
                MSG.Warning(ex.ToString());
            }
            return status;
        }

        void UpdateInwardOutwardFromAssembly<T>(RSInwardOutward input, T @select, int statusForm, int typeid)
        {
            if (statusForm.Equals(ConstFrm.optStatusForm.Add))
            {
                IRSInwardOutwardService.CreateNew(input);
                //    IRSInwardOutwardService.CreateNew(rsOutward);
                IGenCodeService.UpdateGenCodeForm(input.TypeID.GetTypeGroupFromTypeId() ?? (input.TypeID.Equals(404) ? 40 : 41), input.GetProperty<RSInwardOutward, string>("No"), input.GetProperty<RSInwardOutward, Guid>("ID"));
                //IGenCodeService.UpdateGenCodeForm(rsOutward.TypeID.GetTypeGroupFromTypeId() ?? (rsOutward.TypeID.Equals(404) ? 40 : 41), rsOutward.GetProperty<RSInwardOutward, string>("No"));
            }
            else
            {
                IRSInwardOutwardService.Update(input);
                //IRSInwardOutwardService.Update(rsOutward);
            }
        }

        public RSInwardOutward CreateVoucherFromAssembly<T>(T @select, int statusForm, int typeid)
        {
            RSInwardOutward input = new RSInwardOutward();
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                input.ID = Guid.NewGuid();
                input.Recorded = false;
                input.Exported = true;
            }
            else
            {
                if (typeid.Equals(900))
                {
                    input =
                           IRSInwardOutwardService.Query.FirstOrDefault(
                               p => p.RSInwardID == @select.GetProperty<T, Guid>("ID"));
                    IRSInwardOutwardService.UnbindSession(input);
                    input =
                           IRSInwardOutwardService.Query.FirstOrDefault(
                               p => p.RSInwardID == @select.GetProperty<T, Guid>("ID"));
                }
                else
                {
                    input =
                           IRSInwardOutwardService.Query.FirstOrDefault(
                               p => p.RSOutwardID == @select.GetProperty<T, Guid>("ID"));
                    IRSInwardOutwardService.UnbindSession(input);
                    input =
                           IRSInwardOutwardService.Query.FirstOrDefault(
                               p => p.RSOutwardID == @select.GetProperty<T, Guid>("ID"));
                }
            }
            if (input != null)
            {
                input.TypeID = typeid == 900 ? 404 : 411;
                input.No = @select.GetProperty<T, string>(typeid == 900 ? "IWNo" : "OWNo");
                input.PostedDate = @select.GetProperty<T, DateTime>(typeid == 900 ? "IWPostedDate" : "OWPostedDate");
                input.Date = @select.GetProperty<T, DateTime>(typeid == 900 ? "IWDate" : "OWDate");
                input.AccountingObjectID = @select.GetProperty<T, Guid>(typeid == 900 ? "IWAccountingObjectID" : "OWAccountingObjectID");
                input.AccountingObjectName = @select.GetProperty<T, string>(typeid == 900 ? "IWAccountingObjectName" : "OWAccountingObjectName");
                input.Reason = @select.GetProperty<T, string>(typeid == 900 ? "IWReason" : "OWReason");
                input.CurrencyID = @select.GetProperty<T, string>("CurrencyID");
                input.ExchangeRate = @select.GetProperty<T, decimal>("ExchangeRate");
                input.RefDateTime = @select.GetProperty<T, DateTime?>(typeid == 900 ? "RefDateTimeIn" : "RefDateTimeOut");
                if (typeid == 900)
                    input.RSInwardID = @select.GetProperty<T, Guid>("ID");
                else
                    input.RSOutwardID = @select.GetProperty<T, Guid>("ID");
                input.TotalAmount = @select.GetProperty<T, decimal>("Amount");
                input.TotalAmountOriginal = @select.GetProperty<T, decimal>("AmountOriginal");
                var rsIwowDetails = new List<RSInwardOutwardDetail>();
                var rsIoDetail = new RSInwardOutwardDetail();
                rsIoDetail.RSInwardOutwardID = input.ID;
                rsIoDetail.MaterialGoodsID = @select.GetProperty<T, Guid>("MaterialGoodsID");
                rsIoDetail.Description = @select.GetProperty<T, string>("MaterialGoodsName");
                rsIoDetail.RepositoryID = @select.GetProperty<T, Guid>("RepositoryID");
                rsIoDetail.Unit = @select.GetProperty<T, string>("Unit");
                rsIoDetail.UnitPrice = @select.GetProperty<T, decimal>("UnitPrice");
                rsIoDetail.UnitPriceOriginal = @select.GetProperty<T, decimal>("UnitPriceOriginal");
                rsIoDetail.Quantity = @select.GetProperty<T, decimal>("Quantity");
                rsIoDetail.Amount = rsIoDetail.UnitPrice * (decimal)rsIoDetail.Quantity;
                rsIoDetail.AmountOriginal = rsIoDetail.UnitPriceOriginal * (decimal)rsIoDetail.Quantity;
                rsIoDetail.CreditAccount = @select.GetProperty<T, string>("CreditAccount");
                rsIoDetail.DebitAccount = @select.GetProperty<T, string>("DebitAccount");
                rsIwowDetails.Add(rsIoDetail);
                input.RSInwardOutwardDetails.Clear();
                foreach (var item in rsIwowDetails)
                {
                    input.RSInwardOutwardDetails.Add(item);
                }
            }
            return input;
        }

        public RSInwardOutward CreateVoucherFromDismantlement<T>(T @select, int statusForm, int typeid)
        {
            var input = new RSInwardOutward();
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                input.ID = Guid.NewGuid();
                input.Recorded = false;
                input.Exported = true;
            }
            else
            {
                if (typeid == 900)
                {
                    input =
                        IRSInwardOutwardService.Query.FirstOrDefault(
                            p => p.RSOutwardID == @select.GetProperty<T, Guid>("ID"));
                    IRSInwardOutwardService.UnbindSession(input);
                    input =
                        IRSInwardOutwardService.Query.FirstOrDefault(
                            p => p.RSOutwardID == @select.GetProperty<T, Guid>("ID"));
                }
                else
                {
                    input =
                        IRSInwardOutwardService.Query.FirstOrDefault(
                            p => p.RSInwardID == @select.GetProperty<T, Guid>("ID"));
                    IRSInwardOutwardService.UnbindSession(input);
                    input =
                        IRSInwardOutwardService.Query.FirstOrDefault(
                            p => p.RSInwardID == @select.GetProperty<T, Guid>("ID"));
                }
            }
            if (input != null)
            {
                input.TypeID = typeid == 900 ? 411 : 404;
                input.No = @select.GetProperty<T, string>(typeid == 900 ? "OWNo" : "IWNo");
                input.PostedDate = @select.GetProperty<T, DateTime>(typeid == 900 ? "OWPostedDate" : "IWPostedDate");
                input.Date = @select.GetProperty<T, DateTime>(typeid == 900 ? "OWDate" : "IWDate");
                input.AccountingObjectID = @select.GetProperty<T, Guid>(typeid == 900 ? "OWAccountingObjectID" : "IWAccountingObjectID");
                input.AccountingObjectName = @select.GetProperty<T, string>(typeid == 900 ? "OWAccountingObjectName" : "IWAccountingObjectName");
                input.Reason = @select.GetProperty<T, string>(typeid == 900 ? "OWReason" : "IWReason");
                input.CurrencyID = @select.GetProperty<T, string>("CurrencyID");
                input.ExchangeRate = @select.GetProperty<T, decimal>("ExchangeRate");
                input.RefDateTime = @select.GetProperty<T, DateTime?>(typeid == 900 ? "RefDateTimeOut" : "RefDateTimeIn");
                if (typeid == 900)
                    input.RSOutwardID = @select.GetProperty<T, Guid>("ID");
                else
                    input.RSInwardID = @select.GetProperty<T, Guid>("ID");
                //rsOutward.TotalAmount = _select.GetProperty<T, decimal>("Amount");
                //rsOutward.TotalAmountOriginal = _select.GetProperty<T, decimal>("AmountOriginal");
                List<RSInwardOutwardDetail> rsOutwardDetails = new List<RSInwardOutwardDetail>();
                IList<RSAssemblyDismantlementDetail> rsAssemblyDismantlementDetails =
                    @select.GetProperty<T, IList<RSAssemblyDismantlementDetail>>(
                        typeof(RSAssemblyDismantlementDetail).Name + "s");
                foreach (RSAssemblyDismantlementDetail item in rsAssemblyDismantlementDetails)
                {
                    var rsOutwardDetail = new RSInwardOutwardDetail();
                    PropertyInfo[] propInfo = rsOutwardDetail.GetType().GetProperties();
                    foreach (PropertyInfo info in propInfo)
                    {
                        if (info != null && info.CanWrite && !info.Name.Equals("ID"))
                        {
                            PropertyInfo propItem = item.GetType().GetProperty(info.Name);
                            if (propItem != null && propItem.CanWrite && propItem.CanRead)
                                info.SetValue(rsOutwardDetail, propItem.GetValue(item, null), null);
                        }
                    }
                    rsOutwardDetail.RSInwardOutwardID = input.ID;
                    rsOutwardDetails.Add(rsOutwardDetail);
                }
                input.TotalAmount = rsOutwardDetails.Sum(r => r.Amount);
                input.TotalAmountOriginal = rsOutwardDetails.Sum(r => r.AmountOriginal);
                input.RSInwardOutwardDetails.Clear();
                foreach (var item in rsOutwardDetails)
                {
                    input.RSInwardOutwardDetails.Add(item);
                }
            }
            return input;
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseForm));
            this.SuspendLayout();
            // 
            // BaseForm
            // 
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "BaseForm";
            this.ResumeLayout(false);

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            //CreateLoading();

        }
    }
}
