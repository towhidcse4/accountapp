﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.Core.Domain;
using Accounting.Core.DAO;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinEditors;
using System.Windows.Forms.DataVisualization.Charting;

namespace Accounting.Frm.FMain
{
    public partial class UCHealthFinanceEnterprise : UserControl
    {
        ReportProcedureSDS reportProc = new ReportProcedureSDS();
        DateTime _dtFromDateKVBD = new DateTime(DateTime.Now.Year, 1, 1);
        DateTime _dtToDateKVBD = new DateTime(DateTime.Now.Year, 12, 31);
        public UCHealthFinanceEnterprise(bool isGet = true)
        {
            if (!isGet) return;
            InitializeComponent();
            ReportUtils.ProcessControls(this);
            cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            cbbDateTime.ValueChanged += ReportUtils.cbbDateTime_ValueChanged;
            //loaddata for cbbKyVeBieuDo
            loadDataToCbbKyVeBD(cbbKyVeBieuDo);

            loadDataDefault();
        }

        private void loadDataToCbbKyVeBD(UltraCombo cbb)
        {
            List<Item> lstItems = Utils.ObjConstValue.SelectTimesKVBD;
            cbb.DataSource = lstItems;
            cbb.DisplayMember = "Name";
            Utils.ConfigGrid(cbb, ConstDatabase.ConfigXML_TableName);
            cbb.SelectedRow = cbb.Rows[6];
            cbb.ValueChanged += cbbDateTimeKyVeBD_ValueChanged;
        }

        private void cbbDateTimeKyVeBD_ValueChanged(object sender, EventArgs e)
        {
            var cbb = (UltraCombo)sender;
            if (cbb.SelectedRow != null)
            {
                var model = cbb.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                GetDateBeginDateEndKyVeBD(DateTime.Now, model, out dtBegin, out dtEnd);
                _dtFromDateKVBD = dtBegin;
                _dtToDateKVBD = dtEnd;

                loadDataChart();
            }
        }

        private void GetDateBeginDateEndKyVeBD(DateTime ngayHoachToan, Item item, out DateTime dtBegin, out DateTime dtEnd)
        {
            dtBegin = DateTime.Now;
            dtEnd = DateTime.Now;
            int month = ngayHoachToan.Month;
            int year = ngayHoachToan.Year;
            // số ngày từ ngày đầu tuần tới ngày hiện tại
            int alpha = DayOfWeek.Monday - ngayHoachToan.DayOfWeek;
            switch (item.Value)
            {
                case "0": // tháng này
                    dtBegin = new DateTime(year, month, 1);
                    dtEnd = new DateTime(year, month, DateTime.DaysInMonth(year, month));
                    break;
                case "1": // tháng trước
                    int lastMonth, lastYear;
                    if (month == 1)
                    {
                        lastMonth = 12;
                        lastYear = year - 1;
                    }
                    else
                    {
                        lastMonth = month - 1;
                        lastYear = year;
                    }
                    dtBegin = new DateTime(lastYear, lastMonth, 1);
                    dtEnd = new DateTime(lastYear, lastMonth, DateTime.DaysInMonth(lastYear, lastMonth));//edit by cuongpv DateTime.DaysInMonth(year, month) -> DateTime.DaysInMonth(lastYear, lastMonth)
                    break;
                case "2": // quý I
                    dtBegin = new DateTime(year, 1, 1);
                    dtEnd = new DateTime(year, 3, 31);
                    break;
                case "3": // quý II
                    dtBegin = new DateTime(year, 4, 1);
                    dtEnd = new DateTime(year, 6, 30);
                    break;
                case "4": // quý III
                    dtBegin = new DateTime(year, 7, 1);
                    dtEnd = new DateTime(year, 9, 30);
                    break;
                case "5": // quý IV
                    dtBegin = new DateTime(year, 10, 1);
                    dtEnd = new DateTime(year, 12, 31);
                    break;
                case "6": // năm nay
                    dtBegin = new DateTime(year, 1, 1);
                    dtEnd = new DateTime(year, 12, 31);
                    break;
                case "7": // năm trước
                    dtBegin = new DateTime(year - 1, 1, 1);
                    dtEnd = new DateTime(year - 1, 12, 31);
                    break;
            }
        }

        private void loadDataDefault()
        {
            loadDataChart();
            loadDataFinanceMoney();
        }

        private void loadDataChart()
        {
            reportProc = new ReportProcedureSDS();
            var dataChart = reportProc.GetHealthFinanceEnterpriseChart(_dtFromDateKVBD, _dtToDateKVBD);

            if (dataChart != null && dataChart.Count > 0)
            {
                foreach (var item in dataChart)
                {
                    item.TurnoverTotal = Convert.ToDecimal(item.TurnoverTotal.FormatNumberic(ConstDatabase.Format_TienVND));
                    item.CostsTotal = Convert.ToDecimal(item.CostsTotal.FormatNumberic(ConstDatabase.Format_TienVND));
                }
                chartHealthFinance.DataSource = dataChart;
                chartHealthFinance.Series[0].LabelFormat = "###,###,###,###,##0";
                chartHealthFinance.Series[1].LabelFormat = "###,###,###,###,##0";
                chartHealthFinance.DataBind();
            }
            //else
            //    MessageBox.Show("Không có dữ liệu để vẽ biểu đồ");
            
        }

        private void loadDataFinanceMoney()
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            reportProc = new ReportProcedureSDS();

            var dataMoney = reportProc.GetHealthFinanceEnterpriseMoney((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);

            if (dataMoney != null && dataMoney.Count > 0)
            {
                foreach (var item in dataMoney)
                {
                    txtTySoNo.Text = item.tySoNo != 0 ? item.tySoNo.ToString() : "";
                    txtTySoTuTaiTro.Text = item.tyTaiTro != 0 ? item.tyTaiTro.ToString() : "";
                    txtVonLuanChuyen.Text = item.vonLuanChuyen != 0 ? item.vonLuanChuyen.FormatNumberic(ConstDatabase.Format_TienVND) : "";

                    if (item.vonLuanChuyen < 0)
                        txtVonLuanChuyen.Appearance.ForeColor = System.Drawing.Color.Red;
                    else
                        txtVonLuanChuyen.Appearance.ForeColor = System.Drawing.Color.Black;

                    txtHSThanhToanNganHan.Text = item.hsTTNganHan != 0 ? item.hsTTNganHan.ToString() : "";
                    txtHSThanhToanNhanh.Text = item.hsTTNhanh != 0 ? item.hsTTNhanh.ToString() : "";
                    txtHSThanhToanTucThoi.Text = item.hsTTTucThoi != 0 ? item.hsTTTucThoi.ToString() : "";
                    txtHSThanhToanChung.Text = item.hsTTChung != 0 ? item.hsTTChung.ToString() : "";
                    txtHSQuayVongHangTonKho.Text = item.hsQVHangTonKho != 0 ? item.hsQVHangTonKho.ToString() : "";

                    txtHSLoiNhuanVonKD.Text = item.hsLNTrenVonKD > 0 ? item.hsLNTrenVonKD.ToString() : "";
                    txtHSLoiNhuanDoanhThuThuan.Text = item.hsLNTrenDTThuan > 0 ? item.hsLNTrenDTThuan.ToString() : "";
                    txtHSLoiNhuanVonChuSoHuu.Text = item.hsLNTrenVonCSH > 0 ? item.hsLNTrenVonCSH.ToString() : "";

                    txtTienMat.Text = item.tienMat != 0 ? item.tienMat.FormatNumberic(ConstDatabase.Format_TienVND) : "";
                    if (item.tienMat < 0)
                        txtTienMat.Appearance.ForeColor = System.Drawing.Color.Red;
                    else
                        txtTienMat.Appearance.ForeColor = System.Drawing.Color.Black;

                    txtTienGui.Text = item.tienGui != 0 ? item.tienGui.FormatNumberic(ConstDatabase.Format_TienVND) : "";
                    if (item.tienGui < 0)
                        txtTienGui.Appearance.ForeColor = System.Drawing.Color.Red;
                    else
                        txtTienGui.Appearance.ForeColor = System.Drawing.Color.Black;

                    txtDoanhThu.Text = item.doanhThu != 0 ? item.doanhThu.FormatNumberic(ConstDatabase.Format_TienVND) : "";
                    if (item.doanhThu < 0)
                        txtDoanhThu.Appearance.ForeColor = System.Drawing.Color.Red;
                    else
                        txtDoanhThu.Appearance.ForeColor = System.Drawing.Color.Black;

                    txtChiPhi.Text = item.chiPhi != 0 ? item.chiPhi.FormatNumberic(ConstDatabase.Format_TienVND) : "";
                    if (item.chiPhi < 0)
                        txtChiPhi.Appearance.ForeColor = System.Drawing.Color.Red;
                    else
                        txtChiPhi.Appearance.ForeColor = System.Drawing.Color.Black;

                    txtLoiNhuanTruocThue.Text = item.loiNhuanTruocThue !=0 ? item.loiNhuanTruocThue.FormatNumberic(ConstDatabase.Format_TienVND) : "";
                    if (item.loiNhuanTruocThue < 0)
                        txtLoiNhuanTruocThue.Appearance.ForeColor = System.Drawing.Color.Red;
                    else
                        txtLoiNhuanTruocThue.Appearance.ForeColor = System.Drawing.Color.Black;

                    txtPhaiThu.Text = item.phaiThu !=0 ? item.phaiThu.FormatNumberic(ConstDatabase.Format_TienVND) : "";
                    if (item.phaiThu < 0)
                        txtPhaiThu.Appearance.ForeColor = System.Drawing.Color.Red;
                    else
                        txtPhaiThu.Appearance.ForeColor = System.Drawing.Color.Black;

                    txtPhaiTra.Text = item.phaiTra != 0 ? item.phaiTra.FormatNumberic(ConstDatabase.Format_TienVND) : "";
                    if (item.phaiTra < 0)
                        txtPhaiTra.Appearance.ForeColor = System.Drawing.Color.Red;
                    else
                        txtPhaiTra.Appearance.ForeColor = System.Drawing.Color.Black;

                    txtHangTonKho.Text = item.hangTonKho != 0 ? item.hangTonKho.FormatNumberic(ConstDatabase.Format_TienVND) : "";
                    if (item.hangTonKho < 0)
                        txtHangTonKho.Appearance.ForeColor = System.Drawing.Color.Red;
                    else
                        txtHangTonKho.Appearance.ForeColor = System.Drawing.Color.Black;
                }
            }
            else
            {
                //MessageBox.Show("Không có dữ liệu tính các chỉ số tài chính");
                txtTySoNo.Text = "";
                txtTySoTuTaiTro.Text = "";
                txtVonLuanChuyen.Text = "";
                txtHSThanhToanNganHan.Text = "";
                txtHSThanhToanNhanh.Text = "";
                txtHSThanhToanTucThoi.Text = "";
                txtHSThanhToanChung.Text = "";
                txtHSQuayVongHangTonKho.Text = "";

                txtHSLoiNhuanVonKD.Text = "";
                txtHSLoiNhuanDoanhThuThuan.Text = "";
                txtHSLoiNhuanVonChuSoHuu.Text = "";

                txtTienMat.Text = "";
                txtTienGui.Text = "";
                txtDoanhThu.Text = "";
                txtChiPhi.Text = "";
                txtLoiNhuanTruocThue.Text = "";
                txtPhaiThu.Text = "";
                txtPhaiTra.Text = "";
                txtHangTonKho.Text = "";
            }
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            loadDataFinanceMoney();
        }
    }
}
