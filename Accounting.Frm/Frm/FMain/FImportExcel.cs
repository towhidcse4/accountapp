﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;
using Infragistics.Win.UltraWinGrid;
using Spire.Xls;
using System.IO;
using ClosedXML.Excel;
using Infragistics.Win;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using Accounting.Core;

namespace Accounting.Frm.FMain
{
    public partial class FImportExcel : CustormForm
    {
        string path = "";
        List<CategoryMapping> lstCategoryMapping = new List<CategoryMapping>();
        System.Data.DataTable dt;
        public bool closeForm = false;
        public string cbbCheck = null;
        List<Account> list = new List<Account>();
        List<Account> list2 = new List<Account>();
        List<Account> list3 = new List<Account>();
        public readonly List<String> HEADER1 = new List<String> { "Mã nhân viên", "Tên nhân viên", "Phòng ban", "Ngày sinh", "Địa chỉ", "Điện thoại", "Email", "Chức vụ", "Giới tính", "Số tài khoản ngân hàng", "Tên ngân hàng", "Mã số thuế", "Số CMND", "Ngày cấp", "Nơi cấp", "Số người phụ thuộc", "Lương thỏa thuận", "Lương đóng bảo hiểm", "Hệ số lương" };
        public readonly List<String> HEADER2 = new List<String> { "Mã khách hàng", "Tên khách hàng", "Quy mô kế toán", "Địa chỉ", "Mã số thuế", "Điện thoại", "Fax", "Email", "Số tài khoản ngân hàng", "Tên ngân hàng", "Tên người liên hệ", "Chức vụ người liên hệ", "Giới tính người liên hệ", "SĐT người liên hệ", "Email người liên hệ", "Địa chỉ người liên hệ", "Số CMND người liên hệ", "Ngày cấp", "Nơi cấp" };
        public readonly List<String> HEADER3 = new List<String> { "Mã nhà cung cấp", "Tên nhà cung cấp", "Quy mô kế toán", "Địa chỉ", "Mã số thuế", "Điện thoại", "Fax", "Email", "Số tài khoản ngân hàng", "Tên ngân hàng", "Tên người liên hệ", "Chức vụ người liên hệ", "Giới tính người liên hệ", "SĐT người liên hệ", "Email người liên hệ", "Địa chỉ người liên hệ", "Số CMND người liên hệ", "Ngày cấp", "Nơi cấp" };
        public readonly List<String> HEADER4 = new List<String> { "Mã VTHH", "Tên VTHH", "Tính chất", "Đơn vị tính", "Loại VTHH", "Kho ngầm định", "TK Kho", "Đơn giá mua", "TK chi phí", "Tỷ lệ CKMH (%)", "Số lượng tối thiểu", "Thời hạn bảo hành", "Nhóm HHDV chịu thuế TTĐB", "Đơn giá bán", "Thuế suất (%)", "Tỷ lệ CKBH (%)", "TK doanh thu", "Nguồn gốc" };
        public readonly List<String> HEADER5 = new List<String> { "Mã TSCĐ", "Tên TSCĐ", "Loại", "Số hiệu", "Mã Phòng Ban", "Ngày mua", "Ngày ghi tăng","Ngày sử dụng", "Ngày BĐ tính khấu hao", "TK Nguyên giá", "TK khấu hao", "TK chi phí", "Mã ĐT tập hợp chi phí", "Nguyên giá",
                            "Giá trị tính khấu hao", "Thời gian sử dụng ( Tháng)", "Tỷ lệ khấu hao năm", "Giá trị khấu hao năm", "Tỷ lệ khấu hao tháng", "Giá trị khấu hao tháng", "Năm sản xuất", "Nơi sản xuất",
                            "Thời gian bảo hành", "Điều kiện bảo hành", "Mô tả", "Nhà cung cấp", "Trạng thái", "BB giao nhận số", "Ngày" };
        public readonly List<string> HEADER6 = new List<string> { "Mã CCDC", "Tên CCDC", "Đơn vị tính", "Số lượng", "Đơn giá", "Tổng giá trị", "Kiểu phân bổ", "Số kỳ phân bổ", "TK chờ phân bổ", "Đối tượng phân bổ", "Số lượng phân bổ", "Tỷ lệ phân bổ", "Tài khoản chi phí", "Khoản mục chi phí" };
        List<string> accNumber2 = new List<string>() { "241", "154", "642", "811", "631", "632" };
        List<string> accNumber1 = new List<string>() { "152", "153", "154", "155", "156", "157" };
        List<string> accNumber3 = new List<string>() { "5111", "5112", "5113", "5118", ",711", "515" };

        XLWorkbook wb;
        IXLWorksheet ws;
        IXLRange xlRange;
        Dictionary<string, string> dicCategory = new Dictionary<string, string>();
        Dictionary<string, int> headers = new Dictionary<string, int>();
        public FImportExcel()
        {
            InitializeComponent();
            MinimizeBox = true;
            MaximizeBox = true;
            StartPosition = FormStartPosition.CenterScreen;

            Dictionary<string, string> lstCategory = new Dictionary<string, string>()
            {
                //{"Hệ thống tài khoản", "Account"},
                //{"Nhóm tài khoản", "AccountGroup"},
                {"Nhân viên", "AccountingObject1"},
                {"Khách hàng", "AccountingObject2"},
                {"Nhà cung cấp", "AccountingObject3"},
                //{"Tài khoản ngân hàng của khách hàng, nhà cung cấp", "AccountingObjectBankAccount"},
                //{"Loại đối tượng khách hàng, nhà cung cấp (tính chất)", "AccountingObjectCategory"},
                //{"Nhóm đối tượng khách hàng, nhà cung cấp (phạm vi)", "AccountingObjectGroup"},
                //{"Danh sách ngân hàng", "Bank"},
                //{"Tài khoản ngân hàng của doanh nghiệp", "BankAccountDetail"},
                //{"Mục thu/chi", "BudgetItem"},
                //{"Tình trạng hợp đồng", "ContractState"},
                //{"Đối tượng tập hợp chi phí", "CostSet"},
                //{"Đối tượng tính giá thành", "CostSetMaterialGoods"},
                //{"Thẻ tín dụng", "CreditCard"},
                //{"Các loại tiền tệ", "Currency"},
                //{"Danh sách phòng ban", "Department"},
                //{"Đối trừ chứng từ", "ExceptVoucher"},
                //{"Khoản mục chi phí", "ExpenseItem"},
                {"Công cụ dụng cụ", "TIInit" },
                //{"Chi tiết Công cụ dụng cụ", "TIInitDetail" },
                {"Tài sản cố định", "FixedAsset"},
                //{"Phụ kiện kèm theo tài sản cố định", "FixedAssetAccessories"},
                //{"Loại tài sản cố định", "FixedAssetCategory"},
                //{"Chi tiết tài sản cố định", "FixedAssetDetail"},
                //{"Nhóm hàng hóa dịch vụ mua vào", "GoodsServicePurchase"},
                //{"Danh sách nhà đầu tư", "Investor"},
                //{"Nhóm nhà đầu tư", "InvestorGroup"},
                //{"Nhóm đăng ký mua cổ phiếu", "RegistrationGroup"},
                //{"Loại hóa đơn", "InvoiceType"},
                {"Vật tư hàng hóa", "MaterialGoods"},
                //{"Loại vật tư hàng hóa", "MaterialGoodsCategory"},
                //{"Nhóm hàng hóa chịu thuế tài nguyên", "MaterialGoodsResourceTaxGroup"},
                //{"Nhóm hàng hóa, dịch vụ chịu thuế tiêu thụ đặc biệt", "MaterialGoodsSpecialTaxGroup"},
                //{"Điều kiện thanh toán", "PaymentClause"},
                //{"Biểu thuế thu nhập cá nhân", "PersonalSalaryTax"},
                //{"Danh sách kho", "Repository"},
                //{"Nhóm giá bán", "SalePriceGroup"},
                //{"Nhóm cổ đông", "ShareHolderGroup"},
                //{"Loại cổ phần", "StockCategory"},
                //{"Phương thức vận chuyển", "TransportMethod"},
                //{"Thời gian bảo hành", "Warranty"},
                //{"Ký hiệu chấm công", "TimeSheetSymbols"},
                //{"Quy định lương, thuế, bảo hiểm", "PSSalaryTaxInsuranceRegulation"}
            };

            cbbDanhMuc.DataSource = lstCategory;
            cbbDanhMuc.DisplayMember = "Key";
            cbbDanhMuc.ValueMember = "Value";
            Utils.ConfigGrid(cbbDanhMuc, "");
            cbbDanhMuc.DisplayLayout.Bands[0].Columns[0].Header.Caption = "Tên danh mục";
            cbbDanhMuc.DisplayLayout.Bands[0].Columns[1].Hidden = true;
            cbbDanhMuc.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            cbbDanhMuc.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.StartsWith;

            lstCategoryMapping = new List<CategoryMapping>();
            uGridMapping.DataSource = lstCategoryMapping;
            Utils.ConfigGrid(uGridMapping, ConstDatabase.CategoryMapping_TableName);
            uGridMapping.DisplayLayout.Bands[0].Columns["ColumnMapping"].Width = 120;
            uGridMapping.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;


        }

        private void btnBrowser_Click(object sender, EventArgs e)
        {
            #region Code mới
            var openFile = new OpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                Filter = "Excel Files|*.xls;*.xlsx;*.xlsm"

            };

            var result = openFile.ShowDialog(this);
            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                try
                {
                    double fileSize = (double)new System.IO.FileInfo(openFile.FileName).Length / 1024;
                    if (fileSize > 102400)
                    {
                        MSG.Error("Dung lượng file vượt quá mức cho phép (100MB)");
                        return;
                    }
                    lblFileName.Value = openFile.FileName;
                    //using (FileStream file = new FileStream(openFile.FileName, FileMode.Open, FileAccess.Read))
                    //{


                    //}

                }
                catch (Exception ex)
                {
                    MSG.Error("File này đang được mở bởi ứng dụng khác");
                    return;
                }

            }
            #endregion
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            if (cbbDanhMuc.Value == null)
            {
                MSG.Error("Chưa chọn danh mục");
                return;
            }
            string key = cbbDanhMuc.Value.ToString();
            try
            {
                if (lblFileName.Text.IsNullOrEmpty())
                {
                    MSG.Error("Chưa chọn file excel");
                    return;
                }
                if (cbbSheet.Value == null)
                {
                    MSG.Error("Chưa chọn Sheet");
                    return;
                }
                foreach (var item_ in accNumber1)
                {
                    foreach (Account acc in Utils.GetChildrenAccount(new Account(item_)))
                        list.Add(acc);
                }

                foreach (var item_ in accNumber2)
                {
                    foreach (Account acc in Utils.GetChildrenAccount(new Account(item_)))
                        list2.Add(acc);
                }

                foreach (var item_ in accNumber3)
                {
                    foreach (Account acc in Utils.GetChildrenAccount(new Account(item_)))
                        list3.Add(acc);
                }
                ObjectMapping(key);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void cbbDanhMuc_ValueChanged(object sender, EventArgs e)
        {
            List<string> lstColumn = new List<string>();
            List<string> lstColumnCaption = new List<string>();
            List<string> lstDescribe = new List<string>();
            List<string> lstNotAllowNull = new List<string>();

            string key = cbbDanhMuc.Value.ToString();
            cbbCheck = cbbDanhMuc.Value.ToString();
            if (!key.IsNullOrEmpty())
            {
                switch (key)
                {
                    case "AccountingObject1":
                        {//42 
                            lstColumn = new List<string>()
                            {
                                "AccountingObjectCode", "AccountingObjectName", "DepartmentID", "EmployeeBirthday", "Address", "Tel", "Email", "ContactTitle", "ContactSex", "BankAccount", "BankName", "TaxCode", "IdentificationNo", "IssueDate", "IssueBy", "NumberOfDependent", "AgreementSalary", "InsuranceSalary", "SalaryCoefficient"
                            };
                            lstColumnCaption = new List<string>()
                            {
                                "Mã nhân viên", "Tên nhân viên", "Phòng ban", "Ngày sinh", "Địa chỉ", "Điện thoại", "Email", "Chức vụ", "Giới tính", "Tài khoản ngân hàng", "Tên ngân hàng", "Mã số thuế", "Số CMND", "Ngày cấp", "Nơi cấp", "Số người phụ thuộc", "Lương thỏa thuận", "Lương đóng bảo hiểm", "Hệ số lương"
                            };
                            lstDescribe = new List<string>()
                            {
                                "Mã nhân viên", "Tên nhân viên", "Phòng ban", "Ngày sinh", "Địa chỉ", "Điện thoại", "Email", "Chức vụ", "0: Nam; 1: Nữ", "Tài khoản ngân hàng", "Tên ngân hàng", "Mã số thuế", "Số CMND", "Ngày cấp", "Nơi cấp", "Số người phụ thuộc", "Lương thỏa thuận", "Lương đóng bảo hiểm", "Hệ số lương"
                            };
                            lstNotAllowNull = new List<string>() { "AccountingObjectCode", "AccountingObjectName", "DepartmentID" };
                            FileDemo.Text = "Import_NhanVien";
                            break;
                        }
                    case "AccountingObject2":
                        {//42 
                            lstColumn = new List<string>()
                            {
                                "AccountingObjectCode", "AccountingObjectName", "ScaleType", "Address", "TaxCode", "Tel", "Fax", "Email", "BankAccount", "BankName", "ContactName", "ContactTitle", "ContactSex", "ContactMobile",
                                "ContactEmail", "ContactAddress", "IdentificationNo", "IssueDate", "IssueBy",
                            };
                            lstColumnCaption = new List<string>()
                            {
                                "Mã khách hàng", "Tên khách hàng", "Quy mô đối tượng kế toán ", "Địa chỉ", "Mã số thuế", "Điện thoại", "Fax", "Email", "Tài khoản ngân hàng", "Tên ngân hàng", "Tên người liên hệ", "Chức vụ người liên hệ", "Giới tính người liên hệ", "Số điện thoại người liên hệ",
                                "Email người liên hệ", "Địa chỉ người liên hệ", "Số CMND người liên hệ", "Ngày cấp", "Nơi cấp"
                            };
                            lstDescribe = new List<string>()
                            {
                                "Mã khách hàng", "Tên khách hàng", "0: Tổ chức; 1: Cá nhân", "Địa chỉ", "Mã số thuế", "Điện thoại", "Fax", "Email", "Tài khoản ngân hàng", "Tên ngân hàng", "Tên người liên hệ", "Chức vụ người liên hệ", "0: Nam; 1: Nữ", "Số điện thoại người liên hệ",
                                "Email người liên hệ", "Địa chỉ người liên hệ", "Số CMND", "Ngày cấp", "Nơi cấp"
                            };
                            lstNotAllowNull = new List<string>() { "AccountingObjectCode", "AccountingObjectName", "ScaleType" };
                            FileDemo.Text = "Import_KhachHang";
                            break;
                        }
                    case "AccountingObject3":
                        {//42 
                            lstColumn = new List<string>()
                            {
                                "AccountingObjectCode", "AccountingObjectName", "ScaleType", "Address", "TaxCode", "Tel", "Fax", "Email", "BankAccount", "BankName", "ContactName", "ContactTitle", "ContactSex", "ContactMobile", "ContactEmail", "ContactAddress", "IdentificationNo", "IssueDate", "IssueBy",
                            };
                            lstColumnCaption = new List<string>()
                            {
                                "Mã nhà cung cấp", "Tên nhà cung cấp", "Quy mô đối tượng kế toán ", "Địa chỉ", "Mã số thuế", "Điện thoại", "Fax", "Email", "Tài khoản ngân hàng", "Tên ngân hàng", "Tên người liên hệ", "Chức vụ người liên hệ", "Giới tính người liên hệ", "Số điện thoại người liên hệ",
                                "Email người liên hệ", "Địa chỉ người liên hệ", "Số CMND người liên hệ", "Ngày cấp", "Nơi cấp"
                            };
                            lstDescribe = new List<string>()
                            {
                                "Mã nhà cung cấp", "Tên nhà cung cấp", "0: Tổ chức; 1: Cá nhân", "Địa chỉ", "Mã số thuế", "Điện thoại", "Fax", "Email", "Tài khoản ngân hàng", "Tên ngân hàng", "Tên người liên hệ", "Chức vụ người liên hệ", "0: Nam; 1: Nữ", "Số điện thoại người liên hệ",
                                "Email người liên hệ", "Địa chỉ người liên hệ", "Số CMND", "Ngày cấp", "Nơi cấp"
                            };
                            lstNotAllowNull = new List<string>() { "AccountingObjectCode", "AccountingObjectName", "ScaleType" };
                            FileDemo.Text = "Import_NhaCungCap";
                            break;
                        }
                    case "TIInit":
                        {//24
                            lstColumn = new List<string> { "ToolsCode", "ToolsName", "Unit", "Quantity", "UnitPrice", "Amount", "AllocationType", "AllocationTimes", "AllocationAwaitAccount", "ObjectID", "Quantity1", "Rate", "CostAccount", "ExpenseItemID" };
                            lstColumnCaption = new List<string> { "Mã CCDC", "Tên CCDC", "Đơn vị tính", "Số lượng", "Đơn giá", "Tổng giá trị", "Kiểu phân bổ", "Số kỳ phân bổ", "TK chờ phân bổ", "Đối tượng phân bổ", "Số lượng phân bổ", "Tỷ lệ phân bổ", "Tài khoản chi phí", "Khoản mục chi phí" };
                            lstDescribe = new List<string> { "Mã CCDC", "Tên CCDC", "Đơn vị tính", "Số lượng", "Đơn giá", "Tổng giá trị", "Kiểu phân bổ", "Số kỳ phân bổ", "TK chờ phân bổ", "Đối tượng phân bổ", "Số lượng phân bổ", "Tỷ lệ phân bổ", "Tài khoản chi phí", "Khoản mục chi phí" };
                            lstNotAllowNull = new List<string>() { "ToolsCode", "ToolsName", "Quantity", "UnitPrice", "Amount", "AllocationTimes", "ObjectID", "Quantity1" };
                            FileDemo.Text = "Import_CCDC";
                            break;
                        }
                    case "FixedAsset":
                        {//52
                            lstColumn = new List<string>() { /*, "BranchID"*/ "FixedAssetCode", "FixedAssetName",
                            "FixedAssetCategoryID", "SerialNumber", "DepartmentID", "PurchasedDate", "IncrementDate", "UsedDate", "DepreciationDate", "OriginalPriceAccount",
                            "DepreciationAccount", "ExpenditureAccount",  "CostSetID", "OriginalPrice", "PurchasePrice", "UsedTime", "DepreciationRate",
                            "PeriodDepreciationAmount", "MonthDepreciationRate", "MonthPeriodDepreciationAmount", "ProductionYear", "MadeIn", "GuaranteeDuration", "GuaranteeCodition", "Description",
                            "AccountingObjectID", "CurrentState", "DeliveryRecordNo", "DeliveryRecordDate"};
                            lstColumnCaption = new List<string>() { /*, "ID chi nhánh doanh nghiệp"*/  "Mã TSCĐ", "Tên TSCĐ",
                            "Loại", "Số hiệu", "Mã Phòng Ban", "Ngày mua", "Ngày ghi tăng",
                            "Ngày sử dụng", "Ngày BĐ tính khấu hao", "TK Nguyên giá", "TK khấu hao", "TK chi phí",  "Mã ĐT tập hợp chi phí", "Nguyên giá",
                            "Giá trị tính khấu hao", "Thời gian sử dụng ( Tháng)", "Tỷ lệ khấu hao năm", "Giá trị khấu hao năm", "Tỷ lệ khấu hao tháng", "Giá trị khấu hao tháng", "Năm sản xuất", "Nơi sản xuất",
                            "Thời gian bảo hành", "Điều kiện bảo hành", "Mô tả", "Nhà cung cấp", "Trạng thái", "BB giao nhận số", "Ngày"};
                            lstDescribe = new List<string>() { /*, "ID chi nhánh doanh nghiệp"*/  "Mã TSCĐ", "Tên TSCĐ",
                            "Loại", "Số hiệu", "Mã Phòng Ban", "Ngày mua", "Ngày ghi tăng",
                            "Ngày sử dụng", "Ngày BĐ tính khấu hao", "TK Nguyên giá", "TK khấu hao", "TK chi phí",  "Mã ĐT tập hợp chi phí", "Nguyên giá",
                            "Giá trị tính khấu hao", "Thời gian sử dụng ( Tháng)", "Tỷ lệ khấu hao năm", "Giá trị khấu hao năm", "Tỷ lệ khấu hao tháng", "Giá trị khấu hao tháng", "Năm sản xuất", "Nơi sản xuất",
                            "Thời gian bảo hành", "Điều kiện bảo hành", "Mô tả", "Nhà cung cấp", "Trạng thái", "BB giao nhận số", "Ngày"};
                            lstNotAllowNull = new List<string>() { "FixedAssetCode", "FixedAssetName",
                            "FixedAssetCategoryID", "DepartmentID", "IncrementDate", "DepreciationDate", "OriginalPriceAccount",
                            "DepreciationAccount", "ExpenditureAccount" };
                            FileDemo.Text = "Import_TSCĐ";
                            break;
                        }
                    case "MaterialGoods":
                        {//50
                            lstColumn = new List<string>() { "MaterialGoodsCode", "MaterialGoodsName", "MaterialGoodsType", "Unit", "MaterialGoodsCategoryID", "RepositoryID", "ReponsitoryAccount", "PurchasePrice", "ExpenseAccount", "PurchaseDiscountRate", "MinimumStock", "WarrantyTime", "MaterialGoodsGSTID", "SalePrice", "TaxRate", "SaleDiscountRate", "RevenueAccount", "ItemSource" };
                            lstColumnCaption = new List<string>() { "Mã hàng", "Tên hàng", "Tính chất", "Đơn vị tính", "Loại VTHH", "Kho ngầm định", "TK kho", "Đơn giá mua", "TK chi phí", "Tỷ lệ CKMH (%)", "Số lương tối thiểu", "Thời hạn bảo hành", "Nhóm HHDV chịu thuế TTĐB", "Đơn giá bán", "Thuế suất (%)", "Tỷ lệ CKBH (%)", "TK doanh thu", "Nguồn gốc" };
                            lstDescribe = new List<string>() { "Mã hàng", "Tên hàng", "0: VTHH; 1: VTHH lắp ráp/tháo dỡ; 2: Dịch vụ; 3: Thành phẩm; 4: Chỉ là diễn giải; 5: Khác", "Đơn vị tính", "Loại vật tư hàng hóa", "Kho", "Tài khoản kho", "Đơn giá mua", "Tài khoản chi phí", "Tỷ lệ chiết khấu mua hàng (%)", "Số lượng tối thiểu", "Thời hạn bảo hành", "Nhóm hàng hóa dịch vụ chịu thuế tiêu thụ đặc biệt", "Đơn giá bán", "Thuế suất (0; 5; 10; KCT,KTT)", "Tỷ lệ chiết khấu bán hàng (%)", "Tài khoản doanh thu", "Nguồn gốc" };
                            lstNotAllowNull = new List<string>() { "MaterialGoodsCode", "MaterialGoodsName", "MaterialGoodsType" };
                            FileDemo.Text = "Import_VTHH";
                            break;
                        }
                }

                dicCategory = new Dictionary<string, string>();
                for (int i = 0; i < lstColumn.Count; i++)
                {
                    dicCategory.Add(lstColumnCaption[i], lstColumn[i]);
                }
                lstCategoryMapping = new List<CategoryMapping>();
                for (int i = 0; i < lstColumn.Count; i++)
                {
                    lstCategoryMapping.Add(new CategoryMapping
                    {
                        ColumnMapping = lstColumnCaption[i],
                        SheetColumnHeader = "",
                        Describe = lstDescribe[i],
                    });
                }
                foreach (var item in lstCategoryMapping)
                {
                    if (lstNotAllowNull.Contains(dicCategory[item.ColumnMapping]))
                    {
                        item.Status = true;
                    }
                }
                LoadUGrid(lstCategoryMapping);

                //cbbMethods.DataSource = dicCategory;
                //Utils.ConfigGrid(cbbMethods, "");
                //cbbMethods.DisplayMember = "Key";
                //cbbMethods.ValueMember = "Value";
                //cbbMethods.DisplayLayout.Bands[0].Columns[0].Header.Caption = "Tên trường";
                ////cbbMethods.DisplayLayout.Bands[0].Columns[0].Width = 120;
                //cbbMethods.DisplayLayout.Bands[0].Columns[1].Hidden = true;
                //cbbMethods.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                //cbbMethods.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.StartsWith;
                //UltraGridBand band = cbbMethods.DisplayLayout.Bands[0];
                //band.Override.SelectedRowAppearance.ForeColor = Color.Black;
                //band.Override.ActiveRowAppearance.ForeColor = Color.Black;
                ////band.Override.CellAppearance.BackColor = System.Drawing.Color.LightYellow;
                ////band.Override.CellAppearance.BackColor2 = System.Drawing.Color.Black;

                //foreach (var column in uGridMapping.DisplayLayout.Bands[0].Columns)
                //{
                //    this.ConfigEachColumn4Grid(0, column, uGridMapping);
                //    if (column.Key.Equals("ColumnMapping"))
                //    {
                //        column.EditorComponent = cbbMethods;
                //        column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                //    }
                //}
                //foreach (var item in uGridMapping.Rows)
                //{
                //    item.Cells[3].Value = null;
                //}
            }
        }

        private void cbbSheet_ValueChanged(object sender, EventArgs e)
        {
            #region Code mới

            #endregion
        }
        public bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }
        private void ObjectMapping(string tableName)
        {
            try
            {
                WaitingFrm.StartWaiting();
                #region Code mới
                var nonEmptyDataRows = ws.RowsUsed();
                List<TIInit> lstTIInit = new List<TIInit>();
                List<AccountingObject> lstAccountingObject = new List<AccountingObject>();
                List<AccountingObject> lstAccountingObjectUpdate = new List<AccountingObject>();
                List<FixedAsset> lstFixedAsset = new List<FixedAsset>();
                List<MaterialGoods> lstMaterialGoods = new List<MaterialGoods>();
                List<IXLCell> lstErrR = new List<IXLCell>();
                List<FixedAsset> lstUpdateFixedAsset = new List<FixedAsset>();
                string patent = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                                            @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                                            @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                if (tableName == "TIInit")
                {

                    Utils.ITIInitService.BeginTran();

                    List<CategoryMapping> lstA = ((BindingList<CategoryMapping>)uGridMapping.DataSource).ToList();
                    if (lstA.Where(n => n.Status).Any(n => n.SheetColumnHeader == "" || n.SheetColumnHeader == null))
                    {
                        WaitingFrm.StopWaiting();
                        MSG.Error("Cột bắt buộc không được để trống");
                        return;
                    }
                    int IsEndItemDetail = 1;
                    foreach (var item in nonEmptyDataRows.Where(n => n.RowNumber() > 1))
                    {
                        int isDetail = 0;
                        TIInit tiInit = new TIInit();
                        TIInitDetail tiInitdetail = new TIInitDetail();
                        for (int j = 1; j <= headers.Count(); j++)
                        {
                            string key = "";
                            UltraGridColumn column = uGridMapping.DisplayLayout.Bands[0].Columns[1];
                            CategoryMapping categoryMapping = lstA.FirstOrDefault(n => n.SheetColumnHeader == headers.ToList().FirstOrDefault(m => m.Value == j).Value.ToString());
                            if (categoryMapping != null)
                                key = dicCategory[categoryMapping.ColumnMapping];
                            IXLCell xLCell = item.Cell(j);
                            xLCell.MergedRange();
                            xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                            if (xLCell.HasComment)
                                xLCell.Comment.Delete();
                            string value = xLCell.Value.ToString().Replace(" ", "");
                            //foreach (var item in nonEmptyDataRows.Where(n => n.RowNumber() > 1))
                            //{
                            //    TIInit iInit = new TIInit();
                            //    for (int j = 1; j <= headers.Count(); j++)
                            //    {
                            //        CategoryMapping categoryMapping = (CategoryMapping)uGridMapping.Rows[j - 1].ListObject;
                            //        UltraGridColumn column = uGridMapping.DisplayLayout.Bands[0].Columns[1];
                            //        string key = categoryMapping.ColumnMapping;
                            //        IXLCell xLCell = item.Cell(j);
                            //        xLCell.MergedRange();
                            //        xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                            //        if (xLCell.HasComment)
                            //            xLCell.Comment.Delete();
                            //        string value = xLCell.Value.ToString();
                            switch (key)
                            {
                                //case "BranchID":
                                //    {
                                //        iInit.BranchID = Guid.Parse(value);
                                //        break;
                                //    }
                                //case "PostedDate":
                                //    {
                                //        try
                                //        {
                                //            iInit.PostedDate = ((DateTime)xLCell.Value);
                                //        }
                                //        catch (Exception ex) { }
                                //        break;
                                //    }
                                case "ToolsCode":
                                    {

                                        if (string.IsNullOrEmpty(xLCell.Value.ToString().Replace(" ", "")))
                                        {
                                            xLCell.Comment.AddText("Mã công cụ dụng cụ không được để trống");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        else
                                        {
                                            if (xLCell.Value.ToString().Length <= 25)
                                            {
                                                IXLRow check1 = ws.Row(item.RowNumber() + 1);
                                                IXLCell cellcheck = check1.Cell(j);
                                                if (!cellcheck.Value.ToString().IsNullOrEmpty())
                                                {
                                                    if (value == cellcheck.Value.ToString())
                                                    { IsEndItemDetail = 0; }
                                                    else
                                                    {
                                                        IsEndItemDetail = 1;
                                                    }
                                                }
                                                TIInit check = lstTIInit.FirstOrDefault(n => n.ToolsCode == value);
                                                if (check != null)
                                                {
                                                    tiInit = check;
                                                    isDetail = 1;
                                                }
                                                try
                                                {
                                                    tiInit.ToolsCode = (xLCell.Value.ToString().Replace(" ", ""));
                                                }
                                                catch (Exception ex) { }
                                                if (value.IsNullOrEmpty())
                                                {


                                                    xLCell.Comment.AddText("Mã công cụ dụng cụ không được để trống!.");
                                                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                    lstErrR.Add(xLCell);
                                                }
                                                else
                                                {
                                                    Guid id = Utils.ITIInitService.GetGuidBytiinitCode(value) ?? Guid.Empty;

                                                    if (id != Guid.Empty)
                                                    {
                                                        xLCell.Comment.AddText("Mã công cụ dụng cụ đã bị trùng!.");
                                                        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                        lstErrR.Add(xLCell);
                                                    }


                                                }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Mã công cụ dụng cụ không được vượt quá 25 kí tự!.");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }

                                        break;
                                    }
                                case "Quantity":
                                    {
                                        if (isDetail != 1)
                                        {
                                            if (string.IsNullOrEmpty(xLCell.Value.ToString()))
                                            {
                                                xLCell.Comment.AddText("Số lượng không được để trống");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                            {
                                                try
                                                {
                                                    tiInit.Quantity = decimal.Parse(xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                        }
                                        break;
                                    }
                                case "UnitPrice":
                                    {
                                        if (isDetail != 1)
                                        {
                                            if (string.IsNullOrEmpty(xLCell.Value.ToString()))
                                            {
                                                xLCell.Comment.AddText("Đơn giá không được để trống");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                            {
                                                try
                                                {
                                                    tiInit.UnitPrice = decimal.Parse(xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                        }
                                        break;
                                    }
                                case "Amount":
                                    {
                                        if (isDetail != 1)
                                        {
                                            if (string.IsNullOrEmpty(xLCell.Value.ToString()))

                                            {
                                                if (xLCell.ValueCached == null)
                                                {
                                                    xLCell.Comment.AddText("Tổng giá trị không được để trống");
                                                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                    lstErrR.Add(xLCell);
                                                }

                                            }
                                            else
                                            {
                                                try
                                                {
                                                    if (xLCell.ValueCached == null)
                                                    {
                                                        tiInit.Amount = decimal.Parse(xLCell.Value.ToString());
                                                    }
                                                    else
                                                    {
                                                        tiInit.Amount = decimal.Parse(xLCell.ValueCached.ToString());
                                                    }
                                                }
                                                catch (Exception ex) { }
                                            }
                                        }
                                        break;
                                    }
                                case "AllocationTimes":
                                    {
                                        if (isDetail != 1)
                                        {
                                            if (string.IsNullOrEmpty(xLCell.Value.ToString()))
                                            {
                                                xLCell.Comment.AddText("Số kỳ phân bổ không được để trống");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                            {
                                                try
                                                {
                                                    tiInit.AllocationTimes = int.Parse(xLCell.Value.ToString());
                                                }
                                                catch (Exception ex)
                                                {

                                                }
                                            }
                                        }
                                        break;
                                    }


                                case "AllocationType":
                                    {
                                        if (isDetail != 1)
                                        {
                                            try
                                            {
                                                tiInit.AllocationType = int.Parse(xLCell.Value.ToString());
                                            }
                                            catch (Exception ex) { }
                                        }
                                        break;
                                    }

                                case "AllocationAwaitAccount":
                                    {
                                        if (isDetail != 1)
                                        {
                                            try
                                            {
                                                tiInit.AllocationAwaitAccount = (xLCell.Value.ToString());
                                            }
                                            catch (Exception ex) { }
                                        }
                                        break;
                                    }
                                case "ToolsName":
                                    {
                                        if (isDetail != 1)
                                        {
                                            if (string.IsNullOrEmpty(xLCell.Value.ToString()))
                                            {
                                                xLCell.Comment.AddText("Tên công cụ dụng cụ không được để trống");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                            {
                                                if (xLCell.Value.ToString().Length <= 255)
                                                {
                                                    try
                                                    {
                                                        tiInit.ToolsName = (xLCell.Value.ToString());

                                                    }
                                                    catch (Exception ex)
                                                    {

                                                    }
                                                }
                                                else
                                                {
                                                    xLCell.Comment.AddText("Tên công cụ dụng cụ không được quá 255 kí tự");
                                                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                    lstErrR.Add(xLCell);
                                                }
                                            }
                                        }
                                        break;
                                    }


                                case "Unit":
                                    {
                                        if (isDetail != 1)
                                        {
                                            if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                            {
                                                if (xLCell.Value.ToString().Length <= 255)
                                                {
                                                    try
                                                    {
                                                        tiInit.Unit = (xLCell.Value.ToString());
                                                    }
                                                    catch (Exception ex) { }
                                                }
                                                else
                                                {
                                                    xLCell.Comment.AddText("Đơn vị không được quá 255 kí tự");
                                                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                    lstErrR.Add(xLCell);
                                                }
                                            }
                                        }
                                        break;
                                    }
                                case "ObjectID":
                                    {
                                        if (string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            xLCell.Comment.AddText("Mã đối tượng không được để trống");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        else
                                        {
                                            try
                                            {
                                                Guid id = Utils.IDepartmentService.GetGuidBydepartmentCode(value) ?? Guid.Empty;
                                                Guid id1 = Utils.IExpenseItemService.GetGuidByExpenseItemCode(value) ?? Guid.Empty;
                                                if (id != Guid.Empty || id1 != Guid.Empty)
                                                {
                                                    if (id != Guid.Empty)
                                                    {
                                                        tiInitdetail.ObjectID = id;
                                                        tiInitdetail.ObjectType = 1;
                                                    }
                                                    else if (id1 != Guid.Empty)
                                                    {
                                                        tiInitdetail.ObjectID = id1;
                                                        tiInitdetail.ObjectType = 0;
                                                    }
                                                    tiInit.TIInitDetails.Add(tiInitdetail);
                                                }
                                                else
                                                {
                                                    xLCell.Comment.AddText("Mã đối tượng không hợp lệ!.");
                                                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                    lstErrR.Add(xLCell);
                                                }
                                            }
                                            catch (Exception ex) { }
                                        }
                                        break;
                                    }
                                case "Rate":
                                    {
                                        try
                                        {
                                            tiInitdetail.Rate = decimal.Parse(xLCell.Value.ToString());
                                            if (IsEndItemDetail == 1)
                                            {
                                                if (tiInit.TIInitDetails.Sum(n => n.Rate == null ? 0 : n.Rate) != 100)
                                                {
                                                    xLCell.Comment.AddText("Tỷ lệ phân bổ của 1 mã công cụ dụng cụ phải bằng 100%!.");
                                                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                    lstErrR.Add(xLCell);
                                                }
                                            }
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "CostAccount":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (Utils.IAccountService.Query.Any(n => n.AccountNumber == xLCell.Value.ToString()))
                                            {
                                                try
                                                {
                                                    tiInitdetail.CostAccount = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Tài khoản chi phí không hợp lệ!");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "ExpenseItemID":
                                    {
                                        if (!value.IsNullOrEmpty())
                                        {
                                            try
                                            {
                                                Guid id1 = Utils.IExpenseItemService.GetGuidByExpenseItemCode(value) ?? Guid.Empty;
                                                if (id1 != Guid.Empty)
                                                {
                                                    ExpenseItem expenseItem = Utils.IExpenseItemService.GetExpenseItemByGuid(id1);
                                                    if (expenseItem != null)
                                                    {
                                                        if (expenseItem.IsParentNode == true)
                                                        {
                                                            xLCell.Comment.AddText("Khoản mục chi phí không được là chỉ tiêu tổng hợp!.");
                                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                            lstErrR.Add(xLCell);
                                                        }
                                                        else
                                                        {
                                                            tiInitdetail.ExpenseItemID = id1;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        xLCell.Comment.AddText("Mã đối tượng tập hợp chi phí không hợp lệ!.");
                                                        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                        lstErrR.Add(xLCell);
                                                    }

                                                }
                                                else
                                                {
                                                    xLCell.Comment.AddText("Mã đối tượng tập hợp chi phí không hợp lệ!.");
                                                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                    lstErrR.Add(xLCell);
                                                }
                                            }
                                            catch (Exception ex) { }
                                        }
                                        break;
                                    }
                                case "Quantity1":
                                    {
                                        if (string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            xLCell.Comment.AddText("Số lượng đối tượng không được để trống");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        else
                                        {
                                            try
                                            {
                                                tiInitdetail.Quantity = decimal.Parse(xLCell.Value.ToString());
                                                if (IsEndItemDetail == 1)
                                                {
                                                    if (tiInit.Quantity != tiInit.TIInitDetails.Sum(n => n.Quantity == null ? 0 : n.Quantity))
                                                    {
                                                        xLCell.Comment.AddText("Số lượng phân bổ không khớp với số lượng phần chi tiết!.");
                                                        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                        lstErrR.Add(xLCell);
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                xLCell.Comment.AddText("Sai số lượng!.");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                            }
                        }
                        if (isDetail == 0)
                        {
                            lstTIInit.Add(tiInit);
                        }

                    }
                    //foreach(var item in lstTIInit)
                    //{
                    //    if (item.Quantity != item.TIInitDetails.Sum(n => n.Quantity == null ? 0 : n.Quantity))
                    //    {
                    //        IXLRow xLRow = ws.Row(1);
                    //        IXLCell xLCell = xLRow.Cell(12);
                    //        xLCell.Comment.AddText("Số lượng phân bổ của công cụ dụng cụ"+item.ToolsCode+" không khớp với phần chi tiết!.");
                    //        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    //        lstErrR.Add(xLCell);
                    //    };
                    //    if (item.TIInitDetails.Sum(n => n.Rate == null ? 0 : n.Rate)!=100)
                    //    {
                    //        IXLRow xLRow = ws.Row(1);
                    //        IXLCell xLCell = xLRow.Cell(13);
                    //        xLCell.Comment.AddText("Tỷ lệ phân bổ của công cụ dụng cụ" + item.ToolsCode + " phải bằng 100%!.");
                    //        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    //        lstErrR.Add(xLCell);
                    //    }
                    //}
                }
                else if (tableName == "AccountingObject1" || tableName == "AccountingObject2" || tableName == "AccountingObject3")
                {
                    Utils.IAccountingObjectService.BeginTran();
                    List<CategoryMapping> lstA = ((BindingList<CategoryMapping>)uGridMapping.DataSource).ToList();
                    if (lstA.Where(n => n.Status).Any(n => n.SheetColumnHeader == "" || n.SheetColumnHeader == null))
                    {
                        WaitingFrm.StopWaiting();
                        MSG.Error("Cột bắt buộc không được để trống");
                        return;
                    }
                    foreach (var item in nonEmptyDataRows.Where(n => n.RowNumber() > 1))
                    {
                        AccountingObject accountingObject = new AccountingObject();
                        if (tableName == "AccountingObject1")
                        {
                            accountingObject.IsEmployee = true;

                        }
                        else if (tableName == "AccountingObject2")
                        {
                            accountingObject.ObjectType = 0;
                        }
                        else if (tableName == "AccountingObject3")
                        {
                            accountingObject.ObjectType = 1;
                        }
                        bool isUpdate = false;
                        for (int j = 1; j <= headers.Count(); j++)
                        {
                            string key = "";
                            UltraGridColumn column = uGridMapping.DisplayLayout.Bands[0].Columns[1];
                            CategoryMapping categoryMapping = lstA.FirstOrDefault(n => n.SheetColumnHeader == headers.ToList().FirstOrDefault(m => m.Value == j).Value.ToString());
                            if (categoryMapping != null)
                                key = dicCategory[categoryMapping.ColumnMapping];
                            IXLCell xLCell = item.Cell(j);
                            xLCell.MergedRange();
                            xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                            if (xLCell.HasComment)
                                xLCell.Comment.Delete();
                            string value = xLCell.Value.ToString().Replace(" ", "");
                            switch (key)
                            {
                                //case "BranchID":
                                //    {
                                //        accountingObject.BranchID = Guid.Parse(value);
                                //        break;
                                //    }
                                case "AccountingObjectCode":
                                    {
                                        try
                                        {
                                            if (!string.IsNullOrEmpty(xLCell.Value.ToString().Replace(" ", "")))
                                            {
                                                if (xLCell.Value.ToString().Length <= 25)
                                                {
                                                    accountingObject.AccountingObjectCode = (xLCell.Value.ToString().Replace(" ", ""));
                                                    if (string.IsNullOrEmpty(xLCell.Value.ToString()))
                                                    {
                                                        xLCell.Comment.AddText("Mã đối tượng không được để trống");
                                                        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                        lstErrR.Add(xLCell);
                                                    }
                                                    else if (Utils.ListAccountingObject.Any(n => n.AccountingObjectCode.Equals(xLCell.Value.ToString().Replace(" ", ""))))
                                                    {
                                                        if (tableName == "AccountingObject2" || tableName == "AccountingObject3")
                                                        {
                                                            isUpdate = true;
                                                            lstAccountingObjectUpdate.Add(accountingObject);
                                                            xLCell.Comment.AddText("Mã đối tượng bị trùng");
                                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                        }
                                                        else if (tableName == "AccountingObject1")
                                                        {
                                                            xLCell.Comment.AddText("Mã đối tượng đã tồn tại");
                                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                            lstErrR.Add(xLCell);
                                                        }
                                                    }
                                                    else if (lstAccountingObject.Any(n => n.AccountingObjectCode == accountingObject.AccountingObjectCode))
                                                    {
                                                        xLCell.Comment.AddText("Mã đối tượng bị trùng");
                                                        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                        lstErrR.Add(xLCell);
                                                    }
                                                }
                                                else
                                                {
                                                    xLCell.Comment.AddText("Mã đối tượng không được vượt quá 25 kí tự");
                                                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                    lstErrR.Add(xLCell);
                                                }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Mã đối tượng không được để trống");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Mã đối tượng không được để trống");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                    }
                                case "AccountingObjectName":
                                    {
                                        try
                                        {
                                            if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                            {
                                                if (xLCell.Value.ToString().Length <= 512)
                                                {
                                                    accountingObject.AccountingObjectName = (xLCell.Value.ToString());
                                                    if (string.IsNullOrEmpty(accountingObject.AccountingObjectName))
                                                    {
                                                        xLCell.Comment.AddText("Tên đối tượng không được để trống");
                                                        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                        lstErrR.Add(xLCell);
                                                    }
                                                }
                                                else
                                                {
                                                    xLCell.Comment.AddText("Tên đối tượng không được quá 512 kí tự");
                                                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                    lstErrR.Add(xLCell);
                                                }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Tên đối tượng không được để trống");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Tên đối tượng không được để trống");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                        //if (value.IsNullOrEmpty())
                                        //{
                                        //    MessageBox.Show("Tên đối tượng kế toán không được để trống!.");
                                        //    return;
                                        //}
                                        //else
                                        //{
                                        //    accountingObject.AccountingObjectName = value;
                                        //}
                                        //break;
                                    }
                                case "AccountingObjectCategory":
                                    {
                                        try
                                        {
                                            accountingObject.AccountingObjectCategory = (xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }


                                        string AccountingObjectC = "";
                                        try
                                        {
                                            AccountingObjectC = (xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        if (AccountingObjectC.IsNullOrEmpty())
                                        {
                                            xLCell.Comment.AddText("Phòng ban không hợp lệ");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        else
                                        {
                                            Guid id = Utils.IAccountingObjectCategoryService.GetGuidAccountingObjectCategoryByCode(AccountingObjectC) ?? Guid.Empty;
                                            if (id == null || id == Guid.Empty)
                                            {
                                                xLCell.Comment.AddText("Loại khách hàng/NCC không hợp lệ");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                            {
                                                accountingObject.AccountingObjectCategory = xLCell.Value.ToString();
                                            }
                                        }
                                        break;
                                    }
                                case "EmployeeBirthday":
                                    {
                                        try
                                        {
                                            if (xLCell.Value.ToString() != "")
                                                accountingObject.EmployeeBirthday = ((DateTime)xLCell.Value);
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Ngày sinh không hợp lệ");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                    }
                                case "Address":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 512)
                                            {
                                                try
                                                {
                                                    accountingObject.Address = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Địa chỉ không được quá 512 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "Tel":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 25)
                                            {
                                                try
                                                {
                                                    accountingObject.Tel = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Số điện thoại không được quá 25 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "Fax":
                                    {
                                        try
                                        {
                                            if (xLCell.Value.ToString().Trim(' ').Count() > 25)
                                            {
                                                xLCell.Comment.AddText("Fax vượt quá ký tự cho phép");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                                accountingObject.Fax = (xLCell.Value.ToString().Trim(' '));
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "Email":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 100)
                                            {
                                                try
                                                {
                                                    accountingObject.Email = (xLCell.Value.ToString().Trim(' '));
                                                    Regex objNotWholePattern = new Regex(patent);
                                                    // Bắt kiếu số cho loại sổ lưu

                                                    Match Email = objNotWholePattern.Match(accountingObject.Email);
                                                    if (!xLCell.Value.ToString().Equals(""))
                                                    {
                                                        if (!xLCell.Value.ToString().Equals(""))
                                                        {
                                                            if (!Email.Success)
                                                            {
                                                                xLCell.Comment.AddText("Email không đúng định dạng");
                                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                                lstErrR.Add(xLCell);
                                                            }
                                                        }
                                                    }
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Email không được quá 100 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "Website":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 100)
                                            {
                                                try
                                                {
                                                    accountingObject.Website = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Website không được quá 100 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "BankAccount":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 50)
                                            {
                                                try
                                                {
                                                    accountingObject.BankAccount = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Tài khoản ngân hàng không được quá 50 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "BankName":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 512)
                                            {
                                                try
                                                {
                                                    accountingObject.BankName = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Tên ngân hàng không được quá 512 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;

                                    }
                                case "TaxCode":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 50)
                                            {
                                                try
                                                {
                                                    accountingObject.TaxCode = (xLCell.Value.ToString().Trim(' '));
                                                    if (!(xLCell.Value.ToString()).Equals(""))
                                                    {
                                                        if (!Core.Utils.CheckMST((xLCell.Value.ToString())))
                                                        {
                                                            xLCell.Comment.AddText("Mã số thuế không hợp lệ");
                                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                            lstErrR.Add(xLCell);
                                                        }
                                                    }
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Mã số thuế không được quá 50 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "Description":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 512)
                                            {
                                                try
                                                {
                                                    accountingObject.Description = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Mô tả không được quá 512 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "ContactName":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 512)
                                            {
                                                try
                                                {
                                                    accountingObject.ContactName = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Tên liện hệ không được quá 512 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "ContactTitle":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 512)
                                            {
                                                try
                                                {
                                                    accountingObject.ContactTitle = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Chúc vụ không được quá 512 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "ContactSex":
                                    {
                                        try
                                        {
                                            if (xLCell.Value.ToString() != "")
                                            {
                                                if (xLCell.Value.ToString().ToUpper() == "NAM")
                                                {
                                                    accountingObject.ContactSex = 0;
                                                }
                                                else if (xLCell.Value.ToString().ToUpper() == "NỮ")
                                                {
                                                    accountingObject.ContactSex = 1;
                                                }
                                                else
                                                {
                                                    accountingObject.ContactSex = int.Parse(xLCell.Value.ToString());
                                                }
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Dữ liệu không hợp lệ");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                    }
                                case "ContactMobile":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 25)
                                            {
                                                try
                                                {
                                                    accountingObject.ContactMobile = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Số điện thoại liện hệ không được quá 25 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "ContactEmail":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 100)
                                            {
                                                try
                                                {
                                                    accountingObject.ContactEmail = (xLCell.Value.ToString().Trim(' '));
                                                    Regex objNotWholePattern = new Regex(patent);
                                                    // Bắt kiếu số cho loại sổ lưu
                                                    Match Email = objNotWholePattern.Match(accountingObject.ContactEmail);
                                                    if (!xLCell.Value.ToString().Equals(""))
                                                    {
                                                        if (!xLCell.Value.ToString().Equals(""))
                                                        {
                                                            if (!Email.Success)
                                                            {
                                                                xLCell.Comment.AddText("Email người liên hệ không đúng định dạng");
                                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                                lstErrR.Add(xLCell);
                                                            }
                                                        }
                                                    }
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Email liện hệ không được quá 100 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "ContactHomeTel":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 25)
                                            {
                                                try
                                                {
                                                    accountingObject.ContactHomeTel = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Số điện thoại tại nhà không được quá 25 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "ContactOfficeTel":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 25)
                                            {
                                                try
                                                {
                                                    accountingObject.ContactOfficeTel = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Số điện thoại văn phòng không được quá 25 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "ContactAddress":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 512)
                                            {
                                                try
                                                {
                                                    accountingObject.ContactAddress = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Địa chỉ liên lạc không được quá 25 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "ScaleType":// Quy mô đối tượng kế toán
                                    {
                                        try
                                        {
                                            if (xLCell.Value.ToString() != "")
                                            {
                                                if (xLCell.Value.ToString().ToUpper() == "TỔ CHỨC")
                                                {
                                                    accountingObject.ScaleType = 0;
                                                }
                                                else if (xLCell.Value.ToString().ToUpper() == "CÁ NHÂN")
                                                {
                                                    accountingObject.ScaleType = 1;
                                                }
                                                else
                                                {
                                                    accountingObject.ScaleType = int.Parse(xLCell.Value.ToString());
                                                }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Dữ liệu không hợp lệ");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Dữ liệu không hợp lệ");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                    }
                                case "ObjectType":
                                    {
                                        try
                                        {
                                            if (xLCell.Value.ToString() != "0" && xLCell.Value.ToString() != "1"/*&& xLCell.Value.ToString() != "2"*/)
                                            {
                                                xLCell.Comment.AddText("Dữ liệu không hợp lệ");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                                accountingObject.ObjectType = int.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Dữ liệu không hợp lệ");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                    }
                                case "IsEmployee":
                                    {
                                        string IsEmployee = "";
                                        try
                                        {
                                            IsEmployee = (xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        if (IsEmployee.IsNullOrEmpty())
                                        {
                                            MessageBox.Show("Có phải là nhân viên không được để trống!.");
                                        }
                                        else
                                        {
                                            accountingObject.IsEmployee = (IsEmployee == "1") ? true : false;
                                        }
                                        break;
                                    }
                                case "IdentificationNo":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 25)
                                            {
                                                try
                                                {
                                                    accountingObject.IdentificationNo = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Số CMND không được quá 25 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "IssueDate":
                                    {
                                        try
                                        {
                                            if (xLCell.Value.ToString() != "")
                                                accountingObject.IssueDate = ((DateTime)xLCell.Value);
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Dữ liệu không hợp lệ");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                    }
                                case "IssueBy":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 512)
                                            {
                                                try
                                                {
                                                    accountingObject.IssueBy = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Nơi cấp không được quá 512 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "DepartmentID":
                                    {
                                        string DepartmentCode = "";
                                        try
                                        {
                                            DepartmentCode = (xLCell.Value.ToString().Replace(" ", ""));
                                        }
                                        catch (Exception ex) { }
                                        if (DepartmentCode.IsNullOrEmpty())
                                        {
                                            xLCell.Comment.AddText("Phòng ban không hợp lệ");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        else
                                        {
                                            Guid id = Utils.IDepartmentService.GetGuidDepartmentByCode(DepartmentCode) ?? Guid.Empty;
                                            if (id == null || id == Guid.Empty)
                                            {
                                                xLCell.Comment.AddText("Phòng ban không hợp lệ");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                            {
                                                accountingObject.DepartmentID = id;
                                            }
                                        }
                                        break;
                                    }
                                case "IsInsured":
                                    {
                                        try
                                        {
                                            if (xLCell.Value.ToString() != "1" && xLCell.Value.ToString() != "0")
                                            {
                                                xLCell.Comment.AddText("Dữ liệu không hợp lệ");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                                accountingObject.IsInsured = ((xLCell.Value.ToString()) == "1") ? true : false;
                                        }
                                        catch (Exception ex) { }
                                        break;
                                        //if (value.IsNullOrEmpty())
                                        //{
                                        //    MessageBox.Show("Đóng bảo hiểm không được để trống!.");
                                        //    return;
                                        //}
                                        //else
                                        //{
                                        //    accountingObject.IsInsured = (value == "1") ? true : false;
                                        //}
                                        //break;
                                    }
                                case "IsLabourUnionFree":
                                    {
                                        try
                                        {
                                            if (xLCell.Value.ToString() != "1" && xLCell.Value.ToString() != "0")
                                            {
                                                xLCell.Comment.AddText("Dữ liệu không hợp lệ");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                                accountingObject.IsLabourUnionFree = ((xLCell.Value.ToString()) == "1") ? true : false;
                                        }
                                        catch (Exception ex) { }
                                        break;
                                        //if (value.IsNullOrEmpty())
                                        //{
                                        //    MessageBox.Show("Đóng đoàn phí không được để trống!.");
                                        //    return;
                                        //}
                                        //else
                                        //{
                                        //    accountingObject.IsLabourUnionFree = (value == "1") ? true : false;
                                        //}
                                        //break;
                                    }
                                case "FamilyDeductionAmount":
                                    {
                                        try
                                        {
                                            if (xLCell.Value.ToString() != "")
                                                accountingObject.FamilyDeductionAmount = decimal.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Dữ liệu không hợp lệ");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                        //if (value.IsNullOrEmpty())
                                        //{
                                        //    MessageBox.Show("Khoản giảm trừ gia cảnh không được để trống!.");
                                        //    return;
                                        //}
                                        //else
                                        //{
                                        //    accountingObject.FamilyDeductionAmount = decimal.Parse(value);
                                        //}
                                        //break;
                                    }
                                case "MaximizaDebtAmount":
                                    {
                                        try
                                        {
                                            if (xLCell.Value.ToString() != "")
                                                accountingObject.MaximizaDebtAmount = decimal.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Dữ liệu không hợp lệ");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                    }
                                case "DueTime":
                                    {
                                        try
                                        {
                                            if (xLCell.Value.ToString() != "")
                                                accountingObject.DueTime = int.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Dữ liệu không hợp lệ");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                    }
                                case "AccountObjectGroupID":
                                    {
                                        string accountingObjectCode = "";
                                        try
                                        {
                                            accountingObjectCode = (xLCell.Value.ToString().Replace(" ", ""));
                                        }
                                        catch (Exception ex) { }
                                        if (accountingObjectCode.IsNullOrEmpty())
                                        {
                                            accountingObject.AccountObjectGroupID = null;
                                        }
                                        else
                                        {
                                            Guid id = Utils.IAccountingObjectGroupService.GetGuidAccountingObjectGroupByCode(accountingObjectCode) ?? Guid.Empty;
                                            if (id == null || id == Guid.Empty)
                                            {
                                                MessageBox.Show("Nhóm đối tượng không tồn tại!.");
                                                accountingObject.AccountObjectGroupID = null;
                                            }
                                            else
                                            {
                                                accountingObject.AccountObjectGroupID = id;
                                            }
                                        }
                                        break;
                                    }
                                case "IsActive":
                                    {
                                        try
                                        {
                                            if (xLCell.Value.ToString() != "1" && xLCell.Value.ToString() != "0")
                                            {
                                                xLCell.Comment.AddText("Dữ liệu không hợp lệ");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                            {
                                                int check = int.Parse(xLCell.Value.ToString());
                                                accountingObject.IsActive = check == 1 ? true : false;
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Lỗi");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                    }
                                case "NumberOfDependent":
                                    {
                                        try
                                        {
                                            if (xLCell.Value.ToString() != "")
                                                accountingObject.NumberOfDependent = int.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Dữ liệu không hợp lệ");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                        //if (value.IsNullOrEmpty())
                                        //    accountingObject.NumberOfDependent = 0;
                                        //else
                                        //    accountingObject.NumberOfDependent = int.Parse(value);
                                        //break;
                                    }
                                case "PaymentClauseID":
                                    {
                                        string PaymentClauseCode = "";
                                        try
                                        {
                                            PaymentClauseCode = (xLCell.Value.ToString().Replace(" ", ""));
                                        }
                                        catch (Exception ex) { }
                                        if (PaymentClauseCode.IsNullOrEmpty())
                                        {
                                            accountingObject.PaymentClauseID = null;
                                        }
                                        else
                                        {
                                            Guid id = Utils.IPaymentClauseService.GetGuidPaymentClauseByCode(PaymentClauseCode);
                                            if (id == null || id == Guid.Empty)
                                            {
                                                MessageBox.Show("Điều khoản thanh toán không tồn tại!.");
                                                accountingObject.PaymentClauseID = null;
                                            }
                                            else
                                            {
                                                accountingObject.PaymentClauseID = id;
                                            }
                                        }
                                        break;
                                    }
                                case "AgreementSalary":
                                    {
                                        try
                                        {
                                            if (xLCell.Value.ToString() != "")
                                                accountingObject.AgreementSalary = decimal.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Dữ liệu không hợp lệ");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                    }
                                case "InsuranceSalary":
                                    {
                                        try
                                        {
                                            if (xLCell.Value.ToString() != "")
                                                accountingObject.InsuranceSalary = decimal.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Dữ liệu không hợp lệ");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                    }
                                case "SalaryCoefficient":
                                    {
                                        try
                                        {
                                            if (xLCell.Value.ToString() != "")
                                                accountingObject.SalaryCoefficient = decimal.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Dữ liệu không hợp lệ");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                    }
                            }
                        }
                        if (!accountingObject.BankAccount.IsNullOrEmpty() || !accountingObject.BankName.IsNullOrEmpty())
                        {
                            AccountingObjectBankAccount accountingObjectBankAccount = new AccountingObjectBankAccount();
                            accountingObjectBankAccount.BankName = accountingObject.BankName;
                            accountingObjectBankAccount.BankAccount = accountingObject.BankAccount;
                            accountingObjectBankAccount.OrderPriority = 0;
                            accountingObjectBankAccount.IsSelect = true;

                            accountingObject.BankAccounts.Add(accountingObjectBankAccount);

                        }
                        if (isUpdate == false)
                        {
                            lstAccountingObject.Add(accountingObject);
                        }
                        //accountingObject.ID = Guid.NewGuid();
                        //accountingObject.BranchID = null;
                        //if (!(accountingObject.AccountingObjectCode.IsNullOrEmpty() || accountingObject.AccountingObjectName.IsNullOrEmpty() || accountingObject.IsEmployee.ToString() == "" ||
                        //    accountingObject.IsInsured.ToString() == "" || accountingObject.IsLabourUnionFree.ToString() == "" || accountingObject.FamilyDeductionAmount.ToString() == "" ||
                        //    accountingObject.IsActive.ToString() == ""))
                        //{
                        //    Utils.IAccountingObjectService.CreateNew(accountingObject);
                        //}
                    }
                    //Utils.IAccountingObjectService.CommitTran();
                }
                else if (tableName == "FixedAsset")
                {
                    Utils.IFixedAssetService.BeginTran();
                    List<CategoryMapping> lstA = ((BindingList<CategoryMapping>)uGridMapping.DataSource).ToList();
                    if (lstA.Where(n => n.Status).Any(n => n.SheetColumnHeader == "" || n.SheetColumnHeader == null))
                    {
                        WaitingFrm.StopWaiting();
                        MSG.Error("Cột bắt buộc không được để trống");
                        return;
                    }
                    foreach (var item in nonEmptyDataRows.Where(n => n.RowNumber() > 1))
                    {
                        FixedAsset fixedAsset = new FixedAsset();
                        for (int j = 1; j <= headers.Count(); j++)
                        {
                            string key = "";
                            UltraGridColumn column = uGridMapping.DisplayLayout.Bands[0].Columns[1];
                            CategoryMapping categoryMapping = lstA.FirstOrDefault(n => n.SheetColumnHeader == headers.ToList().FirstOrDefault(m => m.Value == j).Value.ToString());
                            if (categoryMapping != null)
                                key = dicCategory[categoryMapping.ColumnMapping];
                            IXLCell xLCell = item.Cell(j);
                            xLCell.MergedRange();
                            xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                            if (xLCell.HasComment)
                                xLCell.Comment.Delete();
                            string value = xLCell.Value.ToString().Replace(" ", "");
                            //        Utils.IFixedAssetService.BeginTran();
                            //foreach (var item in nonEmptyDataRows.Where(n => n.RowNumber() > 1))
                            //{
                            //    FixedAsset fixedAsset = new FixedAsset();
                            //    for (int j = 1; j < headers.Count(); j++)
                            //    {
                            //        CategoryMapping categoryMapping = (CategoryMapping)uGridMapping.Rows[j - 1].ListObject;
                            //        UltraGridColumn column = uGridMapping.DisplayLayout.Bands[0].Columns[1];
                            //        string key = categoryMapping.ColumnMapping;
                            //        IXLCell xLCell = item.Cell(j);
                            //        xLCell.MergedRange();
                            //        xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                            //        if (xLCell.HasComment)
                            //            xLCell.Comment.Delete();
                            //        string value = xLCell.Value.ToString();
                            switch (key)
                            {
                                //case "BranchID":
                                //    {
                                //        fixedAsset.BranchID = Guid.Parse(value);
                                //        break;
                                //    }
                                case "FixedAssetCategoryID":
                                    {
                                        string FixedAssetCategoryCode = "";
                                        try
                                        {
                                            FixedAssetCategoryCode = (xLCell.Value.ToString().Replace(" ", ""));
                                        }
                                        catch (Exception ex) { }
                                        if (FixedAssetCategoryCode.IsNullOrEmpty())
                                        {
                                            xLCell.Comment.AddText("Mã loại tài sản số định không được để trống");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        else
                                        {
                                            if (value.IsNullOrEmpty())
                                            {
                                                fixedAsset.FixedAssetCategoryID = Guid.Empty;
                                            }
                                            else
                                            {
                                                Guid id = Utils.IFixedAssetCategoryService.GetGuidFixedAssetCategoryByCode(FixedAssetCategoryCode) ?? Guid.Empty;
                                                if (id == null || id == Guid.Empty)
                                                {
                                                    xLCell.Comment.AddText("Loại tài sản cố định không tồn tại");
                                                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                    lstErrR.Add(xLCell);
                                                }
                                                else
                                                {
                                                    fixedAsset.FixedAssetCategoryID = id;
                                                }
                                            }
                                        }
                                        break;
                                    }
                                case "DepartmentID":
                                    {
                                        string DepartmentCode = "";
                                        try
                                        {
                                            DepartmentCode = (xLCell.Value.ToString().Replace(" ", ""));
                                        }
                                        catch (Exception ex) { }
                                        if (DepartmentCode.IsNullOrEmpty())
                                        {
                                            fixedAsset.DepartmentID = null;
                                        }
                                        else
                                        {
                                            Guid id = Utils.IDepartmentService.GetGuidDepartmentByCode(DepartmentCode) ?? Guid.Empty;
                                            if (id == null || id == Guid.Empty)
                                            {
                                                xLCell.Comment.AddText("Mã phòng ban không tồn tại");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                            {
                                                fixedAsset.DepartmentID = id;
                                            }
                                        }
                                        break;
                                    }
                                case "FixedAssetCode":
                                    {
                                        try
                                        {
                                            fixedAsset.FixedAssetCode = (xLCell.Value.ToString().Replace(" ", ""));
                                        }
                                        catch (Exception ex) { }
                                        if (value.IsNullOrEmpty())
                                        {


                                            xLCell.Comment.AddText("Mã tài sản cố định không được để trống!.");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        else
                                        {
                                            if (value.Length <= 25)
                                            {
                                                Guid id = Utils.IFixedAssetService.GetGuidByFixedAssetCode(value) ?? Guid.Empty;

                                                if (id != Guid.Empty)
                                                {
                                                    FixedAsset fa = Utils.IFixedAssetService.GetFixedAssetByCode(value);
                                                    fixedAsset = fa;
                                                    lstUpdateFixedAsset.Add(fixedAsset);
                                                }
                                                else
                                                {
                                                    fixedAsset.FixedAssetCode = value;
                                                }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Mã tài sản cố định không được quá 25 kí tự!.");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "FixedAssetName":
                                    {
                                        try
                                        {
                                            fixedAsset.FixedAssetName = (xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }

                                        if (value.IsNullOrEmpty())
                                        {
                                            xLCell.Comment.AddText("Tên tài sản cố định không được để trống!.");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        else
                                        {
                                            if (xLCell.Value.ToString().Length <= 512)
                                            {
                                                fixedAsset.FixedAssetName = value;
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Tên tài sản cố định không được quá 512 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }

                                case "Description":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 512)
                                            {
                                                try
                                                {
                                                    fixedAsset.Description = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Mô tả không được quá 512 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "ProductionYear":
                                    {
                                        try
                                        {
                                            fixedAsset.ProductionYear = int.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "MadeIn":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 100)
                                            {
                                                try
                                                {
                                                    fixedAsset.MadeIn = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Nơi sản xuất không được quá 100 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;

                                    }
                                case "SerialNumber":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 100)
                                            {
                                                try
                                                {
                                                    fixedAsset.SerialNumber = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Số sê ri không được quá 100 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }

                                case "AccountingObjectID":
                                    {
                                        string AccountingObjectCode = "";
                                        try
                                        {
                                            AccountingObjectCode = (xLCell.Value.ToString().Replace(" ", ""));
                                        }
                                        catch (Exception ex) { }
                                        if (AccountingObjectCode.IsNullOrEmpty())
                                        {
                                            fixedAsset.AccountingObjectID = null;
                                            fixedAsset.AccountingObjectName = null;
                                            fixedAsset.AccountingObjectAddress = null;
                                        }
                                        else
                                        {
                                            Guid id = Utils.IAccountingObjectService.GetGuidAccountingObjectByCode(AccountingObjectCode) ?? Guid.Empty;
                                            if (id == null || id == Guid.Empty)
                                            {

                                                xLCell.Comment.AddText("Đối tượng kế toán không tồn tại!.");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);


                                            }
                                            else
                                            {
                                                AccountingObject AO = Utils.IAccountingObjectService.GetAccountingObjectByCode(value);
                                                fixedAsset.AccountingObjectID = id;
                                                fixedAsset.AccountingObjectName = AO.AccountingObjectName;
                                                fixedAsset.AccountingObjectAddress = AO.Address;
                                            }
                                        }
                                        break;
                                    }


                                case "GuaranteeDuration":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 25)
                                            {
                                                try
                                                {
                                                    fixedAsset.GuaranteeDuration = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Thời gian bảo hành không được quá 25 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "GuaranteeCodition":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 512)
                                            {
                                                try
                                                {
                                                    fixedAsset.GuaranteeCodition = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Điều kiện bảo hành không được quá 512 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }

                                case "CurrentState":
                                    {
                                        try
                                        {
                                            fixedAsset.CurrentState = int.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }

                                case "PurchasedDate":
                                    {
                                        try
                                        {
                                            if (!xLCell.RichText.IsNullOrEmpty())
                                            {
                                                string[] arrListStr = xLCell.RichText.ToString().Split('/');
                                                string formatDate = string.Format("{0}/{1}/{2}", arrListStr[1], arrListStr[0], arrListStr[2]);
                                                fixedAsset.PurchasedDate = DateTime.Parse(formatDate);
                                            }
                                            else
                                            { fixedAsset.PurchasedDate = ((DateTime)xLCell.Value); }
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Ngày không hợp lệ!.");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                    }
                                case "UsedDate":
                                    {
                                        try
                                        {
                                            if (!xLCell.RichText.IsNullOrEmpty())
                                            {
                                                string[] arrListStr = xLCell.RichText.ToString().Split('/');
                                                string formatDate = string.Format("{0}/{1}/{2}", arrListStr[1], arrListStr[0], arrListStr[2]);
                                                fixedAsset.UsedDate = DateTime.Parse(formatDate);
                                            }
                                            else
                                            { fixedAsset.UsedDate = ((DateTime)xLCell.Value); }
                                            //fixedAsset.UsedDate = ((DateTime)xLCell.Value);
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Ngày không hợp lệ!.");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                    }
                                case "IncrementDate":
                                    {
                                        try
                                        {
                                            if (!xLCell.RichText.IsNullOrEmpty())
                                            {
                                                string[] arrListStr = xLCell.RichText.ToString().Split('/');
                                                string formatDate = string.Format("{0}/{1}/{2}", arrListStr[1], arrListStr[0], arrListStr[2]);
                                                fixedAsset.IncrementDate = DateTime.Parse(formatDate);
                                            }
                                            else
                                            { fixedAsset.IncrementDate = ((DateTime)xLCell.Value); }
                                            //fixedAsset.IncrementDate = ((DateTime)xLCell.Value);
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Ngày không hợp lệ!.");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                    }
                                case "DepreciationDate":
                                    {
                                        try
                                        {
                                            if (!xLCell.RichText.IsNullOrEmpty())
                                            {
                                                string[] arrListStr = xLCell.RichText.ToString().Split('/');
                                                string formatDate = string.Format("{0}/{1}/{2}", arrListStr[1], arrListStr[0], arrListStr[2]);
                                                fixedAsset.DepreciationDate = DateTime.Parse(formatDate);
                                            }
                                            else
                                            { fixedAsset.DepreciationDate = ((DateTime)xLCell.Value); }
                                            //fixedAsset.DepreciationDate = ((DateTime)xLCell.Value);
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Ngày không hợp lệ!.");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                    }
                                case "PurchasePrice":
                                    {
                                        try
                                        {
                                            fixedAsset.PurchasePrice = decimal.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "OriginalPrice":
                                    {
                                        try
                                        {
                                            fixedAsset.OriginalPrice = decimal.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }



                                case "UsedTime":
                                    {
                                        try
                                        {
                                            fixedAsset.UsedTime = decimal.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "DepreciationAccount":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (Utils.IAccountService.Query.Any(n => n.AccountNumber == xLCell.Value.ToString()))
                                            {
                                                try
                                                {
                                                    fixedAsset.DepreciationAccount = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Tài khoản không tồn tại");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "PeriodDepreciationAmount":
                                    {
                                        try
                                        {
                                            fixedAsset.PeriodDepreciationAmount = decimal.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "OriginalPriceAccount":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (Utils.IAccountService.Query.Any(n => n.AccountNumber == xLCell.Value.ToString()))
                                            {
                                                try
                                                {
                                                    fixedAsset.OriginalPriceAccount = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Tài khoản không tồn tại");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "DepreciationRate":
                                    {
                                        try
                                        {
                                            fixedAsset.DepreciationRate = decimal.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "ExpenditureAccount":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (Utils.IAccountService.Query.Any(n => n.AccountNumber == xLCell.Value.ToString()))
                                            {
                                                try
                                                {
                                                    fixedAsset.ExpenditureAccount = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Tài khoản không tồn tại");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }



                                case "CostSetID":
                                    {
                                        string CostSetCode = "";
                                        try
                                        {
                                            CostSetCode = (xLCell.Value.ToString().Replace(" ", ""));
                                        }
                                        catch (Exception ex) { }
                                        if (CostSetCode.IsNullOrEmpty())
                                        {
                                            fixedAsset.CostSetID = null;
                                        }
                                        else
                                        {
                                            Guid id = Utils.ICostSetService.GetGuidCostSetByCode(CostSetCode) ?? Guid.Empty;
                                            if (id == null || id == Guid.Empty)
                                            {

                                                xLCell.Comment.AddText("Đối tượng tập hợp chi phí không tồn tại!.");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                            {
                                                fixedAsset.CostSetID = id;
                                            }
                                        }
                                        break;
                                    }

                                case "MonthDepreciationRate":
                                    {
                                        try
                                        {
                                            fixedAsset.MonthDepreciationRate = decimal.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "MonthPeriodDepreciationAmount":
                                    {
                                        try
                                        {
                                            fixedAsset.MonthPeriodDepreciationAmount = decimal.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "DeliveryRecordNo":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 50)
                                            {
                                                try
                                                {
                                                    fixedAsset.DeliveryRecordNo = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("số biên bản giao nhận không được quá 50 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "DeliveryRecordDate":
                                    {
                                        try
                                        {
                                            fixedAsset.DeliveryRecordDate = ((DateTime)xLCell.Value);
                                        }
                                        catch (Exception ex)
                                        {
                                            //xLCell.Comment.AddText("Ngày không hợp lệ!.");
                                            //xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            //lstErrR.Add(xLCell);
                                        }
                                        break;
                                    }




                            }
                        }
                        lstFixedAsset.Add(fixedAsset);
                        //fixedAsset.ID = Guid.NewGuid();
                        //fixedAsset.BranchID = null;
                        //if (!(fixedAsset.FixedAssetCategoryID.ToString() == "" || fixedAsset.FixedAssetCode.IsNullOrEmpty() || fixedAsset.FixedAssetName.IsNullOrEmpty() ||
                        //    fixedAsset.IsSecondHand.ToString() == "" || fixedAsset.DisposedAmount.ToString() == "" || fixedAsset.IsDisplayMonth.ToString() == "" ||
                        //    fixedAsset.IsDepreciationAllocation.ToString() == "" || fixedAsset.IsCreateFromOldDatabase.ToString() == ""))
                        //{
                        //    Utils.IFixedAssetService.CreateNew(fixedAsset);
                        //}
                    }
                    //Utils.IFixedAssetService.CommitTran();
                }
                else if (tableName == "MaterialGoods")
                {
                    Utils.IMaterialGoodsService.BeginTran();
                    List<CategoryMapping> lstA = ((BindingList<CategoryMapping>)uGridMapping.DataSource).ToList();
                    if (lstA.Where(n => n.Status).Any(n => n.SheetColumnHeader == "" || n.SheetColumnHeader == null))
                    {
                        WaitingFrm.StopWaiting();
                        MSG.Error("Cột bắt buộc không được để trống");
                        return;
                    }
                    foreach (var item in nonEmptyDataRows.Where(n => n.RowNumber() > 1))
                    {
                        MaterialGoods materialGoods = new MaterialGoods();
                        for (int j = 1; j <= headers.Count(); j++)
                        {
                            string key = "";
                            UltraGridColumn column = uGridMapping.DisplayLayout.Bands[0].Columns[1];
                            CategoryMapping categoryMapping = lstA.FirstOrDefault(n => n.SheetColumnHeader == headers.ToList().FirstOrDefault(m => m.Value == j).Value.ToString());
                            if (categoryMapping != null)
                                key = dicCategory[categoryMapping.ColumnMapping];
                            IXLCell xLCell = item.Cell(j);
                            xLCell.MergedRange();
                            xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                            if (xLCell.HasComment)
                                xLCell.Comment.Delete();
                            string value = xLCell.Value.ToString().Replace(" ", "");
                            switch (key)
                            {
                                case "MaterialGoodsCategoryID":
                                    {
                                        string MaterialGoodsCategoryCode = "";
                                        try
                                        {
                                            MaterialGoodsCategoryCode = (xLCell.Value.ToString().Replace(" ", ""));
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Loại vật tư hàng hóa không tồn tại");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        if (MaterialGoodsCategoryCode.IsNullOrEmpty())
                                        {
                                            materialGoods.MaterialGoodsCategoryID = null;
                                        }
                                        else
                                        {
                                            Guid id = Utils.IMaterialGoodsCategoryService.GetGuidMaterialGoodsCategoryByCode(MaterialGoodsCategoryCode) ?? Guid.Empty;
                                            if (id == null || id == Guid.Empty)
                                            {
                                                xLCell.Comment.AddText("Loại vật tư hàng hóa không tồn tại");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                            {
                                                materialGoods.MaterialGoodsCategoryID = id;
                                            }
                                        }
                                        break;
                                    }
                                case "MaterialGoodsCode":
                                    {
                                        try
                                        {
                                            materialGoods.MaterialGoodsCode = (xLCell.Value.ToString().Replace(" ", ""));
                                            if (string.IsNullOrEmpty(xLCell.Value.ToString()))
                                            {
                                                xLCell.Comment.AddText("Mã VTHH không được để trống");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                            {
                                                if (xLCell.Value.ToString().Length <= 25)
                                                {

                                                    if (Utils.ListMaterialGoods.Any(n => n.MaterialGoodsCode.ToUpper().Equals(xLCell.Value.ToString().Replace(" ", "").ToUpper())))
                                                    {
                                                        xLCell.Comment.AddText("Mã VTHH đã tồn tại");
                                                        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                        lstErrR.Add(xLCell);
                                                    }
                                                    else if (lstMaterialGoods.Any(n => n.MaterialGoodsCode == materialGoods.MaterialGoodsCode))
                                                    {
                                                        xLCell.Comment.AddText("Mã VTHH bị trùng");
                                                        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                        lstErrR.Add(xLCell);
                                                    }
                                                }
                                                else
                                                {
                                                    xLCell.Comment.AddText("Mã vật tư hàng hóa không được quá 25 kí tự");
                                                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                    lstErrR.Add(xLCell);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Mã VTHH không được để trống");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                        //if (value.IsNullOrEmpty())
                                        //{
                                        //    MessageBox.Show("Mã vật tư hàng hóa không được để trống!.");
                                        //    return;
                                        //}
                                        //else
                                        //{
                                        //    materialGoods.MaterialGoodsCode = value;
                                        //}
                                        //break;
                                    }
                                case "MaterialGoodsName":
                                    {
                                        try
                                        {

                                            if (string.IsNullOrEmpty(xLCell.Value.ToString()))
                                            {
                                                xLCell.Comment.AddText("Tên VTHH không được để trống");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                            {
                                                if (xLCell.Value.ToString().Length <= 512)
                                                {
                                                    materialGoods.MaterialGoodsName = (xLCell.Value.ToString());
                                                }
                                                else
                                                {
                                                    xLCell.Comment.AddText("Tên VTHH không được quá 512 kí tự");
                                                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                    lstErrR.Add(xLCell);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Tên VTHH không được để trống");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        break;
                                        //if (value.IsNullOrEmpty())
                                        //{
                                        //    MessageBox.Show("Tên vật tư hàng hóa không được để trống!.");
                                        //    return;
                                        //}
                                        //else
                                        //{
                                        //    materialGoods.MaterialGoodsName = value;
                                        //}
                                        //break;
                                    }
                                case "MaterialGoodsType":
                                    {
                                        string t = "";
                                        try
                                        {
                                            if (string.IsNullOrEmpty(xLCell.Value.ToString()))
                                            {
                                                xLCell.Comment.AddText("Tính chất VTHH không được để trống");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            t = (xLCell.Value.ToString());
                                            if (t.ToUpper() == "Vật tư hàng hóa".ToUpper())
                                            {
                                                materialGoods.MaterialGoodsType = 0;
                                            }
                                            else if (t.ToUpper() == "VTHH lắp ráp/tháo dỡ".ToUpper())
                                            {
                                                materialGoods.MaterialGoodsType = 1;
                                            }
                                            else if (t.ToUpper() == "Dịch vụ".ToUpper())
                                            {
                                                materialGoods.MaterialGoodsType = 2;
                                            }
                                            else if (t.ToUpper() == "Thành phẩm".ToUpper())
                                            {
                                                materialGoods.MaterialGoodsType = 3;
                                            }
                                            else if (t.ToUpper() == "Chỉ là diễn giải".ToUpper())
                                            {
                                                materialGoods.MaterialGoodsType = 4;
                                            }
                                            else if (t.ToUpper() == "Khác".ToUpper())
                                            {
                                                materialGoods.MaterialGoodsType = 5;
                                            }
                                            else
                                            {
                                                int check = int.Parse(t);
                                                if (check >= 0 && check <= 5)
                                                {
                                                    materialGoods.MaterialGoodsType = check;
                                                }
                                                else
                                                {
                                                    xLCell.Comment.AddText("Tính chất VTHH không hợp lệ");
                                                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                    lstErrR.Add(xLCell);
                                                }
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Tính chất VTHH không hợp lệ");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }

                                        break;
                                    }
                                case "MaterialToolType":
                                    {
                                        string t = "";
                                        try
                                        {
                                            t = (xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        if (t.IsNullOrEmpty())
                                            materialGoods.MaterialToolType = 0;
                                        else
                                            materialGoods.MaterialToolType = int.Parse(t);
                                        break;
                                    }
                                case "Unit":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 25)
                                            {
                                                try
                                                {
                                                    materialGoods.Unit = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Đơn vị không được quá 25 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "ConvertUnit":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 25)
                                            {
                                                try
                                                {
                                                    materialGoods.ConvertUnit = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Đơn vị quy đổi không được quá 25 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "ConvertRate":
                                    {
                                        try
                                        {
                                            materialGoods.ConvertRate = decimal.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "PurchasePrice":
                                    {
                                        try
                                        {
                                            if (!IsNumber(xLCell.Value.ToString()))
                                            {
                                                xLCell.Comment.AddText("Đơn giá mua không hợp lệ");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                                materialGoods.PurchasePrice = decimal.Parse(xLCell.Value.ToString());

                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "SalePrice":
                                    {
                                        try
                                        {
                                            if (!IsNumber(xLCell.Value.ToString()))
                                            {
                                                xLCell.Comment.AddText("Đơn giá bán không hợp lệ");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                                materialGoods.SalePrice = decimal.Parse(xLCell.Value.ToString());

                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                        break;
                                    }
                                case "SalePrice2":
                                    {
                                        try
                                        {
                                            materialGoods.SalePrice2 = decimal.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "SalePrice3":
                                    {
                                        try
                                        {
                                            materialGoods.SalePrice3 = decimal.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "FixedSalePrice":
                                    {
                                        try
                                        {
                                            materialGoods.FixedSalePrice = decimal.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "SalePriceAfterTax":
                                    {
                                        string t = "";
                                        try
                                        {
                                            t = (xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        if (t.IsNullOrEmpty())
                                            materialGoods.SalePriceAfterTax = 0;
                                        else
                                            materialGoods.SalePriceAfterTax = Decimal.Parse(t);
                                        break;
                                    }
                                case "SalePriceAfterTax2":
                                    {
                                        string t = "";
                                        try
                                        {
                                            t = (xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        if (t.IsNullOrEmpty())
                                            materialGoods.SalePriceAfterTax2 = 0;
                                        else
                                            materialGoods.SalePriceAfterTax2 = Decimal.Parse(t);
                                        break;
                                    }
                                case "SalePriceAfterTax3":
                                    {
                                        string t = "";
                                        try
                                        {
                                            t = (xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        if (t.IsNullOrEmpty())
                                            materialGoods.SalePriceAfterTax3 = 0;
                                        else
                                            materialGoods.SalePriceAfterTax3 = Decimal.Parse(t);
                                        break;
                                    }
                                case "IsSalePriceAfterTax":
                                    {
                                        try
                                        {
                                            materialGoods.IsSalePriceAfterTax = ((xLCell.Value.ToString()) == "1") ? true : false;
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "RepositoryID":
                                    {
                                        string RepositoryCode = "";
                                        try
                                        {
                                            RepositoryCode = (xLCell.Value.ToString().Replace(" ", ""));
                                        }
                                        catch (Exception ex) { }
                                        if (RepositoryCode.IsNullOrEmpty())
                                        {
                                            materialGoods.RepositoryID = null;
                                        }
                                        else
                                        {
                                            Guid id = Utils.IRepositoryService.GetGuidRespositoryByCode(RepositoryCode) ?? Guid.Empty;
                                            if (id == null || id == Guid.Empty)
                                            {
                                                xLCell.Comment.AddText("Kho ngầm định không hợp lệ");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                            {
                                                materialGoods.RepositoryID = id;
                                            }
                                        }
                                        break;
                                    }
                                case "ReponsitoryAccount":
                                    {
                                        try
                                        {
                                            if (!xLCell.Value.ToString().StrIsNullOrEmpty())
                                            {

                                                if (list.ToList().OrderBy(p => p.AccountNumber).Where(p => p.IsActive == true).ToList().Any(n => n.AccountNumber == xLCell.Value.ToString()))
                                                {
                                                    materialGoods.ReponsitoryAccount = xLCell.Value.ToString();
                                                }
                                                else
                                                {
                                                    xLCell.Comment.AddText("Tài khoản kho không hợp lệ");
                                                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                    lstErrR.Add(xLCell);
                                                }
                                            }
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "ExpenseAccount":
                                    {
                                        try
                                        {

                                            if (!xLCell.Value.ToString().StrIsNullOrEmpty())
                                            {

                                                if (list2.ToList().OrderBy(p => p.AccountNumber).Where(p => p.IsActive == true).ToList().Any(n => n.AccountNumber == xLCell.Value.ToString()))
                                                {
                                                    materialGoods.ExpenseAccount = xLCell.Value.ToString();
                                                }
                                                else
                                                {
                                                    xLCell.Comment.AddText("Tài khoản chi phí không hợp lệ");
                                                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                    lstErrR.Add(xLCell);
                                                }
                                            }
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "RevenueAccount":
                                    {
                                        try
                                        {
                                            if (!xLCell.Value.ToString().StrIsNullOrEmpty())
                                            {

                                                if (list3.ToList().OrderBy(p => p.AccountNumber).Where(p => p.IsActive == true).ToList().Any(n => n.AccountNumber == xLCell.Value.ToString()))
                                                {
                                                    materialGoods.RevenueAccount = xLCell.Value.ToString();
                                                }
                                                else
                                                {
                                                    xLCell.Comment.AddText("Tài khoản doanh thu không hợp lệ");
                                                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                    lstErrR.Add(xLCell);
                                                }
                                            }
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "MinimumStock":
                                    {
                                        try
                                        {
                                            if (!IsNumber(xLCell.Value.ToString()))
                                            {
                                                xLCell.Comment.AddText("Số lượng tối thiểu không hợp lệ");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                                materialGoods.MinimumStock = decimal.Parse(xLCell.Value.ToString());

                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                        break;
                                    }
                                case "AccountingObjectID":
                                    {
                                        string AccountingObjectCode = "";
                                        try
                                        {
                                            AccountingObjectCode = (xLCell.Value.ToString().Replace(" ", ""));
                                        }
                                        catch (Exception ex) { }
                                        if (AccountingObjectCode.IsNullOrEmpty())
                                        {
                                            materialGoods.AccountingObjectID = null;
                                        }
                                        else
                                        {
                                            Guid id = Utils.IAccountingObjectService.GetGuidAccountingObjectByCode(AccountingObjectCode) ?? Guid.Empty;
                                            if (id == null || id == Guid.Empty)
                                            {
                                                MessageBox.Show("Đối tượng kế toán không tồn tại!.");
                                            }
                                            else
                                            {
                                                materialGoods.AccountingObjectID = id;
                                            }
                                        }
                                        break;
                                    }
                                case "TaxRate":
                                    {
                                        try
                                        {
                                            if (!xLCell.Value.ToString().IsNullOrEmpty())
                                            {
                                                if (xLCell.Value.ToString() == "KCT")
                                                {
                                                    materialGoods.TaxRate = -1;
                                                }
                                                else if (xLCell.Value.ToString() == "KTT")
                                                {
                                                    materialGoods.TaxRate = -2;
                                                }
                                                else
                                                {
                                                    if (decimal.Parse(xLCell.RichText.Text.ToString().Trim('%')) == 0 || decimal.Parse(xLCell.RichText.Text.ToString().Trim('%')) == 5 || decimal.Parse(xLCell.RichText.Text.ToString().Trim('%')) == 10)
                                                    {
                                                        materialGoods.TaxRate = decimal.Parse(xLCell.RichText.Text.ToString().Trim('%'));
                                                    }
                                                    else
                                                    {
                                                        xLCell.Comment.AddText("Thuế suất không hợp lệ");
                                                        xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                        lstErrR.Add(xLCell);
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                        break;
                                    }
                                case "SystemMaterialGoodsType":
                                    {
                                        try
                                        {
                                            materialGoods.SystemMaterialGoodsType = int.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "SaleDescription":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 512)
                                            {
                                                try
                                                {
                                                    materialGoods.SaleDescription = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Mô tả bán hàng không được quá 512 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "PurchaseDescription":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 512)
                                            {
                                                try
                                                {
                                                    materialGoods.PurchaseDescription = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Mô tả mua hàng không được quá 512 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "ItemSource":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 512)
                                            {
                                                try
                                                {
                                                    materialGoods.ItemSource = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Nguồn gốc không được quá 512 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "MaterialGoodsGSTID":
                                    {
                                        string MaterialGoodsGSTCode = "";
                                        try
                                        {
                                            MaterialGoodsGSTCode = (xLCell.Value.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            xLCell.Comment.AddText("Nhóm HHDV chịu thuế TTĐB không tồn tại");
                                            xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                            lstErrR.Add(xLCell);
                                        }
                                        if (MaterialGoodsGSTCode.IsNullOrEmpty())
                                        {
                                            materialGoods.MaterialGoodsGSTID = null;
                                        }
                                        else
                                        {
                                            Guid id = Utils.IMaterialGoodsSpecialTaxGroupService.GetGuidMaterialGoodsSpecialTaxGroupByCode(MaterialGoodsGSTCode);
                                            if (id == null || id == Guid.Empty)
                                            {
                                                xLCell.Comment.AddText("Nhóm HHDV chịu thuế TTĐB không tồn tại");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                            {
                                                materialGoods.MaterialGoodsGSTID = id;
                                            }
                                        }
                                        break;
                                    }
                                case "SaleDiscountRate":
                                    {
                                        try
                                        {
                                            if (!IsNumber(xLCell.Value.ToString()))
                                            {
                                                xLCell.Comment.AddText("Chiết khấu bán hàng không hợp lệ");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                                materialGoods.SaleDiscountRate = decimal.Parse(xLCell.Value.ToString());

                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                        break;
                                    }
                                case "PurchaseDiscountRate":
                                    {
                                        try
                                        {
                                            if (!IsNumber(xLCell.Value.ToString()))
                                            {
                                                xLCell.Comment.AddText("Chiết khấu mua hàng không hợp lệ");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                            else
                                                materialGoods.PurchaseDiscountRate = decimal.Parse(xLCell.Value.ToString());


                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                        break;
                                    }
                                case "IsSaleDiscountPolicy":
                                    {
                                        try
                                        {
                                            materialGoods.IsSaleDiscountPolicy = (xLCell.Value.ToString() == "1") ? true : false;
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "GuarantyPeriod":
                                    {
                                        if (!string.IsNullOrEmpty(xLCell.Value.ToString()))
                                        {
                                            if (xLCell.Value.ToString().Length <= 512)
                                            {
                                                try
                                                {
                                                    materialGoods.GuarantyPeriod = (xLCell.Value.ToString());
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                xLCell.Comment.AddText("Thời gian bảo hành không được quá 512 kí tự");
                                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                                lstErrR.Add(xLCell);
                                            }
                                        }
                                        break;
                                    }
                                case "CostMethod":
                                    {
                                        try
                                        {
                                            materialGoods.CostMethod = int.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "IsActive":
                                    {
                                        try
                                        {
                                            materialGoods.IsActive = (xLCell.Value.ToString() == "1") ? true : false;
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "IsSecurity":
                                    {
                                        try
                                        {
                                            materialGoods.IsSecurity = (xLCell.Value.ToString() == "1") ? true : false;
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "PrintMetarial":
                                    {
                                        try
                                        {
                                            materialGoods.PrintMetarial = (xLCell.Value.ToString() == "1") ? true : false;
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "LastPurchasePriceAfterTax":
                                    {
                                        try
                                        {
                                            materialGoods.LastPurchasePriceAfterTax = decimal.Parse(xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        break;
                                    }
                                case "WarrantyTime":
                                    {
                                        try
                                        {
                                            int time = Utils.IWarrantyService.Query.FirstOrDefault(n => n.WarrantyName == xLCell.Value.ToString()).WarrantyTime;
                                            if (time == 1 || time == 2 || time == 3 || time == 4 || time == 5 || time == 6 || time == 7 || time == 8 || time == 9 || time == 10 || time == 11 || time == 12 ||
                                             time == 18 || time == 24 || time == 36 || time == 48 || time == 60 || time == 72 || time == 84 || time == 96 || time == 108 || time == 120)
                                                materialGoods.WarrantyTime = xLCell.Value.ToString();
                                            else
                                            {

                                            }

                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                        break;
                                    }
                                case "Quantity":
                                    {
                                        string t = "";
                                        try
                                        {
                                            t = (xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        if (t.IsNullOrEmpty())
                                            materialGoods.Quantity = 0;
                                        else
                                            materialGoods.Quantity = Decimal.Parse(t);
                                        break;
                                    }
                                case "UnitPrice":
                                    {
                                        string t = "";
                                        try
                                        {
                                            t = (xLCell.Value.ToString());
                                        }
                                        catch (Exception ex) { }
                                        if (t.IsNullOrEmpty())
                                            materialGoods.UnitPrice = 0;
                                        else
                                            materialGoods.UnitPrice = Decimal.Parse(t);
                                        break;
                                    }
                                    //case "Amount":
                                    //    {
                                    //        materialGoods.Amount = Decimal.Parse(value);
                                    //        break;
                                    //    }
                                    //case "AllocationTimes": 
                                    //    {
                                    //        materialGoods.AllocationTimes = int.Parse(value);
                                    //        break;
                                    //    }
                                    //case "AllocatedAmount": 
                                    //    {
                                    //        materialGoods.AllocatedAmount = Decimal.Parse(value);
                                    //        break;
                                    //    }
                                    //case "AllocationAccount":
                                    //    {
                                    //        materialGoods.AllocationAccount = value;
                                    //        break;
                                    //    }
                                    //case "CostSetID":
                                    //    {
                                    //        Guid id = (Guid)Utils.ICostSetService.GetGuidCostSetByCode(value);
                                    //        if (id == null || id == Guid.Empty)
                                    //        {
                                    //            MessageBox.Show("Mục thu/chi không tồn tại!.");
                                    //        }
                                    //        else
                                    //        {
                                    //            materialGoods.CostSetID = id;
                                    //        }
                                    //        break;
                                    //    }
                                    //case "AllocationType":
                                    //    {
                                    //        materialGoods.AllocationType = int.Parse(value);
                                    //        break;
                                    //    }
                                    //case "AllocationAwaitAccount":
                                    //    {
                                    //        materialGoods.AllocationAwaitAccount = value;
                                    //        break;
                                    //    }
                            }

                        }
                        lstMaterialGoods.Add(materialGoods);
                        //materialGoods.ID = Guid.NewGuid();
                        //if (!(materialGoods.MaterialGoodsCode.IsNullOrEmpty() || materialGoods.MaterialGoodsName.IsNullOrEmpty()))
                        //{
                        //    Utils.IMaterialGoodsService.CreateNew(materialGoods);
                        //}
                    }
                    //Utils.IMaterialGoodsService.CommitTran();
                }
                WaitingFrm.StopWaiting();

                if (lstTIInit.Count > 0)
                {
                    if (lstErrR.Count == 0)
                    {
                        string query = "";
                        try
                        {

                            foreach (var item in lstTIInit)
                            {
                                item.ID = Guid.NewGuid();
                                item.AllocatedAmount = item.Amount / item.AllocationTimes;
                                query += " " + string.Format("INSERT INTO TIInit(" +
                                    "ID, " +
                                    "ToolsCode, " +
                                    "ToolsName, " +
                                    "Unit, " +
                                    "Quantity, " +
                                    "UnitPrice, " +
                                    "Amount, " +
                                    "AllocationType, " +
                                    "AllocationTimes, " +
                                    "AllocatedAmount, " +
                                    "AllocationAwaitAccount,IsActive,DeclareType) " +
                                    "VALUES ('{0}',N'{1}',{2},{3},{4},{5},{6},{7},{8},{9},{10},0,0)",
                                    item.ID, item.ToolsCode.Replace("'", "'+CHAR(39)+N'"),
                                    item.ToolsName == null ? "NULL" : "N'" + item.ToolsName.Replace("'", "'+CHAR(39)+N'") + "'",
                                    item.Unit == null ? "NULL" : "N'" + item.Unit.Replace("'", "'+CHAR(39)+N'") + "'",
                                    item.Quantity == null ? "NULL" : "'" + item.Quantity + "'",
                                    "'" + item.UnitPrice + "'",
                                     "'" + item.Amount + "'",
                                    item.AllocationType == null ? "NULL" : item.AllocationType.ToString(),
                                    item.AllocationTimes == null ? "NULL" : item.AllocationTimes.ToString(),
                                     item.AllocatedAmount == null ? "NULL" : "'" + item.AllocatedAmount.ToString().Replace(",", ".") + "'",
                                     item.AllocationAwaitAccount == null ? "NULL" : "N" + "'" + item.AllocationAwaitAccount.Replace("'", "'+CHAR(39)+N'") + "'"
                                     );
                                foreach (var item1 in item.TIInitDetails)
                                {
                                    item1.ID = Guid.NewGuid();
                                    item1.TIInitID = item.ID;
                                    query += " " + string.Format("INSERT INTO TIInitDetail(" +
                                    "ID, " +
                                    "TIInitID, " +
                                    "ObjectID, " +
                                    "ObjectType, " +
                                    "Quantity, " +
                                    "Rate, " +
                                    "CostAccount, " +
                                    "ExpenseItemID) " +
                                    "VALUES ('{0}','{1}','{2}',{3},{4},{5},{6},{7})",
                                    item1.ID, item1.TIInitID,
                                    item1.ObjectID,
                                    item1.ObjectType.ToString(),
                                    item1.Quantity == null ? "NULL" : "'" + item1.Quantity + "'",
                                    item1.Rate == null ? "NULL" : "'" + item1.Rate + "'",
                                     item1.CostAccount == null ? "NULL" : "N'" + item1.CostAccount.Replace("'", "'+CHAR(39)+N'") + "'",
                                    item1.ExpenseItemID == null ? "NULL" : "'" + item1.ExpenseItemID.ToString() + "'"
                                    );
                                }
                            }

                            if (!ExecQueries(query))
                            {
                                WaitingFrm.StopWaiting();
                                MSG.Error("Thêm dữ liệu không thành công");
                                return;
                            }
                            Utils.ITIInitService.RolbackTran();
                            Utils.ClearCacheByType<TIInit>();
                            //Utils.ClearCacheByType<MaterialGoodsCustom>();
                            //Utils.IMaterialGoodsService.UnbindSession(Utils.ListMaterialGoods);
                            //var a = Utils.ListMaterialGoods;
                            WaitingFrm.StopWaiting();
                            MSG.Information("Thêm dữ liệu thành công");
                            closeForm = true;
                            this.Close();
                        }
                        catch (Exception ex)
                        {
                            Utils.ITIInitService.RolbackTran();
                            WaitingFrm.StopWaiting();
                        }
                    }
                    else
                    {
                        if (MSG.Question("Dữ liệu file không đúng. Bạn có muốn tải file lỗi về không?") == System.Windows.Forms.DialogResult.Yes)
                        {
                            System.Windows.Forms.SaveFileDialog sf = new System.Windows.Forms.SaveFileDialog
                            {
                                FileName = "FileExcelError.xlsx",
                                AddExtension = true,
                                Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
                            };
                            sf.Title = "Chọn thư mục lưu file";
                            if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                //XLWorkbook xLWorkbook = new XLWorkbook();
                                //xLWorkbook.AddWorksheet(ws);
                                try
                                {
                                    wb.SaveAs(sf.FileName);
                                    //xLWorkbook.SaveAs(sf.FileName);
                                    //xLWorkbook.Dispose();
                                }
                                catch
                                {
                                    MSG.Error("Lỗi khi lưu");
                                }
                                if (MSG.Question("Tải xuống thành công, Bạn có muốn mở file vừa tải") == System.Windows.Forms.DialogResult.Yes)
                                {
                                    try
                                    {
                                        System.Diagnostics.Process.Start(sf.FileName);
                                    }
                                    catch
                                    {
                                    }

                                }
                            }
                            else
                            {
                            }
                        }
                        else
                            MSG.Error("Thêm dữ liệu không thành công");
                    }
                }
                if (lstAccountingObject.Count > 0 || lstAccountingObjectUpdate.Count > 0)
                {
                    if (lstErrR.Count == 0)
                    {
                        if (lstAccountingObject.Count > 0 && lstAccountingObjectUpdate.Count == 0)
                        {
                            //Utils.IAccountingObjectService.BeginTran();
                            //foreach (var item in lstAccountingObject)
                            //{
                            //    item.ID = Guid.NewGuid();
                            //    AccountingObjectBankAccount accountingObjectBankAccount = new AccountingObjectBankAccount();
                            //accountingObjectBankAccount.AccountingObjectID = item.ID;
                            //accountingObjectBankAccount.BankName = item.BankName;
                            //accountingObjectBankAccount.BankAccount = item.BankAccount;
                            //item.BankAccounts.Add(accountingObjectBankAccount);
                            //    Utils.IAccountingObjectService.CreateNew(item);
                            //}
                            #region SQL
                            string query = "";
                            try
                            {
                                //query += "Set IDENTITY_INSERT AccountingObjectBankAccount on";
                                var count = lstAccountingObject.GroupBy(n => n.AccountingObjectCode);
                                foreach (var item in lstAccountingObject)
                                {
                                    item.IsActive = true;
                                    item.ID = Guid.NewGuid();
                                    AccountingObjectBankAccount accountingObjectBankAccount = new AccountingObjectBankAccount();
                                    if (!item.BankName.IsNullOrEmpty() || !item.BankAccount.IsNullOrEmpty())
                                    {
                                        accountingObjectBankAccount.ID = Guid.NewGuid();
                                        accountingObjectBankAccount.AccountingObjectID = item.ID;
                                        accountingObjectBankAccount.BankName = item.BankName;
                                        accountingObjectBankAccount.BankAccount = item.BankAccount;
                                        item.BankAccounts.Add(accountingObjectBankAccount);
                                    }
                                    query += " " + string.Format("INSERT INTO AccountingObject(" +
                                        "ID, " +
                                        "AccountingObjectCode, " +
                                        "AccountingObjectName, " +
                                        "EmployeeBirthday, " +
                                        "Address, " +
                                        "Tel, " +
                                        "Email, " +
                                        "Website, " +
                                        "BankAccount, " +
                                        "BankName, " +
                                        "TaxCode, " +
                                        "ContactTitle, " +
                                        "ContactSex, " +
                                        "ObjectType, " +
                                        "IdentificationNo, " +
                                        "IssueDate, " +
                                        "IssueBy, " +
                                        "DepartmentID, " +
                                        "IsInsured, " +
                                        "IsLabourUnionFree, " +
                                        "IsActive, " +
                                        "NumberOfDependent," +
                                        " AgreementSalary, " +
                                        "InsuranceSalary, " +
                                        "SalaryCoefficient, " +
                                        "IsEmployee, " +
                                        "ScaleType," +
                                        "Fax," +
                                        "ContactEmail) VALUES ('{0}',N'{1}',N'{2}',{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28});",
                                        item.ID,
                                        item.AccountingObjectCode.Replace("'", "'+CHAR(39)+N'"),
                                        item.AccountingObjectName.Replace("'", "'+CHAR(39)+N'"),
                                        item.EmployeeBirthday == null ? "NULL" : "'" + item.EmployeeBirthday.ToString() + "'",
                                        item.Address == null ? "NULL" : "N" + "'" + item.Address.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.Tel == null ? "NULL" : "N" + "'" + item.Tel.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.Email == null ? "NULL" : "N" + "'" + item.Email.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.Website == null ? "NULL" : "N" + "'" + item.Website.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.BankAccount == null ? "NULL" : "N" + "'" + item.BankAccount.Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.BankName == null ? "NULL" : "N" + "'" + item.BankName.Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.TaxCode == null ? "NULL" : "N" + "'" + item.TaxCode.Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.ContactTitle == null ? "NULL" : "N" + "'" + item.ContactTitle.Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.ContactSex == null ? "NULL" : "'" + "" + item.ContactSex + "'",
                                        item.ObjectType == null ? "NULL" : "'" + "" + item.ObjectType + "'",
                                        item.IdentificationNo == null ? "NULL" : "N" + "'" + item.IdentificationNo.Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.IssueDate == null ? "NULL" : "'" + "" + item.IssueDate + "'",
                                        item.IssueBy == null ? "NULL" : "N" + "'" + item.IssueBy.Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.DepartmentID == null ? "NULL" : "" + "'" + item.DepartmentID + "'",
                                        item.IsInsured == null ? "NULL" : "'" + (item.IsInsured == true ? "1" : "0") + "'",
                                        item.IsLabourUnionFree == null ? "NULL" : "'" + (item.IsLabourUnionFree == true ? "1" : "0") + "'",
                                        item.IsActive == null ? "NULL" : "'" + (item.IsActive == true ? "1" : "0") + "'",
                                        item.NumberOfDependent == null ? "NULL" : "'" + "" + item.NumberOfDependent + "'",
                                        item.AgreementSalary == null ? "NULL" : "" + "'" + item.AgreementSalary.ToString().Replace(',', '.') + "'",
                                        item.InsuranceSalary == null ? "NULL" : "" + "'" + item.InsuranceSalary.ToString().Replace(',', '.') + "'",
                                        item.SalaryCoefficient == null ? "NULL" : "" + "'" + item.SalaryCoefficient.ToString().Replace(',', '.') + "'",
                                        item.IsEmployee == null ? "NULL" : "'" + (item.IsEmployee == true ? "1" : "0") + "'",
                                        item.ScaleType == null ? "NULL" : "'" + "" + item.ScaleType + "'",
                                        item.Fax == null ? "NULL" : "'" + "" + item.Fax.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.ContactEmail == null ? "NULL" : "N" + "'" + item.ContactEmail.ToString().Replace("'", "'+CHAR(39)+N'") + "'"
                                        );
                                    if (!item.BankName.IsNullOrEmpty() || !item.BankAccount.IsNullOrEmpty())
                                    {
                                        query += " " + string.Format("INSERT INTO AccountingObjectBankAccount(" +
                                            "ID," +
                                            "AccountingObjectID," +
                                            "BankName," +
                                            "BankAccount," +
                                            "IsSelect) VALUES ('{0}',N'{1}',N'{2}',N'{3}','{4}');",
                                            accountingObjectBankAccount.ID,
                                            accountingObjectBankAccount.AccountingObjectID == null ? "NULL" : accountingObjectBankAccount.AccountingObjectID.ToString(),
                                            accountingObjectBankAccount.BankName == null ? "NULL" : accountingObjectBankAccount.BankName.ToString().Replace("'", "'+CHAR(39)+N'"),
                                            accountingObjectBankAccount.BankAccount == null ? "NULL" : accountingObjectBankAccount.BankAccount.ToString().Replace("'", "'+CHAR(39)+N'"),
                                            "1");
                                    }

                                }
                                //query += " Set IDENTITY_INSERT AccountingObjectBankAccount off";

                                //Utils.ISABillService.CommitTran();
                                if (!ExecQueries(query))
                                {
                                    WaitingFrm.StopWaiting();
                                    MSG.Error("Thêm dữ liệu không thành công");
                                    return;
                                }
                                //var t = Utils.IAccountingObjectService.Query.ToList();
                                //Utils.IAccountingObjectService.UnbindSession(Utils.ListAccountingObject);
                                Utils.IAccountingObjectService.RolbackTran();
                                Utils.IAccountingObjectBankAccountService.RolbackTran();
                                Utils.ClearCacheByType<AccountingObject>();
                                //Utils.IAccountingObjectBankAccountService.UnbindSession(Utils.ListAccountingObjectBank);
                                Utils.ClearCacheByType<AccountingObjectBankAccount>();
                                List<AccountingObject> lstP = lstAccountingObject.Where(n => n.ObjectType == 0 || n.ObjectType == 2).ToList();
                                if (lstP.Count > 0)
                                {
                                    Post_Customers(lstP);
                                }
                                WaitingFrm.StopWaiting();
                                MSG.Information("Thêm dữ liệu thành công");
                                closeForm = true;


                            }
                            catch (Exception ex)
                            {
                                Utils.ISABillService.RolbackTran();
                                WaitingFrm.StopWaiting();
                            }
                            #endregion
                            //Utils.IAccountingObjectService.CommitTran();

                            //Utils.IAccountingObjectService.UnbindSession(Utils.ListAccountingObject);
                            //Utils.ClearCacheByType<AccountingObject>();
                            //MSG.Information("Import thành công");
                            //this.Close();
                        }
                        {
                            Utils.ISABillService.RolbackTran();
                            WaitingFrm.StopWaiting();
                        }
                        if (lstAccountingObjectUpdate.Count > 0)
                        {
                            new FWarningImportUpdateData("Dữ liệu trong file import có mã đối tượng đã tồn tại trên phần mềm. Vui lòng lựa chọn 1 trong 2 tùy chọn sau: ", lstAccountingObjectUpdate, lstAccountingObject, wb).ShowDialog();
                        }
                        this.Close();
                    }
                    else
                    {
                        if (MSG.Question("Dữ liệu file không đúng. Bạn có muốn tải file lỗi về không?") == System.Windows.Forms.DialogResult.Yes)
                        {
                            System.Windows.Forms.SaveFileDialog sf = new System.Windows.Forms.SaveFileDialog
                            {
                                FileName = "FileExcelError.xlsx",
                                AddExtension = true,
                                Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
                            };
                            sf.Title = "Chọn thư mục lưu file";
                            if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                //XLWorkbook xLWorkbook = new XLWorkbook();
                                //xLWorkbook.AddWorksheet(ws);
                                try
                                {
                                    wb.SaveAs(sf.FileName);
                                    //xLWorkbook.SaveAs(sf.FileName);
                                    //xLWorkbook.Dispose();
                                }
                                catch
                                {
                                    MSG.Error("Lỗi khi lưu");
                                }
                                if (MSG.Question("Tải xuống thành công, Bạn có muốn mở file vừa tải") == System.Windows.Forms.DialogResult.Yes)
                                {
                                    try
                                    {
                                        System.Diagnostics.Process.Start(sf.FileName);
                                    }
                                    catch
                                    {
                                    }

                                }
                            }
                            else
                            {
                            }
                        }
                        else
                            MSG.Error("Thêm dữ liệu không thành công");
                    }
                }
                else
                {
                    if (cbbDanhMuc.Value.ToString() == "AccountingObject1" || cbbDanhMuc.Value.ToString() == "AccountingObject2" || cbbDanhMuc.Value.ToString() == "AccountingObject3")
                    {
                        WaitingFrm.StopWaiting();
                        MSG.Error("Dữ liệu file trống");
                        return;
                    }
                }
                if (lstMaterialGoods.Count > 0)
                {
                    if (lstErrR.Count == 0)
                    {
                        //Utils.IMaterialGoodsService.BeginTran();
                        //foreach (var item in lstMaterialGoods)
                        //{
                        //    item.ID = Guid.NewGuid();
                        //    Utils.IMaterialGoodsService.CreateNew(item);
                        //}
                        //Utils.IMaterialGoodsService.CommitTran();


                        //Utils.IMaterialGoodsService.UnbindSession(Utils.ListMaterialGoods);
                        //Utils.ClearCacheByType<MaterialGoods>();
                        //MSG.Information("Import thành công");
                        //this.Close();

                        #region SQL
                        string query = "";
                        try
                        {
                            foreach (var item in lstMaterialGoods)
                            {
                                item.ID = Guid.NewGuid();
                                query += " " + string.Format("INSERT INTO MaterialGoods(" +
                                    "ID, " +
                                    "MaterialGoodsCode, " +
                                    "MaterialGoodsName, " +
                                    "MaterialGoodsType, " +
                                    "Unit, " +
                                    "RepositoryID, " +
                                    "ReponsitoryAccount, " +
                                    "TaxRate, " +
                                    "ExpenseAccount, " +
                                    "RevenueAccount, " +
                                    "MaterialToolType, " +
                                    "MaterialGoodsGSTID, " +
                                    "PurchasePrice, " +
                                    "SalePrice, " +
                                    "MinimumStock," +
                                    "SaleDiscountRate, " +
                                    "PurchaseDiscountRate, " +
                                    "WarrantyTime, " +
                                    "ItemSource, " +
                                    "MaterialGoodsCategoryID, " +
                                    "ConvertRate," +
                                    "SalePrice2, " +
                                    "SalePrice3, " +
                                    "FixedSalePrice, " +
                                    "SalePriceAfterTax, " +
                                    "SalePriceAfterTax2, " +
                                    "SalePriceAfterTax3," +
                                    "IsSalePriceAfterTax, " +
                                    "IsSaleDiscountPolicy, " +
                                    "IsActive," +
                                    "IsSecurity," +
                                    "PrintMetarial," +
                                    "Quantity," +
                                    "UnitPrice," +
                                    "Amount," +
                                    "AllocationTimes," +
                                    "AllocatedAmount," +
                                    "AllocationType) " +
                                    "VALUES ('{0}',N'{1}',N'{2}',{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0)",
                                    item.ID,
                                    item.MaterialGoodsCode.Replace("'", "'+CHAR(39)+N'"),
                                    item.MaterialGoodsName.Replace("'", "'+CHAR(39)+N'"),
                                    item.MaterialGoodsType == null ? "NULL" : "'" + item.MaterialGoodsType.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                    item.Unit == null ? "NULL" : "N" + "'" + item.Unit.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                    item.RepositoryID == null ? "NULL" : "'" + item.RepositoryID.ToString() + "'",
                                    item.ReponsitoryAccount == null ? "NULL" : "N" + "'" + item.ReponsitoryAccount.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                    item.TaxRate == null ? "NULL" : "'" + item.TaxRate.ToString().Replace(",", ".") + "'",
                                    item.ExpenseAccount == null ? "NULL" : "N" + "'" + item.ExpenseAccount.Replace("'", "'+CHAR(39)+N'") + "'",
                                    item.RevenueAccount == null ? "NULL" : "N" + "'" + item.RevenueAccount.Replace("'", "'+CHAR(39)+N'") + "'",
                                    0,
                                    item.MaterialGoodsGSTID == null ? "NULL" : "'" + item.MaterialGoodsGSTID.ToString() + "'",
                                    item.PurchasePrice == null ? "NULL" : "'" + item.PurchasePrice + "'",
                                    item.SalePrice == null ? "NULL" : "'" + item.SalePrice + "'",
                                    item.MinimumStock == null ? "NULL" : "'" + item.MinimumStock + "'",
                                    item.SaleDiscountRate == null ? "NULL" : "'" + item.SaleDiscountRate + "'",
                                    item.PurchaseDiscountRate == null ? "NULL" : "'" + item.PurchaseDiscountRate + "'",
                                    item.WarrantyTime == null ? "NULL" : "N" + "'" + item.WarrantyTime.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                    item.ItemSource == null ? "NULL" : "N" + "'" + item.ItemSource.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                    item.MaterialGoodsCategoryID == null ? "NULL" : "'" + item.MaterialGoodsCategoryID.ToString() + "'"
                                    );
                            }
                            if (!ExecQueries(query))
                            {
                                WaitingFrm.StopWaiting();
                                MSG.Error("Thêm dữ liệu không thành công");
                                return;
                            }
                            Utils.IMaterialGoodsService.RolbackTran();
                            Utils.ClearCacheByType<MaterialGoods>();
                            Utils.ClearCacheByType<MaterialGoodsCustom>();
                            //Utils.IMaterialGoodsService.UnbindSession(Utils.ListMaterialGoods);
                            //var a = Utils.ListMaterialGoods;
                            WaitingFrm.StopWaiting();
                            MSG.Information("Thêm dữ liệu thành công");
                            closeForm = true;
                            this.Close();
                        }
                        catch (Exception ex)
                        {
                            Utils.ISABillService.RolbackTran();
                            WaitingFrm.StopWaiting();
                        }
                        #endregion

                    }
                    else
                    {
                        if (MSG.Question("Dữ liệu file không đúng. Bạn có muốn tải file lỗi về không?") == System.Windows.Forms.DialogResult.Yes)
                        {
                            System.Windows.Forms.SaveFileDialog sf = new System.Windows.Forms.SaveFileDialog
                            {
                                FileName = "FileExcelError.xlsx",
                                AddExtension = true,
                                Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
                            };
                            sf.Title = "Chọn thư mục lưu file";
                            if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                try
                                {
                                    wb.SaveAs(sf.FileName);
                                }
                                catch
                                {
                                    MSG.Error("Lỗi khi lưu");
                                }
                                if (MSG.Question("Tải xuống thành công, Bạn có muốn mở file vừa tải") == System.Windows.Forms.DialogResult.Yes)
                                {
                                    try
                                    {
                                        System.Diagnostics.Process.Start(sf.FileName);
                                    }
                                    catch
                                    {
                                    }
                                }
                            }
                            else
                            {
                            }
                        }
                        else
                            MSG.Error("Import không thành công");
                    }
                }
                else
                {
                    if (cbbDanhMuc.Value.ToString() == "MaterialGoods")
                    {
                        WaitingFrm.StopWaiting();
                        MSG.Error("Dữ liệu file trống");
                        return;
                    }
                }
                if (lstFixedAsset.Count > 0)
                {
                    if (lstErrR.Count == 0)
                    {
                        //Utils.IMaterialGoodsService.BeginTran();
                        //foreach (var item in lstMaterialGoods)
                        //{
                        //    item.ID = Guid.NewGuid();
                        //    Utils.IMaterialGoodsService.CreateNew(item);
                        //}
                        //Utils.IMaterialGoodsService.CommitTran();


                        //Utils.IMaterialGoodsService.UnbindSession(Utils.ListMaterialGoods);
                        //Utils.ClearCacheByType<MaterialGoods>();
                        //MSG.Information("Import thành công");
                        //this.Close();

                        #region SQL
                        string query = "";
                        try
                        {

                            foreach (var item in lstFixedAsset)
                            {
                                if (lstUpdateFixedAsset.Where(n => n.FixedAssetCode == item.FixedAssetCode).Count<FixedAsset>() != 0)
                                {
                                    query += " " + string.Format("UPDATE FixedAsset SET " +
                                        "FixedAssetName={0}," +
                                        "FixedAssetCategoryID='{1}', " +
                                        "SerialNumber={2}, " +
                                        "DepartmentID={3}, " +
                                        "PurchasedDate={4}, " +
                                        "IncrementDate={5}, " +
                                        "UsedDate={6}, " +
                                        "DepreciationDate={7}, " +
                                        "OriginalPriceAccount={8}," +
                                        "DepreciationAccount={9}, " +
                                        "ExpenditureAccount={10}, " +
                                        "CostSetID={11}, " +
                                        "OriginalPrice={12}, " +
                                        "PurchasePrice={13}, " +
                                        "UsedTime={14}, " +
                                        "DepreciationRate={15}," +
                                        "PeriodDepreciationAmount={16}, " +
                                        "MonthDepreciationRate={17}, " +
                                        "MonthPeriodDepreciationAmount={18}, " +
                                        "ProductionYear={19}, " +
                                        "MadeIn={20}, " +
                                        "GuaranteeDuration={21}, " +
                                        "GuaranteeCodition={22}, " +
                                        "Description={23}," +
                                        "AccountingObjectID={24}," +
                                        "AccountingObjectName={25}," +
                                        "AccountingObjectAddress={26}, " +
                                        "CurrentState={27}, " +
                                        "DeliveryRecordNo={28}, " +
                                        "DeliveryRecordDate={29} WHERE FixedAssetCode ={30}"
                                        , item.FixedAssetName == null ? "NULL" : "N" + "'" + item.FixedAssetName.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.FixedAssetCategoryID,
                                        item.SerialNumber == null ? "NULL" : "N" + "'" + item.SerialNumber.Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.DepartmentID == null ? "NULL" : "'" + item.DepartmentID + "'",
                                        item.PurchasedDate == null ? "NULL" : "'" + item.PurchasedDate.ToString() + "'",
                                        item.IncrementDate == null ? "NULL" : "'" + item.IncrementDate.ToString() + "'",
                                        item.UsedDate == null ? "NULL" : "'" + item.UsedDate.ToString() + "'",
                                        item.DepreciationDate == null ? "NULL" : "'" + item.DepreciationDate.ToString() + "'",
                                        item.OriginalPriceAccount == null ? "NULL" : "N'" + item.OriginalPriceAccount.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.DepreciationAccount == null ? "NULL" : "N'" + item.DepreciationAccount.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.ExpenditureAccount == null ? "NULL" : "N'" + item.ExpenditureAccount.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.CostSetID == null ? "NULL" : "'" + item.CostSetID.ToString() + "'",
                                        item.OriginalPrice == null ? "NULL" : "'" + item.OriginalPrice + "'",
                                        item.PurchasePrice == null ? "NULL" : "'" + item.PurchasePrice + "'",
                                        item.UsedTime == null ? "NULL" : "" + "'" + item.UsedTime.ToString().Replace(',', '.') + "'",
                                        item.DepreciationRate == null ? "NULL" : "" + "'" + item.DepreciationRate.ToString().Replace(',', '.') + "'",
                                        item.PeriodDepreciationAmount == null ? "NULL" : "'" + item.PeriodDepreciationAmount + "'",
                                        item.MonthDepreciationRate == null ? "NULL" : "" + "'" + item.MonthDepreciationRate.ToString().Replace(',', '.') + "'",
                                        item.MonthPeriodDepreciationAmount == null ? "NULL" : "'" + item.MonthPeriodDepreciationAmount + "'",
                                        item.ProductionYear == null ? "NULL" : item.ProductionYear.ToString(),
                                        item.MadeIn == null ? "NULL" : "N'" + item.MadeIn.Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.GuaranteeDuration == null ? "NULL" : "N'" + item.GuaranteeDuration.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.GuaranteeCodition == null ? "NULL" : "N'" + item.GuaranteeCodition.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.Description == null ? "NULL" : "N" + "'" + item.Description.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.AccountingObjectID == null ? "NULL" : "'" + item.AccountingObjectID.ToString() + "'",
                                        item.AccountingObjectName == null ? "NULL" : "N'" + item.AccountingObjectName.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.AccountingObjectAddress == null ? "NULL" : "N'" + item.AccountingObjectAddress.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.CurrentState == null ? "NULL" : item.CurrentState.ToString(),
                                        item.DeliveryRecordNo == null ? "NULL" : "N'" + item.DeliveryRecordNo.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.DeliveryRecordDate == null ? "NULL" : "'" + item.DeliveryRecordDate.ToString() + "'",
                                        "N'" + item.FixedAssetCode.ToString().Replace("'", "'+CHAR(39)+N'") + "'");

                                }
                                else
                                {
                                    item.ID = Guid.NewGuid();
                                    query += " " + string.Format("INSERT INTO FixedAsset(" +
                                        "ID, " +
                                        "BranchID, " +
                                        "FixedAssetCategoryID, " +
                                        "DepartmentID, " +
                                        "FixedAssetCode, " +
                                        "FixedAssetName, " +
                                        "Quantity, " +
                                        "Description, " +
                                        "ProductionYear, " +
                                        "MadeIn, " +
                                        "SerialNumber, " +
                                        "Accessories, " +
                                        "AccountingObjectID, " +
                                        "AccountingObjectName, " +
                                        "AccountingObjectAddress," +
                                        "GuaranteeDuration, " +
                                        "GuaranteeCodition, " +
                                        "IsSecondHand, " +
                                        "CurrentState, " +
                                        "DisposedDate, " +
                                        "DisposedAmount," +
                                        "DisposedReason, " +
                                        "PurchasedDate, " +
                                        "UsedDate, " +
                                        "IncrementDate, " +
                                        "DepreciationDate, " +
                                        "PurchasePrice," +
                                        "OriginalPrice, " +
                                        "AcDepreciationAmount, " +
                                        "RemainingAmount," +
                                        "UsedTime," +
                                        "DepreciationAccount," +
                                        "PeriodDepreciationAmount," +
                                        "OriginalPriceAccount," +
                                        "DepreciationRate," +
                                        "ExpenditureAccount," +
                                        "DepreciationMethod," +
                                        "CostByLoan," +
                                        "CostByEquity," +
                                        "CostByVenture," +
                                        "CostByBudget," +
                                        "CostByOther," +
                                        "CostSetID," +
                                        "IsDepreciationAllocation," +
                                        "MonthDepreciationRate," +
                                        "MonthPeriodDepreciationAmount," +
                                        "DeliveryRecordNo," +
                                        "DeliveryRecordDate," +
                                        "IsIrrationalCost," +
                                        "IsDisplayMonth," +
                                        "IsCreateFromOldDatabase," +
                                        "No," +
                                        "IsDisplayMonthRemain," +
                                        "PostedDate," +
                                        "StatisticsCodeID," +
                                        "BudgetItemID," +
                                        "UsedTimeRemain," +
                                        "IsActive) " +
                                        "VALUES ('{0}',{1},'{2}',{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45},{46},{47},{48},{49},{50},{51},{52},{53},{54},{55},{56},{57})",
                                        item.ID, item.BranchID == null ? "NULL" : "'" + item.BranchID + "'",
                                        item.FixedAssetCategoryID,
                                        item.DepartmentID == null ? "NULL" : "'" + item.DepartmentID + "'",
                                        "N'" + item.FixedAssetCode.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.FixedAssetName == null ? "NULL" : "N" + "'" + item.FixedAssetName.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                        "NULL",
                                        item.Description == null ? "NULL" : "N" + "'" + item.Description.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                        item.ProductionYear == null ? "NULL" : item.ProductionYear.ToString(),
                                         item.MadeIn == null ? "NULL" : "N'" + item.MadeIn.Replace("'", "'+CHAR(39)+N'") + "'",
                                         item.SerialNumber == null ? "NULL" : "N" + "'" + item.SerialNumber.Replace("'", "'+CHAR(39)+N'") + "'",
                                         "NULL",
                                         item.AccountingObjectID == null ? "NULL" : "'" + item.AccountingObjectID.ToString() + "'",
                                         item.AccountingObjectName == null ? "NULL" : "N'" + item.AccountingObjectName.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                         item.AccountingObjectAddress == null ? "NULL" : "N'" + item.AccountingObjectAddress.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                         item.GuaranteeDuration == null ? "NULL" : "N'" + item.GuaranteeDuration.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                         item.GuaranteeCodition == null ? "NULL" : "N'" + item.GuaranteeCodition.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                         "0", item.CurrentState == null ? "NULL" : item.CurrentState.ToString(), item.DisposedDate == null ? "NULL" : "'" + item.DisposedDate.ToString() + "'",
                                         "0",
                                         "NULL",
                                         item.PurchasedDate == null ? "NULL" : "'" + item.PurchasedDate.ToString() + "'",
                                         item.UsedDate == null ? "NULL" : "'" + item.UsedDate.ToString() + "'",
                                         item.IncrementDate == null ? "NULL" : "'" + item.IncrementDate.ToString() + "'",
                                         item.DepreciationDate == null ? "NULL" : "'" + item.DepreciationDate.ToString() + "'",
                                         item.PurchasePrice == null ? "NULL" : "'" + item.PurchasePrice + "'",
                                         item.OriginalPrice == null ? "NULL" : "'" + item.OriginalPrice + "'",
                                         "NULL",
                                         "NULL",
                                         item.UsedTime == null ? "NULL" : "" + "'" + item.UsedTime.ToString().Replace(',', '.') + "'",
                                         item.DepreciationAccount == null ? "NULL" : "N'" + item.DepreciationAccount.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                         item.PeriodDepreciationAmount == null ? "NULL" : "'" + item.PeriodDepreciationAmount + "'",
                                         item.OriginalPriceAccount == null ? "NULL" : "N'" + item.OriginalPriceAccount.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                         item.DepreciationRate == null ? "NULL" : "" + "'" + item.DepreciationRate.ToString().Replace(',', '.') + "'",
                                         item.ExpenditureAccount == null ? "NULL" : "N'" + item.ExpenditureAccount.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                         "NULL",
                                         "NULL",
                                         "NULL",
                                         "NULL",
                                         "NULL",
                                         "NULL",
                                         item.CostSetID == null ? "NULL" : "'" + item.CostSetID.ToString() + "'",
                                         "0",
                                         item.MonthDepreciationRate == null ? "NULL" : "" + "'" + item.MonthDepreciationRate.ToString().Replace(',', '.') + "'",
                                         item.MonthPeriodDepreciationAmount == null ? "NULL" : "'" + item.MonthPeriodDepreciationAmount + "'",
                                         item.DeliveryRecordNo == null ? "NULL" : "N'" + item.DeliveryRecordNo.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                                         item.DeliveryRecordDate == null ? "NULL" : "'" + item.DeliveryRecordDate.ToString() + "'",
                                         "NULL",
                                         "0",
                                         "0",
                                         "NULL",
                                         "NULL",
                                         "NULL",
                                         "NULL",
                                         "NULL",
                                         "NULL",
                                         "1");
                                }
                            }
                            if (!ExecQueries(query))
                            {
                                WaitingFrm.StopWaiting();
                                MSG.Error("Thêm dữ liệu không thành công");
                                return;
                            }
                            Utils.IFixedAssetService.RolbackTran();
                            Utils.ClearCacheByType<FixedAsset>();
                            //Utils.ClearCacheByType<MaterialGoodsCustom>();
                            //Utils.IMaterialGoodsService.UnbindSession(Utils.ListMaterialGoods);
                            //var a = Utils.ListMaterialGoods;
                            WaitingFrm.StopWaiting();
                            MSG.Information("Thêm dữ liệu thành công");
                            closeForm = true;
                            this.Close();
                        }
                        catch (Exception ex)
                        {
                            Utils.IFixedAssetService.RolbackTran();
                            WaitingFrm.StopWaiting();
                        }
                        #endregion

                    }
                    else
                    {
                        if (MSG.Question("Dữ liệu file không đúng. Bạn có muốn tải file lỗi về không?") == System.Windows.Forms.DialogResult.Yes)
                        {
                            System.Windows.Forms.SaveFileDialog sf = new System.Windows.Forms.SaveFileDialog
                            {
                                FileName = "FileExcelError.xlsx",
                                AddExtension = true,
                                Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
                            };
                            sf.Title = "Chọn thư mục lưu file";
                            if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                try
                                {
                                    wb.SaveAs(sf.FileName);
                                }
                                catch
                                {
                                    MSG.Error("Lỗi khi lưu");
                                }
                                if (MSG.Question("Tải xuống thành công, Bạn có muốn mở file vừa tải") == System.Windows.Forms.DialogResult.Yes)
                                {
                                    try
                                    {
                                        System.Diagnostics.Process.Start(sf.FileName);
                                    }
                                    catch
                                    {
                                    }
                                }
                            }
                            else
                            {
                            }
                        }
                        else
                            MSG.Error("Import không thành công");
                    }
                }
                else
                {
                    if (cbbDanhMuc.Value.ToString() == "FixedAsset")
                    {
                        WaitingFrm.StopWaiting();
                        MSG.Error("Dữ liệu file trống");
                        return;
                    }
                }
                #endregion
            }

            catch (Exception e)
            {
                WaitingFrm.StopWaiting();
            }
        }
        private void Post_Customers(List<AccountingObject> accountingObjects)
        {
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "HDDT").Data != "1") return;
            SystemOption systemOption = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "EI_IDNhaCungCapDichVu");
            if (string.IsNullOrEmpty(systemOption.Data))
            {
                //MSG.Error("Chưa kết nối hóa đơn điện tử");
                return;
            }
            SupplierService supplierService = Utils.ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
            if (supplierService == null) return;
            Request request = new Request();
            if (supplierService.SupplierServiceCode == "SDS")
            {
                Customers customers = new Customers();
                foreach (AccountingObject accountingObject in accountingObjects)
                {

                    Customer_If customer = new Customer_If();
                    customer.Code = accountingObject.AccountingObjectCode;
                    customer.AccountName = accountingObject.AccountingObjectCode;
                    customer.Name = accountingObject.AccountingObjectName;
                    //customer.Buyer = accountingObject.ContactName; // Người mua
                    customer.Address = accountingObject.Address;
                    customer.TaxCode = accountingObject.TaxCode;
                    if (accountingObject.BankAccounts.Count > 0)
                    {
                        customer.BankName = accountingObject.BankAccounts.First().BankName;
                        customer.BankNumber = accountingObject.BankAccounts.First().BankAccount;
                        //customer.BankAccountName = 
                    }
                    customer.Email = accountingObject.Email;
                    customer.Fax = accountingObject.Fax;
                    customer.Phone = accountingObject.Tel;
                    customer.ContactPerson = accountingObject.ContactName;
                    //customer.RepresentPerson = accountingObject.
                    customers.Customer.Add(customer);
                }
                var xmldata = Utils.Serialize<Customers>(customers);
                request.XmlData = xmldata;
                RestApiService client = new RestApiService(Utils.GetPathAccess(), Utils.GetUserName(), Core.ITripleDes.TripleDesDefaultDecryption(Utils.GetPassWords()));
                Response response = client.Post(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "ThemSuaKH").ApiPath, request);
                if (response.Status != 2)
                {
                    MSG.Error("Lỗi khi cập nhật lên web hóa đơn điện tử: \n" + response.Message);
                }
            }
        }
        public void ChooseFile(string path)
        {
            this.path = path;
            wb = new XLWorkbook(@path);
        }
        //Chose Sheet
        public void ChooseSheet(string sheet)
        {
            ws = wb.Worksheet(sheet);
            //xlRange = ws.RangeUsed();
        }
        public Dictionary<string, int> ReadHeader()
        {
            Dictionary<string, int> headers = new Dictionary<string, int>();
            var head = ws.Row(1);
            for (int i = 1; i <= ws.ColumnsUsed().Count(); i++)
            {
                try
                {
                    headers.Add((string)head.Cell(i).Value, i);
                }
                catch
                {

                }
            }
            return headers;
        }
        public List<string> ReadSheetName()
        {
            List<string> result = new List<string>();
            for (int i = 1; i <= wb.Worksheets.Count; i++)
            {
                result.Add(wb.Worksheet(i).Name);
            }
            return result;
        }

        private void cbbSheet_BeforeDropDown(object sender, CancelEventArgs e)
        {

            if (!check())
            {
                cbbSheet.Text = null;
                cbbSheet.DataSource = null;
                //LoadUGrid(new List<CategoryMapping>());
                return;
            }
            try
            {
                WaitingFrm.StartWaiting();
                ChooseFile(lblFileName.Text);
                Dictionary<int, string> lst = new Dictionary<int, string>();
                cbbSheet.DataSource = wb.Worksheets.Select(n => n.Name).ToList();
                Utils.ConfigGrid(cbbSheet, "");
                //cbbSheet.DisplayMember = "Value";
                //cbbSheet.ValueMember = "Key";
                //cbbSheet.DisplayLayout.Bands[0].Columns[0].Header.Caption = "STT";
                cbbSheet.DisplayLayout.Bands[0].Columns[0].Header.Caption = "Tên sheet";
                cbbSheet.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.StartsWith;

                WaitingFrm.StopWaiting();
            }
            catch (Exception ex)
            {
                WaitingFrm.StopWaiting();
                MSG.Error("File đang được mở");
                return;
            }

        }
        public bool check()
        {
            if (string.IsNullOrEmpty(cbbDanhMuc.Text))
            {
                MSG.Error("Bạn chưa chọn danh mục");
                cbbDanhMuc.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(lblFileName.Text))
            {
                MSG.Error("Bạn chưa chọn file");
                return false;
            }
            if (!File.Exists(lblFileName.Text))
            {
                MSG.Error("File không tồn tại");
                return false;
            }

            if (new FileInfo(lblFileName.Text).Length == 0)
            {
                MSG.Error("File trống");
                return false;
            }
            string extenstion = Path.GetExtension(lblFileName.Text);
            if (extenstion == ".xlsx" || extenstion == ".xls")
            {

            }
            else
            {
                MSG.Error("File không đúng định dạng file Excel");
                return false;
            }
            double fileSize = (double)new System.IO.FileInfo(lblFileName.Text).Length / 1024;
            if (fileSize > 102400)
            {
                MSG.Error("Dung lượng file vượt quá mức cho phép (100MB)");
                return false;
            }
            return true;
        }

        private void cbbSheet_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (cbbSheet.Value != null)
            {
                ChooseSheet(cbbSheet.Value.ToString());
                if (ws.RowsUsed().Count() > 0)
                {
                    headers = ReadHeader();
                    var cbbMethods = new UltraCombo();

                    cbbMethods.DataSource = headers;
                    Utils.ConfigGrid(cbbMethods, "");
                    cbbMethods.DisplayMember = "Key";
                    cbbMethods.ValueMember = "Value";
                    cbbMethods.DisplayLayout.Bands[0].Columns[0].Header.Caption = "Tên trường";
                    //cbbMethods.DisplayLayout.Bands[0].Columns[0].Width = 120;
                    cbbMethods.DisplayLayout.Bands[0].Columns[1].Hidden = true;
                    cbbMethods.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                    cbbMethods.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.StartsWith;
                    UltraGridBand band = cbbMethods.DisplayLayout.Bands[0];
                    band.Override.SelectedRowAppearance.ForeColor = Color.Black;
                    band.Override.ActiveRowAppearance.ForeColor = Color.Black;
                    //band.Override.CellAppearance.BackColor = System.Drawing.Color.LightYellow;
                    //band.Override.CellAppearance.BackColor2 = System.Drawing.Color.Black;

                    foreach (var column in uGridMapping.DisplayLayout.Bands[0].Columns)
                    {
                        this.ConfigEachColumn4Grid(0, column, uGridMapping);
                        if (column.Key.Equals("SheetColumnHeader"))
                        {
                            column.EditorComponent = cbbMethods;
                            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                        }
                    }
                    foreach (var item in uGridMapping.Rows)
                    {
                        item.Cells[3].Value = null;
                    }
                    if (cbbDanhMuc.Value.ToString() == "AccountingObject1")
                    {
                        if (checkList(HEADER1, headers.Keys.ToList()))
                        {
                            foreach (var item in uGridMapping.Rows)
                            {
                                item.Cells["SheetColumnHeader"].Value = item.Index + 1;
                            }
                        }
                    }
                    else if (cbbDanhMuc.Value.ToString() == "AccountingObject2")
                    {
                        if (checkList(HEADER2, headers.Keys.ToList()))
                        {
                            foreach (var item in uGridMapping.Rows)
                            {
                                item.Cells["SheetColumnHeader"].Value = item.Index + 1;
                            }
                        }
                    }
                    else if (cbbDanhMuc.Value.ToString() == "AccountingObject3")
                    {
                        if (checkList(HEADER3, headers.Keys.ToList()))
                        {
                            foreach (var item in uGridMapping.Rows)
                            {
                                item.Cells["SheetColumnHeader"].Value = item.Index + 1;
                            }
                        }
                    }
                    else if (cbbDanhMuc.Value.ToString() == "MaterialGoods")
                    {
                        if (checkList(HEADER4, headers.Keys.ToList()))
                        {
                            foreach (var item in uGridMapping.Rows)
                            {
                                item.Cells["SheetColumnHeader"].Value = item.Index + 1;
                            }
                        }
                    }
                    else if (cbbDanhMuc.Value.ToString() == "FixedAsset")
                    {
                        if (checkList(HEADER5, headers.Keys.ToList()))
                        {
                            foreach (var item in uGridMapping.Rows)
                            {
                                item.Cells["SheetColumnHeader"].Value = item.Index + 1;
                            }
                        }
                    }
                    else if (cbbDanhMuc.Value.ToString() == "TIInit")
                    {
                        if (checkList(HEADER6, headers.Keys.ToList()))
                        {
                            foreach (var item in uGridMapping.Rows)
                            {
                                item.Cells["SheetColumnHeader"].Value = item.Index + 1;
                            }
                        }
                    }
                }
            }
        }

        public bool checkList(List<string> ls1, List<string> ls2)
        {
            if (ls1.Count != ls2.Count) return false;
            List<string> ls3 = new List<string>();
            foreach (var item in ls2)
            {
                ls3.Add(item.Trim(' ').ToUpper());
            }
            foreach (var item in ls1)
            {
                if (!ls3.Contains(item.Trim(' ').ToUpper()))
                    return false;
            }
            return true;
        }
        public void LoadUGrid(List<CategoryMapping> ls)
        {
            //lstCategoryMapping = new List<CategoryMapping>();
            //foreach (var item in ls)
            //{
            //    CategoryMapping categoryMapping = new CategoryMapping();
            //    categoryMapping.ColumnMapping = item;
            //    categoryMapping.SheetColumnHeader = "";
            //    lstCategoryMapping.Add(categoryMapping);
            //}

            uGridMapping.DataSource = new BindingList<CategoryMapping>(ls);
            Utils.ConfigGrid(uGridMapping, ConstDatabase.CategoryMapping_TableName);
            //uGridMapping.DisplayLayout.Bands[0].Columns["ColumnMapping"].Width = 120;
            uGridMapping.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGridMapping.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGridMapping.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGridMapping.DisplayLayout.Bands[0].Summaries.Clear();
            uGridMapping.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            uGridMapping.DisplayLayout.Bands[0].Columns["Status"].CellActivation = Activation.NoEdit;
            uGridMapping.DisplayLayout.Bands[0].Columns["Describe"].CellActivation = Activation.NoEdit;
            uGridMapping.DisplayLayout.Bands[0].Columns["ColumnMapping"].CellActivation = Activation.NoEdit;
            uGridMapping.DisplayLayout.Bands[0].Columns["Status"].Width = 60;
        }

        private void uGridMapping_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("SheetColumnHeader"))
            {
                List<CategoryMapping> lstA = ((BindingList<CategoryMapping>)uGridMapping.DataSource).Where(n => !n.SheetColumnHeader.IsNullOrEmpty()).ToList();
                if (lstA.GroupBy(n => n.SheetColumnHeader).Any(n => n.Count() > 1))
                {
                    e.Cell.Value = null;
                    MSG.Error("Tên trường đã được chọn");
                }
            }
        }

        private void uGridMapping_CellDataError(object sender, CellDataErrorEventArgs e)
        {
            if (!e.ForceExit)
            {
                // Default for StayInEditMode is true. However you can set it to false to
                // cause the grid to exit the edit mode and restore the original value. We
                // will just leave it true for this example.
                e.StayInEditMode = true;

                // Set the RaiseErrorEvent to false to prevent the grid from raising 
                // the error event and displaying any message.
                e.RaiseErrorEvent = false;

                // Instead display our own message.
                if (this.uGridMapping.ActiveCell.Column.Key.Equals("SheetColumnHeader"))
                {
                    //MessageBox.Show(this, "Vui lòng nhập ngày hợp lệ.", "Đầu vào không hợp lệ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    MSG.Error("Tên trường không hợp lệ");
                }
                //else if (this.uGrid.ActiveCell.Column.DataType == typeof(decimal))
                //{
                //    MessageBox.Show(this, "Please enter a valid numer.", "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //}
            }
            else
            {
                // Set the RaiseErrorEvent to false to prevent the grid from raising 
                // the error event.
                e.RaiseErrorEvent = false;
            }

        }

        private bool ExecQueries(string query)
        {
            string serverName = Properties.Settings.Default.ServerName;
            string database = Properties.Settings.Default.DatabaseName;
            string user = Properties.Settings.Default.UserName;
            string pass = Properties.Settings.Default.Password;

            if (serverName == null || database == null || user == null || pass == null)
            {
                return false;
            }
            SqlConnection sqlConnection = new SqlConnection("");
            if (!string.IsNullOrEmpty(serverName) && !string.IsNullOrEmpty(database) && !string.IsNullOrEmpty(pass) && !serverName.Equals("14.225.3.208") && !serverName.Equals("14.225.3.218"))
            {
                string _ConnectionString = "Data Source=" + serverName + ";Initial Catalog=" + database + ";User ID=" + user + ";Password=" + pass + "";

                sqlConnection = new SqlConnection(_ConnectionString);

                SqlTransaction transaction;
                try
                {
                    sqlConnection.Open();
                }
                catch (Exception)
                {
                    WaitingFrm.StopWaiting();
                    MSG.Warning("Máy chủ SQL không tồn tại. Vui lòng chọn máy chủ SQL khác.");
                    return false;
                }
                transaction = sqlConnection.BeginTransaction();
                try
                {
                    //string q = "";
                    //foreach (var query in sqlqueries)
                    //{
                    //    q += query + " ";
                    //}
                    new SqlCommand(query, sqlConnection, transaction).ExecuteNonQuery();
                    transaction.Commit();
                    //transaction.Dispose();
                    //UnbindSess
                }
                catch (SqlException sqlError)
                {
                    WaitingFrm.StopWaiting();
                    MSG.Warning(sqlError.ToString());
                    transaction.Rollback();
                    return false;
                }
                sqlConnection.Close();
                //sqlConnection.Dispose();
            }
            else return false;
            return true;
        }

        private void FileDemo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (FileDemo.Text == "Import_KhachHang")
            {
                SaveFileDialog sf = new SaveFileDialog
                {
                    FileName = "Import_KhachHang.xlsx",
                    AddExtension = true,
                    Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
                };

                if (sf.ShowDialog() == DialogResult.OK)
                {
                    string path = System.IO.Path.GetDirectoryName(
                            System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                    string filePath = string.Format("{0}\\TemplateResource\\excel\\Import_KhachHang.xlsx", path);
                    ResourceHelper.MakeFileOutOfAStream(filePath, sf.FileName);
                    //MSG.Information("Tải xuống thành công");
                    if (MSG.Question("Tải xuống thành công, Bạn có muốn mở file vừa tải về không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(sf.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
            }
            else if (FileDemo.Text == "Import_NhaCungCap")
            {
                SaveFileDialog sf = new SaveFileDialog
                {
                    FileName = "Import_NhaCungCap.xlsx",
                    AddExtension = true,
                    Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
                };

                if (sf.ShowDialog() == DialogResult.OK)
                {
                    string path = System.IO.Path.GetDirectoryName(
                            System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                    string filePath = string.Format("{0}\\TemplateResource\\excel\\Import_NhaCungCap.xlsx", path);
                    ResourceHelper.MakeFileOutOfAStream(filePath, sf.FileName);
                    //MSG.Information("Tải xuống thành công");
                    if (MSG.Question("Tải xuống thành công, Bạn có muốn mở file vừa tải về không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(sf.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
            }
            else if (FileDemo.Text == "Import_NhanVien")
            {
                SaveFileDialog sf = new SaveFileDialog
                {
                    FileName = "Import_NhanVien.xlsx",
                    AddExtension = true,
                    Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
                };

                if (sf.ShowDialog() == DialogResult.OK)
                {
                    string path = System.IO.Path.GetDirectoryName(
                            System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                    string filePath = string.Format("{0}\\TemplateResource\\excel\\Import_NhanVien.xlsx", path);
                    ResourceHelper.MakeFileOutOfAStream(filePath, sf.FileName);
                    //MSG.Information("Tải xuống thành công");
                    if (MSG.Question("Tải xuống thành công, Bạn có muốn mở file vừa tải về không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(sf.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
            }
            else if (FileDemo.Text == "Import_VTHH")
            {
                SaveFileDialog sf = new SaveFileDialog
                {
                    FileName = "Import_VTHH.xlsx",
                    AddExtension = true,
                    Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
                };

                if (sf.ShowDialog() == DialogResult.OK)
                {
                    string path = System.IO.Path.GetDirectoryName(
                            System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                    string filePath = string.Format("{0}\\TemplateResource\\excel\\Import_VTHH.xlsx", path);
                    ResourceHelper.MakeFileOutOfAStream(filePath, sf.FileName);
                    //MSG.Information("Tải xuống thành công");
                    if (MSG.Question("Tải xuống thành công, Bạn có muốn mở file vừa tải về không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(sf.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
            }
            else if (FileDemo.Text == "Import_TSCĐ")
            {
                SaveFileDialog sf = new SaveFileDialog
                {
                    FileName = "Import_DMTSCĐ.xlsx",
                    AddExtension = true,
                    Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
                };

                if (sf.ShowDialog() == DialogResult.OK)
                {
                    string path = System.IO.Path.GetDirectoryName(
                            System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                    string filePath = string.Format("{0}\\TemplateResource\\excel\\Import_DMTSCĐ.xlsx", path);
                    ResourceHelper.MakeFileOutOfAStream(filePath, sf.FileName);
                    //MSG.Information("Tải xuống thành công");
                    if (MSG.Question("Tải xuống thành công, Bạn có muốn mở file vừa tải về không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(sf.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
            }
            else if (FileDemo.Text == "Import_CCDC")
            {
                SaveFileDialog sf = new SaveFileDialog
                {
                    FileName = "Import_CCDC.xlsx",
                    AddExtension = true,
                    Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
                };

                if (sf.ShowDialog() == DialogResult.OK)
                {
                    string path = System.IO.Path.GetDirectoryName(
                            System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                    string filePath = string.Format("{0}\\TemplateResource\\excel\\Import_CCDC.xlsx", path);
                    ResourceHelper.MakeFileOutOfAStream(filePath, sf.FileName);
                    //MSG.Information("Tải xuống thành công");
                    if (MSG.Question("Tải xuống thành công, Bạn có muốn mở file vừa tải về không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(sf.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
            }
            else
            {
                MSG.Error("Chưa chọn danh mục");
            }

        }
    }
}
