﻿namespace Accounting
{
    partial class UCSalary
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCSalary));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.btnBh = new Infragistics.Win.Misc.UltraButton();
            this.btnOtherVoucher = new Infragistics.Win.Misc.UltraButton();
            this.btnTimeSheet = new Infragistics.Win.Misc.UltraButton();
            this.btnTimeSheetSummary = new Infragistics.Win.Misc.UltraButton();
            this.btnSalaryPayment = new Infragistics.Win.Misc.UltraButton();
            this.btnSalarySheet = new Infragistics.Win.Misc.UltraButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnBh
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance1.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnBh.Appearance = appearance1;
            this.btnBh.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
            this.btnBh.HotTrackAppearance = appearance2;
            this.btnBh.ImageSize = new System.Drawing.Size(80, 80);
            this.btnBh.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnBh.Location = new System.Drawing.Point(1074, 329);
            this.btnBh.Name = "btnBh";
            this.btnBh.Size = new System.Drawing.Size(92, 97);
            this.btnBh.TabIndex = 38;
            // 
            // btnOtherVoucher
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOtherVoucher.Appearance = appearance3;
            this.btnOtherVoucher.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
            this.btnOtherVoucher.HotTrackAppearance = appearance4;
            this.btnOtherVoucher.ImageSize = new System.Drawing.Size(80, 80);
            this.btnOtherVoucher.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnOtherVoucher.Location = new System.Drawing.Point(725, 170);
            this.btnOtherVoucher.Name = "btnOtherVoucher";
            this.btnOtherVoucher.Size = new System.Drawing.Size(92, 97);
            this.btnOtherVoucher.TabIndex = 39;
            // 
            // btnTimeSheet
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance5.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnTimeSheet.Appearance = appearance5;
            this.btnTimeSheet.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
            this.btnTimeSheet.HotTrackAppearance = appearance6;
            this.btnTimeSheet.ImageSize = new System.Drawing.Size(80, 80);
            this.btnTimeSheet.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnTimeSheet.Location = new System.Drawing.Point(41, 170);
            this.btnTimeSheet.Name = "btnTimeSheet";
            this.btnTimeSheet.Size = new System.Drawing.Size(92, 97);
            this.btnTimeSheet.TabIndex = 40;
            // 
            // btnTimeSheetSummary
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.Image = ((object)(resources.GetObject("appearance7.Image")));
            appearance7.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance7.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance7.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnTimeSheetSummary.Appearance = appearance7;
            this.btnTimeSheetSummary.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance8.Image = ((object)(resources.GetObject("appearance8.Image")));
            this.btnTimeSheetSummary.HotTrackAppearance = appearance8;
            this.btnTimeSheetSummary.ImageSize = new System.Drawing.Size(80, 80);
            this.btnTimeSheetSummary.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnTimeSheetSummary.Location = new System.Drawing.Point(261, 169);
            this.btnTimeSheetSummary.Name = "btnTimeSheetSummary";
            this.btnTimeSheetSummary.Size = new System.Drawing.Size(92, 97);
            this.btnTimeSheetSummary.TabIndex = 41;
            // 
            // btnSalaryPayment
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.Image = ((object)(resources.GetObject("appearance9.Image")));
            appearance9.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance9.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance9.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnSalaryPayment.Appearance = appearance9;
            this.btnSalaryPayment.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance10.Image = ((object)(resources.GetObject("appearance10.Image")));
            this.btnSalaryPayment.HotTrackAppearance = appearance10;
            this.btnSalaryPayment.ImageSize = new System.Drawing.Size(80, 80);
            this.btnSalaryPayment.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnSalaryPayment.Location = new System.Drawing.Point(1074, 3);
            this.btnSalaryPayment.Name = "btnSalaryPayment";
            this.btnSalaryPayment.Size = new System.Drawing.Size(92, 97);
            this.btnSalaryPayment.TabIndex = 42;
            // 
            // btnSalarySheet
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.Image = ((object)(resources.GetObject("appearance11.Image")));
            appearance11.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance11.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance11.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnSalarySheet.Appearance = appearance11;
            this.btnSalarySheet.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance12.Image = ((object)(resources.GetObject("appearance12.Image")));
            this.btnSalarySheet.HotTrackAppearance = appearance12;
            this.btnSalarySheet.ImageSize = new System.Drawing.Size(80, 80);
            this.btnSalarySheet.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnSalarySheet.Location = new System.Drawing.Point(475, 170);
            this.btnSalarySheet.Name = "btnSalarySheet";
            this.btnSalarySheet.Size = new System.Drawing.Size(92, 97);
            this.btnSalarySheet.TabIndex = 43;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(976, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 33);
            this.label2.TabIndex = 45;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(171, 202);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 33);
            this.label1.TabIndex = 46;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.Location = new System.Drawing.Point(385, 202);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 33);
            this.label3.TabIndex = 47;
            // 
            // label6
            // 
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Image = ((System.Drawing.Image)(resources.GetObject("label6.Image")));
            this.label6.Location = new System.Drawing.Point(615, 202);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 33);
            this.label6.TabIndex = 50;
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Image = ((System.Drawing.Image)(resources.GetObject("label4.Image")));
            this.label4.Location = new System.Drawing.Point(864, 202);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 33);
            this.label4.TabIndex = 51;
            // 
            // label5
            // 
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Image = ((System.Drawing.Image)(resources.GetObject("label5.Image")));
            this.label5.Location = new System.Drawing.Point(976, 364);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 33);
            this.label5.TabIndex = 52;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.HotTrack;
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label7.Location = new System.Drawing.Point(949, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(6, 331);
            this.label7.TabIndex = 53;
            // 
            // UCSalary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSalarySheet);
            this.Controls.Add(this.btnSalaryPayment);
            this.Controls.Add(this.btnTimeSheetSummary);
            this.Controls.Add(this.btnTimeSheet);
            this.Controls.Add(this.btnOtherVoucher);
            this.Controls.Add(this.btnBh);
            this.Name = "UCSalary";
            this.Size = new System.Drawing.Size(1240, 448);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnBh;
        private Infragistics.Win.Misc.UltraButton btnOtherVoucher;
        private Infragistics.Win.Misc.UltraButton btnTimeSheet;
        private Infragistics.Win.Misc.UltraButton btnTimeSheetSummary;
        private Infragistics.Win.Misc.UltraButton btnSalaryPayment;
        private Infragistics.Win.Misc.UltraButton btnSalarySheet;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
    }
}
