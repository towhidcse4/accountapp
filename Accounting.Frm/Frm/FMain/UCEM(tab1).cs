﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class UCEM_tab1_ : UserControl
    {
        public UCEM_tab1_(FMain @this)
        {
            InitializeComponent();
            btnEMContractBuy.Click += (s, e) => btnEMContractBuy_Click(s, e, @this);
            btnEMContractSale.Click += (s, e) => btnEMContractSale_Click(s, e, @this);
            btnSaleReport.Click += (s, e) => btnSaleReport_Click(s, e, @this);
        }

        private void btnEMContractBuy_Click(object sender, EventArgs e,FMain fMain)
        {
            fMain.ShowFEMContractBuy();
        }

        private void btnEMContractSale_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFEMContractSale();
        }

        private void btnSaleReport_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFRContractStatusSale();
        }
    }
}
