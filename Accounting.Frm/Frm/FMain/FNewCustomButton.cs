﻿using Accounting.TextMessage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FNewCustomButton : DialogForm
    {
        string groupKey = "";
        private Accounting.Core.Domain.ControlsMapping.Item newItem;

        public Accounting.Core.Domain.ControlsMapping.Item NewItem
        {
            get { return newItem; }
        }
        public FNewCustomButton(string groupKey, List<Accounting.Core.Domain.ControlsMapping.MappingControl> lstItems, List<Accounting.Core.Domain.ControlsMapping.MappingControl> lstCategory)
        {
            InitializeComponent();
            this.groupKey = groupKey;
            ConfigCombo(lstItems, lstCategory);
        }
        private void ConfigCombo(List<Accounting.Core.Domain.ControlsMapping.MappingControl> source1, List<Accounting.Core.Domain.ControlsMapping.MappingControl> source2)
        {
            cbbMapping.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            cbbMapping.DataSource = source1;
            cbbMapping.ValueMember = "Code";
            cbbMapping.DisplayMember = "Name";
            cbbMapping.DisplayLayout.Bands[0].ColHeadersVisible = false;
            cbbMapping.DropDownWidth = cbbMapping.Size.Width;
            this.ConfigGrid<Accounting.Core.Domain.ControlsMapping.MappingControl>(cbbMapping, ConstDatabase.MappingControl_KeyName, "ParentCode", "IsParent");
            cbbMapping.DisplayLayout.Bands[0].Columns[cbbMapping.DisplayMember].Width = cbbMapping.Size.Width - 20;
            //cbbMapping.
            cbbCategory.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            cbbCategory.DataSource = source2;
            cbbCategory.ValueMember = "Code";
            cbbCategory.DisplayMember = "Name";
            cbbCategory.DisplayLayout.Bands[0].ColHeadersVisible = false;
            cbbCategory.DropDownWidth = cbbMapping.Size.Width;
            this.ConfigGrid<Accounting.Core.Domain.ControlsMapping.MappingControl>(cbbCategory, ConstDatabase.MappingControl_KeyName, "ParentCode", "IsParent");
            cbbCategory.DisplayLayout.Bands[0].Columns[cbbCategory.DisplayMember].Width = cbbCategory.Size.Width - 20;
        }

        private void cbbMapping_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            var combo = (Infragistics.Win.UltraWinGrid.UltraCombo)sender;
            var temp = e.Row.ListObject as Accounting.Core.Domain.ControlsMapping.MappingControl;
            if (temp.IsParent && (e.Row.Index + 1 < combo.Rows.Count))
            {
                combo.SelectedRow = combo.Rows[e.Row.Index + 1];
                return;
            }
            txtCaption.Text = temp.Name;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtCaption.Text.Trim()) || (uTab.ActiveTab.Index == 0 && cbbMapping.Value == null) || (uTab.ActiveTab.Index == 1 && cbbCategory.Value == null)) return;
            newItem = new Core.Domain.ControlsMapping.Item();
            newItem.Caption = txtCaption.Text.Trim();
            newItem.GroupKey = groupKey;
            newItem.MappingCode = uTab.ActiveTab.Index == 0 ? cbbMapping.Value.ToString() : cbbCategory.Value.ToString();
            if (picPhoto.Image != null)
                newItem.Image = (picPhoto.Image as Bitmap).ImageToByteArray();
            else if (picPhoto.DefaultImage != null)
                newItem.Image = (picPhoto.DefaultImage as Bitmap).ImageToByteArray();
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = @"All Images|*.BMP;*.DIB;*.RLE;*.JPG;*.JPEG;*.JPE;*.JFIF;*.GIF;*.TIF;*.TIFF;*.PNG|BMP Files: (*.BMP;*.DIB;*.RLE)|*.BMP;*.DIB;*.RLE|JPEG Files: (*.JPG;*.JPEG;*.JPE;*.JFIF)|*.JPG;*.JPEG;*.JPE;*.JFIF|GIF Files: (*.GIF)|*.GIF|TIFF Files: (*.TIF;*.TIFF)|*.TIF;*.TIFF|PNG Files: (*.PNG)|*.PNG";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            string filePath = string.Format("{0}\\TemplateResource\\Image", path);
            if (System.IO.Directory.Exists(filePath))
                fileDialog.InitialDirectory = filePath;
            fileDialog.FilterIndex = 1;
            fileDialog.ShowDialog(this);
            if (!string.IsNullOrEmpty(fileDialog.FileName))
            {
                picPhoto.Image = new Bitmap(fileDialog.FileName);
            }
        }

    }
}
