﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class UCSalary : UserControl
    {
        public UCSalary(FMain @this)
        {
            InitializeComponent();
            btnBh.Click += (s, e) => btnBh_Click(s, e, @this);
            btnOtherVoucher.Click += (s, e) => btnOtherVoucher_Click(s, e, @this);
            btnTimeSheet.Click += (s, e) => btnTimeSheet_Click(s, e, @this);
            btnTimeSheetSummary.Click += (s, e) => btnTimeSheetSummary_Click(s, e, @this);
            btnSalarySheet.Click += (s, e) => btnSalarySheet_Click(s, e, @this);
            btnSalaryPayment.Click += (s, e) => btnSalaryPayment_Click(s, e, @this);
        }

        private void btnBh_Click(object sender, EventArgs e,FMain fMain)
        {
            fMain.ShowFInsurancePayment();
        }

        private void btnOtherVoucher_Click(object sender, EventArgs e,FMain fMain)
        {
            fMain.ShowFPSOtherVoucher();
        }

        private void btnTimeSheet_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFPSTimeSheet();
        }

        private void btnTimeSheetSummary_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFPSTimeSheetSummary();
        }

        private void btnSalarySheet_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFPSalarySheet();
        }

        private void btnSalaryPayment_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFWagePayment();
        }
    }
}
