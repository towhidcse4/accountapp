﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class UCCost : UserControl
    {
        public UCCost(FMain @this)
        {
            InitializeComponent();
            btnExpenses.Click += (s, e) => btnFGOInterestAndLossTransfer_Click(s, e, @this);
            btnCostingPeriod.Click += (s, e) => btnGOtherVoucher_Click(s, e, @this);
            btnUnfinishedEvaluation.Click += (s, e) => btnGDBDateClosed_Click(s, e, @this);
            btnCostingCard.Click += (s, e) => btnCostingCard_Click(s, e, @this);
        }

        void btnGDBDateClosed_Click(object sender, EventArgs e, FMain fMain)
        {
            //fMain.ShowFDBDateClosed();
        }

        private void btnGOtherVoucher_Click(object sender, EventArgs e, FMain fMain)
        {
            //fMain.ShowFGOtherVoucher();
        }

        private void btnFGOInterestAndLossTransfer_Click(object sender, EventArgs e, FMain fMain)
        {
            //fMain.ShowFGOInterestAndLossTransfer();
        }

        private void btnCostingCard_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFRCPPeriod();
        }
    }
}
