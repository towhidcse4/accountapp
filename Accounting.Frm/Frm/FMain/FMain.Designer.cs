﻿namespace Accounting
{
    partial class FMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool1 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnExit");
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance116 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance123 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance127 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.RibbonTab ribbonTab1 = new Infragistics.Win.UltraWinToolbars.RibbonTab("tabSystem");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup1 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool2 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnDate");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool3 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnLog");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool4 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnChangePass");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool5 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnManagerUsers");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool6 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnChangeProfile");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool7 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnRole");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool8 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnAddFunction");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool9 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnSettings");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool10 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Change_sesssion");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool11 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnChooseNoteBook");
            Infragistics.Win.UltraWinToolbars.RibbonTab ribbonTab2 = new Infragistics.Win.UltraWinToolbars.RibbonTab("tabCatalog");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup2 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("Category_01");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool12 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_Account");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool13 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_AccountTransfer");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool14 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_AccountDefault");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool15 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_AutoPrinciple");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup3 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("Category_02");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool16 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_AccountingObjectCustorm");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool17 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_Employee");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool18 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_AccountingObjectGroup");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool19 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_Department");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup4 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("Category_03");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool20 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_MaterialGoods");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool21 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_CCDC");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool22 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_FixedAsset");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool23 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_Repository");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup5 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("Category_04");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool24 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_BankAccountDetail");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool25 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_Bank");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool26 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_CreditCard");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool27 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_StockCategory");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool28 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_Currency");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup6 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("Category_05");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool29 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_CostSet");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool30 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_PersonalSalaryTax");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool31 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_TimeSheetSymbols");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool32 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_ExpenseItem");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup7 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("Category_06");
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool1 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("PopupMenuTool1");
            Infragistics.Win.UltraWinToolbars.RibbonTab ribbonTab3 = new Infragistics.Win.UltraWinToolbars.RibbonTab("tabBusiness");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup8 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup6");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool33 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FMCPayment");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool34 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FMCReceipt");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool35 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FMBDeposit");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool36 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FMBTellerPaper");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup9 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool37 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FPPInvoice1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool38 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FPPService");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool39 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FPPPayVendor");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool40 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FPPDiscountReturn");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup10 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup2");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool41 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FSAInvoicePayment0");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool42 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FSAInvoicePayment1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool43 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FSAReceiptCustomer");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool44 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FSAReturn");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup11 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup3");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool45 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FRSInwardOutward");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool46 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FRSInwardOutwardOutput");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool47 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FRSTransfer");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool48 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FRSInventoryAdjustments");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup12 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup4");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool49 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FFixedAsset");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool50 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FFABuyAndIncrement");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool51 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FFADecrement");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool52 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FFADepreciation");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup13 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup7");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool53 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FGOtherVoucher");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool54 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FGDisposeDbDateClosed");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool55 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FDBDateClosed");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool56 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FGOInterestAndLossTransfer");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup14 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup5");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool57 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FPSSalarySheet");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool58 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FPSTimeSheet");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool59 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FPaySalary");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool60 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FAccountingSalary");
            Infragistics.Win.UltraWinToolbars.RibbonTab ribbonTab4 = new Infragistics.Win.UltraWinToolbars.RibbonTab("tabUtils");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup15 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool61 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnConfig");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool62 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnSearch");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool63 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnQuickReport");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool64 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnMaintenanceData");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool65 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnConfigFinancialStatements");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool66 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnManagerDocuments");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool67 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnManagerWorking");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool68 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnOPAccount");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool69 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnGeneralLedger");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool70 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnRefresh");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool71 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ButtonTool1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool72 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnImportExcel");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool73 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnChangeStyles");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool74 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnImportChungtu");
            Infragistics.Win.UltraWinToolbars.RibbonTab ribbonTab5 = new Infragistics.Win.UltraWinToolbars.RibbonTab("ribbon1");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup16 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool75 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnInBaoCao");
            Infragistics.Win.UltraWinToolbars.RibbonTab ribbonTab6 = new Infragistics.Win.UltraWinToolbars.RibbonTab("ribbon2");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup17 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool76 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnHuongDan");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool77 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnHuongDan");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool78 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnHuongDan");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool79 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnHuongDan");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool80 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnInfo");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool81 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnUltraviewer1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool82 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnTeamviewer1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool83 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnUpdate");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool84 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnHuongDan");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool85 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnHuongDan");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool86 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnHuongDan");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool87 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnHuongDan");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool88 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnHuongDan");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool89 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnAdminTools1");
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance129 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance130 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance131 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance132 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance133 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance134 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance135 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance136 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance137 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance138 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance139 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance140 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance141 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance142 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance143 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance144 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool90 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnAccountingObjects");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool91 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ButtonTool1");
            Infragistics.Win.Appearance appearance145 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool92 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_Bank");
            Infragistics.Win.Appearance appearance146 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool93 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_Department");
            Infragistics.Win.Appearance appearance147 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool94 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_AccountingObjectCategory");
            Infragistics.Win.Appearance appearance148 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool95 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_AccountingObjectGroup");
            Infragistics.Win.Appearance appearance149 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool96 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_AccountingObjectCustorm");
            Infragistics.Win.Appearance appearance150 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance151 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool97 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnExit");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool98 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnGeneralLedger");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool99 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnOPAccount");
            Infragistics.Win.Appearance appearance152 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool100 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnSystemOption");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool101 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnDate");
            Infragistics.Win.Appearance appearance153 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool102 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnLog");
            Infragistics.Win.Appearance appearance154 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool103 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnChangePass");
            Infragistics.Win.Appearance appearance155 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool104 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnChangeProfile");
            Infragistics.Win.Appearance appearance156 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool105 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnRole");
            Infragistics.Win.Appearance appearance157 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool106 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnAddFunction");
            Infragistics.Win.Appearance appearance158 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool107 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_AccountTransfer");
            Infragistics.Win.Appearance appearance159 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool108 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_AccountGroup");
            Infragistics.Win.Appearance appearance160 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool109 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_AutoPrinciple");
            Infragistics.Win.Appearance appearance161 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool110 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_Employee");
            Infragistics.Win.Appearance appearance162 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool111 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_MaterialGoods");
            Infragistics.Win.Appearance appearance163 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance164 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool112 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_FixedAsset");
            Infragistics.Win.Appearance appearance165 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool113 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_Repository");
            Infragistics.Win.Appearance appearance166 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool114 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_BankAccountDetail");
            Infragistics.Win.Appearance appearance167 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance168 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool115 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_CreditCard");
            Infragistics.Win.Appearance appearance169 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool116 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_StockCategory");
            Infragistics.Win.Appearance appearance170 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool117 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_Investor");
            Infragistics.Win.Appearance appearance171 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool118 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_CostSet");
            Infragistics.Win.Appearance appearance172 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance173 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool119 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_PersonalSalaryTax");
            Infragistics.Win.Appearance appearance174 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool120 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_TimeSheetSymbols");
            Infragistics.Win.Appearance appearance175 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool121 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_ExpenseItem");
            Infragistics.Win.Appearance appearance176 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool122 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_Currency");
            Infragistics.Win.Appearance appearance177 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool123 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_BudgetItem");
            Infragistics.Win.Appearance appearance178 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool124 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_Type");
            Infragistics.Win.Appearance appearance179 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool125 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_PaymentClause");
            Infragistics.Win.Appearance appearance180 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool126 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FPPInvoice1");
            Infragistics.Win.Appearance appearance181 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool127 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FPPService");
            Infragistics.Win.Appearance appearance182 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool128 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FPPPayVendor");
            Infragistics.Win.Appearance appearance183 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool129 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FPPDiscountReturn");
            Infragistics.Win.Appearance appearance184 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool130 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FSAInvoicePayment0");
            Infragistics.Win.Appearance appearance185 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool131 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FSAInvoicePayment1");
            Infragistics.Win.Appearance appearance186 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool132 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FSAReceiptCustomer");
            Infragistics.Win.Appearance appearance187 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool133 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FSAReturn");
            Infragistics.Win.Appearance appearance188 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool134 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FRSInwardOutward");
            Infragistics.Win.Appearance appearance189 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool135 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FRSInwardOutwardOutput");
            Infragistics.Win.Appearance appearance190 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool136 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FRSTransfer");
            Infragistics.Win.Appearance appearance191 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool137 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FRSPricingOW");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool138 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FRSInventoryAdjustments");
            Infragistics.Win.Appearance appearance192 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool139 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FFixedAsset");
            Infragistics.Win.Appearance appearance193 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool140 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FFABuyAndIncrement");
            Infragistics.Win.Appearance appearance194 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool141 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FFADecrement");
            Infragistics.Win.Appearance appearance195 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool142 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FFADepreciation");
            Infragistics.Win.Appearance appearance196 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool143 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FPSSalarySheet");
            Infragistics.Win.Appearance appearance197 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool144 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FPSTimeSheet");
            Infragistics.Win.Appearance appearance198 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool145 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FPaySalary");
            Infragistics.Win.Appearance appearance199 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool146 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FAccountingSalary");
            Infragistics.Win.Appearance appearance200 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool147 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnTaxSheet");
            Infragistics.Win.Appearance appearance201 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool148 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnSATaxSheet");
            Infragistics.Win.Appearance appearance202 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool149 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnPPTaxSheet");
            Infragistics.Win.Appearance appearance203 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool150 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnTaxPayment");
            Infragistics.Win.Appearance appearance204 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool151 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FGOtherVoucher");
            Infragistics.Win.Appearance appearance205 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool152 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FGOInterestAndLossTransfer");
            Infragistics.Win.Appearance appearance206 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool153 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FDBDateClosed");
            Infragistics.Win.Appearance appearance207 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance208 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool154 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FGDisposeDbDateClosed");
            Infragistics.Win.Appearance appearance209 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool155 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnConfig");
            Infragistics.Win.Appearance appearance210 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool156 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnSearch");
            Infragistics.Win.Appearance appearance211 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool157 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnQuickReport");
            Infragistics.Win.Appearance appearance212 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool158 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnMaintenanceData");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool159 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnConfigFinancialStatements");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool160 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnManagerDocuments");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool161 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnManagerWorking");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool162 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnHelp");
            Infragistics.Win.Appearance appearance213 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool163 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnUpdate");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool164 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnAbout");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool165 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnSupport");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool166 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnRefresh");
            Infragistics.Win.Appearance appearance214 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool167 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnSettings");
            Infragistics.Win.Appearance appearance215 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool168 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FMCPayment");
            Infragistics.Win.Appearance appearance216 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool169 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FMBTellerPaper");
            Infragistics.Win.Appearance appearance217 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool170 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FMBDeposit");
            Infragistics.Win.Appearance appearance218 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool171 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_Account");
            Infragistics.Win.Appearance appearance219 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance220 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.PopupGalleryTool popupGalleryTool1 = new Infragistics.Win.UltraWinToolbars.PopupGalleryTool("pupGAccount");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool172 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_Account");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool173 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_AccountTransfer");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool174 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_AccountGroup");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool175 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_AutoPrinciple");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool176 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnAdminTools1");
            Infragistics.Win.Appearance appearance221 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool2 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("PopupMenuTool1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool177 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_MaterialGoodsCategory");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool178 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_FixedAssetCategory");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool179 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_MaterialQuantum");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool180 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_Investor");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool181 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_InvestorGroup");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool182 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_RegistrationGroup");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool183 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_ShareHolderGroup");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool184 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_StatisticsCode");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool185 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_PaymentClause");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool186 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_TransportMethod");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool187 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_BudgetItem");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool188 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_SalePriceGroup");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool189 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_MaterialGoodsSpecialTaxGroup");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool190 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_MaterialGoodsResourceTaxGroup");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool191 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_ContractState");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool192 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnQuyDinhLuongThueBh");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool193 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnImportData");
            Infragistics.Win.Appearance appearance222 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance223 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool194 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_AccountDefault");
            Infragistics.Win.Appearance appearance224 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool195 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_MaterialGoodsCategory");
            Infragistics.Win.Appearance appearance225 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool196 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_FixedAssetCategory");
            Infragistics.Win.Appearance appearance226 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool197 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_MaterialQuantum");
            Infragistics.Win.Appearance appearance227 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool198 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_InvestorGroup");
            Infragistics.Win.Appearance appearance228 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool199 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_RegistrationGroup");
            Infragistics.Win.Appearance appearance229 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool200 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_ShareHolderGroup");
            Infragistics.Win.Appearance appearance230 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool201 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_StatisticsCode");
            Infragistics.Win.Appearance appearance231 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool202 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_TransportMethod");
            Infragistics.Win.Appearance appearance232 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool203 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_SalePriceGroup");
            Infragistics.Win.Appearance appearance233 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool204 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_MaterialGoodsSpecialTaxGroup");
            Infragistics.Win.Appearance appearance234 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool205 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_MaterialGoodsResourceTaxGroup");
            Infragistics.Win.Appearance appearance235 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool206 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_ContractState");
            Infragistics.Win.Appearance appearance236 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool207 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_InvoiceType");
            Infragistics.Win.Appearance appearance237 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool208 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnManagerUsers");
            Infragistics.Win.Appearance appearance238 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool209 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnChangeStyles");
            Infragistics.Win.Appearance appearance239 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool210 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnCheckForUpdate");
            Infragistics.Win.Appearance appearance240 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool211 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnBangCanDoiTK");
            Infragistics.Win.Appearance appearance241 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool212 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnSoKTQuyTM");
            Infragistics.Win.Appearance appearance242 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool213 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btSoTGNH");
            Infragistics.Win.Appearance appearance243 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool214 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnBangKeSoDuNH");
            Infragistics.Win.Appearance appearance244 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool215 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnSoChiTietBanHang");
            Infragistics.Win.Appearance appearance245 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool216 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnSoNhatKyBH");
            Infragistics.Win.Appearance appearance246 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool217 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnTHCongNoPhaiThu");
            Infragistics.Win.Appearance appearance247 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool218 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnCTCNPTKH");
            Infragistics.Win.Appearance appearance248 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool219 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnTongHopNoThuGroupKH");
            Infragistics.Win.Appearance appearance249 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool220 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnTongCongNoPTNCC");
            Infragistics.Win.Appearance appearance250 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool221 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnSoNhatKyMuaHang");
            Infragistics.Win.Appearance appearance251 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool222 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnSoChiTietCacTK");
            Infragistics.Win.Appearance appearance252 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool223 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnSoNhatKyChung");
            Infragistics.Win.Appearance appearance253 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool224 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnB01aDNN");
            Infragistics.Win.Appearance appearance254 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool225 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnB01bDNN");
            Infragistics.Win.Appearance appearance255 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ComboBoxTool comboBoxTool1 = new Infragistics.Win.UltraWinToolbars.ComboBoxTool("CAT_MaterialTools");
            Infragistics.Win.ValueList valueList1 = new Infragistics.Win.ValueList(0);
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool226 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnInBaoCao");
            Infragistics.Win.Appearance appearance256 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool227 = new Infragistics.Win.UltraWinToolbars.ButtonTool("CAT_CCDC");
            Infragistics.Win.Appearance appearance257 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool228 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnQuyDinhLuongThueBh");
            Infragistics.Win.Appearance appearance258 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool229 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Change_sesssion");
            Infragistics.Win.Appearance appearance259 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance260 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool230 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnChooseNoteBook");
            Infragistics.Win.Appearance appearance261 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance262 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool231 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnSearchVoucher");
            Infragistics.Win.Appearance appearance263 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance264 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool232 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ResetVoucherNo");
            Infragistics.Win.Appearance appearance265 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool233 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnImportExcel");
            Infragistics.Win.Appearance appearance266 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance267 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool234 = new Infragistics.Win.UltraWinToolbars.ButtonTool("FMCReceipt");
            Infragistics.Win.Appearance appearance268 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool235 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnGuide");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool236 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnUltraviewer");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool237 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnTeamviewer");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool238 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnInforProduct");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool239 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnHuongDan");
            Infragistics.Win.Appearance appearance269 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool240 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnUltraviewer1");
            Infragistics.Win.Appearance appearance270 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance271 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool241 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnTeamviewer1");
            Infragistics.Win.Appearance appearance272 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance273 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool242 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnInfo");
            Infragistics.Win.Appearance appearance274 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool243 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnXuLyChungTu");
            Infragistics.Win.Appearance appearance275 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance276 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool244 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnPrintAll");
            Infragistics.Win.Appearance appearance277 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance278 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool245 = new Infragistics.Win.UltraWinToolbars.ButtonTool("btnImportChungtu");
            Infragistics.Win.Appearance appearance279 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ComboBoxTool comboBoxTool2 = new Infragistics.Win.UltraWinToolbars.ComboBoxTool("btnDonwloadHDSD");
            Infragistics.Win.ValueList valueList2 = new Infragistics.Win.ValueList(0);
            Infragistics.Win.UltraWinToolbars.ComboBoxTool comboBoxTool3 = new Infragistics.Win.UltraWinToolbars.ComboBoxTool("btnUltraviewer2");
            Infragistics.Win.ValueList valueList3 = new Infragistics.Win.ValueList(0);
            Infragistics.Win.UltraWinToolbars.ComboBoxTool comboBoxTool4 = new Infragistics.Win.UltraWinToolbars.ComboBoxTool("btnTeamviewer2");
            Infragistics.Win.ValueList valueList4 = new Infragistics.Win.ValueList(0);
            Infragistics.Win.UltraWinToolbars.ComboBoxTool comboBoxTool5 = new Infragistics.Win.UltraWinToolbars.ComboBoxTool("btnHuongDanSuDung");
            Infragistics.Win.Appearance appearance280 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueList valueList5 = new Infragistics.Win.ValueList(0);
            Infragistics.Win.UltraWinToolbars.ComboBoxTool comboBoxTool6 = new Infragistics.Win.UltraWinToolbars.ComboBoxTool("btnHDSD");
            Infragistics.Win.Appearance appearance281 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueList valueList6 = new Infragistics.Win.ValueList(0);
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel1 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel2 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel3 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel4 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel5 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
            Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel6 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel7 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FMain));
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance("Tax", 13884947);
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup ultraExplorerBarGroup1 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup ultraExplorerBarGroup2 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem1 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem2 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem3 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem4 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem5 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem6 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem7 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem8 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup ultraExplorerBarGroup3 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem9 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem10 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem11 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem12 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem13 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem14 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem15 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem16 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem17 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup ultraExplorerBarGroup4 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem18 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem19 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem20 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem21 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem22 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem23 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem24 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem25 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem26 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem27 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem28 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem29 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem30 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup ultraExplorerBarGroup5 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem31 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem32 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem33 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem34 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem35 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem36 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem37 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem38 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem39 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem40 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem41 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup ultraExplorerBarGroup6 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem42 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem43 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem44 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem45 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem46 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem47 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem48 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem49 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem50 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem51 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup ultraExplorerBarGroup7 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem52 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem53 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem54 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem55 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem56 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem57 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem58 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem59 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem60 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem61 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem62 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup ultraExplorerBarGroup8 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem63 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem64 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem65 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem66 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup ultraExplorerBarGroup9 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem67 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem68 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem69 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem70 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem71 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem72 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem73 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem74 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem75 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem76 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem77 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup ultraExplorerBarGroup10 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem78 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem79 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem80 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem81 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem82 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem83 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup ultraExplorerBarGroup11 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem84 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem85 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem86 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem87 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem88 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem89 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem90 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem91 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup ultraExplorerBarGroup12 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem92 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem93 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem94 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem95 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem96 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem97 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem98 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem99 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem100 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem101 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem102 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup ultraExplorerBarGroup13 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem103 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem104 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem105 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem106 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem107 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem108 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem109 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem110 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem ultraExplorerBarItem111 = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinScrollBar.ScrollBarLook scrollBarLook1 = new Infragistics.Win.UltraWinScrollBar.ScrollBarLook();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            this.splitContainer1_Fill_Panel = new Infragistics.Win.Misc.UltraPanel();
            this.ClientArea_Fill_Panel = new Infragistics.Win.Misc.UltraPanel();
            this.ClientArea_Fill_Panel_1 = new Infragistics.Win.Misc.UltraPanel();
            this._ClientArea_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._ClientArea_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._ClientArea_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._ClientArea_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.cms4uExBar = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnAddNewGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.btnShowAddNewGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.btnDeleteGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnuExBarOption = new System.Windows.Forms.ToolStripMenuItem();
            this.popAddNewGroup = new Infragistics.Win.Misc.UltraPopupControlContainer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupMainMenu = new Infragistics.Win.Misc.UltraGroupBox();
            this.palMainMenu = new Infragistics.Win.Misc.UltraPanel();
            this.palUserControl = new Infragistics.Win.Misc.UltraPanel();
            this.pnlAddNewGroup = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtToolTipNewGroup = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.btnApplyAddNewGroup = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNewCaptionGroup = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._MainFrm_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.uToolbarsManager = new Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(this.components);
            this.uSttBar = new Infragistics.Win.UltraWinStatusBar.UltraStatusBar();
            this._MainFrm_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._MainFrm_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._MainFrm_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.uExBar = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar();
            this.splitContainer1_Fill_Panel.ClientArea.SuspendLayout();
            this.ClientArea_Fill_Panel.ClientArea.SuspendLayout();
            this.cms4uExBar.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupMainMenu)).BeginInit();
            this.groupMainMenu.SuspendLayout();
            this.palMainMenu.ClientArea.SuspendLayout();
            this.palUserControl.ClientArea.SuspendLayout();
            this.pnlAddNewGroup.ClientArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtToolTipNewGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNewCaptionGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uToolbarsManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSttBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uExBar)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1_Fill_Panel
            // 
            // 
            // 
            // 
            this.splitContainer1_Fill_Panel.ClientArea.Controls.Add(this.ClientArea_Fill_Panel);
            this.splitContainer1_Fill_Panel.ClientArea.Controls.Add(this._ClientArea_Toolbars_Dock_Area_Left);
            this.splitContainer1_Fill_Panel.ClientArea.Controls.Add(this._ClientArea_Toolbars_Dock_Area_Right);
            this.splitContainer1_Fill_Panel.ClientArea.Controls.Add(this._ClientArea_Toolbars_Dock_Area_Bottom);
            this.splitContainer1_Fill_Panel.ClientArea.Controls.Add(this._ClientArea_Toolbars_Dock_Area_Top);
            this.splitContainer1_Fill_Panel.Controls.Add(this.splitContainer1_Fill_Panel.ClientArea);
            this.splitContainer1_Fill_Panel.Cursor = System.Windows.Forms.Cursors.Default;
            this.splitContainer1_Fill_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1_Fill_Panel.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1_Fill_Panel.Name = "splitContainer1_Fill_Panel";
            this.splitContainer1_Fill_Panel.Size = new System.Drawing.Size(829, 254);
            this.splitContainer1_Fill_Panel.TabIndex = 0;
            // 
            // ClientArea_Fill_Panel
            // 
            // 
            // 
            // 
            this.ClientArea_Fill_Panel.ClientArea.Controls.Add(this.ClientArea_Fill_Panel_1);
            this.ClientArea_Fill_Panel.Controls.Add(this.ClientArea_Fill_Panel.ClientArea);
            this.ClientArea_Fill_Panel.Cursor = System.Windows.Forms.Cursors.Default;
            this.ClientArea_Fill_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ClientArea_Fill_Panel.Location = new System.Drawing.Point(0, 0);
            this.ClientArea_Fill_Panel.Name = "ClientArea_Fill_Panel";
            this.ClientArea_Fill_Panel.Size = new System.Drawing.Size(829, 254);
            this.ClientArea_Fill_Panel.TabIndex = 0;
            // 
            // ClientArea_Fill_Panel_1
            // 
            this.ClientArea_Fill_Panel_1.Controls.Add(this.ClientArea_Fill_Panel_1.ClientArea);
            this.ClientArea_Fill_Panel_1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ClientArea_Fill_Panel_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ClientArea_Fill_Panel_1.Location = new System.Drawing.Point(0, 0);
            this.ClientArea_Fill_Panel_1.Name = "ClientArea_Fill_Panel_1";
            this.ClientArea_Fill_Panel_1.Size = new System.Drawing.Size(829, 254);
            this.ClientArea_Fill_Panel_1.TabIndex = 0;
            // 
            // _ClientArea_Toolbars_Dock_Area_Left
            // 
            this._ClientArea_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._ClientArea_Toolbars_Dock_Area_Left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._ClientArea_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
            this._ClientArea_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.SystemColors.ControlText;
            this._ClientArea_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 0);
            this._ClientArea_Toolbars_Dock_Area_Left.Name = "_ClientArea_Toolbars_Dock_Area_Left";
            this._ClientArea_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(0, 254);
            // 
            // _ClientArea_Toolbars_Dock_Area_Right
            // 
            this._ClientArea_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._ClientArea_Toolbars_Dock_Area_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._ClientArea_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right;
            this._ClientArea_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.SystemColors.ControlText;
            this._ClientArea_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(829, 0);
            this._ClientArea_Toolbars_Dock_Area_Right.Name = "_ClientArea_Toolbars_Dock_Area_Right";
            this._ClientArea_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(0, 254);
            // 
            // _ClientArea_Toolbars_Dock_Area_Bottom
            // 
            this._ClientArea_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._ClientArea_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._ClientArea_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom;
            this._ClientArea_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.SystemColors.ControlText;
            this._ClientArea_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 254);
            this._ClientArea_Toolbars_Dock_Area_Bottom.Name = "_ClientArea_Toolbars_Dock_Area_Bottom";
            this._ClientArea_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(829, 0);
            // 
            // _ClientArea_Toolbars_Dock_Area_Top
            // 
            this._ClientArea_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._ClientArea_Toolbars_Dock_Area_Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._ClientArea_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top;
            this._ClientArea_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.SystemColors.ControlText;
            this._ClientArea_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
            this._ClientArea_Toolbars_Dock_Area_Top.Name = "_ClientArea_Toolbars_Dock_Area_Top";
            this._ClientArea_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(829, 0);
            // 
            // cms4uExBar
            // 
            this.cms4uExBar.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cms4uExBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddNewGroup,
            this.btnDeleteGroup,
            this.toolStripSeparator1,
            this.btnuExBarOption});
            this.cms4uExBar.Name = "cms4uExBar";
            this.cms4uExBar.Size = new System.Drawing.Size(182, 88);
            this.cms4uExBar.Closing += new System.Windows.Forms.ToolStripDropDownClosingEventHandler(this.cms4uExBar_Closing);
            this.cms4uExBar.Opening += new System.ComponentModel.CancelEventHandler(this.cms4uExBar_Opening);
            // 
            // btnAddNewGroup
            // 
            this.btnAddNewGroup.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnShowAddNewGroup});
            this.btnAddNewGroup.Image = global::Accounting.Properties.Resources._1437136799_add;
            this.btnAddNewGroup.Name = "btnAddNewGroup";
            this.btnAddNewGroup.Size = new System.Drawing.Size(181, 26);
            this.btnAddNewGroup.Text = "Thêm vào mục mới";
            // 
            // btnShowAddNewGroup
            // 
            this.btnShowAddNewGroup.Name = "btnShowAddNewGroup";
            this.btnShowAddNewGroup.Size = new System.Drawing.Size(67, 22);
            this.btnShowAddNewGroup.Paint += new System.Windows.Forms.PaintEventHandler(this.btnShowAddNewGroup_Paint);
            // 
            // btnDeleteGroup
            // 
            this.btnDeleteGroup.Image = global::Accounting.Properties.Resources._1437136435_14;
            this.btnDeleteGroup.Name = "btnDeleteGroup";
            this.btnDeleteGroup.Size = new System.Drawing.Size(181, 26);
            this.btnDeleteGroup.Text = "Xóa mục hiện tại";
            this.btnDeleteGroup.Click += new System.EventHandler(this.btnDeleteGroup_Click);
            this.btnDeleteGroup.MouseHover += new System.EventHandler(this.btnDeleteGroup_MouseHover);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(178, 6);
            // 
            // btnuExBarOption
            // 
            this.btnuExBarOption.Image = global::Accounting.Properties.Resources.settings;
            this.btnuExBarOption.Name = "btnuExBarOption";
            this.btnuExBarOption.Size = new System.Drawing.Size(181, 26);
            this.btnuExBarOption.Text = "Tùy chỉnh";
            this.btnuExBarOption.Click += new System.EventHandler(this.btnuExBarOption_Click);
            this.btnuExBarOption.MouseHover += new System.EventHandler(this.btnuExBarOption_MouseHover);
            // 
            // popAddNewGroup
            // 
            this.popAddNewGroup.UseAppStyling = false;
            this.popAddNewGroup.Opening += new System.ComponentModel.CancelEventHandler(this.popAddNewGroup_Opening);
            this.popAddNewGroup.Closed += new System.EventHandler(this.popAddNewGroup_Closed);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Blue;
            this.panel1.Controls.Add(this.groupMainMenu);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(197, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(814, 690);
            this.panel1.TabIndex = 0;
            // 
            // groupMainMenu
            // 
            this.groupMainMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupMainMenu.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.groupMainMenu.Controls.Add(this.palMainMenu);
            this.groupMainMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance5.FontData.BoldAsString = "True";
            appearance5.FontData.SizeInPoints = 13F;
            this.groupMainMenu.HeaderAppearance = appearance5;
            this.groupMainMenu.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOutsideBorder;
            this.groupMainMenu.Location = new System.Drawing.Point(6, 0);
            this.groupMainMenu.Name = "groupMainMenu";
            this.groupMainMenu.Size = new System.Drawing.Size(808, 717);
            this.groupMainMenu.TabIndex = 1;
            this.groupMainMenu.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            this.groupMainMenu.Paint += new System.Windows.Forms.PaintEventHandler(this.groupMainMenu_Paint);
            // 
            // palMainMenu
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.BackColor2 = System.Drawing.Color.Transparent;
            appearance1.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched;
            appearance1.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.palMainMenu.Appearance = appearance1;
            // 
            // 
            // 
            this.palMainMenu.ClientArea.Controls.Add(this.palUserControl);
            this.palMainMenu.Controls.Add(this.palMainMenu.ClientArea);
            this.palMainMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.palMainMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.palMainMenu.Location = new System.Drawing.Point(3, 3);
            this.palMainMenu.Name = "palMainMenu";
            this.palMainMenu.Size = new System.Drawing.Size(802, 518);
            this.palMainMenu.TabIndex = 6;
            // 
            // palUserControl
            // 
            this.palUserControl.AutoSize = true;
            // 
            // 
            // 
            this.palUserControl.ClientArea.Controls.Add(this.pnlAddNewGroup);
            this.palUserControl.Controls.Add(this.palUserControl.ClientArea);
            this.palUserControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.palUserControl.Location = new System.Drawing.Point(0, 0);
            this.palUserControl.Name = "palUserControl";
            this.palUserControl.Size = new System.Drawing.Size(802, 0);
            this.palUserControl.TabIndex = 0;
            // 
            // pnlAddNewGroup
            // 
            this.pnlAddNewGroup.BorderStyle = Infragistics.Win.UIElementBorderStyle.RaisedSoft;
            // 
            // 
            // 
            this.pnlAddNewGroup.ClientArea.Controls.Add(this.ultraLabel2);
            this.pnlAddNewGroup.ClientArea.Controls.Add(this.txtToolTipNewGroup);
            this.pnlAddNewGroup.ClientArea.Controls.Add(this.btnApplyAddNewGroup);
            this.pnlAddNewGroup.ClientArea.Controls.Add(this.ultraLabel1);
            this.pnlAddNewGroup.ClientArea.Controls.Add(this.txtNewCaptionGroup);
            this.pnlAddNewGroup.Controls.Add(this.pnlAddNewGroup.ClientArea);
            this.pnlAddNewGroup.Location = new System.Drawing.Point(96, 39);
            this.pnlAddNewGroup.Name = "pnlAddNewGroup";
            this.pnlAddNewGroup.Size = new System.Drawing.Size(268, 112);
            this.pnlAddNewGroup.TabIndex = 0;
            this.pnlAddNewGroup.Visible = false;
            // 
            // ultraLabel2
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance2;
            this.ultraLabel2.Location = new System.Drawing.Point(15, 44);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(53, 22);
            this.ultraLabel2.TabIndex = 5;
            this.ultraLabel2.Text = "Tên gợi ý";
            // 
            // txtToolTipNewGroup
            // 
            this.txtToolTipNewGroup.AutoSize = false;
            this.txtToolTipNewGroup.Location = new System.Drawing.Point(83, 44);
            this.txtToolTipNewGroup.Name = "txtToolTipNewGroup";
            this.txtToolTipNewGroup.Size = new System.Drawing.Size(164, 22);
            this.txtToolTipNewGroup.TabIndex = 3;
            // 
            // btnApplyAddNewGroup
            // 
            appearance3.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnApplyAddNewGroup.Appearance = appearance3;
            this.btnApplyAddNewGroup.Location = new System.Drawing.Point(94, 78);
            this.btnApplyAddNewGroup.Name = "btnApplyAddNewGroup";
            this.btnApplyAddNewGroup.Size = new System.Drawing.Size(80, 23);
            this.btnApplyAddNewGroup.TabIndex = 4;
            this.btnApplyAddNewGroup.Text = "Đồng ý";
            // 
            // ultraLabel1
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance4;
            this.ultraLabel1.Location = new System.Drawing.Point(15, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(53, 22);
            this.ultraLabel1.TabIndex = 2;
            this.ultraLabel1.Text = "Tên mục";
            // 
            // txtNewCaptionGroup
            // 
            this.txtNewCaptionGroup.AutoSize = false;
            this.txtNewCaptionGroup.Location = new System.Drawing.Point(83, 16);
            this.txtNewCaptionGroup.Name = "txtNewCaptionGroup";
            this.txtNewCaptionGroup.Size = new System.Drawing.Size(164, 22);
            this.txtNewCaptionGroup.TabIndex = 1;
            // 
            // _MainFrm_Toolbars_Dock_Area_Left
            // 
            this._MainFrm_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._MainFrm_Toolbars_Dock_Area_Left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._MainFrm_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
            this._MainFrm_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.SystemColors.ControlText;
            this._MainFrm_Toolbars_Dock_Area_Left.InitialResizeAreaExtent = 4;
            this._MainFrm_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 28);
            this._MainFrm_Toolbars_Dock_Area_Left.Name = "_MainFrm_Toolbars_Dock_Area_Left";
            this._MainFrm_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(4, 690);
            this._MainFrm_Toolbars_Dock_Area_Left.ToolbarsManager = this.uToolbarsManager;
            // 
            // uToolbarsManager
            // 
            this.uToolbarsManager.DesignerFlags = 1;
            this.uToolbarsManager.DockWithinContainer = this;
            this.uToolbarsManager.DockWithinContainerBaseType = typeof(Accounting.StartApplication);
            this.uToolbarsManager.FormDisplayStyle = Infragistics.Win.UltraWinToolbars.FormDisplayStyle.RoundedSizable;
            this.uToolbarsManager.MenuAnimationStyle = Infragistics.Win.UltraWinToolbars.MenuAnimationStyle.Slide;
            appearance100.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.MenuSettings.Appearance = appearance100;
            appearance101.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.MenuSettings.EditAppearance = appearance101;
            appearance102.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.MenuSettings.HotTrackAppearance = appearance102;
            appearance103.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.MenuSettings.IconAreaAppearance = appearance103;
            appearance104.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.MenuSettings.IconAreaExpandedAppearance = appearance104;
            appearance105.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.MenuSettings.PressedAppearance = appearance105;
            appearance106.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.MenuSettings.SideStripAppearance = appearance106;
            appearance107.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.MenuSettings.ToolAppearance = appearance107;
            appearance108.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance108.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.uToolbarsManager.NavigationToolbar.MenuSettings.Appearance = appearance108;
            appearance109.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.NavigationToolbar.MenuSettings.HotTrackAppearance = appearance109;
            appearance110.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.NavigationToolbar.MenuSettings.IconAreaAppearance = appearance110;
            appearance111.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.NavigationToolbar.MenuSettings.PressedAppearance = appearance111;
            this.uToolbarsManager.Office2007UICompatibility = false;
            this.uToolbarsManager.Ribbon.ApplicationMenu2010.NavigationMenu.NonInheritedTools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool1});
            this.uToolbarsManager.Ribbon.ApplicationMenuButtonImage = global::Accounting.Properties.Resources.logox48;
            appearance112.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.Ribbon.CaptionAreaActiveAppearance = appearance112;
            appearance113.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.Ribbon.CaptionAreaAppearance = appearance113;
            appearance114.Image = ((object)(resources.GetObject("appearance114.Image")));
            this.uToolbarsManager.Ribbon.FileMenuButtonActiveAppearance = appearance114;
            appearance115.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.Ribbon.FileMenuButtonAppearance = appearance115;
            appearance116.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.Ribbon.FileMenuButtonHotTrackAppearance = appearance116;
            appearance117.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance117.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance117.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(227)))), ((int)(((byte)(228)))));
            appearance117.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance117.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            this.uToolbarsManager.Ribbon.GroupSettings.Appearance = appearance117;
            this.uToolbarsManager.Ribbon.GroupSettings.CanCollapse = Infragistics.Win.DefaultableBoolean.False;
            appearance118.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance118.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance118.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(146)))), ((int)(((byte)(146)))));
            this.uToolbarsManager.Ribbon.GroupSettings.CaptionAppearance = appearance118;
            appearance119.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance119.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.Ribbon.GroupSettings.CollapsedAppearance = appearance119;
            appearance120.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.Ribbon.GroupSettings.EditAppearance = appearance120;
            appearance121.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance121.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(227)))), ((int)(((byte)(228)))));
            appearance121.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance121.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            this.uToolbarsManager.Ribbon.GroupSettings.HotTrackAppearance = appearance121;
            appearance122.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance122.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance122.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(146)))), ((int)(((byte)(146)))));
            this.uToolbarsManager.Ribbon.GroupSettings.HotTrackCaptionAppearance = appearance122;
            appearance123.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance123.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance123.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.uToolbarsManager.Ribbon.GroupSettings.HotTrackCollapsedAppearance = appearance123;
            appearance124.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance124.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance124.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(227)))), ((int)(((byte)(228)))));
            appearance124.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance124.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            this.uToolbarsManager.Ribbon.GroupSettings.HotTrackGroupAppearance = appearance124;
            appearance125.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance125.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.Ribbon.GroupSettings.PressedAppearance = appearance125;
            appearance126.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance126.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.Ribbon.GroupSettings.PressedCollapsedAppearance = appearance126;
            this.uToolbarsManager.Ribbon.GroupSettings.ShowToolTips = Infragistics.Win.DefaultableBoolean.True;
            appearance127.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance127.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(227)))), ((int)(((byte)(228)))));
            appearance127.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance127.ForeColor = System.Drawing.Color.Black;
            this.uToolbarsManager.Ribbon.GroupSettings.ToolAppearance = appearance127;
            ribbonTab1.Caption = "Hệ thống";
            ribbonGroup1.Caption = "Hệ thống";
            buttonTool2.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool3.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool4.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool5.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool6.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool7.InstanceProps.MinimumSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool8.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool9.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool10.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool11.InstanceProps.MinimumSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup1.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool2,
            buttonTool3,
            buttonTool4,
            buttonTool5,
            buttonTool6,
            buttonTool7,
            buttonTool8,
            buttonTool9,
            buttonTool10,
            buttonTool11});
            ribbonTab1.Groups.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonGroup[] {
            ribbonGroup1});
            ribbonTab2.Caption = "Danh Mục";
            ribbonGroup2.Caption = "Hệ thống tài khoản";
            ribbonGroup2.LayoutAlignment = Infragistics.Win.UltraWinToolbars.RibbonGroupLayoutAlignment.Center;
            ribbonGroup2.PreferredToolSize = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool13.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Normal;
            buttonTool14.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Normal;
            buttonTool15.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Normal;
            ribbonGroup2.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool12,
            buttonTool13,
            buttonTool14,
            buttonTool15});
            ribbonGroup3.Caption = "Nhóm đối tượng";
            buttonTool16.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup3.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool16,
            buttonTool17,
            buttonTool18,
            buttonTool19});
            ribbonGroup4.Caption = "Nhóm VTHH, TSCĐ, Kho";
            buttonTool20.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup4.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool20,
            buttonTool21,
            buttonTool22,
            buttonTool23});
            ribbonGroup5.Caption = "Nhóm ngân hàng";
            buttonTool24.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup5.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool24,
            buttonTool25,
            buttonTool26,
            buttonTool27,
            buttonTool28});
            ribbonGroup6.Caption = "Lương nhân viên, chi phí";
            buttonTool29.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup6.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool29,
            buttonTool30,
            buttonTool31,
            buttonTool32});
            ribbonGroup7.Caption = "Khác";
            popupMenuTool1.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup7.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            popupMenuTool1});
            ribbonTab2.Groups.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonGroup[] {
            ribbonGroup2,
            ribbonGroup3,
            ribbonGroup4,
            ribbonGroup5,
            ribbonGroup6,
            ribbonGroup7});
            ribbonTab3.Caption = "Nghiệp vụ";
            ribbonGroup8.Caption = "Tiền và tương đương tiền";
            buttonTool33.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup8.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool33,
            buttonTool34,
            buttonTool35,
            buttonTool36});
            ribbonGroup9.Caption = "Mua hàng và công nợ phải trả";
            buttonTool37.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup9.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool37,
            buttonTool38,
            buttonTool39,
            buttonTool40});
            ribbonGroup10.Caption = "Bán hàng và công nợ phải thu";
            buttonTool41.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup10.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool41,
            buttonTool42,
            buttonTool43,
            buttonTool44});
            ribbonGroup11.Caption = "Kho";
            buttonTool45.InstanceProps.MinimumSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup11.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool45,
            buttonTool46,
            buttonTool47,
            buttonTool48});
            ribbonGroup12.Caption = "Tài sản cố định";
            buttonTool49.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup12.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool49,
            buttonTool50,
            buttonTool51,
            buttonTool52});
            ribbonGroup13.Caption = "Tổng hợp";
            buttonTool53.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool55.InstanceProps.MinimumSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Normal;
            buttonTool56.InstanceProps.MinimumSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Normal;
            ribbonGroup13.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool53,
            buttonTool54,
            buttonTool55,
            buttonTool56});
            ribbonGroup14.Caption = "Tiền lương";
            buttonTool57.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup14.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool57,
            buttonTool58,
            buttonTool59,
            buttonTool60});
            ribbonGroup14.Visible = false;
            ribbonTab3.Groups.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonGroup[] {
            ribbonGroup8,
            ribbonGroup9,
            ribbonGroup10,
            ribbonGroup11,
            ribbonGroup12,
            ribbonGroup13,
            ribbonGroup14});
            ribbonTab3.GroupSettings.CanCollapse = Infragistics.Win.DefaultableBoolean.True;
            ribbonTab4.Caption = "Tiện ích";
            ribbonGroup15.Caption = "Tiện ích";
            buttonTool61.InstanceProps.MinimumSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool62.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool63.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool64.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool65.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool66.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool67.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool68.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool69.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool70.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool71.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool72.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool73.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool74.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup15.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool61,
            buttonTool62,
            buttonTool63,
            buttonTool64,
            buttonTool65,
            buttonTool66,
            buttonTool67,
            buttonTool68,
            buttonTool69,
            buttonTool70,
            buttonTool71,
            buttonTool72,
            buttonTool73,
            buttonTool74});
            ribbonTab4.Groups.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonGroup[] {
            ribbonGroup15});
            ribbonTab5.Caption = "Báo cáo";
            ribbonGroup16.Caption = "";
            buttonTool75.InstanceProps.MinimumSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool75.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup16.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool75});
            ribbonTab5.Groups.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonGroup[] {
            ribbonGroup16});
            ribbonTab6.Caption = "Trợ giúp";
            ribbonGroup17.Caption = "Trợ giúp";
            buttonTool81.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool82.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool89.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup17.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool76,
            buttonTool77,
            buttonTool78,
            buttonTool79,
            buttonTool80,
            buttonTool81,
            buttonTool82,
            buttonTool83,
            buttonTool84,
            buttonTool85,
            buttonTool86,
            buttonTool87,
            buttonTool88,
            buttonTool89});
            ribbonTab6.Groups.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonGroup[] {
            ribbonGroup17});
            this.uToolbarsManager.Ribbon.NonInheritedRibbonTabs.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonTab[] {
            ribbonTab1,
            ribbonTab2,
            ribbonTab3,
            ribbonTab4,
            ribbonTab5,
            ribbonTab6});
            appearance128.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.Ribbon.RibbonAreaAppearance = appearance128;
            appearance129.BackColor = System.Drawing.Color.White;
            appearance129.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance129.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(219)))), ((int)(((byte)(220)))));
            appearance129.BorderColor2 = System.Drawing.Color.Transparent;
            appearance129.BorderColor3DBase = System.Drawing.Color.Transparent;
            appearance129.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(91)))));
            this.uToolbarsManager.Ribbon.TabAreaAppearance = appearance129;
            appearance130.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            appearance130.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance130.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance130.ForeColor = System.Drawing.Color.Black;
            this.uToolbarsManager.Ribbon.TabSettings.ActiveTabItemAppearance = appearance130;
            appearance131.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance131.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(219)))), ((int)(((byte)(220)))));
            appearance131.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance131.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance131.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(91)))));
            this.uToolbarsManager.Ribbon.TabSettings.Appearance = appearance131;
            appearance132.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.Ribbon.TabSettings.ClientAreaAppearance = appearance132;
            appearance133.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance133.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance133.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(219)))), ((int)(((byte)(220)))));
            appearance133.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance133.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            this.uToolbarsManager.Ribbon.TabSettings.HotTrackSelectedTabItemAppearance = appearance133;
            appearance134.BackColor = System.Drawing.Color.White;
            appearance134.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance134.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(236)))));
            appearance134.BorderColor2 = System.Drawing.Color.White;
            appearance134.BorderColor3DBase = System.Drawing.Color.White;
            this.uToolbarsManager.Ribbon.TabSettings.HotTrackTabItemAppearance = appearance134;
            appearance135.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance135.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance135.ForeColor = System.Drawing.Color.Black;
            this.uToolbarsManager.Ribbon.TabSettings.SelectedAppearance = appearance135;
            appearance136.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance136.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance136.ForeColor = System.Drawing.Color.Black;
            this.uToolbarsManager.Ribbon.TabSettings.SelectedTabItemAppearance = appearance136;
            appearance137.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.Ribbon.TabSettings.TabItemAppearance = appearance137;
            this.uToolbarsManager.Ribbon.Visible = true;
            this.uToolbarsManager.SettingsKey = "";
            this.uToolbarsManager.ShowFullMenusDelay = 500;
            this.uToolbarsManager.ShowMenuShadows = Infragistics.Win.DefaultableBoolean.True;
            this.uToolbarsManager.ShowShortcutsInToolTips = true;
            appearance138.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance138.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.uToolbarsManager.ToolbarSettings.Appearance = appearance138;
            appearance139.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.ToolbarSettings.DockedAppearance = appearance139;
            appearance140.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.ToolbarSettings.EditAppearance = appearance140;
            appearance141.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.ToolbarSettings.FloatingAppearance = appearance141;
            appearance142.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.ToolbarSettings.HotTrackAppearance = appearance142;
            appearance143.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uToolbarsManager.ToolbarSettings.PressedAppearance = appearance143;
            appearance144.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance144.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.uToolbarsManager.ToolbarSettings.ToolAppearance = appearance144;
            buttonTool90.SharedPropsInternal.Caption = "Nhóm đối tượng";
            appearance145.Image = ((object)(resources.GetObject("appearance145.Image")));
            buttonTool91.SharedPropsInternal.AppearancesSmall.Appearance = appearance145;
            buttonTool91.SharedPropsInternal.Caption = "Danh mục Full";
            buttonTool91.SharedPropsInternal.Visible = false;
            appearance146.Image = ((object)(resources.GetObject("appearance146.Image")));
            buttonTool92.SharedPropsInternal.AppearancesSmall.Appearance = appearance146;
            buttonTool92.SharedPropsInternal.Caption = "Ngân hàng";
            buttonTool92.Tag = "Category";
            appearance147.Image = ((object)(resources.GetObject("appearance147.Image")));
            appearance147.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance147.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool93.SharedPropsInternal.AppearancesSmall.Appearance = appearance147;
            buttonTool93.SharedPropsInternal.Caption = "Phòng ban";
            buttonTool93.Tag = "Category";
            appearance148.Image = global::Accounting.Properties.Resources.hethong_taikhoan;
            buttonTool94.SharedPropsInternal.AppearancesSmall.Appearance = appearance148;
            buttonTool94.SharedPropsInternal.Caption = "Loại khách hàng, Nhà cung cấp";
            buttonTool94.Tag = "Category";
            appearance149.Image = global::Accounting.Properties.Resources.Usercard;
            appearance149.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance149.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool95.SharedPropsInternal.AppearancesSmall.Appearance = appearance149;
            buttonTool95.SharedPropsInternal.Caption = "Nhóm khách hàng, nhà cung cấp";
            buttonTool95.Tag = "Category";
            appearance150.Image = global::Accounting.Properties.Resources.khachhang_ncc;
            appearance150.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance150.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool96.SharedPropsInternal.AppearancesLarge.Appearance = appearance150;
            appearance151.Image = global::Accounting.Properties.Resources.User_group;
            buttonTool96.SharedPropsInternal.AppearancesSmall.Appearance = appearance151;
            buttonTool96.SharedPropsInternal.Caption = "Khách hàng, nhà cung cấp";
            buttonTool96.Tag = "Category";
            buttonTool97.SharedPropsInternal.Caption = "Thoát";
            buttonTool98.SharedPropsInternal.Caption = "Sổ cái - Sổ kho";
            buttonTool98.SharedPropsInternal.Visible = false;
            appearance152.Image = global::Accounting.Properties.Resources.balance;
            buttonTool99.SharedPropsInternal.AppearancesLarge.Appearance = appearance152;
            buttonTool99.SharedPropsInternal.Caption = "Nhập số dư đầu kỳ";
            buttonTool100.SharedPropsInternal.Caption = "Thiết lập Tùy chọn";
            appearance153.Image = global::Accounting.Properties.Resources.ngayhachtoan;
            appearance153.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance153.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool101.SharedPropsInternal.AppearancesLarge.Appearance = appearance153;
            buttonTool101.SharedPropsInternal.Caption = "Ngày hạch toán";
            appearance154.Image = global::Accounting.Properties.Resources.history;
            appearance154.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance154.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool102.SharedPropsInternal.AppearancesLarge.Appearance = appearance154;
            buttonTool102.SharedPropsInternal.Caption = "Nhật ký truy cập";
            appearance155.Image = global::Accounting.Properties.Resources.doimatkhau;
            appearance155.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance155.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool103.SharedPropsInternal.AppearancesLarge.Appearance = appearance155;
            buttonTool103.SharedPropsInternal.Caption = "Đổi mật khẩu";
            appearance156.Image = global::Accounting.Properties.Resources.id_card;
            appearance156.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance156.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool104.SharedPropsInternal.AppearancesLarge.Appearance = appearance156;
            buttonTool104.SharedPropsInternal.Caption = "Thay đổi TT cá nhân";
            buttonTool104.SharedPropsInternal.CustomizerCaption = "Thay đổi thông tin cá nhân";
            appearance157.Image = ((object)(resources.GetObject("appearance157.Image")));
            appearance157.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance157.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool105.SharedPropsInternal.AppearancesLarge.Appearance = appearance157;
            buttonTool105.SharedPropsInternal.Caption = "Vai trò, quyền hạn";
            appearance158.Image = ((object)(resources.GetObject("appearance158.Image")));
            appearance158.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance158.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool106.SharedPropsInternal.AppearancesLarge.Appearance = appearance158;
            buttonTool106.SharedPropsInternal.Caption = "Tính năng bổ sung";
            buttonTool106.SharedPropsInternal.Visible = false;
            appearance159.Image = ((object)(resources.GetObject("appearance159.Image")));
            appearance159.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance159.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool107.SharedPropsInternal.AppearancesSmall.Appearance = appearance159;
            buttonTool107.SharedPropsInternal.Caption = "Tài khoản kết chuyển";
            buttonTool107.Tag = "Category";
            appearance160.Image = global::Accounting.Properties.Resources.group_data;
            buttonTool108.SharedPropsInternal.AppearancesSmall.Appearance = appearance160;
            buttonTool108.SharedPropsInternal.Caption = "Nhóm tài khoản";
            buttonTool108.Tag = "Category";
            appearance161.Image = global::Accounting.Properties.Resources.dinhkhoan;
            buttonTool109.SharedPropsInternal.AppearancesSmall.Appearance = appearance161;
            buttonTool109.SharedPropsInternal.Caption = "Định khoản tự động";
            buttonTool109.Tag = "Category";
            appearance162.Image = global::Accounting.Properties.Resources.NHAN_VIEN;
            appearance162.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance162.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool110.SharedPropsInternal.AppearancesSmall.Appearance = appearance162;
            buttonTool110.SharedPropsInternal.Caption = "Nhân viên";
            buttonTool110.Tag = "Category";
            appearance163.Image = global::Accounting.Properties.Resources.Shipping;
            buttonTool111.SharedPropsInternal.AppearancesLarge.Appearance = appearance163;
            appearance164.Image = global::Accounting.Properties.Resources.Shipping;
            buttonTool111.SharedPropsInternal.AppearancesSmall.Appearance = appearance164;
            buttonTool111.SharedPropsInternal.Caption = "Vật tư, hàng hóa";
            buttonTool111.Tag = "Category";
            appearance165.Image = ((object)(resources.GetObject("appearance165.Image")));
            appearance165.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance165.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool112.SharedPropsInternal.AppearancesSmall.Appearance = appearance165;
            buttonTool112.SharedPropsInternal.Caption = "Tài sản cố định";
            buttonTool112.Tag = "Category";
            appearance166.Image = global::Accounting.Properties.Resources.kho;
            appearance166.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance166.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool113.SharedPropsInternal.AppearancesSmall.Appearance = appearance166;
            buttonTool113.SharedPropsInternal.Caption = "Kho";
            buttonTool113.Tag = "Category";
            appearance167.Image = ((object)(resources.GetObject("appearance167.Image")));
            buttonTool114.SharedPropsInternal.AppearancesLarge.Appearance = appearance167;
            appearance168.Image = global::Accounting.Properties.Resources.khaibaotscd;
            buttonTool114.SharedPropsInternal.AppearancesSmall.Appearance = appearance168;
            buttonTool114.SharedPropsInternal.Caption = "Tài khoản ngân hàng";
            buttonTool114.Tag = "Category";
            appearance169.Image = ((object)(resources.GetObject("appearance169.Image")));
            appearance169.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance169.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool115.SharedPropsInternal.AppearancesSmall.Appearance = appearance169;
            buttonTool115.SharedPropsInternal.Caption = "Thẻ tín dụng";
            buttonTool115.Tag = "Category";
            appearance170.Image = ((object)(resources.GetObject("appearance170.Image")));
            appearance170.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance170.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool116.SharedPropsInternal.AppearancesSmall.Appearance = appearance170;
            buttonTool116.SharedPropsInternal.Caption = "Loại cổ phần";
            buttonTool116.SharedPropsInternal.Visible = false;
            buttonTool116.Tag = "Category";
            appearance171.Image = ((object)(resources.GetObject("appearance171.Image")));
            appearance171.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance171.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool117.SharedPropsInternal.AppearancesSmall.Appearance = appearance171;
            buttonTool117.SharedPropsInternal.Caption = "Nhà đầu tư";
            buttonTool117.SharedPropsInternal.Visible = false;
            buttonTool117.Tag = "Category";
            appearance172.Image = ((object)(resources.GetObject("appearance172.Image")));
            buttonTool118.SharedPropsInternal.AppearancesLarge.Appearance = appearance172;
            appearance173.Image = global::Accounting.Properties.Resources.loai_co_phan;
            appearance173.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance173.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool118.SharedPropsInternal.AppearancesSmall.Appearance = appearance173;
            buttonTool118.SharedPropsInternal.Caption = "Đối tượng tập hợp CP";
            buttonTool118.Tag = "Category";
            appearance174.Image = ((object)(resources.GetObject("appearance174.Image")));
            appearance174.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance174.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool119.SharedPropsInternal.AppearancesSmall.Appearance = appearance174;
            buttonTool119.SharedPropsInternal.Caption = "Biểu thuế TNCN";
            buttonTool119.SharedPropsInternal.CustomizerCaption = "Biểu thuế thu nhập cá nhân";
            buttonTool119.Tag = "Category";
            appearance175.Image = ((object)(resources.GetObject("appearance175.Image")));
            appearance175.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance175.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool120.SharedPropsInternal.AppearancesSmall.Appearance = appearance175;
            buttonTool120.SharedPropsInternal.Caption = "Ký hiệu chấm công";
            buttonTool120.Tag = "Category";
            appearance176.Image = ((object)(resources.GetObject("appearance176.Image")));
            appearance176.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance176.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool121.SharedPropsInternal.AppearancesSmall.Appearance = appearance176;
            buttonTool121.SharedPropsInternal.Caption = "Khoản mục CP";
            buttonTool121.SharedPropsInternal.CustomizerCaption = "Khoản mục chi phí";
            buttonTool121.Tag = "Category";
            appearance177.Image = global::Accounting.Properties.Resources.PHIEU_THU;
            buttonTool122.SharedPropsInternal.AppearancesSmall.Appearance = appearance177;
            buttonTool122.SharedPropsInternal.Caption = "Loại tiền";
            buttonTool122.Tag = "Category";
            appearance178.Image = ((object)(resources.GetObject("appearance178.Image")));
            appearance178.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance178.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool123.SharedPropsInternal.AppearancesSmall.Appearance = appearance178;
            buttonTool123.SharedPropsInternal.Caption = "Mục thu/chi";
            buttonTool123.Tag = "Category";
            appearance179.Image = global::Accounting.Properties.Resources.nhap_hoa_don_dich_vu;
            buttonTool124.SharedPropsInternal.AppearancesSmall.Appearance = appearance179;
            buttonTool124.SharedPropsInternal.Caption = "Loại chứng từ";
            buttonTool124.Tag = "Category";
            appearance180.Image = global::Accounting.Properties.Resources.mucthuchi2;
            buttonTool125.SharedPropsInternal.AppearancesSmall.Appearance = appearance180;
            buttonTool125.SharedPropsInternal.Caption = "Điều khoản thanh toán";
            buttonTool125.Tag = "Category";
            appearance181.Image = global::Accounting.Properties.Resources.ketoanmua_vs_phaitra2;
            buttonTool126.SharedPropsInternal.AppearancesLarge.Appearance = appearance181;
            buttonTool126.SharedPropsInternal.Caption = "Mua hàng";
            appearance182.Image = ((object)(resources.GetObject("appearance182.Image")));
            buttonTool127.SharedPropsInternal.AppearancesSmall.Appearance = appearance182;
            buttonTool127.SharedPropsInternal.Caption = "Mua dịch vụ";
            appearance183.Image = ((object)(resources.GetObject("appearance183.Image")));
            buttonTool128.SharedPropsInternal.AppearancesSmall.Appearance = appearance183;
            buttonTool128.SharedPropsInternal.Caption = "Trả tiền nhà cung cấp";
            appearance184.Image = ((object)(resources.GetObject("appearance184.Image")));
            buttonTool129.SharedPropsInternal.AppearancesSmall.Appearance = appearance184;
            buttonTool129.SharedPropsInternal.Caption = "Hàng mua trả lại, giảm giá";
            appearance185.Image = ((object)(resources.GetObject("appearance185.Image")));
            buttonTool130.SharedPropsInternal.AppearancesLarge.Appearance = appearance185;
            buttonTool130.SharedPropsInternal.Caption = "Bán hàng chưa thu tiền";
            appearance186.Image = ((object)(resources.GetObject("appearance186.Image")));
            buttonTool131.SharedPropsInternal.AppearancesSmall.Appearance = appearance186;
            buttonTool131.SharedPropsInternal.Caption = "Bán hàng thu tiền ngay";
            appearance187.Image = ((object)(resources.GetObject("appearance187.Image")));
            buttonTool132.SharedPropsInternal.AppearancesSmall.Appearance = appearance187;
            buttonTool132.SharedPropsInternal.Caption = "Thu tiền khách hàng";
            appearance188.Image = ((object)(resources.GetObject("appearance188.Image")));
            buttonTool133.SharedPropsInternal.AppearancesSmall.Appearance = appearance188;
            buttonTool133.SharedPropsInternal.Caption = "Hàng trả lại, giảm giá";
            appearance189.Image = ((object)(resources.GetObject("appearance189.Image")));
            buttonTool134.SharedPropsInternal.AppearancesLarge.Appearance = appearance189;
            buttonTool134.SharedPropsInternal.Caption = "Nhập kho";
            appearance190.Image = ((object)(resources.GetObject("appearance190.Image")));
            buttonTool135.SharedPropsInternal.AppearancesSmall.Appearance = appearance190;
            buttonTool135.SharedPropsInternal.Caption = "Xuất Kho";
            appearance191.Image = ((object)(resources.GetObject("appearance191.Image")));
            buttonTool136.SharedPropsInternal.AppearancesSmall.Appearance = appearance191;
            buttonTool136.SharedPropsInternal.Caption = "Chuyển kho";
            buttonTool137.SharedPropsInternal.Caption = "Tính giá xuất kho";
            appearance192.Image = ((object)(resources.GetObject("appearance192.Image")));
            buttonTool138.SharedPropsInternal.AppearancesSmall.Appearance = appearance192;
            buttonTool138.SharedPropsInternal.Caption = "Điều chỉnh tồn kho";
            appearance193.Image = ((object)(resources.GetObject("appearance193.Image")));
            buttonTool139.SharedPropsInternal.AppearancesLarge.Appearance = appearance193;
            buttonTool139.SharedPropsInternal.Caption = "Khai báo TSCĐ";
            buttonTool139.SharedPropsInternal.CustomizerCaption = "Khai báo tài sản cố định";
            appearance194.Image = ((object)(resources.GetObject("appearance194.Image")));
            buttonTool140.SharedPropsInternal.AppearancesSmall.Appearance = appearance194;
            buttonTool140.SharedPropsInternal.Caption = "Ghi tăng TSCĐ";
            buttonTool140.SharedPropsInternal.CustomizerCaption = "Ghi tăng tài sản cố định";
            appearance195.Image = ((object)(resources.GetObject("appearance195.Image")));
            buttonTool141.SharedPropsInternal.AppearancesSmall.Appearance = appearance195;
            buttonTool141.SharedPropsInternal.Caption = "Ghi giảm TSCĐ";
            buttonTool141.SharedPropsInternal.CustomizerCaption = "Ghi giảm tài sản cố định";
            appearance196.Image = ((object)(resources.GetObject("appearance196.Image")));
            buttonTool142.SharedPropsInternal.AppearancesSmall.Appearance = appearance196;
            buttonTool142.SharedPropsInternal.Caption = "Tính khấu hao TSCĐ";
            buttonTool142.SharedPropsInternal.CustomizerCaption = "Tính khấu hao tài sản cố định";
            appearance197.Image = ((object)(resources.GetObject("appearance197.Image")));
            buttonTool143.SharedPropsInternal.AppearancesLarge.Appearance = appearance197;
            buttonTool143.SharedPropsInternal.Caption = "Bảng lương";
            appearance198.Image = ((object)(resources.GetObject("appearance198.Image")));
            buttonTool144.SharedPropsInternal.AppearancesSmall.Appearance = appearance198;
            buttonTool144.SharedPropsInternal.Caption = "Bảng chấm công";
            appearance199.Image = ((object)(resources.GetObject("appearance199.Image")));
            buttonTool145.SharedPropsInternal.AppearancesSmall.Appearance = appearance199;
            buttonTool145.SharedPropsInternal.Caption = "Trả lương";
            appearance200.Image = ((object)(resources.GetObject("appearance200.Image")));
            buttonTool146.SharedPropsInternal.AppearancesSmall.Appearance = appearance200;
            buttonTool146.SharedPropsInternal.Caption = "Hạch toán chi phí lương";
            appearance201.Image = ((object)(resources.GetObject("appearance201.Image")));
            buttonTool147.SharedPropsInternal.AppearancesLarge.Appearance = appearance201;
            buttonTool147.SharedPropsInternal.Caption = "Tờ khai thuế";
            appearance202.Image = ((object)(resources.GetObject("appearance202.Image")));
            buttonTool148.SharedPropsInternal.AppearancesSmall.Appearance = appearance202;
            buttonTool148.SharedPropsInternal.Caption = "Lập bảng kê bán ra";
            appearance203.Image = ((object)(resources.GetObject("appearance203.Image")));
            buttonTool149.SharedPropsInternal.AppearancesSmall.Appearance = appearance203;
            buttonTool149.SharedPropsInternal.Caption = "Lập bảng kê mua vào";
            appearance204.Image = ((object)(resources.GetObject("appearance204.Image")));
            buttonTool150.SharedPropsInternal.AppearancesSmall.Appearance = appearance204;
            buttonTool150.SharedPropsInternal.Caption = "Nộp thuế";
            appearance205.Image = ((object)(resources.GetObject("appearance205.Image")));
            buttonTool151.SharedPropsInternal.AppearancesLarge.Appearance = appearance205;
            buttonTool151.SharedPropsInternal.Caption = "Chứng từ, nghiệp vụ khác";
            appearance206.Image = ((object)(resources.GetObject("appearance206.Image")));
            buttonTool152.SharedPropsInternal.AppearancesSmall.Appearance = appearance206;
            buttonTool152.SharedPropsInternal.Caption = "Kết chuyển lãi lỗ";
            appearance207.Image = global::Accounting.Properties.Resources.Lock;
            buttonTool153.SharedPropsInternal.AppearancesLarge.Appearance = appearance207;
            appearance208.Image = global::Accounting.Properties.Resources.Lock;
            buttonTool153.SharedPropsInternal.AppearancesSmall.Appearance = appearance208;
            buttonTool153.SharedPropsInternal.Caption = "Khóa số kế toán";
            appearance209.Image = global::Accounting.Properties.Resources.Unlock;
            buttonTool154.SharedPropsInternal.AppearancesSmall.Appearance = appearance209;
            buttonTool154.SharedPropsInternal.Caption = "Bỏ khóa sổ";
            appearance210.Image = ((object)(resources.GetObject("appearance210.Image")));
            buttonTool155.SharedPropsInternal.AppearancesLarge.Appearance = appearance210;
            buttonTool155.SharedPropsInternal.Caption = "Thiết lập thông tin ban đầu";
            buttonTool155.SharedPropsInternal.Visible = false;
            appearance211.Image = ((object)(resources.GetObject("appearance211.Image")));
            buttonTool156.SharedPropsInternal.AppearancesLarge.Appearance = appearance211;
            buttonTool156.SharedPropsInternal.Caption = "Tìm kiếm";
            buttonTool156.SharedPropsInternal.Visible = false;
            appearance212.Image = ((object)(resources.GetObject("appearance212.Image")));
            buttonTool157.SharedPropsInternal.AppearancesLarge.Appearance = appearance212;
            buttonTool157.SharedPropsInternal.Caption = "Báo cáo nhanh";
            buttonTool157.SharedPropsInternal.Visible = false;
            buttonTool158.SharedPropsInternal.Caption = "Bảo trì dữ liệu";
            buttonTool158.SharedPropsInternal.Visible = false;
            buttonTool159.SharedPropsInternal.Caption = "Thiết lập BCTC";
            buttonTool159.SharedPropsInternal.Visible = false;
            buttonTool160.SharedPropsInternal.Caption = "Quản lý tài liệu";
            buttonTool160.SharedPropsInternal.Visible = false;
            buttonTool161.SharedPropsInternal.Caption = "Quản lý công việc";
            buttonTool161.SharedPropsInternal.Visible = false;
            appearance213.Image = ((object)(resources.GetObject("appearance213.Image")));
            buttonTool162.SharedPropsInternal.AppearancesLarge.Appearance = appearance213;
            buttonTool162.SharedPropsInternal.Caption = "Nội dung trợ giúp";
            buttonTool162.SharedPropsInternal.Visible = false;
            buttonTool163.SharedPropsInternal.Caption = "Cập nhật phần mềm";
            buttonTool163.SharedPropsInternal.Visible = false;
            buttonTool164.SharedPropsInternal.Caption = "Thông tin sản phẩm";
            buttonTool164.SharedPropsInternal.Visible = false;
            buttonTool165.SharedPropsInternal.Caption = "Thông tin hỗ trợ";
            buttonTool165.SharedPropsInternal.Visible = false;
            appearance214.Image = global::Accounting.Properties.Resources.refresh;
            buttonTool166.SharedPropsInternal.AppearancesLarge.Appearance = appearance214;
            buttonTool166.SharedPropsInternal.Caption = "Làm mới";
            buttonTool166.SharedPropsInternal.Visible = false;
            appearance215.Image = global::Accounting.Properties.Resources.tuychon;
            buttonTool167.SharedPropsInternal.AppearancesLarge.Appearance = appearance215;
            buttonTool167.SharedPropsInternal.Caption = "Tùy chọn";
            appearance216.Image = global::Accounting.Properties.Resources.PHIEU_CHI;
            buttonTool168.SharedPropsInternal.AppearancesLarge.Appearance = appearance216;
            buttonTool168.SharedPropsInternal.Caption = "Phiếu chi";
            appearance217.Image = ((object)(resources.GetObject("appearance217.Image")));
            buttonTool169.SharedPropsInternal.AppearancesSmall.Appearance = appearance217;
            buttonTool169.SharedPropsInternal.Caption = "Séc/ủy nhiệm chi";
            appearance218.Image = ((object)(resources.GetObject("appearance218.Image")));
            buttonTool170.SharedPropsInternal.AppearancesSmall.Appearance = appearance218;
            buttonTool170.SharedPropsInternal.Caption = "Thu qua ngân hàng";
            appearance219.Image = ((object)(resources.GetObject("appearance219.Image")));
            buttonTool171.SharedPropsInternal.AppearancesLarge.Appearance = appearance219;
            appearance220.Image = global::Accounting.Properties.Resources.sec_uynhiemchi;
            buttonTool171.SharedPropsInternal.AppearancesSmall.Appearance = appearance220;
            buttonTool171.SharedPropsInternal.Caption = "Hệ thống tài khoản";
            buttonTool171.Tag = "Category";
            popupGalleryTool1.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool172,
            buttonTool173,
            buttonTool174,
            buttonTool175});
            appearance221.Image = global::Accounting.Properties.Resources.Settings_icon;
            buttonTool176.SharedPropsInternal.AppearancesSmall.Appearance = appearance221;
            buttonTool176.SharedPropsInternal.Caption = "Hướng dẫn sử dụng";
            buttonTool176.SharedPropsInternal.CustomizerCaption = "Hướng dẫn sử dụng";
            popupMenuTool2.SharedPropsInternal.Caption = "...";
            buttonTool177.InstanceProps.IsFirstInGroup = true;
            buttonTool180.InstanceProps.IsFirstInGroup = true;
            popupMenuTool2.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool177,
            buttonTool178,
            buttonTool179,
            buttonTool180,
            buttonTool181,
            buttonTool182,
            buttonTool183,
            buttonTool184,
            buttonTool185,
            buttonTool186,
            buttonTool187,
            buttonTool188,
            buttonTool189,
            buttonTool190,
            buttonTool191,
            buttonTool192});
            appearance222.Image = global::Accounting.Properties.Resources.import;
            buttonTool193.SharedPropsInternal.AppearancesLarge.Appearance = appearance222;
            appearance223.Image = global::Accounting.Properties.Resources.import_16;
            buttonTool193.SharedPropsInternal.AppearancesSmall.Appearance = appearance223;
            buttonTool193.SharedPropsInternal.Caption = "Nhập dữ liệu";
            appearance224.Image = global::Accounting.Properties.Resources.account_template;
            buttonTool194.SharedPropsInternal.AppearancesSmall.Appearance = appearance224;
            buttonTool194.SharedPropsInternal.Caption = "Tài khoản ngầm định";
            buttonTool194.Tag = "Category";
            appearance225.Image = global::Accounting.Properties.Resources.kho;
            buttonTool195.SharedPropsInternal.AppearancesSmall.Appearance = appearance225;
            buttonTool195.SharedPropsInternal.Caption = "Loại VTHH";
            buttonTool195.Tag = "Category";
            appearance226.Image = global::Accounting.Properties.Resources.loai_co_phan;
            buttonTool196.SharedPropsInternal.AppearancesSmall.Appearance = appearance226;
            buttonTool196.SharedPropsInternal.Caption = "Loại TSCĐ";
            buttonTool196.Tag = "Category";
            appearance227.Image = global::Accounting.Properties.Resources.tinhkhauhao;
            buttonTool197.SharedPropsInternal.AppearancesSmall.Appearance = appearance227;
            buttonTool197.SharedPropsInternal.Caption = "Định mức nguyên vật liệu";
            buttonTool197.Tag = "Category";
            appearance228.Image = global::Accounting.Properties.Resources.User_group;
            buttonTool198.SharedPropsInternal.AppearancesSmall.Appearance = appearance228;
            buttonTool198.SharedPropsInternal.Caption = "Nhóm nhà đầu tư";
            buttonTool198.SharedPropsInternal.Visible = false;
            buttonTool198.Tag = "Category";
            appearance229.Image = global::Accounting.Properties.Resources.vaitro_quyenhan;
            buttonTool199.SharedPropsInternal.AppearancesSmall.Appearance = appearance229;
            buttonTool199.SharedPropsInternal.Caption = "Nhóm đăng ký";
            buttonTool199.SharedPropsInternal.Visible = false;
            buttonTool199.Tag = "Category";
            appearance230.Image = global::Accounting.Properties.Resources.NHOMCODONG;
            buttonTool200.SharedPropsInternal.AppearancesSmall.Appearance = appearance230;
            buttonTool200.SharedPropsInternal.Caption = "Nhóm cổ đông";
            buttonTool200.SharedPropsInternal.Visible = false;
            buttonTool200.Tag = "Category";
            appearance231.Image = global::Accounting.Properties.Resources.ngayhachtoan;
            buttonTool201.SharedPropsInternal.AppearancesSmall.Appearance = appearance231;
            buttonTool201.SharedPropsInternal.Caption = "Mã thống kê";
            buttonTool201.Tag = "Category";
            appearance232.Image = global::Accounting.Properties.Resources.THUTIENKHACHHANG1;
            buttonTool202.SharedPropsInternal.AppearancesSmall.Appearance = appearance232;
            buttonTool202.SharedPropsInternal.Caption = "Phương thức vận chuyển";
            buttonTool202.Tag = "Category";
            appearance233.Image = global::Accounting.Properties.Resources.tra_tien_ncc;
            buttonTool203.SharedPropsInternal.AppearancesSmall.Appearance = appearance233;
            buttonTool203.SharedPropsInternal.Caption = "Nhóm giá bán";
            buttonTool203.Tag = "Category";
            appearance234.Image = global::Accounting.Properties.Resources.Shipping;
            buttonTool204.SharedPropsInternal.AppearancesSmall.Appearance = appearance234;
            buttonTool204.SharedPropsInternal.Caption = "Nhóm HHDV chịu thuế TTĐB";
            buttonTool204.Tag = "Category";
            appearance235.Image = global::Accounting.Properties.Resources.sec_uynhiemchi;
            buttonTool205.SharedPropsInternal.AppearancesSmall.Appearance = appearance235;
            buttonTool205.SharedPropsInternal.Caption = "Biểu thuế tài nguyên";
            buttonTool205.Tag = "Category";
            appearance236.Image = global::Accounting.Properties.Resources.listview;
            buttonTool206.SharedPropsInternal.AppearancesSmall.Appearance = appearance236;
            buttonTool206.SharedPropsInternal.Caption = "Tình trạng hợp đồng";
            buttonTool206.Tag = "Category";
            appearance237.Image = global::Accounting.Properties.Resources.iEdit;
            buttonTool207.SharedPropsInternal.AppearancesSmall.Appearance = appearance237;
            buttonTool207.SharedPropsInternal.Caption = "Mẫu số hóa đơn";
            buttonTool207.Tag = "Category";
            appearance238.Image = global::Accounting.Properties.Resources.account_template;
            appearance238.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance238.ImageVAlign = Infragistics.Win.VAlign.Middle;
            buttonTool208.SharedPropsInternal.AppearancesLarge.Appearance = appearance238;
            buttonTool208.SharedPropsInternal.Caption = "Quản trị người dùng";
            buttonTool208.SharedPropsInternal.CustomizerCaption = "Quản trị người dùng";
            buttonTool208.SharedPropsInternal.ToolTipText = "Quản trị doanh nghiệp";
            appearance239.Image = global::Accounting.Properties.Resources.style_change;
            buttonTool209.SharedPropsInternal.AppearancesLarge.Appearance = appearance239;
            buttonTool209.SharedPropsInternal.Caption = "Thay đổi giao diện CT";
            buttonTool209.SharedPropsInternal.ToolTipText = "Thay đổi giao diện chương trình";
            buttonTool209.SharedPropsInternal.Visible = false;
            appearance240.Image = global::Accounting.Properties.Resources.Software_Update;
            buttonTool210.SharedPropsInternal.AppearancesLarge.Appearance = appearance240;
            buttonTool210.SharedPropsInternal.Caption = "Kiểm tra phiên bản";
            buttonTool210.SharedPropsInternal.CustomizerCaption = "Kiểm tra phiên bản và cập nhật phần mềm";
            appearance241.Image = global::Accounting.Properties.Resources.account_template;
            buttonTool211.SharedPropsInternal.AppearancesSmall.Appearance = appearance241;
            buttonTool211.SharedPropsInternal.Caption = "Bảng cân đối tài khoản";
            appearance242.Image = global::Accounting.Properties.Resources.bangluong;
            buttonTool212.SharedPropsInternal.AppearancesSmall.Appearance = appearance242;
            buttonTool212.SharedPropsInternal.Caption = "Sổ kế toán chi tiết quỹ tiền mặt";
            appearance243.Image = global::Accounting.Properties.Resources.BANHANGCHUATHUTIEN1;
            buttonTool213.SharedPropsInternal.AppearancesSmall.Appearance = appearance243;
            buttonTool213.SharedPropsInternal.Caption = "Sổ tiền gửi ngân hàng";
            appearance244.Image = global::Accounting.Properties.Resources.bieuthuethunhap_copy;
            buttonTool214.SharedPropsInternal.AppearancesSmall.Appearance = appearance244;
            buttonTool214.SharedPropsInternal.Caption = "Bảng kê số dư ngân hàng";
            appearance245.Image = global::Accounting.Properties.Resources.btnSave;
            buttonTool215.SharedPropsInternal.AppearancesSmall.Appearance = appearance245;
            buttonTool215.SharedPropsInternal.Caption = "Sổ chi tiết bán hàng";
            appearance246.Image = global::Accounting.Properties.Resources.mucthuchi;
            buttonTool216.SharedPropsInternal.AppearancesSmall.Appearance = appearance246;
            buttonTool216.SharedPropsInternal.Caption = "Sổ nhật ký bán hàng";
            appearance247.Image = global::Accounting.Properties.Resources.CHUNGTUNGHIEPVU_KHAC;
            buttonTool217.SharedPropsInternal.AppearancesSmall.Appearance = appearance247;
            buttonTool217.SharedPropsInternal.Caption = "Tổng hợp công nợ phải thu";
            appearance248.Image = global::Accounting.Properties.Resources.dutoanchi;
            buttonTool218.SharedPropsInternal.AppearancesSmall.Appearance = appearance248;
            buttonTool218.SharedPropsInternal.Caption = "Chi tiết công nợ phải thu khách hàng";
            appearance249.Image = global::Accounting.Properties.Resources.dutoanchi;
            buttonTool219.SharedPropsInternal.AppearancesSmall.Appearance = appearance249;
            buttonTool219.SharedPropsInternal.Caption = "Tổng hợp công nợ phải thu theo nhóm khách hàng";
            appearance250.Image = global::Accounting.Properties.Resources.CHUNGTUNGHIEPVU_KHAC;
            buttonTool220.SharedPropsInternal.AppearancesSmall.Appearance = appearance250;
            buttonTool220.SharedPropsInternal.Caption = "Tổng công nợ phải trả nhà cung cấp";
            appearance251.Image = global::Accounting.Properties.Resources.CHUNGTUNGHIEPVU_KHAC;
            buttonTool221.SharedPropsInternal.AppearancesSmall.Appearance = appearance251;
            buttonTool221.SharedPropsInternal.Caption = "Sổ nhật ký mua hàng";
            appearance252.Image = global::Accounting.Properties.Resources.CHUNGTUNGHIEPVU_KHAC;
            buttonTool222.SharedPropsInternal.AppearancesSmall.Appearance = appearance252;
            buttonTool222.SharedPropsInternal.Caption = "Sổ chi tiết các tài khoản";
            appearance253.Image = global::Accounting.Properties.Resources.BANHANGCHUATHUTIEN;
            buttonTool223.SharedPropsInternal.AppearancesSmall.Appearance = appearance253;
            buttonTool223.SharedPropsInternal.Caption = "S03b-DN: Sổ cái (Hình thức Nhật ký chung)";
            appearance254.Image = global::Accounting.Properties.Resources.baocaonhanh;
            buttonTool224.SharedPropsInternal.AppearancesSmall.Appearance = appearance254;
            buttonTool224.SharedPropsInternal.Caption = "B01aDNN: Báo cáo tình hình tài chính";
            appearance255.Image = global::Accounting.Properties.Resources.bieuthuethunhap_copy;
            buttonTool225.SharedPropsInternal.AppearancesSmall.Appearance = appearance255;
            buttonTool225.SharedPropsInternal.Caption = "B01bDNN: Báo cáo tình hình tài chính";
            comboBoxTool1.ValueList = valueList1;
            appearance256.Image = global::Accounting.Properties.Resources.iEdit;
            buttonTool226.SharedPropsInternal.AppearancesLarge.Appearance = appearance256;
            buttonTool226.SharedPropsInternal.Caption = "Danh sách báo cáo";
            appearance257.Image = global::Accounting.Properties.Resources.Congcu_dungcu2;
            buttonTool227.SharedPropsInternal.AppearancesSmall.Appearance = appearance257;
            buttonTool227.SharedPropsInternal.Caption = "Công cụ dụng cụ";
            buttonTool227.SharedPropsInternal.CustomizerCaption = "CCDC";
            appearance258.Image = global::Accounting.Properties.Resources.id_card;
            buttonTool228.SharedPropsInternal.AppearancesSmall.Appearance = appearance258;
            buttonTool228.SharedPropsInternal.Caption = "Quy định lương, thuế, bảo hiểm";
            appearance259.Image = global::Accounting.Properties.Resources.import;
            buttonTool229.SharedPropsInternal.AppearancesLarge.Appearance = appearance259;
            appearance260.Image = global::Accounting.Properties.Resources.import;
            buttonTool229.SharedPropsInternal.AppearancesSmall.Appearance = appearance260;
            buttonTool229.SharedPropsInternal.Caption = "Chuyển phiên làm việc";
            appearance261.Image = global::Accounting.Properties.Resources.edit_validated_icon;
            buttonTool230.SharedPropsInternal.AppearancesLarge.Appearance = appearance261;
            appearance262.Image = global::Accounting.Properties.Resources.edit_validated_icon;
            buttonTool230.SharedPropsInternal.AppearancesSmall.Appearance = appearance262;
            buttonTool230.SharedPropsInternal.Caption = "Lựa chọn sổ kế toán";
            buttonTool230.SharedPropsInternal.Visible = false;
            appearance263.Image = global::Accounting.Properties.Resources.timkiem2;
            buttonTool231.SharedPropsInternal.AppearancesLarge.Appearance = appearance263;
            appearance264.Image = global::Accounting.Properties.Resources.timkiem2;
            buttonTool231.SharedPropsInternal.AppearancesSmall.Appearance = appearance264;
            buttonTool231.SharedPropsInternal.Caption = "Tìm kiếm chứng từ";
            buttonTool231.SharedPropsInternal.CustomizerCaption = "Tìm kiếm nhanh theo số chứng từ";
            appearance265.Image = global::Accounting.Properties.Resources.tinhnangbosung;
            buttonTool232.SharedPropsInternal.AppearancesLarge.Appearance = appearance265;
            buttonTool232.SharedPropsInternal.Caption = "Đánh lại số chứng từ";
            appearance266.Image = global::Accounting.Properties.Resources.import_spreadsheet_512;
            buttonTool233.SharedPropsInternal.AppearancesLarge.Appearance = appearance266;
            appearance267.Image = global::Accounting.Properties.Resources.import_spreadsheet_512;
            buttonTool233.SharedPropsInternal.AppearancesSmall.Appearance = appearance267;
            buttonTool233.SharedPropsInternal.Caption = "Nhập danh mục từ file excel";
            appearance268.Image = global::Accounting.Properties.Resources.PHIEU_THU;
            buttonTool234.SharedPropsInternal.AppearancesSmall.Appearance = appearance268;
            buttonTool234.SharedPropsInternal.Caption = "Phiếu thu";
            appearance269.Image = global::Accounting.Properties.Resources.books_icon;
            buttonTool239.SharedPropsInternal.AppearancesLarge.Appearance = appearance269;
            buttonTool239.SharedPropsInternal.Caption = "Hướng dẫn sử dụng";
            buttonTool239.SharedPropsInternal.Enabled = false;
            buttonTool239.SharedPropsInternal.Visible = false;
            appearance270.Image = global::Accounting.Properties.Resources.tv_2223047_960_720;
            buttonTool240.SharedPropsInternal.AppearancesLarge.Appearance = appearance270;
            appearance271.Image = global::Accounting.Properties.Resources.tv_2223047_960_720;
            buttonTool240.SharedPropsInternal.AppearancesSmall.Appearance = appearance271;
            buttonTool240.SharedPropsInternal.Caption = "Ultraviewer";
            appearance272.Image = global::Accounting.Properties.Resources.teamview;
            buttonTool241.SharedPropsInternal.AppearancesLarge.Appearance = appearance272;
            appearance273.Image = global::Accounting.Properties.Resources.teamview;
            buttonTool241.SharedPropsInternal.AppearancesSmall.Appearance = appearance273;
            buttonTool241.SharedPropsInternal.Caption = "Teamviewer";
            appearance274.Image = global::Accounting.Properties.Resources.logox48;
            buttonTool242.SharedPropsInternal.AppearancesLarge.Appearance = appearance274;
            buttonTool242.SharedPropsInternal.Caption = "Thông tin phần mềm";
            buttonTool242.SharedPropsInternal.Enabled = false;
            buttonTool242.SharedPropsInternal.Visible = false;
            appearance275.Image = global::Accounting.Properties.Resources.ubtnPost;
            buttonTool243.SharedPropsInternal.AppearancesLarge.Appearance = appearance275;
            appearance276.Image = global::Accounting.Properties.Resources.ubtnPost;
            buttonTool243.SharedPropsInternal.AppearancesSmall.Appearance = appearance276;
            buttonTool243.SharedPropsInternal.Caption = "Xử lý chứng từ";
            appearance277.Image = global::Accounting.Properties.Resources.ubtnPrint;
            buttonTool244.SharedPropsInternal.AppearancesLarge.Appearance = appearance277;
            appearance278.Image = global::Accounting.Properties.Resources.ubtnPrint;
            buttonTool244.SharedPropsInternal.AppearancesSmall.Appearance = appearance278;
            buttonTool244.SharedPropsInternal.Caption = "In chứng từ hàng loạt";
            appearance279.Image = global::Accounting.Properties.Resources.import_spreadsheet_512;
            buttonTool245.SharedPropsInternal.AppearancesSmall.Appearance = appearance279;
            buttonTool245.SharedPropsInternal.Caption = "Nhập chứng từ từ excel";
            comboBoxTool2.SharedPropsInternal.Caption = "btnDonwloadHDSD";
            comboBoxTool2.ValueList = valueList2;
            comboBoxTool3.SharedPropsInternal.Caption = "btnUltraviewer2";
            comboBoxTool3.ValueList = valueList3;
            comboBoxTool4.SharedPropsInternal.Caption = "btnTeamviewer2";
            comboBoxTool4.ValueList = valueList4;
            comboBoxTool5.DropDownStyle = Infragistics.Win.DropDownStyle.DropDown;
            appearance280.Image = global::Accounting.Properties.Resources.Settings_icon;
            comboBoxTool5.SharedPropsInternal.AppearancesSmall.Appearance = appearance280;
            comboBoxTool5.SharedPropsInternal.Caption = "btnHuongDanSuDung";
            comboBoxTool5.ValueList = valueList5;
            comboBoxTool5.VerticalDisplayStyle = Infragistics.Win.UltraWinToolbars.VerticalDisplayStyle.Hide;
            appearance281.Image = global::Accounting.Properties.Resources.Settings_icon;
            comboBoxTool6.SharedPropsInternal.AppearancesLarge.Appearance = appearance281;
            comboBoxTool6.SharedPropsInternal.Caption = "btnHDSD";
            comboBoxTool6.ValueList = valueList6;
            this.uToolbarsManager.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool90,
            buttonTool91,
            buttonTool92,
            buttonTool93,
            buttonTool94,
            buttonTool95,
            buttonTool96,
            buttonTool97,
            buttonTool98,
            buttonTool99,
            buttonTool100,
            buttonTool101,
            buttonTool102,
            buttonTool103,
            buttonTool104,
            buttonTool105,
            buttonTool106,
            buttonTool107,
            buttonTool108,
            buttonTool109,
            buttonTool110,
            buttonTool111,
            buttonTool112,
            buttonTool113,
            buttonTool114,
            buttonTool115,
            buttonTool116,
            buttonTool117,
            buttonTool118,
            buttonTool119,
            buttonTool120,
            buttonTool121,
            buttonTool122,
            buttonTool123,
            buttonTool124,
            buttonTool125,
            buttonTool126,
            buttonTool127,
            buttonTool128,
            buttonTool129,
            buttonTool130,
            buttonTool131,
            buttonTool132,
            buttonTool133,
            buttonTool134,
            buttonTool135,
            buttonTool136,
            buttonTool137,
            buttonTool138,
            buttonTool139,
            buttonTool140,
            buttonTool141,
            buttonTool142,
            buttonTool143,
            buttonTool144,
            buttonTool145,
            buttonTool146,
            buttonTool147,
            buttonTool148,
            buttonTool149,
            buttonTool150,
            buttonTool151,
            buttonTool152,
            buttonTool153,
            buttonTool154,
            buttonTool155,
            buttonTool156,
            buttonTool157,
            buttonTool158,
            buttonTool159,
            buttonTool160,
            buttonTool161,
            buttonTool162,
            buttonTool163,
            buttonTool164,
            buttonTool165,
            buttonTool166,
            buttonTool167,
            buttonTool168,
            buttonTool169,
            buttonTool170,
            buttonTool171,
            popupGalleryTool1,
            buttonTool176,
            popupMenuTool2,
            buttonTool193,
            buttonTool194,
            buttonTool195,
            buttonTool196,
            buttonTool197,
            buttonTool198,
            buttonTool199,
            buttonTool200,
            buttonTool201,
            buttonTool202,
            buttonTool203,
            buttonTool204,
            buttonTool205,
            buttonTool206,
            buttonTool207,
            buttonTool208,
            buttonTool209,
            buttonTool210,
            buttonTool211,
            buttonTool212,
            buttonTool213,
            buttonTool214,
            buttonTool215,
            buttonTool216,
            buttonTool217,
            buttonTool218,
            buttonTool219,
            buttonTool220,
            buttonTool221,
            buttonTool222,
            buttonTool223,
            buttonTool224,
            buttonTool225,
            comboBoxTool1,
            buttonTool226,
            buttonTool227,
            buttonTool228,
            buttonTool229,
            buttonTool230,
            buttonTool231,
            buttonTool232,
            buttonTool233,
            buttonTool234,
            buttonTool235,
            buttonTool236,
            buttonTool237,
            buttonTool238,
            buttonTool239,
            buttonTool240,
            buttonTool241,
            buttonTool242,
            buttonTool243,
            buttonTool244,
            buttonTool245,
            comboBoxTool2,
            comboBoxTool3,
            comboBoxTool4,
            comboBoxTool5,
            comboBoxTool6});
            this.uToolbarsManager.BeforeApplicationMenuDropDown += new System.ComponentModel.CancelEventHandler(this.uToolbarsManager_BeforeApplicationMenuDropDown);
            this.uToolbarsManager.ToolClick += new Infragistics.Win.UltraWinToolbars.ToolClickEventHandler(this.uToolbarsManagerToolClick);
            // 
            // uSttBar
            // 
            this.uSttBar.Location = new System.Drawing.Point(0, 718);
            this.uSttBar.Name = "uSttBar";
            this.uSttBar.Padding = new Infragistics.Win.UltraWinStatusBar.UIElementMargins(5, 2, 0, 0);
            appearance93.ImageHAlign = Infragistics.Win.HAlign.Left;
            appearance93.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance93.TextHAlignAsString = "Right";
            appearance93.TextVAlignAsString = "Middle";
            this.uSttBar.PanelAppearance = appearance93;
            appearance94.Image = global::Accounting.Properties.Resources.Server;
            ultraStatusPanel1.Appearance = appearance94;
            ultraStatusPanel1.Key = "Server";
            ultraStatusPanel1.Padding = new System.Drawing.Size(5, 0);
            ultraStatusPanel1.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Automatic;
            ultraStatusPanel1.Text = "Máy chủ";
            ultraStatusPanel1.ToolTipText = "Máy chủ";
            ultraStatusPanel1.WrapText = Infragistics.Win.DefaultableBoolean.False;
            appearance95.Image = global::Accounting.Properties.Resources.database;
            ultraStatusPanel2.Appearance = appearance95;
            ultraStatusPanel2.Key = "Database";
            ultraStatusPanel2.Padding = new System.Drawing.Size(5, 0);
            ultraStatusPanel2.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Automatic;
            ultraStatusPanel2.Text = "Tên dữ liệu kế toán";
            ultraStatusPanel2.ToolTipText = "Tên dữ liệu kế toán";
            appearance96.Image = global::Accounting.Properties.Resources.user;
            ultraStatusPanel3.Appearance = appearance96;
            ultraStatusPanel3.Key = "User";
            ultraStatusPanel3.Padding = new System.Drawing.Size(5, 0);
            ultraStatusPanel3.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Automatic;
            ultraStatusPanel3.Text = "Người dùng";
            ultraStatusPanel3.ToolTipText = "Người dùng";
            appearance97.Image = ((object)(resources.GetObject("appearance97.Image")));
            ultraStatusPanel4.Appearance = appearance97;
            ultraStatusPanel4.Key = "Hotline";
            ultraStatusPanel4.Padding = new System.Drawing.Size(5, 0);
            ultraStatusPanel4.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Automatic;
            ultraStatusPanel4.Text = "083.6868.89";
            ultraStatusPanel4.ToolTipText = "Hotline";
            ultraStatusPanel5.Key = "Space";
            ultraStatusPanel5.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Spring;
            appearance98.Image = global::Accounting.Properties.Resources.clock1;
            ultraStatusPanel6.Appearance = appearance98;
            ultraStatusPanel6.DateTimeFormat = "h:mm tt";
            ultraStatusPanel6.Key = "Time";
            ultraStatusPanel6.Padding = new System.Drawing.Size(5, 0);
            ultraStatusPanel6.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Automatic;
            ultraStatusPanel6.Style = Infragistics.Win.UltraWinStatusBar.PanelStyle.Time;
            ultraStatusPanel6.ToolTipText = "Giờ";
            appearance99.Image = global::Accounting.Properties.Resources.calendar_black;
            appearance99.ImageHAlign = Infragistics.Win.HAlign.Left;
            appearance99.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance99.TextHAlignAsString = "Right";
            appearance99.TextVAlignAsString = "Middle";
            ultraStatusPanel7.Appearance = appearance99;
            ultraStatusPanel7.DateTimeFormat = "dd/MM/yyyy";
            ultraStatusPanel7.Key = "Date";
            ultraStatusPanel7.Padding = new System.Drawing.Size(5, 0);
            ultraStatusPanel7.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Automatic;
            ultraStatusPanel7.Style = Infragistics.Win.UltraWinStatusBar.PanelStyle.Date;
            ultraStatusPanel7.ToolTipText = "Ngày tháng";
            this.uSttBar.Panels.AddRange(new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel[] {
            ultraStatusPanel1,
            ultraStatusPanel2,
            ultraStatusPanel3,
            ultraStatusPanel4,
            ultraStatusPanel5,
            ultraStatusPanel6,
            ultraStatusPanel7});
            this.uSttBar.ResizeStyle = Infragistics.Win.UltraWinStatusBar.ResizeStyle.None;
            this.uSttBar.ScaleImages = Infragistics.Win.ScaleImage.Never;
            this.uSttBar.Size = new System.Drawing.Size(1015, 31);
            this.uSttBar.TabIndex = 5;
            this.uSttBar.WrapText = false;
            this.uSttBar.MouseHover += new System.EventHandler(this.uSttBar_MouseHover);
            // 
            // _MainFrm_Toolbars_Dock_Area_Right
            // 
            this._MainFrm_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._MainFrm_Toolbars_Dock_Area_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._MainFrm_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right;
            this._MainFrm_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.SystemColors.ControlText;
            this._MainFrm_Toolbars_Dock_Area_Right.InitialResizeAreaExtent = 4;
            this._MainFrm_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(1011, 28);
            this._MainFrm_Toolbars_Dock_Area_Right.Name = "_MainFrm_Toolbars_Dock_Area_Right";
            this._MainFrm_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(4, 690);
            this._MainFrm_Toolbars_Dock_Area_Right.ToolbarsManager = this.uToolbarsManager;
            // 
            // _MainFrm_Toolbars_Dock_Area_Bottom
            // 
            this._MainFrm_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._MainFrm_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._MainFrm_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom;
            this._MainFrm_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.SystemColors.ControlText;
            this._MainFrm_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 718);
            this._MainFrm_Toolbars_Dock_Area_Bottom.Name = "_MainFrm_Toolbars_Dock_Area_Bottom";
            this._MainFrm_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(1015, 0);
            this._MainFrm_Toolbars_Dock_Area_Bottom.ToolbarsManager = this.uToolbarsManager;
            // 
            // _MainFrm_Toolbars_Dock_Area_Top
            // 
            this._MainFrm_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._MainFrm_Toolbars_Dock_Area_Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._MainFrm_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top;
            this._MainFrm_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.SystemColors.ControlText;
            this._MainFrm_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
            this._MainFrm_Toolbars_Dock_Area_Top.Name = "_MainFrm_Toolbars_Dock_Area_Top";
            this._MainFrm_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(1015, 28);
            this._MainFrm_Toolbars_Dock_Area_Top.ToolbarsManager = this.uToolbarsManager;
            // 
            // ultraSplitter1
            // 
            appearance6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance6.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance6.BackColorAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance6.BackColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance6.BackColorDisabled2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BackHatchStyle = Infragistics.Win.BackHatchStyle.Horizontal;
            appearance6.BorderAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance6.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance6.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance6.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance6.Cursor = System.Windows.Forms.Cursors.SizeWE;
            appearance6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance6.ForeColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
            appearance6.ImageBackground = ((System.Drawing.Image)(resources.GetObject("appearance6.ImageBackground")));
            this.ultraSplitter1.Appearance = appearance6;
            this.ultraSplitter1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance7.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance7.BackColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance7.BackColorDisabled2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance7.BackHatchStyle = Infragistics.Win.BackHatchStyle.Vertical;
            appearance7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance7.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance7.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance7.ForeColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            this.ultraSplitter1.ButtonAppearance = appearance7;
            this.ultraSplitter1.ButtonExtent = 1;
            appearance8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance8.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance8.BackColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance8.BackColorDisabled2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance8.BackHatchStyle = Infragistics.Win.BackHatchStyle.Vertical;
            appearance8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance8.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance8.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance8.ForeColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            this.ultraSplitter1.CollapsedAppearance = appearance8;
            appearance9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance9.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance9.BackColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance9.BackColorDisabled2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance9.BackHatchStyle = Infragistics.Win.BackHatchStyle.Vertical;
            appearance9.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance9.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance9.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance9.ForeColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            this.ultraSplitter1.CollapsedButtonAppearance = appearance9;
            this.ultraSplitter1.Cursor = System.Windows.Forms.Cursors.SizeWE;
            this.ultraSplitter1.DragIndicatorColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance10.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance10.BackColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance10.BackColorDisabled2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance10.BackHatchStyle = Infragistics.Win.BackHatchStyle.Vertical;
            appearance10.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance10.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance10.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance10.ForeColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            this.ultraSplitter1.HotTrackingAppearance = appearance10;
            appearance11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance11.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance11.BackColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance11.BackColorDisabled2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance11.BackHatchStyle = Infragistics.Win.BackHatchStyle.Vertical;
            appearance11.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance11.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance11.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance11.ForeColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            this.ultraSplitter1.HotTrackingButtonAppearance = appearance11;
            this.ultraSplitter1.Location = new System.Drawing.Point(197, 28);
            this.ultraSplitter1.Name = "ultraSplitter1";
            appearance12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance12.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance12.BackColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance12.BackColorDisabled2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance12.BackHatchStyle = Infragistics.Win.BackHatchStyle.Vertical;
            appearance12.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance12.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance12.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance12.ForeColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            this.ultraSplitter1.PressedButtonAppearance = appearance12;
            this.ultraSplitter1.RestoreExtent = 0;
            this.ultraSplitter1.Size = new System.Drawing.Size(6, 690);
            this.ultraSplitter1.TabIndex = 12;
            // 
            // uExBar
            // 
            this.uExBar.AcceptsFocus = Infragistics.Win.DefaultableBoolean.True;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance13.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance13.Image = global::Accounting.Properties.Resources.NOPTHUE1;
            this.uExBar.Appearance = appearance13;
            appearance14.Image = global::Accounting.Properties.Resources.NOPTHUE1;
            this.uExBar.Appearances.Add(appearance14);
            this.uExBar.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uExBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uExBar.Dock = System.Windows.Forms.DockStyle.Left;
            ultraExplorerBarGroup1.Key = "Desktop";
            appearance15.Image = ((object)(resources.GetObject("appearance15.Image")));
            ultraExplorerBarGroup1.Settings.AppearancesLarge.HeaderAppearance = appearance15;
            appearance16.Image = ((object)(resources.GetObject("appearance16.Image")));
            ultraExplorerBarGroup1.Settings.AppearancesSmall.HeaderAppearance = appearance16;
            ultraExplorerBarGroup1.Settings.NavigationPaneCollapsedGroupAreaText = "Bàn làm việc";
            ultraExplorerBarGroup1.Text = "Bàn làm việc";
            ultraExplorerBarGroup1.ToolTipText = "Bàn làm việc";
            ultraExplorerBarGroup2.Expanded = false;
            ultraExplorerBarItem1.Key = "FMCReceipt";
            ultraExplorerBarItem1.Text = "Phiếu thu";
            ultraExplorerBarItem2.Key = "FMCPayment";
            ultraExplorerBarItem2.Text = "Phiếu chi";
            ultraExplorerBarItem3.Key = "FMBTellerPaper";
            ultraExplorerBarItem3.Text = "Séc/Ủy nhiệm chi";
            ultraExplorerBarItem4.Key = "FMBCreditCard";
            ultraExplorerBarItem4.Text = "Thẻ tín dụng";
            ultraExplorerBarItem5.Key = "FMBDeposit";
            ultraExplorerBarItem5.Text = "Thu qua ngân hàng";
            ultraExplorerBarItem6.Key = "FMBInternalTransfer";
            ultraExplorerBarItem6.Text = "Chuyển tiền nội bộ";
            ultraExplorerBarItem7.Key = "FMCompare";
            ultraExplorerBarItem7.Text = "Đối chiếu ngân hàng";
            ultraExplorerBarItem8.Key = "FMCAudit";
            ultraExplorerBarItem8.Text = "Kiểm kê quỹ";
            ultraExplorerBarGroup2.Items.AddRange(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem[] {
            ultraExplorerBarItem1,
            ultraExplorerBarItem2,
            ultraExplorerBarItem3,
            ultraExplorerBarItem4,
            ultraExplorerBarItem5,
            ultraExplorerBarItem6,
            ultraExplorerBarItem7,
            ultraExplorerBarItem8});
            ultraExplorerBarGroup2.ItemSettings.HotTrackBorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            ultraExplorerBarGroup2.ItemSettings.HotTracking = Infragistics.Win.DefaultableBoolean.True;
            ultraExplorerBarGroup2.ItemSettings.HotTrackStyle = Infragistics.Win.UltraWinExplorerBar.ItemHotTrackStyle.HighlightEntireItem;
            ultraExplorerBarGroup2.ItemSettings.ShowToolTips = Infragistics.Win.DefaultableBoolean.True;
            ultraExplorerBarGroup2.Key = "Money&BankModule";
            appearance17.Image = global::Accounting.Properties.Resources.taikhoannganhang;
            ultraExplorerBarGroup2.Settings.AppearancesLarge.Appearance = appearance17;
            appearance18.Image = global::Accounting.Properties.Resources.taikhoannganhang;
            ultraExplorerBarGroup2.Settings.AppearancesLarge.HeaderAppearance = appearance18;
            appearance19.Image = global::Accounting.Properties.Resources.taikhoannganhang;
            ultraExplorerBarGroup2.Settings.AppearancesSmall.Appearance = appearance19;
            appearance20.Image = global::Accounting.Properties.Resources.taikhoannganhang;
            ultraExplorerBarGroup2.Settings.AppearancesSmall.HeaderAppearance = appearance20;
            ultraExplorerBarGroup2.Settings.NavigationPaneCollapsedGroupAreaText = "Tiền mặt và Ngân hàng";
            ultraExplorerBarGroup2.Text = "Tiền mặt và Ngân hàng";
            ultraExplorerBarGroup2.ToolTipText = "Tiền mặt và Ngân hàng";
            ultraExplorerBarItem9.Key = "FPPOrder";
            ultraExplorerBarItem9.Text = "Đơn mua hàng";
            ultraExplorerBarItem10.Key = "FPPInvoice1";
            ultraExplorerBarItem10.Text = "Mua hàng qua kho";
            ultraExplorerBarItem11.Key = "FPPInvoice2";
            ultraExplorerBarItem11.Text = "Mua hàng không qua kho";
            ultraExplorerBarItem12.Key = "FPPGetInvoices";
            ultraExplorerBarItem12.Text = "Nhận hóa đơn";
            ultraExplorerBarItem13.Key = "FPPService";
            ultraExplorerBarItem13.Text = "Mua dịch vụ";
            ultraExplorerBarItem14.Key = "FPPDiscountReturn";
            ultraExplorerBarItem14.Text = "Hàng mua trả lại, giảm giá";
            ultraExplorerBarItem15.Key = "FPPDiscountReturn1";
            ultraExplorerBarItem15.Text = "Hàng mua giảm giá";
            ultraExplorerBarItem15.Visible = false;
            ultraExplorerBarItem16.Key = "FPPPayVendor";
            ultraExplorerBarItem16.Text = "Trả tiền nhà cung cấp";
            ultraExplorerBarItem17.Key = "FPPExceptVoucher";
            ultraExplorerBarItem17.Text = "Đối trừ chứng từ";
            ultraExplorerBarGroup3.Items.AddRange(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem[] {
            ultraExplorerBarItem9,
            ultraExplorerBarItem10,
            ultraExplorerBarItem11,
            ultraExplorerBarItem12,
            ultraExplorerBarItem13,
            ultraExplorerBarItem14,
            ultraExplorerBarItem15,
            ultraExplorerBarItem16,
            ultraExplorerBarItem17});
            appearance21.Image = ((object)(resources.GetObject("appearance21.Image")));
            ultraExplorerBarGroup3.ItemSettings.AppearancesLarge.Appearance = appearance21;
            ultraExplorerBarGroup3.Key = "PurchasePriceModule";
            appearance22.Image = ((object)(resources.GetObject("appearance22.Image")));
            ultraExplorerBarGroup3.Settings.AppearancesLarge.HeaderAppearance = appearance22;
            appearance23.Image = global::Accounting.Properties.Resources.ketoanmua_vs_phaitra2;
            ultraExplorerBarGroup3.Settings.AppearancesSmall.HeaderAppearance = appearance23;
            ultraExplorerBarGroup3.Settings.NavigationPaneCollapsedGroupAreaText = "Mua hàng";
            ultraExplorerBarGroup3.Text = "Mua hàng";
            ultraExplorerBarGroup3.ToolTipText = "Mua hàng";
            ultraExplorerBarItem18.Key = "FSAQuote";
            ultraExplorerBarItem18.Text = "Báo giá";
            ultraExplorerBarItem19.Key = "FSAOrder";
            ultraExplorerBarItem19.Text = "Đơn đặt hàng";
            ultraExplorerBarItem20.Key = "FSAInvoicePayment0";
            ultraExplorerBarItem20.Text = "Bán hàng chưa thu tiền";
            ultraExplorerBarItem21.Key = "FSAInvoicePayment1";
            ultraExplorerBarItem21.Text = "Bán hàng thu tiền ngay";
            ultraExplorerBarItem22.Key = "FSAInvoicePayment2";
            ultraExplorerBarItem22.Text = "Bán hàng đại lý bán đúng giá, nhận ủy thác XNK";
            ultraExplorerBarItem23.Key = "FSABill";
            ultraExplorerBarItem23.Text = "Xuất hóa đơn";
            ultraExplorerBarItem24.Key = "FSAReturn";
            ultraExplorerBarItem24.Text = "Hàng bán trả lại, giảm giá";
            ultraExplorerBarItem25.Key = "FSAReceiptCustomer";
            ultraExplorerBarItem25.Text = "Thu tiền khách hàng";
            ultraExplorerBarItem26.Key = "FSAOverdueInterest";
            ultraExplorerBarItem26.Text = "Tính lãi nợ";
            ultraExplorerBarItem27.Key = "FSALiabilitiesReport";
            ultraExplorerBarItem27.Text = "Thông báo công nợ";
            ultraExplorerBarItem28.Key = "FSAExceptVoucher";
            ultraExplorerBarItem28.Text = "Đối trừ chứng từ";
            ultraExplorerBarItem29.Key = "FSASetPricePolicy";
            ultraExplorerBarItem29.Text = "Thiết lập chính sách giá";
            ultraExplorerBarItem30.Key = "FSACalculateTheCost";
            ultraExplorerBarItem30.Text = "Tính giá bán";
            ultraExplorerBarGroup4.Items.AddRange(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem[] {
            ultraExplorerBarItem18,
            ultraExplorerBarItem19,
            ultraExplorerBarItem20,
            ultraExplorerBarItem21,
            ultraExplorerBarItem22,
            ultraExplorerBarItem23,
            ultraExplorerBarItem24,
            ultraExplorerBarItem25,
            ultraExplorerBarItem26,
            ultraExplorerBarItem27,
            ultraExplorerBarItem28,
            ultraExplorerBarItem29,
            ultraExplorerBarItem30});
            ultraExplorerBarGroup4.Key = "SalePriceModule";
            appearance24.Image = ((object)(resources.GetObject("appearance24.Image")));
            ultraExplorerBarGroup4.Settings.AppearancesLarge.HeaderAppearance = appearance24;
            appearance25.Image = global::Accounting.Properties.Resources.BANHANGTHUTIENNGAY;
            ultraExplorerBarGroup4.Settings.AppearancesSmall.HeaderAppearance = appearance25;
            ultraExplorerBarGroup4.Settings.NavigationPaneCollapsedGroupAreaText = "Bán hàng";
            ultraExplorerBarGroup4.Text = "Bán hàng";
            ultraExplorerBarGroup4.ToolTipText = "Bán hàng";
            ultraExplorerBarItem31.Key = "FRSInwardOutward";
            ultraExplorerBarItem31.Text = "Nhập kho";
            ultraExplorerBarItem32.Key = "FRSInwardOutwardOutput";
            ultraExplorerBarItem32.Text = "Xuất kho";
            ultraExplorerBarItem33.Key = "FRSAssemblyDismantlement";
            ultraExplorerBarItem33.Text = "Lệnh lắp ráp, tháo dỡ";
            ultraExplorerBarItem34.Key = "FRSTransfer";
            ultraExplorerBarItem34.Text = "Chuyển kho";
            ultraExplorerBarItem35.Key = "FRSInventoryAdjustments";
            ultraExplorerBarItem35.Text = "Điều chỉnh tồn kho";
            ultraExplorerBarItem36.Key = "FRSPricingOW";
            ultraExplorerBarItem36.Text = "Tính giá xuất kho";
            ultraExplorerBarItem37.Key = "FRSInwardOutwardReorganize";
            ultraExplorerBarItem37.Text = "Sắp xếp thứ tự nhập xuất";
            ultraExplorerBarItem37.ToolTipText = "Sắp xếp lại thứ tự chứng từ xuất, nhập kho trong ngày";
            ultraExplorerBarItem38.Key = "FRSUpdatingIW";
            ultraExplorerBarItem38.Text = "Cập nhật giá nhập kho thành phẩm";
            ultraExplorerBarItem39.Key = "Rpt_FRS09DNN";
            appearance26.Image = global::Accounting.Properties.Resources.report;
            ultraExplorerBarItem39.Settings.AppearancesSmall.Appearance = appearance26;
            ultraExplorerBarItem39.Text = "Thẻ kho";
            ultraExplorerBarItem39.Visible = false;
            ultraExplorerBarItem40.Key = "Rpt_FRS07DNN";
            appearance27.Image = global::Accounting.Properties.Resources.report;
            ultraExplorerBarItem40.Settings.AppearancesSmall.Appearance = appearance27;
            ultraExplorerBarItem40.Text = "Sổ chi tiết vật liệu, dụng cụ, sản phẩm, hàng hoá";
            ultraExplorerBarItem40.Visible = false;
            ultraExplorerBarItem41.Key = "Rpt_FRS07DNN16";
            appearance28.Image = global::Accounting.Properties.Resources.report;
            ultraExplorerBarItem41.Settings.AppearancesSmall.Appearance = appearance28;
            ultraExplorerBarItem41.Text = "Bảng tổng hợp chi tiết";
            ultraExplorerBarItem41.Visible = false;
            ultraExplorerBarGroup5.Items.AddRange(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem[] {
            ultraExplorerBarItem31,
            ultraExplorerBarItem32,
            ultraExplorerBarItem33,
            ultraExplorerBarItem34,
            ultraExplorerBarItem35,
            ultraExplorerBarItem36,
            ultraExplorerBarItem37,
            ultraExplorerBarItem38,
            ultraExplorerBarItem39,
            ultraExplorerBarItem40,
            ultraExplorerBarItem41});
            ultraExplorerBarGroup5.Key = "RepositoryModule";
            appearance29.Image = ((object)(resources.GetObject("appearance29.Image")));
            ultraExplorerBarGroup5.Settings.AppearancesLarge.HeaderAppearance = appearance29;
            appearance30.Image = global::Accounting.Properties.Resources.XUATKHO;
            ultraExplorerBarGroup5.Settings.AppearancesSmall.HeaderAppearance = appearance30;
            ultraExplorerBarGroup5.Settings.NavigationPaneCollapsedGroupAreaText = "Kho";
            ultraExplorerBarGroup5.Text = "Kho";
            ultraExplorerBarGroup5.ToolTipText = "Kho";
            ultraExplorerBarItem42.Key = "CAT_MaterialTools";
            ultraExplorerBarItem42.Text = "Danh mục CCDC";
            ultraExplorerBarItem42.ToolTipText = "Danh mục công cụ dụng cụ";
            ultraExplorerBarItem42.Visible = false;
            ultraExplorerBarItem43.Key = "FTIInit";
            ultraExplorerBarItem43.Text = "Khai báo CCDC đầu kỳ";
            ultraExplorerBarItem44.Key = "FTIIncrement";
            ultraExplorerBarItem44.Text = "Mua và ghi tăng CCDC";
            ultraExplorerBarItem45.Key = "FTIIncrementAnother";
            ultraExplorerBarItem45.Text = "Ghi tăng khác";
            ultraExplorerBarItem46.Key = "FTIDecrement";
            ultraExplorerBarItem46.Text = "Ghi giảm CCDC";
            ultraExplorerBarItem47.Key = "FTIAdjustment";
            ultraExplorerBarItem47.Text = "Điều chỉnh CCDC";
            ultraExplorerBarItem48.Key = "FTITransfer";
            ultraExplorerBarItem48.Text = "Điều chuyển CCDC";
            ultraExplorerBarItem49.Key = "FTIAudit";
            ultraExplorerBarItem49.Text = "Kiểm kê CCDC";
            ultraExplorerBarItem49.ToolTipText = "Kiểm kê công cụ dụng cụ";
            ultraExplorerBarItem50.Key = "FTIAllocation";
            ultraExplorerBarItem50.Text = "Phân bổ CCDC";
            ultraExplorerBarItem50.ToolTipText = "Phân bổ công cụ dụng cụ";
            ultraExplorerBarItem51.Key = "Rpt_FRS11DNN_CCDC";
            appearance31.Image = global::Accounting.Properties.Resources.report;
            ultraExplorerBarItem51.Settings.AppearancesSmall.Appearance = appearance31;
            ultraExplorerBarItem51.Text = "Sổ theo dõi CCDC tại nơi SD";
            ultraExplorerBarItem51.ToolTipText = "Sổ công cụ dụng cụ";
            ultraExplorerBarItem51.Visible = false;
            ultraExplorerBarGroup6.Items.AddRange(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem[] {
            ultraExplorerBarItem42,
            ultraExplorerBarItem43,
            ultraExplorerBarItem44,
            ultraExplorerBarItem45,
            ultraExplorerBarItem46,
            ultraExplorerBarItem47,
            ultraExplorerBarItem48,
            ultraExplorerBarItem49,
            ultraExplorerBarItem50,
            ultraExplorerBarItem51});
            ultraExplorerBarGroup6.Key = "ToolModule";
            appearance32.Image = ((object)(resources.GetObject("appearance32.Image")));
            ultraExplorerBarGroup6.Settings.AppearancesLarge.HeaderAppearance = appearance32;
            appearance33.Image = global::Accounting.Properties.Resources.Congcu_dungcu21;
            ultraExplorerBarGroup6.Settings.AppearancesSmall.HeaderAppearance = appearance33;
            ultraExplorerBarGroup6.Settings.NavigationPaneCollapsedGroupAreaText = "Công cụ dụng cụ";
            ultraExplorerBarGroup6.Text = "Công cụ dụng cụ";
            ultraExplorerBarGroup6.ToolTipText = "Công cụ dụng cụ";
            ultraExplorerBarItem52.Key = "FFAInit";
            ultraExplorerBarItem52.Text = "Khai báo TSCĐ đầu kỳ";
            ultraExplorerBarItem53.Key = "FFABuyAndIncrement";
            ultraExplorerBarItem53.Text = "Mua và ghi tăng TSCĐ";
            ultraExplorerBarItem54.Key = "FFAIncrement";
            ultraExplorerBarItem54.Text = "Ghi tăng TSCĐ khác";
            ultraExplorerBarItem55.Key = "FFADecrement";
            ultraExplorerBarItem55.Text = "Ghi giảm TSCĐ";
            ultraExplorerBarItem56.Key = "FFAAdjustment";
            ultraExplorerBarItem56.Text = "Điều chỉnh TSCĐ";
            ultraExplorerBarItem57.Key = "FFADepreciation";
            ultraExplorerBarItem57.Text = "Tính khấu hao TSCĐ";
            ultraExplorerBarItem58.Key = "FFAAudit";
            ultraExplorerBarItem58.Text = "Kiểm kê TSCĐ";
            ultraExplorerBarItem59.Key = "FFATransfer";
            ultraExplorerBarItem59.Text = "Điều chuyển TSCĐ";
            ultraExplorerBarItem60.Key = "Rpt_FRS12SDNN";
            appearance34.Image = global::Accounting.Properties.Resources.report;
            ultraExplorerBarItem60.Settings.AppearancesSmall.Appearance = appearance34;
            ultraExplorerBarItem60.Text = "Thẻ TSCĐ";
            ultraExplorerBarItem60.Visible = false;
            ultraExplorerBarItem61.Key = "Rpt_FRS10DNN";
            ultraExplorerBarItem61.Settings.AppearancesSmall.Appearance = appearance31;
            ultraExplorerBarItem61.Text = "Sổ TSCĐ";
            ultraExplorerBarItem61.Visible = false;
            ultraExplorerBarItem62.Key = "Rpt_FRS11DNN";
            appearance35.Image = global::Accounting.Properties.Resources.report;
            ultraExplorerBarItem62.Settings.AppearancesSmall.Appearance = appearance35;
            ultraExplorerBarItem62.Text = "Sổ theo dõi TSCĐ tại nơi sử dụng";
            ultraExplorerBarItem62.Visible = false;
            ultraExplorerBarGroup7.Items.AddRange(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem[] {
            ultraExplorerBarItem52,
            ultraExplorerBarItem53,
            ultraExplorerBarItem54,
            ultraExplorerBarItem55,
            ultraExplorerBarItem56,
            ultraExplorerBarItem57,
            ultraExplorerBarItem58,
            ultraExplorerBarItem59,
            ultraExplorerBarItem60,
            ultraExplorerBarItem61,
            ultraExplorerBarItem62});
            ultraExplorerBarGroup7.Key = "FixedAssetModule";
            appearance36.Image = ((object)(resources.GetObject("appearance36.Image")));
            ultraExplorerBarGroup7.Settings.AppearancesLarge.HeaderAppearance = appearance36;
            appearance37.Image = global::Accounting.Properties.Resources.khaibaotscd1;
            ultraExplorerBarGroup7.Settings.AppearancesSmall.HeaderAppearance = appearance37;
            ultraExplorerBarGroup7.Settings.NavigationPaneCollapsedGroupAreaText = "Tài sản cố định";
            ultraExplorerBarGroup7.Text = "Tài sản cố định";
            ultraExplorerBarGroup7.ToolTipText = "Tài sản cố định";
            ultraExplorerBarItem63.Key = "FEMContractSale";
            ultraExplorerBarItem63.Text = "Hợp đồng bán";
            ultraExplorerBarItem64.Key = "FEMContractBuy";
            ultraExplorerBarItem64.Text = "Hợp đồng mua";
            ultraExplorerBarItem65.Key = "FRContractStatusSale";
            ultraExplorerBarItem65.Settings.AppearancesSmall.Appearance = appearance35;
            ultraExplorerBarItem65.Text = "Tình hình thực hiện HD bán";
            ultraExplorerBarItem65.Visible = false;
            ultraExplorerBarItem66.Key = "FRContractStatusPurchase";
            ultraExplorerBarItem66.Settings.AppearancesSmall.Appearance = appearance35;
            ultraExplorerBarItem66.Text = "Tình hình thực hiện HD mua";
            ultraExplorerBarItem66.Visible = false;
            ultraExplorerBarGroup8.Items.AddRange(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem[] {
            ultraExplorerBarItem63,
            ultraExplorerBarItem64,
            ultraExplorerBarItem65,
            ultraExplorerBarItem66});
            ultraExplorerBarGroup8.Key = "EMContractModule";
            appearance38.Image = global::Accounting.Properties.Resources.HOPDONGBAN;
            ultraExplorerBarGroup8.Settings.AppearancesLarge.HeaderAppearance = appearance38;
            ultraExplorerBarGroup8.Settings.AppearancesSmall.HeaderAppearance = appearance38;
            ultraExplorerBarGroup8.Settings.NavigationPaneCollapsedGroupAreaText = "Hợp đồng";
            ultraExplorerBarGroup8.Text = "Hợp đồng";
            ultraExplorerBarGroup8.ToolTipText = "Hợp đồng";
            ultraExplorerBarItem67.Key = "FGOtherVoucher";
            ultraExplorerBarItem67.Text = "Chứng từ nghiệp vụ khác";
            ultraExplorerBarItem68.Key = "FGOInterestAndLossTransfer";
            ultraExplorerBarItem68.Text = "Kết chuyển lãi lỗ";
            ultraExplorerBarItem69.Key = "FGVoucherList";
            ultraExplorerBarItem69.Text = "Chứng từ ghi sổ";
            ultraExplorerBarItem70.Key = "FOffsetLiabilities";
            ultraExplorerBarItem70.Text = "Bù trừ công nợ";
            ultraExplorerBarItem71.Key = "FDBDateClosed";
            ultraExplorerBarItem71.Text = "Khóa sổ kỳ kế toán";
            ultraExplorerBarItem72.Key = "FGDisposeDbDateClosed";
            ultraExplorerBarItem72.Text = "Bỏ khóa sổ kỳ kế toán";
            ultraExplorerBarItem73.Key = "FGSolveExchangeRate";
            ultraExplorerBarItem73.Text = "Xử lý chênh lệch tỷ giá";
            ultraExplorerBarItem73.Visible = false;
            ultraExplorerBarItem74.Key = "FGCashBankRate";
            ultraExplorerBarItem74.Text = "Tính tỷ giá xuất quỹ";
            ultraExplorerBarItem75.Key = "FGCurrencyAssessment";
            ultraExplorerBarItem75.Text = "Đánh giá lại tài khoản ngoại tệ";
            ultraExplorerBarItem76.Key = "FPrepaidExpense";
            ultraExplorerBarItem76.Text = "Chi phí trả trước";
            ultraExplorerBarItem77.Key = "FGOtherVoucherExpense";
            ultraExplorerBarItem77.Text = "Phân bổ chi phí trả trước";
            ultraExplorerBarGroup9.Items.AddRange(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem[] {
            ultraExplorerBarItem67,
            ultraExplorerBarItem68,
            ultraExplorerBarItem69,
            ultraExplorerBarItem70,
            ultraExplorerBarItem71,
            ultraExplorerBarItem72,
            ultraExplorerBarItem73,
            ultraExplorerBarItem74,
            ultraExplorerBarItem75,
            ultraExplorerBarItem76,
            ultraExplorerBarItem77});
            ultraExplorerBarGroup9.Key = "GeneralModule";
            appearance39.Image = ((object)(resources.GetObject("appearance39.Image")));
            ultraExplorerBarGroup9.Settings.AppearancesLarge.HeaderAppearance = appearance39;
            ultraExplorerBarGroup9.Settings.AppearancesSmall.HeaderAppearance = appearance39;
            ultraExplorerBarGroup9.Settings.NavigationPaneCollapsedGroupAreaText = "Tổng hợp";
            ultraExplorerBarGroup9.Text = "Tổng hợp";
            ultraExplorerBarGroup9.ToolTipText = "Tổng hợp";
            ultraExplorerBarItem78.Key = "FPSTimeSheet";
            ultraExplorerBarItem78.Text = "Chấm công";
            ultraExplorerBarItem78.ToolTipText = "Chấm công";
            ultraExplorerBarItem79.Key = "FPSTimeSheetSummary";
            ultraExplorerBarItem79.Text = "Tổng hợp chấm công";
            ultraExplorerBarItem79.ToolTipText = "Tổng hợp chấm công";
            ultraExplorerBarItem80.Key = "FPSalarySheet";
            ultraExplorerBarItem80.Text = "Bảng lương";
            ultraExplorerBarItem80.ToolTipText = "Bảng lương";
            ultraExplorerBarItem81.Key = "FPSOtherVoucher";
            ultraExplorerBarItem81.Text = "Hạch toán chi phí lương";
            ultraExplorerBarItem82.Key = "FInsurancePayment";
            ultraExplorerBarItem82.Text = "Nộp tiền bảo hiểm";
            ultraExplorerBarItem83.Key = "FWagePayment";
            ultraExplorerBarItem83.Text = "Thanh toán lương";
            ultraExplorerBarGroup10.Items.AddRange(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem[] {
            ultraExplorerBarItem78,
            ultraExplorerBarItem79,
            ultraExplorerBarItem80,
            ultraExplorerBarItem81,
            ultraExplorerBarItem82,
            ultraExplorerBarItem83});
            ultraExplorerBarGroup10.Key = "SalaryModule";
            appearance40.Image = ((object)(resources.GetObject("appearance40.Image")));
            ultraExplorerBarGroup10.Settings.AppearancesLarge.HeaderAppearance = appearance40;
            appearance41.Image = ((object)(resources.GetObject("appearance41.Image")));
            ultraExplorerBarGroup10.Settings.AppearancesSmall.HeaderAppearance = appearance41;
            ultraExplorerBarGroup10.Text = "Tiền lương";
            ultraExplorerBarGroup10.ToolTipText = "Tiền lương";
            ultraExplorerBarItem84.Key = "FTT153Report";
            ultraExplorerBarItem84.Text = "Khởi tạo mẫu hóa đơn";
            ultraExplorerBarItem84.ToolTipText = "Khởi tạo mẫu hóa đơn";
            ultraExplorerBarItem85.Key = "FTT153RegisterInvoice";
            ultraExplorerBarItem85.Text = "Đăng ký sử dụng HD";
            ultraExplorerBarItem85.ToolTipText = "Đăng ký sử dụng HD";
            ultraExplorerBarItem86.Key = "FTT153PublishInvoice";
            ultraExplorerBarItem86.Text = "Thông báo phát hành HD";
            ultraExplorerBarItem86.ToolTipText = "Thông báo phát hành HD";
            ultraExplorerBarItem87.Key = "FTT153DestructionInvoice";
            ultraExplorerBarItem87.Text = "Hủy hóa đơn";
            ultraExplorerBarItem87.ToolTipText = "Hủy hóa đơn";
            ultraExplorerBarItem88.Key = "FTT153LostInvoice";
            ultraExplorerBarItem88.Text = "Mất, cháy, hỏng hóa đơn";
            ultraExplorerBarItem88.ToolTipText = "Mất, cháy, hỏng hóa đơn";
            ultraExplorerBarItem89.Key = "FTT153DeletedInvoice";
            ultraExplorerBarItem89.Text = "Xóa hóa đơn";
            ultraExplorerBarItem89.ToolTipText = "Xóa hóa đơn";
            ultraExplorerBarItem90.Key = "FTT153AdjustAnnouncement";
            ultraExplorerBarItem90.Text = "Điều chỉnh thông báo phát hành";
            ultraExplorerBarItem90.ToolTipText = "Điều chỉnh thông báo phát hành";
            ultraExplorerBarItem91.Key = "FRTinhHinhSuDungHD";
            appearance42.Image = global::Accounting.Properties.Resources.report;
            ultraExplorerBarItem91.Settings.AppearancesSmall.Appearance = appearance42;
            ultraExplorerBarItem91.Text = "Tình hình sử dụng hóa đơn";
            ultraExplorerBarItem91.ToolTipText = "Tình hình sử dụng hóa đơn";
            ultraExplorerBarItem91.Visible = false;
            ultraExplorerBarGroup11.Items.AddRange(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem[] {
            ultraExplorerBarItem84,
            ultraExplorerBarItem85,
            ultraExplorerBarItem86,
            ultraExplorerBarItem87,
            ultraExplorerBarItem88,
            ultraExplorerBarItem89,
            ultraExplorerBarItem90,
            ultraExplorerBarItem91});
            ultraExplorerBarGroup11.Key = "InvoiceModule";
            appearance43.Image = global::Accounting.Properties.Resources.hd;
            ultraExplorerBarGroup11.Settings.AppearancesLarge.HeaderAppearance = appearance43;
            appearance44.Image = global::Accounting.Properties.Resources.hd;
            ultraExplorerBarGroup11.Settings.AppearancesSmall.HeaderAppearance = appearance44;
            ultraExplorerBarGroup11.Text = "Quản lý hóa đơn";
            ultraExplorerBarGroup11.ToolTipText = "Quản lý hóa đơn";
            ultraExplorerBarItem92.Key = "FMaterialQuantum";
            appearance45.Image = global::Accounting.Properties.Resources.listview;
            ultraExplorerBarItem92.Settings.AppearancesSmall.Appearance = appearance45;
            ultraExplorerBarItem92.Text = "Định mức NVL";
            ultraExplorerBarItem93.Key = "FCPProductQuantum";
            appearance46.Image = global::Accounting.Properties.Resources.listview;
            ultraExplorerBarItem93.Settings.AppearancesSmall.Appearance = appearance46;
            ultraExplorerBarItem93.Text = "Định mức giá thành thành phẩm";
            ultraExplorerBarItem94.Key = "FCPAllocationQuantum";
            appearance47.Image = global::Accounting.Properties.Resources.listview;
            ultraExplorerBarItem94.Settings.AppearancesSmall.Appearance = appearance47;
            ultraExplorerBarItem94.Text = "Định mức phân bổ chi phí ";
            ultraExplorerBarItem95.Key = "FCPOPN";
            appearance48.Image = global::Accounting.Properties.Resources.listview;
            ultraExplorerBarItem95.Settings.AppearancesSmall.Appearance = appearance48;
            ultraExplorerBarItem95.Text = "Chi phí dở dang đầu kỳ";
            ultraExplorerBarItem96.Key = "FCPSimpleMethod";
            appearance49.Image = global::Accounting.Properties.Resources.listview;
            ultraExplorerBarItem96.Settings.AppearancesSmall.Appearance = appearance49;
            ultraExplorerBarItem96.Text = "Phương pháp giản đơn";
            ultraExplorerBarItem97.Key = "FCPFactorMethod";
            appearance50.Image = global::Accounting.Properties.Resources.listview;
            ultraExplorerBarItem97.Settings.AppearancesSmall.Appearance = appearance50;
            ultraExplorerBarItem97.Text = "Phương pháp hệ số";
            ultraExplorerBarItem98.Key = "FCPCostOfRate";
            appearance51.Image = global::Accounting.Properties.Resources.listview;
            ultraExplorerBarItem98.Settings.AppearancesSmall.Appearance = appearance51;
            ultraExplorerBarItem98.Text = "Phương pháp tỷ lệ";
            ultraExplorerBarItem99.Key = "FCPCostOfConstruction";
            appearance52.Image = global::Accounting.Properties.Resources.listview;
            ultraExplorerBarItem99.Settings.AppearancesSmall.Appearance = appearance52;
            ultraExplorerBarItem99.Text = "Giá thành theo công trình, vụ việc";
            ultraExplorerBarItem100.Key = "FCPCostPerOrder";
            appearance53.Image = global::Accounting.Properties.Resources.listview;
            ultraExplorerBarItem100.Settings.AppearancesSmall.Appearance = appearance53;
            ultraExplorerBarItem100.Text = "Giá thành theo đơn hàng";
            ultraExplorerBarItem101.Key = "FCPCostOfContract";
            appearance54.Image = global::Accounting.Properties.Resources.listview;
            ultraExplorerBarItem101.Settings.AppearancesSmall.Appearance = appearance54;
            ultraExplorerBarItem101.Text = "Giá thành theo hợp đồng";
            ultraExplorerBarItem102.Key = "FRCPPeriod";
            appearance55.Image = global::Accounting.Properties.Resources.report;
            ultraExplorerBarItem102.Settings.AppearancesSmall.Appearance = appearance55;
            ultraExplorerBarItem102.Text = "Thẻ tính giá thành";
            ultraExplorerBarItem102.Visible = false;
            ultraExplorerBarGroup12.Items.AddRange(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem[] {
            ultraExplorerBarItem92,
            ultraExplorerBarItem93,
            ultraExplorerBarItem94,
            ultraExplorerBarItem95,
            ultraExplorerBarItem96,
            ultraExplorerBarItem97,
            ultraExplorerBarItem98,
            ultraExplorerBarItem99,
            ultraExplorerBarItem100,
            ultraExplorerBarItem101,
            ultraExplorerBarItem102});
            appearance56.Image = global::Accounting.Properties.Resources.moduleGiaThanh;
            ultraExplorerBarGroup12.ItemSettings.AppearancesLarge.ActiveAppearance = appearance56;
            appearance57.Image = global::Accounting.Properties.Resources.moduleGiaThanh;
            ultraExplorerBarGroup12.ItemSettings.AppearancesLarge.Appearance = appearance57;
            appearance58.Image = global::Accounting.Properties.Resources.moduleGiaThanh;
            ultraExplorerBarGroup12.ItemSettings.AppearancesSmall.Appearance = appearance58;
            ultraExplorerBarGroup12.Key = "CostModule";
            appearance59.Image = global::Accounting.Properties.Resources.gt;
            ultraExplorerBarGroup12.Settings.AppearancesLarge.HeaderAppearance = appearance59;
            appearance60.Image = global::Accounting.Properties.Resources.gt;
            ultraExplorerBarGroup12.Settings.AppearancesSmall.HeaderAppearance = appearance60;
            ultraExplorerBarGroup12.Text = "Giá thành";
            ultraExplorerBarGroup12.ToolTipText = "Giá thành";
            ultraExplorerBarItem103.Key = "FDeductionGTGT01";
            ultraExplorerBarItem103.Text = "Tờ khai thuế GTGT khấu trừ (01/GTGT)";
            ultraExplorerBarItem103.ToolTipText = "Tờ khai thuế GTGT khấu trừ (01/GTGT)";
            ultraExplorerBarItem104.Key = "FDirectGTGT04";
            ultraExplorerBarItem104.Text = "Tờ khai thuế GTGT trực tiếp (04/GTGT)";
            ultraExplorerBarItem104.ToolTipText = "Tờ khai thuế GTGT trực tiếp (04/GTGT)";
            ultraExplorerBarItem104.Visible = false;
            ultraExplorerBarItem105.Key = "FTaxInvestmentProjects";
            ultraExplorerBarItem105.Text = "Tờ khai thuế GTGT cho dự án đầu tư (02/GTGT)";
            ultraExplorerBarItem105.ToolTipText = "Tờ khai thuế GTGT cho dự án đầu tư (02/GTGT)";
            ultraExplorerBarItem106.Key = "FTaxFinalization";
            ultraExplorerBarItem106.Text = "Quyết toán thuế TNDN năm (03/TNDN)";
            ultraExplorerBarItem106.ToolTipText = "Quyết toán thuế TNDN năm (03/TNDN)";
            ultraExplorerBarItem107.Key = "FDeductionTTDB01";
            ultraExplorerBarItem107.Text = "Tờ khai thuế tiêu thụ đặc biệt (01/TTĐB)";
            ultraExplorerBarItem107.ToolTipText = "Tờ khai thuế tiêu thụ đặc biệt (01/TTĐB)";
            ultraExplorerBarItem107.Visible = false;
            ultraExplorerBarItem108.Key = "FDeductionResourcesTAIN01";
            ultraExplorerBarItem108.Text = "Tờ khai thuế tài nguyên (01/TAIN)";
            ultraExplorerBarItem108.ToolTipText = "Tờ khai thuế tài nguyên (01/TAIN)";
            ultraExplorerBarItem109.Key = "FTaxFinalizationResourcesTAIN02";
            ultraExplorerBarItem109.Text = "Tờ khai quyết toán thuế tài nguyên (02/TAIN)";
            ultraExplorerBarItem109.ToolTipText = "Tờ khai quyết toán thuế tài nguyên (02/TAIN)";
            ultraExplorerBarItem110.Key = "TaxPeriod";
            ultraExplorerBarItem110.Text = "Khấu trừ thuế GTGT";
            ultraExplorerBarItem111.Key = "FTaxSubmit";
            ultraExplorerBarItem111.Text = "Nộp thuế";
            ultraExplorerBarGroup13.Items.AddRange(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarItem[] {
            ultraExplorerBarItem103,
            ultraExplorerBarItem104,
            ultraExplorerBarItem105,
            ultraExplorerBarItem106,
            ultraExplorerBarItem107,
            ultraExplorerBarItem108,
            ultraExplorerBarItem109,
            ultraExplorerBarItem110,
            ultraExplorerBarItem111});
            ultraExplorerBarGroup13.Key = "Tax";
            appearance61.Image = ((object)(resources.GetObject("appearance61.Image")));
            ultraExplorerBarGroup13.Settings.AppearancesLarge.Appearance = appearance61;
            appearance62.Image = global::Accounting.Properties.Resources.tax;
            ultraExplorerBarGroup13.Settings.AppearancesLarge.HeaderAppearance = appearance62;
            appearance63.Image = ((object)(resources.GetObject("appearance63.Image")));
            ultraExplorerBarGroup13.Settings.AppearancesSmall.Appearance = appearance63;
            appearance64.Image = global::Accounting.Properties.Resources.tax;
            ultraExplorerBarGroup13.Settings.AppearancesSmall.HeaderAppearance = appearance64;
            ultraExplorerBarGroup13.Text = "Thuế";
            ultraExplorerBarGroup13.ToolTipText = "Thuế";
            this.uExBar.Groups.AddRange(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup[] {
            ultraExplorerBarGroup1,
            ultraExplorerBarGroup2,
            ultraExplorerBarGroup3,
            ultraExplorerBarGroup4,
            ultraExplorerBarGroup5,
            ultraExplorerBarGroup6,
            ultraExplorerBarGroup7,
            ultraExplorerBarGroup8,
            ultraExplorerBarGroup9,
            ultraExplorerBarGroup10,
            ultraExplorerBarGroup11,
            ultraExplorerBarGroup12,
            ultraExplorerBarGroup13});
            appearance65.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance65.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance65.Image = global::Accounting.Properties.Resources.report;
            this.uExBar.GroupSettings.AppearancesLarge.ActiveAppearance = appearance65;
            appearance66.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance66.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.uExBar.GroupSettings.AppearancesLarge.ActiveHeaderAppearance = appearance66;
            appearance67.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance67.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance67.Image = global::Accounting.Properties.Resources.shortcut;
            this.uExBar.GroupSettings.AppearancesLarge.Appearance = appearance67;
            appearance68.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uExBar.GroupSettings.AppearancesLarge.NavigationPaneCollapsedGroupAreaAppearance = appearance68;
            appearance69.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uExBar.GroupSettings.AppearancesLarge.NavigationPaneCollapsedGroupAreaHotTrackAppearance = appearance69;
            appearance70.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uExBar.GroupSettings.AppearancesLarge.NavigationPaneExpansionButtonAppearance = appearance70;
            appearance71.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uExBar.GroupSettings.AppearancesLarge.NavigationPaneExpansionButtonHotTrackAppearance = appearance71;
            appearance72.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uExBar.GroupSettings.AppearancesLarge.NavigationPaneHeaderAppearance = appearance72;
            appearance73.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uExBar.GroupSettings.AppearancesLarge.ScrollButtonAppearance = appearance73;
            appearance74.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uExBar.GroupSettings.AppearancesLarge.ScrollButtonHotTrackAppearance = appearance74;
            appearance75.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uExBar.GroupSettings.AppearancesLarge.SelectedAppearance = appearance75;
            appearance76.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uExBar.GroupSettings.AppearancesLarge.SelectedHeaderAppearance = appearance76;
            appearance77.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uExBar.GroupSettings.AppearancesSmall.Appearance = appearance77;
            appearance78.Image = ((object)(resources.GetObject("appearance78.Image")));
            this.uExBar.GroupSettings.AppearancesSmall.HeaderAppearance = appearance78;
            this.uExBar.GroupSettings.BorderStyleItemArea = Infragistics.Win.UIElementBorderStyle.None;
            this.uExBar.GroupSettings.HeaderButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.uExBar.GroupSettings.HotTracking = Infragistics.Win.DefaultableBoolean.True;
            this.uExBar.GroupSettings.NavigationAllowHide = Infragistics.Win.DefaultableBoolean.True;
            this.uExBar.GroupSettings.NavigationPaneCollapsedGroupAreaText = "Mở rộng";
            this.uExBar.GroupSettings.ShowToolTips = Infragistics.Win.DefaultableBoolean.True;
            this.uExBar.GroupSettings.Style = Infragistics.Win.UltraWinExplorerBar.GroupStyle.SmallImagesWithText;
            this.uExBar.ImageSizeSmall = new System.Drawing.Size(20, 20);
            appearance79.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance79.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance79.Image = global::Accounting.Properties.Resources.HDDT_Module_2;
            this.uExBar.ItemSettings.AppearancesLarge.ActiveAppearance = appearance79;
            appearance80.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance80.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance80.Image = ((object)(resources.GetObject("appearance80.Image")));
            this.uExBar.ItemSettings.AppearancesLarge.Appearance = appearance80;
            appearance81.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance81.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance81.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance81.Image = global::Accounting.Properties.Resources.HDDT_Module_2;
            this.uExBar.ItemSettings.AppearancesLarge.CheckedAppearance = appearance81;
            appearance82.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance82.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance82.Image = global::Accounting.Properties.Resources.HDDT_Module_2;
            this.uExBar.ItemSettings.AppearancesLarge.EditAppearance = appearance82;
            appearance83.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance83.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance83.Image = global::Accounting.Properties.Resources.HDDT_Module_2;
            this.uExBar.ItemSettings.AppearancesLarge.HotTrackAppearance = appearance83;
            appearance84.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance84.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance84.Image = global::Accounting.Properties.Resources.listview;
            this.uExBar.ItemSettings.AppearancesSmall.ActiveAppearance = appearance84;
            appearance85.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance85.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance85.Image = global::Accounting.Properties.Resources.listview;
            this.uExBar.ItemSettings.AppearancesSmall.Appearance = appearance85;
            appearance86.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance86.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance86.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance86.Image = global::Accounting.Properties.Resources.listview;
            this.uExBar.ItemSettings.AppearancesSmall.CheckedAppearance = appearance86;
            appearance87.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance87.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance87.Image = global::Accounting.Properties.Resources.listview;
            this.uExBar.ItemSettings.AppearancesSmall.EditAppearance = appearance87;
            appearance88.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance88.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance88.Image = global::Accounting.Properties.Resources.listview;
            this.uExBar.ItemSettings.AppearancesSmall.HotTrackAppearance = appearance88;
            this.uExBar.ItemSettings.HotTrackBorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uExBar.ItemSettings.HotTracking = Infragistics.Win.DefaultableBoolean.True;
            this.uExBar.ItemSettings.HotTrackStyle = Infragistics.Win.UltraWinExplorerBar.ItemHotTrackStyle.HighlightEntireItem;
            this.uExBar.ItemSettings.ShowInkButton = Infragistics.Win.ShowInkButton.WhenInkAvailable;
            this.uExBar.ItemSettings.ShowToolTips = Infragistics.Win.DefaultableBoolean.True;
            this.uExBar.ItemSettings.Style = Infragistics.Win.UltraWinExplorerBar.ItemStyle.Button;
            this.uExBar.ItemSettings.UseMnemonics = Infragistics.Win.DefaultableBoolean.True;
            this.uExBar.Location = new System.Drawing.Point(4, 28);
            this.uExBar.Name = "uExBar";
            this.uExBar.NavigationMaxGroupHeaders = 11;
            appearance89.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(186)))), ((int)(((byte)(225)))));
            appearance89.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uExBar.NavigationOverflowButtonAreaAppearance = appearance89;
            this.uExBar.NavigationPaneExpansionMode = ((Infragistics.Win.UltraWinExplorerBar.NavigationPaneExpansionMode)((Infragistics.Win.UltraWinExplorerBar.NavigationPaneExpansionMode.OnButtonClick | Infragistics.Win.UltraWinExplorerBar.NavigationPaneExpansionMode.OnSizeChanged)));
            appearance90.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance90.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance90.BackColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance90.BackColorDisabled2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance90.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance90.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance90.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance90.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance90.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance90.ForeColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            this.uExBar.NavigationSplitterAppearance = appearance90;
            appearance91.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            scrollBarLook1.Appearance = appearance91;
            appearance92.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            scrollBarLook1.ButtonAppearance = appearance92;
            scrollBarLook1.ScrollBarArrowStyle = Infragistics.Win.UltraWinScrollBar.ScrollBarArrowStyle.BothAtEachEnd;
            scrollBarLook1.ViewStyle = Infragistics.Win.UltraWinScrollBar.ScrollBarViewStyle.Office2010;
            this.uExBar.ScrollBarLook = scrollBarLook1;
            this.uExBar.SettingsKey = "";
            this.uExBar.Size = new System.Drawing.Size(193, 690);
            this.uExBar.Style = Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarStyle.OutlookNavigationPane;
            this.uExBar.TabIndex = 0;
            this.uExBar.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uExBar.ViewStyle = Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarViewStyle.Office2007;
            this.uExBar.GroupClick += new Infragistics.Win.UltraWinExplorerBar.GroupClickEventHandler(this.uExBar_GroupClick);
            this.uExBar.ItemClick += new Infragistics.Win.UltraWinExplorerBar.ItemClickEventHandler(this.uExBarItemClick);
            this.uExBar.SelectedGroupChanged += new Infragistics.Win.UltraWinExplorerBar.SelectedGroupChangedEventHandler(this.uExBar_SelectedGroupChanged);
            // 
            // FMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1015, 749);
            this.Controls.Add(this.ultraSplitter1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.uExBar);
            this.Controls.Add(this._MainFrm_Toolbars_Dock_Area_Left);
            this.Controls.Add(this._MainFrm_Toolbars_Dock_Area_Right);
            this.Controls.Add(this._MainFrm_Toolbars_Dock_Area_Bottom);
            this.Controls.Add(this.uSttBar);
            this.Controls.Add(this._MainFrm_Toolbars_Dock_Area_Top);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phần mềm kế toán";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FMain_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FMain_FormClosed);
            this.Shown += new System.EventHandler(this.FMain_Shown);
            this.Layout += new System.Windows.Forms.LayoutEventHandler(this.FMain_Layout);
            this.Resize += new System.EventHandler(this.FMain_Resize);
            this.splitContainer1_Fill_Panel.ClientArea.ResumeLayout(false);
            this.ClientArea_Fill_Panel.ClientArea.ResumeLayout(false);
            this.cms4uExBar.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupMainMenu)).EndInit();
            this.groupMainMenu.ResumeLayout(false);
            this.palMainMenu.ClientArea.ResumeLayout(false);
            this.palMainMenu.ClientArea.PerformLayout();
            this.palUserControl.ClientArea.ResumeLayout(false);
            this.pnlAddNewGroup.ClientArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtToolTipNewGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNewCaptionGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uToolbarsManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uSttBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uExBar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel splitContainer1_Fill_Panel;
        private Infragistics.Win.UltraWinToolbars.RibbonGroup groupAccount;
        private Infragistics.Win.Misc.UltraPanel ClientArea_Fill_Panel;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _ClientArea_Toolbars_Dock_Area_Left;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _ClientArea_Toolbars_Dock_Area_Right;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _ClientArea_Toolbars_Dock_Area_Bottom;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _ClientArea_Toolbars_Dock_Area_Top;
        private Infragistics.Win.Misc.UltraPanel ClientArea_Fill_Panel_1;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _MainFrm_Toolbars_Dock_Area_Left;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _MainFrm_Toolbars_Dock_Area_Right;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _MainFrm_Toolbars_Dock_Area_Bottom;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _MainFrm_Toolbars_Dock_Area_Top;
        private System.Windows.Forms.Panel panel1;
        public Infragistics.Win.UltraWinStatusBar.UltraStatusBar uSttBar;
        private System.Windows.Forms.ContextMenuStrip cms4uExBar;
        private System.Windows.Forms.ToolStripMenuItem btnAddNewGroup;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem btnuExBarOption;
        private System.Windows.Forms.ToolStripMenuItem btnDeleteGroup;
        private Infragistics.Win.Misc.UltraPopupControlContainer popAddNewGroup;
        private System.Windows.Forms.ToolStripMenuItem btnShowAddNewGroup;
        public Infragistics.Win.UltraWinToolbars.UltraToolbarsManager uToolbarsManager;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        public Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar uExBar;
        private Infragistics.Win.Misc.UltraPanel palMainMenu;
        private Infragistics.Win.Misc.UltraPanel palUserControl;
        private Infragistics.Win.Misc.UltraPanel pnlAddNewGroup;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtToolTipNewGroup;
        private Infragistics.Win.Misc.UltraButton btnApplyAddNewGroup;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNewCaptionGroup;
        public Infragistics.Win.Misc.UltraGroupBox groupMainMenu;
    }
}