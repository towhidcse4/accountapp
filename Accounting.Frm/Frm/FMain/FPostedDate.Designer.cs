﻿namespace Accounting
{
    partial class FPostedDate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FPostedDate));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            this.btnApply = new Infragistics.Win.Misc.UltraButton();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.lblImage = new Infragistics.Win.Misc.UltraLabel();
            this.dtePostedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            ((System.ComponentModel.ISupportInitialize)(this.dtePostedDate)).BeginInit();
            this.SuspendLayout();
            // 
            // btnApply
            // 
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            this.btnApply.Appearance = appearance1;
            this.btnApply.Location = new System.Drawing.Point(94, 76);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(85, 26);
            this.btnApply.TabIndex = 8;
            this.btnApply.Text = "Đồng ý";
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnCancel
            // 
            appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
            this.btnCancel.Appearance = appearance2;
            this.btnCancel.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(185, 76);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 27);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(68, 29);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(95, 23);
            this.ultraLabel1.TabIndex = 9;
            this.ultraLabel1.Text = "Ngày hạch toán";
            // 
            // lblImage
            // 
            appearance3.Image = global::Accounting.Properties.Resources.Calendar;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance3.TextVAlignAsString = "Middle";
            this.lblImage.Appearance = appearance3;
            this.lblImage.AutoSize = true;
            this.lblImage.ImageSize = new System.Drawing.Size(48, 48);
            this.lblImage.Location = new System.Drawing.Point(12, 12);
            this.lblImage.Name = "lblImage";
            this.lblImage.Size = new System.Drawing.Size(48, 48);
            this.lblImage.TabIndex = 10;
            // 
            // dtePostedDate
            // 
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.dtePostedDate.Appearance = appearance4;
            this.dtePostedDate.AutoSize = false;
            this.dtePostedDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.dtePostedDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtePostedDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.dtePostedDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtePostedDate.Location = new System.Drawing.Point(162, 26);
            this.dtePostedDate.MaskInput = "dd/mm/yyyy";
            this.dtePostedDate.Name = "dtePostedDate";
            this.dtePostedDate.Size = new System.Drawing.Size(98, 21);
            this.dtePostedDate.TabIndex = 12;
            this.dtePostedDate.Value = null;
            // 
            // FPostedDate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(271, 110);
            this.Controls.Add(this.dtePostedDate);
            this.Controls.Add(this.lblImage);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FPostedDate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ngày hạch toán";
            ((System.ComponentModel.ISupportInitialize)(this.dtePostedDate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnApply;
        private Infragistics.Win.Misc.UltraButton btnCancel;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel lblImage;
        public Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtePostedDate;
    }
}