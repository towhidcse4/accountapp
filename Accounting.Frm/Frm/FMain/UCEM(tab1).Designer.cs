﻿namespace Accounting
{
    partial class UCEM_tab1_
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCEM_tab1_));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            this.btnEMContractBuy = new Infragistics.Win.Misc.UltraButton();
            this.btnEMContractSale = new Infragistics.Win.Misc.UltraButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSaleReport = new Infragistics.Win.Misc.UltraButton();
            this.SuspendLayout();
            // 
            // btnEMContractBuy
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance1.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnEMContractBuy.Appearance = appearance1;
            this.btnEMContractBuy.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
            this.btnEMContractBuy.HotTrackAppearance = appearance2;
            this.btnEMContractBuy.ImageSize = new System.Drawing.Size(80, 80);
            this.btnEMContractBuy.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnEMContractBuy.Location = new System.Drawing.Point(36, 35);
            this.btnEMContractBuy.Name = "btnEMContractBuy";
            this.btnEMContractBuy.Size = new System.Drawing.Size(92, 97);
            this.btnEMContractBuy.TabIndex = 35;
            // 
            // btnEMContractSale
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnEMContractSale.Appearance = appearance3;
            this.btnEMContractSale.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
            this.btnEMContractSale.HotTrackAppearance = appearance4;
            this.btnEMContractSale.ImageSize = new System.Drawing.Size(80, 80);
            this.btnEMContractSale.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnEMContractSale.Location = new System.Drawing.Point(36, 241);
            this.btnEMContractSale.Name = "btnEMContractSale";
            this.btnEMContractSale.Size = new System.Drawing.Size(92, 97);
            this.btnEMContractSale.TabIndex = 36;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(171, 273);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 33);
            this.label1.TabIndex = 42;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(171, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 33);
            this.label2.TabIndex = 43;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(108)))), ((int)(((byte)(158)))));
            this.label8.Location = new System.Drawing.Point(235, 82);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(6, 207);
            this.label8.TabIndex = 52;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.Location = new System.Drawing.Point(247, 170);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 33);
            this.label3.TabIndex = 53;
            // 
            // btnSaleReport
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance5.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnSaleReport.Appearance = appearance5;
            this.btnSaleReport.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
            this.btnSaleReport.HotTrackAppearance = appearance6;
            this.btnSaleReport.ImageSize = new System.Drawing.Size(80, 80);
            this.btnSaleReport.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnSaleReport.Location = new System.Drawing.Point(327, 138);
            this.btnSaleReport.Name = "btnSaleReport";
            this.btnSaleReport.Size = new System.Drawing.Size(92, 97);
            this.btnSaleReport.TabIndex = 54;
            // 
            // UCEM_tab1_
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.btnSaleReport);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnEMContractSale);
            this.Controls.Add(this.btnEMContractBuy);
            this.Name = "UCEM_tab1_";
            this.Size = new System.Drawing.Size(631, 433);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnEMContractBuy;
        private Infragistics.Win.Misc.UltraButton btnEMContractSale;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private Infragistics.Win.Misc.UltraButton btnSaleReport;
    }
}
