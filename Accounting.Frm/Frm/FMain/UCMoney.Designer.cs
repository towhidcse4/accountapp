﻿namespace Accounting
{
    partial class UCMoney
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCMoney));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            this.btnMCPayment = new Infragistics.Win.Misc.UltraButton();
            this.btnMCReceipt = new Infragistics.Win.Misc.UltraButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnRS05aDNN = new Infragistics.Win.Misc.UltraButton();
            this.SuspendLayout();
            // 
            // btnMCPayment
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance1.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnMCPayment.Appearance = appearance1;
            this.btnMCPayment.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
            this.btnMCPayment.HotTrackAppearance = appearance2;
            this.btnMCPayment.ImageSize = new System.Drawing.Size(80, 80);
            this.btnMCPayment.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnMCPayment.Location = new System.Drawing.Point(11, 257);
            this.btnMCPayment.Name = "btnMCPayment";
            this.btnMCPayment.Size = new System.Drawing.Size(92, 97);
            this.btnMCPayment.TabIndex = 39;
            // 
            // btnMCReceipt
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnMCReceipt.Appearance = appearance3;
            this.btnMCReceipt.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
            this.btnMCReceipt.HotTrackAppearance = appearance4;
            this.btnMCReceipt.ImageSize = new System.Drawing.Size(80, 80);
            this.btnMCReceipt.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnMCReceipt.Location = new System.Drawing.Point(11, 13);
            this.btnMCReceipt.Name = "btnMCReceipt";
            this.btnMCReceipt.Size = new System.Drawing.Size(92, 97);
            this.btnMCReceipt.TabIndex = 40;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(130, 289);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 33);
            this.label2.TabIndex = 46;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(130, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 33);
            this.label1.TabIndex = 47;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(108)))), ((int)(((byte)(158)))));
            this.label8.Location = new System.Drawing.Point(194, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(6, 248);
            this.label8.TabIndex = 53;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.Location = new System.Drawing.Point(208, 167);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 33);
            this.label3.TabIndex = 54;
            // 
            // btnRS05aDNN
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance5.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnRS05aDNN.Appearance = appearance5;
            this.btnRS05aDNN.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
            this.btnRS05aDNN.HotTrackAppearance = appearance6;
            this.btnRS05aDNN.ImageSize = new System.Drawing.Size(80, 80);
            this.btnRS05aDNN.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnRS05aDNN.Location = new System.Drawing.Point(288, 135);
            this.btnRS05aDNN.Name = "btnRS05aDNN";
            this.btnRS05aDNN.Size = new System.Drawing.Size(92, 97);
            this.btnRS05aDNN.TabIndex = 55;
            // 
            // UCMoney
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.btnRS05aDNN);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnMCReceipt);
            this.Controls.Add(this.btnMCPayment);
            this.Name = "UCMoney";
            this.Size = new System.Drawing.Size(394, 367);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        public Infragistics.Win.Misc.UltraButton btnMCPayment;
        public Infragistics.Win.Misc.UltraButton btnMCReceipt;
        public Infragistics.Win.Misc.UltraButton btnRS05aDNN;
    }
}
