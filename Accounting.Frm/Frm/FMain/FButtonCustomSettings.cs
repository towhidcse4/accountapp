﻿using Accounting.TextMessage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FButtonCustomSettings : DialogForm
    {
        private List<Accounting.Core.Domain.ControlsMapping.Item> buttons = new List<Core.Domain.ControlsMapping.Item>();

        public List<Accounting.Core.Domain.ControlsMapping.Item> Buttons
        {
            get { return buttons; }
        }
        public FButtonCustomSettings(List<Accounting.Core.Domain.ControlsMapping.Item> input)
        {
            InitializeComponent();
            if (input.Count > 0)
                buttons = input.CloneObject();
            ConfigGrid();
            if (uGrid.Rows.Count > 0)
                uGrid.ActiveRow = uGrid.Rows[0];
            else
            {
                btnUp.Enabled = false;
                btnDown.Enabled = false;
            }
            if (uGrid.ActiveRow != null)
                uGrid_ClickCell(uGrid, new Infragistics.Win.UltraWinGrid.ClickCellEventArgs(uGrid.ActiveRow.Cells["Caption"]));
        }

        private void ConfigGrid()
        {
            uGrid.DataSource = buttons;
            uGrid.ConfigGrid(ConstDatabase.ItemControl_KeyName, setReadOnlyColor: false);
            uGrid.DisplayLayout.Bands[0].ColHeadersVisible = false;
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            if (uGrid.ActiveRow == null) return;
            var item = (uGrid.Rows[uGrid.ActiveRow.Index - 1].ListObject as Accounting.Core.Domain.ControlsMapping.Item).CloneObject();
            foreach (var cell in uGrid.Rows[uGrid.ActiveRow.Index - 1].Cells)
            {
                if (cell.Column.Key == "Position")
                    cell.Value = Convert.ToInt32(uGrid.ActiveRow.Cells[cell.Column.Key].Value) - 1;
                else
                    cell.Value = uGrid.ActiveRow.Cells[cell.Column.Key].Value;
            }
            foreach (var cell in uGrid.ActiveRow.Cells)
            {
                if (cell.Column.Key == "Position")
                    cell.Value = Convert.ToInt32(item.GetProperty(cell.Column.Key)) + 1;
                else
                    cell.Value = item.GetProperty(cell.Column.Key);
            }
            uGrid.ActiveRow = uGrid.Rows[uGrid.ActiveRow.Index - 1];
            uGrid_ClickCell(uGrid, new Infragistics.Win.UltraWinGrid.ClickCellEventArgs(uGrid.ActiveRow.Cells["Caption"]));
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            if (uGrid.ActiveRow == null) return;
            var item = (uGrid.Rows[uGrid.ActiveRow.Index + 1].ListObject as Accounting.Core.Domain.ControlsMapping.Item).CloneObject();
            foreach (var cell in uGrid.Rows[uGrid.ActiveRow.Index + 1].Cells)
            {
                if (cell.Column.Key == "Position")
                    cell.Value = Convert.ToInt32(uGrid.ActiveRow.Cells[cell.Column.Key].Value) + 1;
                else
                    cell.Value = uGrid.ActiveRow.Cells[cell.Column.Key].Value;
            }
            foreach (var cell in uGrid.ActiveRow.Cells)
            {
                if (cell.Column.Key == "Position")
                    cell.Value = Convert.ToInt32(item.GetProperty(cell.Column.Key)) - 1;
                else
                    cell.Value = item.GetProperty(cell.Column.Key);
            }
            uGrid.ActiveRow = uGrid.Rows[uGrid.ActiveRow.Index + 1];
            uGrid_ClickCell(uGrid, new Infragistics.Win.UltraWinGrid.ClickCellEventArgs(uGrid.ActiveRow.Cells["Caption"]));
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            uGrid.UpdateDataGrid();
            buttons = uGrid.DataSource as List<Accounting.Core.Domain.ControlsMapping.Item>;
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void uGrid_ClickCell(object sender, Infragistics.Win.UltraWinGrid.ClickCellEventArgs e)
        {
            if (uGrid.ActiveRow == null || uGrid.ActiveRow != e.Cell.Row)
                uGrid.ActiveRow = e.Cell.Row;
            btnUp.Enabled = e.Cell.Row.Index > 0;
            btnDown.Enabled = e.Cell.Row.Index < uGrid.Rows.Count - 1 && e.Cell.Row.Index > -1;
        }
    }
}
