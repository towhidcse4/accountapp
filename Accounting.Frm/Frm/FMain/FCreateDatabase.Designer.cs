﻿namespace Accounting
{
    partial class FCreateDatabase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FCreateDatabase));
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.txtEmailForgotPass = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.txtEmail = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.txtFax = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.txtPhoneNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCompanyTaxcode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCompanyAdress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCompanyName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnBrowserFilder = new Infragistics.Win.Misc.UltraButton();
            this.txtDataPatch = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDatabase = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.txtPwd = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtUsr = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbServers = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.dteDBStartDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.txtYear = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.dteDBStartYear = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox7 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbPPTGXK = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox6 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbNhomNNMD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbNhomHHDV = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraOptionSet1 = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.fbrDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.tabControl = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraPanel4 = new Infragistics.Win.Misc.UltraPanel();
            this.pnlSelectServerOldDB = new Infragistics.Win.Misc.UltraPanel();
            this.btnApplyServerOldDB = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox9 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbServerOldDB = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel29 = new Infragistics.Win.Misc.UltraLabel();
            this.txtPwdServerOldDB = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtUsrServerOldDB = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this.btnBack = new Infragistics.Win.Misc.UltraButton();
            this.btnNext = new Infragistics.Win.Misc.UltraButton();
            this.btnOk = new Infragistics.Win.Misc.UltraButton();
            this.BtnCancel = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.popContainer = new Infragistics.Win.Misc.UltraPopupControlContainer(this.components);
            this.ultraGroupBox8 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.ultraGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailForgotPass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhoneNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyAdress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDataPatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatabase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPwd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbServers)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteDBStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDBStartYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).BeginInit();
            this.ultraGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPPTGXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).BeginInit();
            this.ultraGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNhomNNMD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNhomHHDV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet1)).BeginInit();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.ultraPanel4.ClientArea.SuspendLayout();
            this.ultraPanel4.SuspendLayout();
            this.pnlSelectServerOldDB.ClientArea.SuspendLayout();
            this.pnlSelectServerOldDB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox9)).BeginInit();
            this.ultraGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbServerOldDB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPwdServerOldDB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsrServerOldDB)).BeginInit();
            this.ultraPanel3.ClientArea.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox8)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox5);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox3);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(0, 0);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(645, 471);
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox5.Controls.Add(this.ultraLabel13);
            this.ultraGroupBox5.Controls.Add(this.txtEmailForgotPass);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel12);
            this.ultraGroupBox5.Controls.Add(this.txtEmail);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel11);
            this.ultraGroupBox5.Controls.Add(this.txtFax);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox5.Controls.Add(this.txtPhoneNumber);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox5.Controls.Add(this.txtCompanyTaxcode);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox5.Controls.Add(this.txtCompanyAdress);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox5.Controls.Add(this.txtCompanyName);
            this.ultraGroupBox5.Location = new System.Drawing.Point(12, 256);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(621, 198);
            this.ultraGroupBox5.TabIndex = 7;
            this.ultraGroupBox5.Text = "Thông tin công ty";
            // 
            // ultraLabel13
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance1;
            this.ultraLabel13.Location = new System.Drawing.Point(22, 163);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(111, 20);
            this.ultraLabel13.TabIndex = 23;
            this.ultraLabel13.Text = "Email quên mật khẩu";
            // 
            // txtEmailForgotPass
            // 
            this.txtEmailForgotPass.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEmailForgotPass.AutoSize = false;
            this.txtEmailForgotPass.Location = new System.Drawing.Point(139, 160);
            this.txtEmailForgotPass.Name = "txtEmailForgotPass";
            this.txtEmailForgotPass.Size = new System.Drawing.Size(462, 22);
            this.txtEmailForgotPass.TabIndex = 22;
            this.txtEmailForgotPass.Leave += new System.EventHandler(this.txtEmailForgotPass_Leave);
            // 
            // ultraLabel12
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance2;
            this.ultraLabel12.Location = new System.Drawing.Point(23, 135);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(90, 20);
            this.ultraLabel12.TabIndex = 21;
            this.ultraLabel12.Text = "Email công ty";
            // 
            // txtEmail
            // 
            this.txtEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEmail.AutoSize = false;
            this.txtEmail.Location = new System.Drawing.Point(140, 132);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(462, 22);
            this.txtEmail.TabIndex = 20;
            this.txtEmail.Leave += new System.EventHandler(this.txtEmail_Leave);
            // 
            // ultraLabel11
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance3;
            this.ultraLabel11.Location = new System.Drawing.Point(343, 105);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(31, 20);
            this.ultraLabel11.TabIndex = 19;
            this.ultraLabel11.Text = "Fax";
            // 
            // txtFax
            // 
            this.txtFax.AutoSize = false;
            this.txtFax.Location = new System.Drawing.Point(393, 104);
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(209, 22);
            this.txtFax.TabIndex = 18;
            // 
            // ultraLabel10
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance4;
            this.ultraLabel10.Location = new System.Drawing.Point(22, 107);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(87, 20);
            this.ultraLabel10.TabIndex = 17;
            this.ultraLabel10.Text = "Điện thoại ";
            // 
            // txtPhoneNumber
            // 
            this.txtPhoneNumber.AutoSize = false;
            this.txtPhoneNumber.Location = new System.Drawing.Point(140, 105);
            this.txtPhoneNumber.Name = "txtPhoneNumber";
            this.txtPhoneNumber.Size = new System.Drawing.Size(174, 22);
            this.txtPhoneNumber.TabIndex = 16;
            // 
            // ultraLabel9
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance5;
            this.ultraLabel9.Location = new System.Drawing.Point(22, 80);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(87, 20);
            this.ultraLabel9.TabIndex = 15;
            this.ultraLabel9.Text = "Mã số thuế (*)";
            // 
            // txtCompanyTaxcode
            // 
            this.txtCompanyTaxcode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCompanyTaxcode.AutoSize = false;
            this.txtCompanyTaxcode.Location = new System.Drawing.Point(140, 78);
            this.txtCompanyTaxcode.Name = "txtCompanyTaxcode";
            this.txtCompanyTaxcode.Size = new System.Drawing.Size(462, 22);
            this.txtCompanyTaxcode.TabIndex = 14;
            // 
            // ultraLabel8
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance6;
            this.ultraLabel8.Location = new System.Drawing.Point(22, 53);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(87, 20);
            this.ultraLabel8.TabIndex = 13;
            this.ultraLabel8.Text = "Địa chỉ (*)";
            // 
            // txtCompanyAdress
            // 
            this.txtCompanyAdress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCompanyAdress.AutoSize = false;
            this.txtCompanyAdress.Location = new System.Drawing.Point(140, 51);
            this.txtCompanyAdress.Name = "txtCompanyAdress";
            this.txtCompanyAdress.Size = new System.Drawing.Size(462, 22);
            this.txtCompanyAdress.TabIndex = 12;
            // 
            // ultraLabel7
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance7;
            this.ultraLabel7.Location = new System.Drawing.Point(22, 26);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(87, 20);
            this.ultraLabel7.TabIndex = 11;
            this.ultraLabel7.Text = "Tên công ty (*)";
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCompanyName.AutoSize = false;
            this.txtCompanyName.Location = new System.Drawing.Point(140, 24);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(462, 22);
            this.txtCompanyName.TabIndex = 10;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox3.Controls.Add(this.btnBrowserFilder);
            this.ultraGroupBox3.Controls.Add(this.txtDataPatch);
            this.ultraGroupBox3.Controls.Add(this.txtDatabase);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox3.Location = new System.Drawing.Point(12, 146);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(620, 106);
            this.ultraGroupBox3.TabIndex = 6;
            this.ultraGroupBox3.Text = "Nơi lưu dữ liệu kế toán";
            // 
            // btnBrowserFilder
            // 
            appearance8.Image = global::Accounting.Properties.Resources.folder;
            appearance8.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance8.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnBrowserFilder.Appearance = appearance8;
            this.btnBrowserFilder.ImageSize = new System.Drawing.Size(12, 12);
            this.btnBrowserFilder.Location = new System.Drawing.Point(572, 59);
            this.btnBrowserFilder.Margin = new System.Windows.Forms.Padding(0);
            this.btnBrowserFilder.Name = "btnBrowserFilder";
            this.btnBrowserFilder.Size = new System.Drawing.Size(33, 22);
            this.btnBrowserFilder.TabIndex = 4;
            this.btnBrowserFilder.Click += new System.EventHandler(this.btnBrowserFilder_Click);
            // 
            // txtDataPatch
            // 
            this.txtDataPatch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDataPatch.AutoSize = false;
            this.txtDataPatch.Location = new System.Drawing.Point(140, 59);
            this.txtDataPatch.Name = "txtDataPatch";
            this.txtDataPatch.Size = new System.Drawing.Size(426, 22);
            this.txtDataPatch.TabIndex = 3;
            // 
            // txtDatabase
            // 
            this.txtDatabase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDatabase.AutoSize = false;
            this.txtDatabase.Location = new System.Drawing.Point(140, 28);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(465, 22);
            this.txtDatabase.TabIndex = 2;
            this.txtDatabase.Text = "ACCOUNTING";
            // 
            // ultraLabel4
            // 
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance9;
            this.ultraLabel4.Location = new System.Drawing.Point(22, 59);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(122, 23);
            this.ultraLabel4.TabIndex = 1;
            this.ultraLabel4.Text = "Đường dẫn lưu trữ (*)";
            // 
            // ultraLabel3
            // 
            appearance10.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance10;
            this.ultraLabel3.Location = new System.Drawing.Point(22, 29);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(122, 23);
            this.ultraLabel3.TabIndex = 0;
            this.ultraLabel3.Text = "Tên dữ liệu (*)";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox1.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox1.Controls.Add(this.txtPwd);
            this.ultraGroupBox1.Controls.Add(this.txtUsr);
            this.ultraGroupBox1.Controls.Add(this.cbbServers);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Location = new System.Drawing.Point(12, 14);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(620, 124);
            this.ultraGroupBox1.TabIndex = 4;
            this.ultraGroupBox1.Text = "Máy chủ chứa cơ sở dữ liệu";
            // 
            // ultraLabel5
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance11;
            this.ultraLabel5.Location = new System.Drawing.Point(22, 81);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(108, 20);
            this.ultraLabel5.TabIndex = 9;
            this.ultraLabel5.Text = "&Mật khẩu";
            // 
            // ultraLabel6
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance12;
            this.ultraLabel6.Location = new System.Drawing.Point(22, 53);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(108, 20);
            this.ultraLabel6.TabIndex = 8;
            this.ultraLabel6.Text = "&Tài khoản quản trị";
            // 
            // txtPwd
            // 
            this.txtPwd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPwd.AutoSize = false;
            this.txtPwd.Location = new System.Drawing.Point(140, 80);
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.PasswordChar = '*';
            this.txtPwd.Size = new System.Drawing.Size(465, 22);
            this.txtPwd.TabIndex = 7;
            // 
            // txtUsr
            // 
            this.txtUsr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUsr.AutoSize = false;
            this.txtUsr.Location = new System.Drawing.Point(140, 52);
            this.txtUsr.Name = "txtUsr";
            this.txtUsr.Size = new System.Drawing.Size(465, 22);
            this.txtUsr.TabIndex = 6;
            this.txtUsr.Text = "ADMIN";
            // 
            // cbbServers
            // 
            this.cbbServers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbServers.AutoSize = false;
            this.cbbServers.Location = new System.Drawing.Point(140, 24);
            this.cbbServers.Name = "cbbServers";
            this.cbbServers.Size = new System.Drawing.Size(465, 22);
            this.cbbServers.TabIndex = 1;
            this.cbbServers.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cbbServers_BeforeDropDown);
            // 
            // ultraLabel2
            // 
            appearance13.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance13;
            this.ultraLabel2.Location = new System.Drawing.Point(22, 24);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(100, 23);
            this.ultraLabel2.TabIndex = 0;
            this.ultraLabel2.Text = "Máy chủ SQL";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.ultraGroupBox4);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(645, 471);
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.ultraGroupBox2);
            this.ultraGroupBox4.Controls.Add(this.ultraGroupBox7);
            this.ultraGroupBox4.Controls.Add(this.ultraGroupBox6);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance46.FontData.BoldAsString = "True";
            appearance46.FontData.SizeInPoints = 12F;
            this.ultraGroupBox4.HeaderAppearance = appearance46;
            this.ultraGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(645, 471);
            this.ultraGroupBox4.TabIndex = 0;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox2.Controls.Add(this.dteDBStartDate);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel23);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel22);
            this.ultraGroupBox2.Controls.Add(this.txtYear);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel21);
            this.ultraGroupBox2.Controls.Add(this.dteDBStartYear);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel16);
            this.ultraGroupBox2.Location = new System.Drawing.Point(12, 257);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(621, 178);
            this.ultraGroupBox2.TabIndex = 355;
            this.ultraGroupBox2.Text = "Thiết lập kỳ kế toán";
            // 
            // dteDBStartDate
            // 
            this.dteDBStartDate.AutoSize = false;
            this.dteDBStartDate.Location = new System.Drawing.Point(253, 119);
            this.dteDBStartDate.Name = "dteDBStartDate";
            this.dteDBStartDate.Size = new System.Drawing.Size(127, 22);
            this.dteDBStartDate.TabIndex = 368;
            // 
            // ultraLabel23
            // 
            appearance14.TextVAlignAsString = "Middle";
            this.ultraLabel23.Appearance = appearance14;
            this.ultraLabel23.Location = new System.Drawing.Point(39, 120);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(208, 22);
            this.ultraLabel23.TabIndex = 367;
            this.ultraLabel23.Text = "Ngày bắt đầu hạch toán trên phần mềm";
            // 
            // ultraLabel22
            // 
            this.ultraLabel22.Location = new System.Drawing.Point(39, 57);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(563, 66);
            this.ultraLabel22.TabIndex = 366;
            this.ultraLabel22.Text = resources.GetString("ultraLabel22.Text");
            // 
            // txtYear
            // 
            this.txtYear.AutoSize = false;
            this.txtYear.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtYear.Location = new System.Drawing.Point(443, 28);
            this.txtYear.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtYear.MaskInput = "nnnn";
            this.txtYear.MinValue = 1980;
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(86, 22);
            this.txtYear.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.OnMouseEnter;
            this.txtYear.TabIndex = 365;
            this.txtYear.Tag = "";
            this.txtYear.Value = 2012;
            // 
            // ultraLabel21
            // 
            appearance15.TextVAlignAsString = "Middle";
            this.ultraLabel21.Appearance = appearance15;
            this.ultraLabel21.Location = new System.Drawing.Point(351, 30);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(81, 22);
            this.ultraLabel21.TabIndex = 364;
            this.ultraLabel21.Text = "Năm tài chính";
            // 
            // dteDBStartYear
            // 
            this.dteDBStartYear.AutoSize = false;
            this.dteDBStartYear.Location = new System.Drawing.Point(195, 29);
            this.dteDBStartYear.Name = "dteDBStartYear";
            this.dteDBStartYear.Size = new System.Drawing.Size(113, 22);
            this.dteDBStartYear.TabIndex = 363;
            // 
            // ultraLabel16
            // 
            appearance16.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance16;
            this.ultraLabel16.Location = new System.Drawing.Point(36, 29);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(150, 22);
            this.ultraLabel16.TabIndex = 362;
            this.ultraLabel16.Text = "Ngày bắt đầu năm tài chính";
            // 
            // ultraGroupBox7
            // 
            this.ultraGroupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox7.Controls.Add(this.cbbPPTGXK);
            this.ultraGroupBox7.Controls.Add(this.ultraLabel20);
            this.ultraGroupBox7.Location = new System.Drawing.Point(12, 181);
            this.ultraGroupBox7.Name = "ultraGroupBox7";
            this.ultraGroupBox7.Size = new System.Drawing.Size(621, 53);
            this.ultraGroupBox7.TabIndex = 2;
            this.ultraGroupBox7.Text = "Phương pháp tính giá xuất kho";
            // 
            // cbbPPTGXK
            // 
            this.cbbPPTGXK.AutoSize = false;
            this.cbbPPTGXK.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem3.DataValue = "";
            valueListItem3.DisplayText = " ";
            valueListItem4.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem4.DataValue = "Bình quân cuối kỳ";
            valueListItem4.DisplayText = "Bình quân cuối kỳ";
            valueListItem5.DataValue = "Bình quân tức thời";
            valueListItem5.DisplayText = "Bình quân tức thời";
            valueListItem6.DataValue = "Nhập trước xuất trước";
            valueListItem6.DisplayText = "Nhập trước xuất trước";
            valueListItem7.DataValue = "Đích danh";
            valueListItem7.DisplayText = "Đích danh";
            this.cbbPPTGXK.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem4,
            valueListItem5,
            valueListItem6,
            valueListItem7});
            this.cbbPPTGXK.Location = new System.Drawing.Point(195, 21);
            this.cbbPPTGXK.Name = "cbbPPTGXK";
            this.cbbPPTGXK.Size = new System.Drawing.Size(407, 22);
            this.cbbPPTGXK.TabIndex = 37;
            // 
            // ultraLabel20
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance17;
            this.ultraLabel20.Location = new System.Drawing.Point(36, 23);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(120, 20);
            this.ultraLabel20.TabIndex = 11;
            this.ultraLabel20.Text = "Phương pháp tính";
            // 
            // ultraGroupBox6
            // 
            this.ultraGroupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox6.Controls.Add(this.cbbNhomNNMD);
            this.ultraGroupBox6.Controls.Add(this.cbbNhomHHDV);
            this.ultraGroupBox6.Controls.Add(this.ultraLabel14);
            this.ultraGroupBox6.Controls.Add(this.ultraLabel15);
            this.ultraGroupBox6.Controls.Add(this.ultraOptionSet1);
            this.ultraGroupBox6.Location = new System.Drawing.Point(12, 4);
            this.ultraGroupBox6.Name = "ultraGroupBox6";
            this.ultraGroupBox6.Size = new System.Drawing.Size(621, 152);
            this.ultraGroupBox6.TabIndex = 1;
            this.ultraGroupBox6.Text = "Phương pháp tính thuế GTGT";
            // 
            // cbbNhomNNMD
            // 
            appearance18.FontData.BoldAsString = "False";
            this.cbbNhomNNMD.Appearance = appearance18;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbNhomNNMD.DisplayLayout.Appearance = appearance19;
            this.cbbNhomNNMD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbNhomNNMD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance20.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance20.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance20.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbNhomNNMD.DisplayLayout.GroupByBox.Appearance = appearance20;
            appearance21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbNhomNNMD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance21;
            this.cbbNhomNNMD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance22.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance22.BackColor2 = System.Drawing.SystemColors.Control;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbNhomNNMD.DisplayLayout.GroupByBox.PromptAppearance = appearance22;
            this.cbbNhomNNMD.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbNhomNNMD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbNhomNNMD.DisplayLayout.Override.ActiveCellAppearance = appearance23;
            appearance24.BackColor = System.Drawing.SystemColors.Highlight;
            appearance24.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbNhomNNMD.DisplayLayout.Override.ActiveRowAppearance = appearance24;
            this.cbbNhomNNMD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbNhomNNMD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            this.cbbNhomNNMD.DisplayLayout.Override.CardAreaAppearance = appearance25;
            appearance26.BorderColor = System.Drawing.Color.Silver;
            appearance26.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbNhomNNMD.DisplayLayout.Override.CellAppearance = appearance26;
            this.cbbNhomNNMD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbNhomNNMD.DisplayLayout.Override.CellPadding = 0;
            appearance27.BackColor = System.Drawing.SystemColors.Control;
            appearance27.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance27.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbNhomNNMD.DisplayLayout.Override.GroupByRowAppearance = appearance27;
            appearance28.TextHAlignAsString = "Left";
            this.cbbNhomNNMD.DisplayLayout.Override.HeaderAppearance = appearance28;
            this.cbbNhomNNMD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbNhomNNMD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            this.cbbNhomNNMD.DisplayLayout.Override.RowAppearance = appearance29;
            this.cbbNhomNNMD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance30.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbNhomNNMD.DisplayLayout.Override.TemplateAddRowAppearance = appearance30;
            this.cbbNhomNNMD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbNhomNNMD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbNhomNNMD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbNhomNNMD.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbNhomNNMD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbNhomNNMD.Location = new System.Drawing.Point(195, 120);
            this.cbbNhomNNMD.Name = "cbbNhomNNMD";
            this.cbbNhomNNMD.Size = new System.Drawing.Size(407, 24);
            this.cbbNhomNNMD.TabIndex = 24;
            // 
            // cbbNhomHHDV
            // 
            appearance31.FontData.BoldAsString = "False";
            this.cbbNhomHHDV.Appearance = appearance31;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            appearance32.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbNhomHHDV.DisplayLayout.Appearance = appearance32;
            this.cbbNhomHHDV.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbNhomHHDV.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance33.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbNhomHHDV.DisplayLayout.GroupByBox.Appearance = appearance33;
            appearance34.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbNhomHHDV.DisplayLayout.GroupByBox.BandLabelAppearance = appearance34;
            this.cbbNhomHHDV.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance35.BackColor2 = System.Drawing.SystemColors.Control;
            appearance35.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance35.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbNhomHHDV.DisplayLayout.GroupByBox.PromptAppearance = appearance35;
            this.cbbNhomHHDV.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbNhomHHDV.DisplayLayout.MaxRowScrollRegions = 1;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbNhomHHDV.DisplayLayout.Override.ActiveCellAppearance = appearance36;
            appearance37.BackColor = System.Drawing.SystemColors.Highlight;
            appearance37.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbNhomHHDV.DisplayLayout.Override.ActiveRowAppearance = appearance37;
            this.cbbNhomHHDV.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbNhomHHDV.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            this.cbbNhomHHDV.DisplayLayout.Override.CardAreaAppearance = appearance38;
            appearance39.BorderColor = System.Drawing.Color.Silver;
            appearance39.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbNhomHHDV.DisplayLayout.Override.CellAppearance = appearance39;
            this.cbbNhomHHDV.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbNhomHHDV.DisplayLayout.Override.CellPadding = 0;
            appearance40.BackColor = System.Drawing.SystemColors.Control;
            appearance40.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance40.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbNhomHHDV.DisplayLayout.Override.GroupByRowAppearance = appearance40;
            appearance41.TextHAlignAsString = "Left";
            this.cbbNhomHHDV.DisplayLayout.Override.HeaderAppearance = appearance41;
            this.cbbNhomHHDV.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbNhomHHDV.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            appearance42.BorderColor = System.Drawing.Color.Silver;
            this.cbbNhomHHDV.DisplayLayout.Override.RowAppearance = appearance42;
            this.cbbNhomHHDV.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance43.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbNhomHHDV.DisplayLayout.Override.TemplateAddRowAppearance = appearance43;
            this.cbbNhomHHDV.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbNhomHHDV.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbNhomHHDV.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbNhomHHDV.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbNhomHHDV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbNhomHHDV.Location = new System.Drawing.Point(195, 59);
            this.cbbNhomHHDV.Name = "cbbNhomHHDV";
            this.cbbNhomHHDV.Size = new System.Drawing.Size(407, 24);
            this.cbbNhomHHDV.TabIndex = 23;
            // 
            // ultraLabel14
            // 
            appearance44.BackColor = System.Drawing.Color.Transparent;
            appearance44.TextVAlignAsString = "Middle";
            this.ultraLabel14.Appearance = appearance44;
            this.ultraLabel14.Location = new System.Drawing.Point(35, 121);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(177, 23);
            this.ultraLabel14.TabIndex = 22;
            this.ultraLabel14.Text = "Nhóm ngành nghề mặc định";
            // 
            // ultraLabel15
            // 
            appearance45.BackColor = System.Drawing.Color.Transparent;
            appearance45.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance45;
            this.ultraLabel15.Location = new System.Drawing.Point(36, 63);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(176, 20);
            this.ultraLabel15.TabIndex = 21;
            this.ultraLabel15.Text = "Nhóm HHDV mua vào chính";
            // 
            // ultraOptionSet1
            // 
            this.ultraOptionSet1.BackColor = System.Drawing.Color.Transparent;
            this.ultraOptionSet1.BackColorInternal = System.Drawing.Color.Transparent;
            this.ultraOptionSet1.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem1.DataValue = "Default Item";
            valueListItem1.DisplayText = "Phương pháp khấu trừ";
            valueListItem2.DataValue = "ValueListItem1";
            valueListItem2.DisplayText = "Phương pháp trực tiếp trên doanh thu";
            this.ultraOptionSet1.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.ultraOptionSet1.ItemSpacingVertical = 40;
            this.ultraOptionSet1.Location = new System.Drawing.Point(9, 19);
            this.ultraOptionSet1.Name = "ultraOptionSet1";
            this.ultraOptionSet1.Size = new System.Drawing.Size(226, 114);
            this.ultraOptionSet1.TabIndex = 20;
            this.ultraOptionSet1.ValueChanged += new System.EventHandler(this.ultraOptionSet1_ValueChanged);
            // 
            // fbrDialog
            // 
            this.fbrDialog.Description = "Chọn thư mục lưu trữ dữ liệu";
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.tabControl);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraPanel4);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 43);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(831, 471);
            this.ultraPanel2.TabIndex = 1;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.ultraTabSharedControlsPage1);
            this.tabControl.Controls.Add(this.ultraTabPageControl1);
            this.tabControl.Controls.Add(this.ultraTabPageControl2);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(186, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.tabControl.Size = new System.Drawing.Size(645, 471);
            this.tabControl.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Wizard;
            this.tabControl.TabIndex = 4;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "";
            this.tabControl.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            this.tabControl.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.tabControl_SelectedTabChanged);
            this.tabControl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tabControl_KeyDown);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(645, 471);
            // 
            // ultraPanel4
            // 
            appearance47.ImageBackground = global::Accounting.Properties.Resources.banner01;
            appearance47.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Tiled;
            this.ultraPanel4.Appearance = appearance47;
            // 
            // ultraPanel4.ClientArea
            // 
            this.ultraPanel4.ClientArea.Controls.Add(this.pnlSelectServerOldDB);
            this.ultraPanel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.ultraPanel4.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel4.Name = "ultraPanel4";
            this.ultraPanel4.Size = new System.Drawing.Size(186, 471);
            this.ultraPanel4.TabIndex = 0;
            // 
            // pnlSelectServerOldDB
            // 
            this.pnlSelectServerOldDB.BorderStyle = Infragistics.Win.UIElementBorderStyle.RaisedSoft;
            // 
            // pnlSelectServerOldDB.ClientArea
            // 
            this.pnlSelectServerOldDB.ClientArea.Controls.Add(this.btnApplyServerOldDB);
            this.pnlSelectServerOldDB.Location = new System.Drawing.Point(186, 250);
            this.pnlSelectServerOldDB.Name = "pnlSelectServerOldDB";
            this.pnlSelectServerOldDB.Size = new System.Drawing.Size(342, 180);
            this.pnlSelectServerOldDB.TabIndex = 6;
            this.pnlSelectServerOldDB.Visible = false;
            // 
            // btnApplyServerOldDB
            // 
            appearance48.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnApplyServerOldDB.Appearance = appearance48;
            this.btnApplyServerOldDB.Location = new System.Drawing.Point(130, 140);
            this.btnApplyServerOldDB.Name = "btnApplyServerOldDB";
            this.btnApplyServerOldDB.Size = new System.Drawing.Size(80, 30);
            this.btnApplyServerOldDB.TabIndex = 22;
            this.btnApplyServerOldDB.Text = "Đồng ý";
            this.btnApplyServerOldDB.Click += new System.EventHandler(this.btnApplyServerOldDB_Click);
            // 
            // ultraGroupBox9
            // 
            this.ultraGroupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance49.FontData.BoldAsString = "True";
            appearance49.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(33)))), ((int)(((byte)(122)))));
            this.ultraGroupBox9.Appearance = appearance49;
            this.ultraGroupBox9.Controls.Add(this.cbbServerOldDB);
            this.ultraGroupBox9.Controls.Add(this.ultraLabel27);
            this.ultraGroupBox9.Controls.Add(this.ultraLabel28);
            this.ultraGroupBox9.Controls.Add(this.ultraLabel29);
            this.ultraGroupBox9.Controls.Add(this.txtPwdServerOldDB);
            this.ultraGroupBox9.Controls.Add(this.txtUsrServerOldDB);
            this.ultraGroupBox9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGroupBox9.Location = new System.Drawing.Point(70, 43);
            this.ultraGroupBox9.Name = "ultraGroupBox9";
            this.ultraGroupBox9.Size = new System.Drawing.Size(322, 124);
            this.ultraGroupBox9.TabIndex = 21;
            this.ultraGroupBox9.Text = "Thông tin máy chủ kế toán năm trước";
            // 
            // cbbServerOldDB
            // 
            this.cbbServerOldDB.AutoSize = false;
            this.cbbServerOldDB.Location = new System.Drawing.Point(123, 27);
            this.cbbServerOldDB.Name = "cbbServerOldDB";
            this.cbbServerOldDB.Size = new System.Drawing.Size(186, 22);
            this.cbbServerOldDB.TabIndex = 25;
            this.cbbServerOldDB.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cbbServers_BeforeDropDown);
            // 
            // ultraLabel27
            // 
            appearance50.BackColor = System.Drawing.Color.Transparent;
            appearance50.TextVAlignAsString = "Middle";
            this.ultraLabel27.Appearance = appearance50;
            this.ultraLabel27.Location = new System.Drawing.Point(9, 27);
            this.ultraLabel27.Name = "ultraLabel27";
            this.ultraLabel27.Size = new System.Drawing.Size(108, 20);
            this.ultraLabel27.TabIndex = 26;
            this.ultraLabel27.Text = "Máy chủ &SQL";
            this.ultraLabel27.Visible = false;
            // 
            // ultraLabel28
            // 
            appearance51.BackColor = System.Drawing.Color.Transparent;
            appearance51.TextVAlignAsString = "Middle";
            this.ultraLabel28.Appearance = appearance51;
            this.ultraLabel28.Location = new System.Drawing.Point(9, 83);
            this.ultraLabel28.Name = "ultraLabel28";
            this.ultraLabel28.Size = new System.Drawing.Size(108, 20);
            this.ultraLabel28.TabIndex = 24;
            this.ultraLabel28.Text = "&Mật khẩu quản trị";
            // 
            // ultraLabel29
            // 
            appearance52.BackColor = System.Drawing.Color.Transparent;
            appearance52.TextVAlignAsString = "Middle";
            this.ultraLabel29.Appearance = appearance52;
            this.ultraLabel29.Location = new System.Drawing.Point(9, 55);
            this.ultraLabel29.Name = "ultraLabel29";
            this.ultraLabel29.Size = new System.Drawing.Size(108, 20);
            this.ultraLabel29.TabIndex = 23;
            this.ultraLabel29.Text = "&Tài khoản quản trị";
            // 
            // txtPwdServerOldDB
            // 
            this.txtPwdServerOldDB.AutoSize = false;
            this.txtPwdServerOldDB.Location = new System.Drawing.Point(123, 82);
            this.txtPwdServerOldDB.Name = "txtPwdServerOldDB";
            this.txtPwdServerOldDB.PasswordChar = '*';
            this.txtPwdServerOldDB.Size = new System.Drawing.Size(186, 22);
            this.txtPwdServerOldDB.TabIndex = 22;
            // 
            // txtUsrServerOldDB
            // 
            this.txtUsrServerOldDB.AutoSize = false;
            this.txtUsrServerOldDB.Location = new System.Drawing.Point(123, 55);
            this.txtUsrServerOldDB.Name = "txtUsrServerOldDB";
            this.txtUsrServerOldDB.Size = new System.Drawing.Size(186, 22);
            this.txtUsrServerOldDB.TabIndex = 21;
            this.txtUsrServerOldDB.Text = "ADMIN";
            // 
            // ultraPanel3
            // 
            // 
            // ultraPanel3.ClientArea
            // 
            this.ultraPanel3.ClientArea.Controls.Add(this.btnBack);
            this.ultraPanel3.ClientArea.Controls.Add(this.btnNext);
            this.ultraPanel3.ClientArea.Controls.Add(this.btnOk);
            this.ultraPanel3.ClientArea.Controls.Add(this.BtnCancel);
            this.ultraPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel3.Location = new System.Drawing.Point(0, 514);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(831, 40);
            this.ultraPanel3.TabIndex = 2;
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance53.Image = global::Accounting.Properties.Resources.ubtnBack;
            this.btnBack.Appearance = appearance53;
            this.btnBack.Location = new System.Drawing.Point(457, 7);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(86, 26);
            this.btnBack.TabIndex = 3;
            this.btnBack.Text = "Quay lại";
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance54.Image = global::Accounting.Properties.Resources.ubtnForward;
            appearance54.ImageHAlign = Infragistics.Win.HAlign.Right;
            appearance54.TextHAlignAsString = "Center";
            this.btnNext.Appearance = appearance54;
            this.btnNext.Location = new System.Drawing.Point(549, 7);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(86, 26);
            this.btnNext.TabIndex = 2;
            this.btnNext.Text = "Tiếp theo";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance55.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnOk.Appearance = appearance55;
            this.btnOk.Location = new System.Drawing.Point(641, 7);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(86, 26);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "Thực hiện";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance56.Image = global::Accounting.Properties.Resources.cancel_16;
            this.BtnCancel.Appearance = appearance56;
            this.BtnCancel.Location = new System.Drawing.Point(733, 7);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(86, 26);
            this.BtnCancel.TabIndex = 0;
            this.BtnCancel.Text = "Hủy bỏ";
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox9);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel1);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(831, 43);
            this.ultraPanel1.TabIndex = 0;
            // 
            // ultraLabel1
            // 
            appearance57.FontData.BoldAsString = "True";
            appearance57.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(111)))), ((int)(((byte)(33)))));
            appearance57.Image = global::Accounting.Properties.Resources.new_database1;
            appearance57.TextHAlignAsString = "Left";
            appearance57.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance57;
            this.ultraLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel1.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel1.ImageSize = new System.Drawing.Size(32, 32);
            this.ultraLabel1.Location = new System.Drawing.Point(0, 0);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(831, 43);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Tạo mới dữ liệu kế toán";
            // 
            // popContainer
            // 
            this.popContainer.PopupControl = this.pnlSelectServerOldDB;
            this.popContainer.Opening += new System.ComponentModel.CancelEventHandler(this.popContainer_Opening);
            // 
            // ultraGroupBox8
            // 
            this.ultraGroupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox8.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox8.Name = "ultraGroupBox8";
            this.ultraGroupBox8.Size = new System.Drawing.Size(645, 471);
            this.ultraGroupBox8.TabIndex = 1;
            // 
            // FCreateDatabase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 554);
            this.Controls.Add(this.ultraPanel2);
            this.Controls.Add(this.ultraPanel3);
            this.Controls.Add(this.ultraPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FCreateDatabase";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tạo dữ liệu kế toán";
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.ultraGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailForgotPass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhoneNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyAdress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDataPatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatabase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPwd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbServers)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dteDBStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDBStartYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).EndInit();
            this.ultraGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbPPTGXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).EndInit();
            this.ultraGroupBox6.ResumeLayout(false);
            this.ultraGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNhomNNMD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNhomHHDV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet1)).EndInit();
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.ultraPanel4.ClientArea.ResumeLayout(false);
            this.ultraPanel4.ResumeLayout(false);
            this.pnlSelectServerOldDB.ClientArea.ResumeLayout(false);
            this.pnlSelectServerOldDB.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox9)).EndInit();
            this.ultraGroupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbServerOldDB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPwdServerOldDB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsrServerOldDB)).EndInit();
            this.ultraPanel3.ClientArea.ResumeLayout(false);
            this.ultraPanel3.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox8)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
        private Infragistics.Win.Misc.UltraPanel ultraPanel4;
        private Infragistics.Win.Misc.UltraButton btnBack;
        private Infragistics.Win.Misc.UltraButton btnNext;
        private Infragistics.Win.Misc.UltraButton btnOk;
        private Infragistics.Win.Misc.UltraButton BtnCancel;
        private System.Windows.Forms.FolderBrowserDialog fbrDialog;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl tabControl;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraButton btnBrowserFilder;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDataPatch;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDatabase;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPwd;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtUsr;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbServers;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox6;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.Misc.UltraPopupControlContainer popContainer;
        private Infragistics.Win.Misc.UltraPanel pnlSelectServerOldDB;
        private Infragistics.Win.Misc.UltraButton btnApplyServerOldDB;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox9;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbServerOldDB;
        private Infragistics.Win.Misc.UltraLabel ultraLabel27;
        private Infragistics.Win.Misc.UltraLabel ultraLabel28;
        private Infragistics.Win.Misc.UltraLabel ultraLabel29;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPwdServerOldDB;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtUsrServerOldDB;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet ultraOptionSet1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbPPTGXK;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbNhomNNMD;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbNhomHHDV;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtEmail;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFax;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPhoneNumber;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxcode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyAdress;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyName;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDBStartDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtYear;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDBStartYear;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtEmailForgotPass;
    }
}