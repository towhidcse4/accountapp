﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.Core.Domain;
using FX.Core;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinTree;
using Accounting.Core.DAO;

namespace Accounting.Frm.FMain
{

    public partial class UCAccountBalance : UserControl
    {
        public UCAccountBalance(bool isGet = true)
        {
            if (!isGet) return;
            InitializeComponent();
            this.uTree.InitializeDataNode += new Infragistics.Win.UltraWinTree.InitializeDataNodeEventHandler(this.uTree_InitializeDataNode);
            this.uTree.AfterDataNodesCollectionPopulated += new Infragistics.Win.UltraWinTree.AfterDataNodesCollectionPopulatedEventHandler(Utils.uTree_AfterDataNodesCollectionPopulated);
            this.uTree.ColumnSetGenerated += new Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventHandler(this.uTree_ColumnSetGenerated);
            LoadDuLieu();
        }
        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configTree)
        {
            #region Lấy dữ liệu từ CSDL
            ReportProcedureSDS sp = new ReportProcedureSDS();
            List<BalanceAccountF01> data = new List<BalanceAccountF01>();
            data = sp.GetBalanceAccountF01(new DateTime(1753, 1, 1), DateTime.Now, 3, true);
            List<AccountBalance> lst = new List<AccountBalance>();
            foreach (var x in Utils.ListAccount.CloneObject())
            {
                AccountBalance model = new AccountBalance();
                model.ID = x.ID;
                model.AccountNumber = x.AccountNumber;
                model.AccountName = x.AccountName;
                model.ParentID = x.ParentID;
                model.IsParentNode = x.IsParentNode;
                model.Grade = x.Grade;
                model.AccountGroupID = x.AccountGroupID;
                model.IsActive = x.IsActive;
                model.DetailType = x.DetailType;
                model.AccountNameGlobal = x.AccountNameGlobal;
                model.Description = x.Description;
                decimal? co = null;
                decimal? no = null;
                model.ClosingCreditAmount = data.FirstOrDefault(c => c.AccountNumber == x.AccountNumber) != null ? (data.FirstOrDefault(c => c.AccountNumber == x.AccountNumber).ClosingCreditAmount != 0 ? data.FirstOrDefault(c => c.AccountNumber == x.AccountNumber).ClosingCreditAmount : co) : co;
                model.ClosingDebitAmount = data.FirstOrDefault(c => c.AccountNumber == x.AccountNumber) != null ? (data.FirstOrDefault(c => c.AccountNumber == x.AccountNumber).ClosingDebitAmount != 0 ? data.FirstOrDefault(c => c.AccountNumber == x.AccountNumber).ClosingDebitAmount : no) : no;
                if(model.ClosingCreditAmount != null || model.ClosingDebitAmount != null) lst.Add(model);
            }
            lst = lst.OrderBy(c => c.AccountNumber).ToList();
            #endregion

            #region hiển thị và xử lý hiển thị
            DataSet ds = Utils.ToDataSet<AccountBalance>(lst, ConstDatabase.AccountBalance_TableName);
            uTree.SetDataBinding(ds, ConstDatabase.AccountBalance_TableName);
            if (configTree) ConfigTree(uTree);
            #endregion
        }
        private void uTree_ColumnSetGenerated(object sender, ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, ConstDatabase.AccountBalance_TableName);
        }

        private void uTree_InitializeDataNode(object sender, InitializeDataNodeEventArgs e)
        {
            Utils.uTree_InitializeDataNode(sender, e, ConstDatabase.AccountBalance_TableName);
        }
        void ConfigTree(Infragistics.Win.UltraWinTree.UltraTree ultraTree)
        {//hàm chung
            Utils.ConfigTree(ultraTree, TextMessage.ConstDatabase.AccountBalance_TableName);
        }
    }
}
