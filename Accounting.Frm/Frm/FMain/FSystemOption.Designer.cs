﻿namespace Accounting
{
    partial class FSystemOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem13 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FSystemOption));
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem16 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem17 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem18 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem19 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem14 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem15 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance137 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance116 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance123 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance127 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance129 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance130 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance131 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance132 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance133 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance134 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance135 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance136 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance138 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance141 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance139 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance140 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance142 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance145 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance143 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem9 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem12 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance144 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance146 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance152 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance147 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance148 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance149 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance150 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance151 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance153 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance184 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance154 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance155 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance156 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance157 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance158 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance159 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance160 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance161 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance162 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance163 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance164 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance165 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance166 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance167 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance168 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance169 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance170 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance171 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance172 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance173 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance174 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance175 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance176 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance177 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance178 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance179 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance180 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance181 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance182 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance183 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance185 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance220 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance186 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance187 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance188 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance189 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance190 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance191 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance192 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance193 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance194 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance195 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance196 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance197 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance198 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance199 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance200 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance201 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance202 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance203 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance204 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance205 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance206 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance207 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance208 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance209 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance210 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance211 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance212 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance213 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance214 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance215 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance216 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance217 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance218 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance219 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance221 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance227 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance222 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance223 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem20 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem10 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem22 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance224 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance225 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance226 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance228 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance271 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance229 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance230 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance231 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem8 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance232 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance233 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance234 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance235 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance236 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance237 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance238 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance239 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance240 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance241 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance242 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance243 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem11 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem21 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance244 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance245 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance246 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance247 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance248 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance249 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance250 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance251 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance252 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance253 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance254 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance255 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance256 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance257 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance258 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance259 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance260 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance261 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance262 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance263 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance264 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance265 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance266 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance267 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance268 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance269 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance270 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance272 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance312 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance273 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance274 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance275 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance276 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance277 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance278 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance279 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance280 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance281 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance282 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance283 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance284 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance285 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance286 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance287 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance288 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance289 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance290 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance291 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance292 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance293 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance294 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance295 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance296 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance297 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance298 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance299 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance300 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance301 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance302 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance303 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance304 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance305 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance306 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance307 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance308 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance309 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance310 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance311 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance313 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance360 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance314 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance315 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance316 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance317 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance318 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance319 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance320 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance321 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance322 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance323 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance324 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance325 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance326 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance327 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance328 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance329 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance330 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance331 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance332 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance333 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance334 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance335 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance336 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance337 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance338 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance339 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance340 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance341 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance342 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance343 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance344 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance345 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance346 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance347 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance348 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance349 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance350 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance351 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance352 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance353 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance354 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance355 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance356 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance357 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance358 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance359 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance361 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance378 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance362 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance363 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance364 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance365 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance366 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance367 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance368 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance369 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance370 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance371 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance372 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance373 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance374 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance375 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance376 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance377 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance379 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab8 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance380 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance381 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbcTTDN_SoTKNH = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.txtTTDN_Nganhang = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.chkTTDN_TTDailythue = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanelDaiLyThue = new Infragistics.Win.Misc.UltraPanel();
            this.dateTTDN_NgayDLT = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtTTDN_DiachiDLT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTTDN_TenDLT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTTDN_NVDLT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTTDN_DienthoaiDLT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTTDN_FaxDLT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTTDN_MSTDLT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTTDN_HDDLso = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTTDN_CCHNDLT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTTDN_QuanDLT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTTDN_TinhDLT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.chkTTDN_TTDvichuquan = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanelDonViChuQuan = new Infragistics.Win.Misc.UltraPanel();
            this.txtTTDN_Tendvichuquan = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTTDN_MSTdvichuquan = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel98 = new Infragistics.Win.Misc.UltraLabel();
            this.txtProductID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTTDN_Tencty = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtTTDN_Diachi = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTTDN_MST = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox6 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbcNKY_vitrikyDT = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.meNKY_ChieurongkyDT = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.meNKY_ChieudaikyDT = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtNKY_NoiluukyDT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ButNK_ChonNoiLuTep = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel30 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel32 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel29 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel31 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.chkNKY_Inten = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.txtNKY_Giamdoc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNKY_Thukho = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtNKY_Nguoilapbieu = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtNKY_Ketoantruong = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtNKY_Thuquy = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox9 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel46 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel44 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel45 = new Infragistics.Win.Misc.UltraLabel();
            this.chkBC_LapTDeBC = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkBC_CongGopBtoan = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkBC_KoInCtu = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.BssssC_HthiFax = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbcBC_VtriTtinDN = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.optHienThi = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.chkBC_HthiEmail = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkBC_HthiFax = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkBC_HthiDThoai = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel42 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel41 = new Infragistics.Win.Misc.UltraLabel();
            this.BC_HthiDThoai = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel43 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel39 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox7 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtBC_FontTencty = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtBC_FontDChi = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtBC_FontTenchuquan = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel34 = new Infragistics.Win.Misc.UltraLabel();
            this.ButChonFontTenCongTy = new Infragistics.Win.Misc.UltraButton();
            this.ButFontDonViChuQuan = new Infragistics.Win.Misc.UltraButton();
            this.ButFontDiaChi = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel36 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel38 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox28 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraOptionSet1 = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.ultraGroupBox14 = new Infragistics.Win.Misc.UltraGroupBox();
            this.chkAllowInputCostUnit = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkVTHH_ChoXuatQuaLuongTon = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkVTHH_CBaoXuatQuaLuongTon = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel53 = new Infragistics.Win.Misc.UltraLabel();
            this.cbcVTHH_KhoMacDinh = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel50 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox13 = new Infragistics.Win.Misc.UltraGroupBox();
            this.opttuychonkhilaphoadon = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.ultraGroupBox12 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBox27 = new Infragistics.Win.Misc.UltraGroupBox();
            this.optcachtinhgia = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.ultraGroupBox11 = new Infragistics.Win.Misc.UltraGroupBox();
            this.optGia = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.ultraGroupBox10 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbcVTHH_PPTinhGiaXKho = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraPictureBox1 = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.ultraLabel48 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel49 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel47 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl5 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox18 = new Infragistics.Win.Misc.UltraGroupBox();
            this.chkTL_LamThemCN = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel67 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox17 = new Infragistics.Win.Misc.UltraGroupBox();
            this.meTL_BHTNLDong = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.meTL_BHYTLDong = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.meTL_BHXHLDong = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.meTL_KPCDCty = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.meTL_BHTNCty = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.meTL_BHYTCty = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.meTL_BHXHCty = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel64 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel62 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel60 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel63 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel108 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel107 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel106 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel105 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel92 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel84 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel40 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel58 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel61 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel57 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox16 = new Infragistics.Win.Misc.UltraGroupBox();
            this.chkTL_LayTUlenBLuong = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel59 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox15 = new Infragistics.Win.Misc.UltraGroupBox();
            this.optLoaiDT = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.ultraLabel54 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl6 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox21 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ButDDS_MacDinh = new Infragistics.Win.Misc.UltraButton();
            this.txtDDSo_NCachHangNghin = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDDSo_NCachHangDVi = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel82 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel85 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox20 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbcDDSo_SoAm = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbcDDSo_KyHieuTien = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel81 = new Infragistics.Win.Misc.UltraLabel();
            this.H = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel83 = new Infragistics.Win.Misc.UltraLabel();
            this.lblHienThiSoAm = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox19 = new Infragistics.Win.Misc.UltraGroupBox();
            this.numDDSo_DonGiaNT = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.lblDonGiaNT = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel99 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel101 = new Infragistics.Win.Misc.UltraLabel();
            this.ButDDS_XemThu = new Infragistics.Win.Misc.UltraButton();
            this.numDDSo_TyLePBo = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.numDDSo_TyLe = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.numDDSo_TyGia = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.numDDSo_SoLuong = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.numDDSo_DonGia = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.numDDSo_NgoaiTe = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.numDDSo_TienVND = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.lblTienVietNam = new Infragistics.Win.Misc.UltraLabel();
            this.lblSoLuong = new Infragistics.Win.Misc.UltraLabel();
            this.lblTienNgoaiTe = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel80 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel65 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel79 = new Infragistics.Win.Misc.UltraLabel();
            this.lblTyGia = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel66 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel78 = new Infragistics.Win.Misc.UltraLabel();
            this.lblDonGia = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel77 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel68 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel69 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel76 = new Infragistics.Win.Misc.UltraLabel();
            this.lblTyLePhanBo = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel70 = new Infragistics.Win.Misc.UltraLabel();
            this.lblHeSoTyLe = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel75 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel73 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel74 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel71 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel72 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl7 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox22 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.numSLUU_NgaySLUU = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.ultraLabel102 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel86 = new Infragistics.Win.Misc.UltraLabel();
            this.opthinhthucsaoluu = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.txtSLUU_TMSaoLuu = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel87 = new Infragistics.Win.Misc.UltraLabel();
            this.ButSL_Chon = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabPageControl8 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox8 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel104 = new Infragistics.Win.Misc.UltraLabel();
            this.txtEmailForgotPass = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel56 = new Infragistics.Win.Misc.UltraLabel();
            this.optPBCCDC = new Accounting.UltraOptionSet_Ex();
            this.ultraLabel55 = new Infragistics.Win.Misc.UltraLabel();
            this.optPB = new Accounting.UltraOptionSet_Ex();
            this.chkTCKHAC_SUDUNGTHEMSOQUANTRI = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkHDDT = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.meTCKHAC_MenhGiaCP = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.chkTCKHAC_DocTienBangChu = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkTCKHAC_CBKoChonNVBH = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkTCKHAC_ChepDL = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkTCKHAC_CPKoHLy = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkTCKHAC_ChiQuaTonQuy = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkTCKHAC_SinhTK007 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.opt = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.ultraLabel94 = new Infragistics.Win.Misc.UltraLabel();
            this.cbcTCKHAC_ChuHThiInHDLan2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel97 = new Infragistics.Win.Misc.UltraLabel();
            this.cbcTCKHAC_DocTienLe = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel95 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox26 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel33 = new Infragistics.Win.Misc.UltraLabel();
            this.cbcTCKHAC_PPTinhGiaXQuy = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbcTCKHAC_TKCLechLai = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbcTCKHAC_TKCLechLo = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel35 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel37 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox25 = new Infragistics.Win.Misc.UltraGroupBox();
            this.chkLimitAccount = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkIsMinimized = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.cbcPPTTGTGT = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel100 = new Infragistics.Win.Misc.UltraLabel();
            this.cbcVTHH_NhomNNMD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel96 = new Infragistics.Win.Misc.UltraLabel();
            this.cbcVTHH_NhomHHDV = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel51 = new Infragistics.Win.Misc.UltraLabel();
            this.clpTCKHAC_MauSoAm = new Infragistics.Win.UltraWinEditors.UltraColorPicker();
            this.clpTCKHAC_MauCTuChuaGS = new Infragistics.Win.UltraWinEditors.UltraColorPicker();
            this.ultraLabel93 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel91 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox24 = new Infragistics.Win.Misc.UltraGroupBox();
            this.dateApplicationStartDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel103 = new Infragistics.Win.Misc.UltraLabel();
            this.cbcTCKHAC_KieuGiaoDien = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dateTCKHAC_NgayBDNamTC = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel88 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel90 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel89 = new Infragistics.Win.Misc.UltraLabel();
            this.txtTCKHAC_NamTC = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.folderBrowserDialogNoiLuuTepDaKy = new System.Windows.Forms.FolderBrowserDialog();
            this.ButApDung = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ButHuyBo = new Infragistics.Win.Misc.UltraButton();
            this.ButDongY = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbcTTDN_SoTKNH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_Nganhang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDN_TTDailythue)).BeginInit();
            this.ultraPanelDaiLyThue.ClientArea.SuspendLayout();
            this.ultraPanelDaiLyThue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateTTDN_NgayDLT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_DiachiDLT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_TenDLT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_NVDLT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_DienthoaiDLT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_FaxDLT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_MSTDLT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_HDDLso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_CCHNDLT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_QuanDLT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_TinhDLT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDN_TTDvichuquan)).BeginInit();
            this.ultraPanelDonViChuQuan.ClientArea.SuspendLayout();
            this.ultraPanelDonViChuQuan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_Tendvichuquan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_MSTdvichuquan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProductID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_Tencty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_Diachi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_MST)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).BeginInit();
            this.ultraGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbcNKY_vitrikyDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNKY_NoiluukyDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.ultraGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkNKY_Inten)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNKY_Giamdoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNKY_Thukho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNKY_Nguoilapbieu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNKY_Ketoantruong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNKY_Thuquy)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox9)).BeginInit();
            this.ultraGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBC_LapTDeBC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBC_CongGopBtoan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBC_KoInCtu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BssssC_HthiFax)).BeginInit();
            this.BssssC_HthiFax.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbcBC_VtriTtinDN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optHienThi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBC_HthiEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBC_HthiFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBC_HthiDThoai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).BeginInit();
            this.ultraGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBC_FontTencty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBC_FontDChi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBC_FontTenchuquan)).BeginInit();
            this.ultraTabPageControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox28)).BeginInit();
            this.ultraGroupBox28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox14)).BeginInit();
            this.ultraGroupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllowInputCostUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVTHH_ChoXuatQuaLuongTon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVTHH_CBaoXuatQuaLuongTon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcVTHH_KhoMacDinh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox13)).BeginInit();
            this.ultraGroupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.opttuychonkhilaphoadon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox12)).BeginInit();
            this.ultraGroupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optcachtinhgia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox11)).BeginInit();
            this.ultraGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox10)).BeginInit();
            this.ultraGroupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbcVTHH_PPTinhGiaXKho)).BeginInit();
            this.ultraTabPageControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox18)).BeginInit();
            this.ultraGroupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTL_LamThemCN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox17)).BeginInit();
            this.ultraGroupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox16)).BeginInit();
            this.ultraGroupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTL_LayTUlenBLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox15)).BeginInit();
            this.ultraGroupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optLoaiDT)).BeginInit();
            this.ultraTabPageControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox21)).BeginInit();
            this.ultraGroupBox21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDDSo_NCachHangNghin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDDSo_NCachHangDVi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox20)).BeginInit();
            this.ultraGroupBox20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbcDDSo_SoAm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcDDSo_KyHieuTien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox19)).BeginInit();
            this.ultraGroupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDDSo_DonGiaNT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDDSo_TyLePBo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDDSo_TyLe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDDSo_TyGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDDSo_SoLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDDSo_DonGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDDSo_NgoaiTe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDDSo_TienVND)).BeginInit();
            this.ultraTabPageControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox22)).BeginInit();
            this.ultraGroupBox22.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSLUU_NgaySLUU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.opthinhthucsaoluu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSLUU_TMSaoLuu)).BeginInit();
            this.ultraTabPageControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox8)).BeginInit();
            this.ultraGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailForgotPass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optPBCCDC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTCKHAC_SUDUNGTHEMSOQUANTRI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHDDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTCKHAC_DocTienBangChu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTCKHAC_CBKoChonNVBH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTCKHAC_ChepDL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTCKHAC_CPKoHLy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTCKHAC_ChiQuaTonQuy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTCKHAC_SinhTK007)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.opt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcTCKHAC_ChuHThiInHDLan2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcTCKHAC_DocTienLe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox26)).BeginInit();
            this.ultraGroupBox26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbcTCKHAC_PPTinhGiaXQuy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcTCKHAC_TKCLechLai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcTCKHAC_TKCLechLo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox25)).BeginInit();
            this.ultraGroupBox25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkLimitAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsMinimized)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcPPTTGTGT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcVTHH_NhomNNMD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcVTHH_NhomHHDV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clpTCKHAC_MauSoAm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clpTCKHAC_MauCTuChuaGS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox24)).BeginInit();
            this.ultraGroupBox24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateApplicationStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcTCKHAC_KieuGiaoDien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTCKHAC_NgayBDNamTC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTCKHAC_NamTC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox4);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox3);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox2);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1218, 568);
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.ContentPadding.Top = 10;
            this.ultraGroupBox4.Controls.Add(this.cbcTTDN_SoTKNH);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel20);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel21);
            this.ultraGroupBox4.Controls.Add(this.txtTTDN_Nganhang);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox4.Location = new System.Drawing.Point(0, 484);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(1218, 85);
            this.ultraGroupBox4.TabIndex = 10;
            this.ultraGroupBox4.Text = "Tài Khoản Ngân Hàng";
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            this.ultraGroupBox4.Visible = false;
            // 
            // cbcTTDN_SoTKNH
            // 
            this.cbcTTDN_SoTKNH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbcTTDN_SoTKNH.Location = new System.Drawing.Point(158, 25);
            this.cbcTTDN_SoTKNH.Name = "cbcTTDN_SoTKNH";
            this.cbcTTDN_SoTKNH.Size = new System.Drawing.Size(1052, 24);
            this.cbcTTDN_SoTKNH.TabIndex = 3;
            this.cbcTTDN_SoTKNH.ValueChanged += new System.EventHandler(this.cbcTTDN_SoTKNH_ValueChanged);
            // 
            // ultraLabel20
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextHAlignAsString = "Left";
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance1;
            this.ultraLabel20.AutoSize = true;
            this.ultraLabel20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel20.Location = new System.Drawing.Point(10, 31);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(73, 14);
            this.ultraLabel20.TabIndex = 0;
            this.ultraLabel20.Text = "Số tài khoản :";
            // 
            // ultraLabel21
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextHAlignAsString = "Left";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel21.Appearance = appearance2;
            this.ultraLabel21.AutoSize = true;
            this.ultraLabel21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel21.Location = new System.Drawing.Point(10, 55);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(66, 14);
            this.ultraLabel21.TabIndex = 0;
            this.ultraLabel21.Text = "Ngân hàng :";
            // 
            // txtTTDN_Nganhang
            // 
            this.txtTTDN_Nganhang.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTTDN_Nganhang.Location = new System.Drawing.Point(158, 49);
            this.txtTTDN_Nganhang.Name = "txtTTDN_Nganhang";
            this.txtTTDN_Nganhang.Size = new System.Drawing.Size(1052, 23);
            this.txtTTDN_Nganhang.TabIndex = 2;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.ContentPadding.Top = 10;
            this.ultraGroupBox3.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel11);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel12);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel14);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel13);
            this.ultraGroupBox3.Controls.Add(this.chkTTDN_TTDailythue);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel15);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel18);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel17);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel16);
            this.ultraGroupBox3.Controls.Add(this.ultraPanelDaiLyThue);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 224);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(1218, 260);
            this.ultraGroupBox3.TabIndex = 9;
            this.ultraGroupBox3.Text = "Đại lý thuế";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel8
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextHAlignAsString = "Left";
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance3;
            this.ultraLabel8.AutoSize = true;
            this.ultraLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel8.Location = new System.Drawing.Point(10, 70);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(84, 14);
            this.ultraLabel8.TabIndex = 0;
            this.ultraLabel8.Text = "Tên đại lý thuế :";
            // 
            // ultraLabel11
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextHAlignAsString = "Left";
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance4;
            this.ultraLabel11.AutoSize = true;
            this.ultraLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel11.Location = new System.Drawing.Point(10, 139);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(80, 14);
            this.ultraLabel11.TabIndex = 0;
            this.ultraLabel11.Text = "Quận / Huyện :";
            // 
            // ultraLabel9
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextHAlignAsString = "Left";
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance5;
            this.ultraLabel9.AutoSize = true;
            this.ultraLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel9.Location = new System.Drawing.Point(10, 93);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(121, 14);
            this.ultraLabel9.TabIndex = 0;
            this.ultraLabel9.Text = "Mã số thuế đại lý thuế :";
            // 
            // ultraLabel12
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance6;
            this.ultraLabel12.AutoSize = true;
            this.ultraLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel12.Location = new System.Drawing.Point(10, 160);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(61, 14);
            this.ultraLabel12.TabIndex = 0;
            this.ultraLabel12.Text = "Điện thoại :";
            // 
            // ultraLabel10
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance7;
            this.ultraLabel10.AutoSize = true;
            this.ultraLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel10.Location = new System.Drawing.Point(10, 116);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(76, 14);
            this.ultraLabel10.TabIndex = 0;
            this.ultraLabel10.Text = "Địa chỉ trụ sở :";
            // 
            // ultraLabel14
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextHAlignAsString = "Left";
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel14.Appearance = appearance8;
            this.ultraLabel14.AutoSize = true;
            this.ultraLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel14.Location = new System.Drawing.Point(10, 208);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(117, 14);
            this.ultraLabel14.TabIndex = 0;
            this.ultraLabel14.Text = "Họ tên NV đại lý thuế :";
            // 
            // ultraLabel13
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextHAlignAsString = "Left";
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance9;
            this.ultraLabel13.AutoSize = true;
            this.ultraLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel13.Location = new System.Drawing.Point(10, 181);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(98, 14);
            this.ultraLabel13.TabIndex = 0;
            this.ultraLabel13.Text = "HĐ Đại lý thuế số :";
            // 
            // chkTTDN_TTDailythue
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            this.chkTTDN_TTDailythue.Appearance = appearance10;
            this.chkTTDN_TTDailythue.BackColor = System.Drawing.Color.Transparent;
            this.chkTTDN_TTDailythue.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkTTDN_TTDailythue.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkTTDN_TTDailythue.Location = new System.Drawing.Point(158, 43);
            this.chkTTDN_TTDailythue.Name = "chkTTDN_TTDailythue";
            this.chkTTDN_TTDailythue.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkTTDN_TTDailythue.Size = new System.Drawing.Size(17, 14);
            this.chkTTDN_TTDailythue.TabIndex = 3;
            this.chkTTDN_TTDailythue.CheckedChanged += new System.EventHandler(this.chkTTDN_TTDailythue_CheckedChanged);
            // 
            // ultraLabel15
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextHAlignAsString = "Left";
            appearance11.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance11;
            this.ultraLabel15.AutoSize = true;
            this.ultraLabel15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel15.Location = new System.Drawing.Point(10, 231);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(133, 14);
            this.ultraLabel15.TabIndex = 0;
            this.ultraLabel15.Text = "Chứng chỉ hành nghề số :";
            // 
            // ultraLabel18
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.TextHAlignAsString = "Left";
            appearance12.TextVAlignAsString = "Middle";
            this.ultraLabel18.Appearance = appearance12;
            this.ultraLabel18.AutoSize = true;
            this.ultraLabel18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel18.Location = new System.Drawing.Point(700, 185);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(37, 14);
            this.ultraLabel18.TabIndex = 0;
            this.ultraLabel18.Text = "Ngày :";
            // 
            // ultraLabel7
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextHAlignAsString = "Left";
            appearance13.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance13;
            this.ultraLabel7.AutoSize = true;
            this.ultraLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel7.Location = new System.Drawing.Point(10, 43);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(112, 14);
            this.ultraLabel7.TabIndex = 0;
            this.ultraLabel7.Text = "Thông tin đại lý thuế : ";
            // 
            // ultraLabel17
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextHAlignAsString = "Left";
            appearance14.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance14;
            this.ultraLabel17.AutoSize = true;
            this.ultraLabel17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel17.Location = new System.Drawing.Point(700, 162);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(29, 14);
            this.ultraLabel17.TabIndex = 0;
            this.ultraLabel17.Text = "Fax :";
            // 
            // ultraLabel16
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextHAlignAsString = "Left";
            appearance15.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance15;
            this.ultraLabel16.AutoSize = true;
            this.ultraLabel16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel16.Location = new System.Drawing.Point(700, 139);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(96, 14);
            this.ultraLabel16.TabIndex = 0;
            this.ultraLabel16.Text = "Tỉnh / Thành phố :";
            // 
            // ultraPanelDaiLyThue
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.BorderColor = System.Drawing.Color.Transparent;
            this.ultraPanelDaiLyThue.Appearance = appearance16;
            // 
            // ultraPanelDaiLyThue.ClientArea
            // 
            this.ultraPanelDaiLyThue.ClientArea.Controls.Add(this.dateTTDN_NgayDLT);
            this.ultraPanelDaiLyThue.ClientArea.Controls.Add(this.txtTTDN_DiachiDLT);
            this.ultraPanelDaiLyThue.ClientArea.Controls.Add(this.txtTTDN_TenDLT);
            this.ultraPanelDaiLyThue.ClientArea.Controls.Add(this.txtTTDN_NVDLT);
            this.ultraPanelDaiLyThue.ClientArea.Controls.Add(this.txtTTDN_DienthoaiDLT);
            this.ultraPanelDaiLyThue.ClientArea.Controls.Add(this.txtTTDN_FaxDLT);
            this.ultraPanelDaiLyThue.ClientArea.Controls.Add(this.txtTTDN_MSTDLT);
            this.ultraPanelDaiLyThue.ClientArea.Controls.Add(this.txtTTDN_HDDLso);
            this.ultraPanelDaiLyThue.ClientArea.Controls.Add(this.txtTTDN_CCHNDLT);
            this.ultraPanelDaiLyThue.ClientArea.Controls.Add(this.txtTTDN_QuanDLT);
            this.ultraPanelDaiLyThue.ClientArea.Controls.Add(this.txtTTDN_TinhDLT);
            this.ultraPanelDaiLyThue.Location = new System.Drawing.Point(158, 64);
            this.ultraPanelDaiLyThue.Name = "ultraPanelDaiLyThue";
            this.ultraPanelDaiLyThue.Size = new System.Drawing.Size(1055, 190);
            this.ultraPanelDaiLyThue.TabIndex = 5;
            // 
            // dateTTDN_NgayDLT
            // 
            this.dateTTDN_NgayDLT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTTDN_NgayDLT.FormatString = "";
            this.dateTTDN_NgayDLT.Location = new System.Drawing.Point(645, 115);
            this.dateTTDN_NgayDLT.MaskInput = "{date}";
            this.dateTTDN_NgayDLT.Name = "dateTTDN_NgayDLT";
            this.dateTTDN_NgayDLT.Size = new System.Drawing.Size(247, 23);
            this.dateTTDN_NgayDLT.TabIndex = 4;
            // 
            // txtTTDN_DiachiDLT
            // 
            this.txtTTDN_DiachiDLT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTTDN_DiachiDLT.Location = new System.Drawing.Point(0, 46);
            this.txtTTDN_DiachiDLT.Name = "txtTTDN_DiachiDLT";
            this.txtTTDN_DiachiDLT.Size = new System.Drawing.Size(1052, 23);
            this.txtTTDN_DiachiDLT.TabIndex = 2;
            // 
            // txtTTDN_TenDLT
            // 
            this.txtTTDN_TenDLT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTTDN_TenDLT.Location = new System.Drawing.Point(0, 0);
            this.txtTTDN_TenDLT.Name = "txtTTDN_TenDLT";
            this.txtTTDN_TenDLT.Size = new System.Drawing.Size(1052, 23);
            this.txtTTDN_TenDLT.TabIndex = 2;
            // 
            // txtTTDN_NVDLT
            // 
            this.txtTTDN_NVDLT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTTDN_NVDLT.Location = new System.Drawing.Point(0, 138);
            this.txtTTDN_NVDLT.Name = "txtTTDN_NVDLT";
            this.txtTTDN_NVDLT.Size = new System.Drawing.Size(1052, 23);
            this.txtTTDN_NVDLT.TabIndex = 2;
            // 
            // txtTTDN_DienthoaiDLT
            // 
            this.txtTTDN_DienthoaiDLT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTTDN_DienthoaiDLT.Location = new System.Drawing.Point(0, 92);
            this.txtTTDN_DienthoaiDLT.Name = "txtTTDN_DienthoaiDLT";
            this.txtTTDN_DienthoaiDLT.Size = new System.Drawing.Size(443, 23);
            this.txtTTDN_DienthoaiDLT.TabIndex = 2;
            // 
            // txtTTDN_FaxDLT
            // 
            this.txtTTDN_FaxDLT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTTDN_FaxDLT.Location = new System.Drawing.Point(645, 92);
            this.txtTTDN_FaxDLT.Name = "txtTTDN_FaxDLT";
            this.txtTTDN_FaxDLT.Size = new System.Drawing.Size(407, 23);
            this.txtTTDN_FaxDLT.TabIndex = 2;
            // 
            // txtTTDN_MSTDLT
            // 
            this.txtTTDN_MSTDLT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTTDN_MSTDLT.Location = new System.Drawing.Point(0, 23);
            this.txtTTDN_MSTDLT.Name = "txtTTDN_MSTDLT";
            this.txtTTDN_MSTDLT.Size = new System.Drawing.Size(1052, 23);
            this.txtTTDN_MSTDLT.TabIndex = 2;
            // 
            // txtTTDN_HDDLso
            // 
            this.txtTTDN_HDDLso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTTDN_HDDLso.Location = new System.Drawing.Point(0, 115);
            this.txtTTDN_HDDLso.Name = "txtTTDN_HDDLso";
            this.txtTTDN_HDDLso.Size = new System.Drawing.Size(443, 23);
            this.txtTTDN_HDDLso.TabIndex = 2;
            // 
            // txtTTDN_CCHNDLT
            // 
            this.txtTTDN_CCHNDLT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTTDN_CCHNDLT.Location = new System.Drawing.Point(0, 161);
            this.txtTTDN_CCHNDLT.Name = "txtTTDN_CCHNDLT";
            this.txtTTDN_CCHNDLT.Size = new System.Drawing.Size(1052, 23);
            this.txtTTDN_CCHNDLT.TabIndex = 2;
            // 
            // txtTTDN_QuanDLT
            // 
            this.txtTTDN_QuanDLT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTTDN_QuanDLT.Location = new System.Drawing.Point(0, 69);
            this.txtTTDN_QuanDLT.Name = "txtTTDN_QuanDLT";
            this.txtTTDN_QuanDLT.Size = new System.Drawing.Size(443, 23);
            this.txtTTDN_QuanDLT.TabIndex = 2;
            // 
            // txtTTDN_TinhDLT
            // 
            this.txtTTDN_TinhDLT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTTDN_TinhDLT.Location = new System.Drawing.Point(645, 69);
            this.txtTTDN_TinhDLT.Name = "txtTTDN_TinhDLT";
            this.txtTTDN_TinhDLT.Size = new System.Drawing.Size(407, 23);
            this.txtTTDN_TinhDLT.TabIndex = 2;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.ContentPadding.Top = 10;
            this.ultraGroupBox2.Controls.Add(this.chkTTDN_TTDvichuquan);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox2.Controls.Add(this.ultraPanelDonViChuQuan);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 109);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(1218, 115);
            this.ultraGroupBox2.TabIndex = 8;
            this.ultraGroupBox2.Text = "Đơn vị chủ quản";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // chkTTDN_TTDvichuquan
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            this.chkTTDN_TTDvichuquan.Appearance = appearance17;
            this.chkTTDN_TTDvichuquan.BackColor = System.Drawing.Color.Transparent;
            this.chkTTDN_TTDvichuquan.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkTTDN_TTDvichuquan.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkTTDN_TTDvichuquan.Location = new System.Drawing.Point(158, 41);
            this.chkTTDN_TTDvichuquan.Name = "chkTTDN_TTDvichuquan";
            this.chkTTDN_TTDvichuquan.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkTTDN_TTDvichuquan.Size = new System.Drawing.Size(17, 16);
            this.chkTTDN_TTDvichuquan.TabIndex = 4;
            this.chkTTDN_TTDvichuquan.CheckedChanged += new System.EventHandler(this.chkTTDN_TTDvichuquan_CheckedChanged);
            // 
            // ultraLabel4
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextHAlignAsString = "Left";
            appearance18.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance18;
            this.ultraLabel4.AutoSize = true;
            this.ultraLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel4.Location = new System.Drawing.Point(10, 41);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(109, 14);
            this.ultraLabel4.TabIndex = 0;
            this.ultraLabel4.Text = "TT Đơn vị chủ quản :";
            // 
            // ultraLabel5
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.TextHAlignAsString = "Left";
            appearance19.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance19;
            this.ultraLabel5.AutoSize = true;
            this.ultraLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel5.Location = new System.Drawing.Point(10, 67);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(30, 14);
            this.ultraLabel5.TabIndex = 0;
            this.ultraLabel5.Text = "Tên :";
            // 
            // ultraLabel6
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            appearance20.TextHAlignAsString = "Left";
            appearance20.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance20;
            this.ultraLabel6.AutoSize = true;
            this.ultraLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel6.Location = new System.Drawing.Point(10, 90);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(66, 14);
            this.ultraLabel6.TabIndex = 0;
            this.ultraLabel6.Text = "Mã số thuế :";
            // 
            // ultraPanelDonViChuQuan
            // 
            appearance21.BackColor = System.Drawing.Color.Transparent;
            appearance21.BorderColor = System.Drawing.Color.Transparent;
            this.ultraPanelDonViChuQuan.Appearance = appearance21;
            // 
            // ultraPanelDonViChuQuan.ClientArea
            // 
            this.ultraPanelDonViChuQuan.ClientArea.Controls.Add(this.txtTTDN_Tendvichuquan);
            this.ultraPanelDonViChuQuan.ClientArea.Controls.Add(this.txtTTDN_MSTdvichuquan);
            this.ultraPanelDonViChuQuan.Location = new System.Drawing.Point(158, 61);
            this.ultraPanelDonViChuQuan.Name = "ultraPanelDonViChuQuan";
            this.ultraPanelDonViChuQuan.Size = new System.Drawing.Size(1052, 48);
            this.ultraPanelDonViChuQuan.TabIndex = 5;
            // 
            // txtTTDN_Tendvichuquan
            // 
            this.txtTTDN_Tendvichuquan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTTDN_Tendvichuquan.Location = new System.Drawing.Point(0, 0);
            this.txtTTDN_Tendvichuquan.Name = "txtTTDN_Tendvichuquan";
            this.txtTTDN_Tendvichuquan.Size = new System.Drawing.Size(1052, 23);
            this.txtTTDN_Tendvichuquan.TabIndex = 2;
            // 
            // txtTTDN_MSTdvichuquan
            // 
            this.txtTTDN_MSTdvichuquan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTTDN_MSTdvichuquan.Location = new System.Drawing.Point(0, 23);
            this.txtTTDN_MSTdvichuquan.Name = "txtTTDN_MSTdvichuquan";
            this.txtTTDN_MSTdvichuquan.Size = new System.Drawing.Size(274, 23);
            this.txtTTDN_MSTdvichuquan.TabIndex = 2;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.ContentPadding.Top = 10;
            this.ultraGroupBox1.Controls.Add(this.ultraLabel98);
            this.ultraGroupBox1.Controls.Add(this.txtProductID);
            this.ultraGroupBox1.Controls.Add(this.txtTTDN_Tencty);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox1.Controls.Add(this.txtTTDN_Diachi);
            this.ultraGroupBox1.Controls.Add(this.txtTTDN_MST);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance26.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox1.HeaderAppearance = appearance26;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1218, 109);
            this.ultraGroupBox1.TabIndex = 7;
            this.ultraGroupBox1.Text = "Công ty";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel98
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            appearance22.TextHAlignAsString = "Left";
            appearance22.TextVAlignAsString = "Middle";
            this.ultraLabel98.Appearance = appearance22;
            this.ultraLabel98.AutoSize = true;
            this.ultraLabel98.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel98.Location = new System.Drawing.Point(788, 83);
            this.ultraLabel98.Name = "ultraLabel98";
            this.ultraLabel98.Size = new System.Drawing.Size(132, 14);
            this.ultraLabel98.TabIndex = 3;
            this.ultraLabel98.Text = "Mã định danh phần mềm:";
            // 
            // txtProductID
            // 
            this.txtProductID.Enabled = false;
            this.txtProductID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductID.Location = new System.Drawing.Point(936, 77);
            this.txtProductID.Name = "txtProductID";
            this.txtProductID.PasswordChar = '*';
            this.txtProductID.Size = new System.Drawing.Size(274, 23);
            this.txtProductID.TabIndex = 4;
            // 
            // txtTTDN_Tencty
            // 
            this.txtTTDN_Tencty.Enabled = false;
            this.txtTTDN_Tencty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTTDN_Tencty.Location = new System.Drawing.Point(158, 31);
            this.txtTTDN_Tencty.Name = "txtTTDN_Tencty";
            this.txtTTDN_Tencty.Size = new System.Drawing.Size(1052, 23);
            this.txtTTDN_Tencty.TabIndex = 2;
            // 
            // ultraLabel1
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            appearance23.TextHAlignAsString = "Left";
            appearance23.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance23;
            this.ultraLabel1.AutoSize = true;
            this.ultraLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel1.Location = new System.Drawing.Point(10, 37);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(69, 14);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Tên công ty :";
            // 
            // ultraLabel2
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            appearance24.TextHAlignAsString = "Left";
            appearance24.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance24;
            this.ultraLabel2.AutoSize = true;
            this.ultraLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel2.Location = new System.Drawing.Point(10, 60);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(45, 14);
            this.ultraLabel2.TabIndex = 0;
            this.ultraLabel2.Text = "Địa chỉ :";
            // 
            // ultraLabel3
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            appearance25.TextHAlignAsString = "Left";
            appearance25.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance25;
            this.ultraLabel3.AutoSize = true;
            this.ultraLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel3.Location = new System.Drawing.Point(10, 83);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(66, 14);
            this.ultraLabel3.TabIndex = 0;
            this.ultraLabel3.Text = "Mã số thuế :";
            // 
            // txtTTDN_Diachi
            // 
            this.txtTTDN_Diachi.Enabled = false;
            this.txtTTDN_Diachi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTTDN_Diachi.Location = new System.Drawing.Point(158, 54);
            this.txtTTDN_Diachi.Name = "txtTTDN_Diachi";
            this.txtTTDN_Diachi.Size = new System.Drawing.Size(1052, 23);
            this.txtTTDN_Diachi.TabIndex = 2;
            // 
            // txtTTDN_MST
            // 
            this.txtTTDN_MST.Enabled = false;
            this.txtTTDN_MST.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTTDN_MST.Location = new System.Drawing.Point(158, 77);
            this.txtTTDN_MST.Name = "txtTTDN_MST";
            this.txtTTDN_MST.Size = new System.Drawing.Size(274, 23);
            this.txtTTDN_MST.TabIndex = 2;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.ultraGroupBox6);
            this.ultraTabPageControl2.Controls.Add(this.ultraGroupBox5);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1218, 568);
            // 
            // ultraGroupBox6
            // 
            this.ultraGroupBox6.ContentPadding.Top = 10;
            this.ultraGroupBox6.Controls.Add(this.cbcNKY_vitrikyDT);
            this.ultraGroupBox6.Controls.Add(this.meNKY_ChieurongkyDT);
            this.ultraGroupBox6.Controls.Add(this.meNKY_ChieudaikyDT);
            this.ultraGroupBox6.Controls.Add(this.txtNKY_NoiluukyDT);
            this.ultraGroupBox6.Controls.Add(this.ButNK_ChonNoiLuTep);
            this.ultraGroupBox6.Controls.Add(this.ultraLabel27);
            this.ultraGroupBox6.Controls.Add(this.ultraLabel30);
            this.ultraGroupBox6.Controls.Add(this.ultraLabel32);
            this.ultraGroupBox6.Controls.Add(this.ultraLabel29);
            this.ultraGroupBox6.Controls.Add(this.ultraLabel31);
            this.ultraGroupBox6.Controls.Add(this.ultraLabel28);
            this.ultraGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox6.Location = new System.Drawing.Point(0, 188);
            this.ultraGroupBox6.Name = "ultraGroupBox6";
            this.ultraGroupBox6.Size = new System.Drawing.Size(1218, 377);
            this.ultraGroupBox6.TabIndex = 9;
            this.ultraGroupBox6.Text = "Thông tin chữ ký điện tử ";
            this.ultraGroupBox6.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            this.ultraGroupBox6.Visible = false;
            // 
            // cbcNKY_vitrikyDT
            // 
            this.cbcNKY_vitrikyDT.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.cbcNKY_vitrikyDT.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbcNKY_vitrikyDT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbcNKY_vitrikyDT.Location = new System.Drawing.Point(154, 75);
            this.cbcNKY_vitrikyDT.Name = "cbcNKY_vitrikyDT";
            this.cbcNKY_vitrikyDT.Size = new System.Drawing.Size(274, 24);
            this.cbcNKY_vitrikyDT.TabIndex = 14;
            // 
            // meNKY_ChieurongkyDT
            // 
            appearance27.FontData.BoldAsString = "False";
            appearance27.TextHAlignAsString = "Right";
            this.meNKY_ChieurongkyDT.Appearance = appearance27;
            this.meNKY_ChieurongkyDT.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.String;
            this.meNKY_ChieurongkyDT.InputMask = "nnn,nn";
            this.meNKY_ChieurongkyDT.Location = new System.Drawing.Point(154, 54);
            this.meNKY_ChieurongkyDT.Name = "meNKY_ChieurongkyDT";
            this.meNKY_ChieurongkyDT.PromptChar = ' ';
            this.meNKY_ChieurongkyDT.Size = new System.Drawing.Size(274, 21);
            this.meNKY_ChieurongkyDT.TabIndex = 13;
            // 
            // meNKY_ChieudaikyDT
            // 
            appearance28.FontData.BoldAsString = "False";
            appearance28.TextHAlignAsString = "Right";
            this.meNKY_ChieudaikyDT.Appearance = appearance28;
            this.meNKY_ChieudaikyDT.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.String;
            this.meNKY_ChieudaikyDT.InputMask = "nnn,nn";
            this.meNKY_ChieudaikyDT.Location = new System.Drawing.Point(154, 33);
            this.meNKY_ChieudaikyDT.Name = "meNKY_ChieudaikyDT";
            this.meNKY_ChieudaikyDT.PromptChar = ' ';
            this.meNKY_ChieudaikyDT.Size = new System.Drawing.Size(274, 21);
            this.meNKY_ChieudaikyDT.TabIndex = 13;
            // 
            // txtNKY_NoiluukyDT
            // 
            this.txtNKY_NoiluukyDT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNKY_NoiluukyDT.Location = new System.Drawing.Point(154, 99);
            this.txtNKY_NoiluukyDT.Name = "txtNKY_NoiluukyDT";
            this.txtNKY_NoiluukyDT.Size = new System.Drawing.Size(911, 23);
            this.txtNKY_NoiluukyDT.TabIndex = 7;
            // 
            // ButNK_ChonNoiLuTep
            // 
            appearance29.Image = global::Accounting.Properties.Resources.Avosoft_Warm_Toolbar_Folder_open;
            this.ButNK_ChonNoiLuTep.Appearance = appearance29;
            this.ButNK_ChonNoiLuTep.Location = new System.Drawing.Point(1075, 101);
            this.ButNK_ChonNoiLuTep.Name = "ButNK_ChonNoiLuTep";
            this.ButNK_ChonNoiLuTep.Size = new System.Drawing.Size(96, 24);
            this.ButNK_ChonNoiLuTep.TabIndex = 12;
            this.ButNK_ChonNoiLuTep.Text = "Chọn ...";
            this.ButNK_ChonNoiLuTep.Click += new System.EventHandler(this.ButNK_ChonNoiLuTep_Click);
            // 
            // ultraLabel27
            // 
            appearance30.BackColor = System.Drawing.Color.Transparent;
            appearance30.TextHAlignAsString = "Left";
            appearance30.TextVAlignAsString = "Middle";
            this.ultraLabel27.Appearance = appearance30;
            this.ultraLabel27.AutoSize = true;
            this.ultraLabel27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel27.Location = new System.Drawing.Point(10, 34);
            this.ultraLabel27.Name = "ultraLabel27";
            this.ultraLabel27.Size = new System.Drawing.Size(58, 14);
            this.ultraLabel27.TabIndex = 0;
            this.ultraLabel27.Text = "Chiều dài :";
            // 
            // ultraLabel30
            // 
            appearance31.BackColor = System.Drawing.Color.Transparent;
            appearance31.TextHAlignAsString = "Left";
            appearance31.TextVAlignAsString = "Middle";
            this.ultraLabel30.Appearance = appearance31;
            this.ultraLabel30.AutoSize = true;
            this.ultraLabel30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel30.Location = new System.Drawing.Point(10, 107);
            this.ultraLabel30.Name = "ultraLabel30";
            this.ultraLabel30.Size = new System.Drawing.Size(94, 14);
            this.ultraLabel30.TabIndex = 0;
            this.ultraLabel30.Text = "Nơi lưu tệp đã ký :";
            // 
            // ultraLabel32
            // 
            appearance32.BackColor = System.Drawing.Color.Transparent;
            appearance32.TextHAlignAsString = "Left";
            appearance32.TextVAlignAsString = "Middle";
            this.ultraLabel32.Appearance = appearance32;
            this.ultraLabel32.AutoSize = true;
            this.ultraLabel32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel32.Location = new System.Drawing.Point(438, 60);
            this.ultraLabel32.Name = "ultraLabel32";
            this.ultraLabel32.Size = new System.Drawing.Size(29, 14);
            this.ultraLabel32.TabIndex = 0;
            this.ultraLabel32.Text = "(Cm)";
            // 
            // ultraLabel29
            // 
            appearance33.BackColor = System.Drawing.Color.Transparent;
            appearance33.TextHAlignAsString = "Left";
            appearance33.TextVAlignAsString = "Middle";
            this.ultraLabel29.Appearance = appearance33;
            this.ultraLabel29.AutoSize = true;
            this.ultraLabel29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel29.Location = new System.Drawing.Point(10, 87);
            this.ultraLabel29.Name = "ultraLabel29";
            this.ultraLabel29.Size = new System.Drawing.Size(69, 14);
            this.ultraLabel29.TabIndex = 0;
            this.ultraLabel29.Text = "Vị trí chữ ký :";
            // 
            // ultraLabel31
            // 
            appearance34.BackColor = System.Drawing.Color.Transparent;
            appearance34.TextHAlignAsString = "Left";
            appearance34.TextVAlignAsString = "Middle";
            this.ultraLabel31.Appearance = appearance34;
            this.ultraLabel31.AutoSize = true;
            this.ultraLabel31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel31.Location = new System.Drawing.Point(438, 37);
            this.ultraLabel31.Name = "ultraLabel31";
            this.ultraLabel31.Size = new System.Drawing.Size(29, 14);
            this.ultraLabel31.TabIndex = 0;
            this.ultraLabel31.Text = "(Cm)";
            // 
            // ultraLabel28
            // 
            appearance35.BackColor = System.Drawing.Color.Transparent;
            appearance35.TextHAlignAsString = "Left";
            appearance35.TextVAlignAsString = "Middle";
            this.ultraLabel28.Appearance = appearance35;
            this.ultraLabel28.AutoSize = true;
            this.ultraLabel28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel28.Location = new System.Drawing.Point(10, 60);
            this.ultraLabel28.Name = "ultraLabel28";
            this.ultraLabel28.Size = new System.Drawing.Size(66, 14);
            this.ultraLabel28.TabIndex = 0;
            this.ultraLabel28.Text = "Chiều rộng :";
            // 
            // ultraGroupBox5
            // 
            appearance36.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox5.Appearance = appearance36;
            this.ultraGroupBox5.ContentPadding.Top = 10;
            this.ultraGroupBox5.Controls.Add(this.ultraLabel26);
            this.ultraGroupBox5.Controls.Add(this.chkNKY_Inten);
            this.ultraGroupBox5.Controls.Add(this.txtNKY_Giamdoc);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel19);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel25);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel22);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel24);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel23);
            this.ultraGroupBox5.Controls.Add(this.txtNKY_Thukho);
            this.ultraGroupBox5.Controls.Add(this.txtNKY_Nguoilapbieu);
            this.ultraGroupBox5.Controls.Add(this.txtNKY_Ketoantruong);
            this.ultraGroupBox5.Controls.Add(this.txtNKY_Thuquy);
            this.ultraGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance44.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox5.HeaderAppearance = appearance44;
            this.ultraGroupBox5.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(1218, 188);
            this.ultraGroupBox5.TabIndex = 8;
            this.ultraGroupBox5.Text = "Tên người ký";
            this.ultraGroupBox5.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel26
            // 
            appearance37.BackColor = System.Drawing.Color.Transparent;
            appearance37.TextHAlignAsString = "Left";
            appearance37.TextVAlignAsString = "Middle";
            this.ultraLabel26.Appearance = appearance37;
            this.ultraLabel26.AutoSize = true;
            this.ultraLabel26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel26.Location = new System.Drawing.Point(10, 152);
            this.ultraLabel26.Name = "ultraLabel26";
            this.ultraLabel26.Size = new System.Drawing.Size(104, 14);
            this.ultraLabel26.TabIndex = 4;
            this.ultraLabel26.Text = "In tên trên báo cáo :";
            // 
            // chkNKY_Inten
            // 
            appearance38.BackColor = System.Drawing.Color.Transparent;
            this.chkNKY_Inten.Appearance = appearance38;
            this.chkNKY_Inten.BackColor = System.Drawing.Color.Transparent;
            this.chkNKY_Inten.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkNKY_Inten.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkNKY_Inten.Location = new System.Drawing.Point(158, 152);
            this.chkNKY_Inten.Name = "chkNKY_Inten";
            this.chkNKY_Inten.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkNKY_Inten.Size = new System.Drawing.Size(15, 14);
            this.chkNKY_Inten.TabIndex = 5;
            // 
            // txtNKY_Giamdoc
            // 
            this.txtNKY_Giamdoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNKY_Giamdoc.Location = new System.Drawing.Point(158, 31);
            this.txtNKY_Giamdoc.Name = "txtNKY_Giamdoc";
            this.txtNKY_Giamdoc.Size = new System.Drawing.Size(1052, 23);
            this.txtNKY_Giamdoc.TabIndex = 2;
            // 
            // ultraLabel19
            // 
            appearance39.BackColor = System.Drawing.Color.Transparent;
            appearance39.TextHAlignAsString = "Left";
            appearance39.TextVAlignAsString = "Middle";
            this.ultraLabel19.Appearance = appearance39;
            this.ultraLabel19.AutoSize = true;
            this.ultraLabel19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel19.Location = new System.Drawing.Point(10, 37);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(59, 14);
            this.ultraLabel19.TabIndex = 0;
            this.ultraLabel19.Text = "Giám đốc :";
            // 
            // ultraLabel25
            // 
            appearance40.BackColor = System.Drawing.Color.Transparent;
            appearance40.TextHAlignAsString = "Left";
            appearance40.TextVAlignAsString = "Middle";
            this.ultraLabel25.Appearance = appearance40;
            this.ultraLabel25.AutoSize = true;
            this.ultraLabel25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel25.Location = new System.Drawing.Point(10, 106);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(51, 14);
            this.ultraLabel25.TabIndex = 0;
            this.ultraLabel25.Text = "Thủ kho :";
            // 
            // ultraLabel22
            // 
            appearance41.BackColor = System.Drawing.Color.Transparent;
            appearance41.TextHAlignAsString = "Left";
            appearance41.TextVAlignAsString = "Middle";
            this.ultraLabel22.Appearance = appearance41;
            this.ultraLabel22.AutoSize = true;
            this.ultraLabel22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel22.Location = new System.Drawing.Point(10, 60);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(84, 14);
            this.ultraLabel22.TabIndex = 0;
            this.ultraLabel22.Text = "Kế toán trưởng :";
            // 
            // ultraLabel24
            // 
            appearance42.BackColor = System.Drawing.Color.Transparent;
            appearance42.TextHAlignAsString = "Left";
            appearance42.TextVAlignAsString = "Middle";
            this.ultraLabel24.Appearance = appearance42;
            this.ultraLabel24.AutoSize = true;
            this.ultraLabel24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel24.Location = new System.Drawing.Point(10, 129);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(83, 14);
            this.ultraLabel24.TabIndex = 0;
            this.ultraLabel24.Text = "Người lập biểu :";
            // 
            // ultraLabel23
            // 
            appearance43.BackColor = System.Drawing.Color.Transparent;
            appearance43.TextHAlignAsString = "Left";
            appearance43.TextVAlignAsString = "Middle";
            this.ultraLabel23.Appearance = appearance43;
            this.ultraLabel23.AutoSize = true;
            this.ultraLabel23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel23.Location = new System.Drawing.Point(10, 83);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(51, 14);
            this.ultraLabel23.TabIndex = 0;
            this.ultraLabel23.Text = "Thủ quỹ :";
            // 
            // txtNKY_Thukho
            // 
            this.txtNKY_Thukho.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNKY_Thukho.Location = new System.Drawing.Point(158, 100);
            this.txtNKY_Thukho.Name = "txtNKY_Thukho";
            this.txtNKY_Thukho.Size = new System.Drawing.Size(1052, 23);
            this.txtNKY_Thukho.TabIndex = 2;
            // 
            // txtNKY_Nguoilapbieu
            // 
            this.txtNKY_Nguoilapbieu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNKY_Nguoilapbieu.Location = new System.Drawing.Point(158, 123);
            this.txtNKY_Nguoilapbieu.Name = "txtNKY_Nguoilapbieu";
            this.txtNKY_Nguoilapbieu.Size = new System.Drawing.Size(1052, 23);
            this.txtNKY_Nguoilapbieu.TabIndex = 2;
            // 
            // txtNKY_Ketoantruong
            // 
            this.txtNKY_Ketoantruong.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNKY_Ketoantruong.Location = new System.Drawing.Point(158, 54);
            this.txtNKY_Ketoantruong.Name = "txtNKY_Ketoantruong";
            this.txtNKY_Ketoantruong.Size = new System.Drawing.Size(1052, 23);
            this.txtNKY_Ketoantruong.TabIndex = 2;
            // 
            // txtNKY_Thuquy
            // 
            this.txtNKY_Thuquy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNKY_Thuquy.Location = new System.Drawing.Point(158, 77);
            this.txtNKY_Thuquy.Name = "txtNKY_Thuquy";
            this.txtNKY_Thuquy.Size = new System.Drawing.Size(1052, 23);
            this.txtNKY_Thuquy.TabIndex = 2;
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.ultraGroupBox9);
            this.ultraTabPageControl3.Controls.Add(this.BssssC_HthiFax);
            this.ultraTabPageControl3.Controls.Add(this.ultraGroupBox7);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(1218, 568);
            // 
            // ultraGroupBox9
            // 
            appearance45.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox9.Appearance = appearance45;
            this.ultraGroupBox9.ContentPadding.Top = 10;
            this.ultraGroupBox9.Controls.Add(this.ultraLabel46);
            this.ultraGroupBox9.Controls.Add(this.ultraLabel44);
            this.ultraGroupBox9.Controls.Add(this.ultraLabel45);
            this.ultraGroupBox9.Controls.Add(this.chkBC_LapTDeBC);
            this.ultraGroupBox9.Controls.Add(this.chkBC_CongGopBtoan);
            this.ultraGroupBox9.Controls.Add(this.chkBC_KoInCtu);
            this.ultraGroupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance52.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox9.HeaderAppearance = appearance52;
            this.ultraGroupBox9.Location = new System.Drawing.Point(610, 285);
            this.ultraGroupBox9.Name = "ultraGroupBox9";
            this.ultraGroupBox9.Size = new System.Drawing.Size(608, 135);
            this.ultraGroupBox9.TabIndex = 18;
            this.ultraGroupBox9.Text = "Thiết lập khác";
            this.ultraGroupBox9.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel46
            // 
            appearance46.BackColor = System.Drawing.Color.Transparent;
            appearance46.TextHAlignAsString = "Left";
            appearance46.TextVAlignAsString = "Middle";
            this.ultraLabel46.Appearance = appearance46;
            this.ultraLabel46.AutoSize = true;
            this.ultraLabel46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel46.Location = new System.Drawing.Point(33, 31);
            this.ultraLabel46.Name = "ultraLabel46";
            this.ultraLabel46.Size = new System.Drawing.Size(195, 14);
            this.ultraLabel46.TabIndex = 0;
            this.ultraLabel46.Text = "Lặp lại tiêu đề báo cáo sang trang sau";
            // 
            // ultraLabel44
            // 
            appearance47.BackColor = System.Drawing.Color.Transparent;
            appearance47.TextHAlignAsString = "Left";
            appearance47.TextVAlignAsString = "Middle";
            this.ultraLabel44.Appearance = appearance47;
            this.ultraLabel44.AutoSize = true;
            this.ultraLabel44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel44.Location = new System.Drawing.Point(33, 73);
            this.ultraLabel44.Name = "ultraLabel44";
            this.ultraLabel44.Size = new System.Drawing.Size(371, 14);
            this.ultraLabel44.TabIndex = 0;
            this.ultraLabel44.Text = "Cộng gộp các dòng giống nhau trên chứng từ bán hàng và phiếu xuất kho";
            // 
            // ultraLabel45
            // 
            appearance48.BackColor = System.Drawing.Color.Transparent;
            appearance48.TextHAlignAsString = "Left";
            appearance48.TextVAlignAsString = "Middle";
            this.ultraLabel45.Appearance = appearance48;
            this.ultraLabel45.AutoSize = true;
            this.ultraLabel45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel45.Location = new System.Drawing.Point(33, 53);
            this.ultraLabel45.Name = "ultraLabel45";
            this.ultraLabel45.Size = new System.Drawing.Size(183, 14);
            this.ultraLabel45.TabIndex = 0;
            this.ultraLabel45.Text = "Không in chứng từ chưa được ghi sổ";
            // 
            // chkBC_LapTDeBC
            // 
            appearance49.BackColor = System.Drawing.Color.Transparent;
            this.chkBC_LapTDeBC.Appearance = appearance49;
            this.chkBC_LapTDeBC.BackColor = System.Drawing.Color.Transparent;
            this.chkBC_LapTDeBC.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkBC_LapTDeBC.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkBC_LapTDeBC.Location = new System.Drawing.Point(10, 29);
            this.chkBC_LapTDeBC.Name = "chkBC_LapTDeBC";
            this.chkBC_LapTDeBC.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkBC_LapTDeBC.Size = new System.Drawing.Size(17, 16);
            this.chkBC_LapTDeBC.TabIndex = 5;
            // 
            // chkBC_CongGopBtoan
            // 
            appearance50.BackColor = System.Drawing.Color.Transparent;
            this.chkBC_CongGopBtoan.Appearance = appearance50;
            this.chkBC_CongGopBtoan.BackColor = System.Drawing.Color.Transparent;
            this.chkBC_CongGopBtoan.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkBC_CongGopBtoan.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkBC_CongGopBtoan.Location = new System.Drawing.Point(10, 73);
            this.chkBC_CongGopBtoan.Name = "chkBC_CongGopBtoan";
            this.chkBC_CongGopBtoan.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkBC_CongGopBtoan.Size = new System.Drawing.Size(17, 16);
            this.chkBC_CongGopBtoan.TabIndex = 5;
            // 
            // chkBC_KoInCtu
            // 
            appearance51.BackColor = System.Drawing.Color.Transparent;
            this.chkBC_KoInCtu.Appearance = appearance51;
            this.chkBC_KoInCtu.BackColor = System.Drawing.Color.Transparent;
            this.chkBC_KoInCtu.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkBC_KoInCtu.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkBC_KoInCtu.Location = new System.Drawing.Point(10, 51);
            this.chkBC_KoInCtu.Name = "chkBC_KoInCtu";
            this.chkBC_KoInCtu.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkBC_KoInCtu.Size = new System.Drawing.Size(17, 16);
            this.chkBC_KoInCtu.TabIndex = 5;
            // 
            // BssssC_HthiFax
            // 
            appearance53.BackColor = System.Drawing.Color.Transparent;
            this.BssssC_HthiFax.Appearance = appearance53;
            this.BssssC_HthiFax.ContentPadding.Top = 10;
            this.BssssC_HthiFax.Controls.Add(this.cbcBC_VtriTtinDN);
            this.BssssC_HthiFax.Controls.Add(this.optHienThi);
            this.BssssC_HthiFax.Controls.Add(this.chkBC_HthiEmail);
            this.BssssC_HthiFax.Controls.Add(this.chkBC_HthiFax);
            this.BssssC_HthiFax.Controls.Add(this.chkBC_HthiDThoai);
            this.BssssC_HthiFax.Controls.Add(this.ultraLabel42);
            this.BssssC_HthiFax.Controls.Add(this.ultraLabel41);
            this.BssssC_HthiFax.Controls.Add(this.BC_HthiDThoai);
            this.BssssC_HthiFax.Controls.Add(this.ultraLabel43);
            this.BssssC_HthiFax.Controls.Add(this.ultraLabel39);
            this.BssssC_HthiFax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance63.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.BssssC_HthiFax.HeaderAppearance = appearance63;
            this.BssssC_HthiFax.Location = new System.Drawing.Point(0, 285);
            this.BssssC_HthiFax.Name = "BssssC_HthiFax";
            this.BssssC_HthiFax.Size = new System.Drawing.Size(1219, 135);
            this.BssssC_HthiFax.TabIndex = 17;
            this.BssssC_HthiFax.Text = "Hiển thị thông tin công ty";
            this.BssssC_HthiFax.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbcBC_VtriTtinDN
            // 
            this.cbcBC_VtriTtinDN.AutoSize = false;
            this.cbcBC_VtriTtinDN.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.cbcBC_VtriTtinDN.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbcBC_VtriTtinDN.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbcBC_VtriTtinDN.Location = new System.Drawing.Point(156, 89);
            this.cbcBC_VtriTtinDN.Name = "cbcBC_VtriTtinDN";
            this.cbcBC_VtriTtinDN.Size = new System.Drawing.Size(196, 22);
            this.cbcBC_VtriTtinDN.TabIndex = 7;
            this.cbcBC_VtriTtinDN.Visible = false;
            // 
            // optHienThi
            // 
            appearance54.FontData.BoldAsString = "False";
            this.optHienThi.Appearance = appearance54;
            this.optHienThi.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.optHienThi.CheckedIndex = 0;
            valueListItem5.DataValue = "Default Item";
            valueListItem5.DisplayText = "Hiển thị tên công ty , địa chỉ  và mã số thuế";
            valueListItem5.Tag = "optBC_HthitenCtyDchiMST";
            valueListItem4.DataValue = "ValueListItem1";
            valueListItem4.DisplayText = "Hiển thị tên công ty và địa chỉ";
            valueListItem4.Tag = "optBC_HthitenCtyDchi";
            valueListItem13.DataValue = "ValueListItem2";
            valueListItem13.DisplayText = "Hiển thị tên công ty , địa chỉ  và  đơn vị chủ quản";
            valueListItem13.Tag = "optBC_HthitenCtyDchiCQuan";
            this.optHienThi.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem5,
            valueListItem4,
            valueListItem13});
            this.optHienThi.Location = new System.Drawing.Point(10, 31);
            this.optHienThi.Name = "optHienThi";
            this.optHienThi.Size = new System.Drawing.Size(361, 54);
            this.optHienThi.TabIndex = 6;
            this.optHienThi.Text = "Hiển thị tên công ty , địa chỉ  và mã số thuế";
            // 
            // chkBC_HthiEmail
            // 
            appearance55.BackColor = System.Drawing.Color.Transparent;
            this.chkBC_HthiEmail.Appearance = appearance55;
            this.chkBC_HthiEmail.BackColor = System.Drawing.Color.Transparent;
            this.chkBC_HthiEmail.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkBC_HthiEmail.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkBC_HthiEmail.Location = new System.Drawing.Point(654, 51);
            this.chkBC_HthiEmail.Name = "chkBC_HthiEmail";
            this.chkBC_HthiEmail.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkBC_HthiEmail.Size = new System.Drawing.Size(17, 16);
            this.chkBC_HthiEmail.TabIndex = 5;
            this.chkBC_HthiEmail.Visible = false;
            // 
            // chkBC_HthiFax
            // 
            appearance56.BackColor = System.Drawing.Color.Transparent;
            this.chkBC_HthiFax.Appearance = appearance56;
            this.chkBC_HthiFax.BackColor = System.Drawing.Color.Transparent;
            this.chkBC_HthiFax.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkBC_HthiFax.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkBC_HthiFax.Location = new System.Drawing.Point(529, 51);
            this.chkBC_HthiFax.Name = "chkBC_HthiFax";
            this.chkBC_HthiFax.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkBC_HthiFax.Size = new System.Drawing.Size(17, 16);
            this.chkBC_HthiFax.TabIndex = 5;
            this.chkBC_HthiFax.Visible = false;
            // 
            // chkBC_HthiDThoai
            // 
            appearance57.BackColor = System.Drawing.Color.Transparent;
            this.chkBC_HthiDThoai.Appearance = appearance57;
            this.chkBC_HthiDThoai.BackColor = System.Drawing.Color.Transparent;
            this.chkBC_HthiDThoai.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkBC_HthiDThoai.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkBC_HthiDThoai.Location = new System.Drawing.Point(399, 51);
            this.chkBC_HthiDThoai.Name = "chkBC_HthiDThoai";
            this.chkBC_HthiDThoai.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkBC_HthiDThoai.Size = new System.Drawing.Size(17, 16);
            this.chkBC_HthiDThoai.TabIndex = 5;
            this.chkBC_HthiDThoai.Visible = false;
            // 
            // ultraLabel42
            // 
            appearance58.BackColor = System.Drawing.Color.Transparent;
            appearance58.TextHAlignAsString = "Left";
            appearance58.TextVAlignAsString = "Middle";
            this.ultraLabel42.Appearance = appearance58;
            this.ultraLabel42.AutoSize = true;
            this.ultraLabel42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel42.Location = new System.Drawing.Point(677, 51);
            this.ultraLabel42.Name = "ultraLabel42";
            this.ultraLabel42.Size = new System.Drawing.Size(33, 14);
            this.ultraLabel42.TabIndex = 0;
            this.ultraLabel42.Text = "Email";
            this.ultraLabel42.Visible = false;
            // 
            // ultraLabel41
            // 
            appearance59.BackColor = System.Drawing.Color.Transparent;
            appearance59.TextHAlignAsString = "Left";
            appearance59.TextVAlignAsString = "Middle";
            this.ultraLabel41.Appearance = appearance59;
            this.ultraLabel41.AutoSize = true;
            this.ultraLabel41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel41.Location = new System.Drawing.Point(552, 51);
            this.ultraLabel41.Name = "ultraLabel41";
            this.ultraLabel41.Size = new System.Drawing.Size(23, 14);
            this.ultraLabel41.TabIndex = 0;
            this.ultraLabel41.Text = "Fax";
            this.ultraLabel41.Visible = false;
            // 
            // BC_HthiDThoai
            // 
            appearance60.BackColor = System.Drawing.Color.Transparent;
            appearance60.TextHAlignAsString = "Left";
            appearance60.TextVAlignAsString = "Middle";
            this.BC_HthiDThoai.Appearance = appearance60;
            this.BC_HthiDThoai.AutoSize = true;
            this.BC_HthiDThoai.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BC_HthiDThoai.Location = new System.Drawing.Point(422, 51);
            this.BC_HthiDThoai.Name = "BC_HthiDThoai";
            this.BC_HthiDThoai.Size = new System.Drawing.Size(55, 14);
            this.BC_HthiDThoai.TabIndex = 0;
            this.BC_HthiDThoai.Text = "Điện thoại ";
            this.BC_HthiDThoai.Visible = false;
            // 
            // ultraLabel43
            // 
            appearance61.BackColor = System.Drawing.Color.Transparent;
            appearance61.TextHAlignAsString = "Left";
            appearance61.TextVAlignAsString = "Middle";
            this.ultraLabel43.Appearance = appearance61;
            this.ultraLabel43.AutoSize = true;
            this.ultraLabel43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel43.Location = new System.Drawing.Point(1, 93);
            this.ultraLabel43.Name = "ultraLabel43";
            this.ultraLabel43.Size = new System.Drawing.Size(151, 14);
            this.ultraLabel43.TabIndex = 0;
            this.ultraLabel43.Text = "Vị trí thông tin doanh nghiệp :";
            this.ultraLabel43.Visible = false;
            // 
            // ultraLabel39
            // 
            appearance62.BackColor = System.Drawing.Color.Transparent;
            appearance62.TextHAlignAsString = "Left";
            appearance62.TextVAlignAsString = "Middle";
            this.ultraLabel39.Appearance = appearance62;
            this.ultraLabel39.AutoSize = true;
            this.ultraLabel39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel39.Location = new System.Drawing.Point(399, 29);
            this.ultraLabel39.Name = "ultraLabel39";
            this.ultraLabel39.Size = new System.Drawing.Size(123, 14);
            this.ultraLabel39.TabIndex = 0;
            this.ultraLabel39.Text = "Hiển thị thêm thông tin :";
            this.ultraLabel39.Visible = false;
            // 
            // ultraGroupBox7
            // 
            this.ultraGroupBox7.ContentPadding.Top = 10;
            this.ultraGroupBox7.Controls.Add(this.txtBC_FontTencty);
            this.ultraGroupBox7.Controls.Add(this.txtBC_FontDChi);
            this.ultraGroupBox7.Controls.Add(this.txtBC_FontTenchuquan);
            this.ultraGroupBox7.Controls.Add(this.ultraLabel34);
            this.ultraGroupBox7.Controls.Add(this.ButChonFontTenCongTy);
            this.ultraGroupBox7.Controls.Add(this.ButFontDonViChuQuan);
            this.ultraGroupBox7.Controls.Add(this.ButFontDiaChi);
            this.ultraGroupBox7.Controls.Add(this.ultraLabel36);
            this.ultraGroupBox7.Controls.Add(this.ultraLabel38);
            this.ultraGroupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox7.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox7.Name = "ultraGroupBox7";
            this.ultraGroupBox7.Size = new System.Drawing.Size(1218, 285);
            this.ultraGroupBox7.TabIndex = 16;
            this.ultraGroupBox7.Text = "Thiết lập font chữ báo cáo";
            this.ultraGroupBox7.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtBC_FontTencty
            // 
            appearance64.TextHAlignAsString = "Left";
            this.txtBC_FontTencty.Appearance = appearance64;
            this.txtBC_FontTencty.AutoSize = false;
            this.txtBC_FontTencty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBC_FontTencty.Location = new System.Drawing.Point(156, 38);
            this.txtBC_FontTencty.Name = "txtBC_FontTencty";
            appearance65.TextHAlignAsString = "Center";
            appearance65.TextVAlignAsString = "Middle";
            this.txtBC_FontTencty.NullTextAppearance = appearance65;
            this.txtBC_FontTencty.ReadOnly = true;
            this.txtBC_FontTencty.Size = new System.Drawing.Size(911, 64);
            this.txtBC_FontTencty.TabIndex = 7;
            // 
            // txtBC_FontDChi
            // 
            appearance66.TextHAlignAsString = "Left";
            this.txtBC_FontDChi.Appearance = appearance66;
            this.txtBC_FontDChi.AutoSize = false;
            this.txtBC_FontDChi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBC_FontDChi.Location = new System.Drawing.Point(156, 108);
            this.txtBC_FontDChi.Name = "txtBC_FontDChi";
            this.txtBC_FontDChi.ReadOnly = true;
            this.txtBC_FontDChi.Size = new System.Drawing.Size(911, 64);
            this.txtBC_FontDChi.TabIndex = 7;
            // 
            // txtBC_FontTenchuquan
            // 
            appearance67.TextHAlignAsString = "Left";
            this.txtBC_FontTenchuquan.Appearance = appearance67;
            this.txtBC_FontTenchuquan.AutoSize = false;
            this.txtBC_FontTenchuquan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBC_FontTenchuquan.Location = new System.Drawing.Point(156, 178);
            this.txtBC_FontTenchuquan.Name = "txtBC_FontTenchuquan";
            this.txtBC_FontTenchuquan.ReadOnly = true;
            this.txtBC_FontTenchuquan.Size = new System.Drawing.Size(911, 64);
            this.txtBC_FontTenchuquan.TabIndex = 7;
            // 
            // ultraLabel34
            // 
            appearance68.BackColor = System.Drawing.Color.Transparent;
            appearance68.TextHAlignAsString = "Left";
            appearance68.TextVAlignAsString = "Middle";
            this.ultraLabel34.Appearance = appearance68;
            this.ultraLabel34.AutoSize = true;
            this.ultraLabel34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel34.Location = new System.Drawing.Point(10, 207);
            this.ultraLabel34.Name = "ultraLabel34";
            this.ultraLabel34.Size = new System.Drawing.Size(92, 14);
            this.ultraLabel34.TabIndex = 0;
            this.ultraLabel34.Text = "Đơn vị chủ quản :";
            // 
            // ButChonFontTenCongTy
            // 
            appearance69.Image = ((object)(resources.GetObject("appearance69.Image")));
            this.ButChonFontTenCongTy.Appearance = appearance69;
            this.ButChonFontTenCongTy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButChonFontTenCongTy.Location = new System.Drawing.Point(1094, 55);
            this.ButChonFontTenCongTy.Name = "ButChonFontTenCongTy";
            this.ButChonFontTenCongTy.Size = new System.Drawing.Size(95, 23);
            this.ButChonFontTenCongTy.TabIndex = 12;
            this.ButChonFontTenCongTy.Text = "Chọn ...";
            this.ButChonFontTenCongTy.Click += new System.EventHandler(this.ButChonFontTenCongTy_Click);
            // 
            // ButFontDonViChuQuan
            // 
            appearance70.Image = ((object)(resources.GetObject("appearance70.Image")));
            this.ButFontDonViChuQuan.Appearance = appearance70;
            this.ButFontDonViChuQuan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButFontDonViChuQuan.Location = new System.Drawing.Point(1094, 207);
            this.ButFontDonViChuQuan.Name = "ButFontDonViChuQuan";
            this.ButFontDonViChuQuan.Size = new System.Drawing.Size(95, 23);
            this.ButFontDonViChuQuan.TabIndex = 12;
            this.ButFontDonViChuQuan.Text = "Chọn ...";
            this.ButFontDonViChuQuan.Click += new System.EventHandler(this.ButFontDonViChuQuan_Click);
            // 
            // ButFontDiaChi
            // 
            appearance71.Image = ((object)(resources.GetObject("appearance71.Image")));
            this.ButFontDiaChi.Appearance = appearance71;
            this.ButFontDiaChi.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButFontDiaChi.Location = new System.Drawing.Point(1094, 129);
            this.ButFontDiaChi.Name = "ButFontDiaChi";
            this.ButFontDiaChi.Size = new System.Drawing.Size(95, 23);
            this.ButFontDiaChi.TabIndex = 12;
            this.ButFontDiaChi.Text = "Chọn ...";
            this.ButFontDiaChi.Click += new System.EventHandler(this.ButFontDiaChi_Click);
            // 
            // ultraLabel36
            // 
            appearance72.BackColor = System.Drawing.Color.Transparent;
            appearance72.TextHAlignAsString = "Left";
            appearance72.TextVAlignAsString = "Middle";
            this.ultraLabel36.Appearance = appearance72;
            this.ultraLabel36.AutoSize = true;
            this.ultraLabel36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel36.Location = new System.Drawing.Point(10, 134);
            this.ultraLabel36.Name = "ultraLabel36";
            this.ultraLabel36.Size = new System.Drawing.Size(45, 14);
            this.ultraLabel36.TabIndex = 0;
            this.ultraLabel36.Text = "Địa chỉ :";
            // 
            // ultraLabel38
            // 
            appearance73.BackColor = System.Drawing.Color.Transparent;
            appearance73.TextHAlignAsString = "Left";
            appearance73.TextVAlignAsString = "Middle";
            this.ultraLabel38.Appearance = appearance73;
            this.ultraLabel38.AutoSize = true;
            this.ultraLabel38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel38.Location = new System.Drawing.Point(10, 64);
            this.ultraLabel38.Name = "ultraLabel38";
            this.ultraLabel38.Size = new System.Drawing.Size(69, 14);
            this.ultraLabel38.TabIndex = 0;
            this.ultraLabel38.Text = "Tên công ty :";
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ultraTabPageControl4.Controls.Add(this.ultraGroupBox28);
            this.ultraTabPageControl4.Controls.Add(this.ultraGroupBox14);
            this.ultraTabPageControl4.Controls.Add(this.ultraGroupBox13);
            this.ultraTabPageControl4.Controls.Add(this.ultraGroupBox12);
            this.ultraTabPageControl4.Controls.Add(this.ultraGroupBox11);
            this.ultraTabPageControl4.Controls.Add(this.ultraGroupBox10);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(1, 41);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(1218, 568);
            // 
            // ultraGroupBox28
            // 
            appearance74.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox28.Appearance = appearance74;
            this.ultraGroupBox28.ContentPadding.Top = 10;
            this.ultraGroupBox28.Controls.Add(this.ultraOptionSet1);
            this.ultraGroupBox28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance76.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox28.HeaderAppearance = appearance76;
            this.ultraGroupBox28.Location = new System.Drawing.Point(606, 296);
            this.ultraGroupBox28.Name = "ultraGroupBox28";
            this.ultraGroupBox28.Size = new System.Drawing.Size(610, 80);
            this.ultraGroupBox28.TabIndex = 13;
            this.ultraGroupBox28.Text = "Tùy chọn khi lập chứng từ bán hàng";
            this.ultraGroupBox28.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraOptionSet1
            // 
            appearance75.BackColor = System.Drawing.Color.Transparent;
            appearance75.FontData.BoldAsString = "False";
            this.ultraOptionSet1.Appearance = appearance75;
            this.ultraOptionSet1.BackColor = System.Drawing.Color.Transparent;
            this.ultraOptionSet1.BackColorInternal = System.Drawing.Color.Transparent;
            this.ultraOptionSet1.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraOptionSet1.CheckedIndex = 0;
            valueListItem6.DataValue = "Default Item";
            valueListItem6.DisplayText = "Chứng từ bán hàng kiêm hóa đơn";
            valueListItem6.Tag = "optVTHH_KiemHD";
            valueListItem2.DataValue = "ValueListItem1";
            valueListItem2.DisplayText = "Chứng từ bán hàng không kiêm hóa đơn";
            valueListItem2.Tag = "optVTHH_KoKiemHD";
            this.ultraOptionSet1.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem6,
            valueListItem2});
            this.ultraOptionSet1.Location = new System.Drawing.Point(15, 31);
            this.ultraOptionSet1.Name = "ultraOptionSet1";
            this.ultraOptionSet1.Size = new System.Drawing.Size(346, 35);
            this.ultraOptionSet1.TabIndex = 13;
            this.ultraOptionSet1.Text = "Chứng từ bán hàng kiêm hóa đơn";
            // 
            // ultraGroupBox14
            // 
            appearance77.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox14.Appearance = appearance77;
            this.ultraGroupBox14.ContentPadding.Top = 10;
            this.ultraGroupBox14.Controls.Add(this.chkAllowInputCostUnit);
            this.ultraGroupBox14.Controls.Add(this.chkVTHH_ChoXuatQuaLuongTon);
            this.ultraGroupBox14.Controls.Add(this.chkVTHH_CBaoXuatQuaLuongTon);
            this.ultraGroupBox14.Controls.Add(this.ultraLabel53);
            this.ultraGroupBox14.Controls.Add(this.cbcVTHH_KhoMacDinh);
            this.ultraGroupBox14.Controls.Add(this.ultraLabel50);
            this.ultraGroupBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance96.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox14.HeaderAppearance = appearance96;
            this.ultraGroupBox14.Location = new System.Drawing.Point(0, 376);
            this.ultraGroupBox14.Name = "ultraGroupBox14";
            this.ultraGroupBox14.Size = new System.Drawing.Size(1218, 189);
            this.ultraGroupBox14.TabIndex = 12;
            this.ultraGroupBox14.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // chkAllowInputCostUnit
            // 
            appearance78.BackColor = System.Drawing.Color.Transparent;
            appearance78.FontData.BoldAsString = "False";
            this.chkAllowInputCostUnit.Appearance = appearance78;
            this.chkAllowInputCostUnit.BackColor = System.Drawing.Color.Transparent;
            this.chkAllowInputCostUnit.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkAllowInputCostUnit.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkAllowInputCostUnit.Location = new System.Drawing.Point(19, 70);
            this.chkAllowInputCostUnit.Name = "chkAllowInputCostUnit";
            this.chkAllowInputCostUnit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkAllowInputCostUnit.Size = new System.Drawing.Size(258, 16);
            this.chkAllowInputCostUnit.TabIndex = 13;
            this.chkAllowInputCostUnit.Text = "Cho phép nhập đơn giá vốn ";
            // 
            // chkVTHH_ChoXuatQuaLuongTon
            // 
            appearance79.BackColor = System.Drawing.Color.Transparent;
            appearance79.FontData.BoldAsString = "False";
            this.chkVTHH_ChoXuatQuaLuongTon.Appearance = appearance79;
            this.chkVTHH_ChoXuatQuaLuongTon.BackColor = System.Drawing.Color.Transparent;
            this.chkVTHH_ChoXuatQuaLuongTon.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkVTHH_ChoXuatQuaLuongTon.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkVTHH_ChoXuatQuaLuongTon.Location = new System.Drawing.Point(19, 24);
            this.chkVTHH_ChoXuatQuaLuongTon.Name = "chkVTHH_ChoXuatQuaLuongTon";
            this.chkVTHH_ChoXuatQuaLuongTon.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkVTHH_ChoXuatQuaLuongTon.Size = new System.Drawing.Size(258, 16);
            this.chkVTHH_ChoXuatQuaLuongTon.TabIndex = 11;
            this.chkVTHH_ChoXuatQuaLuongTon.Text = "Cho phép xuất quá số lượng tồn";
            // 
            // chkVTHH_CBaoXuatQuaLuongTon
            // 
            appearance80.BackColor = System.Drawing.Color.Transparent;
            appearance80.FontData.BoldAsString = "False";
            this.chkVTHH_CBaoXuatQuaLuongTon.Appearance = appearance80;
            this.chkVTHH_CBaoXuatQuaLuongTon.BackColor = System.Drawing.Color.Transparent;
            this.chkVTHH_CBaoXuatQuaLuongTon.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkVTHH_CBaoXuatQuaLuongTon.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkVTHH_CBaoXuatQuaLuongTon.Location = new System.Drawing.Point(19, 48);
            this.chkVTHH_CBaoXuatQuaLuongTon.Name = "chkVTHH_CBaoXuatQuaLuongTon";
            this.chkVTHH_CBaoXuatQuaLuongTon.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkVTHH_CBaoXuatQuaLuongTon.Size = new System.Drawing.Size(258, 16);
            this.chkVTHH_CBaoXuatQuaLuongTon.TabIndex = 12;
            this.chkVTHH_CBaoXuatQuaLuongTon.Text = "Cảnh báo khi xuất quá số lượng tồn";
            this.chkVTHH_CBaoXuatQuaLuongTon.Visible = false;
            // 
            // ultraLabel53
            // 
            appearance81.BackColor = System.Drawing.Color.Transparent;
            appearance81.TextHAlignAsString = "Left";
            appearance81.TextVAlignAsString = "Middle";
            this.ultraLabel53.Appearance = appearance81;
            this.ultraLabel53.AutoSize = true;
            this.ultraLabel53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel53.Location = new System.Drawing.Point(42, 98);
            this.ultraLabel53.Name = "ultraLabel53";
            this.ultraLabel53.Size = new System.Drawing.Size(0, 0);
            this.ultraLabel53.TabIndex = 10;
            this.ultraLabel53.Visible = false;
            // 
            // cbcVTHH_KhoMacDinh
            // 
            appearance82.FontData.BoldAsString = "False";
            this.cbcVTHH_KhoMacDinh.Appearance = appearance82;
            appearance83.BackColor = System.Drawing.SystemColors.Window;
            appearance83.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.Appearance = appearance83;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance84.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance84.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance84.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance84.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.GroupByBox.Appearance = appearance84;
            appearance85.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.GroupByBox.BandLabelAppearance = appearance85;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance86.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance86.BackColor2 = System.Drawing.SystemColors.Control;
            appearance86.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance86.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.GroupByBox.PromptAppearance = appearance86;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.MaxColScrollRegions = 1;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.MaxRowScrollRegions = 1;
            appearance87.BackColor = System.Drawing.SystemColors.Window;
            appearance87.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.Override.ActiveCellAppearance = appearance87;
            appearance88.BackColor = System.Drawing.SystemColors.Highlight;
            appearance88.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.Override.ActiveRowAppearance = appearance88;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance89.BackColor = System.Drawing.SystemColors.Window;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.Override.CardAreaAppearance = appearance89;
            appearance90.BorderColor = System.Drawing.Color.Silver;
            appearance90.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.Override.CellAppearance = appearance90;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.Override.CellPadding = 0;
            appearance91.BackColor = System.Drawing.SystemColors.Control;
            appearance91.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance91.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance91.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance91.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.Override.GroupByRowAppearance = appearance91;
            appearance92.TextHAlignAsString = "Left";
            this.cbcVTHH_KhoMacDinh.DisplayLayout.Override.HeaderAppearance = appearance92;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance93.BackColor = System.Drawing.SystemColors.Window;
            appearance93.BorderColor = System.Drawing.Color.Silver;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.Override.RowAppearance = appearance93;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance94.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.Override.TemplateAddRowAppearance = appearance94;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbcVTHH_KhoMacDinh.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbcVTHH_KhoMacDinh.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbcVTHH_KhoMacDinh.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbcVTHH_KhoMacDinh.Location = new System.Drawing.Point(185, 136);
            this.cbcVTHH_KhoMacDinh.Name = "cbcVTHH_KhoMacDinh";
            this.cbcVTHH_KhoMacDinh.Size = new System.Drawing.Size(1014, 24);
            this.cbcVTHH_KhoMacDinh.TabIndex = 8;
            this.cbcVTHH_KhoMacDinh.Visible = false;
            // 
            // ultraLabel50
            // 
            appearance95.BackColor = System.Drawing.Color.Transparent;
            appearance95.TextHAlignAsString = "Left";
            appearance95.TextVAlignAsString = "Middle";
            this.ultraLabel50.Appearance = appearance95;
            this.ultraLabel50.AutoSize = true;
            this.ultraLabel50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel50.Location = new System.Drawing.Point(8, 142);
            this.ultraLabel50.Name = "ultraLabel50";
            this.ultraLabel50.Size = new System.Drawing.Size(79, 14);
            this.ultraLabel50.TabIndex = 7;
            this.ultraLabel50.Text = "Kho mặc định :";
            this.ultraLabel50.Visible = false;
            // 
            // ultraGroupBox13
            // 
            appearance97.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox13.Appearance = appearance97;
            this.ultraGroupBox13.ContentPadding.Top = 10;
            this.ultraGroupBox13.Controls.Add(this.opttuychonkhilaphoadon);
            this.ultraGroupBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance99.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox13.HeaderAppearance = appearance99;
            this.ultraGroupBox13.Location = new System.Drawing.Point(0, 296);
            this.ultraGroupBox13.Name = "ultraGroupBox13";
            this.ultraGroupBox13.Size = new System.Drawing.Size(605, 80);
            this.ultraGroupBox13.TabIndex = 11;
            this.ultraGroupBox13.Text = "Tùy chọn khi lập hóa đơn bán hàng";
            this.ultraGroupBox13.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // opttuychonkhilaphoadon
            // 
            appearance98.BackColor = System.Drawing.Color.Transparent;
            appearance98.FontData.BoldAsString = "False";
            this.opttuychonkhilaphoadon.Appearance = appearance98;
            this.opttuychonkhilaphoadon.BackColor = System.Drawing.Color.Transparent;
            this.opttuychonkhilaphoadon.BackColorInternal = System.Drawing.Color.Transparent;
            this.opttuychonkhilaphoadon.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.opttuychonkhilaphoadon.CheckedIndex = 0;
            valueListItem16.DataValue = "Default Item";
            valueListItem16.DisplayText = "Hóa đơn bán hàng kiêm phiếu xuất kho";
            valueListItem16.Tag = "optVTHH_KiemPXuatKho";
            valueListItem17.DataValue = "ValueListItem1";
            valueListItem17.DisplayText = "Hóa đơn bán hàng không kiêm phiếu xuất kho";
            valueListItem17.Tag = "optVTHH_KoKiemPXuatKho";
            this.opttuychonkhilaphoadon.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem16,
            valueListItem17});
            this.opttuychonkhilaphoadon.Location = new System.Drawing.Point(15, 31);
            this.opttuychonkhilaphoadon.Name = "opttuychonkhilaphoadon";
            this.opttuychonkhilaphoadon.Size = new System.Drawing.Size(346, 35);
            this.opttuychonkhilaphoadon.TabIndex = 10;
            this.opttuychonkhilaphoadon.Text = "Hóa đơn bán hàng kiêm phiếu xuất kho";
            // 
            // ultraGroupBox12
            // 
            appearance100.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox12.Appearance = appearance100;
            this.ultraGroupBox12.ContentPadding.Top = 10;
            this.ultraGroupBox12.Controls.Add(this.ultraGroupBox27);
            this.ultraGroupBox12.Controls.Add(this.optcachtinhgia);
            this.ultraGroupBox12.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance102.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox12.HeaderAppearance = appearance102;
            this.ultraGroupBox12.Location = new System.Drawing.Point(0, 216);
            this.ultraGroupBox12.Name = "ultraGroupBox12";
            this.ultraGroupBox12.Size = new System.Drawing.Size(1218, 80);
            this.ultraGroupBox12.TabIndex = 10;
            this.ultraGroupBox12.Text = "Cách tính giá (Đối với phương pháp bình quân cuối kỳ)";
            this.ultraGroupBox12.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraGroupBox27
            // 
            this.ultraGroupBox27.Location = new System.Drawing.Point(411, 79);
            this.ultraGroupBox27.Name = "ultraGroupBox27";
            this.ultraGroupBox27.Size = new System.Drawing.Size(200, 110);
            this.ultraGroupBox27.TabIndex = 13;
            this.ultraGroupBox27.Text = "ultraGroupBox27";
            // 
            // optcachtinhgia
            // 
            appearance101.BackColor = System.Drawing.Color.Transparent;
            appearance101.FontData.BoldAsString = "False";
            this.optcachtinhgia.Appearance = appearance101;
            this.optcachtinhgia.BackColor = System.Drawing.Color.Transparent;
            this.optcachtinhgia.BackColorInternal = System.Drawing.Color.Transparent;
            this.optcachtinhgia.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.optcachtinhgia.CheckedIndex = 0;
            valueListItem18.DataValue = "Default Item";
            valueListItem18.DisplayText = "Tính riêng cho từng kho";
            valueListItem18.Tag = "optVTHH_TinhgiaTungKho";
            valueListItem19.DataValue = "ValueListItem1";
            valueListItem19.DisplayText = "Không tính theo kho";
            valueListItem19.Tag = "optVTHH_TinhgiaKoTheoKho";
            this.optcachtinhgia.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem18,
            valueListItem19});
            this.optcachtinhgia.Location = new System.Drawing.Point(15, 31);
            this.optcachtinhgia.Name = "optcachtinhgia";
            this.optcachtinhgia.Size = new System.Drawing.Size(346, 35);
            this.optcachtinhgia.TabIndex = 10;
            this.optcachtinhgia.Text = "Tính riêng cho từng kho";
            // 
            // ultraGroupBox11
            // 
            appearance103.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox11.Appearance = appearance103;
            this.ultraGroupBox11.ContentPadding.Top = 10;
            this.ultraGroupBox11.Controls.Add(this.optGia);
            this.ultraGroupBox11.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance105.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox11.HeaderAppearance = appearance105;
            this.ultraGroupBox11.Location = new System.Drawing.Point(0, 136);
            this.ultraGroupBox11.Name = "ultraGroupBox11";
            this.ultraGroupBox11.Size = new System.Drawing.Size(1218, 80);
            this.ultraGroupBox11.TabIndex = 9;
            this.ultraGroupBox11.Text = "Thời điểm tính giá xuất kho (đối với phương pháp nhập trước xuất trước và bình qu" +
    "ân tức thời)";
            this.ultraGroupBox11.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // optGia
            // 
            appearance104.BackColor = System.Drawing.Color.Transparent;
            appearance104.FontData.BoldAsString = "False";
            this.optGia.Appearance = appearance104;
            this.optGia.BackColor = System.Drawing.Color.Transparent;
            this.optGia.BackColorInternal = System.Drawing.Color.Transparent;
            this.optGia.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.optGia.CheckedIndex = 0;
            valueListItem14.DataValue = "Default Item";
            valueListItem14.DisplayText = "Ngay khi lập chứng từ";
            valueListItem14.Tag = "optVTHH_TDTGia_NgayLapCTu";
            valueListItem15.DataValue = "ValueListItem1";
            valueListItem15.DisplayText = "Sau khi sử dụng chức năng tính giá";
            valueListItem15.Tag = "optVTHH_TDTGia_SDChucNang";
            this.optGia.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem14,
            valueListItem15});
            this.optGia.Location = new System.Drawing.Point(15, 31);
            this.optGia.Name = "optGia";
            this.optGia.Size = new System.Drawing.Size(346, 35);
            this.optGia.TabIndex = 10;
            this.optGia.Text = "Ngay khi lập chứng từ";
            // 
            // ultraGroupBox10
            // 
            appearance106.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox10.Appearance = appearance106;
            this.ultraGroupBox10.ContentPadding.Top = 10;
            this.ultraGroupBox10.Controls.Add(this.cbcVTHH_PPTinhGiaXKho);
            this.ultraGroupBox10.Controls.Add(this.ultraPictureBox1);
            this.ultraGroupBox10.Controls.Add(this.ultraLabel48);
            this.ultraGroupBox10.Controls.Add(this.ultraLabel49);
            this.ultraGroupBox10.Controls.Add(this.ultraLabel47);
            this.ultraGroupBox10.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance110.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox10.HeaderAppearance = appearance110;
            this.ultraGroupBox10.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox10.Name = "ultraGroupBox10";
            this.ultraGroupBox10.Size = new System.Drawing.Size(1218, 136);
            this.ultraGroupBox10.TabIndex = 8;
            this.ultraGroupBox10.Text = "Phương pháp";
            this.ultraGroupBox10.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbcVTHH_PPTinhGiaXKho
            // 
            this.cbcVTHH_PPTinhGiaXKho.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.cbcVTHH_PPTinhGiaXKho.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbcVTHH_PPTinhGiaXKho.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbcVTHH_PPTinhGiaXKho.Location = new System.Drawing.Point(185, 24);
            this.cbcVTHH_PPTinhGiaXKho.Name = "cbcVTHH_PPTinhGiaXKho";
            this.cbcVTHH_PPTinhGiaXKho.Size = new System.Drawing.Size(367, 24);
            this.cbcVTHH_PPTinhGiaXKho.TabIndex = 10;
            this.cbcVTHH_PPTinhGiaXKho.TextChanged += new System.EventHandler(this.cbcVTHH_PPTinhGiaXKho_TextChanged_1);
            // 
            // ultraPictureBox1
            // 
            this.ultraPictureBox1.BorderShadowColor = System.Drawing.Color.Empty;
            this.ultraPictureBox1.Image = ((object)(resources.GetObject("ultraPictureBox1.Image")));
            this.ultraPictureBox1.Location = new System.Drawing.Point(10, 61);
            this.ultraPictureBox1.Name = "ultraPictureBox1";
            this.ultraPictureBox1.Size = new System.Drawing.Size(49, 45);
            this.ultraPictureBox1.TabIndex = 9;
            // 
            // ultraLabel48
            // 
            appearance107.BackColor = System.Drawing.Color.Transparent;
            appearance107.ImageBackground = global::Accounting.Properties.Resources.Icojam_Blueberry_Basic_Attention;
            appearance107.TextHAlignAsString = "Left";
            appearance107.TextVAlignAsString = "Middle";
            this.ultraLabel48.Appearance = appearance107;
            this.ultraLabel48.AutoSize = true;
            this.ultraLabel48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel48.Location = new System.Drawing.Point(15, 65);
            this.ultraLabel48.Name = "ultraLabel48";
            this.ultraLabel48.Size = new System.Drawing.Size(0, 0);
            this.ultraLabel48.TabIndex = 7;
            // 
            // ultraLabel49
            // 
            appearance108.BackColor = System.Drawing.Color.White;
            appearance108.TextHAlignAsString = "Left";
            appearance108.TextVAlignAsString = "Middle";
            this.ultraLabel49.Appearance = appearance108;
            this.ultraLabel49.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel49.Location = new System.Drawing.Point(65, 61);
            this.ultraLabel49.Name = "ultraLabel49";
            this.ultraLabel49.Size = new System.Drawing.Size(576, 45);
            this.ultraLabel49.TabIndex = 7;
            this.ultraLabel49.Text = "Lưu ý: Nên chạy chức năng bảo trì dữ liệu sau khi thay đổi phương pháp tính giá x" +
    "uất kho để đảm bảo tính đúng đắn của dữ liệu.";
            // 
            // ultraLabel47
            // 
            appearance109.BackColor = System.Drawing.Color.Transparent;
            appearance109.TextHAlignAsString = "Left";
            appearance109.TextVAlignAsString = "Middle";
            this.ultraLabel47.Appearance = appearance109;
            this.ultraLabel47.AutoSize = true;
            this.ultraLabel47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel47.Location = new System.Drawing.Point(15, 33);
            this.ultraLabel47.Name = "ultraLabel47";
            this.ultraLabel47.Size = new System.Drawing.Size(163, 14);
            this.ultraLabel47.TabIndex = 7;
            this.ultraLabel47.Text = "Phương pháp tính giá xuất kho :";
            // 
            // ultraTabPageControl5
            // 
            this.ultraTabPageControl5.Controls.Add(this.ultraGroupBox18);
            this.ultraTabPageControl5.Controls.Add(this.ultraGroupBox17);
            this.ultraTabPageControl5.Controls.Add(this.ultraGroupBox16);
            this.ultraTabPageControl5.Controls.Add(this.ultraGroupBox15);
            this.ultraTabPageControl5.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl5.Name = "ultraTabPageControl5";
            this.ultraTabPageControl5.Size = new System.Drawing.Size(1218, 568);
            // 
            // ultraGroupBox18
            // 
            appearance111.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox18.Appearance = appearance111;
            this.ultraGroupBox18.ContentPadding.Top = 10;
            this.ultraGroupBox18.Controls.Add(this.chkTL_LamThemCN);
            this.ultraGroupBox18.Controls.Add(this.ultraLabel67);
            this.ultraGroupBox18.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance114.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox18.HeaderAppearance = appearance114;
            this.ultraGroupBox18.Location = new System.Drawing.Point(0, 326);
            this.ultraGroupBox18.Name = "ultraGroupBox18";
            this.ultraGroupBox18.Size = new System.Drawing.Size(1218, 204);
            this.ultraGroupBox18.TabIndex = 16;
            this.ultraGroupBox18.Text = "Chấm công";
            this.ultraGroupBox18.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            this.ultraGroupBox18.Visible = false;
            // 
            // chkTL_LamThemCN
            // 
            appearance112.BackColor = System.Drawing.Color.Transparent;
            this.chkTL_LamThemCN.Appearance = appearance112;
            this.chkTL_LamThemCN.BackColor = System.Drawing.Color.Transparent;
            this.chkTL_LamThemCN.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkTL_LamThemCN.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkTL_LamThemCN.Location = new System.Drawing.Point(19, 38);
            this.chkTL_LamThemCN.Name = "chkTL_LamThemCN";
            this.chkTL_LamThemCN.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkTL_LamThemCN.Size = new System.Drawing.Size(17, 16);
            this.chkTL_LamThemCN.TabIndex = 11;
            // 
            // ultraLabel67
            // 
            appearance113.BackColor = System.Drawing.Color.Transparent;
            appearance113.TextHAlignAsString = "Left";
            appearance113.TextVAlignAsString = "Middle";
            this.ultraLabel67.Appearance = appearance113;
            this.ultraLabel67.AutoSize = true;
            this.ultraLabel67.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel67.Location = new System.Drawing.Point(42, 40);
            this.ultraLabel67.Name = "ultraLabel67";
            this.ultraLabel67.Size = new System.Drawing.Size(150, 14);
            this.ultraLabel67.TabIndex = 10;
            this.ultraLabel67.Text = "Làm thêm thứ 7 và chủ nhật .";
            // 
            // ultraGroupBox17
            // 
            appearance115.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox17.Appearance = appearance115;
            this.ultraGroupBox17.ContentPadding.Top = 10;
            this.ultraGroupBox17.Controls.Add(this.meTL_BHTNLDong);
            this.ultraGroupBox17.Controls.Add(this.meTL_BHYTLDong);
            this.ultraGroupBox17.Controls.Add(this.meTL_BHXHLDong);
            this.ultraGroupBox17.Controls.Add(this.meTL_KPCDCty);
            this.ultraGroupBox17.Controls.Add(this.meTL_BHTNCty);
            this.ultraGroupBox17.Controls.Add(this.meTL_BHYTCty);
            this.ultraGroupBox17.Controls.Add(this.meTL_BHXHCty);
            this.ultraGroupBox17.Controls.Add(this.ultraLabel64);
            this.ultraGroupBox17.Controls.Add(this.ultraLabel62);
            this.ultraGroupBox17.Controls.Add(this.ultraLabel60);
            this.ultraGroupBox17.Controls.Add(this.ultraLabel63);
            this.ultraGroupBox17.Controls.Add(this.ultraLabel108);
            this.ultraGroupBox17.Controls.Add(this.ultraLabel107);
            this.ultraGroupBox17.Controls.Add(this.ultraLabel106);
            this.ultraGroupBox17.Controls.Add(this.ultraLabel105);
            this.ultraGroupBox17.Controls.Add(this.ultraLabel92);
            this.ultraGroupBox17.Controls.Add(this.ultraLabel84);
            this.ultraGroupBox17.Controls.Add(this.ultraLabel40);
            this.ultraGroupBox17.Controls.Add(this.ultraLabel58);
            this.ultraGroupBox17.Controls.Add(this.ultraLabel61);
            this.ultraGroupBox17.Controls.Add(this.ultraLabel57);
            this.ultraGroupBox17.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance137.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox17.HeaderAppearance = appearance137;
            this.ultraGroupBox17.Location = new System.Drawing.Point(0, 139);
            this.ultraGroupBox17.Name = "ultraGroupBox17";
            this.ultraGroupBox17.Size = new System.Drawing.Size(1218, 187);
            this.ultraGroupBox17.TabIndex = 15;
            this.ultraGroupBox17.Text = "Bảo hiểm";
            this.ultraGroupBox17.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            this.ultraGroupBox17.Visible = false;
            // 
            // meTL_BHTNLDong
            // 
            appearance116.FontData.BoldAsString = "False";
            appearance116.TextHAlignAsString = "Right";
            this.meTL_BHTNLDong.Appearance = appearance116;
            this.meTL_BHTNLDong.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.meTL_BHTNLDong.InputMask = "{double:3.2}";
            this.meTL_BHTNLDong.Location = new System.Drawing.Point(754, 74);
            this.meTL_BHTNLDong.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.meTL_BHTNLDong.Name = "meTL_BHTNLDong";
            this.meTL_BHTNLDong.PromptChar = ' ';
            this.meTL_BHTNLDong.Size = new System.Drawing.Size(383, 21);
            this.meTL_BHTNLDong.TabIndex = 8;
            // 
            // meTL_BHYTLDong
            // 
            appearance117.FontData.BoldAsString = "False";
            appearance117.TextHAlignAsString = "Right";
            this.meTL_BHYTLDong.Appearance = appearance117;
            this.meTL_BHYTLDong.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.meTL_BHYTLDong.InputMask = "{double:3.2}";
            this.meTL_BHYTLDong.Location = new System.Drawing.Point(754, 53);
            this.meTL_BHYTLDong.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.meTL_BHYTLDong.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.meTL_BHYTLDong.Name = "meTL_BHYTLDong";
            this.meTL_BHYTLDong.PromptChar = ' ';
            this.meTL_BHYTLDong.Size = new System.Drawing.Size(383, 21);
            this.meTL_BHYTLDong.TabIndex = 8;
            // 
            // meTL_BHXHLDong
            // 
            appearance118.FontData.BoldAsString = "False";
            appearance118.TextHAlignAsString = "Right";
            this.meTL_BHXHLDong.Appearance = appearance118;
            this.meTL_BHXHLDong.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.meTL_BHXHLDong.InputMask = "{double:3.2}";
            this.meTL_BHXHLDong.Location = new System.Drawing.Point(754, 32);
            this.meTL_BHXHLDong.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.meTL_BHXHLDong.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.meTL_BHXHLDong.Name = "meTL_BHXHLDong";
            this.meTL_BHXHLDong.PromptChar = ' ';
            this.meTL_BHXHLDong.Size = new System.Drawing.Size(383, 21);
            this.meTL_BHXHLDong.TabIndex = 8;
            // 
            // meTL_KPCDCty
            // 
            appearance119.FontData.BoldAsString = "False";
            appearance119.TextHAlignAsString = "Right";
            this.meTL_KPCDCty.Appearance = appearance119;
            this.meTL_KPCDCty.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.meTL_KPCDCty.InputMask = "{double:3.2}";
            this.meTL_KPCDCty.Location = new System.Drawing.Point(156, 97);
            this.meTL_KPCDCty.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.meTL_KPCDCty.Name = "meTL_KPCDCty";
            this.meTL_KPCDCty.PromptChar = ' ';
            this.meTL_KPCDCty.Size = new System.Drawing.Size(383, 21);
            this.meTL_KPCDCty.TabIndex = 8;
            // 
            // meTL_BHTNCty
            // 
            appearance120.FontData.BoldAsString = "False";
            appearance120.TextHAlignAsString = "Right";
            this.meTL_BHTNCty.Appearance = appearance120;
            this.meTL_BHTNCty.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.meTL_BHTNCty.InputMask = "{double:3.2}";
            this.meTL_BHTNCty.Location = new System.Drawing.Point(156, 76);
            this.meTL_BHTNCty.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.meTL_BHTNCty.Name = "meTL_BHTNCty";
            this.meTL_BHTNCty.PromptChar = ' ';
            this.meTL_BHTNCty.Size = new System.Drawing.Size(383, 21);
            this.meTL_BHTNCty.TabIndex = 8;
            // 
            // meTL_BHYTCty
            // 
            appearance121.FontData.BoldAsString = "False";
            appearance121.TextHAlignAsString = "Right";
            this.meTL_BHYTCty.Appearance = appearance121;
            this.meTL_BHYTCty.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.meTL_BHYTCty.InputMask = "{double:3.2}";
            this.meTL_BHYTCty.Location = new System.Drawing.Point(156, 55);
            this.meTL_BHYTCty.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.meTL_BHYTCty.Name = "meTL_BHYTCty";
            this.meTL_BHYTCty.PromptChar = ' ';
            this.meTL_BHYTCty.Size = new System.Drawing.Size(383, 21);
            this.meTL_BHYTCty.TabIndex = 8;
            // 
            // meTL_BHXHCty
            // 
            appearance122.FontData.BoldAsString = "False";
            appearance122.TextHAlignAsString = "Right";
            this.meTL_BHXHCty.Appearance = appearance122;
            this.meTL_BHXHCty.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.meTL_BHXHCty.InputMask = "{double:3.2}";
            this.meTL_BHXHCty.Location = new System.Drawing.Point(156, 34);
            this.meTL_BHXHCty.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.meTL_BHXHCty.Name = "meTL_BHXHCty";
            this.meTL_BHXHCty.PromptChar = ' ';
            this.meTL_BHXHCty.Size = new System.Drawing.Size(383, 21);
            this.meTL_BHXHCty.TabIndex = 8;
            // 
            // ultraLabel64
            // 
            appearance123.BackColor = System.Drawing.Color.Transparent;
            appearance123.TextHAlignAsString = "Left";
            appearance123.TextVAlignAsString = "Middle";
            this.ultraLabel64.Appearance = appearance123;
            this.ultraLabel64.AutoSize = true;
            this.ultraLabel64.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel64.Location = new System.Drawing.Point(593, 58);
            this.ultraLabel64.Name = "ultraLabel64";
            this.ultraLabel64.Size = new System.Drawing.Size(146, 14);
            this.ultraLabel64.TabIndex = 3;
            this.ultraLabel64.Text = "BHYT người lao động đóng :";
            // 
            // ultraLabel62
            // 
            appearance124.BackColor = System.Drawing.Color.Transparent;
            appearance124.TextHAlignAsString = "Left";
            appearance124.TextVAlignAsString = "Middle";
            this.ultraLabel62.Appearance = appearance124;
            this.ultraLabel62.AutoSize = true;
            this.ultraLabel62.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel62.Location = new System.Drawing.Point(9, 81);
            this.ultraLabel62.Name = "ultraLabel62";
            this.ultraLabel62.Size = new System.Drawing.Size(109, 14);
            this.ultraLabel62.TabIndex = 3;
            this.ultraLabel62.Text = "BHTN công ty đóng :";
            // 
            // ultraLabel60
            // 
            appearance125.BackColor = System.Drawing.Color.Transparent;
            appearance125.TextHAlignAsString = "Left";
            appearance125.TextVAlignAsString = "Middle";
            this.ultraLabel60.Appearance = appearance125;
            this.ultraLabel60.AutoSize = true;
            this.ultraLabel60.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel60.Location = new System.Drawing.Point(9, 37);
            this.ultraLabel60.Name = "ultraLabel60";
            this.ultraLabel60.Size = new System.Drawing.Size(109, 14);
            this.ultraLabel60.TabIndex = 3;
            this.ultraLabel60.Text = "BHXH công ty đóng :";
            // 
            // ultraLabel63
            // 
            appearance126.BackColor = System.Drawing.Color.Transparent;
            appearance126.TextHAlignAsString = "Left";
            appearance126.TextVAlignAsString = "Middle";
            this.ultraLabel63.Appearance = appearance126;
            this.ultraLabel63.AutoSize = true;
            this.ultraLabel63.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel63.Location = new System.Drawing.Point(593, 79);
            this.ultraLabel63.Name = "ultraLabel63";
            this.ultraLabel63.Size = new System.Drawing.Size(147, 14);
            this.ultraLabel63.TabIndex = 4;
            this.ultraLabel63.Text = "BHTN người lao động đóng :";
            // 
            // ultraLabel108
            // 
            appearance127.BackColor = System.Drawing.Color.Transparent;
            appearance127.TextHAlignAsString = "Left";
            appearance127.TextVAlignAsString = "Middle";
            this.ultraLabel108.Appearance = appearance127;
            this.ultraLabel108.AutoSize = true;
            this.ultraLabel108.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel108.Location = new System.Drawing.Point(1143, 79);
            this.ultraLabel108.Name = "ultraLabel108";
            this.ultraLabel108.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel108.TabIndex = 4;
            this.ultraLabel108.Text = "%";
            // 
            // ultraLabel107
            // 
            appearance128.BackColor = System.Drawing.Color.Transparent;
            appearance128.TextHAlignAsString = "Left";
            appearance128.TextVAlignAsString = "Middle";
            this.ultraLabel107.Appearance = appearance128;
            this.ultraLabel107.AutoSize = true;
            this.ultraLabel107.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel107.Location = new System.Drawing.Point(1143, 58);
            this.ultraLabel107.Name = "ultraLabel107";
            this.ultraLabel107.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel107.TabIndex = 4;
            this.ultraLabel107.Text = "%";
            // 
            // ultraLabel106
            // 
            appearance129.BackColor = System.Drawing.Color.Transparent;
            appearance129.TextHAlignAsString = "Left";
            appearance129.TextVAlignAsString = "Middle";
            this.ultraLabel106.Appearance = appearance129;
            this.ultraLabel106.AutoSize = true;
            this.ultraLabel106.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel106.Location = new System.Drawing.Point(1143, 34);
            this.ultraLabel106.Name = "ultraLabel106";
            this.ultraLabel106.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel106.TabIndex = 4;
            this.ultraLabel106.Text = "%";
            // 
            // ultraLabel105
            // 
            appearance130.BackColor = System.Drawing.Color.Transparent;
            appearance130.TextHAlignAsString = "Left";
            appearance130.TextVAlignAsString = "Middle";
            this.ultraLabel105.Appearance = appearance130;
            this.ultraLabel105.AutoSize = true;
            this.ultraLabel105.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel105.Location = new System.Drawing.Point(545, 106);
            this.ultraLabel105.Name = "ultraLabel105";
            this.ultraLabel105.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel105.TabIndex = 4;
            this.ultraLabel105.Text = "%";
            // 
            // ultraLabel92
            // 
            appearance131.BackColor = System.Drawing.Color.Transparent;
            appearance131.TextHAlignAsString = "Left";
            appearance131.TextVAlignAsString = "Middle";
            this.ultraLabel92.Appearance = appearance131;
            this.ultraLabel92.AutoSize = true;
            this.ultraLabel92.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel92.Location = new System.Drawing.Point(545, 83);
            this.ultraLabel92.Name = "ultraLabel92";
            this.ultraLabel92.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel92.TabIndex = 4;
            this.ultraLabel92.Text = "%";
            // 
            // ultraLabel84
            // 
            appearance132.BackColor = System.Drawing.Color.Transparent;
            appearance132.TextHAlignAsString = "Left";
            appearance132.TextVAlignAsString = "Middle";
            this.ultraLabel84.Appearance = appearance132;
            this.ultraLabel84.AutoSize = true;
            this.ultraLabel84.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel84.Location = new System.Drawing.Point(545, 60);
            this.ultraLabel84.Name = "ultraLabel84";
            this.ultraLabel84.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel84.TabIndex = 4;
            this.ultraLabel84.Text = "%";
            // 
            // ultraLabel40
            // 
            appearance133.BackColor = System.Drawing.Color.Transparent;
            appearance133.TextHAlignAsString = "Left";
            appearance133.TextVAlignAsString = "Middle";
            this.ultraLabel40.Appearance = appearance133;
            this.ultraLabel40.AutoSize = true;
            this.ultraLabel40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel40.Location = new System.Drawing.Point(545, 34);
            this.ultraLabel40.Name = "ultraLabel40";
            this.ultraLabel40.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel40.TabIndex = 4;
            this.ultraLabel40.Text = "%";
            // 
            // ultraLabel58
            // 
            appearance134.BackColor = System.Drawing.Color.Transparent;
            appearance134.TextHAlignAsString = "Left";
            appearance134.TextVAlignAsString = "Middle";
            this.ultraLabel58.Appearance = appearance134;
            this.ultraLabel58.AutoSize = true;
            this.ultraLabel58.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel58.Location = new System.Drawing.Point(593, 37);
            this.ultraLabel58.Name = "ultraLabel58";
            this.ultraLabel58.Size = new System.Drawing.Size(147, 14);
            this.ultraLabel58.TabIndex = 4;
            this.ultraLabel58.Text = "BHXH người lao động đóng :";
            // 
            // ultraLabel61
            // 
            appearance135.BackColor = System.Drawing.Color.Transparent;
            appearance135.TextHAlignAsString = "Left";
            appearance135.TextVAlignAsString = "Middle";
            this.ultraLabel61.Appearance = appearance135;
            this.ultraLabel61.AutoSize = true;
            this.ultraLabel61.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel61.Location = new System.Drawing.Point(9, 102);
            this.ultraLabel61.Name = "ultraLabel61";
            this.ultraLabel61.Size = new System.Drawing.Size(109, 14);
            this.ultraLabel61.TabIndex = 4;
            this.ultraLabel61.Text = "KPCD công ty đóng :";
            // 
            // ultraLabel57
            // 
            appearance136.BackColor = System.Drawing.Color.Transparent;
            appearance136.TextHAlignAsString = "Left";
            appearance136.TextVAlignAsString = "Middle";
            this.ultraLabel57.Appearance = appearance136;
            this.ultraLabel57.AutoSize = true;
            this.ultraLabel57.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel57.Location = new System.Drawing.Point(9, 60);
            this.ultraLabel57.Name = "ultraLabel57";
            this.ultraLabel57.Size = new System.Drawing.Size(108, 14);
            this.ultraLabel57.TabIndex = 4;
            this.ultraLabel57.Text = "BHYT công ty đóng :";
            // 
            // ultraGroupBox16
            // 
            appearance138.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox16.Appearance = appearance138;
            this.ultraGroupBox16.ContentPadding.Top = 10;
            this.ultraGroupBox16.Controls.Add(this.chkTL_LayTUlenBLuong);
            this.ultraGroupBox16.Controls.Add(this.ultraLabel59);
            this.ultraGroupBox16.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance141.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox16.HeaderAppearance = appearance141;
            this.ultraGroupBox16.Location = new System.Drawing.Point(0, 70);
            this.ultraGroupBox16.Name = "ultraGroupBox16";
            this.ultraGroupBox16.Size = new System.Drawing.Size(1218, 69);
            this.ultraGroupBox16.TabIndex = 14;
            this.ultraGroupBox16.Text = "Bảng lương";
            this.ultraGroupBox16.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            this.ultraGroupBox16.Visible = false;
            // 
            // chkTL_LayTUlenBLuong
            // 
            appearance139.BackColor = System.Drawing.Color.Transparent;
            this.chkTL_LayTUlenBLuong.Appearance = appearance139;
            this.chkTL_LayTUlenBLuong.BackColor = System.Drawing.Color.Transparent;
            this.chkTL_LayTUlenBLuong.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkTL_LayTUlenBLuong.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkTL_LayTUlenBLuong.Location = new System.Drawing.Point(19, 29);
            this.chkTL_LayTUlenBLuong.Name = "chkTL_LayTUlenBLuong";
            this.chkTL_LayTUlenBLuong.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkTL_LayTUlenBLuong.Size = new System.Drawing.Size(17, 16);
            this.chkTL_LayTUlenBLuong.TabIndex = 11;
            // 
            // ultraLabel59
            // 
            appearance140.BackColor = System.Drawing.Color.Transparent;
            appearance140.TextHAlignAsString = "Left";
            appearance140.TextVAlignAsString = "Middle";
            this.ultraLabel59.Appearance = appearance140;
            this.ultraLabel59.AutoSize = true;
            this.ultraLabel59.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel59.Location = new System.Drawing.Point(42, 31);
            this.ultraLabel59.Name = "ultraLabel59";
            this.ultraLabel59.Size = new System.Drawing.Size(225, 14);
            this.ultraLabel59.TabIndex = 10;
            this.ultraLabel59.Text = "Lấy tạm ứng tài khoản 141 trên bảng lương .";
            // 
            // ultraGroupBox15
            // 
            appearance142.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox15.Appearance = appearance142;
            this.ultraGroupBox15.ContentPadding.Top = 10;
            this.ultraGroupBox15.Controls.Add(this.optLoaiDT);
            this.ultraGroupBox15.Controls.Add(this.ultraLabel54);
            this.ultraGroupBox15.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance145.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox15.HeaderAppearance = appearance145;
            this.ultraGroupBox15.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox15.Name = "ultraGroupBox15";
            this.ultraGroupBox15.Size = new System.Drawing.Size(1218, 70);
            this.ultraGroupBox15.TabIndex = 13;
            this.ultraGroupBox15.Text = "Tiền lương";
            this.ultraGroupBox15.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // optLoaiDT
            // 
            appearance143.BackColor = System.Drawing.Color.Transparent;
            appearance143.FontData.BoldAsString = "False";
            this.optLoaiDT.Appearance = appearance143;
            this.optLoaiDT.BackColor = System.Drawing.Color.Transparent;
            this.optLoaiDT.BackColorInternal = System.Drawing.Color.Transparent;
            this.optLoaiDT.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.optLoaiDT.CheckedIndex = 0;
            valueListItem9.DataValue = "1";
            valueListItem9.DisplayText = "Hệ số lương và lương cơ bản";
            valueListItem9.Tag = "optTL_TheoHeSoLuong";
            valueListItem12.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem12.DataValue = "1";
            valueListItem12.DisplayText = "Lương thỏa thuận";
            valueListItem12.Tag = "optTL_TheoLuongThoaThuan";
            this.optLoaiDT.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem9,
            valueListItem12});
            this.optLoaiDT.Location = new System.Drawing.Point(141, 30);
            this.optLoaiDT.Name = "optLoaiDT";
            this.optLoaiDT.Size = new System.Drawing.Size(215, 35);
            this.optLoaiDT.TabIndex = 11;
            this.optLoaiDT.Text = "Hệ số lương và lương cơ bản";
            // 
            // ultraLabel54
            // 
            appearance144.BackColor = System.Drawing.Color.Transparent;
            appearance144.TextHAlignAsString = "Left";
            appearance144.TextVAlignAsString = "Middle";
            this.ultraLabel54.Appearance = appearance144;
            this.ultraLabel54.AutoSize = true;
            this.ultraLabel54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel54.Location = new System.Drawing.Point(8, 31);
            this.ultraLabel54.Name = "ultraLabel54";
            this.ultraLabel54.Size = new System.Drawing.Size(105, 14);
            this.ultraLabel54.TabIndex = 3;
            this.ultraLabel54.Text = "Tính lương dựa trên:";
            // 
            // ultraTabPageControl6
            // 
            this.ultraTabPageControl6.Controls.Add(this.ultraGroupBox21);
            this.ultraTabPageControl6.Controls.Add(this.ultraGroupBox20);
            this.ultraTabPageControl6.Controls.Add(this.ultraGroupBox19);
            this.ultraTabPageControl6.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl6.Name = "ultraTabPageControl6";
            this.ultraTabPageControl6.Size = new System.Drawing.Size(1218, 568);
            // 
            // ultraGroupBox21
            // 
            appearance146.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox21.Appearance = appearance146;
            this.ultraGroupBox21.ContentPadding.Top = 10;
            this.ultraGroupBox21.Controls.Add(this.ButDDS_MacDinh);
            this.ultraGroupBox21.Controls.Add(this.txtDDSo_NCachHangNghin);
            this.ultraGroupBox21.Controls.Add(this.txtDDSo_NCachHangDVi);
            this.ultraGroupBox21.Controls.Add(this.ultraLabel82);
            this.ultraGroupBox21.Controls.Add(this.ultraLabel85);
            this.ultraGroupBox21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance152.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox21.HeaderAppearance = appearance152;
            this.ultraGroupBox21.Location = new System.Drawing.Point(0, 295);
            this.ultraGroupBox21.Name = "ultraGroupBox21";
            this.ultraGroupBox21.Size = new System.Drawing.Size(1218, 273);
            this.ultraGroupBox21.TabIndex = 19;
            this.ultraGroupBox21.Text = "Dấu ngăn cách";
            this.ultraGroupBox21.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ButDDS_MacDinh
            // 
            appearance147.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.ButDDS_MacDinh.Appearance = appearance147;
            this.ButDDS_MacDinh.Location = new System.Drawing.Point(10, 112);
            this.ButDDS_MacDinh.Name = "ButDDS_MacDinh";
            this.ButDDS_MacDinh.Size = new System.Drawing.Size(122, 30);
            this.ButDDS_MacDinh.TabIndex = 19;
            this.ButDDS_MacDinh.Text = "Mặc định";
            this.ButDDS_MacDinh.Click += new System.EventHandler(this.ButDDS_MacDinh_Click);
            // 
            // txtDDSo_NCachHangNghin
            // 
            appearance148.TextHAlignAsString = "Center";
            this.txtDDSo_NCachHangNghin.Appearance = appearance148;
            this.txtDDSo_NCachHangNghin.AutoSize = false;
            this.txtDDSo_NCachHangNghin.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDDSo_NCachHangNghin.Location = new System.Drawing.Point(158, 25);
            this.txtDDSo_NCachHangNghin.MaxLength = 1;
            this.txtDDSo_NCachHangNghin.Multiline = true;
            this.txtDDSo_NCachHangNghin.Name = "txtDDSo_NCachHangNghin";
            this.txtDDSo_NCachHangNghin.Size = new System.Drawing.Size(64, 24);
            this.txtDDSo_NCachHangNghin.TabIndex = 14;
            this.txtDDSo_NCachHangNghin.Text = ".";
            // 
            // txtDDSo_NCachHangDVi
            // 
            appearance149.TextHAlignAsString = "Center";
            this.txtDDSo_NCachHangDVi.Appearance = appearance149;
            this.txtDDSo_NCachHangDVi.AutoSize = false;
            this.txtDDSo_NCachHangDVi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDDSo_NCachHangDVi.Location = new System.Drawing.Point(158, 54);
            this.txtDDSo_NCachHangDVi.MaxLength = 1;
            this.txtDDSo_NCachHangDVi.Multiline = true;
            this.txtDDSo_NCachHangDVi.Name = "txtDDSo_NCachHangDVi";
            this.txtDDSo_NCachHangDVi.Size = new System.Drawing.Size(64, 26);
            this.txtDDSo_NCachHangDVi.TabIndex = 18;
            this.txtDDSo_NCachHangDVi.Text = ",";
            // 
            // ultraLabel82
            // 
            appearance150.BackColor = System.Drawing.Color.Transparent;
            appearance150.TextHAlignAsString = "Left";
            appearance150.TextVAlignAsString = "Middle";
            this.ultraLabel82.Appearance = appearance150;
            this.ultraLabel82.AutoSize = true;
            this.ultraLabel82.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel82.Location = new System.Drawing.Point(10, 31);
            this.ultraLabel82.Name = "ultraLabel82";
            this.ultraLabel82.Size = new System.Drawing.Size(123, 14);
            this.ultraLabel82.TabIndex = 9;
            this.ultraLabel82.Text = "Ngăn cách hàng nghìn :";
            // 
            // ultraLabel85
            // 
            appearance151.BackColor = System.Drawing.Color.Transparent;
            appearance151.TextHAlignAsString = "Left";
            appearance151.TextVAlignAsString = "Middle";
            this.ultraLabel85.Appearance = appearance151;
            this.ultraLabel85.AutoSize = true;
            this.ultraLabel85.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel85.Location = new System.Drawing.Point(11, 61);
            this.ultraLabel85.Name = "ultraLabel85";
            this.ultraLabel85.Size = new System.Drawing.Size(126, 14);
            this.ultraLabel85.TabIndex = 8;
            this.ultraLabel85.Text = "Ngăn cách hàng đơn vị :";
            // 
            // ultraGroupBox20
            // 
            appearance153.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox20.Appearance = appearance153;
            this.ultraGroupBox20.ContentPadding.Top = 10;
            this.ultraGroupBox20.Controls.Add(this.cbcDDSo_SoAm);
            this.ultraGroupBox20.Controls.Add(this.cbcDDSo_KyHieuTien);
            this.ultraGroupBox20.Controls.Add(this.ultraLabel81);
            this.ultraGroupBox20.Controls.Add(this.H);
            this.ultraGroupBox20.Controls.Add(this.ultraLabel83);
            this.ultraGroupBox20.Controls.Add(this.lblHienThiSoAm);
            this.ultraGroupBox20.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance184.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox20.HeaderAppearance = appearance184;
            this.ultraGroupBox20.Location = new System.Drawing.Point(0, 227);
            this.ultraGroupBox20.Name = "ultraGroupBox20";
            this.ultraGroupBox20.Size = new System.Drawing.Size(1218, 68);
            this.ultraGroupBox20.TabIndex = 18;
            this.ultraGroupBox20.Text = "Định dạng tiền âm";
            this.ultraGroupBox20.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbcDDSo_SoAm
            // 
            appearance154.FontData.BoldAsString = "False";
            this.cbcDDSo_SoAm.Appearance = appearance154;
            appearance155.BackColor = System.Drawing.SystemColors.Window;
            appearance155.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbcDDSo_SoAm.DisplayLayout.Appearance = appearance155;
            this.cbcDDSo_SoAm.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbcDDSo_SoAm.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance156.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance156.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance156.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance156.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcDDSo_SoAm.DisplayLayout.GroupByBox.Appearance = appearance156;
            appearance157.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcDDSo_SoAm.DisplayLayout.GroupByBox.BandLabelAppearance = appearance157;
            this.cbcDDSo_SoAm.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance158.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance158.BackColor2 = System.Drawing.SystemColors.Control;
            appearance158.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance158.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcDDSo_SoAm.DisplayLayout.GroupByBox.PromptAppearance = appearance158;
            this.cbcDDSo_SoAm.DisplayLayout.MaxColScrollRegions = 1;
            this.cbcDDSo_SoAm.DisplayLayout.MaxRowScrollRegions = 1;
            appearance159.BackColor = System.Drawing.SystemColors.Window;
            appearance159.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbcDDSo_SoAm.DisplayLayout.Override.ActiveCellAppearance = appearance159;
            appearance160.BackColor = System.Drawing.SystemColors.Highlight;
            appearance160.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbcDDSo_SoAm.DisplayLayout.Override.ActiveRowAppearance = appearance160;
            this.cbcDDSo_SoAm.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbcDDSo_SoAm.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance161.BackColor = System.Drawing.SystemColors.Window;
            this.cbcDDSo_SoAm.DisplayLayout.Override.CardAreaAppearance = appearance161;
            appearance162.BorderColor = System.Drawing.Color.Silver;
            appearance162.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbcDDSo_SoAm.DisplayLayout.Override.CellAppearance = appearance162;
            this.cbcDDSo_SoAm.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbcDDSo_SoAm.DisplayLayout.Override.CellPadding = 0;
            appearance163.BackColor = System.Drawing.SystemColors.Control;
            appearance163.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance163.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance163.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance163.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcDDSo_SoAm.DisplayLayout.Override.GroupByRowAppearance = appearance163;
            appearance164.TextHAlignAsString = "Left";
            this.cbcDDSo_SoAm.DisplayLayout.Override.HeaderAppearance = appearance164;
            this.cbcDDSo_SoAm.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbcDDSo_SoAm.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance165.BackColor = System.Drawing.SystemColors.Window;
            appearance165.BorderColor = System.Drawing.Color.Silver;
            this.cbcDDSo_SoAm.DisplayLayout.Override.RowAppearance = appearance165;
            this.cbcDDSo_SoAm.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance166.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbcDDSo_SoAm.DisplayLayout.Override.TemplateAddRowAppearance = appearance166;
            this.cbcDDSo_SoAm.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbcDDSo_SoAm.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbcDDSo_SoAm.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbcDDSo_SoAm.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbcDDSo_SoAm.Location = new System.Drawing.Point(158, 36);
            this.cbcDDSo_SoAm.Name = "cbcDDSo_SoAm";
            this.cbcDDSo_SoAm.Size = new System.Drawing.Size(443, 24);
            this.cbcDDSo_SoAm.TabIndex = 10;
            // 
            // cbcDDSo_KyHieuTien
            // 
            appearance167.FontData.BoldAsString = "False";
            this.cbcDDSo_KyHieuTien.Appearance = appearance167;
            appearance168.BackColor = System.Drawing.SystemColors.Window;
            appearance168.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbcDDSo_KyHieuTien.DisplayLayout.Appearance = appearance168;
            this.cbcDDSo_KyHieuTien.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.cbcDDSo_KyHieuTien.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbcDDSo_KyHieuTien.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance169.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance169.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance169.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance169.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcDDSo_KyHieuTien.DisplayLayout.GroupByBox.Appearance = appearance169;
            appearance170.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcDDSo_KyHieuTien.DisplayLayout.GroupByBox.BandLabelAppearance = appearance170;
            this.cbcDDSo_KyHieuTien.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance171.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance171.BackColor2 = System.Drawing.SystemColors.Control;
            appearance171.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance171.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcDDSo_KyHieuTien.DisplayLayout.GroupByBox.PromptAppearance = appearance171;
            this.cbcDDSo_KyHieuTien.DisplayLayout.MaxColScrollRegions = 1;
            this.cbcDDSo_KyHieuTien.DisplayLayout.MaxRowScrollRegions = 1;
            appearance172.BackColor = System.Drawing.SystemColors.Window;
            appearance172.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbcDDSo_KyHieuTien.DisplayLayout.Override.ActiveCellAppearance = appearance172;
            appearance173.BackColor = System.Drawing.SystemColors.Highlight;
            appearance173.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbcDDSo_KyHieuTien.DisplayLayout.Override.ActiveRowAppearance = appearance173;
            this.cbcDDSo_KyHieuTien.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbcDDSo_KyHieuTien.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance174.BackColor = System.Drawing.SystemColors.Window;
            this.cbcDDSo_KyHieuTien.DisplayLayout.Override.CardAreaAppearance = appearance174;
            appearance175.BorderColor = System.Drawing.Color.Silver;
            appearance175.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbcDDSo_KyHieuTien.DisplayLayout.Override.CellAppearance = appearance175;
            this.cbcDDSo_KyHieuTien.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbcDDSo_KyHieuTien.DisplayLayout.Override.CellPadding = 0;
            appearance176.BackColor = System.Drawing.SystemColors.Control;
            appearance176.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance176.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance176.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance176.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcDDSo_KyHieuTien.DisplayLayout.Override.GroupByRowAppearance = appearance176;
            appearance177.TextHAlignAsString = "Left";
            this.cbcDDSo_KyHieuTien.DisplayLayout.Override.HeaderAppearance = appearance177;
            this.cbcDDSo_KyHieuTien.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbcDDSo_KyHieuTien.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance178.BackColor = System.Drawing.SystemColors.Window;
            appearance178.BorderColor = System.Drawing.Color.Silver;
            this.cbcDDSo_KyHieuTien.DisplayLayout.Override.RowAppearance = appearance178;
            this.cbcDDSo_KyHieuTien.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance179.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbcDDSo_KyHieuTien.DisplayLayout.Override.TemplateAddRowAppearance = appearance179;
            this.cbcDDSo_KyHieuTien.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbcDDSo_KyHieuTien.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbcDDSo_KyHieuTien.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbcDDSo_KyHieuTien.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbcDDSo_KyHieuTien.Location = new System.Drawing.Point(158, 12);
            this.cbcDDSo_KyHieuTien.Name = "cbcDDSo_KyHieuTien";
            this.cbcDDSo_KyHieuTien.Size = new System.Drawing.Size(443, 24);
            this.cbcDDSo_KyHieuTien.TabIndex = 10;
            this.cbcDDSo_KyHieuTien.Visible = false;
            // 
            // ultraLabel81
            // 
            appearance180.BackColor = System.Drawing.Color.Transparent;
            appearance180.TextHAlignAsString = "Left";
            appearance180.TextVAlignAsString = "Middle";
            this.ultraLabel81.Appearance = appearance180;
            this.ultraLabel81.AutoSize = true;
            this.ultraLabel81.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel81.Location = new System.Drawing.Point(10, 19);
            this.ultraLabel81.Name = "ultraLabel81";
            this.ultraLabel81.Size = new System.Drawing.Size(82, 14);
            this.ultraLabel81.TabIndex = 9;
            this.ultraLabel81.Text = "Ký hiệu tiền tệ :";
            this.ultraLabel81.Visible = false;
            // 
            // H
            // 
            appearance181.BackColor = System.Drawing.Color.Transparent;
            appearance181.TextHAlignAsString = "Left";
            appearance181.TextVAlignAsString = "Middle";
            this.H.Appearance = appearance181;
            this.H.AutoSize = true;
            this.H.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.H.Location = new System.Drawing.Point(10, 39);
            this.H.Name = "H";
            this.H.Size = new System.Drawing.Size(82, 14);
            this.H.TabIndex = 8;
            this.H.Text = "Hiển thị số âm :";
            // 
            // ultraLabel83
            // 
            appearance182.BackColor = System.Drawing.Color.Transparent;
            appearance182.TextHAlignAsString = "Left";
            appearance182.TextVAlignAsString = "Middle";
            this.ultraLabel83.Appearance = appearance182;
            this.ultraLabel83.AutoSize = true;
            this.ultraLabel83.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.ultraLabel83.Location = new System.Drawing.Point(677, 42);
            this.ultraLabel83.Name = "ultraLabel83";
            this.ultraLabel83.Size = new System.Drawing.Size(37, 14);
            this.ultraLabel83.TabIndex = 8;
            this.ultraLabel83.Text = "Ví dụ :";
            // 
            // lblHienThiSoAm
            // 
            appearance183.BackColor = System.Drawing.Color.Transparent;
            appearance183.TextHAlignAsString = "Left";
            appearance183.TextVAlignAsString = "Middle";
            this.lblHienThiSoAm.Appearance = appearance183;
            this.lblHienThiSoAm.AutoSize = true;
            this.lblHienThiSoAm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblHienThiSoAm.Location = new System.Drawing.Point(734, 42);
            this.lblHienThiSoAm.Name = "lblHienThiSoAm";
            this.lblHienThiSoAm.Size = new System.Drawing.Size(0, 0);
            this.lblHienThiSoAm.TabIndex = 8;
            // 
            // ultraGroupBox19
            // 
            appearance185.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox19.Appearance = appearance185;
            this.ultraGroupBox19.ContentPadding.Top = 10;
            this.ultraGroupBox19.Controls.Add(this.numDDSo_DonGiaNT);
            this.ultraGroupBox19.Controls.Add(this.lblDonGiaNT);
            this.ultraGroupBox19.Controls.Add(this.ultraLabel99);
            this.ultraGroupBox19.Controls.Add(this.ultraLabel101);
            this.ultraGroupBox19.Controls.Add(this.ButDDS_XemThu);
            this.ultraGroupBox19.Controls.Add(this.numDDSo_TyLePBo);
            this.ultraGroupBox19.Controls.Add(this.numDDSo_TyLe);
            this.ultraGroupBox19.Controls.Add(this.numDDSo_TyGia);
            this.ultraGroupBox19.Controls.Add(this.numDDSo_SoLuong);
            this.ultraGroupBox19.Controls.Add(this.numDDSo_DonGia);
            this.ultraGroupBox19.Controls.Add(this.numDDSo_NgoaiTe);
            this.ultraGroupBox19.Controls.Add(this.numDDSo_TienVND);
            this.ultraGroupBox19.Controls.Add(this.lblTienVietNam);
            this.ultraGroupBox19.Controls.Add(this.lblSoLuong);
            this.ultraGroupBox19.Controls.Add(this.lblTienNgoaiTe);
            this.ultraGroupBox19.Controls.Add(this.ultraLabel80);
            this.ultraGroupBox19.Controls.Add(this.ultraLabel65);
            this.ultraGroupBox19.Controls.Add(this.ultraLabel79);
            this.ultraGroupBox19.Controls.Add(this.lblTyGia);
            this.ultraGroupBox19.Controls.Add(this.ultraLabel66);
            this.ultraGroupBox19.Controls.Add(this.ultraLabel78);
            this.ultraGroupBox19.Controls.Add(this.lblDonGia);
            this.ultraGroupBox19.Controls.Add(this.ultraLabel77);
            this.ultraGroupBox19.Controls.Add(this.ultraLabel68);
            this.ultraGroupBox19.Controls.Add(this.ultraLabel69);
            this.ultraGroupBox19.Controls.Add(this.ultraLabel76);
            this.ultraGroupBox19.Controls.Add(this.lblTyLePhanBo);
            this.ultraGroupBox19.Controls.Add(this.ultraLabel70);
            this.ultraGroupBox19.Controls.Add(this.lblHeSoTyLe);
            this.ultraGroupBox19.Controls.Add(this.ultraLabel75);
            this.ultraGroupBox19.Controls.Add(this.ultraLabel73);
            this.ultraGroupBox19.Controls.Add(this.ultraLabel74);
            this.ultraGroupBox19.Controls.Add(this.ultraLabel71);
            this.ultraGroupBox19.Controls.Add(this.ultraLabel72);
            this.ultraGroupBox19.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance220.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox19.HeaderAppearance = appearance220;
            this.ultraGroupBox19.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox19.Name = "ultraGroupBox19";
            this.ultraGroupBox19.Size = new System.Drawing.Size(1218, 227);
            this.ultraGroupBox19.TabIndex = 17;
            this.ultraGroupBox19.Text = "Định dạng số sau dấu phẩy";
            this.ultraGroupBox19.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // numDDSo_DonGiaNT
            // 
            appearance186.FontData.BoldAsString = "False";
            this.numDDSo_DonGiaNT.Appearance = appearance186;
            this.numDDSo_DonGiaNT.Location = new System.Drawing.Point(158, 100);
            this.numDDSo_DonGiaNT.MaskInput = "nnn";
            this.numDDSo_DonGiaNT.MaxValue = 4;
            this.numDDSo_DonGiaNT.MinValue = 0;
            this.numDDSo_DonGiaNT.Name = "numDDSo_DonGiaNT";
            this.numDDSo_DonGiaNT.PromptChar = ' ';
            this.numDDSo_DonGiaNT.Size = new System.Drawing.Size(443, 23);
            this.numDDSo_DonGiaNT.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.numDDSo_DonGiaNT.TabIndex = 24;
            // 
            // lblDonGiaNT
            // 
            appearance187.BackColor = System.Drawing.Color.Transparent;
            appearance187.TextHAlignAsString = "Left";
            appearance187.TextVAlignAsString = "Middle";
            this.lblDonGiaNT.Appearance = appearance187;
            this.lblDonGiaNT.AutoSize = true;
            this.lblDonGiaNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDonGiaNT.Location = new System.Drawing.Point(910, 104);
            this.lblDonGiaNT.Name = "lblDonGiaNT";
            this.lblDonGiaNT.Size = new System.Drawing.Size(73, 14);
            this.lblDonGiaNT.TabIndex = 23;
            this.lblDonGiaNT.Text = "1.234.567,89";
            // 
            // ultraLabel99
            // 
            appearance188.BackColor = System.Drawing.Color.Transparent;
            appearance188.TextHAlignAsString = "Left";
            appearance188.TextVAlignAsString = "Middle";
            this.ultraLabel99.Appearance = appearance188;
            this.ultraLabel99.AutoSize = true;
            this.ultraLabel99.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel99.Location = new System.Drawing.Point(794, 104);
            this.ultraLabel99.Name = "ultraLabel99";
            this.ultraLabel99.Size = new System.Drawing.Size(93, 14);
            this.ultraLabel99.TabIndex = 21;
            this.ultraLabel99.Text = "Đơn giá ngoại tệ :";
            // 
            // ultraLabel101
            // 
            appearance189.BackColor = System.Drawing.Color.Transparent;
            appearance189.TextHAlignAsString = "Left";
            appearance189.TextVAlignAsString = "Middle";
            this.ultraLabel101.Appearance = appearance189;
            this.ultraLabel101.AutoSize = true;
            this.ultraLabel101.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel101.Location = new System.Drawing.Point(10, 104);
            this.ultraLabel101.Name = "ultraLabel101";
            this.ultraLabel101.Size = new System.Drawing.Size(93, 14);
            this.ultraLabel101.TabIndex = 22;
            this.ultraLabel101.Text = "Đơn giá ngoại tệ :";
            // 
            // ButDDS_XemThu
            // 
            appearance190.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.ButDDS_XemThu.Appearance = appearance190;
            this.ButDDS_XemThu.Location = new System.Drawing.Point(646, 100);
            this.ButDDS_XemThu.Name = "ButDDS_XemThu";
            this.ButDDS_XemThu.Size = new System.Drawing.Size(96, 29);
            this.ButDDS_XemThu.TabIndex = 19;
            this.ButDDS_XemThu.Text = "Xem thử";
            this.ButDDS_XemThu.Click += new System.EventHandler(this.ButDDS_XemThu_Click);
            // 
            // numDDSo_TyLePBo
            // 
            appearance191.FontData.BoldAsString = "False";
            this.numDDSo_TyLePBo.Appearance = appearance191;
            this.numDDSo_TyLePBo.Location = new System.Drawing.Point(158, 192);
            this.numDDSo_TyLePBo.MaskInput = "nnn";
            this.numDDSo_TyLePBo.MaxValue = 10;
            this.numDDSo_TyLePBo.MinValue = 0;
            this.numDDSo_TyLePBo.Name = "numDDSo_TyLePBo";
            this.numDDSo_TyLePBo.PromptChar = ' ';
            this.numDDSo_TyLePBo.Size = new System.Drawing.Size(443, 23);
            this.numDDSo_TyLePBo.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.numDDSo_TyLePBo.TabIndex = 20;
            // 
            // numDDSo_TyLe
            // 
            appearance192.FontData.BoldAsString = "False";
            this.numDDSo_TyLe.Appearance = appearance192;
            this.numDDSo_TyLe.Location = new System.Drawing.Point(158, 169);
            this.numDDSo_TyLe.MaskInput = "nnn";
            this.numDDSo_TyLe.MaxValue = 8;
            this.numDDSo_TyLe.MinValue = 0;
            this.numDDSo_TyLe.Name = "numDDSo_TyLe";
            this.numDDSo_TyLe.PromptChar = ' ';
            this.numDDSo_TyLe.Size = new System.Drawing.Size(443, 23);
            this.numDDSo_TyLe.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.numDDSo_TyLe.TabIndex = 20;
            // 
            // numDDSo_TyGia
            // 
            appearance193.FontData.BoldAsString = "False";
            this.numDDSo_TyGia.Appearance = appearance193;
            this.numDDSo_TyGia.Location = new System.Drawing.Point(158, 146);
            this.numDDSo_TyGia.MaskInput = "nnn";
            this.numDDSo_TyGia.MaxValue = 4;
            this.numDDSo_TyGia.MinValue = 0;
            this.numDDSo_TyGia.Name = "numDDSo_TyGia";
            this.numDDSo_TyGia.PromptChar = ' ';
            this.numDDSo_TyGia.Size = new System.Drawing.Size(443, 23);
            this.numDDSo_TyGia.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.numDDSo_TyGia.TabIndex = 20;
            // 
            // numDDSo_SoLuong
            // 
            appearance194.FontData.BoldAsString = "False";
            this.numDDSo_SoLuong.Appearance = appearance194;
            this.numDDSo_SoLuong.Location = new System.Drawing.Point(158, 123);
            this.numDDSo_SoLuong.MaskInput = "nnn";
            this.numDDSo_SoLuong.MaxValue = 8;
            this.numDDSo_SoLuong.MinValue = 0;
            this.numDDSo_SoLuong.Name = "numDDSo_SoLuong";
            this.numDDSo_SoLuong.PromptChar = ' ';
            this.numDDSo_SoLuong.Size = new System.Drawing.Size(443, 23);
            this.numDDSo_SoLuong.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.numDDSo_SoLuong.TabIndex = 20;
            // 
            // numDDSo_DonGia
            // 
            appearance195.FontData.BoldAsString = "False";
            this.numDDSo_DonGia.Appearance = appearance195;
            this.numDDSo_DonGia.Location = new System.Drawing.Point(158, 77);
            this.numDDSo_DonGia.MaskInput = "nnn";
            this.numDDSo_DonGia.MaxValue = 4;
            this.numDDSo_DonGia.MinValue = 0;
            this.numDDSo_DonGia.Name = "numDDSo_DonGia";
            this.numDDSo_DonGia.PromptChar = ' ';
            this.numDDSo_DonGia.Size = new System.Drawing.Size(443, 23);
            this.numDDSo_DonGia.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.numDDSo_DonGia.TabIndex = 20;
            // 
            // numDDSo_NgoaiTe
            // 
            appearance196.FontData.BoldAsString = "False";
            this.numDDSo_NgoaiTe.Appearance = appearance196;
            this.numDDSo_NgoaiTe.Location = new System.Drawing.Point(158, 54);
            this.numDDSo_NgoaiTe.MaskInput = "nnn";
            this.numDDSo_NgoaiTe.MaxValue = 4;
            this.numDDSo_NgoaiTe.MinValue = 0;
            this.numDDSo_NgoaiTe.Name = "numDDSo_NgoaiTe";
            this.numDDSo_NgoaiTe.PromptChar = ' ';
            this.numDDSo_NgoaiTe.Size = new System.Drawing.Size(443, 23);
            this.numDDSo_NgoaiTe.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.numDDSo_NgoaiTe.TabIndex = 20;
            // 
            // numDDSo_TienVND
            // 
            appearance197.FontData.BoldAsString = "False";
            this.numDDSo_TienVND.Appearance = appearance197;
            this.numDDSo_TienVND.Location = new System.Drawing.Point(158, 31);
            this.numDDSo_TienVND.MaskInput = "nnn";
            this.numDDSo_TienVND.MaxValue = 4;
            this.numDDSo_TienVND.MinValue = 0;
            this.numDDSo_TienVND.Name = "numDDSo_TienVND";
            this.numDDSo_TienVND.PromptChar = ' ';
            this.numDDSo_TienVND.Size = new System.Drawing.Size(443, 23);
            this.numDDSo_TienVND.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.numDDSo_TienVND.TabIndex = 20;
            // 
            // lblTienVietNam
            // 
            appearance198.BackColor = System.Drawing.Color.Transparent;
            appearance198.TextHAlignAsString = "Left";
            appearance198.TextVAlignAsString = "Middle";
            this.lblTienVietNam.Appearance = appearance198;
            this.lblTienVietNam.AutoSize = true;
            this.lblTienVietNam.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTienVietNam.Location = new System.Drawing.Point(910, 35);
            this.lblTienVietNam.Name = "lblTienVietNam";
            this.lblTienVietNam.Size = new System.Drawing.Size(57, 14);
            this.lblTienVietNam.TabIndex = 3;
            this.lblTienVietNam.Text = "1.234.567";
            // 
            // lblSoLuong
            // 
            appearance199.BackColor = System.Drawing.Color.Transparent;
            appearance199.TextHAlignAsString = "Left";
            appearance199.TextVAlignAsString = "Middle";
            this.lblSoLuong.Appearance = appearance199;
            this.lblSoLuong.AutoSize = true;
            this.lblSoLuong.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoLuong.Location = new System.Drawing.Point(910, 127);
            this.lblSoLuong.Name = "lblSoLuong";
            this.lblSoLuong.Size = new System.Drawing.Size(73, 14);
            this.lblSoLuong.TabIndex = 4;
            this.lblSoLuong.Text = "1.234.567,89";
            // 
            // lblTienNgoaiTe
            // 
            appearance200.BackColor = System.Drawing.Color.Transparent;
            appearance200.TextHAlignAsString = "Left";
            appearance200.TextVAlignAsString = "Middle";
            this.lblTienNgoaiTe.Appearance = appearance200;
            this.lblTienNgoaiTe.AutoSize = true;
            this.lblTienNgoaiTe.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTienNgoaiTe.Location = new System.Drawing.Point(910, 58);
            this.lblTienNgoaiTe.Name = "lblTienNgoaiTe";
            this.lblTienNgoaiTe.Size = new System.Drawing.Size(73, 14);
            this.lblTienNgoaiTe.TabIndex = 5;
            this.lblTienNgoaiTe.Text = "1.234.567,89";
            // 
            // ultraLabel80
            // 
            appearance201.BackColor = System.Drawing.Color.Transparent;
            appearance201.TextHAlignAsString = "Left";
            appearance201.TextVAlignAsString = "Middle";
            this.ultraLabel80.Appearance = appearance201;
            this.ultraLabel80.AutoSize = true;
            this.ultraLabel80.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel80.Location = new System.Drawing.Point(794, 35);
            this.ultraLabel80.Name = "ultraLabel80";
            this.ultraLabel80.Size = new System.Drawing.Size(110, 14);
            this.ultraLabel80.TabIndex = 3;
            this.ultraLabel80.Text = "Tiền Việt Nam đồng :";
            // 
            // ultraLabel65
            // 
            appearance202.BackColor = System.Drawing.Color.Transparent;
            appearance202.TextHAlignAsString = "Left";
            appearance202.TextVAlignAsString = "Middle";
            this.ultraLabel65.Appearance = appearance202;
            this.ultraLabel65.AutoSize = true;
            this.ultraLabel65.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel65.Location = new System.Drawing.Point(10, 35);
            this.ultraLabel65.Name = "ultraLabel65";
            this.ultraLabel65.Size = new System.Drawing.Size(110, 14);
            this.ultraLabel65.TabIndex = 3;
            this.ultraLabel65.Text = "Tiền Việt Nam đồng :";
            // 
            // ultraLabel79
            // 
            appearance203.BackColor = System.Drawing.Color.Transparent;
            appearance203.TextHAlignAsString = "Left";
            appearance203.TextVAlignAsString = "Middle";
            this.ultraLabel79.Appearance = appearance203;
            this.ultraLabel79.AutoSize = true;
            this.ultraLabel79.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel79.Location = new System.Drawing.Point(794, 127);
            this.ultraLabel79.Name = "ultraLabel79";
            this.ultraLabel79.Size = new System.Drawing.Size(55, 14);
            this.ultraLabel79.TabIndex = 4;
            this.ultraLabel79.Text = "Số lượng :";
            // 
            // lblTyGia
            // 
            appearance204.BackColor = System.Drawing.Color.Transparent;
            appearance204.TextHAlignAsString = "Left";
            appearance204.TextVAlignAsString = "Middle";
            this.lblTyGia.Appearance = appearance204;
            this.lblTyGia.AutoSize = true;
            this.lblTyGia.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTyGia.Location = new System.Drawing.Point(910, 150);
            this.lblTyGia.Name = "lblTyGia";
            this.lblTyGia.Size = new System.Drawing.Size(86, 14);
            this.lblTyGia.TabIndex = 6;
            this.lblTyGia.Text = "123.456.789,00";
            // 
            // ultraLabel66
            // 
            appearance205.BackColor = System.Drawing.Color.Transparent;
            appearance205.TextHAlignAsString = "Left";
            appearance205.TextVAlignAsString = "Middle";
            this.ultraLabel66.Appearance = appearance205;
            this.ultraLabel66.AutoSize = true;
            this.ultraLabel66.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel66.Location = new System.Drawing.Point(10, 127);
            this.ultraLabel66.Name = "ultraLabel66";
            this.ultraLabel66.Size = new System.Drawing.Size(55, 14);
            this.ultraLabel66.TabIndex = 4;
            this.ultraLabel66.Text = "Số lượng :";
            // 
            // ultraLabel78
            // 
            appearance206.BackColor = System.Drawing.Color.Transparent;
            appearance206.TextHAlignAsString = "Left";
            appearance206.TextVAlignAsString = "Middle";
            this.ultraLabel78.Appearance = appearance206;
            this.ultraLabel78.AutoSize = true;
            this.ultraLabel78.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel78.Location = new System.Drawing.Point(794, 58);
            this.ultraLabel78.Name = "ultraLabel78";
            this.ultraLabel78.Size = new System.Drawing.Size(76, 14);
            this.ultraLabel78.TabIndex = 5;
            this.ultraLabel78.Text = "Tiền ngoại tệ :";
            // 
            // lblDonGia
            // 
            appearance207.BackColor = System.Drawing.Color.Transparent;
            appearance207.TextHAlignAsString = "Left";
            appearance207.TextVAlignAsString = "Middle";
            this.lblDonGia.Appearance = appearance207;
            this.lblDonGia.AutoSize = true;
            this.lblDonGia.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDonGia.Location = new System.Drawing.Point(910, 81);
            this.lblDonGia.Name = "lblDonGia";
            this.lblDonGia.Size = new System.Drawing.Size(73, 14);
            this.lblDonGia.TabIndex = 7;
            this.lblDonGia.Text = "1.234.567,89";
            // 
            // ultraLabel77
            // 
            appearance208.BackColor = System.Drawing.Color.Transparent;
            appearance208.TextHAlignAsString = "Left";
            appearance208.TextVAlignAsString = "Middle";
            this.ultraLabel77.Appearance = appearance208;
            this.ultraLabel77.AutoSize = true;
            this.ultraLabel77.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel77.Location = new System.Drawing.Point(794, 150);
            this.ultraLabel77.Name = "ultraLabel77";
            this.ultraLabel77.Size = new System.Drawing.Size(41, 14);
            this.ultraLabel77.TabIndex = 6;
            this.ultraLabel77.Text = "Tỷ giá :";
            // 
            // ultraLabel68
            // 
            appearance209.BackColor = System.Drawing.Color.Transparent;
            appearance209.TextHAlignAsString = "Left";
            appearance209.TextVAlignAsString = "Middle";
            this.ultraLabel68.Appearance = appearance209;
            this.ultraLabel68.AutoSize = true;
            this.ultraLabel68.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel68.Location = new System.Drawing.Point(10, 58);
            this.ultraLabel68.Name = "ultraLabel68";
            this.ultraLabel68.Size = new System.Drawing.Size(76, 14);
            this.ultraLabel68.TabIndex = 5;
            this.ultraLabel68.Text = "Tiền ngoại tệ :";
            // 
            // ultraLabel69
            // 
            appearance210.BackColor = System.Drawing.Color.Transparent;
            appearance210.TextHAlignAsString = "Left";
            appearance210.TextVAlignAsString = "Middle";
            this.ultraLabel69.Appearance = appearance210;
            this.ultraLabel69.AutoSize = true;
            this.ultraLabel69.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel69.Location = new System.Drawing.Point(10, 150);
            this.ultraLabel69.Name = "ultraLabel69";
            this.ultraLabel69.Size = new System.Drawing.Size(41, 14);
            this.ultraLabel69.TabIndex = 6;
            this.ultraLabel69.Text = "Tỷ giá :";
            // 
            // ultraLabel76
            // 
            appearance211.BackColor = System.Drawing.Color.Transparent;
            appearance211.TextHAlignAsString = "Left";
            appearance211.TextVAlignAsString = "Middle";
            this.ultraLabel76.Appearance = appearance211;
            this.ultraLabel76.AutoSize = true;
            this.ultraLabel76.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel76.Location = new System.Drawing.Point(794, 81);
            this.ultraLabel76.Name = "ultraLabel76";
            this.ultraLabel76.Size = new System.Drawing.Size(49, 14);
            this.ultraLabel76.TabIndex = 7;
            this.ultraLabel76.Text = "Đơn giá :";
            // 
            // lblTyLePhanBo
            // 
            appearance212.BackColor = System.Drawing.Color.Transparent;
            appearance212.TextHAlignAsString = "Left";
            appearance212.TextVAlignAsString = "Middle";
            this.lblTyLePhanBo.Appearance = appearance212;
            this.lblTyLePhanBo.AutoSize = true;
            this.lblTyLePhanBo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTyLePhanBo.Location = new System.Drawing.Point(910, 196);
            this.lblTyLePhanBo.Name = "lblTyLePhanBo";
            this.lblTyLePhanBo.Size = new System.Drawing.Size(125, 14);
            this.lblTyLePhanBo.TabIndex = 8;
            this.lblTyLePhanBo.Text = "1.234.567,8900000000";
            // 
            // ultraLabel70
            // 
            appearance213.BackColor = System.Drawing.Color.Transparent;
            appearance213.TextHAlignAsString = "Left";
            appearance213.TextVAlignAsString = "Middle";
            this.ultraLabel70.Appearance = appearance213;
            this.ultraLabel70.AutoSize = true;
            this.ultraLabel70.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel70.Location = new System.Drawing.Point(10, 81);
            this.ultraLabel70.Name = "ultraLabel70";
            this.ultraLabel70.Size = new System.Drawing.Size(49, 14);
            this.ultraLabel70.TabIndex = 7;
            this.ultraLabel70.Text = "Đơn giá :";
            // 
            // lblHeSoTyLe
            // 
            appearance214.BackColor = System.Drawing.Color.Transparent;
            appearance214.TextHAlignAsString = "Left";
            appearance214.TextVAlignAsString = "Middle";
            this.lblHeSoTyLe.Appearance = appearance214;
            this.lblHeSoTyLe.AutoSize = true;
            this.lblHeSoTyLe.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeSoTyLe.Location = new System.Drawing.Point(910, 173);
            this.lblHeSoTyLe.Name = "lblHeSoTyLe";
            this.lblHeSoTyLe.Size = new System.Drawing.Size(86, 14);
            this.lblHeSoTyLe.TabIndex = 9;
            this.lblHeSoTyLe.Text = "123.456.789,00";
            // 
            // ultraLabel75
            // 
            appearance215.BackColor = System.Drawing.Color.Transparent;
            appearance215.TextHAlignAsString = "Left";
            appearance215.TextVAlignAsString = "Middle";
            this.ultraLabel75.Appearance = appearance215;
            this.ultraLabel75.AutoSize = true;
            this.ultraLabel75.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel75.Location = new System.Drawing.Point(794, 196);
            this.ultraLabel75.Name = "ultraLabel75";
            this.ultraLabel75.Size = new System.Drawing.Size(79, 14);
            this.ultraLabel75.TabIndex = 8;
            this.ultraLabel75.Text = "Tỷ lệ phân bổ :";
            // 
            // ultraLabel73
            // 
            appearance216.BackColor = System.Drawing.Color.Transparent;
            appearance216.TextHAlignAsString = "Left";
            appearance216.TextVAlignAsString = "Middle";
            this.ultraLabel73.Appearance = appearance216;
            this.ultraLabel73.AutoSize = true;
            this.ultraLabel73.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.ultraLabel73.Location = new System.Drawing.Point(676, 77);
            this.ultraLabel73.Name = "ultraLabel73";
            this.ultraLabel73.Size = new System.Drawing.Size(37, 14);
            this.ultraLabel73.TabIndex = 8;
            this.ultraLabel73.Text = "Ví dụ :";
            // 
            // ultraLabel74
            // 
            appearance217.BackColor = System.Drawing.Color.Transparent;
            appearance217.TextHAlignAsString = "Left";
            appearance217.TextVAlignAsString = "Middle";
            this.ultraLabel74.Appearance = appearance217;
            this.ultraLabel74.AutoSize = true;
            this.ultraLabel74.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel74.Location = new System.Drawing.Point(794, 173);
            this.ultraLabel74.Name = "ultraLabel74";
            this.ultraLabel74.Size = new System.Drawing.Size(70, 14);
            this.ultraLabel74.TabIndex = 9;
            this.ultraLabel74.Text = "Hệ số / tỷ lệ :";
            // 
            // ultraLabel71
            // 
            appearance218.BackColor = System.Drawing.Color.Transparent;
            appearance218.TextHAlignAsString = "Left";
            appearance218.TextVAlignAsString = "Middle";
            this.ultraLabel71.Appearance = appearance218;
            this.ultraLabel71.AutoSize = true;
            this.ultraLabel71.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel71.Location = new System.Drawing.Point(10, 196);
            this.ultraLabel71.Name = "ultraLabel71";
            this.ultraLabel71.Size = new System.Drawing.Size(79, 14);
            this.ultraLabel71.TabIndex = 8;
            this.ultraLabel71.Text = "Tỷ lệ phân bổ :";
            // 
            // ultraLabel72
            // 
            appearance219.BackColor = System.Drawing.Color.Transparent;
            appearance219.TextHAlignAsString = "Left";
            appearance219.TextVAlignAsString = "Middle";
            this.ultraLabel72.Appearance = appearance219;
            this.ultraLabel72.AutoSize = true;
            this.ultraLabel72.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel72.Location = new System.Drawing.Point(10, 173);
            this.ultraLabel72.Name = "ultraLabel72";
            this.ultraLabel72.Size = new System.Drawing.Size(70, 14);
            this.ultraLabel72.TabIndex = 9;
            this.ultraLabel72.Text = "Hệ số / tỷ lệ :";
            // 
            // ultraTabPageControl7
            // 
            this.ultraTabPageControl7.Controls.Add(this.ultraGroupBox22);
            this.ultraTabPageControl7.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl7.Name = "ultraTabPageControl7";
            this.ultraTabPageControl7.Size = new System.Drawing.Size(1218, 568);
            // 
            // ultraGroupBox22
            // 
            appearance221.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox22.Appearance = appearance221;
            this.ultraGroupBox22.ContentPadding.Top = 10;
            this.ultraGroupBox22.Controls.Add(this.ultraPanel1);
            this.ultraGroupBox22.Controls.Add(this.opthinhthucsaoluu);
            this.ultraGroupBox22.Controls.Add(this.txtSLUU_TMSaoLuu);
            this.ultraGroupBox22.Controls.Add(this.ultraLabel87);
            this.ultraGroupBox22.Controls.Add(this.ButSL_Chon);
            this.ultraGroupBox22.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance227.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox22.HeaderAppearance = appearance227;
            this.ultraGroupBox22.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox22.Name = "ultraGroupBox22";
            this.ultraGroupBox22.Size = new System.Drawing.Size(1218, 490);
            this.ultraGroupBox22.TabIndex = 20;
            this.ultraGroupBox22.Text = "Tùy chọn sao lưu dữ liệu";
            this.ultraGroupBox22.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraPanel1
            // 
            this.ultraPanel1.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.numSLUU_NgaySLUU);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel102);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel86);
            this.ultraPanel1.Location = new System.Drawing.Point(18, 146);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(1024, 59);
            this.ultraPanel1.TabIndex = 21;
            this.ultraPanel1.Visible = false;
            // 
            // numSLUU_NgaySLUU
            // 
            this.numSLUU_NgaySLUU.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numSLUU_NgaySLUU.Location = new System.Drawing.Point(276, 19);
            this.numSLUU_NgaySLUU.MaxValue = 30;
            this.numSLUU_NgaySLUU.MinValue = 1;
            this.numSLUU_NgaySLUU.Name = "numSLUU_NgaySLUU";
            this.numSLUU_NgaySLUU.PromptChar = ' ';
            this.numSLUU_NgaySLUU.Size = new System.Drawing.Size(100, 21);
            this.numSLUU_NgaySLUU.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.numSLUU_NgaySLUU.TabIndex = 23;
            this.numSLUU_NgaySLUU.Value = 7;
            this.numSLUU_NgaySLUU.Validated += new System.EventHandler(this.numSLUU_NgaySLUU_ValueChanged);
            // 
            // ultraLabel102
            // 
            this.ultraLabel102.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel102.Location = new System.Drawing.Point(382, 23);
            this.ultraLabel102.Name = "ultraLabel102";
            this.ultraLabel102.Size = new System.Drawing.Size(245, 17);
            this.ultraLabel102.TabIndex = 22;
            this.ultraLabel102.Text = "ngày, kể từ bản sao lưu gần nhất";
            // 
            // ultraLabel86
            // 
            appearance222.Image = global::Accounting.Properties.Resources._1153px_Warning_icon_svg;
            appearance222.ImageHAlign = Infragistics.Win.HAlign.Left;
            appearance222.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance222.TextVAlignAsString = "Middle";
            this.ultraLabel86.Appearance = appearance222;
            this.ultraLabel86.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel86.Location = new System.Drawing.Point(10, 20);
            this.ultraLabel86.Name = "ultraLabel86";
            this.ultraLabel86.Size = new System.Drawing.Size(313, 20);
            this.ultraLabel86.TabIndex = 21;
            this.ultraLabel86.Text = "   Lưu ý: Chương trình tự động sao lưu dữ liệu sau";
            // 
            // opthinhthucsaoluu
            // 
            appearance223.BackColor = System.Drawing.Color.Transparent;
            appearance223.FontData.BoldAsString = "False";
            this.opthinhthucsaoluu.Appearance = appearance223;
            this.opthinhthucsaoluu.BackColor = System.Drawing.Color.Transparent;
            this.opthinhthucsaoluu.BackColorInternal = System.Drawing.Color.Transparent;
            this.opthinhthucsaoluu.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.opthinhthucsaoluu.CheckedIndex = 0;
            valueListItem20.DataValue = "Default Item";
            valueListItem20.DisplayText = "Tự động sao lưu dữ liệu khi đóng chương trình";
            valueListItem20.Tag = "optSLUU_SauKhiDongCT";
            valueListItem10.DataValue = "ValueListItem1";
            valueListItem10.DisplayText = "Nhắc nhở sao lưu dữ liệu khi đóng chương trình";
            valueListItem10.Tag = "optSLUU_NhacKhiDong";
            valueListItem22.DataValue = "ValueListItem2";
            valueListItem22.DisplayText = "Không sao lưu dữ liệu khi đóng chương trình";
            valueListItem22.Tag = "optSLUU_KhongLuu";
            this.opthinhthucsaoluu.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem20,
            valueListItem10,
            valueListItem22});
            this.opthinhthucsaoluu.Location = new System.Drawing.Point(18, 36);
            this.opthinhthucsaoluu.Name = "opthinhthucsaoluu";
            this.opthinhthucsaoluu.Size = new System.Drawing.Size(346, 55);
            this.opthinhthucsaoluu.TabIndex = 11;
            this.opthinhthucsaoluu.Text = "Tự động sao lưu dữ liệu khi đóng chương trình";
            this.opthinhthucsaoluu.ValueChanged += new System.EventHandler(this.opthinhthucsaoluu_ValueChanged);
            // 
            // txtSLUU_TMSaoLuu
            // 
            appearance224.FontData.BoldAsString = "False";
            this.txtSLUU_TMSaoLuu.Appearance = appearance224;
            this.txtSLUU_TMSaoLuu.Location = new System.Drawing.Point(131, 108);
            this.txtSLUU_TMSaoLuu.Name = "txtSLUU_TMSaoLuu";
            this.txtSLUU_TMSaoLuu.Size = new System.Drawing.Size(911, 23);
            this.txtSLUU_TMSaoLuu.TabIndex = 14;
            // 
            // ultraLabel87
            // 
            appearance225.BackColor = System.Drawing.Color.Transparent;
            appearance225.TextHAlignAsString = "Left";
            appearance225.TextVAlignAsString = "Middle";
            this.ultraLabel87.Appearance = appearance225;
            this.ultraLabel87.AutoSize = true;
            this.ultraLabel87.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel87.Location = new System.Drawing.Point(18, 114);
            this.ultraLabel87.Name = "ultraLabel87";
            this.ultraLabel87.Size = new System.Drawing.Size(94, 14);
            this.ultraLabel87.TabIndex = 13;
            this.ultraLabel87.Text = "Thư mục sao lưu :";
            // 
            // ButSL_Chon
            // 
            appearance226.Image = ((object)(resources.GetObject("appearance226.Image")));
            this.ButSL_Chon.Appearance = appearance226;
            this.ButSL_Chon.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButSL_Chon.Location = new System.Drawing.Point(1085, 108);
            this.ButSL_Chon.Name = "ButSL_Chon";
            this.ButSL_Chon.Size = new System.Drawing.Size(95, 23);
            this.ButSL_Chon.TabIndex = 15;
            this.ButSL_Chon.Text = "Chọn ...";
            this.ButSL_Chon.Click += new System.EventHandler(this.ButSL_Chon_Click);
            // 
            // ultraTabPageControl8
            // 
            this.ultraTabPageControl8.Controls.Add(this.ultraGroupBox8);
            this.ultraTabPageControl8.Controls.Add(this.ultraGroupBox26);
            this.ultraTabPageControl8.Controls.Add(this.ultraGroupBox25);
            this.ultraTabPageControl8.Controls.Add(this.ultraGroupBox24);
            this.ultraTabPageControl8.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl8.Name = "ultraTabPageControl8";
            this.ultraTabPageControl8.Size = new System.Drawing.Size(1218, 568);
            // 
            // ultraGroupBox8
            // 
            appearance228.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox8.Appearance = appearance228;
            this.ultraGroupBox8.ContentPadding.Top = 10;
            this.ultraGroupBox8.Controls.Add(this.ultraLabel104);
            this.ultraGroupBox8.Controls.Add(this.txtEmailForgotPass);
            this.ultraGroupBox8.Controls.Add(this.ultraLabel56);
            this.ultraGroupBox8.Controls.Add(this.optPBCCDC);
            this.ultraGroupBox8.Controls.Add(this.ultraLabel55);
            this.ultraGroupBox8.Controls.Add(this.optPB);
            this.ultraGroupBox8.Controls.Add(this.chkTCKHAC_SUDUNGTHEMSOQUANTRI);
            this.ultraGroupBox8.Controls.Add(this.chkHDDT);
            this.ultraGroupBox8.Controls.Add(this.meTCKHAC_MenhGiaCP);
            this.ultraGroupBox8.Controls.Add(this.chkTCKHAC_DocTienBangChu);
            this.ultraGroupBox8.Controls.Add(this.chkTCKHAC_CBKoChonNVBH);
            this.ultraGroupBox8.Controls.Add(this.chkTCKHAC_ChepDL);
            this.ultraGroupBox8.Controls.Add(this.chkTCKHAC_CPKoHLy);
            this.ultraGroupBox8.Controls.Add(this.chkTCKHAC_ChiQuaTonQuy);
            this.ultraGroupBox8.Controls.Add(this.chkTCKHAC_SinhTK007);
            this.ultraGroupBox8.Controls.Add(this.opt);
            this.ultraGroupBox8.Controls.Add(this.ultraLabel94);
            this.ultraGroupBox8.Controls.Add(this.cbcTCKHAC_ChuHThiInHDLan2);
            this.ultraGroupBox8.Controls.Add(this.ultraLabel97);
            this.ultraGroupBox8.Controls.Add(this.cbcTCKHAC_DocTienLe);
            this.ultraGroupBox8.Controls.Add(this.ultraLabel95);
            this.ultraGroupBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance271.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox8.HeaderAppearance = appearance271;
            this.ultraGroupBox8.Location = new System.Drawing.Point(0, 296);
            this.ultraGroupBox8.Name = "ultraGroupBox8";
            this.ultraGroupBox8.Size = new System.Drawing.Size(1218, 267);
            this.ultraGroupBox8.TabIndex = 26;
            this.ultraGroupBox8.Text = "Thiết lập khác";
            this.ultraGroupBox8.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel104
            // 
            appearance229.BackColor = System.Drawing.Color.Transparent;
            appearance229.TextHAlignAsString = "Left";
            appearance229.TextVAlignAsString = "Middle";
            this.ultraLabel104.Appearance = appearance229;
            this.ultraLabel104.AutoSize = true;
            this.ultraLabel104.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel104.Location = new System.Drawing.Point(566, 213);
            this.ultraLabel104.Name = "ultraLabel104";
            this.ultraLabel104.Size = new System.Drawing.Size(190, 14);
            this.ultraLabel104.TabIndex = 52;
            this.ultraLabel104.Text = "Email khi quên mật khẩu đăng nhập :";
            // 
            // txtEmailForgotPass
            // 
            this.txtEmailForgotPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmailForgotPass.Location = new System.Drawing.Point(769, 207);
            this.txtEmailForgotPass.Name = "txtEmailForgotPass";
            this.txtEmailForgotPass.Size = new System.Drawing.Size(290, 23);
            this.txtEmailForgotPass.TabIndex = 53;
            this.txtEmailForgotPass.AfterExitEditMode += new System.EventHandler(this.txtEmailForgotPass_AfterExitEditMode);
            // 
            // ultraLabel56
            // 
            appearance230.BackColor = System.Drawing.Color.Transparent;
            appearance230.TextHAlignAsString = "Left";
            appearance230.TextVAlignAsString = "Middle";
            this.ultraLabel56.Appearance = appearance230;
            this.ultraLabel56.AutoSize = true;
            this.ultraLabel56.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel56.Location = new System.Drawing.Point(710, 163);
            this.ultraLabel56.Name = "ultraLabel56";
            this.ultraLabel56.Size = new System.Drawing.Size(135, 14);
            this.ultraLabel56.TabIndex = 51;
            this.ultraLabel56.Text = "Phân bổ công cụ, dụng cụ";
            // 
            // optPBCCDC
            // 
            appearance231.FontData.BoldAsString = "False";
            appearance231.TextHAlignAsString = "Left";
            appearance231.TextVAlignAsString = "Middle";
            this.optPBCCDC.Appearance = appearance231;
            this.optPBCCDC.BackColor = System.Drawing.Color.Transparent;
            this.optPBCCDC.BackColorInternal = System.Drawing.Color.Transparent;
            this.optPBCCDC.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.optPBCCDC.CheckedIndex = 0;
            valueListItem7.DataValue = "Default Item";
            valueListItem7.DisplayText = "Phân bổ đều hàng kỳ";
            valueListItem7.Tag = "optPB_HangKyCCDC";
            valueListItem8.DataValue = "ValueListItem1";
            valueListItem8.DisplayText = "Phân bổ theo ngày ghi tăng";
            valueListItem8.Tag = "optPB_TheoNgayCCDC";
            this.optPBCCDC.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem7,
            valueListItem8});
            this.optPBCCDC.ItemSpacingHorizontal = 17;
            this.optPBCCDC.Location = new System.Drawing.Point(710, 185);
            this.optPBCCDC.Name = "optPBCCDC";
            this.optPBCCDC.ReadOnly = false;
            this.optPBCCDC.Size = new System.Drawing.Size(371, 17);
            this.optPBCCDC.TabIndex = 50;
            this.optPBCCDC.Text = "Phân bổ đều hàng kỳ";
            // 
            // ultraLabel55
            // 
            appearance232.BackColor = System.Drawing.Color.Transparent;
            appearance232.TextHAlignAsString = "Left";
            appearance232.TextVAlignAsString = "Middle";
            this.ultraLabel55.Appearance = appearance232;
            this.ultraLabel55.AutoSize = true;
            this.ultraLabel55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel55.Location = new System.Drawing.Point(710, 111);
            this.ultraLabel55.Name = "ultraLabel55";
            this.ultraLabel55.Size = new System.Drawing.Size(126, 14);
            this.ultraLabel55.TabIndex = 49;
            this.ultraLabel55.Text = "Phân bổ chi phí trả trước";
            // 
            // optPB
            // 
            appearance233.FontData.BoldAsString = "False";
            appearance233.TextHAlignAsString = "Left";
            appearance233.TextVAlignAsString = "Middle";
            this.optPB.Appearance = appearance233;
            this.optPB.BackColor = System.Drawing.Color.Transparent;
            this.optPB.BackColorInternal = System.Drawing.Color.Transparent;
            this.optPB.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.optPB.CheckedIndex = 0;
            valueListItem1.DataValue = "Default Item";
            valueListItem1.DisplayText = "Phân bổ đều hàng kỳ";
            valueListItem1.Tag = "optPB_HangKy";
            valueListItem3.DataValue = "ValueListItem1";
            valueListItem3.DisplayText = "Phân bổ theo ngày ghi nhận chi phí";
            valueListItem3.Tag = "optPB_TheoNgay";
            this.optPB.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem3});
            this.optPB.ItemSpacingHorizontal = 17;
            this.optPB.Location = new System.Drawing.Point(710, 133);
            this.optPB.Name = "optPB";
            this.optPB.ReadOnly = false;
            this.optPB.Size = new System.Drawing.Size(394, 17);
            this.optPB.TabIndex = 48;
            this.optPB.Text = "Phân bổ đều hàng kỳ";
            // 
            // chkTCKHAC_SUDUNGTHEMSOQUANTRI
            // 
            appearance234.BackColor = System.Drawing.Color.Transparent;
            appearance234.FontData.BoldAsString = "False";
            this.chkTCKHAC_SUDUNGTHEMSOQUANTRI.Appearance = appearance234;
            this.chkTCKHAC_SUDUNGTHEMSOQUANTRI.BackColor = System.Drawing.Color.Transparent;
            this.chkTCKHAC_SUDUNGTHEMSOQUANTRI.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkTCKHAC_SUDUNGTHEMSOQUANTRI.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkTCKHAC_SUDUNGTHEMSOQUANTRI.Location = new System.Drawing.Point(17, 189);
            this.chkTCKHAC_SUDUNGTHEMSOQUANTRI.Name = "chkTCKHAC_SUDUNGTHEMSOQUANTRI";
            this.chkTCKHAC_SUDUNGTHEMSOQUANTRI.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkTCKHAC_SUDUNGTHEMSOQUANTRI.Size = new System.Drawing.Size(221, 16);
            this.chkTCKHAC_SUDUNGTHEMSOQUANTRI.TabIndex = 43;
            this.chkTCKHAC_SUDUNGTHEMSOQUANTRI.Text = "Sử dụng thêm sổ quản trị";
            this.chkTCKHAC_SUDUNGTHEMSOQUANTRI.Visible = false;
            // 
            // chkHDDT
            // 
            appearance235.BackColor = System.Drawing.Color.Transparent;
            appearance235.FontData.BoldAsString = "False";
            this.chkHDDT.Appearance = appearance235;
            this.chkHDDT.BackColor = System.Drawing.Color.Transparent;
            this.chkHDDT.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkHDDT.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkHDDT.Location = new System.Drawing.Point(17, 116);
            this.chkHDDT.Name = "chkHDDT";
            this.chkHDDT.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkHDDT.Size = new System.Drawing.Size(221, 16);
            this.chkHDDT.TabIndex = 42;
            this.chkHDDT.Text = "Tích hợp hóa đơn điện tử";
            // 
            // meTCKHAC_MenhGiaCP
            // 
            appearance236.FontData.BoldAsString = "False";
            appearance236.TextHAlignAsString = "Right";
            this.meTCKHAC_MenhGiaCP.Appearance = appearance236;
            this.meTCKHAC_MenhGiaCP.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.String;
            this.meTCKHAC_MenhGiaCP.InputMask = "nn,nnn,nnn,nnn,nnn";
            this.meTCKHAC_MenhGiaCP.Location = new System.Drawing.Point(244, 207);
            this.meTCKHAC_MenhGiaCP.Name = "meTCKHAC_MenhGiaCP";
            this.meTCKHAC_MenhGiaCP.PromptChar = ' ';
            this.meTCKHAC_MenhGiaCP.Size = new System.Drawing.Size(287, 21);
            this.meTCKHAC_MenhGiaCP.TabIndex = 41;
            this.meTCKHAC_MenhGiaCP.Visible = false;
            // 
            // chkTCKHAC_DocTienBangChu
            // 
            appearance237.BackColor = System.Drawing.Color.Transparent;
            appearance237.FontData.BoldAsString = "False";
            this.chkTCKHAC_DocTienBangChu.Appearance = appearance237;
            this.chkTCKHAC_DocTienBangChu.BackColor = System.Drawing.Color.Transparent;
            this.chkTCKHAC_DocTienBangChu.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkTCKHAC_DocTienBangChu.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkTCKHAC_DocTienBangChu.Location = new System.Drawing.Point(17, 67);
            this.chkTCKHAC_DocTienBangChu.Name = "chkTCKHAC_DocTienBangChu";
            this.chkTCKHAC_DocTienBangChu.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkTCKHAC_DocTienBangChu.Size = new System.Drawing.Size(270, 16);
            this.chkTCKHAC_DocTienBangChu.TabIndex = 37;
            this.chkTCKHAC_DocTienBangChu.Text = "Hiển thị chữ \"chẵn\" khi đọc tiền bằng chữ ";
            // 
            // chkTCKHAC_CBKoChonNVBH
            // 
            appearance238.BackColor = System.Drawing.Color.Transparent;
            appearance238.FontData.BoldAsString = "False";
            this.chkTCKHAC_CBKoChonNVBH.Appearance = appearance238;
            this.chkTCKHAC_CBKoChonNVBH.BackColor = System.Drawing.Color.Transparent;
            this.chkTCKHAC_CBKoChonNVBH.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkTCKHAC_CBKoChonNVBH.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkTCKHAC_CBKoChonNVBH.Location = new System.Drawing.Point(710, 62);
            this.chkTCKHAC_CBKoChonNVBH.Name = "chkTCKHAC_CBKoChonNVBH";
            this.chkTCKHAC_CBKoChonNVBH.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkTCKHAC_CBKoChonNVBH.Size = new System.Drawing.Size(313, 16);
            this.chkTCKHAC_CBKoChonNVBH.TabIndex = 38;
            this.chkTCKHAC_CBKoChonNVBH.Text = "Cảnh báo khi không chọn nhân viên bán hàng ";
            // 
            // chkTCKHAC_ChepDL
            // 
            appearance239.BackColor = System.Drawing.Color.Transparent;
            appearance239.FontData.BoldAsString = "False";
            this.chkTCKHAC_ChepDL.Appearance = appearance239;
            this.chkTCKHAC_ChepDL.BackColor = System.Drawing.Color.Transparent;
            this.chkTCKHAC_ChepDL.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkTCKHAC_ChepDL.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkTCKHAC_ChepDL.Location = new System.Drawing.Point(248, 167);
            this.chkTCKHAC_ChepDL.Name = "chkTCKHAC_ChepDL";
            this.chkTCKHAC_ChepDL.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkTCKHAC_ChepDL.Size = new System.Drawing.Size(354, 16);
            this.chkTCKHAC_ChepDL.TabIndex = 34;
            this.chkTCKHAC_ChepDL.Text = "Cho phép sao chép dữ liệu khi thêm mới dòng dữ liệu ";
            this.chkTCKHAC_ChepDL.Visible = false;
            // 
            // chkTCKHAC_CPKoHLy
            // 
            appearance240.BackColor = System.Drawing.Color.Transparent;
            appearance240.FontData.BoldAsString = "False";
            this.chkTCKHAC_CPKoHLy.Appearance = appearance240;
            this.chkTCKHAC_CPKoHLy.BackColor = System.Drawing.Color.Transparent;
            this.chkTCKHAC_CPKoHLy.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkTCKHAC_CPKoHLy.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkTCKHAC_CPKoHLy.Location = new System.Drawing.Point(248, 123);
            this.chkTCKHAC_CPKoHLy.Name = "chkTCKHAC_CPKoHLy";
            this.chkTCKHAC_CPKoHLy.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkTCKHAC_CPKoHLy.Size = new System.Drawing.Size(279, 16);
            this.chkTCKHAC_CPKoHLy.TabIndex = 40;
            this.chkTCKHAC_CPKoHLy.Text = "Theo dõi các khoản chi phí không hợp lý ";
            this.chkTCKHAC_CPKoHLy.Visible = false;
            // 
            // chkTCKHAC_ChiQuaTonQuy
            // 
            appearance241.BackColor = System.Drawing.Color.Transparent;
            appearance241.FontData.BoldAsString = "False";
            this.chkTCKHAC_ChiQuaTonQuy.Appearance = appearance241;
            this.chkTCKHAC_ChiQuaTonQuy.BackColor = System.Drawing.Color.Transparent;
            this.chkTCKHAC_ChiQuaTonQuy.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkTCKHAC_ChiQuaTonQuy.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkTCKHAC_ChiQuaTonQuy.Location = new System.Drawing.Point(710, 39);
            this.chkTCKHAC_ChiQuaTonQuy.Name = "chkTCKHAC_ChiQuaTonQuy";
            this.chkTCKHAC_ChiQuaTonQuy.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkTCKHAC_ChiQuaTonQuy.Size = new System.Drawing.Size(270, 16);
            this.chkTCKHAC_ChiQuaTonQuy.TabIndex = 36;
            this.chkTCKHAC_ChiQuaTonQuy.Text = "Cho phép chi quá tồn quỹ";
            this.chkTCKHAC_ChiQuaTonQuy.CheckStateChanged += new System.EventHandler(this.chkTCKHAC_ChiQuaTonQuy_CheckStateChanged);
            // 
            // chkTCKHAC_SinhTK007
            // 
            appearance242.BackColor = System.Drawing.Color.Transparent;
            appearance242.FontData.BoldAsString = "False";
            this.chkTCKHAC_SinhTK007.Appearance = appearance242;
            this.chkTCKHAC_SinhTK007.BackColor = System.Drawing.Color.Transparent;
            this.chkTCKHAC_SinhTK007.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkTCKHAC_SinhTK007.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkTCKHAC_SinhTK007.Location = new System.Drawing.Point(248, 145);
            this.chkTCKHAC_SinhTK007.Name = "chkTCKHAC_SinhTK007";
            this.chkTCKHAC_SinhTK007.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkTCKHAC_SinhTK007.Size = new System.Drawing.Size(327, 16);
            this.chkTCKHAC_SinhTK007.TabIndex = 39;
            this.chkTCKHAC_SinhTK007.Text = "Sinh đồng thời TK007 khi hạch toán ngoại tệ ";
            this.chkTCKHAC_SinhTK007.Visible = false;
            // 
            // opt
            // 
            appearance243.BackColor = System.Drawing.Color.Transparent;
            appearance243.FontData.BoldAsString = "False";
            this.opt.Appearance = appearance243;
            this.opt.BackColor = System.Drawing.Color.Transparent;
            this.opt.BackColorInternal = System.Drawing.Color.Transparent;
            this.opt.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.opt.CheckedIndex = 0;
            valueListItem11.DataValue = "Default Item";
            valueListItem11.DisplayText = "Lưu đồng thời ghi sổ ";
            valueListItem11.Tag = "optTCKHAC_LuuGSo";
            valueListItem21.DataValue = "ValueListItem1";
            valueListItem21.DisplayText = "Lưu dữ liệu nhưng không ghi sổ ";
            valueListItem21.Tag = "optTCKHAC_LuuKoGSo";
            this.opt.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem11,
            valueListItem21});
            this.opt.ItemSpacingHorizontal = 93;
            this.opt.Location = new System.Drawing.Point(17, 91);
            this.opt.Name = "opt";
            this.opt.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.opt.Size = new System.Drawing.Size(514, 20);
            this.opt.TabIndex = 25;
            this.opt.Text = "Lưu đồng thời ghi sổ ";
            // 
            // ultraLabel94
            // 
            appearance244.BackColor = System.Drawing.Color.Transparent;
            appearance244.TextHAlignAsString = "Left";
            appearance244.TextVAlignAsString = "Middle";
            this.ultraLabel94.Appearance = appearance244;
            this.ultraLabel94.AutoSize = true;
            this.ultraLabel94.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel94.Location = new System.Drawing.Point(13, 214);
            this.ultraLabel94.Name = "ultraLabel94";
            this.ultraLabel94.Size = new System.Drawing.Size(100, 14);
            this.ultraLabel94.TabIndex = 20;
            this.ultraLabel94.Text = "Mệnh giá cổ phần :";
            this.ultraLabel94.Visible = false;
            // 
            // cbcTCKHAC_ChuHThiInHDLan2
            // 
            appearance245.BackColor = System.Drawing.SystemColors.Window;
            appearance245.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.Appearance = appearance245;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance246.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance246.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance246.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance246.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.GroupByBox.Appearance = appearance246;
            appearance247.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance247;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance248.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance248.BackColor2 = System.Drawing.SystemColors.Control;
            appearance248.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance248.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.GroupByBox.PromptAppearance = appearance248;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.MaxColScrollRegions = 1;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance249.BackColor = System.Drawing.SystemColors.Window;
            appearance249.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.Override.ActiveCellAppearance = appearance249;
            appearance250.BackColor = System.Drawing.SystemColors.Highlight;
            appearance250.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.Override.ActiveRowAppearance = appearance250;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance251.BackColor = System.Drawing.SystemColors.Window;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.Override.CardAreaAppearance = appearance251;
            appearance252.BorderColor = System.Drawing.Color.Silver;
            appearance252.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.Override.CellAppearance = appearance252;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.Override.CellPadding = 0;
            appearance253.BackColor = System.Drawing.SystemColors.Control;
            appearance253.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance253.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance253.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance253.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.Override.GroupByRowAppearance = appearance253;
            appearance254.TextHAlignAsString = "Left";
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.Override.HeaderAppearance = appearance254;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance255.BackColor = System.Drawing.SystemColors.Window;
            appearance255.BorderColor = System.Drawing.Color.Silver;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.Override.RowAppearance = appearance255;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance256.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.Override.TemplateAddRowAppearance = appearance256;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbcTCKHAC_ChuHThiInHDLan2.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbcTCKHAC_ChuHThiInHDLan2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbcTCKHAC_ChuHThiInHDLan2.Location = new System.Drawing.Point(244, 234);
            this.cbcTCKHAC_ChuHThiInHDLan2.Name = "cbcTCKHAC_ChuHThiInHDLan2";
            this.cbcTCKHAC_ChuHThiInHDLan2.Size = new System.Drawing.Size(287, 24);
            this.cbcTCKHAC_ChuHThiInHDLan2.TabIndex = 24;
            this.cbcTCKHAC_ChuHThiInHDLan2.Visible = false;
            // 
            // ultraLabel97
            // 
            appearance257.BackColor = System.Drawing.Color.Transparent;
            appearance257.TextHAlignAsString = "Left";
            appearance257.TextVAlignAsString = "Middle";
            this.ultraLabel97.Appearance = appearance257;
            this.ultraLabel97.AutoSize = true;
            this.ultraLabel97.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel97.Location = new System.Drawing.Point(13, 240);
            this.ultraLabel97.Name = "ultraLabel97";
            this.ultraLabel97.Size = new System.Drawing.Size(221, 14);
            this.ultraLabel97.TabIndex = 19;
            this.ultraLabel97.Text = "Chữ hiển thị khi in hóa đơn  lần thứ 2 trở đi :";
            this.ultraLabel97.Visible = false;
            // 
            // cbcTCKHAC_DocTienLe
            // 
            appearance258.BackColor = System.Drawing.SystemColors.Window;
            appearance258.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.Appearance = appearance258;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance259.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance259.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance259.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance259.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.GroupByBox.Appearance = appearance259;
            appearance260.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.GroupByBox.BandLabelAppearance = appearance260;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance261.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance261.BackColor2 = System.Drawing.SystemColors.Control;
            appearance261.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance261.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.GroupByBox.PromptAppearance = appearance261;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.MaxColScrollRegions = 1;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.MaxRowScrollRegions = 1;
            appearance262.BackColor = System.Drawing.SystemColors.Window;
            appearance262.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.Override.ActiveCellAppearance = appearance262;
            appearance263.BackColor = System.Drawing.SystemColors.Highlight;
            appearance263.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.Override.ActiveRowAppearance = appearance263;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance264.BackColor = System.Drawing.SystemColors.Window;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.Override.CardAreaAppearance = appearance264;
            appearance265.BorderColor = System.Drawing.Color.Silver;
            appearance265.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.Override.CellAppearance = appearance265;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.Override.CellPadding = 0;
            appearance266.BackColor = System.Drawing.SystemColors.Control;
            appearance266.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance266.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance266.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance266.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.Override.GroupByRowAppearance = appearance266;
            appearance267.TextHAlignAsString = "Left";
            this.cbcTCKHAC_DocTienLe.DisplayLayout.Override.HeaderAppearance = appearance267;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance268.BackColor = System.Drawing.SystemColors.Window;
            appearance268.BorderColor = System.Drawing.Color.Silver;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.Override.RowAppearance = appearance268;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance269.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.Override.TemplateAddRowAppearance = appearance269;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbcTCKHAC_DocTienLe.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbcTCKHAC_DocTienLe.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbcTCKHAC_DocTienLe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbcTCKHAC_DocTienLe.Location = new System.Drawing.Point(182, 31);
            this.cbcTCKHAC_DocTienLe.Name = "cbcTCKHAC_DocTienLe";
            this.cbcTCKHAC_DocTienLe.Size = new System.Drawing.Size(349, 24);
            this.cbcTCKHAC_DocTienLe.TabIndex = 26;
            // 
            // ultraLabel95
            // 
            appearance270.BackColor = System.Drawing.Color.Transparent;
            appearance270.TextHAlignAsString = "Left";
            appearance270.TextVAlignAsString = "Middle";
            this.ultraLabel95.Appearance = appearance270;
            this.ultraLabel95.AutoSize = true;
            this.ultraLabel95.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel95.Location = new System.Drawing.Point(17, 42);
            this.ultraLabel95.Name = "ultraLabel95";
            this.ultraLabel95.Size = new System.Drawing.Size(106, 14);
            this.ultraLabel95.TabIndex = 18;
            this.ultraLabel95.Text = "Cách đọc số tiền lẻ :";
            // 
            // ultraGroupBox26
            // 
            appearance272.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox26.Appearance = appearance272;
            this.ultraGroupBox26.ContentPadding.Top = 10;
            this.ultraGroupBox26.Controls.Add(this.ultraLabel33);
            this.ultraGroupBox26.Controls.Add(this.cbcTCKHAC_PPTinhGiaXQuy);
            this.ultraGroupBox26.Controls.Add(this.cbcTCKHAC_TKCLechLai);
            this.ultraGroupBox26.Controls.Add(this.cbcTCKHAC_TKCLechLo);
            this.ultraGroupBox26.Controls.Add(this.ultraLabel35);
            this.ultraGroupBox26.Controls.Add(this.ultraLabel37);
            this.ultraGroupBox26.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance312.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox26.HeaderAppearance = appearance312;
            this.ultraGroupBox26.Location = new System.Drawing.Point(0, 214);
            this.ultraGroupBox26.Name = "ultraGroupBox26";
            this.ultraGroupBox26.Size = new System.Drawing.Size(1218, 82);
            this.ultraGroupBox26.TabIndex = 25;
            this.ultraGroupBox26.Text = "Tỷ giá xuất quỹ";
            this.ultraGroupBox26.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel33
            // 
            appearance273.BackColor = System.Drawing.Color.Transparent;
            appearance273.TextHAlignAsString = "Left";
            appearance273.TextVAlignAsString = "Middle";
            this.ultraLabel33.Appearance = appearance273;
            this.ultraLabel33.AutoSize = true;
            this.ultraLabel33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel33.Location = new System.Drawing.Point(568, 51);
            this.ultraLabel33.Name = "ultraLabel33";
            this.ultraLabel33.Size = new System.Drawing.Size(121, 14);
            this.ultraLabel33.TabIndex = 8;
            this.ultraLabel33.Text = "TK xử lý chênh lệch lỗ :";
            // 
            // cbcTCKHAC_PPTinhGiaXQuy
            // 
            appearance274.BackColor = System.Drawing.SystemColors.Window;
            appearance274.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.Appearance = appearance274;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance275.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance275.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance275.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance275.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.GroupByBox.Appearance = appearance275;
            appearance276.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.GroupByBox.BandLabelAppearance = appearance276;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance277.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance277.BackColor2 = System.Drawing.SystemColors.Control;
            appearance277.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance277.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.GroupByBox.PromptAppearance = appearance277;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.MaxColScrollRegions = 1;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.MaxRowScrollRegions = 1;
            appearance278.BackColor = System.Drawing.SystemColors.Window;
            appearance278.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.Override.ActiveCellAppearance = appearance278;
            appearance279.BackColor = System.Drawing.SystemColors.Highlight;
            appearance279.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.Override.ActiveRowAppearance = appearance279;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance280.BackColor = System.Drawing.SystemColors.Window;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.Override.CardAreaAppearance = appearance280;
            appearance281.BorderColor = System.Drawing.Color.Silver;
            appearance281.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.Override.CellAppearance = appearance281;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.Override.CellPadding = 0;
            appearance282.BackColor = System.Drawing.SystemColors.Control;
            appearance282.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance282.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance282.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance282.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.Override.GroupByRowAppearance = appearance282;
            appearance283.TextHAlignAsString = "Left";
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.Override.HeaderAppearance = appearance283;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance284.BackColor = System.Drawing.SystemColors.Window;
            appearance284.BorderColor = System.Drawing.Color.Silver;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.Override.RowAppearance = appearance284;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance285.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.Override.TemplateAddRowAppearance = appearance285;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbcTCKHAC_PPTinhGiaXQuy.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbcTCKHAC_PPTinhGiaXQuy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbcTCKHAC_PPTinhGiaXQuy.Location = new System.Drawing.Point(182, 25);
            this.cbcTCKHAC_PPTinhGiaXQuy.Name = "cbcTCKHAC_PPTinhGiaXQuy";
            this.cbcTCKHAC_PPTinhGiaXQuy.Size = new System.Drawing.Size(349, 24);
            this.cbcTCKHAC_PPTinhGiaXQuy.TabIndex = 11;
            // 
            // cbcTCKHAC_TKCLechLai
            // 
            appearance286.BackColor = System.Drawing.SystemColors.Window;
            appearance286.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.Appearance = appearance286;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance287.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance287.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance287.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance287.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.GroupByBox.Appearance = appearance287;
            appearance288.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.GroupByBox.BandLabelAppearance = appearance288;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance289.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance289.BackColor2 = System.Drawing.SystemColors.Control;
            appearance289.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance289.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.GroupByBox.PromptAppearance = appearance289;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.MaxColScrollRegions = 1;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.MaxRowScrollRegions = 1;
            appearance290.BackColor = System.Drawing.SystemColors.Window;
            appearance290.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.Override.ActiveCellAppearance = appearance290;
            appearance291.BackColor = System.Drawing.SystemColors.Highlight;
            appearance291.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.Override.ActiveRowAppearance = appearance291;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance292.BackColor = System.Drawing.SystemColors.Window;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.Override.CardAreaAppearance = appearance292;
            appearance293.BorderColor = System.Drawing.Color.Silver;
            appearance293.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.Override.CellAppearance = appearance293;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.Override.CellPadding = 0;
            appearance294.BackColor = System.Drawing.SystemColors.Control;
            appearance294.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance294.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance294.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance294.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.Override.GroupByRowAppearance = appearance294;
            appearance295.TextHAlignAsString = "Left";
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.Override.HeaderAppearance = appearance295;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance296.BackColor = System.Drawing.SystemColors.Window;
            appearance296.BorderColor = System.Drawing.Color.Silver;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.Override.RowAppearance = appearance296;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance297.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.Override.TemplateAddRowAppearance = appearance297;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbcTCKHAC_TKCLechLai.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbcTCKHAC_TKCLechLai.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbcTCKHAC_TKCLechLai.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbcTCKHAC_TKCLechLai.Location = new System.Drawing.Point(182, 49);
            this.cbcTCKHAC_TKCLechLai.Name = "cbcTCKHAC_TKCLechLai";
            this.cbcTCKHAC_TKCLechLai.Size = new System.Drawing.Size(349, 24);
            this.cbcTCKHAC_TKCLechLai.TabIndex = 11;
            // 
            // cbcTCKHAC_TKCLechLo
            // 
            appearance298.BackColor = System.Drawing.SystemColors.Window;
            appearance298.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.Appearance = appearance298;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance299.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance299.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance299.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance299.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.GroupByBox.Appearance = appearance299;
            appearance300.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.GroupByBox.BandLabelAppearance = appearance300;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance301.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance301.BackColor2 = System.Drawing.SystemColors.Control;
            appearance301.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance301.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.GroupByBox.PromptAppearance = appearance301;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.MaxColScrollRegions = 1;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.MaxRowScrollRegions = 1;
            appearance302.BackColor = System.Drawing.SystemColors.Window;
            appearance302.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.Override.ActiveCellAppearance = appearance302;
            appearance303.BackColor = System.Drawing.SystemColors.Highlight;
            appearance303.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.Override.ActiveRowAppearance = appearance303;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance304.BackColor = System.Drawing.SystemColors.Window;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.Override.CardAreaAppearance = appearance304;
            appearance305.BorderColor = System.Drawing.Color.Silver;
            appearance305.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.Override.CellAppearance = appearance305;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.Override.CellPadding = 0;
            appearance306.BackColor = System.Drawing.SystemColors.Control;
            appearance306.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance306.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance306.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance306.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.Override.GroupByRowAppearance = appearance306;
            appearance307.TextHAlignAsString = "Left";
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.Override.HeaderAppearance = appearance307;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance308.BackColor = System.Drawing.SystemColors.Window;
            appearance308.BorderColor = System.Drawing.Color.Silver;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.Override.RowAppearance = appearance308;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance309.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.Override.TemplateAddRowAppearance = appearance309;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbcTCKHAC_TKCLechLo.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbcTCKHAC_TKCLechLo.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbcTCKHAC_TKCLechLo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbcTCKHAC_TKCLechLo.Location = new System.Drawing.Point(710, 49);
            this.cbcTCKHAC_TKCLechLo.Name = "cbcTCKHAC_TKCLechLo";
            this.cbcTCKHAC_TKCLechLo.Size = new System.Drawing.Size(349, 24);
            this.cbcTCKHAC_TKCLechLo.TabIndex = 11;
            // 
            // ultraLabel35
            // 
            appearance310.BackColor = System.Drawing.Color.Transparent;
            appearance310.TextHAlignAsString = "Left";
            appearance310.TextVAlignAsString = "Middle";
            this.ultraLabel35.Appearance = appearance310;
            this.ultraLabel35.AutoSize = true;
            this.ultraLabel35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel35.Location = new System.Drawing.Point(17, 30);
            this.ultraLabel35.Name = "ultraLabel35";
            this.ultraLabel35.Size = new System.Drawing.Size(129, 14);
            this.ultraLabel35.TabIndex = 7;
            this.ultraLabel35.Text = "Phương pháp tính tỷ giá :";
            // 
            // ultraLabel37
            // 
            appearance311.BackColor = System.Drawing.Color.Transparent;
            appearance311.TextHAlignAsString = "Left";
            appearance311.TextVAlignAsString = "Middle";
            this.ultraLabel37.Appearance = appearance311;
            this.ultraLabel37.AutoSize = true;
            this.ultraLabel37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel37.Location = new System.Drawing.Point(17, 55);
            this.ultraLabel37.Name = "ultraLabel37";
            this.ultraLabel37.Size = new System.Drawing.Size(123, 14);
            this.ultraLabel37.TabIndex = 7;
            this.ultraLabel37.Text = "TK xử lý chênh lệch lãi :";
            // 
            // ultraGroupBox25
            // 
            appearance313.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox25.Appearance = appearance313;
            this.ultraGroupBox25.ContentPadding.Top = 10;
            this.ultraGroupBox25.Controls.Add(this.chkLimitAccount);
            this.ultraGroupBox25.Controls.Add(this.chkIsMinimized);
            this.ultraGroupBox25.Controls.Add(this.cbcPPTTGTGT);
            this.ultraGroupBox25.Controls.Add(this.ultraLabel100);
            this.ultraGroupBox25.Controls.Add(this.cbcVTHH_NhomNNMD);
            this.ultraGroupBox25.Controls.Add(this.ultraLabel96);
            this.ultraGroupBox25.Controls.Add(this.cbcVTHH_NhomHHDV);
            this.ultraGroupBox25.Controls.Add(this.ultraLabel51);
            this.ultraGroupBox25.Controls.Add(this.clpTCKHAC_MauSoAm);
            this.ultraGroupBox25.Controls.Add(this.clpTCKHAC_MauCTuChuaGS);
            this.ultraGroupBox25.Controls.Add(this.ultraLabel93);
            this.ultraGroupBox25.Controls.Add(this.ultraLabel91);
            this.ultraGroupBox25.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance360.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox25.HeaderAppearance = appearance360;
            this.ultraGroupBox25.Location = new System.Drawing.Point(0, 94);
            this.ultraGroupBox25.Name = "ultraGroupBox25";
            this.ultraGroupBox25.Size = new System.Drawing.Size(1218, 120);
            this.ultraGroupBox25.TabIndex = 23;
            this.ultraGroupBox25.Text = "Màn hình hiển thị ";
            this.ultraGroupBox25.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // chkLimitAccount
            // 
            appearance314.BackColor = System.Drawing.Color.Transparent;
            appearance314.FontData.BoldAsString = "False";
            this.chkLimitAccount.Appearance = appearance314;
            this.chkLimitAccount.BackColor = System.Drawing.Color.Transparent;
            this.chkLimitAccount.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkLimitAccount.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkLimitAccount.Location = new System.Drawing.Point(334, 82);
            this.chkLimitAccount.Name = "chkLimitAccount";
            this.chkLimitAccount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkLimitAccount.Size = new System.Drawing.Size(197, 16);
            this.chkLimitAccount.TabIndex = 52;
            this.chkLimitAccount.Text = "Hạn chế tài khoản khi nhập liệu";
            // 
            // chkIsMinimized
            // 
            appearance315.BackColor = System.Drawing.Color.Transparent;
            appearance315.FontData.BoldAsString = "False";
            appearance315.FontData.Name = "Microsoft Sans Serif";
            this.chkIsMinimized.Appearance = appearance315;
            this.chkIsMinimized.BackColor = System.Drawing.Color.Transparent;
            this.chkIsMinimized.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsMinimized.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkIsMinimized.Location = new System.Drawing.Point(17, 82);
            this.chkIsMinimized.Name = "chkIsMinimized";
            this.chkIsMinimized.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkIsMinimized.Size = new System.Drawing.Size(270, 16);
            this.chkIsMinimized.TabIndex = 37;
            this.chkIsMinimized.Text = "Thu nhỏ giao diện nhập chứng từ";
            // 
            // cbcPPTTGTGT
            // 
            appearance316.FontData.BoldAsString = "False";
            this.cbcPPTTGTGT.Appearance = appearance316;
            appearance317.BackColor = System.Drawing.SystemColors.Window;
            appearance317.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbcPPTTGTGT.DisplayLayout.Appearance = appearance317;
            this.cbcPPTTGTGT.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbcPPTTGTGT.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance318.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance318.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance318.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance318.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcPPTTGTGT.DisplayLayout.GroupByBox.Appearance = appearance318;
            appearance319.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcPPTTGTGT.DisplayLayout.GroupByBox.BandLabelAppearance = appearance319;
            this.cbcPPTTGTGT.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance320.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance320.BackColor2 = System.Drawing.SystemColors.Control;
            appearance320.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance320.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcPPTTGTGT.DisplayLayout.GroupByBox.PromptAppearance = appearance320;
            this.cbcPPTTGTGT.DisplayLayout.MaxColScrollRegions = 1;
            this.cbcPPTTGTGT.DisplayLayout.MaxRowScrollRegions = 1;
            appearance321.BackColor = System.Drawing.SystemColors.Window;
            appearance321.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbcPPTTGTGT.DisplayLayout.Override.ActiveCellAppearance = appearance321;
            appearance322.BackColor = System.Drawing.SystemColors.Highlight;
            appearance322.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbcPPTTGTGT.DisplayLayout.Override.ActiveRowAppearance = appearance322;
            this.cbcPPTTGTGT.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbcPPTTGTGT.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance323.BackColor = System.Drawing.SystemColors.Window;
            this.cbcPPTTGTGT.DisplayLayout.Override.CardAreaAppearance = appearance323;
            appearance324.BorderColor = System.Drawing.Color.Silver;
            appearance324.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbcPPTTGTGT.DisplayLayout.Override.CellAppearance = appearance324;
            this.cbcPPTTGTGT.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbcPPTTGTGT.DisplayLayout.Override.CellPadding = 0;
            appearance325.BackColor = System.Drawing.SystemColors.Control;
            appearance325.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance325.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance325.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance325.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcPPTTGTGT.DisplayLayout.Override.GroupByRowAppearance = appearance325;
            appearance326.TextHAlignAsString = "Left";
            this.cbcPPTTGTGT.DisplayLayout.Override.HeaderAppearance = appearance326;
            this.cbcPPTTGTGT.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbcPPTTGTGT.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance327.BackColor = System.Drawing.SystemColors.Window;
            appearance327.BorderColor = System.Drawing.Color.Silver;
            this.cbcPPTTGTGT.DisplayLayout.Override.RowAppearance = appearance327;
            this.cbcPPTTGTGT.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance328.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbcPPTTGTGT.DisplayLayout.Override.TemplateAddRowAppearance = appearance328;
            this.cbcPPTTGTGT.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbcPPTTGTGT.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbcPPTTGTGT.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbcPPTTGTGT.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbcPPTTGTGT.Enabled = false;
            this.cbcPPTTGTGT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbcPPTTGTGT.Location = new System.Drawing.Point(743, 29);
            this.cbcPPTTGTGT.Name = "cbcPPTTGTGT";
            this.cbcPPTTGTGT.Size = new System.Drawing.Size(316, 24);
            this.cbcPPTTGTGT.TabIndex = 18;
            this.cbcPPTTGTGT.TextChanged += new System.EventHandler(this.cbcPPTTGTGT_TextChanged);
            // 
            // ultraLabel100
            // 
            appearance329.BackColor = System.Drawing.Color.Transparent;
            appearance329.TextHAlignAsString = "Left";
            appearance329.TextVAlignAsString = "Middle";
            this.ultraLabel100.Appearance = appearance329;
            this.ultraLabel100.AutoSize = true;
            this.ultraLabel100.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel100.Location = new System.Drawing.Point(566, 35);
            this.ultraLabel100.Name = "ultraLabel100";
            this.ultraLabel100.Size = new System.Drawing.Size(158, 14);
            this.ultraLabel100.TabIndex = 17;
            this.ultraLabel100.Text = "Phương pháp tính thuế GTGT :";
            // 
            // cbcVTHH_NhomNNMD
            // 
            appearance330.FontData.BoldAsString = "False";
            this.cbcVTHH_NhomNNMD.Appearance = appearance330;
            appearance331.BackColor = System.Drawing.SystemColors.Window;
            appearance331.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbcVTHH_NhomNNMD.DisplayLayout.Appearance = appearance331;
            this.cbcVTHH_NhomNNMD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbcVTHH_NhomNNMD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance332.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance332.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance332.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance332.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcVTHH_NhomNNMD.DisplayLayout.GroupByBox.Appearance = appearance332;
            appearance333.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcVTHH_NhomNNMD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance333;
            this.cbcVTHH_NhomNNMD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance334.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance334.BackColor2 = System.Drawing.SystemColors.Control;
            appearance334.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance334.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcVTHH_NhomNNMD.DisplayLayout.GroupByBox.PromptAppearance = appearance334;
            this.cbcVTHH_NhomNNMD.DisplayLayout.MaxColScrollRegions = 1;
            this.cbcVTHH_NhomNNMD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance335.BackColor = System.Drawing.SystemColors.Window;
            appearance335.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbcVTHH_NhomNNMD.DisplayLayout.Override.ActiveCellAppearance = appearance335;
            appearance336.BackColor = System.Drawing.SystemColors.Highlight;
            appearance336.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbcVTHH_NhomNNMD.DisplayLayout.Override.ActiveRowAppearance = appearance336;
            this.cbcVTHH_NhomNNMD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbcVTHH_NhomNNMD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance337.BackColor = System.Drawing.SystemColors.Window;
            this.cbcVTHH_NhomNNMD.DisplayLayout.Override.CardAreaAppearance = appearance337;
            appearance338.BorderColor = System.Drawing.Color.Silver;
            appearance338.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbcVTHH_NhomNNMD.DisplayLayout.Override.CellAppearance = appearance338;
            this.cbcVTHH_NhomNNMD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbcVTHH_NhomNNMD.DisplayLayout.Override.CellPadding = 0;
            appearance339.BackColor = System.Drawing.SystemColors.Control;
            appearance339.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance339.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance339.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance339.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcVTHH_NhomNNMD.DisplayLayout.Override.GroupByRowAppearance = appearance339;
            appearance340.TextHAlignAsString = "Left";
            this.cbcVTHH_NhomNNMD.DisplayLayout.Override.HeaderAppearance = appearance340;
            this.cbcVTHH_NhomNNMD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbcVTHH_NhomNNMD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance341.BackColor = System.Drawing.SystemColors.Window;
            appearance341.BorderColor = System.Drawing.Color.Silver;
            this.cbcVTHH_NhomNNMD.DisplayLayout.Override.RowAppearance = appearance341;
            this.cbcVTHH_NhomNNMD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance342.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbcVTHH_NhomNNMD.DisplayLayout.Override.TemplateAddRowAppearance = appearance342;
            this.cbcVTHH_NhomNNMD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbcVTHH_NhomNNMD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbcVTHH_NhomNNMD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbcVTHH_NhomNNMD.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbcVTHH_NhomNNMD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbcVTHH_NhomNNMD.Location = new System.Drawing.Point(743, 54);
            this.cbcVTHH_NhomNNMD.Name = "cbcVTHH_NhomNNMD";
            this.cbcVTHH_NhomNNMD.Size = new System.Drawing.Size(316, 24);
            this.cbcVTHH_NhomNNMD.TabIndex = 16;
            // 
            // ultraLabel96
            // 
            appearance343.BackColor = System.Drawing.Color.Transparent;
            appearance343.TextHAlignAsString = "Left";
            appearance343.TextVAlignAsString = "Middle";
            this.ultraLabel96.Appearance = appearance343;
            this.ultraLabel96.AutoSize = true;
            this.ultraLabel96.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel96.Location = new System.Drawing.Point(566, 60);
            this.ultraLabel96.Name = "ultraLabel96";
            this.ultraLabel96.Size = new System.Drawing.Size(152, 14);
            this.ultraLabel96.TabIndex = 15;
            this.ultraLabel96.Text = "Nhóm ngành nghề mặc định :";
            // 
            // cbcVTHH_NhomHHDV
            // 
            appearance344.FontData.BoldAsString = "False";
            this.cbcVTHH_NhomHHDV.Appearance = appearance344;
            appearance345.BackColor = System.Drawing.SystemColors.Window;
            appearance345.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbcVTHH_NhomHHDV.DisplayLayout.Appearance = appearance345;
            this.cbcVTHH_NhomHHDV.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbcVTHH_NhomHHDV.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance346.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance346.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance346.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance346.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcVTHH_NhomHHDV.DisplayLayout.GroupByBox.Appearance = appearance346;
            appearance347.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcVTHH_NhomHHDV.DisplayLayout.GroupByBox.BandLabelAppearance = appearance347;
            this.cbcVTHH_NhomHHDV.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance348.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance348.BackColor2 = System.Drawing.SystemColors.Control;
            appearance348.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance348.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcVTHH_NhomHHDV.DisplayLayout.GroupByBox.PromptAppearance = appearance348;
            this.cbcVTHH_NhomHHDV.DisplayLayout.MaxColScrollRegions = 1;
            this.cbcVTHH_NhomHHDV.DisplayLayout.MaxRowScrollRegions = 1;
            appearance349.BackColor = System.Drawing.SystemColors.Window;
            appearance349.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbcVTHH_NhomHHDV.DisplayLayout.Override.ActiveCellAppearance = appearance349;
            appearance350.BackColor = System.Drawing.SystemColors.Highlight;
            appearance350.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbcVTHH_NhomHHDV.DisplayLayout.Override.ActiveRowAppearance = appearance350;
            this.cbcVTHH_NhomHHDV.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbcVTHH_NhomHHDV.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance351.BackColor = System.Drawing.SystemColors.Window;
            this.cbcVTHH_NhomHHDV.DisplayLayout.Override.CardAreaAppearance = appearance351;
            appearance352.BorderColor = System.Drawing.Color.Silver;
            appearance352.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbcVTHH_NhomHHDV.DisplayLayout.Override.CellAppearance = appearance352;
            this.cbcVTHH_NhomHHDV.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbcVTHH_NhomHHDV.DisplayLayout.Override.CellPadding = 0;
            appearance353.BackColor = System.Drawing.SystemColors.Control;
            appearance353.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance353.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance353.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance353.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcVTHH_NhomHHDV.DisplayLayout.Override.GroupByRowAppearance = appearance353;
            appearance354.TextHAlignAsString = "Left";
            this.cbcVTHH_NhomHHDV.DisplayLayout.Override.HeaderAppearance = appearance354;
            this.cbcVTHH_NhomHHDV.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbcVTHH_NhomHHDV.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance355.BackColor = System.Drawing.SystemColors.Window;
            appearance355.BorderColor = System.Drawing.Color.Silver;
            this.cbcVTHH_NhomHHDV.DisplayLayout.Override.RowAppearance = appearance355;
            this.cbcVTHH_NhomHHDV.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance356.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbcVTHH_NhomHHDV.DisplayLayout.Override.TemplateAddRowAppearance = appearance356;
            this.cbcVTHH_NhomHHDV.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbcVTHH_NhomHHDV.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbcVTHH_NhomHHDV.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbcVTHH_NhomHHDV.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbcVTHH_NhomHHDV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbcVTHH_NhomHHDV.Location = new System.Drawing.Point(743, 54);
            this.cbcVTHH_NhomHHDV.Name = "cbcVTHH_NhomHHDV";
            this.cbcVTHH_NhomHHDV.Size = new System.Drawing.Size(316, 24);
            this.cbcVTHH_NhomHHDV.TabIndex = 14;
            // 
            // ultraLabel51
            // 
            appearance357.BackColor = System.Drawing.Color.Transparent;
            appearance357.TextHAlignAsString = "Left";
            appearance357.TextVAlignAsString = "Middle";
            this.ultraLabel51.Appearance = appearance357;
            this.ultraLabel51.AutoSize = true;
            this.ultraLabel51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel51.Location = new System.Drawing.Point(566, 60);
            this.ultraLabel51.Name = "ultraLabel51";
            this.ultraLabel51.Size = new System.Drawing.Size(152, 14);
            this.ultraLabel51.TabIndex = 13;
            this.ultraLabel51.Text = "Nhóm HHDV mua vào chính :";
            // 
            // clpTCKHAC_MauSoAm
            // 
            this.clpTCKHAC_MauSoAm.Color = System.Drawing.Color.Green;
            this.clpTCKHAC_MauSoAm.DefaultColor = System.Drawing.Color.Red;
            this.clpTCKHAC_MauSoAm.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clpTCKHAC_MauSoAm.Location = new System.Drawing.Point(182, 55);
            this.clpTCKHAC_MauSoAm.Name = "clpTCKHAC_MauSoAm";
            this.clpTCKHAC_MauSoAm.Size = new System.Drawing.Size(349, 23);
            this.clpTCKHAC_MauSoAm.TabIndex = 12;
            this.clpTCKHAC_MauSoAm.Text = "Green";
            // 
            // clpTCKHAC_MauCTuChuaGS
            // 
            this.clpTCKHAC_MauCTuChuaGS.Color = System.Drawing.Color.Purple;
            this.clpTCKHAC_MauCTuChuaGS.DefaultColor = System.Drawing.Color.Green;
            this.clpTCKHAC_MauCTuChuaGS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clpTCKHAC_MauCTuChuaGS.Location = new System.Drawing.Point(182, 32);
            this.clpTCKHAC_MauCTuChuaGS.Name = "clpTCKHAC_MauCTuChuaGS";
            this.clpTCKHAC_MauCTuChuaGS.Size = new System.Drawing.Size(349, 23);
            this.clpTCKHAC_MauCTuChuaGS.TabIndex = 12;
            this.clpTCKHAC_MauCTuChuaGS.Text = "Purple";
            this.clpTCKHAC_MauCTuChuaGS.ColorChanged += new System.EventHandler(this.clpTCKHAC_MauCTuChuaGS_ColorChanged);
            // 
            // ultraLabel93
            // 
            appearance358.BackColor = System.Drawing.Color.Transparent;
            appearance358.TextHAlignAsString = "Left";
            appearance358.TextVAlignAsString = "Middle";
            this.ultraLabel93.Appearance = appearance358;
            this.ultraLabel93.AutoSize = true;
            this.ultraLabel93.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel93.Location = new System.Drawing.Point(17, 37);
            this.ultraLabel93.Name = "ultraLabel93";
            this.ultraLabel93.Size = new System.Drawing.Size(144, 14);
            this.ultraLabel93.TabIndex = 7;
            this.ultraLabel93.Text = "Chứng từ chưa được ghi sổ :";
            // 
            // ultraLabel91
            // 
            appearance359.BackColor = System.Drawing.Color.Transparent;
            appearance359.TextHAlignAsString = "Left";
            appearance359.TextVAlignAsString = "Middle";
            this.ultraLabel91.Appearance = appearance359;
            this.ultraLabel91.AutoSize = true;
            this.ultraLabel91.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel91.Location = new System.Drawing.Point(17, 60);
            this.ultraLabel91.Name = "ultraLabel91";
            this.ultraLabel91.Size = new System.Drawing.Size(109, 14);
            this.ultraLabel91.TabIndex = 7;
            this.ultraLabel91.Text = "Số âm trên báo cáo :";
            // 
            // ultraGroupBox24
            // 
            appearance361.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox24.Appearance = appearance361;
            this.ultraGroupBox24.ContentPadding.Top = 10;
            this.ultraGroupBox24.Controls.Add(this.dateApplicationStartDate);
            this.ultraGroupBox24.Controls.Add(this.ultraLabel103);
            this.ultraGroupBox24.Controls.Add(this.cbcTCKHAC_KieuGiaoDien);
            this.ultraGroupBox24.Controls.Add(this.dateTCKHAC_NgayBDNamTC);
            this.ultraGroupBox24.Controls.Add(this.ultraLabel88);
            this.ultraGroupBox24.Controls.Add(this.ultraLabel90);
            this.ultraGroupBox24.Controls.Add(this.ultraLabel89);
            this.ultraGroupBox24.Controls.Add(this.txtTCKHAC_NamTC);
            this.ultraGroupBox24.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance378.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox24.HeaderAppearance = appearance378;
            this.ultraGroupBox24.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox24.Name = "ultraGroupBox24";
            this.ultraGroupBox24.Size = new System.Drawing.Size(1218, 94);
            this.ultraGroupBox24.TabIndex = 22;
            this.ultraGroupBox24.Text = "Dữ liệu kế toán";
            this.ultraGroupBox24.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // dateApplicationStartDate
            // 
            this.dateApplicationStartDate.Enabled = false;
            this.dateApplicationStartDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateApplicationStartDate.Location = new System.Drawing.Point(248, 64);
            this.dateApplicationStartDate.MaskInput = "{LOC}dd/mm/yyyy";
            this.dateApplicationStartDate.Name = "dateApplicationStartDate";
            this.dateApplicationStartDate.Size = new System.Drawing.Size(283, 23);
            this.dateApplicationStartDate.TabIndex = 13;
            // 
            // ultraLabel103
            // 
            appearance362.BackColor = System.Drawing.Color.Transparent;
            appearance362.TextHAlignAsString = "Left";
            appearance362.TextVAlignAsString = "Middle";
            this.ultraLabel103.Appearance = appearance362;
            this.ultraLabel103.AutoSize = true;
            this.ultraLabel103.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel103.Location = new System.Drawing.Point(17, 70);
            this.ultraLabel103.Name = "ultraLabel103";
            this.ultraLabel103.Size = new System.Drawing.Size(215, 14);
            this.ultraLabel103.TabIndex = 12;
            this.ultraLabel103.Text = "Ngày bắt đầu hoạch toán trên phần mềm :";
            // 
            // cbcTCKHAC_KieuGiaoDien
            // 
            appearance363.BackColor = System.Drawing.SystemColors.Window;
            appearance363.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.Appearance = appearance363;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance364.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance364.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance364.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance364.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.GroupByBox.Appearance = appearance364;
            appearance365.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.GroupByBox.BandLabelAppearance = appearance365;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance366.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance366.BackColor2 = System.Drawing.SystemColors.Control;
            appearance366.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance366.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.GroupByBox.PromptAppearance = appearance366;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.MaxColScrollRegions = 1;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.MaxRowScrollRegions = 1;
            appearance367.BackColor = System.Drawing.SystemColors.Window;
            appearance367.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.Override.ActiveCellAppearance = appearance367;
            appearance368.BackColor = System.Drawing.SystemColors.Highlight;
            appearance368.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.Override.ActiveRowAppearance = appearance368;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance369.BackColor = System.Drawing.SystemColors.Window;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.Override.CardAreaAppearance = appearance369;
            appearance370.BorderColor = System.Drawing.Color.Silver;
            appearance370.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.Override.CellAppearance = appearance370;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.Override.CellPadding = 0;
            appearance371.BackColor = System.Drawing.SystemColors.Control;
            appearance371.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance371.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance371.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance371.BorderColor = System.Drawing.SystemColors.Window;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.Override.GroupByRowAppearance = appearance371;
            appearance372.TextHAlignAsString = "Left";
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.Override.HeaderAppearance = appearance372;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance373.BackColor = System.Drawing.SystemColors.Window;
            appearance373.BorderColor = System.Drawing.Color.Silver;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.Override.RowAppearance = appearance373;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance374.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.Override.TemplateAddRowAppearance = appearance374;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbcTCKHAC_KieuGiaoDien.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbcTCKHAC_KieuGiaoDien.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbcTCKHAC_KieuGiaoDien.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbcTCKHAC_KieuGiaoDien.Location = new System.Drawing.Point(732, 64);
            this.cbcTCKHAC_KieuGiaoDien.Name = "cbcTCKHAC_KieuGiaoDien";
            this.cbcTCKHAC_KieuGiaoDien.Size = new System.Drawing.Size(349, 24);
            this.cbcTCKHAC_KieuGiaoDien.TabIndex = 11;
            this.cbcTCKHAC_KieuGiaoDien.Visible = false;
            // 
            // dateTCKHAC_NgayBDNamTC
            // 
            this.dateTCKHAC_NgayBDNamTC.Enabled = false;
            this.dateTCKHAC_NgayBDNamTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTCKHAC_NgayBDNamTC.Location = new System.Drawing.Point(248, 31);
            this.dateTCKHAC_NgayBDNamTC.MaskInput = "{LOC}dd/mm/yyyy";
            this.dateTCKHAC_NgayBDNamTC.Name = "dateTCKHAC_NgayBDNamTC";
            this.dateTCKHAC_NgayBDNamTC.Size = new System.Drawing.Size(283, 23);
            this.dateTCKHAC_NgayBDNamTC.TabIndex = 10;
            // 
            // ultraLabel88
            // 
            appearance375.BackColor = System.Drawing.Color.Transparent;
            appearance375.TextHAlignAsString = "Left";
            appearance375.TextVAlignAsString = "Middle";
            this.ultraLabel88.Appearance = appearance375;
            this.ultraLabel88.AutoSize = true;
            this.ultraLabel88.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel88.Location = new System.Drawing.Point(567, 70);
            this.ultraLabel88.Name = "ultraLabel88";
            this.ultraLabel88.Size = new System.Drawing.Size(147, 14);
            this.ultraLabel88.TabIndex = 7;
            this.ultraLabel88.Text = "Kiểu giao diện chương trình :";
            this.ultraLabel88.Visible = false;
            // 
            // ultraLabel90
            // 
            appearance376.BackColor = System.Drawing.Color.Transparent;
            appearance376.TextHAlignAsString = "Left";
            appearance376.TextVAlignAsString = "Middle";
            this.ultraLabel90.Appearance = appearance376;
            this.ultraLabel90.AutoSize = true;
            this.ultraLabel90.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel90.Location = new System.Drawing.Point(568, 34);
            this.ultraLabel90.Name = "ultraLabel90";
            this.ultraLabel90.Size = new System.Drawing.Size(79, 14);
            this.ultraLabel90.TabIndex = 8;
            this.ultraLabel90.Text = "Năm tài chính :";
            // 
            // ultraLabel89
            // 
            appearance377.BackColor = System.Drawing.Color.Transparent;
            appearance377.TextHAlignAsString = "Left";
            appearance377.TextVAlignAsString = "Middle";
            this.ultraLabel89.Appearance = appearance377;
            this.ultraLabel89.AutoSize = true;
            this.ultraLabel89.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel89.Location = new System.Drawing.Point(17, 37);
            this.ultraLabel89.Name = "ultraLabel89";
            this.ultraLabel89.Size = new System.Drawing.Size(148, 14);
            this.ultraLabel89.TabIndex = 8;
            this.ultraLabel89.Text = "Ngày bắt đầu năm tài chính :";
            // 
            // txtTCKHAC_NamTC
            // 
            this.txtTCKHAC_NamTC.Enabled = false;
            this.txtTCKHAC_NamTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTCKHAC_NamTC.Location = new System.Drawing.Point(710, 28);
            this.txtTCKHAC_NamTC.Name = "txtTCKHAC_NamTC";
            this.txtTCKHAC_NamTC.Size = new System.Drawing.Size(349, 23);
            this.txtTCKHAC_NamTC.TabIndex = 9;
            // 
            // ButApDung
            // 
            appearance379.Image = global::Accounting.Properties.Resources.ubtnComeBack;
            this.ButApDung.Appearance = appearance379;
            this.ButApDung.Enabled = false;
            this.ButApDung.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButApDung.Location = new System.Drawing.Point(1137, 616);
            this.ButApDung.Name = "ButApDung";
            this.ButApDung.Size = new System.Drawing.Size(82, 29);
            this.ButApDung.TabIndex = 10;
            this.ButApDung.Text = "Áp dụng";
            this.ButApDung.Click += new System.EventHandler(this.ButApDung_Click);
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl3);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl4);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl5);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl6);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl7);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl8);
            this.ultraTabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(1220, 610);
            this.ultraTabControl1.TabIndex = 0;
            this.ultraTabControl1.TabOrientation = Infragistics.Win.UltraWinTabs.TabOrientation.TopLeft;
            this.ultraTabControl1.TabPadding = new System.Drawing.Size(20, 1);
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "1. Thông tin doanh nghiệp";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "2. Người ký";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "3. Báo cáo";
            ultraTab4.TabPage = this.ultraTabPageControl4;
            ultraTab4.Text = "4. Vật tư , Hàng hóa";
            ultraTab5.TabPage = this.ultraTabPageControl5;
            ultraTab5.Text = "5. Tiền lương";
            ultraTab6.TabPage = this.ultraTabPageControl6;
            ultraTab6.Text = "6. Định dạng số";
            ultraTab7.TabPage = this.ultraTabPageControl7;
            ultraTab7.Text = "7. Sao lưu";
            ultraTab8.TabPage = this.ultraTabPageControl8;
            ultraTab8.Text = "8. Thiết lập khác";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3,
            ultraTab4,
            ultraTab5,
            ultraTab6,
            ultraTab7,
            ultraTab8});
            this.ultraTabControl1.TabSize = new System.Drawing.Size(0, 40);
            this.ultraTabControl1.TextOrientation = Infragistics.Win.UltraWinTabs.TextOrientation.Horizontal;
            this.ultraTabControl1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1218, 568);
            // 
            // ButHuyBo
            // 
            appearance380.Image = global::Accounting.Properties.Resources.cancel_16;
            this.ButHuyBo.Appearance = appearance380;
            this.ButHuyBo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButHuyBo.Location = new System.Drawing.Point(1049, 616);
            this.ButHuyBo.Name = "ButHuyBo";
            this.ButHuyBo.Size = new System.Drawing.Size(82, 29);
            this.ButHuyBo.TabIndex = 11;
            this.ButHuyBo.Text = "Hủy bỏ";
            this.ButHuyBo.Click += new System.EventHandler(this.ButHuyBo_Click);
            // 
            // ButDongY
            // 
            appearance381.Image = global::Accounting.Properties.Resources.apply_32;
            this.ButDongY.Appearance = appearance381;
            this.ButDongY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButDongY.Location = new System.Drawing.Point(961, 616);
            this.ButDongY.Name = "ButDongY";
            this.ButDongY.Size = new System.Drawing.Size(82, 29);
            this.ButDongY.TabIndex = 12;
            this.ButDongY.Text = "Đồng ý";
            this.ButDongY.Click += new System.EventHandler(this.ButDongY_Click);
            // 
            // FSystemOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1228, 652);
            this.Controls.Add(this.ButApDung);
            this.Controls.Add(this.ultraTabControl1);
            this.Controls.Add(this.ButHuyBo);
            this.Controls.Add(this.ButDongY);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FSystemOption";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thiết Lập Tùy Chọn";
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            this.ultraGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbcTTDN_SoTKNH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_Nganhang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ultraGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDN_TTDailythue)).EndInit();
            this.ultraPanelDaiLyThue.ClientArea.ResumeLayout(false);
            this.ultraPanelDaiLyThue.ClientArea.PerformLayout();
            this.ultraPanelDaiLyThue.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateTTDN_NgayDLT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_DiachiDLT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_TenDLT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_NVDLT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_DienthoaiDLT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_FaxDLT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_MSTDLT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_HDDLso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_CCHNDLT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_QuanDLT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_TinhDLT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTTDN_TTDvichuquan)).EndInit();
            this.ultraPanelDonViChuQuan.ClientArea.ResumeLayout(false);
            this.ultraPanelDonViChuQuan.ClientArea.PerformLayout();
            this.ultraPanelDonViChuQuan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_Tendvichuquan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_MSTdvichuquan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProductID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_Tencty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_Diachi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTTDN_MST)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).EndInit();
            this.ultraGroupBox6.ResumeLayout(false);
            this.ultraGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbcNKY_vitrikyDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNKY_NoiluukyDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.ultraGroupBox5.ResumeLayout(false);
            this.ultraGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkNKY_Inten)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNKY_Giamdoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNKY_Thukho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNKY_Nguoilapbieu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNKY_Ketoantruong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNKY_Thuquy)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox9)).EndInit();
            this.ultraGroupBox9.ResumeLayout(false);
            this.ultraGroupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBC_LapTDeBC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBC_CongGopBtoan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBC_KoInCtu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BssssC_HthiFax)).EndInit();
            this.BssssC_HthiFax.ResumeLayout(false);
            this.BssssC_HthiFax.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbcBC_VtriTtinDN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optHienThi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBC_HthiEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBC_HthiFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBC_HthiDThoai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).EndInit();
            this.ultraGroupBox7.ResumeLayout(false);
            this.ultraGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBC_FontTencty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBC_FontDChi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBC_FontTenchuquan)).EndInit();
            this.ultraTabPageControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox28)).EndInit();
            this.ultraGroupBox28.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox14)).EndInit();
            this.ultraGroupBox14.ResumeLayout(false);
            this.ultraGroupBox14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllowInputCostUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVTHH_ChoXuatQuaLuongTon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVTHH_CBaoXuatQuaLuongTon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcVTHH_KhoMacDinh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox13)).EndInit();
            this.ultraGroupBox13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.opttuychonkhilaphoadon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox12)).EndInit();
            this.ultraGroupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optcachtinhgia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox11)).EndInit();
            this.ultraGroupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox10)).EndInit();
            this.ultraGroupBox10.ResumeLayout(false);
            this.ultraGroupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbcVTHH_PPTinhGiaXKho)).EndInit();
            this.ultraTabPageControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox18)).EndInit();
            this.ultraGroupBox18.ResumeLayout(false);
            this.ultraGroupBox18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTL_LamThemCN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox17)).EndInit();
            this.ultraGroupBox17.ResumeLayout(false);
            this.ultraGroupBox17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox16)).EndInit();
            this.ultraGroupBox16.ResumeLayout(false);
            this.ultraGroupBox16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTL_LayTUlenBLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox15)).EndInit();
            this.ultraGroupBox15.ResumeLayout(false);
            this.ultraGroupBox15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optLoaiDT)).EndInit();
            this.ultraTabPageControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox21)).EndInit();
            this.ultraGroupBox21.ResumeLayout(false);
            this.ultraGroupBox21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDDSo_NCachHangNghin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDDSo_NCachHangDVi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox20)).EndInit();
            this.ultraGroupBox20.ResumeLayout(false);
            this.ultraGroupBox20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbcDDSo_SoAm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcDDSo_KyHieuTien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox19)).EndInit();
            this.ultraGroupBox19.ResumeLayout(false);
            this.ultraGroupBox19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDDSo_DonGiaNT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDDSo_TyLePBo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDDSo_TyLe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDDSo_TyGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDDSo_SoLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDDSo_DonGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDDSo_NgoaiTe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDDSo_TienVND)).EndInit();
            this.ultraTabPageControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox22)).EndInit();
            this.ultraGroupBox22.ResumeLayout(false);
            this.ultraGroupBox22.PerformLayout();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ClientArea.PerformLayout();
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numSLUU_NgaySLUU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.opthinhthucsaoluu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSLUU_TMSaoLuu)).EndInit();
            this.ultraTabPageControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox8)).EndInit();
            this.ultraGroupBox8.ResumeLayout(false);
            this.ultraGroupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailForgotPass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optPBCCDC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTCKHAC_SUDUNGTHEMSOQUANTRI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHDDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTCKHAC_DocTienBangChu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTCKHAC_CBKoChonNVBH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTCKHAC_ChepDL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTCKHAC_CPKoHLy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTCKHAC_ChiQuaTonQuy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTCKHAC_SinhTK007)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.opt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcTCKHAC_ChuHThiInHDLan2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcTCKHAC_DocTienLe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox26)).EndInit();
            this.ultraGroupBox26.ResumeLayout(false);
            this.ultraGroupBox26.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbcTCKHAC_PPTinhGiaXQuy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcTCKHAC_TKCLechLai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcTCKHAC_TKCLechLo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox25)).EndInit();
            this.ultraGroupBox25.ResumeLayout(false);
            this.ultraGroupBox25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkLimitAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsMinimized)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcPPTTGTGT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcVTHH_NhomNNMD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcVTHH_NhomHHDV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clpTCKHAC_MauSoAm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clpTCKHAC_MauCTuChuaGS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox24)).EndInit();
            this.ultraGroupBox24.ResumeLayout(false);
            this.ultraGroupBox24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateApplicationStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbcTCKHAC_KieuGiaoDien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTCKHAC_NgayBDNamTC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTCKHAC_NamTC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl5;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl6;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl7;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTTDN_MST;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTTDN_Diachi;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTTDN_Tencty;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTTDN_MSTdvichuquan;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTTDN_Tendvichuquan;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkTTDN_TTDailythue;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTTDN_DiachiDLT;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTTDN_MSTDLT;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTTDN_TenDLT;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTTDN_CCHNDLT;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTTDN_HDDLso;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTTDN_NVDLT;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTTDN_DienthoaiDLT;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTTDN_QuanDLT;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTTDN_Nganhang;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNKY_Giamdoc;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNKY_Ketoantruong;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNKY_Thuquy;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNKY_Thukho;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNKY_Nguoilapbieu;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel27;
        private Infragistics.Win.Misc.UltraLabel ultraLabel28;
        private Infragistics.Win.Misc.UltraLabel ultraLabel26;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkNKY_Inten;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNKY_NoiluukyDT;
        private Infragistics.Win.Misc.UltraLabel ultraLabel30;
        private Infragistics.Win.Misc.UltraLabel ultraLabel29;
        private Infragistics.Win.Misc.UltraLabel ultraLabel32;
        private Infragistics.Win.Misc.UltraLabel ultraLabel31;
        private Infragistics.Win.Misc.UltraButton ButNK_ChonNoiLuTep;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTTDN_TinhDLT;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTTDN_FaxDLT;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dateTTDN_NgayDLT;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkTTDN_TTDvichuquan;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBC_FontTenchuquan;
        private Infragistics.Win.Misc.UltraLabel ultraLabel34;
        private Infragistics.Win.Misc.UltraLabel ultraLabel36;
        private Infragistics.Win.Misc.UltraLabel ultraLabel38;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBC_FontTencty;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBC_FontDChi;
        private Infragistics.Win.Misc.UltraButton ButChonFontTenCongTy;
        private Infragistics.Win.Misc.UltraButton ButFontDiaChi;
        private Infragistics.Win.Misc.UltraButton ButFontDonViChuQuan;
        private Infragistics.Win.Misc.UltraGroupBox BssssC_HthiFax;
        private Infragistics.Win.Misc.UltraLabel ultraLabel39;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkBC_HthiDThoai;
        private Infragistics.Win.Misc.UltraLabel BC_HthiDThoai;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkBC_HthiEmail;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkBC_HthiFax;
        private Infragistics.Win.Misc.UltraLabel ultraLabel42;
        private Infragistics.Win.Misc.UltraLabel ultraLabel41;
        private Infragistics.Win.Misc.UltraLabel ultraLabel43;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel46;
        private Infragistics.Win.Misc.UltraLabel ultraLabel44;
        private Infragistics.Win.Misc.UltraLabel ultraLabel45;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkBC_LapTDeBC;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkBC_CongGopBtoan;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkBC_KoInCtu;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox11;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel47;
        private Infragistics.Win.Misc.UltraLabel ultraLabel48;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox ultraPictureBox1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel49;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet optGia;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox13;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet opttuychonkhilaphoadon;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox12;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet optcachtinhgia;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox14;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbcVTHH_KhoMacDinh;
        private Infragistics.Win.Misc.UltraLabel ultraLabel50;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkVTHH_ChoXuatQuaLuongTon;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkVTHH_CBaoXuatQuaLuongTon;
        private Infragistics.Win.Misc.UltraLabel ultraLabel53;
        private Infragistics.Win.Misc.UltraButton ButApDung;
        private Infragistics.Win.Misc.UltraButton ButHuyBo;
        private Infragistics.Win.Misc.UltraButton ButDongY;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox18;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkTL_LamThemCN;
        private Infragistics.Win.Misc.UltraLabel ultraLabel67;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox16;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkTL_LayTUlenBLuong;
        private Infragistics.Win.Misc.UltraLabel ultraLabel59;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel54;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox21;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox20;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel65;
        private Infragistics.Win.Misc.UltraLabel ultraLabel66;
        private Infragistics.Win.Misc.UltraLabel ultraLabel68;
        private Infragistics.Win.Misc.UltraLabel ultraLabel69;
        private Infragistics.Win.Misc.UltraLabel ultraLabel70;
        private Infragistics.Win.Misc.UltraLabel ultraLabel71;
        private Infragistics.Win.Misc.UltraLabel ultraLabel72;
        private Infragistics.Win.Misc.UltraLabel ultraLabel73;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbcDDSo_SoAm;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbcDDSo_KyHieuTien;
        private Infragistics.Win.Misc.UltraLabel ultraLabel81;
        private Infragistics.Win.Misc.UltraLabel H;
        private Infragistics.Win.Misc.UltraLabel ultraLabel83;
        private Infragistics.Win.Misc.UltraLabel lblHienThiSoAm;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDDSo_NCachHangNghin;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDDSo_NCachHangDVi;
        private Infragistics.Win.Misc.UltraLabel ultraLabel82;
        private Infragistics.Win.Misc.UltraLabel ultraLabel85;
        private Infragistics.Win.Misc.UltraButton ButDDS_MacDinh;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox22;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSLUU_TMSaoLuu;
        private Infragistics.Win.Misc.UltraLabel ultraLabel87;
        private Infragistics.Win.Misc.UltraButton ButSL_Chon;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet opthinhthucsaoluu;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel88;
        private Infragistics.Win.Misc.UltraLabel ultraLabel89;
        private Infragistics.Win.Misc.UltraLabel ultraLabel90;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTCKHAC_NamTC;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dateTCKHAC_NgayBDNamTC;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbcTCKHAC_KieuGiaoDien;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox25;
        private Infragistics.Win.Misc.UltraLabel ultraLabel91;
        private Infragistics.Win.Misc.UltraLabel ultraLabel93;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogNoiLuuTepDaKy;
        private Infragistics.Win.Misc.UltraLabel lblTienVietNam;
        private Infragistics.Win.Misc.UltraLabel lblSoLuong;
        private Infragistics.Win.Misc.UltraLabel lblTienNgoaiTe;
        private Infragistics.Win.Misc.UltraLabel lblTyGia;
        private Infragistics.Win.Misc.UltraLabel lblDonGia;
        private Infragistics.Win.Misc.UltraLabel lblTyLePhanBo;
        private Infragistics.Win.Misc.UltraLabel lblHeSoTyLe;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor numDDSo_TienVND;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor numDDSo_TyLePBo;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor numDDSo_TyLe;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor numDDSo_TyGia;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor numDDSo_SoLuong;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor numDDSo_DonGia;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor numDDSo_NgoaiTe;
        private Infragistics.Win.Misc.UltraButton ButDDS_XemThu;
        private Infragistics.Win.UltraWinEditors.UltraColorPicker clpTCKHAC_MauSoAm;
        private Infragistics.Win.UltraWinEditors.UltraColorPicker clpTCKHAC_MauCTuChuaGS;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox8;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkTCKHAC_DocTienBangChu;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkTCKHAC_CBKoChonNVBH;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkTCKHAC_ChepDL;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkTCKHAC_CPKoHLy;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkTCKHAC_SinhTK007;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet opt;
        private Infragistics.Win.Misc.UltraLabel ultraLabel94;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbcTCKHAC_ChuHThiInHDLan2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel97;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbcTCKHAC_DocTienLe;
        private Infragistics.Win.Misc.UltraLabel ultraLabel95;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox26;
        private Infragistics.Win.Misc.UltraLabel ultraLabel33;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbcTCKHAC_TKCLechLo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel35;
        private Infragistics.Win.Misc.UltraLabel ultraLabel37;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbcTCKHAC_PPTinhGiaXQuy;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbcTCKHAC_TKCLechLai;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet optHienThi;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit meTCKHAC_MenhGiaCP;
        private Infragistics.Win.Misc.UltraLabel ultraLabel80;
        private Infragistics.Win.Misc.UltraLabel ultraLabel79;
        private Infragistics.Win.Misc.UltraLabel ultraLabel78;
        private Infragistics.Win.Misc.UltraLabel ultraLabel77;
        private Infragistics.Win.Misc.UltraLabel ultraLabel76;
        private Infragistics.Win.Misc.UltraLabel ultraLabel75;
        private Infragistics.Win.Misc.UltraLabel ultraLabel74;
        private Infragistics.Win.Misc.UltraPanel ultraPanelDonViChuQuan;
        private Infragistics.Win.Misc.UltraPanel ultraPanelDaiLyThue;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit meNKY_ChieurongkyDT;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit meNKY_ChieudaikyDT;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbcTTDN_SoTKNH;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbcNKY_vitrikyDT;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbcBC_VtriTtinDN;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbcVTHH_PPTinhGiaXKho;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkTCKHAC_ChiQuaTonQuy;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor numDDSo_DonGiaNT;
        private Infragistics.Win.Misc.UltraLabel lblDonGiaNT;
        private Infragistics.Win.Misc.UltraLabel ultraLabel99;
        private Infragistics.Win.Misc.UltraLabel ultraLabel101;
        private Infragistics.Win.Misc.UltraLabel ultraLabel98;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtProductID;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet optLoaiDT;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox17;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit meTL_BHTNLDong;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit meTL_BHYTLDong;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit meTL_BHXHLDong;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit meTL_KPCDCty;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit meTL_BHTNCty;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit meTL_BHYTCty;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit meTL_BHXHCty;
        private Infragistics.Win.Misc.UltraLabel ultraLabel64;
        private Infragistics.Win.Misc.UltraLabel ultraLabel62;
        private Infragistics.Win.Misc.UltraLabel ultraLabel60;
        private Infragistics.Win.Misc.UltraLabel ultraLabel63;
        private Infragistics.Win.Misc.UltraLabel ultraLabel108;
        private Infragistics.Win.Misc.UltraLabel ultraLabel107;
        private Infragistics.Win.Misc.UltraLabel ultraLabel106;
        private Infragistics.Win.Misc.UltraLabel ultraLabel105;
        private Infragistics.Win.Misc.UltraLabel ultraLabel92;
        private Infragistics.Win.Misc.UltraLabel ultraLabel84;
        private Infragistics.Win.Misc.UltraLabel ultraLabel40;
        private Infragistics.Win.Misc.UltraLabel ultraLabel58;
        private Infragistics.Win.Misc.UltraLabel ultraLabel61;
        private Infragistics.Win.Misc.UltraLabel ultraLabel57;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkHDDT;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet ultraOptionSet1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkTCKHAC_SUDUNGTHEMSOQUANTRI;
        private Infragistics.Win.Misc.UltraLabel ultraLabel56;
        private UltraOptionSet_Ex optPBCCDC;
        private Infragistics.Win.Misc.UltraLabel ultraLabel55;
        private UltraOptionSet_Ex optPB;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox28;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox27;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbcVTHH_NhomNNMD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel96;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbcVTHH_NhomHHDV;
        private Infragistics.Win.Misc.UltraLabel ultraLabel51;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbcPPTTGTGT;
        private Infragistics.Win.Misc.UltraLabel ultraLabel100;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor numSLUU_NgaySLUU;
        private Infragistics.Win.Misc.UltraLabel ultraLabel102;
        private Infragistics.Win.Misc.UltraLabel ultraLabel86;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dateApplicationStartDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel103;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsMinimized;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkLimitAccount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel104;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtEmailForgotPass;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkAllowInputCostUnit;
    }
}