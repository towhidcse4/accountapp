﻿namespace Accounting
{
    partial class UCCP_tab3_
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCCP_tab3_));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ultraButton4 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton3 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.btnDuLieu = new Infragistics.Win.Misc.UltraButton();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Image = ((System.Drawing.Image)(resources.GetObject("label4.Image")));
            this.label4.Location = new System.Drawing.Point(369, 243);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 63);
            this.label4.TabIndex = 68;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.Location = new System.Drawing.Point(571, 334);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 33);
            this.label3.TabIndex = 67;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(357, 333);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 33);
            this.label1.TabIndex = 66;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(143, 334);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 33);
            this.label2.TabIndex = 65;
            // 
            // ultraButton4
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance1.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraButton4.Appearance = appearance1;
            this.ultraButton4.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
            this.ultraButton4.HotTrackAppearance = appearance2;
            this.ultraButton4.ImageSize = new System.Drawing.Size(80, 80);
            this.ultraButton4.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.ultraButton4.Location = new System.Drawing.Point(340, 119);
            this.ultraButton4.Name = "ultraButton4";
            this.ultraButton4.Size = new System.Drawing.Size(92, 97);
            this.ultraButton4.TabIndex = 64;
            // 
            // ultraButton3
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraButton3.Appearance = appearance3;
            this.ultraButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
            this.ultraButton3.HotTrackAppearance = appearance4;
            this.ultraButton3.ImageSize = new System.Drawing.Size(80, 80);
            this.ultraButton3.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.ultraButton3.Location = new System.Drawing.Point(661, 302);
            this.ultraButton3.Name = "ultraButton3";
            this.ultraButton3.Size = new System.Drawing.Size(92, 97);
            this.ultraButton3.TabIndex = 63;
            // 
            // ultraButton2
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance5.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraButton2.Appearance = appearance5;
            this.ultraButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
            this.ultraButton2.HotTrackAppearance = appearance6;
            this.ultraButton2.ImageSize = new System.Drawing.Size(80, 80);
            this.ultraButton2.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.ultraButton2.Location = new System.Drawing.Point(447, 302);
            this.ultraButton2.Name = "ultraButton2";
            this.ultraButton2.Size = new System.Drawing.Size(92, 97);
            this.ultraButton2.TabIndex = 62;
            // 
            // ultraButton1
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.Image = ((object)(resources.GetObject("appearance7.Image")));
            appearance7.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance7.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance7.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraButton1.Appearance = appearance7;
            this.ultraButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance8.Image = ((object)(resources.GetObject("appearance8.Image")));
            this.ultraButton1.HotTrackAppearance = appearance8;
            this.ultraButton1.ImageSize = new System.Drawing.Size(80, 80);
            this.ultraButton1.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.ultraButton1.Location = new System.Drawing.Point(233, 302);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(92, 97);
            this.ultraButton1.TabIndex = 61;
            // 
            // btnDuLieu
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.Image = ((object)(resources.GetObject("appearance9.Image")));
            appearance9.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance9.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance9.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnDuLieu.Appearance = appearance9;
            this.btnDuLieu.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance10.Image = ((object)(resources.GetObject("appearance10.Image")));
            this.btnDuLieu.HotTrackAppearance = appearance10;
            this.btnDuLieu.ImageSize = new System.Drawing.Size(80, 80);
            this.btnDuLieu.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnDuLieu.Location = new System.Drawing.Point(19, 302);
            this.btnDuLieu.Name = "btnDuLieu";
            this.btnDuLieu.Size = new System.Drawing.Size(92, 97);
            this.btnDuLieu.TabIndex = 60;
            // 
            // UCCP_tab3_
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ultraButton4);
            this.Controls.Add(this.ultraButton3);
            this.Controls.Add(this.ultraButton2);
            this.Controls.Add(this.ultraButton1);
            this.Controls.Add(this.btnDuLieu);
            this.Name = "UCCP_tab3_";
            this.Size = new System.Drawing.Size(797, 515);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Infragistics.Win.Misc.UltraButton ultraButton4;
        private Infragistics.Win.Misc.UltraButton ultraButton3;
        private Infragistics.Win.Misc.UltraButton ultraButton2;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.Misc.UltraButton btnDuLieu;
    }
}
