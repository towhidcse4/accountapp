﻿namespace Accounting
{
    partial class UCEM_tab2_
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCEM_tab2_));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            this.btnEMRegistration = new Infragistics.Win.Misc.UltraButton();
            this.btnEMShareHolder = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
            this.btnRDividendPayable = new Infragistics.Win.Misc.UltraButton();
            this.btnEMTransfer = new Infragistics.Win.Misc.UltraButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnEMRegistration
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance1.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnEMRegistration.Appearance = appearance1;
            this.btnEMRegistration.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
            this.btnEMRegistration.HotTrackAppearance = appearance2;
            this.btnEMRegistration.ImageSize = new System.Drawing.Size(80, 80);
            this.btnEMRegistration.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnEMRegistration.Location = new System.Drawing.Point(22, 182);
            this.btnEMRegistration.Name = "btnEMRegistration";
            this.btnEMRegistration.Size = new System.Drawing.Size(92, 97);
            this.btnEMRegistration.TabIndex = 36;
            // 
            // btnEMShareHolder
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnEMShareHolder.Appearance = appearance3;
            this.btnEMShareHolder.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
            this.btnEMShareHolder.HotTrackAppearance = appearance4;
            this.btnEMShareHolder.ImageSize = new System.Drawing.Size(80, 80);
            this.btnEMShareHolder.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnEMShareHolder.Location = new System.Drawing.Point(220, 182);
            this.btnEMShareHolder.Name = "btnEMShareHolder";
            this.btnEMShareHolder.Size = new System.Drawing.Size(92, 97);
            this.btnEMShareHolder.TabIndex = 37;
            // 
            // ultraButton2
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance5.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraButton2.Appearance = appearance5;
            this.ultraButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
            this.ultraButton2.HotTrackAppearance = appearance6;
            this.ultraButton2.ImageSize = new System.Drawing.Size(80, 80);
            this.ultraButton2.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.ultraButton2.Location = new System.Drawing.Point(418, 182);
            this.ultraButton2.Name = "ultraButton2";
            this.ultraButton2.Size = new System.Drawing.Size(92, 97);
            this.ultraButton2.TabIndex = 38;
            // 
            // btnRDividendPayable
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.Image = ((object)(resources.GetObject("appearance7.Image")));
            appearance7.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance7.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance7.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnRDividendPayable.Appearance = appearance7;
            this.btnRDividendPayable.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance8.Image = ((object)(resources.GetObject("appearance8.Image")));
            this.btnRDividendPayable.HotTrackAppearance = appearance8;
            this.btnRDividendPayable.ImageSize = new System.Drawing.Size(80, 80);
            this.btnRDividendPayable.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnRDividendPayable.Location = new System.Drawing.Point(616, 182);
            this.btnRDividendPayable.Name = "btnRDividendPayable";
            this.btnRDividendPayable.Size = new System.Drawing.Size(92, 97);
            this.btnRDividendPayable.TabIndex = 39;
            // 
            // btnEMTransfer
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.Image = ((object)(resources.GetObject("appearance9.Image")));
            appearance9.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance9.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance9.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnEMTransfer.Appearance = appearance9;
            this.btnEMTransfer.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance10.Image = ((object)(resources.GetObject("appearance10.Image")));
            this.btnEMTransfer.HotTrackAppearance = appearance10;
            this.btnEMTransfer.ImageSize = new System.Drawing.Size(80, 80);
            this.btnEMTransfer.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnEMTransfer.Location = new System.Drawing.Point(319, 376);
            this.btnEMTransfer.Name = "btnEMTransfer";
            this.btnEMTransfer.Size = new System.Drawing.Size(92, 97);
            this.btnEMTransfer.TabIndex = 40;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(138, 214);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 33);
            this.label2.TabIndex = 44;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(336, 214);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 33);
            this.label1.TabIndex = 45;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.Location = new System.Drawing.Point(534, 214);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 33);
            this.label3.TabIndex = 46;
            // 
            // label5
            // 
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Image = ((System.Drawing.Image)(resources.GetObject("label5.Image")));
            this.label5.Location = new System.Drawing.Point(349, 296);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 63);
            this.label5.TabIndex = 47;
            // 
            // UCEM_tab2_
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnEMTransfer);
            this.Controls.Add(this.btnRDividendPayable);
            this.Controls.Add(this.ultraButton2);
            this.Controls.Add(this.btnEMShareHolder);
            this.Controls.Add(this.btnEMRegistration);
            this.Name = "UCEM_tab2_";
            this.Size = new System.Drawing.Size(731, 517);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnEMRegistration;
        private Infragistics.Win.Misc.UltraButton btnEMShareHolder;
        private Infragistics.Win.Misc.UltraButton ultraButton2;
        private Infragistics.Win.Misc.UltraButton btnRDividendPayable;
        private Infragistics.Win.Misc.UltraButton btnEMTransfer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
    }
}
