﻿namespace Accounting
{
    partial class UCEM_tab3_
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCEM_tab3_));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            this.btnEMEstimate = new Infragistics.Win.Misc.UltraButton();
            this.btnEMAllocation = new Infragistics.Win.Misc.UltraButton();
            this.btnRBUAllocationAndUse = new Infragistics.Win.Misc.UltraButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnEMEstimate
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance1.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnEMEstimate.Appearance = appearance1;
            this.btnEMEstimate.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
            this.btnEMEstimate.HotTrackAppearance = appearance2;
            this.btnEMEstimate.ImageSize = new System.Drawing.Size(80, 80);
            this.btnEMEstimate.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnEMEstimate.Location = new System.Drawing.Point(37, 202);
            this.btnEMEstimate.Name = "btnEMEstimate";
            this.btnEMEstimate.Size = new System.Drawing.Size(92, 97);
            this.btnEMEstimate.TabIndex = 37;
            // 
            // btnEMAllocation
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnEMAllocation.Appearance = appearance3;
            this.btnEMAllocation.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
            this.btnEMAllocation.HotTrackAppearance = appearance4;
            this.btnEMAllocation.ImageSize = new System.Drawing.Size(80, 80);
            this.btnEMAllocation.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnEMAllocation.Location = new System.Drawing.Point(249, 202);
            this.btnEMAllocation.Name = "btnEMAllocation";
            this.btnEMAllocation.Size = new System.Drawing.Size(92, 97);
            this.btnEMAllocation.TabIndex = 38;
            // 
            // btnRBUAllocationAndUse
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance5.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnRBUAllocationAndUse.Appearance = appearance5;
            this.btnRBUAllocationAndUse.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
            this.btnRBUAllocationAndUse.HotTrackAppearance = appearance6;
            this.btnRBUAllocationAndUse.ImageSize = new System.Drawing.Size(80, 80);
            this.btnRBUAllocationAndUse.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnRBUAllocationAndUse.Location = new System.Drawing.Point(461, 202);
            this.btnRBUAllocationAndUse.Name = "btnRBUAllocationAndUse";
            this.btnRBUAllocationAndUse.Size = new System.Drawing.Size(92, 97);
            this.btnRBUAllocationAndUse.TabIndex = 39;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(160, 234);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 33);
            this.label2.TabIndex = 45;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(372, 234);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 33);
            this.label1.TabIndex = 46;
            // 
            // UCEM_tab3_
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnRBUAllocationAndUse);
            this.Controls.Add(this.btnEMAllocation);
            this.Controls.Add(this.btnEMEstimate);
            this.Name = "UCEM_tab3_";
            this.Size = new System.Drawing.Size(673, 449);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnEMEstimate;
        private Infragistics.Win.Misc.UltraButton btnEMAllocation;
        private Infragistics.Win.Misc.UltraButton btnRBUAllocationAndUse;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}
