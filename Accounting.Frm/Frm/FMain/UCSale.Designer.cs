﻿namespace Accounting
{
    partial class UCSale
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCSale));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.btnSAInvoicePayment0 = new Infragistics.Win.Misc.UltraButton();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSAReceiptCustomer = new Infragistics.Win.Misc.UltraButton();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSALiabilitiesReport = new Infragistics.Win.Misc.UltraButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSAInvoicePayment1 = new Infragistics.Win.Misc.UltraButton();
            this.label5 = new System.Windows.Forms.Label();
            this.btnS17DNN = new Infragistics.Win.Misc.UltraButton();
            this.btnReceiptableSummary = new Infragistics.Win.Misc.UltraButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSAInvoicePayment0
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance1.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnSAInvoicePayment0.Appearance = appearance1;
            this.btnSAInvoicePayment0.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
            this.btnSAInvoicePayment0.HotTrackAppearance = appearance2;
            this.btnSAInvoicePayment0.ImageSize = new System.Drawing.Size(80, 80);
            this.btnSAInvoicePayment0.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnSAInvoicePayment0.Location = new System.Drawing.Point(24, 192);
            this.btnSAInvoicePayment0.Name = "btnSAInvoicePayment0";
            this.btnSAInvoicePayment0.Size = new System.Drawing.Size(92, 97);
            this.btnSAInvoicePayment0.TabIndex = 32;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(148, 226);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 33);
            this.label1.TabIndex = 36;
            // 
            // btnSAReceiptCustomer
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnSAReceiptCustomer.Appearance = appearance3;
            this.btnSAReceiptCustomer.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
            this.btnSAReceiptCustomer.HotTrackAppearance = appearance4;
            this.btnSAReceiptCustomer.ImageSize = new System.Drawing.Size(80, 80);
            this.btnSAReceiptCustomer.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnSAReceiptCustomer.Location = new System.Drawing.Point(234, 192);
            this.btnSAReceiptCustomer.Name = "btnSAReceiptCustomer";
            this.btnSAReceiptCustomer.Size = new System.Drawing.Size(92, 97);
            this.btnSAReceiptCustomer.TabIndex = 37;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(157, 293);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 63);
            this.label2.TabIndex = 38;
            // 
            // btnSALiabilitiesReport
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance5.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnSALiabilitiesReport.Appearance = appearance5;
            this.btnSALiabilitiesReport.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
            this.btnSALiabilitiesReport.HotTrackAppearance = appearance6;
            this.btnSALiabilitiesReport.ImageSize = new System.Drawing.Size(80, 80);
            this.btnSALiabilitiesReport.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnSALiabilitiesReport.Location = new System.Drawing.Point(129, 371);
            this.btnSALiabilitiesReport.Name = "btnSALiabilitiesReport";
            this.btnSALiabilitiesReport.Size = new System.Drawing.Size(92, 97);
            this.btnSALiabilitiesReport.TabIndex = 39;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.Location = new System.Drawing.Point(358, 226);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 33);
            this.label3.TabIndex = 40;
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Image = ((System.Drawing.Image)(resources.GetObject("label4.Image")));
            this.label4.Location = new System.Drawing.Point(370, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 63);
            this.label4.TabIndex = 41;
            // 
            // btnSAInvoicePayment1
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.Image = ((object)(resources.GetObject("appearance7.Image")));
            appearance7.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance7.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance7.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnSAInvoicePayment1.Appearance = appearance7;
            this.btnSAInvoicePayment1.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance8.Image = ((object)(resources.GetObject("appearance8.Image")));
            this.btnSAInvoicePayment1.HotTrackAppearance = appearance8;
            this.btnSAInvoicePayment1.ImageSize = new System.Drawing.Size(80, 80);
            this.btnSAInvoicePayment1.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnSAInvoicePayment1.Location = new System.Drawing.Point(340, 17);
            this.btnSAInvoicePayment1.Name = "btnSAInvoicePayment1";
            this.btnSAInvoicePayment1.Size = new System.Drawing.Size(92, 97);
            this.btnSAInvoicePayment1.TabIndex = 42;
            // 
            // label5
            // 
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Image = ((System.Drawing.Image)(resources.GetObject("label5.Image")));
            this.label5.Location = new System.Drawing.Point(468, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 33);
            this.label5.TabIndex = 43;
            // 
            // btnS17DNN
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.Image = ((object)(resources.GetObject("appearance9.Image")));
            appearance9.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance9.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance9.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnS17DNN.Appearance = appearance9;
            this.btnS17DNN.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance10.Image = ((object)(resources.GetObject("appearance10.Image")));
            this.btnS17DNN.HotTrackAppearance = appearance10;
            this.btnS17DNN.ImageSize = new System.Drawing.Size(80, 80);
            this.btnS17DNN.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnS17DNN.Location = new System.Drawing.Point(552, 17);
            this.btnS17DNN.Name = "btnS17DNN";
            this.btnS17DNN.Size = new System.Drawing.Size(92, 97);
            this.btnS17DNN.TabIndex = 44;
            // 
            // btnReceiptableSummary
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.Image = ((object)(resources.GetObject("appearance11.Image")));
            appearance11.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance11.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance11.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnReceiptableSummary.Appearance = appearance11;
            this.btnReceiptableSummary.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance12.Image = ((object)(resources.GetObject("appearance12.Image")));
            this.btnReceiptableSummary.HotTrackAppearance = appearance12;
            this.btnReceiptableSummary.ImageSize = new System.Drawing.Size(80, 80);
            this.btnReceiptableSummary.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnReceiptableSummary.Location = new System.Drawing.Point(552, 371);
            this.btnReceiptableSummary.Name = "btnReceiptableSummary";
            this.btnReceiptableSummary.Size = new System.Drawing.Size(92, 97);
            this.btnReceiptableSummary.TabIndex = 45;
            // 
            // label6
            // 
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Image = ((System.Drawing.Image)(resources.GetObject("label6.Image")));
            this.label6.Location = new System.Drawing.Point(468, 400);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 33);
            this.label6.TabIndex = 46;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(108)))), ((int)(((byte)(158)))));
            this.label7.Location = new System.Drawing.Point(455, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(6, 357);
            this.label7.TabIndex = 47;
            // 
            // UCSale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnReceiptableSummary);
            this.Controls.Add(this.btnS17DNN);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSAInvoicePayment1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSALiabilitiesReport);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSAReceiptCustomer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSAInvoicePayment0);
            this.Name = "UCSale";
            this.Size = new System.Drawing.Size(694, 494);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnSAInvoicePayment0;
        private System.Windows.Forms.Label label1;
        private Infragistics.Win.Misc.UltraButton btnSAReceiptCustomer;
        private System.Windows.Forms.Label label2;
        private Infragistics.Win.Misc.UltraButton btnSALiabilitiesReport;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Infragistics.Win.Misc.UltraButton btnSAInvoicePayment1;
        private System.Windows.Forms.Label label5;
        private Infragistics.Win.Misc.UltraButton btnS17DNN;
        private Infragistics.Win.Misc.UltraButton btnReceiptableSummary;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;

    }
}
