﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class UCBank : UserControl
    {
        public UCBank(FMain @this)
        {
            InitializeComponent();
            btnMBDeposit.Click += (s, e) => btnMBDeposit_Click(s, e, @this);
            btnMBTellerPaper.Click += (s, e) => btnMBTellerPaper_Click(s, e, @this);
            btnMCompare.Click += (s, e) => btnMCompare_Click(s, e, @this);
            btnRS06DNN.Click += (s, e) => btnRS06DNN_Click(s, e, @this);
        }

        private void btnMBDeposit_Click(object sender, EventArgs e,FMain fMain)
        {
            fMain.ShowFMBDeposit();
        }

        private void btnMBTellerPaper_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFMBTellerPaper();
        }

        private void btnRS06DNN_Click(object sender, EventArgs e, FMain fMain)
        {
            new Accounting.Frm.FReport.FRSO_TGNH().ShowFormDialog(this);
        }

        private void btnMCompare_Click(object sender, EventArgs e,FMain fMain)
        {
            fMain.ShowFMCompare();
        }

        private void btnMBTellerPaper_Click(object sender, EventArgs e)
        {

        }
    }
}
