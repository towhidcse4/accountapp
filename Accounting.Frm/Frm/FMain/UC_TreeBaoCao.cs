﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Infragistics.Win.UltraWinTree;
using Infragistics.Win;
using Accounting.TextMessage;
using Accounting.Frm.FReport;

namespace Accounting.Frm.FMain
{
    public partial class UC_TreeBaoCao : UserControl
    {
        string key_baocao = "";
        public UC_TreeBaoCao()
        {
            InitializeComponent();
            //Node cấp 1
            UltraTreeNode nodeParent = ultraTree1.Nodes.Add("GroupBCTC", "Báo cáo tài chính");
            UltraTreeNode nodeParent2 = ultraTree1.Nodes.Add("GroupQuy", "Quỹ");
            UltraTreeNode nodeParent3 = ultraTree1.Nodes.Add("GroupNganHang", "Ngân hàng");
            UltraTreeNode nodeParent4 = ultraTree1.Nodes.Add("GroupBanHang", "Bán hàng");
            UltraTreeNode nodeParent5 = ultraTree1.Nodes.Add("GroupMuaHang", "Mua hàng");
            UltraTreeNode nodeParent12 = ultraTree1.Nodes.Add("GroupKho", "Kho");
            UltraTreeNode nodeParent11 = ultraTree1.Nodes.Add("GroupCCDC", "Công cụ dụng cụ");
            UltraTreeNode nodeParent10 = ultraTree1.Nodes.Add("GroupTSCD", "Tài sản cố định");
            UltraTreeNode nodeParent6 = ultraTree1.Nodes.Add("GroupTongHop", "Tổng hợp");
            UltraTreeNode nodeParent7 = ultraTree1.Nodes.Add("GroupLuong", "Lương");
            UltraTreeNode nodeParent14 = ultraTree1.Nodes.Add("GroupGiaThanh", "Giá thành");
            //UltraTreeNode nodeParent7 = ultraTree1.Nodes.Add("GroupThuQuy", "Thủ quỹ");
            UltraTreeNode nodeParent13 = ultraTree1.Nodes.Add("GroupQLHD", "Quản lý hóa đơn");
            UltraTreeNode nodeParent8 = ultraTree1.Nodes.Add("GroupHoaDonDienTu", "Hóa đơn điện tử");
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "PPTTGTGT").Data == "Phương pháp khấu trừ")
            {
                UltraTreeNode nodeParent9 = ultraTree1.Nodes.Add("GroupThue", "Thuế");
                nodeParent9.Nodes.Add("btnBaoCaoBangKeMuavao", "Bảng kê hoá đơn, chứng từ hàng hoá, dịch vụ mua vào (Mẫu quản trị)");
                nodeParent9.Nodes.Add("btnBaoCaoBangKeBanra", "Bảng kê hoá đơn, chứng từ hàng hoá, dịch vụ bán ra (Mẫu quản trị)");
            }
            UltraTreeNode nodeParent15 = ultraTree1.Nodes.Add("GroupHopDong", "Hợp đồng");


            // Node cấp 2
            nodeParent.Nodes.Add("btnBangCanDoiTK", "Bảng cân đối tài khoản");
            nodeParent.Nodes.Add("btnB01aDNN", "B01aDNN: Báo cáo tình hình tài chính");
            nodeParent.Nodes.Add("btnB01bDNN", "B01bDNN: Báo cáo tình hình tài chính");
            nodeParent.Nodes.Add("btnB02bDNN", "B02-DNN: Báo cáo kết quả hoạt động kinh doanh");
            nodeParent.Nodes.Add("btnB03bDNN", "B03-DNN: Báo cáo lưu chuyển tiền tệ (PP trực tiếp)");
            nodeParent.Nodes.Add("btnB09DNN", "B09-DNN: Bản thuyết minh báo cáo tài chính");
            nodeParent2.Nodes.Add("btnSoKTQuyTM", "Sổ kế toán chi tiết quỹ tiền mặt");
            nodeParent2.Nodes.Add("btnSoQuyTM", "Sổ quỹ tiền mặt");
            nodeParent3.Nodes.Add("btSoTGNH", "Sổ tiền gửi ngân hàng");
            nodeParent3.Nodes.Add("btnBangKeSoDuNH", "Bảng kê số dư ngân hàng");
            nodeParent4.Nodes.Add("btnSoChiTietBanHang", "Sổ chi tiết bán hàng");
            nodeParent4.Nodes.Add("btnSoNhatKyBH", "Sổ nhật ký bán hàng");
            nodeParent4.Nodes.Add("btnTHCongNoPhaiThu", "Tổng hợp công nợ phải thu");
            nodeParent4.Nodes.Add("btnCTCNPTKH", "Chi tiết công nợ phải thu khách hàng");
            nodeParent4.Nodes.Add("btnTongHopCongNoThuGroupKH", "Tổng hợp công nợ phải thu theo nhóm khách hàng");
            nodeParent4.Nodes.Add("btnSoChiTietPhaiThuKHTheoMH", "Sổ chi tiết công nợ phải thu theo mặt hàng");//add by namnh
            nodeParent4.Nodes.Add("btnSoTheoDoiLaiLoTheoMatHang", "Sổ theo dõi lãi lỗ theo mặt hàng");//add by cuongpv
            nodeParent4.Nodes.Add("btnSoTheoDoiCongNoPhaiThuTheoMatHang", "Sổ theo dõi công nợ phải thu theo mặt hàng");//add by cuongpv
            nodeParent4.Nodes.Add("btnSoTheoDoiLaiLoTheoHoaDon", "Sổ theo dõi lãi lỗ theo hóa đơn");//add by cuongpv
            nodeParent4.Nodes.Add("btnSoTheoDoiCongNoPhaiThuTheoHoaDon", "Sổ theo dõi công nợ phải thu theo hóa đơn");//add by cuongpv
            nodeParent4.Nodes.Add("btnSoTheoDoiLaiLoTheoHopDongBan", "Sổ theo dõi lãi lỗ theo hợp đồng bán");//add by cuongpv
            nodeParent4.Nodes.Add("btnSoTheoDoiCongNoPhaiThuTheoHopDongBan", "Sổ theo dõi công nợ phải thu theo hợp đồng bán");//add by cuongpv
            nodeParent4.Nodes.Add("btnSoChiTietDoanhThuTheoNhanVien", "Sổ chi tiết doanh thu theo nhân viên");//add by namnh
            nodeParent4.Nodes.Add("btnSoTongHopDoanhThuTheoNhanVien", "Sổ tổng hợp doanh thu theo nhân viên");//add by anmt1
            nodeParent4.Nodes.Add("btnSoChiTietBanHangTheoNhanVien", "Sổ chi tiết bán hàng theo nhân viên");//add by namnh
            nodeParent5.Nodes.Add("btnTongCongNoPTNCC", "Tổng hợp công nợ phải trả");
            nodeParent5.Nodes.Add("btnSoNhatKyMuaHang", "Sổ nhật ký mua hàng");
            nodeParent5.Nodes.Add("btnSoChiTietMuaHang", "Sổ chi tiết mua hàng");
            nodeParent5.Nodes.Add("btnSoChiTietCongNoPhaiTra", "Chi tiết công nợ phải trả nhà cung cấp");
            nodeParent6.Nodes.Add("btnSoChiTietCacTK", "Sổ chi tiết các tài khoản"); //
            nodeParent6.Nodes.Add("btnSo_NhatKyChung", "Nhật ký chung"); //
            nodeParent6.Nodes.Add("btnSoNhatKyChung", "S03b-DNN: Sổ cái (Hình thức nhật ký chung)"); //
            nodeParent6.Nodes.Add("btnS02bDNN", "Sổ đăng ký chứng từ ghi sổ  - S02b - DNN");//add by namnh //
            nodeParent6.Nodes.Add("btnS02c1DNN", "S02c1-DNN: Sổ cái (Hình thức chứng từ ghi sổ)"); //
            nodeParent6.Nodes.Add("btnBangKeChungTuChuaLapChungTuGhiSo", "Bảng kê chứng từ chưa lập chứng từ ghi sổ");
            //nodeParent6.Nodes.Add("btnNhatKySoCai", "Nhật ký _ Sổ cái");//add by namnh //
            nodeParent6.Nodes.Add("btnSoNhatKyThuTien", "Sổ nhật ký thu tiền");
            nodeParent6.Nodes.Add("btnSoNhatKyChiTien", "Sổ nhật ký chi tiền");
            nodeParent6.Nodes.Add("btnSoChiTietTienVay", "S15-DNN: Sổ chi tiết tiền vay");
            nodeParent6.Nodes.Add("btnSo_ChitietThanhToan", "S12-DNN Sổ chi tiết thanh toán với người mua (người bán)");
            nodeParent6.Nodes.Add("btnSoTheoDoiThanhToanNT", "Sổ theo dõi thanh toán bằng ngoại tệ");//add by cuongpv
            nodeParent6.Nodes.Add("btnTongHopCongNoNV", "Tổng hợp công nợ nhân viên");
            nodeParent6.Nodes.Add("btnCTCNPTNV", "Sổ chi tiết công nợ nhân viên"); //add by namnh
            nodeParent6.Nodes.Add("btnBPBCPTT", "Bảng phân bổ chi phí trả trước");
            nodeParent6.Nodes.Add("btnTongHopCPTheoKhoanMucCP", "Tổng hợp CP theo khoản mục CP");
            nodeParent6.Nodes.Add("btnSoTheoDoiCTTheoMTK", "Sổ theo dõi chi tiết theo mã thống kê");//add by namnh
            nodeParent6.Nodes.Add("btnSoTheoDoiCTTheoTHCP", "Sổ theo dõi chi tiết theo Đối tượng tập hợp chi phí");//add by namnh
            nodeParent6.Nodes.Add("btnSoTheoDoiDoiTuongTHCP", "Sổ theo dõi Đối tượng THCP (theo Khoản mục CP)");//add by anmt1
          
            nodeParent7.Nodes.Add("btnSoTongHopLuongNhanVien", "Sổ tổng hợp lương nhân viên");//add by namnh
            //nodeParent7.Nodes.Add("btnSoQuyTM", "Sổ quỹ tiền mặt");
            nodeParent8.Nodes.Add("btnBaoCaoTinhHingSuSungHD", "Báo cáo tình hình sử dụng hóa đơn");
            nodeParent8.Nodes.Add("btnBangKeHD_ChungTuHH_DVBanRa", "Bảng kê hóa đơn, chứng từ HH, DV bán ra");
            nodeParent8.Nodes.Add("btnBaoCaoDoanhThuTheoSanPham", "Báo cáo doanh thu theo sản phẩm");
            nodeParent8.Nodes.Add("btnBaoCaoDoanhThuTheoBenMua", "Báo cáo doanh thu theo bên mua");

            nodeParent10.Nodes.Add("btnTheTSCD", "Thẻ TSCD");
            nodeParent10.Nodes.Add("btnSoTSCD", "Sổ TSCD");
            nodeParent10.Nodes.Add("btnSoTheoDoiTSCDTaiNoiSD", "Sổ theo dõi TSCD tại nơi SD");
            nodeParent11.Nodes.Add("btnSoTheoDoiCCDCTaiNoiSD", "Sổ theo dõi CCDC tại nơi SD");
            nodeParent11.Nodes.Add("btnSoPBCCDC", "Sổ CCDC");
            nodeParent12.Nodes.Add("btnTheKho", "Thẻ Kho");
            nodeParent12.Nodes.Add("btnSoCTVLDCSPHH", "Sổ chi tiết vật liệu, dụng cụ, sản phẩm, hàng hóa");
            nodeParent12.Nodes.Add("btnBangTongHopCT", "Bảng tổng hợp chi tiết");
            nodeParent12.Nodes.Add("btnTongHopTonKho", "Tổng hợp tồn kho");
            nodeParent13.Nodes.Add("btnQLHD", "Tình hình sử dụng hóa đơn");
            nodeParent14.Nodes.Add("btnTheTinhGT", "Thẻ tính giá thành");
            nodeParent14.Nodes.Add("btnSoChiTietLaiLoTheoCongTrinh", "Sổ chi tiết lãi lỗ theo công trình");//namnh
            nodeParent14.Nodes.Add("btnSoTongHopLaiLoTheoCongTrinh", "Sổ tổng hợp lãi lỗ theo công trình");//namnh
            nodeParent15.Nodes.Add("btnTinhHinhThucHienHDM", "Tình hình thực hiện hợp đồng mua");
            nodeParent15.Nodes.Add("btnTinhHinhThucHienHDB", "Tình hình thực hiện hợp đồng bán");
            ultraTree1.NodeConnectorStyle = NodeConnectorStyle.Dotted;
            ultraTree1.NodeConnectorColor = Color.Blue;
            //Hover
            Infragistics.Win.UltraWinTree.Override ovr;
            ovr = this.ultraTree1.Override;
            ovr.HotTracking = DefaultableBoolean.True;
            ovr.BorderStyleNode = UIElementBorderStyle.Solid;
            ovr.Multiline = DefaultableBoolean.True;
            ovr.NodeAppearance.BorderColor = Color.Black;
            ovr.ActiveNodeAppearance.BorderColor = Color.Black;
            ovr.ExpandedNodeAppearance.BorderColor = Color.Black;
            ovr.HotTrackingNodeAppearance.BorderColor = Color.Black;
            ovr.SelectedNodeAppearance.BorderColor = Color.Black;            
            this.ultraTree1.NodeLevelOverrides[2].ActiveNodeAppearance.BorderColor = Color.Green;
            this.ultraTree1.Nodes.Override.ActiveNodeAppearance.BorderColor = Color.Aqua;
            this.ultraTree1.Override.NodeAppearance.FontData.SizeInPoints = 9;
            this.ultraTree1.Override.NodeAppearance.FontData.Name = "Microsoft Sans Serif";
            this.ultraTree1.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraTree1.Override.HotTrackingNodeAppearance.Cursor = Cursors.Hand;
            SetGridFont();
            //nodeParent.Override.NodeAppearance.Image = global::Accounting.Properties.Resources._1437136799_add;
            for (int i = 0; i < ultraTree1.Nodes.Count; i++)
            {
                ultraTree1.Nodes[i].LeftImages.Add(global::Accounting.Properties.Resources.forder_new);
                for (int j = 0; j < ultraTree1.Nodes[i].Nodes.Count; j++)
                {
                    ultraTree1.Nodes[i].Nodes[j].LeftImages.Add(global::Accounting.Properties.Resources.booknew);
                }
            }
        }

        private void SetGridFont()
        {
            FontData fd = this.ultraTree1.Override.HotTrackingNodeAppearance.FontData;
            fd.Bold = DefaultableBoolean.True;
            fd.Italic = DefaultableBoolean.True;
            fd.Name = "Microsoft Sans Serif";
            fd.SizeInPoints = 9;
            fd.Underline = DefaultableBoolean.True;

        }

        private void ultraTree1_DoubleClick(object sender, EventArgs e)
        {
            UltraTree tree = (UltraTree)sender;
            UIElement element = tree.UIElement.LastElementEntered;
            if (element == null)
                return;

            element = element.GetAncestor(typeof(NodeSelectableAreaUIElement));
            if (element == null)
                return;

            UltraTreeNode node = element.GetContext(typeof(UltraTreeNode)) as UltraTreeNode;
            if (node == null)
                return;
            string key = node.Key.ToString();
            switch (key)
            {
                case "btnBangCanDoiTK"://Bảng cân đối tài khoản
                    new Accounting.Frm.FReport.FRBANGCANDOITAIKHOAN().Show(this);
                    break;
                case "btnSoKTQuyTM"://Sổ kế toán chi tiết quỹ tiền mặt
                    new Accounting.Frm.FReport.FRSOKTCTTIENMAT().Show(this);
                    break;
                case "btSoTGNH"://Sổ tiền gửi ngân hàng
                    new Accounting.Frm.FReport.FRSO_TGNH().Show(this);
                    break;
                case "btnBangKeSoDuNH"://Bảng kê số dư ngân hàng
                    new Accounting.Frm.FReport.FRBangKeSoDuNH().Show(this);
                    break;
                case "btnSoChiTietBanHang"://Bảng sổ chi tiết bán hàng
                    new Accounting.Frm.FReport.FRSoChiTietBanHang().Show(this);
                    break;
                case "btnSoNhatKyBH"://Sổ nhật ký bán hàng
                    new Accounting.Frm.FReport.FRSoNhatKyBanHang().Show(this);
                    break;
                case "btnTHCongNoPhaiThu"://Tổng hợp công nợ phải thu
                    new Accounting.Frm.FReport.FRTHCONGNOPHAITHU().Show(this);
                    break;

                case "btnCTCNPTKH"://Chi tiết công nợ phải thu khách hàng
                    new Accounting.Frm.FReport.FRCHITIETCONGNOPHAITHUKHACHHANG().Show(this);
                    break;
                case "btnCTCNPTNV"://Chi tiết công nợ phải thu nhân viên
                    new Accounting.Frm.FReport.FRCHITIETCONGNOPHAITHUNHANVIEN().Show(this);
                    break;
                case "btnTongHopCongNoThuGroupKH"://Tổng hợp công nợ phải thu theo nhóm khách hàng
                    new Accounting.Frm.FReport.FRTONGCONGNOPTTHEOGROUPKH().Show(this);
                    break;
                case "btnSoTheoDoiLaiLoTheoMatHang"://Sổ theo dõi lãi lỗ theo mặt hàng - add by cuongpv
                    new Accounting.Frm.FReport.FRSoTheoDoiLaiLoTheoMatHang().Show(this);
                    break;
                case "btnSoTheoDoiCongNoPhaiThuTheoMatHang"://Sổ theo dõi công nợ phải thu theo mặt hàng - add by cuongpv
                    new Accounting.Frm.FReport.FRSoTheoDoiCongNoPhaiThuTheoMatHang().Show(this);
                    break;
                case "btnSoTheoDoiLaiLoTheoHoaDon"://Sổ theo dõi lãi lỗ theo hóa đơn - add by cuongpv 
                    new Accounting.Frm.FReport.FRSoTheoDoiLaiLoTheoHoaDon().Show(this);
                    break;
                case "btnSoTheoDoiCongNoPhaiThuTheoHoaDon"://Sổ theo dõi công nợ phải thu theo hóa đơn - add by cuongpv
                    new Accounting.Frm.FReport.FRSoTheoDoiCongNoPhaiThuTheoHoaDon().Show(this);
                    break;
                case "btnSoTheoDoiLaiLoTheoHopDongBan"://Sổ theo dõi lãi lỗ theo hợp đồng bán - add by cuongpv
                    new Accounting.Frm.FReport.FRSoTheoDoiLaiLoTheoHopDongBan().Show(this);
                    break;
                case "btnSoTheoDoiCongNoPhaiThuTheoHopDongBan"://Sổ theo dõi công nợ phải thu theo hợp đồng bán - add by cuongpv 
                    new Accounting.Frm.FReport.FRSoTheoDoiCongNoPhaiThuTheoHopDongBan().Show(this);
                    break;
                case "btnSoChiTietDoanhThuTheoNhanVien"://Sổ chi tiết doanh thu theo nhân viên - add by namnh
                    new Accounting.Frm.FReport.FRSoChiTietDoanhThuTheoNhanVien().Show(this);
                    break;
                case "btnSoTongHopDoanhThuTheoNhanVien"://Sổ tổng hợp doanh thu theo nhân viên - add by anmt1
                    new Accounting.Frm.FReport.FRSoTongHopDoanhThuTheoNhanVien().Show(this);
                    break;
                case "btnTongCongNoPTNCC"://Tổng hợp công nợ phải trả theo nhà cung cấp
                    new Accounting.Frm.FReport.FRTONGCONGNOPHAITRA().Show(this);
                    break;
                case "btnSoNhatKyMuaHang"://Sổ nhật ký mua hàng
                    new Accounting.Frm.FReport.FRSONHATKYMUAHANG().Show(this);
                    break;
                case "btnSoChiTietCacTK"://Sổ chi tiết các tài khoản
                    new Accounting.Frm.FReport.FRSOCHITIETCACTAIKHOAN().Show(this);
                    break;
                case "btnSoNhatKyChung"://Sổ cái (Hình thức chung)
                    new Accounting.Frm.FReport.FRSOCAIS03DN().Show(this);
                    break;
                case "btnB01aDNN"://Báo cáo tình hình tài chính B01aDNN
                    new Accounting.Frm.FReport.FRB01aDNN().Show(this);
                    break;
                case "btnB01bDNN"://Báo cáo tình hình tài chính B01bDNN
                    new Accounting.Frm.FReport.FRB01bDNN().Show(this);
                    break;
                case "btnSo_NhatKyChung"://Sổ nhật ký dùng chung
                    new Accounting.Frm.FReport.FRSONHATKYCHUNG().Show(this);
                    break;
                case "btnSoChiTietMuaHang"://Sổ chi tiết mua hàng
                    new Accounting.Frm.FReport.FRSOCHITIETMUAHANG().Show(this);
                    break;
                case "btnSo_ChitietThanhToan"://S12-DNN Sổ chi tiết thanh toán với người mua (người bán)
                    new Accounting.Frm.FReport.FRS12_SOCHITIETTHANHTOAN().Show(this);
                    break;
                case "btnSoChiTietCongNoPhaiTra"://Chi tiết công nợ phải trả nhà cung cấp
                    new Accounting.Frm.FReport.FRCHITIETCONGNOPHAITRANCC().Show(this);
                    break;
                case "btnSoQuyTM"://Sổ quỹ tiền mặt
                    new Accounting.Frm.FReport.FRSOQUYTM().Show(this);
                    break;
                case "btnSoNhatKyThuTien"://Sổ nhật ký thu tiền
                    new Accounting.Frm.FReport.FRSONHATKYTHUTIEN().Show(this);
                    break;
                case "btnSoNhatKyChiTien"://Sổ nhật ký chi tiền
                    new Accounting.Frm.FReport.FRSONHATKYCHITIEN().Show(this);
                    break;
                case "btnSoChiTietTienVay"://Sổ chi tiết tiền vay
                    new Accounting.Frm.FReport.FRSOCHITIETTIENVAY().Show(this);
                    break;
                case "btnB02bDNN"://Báo cáo kết quả hoạt động kinh doanh
                    new Accounting.Frm.FReport.FRBAOCAOHOATDONGKD().Show(this);
                    break;
                case "btnB03bDNN"://Báo cáo lưu chuyển tiền tệ (PP trực tiếp)
                    new Accounting.Frm.FReport.FRBAOCAOLUUCHUYENTIENPPTT().Show(this);
                    break;
                case "btnB09DNN"://Thuyết minh báo cáo tài chính
                    new Accounting.Frm.FReport.FRB09DNN().Show(this);
                    break;
                case "btnBaoCaoTinhHingSuSungHD"://Báo cáo tình hình sử dụng hóa đơn điện tử
                    new Accounting.Frm.FReport.FRBaoCaoTinhHinhSudungHDDT().Show(this);
                    break;
                case "btnBaoCaoDoanhThuTheoBenMua"://Báo cáo doanh thu theo bên mua
                    new Accounting.Frm.FReport.FRBaoCaoDoanhThuTheoBenMua().Show(this);
                    break;
                case "btnBaoCaoDoanhThuTheoSanPham"://Báo cáo doanh thu theo sản phẩm
                    new Accounting.Frm.FReport.FRBaoCaoDoanhThuTheoSP().Show(this);
                    break;
                case "btnBangKeHD_ChungTuHH_DVBanRa"://Bảng kê hóa đơn, chứng từ hàng hóa, dịch vụ
                    new Accounting.Frm.FReport.FRBangKeHDChungTuHHDV().Show(this);
                    break;
                case "btnBaoCaoBangKeMuavao"://Bảng kê hóa đơn, chứng từ hàng hóa, dịch vụ mua vào
                    new Accounting.Frm.FReport.FRBaoCaoBangKeMuavao().Show(this);
                    break;
                case "btnBaoCaoBangKeBanra"://Bảng kê hóa đơn, chứng từ hàng hóa, dịch vụ bán ra
                    new Accounting.Frm.FReport.FRBaoCaoBangKeBanra().Show(this);
                    break;
                case "btnTongHopCPTheoKhoanMucCP"://Bảng kê hóa đơn, chứng từ hàng hóa, dịch vụ bán ra
                    new Accounting.Frm.FReport.FRTongHopCPTheoKhoanMucCP().Show(this);
                    break;
                case "btnBPBCPTT"://Bảng phân bổ chi phí trả trước
                    new Accounting.Frm.FReport.FRBPBCPTT().Show(this);//btnSoTheoDoiThanhToanNT
                    break;
                case "btnSoTheoDoiThanhToanNT"://Sổ theo dõi thanh toán bằng ngoại tệ - add by Cuongpv
                    new Accounting.Frm.FReport.FRSoTheoDoiThanhToanNgoaiTe().Show(this);
                    break;
                case "btnTongHopCongNoNV":// Tổng hợp công nợ nhân viên
                    new Accounting.Frm.FReport.FRTongHopCongNoNhanVien().Show(this);
                    break;
                //namnh
                case "btnSoTheoDoiCTTheoMTK"://Sổ theo dõi chi tiết theo mã thống kê
                    new Accounting.Frm.FReport.FRSoTheoDoiCTTheoMTK().Show(this);
                    break;
                case "btnSoTheoDoiCTTheoTHCP"://Sổ theo dõi chi tiết theo đối tượng tập hợp chi phí
                    new Accounting.Frm.FReport.FrmSoChiTietDoiTuongTapHopCP().Show(this);
                    break;
                case "btnS02bDNN"://Sổ đăng ký chứng từ ghi sổ - S02b - DNN
                    new Accounting.Frm.FReport.FRS02bDNN().Show(this);
                    break;
                case "btnS02c1DNN":
                    new Accounting.Frm.FReport.FRS02c1DNN().Show(this); 
                    break;
				case "btnSoTheoDoiDoiTuongTHCP"://Sổ theo dõi đối tượng THCP (theo khoản mục CP) = add by anmt1
                    new Accounting.Frm.FReport.FrmSoTheoDoiDoiTuongTHCP().Show(this);
                    break;
                case "btnSoChiTietPhaiThuKHTheoMH"://Sổ theo dõi đối tượng THCP (theo khoản mục CP) = add by anmt1
                    new Accounting.Frm.FReport.FRSOCHITIETPHAITHUKHTHEOMH().Show(this);
                    break;
                case "btnSoChiTietBanHangTheoNhanVien":
                    new Accounting.Frm.FReport.FRSOCHITIETBANHANGTHEONHANVIEN().Show(this);
                    break;
                //case "btnNhatKySoCai"://Nhật ký sổ cái
                //    new Accounting.Frm.FReport.FRNhatKySoCai().Show(this);
                //    break;
                case "btnSoTongHopLuongNhanVien"://Sổ tổng hợp lương nhân viên - add by namnh
                    new Accounting.Frm.FReport.FRSoTongHopLuongNhanVien().Show(this);
                    break;
                case "btnBangKeChungTuChuaLapChungTuGhiSo"://Sổ tổng hợp lương nhân viên - add by namnh
                    new Accounting.Frm.FReport.FRBangKeChungTuChuaLapChungTuGhiSo().Show(this);
                    break;
                case "btnTheTSCD":
                    ShowFRS12SDNN();
                    break;
                case "btnSoTSCD":
                    ShowFRS10DNN();
                    break;
                case "btnSoTheoDoiTSCDTaiNoiSD":
                    ShowFRS11DNN();
                    break;
                case "btnSoTheoDoiCCDCTaiNoiSD":
                    ShowFRS11DNN_CCDC();
                    break;
                case "btnSoPBCCDC":
                    ShowFRS11DNN_PBCCDC();
                    break;
                case "btnTheKho":
                    ShowFRS09DNN();
                    break;
                case "btnSoCTVLDCSPHH":
                    ShowFRS07DNN();
                    break;
                case "btnBangTongHopCT":
                    ShowFRS07DNN16();
                    break;
                case "btnTongHopTonKho":
                    ShowFRTongHopTonKho();
                    break;
                case "btnQLHD":
                    ShowFRTinhHinhSuDungHD();
                    break;
                case "btnTheTinhGT":
                    ShowFRCPPeriod();
                    break;
                case "btnSoChiTietLaiLoTheoCongTrinh":
                    new Accounting.Frm.FReport.FRSoChiTietLaiLoTheoCongTrinh().Show(this);//namnh
                    break;
                case "btnSoTongHopLaiLoTheoCongTrinh":
                    new Accounting.Frm.FReport.FRSoTongHopLaiLoTheoCongTrinh().Show(this);//namnh
                    break;
                    
                case "btnTinhHinhThucHienHDM":
                    ShowFRContractStatusPurchase();
                    break;
                case "btnTinhHinhThucHienHDB":
                    ShowFRContractStatusSale();
                    break;
                default:
                    break;
            }
        }

        private void ultraTree1_Click(object sender, EventArgs e)
        {
            UltraTree tree = (UltraTree)sender;
            UIElement element = tree.UIElement.LastElementEntered;
            if (element == null)
                return;

            element = element.GetAncestor(typeof(NodeSelectableAreaUIElement));
            if (element == null)
                return;

            UltraTreeNode node = element.GetContext(typeof(UltraTreeNode)) as UltraTreeNode;
            if (node == null)
                return;
            //ultraLabel1.Text = node.Text;
            if (!node.Key.ToString().StartsWith("Group"))
            {
                //ultraPictureBox1.Image = global::Accounting.Properties.Resources.mastercard;
                ultraPictureBox1.Image = null;
                key_baocao = node.Key;
            }
            else
            {
                key_baocao = "";
                ultraPictureBox1.Image = null;
            }

        }
        #region
        public void ShowFRCPPeriod()
        {
            var subSystemCode = "FRCPPeriod";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRCPPeriod(subSystemCode).Show(this);
        }

        public void ShowFRTinhHinhSuDungHD()
        {
            var subSystemCode = "FRTinhHinhSuDungHD";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRTinhHinhSuDungHD(subSystemCode).Show(this);
        }
        /// <summary>
        /// Mở form thẻ tài sản cố định
        /// </summary>
        public void ShowFRS12SDNN()
        {
            var subSystemCode = "Rpt_FRS12SDNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS12SDNN(subSystemCode).Show(this);
        }
        /// <summary>
        /// Mở form báo cáo FRS10DNN
        /// </summary>
        public void ShowFRS10DNN()
        {
            var subSystemCode = "Rpt_FRS10DNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS10DNN(subSystemCode).Show(this);
        }
        /// <summary>
        /// mở form báo cáo FRS11DNN
        /// </summary>
        public void ShowFRS11DNN()
        {
            var subSystemCode = "Rpt_FRS11DNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS11DNN(subSystemCode).Show(this);
        }
        /// <summary>
        /// mở form báo cáo FRS11DNN
        /// </summary>
        public void ShowFRS11DNN_CCDC()
        {
            var subSystemCode = "Rpt_FRS11DNN_CCDC";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS11DNN_CCDC(subSystemCode).Show(this);
        }
        public void ShowFRS11DNN_PBCCDC()
        {
            var subSystemCode = "Rpt_FRS11DNN_PBCCDC";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS11DNN_PBCCDC(subSystemCode).Show(this);
        }
        /// <summary>
        /// Mở form thẻ kho
        /// </summary>
        public void ShowFRS09DNN()
        {
            var subSystemCode = "Rpt_FRS09DNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS09DNN(subSystemCode).Show(this);
        }
        /// <summary>
        /// Mở form báo cáo 
        /// </summary>
        public void ShowFRS07DNN()
        {
            var subSystemCode = "Rpt_FRS07DNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS07DNN(subSystemCode).Show(this);
        }
        public void ShowFRContractStatusSale()
        {
            var subSystemCode = "Rpt_FRContractStatusSale";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRContractStatusSale(subSystemCode).Show(this);
        }
        public void ShowFRContractStatusPurchase()
        {
            var subSystemCode = "Rpt_FRContractStatusPurchase";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRContractStatusPurchase(subSystemCode).Show(this);
        }
        /// <summary>
        /// Mở form báo cáo S07DNN
        /// </summary>
        public void ShowFRS07DNN16()
        {
            var subSystemCode = "Rpt_FRS07DNN16";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS07DNN16(subSystemCode).Show(this);
        }
        /// <summary>
        /// Mở form báo cáo B09-DNN
        /// </summary>
        public void ShowFRB09DNN()
        {
            var subSystemCode = "FRB09DNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {

            }
        }
        public void ShowFRTongHopTonKho()
        {
            var subSystemCode = "Rpt_FRTongHopTonKho";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRTongHopTonKho(subSystemCode).Show(this);
        }
        #endregion
        private void ultraTree1_InitializeDataNode(object sender, InitializeDataNodeEventArgs e)
        {
            e.Node.LeftImages.Add(global::Accounting.Properties.Resources._1437136799_add);
        }

        private void btnInBC_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(key_baocao))
            {
                switch (key_baocao)
                {
                    case "btnBangCanDoiTK"://Bảng cân đối tài khoản
                        new Accounting.Frm.FReport.FRBANGCANDOITAIKHOAN().Show(this);
                        break;
                    case "btnSoKTQuyTM"://Sổ kế toán chi tiết quỹ tiền mặt
                        new Accounting.Frm.FReport.FRSOKTCTTIENMAT().Show(this);
                        break;
                    case "btSoTGNH"://Sổ tiền gửi ngân hàng
                        new Accounting.Frm.FReport.FRSO_TGNH().Show(this);
                        break;
                    case "btnBangKeSoDuNH"://Bảng kê số dư ngân hàng
                        new Accounting.Frm.FReport.FRBangKeSoDuNH().Show(this);
                        break;
                    case "btnSoChiTietBanHang"://Bảng sổ chi tiết bán hàng
                        new Accounting.Frm.FReport.FRSoChiTietBanHang().Show(this);
                        break;
                    case "btnSoNhatKyBH"://Sổ nhật ký bán hàng
                        new Accounting.Frm.FReport.FRSoNhatKyBanHang().Show(this);
                        break;
                    case "btnTHCongNoPhaiThu"://Tổng hợp công nợ phải thu
                        new Accounting.Frm.FReport.FRTHCONGNOPHAITHU().Show(this);
                        break;
                    case "btnCTCNPTKH"://Chi tiết công nợ phải thu khách hàng
                        new Accounting.Frm.FReport.FRCHITIETCONGNOPHAITHUKHACHHANG().Show(this);
                        break;
                    case "btnTongHopNoThuGroupKH"://Tổng hợp nợ phải thu theo nhóm khách hàng
                        new Accounting.Frm.FReport.FRTONGCONGNOPTTHEOGROUPKH().Show(this);
                        break;
                    case "btnTongCongNoPTNCC"://Tổng hợp nợ phải trả theo nhà cung cấp
                        new Accounting.Frm.FReport.FRTONGCONGNOPHAITRA().Show(this);
                        break;
                    case "btnSoNhatKyMuaHang"://Sổ nhật ký mua hàng
                        new Accounting.Frm.FReport.FRSONHATKYMUAHANG().Show(this);
                        break;
                    case "btnSoChiTietCacTK"://Sổ chi tiết các tài khoản
                        new Accounting.Frm.FReport.FRSOCHITIETCACTAIKHOAN().Show(this);
                        break;
                    case "btnSoNhatKyChung"://Sổ cái (Hình thức chung)
                        new Accounting.Frm.FReport.FRSOCAIS03DN().Show(this);
                        break;
                    case "btnB01aDNN"://Báo cáo tình hình tài chính B01aDNN
                        new Accounting.Frm.FReport.FRB01aDNN().Show(this);
                        break;
                    case "btnB01bDNN"://Báo cáo tình hình tài chính B01bDNN
                        new Accounting.Frm.FReport.FRB01bDNN().Show(this);
                        break;
                    case "btnSo_NhatKyChung"://Sổ nhật ký dùng chung
                        new Accounting.Frm.FReport.FRSONHATKYCHUNG().Show(this);
                        break;
                    case "btnSoChiTietMuaHang"://Sổ chi tiết mua hàng
                        new Accounting.Frm.FReport.FRSOCHITIETMUAHANG().Show(this);
                        break;
                    case "btnSo_ChitietThanhToan"://S12-DNN Sổ chi tiết thanh toán với người mua (người bán)
                        new Accounting.Frm.FReport.FRS12_SOCHITIETTHANHTOAN().Show(this);
                        break;
                    case "btnSoChiTietCongNoPhaiTra"://Chi tiết công nợ phải trả nhà cung cấp
                        new Accounting.Frm.FReport.FRCHITIETCONGNOPHAITRANCC().Show(this);
                        break;
                    case "btnSoQuyTM"://Sổ quỹ tiền mặt
                        new Accounting.Frm.FReport.FRSOQUYTM().Show(this);
                        break;
                    case "btnSoNhatKyThuTien"://Sổ nhật ký thu tiền
                        new Accounting.Frm.FReport.FRSONHATKYTHUTIEN().Show(this);
                        break;
                    case "btnSoNhatKyChiTien"://Sổ nhật ký chi tiền
                        new Accounting.Frm.FReport.FRSONHATKYCHITIEN().Show(this);
                        break;
                    case "btnSoChiTietTienVay"://Sổ chi tiết tiền vay
                        new Accounting.Frm.FReport.FRSOCHITIETTIENVAY().Show(this);
                        break;
                    case "btnB02bDNN"://Báo cáo kết quả hoạt động kinh doanh
                        new Accounting.Frm.FReport.FRBAOCAOHOATDONGKD().Show(this);
                        break;
                    case "btnB03bDNN"://Báo cáo lưu chuyển tiền tệ (PP trực tiếp)
                        new Accounting.Frm.FReport.FRBAOCAOLUUCHUYENTIENPPTT().Show(this);
                        break;
                    case "btnBaoCaoBangKeMuavao"://Bảng kê hóa đơn, chứng từ hàng hóa, dịch vụ mua vào
                        new Accounting.Frm.FReport.FRBaoCaoBangKeMuavao().Show(this);
                        break;
                    case "btnBaoCaoBangKeBanra"://Bảng kê hóa đơn, chứng từ hàng hóa, dịch vụ bán ra
                        new Accounting.Frm.FReport.FRBaoCaoBangKeBanra().Show(this);
                        break;
                    case "btnTongHopCongNoThuGroupKH"://Tổng hợp công nợ phải thu theo nhóm khách hàng
                        new Accounting.Frm.FReport.FRTONGCONGNOPTTHEOGROUPKH().Show(this);
                        break;
                    case "btnTongHopCPTheoKhoanMucCP"://Bảng kê hóa đơn, chứng từ hàng hóa, dịch vụ bán ra
                        new Accounting.Frm.FReport.FRTongHopCPTheoKhoanMucCP().Show(this);
                        break;
                    case "btnBPBCPTT"://Bảng phân bổ chi phí trả trước
                        new Accounting.Frm.FReport.FRBPBCPTT().Show(this);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
