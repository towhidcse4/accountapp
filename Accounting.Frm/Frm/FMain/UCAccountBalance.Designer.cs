﻿namespace Accounting.Frm.FMain
{
    partial class UCAccountBalance
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uTree = new Infragistics.Win.UltraWinTree.UltraTree();
            ((System.ComponentModel.ISupportInitialize)(this.uTree)).BeginInit();
            this.SuspendLayout();
            // 
            // uTree
            // 
            this.uTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uTree.Location = new System.Drawing.Point(0, 0);
            this.uTree.Name = "uTree";
            this.uTree.Size = new System.Drawing.Size(1129, 549);
            this.uTree.TabIndex = 2;
            // 
            // UCAccountBalance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.uTree);
            this.Name = "UCAccountBalance";
            this.Size = new System.Drawing.Size(1129, 549);
            ((System.ComponentModel.ISupportInitialize)(this.uTree)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinTree.UltraTree uTree;
    }
}
