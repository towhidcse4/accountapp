﻿namespace Accounting
{
    partial class UCFixedAsset
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCFixedAsset));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.btnFixedAsset = new Infragistics.Win.Misc.UltraButton();
            this.btnFABuyAndIncrement = new Infragistics.Win.Misc.UltraButton();
            this.btnFADecrement = new Infragistics.Win.Misc.UltraButton();
            this.btnFADepreciation = new Infragistics.Win.Misc.UltraButton();
            this.btnFAAdjustment = new Infragistics.Win.Misc.UltraButton();
            this.btnRS12SDNN = new Infragistics.Win.Misc.UltraButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnFixedAsset
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance1.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnFixedAsset.Appearance = appearance1;
            this.btnFixedAsset.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
            this.btnFixedAsset.HotTrackAppearance = appearance2;
            this.btnFixedAsset.ImageSize = new System.Drawing.Size(80, 80);
            this.btnFixedAsset.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnFixedAsset.Location = new System.Drawing.Point(41, 23);
            this.btnFixedAsset.Name = "btnFixedAsset";
            this.btnFixedAsset.Size = new System.Drawing.Size(92, 97);
            this.btnFixedAsset.TabIndex = 38;
            // 
            // btnFABuyAndIncrement
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnFABuyAndIncrement.Appearance = appearance3;
            this.btnFABuyAndIncrement.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
            this.btnFABuyAndIncrement.HotTrackAppearance = appearance4;
            this.btnFABuyAndIncrement.ImageSize = new System.Drawing.Size(80, 80);
            this.btnFABuyAndIncrement.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnFABuyAndIncrement.Location = new System.Drawing.Point(261, 23);
            this.btnFABuyAndIncrement.Name = "btnFABuyAndIncrement";
            this.btnFABuyAndIncrement.Size = new System.Drawing.Size(92, 97);
            this.btnFABuyAndIncrement.TabIndex = 39;
            // 
            // btnFADecrement
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance5.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnFADecrement.Appearance = appearance5;
            this.btnFADecrement.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
            this.btnFADecrement.HotTrackAppearance = appearance6;
            this.btnFADecrement.ImageSize = new System.Drawing.Size(80, 80);
            this.btnFADecrement.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnFADecrement.Location = new System.Drawing.Point(41, 218);
            this.btnFADecrement.Name = "btnFADecrement";
            this.btnFADecrement.Size = new System.Drawing.Size(92, 97);
            this.btnFADecrement.TabIndex = 40;
            // 
            // btnFADepreciation
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.Image = ((object)(resources.GetObject("appearance7.Image")));
            appearance7.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance7.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance7.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnFADepreciation.Appearance = appearance7;
            this.btnFADepreciation.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance8.Image = ((object)(resources.GetObject("appearance8.Image")));
            this.btnFADepreciation.HotTrackAppearance = appearance8;
            this.btnFADepreciation.ImageSize = new System.Drawing.Size(80, 80);
            this.btnFADepreciation.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnFADepreciation.Location = new System.Drawing.Point(261, 217);
            this.btnFADepreciation.Name = "btnFADepreciation";
            this.btnFADepreciation.Size = new System.Drawing.Size(92, 97);
            this.btnFADepreciation.TabIndex = 41;
            // 
            // btnFAAdjustment
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.Image = ((object)(resources.GetObject("appearance9.Image")));
            appearance9.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance9.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance9.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnFAAdjustment.Appearance = appearance9;
            this.btnFAAdjustment.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance10.Image = ((object)(resources.GetObject("appearance10.Image")));
            this.btnFAAdjustment.HotTrackAppearance = appearance10;
            this.btnFAAdjustment.ImageSize = new System.Drawing.Size(80, 80);
            this.btnFAAdjustment.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnFAAdjustment.Location = new System.Drawing.Point(261, 411);
            this.btnFAAdjustment.Name = "btnFAAdjustment";
            this.btnFAAdjustment.Size = new System.Drawing.Size(92, 97);
            this.btnFAAdjustment.TabIndex = 42;
            // 
            // btnRS12SDNN
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.Image = ((object)(resources.GetObject("appearance11.Image")));
            appearance11.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance11.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance11.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnRS12SDNN.Appearance = appearance11;
            this.btnRS12SDNN.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance12.Image = ((object)(resources.GetObject("appearance12.Image")));
            this.btnRS12SDNN.HotTrackAppearance = appearance12;
            this.btnRS12SDNN.ImageSize = new System.Drawing.Size(80, 80);
            this.btnRS12SDNN.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnRS12SDNN.Location = new System.Drawing.Point(475, 218);
            this.btnRS12SDNN.Name = "btnRS12SDNN";
            this.btnRS12SDNN.Size = new System.Drawing.Size(92, 97);
            this.btnRS12SDNN.TabIndex = 43;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(171, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 33);
            this.label2.TabIndex = 45;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(171, 250);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 33);
            this.label1.TabIndex = 46;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.Location = new System.Drawing.Point(385, 250);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 33);
            this.label3.TabIndex = 47;
            // 
            // label5
            // 
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Image = ((System.Drawing.Image)(resources.GetObject("label5.Image")));
            this.label5.Location = new System.Drawing.Point(291, 331);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 63);
            this.label5.TabIndex = 48;
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Image = ((System.Drawing.Image)(resources.GetObject("label4.Image")));
            this.label4.Location = new System.Drawing.Point(291, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 63);
            this.label4.TabIndex = 49;
            // 
            // UCFixedAsset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnRS12SDNN);
            this.Controls.Add(this.btnFAAdjustment);
            this.Controls.Add(this.btnFADepreciation);
            this.Controls.Add(this.btnFADecrement);
            this.Controls.Add(this.btnFABuyAndIncrement);
            this.Controls.Add(this.btnFixedAsset);
            this.Name = "UCFixedAsset";
            this.Size = new System.Drawing.Size(673, 521);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnFixedAsset;
        private Infragistics.Win.Misc.UltraButton btnFABuyAndIncrement;
        private Infragistics.Win.Misc.UltraButton btnFADecrement;
        private Infragistics.Win.Misc.UltraButton btnFADepreciation;
        private Infragistics.Win.Misc.UltraButton btnFAAdjustment;
        private Infragistics.Win.Misc.UltraButton btnRS12SDNN;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
    }
}
