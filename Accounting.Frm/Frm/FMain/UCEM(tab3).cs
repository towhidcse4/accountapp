﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class UCEM_tab3_ : UserControl
    {
        public UCEM_tab3_(FMain @this)
        {
            InitializeComponent();
            btnEMEstimate.Click += (s, e) => btnEMEstimate_Click(s, e, @this);
            btnEMAllocation.Click += (s, e) => btnEMAllocation_Click(s, e, @this);
            btnRBUAllocationAndUse.Click += (s, e) => btnRBUAllocationAndUse_Click(s, e, @this);
        }

        private void btnEMEstimate_Click(object sender, EventArgs e,FMain fMain)
        {
            fMain.ShowFEMEstimate();
        }

        private void btnEMAllocation_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFEMAllocation();
        }

        private void btnRBUAllocationAndUse_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFRBUAllocationAndUse();
        }
    }
}
