﻿namespace Accounting
{
    partial class FLogon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.btnSetting = new Infragistics.Win.Misc.UltraButton();
            this.btnLogOn = new Infragistics.Win.Misc.UltraButton();
            this.lblForgotpas = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.txtPassword = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtUser = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbServers = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDatabases = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.btnClose = new System.Windows.Forms.Button();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtPwdServer = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtUsrServer = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.btnRefreshServer = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbServers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDatabases)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPwdServer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsrServer)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraPanel2
            // 
            appearance1.ImageBackground = global::Accounting.Properties.Resources.login_footer;
            this.ultraPanel2.Appearance = appearance1;
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.btnSetting);
            this.ultraPanel2.ClientArea.Controls.Add(this.btnLogOn);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 253);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(361, 102);
            this.ultraPanel2.TabIndex = 27;
            // 
            // btnSetting
            // 
            appearance2.Image = global::Accounting.Properties.Resources.settings;
            this.btnSetting.Appearance = appearance2;
            this.btnSetting.Location = new System.Drawing.Point(229, 6);
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Size = new System.Drawing.Size(104, 28);
            this.btnSetting.TabIndex = 10;
            this.btnSetting.Text = "Tùy chọn >>";
            this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click);
            // 
            // btnLogOn
            // 
            appearance3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            appearance3.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnLogOn.Appearance = appearance3;
            this.btnLogOn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogOn.Location = new System.Drawing.Point(138, 6);
            this.btnLogOn.Name = "btnLogOn";
            this.btnLogOn.Size = new System.Drawing.Size(85, 28);
            this.btnLogOn.TabIndex = 9;
            this.btnLogOn.Text = "Đồng ý";
            this.btnLogOn.Click += new System.EventHandler(this.btnLogOn_Click);
            // 
            // lblForgotpas
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.ForeColor = System.Drawing.Color.Blue;
            appearance4.TextVAlignAsString = "Middle";
            this.lblForgotpas.Appearance = appearance4;
            this.lblForgotpas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblForgotpas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblForgotpas.ImageTransparentColor = System.Drawing.Color.Empty;
            this.lblForgotpas.Location = new System.Drawing.Point(147, 222);
            this.lblForgotpas.Name = "lblForgotpas";
            this.lblForgotpas.Size = new System.Drawing.Size(133, 20);
            this.lblForgotpas.TabIndex = 34;
            this.lblForgotpas.Text = "Quên mật khẩu quản trị?";
            this.lblForgotpas.Click += new System.EventHandler(this.lblForgotpas_Click);
            // 
            // ultraLabel6
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance5;
            this.ultraLabel6.Location = new System.Drawing.Point(23, 195);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(108, 20);
            this.ultraLabel6.TabIndex = 33;
            this.ultraLabel6.Text = "&Mật khẩu";
            // 
            // ultraLabel7
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance6;
            this.ultraLabel7.Location = new System.Drawing.Point(23, 167);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(108, 20);
            this.ultraLabel7.TabIndex = 32;
            this.ultraLabel7.Text = "&Tài khoản";
            // 
            // txtPassword
            // 
            this.txtPassword.AutoSize = false;
            this.txtPassword.Location = new System.Drawing.Point(147, 194);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(186, 22);
            this.txtPassword.TabIndex = 3;
            // 
            // txtUser
            // 
            this.txtUser.AutoSize = false;
            this.txtUser.Location = new System.Drawing.Point(147, 167);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(186, 22);
            this.txtUser.TabIndex = 2;
            this.txtUser.Text = "ADMIN";
            // 
            // cbbServers
            // 
            this.cbbServers.AutoSize = false;
            this.cbbServers.Location = new System.Drawing.Point(137, 265);
            this.cbbServers.Name = "cbbServers";
            this.cbbServers.Size = new System.Drawing.Size(157, 22);
            this.cbbServers.TabIndex = 11;
            this.cbbServers.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cbbServers_BeforeDropDown);
            // 
            // ultraLabel5
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance7;
            this.ultraLabel5.Location = new System.Drawing.Point(23, 265);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(108, 20);
            this.ultraLabel5.TabIndex = 29;
            this.ultraLabel5.Text = "Máy chủ &SQL";
            // 
            // cbbDatabases
            // 
            this.cbbDatabases.AutoSize = false;
            this.cbbDatabases.Location = new System.Drawing.Point(147, 139);
            this.cbbDatabases.Name = "cbbDatabases";
            this.cbbDatabases.Size = new System.Drawing.Size(186, 22);
            this.cbbDatabases.TabIndex = 1;
            this.cbbDatabases.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cbbDatabases_BeforeDropDown);
            // 
            // ultraLabel4
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance8;
            this.ultraLabel4.Location = new System.Drawing.Point(23, 140);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(108, 20);
            this.ultraLabel4.TabIndex = 26;
            this.ultraLabel4.Text = "&Dữ liệu kế toán";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(33)))), ((int)(((byte)(122)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnClose.Location = new System.Drawing.Point(325, 0);
            this.btnClose.Margin = new System.Windows.Forms.Padding(0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(36, 28);
            this.btnClose.TabIndex = 25;
            this.btnClose.Text = "x";
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // ultraLabel3
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(84)))), ((int)(((byte)(228)))));
            appearance9.Image = global::Accounting.Properties.Resources.login;
            appearance9.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance9;
            this.ultraLabel3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel3.Location = new System.Drawing.Point(24, 99);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(159, 28);
            this.ultraLabel3.TabIndex = 24;
            this.ultraLabel3.Text = "ĐĂNG NHẬP";
            // 
            // ultraLabel2
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance10;
            this.ultraLabel2.Location = new System.Drawing.Point(23, 321);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(108, 20);
            this.ultraLabel2.TabIndex = 23;
            this.ultraLabel2.Text = "&Mật khẩu quản trị";
            // 
            // ultraLabel1
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance11;
            this.ultraLabel1.Location = new System.Drawing.Point(23, 293);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(108, 20);
            this.ultraLabel1.TabIndex = 22;
            this.ultraLabel1.Text = "&Tài khoản quản trị";
            // 
            // txtPwdServer
            // 
            this.txtPwdServer.AutoSize = false;
            this.txtPwdServer.Location = new System.Drawing.Point(137, 320);
            this.txtPwdServer.Name = "txtPwdServer";
            this.txtPwdServer.PasswordChar = '*';
            this.txtPwdServer.Size = new System.Drawing.Size(186, 22);
            this.txtPwdServer.TabIndex = 13;
            // 
            // txtUsrServer
            // 
            this.txtUsrServer.AutoSize = false;
            this.txtUsrServer.Location = new System.Drawing.Point(137, 293);
            this.txtUsrServer.Name = "txtUsrServer";
            this.txtUsrServer.Size = new System.Drawing.Size(186, 22);
            this.txtUsrServer.TabIndex = 12;
            this.txtUsrServer.Text = "ADMIN";
            // 
            // btnRefreshServer
            // 
            this.btnRefreshServer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance12.Image = global::Accounting.Properties.Resources.refresh;
            this.btnRefreshServer.Appearance = appearance12;
            this.btnRefreshServer.ImageSize = new System.Drawing.Size(13, 13);
            this.btnRefreshServer.Location = new System.Drawing.Point(309, 265);
            this.btnRefreshServer.Name = "btnRefreshServer";
            this.btnRefreshServer.Size = new System.Drawing.Size(23, 22);
            this.btnRefreshServer.TabIndex = 39;
            this.btnRefreshServer.Click += new System.EventHandler(this.btnRefreshServer_Click);
            // 
            // FLogon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.BackgroundImage = global::Accounting.Properties.Resources.login_header;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(361, 355);
            this.Controls.Add(this.lblForgotpas);
            this.Controls.Add(this.ultraPanel2);
            this.Controls.Add(this.btnRefreshServer);
            this.Controls.Add(this.ultraLabel6);
            this.Controls.Add(this.ultraLabel7);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.cbbServers);
            this.Controls.Add(this.ultraLabel5);
            this.Controls.Add(this.cbbDatabases);
            this.Controls.Add(this.ultraLabel4);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.ultraLabel3);
            this.Controls.Add(this.ultraLabel2);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.txtPwdServer);
            this.Controls.Add(this.txtUsrServer);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FLogon";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đăng nhập";
            this.TransparencyKey = System.Drawing.Color.WhiteSmoke;
            this.Load += new System.EventHandler(this.FLogon_Load);
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbServers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDatabases)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPwdServer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsrServer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraButton btnSetting;
        private Infragistics.Win.Misc.UltraButton btnLogOn;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPassword;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtUser;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbServers;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbDatabases;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private System.Windows.Forms.Button btnClose;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPwdServer;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtUsrServer;
        private Infragistics.Win.Misc.UltraButton btnRefreshServer;
        private Infragistics.Win.Misc.UltraLabel lblForgotpas;
    }
}