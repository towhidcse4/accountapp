﻿namespace Accounting
{
    partial class UCDesktop
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCDesktop));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            this.btnHoaDon = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.btnKeToanThue = new Infragistics.Win.Misc.UltraButton();
            this.btnBanHang = new Infragistics.Win.Misc.UltraButton();
            this.btnTienVaNH = new Infragistics.Win.Misc.UltraButton();
            this.btnCDDC = new Infragistics.Win.Misc.UltraButton();
            this.btnKho = new Infragistics.Win.Misc.UltraButton();
            this.btnMuaHang = new Infragistics.Win.Misc.UltraButton();
            this.btnTaiSanCoDinh = new Infragistics.Win.Misc.UltraButton();
            this.btnKeToanTienLuong = new Infragistics.Win.Misc.UltraButton();
            this.btnHopDong = new Infragistics.Win.Misc.UltraButton();
            this.btnKeToanGiaThanh = new Infragistics.Win.Misc.UltraButton();
            this.btnBaoCao = new Infragistics.Win.Misc.UltraButton();
            this.btnKeToanTongHop = new Infragistics.Win.Misc.UltraButton();
            this.btnDuLieu = new Infragistics.Win.Misc.UltraButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnHoaDon
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance1.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnHoaDon.Appearance = appearance1;
            this.btnHoaDon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHoaDon.ImageSize = new System.Drawing.Size(80, 80);
            this.btnHoaDon.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnHoaDon.Location = new System.Drawing.Point(5, 107);
            this.btnHoaDon.Name = "btnHoaDon";
            this.btnHoaDon.Size = new System.Drawing.Size(92, 97);
            this.btnHoaDon.TabIndex = 18;
            // 
            // ultraPanel1
            // 
            appearance2.BackColor = System.Drawing.Color.White;
            this.ultraPanel1.Appearance = appearance2;
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.btnKeToanThue);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnHoaDon);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnBanHang);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnTienVaNH);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnCDDC);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnKho);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnMuaHang);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnTaiSanCoDinh);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnKeToanTienLuong);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnHopDong);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnKeToanGiaThanh);
            this.ultraPanel1.Location = new System.Drawing.Point(194, 59);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(299, 416);
            this.ultraPanel1.TabIndex = 33;
            // 
            // btnKeToanThue
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.Image = global::Accounting.Properties.Resources.ketoan_thue2;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnKeToanThue.Appearance = appearance3;
            this.btnKeToanThue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnKeToanThue.ImageSize = new System.Drawing.Size(80, 80);
            this.btnKeToanThue.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnKeToanThue.Location = new System.Drawing.Point(103, 209);
            this.btnKeToanThue.Name = "btnKeToanThue";
            this.btnKeToanThue.Size = new System.Drawing.Size(92, 97);
            this.btnKeToanThue.TabIndex = 16;
            // 
            // btnBanHang
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance4.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance4.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnBanHang.Appearance = appearance4;
            this.btnBanHang.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBanHang.ImageSize = new System.Drawing.Size(80, 80);
            this.btnBanHang.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnBanHang.Location = new System.Drawing.Point(201, 4);
            this.btnBanHang.Name = "btnBanHang";
            this.btnBanHang.Size = new System.Drawing.Size(92, 97);
            this.btnBanHang.TabIndex = 12;
            // 
            // btnTienVaNH
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance5.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnTienVaNH.Appearance = appearance5;
            this.btnTienVaNH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTienVaNH.ImageSize = new System.Drawing.Size(80, 80);
            this.btnTienVaNH.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnTienVaNH.Location = new System.Drawing.Point(5, 4);
            this.btnTienVaNH.Name = "btnTienVaNH";
            this.btnTienVaNH.Size = new System.Drawing.Size(92, 97);
            this.btnTienVaNH.TabIndex = 5;
            // 
            // btnCDDC
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
            appearance6.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance6.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance6.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnCDDC.Appearance = appearance6;
            this.btnCDDC.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCDDC.ImageSize = new System.Drawing.Size(80, 80);
            this.btnCDDC.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnCDDC.Location = new System.Drawing.Point(103, 312);
            this.btnCDDC.Name = "btnCDDC";
            this.btnCDDC.Size = new System.Drawing.Size(92, 97);
            this.btnCDDC.TabIndex = 6;
            // 
            // btnKho
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.Image = ((object)(resources.GetObject("appearance7.Image")));
            appearance7.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance7.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance7.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnKho.Appearance = appearance7;
            this.btnKho.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnKho.ImageSize = new System.Drawing.Size(80, 80);
            this.btnKho.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnKho.Location = new System.Drawing.Point(103, 107);
            this.btnKho.Name = "btnKho";
            this.btnKho.Size = new System.Drawing.Size(92, 97);
            this.btnKho.TabIndex = 9;
            // 
            // btnMuaHang
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.Image = ((object)(resources.GetObject("appearance8.Image")));
            appearance8.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance8.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance8.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnMuaHang.Appearance = appearance8;
            this.btnMuaHang.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMuaHang.ImageSize = new System.Drawing.Size(80, 80);
            this.btnMuaHang.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnMuaHang.Location = new System.Drawing.Point(103, 4);
            this.btnMuaHang.Name = "btnMuaHang";
            this.btnMuaHang.Size = new System.Drawing.Size(92, 97);
            this.btnMuaHang.TabIndex = 11;
            // 
            // btnTaiSanCoDinh
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.Image = ((object)(resources.GetObject("appearance9.Image")));
            appearance9.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance9.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance9.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnTaiSanCoDinh.Appearance = appearance9;
            this.btnTaiSanCoDinh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTaiSanCoDinh.ImageSize = new System.Drawing.Size(80, 80);
            this.btnTaiSanCoDinh.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnTaiSanCoDinh.Location = new System.Drawing.Point(5, 312);
            this.btnTaiSanCoDinh.Name = "btnTaiSanCoDinh";
            this.btnTaiSanCoDinh.Size = new System.Drawing.Size(92, 97);
            this.btnTaiSanCoDinh.TabIndex = 8;
            // 
            // btnKeToanTienLuong
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.Image = ((object)(resources.GetObject("appearance10.Image")));
            appearance10.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance10.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance10.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnKeToanTienLuong.Appearance = appearance10;
            this.btnKeToanTienLuong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnKeToanTienLuong.ImageSize = new System.Drawing.Size(80, 80);
            this.btnKeToanTienLuong.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnKeToanTienLuong.Location = new System.Drawing.Point(5, 209);
            this.btnKeToanTienLuong.Name = "btnKeToanTienLuong";
            this.btnKeToanTienLuong.Size = new System.Drawing.Size(92, 97);
            this.btnKeToanTienLuong.TabIndex = 13;
            this.btnKeToanTienLuong.Click += new System.EventHandler(this.btnKeToanTienLuong_Click);
            // 
            // btnHopDong
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.Image = global::Accounting.Properties.Resources.hop_dong_2;
            appearance11.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance11.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance11.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnHopDong.Appearance = appearance11;
            this.btnHopDong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHopDong.ImageSize = new System.Drawing.Size(80, 80);
            this.btnHopDong.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnHopDong.Location = new System.Drawing.Point(201, 210);
            this.btnHopDong.Name = "btnHopDong";
            this.btnHopDong.Size = new System.Drawing.Size(92, 97);
            this.btnHopDong.TabIndex = 14;
            // 
            // btnKeToanGiaThanh
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.Image = ((object)(resources.GetObject("appearance12.Image")));
            appearance12.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance12.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance12.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnKeToanGiaThanh.Appearance = appearance12;
            this.btnKeToanGiaThanh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnKeToanGiaThanh.ImageSize = new System.Drawing.Size(80, 80);
            this.btnKeToanGiaThanh.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnKeToanGiaThanh.Location = new System.Drawing.Point(201, 107);
            this.btnKeToanGiaThanh.Name = "btnKeToanGiaThanh";
            this.btnKeToanGiaThanh.Size = new System.Drawing.Size(92, 97);
            this.btnKeToanGiaThanh.TabIndex = 10;
            // 
            // btnBaoCao
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.Image = ((object)(resources.GetObject("appearance13.Image")));
            appearance13.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance13.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance13.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnBaoCao.Appearance = appearance13;
            this.btnBaoCao.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBaoCao.ImageSize = new System.Drawing.Size(80, 80);
            this.btnBaoCao.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnBaoCao.Location = new System.Drawing.Point(798, 219);
            this.btnBaoCao.Name = "btnBaoCao";
            this.btnBaoCao.Size = new System.Drawing.Size(92, 97);
            this.btnBaoCao.TabIndex = 15;
            // 
            // btnKeToanTongHop
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.Image = ((object)(resources.GetObject("appearance14.Image")));
            appearance14.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance14.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance14.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnKeToanTongHop.Appearance = appearance14;
            this.btnKeToanTongHop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnKeToanTongHop.ImageSize = new System.Drawing.Size(80, 80);
            this.btnKeToanTongHop.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnKeToanTongHop.Location = new System.Drawing.Point(604, 219);
            this.btnKeToanTongHop.Name = "btnKeToanTongHop";
            this.btnKeToanTongHop.Size = new System.Drawing.Size(92, 97);
            this.btnKeToanTongHop.TabIndex = 32;
            this.btnKeToanTongHop.Click += new System.EventHandler(this.btnKeToanTongHop_Click);
            // 
            // btnDuLieu
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.Image = ((object)(resources.GetObject("appearance15.Image")));
            appearance15.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance15.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance15.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnDuLieu.Appearance = appearance15;
            this.btnDuLieu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDuLieu.ImageSize = new System.Drawing.Size(80, 80);
            this.btnDuLieu.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnDuLieu.Location = new System.Drawing.Point(7, 219);
            this.btnDuLieu.Name = "btnDuLieu";
            this.btnDuLieu.Size = new System.Drawing.Size(92, 97);
            this.btnDuLieu.TabIndex = 31;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.Color.Transparent;
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.Location = new System.Drawing.Point(717, 251);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 33);
            this.label3.TabIndex = 37;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.Color.Transparent;
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(526, 251);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 33);
            this.label2.TabIndex = 36;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(121, 251);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 33);
            this.label1.TabIndex = 35;
            // 
            // UCDesktop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.btnBaoCao);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ultraPanel1);
            this.Controls.Add(this.btnKeToanTongHop);
            this.Controls.Add(this.btnDuLieu);
            this.Name = "UCDesktop";
            this.Size = new System.Drawing.Size(948, 534);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Infragistics.Win.Misc.UltraButton btnHoaDon;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraButton btnBanHang;
        private Infragistics.Win.Misc.UltraButton btnTienVaNH;
        private Infragistics.Win.Misc.UltraButton btnCDDC;
        private Infragistics.Win.Misc.UltraButton btnTaiSanCoDinh;
        private Infragistics.Win.Misc.UltraButton btnKho;
        private Infragistics.Win.Misc.UltraButton btnMuaHang;
        private Infragistics.Win.Misc.UltraButton btnKeToanTienLuong;
        private Infragistics.Win.Misc.UltraButton btnHopDong;
        private Infragistics.Win.Misc.UltraButton btnKeToanGiaThanh;
        private Infragistics.Win.Misc.UltraButton btnBaoCao;
        private Infragistics.Win.Misc.UltraButton btnKeToanTongHop;
        private Infragistics.Win.Misc.UltraButton btnDuLieu;
        private Infragistics.Win.Misc.UltraButton btnKeToanThue;
    }
}
