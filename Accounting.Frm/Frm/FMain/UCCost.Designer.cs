﻿namespace Accounting
{
    partial class UCCost
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCCost));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            this.btnCostingPeriod = new Infragistics.Win.Misc.UltraButton();
            this.btnExpenses = new Infragistics.Win.Misc.UltraButton();
            this.btnUnfinishedEvaluation = new Infragistics.Win.Misc.UltraButton();
            this.btnCosting = new Infragistics.Win.Misc.UltraButton();
            this.btnCostingCard = new Infragistics.Win.Misc.UltraButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCostingPeriod
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance1.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnCostingPeriod.Appearance = appearance1;
            this.btnCostingPeriod.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance2.Image = global::Accounting.Properties.Resources.KYTINHGIATHANH2;
            this.btnCostingPeriod.HotTrackAppearance = appearance2;
            this.btnCostingPeriod.ImageSize = new System.Drawing.Size(80, 80);
            this.btnCostingPeriod.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnCostingPeriod.Location = new System.Drawing.Point(44, 165);
            this.btnCostingPeriod.Name = "btnCostingPeriod";
            this.btnCostingPeriod.Size = new System.Drawing.Size(92, 97);
            this.btnCostingPeriod.TabIndex = 39;
            // 
            // btnExpenses
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.Image = global::Accounting.Properties.Resources.phanbochiphi;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnExpenses.Appearance = appearance3;
            this.btnExpenses.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance4.Image = global::Accounting.Properties.Resources.phanbochiphi2;
            this.btnExpenses.HotTrackAppearance = appearance4;
            this.btnExpenses.ImageSize = new System.Drawing.Size(80, 80);
            this.btnExpenses.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnExpenses.Location = new System.Drawing.Point(274, 165);
            this.btnExpenses.Name = "btnExpenses";
            this.btnExpenses.Size = new System.Drawing.Size(92, 97);
            this.btnExpenses.TabIndex = 40;
            // 
            // btnUnfinishedEvaluation
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.Image = global::Accounting.Properties.Resources.danhgiadodang;
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance5.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnUnfinishedEvaluation.Appearance = appearance5;
            this.btnUnfinishedEvaluation.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance6.Image = global::Accounting.Properties.Resources.danhgiadodang2;
            this.btnUnfinishedEvaluation.HotTrackAppearance = appearance6;
            this.btnUnfinishedEvaluation.ImageSize = new System.Drawing.Size(80, 80);
            this.btnUnfinishedEvaluation.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnUnfinishedEvaluation.Location = new System.Drawing.Point(504, 165);
            this.btnUnfinishedEvaluation.Name = "btnUnfinishedEvaluation";
            this.btnUnfinishedEvaluation.Size = new System.Drawing.Size(92, 97);
            this.btnUnfinishedEvaluation.TabIndex = 41;
            // 
            // btnCosting
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.Image = global::Accounting.Properties.Resources.tinhgiathanh;
            appearance7.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance7.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance7.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnCosting.Appearance = appearance7;
            this.btnCosting.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance8.Image = global::Accounting.Properties.Resources.tinhgiathanh2;
            this.btnCosting.HotTrackAppearance = appearance8;
            this.btnCosting.ImageSize = new System.Drawing.Size(80, 80);
            this.btnCosting.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnCosting.Location = new System.Drawing.Point(729, 165);
            this.btnCosting.Name = "btnCosting";
            this.btnCosting.Size = new System.Drawing.Size(92, 97);
            this.btnCosting.TabIndex = 42;
            // 
            // btnCostingCard
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.Image = global::Accounting.Properties.Resources.THETINHGIATHANH;
            appearance9.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance9.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance9.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnCostingCard.Appearance = appearance9;
            this.btnCostingCard.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance10.Image = global::Accounting.Properties.Resources.THETINHGIATHANH2;
            this.btnCostingCard.HotTrackAppearance = appearance10;
            this.btnCostingCard.ImageSize = new System.Drawing.Size(80, 80);
            this.btnCostingCard.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnCostingCard.Location = new System.Drawing.Point(956, 165);
            this.btnCostingCard.Name = "btnCostingCard";
            this.btnCostingCard.Size = new System.Drawing.Size(92, 97);
            this.btnCostingCard.TabIndex = 44;
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Image = ((System.Drawing.Image)(resources.GetObject("label4.Image")));
            this.label4.Location = new System.Drawing.Point(863, 196);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 35);
            this.label4.TabIndex = 43;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.Location = new System.Drawing.Point(636, 196);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 35);
            this.label3.TabIndex = 33;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(406, 196);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 35);
            this.label2.TabIndex = 31;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(176, 196);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 35);
            this.label1.TabIndex = 29;
            // 
            // UCCost
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.btnCostingCard);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnCosting);
            this.Controls.Add(this.btnUnfinishedEvaluation);
            this.Controls.Add(this.btnExpenses);
            this.Controls.Add(this.btnCostingPeriod);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "UCCost";
            this.Size = new System.Drawing.Size(1092, 424);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Infragistics.Win.Misc.UltraButton btnCostingPeriod;
        private Infragistics.Win.Misc.UltraButton btnExpenses;
        private Infragistics.Win.Misc.UltraButton btnUnfinishedEvaluation;
        private Infragistics.Win.Misc.UltraButton btnCosting;
        private Infragistics.Win.Misc.UltraButton btnCostingCard;
        private System.Windows.Forms.Label label4;
    }
}
