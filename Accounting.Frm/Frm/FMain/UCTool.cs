﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class UCTool : UserControl
    {
        public UCTool(FMain @this)
        {
            InitializeComponent();
            btnTool.Click += (s, e) => btnTool_Click(s, e, @this);
            btnTIBuyAndIncrement.Click += (s, e) => btnTIBuyAndIncrement_Click(s, e, @this);
            btnTITransfer.Click += (s, e) => btnFTITransfer_Click(s, e, @this);
            btnTIAllocation.Click += (s, e) => btnTIAllocation_Click(s, e, @this);
            btnTIDecrement.Click += (s, e) => btnFTIDecrement_Click(s, e, @this);
            btnTIAdjustment.Click += (s, e) => btnTIAdjustment_Click(s, e, @this);
        }

        private void btnTool_Click(object sender, EventArgs e,FMain fMain)
        {
            fMain.ShowFMaterialTool();
        }

        private void btnTIBuyAndIncrement_Click(object sender, EventArgs e,FMain fMain)
        {
            fMain.ShowFTIBuyIncrement();
        }

        private void btnFTITransfer_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFTITransfer();
        }

        private void btnTIAllocation_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFTIAllocation();
        }

        private void btnFTIDecrement_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFTIDecrement();
        }

        private void btnTIAdjustment_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFTIAdjustment();
        }
    }
}
