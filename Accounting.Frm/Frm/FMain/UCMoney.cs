﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Accounting.Core;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Accounting.Core.Domain;
using System.Linq;
using Accounting.Model;
using FX.Data;
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinExplorerBar;
using Microsoft.Win32;

namespace Accounting
{
    public partial class UCMoney : UserControl
    {
        public UCMoney(FMain @this)
        {
            InitializeComponent();
            btnMCPayment.Click += (s, e) => btnMCPayment_Click(s, e, @this);
            btnMCReceipt.Click += (s, e) => btnMCReceipt_Click(s, e, @this);
            btnRS05aDNN.Click += (s, e) => btnRS05aDNN_Click(s, e, @this);
        }

        private void btnMCPayment_Click(object sender, EventArgs e,FMain fMain)
        {
            fMain.ShowFMCPayment();
        }

        private void btnMCReceipt_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFMCReceipt();
        }

        private void btnRS05aDNN_Click(object sender, EventArgs e, FMain fMain)
        {
            new Accounting.Frm.FReport.FRSOQUYTM().ShowFormDialog(this);
        }
    }
}
