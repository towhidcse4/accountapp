﻿namespace Accounting
{
    partial class UCRepository
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCRepository));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            this.btnRSInventoryAdjustments = new Infragistics.Win.Misc.UltraButton();
            this.btnRSInwardOutward = new Infragistics.Win.Misc.UltraButton();
            this.btnRSInwardOutwardOutput = new Infragistics.Win.Misc.UltraButton();
            this.btnRSPricingOW = new Infragistics.Win.Misc.UltraButton();
            this.btnRS09DNN = new Infragistics.Win.Misc.UltraButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnRSInventoryAdjustments
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance1.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnRSInventoryAdjustments.Appearance = appearance1;
            this.btnRSInventoryAdjustments.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
            this.btnRSInventoryAdjustments.HotTrackAppearance = appearance2;
            this.btnRSInventoryAdjustments.ImageSize = new System.Drawing.Size(80, 80);
            this.btnRSInventoryAdjustments.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnRSInventoryAdjustments.Location = new System.Drawing.Point(300, 8);
            this.btnRSInventoryAdjustments.Name = "btnRSInventoryAdjustments";
            appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
            this.btnRSInventoryAdjustments.PressedAppearance = appearance3;
            this.btnRSInventoryAdjustments.Size = new System.Drawing.Size(92, 97);
            this.btnRSInventoryAdjustments.TabIndex = 33;
            // 
            // btnRSInwardOutward
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance4.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance4.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnRSInwardOutward.Appearance = appearance4;
            this.btnRSInwardOutward.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
            this.btnRSInwardOutward.HotTrackAppearance = appearance5;
            this.btnRSInwardOutward.ImageSize = new System.Drawing.Size(80, 80);
            this.btnRSInwardOutward.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnRSInwardOutward.Location = new System.Drawing.Point(103, 130);
            this.btnRSInwardOutward.Name = "btnRSInwardOutward";
            this.btnRSInwardOutward.Size = new System.Drawing.Size(92, 97);
            this.btnRSInwardOutward.TabIndex = 34;
            // 
            // btnRSInwardOutwardOutput
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
            appearance6.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance6.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance6.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnRSInwardOutwardOutput.Appearance = appearance6;
            this.btnRSInwardOutwardOutput.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance7.Image = ((object)(resources.GetObject("appearance7.Image")));
            this.btnRSInwardOutwardOutput.HotTrackAppearance = appearance7;
            this.btnRSInwardOutwardOutput.ImageSize = new System.Drawing.Size(80, 80);
            this.btnRSInwardOutwardOutput.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnRSInwardOutwardOutput.Location = new System.Drawing.Point(103, 280);
            this.btnRSInwardOutwardOutput.Name = "btnRSInwardOutwardOutput";
            this.btnRSInwardOutwardOutput.Size = new System.Drawing.Size(92, 97);
            this.btnRSInwardOutwardOutput.TabIndex = 35;
            // 
            // btnRSPricingOW
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.Image = ((object)(resources.GetObject("appearance8.Image")));
            appearance8.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance8.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance8.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnRSPricingOW.Appearance = appearance8;
            this.btnRSPricingOW.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance9.Image = ((object)(resources.GetObject("appearance9.Image")));
            this.btnRSPricingOW.HotTrackAppearance = appearance9;
            this.btnRSPricingOW.ImageSize = new System.Drawing.Size(80, 80);
            this.btnRSPricingOW.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnRSPricingOW.Location = new System.Drawing.Point(300, 398);
            this.btnRSPricingOW.Name = "btnRSPricingOW";
            this.btnRSPricingOW.Size = new System.Drawing.Size(92, 97);
            this.btnRSPricingOW.TabIndex = 36;
            // 
            // btnRS09DNN
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.Image = ((object)(resources.GetObject("appearance10.Image")));
            appearance10.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance10.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance10.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnRS09DNN.Appearance = appearance10;
            this.btnRS09DNN.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance11.Image = ((object)(resources.GetObject("appearance11.Image")));
            this.btnRS09DNN.HotTrackAppearance = appearance11;
            this.btnRS09DNN.ImageSize = new System.Drawing.Size(80, 80);
            this.btnRS09DNN.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnRS09DNN.Location = new System.Drawing.Point(473, 203);
            this.btnRS09DNN.Name = "btnRS09DNN";
            this.btnRS09DNN.Size = new System.Drawing.Size(92, 97);
            this.btnRS09DNN.TabIndex = 37;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(214, 294);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 33);
            this.label1.TabIndex = 38;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(214, 174);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 33);
            this.label2.TabIndex = 39;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.Location = new System.Drawing.Point(395, 236);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 33);
            this.label3.TabIndex = 40;
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Image = ((System.Drawing.Image)(resources.GetObject("label4.Image")));
            this.label4.Location = new System.Drawing.Point(329, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 63);
            this.label4.TabIndex = 42;
            // 
            // label5
            // 
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Image = ((System.Drawing.Image)(resources.GetObject("label5.Image")));
            this.label5.Location = new System.Drawing.Point(329, 323);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 63);
            this.label5.TabIndex = 43;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(108)))), ((int)(((byte)(158)))));
            this.label8.Location = new System.Drawing.Point(274, 187);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(6, 124);
            this.label8.TabIndex = 52;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(108)))), ((int)(((byte)(158)))));
            this.label6.Location = new System.Drawing.Point(278, 249);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 6);
            this.label6.TabIndex = 53;
            // 
            // UCRepository
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnRS09DNN);
            this.Controls.Add(this.btnRSPricingOW);
            this.Controls.Add(this.btnRSInwardOutwardOutput);
            this.Controls.Add(this.btnRSInwardOutward);
            this.Controls.Add(this.btnRSInventoryAdjustments);
            this.Name = "UCRepository";
            this.Size = new System.Drawing.Size(677, 507);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnRSInventoryAdjustments;
        private Infragistics.Win.Misc.UltraButton btnRSInwardOutward;
        private Infragistics.Win.Misc.UltraButton btnRSInwardOutwardOutput;
        private Infragistics.Win.Misc.UltraButton btnRSPricingOW;
        private Infragistics.Win.Misc.UltraButton btnRS09DNN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
    }
}
