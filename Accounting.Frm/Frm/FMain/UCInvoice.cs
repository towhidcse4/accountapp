﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting
{
    public partial class UCInvoice : UserControl
    {
        public UCInvoice(FMain @this)
        {
            InitializeComponent();
            btnInitializationReport.Click += (s, e) => btnInitializationReport_Click(s, e, @this);
            btnRegisterInvoice.Click += (s, e) => btnRegisterInvoice_Click(s, e, @this);
            btnPublishInvoice.Click += (s, e) => btnPublishInvoice_Click(s, e, @this);
            btnDestructionInvoice.Click += (s, e) => btnDestructionInvoice_Click(s, e, @this);
            btnLostInvoice.Click += (s, e) => btnLostInvoice_Click(s, e, @this);
            btnDeletedInvoice.Click += (s, e) => btnDeletedInvoice_Click(s, e, @this);
            btnAdjustAnnouncement.Click += (s, e) => btnAdjustAnnouncement_Click(s, e, @this);
            btnSituationInvoice.Click += (s, e) => btnSituationInvoice_Click(s, e, @this);
        }

        void btnInitializationReport_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFTT153Report();
        }

        void btnRegisterInvoice_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFTT153RegisterInvoice();
        }
        void btnPublishInvoice_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFTT153PublishInvoice();
        }
        void btnDestructionInvoice_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFTT153DestructionInvoice();
        }

        void btnLostInvoice_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFTT153LostInvoice();
        }
        void btnDeletedInvoice_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFTT153DeletedInvoice();
        }
        void btnAdjustAnnouncement_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFTT153AdjustAnnouncement();
        }
        void btnSituationInvoice_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFRTinhHinhSuDungHD();
        }

    }
}
