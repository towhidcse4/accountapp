﻿namespace Accounting
{
    partial class UCTax
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCTax));
            this.btnKhauTruThue = new Infragistics.Win.Misc.UltraButton();
            this.btnToKhai = new Infragistics.Win.Misc.UltraButton();
            this.btnNopThue = new Infragistics.Win.Misc.UltraButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnKhauTruThue
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.Image = global::Accounting.Properties.Resources.khautruthue;
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance1.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnKhauTruThue.Appearance = appearance1;
            this.btnKhauTruThue.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance2.Image = global::Accounting.Properties.Resources.khautruthue;
            this.btnKhauTruThue.HotTrackAppearance = appearance2;
            this.btnKhauTruThue.ImageSize = new System.Drawing.Size(80, 80);
            this.btnKhauTruThue.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnKhauTruThue.Location = new System.Drawing.Point(104, 292);
            this.btnKhauTruThue.Name = "btnKhauTruThue";
            this.btnKhauTruThue.Size = new System.Drawing.Size(92, 97);
            this.btnKhauTruThue.TabIndex = 41;
            // 
            // btnToKhai
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.Image = global::Accounting.Properties.Resources.tokhaithue1;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnToKhai.Appearance = appearance3;
            this.btnToKhai.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance4.Image = global::Accounting.Properties.Resources.tokhaithue1;
            this.btnToKhai.HotTrackAppearance = appearance4;
            this.btnToKhai.ImageSize = new System.Drawing.Size(80, 80);
            this.btnToKhai.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnToKhai.Location = new System.Drawing.Point(104, 3);
            this.btnToKhai.Name = "btnToKhai";
            this.btnToKhai.Size = new System.Drawing.Size(92, 97);
            this.btnToKhai.TabIndex = 42;
            // 
            // btnNopThue
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.Image = global::Accounting.Properties.Resources.NOPTHUE1;
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance5.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnNopThue.Appearance = appearance5;
            this.btnNopThue.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance6.Image = global::Accounting.Properties.Resources.NOPTHUE1;
            this.btnNopThue.HotTrackAppearance = appearance6;
            this.btnNopThue.ImageSize = new System.Drawing.Size(80, 80);
            this.btnNopThue.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnNopThue.Location = new System.Drawing.Point(386, 130);
            this.btnNopThue.Name = "btnNopThue";
            this.btnNopThue.Size = new System.Drawing.Size(92, 97);
            this.btnNopThue.TabIndex = 43;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(305, 162);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 33);
            this.label2.TabIndex = 57;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(108)))), ((int)(((byte)(158)))));
            this.label8.Location = new System.Drawing.Point(281, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(6, 288);
            this.label8.TabIndex = 56;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(216, 323);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 33);
            this.label1.TabIndex = 55;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.Location = new System.Drawing.Point(216, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 33);
            this.label3.TabIndex = 54;
            // 
            // UCTax
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnNopThue);
            this.Controls.Add(this.btnToKhai);
            this.Controls.Add(this.btnKhauTruThue);
            this.Name = "UCTax";
            this.Size = new System.Drawing.Size(580, 392);
            this.ResumeLayout(false);

        }

        #endregion
        public Infragistics.Win.Misc.UltraButton btnKhauTruThue;
        public Infragistics.Win.Misc.UltraButton btnToKhai;
        public Infragistics.Win.Misc.UltraButton btnNopThue;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
    }
}
