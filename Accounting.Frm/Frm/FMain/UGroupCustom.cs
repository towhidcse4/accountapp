﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.Misc;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinExplorerBar;

namespace Accounting
{
    public partial class UGroupCustom : UserControl
    {
        FMain parent = null;
        string groupKey = "";
        BindingList<UltraButton> buttons = new BindingList<UltraButton>();
        int maxPosition = 0;
        int maxIndex = 0;
        public UGroupCustom(FMain fMain, string key)
        {
            InitializeComponent();
            parent = fMain;
            groupKey = key;
            System.Windows.Forms.ToolTip tBtnAddItem = new System.Windows.Forms.ToolTip();
            tBtnAddItem.SetToolTip(btnAddItem, "Thêm mới lỗi tắt");
            System.Windows.Forms.ToolTip tBtnSetting = new System.Windows.Forms.ToolTip();
            tBtnSetting.SetToolTip(btnSettings, "Tùy chỉnh");
            ConfigControl(fMain, key);
        }
        void ConfigControl(FMain fMain, string key)
        {
            if (fMain.ConfigControls != null)
            {
                var first = fMain.ConfigControls.FirstOrDefault(x => x.UserId == Authenticate.User.userid);
                if (first != null)
                {
                    var items = first.Items.Where(b => b.GroupKey == key).OrderBy(b => b.Position).ToList();
                    foreach (var item in items)
                    {
                        var b = new UltraButton();
                        b.Name = item.ItemKey;
                        b.Tag = item.MappingCode;
                        b.Cursor = Cursors.Hand;
                        b.Visible = true;
                        b.ImageSize = new System.Drawing.Size(80, 80);
                        b.Appearance.ImageHAlign = Infragistics.Win.HAlign.Center;
                        b.Appearance.ImageVAlign = Infragistics.Win.VAlign.Middle;
                        if (item.Image != null)
                            b.Appearance.Image = item.Image.ByteArrayToImage();
                        b.MouseUp += Button_MouseUp;
                        b.Click += ButtonItem_Click;
                        this.Controls.Add(b);
                        System.Windows.Forms.ToolTip toolTip = new System.Windows.Forms.ToolTip();
                        toolTip.SetToolTip(b, item.Caption);
                        buttons.Add(b);
                        var t = item.ItemKey.Split(new[] { "ItemCustom_" }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
                        if (t == null) continue;
                        var index = Convert.ToInt32(t);
                        if (index > maxIndex) maxIndex = index;
                    }
                    var last = items.LastOrDefault();
                    if (last != null)
                        maxPosition = last.Position;
                    //maxIndex = items.Max(b => Convert.ToInt32(b.ItemKey.Split(new[] { "ItemCustom_" }, StringSplitOptions.RemoveEmptyEntries)[0]));
                }
            }
            //btnAddItem.MouseHover += (s, e) => Button_MouseHover(s, e, btnAddItem);
            //btnAddItem.MouseLeave += Button_MouseLeave;
            if (buttons.Count < 28)
                buttons.Add(btnAddItem);
            else
                btnAddItem.Visible = false;
            ConfigButtons(buttons);
        }
        void ConfigButtons(BindingList<UltraButton> buttons)
        {
            var size = new Size(92, 97);
            var pBegin = new Point(38, 30);
            var spaceX = 38;
            var spaceY = 30;
            int i = 0;
            //foreach (var button in buttons)
            //{
            int k = 0;
            while (k < 4 && i < buttons.Count)
            {
                for (int j = 0; j < 7 && i < buttons.Count; j++)
                {
                    buttons[i].Visible = true;
                    buttons[i].Location = new Point(pBegin.X + ((size.Width + spaceX) * j), pBegin.Y + ((size.Height + spaceY) * k));
                    buttons[i].Size = size;
                    i++;
                }
                k++;
            }
            //}
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            if (parent == null) return;
            var fMain = new FMain(true);
            var lstItems = new List<Accounting.Core.Domain.ControlsMapping.MappingControl>();
            foreach (var group in fMain.uExBar.Groups)
            {
                if (group.Key.StartsWith("GroupCustom_") || group.Key == "Desktop") continue;
                var parentCode = string.Format("Parent_{0}", group.Index);
                lstItems.Add(new Accounting.Core.Domain.ControlsMapping.MappingControl { Code = parentCode, Name = group.Text, IsParent = true });
                foreach (var item in group.Items)
                {
                    lstItems.Add(new Accounting.Core.Domain.ControlsMapping.MappingControl { Code = item.Key, Name = item.Text, IsParent = false, ParentCode = parentCode });
                }
            }
            var lstCategory = new List<Accounting.Core.Domain.ControlsMapping.MappingControl>();
            foreach (Infragistics.Win.UltraWinToolbars.RibbonGroup group in fMain.uToolbarsManager.Ribbon.Tabs["tabCatalog"].Groups)
            {
                var parentCode = string.Format("Parent_{0}", group.Key);
                lstCategory.Add(new Accounting.Core.Domain.ControlsMapping.MappingControl { Code = parentCode, Name = group.Caption, IsParent = true });
                foreach (var item in group.Tools)
                {
                    if (item is Infragistics.Win.UltraWinToolbars.PopupMenuTool)
                    {
                        foreach (var iitem in ((Infragistics.Win.UltraWinToolbars.PopupMenuTool)item).Tools)
                        {
                            lstCategory.Add(new Accounting.Core.Domain.ControlsMapping.MappingControl { Code = string.Format("Category_{0}", iitem.Key), Name = iitem.SharedProps.Caption, IsParent = false, ParentCode = parentCode });
                        }
                    }
                    else
                        lstCategory.Add(new Accounting.Core.Domain.ControlsMapping.MappingControl { Code = string.Format("Category_{0}", item.Key), Name = item.SharedProps.Caption, IsParent = false, ParentCode = parentCode });
                }
            }
            if (!fMain.IsDisposed) fMain.Dispose();
            var f = new FNewCustomButton(groupKey, lstItems, lstCategory);
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                var item = f.NewItem;
                var first = parent.ConfigControls.FirstOrDefault(x => x.UserId == Authenticate.User.userid);
                if (first == null)
                {
                    first = new Core.Domain.ControlsMapping.Config { UserId = Authenticate.User.userid };
                    parent.ConfigControls.Add(first);
                }
                if (first != null && first.Items.Where(b => b.GroupKey == groupKey).Count() > 0)
                {
                    maxIndex++;
                    maxPosition++;
                }
                item.ItemKey = string.Format("ItemCustom_{0}", maxIndex);
                item.Position = maxPosition;
                if (first != null)
                {
                    first.Items.Add(item);
                }
                var button = new UltraButton();
                button.Name = item.ItemKey;
                button.Tag = item.MappingCode;
                button.Cursor = Cursors.Hand;
                button.Visible = true;
                button.ImageSize = new System.Drawing.Size(80, 80);
                button.Appearance.ImageHAlign = Infragistics.Win.HAlign.Center;
                button.Appearance.ImageVAlign = Infragistics.Win.VAlign.Middle;
                if (item.Image != null)
                    button.Appearance.Image = item.Image.ByteArrayToImage();
                button.MouseUp += Button_MouseUp;
                button.Click += ButtonItem_Click;
                this.Controls.Add(button);
                System.Windows.Forms.ToolTip toolTip = new System.Windows.Forms.ToolTip();
                toolTip.SetToolTip(button, item.Caption);
                buttons.Add(button);
                buttons.Remove(btnAddItem);
                if (buttons.Count < 28)
                    buttons.Add(btnAddItem);
                else
                    btnAddItem.Visible = false;
                ConfigButtons(buttons);
            }
        }

        void Button_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                UltraButton button = (UltraButton)sender;
                if (!popButton.IsDisplayed)
                {
                    btnDelete.Tag = button;
                    popButton.Show(button, new Point((button.GetLocation().X + button.Size.Width), (button.GetLocation().Y - 8)));
                }
                else
                {
                    if (btnDelete.Tag != null && btnDelete.Tag is UltraButton)
                    {
                        var b = (UltraButton)btnDelete.Tag;
                        if (b != button)
                        {
                            popButton.Close();
                            Button_MouseUp(sender, e);
                        }
                    }
                }
            }
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            var first = parent.ConfigControls.FirstOrDefault(x => x.UserId == Authenticate.User.userid);
            if (first == null) return;
            var f = new FButtonCustomSettings(first.Items.Where(b => b.GroupKey == groupKey).OrderBy(b => b.Position).ToList());
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                foreach (var item in f.Buttons)
                {
                    var btn = first.Items.FirstOrDefault(b => b.GroupKey == groupKey && b.ItemKey == item.ItemKey);
                    if (btn != null)
                        btn.Position = item.Position;
                }
                foreach (var button in buttons)
                {
                    if (!button.Name.StartsWith("ItemCustom_")) continue;
                    this.Controls.Remove(button);
                }
                buttons.Clear();
                ConfigControl(parent, groupKey);
            }
        }

        private void UGroupCustom_MouseHover(object sender, EventArgs e)
        {
            if (popButton.IsDisplayed)
                popButton.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (btnDelete.Tag != null && btnDelete.Tag is UltraButton && parent != null && parent.ConfigControls != null)
            {
                var button = (UltraButton)btnDelete.Tag;
                var first = parent.ConfigControls.FirstOrDefault(x => x.UserId == Authenticate.User.userid);
                if (first == null) return;
                if (MSG.MessageBoxStand("Bạn thực sự muốn xóa lối tắt này?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    var t = button.Name.Split(new[] { "ItemCustom_" }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
                    if (t == null) return;
                    var index = Convert.ToInt32(t);
                    var thisButton = first.Items.FirstOrDefault(b => b.ItemKey == button.Name);
                    if (thisButton == null) return;
                    var position = thisButton.Position;
                    first.Items.Remove(thisButton);
                    buttons.Remove(button);
                    this.Controls.Remove(button);
                    foreach (var item in first.Items.Where(b => b.GroupKey == groupKey).OrderBy(b => b.ItemKey).ToList())
                    {
                        var i = Convert.ToInt32(item.ItemKey.Split(new[] { "ItemCustom_" }, StringSplitOptions.RemoveEmptyEntries)[0]);
                        if (i > index)
                        {
                            i--;
                            var btn = buttons.FirstOrDefault(b => b.Name == item.ItemKey);
                            if (btn != null) btn.Name = string.Format("ItemCustom_{0}", i);
                            item.ItemKey = string.Format("ItemCustom_{0}", i);
                        }
                        if (item.Position > position)
                            item.Position--;
                    }
                    if (maxIndex > 0) maxIndex--;
                    if (maxPosition > 0) maxPosition--;
                    buttons.Remove(btnAddItem);
                    if (buttons.Count < 28)
                        buttons.Add(btnAddItem);
                    else
                        btnAddItem.Visible = false;
                    ConfigButtons(buttons);
                    parent.Activate();
                    parent.BringToFront();
                }
            }
        }
        private void ButtonItem_Click(object sender, EventArgs e)
        {
            var button = (UltraButton)sender;
            if (button.Tag == null || (button.Tag != null && !(button.Tag is string))) return;
            var key = button.Tag.ToString();
            if (string.IsNullOrEmpty(key)) return;
            if (parent == null) return;
            if (key.StartsWith("Category_"))
                parent.ToolBarToolClick(key.Split(new[] { "Category_" }, StringSplitOptions.RemoveEmptyEntries)[0]);
            else
                parent.ItemClick(key);
        }

        private void btnAddItem_MouseHover(object sender, EventArgs e)
        {
            if (popButton.IsDisplayed)
                popButton.Close();
        }
    }
}
