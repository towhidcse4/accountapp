﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FExportExcelAccountingObject : Form
    {
        public readonly IAccountingObjectService _IAccountingObjectService;
        private IAccountingObjectGroupService _IAccountingObjectGroupService;
        List<AccountingObject> listAccountingObject = new List<AccountingObject>();
        List<AccountingObjectGroup> listAccountingObjectGroup = new List<AccountingObjectGroup>();
        List<VoucherAccountingObject> listVoucherAccountingObject = new List<VoucherAccountingObject>();
        public FExportExcelAccountingObject()
        {
            InitializeComponent();
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            _IAccountingObjectGroupService = IoC.Resolve<IAccountingObjectGroupService>();
            cbbObjectType.SelectedIndex = 4;
            ultraCheckEditor1.Checked = true;
            LoadDuLieu(true);

        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            // listAccountingObject = _IAccountingObjectService.GetAll().Where(p => p.IsEmployee == false).ToList();
            listAccountingObject = _IAccountingObjectService.GetListAccountingObjectByEmployee(false);
            _IAccountingObjectService.UnbindSession(listAccountingObject);
            listAccountingObject = _IAccountingObjectService.GetListAccountingObjectByEmployee(false);
            var listStartN = listAccountingObject.Where(p => p.AccountingObjectName.StartsWith("N")).ToList();
            listAccountingObjectGroup = _IAccountingObjectGroupService.GetAll();
            listVoucherAccountingObject = _IAccountingObjectService.GetListVoucherAccoungtingObjectView();
            //List<VoucherAccountingObject> a = _IAccountingObjectService.GetListVoucherAccoungtingObjectView();
            //List<BankH> list = _IBankServiceH.GetAll();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = listAccountingObject.OrderBy(p => p.AccountingObjectCode).ToArray(); //sắp xếp danh sách theo thứ tự Mã KH/NCC tăng dần
            foreach (var item in listAccountingObject)
            {
                if (item.AccountObjectGroupID != null)
                {
                    AccountingObjectGroup accGroup = listAccountingObjectGroup.Where(p => p.ID == item.AccountObjectGroupID).SingleOrDefault();

                    item.AccountObjectGroupIdNameView = accGroup != null ? accGroup.AccountingObjectGroupCode : null;
                }

            }
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            if (configGrid) ConfigGrid(uGrid);
            #endregion
            WaitingFrm.StopWaiting();
        }
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.AccountingObject_TableName);
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Header.Caption = "Mã khách hàng, NCC";
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Caption = "Tên khách hàng, NCC";
            uGrid.DisplayLayout.Bands[0].Columns["DepartmentName"].Hidden = true;
            uGrid.DisplayLayout.Bands[0].Columns["ContactTitle"].Hidden = true;
            uGrid.DisplayLayout.Bands[0].Columns["TaxCode"].Hidden = false;
            uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Extended;
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            UltraGrid ultraGridExport = uGridExportExcel;
            ultraGridExport.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            // LoadDuLieuGridExport(ultraGridExport);
            if (listAccountingObject != null)
            {
                foreach (var item in listAccountingObject )
                { if(item.ScaleType==1)
                    {
                        item.ScaleTypeName = "Cá nhân";
                    }
                else if (item.ScaleType == 0)
                    {
                        item.ScaleTypeName = "Tổ chức";
                    }
                }
            }
            ultraGridExport.DataSource = listAccountingObject.ToArray();
            Utils.ConfigGrid(ultraGridExport, TextMessage.ConstDatabase.AccountingObjectExportExcel_TableName);
            Utils.ExportExcel(ultraGridExport, "Export_DMKhachHangNhaCungCap");
        }

        private void cbbObjectType_ValueChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            if (cbbObjectType.SelectedIndex == 0)
            {
                if(ultraCheckEditor1.Checked)
                {
                    listAccountingObject = _IAccountingObjectService.GetAccountingObjects(0,true);
                }
                else
                {
                    listAccountingObject = _IAccountingObjectService.GetAccountingObjectsByType(0);
                }
            }
            else if (cbbObjectType.SelectedIndex == 1)
            {
                if (ultraCheckEditor1.Checked)
                {
                    listAccountingObject = _IAccountingObjectService.GetAccountingObjects(1, true);
                }
                else
                {
                    listAccountingObject = _IAccountingObjectService.GetAccountingObjectsByType(1);
                }
            }
            else if (cbbObjectType.SelectedIndex == 2)
            {
                if (ultraCheckEditor1.Checked)
                {
                    listAccountingObject = _IAccountingObjectService.GetAccountingObjects(2, true);
                }
                else
                {
                    listAccountingObject = _IAccountingObjectService.GetAccountingObjectsByType(2);
                }
            }
            else if (cbbObjectType.SelectedIndex == 3)
            {
                if (ultraCheckEditor1.Checked)
                {
                    listAccountingObject = _IAccountingObjectService.GetAccountingObjects(3, true);
                }
                else
                {
                    listAccountingObject = _IAccountingObjectService.GetAccountingObjectsByType(3);
                }
            }
            else if (cbbObjectType.SelectedIndex == 4)
            {
                if (ultraCheckEditor1.Checked)
                {
                    listAccountingObject = _IAccountingObjectService.GetListAccountingObjectByEmployee(false).Where(p=>p.IsActive==true).ToList();
                }
                else
                {
                    listAccountingObject = _IAccountingObjectService.GetListAccountingObjectByEmployee(false).ToList();
                }
            }
            uGrid.DataSource = listAccountingObject.OrderBy(p => p.AccountingObjectCode).ToArray();
        }
        private void ultraCheckEditor1_CheckedValueChanged(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}
