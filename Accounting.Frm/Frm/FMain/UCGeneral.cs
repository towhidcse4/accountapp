﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class UCGeneral : UserControl
    {
        public UCGeneral(FMain @this)
        {
            InitializeComponent();
            btnFGOInterestAndLossTransfer.Click += (s, e) => btnFGOInterestAndLossTransfer_Click(s, e, @this);
            btnGOtherVoucher.Click += (s, e) => btnGOtherVoucher_Click(s, e, @this);
            btnGDBDateClosed.Click += (s, e) => btnGDBDateClosed_Click(s, e, @this);
            btnBaoCao.Click += (s, e) => btnBaoCao_Click(s, e, @this);
        }

        void btnGDBDateClosed_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFDBDateClosed();
        }

        private void btnGOtherVoucher_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFGOtherVoucher();
        }

        private void btnFGOInterestAndLossTransfer_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFGOInterestAndLossTransfer();
        }

        private void btnBaoCao_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFBaoCao();
        }
    }
}
