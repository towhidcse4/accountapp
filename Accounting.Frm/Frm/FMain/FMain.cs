﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Accounting.Core;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Accounting.Core.Domain;
using System.Linq;
using Accounting.Model;
using FX.Data;
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinExplorerBar;
using Microsoft.Win32;
using Infragistics.Win.UltraWinTabControl;
using System.IO;
using Accounting.Frm.FMain;
using Accounting.Frm.FReport;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Diagnostics;
using Microsoft.Office.Interop;
using System.Xml;
using System.Data;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.Reflection;



//using UltraTabControlNoBorder;

namespace Accounting
{
    public partial class FMain : CustormForm
    {
        public ISystemOptionService _ISystemOptionService { get { return IoC.Resolve<ISystemOptionService>(); } }
        private readonly ICustomerConfigService _ICustomerConfigSrvice;
        private readonly ISupplierServiceService _ISupplierServiceService;
        private Infragistics.Win.UltraWinToolbars.PopupGalleryTool accountGallery = new Infragistics.Win.UltraWinToolbars.PopupGalleryTool("popGallaryAccount");
        public static int? activeUxGroup = null;
        public static string keyUxItem = null;
        public static bool isUxGroupClick = false;
        UltraTabControl uTabMain = new UltraTabControl();
        public List<Accounting.Core.Domain.ControlsMapping.Config> ConfigControls = new List<Accounting.Core.Domain.ControlsMapping.Config>();
        private bool _notevent = false;
        private int maxGroupIndex = 0;
        //      System.ComponentModel.ComponentResourceManager resources1 = new System.ComponentModel.ComponentResourceManager(typeof(UCSalary));
        //For Update
        bool runCheckVersion = false;   //true: chạy chức năng Check Update   |false: không chạy chức năng Check Update
        public static bool CheckVer = false;
        string linkCheckVer = "http://xxx.vn/DownloadFile/Accounting/vesion.html";
        string nameUpdateTool = "EasyBooks.exe";
        string nameKlib = "Klib.dll";
        public Company NextSession;
        private Form currentFormBaseBusiness;
        bool _isDuLieuMau = false;
        bool _isBasebusiness = false;//add by cuongpv to release RAM
        public FMain(bool isDefault = false, bool isDuLieuMau = false)
        {
            _isDuLieuMau = isDuLieuMau;
            InitializeComponent();
            if (isDefault) return;
            InitializeExplorerBar();
            //uToolbarsManager.Ribbon.ApplicationMenuButtonImage = getImage();

            Color color = System.Drawing.Color.FromArgb(27, 84, 228);
            uExBar.NavigationSplitterAppearance.BackColor = color;
            //InitializeRibbon();
            ConfigGUI();
            _ICustomerConfigSrvice = IoC.Resolve<ICustomerConfigService>();
            _ISupplierServiceService = IoC.Resolve<ISupplierServiceService>();
            //Program.SysCofig = new ProgramConfig(_ICustomerConfigSrvice.Query.ToList<CustomerConfig>());
            uSttBar.Panels["Server"].Text = string.Format("Máy chủ: {0}", Authenticate.Server);
            uSttBar.Panels["Database"].Text = string.Format("Tên dữ liệu kế toán: {0}", Authenticate.Database);
            uSttBar.Panels["User"].Text = string.Format("Người dùng: {0}", Authenticate.UserName);

            uSttBar_MouseHover(uSttBar, null);
            ConfigByPermission();
            //AddUserControl(new UCDesktop(this) { Dock = DockStyle.Fill }, "Bàn làm việc");
            uExBar.ActiveGroup = activeUxGroup == null ? uExBar.Groups.All.Cast<UltraExplorerBarGroup>().FirstOrDefault() : uExBar.Groups[activeUxGroup.Value];
            uExBar.SelectedGroup = uExBar.ActiveGroup;
            ConfigSubGroupMain(new GroupEventArgs(uExBar.ActiveGroup), false);
            this.uTabMain.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.ultraTabControl1_SelectedTabChanged);
            // backup at 23h55'00
            //DateTime dt = new DateTime();
            //dt.Date.AddHours(23).AddMinutes(55).AddSeconds(00);
            //runCodeAt(getNextDate(dt, Scheduler.EveryDay), Scheduler.EveryDay);
            try
            {
                LoadVisibleHDC();
                LoadVisibleEInvoice();
                LoadModuleTax();

            }
            catch (Exception ex)
            {

            }




        }
        private Image getImage()
        {
            string exeFile = (new System.Uri(Assembly.GetEntryAssembly().CodeBase)).AbsolutePath;
            string exeDir = Path.GetDirectoryName(exeFile);//
            exeDir = exeDir.Replace(@"bin\Debug", "");
            string fullPath = Path.Combine(exeDir, exeDir + @"Resources\hdsoftx48.png");

            WebClient imgClient = new WebClient();
            String imgURL = fullPath;
            byte[] imageByteArray = imgClient.DownloadData(imgURL);
            MemoryStream ms = new MemoryStream(imageByteArray);
            System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
            return img;
        }
        private void Tabs_SelectedIndexChanged(Object sender, EventArgs e)
        {
            if (uTabMain != null)
            {
                if (uTabMain.SelectedTab is UCDesktop)
                {

                }
            }
        }
        public void LoadModuleTax()
        {
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "PPTTGTGT").Data == "Phương pháp trực tiếp trên doanh thu")
            {
                uExBar.Groups["Tax"].Items["FDeductionGTGT01"].Visible = false;
                uExBar.Groups["Tax"].Items["FTaxInvestmentProjects"].Visible = false;
                uExBar.Groups["Tax"].Items["FDirectGTGT04"].Visible = true;
                uExBar.Groups["Tax"].Items["TaxPeriod"].Visible = false;
            }
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "PPTTGTGT").Data == "Phương pháp khấu trừ")
                uExBar.Groups["Tax"].Items["FDirectGTGT04"].Visible = false;
        }

        public void LoadVisibleHDC()
        {
            uExBar.Groups["EInvoiceModule"].Items["FEIWaitingInvoice"].Visible = false;
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "CheckHDCD").Data == "1")
            {
                SystemOption systemOption = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "EI_IDNhaCungCapDichVu");
                SupplierService supplierService = _ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                if (supplierService.SupplierServiceCode == "VTE")
                {
                    uExBar.Groups["EInvoiceModule"].Items["FEIWaitingInvoice"].Visible = false;
                    uExBar.Groups["EInvoiceModule"].Items["FEIReplaceInvoice"].Visible = true;
                    uExBar.Groups["EInvoiceModule"].Items["FEIConvertedInvoice"].Visible = true;

                }
                else if (supplierService.SupplierServiceCode == "MIV")
                {
                    uExBar.Groups["EInvoiceModule"].Items["FEIWaitingInvoice"].Visible = false;
                    uExBar.Groups["EInvoiceModule"].Items["FEIReplaceInvoice"].Visible = false;
                    uExBar.Groups["EInvoiceModule"].Items["FEIConvertedInvoice"].Visible = false;
                }
                else if (supplierService.SupplierServiceCode == "SDS")
                {
                    uExBar.Groups["EInvoiceModule"].Items["FEIWaitingInvoice"].Visible = true;
                    uExBar.Groups["EInvoiceModule"].Items["FEIReplaceInvoice"].Visible = true;
                    uExBar.Groups["EInvoiceModule"].Items["FEIConvertedInvoice"].Visible = true;
                }
                else
                {
                    uExBar.Groups["EInvoiceModule"].Items["FEIWaitingInvoice"].Visible = true;
                    uExBar.Groups["EInvoiceModule"].Items["FEIReplaceInvoice"].Visible = true;
                    uExBar.Groups["EInvoiceModule"].Items["FEIConvertedInvoice"].Visible = true;
                }

            }
        }
        public void LoadVisibleEInvoice()
        {
            uExBar.Groups["EInvoiceModule"].Visible = false;
            uExBar.Groups["EInvoiceModule"].Enabled = false;
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "HDDT").Data == "1")
            {
                uExBar.Groups["EInvoiceModule"].Visible = true;
                uExBar.Groups["EInvoiceModule"].Enabled = true;
                //try
                //{
                //    SystemOption systemOption = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "EI_IDNhaCungCapDichVu");
                //    SupplierService supplierService = _ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                //    if (supplierService.SupplierServiceCode == "VTE")
                //    {
                //        uExBar.Groups["EInvoiceModule"].Items["FEIReplaceInvoice"].Visible = false;
                //        uExBar.Groups["EInvoiceModule"].Items["FEIAdjustInvoice"].Visible = false;

                //    }
                //    else if (supplierService.SupplierServiceCode == "SDS")
                //    {
                //        uExBar.Groups["EInvoiceModule"].Items["FEIReplaceInvoice"].Visible = true;
                //        uExBar.Groups["EInvoiceModule"].Items["FEIAdjustInvoice"].Visible = true;
                //    }
                //}
                //catch(Exception ex)
                //{

                //}
            }
        }
        private void ConfigByPermission()
        {
            if (!Authenticate.Roles("ADMIN"))
            {
                Utils.ConfigTopMenuDetails(uToolbarsManager, "btnManagerUsers", 0);
                Utils.ConfigTopMenuDetails(uToolbarsManager, "btnRole", 0);
                foreach (var group in uExBar.Groups)
                {
                    if (group.Key.StartsWith("GroupCustom_") || group.Key == "Desktop") continue;
                    int i = 0;
                    while (i < group.Items.Count)
                    {
                        if (!Authenticate.Permissions("SD", group.Items[i].Key))
                            group.Items.RemoveAt(i);
                        else
                            i++;
                    }
                }
                foreach (Infragistics.Win.UltraWinToolbars.RibbonGroup group in this.uToolbarsManager.Ribbon.Tabs["tabCatalog"].Groups)
                {
                    foreach (var tool in group.Tools)
                    {
                        if (tool is Infragistics.Win.UltraWinToolbars.PopupMenuTool)
                        {
                            foreach (var item in ((Infragistics.Win.UltraWinToolbars.PopupMenuTool)tool).Tools)
                            {
                                if (!Authenticate.Permissions("SD", item.Key))
                                    Utils.ConfigTopMenuDetails(uToolbarsManager, item.Key, 1);
                            }
                        }
                        else
                            if (!Authenticate.Permissions("SD", tool.Key))
                            Utils.ConfigTopMenuDetails(uToolbarsManager, tool.Key, 1);
                    }
                }
                foreach (Infragistics.Win.UltraWinToolbars.RibbonGroup group in this.uToolbarsManager.Ribbon.Tabs["tabBusiness"].Groups)
                {
                    foreach (var tool in group.Tools)
                    {
                        if (!Authenticate.Permissions("SD", tool.Key))
                            Utils.ConfigTopMenuDetails(uToolbarsManager, tool.Key, 1);
                    }
                }
            }
            //if (!Authenticate.UserName.Equals("ADMIN"))
            //{
            //}
        }

        private void InitializeExplorerBar()
        {
            System.Xml.Serialization.XmlSerializer reader =
                        new System.Xml.Serialization.XmlSerializer(typeof(List<Accounting.Core.Domain.ControlsMapping.Config>));
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            System.IO.StreamReader file = new
               System.IO.StreamReader(string.Format("{0}\\Config\\control.config", path));
            List<Accounting.Core.Domain.ControlsMapping.Config> overview = new List<Accounting.Core.Domain.ControlsMapping.Config>();
            try
            {
                overview = (List<Accounting.Core.Domain.ControlsMapping.Config>)reader.Deserialize(file);
                if (overview != null)
                {
                    ConfigControls = overview;
                    var first = overview.FirstOrDefault(x => x.UserId == Authenticate.User.userid);
                    if (first == null) return;
                    if (first.Groups.Count > 0)
                    {
                        foreach (var item in first.Groups)
                        {
                            var group = uExBar.Groups.All.Cast<UltraExplorerBarGroup>().FirstOrDefault(g => g.Key == item.KeyCode);
                            if (group != null)
                            {
                                uExBar.Groups.Remove(group);
                                uExBar.Groups.Insert(item.Position, group);
                                group.Visible = item.IsVisible;
                            }
                            else if (!item.IsSecurity)
                            {
                                var nGroup = new UltraExplorerBarGroup
                                {
                                    Key = item.KeyCode,
                                    Text = item.Caption,
                                    ToolTipText = item.ToolTip,
                                    Visible = item.IsVisible,
                                    Tag = "custom"
                                };
                                uExBar.Groups.Insert(item.Position, nGroup);
                            }
                        }
                        //for (int i = 0; i < uExBar.Groups.Count; i++)
                        //{
                        //    var f = overview.FirstOrDefault(c => c.KeyCode == uExBar.Groups[i].Key);
                        //    if (f != null)
                        //    {
                        //        uExBar.Groups.Remove(uExBar.Groups[i]);
                        //        uExBar.Groups.Insert(f.Position, uExBar.Groups[i]);
                        //        uExBar.Groups[i].Visible = f.IsVisible;
                        //    }
                        //}
                        var gFirst = first.Groups.Where(g => g.KeyCode.StartsWith("GroupCustom_")).OrderByDescending(g => g.KeyCode).ToList().FirstOrDefault();
                        if (gFirst != null)
                            maxGroupIndex = Convert.ToInt32(gFirst.KeyCode.Split(new[] { "GroupCustom_" }, StringSplitOptions.RemoveEmptyEntries)[0]);
                    }
                }
            }
            catch (Exception)
            {
                file.Close();
                return;
            }
            file.Close();
        }


        #region Function/// <summary>
        /// Initialize the galleries and other aspects of the ribbon
        /// </summary>
        private void InitializeRibbon()
        {
            // Make the ribbon visible
            this.uToolbarsManager.Ribbon.Visible = true;

            // Add a "Home" tab to the ribbon
            Infragistics.Win.UltraWinToolbars.RibbonTab tab = this.uToolbarsManager.Ribbon.Tabs.Add("Home");
            tab.KeyTip = "H";

            // Add a "Format" group to the home tab
            //Infragistics.Win.UltraWinToolbars.RibbonGroup group = tab.Groups.Add("Format");
            //groupAccount.KeyTip = "ZF";

            // Add galleries to the ribbon for changing back color, font size, and font family
            this.accountGallery = new Infragistics.Win.UltraWinToolbars.PopupGalleryTool("FontColor");
            this.uToolbarsManager.Tools.Add(this.accountGallery);

            // Add the three galleries to the format group in the ribbon
            groupAccount.Tools.AddTool("FontColor");

            // Hook the event needed to implement Live Preview
            //this.uToolbarsManager.GalleryToolActiveItemChange += new Infragistics.Win.UltraWinToolbars.GalleryToolItemEventHandler(ultraToolbarsManager1_GalleryToolActiveItemChange);
            // Hook the event needed to commit the user's selection to the rich text box formatting.
            //this.uToolbarsManager.GalleryToolItemClick += new Infragistics.Win.UltraWinToolbars.GalleryToolItemEventHandler(ultraToolbarsManager1_GalleryToolItemClick);

            // Initialize the three galleries by setting various properties and popuplating them with items
            this.InitializeColorGallery();
        }
        /// <summary>
        /// Initialize the items in the color gallery
        /////// </summary>
        private void InitializeColorGallery()
        {
            this.accountGallery.SharedProps.Caption = "Back Color";
            this.accountGallery.SharedProps.KeyTip = "C";

            // Put some space between the items and give them a gray border
            this.accountGallery.ItemSettings.Appearance.BorderColor = Color.Gray;
            this.accountGallery.ItemPadding.Bottom = 1;
            this.accountGallery.ItemPadding.Right = 1;

            // Set the size of the items in the in-ribbon gallery preview
            this.accountGallery.ItemSizeInPreview = new Size(15, 15);
            this.accountGallery.MinPreviewColumns = 10;

            // Add a gallery item for each color in the KnownColor enumeration
            //foreach (KnownColor knownColor in Enum.GetValues(typeof(KnownColor)))
            //{
            //    // Create the color from the KnownColor value
            //    Color color = Color.FromKnownColor(knownColor);

            //    // Add the item to the gallery items collection
            //    Infragistics.Win.UltraWinToolbars.GalleryToolItem item = this.accountGallery.Items.Add(color.Name);
            //    item.Settings.Appearance.BackColor = color;

            //    // Initialize the title and description which will be displayed in the drop down
            //    item.Title = color.Name;
            //    item.Description = "R = " + color.R + ", G = " + color.G + ", B = " + color.B;

            //    // Set the color as the Tag of the item so it can be accessed later
            //    item.Tag = color;
            //}
            // Add the item to the gallery items collection
            Infragistics.Win.UltraWinToolbars.GalleryToolItem itemAccount = this.accountGallery.Items.Add("btnAccount");
            itemAccount.Settings.Appearance.Image = Properties.Resources.sec_uynhiemchi;
            itemAccount.Settings.Appearance.ImageVAlign = VAlign.Middle;
            itemAccount.Settings.Appearance.ImageHAlign = HAlign.Center;
            // Initialize the title and description which will be displayed in the drop down
            itemAccount.Title = "Hệ thống tài khoản";

        }
        //void UltraToolbarsManagerFunction(Control ufrm, string titleFrm)
        //{
        //    ShowMenuPal("palAddControl");
        //    palAddControl.ClientArea.Controls.Clear();
        //    palAddControl.ClientArea.Controls.Add(ufrm);
        //    ufrm.Dock = DockStyle.Fill;
        //    groupMainMenu.Text = titleFrm;
        //}

        ///// <summary>
        ///// hiện một UltraPanel đang Active (dùng trong xử lý giao diện)
        ///// </summary>
        ///// <param name="namePal">Key đại diện cho một UltraPanel</param>
        //void ShowMenuPal(string namePal)
        //{
        //    #region Ẩn all control
        //    palAddControl.Visible = false;
        //    palMainMenu.Visible = false;
        //    #endregion

        //    switch (namePal)
        //    {
        //        case "palAddControl": palAddControl.Visible = true; palAddControl.Dock = DockStyle.Fill; break;
        //        case "palMainMenu":
        //            {
        //                palMainMenu.Visible = true;
        //                palMainMenu.Location = new Point(53, 40);   //53, 40
        //                groupMainMenu.Text = "Quy trình nghiệp vụ";
        //                //Thay đổi Size
        //                int width = (groupMainMenu.Size.Width - palMainMenu.Size.Width) / 2;
        //                int height = (groupMainMenu.Size.Height - palMainMenu.Size.Height) / 2;
        //                palMainMenu.Location = new Point(width, height);
        //            }
        //            break;
        //    }
        //}

        void ShowFormAndGetData<T>(string title, string subSystemCode, int loaiForm = -1, List<int> lstTypeId = null, int defaultTypeId = 0)
        {
            //add by cuongpv
            _isBasebusiness = true;
            var baseBn = this.Controls.Find("BaseBSS", true).FirstOrDefault();
            if (baseBn != null)
            {
                (baseBn as Form).Close();
            }
            //end add by cuongpv

            ShowForm(title, new BaseBusiness<T>(title, subSystemCode, loaiForm, lstTypeId, defaultTypeId));
        }
        #endregion

        #region Utils
        private static void ConfigGUI()
        {
            //UltraExplorerBar
            const string showMoreButtons = "Hiển thị toàn bộ phân hệ";
            const string showFewerButtons = "Hiển thị các phân hệ điển hình";
            const string navPaneOptions = "Điều chỉnh các phân hệ";
            const string addOrRemoveButtons = "Ẩn/Hiện phân hệ";

            Infragistics.Win.UltraWinExplorerBar.Resources.Customizer.SetCustomizedString("NavigationQuickCustomizeMenu_ShowMoreButtons", showMoreButtons);
            Infragistics.Win.UltraWinExplorerBar.Resources.Customizer.SetCustomizedString("NavigationQuickCustomizeMenu_ShowFewerButtons", showFewerButtons);
            Infragistics.Win.UltraWinExplorerBar.Resources.Customizer.SetCustomizedString("NavigationQuickCustomizeMenu_NavigationPaneOptions", navPaneOptions);
            Infragistics.Win.UltraWinExplorerBar.Resources.Customizer.SetCustomizedString("NavigationQuickCustomizeMenu_AddOrRemoveButtons", addOrRemoveButtons);
        }

        #region Hàm mở Form
        #region Quỹ
        /// <summary>
        /// Mở Form Phiếu thu
        /// </summary>
        public void ShowFMCReceipt()
        {
            var subSystemCode = "FMCReceipt";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<MCReceipt>(resSystem.Title_01, subSystemCode, defaultTypeId: 0);
        }

        /// <summary>
        /// Mở Form Phiếu chi
        /// </summary>
        public void ShowFMCPayment()
        {
            var subSystemCode = "FMCPayment";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<MCPayment>(resSystem.Title_02, subSystemCode, defaultTypeId: 0);
        }
        /// <summary>
        /// Mở form Kiểm kê quỹ
        /// </summary>
        public void ShowFMCAudit()
        {
            var subSystemCode = "FMCAudit";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<MCAudit>(resSystem.Title_96, subSystemCode);
        }
        /// <summary>
        /// Mở Form sổ quỹ
        /// </summary>
        public void ShowFRS05aDNN()
        {
            var subSystemCode = "Rpt_FRS05aDNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS05aDNN(subSystemCode).ShowDialog(this);
        }
        /// <summary>
        /// Mở form báo cáo FRS03a1DNN
        /// </summary>
        public void ShowFRS03a1DNN()
        {
            var subSystemCode = "Rpt_FRS03a1DNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS03a1DNN(subSystemCode).ShowDialog(this);
        }
        /// <summary>
        /// Mở form báo cáo FRS03a2DNN
        /// </summary>
        public void ShowFRS03a2DNN()
        {
            var subSystemCode = "Rpt_FRS03a2DNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS03a2DNN(subSystemCode).ShowDialog(this);
        }
        #endregion

        #region Mua hàng
        /// <summary>
        /// Mở Form Đơn mua hàng
        /// </summary>
        public void ShowFPPOrder()
        {
            var subSystemCode = "FPPOrder";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<PPOrder>(resSystem.Title_10, subSystemCode);
        }

        /// <summary>
        /// Mở Form Hóa đơn Mua hàng
        /// </summary>
        public void ShowFPPInvoice1()
        {
            var subSystemCode = "FPPInvoice1";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<PPInvoice>(resSystem.Title_11, subSystemCode, 0);
        }

        /// <summary>
        /// Mở Form Mua hàng không qua kho
        /// </summary>
        public void ShowFPPInvoice2()
        {
            var subSystemCode = "FPPInvoice2";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<PPInvoice>(resSystem.Title_09, subSystemCode, 1);
        }

        /// <summary>
        /// Mở Form Nhập hóa đơn dịch vụ
        /// </summary>
        public void ShowFPPService()
        {
            var subSystemCode = "FPPService";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<PPService>(resSystem.Title_13, subSystemCode);
        }

        /// <summary>
        /// Mở Form Hàng mua trả lại/giảm giá
        /// </summary>
        public void ShowFPPDiscountReturn()
        {
            var subSystemCode = "FPPDiscountReturn";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            List<int> lstTypeId = new List<int> { 220, 230 };
            ShowFormAndGetData<PPDiscountReturn>(resSystem.Title_14, subSystemCode, -1, lstTypeId, 220);
        }

        /// <summary>
        /// Mở Form Trả tiền nhà cung cấp
        /// </summary>
        public void ShowFPPPayVendor()
        {
            var subSystemCode = "FPPPayVendor";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm(resSystem.Title_15, new FPPPayVendor());
        }

        /// <summary>
        /// Mở form đối trừ chứng từ
        /// </summary>
        public void ShowFPPExceptVoucher()
        {
            var subSystemCode = "FPPExceptVoucher";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            //HUYPD Edit Show Form
            //new FPPExceptVoucher().ShowFormDialog(this);
            new FPPExceptVoucher().ShowDialogHD(this);
        }

        /// <summary>
        /// Mở form nhận hóa đơn
        /// </summary>
        public void ShowFPPGetInvoices()
        {
            var subSystemCode = "FPPGetInvoices";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            //HUYPD Edit Show Form
            //new FPPGetInvoices().ShowFormDialog(this)
            new FPPGetInvoices().ShowDialogHD(this);
        }

        /// <summary>
        /// Mở form báo cáo FRS03a3DNN
        /// </summary>
        public void ShowFRS03a3DN()
        {
            var subSystemCode = "Rpt_FRS03a3DN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS03a3DNN(subSystemCode).ShowFormDialog(this);
        }

        /// <summary>
        /// Mở form tổng hợp công nợ phải trả
        /// </summary>
        public void ShowFRPayableSummary()
        {
            var subSystemCode = "Rpt_FRPayableSummary";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRPayableSummary(subSystemCode).ShowFormDialog(this);
        }

        /// <summary>
        /// Mở form sổ chi tiết mua hàng
        /// </summary>
        public void ShowFRPurchaseDetailSupplerInventoryItem()
        {
            var subSystemCode = "Rpt_FRPurchaseDetailSupplerInventoryItem";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRPurchaseDetailSupplerInventoryItem(subSystemCode).ShowFormDialog(this);
        }
        #endregion

        #region Bán hàng
        /// <summary>
        /// Mở Form Báo giá
        /// </summary>
        public void ShowFSAQuote()
        {
            var subSystemCode = "FSAQuote";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<SAQuote>(resSystem.Title_19, subSystemCode);
        }

        /// <summary>
        /// Mở Form Đơn đặt hàng
        /// </summary>
        public void ShowFSAOrder()
        {
            var subSystemCode = "FSAOrder";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<SAOrder>(resSystem.Title_20, subSystemCode);
        }

        /// <summary>
        /// Mở Form Bán hàng chưa thu tiền
        /// </summary>
        public void ShowFSAInvoicePayment0()
        {
            var subSystemCode = "FSAInvoicePayment0";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            List<int> lstTypeId = new List<int> { 320 };
            ShowFormAndGetData<SAInvoice>(resSystem.Title_21, subSystemCode, 0, lstTypeId);
        }

        /// <summary>
        /// Mở Form Bán hàng thu tiền ngay
        /// </summary>
        public void ShowFSAInvoicePayment1()
        {
            var subSystemCode = "FSAInvoicePayment1";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            List<int> lstTypeId = new List<int> { 321, 322 };
            ShowFormAndGetData<SAInvoice>(resSystem.Title_26, subSystemCode, 1, lstTypeId);
        }

        /// <summary>
        /// Mở Form Bán hàng đại lý bán đúng giá, nhận ủy thác xuất nhập khẩu
        /// </summary>
        public void ShowFSAInvoicePayment2()
        {
            var subSystemCode = "FSAInvoicePayment2";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            List<int> lstTypeId = new List<int> { 323, 324, 325 };
            ShowFormAndGetData<SAInvoice>(resSystem.Title_45, subSystemCode, 2, lstTypeId);
        }
        /// <summary>
        /// Mở Form Xuất hóa đơn
        /// </summary>
        public void ShowFSABill()
        {
            var subSystemCode = "FSABill";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            List<int> lstTypeId = new List<int> { 326 };
            ShowFormAndGetData<SABill>(resSystem.Title_104, subSystemCode, -1, lstTypeId, 326);
        }
        /// <summary>
        /// Mở Form Hàng bán trả lại/giảm giá
        /// </summary>
        public void ShowFSAReturn()
        {
            var subSystemCode = "FSAReturn";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            List<int> lstTypeId = new List<int> { 330, 340 };
            ShowFormAndGetData<SAReturn>(resSystem.Title_27, subSystemCode, -1, lstTypeId, 330);
        }

        /// <summary>
        /// Mở Form Tính lãi nợ
        /// </summary>
        public void ShowFSAOverdueInterest()
        {
            var subSystemCode = "FSAOverdueInterest";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm(resSystem.Title_22, new FSAOverdueInterest());
        }

        /// <summary>
        /// Mở form Đối trừ chứng từ bán hàng
        /// </summary>
        public void ShowFSAExceptVoucher()
        {
            var subSystemCode = "FSAExceptVoucher";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            //HUYPD Edit Show Form
            //new FSAExceptVoucher().ShowFormDialog(this);
            new FSAExceptVoucher().ShowDialogHD(this);
        }

        /// <summary>
        /// Mở form Thông báo công nợ
        /// </summary>
        public void ShowFSALiabilitiesReport()
        {
            var subSystemCode = "FSALiabilitiesReport";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FSALiabilitiesReport().ShowFormDialog(this);
        }

        /// <summary>
        /// Mở Form thu tiền khách hàng 
        /// </summary>
        public void ShowFSAReceiptCustomer()
        {
            var subSystemCode = "FSAReceiptCustomer";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm(resSystem.Title_24, new FSAReceiptCustomer());
        }

        /// <summary>
        /// Mở form Tính giá bán
        /// </summary>
        public void ShowFSACalculateTheCost()
        {
            var subSystemCode = "FSACalculateTheCost";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FSACalculateTheCost().ShowFormDialog(this);
        }

        /// <summary>
        /// Mở form thiết lập chính sách giá bán
        /// </summary>
        public void ShowFSASetPricePolicy()
        {
            var subSystemCode = "FSASetPricePolicy";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm("Thiết lập chính sách giá bán", new FSASetPricePolicy());
        }

        /// <summary>
        /// Mở form báo cáo sổ chi tiết bán hàng
        /// </summary>
        public void ShowFRS17DNN()
        {
            var subSystemCode = "Rpt_FRS17DNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS17DNN(subSystemCode).ShowDialog(this);
        }

        /// <summary>
        /// Mở form Tổng hợp công nợ phải thu
        /// </summary>
        public void ShowFRReceiptableSummary()
        {
            var subSystemCode = "Rpt_FRReceiptableSummary";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRReceiptableSummary(subSystemCode).ShowDialog(this);
        }

        /// <summary>
        /// Mở form báo cáo FRS03a4DN
        /// </summary>
        public void ShowFRS03a4DN()
        {
            var subSystemCode = "Rpt_FRS03a4DN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS03a4DNN(subSystemCode).ShowDialog(this);
        }
        #endregion

        #region Ngân hàng
        /// <summary>
        /// Mở Form Séc/Ủy nhiệm chi 
        /// </summary>
        public void ShowFMBTellerPaper()
        {
            var subSystemCode = "FMBTellerPaper";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<MBTellerPaper>(resSystem.Title_04, subSystemCode);
        }

        /// <summary>
        /// Mở Form Nộp tiền vào tài khoản
        /// </summary>
        public void ShowFMBDeposit()
        {
            var subSystemCode = "FMBDeposit";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<MBDeposit>(resSystem.Title_03, subSystemCode);
        }

        /// <summary>
        /// Mở Form Thẻ tín dụng
        /// </summary>
        public void ShowFMBCreditCard()
        {
            var subSystemCode = "FMBCreditCard";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<MBCreditCard>(resSystem.Title_05, subSystemCode);
        }

        /// <summary>
        /// Mở Form Chuyển tiền nội bộ
        /// </summary>
        public void ShowFMBInternalTransfer()
        {
            var subSystemCode = "FMBInternalTransfer";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<MBInternalTransfer>(resSystem.Title_06, subSystemCode);
        }

        /// <summary>
        /// Mở form FMCompare
        /// </summary>
        public void ShowFMCompare()
        {
            var subSystemCode = "FMCompare";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FMCompare().ShowFormDialog(this);
        }
        /// <summary>
        /// Mở form báo cáo FRS08DNN
        /// </summary>
        public void ShowFRS06DNN()
        {
            var subSystemCode = "Rpt_FRS06DNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS06DNN(subSystemCode).ShowDialog(this);
        }

        /// <summary>
        /// Mở form FRBankCompare
        /// </summary>
        public void ShowFRBankCompare()
        {
            var subSystemCode = "Rpt_FRBankCompare";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRBankCompare(subSystemCode).ShowDialog(this);
        }



        #endregion

        #region Kho
        /// <summary>
        /// Mở Form Nhập kho
        /// </summary>
        public void ShowFRSInwardOutward()
        {
            var subSystemCode = "FRSInwardOutward";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            List<int> lstTypeId = new List<int> { 400, 401, 402, 403, 404 };
            ShowFormAndGetData<RSInwardOutward>(resSystem.Title_31, subSystemCode, -9998, lstTypeId);
        }

        /// <summary>
        /// Mở Form Xuất kho
        /// </summary>
        public void ShowFRSInwardOutwardOutput()
        {
            var subSystemCode = "FRSInwardOutwardOutput";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            List<int> lstTypeId = new List<int> { 410, 411, 412, 413, 414, 415 };
            ShowFormAndGetData<RSInwardOutward>(resSystem.Title_33, subSystemCode, -9999, lstTypeId);
        }

        /// <summary>
        /// Mở Form Chuyển kho
        /// </summary>
        public void ShowFRSTransfer()
        {
            var subSystemCode = "FRSTransfer";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<RSTransfer>(resSystem.Title_34, subSystemCode);
        }

        /// <summary>
        /// Mở form Điều chỉnh tồn kho
        /// </summary>
        public void ShowFRSInventoryAdjustments()
        {
            var subSystemCode = "FRSInventoryAdjustments";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            //HUYPD Edit Show Form
            //new FRSInventoryAdjustments().ShowFormDialog(this);
            new FRSInventoryAdjustments().Show(this);
        }
        /// <summary>
        /// Mở form sắp xếp chứng từ xuất nhập kho
        /// </summary>
        public void ShowFRSInwardOutwardReorganize()
        {
            var subSystemCode = "FRSInwardOutwardReorganize";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            //ShowForm("Sắp xếp chứng từ xuất nhập kho", new FRSInwardOutwardReorganize());
            new FRSInwardOutwardReorganize().Show(this);
        }

        /// <summary>
        /// Mở form tính giá xuất kho
        /// </summary>
        public void ShowFRSPricingOW()
        {
            var subSystemCode = "FRSPricingOW";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            string VTHH_PPTinhGiaXKho = _ISystemOptionService.GetByCode("VTHH_PPTinhGiaXKho").Data;
            if (VTHH_PPTinhGiaXKho == "Đích danh")
                MSG.Error("Phương pháp tính giá xuất kho " + VTHH_PPTinhGiaXKho + " đã được thực hiện ngay khi lập chứng từ.\n Chức năng tính giá xuất kho tại đây chỉ áp dụng cho phương pháp Bình quân cuối kỳ, Bình quân tức thời, Nhập trước xuất trước");
            else
                //HUYPD Edit Show Form
                //new FRSPricingOW().ShowFormDialog(this);
                new FRSPricingOW().Show(this);
        }

        /// <summary>
        /// Mở Form Lắp ráp, tháo dỡ
        /// </summary>
        public void ShowFRSAssemblyDismantlement()
        {
            var subSystemCode = "FRSAssemblyDismantlement";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<RSAssemblyDismantlement>(resSystem.Title_32, subSystemCode);
        }

        /// <summary>
        /// Mở form cập nhật giá nhập kho thành phẩm
        /// </summary>
        public void ShowFRSUpdatingIW()
        {
            var subSystemCode = "FRSUpdatingIW";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            //HUYPD Edit Show Form
            //new FRSUpdatingIW().ShowFormDialog(this);
            new FRSUpdatingIW().ShowDialogHD(this);
        }

        /// <summary>
        /// Mở form báo cáo 
        /// </summary>
        public void ShowFRS07DNN()
        {
            var subSystemCode = "Rpt_FRS07DNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS07DNN(subSystemCode).ShowDialog(this);
        }
        /// <summary>
        /// Mở form báo cáo S07DNN
        /// </summary>
        public void ShowFRS07DNN16()
        {
            var subSystemCode = "Rpt_FRS07DNN16";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS07DNN16(subSystemCode).ShowDialog(this);
        }
        /// <summary>
        /// Mở form thẻ kho
        /// </summary>
        public void ShowFRS09DNN()
        {
            var subSystemCode = "Rpt_FRS09DNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS09DNN(subSystemCode).ShowDialog(this);
        }
        #endregion

        #region Công cụ dụng cụ
        /// <summary>
        /// Mở Form Ghi tăng công cụ dụng cụ
        /// </summary>
        public void ShowFTIBuyIncrement()
        {
            var subSystemCode = "FTIIncrement";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            List<int> lstTypeId = new List<int> { 430, 902, 903, 904, 905, 906 };
            ShowFormAndGetData<TIIncrement>("Mua và ghi tăng CCDC", subSystemCode, -430, lstTypeId);
            //ShowFormAndGetData<TIIncrement>(resSystem.Title_87, subSystemCode);
        }

        /// <summary>
        /// Mở Form gahi tang CCDC khác
        /// </summary>
        public void ShowFTIIncrement()
        {
            var subSystemCode = "FFAIncrementAnother";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            List<int> lstTypeId = new List<int> { 907 };
            ShowFormAndGetData<TIIncrement>(resSystem.Title_87, subSystemCode, -907, lstTypeId);
        }

        /// <summary>
        /// Mở Form Ghi giảm Công cụ dụng cụ
        /// </summary>
        public void ShowFTIDecrement()
        {
            var subSystemCode = "FTIDecrement";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            List<int> lstTypeId = new List<int> { 431 };
            ShowFormAndGetData<TIDecrement>(resSystem.Title_88, subSystemCode);
        }

        /// <summary>
        /// Mở form Phân bổ công cụ dụng cụ
        /// </summary>
        public void ShowFTIAllocation()
        {
            var subSystemCode = "FTIAllocation";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<TIAllocation>(resSystem.Title_89, subSystemCode);
        }

        /// <summary>
        /// Mở form kiểm kê công cụ dụng cụ
        /// </summary>
        public void ShowFTIAudit()
        {
            var subSystemCode = "FTIAudit";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<TIAudit>("Kiểm kê CCDC", subSystemCode);
        }
        /// <summary>
        /// Mở form điều chuyển công cụ dụng cụ
        /// </summary>
        public void ShowFTITransfer()
        {
            var subSystemCode = "FTITransfer";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<TITransfer>(resSystem.Title_90, subSystemCode);
        }
        /// <summary>
        /// Mở form báo mất, báo hỏng công cụ dụng cụ
        /// </summary>
        public void ShowFTILost()
        {
            var subSystemCode = "FTILost";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<TILost>(resSystem.Title_91, subSystemCode);
        }
        /// <summary>
        /// Khai báo công cụ dụng cụ đầu kì
        /// </summary>
        public void ShowFTIInit()
        {
            var subSystemCode = "FTIInit";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm(resSystem.Title_93, new FTIInit());
        }

        /// <summary>
        /// Khai báo công cụ dụng cụ đầu kì
        /// </summary>
        public void ShowFMaterialTool()
        {
            var subSystemCode = "FTIInit";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm("Công cụ dụng cụ", new FMaterialTools());
        }
        /// <summary>
        /// Mở form Điều chỉnh công cụ dụng cụ
        /// </summary>
        public void ShowFTIAdjustment()
        {
            var subSystemCode = "FTIAdjustment";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<TIAdjustment>(resSystem.Title_92, subSystemCode);
        }

        /// <summary>
        /// mở form báo cáo FRS11DNN
        /// </summary>
        public void ShowFRS11DNN_CCDC()
        {
            var subSystemCode = "Rpt_FRS11DNN_CCDC";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS11DNN_CCDC(subSystemCode).ShowDialog(this);
        }
        #endregion

        #region Tài sản cố định
        /// <summary>
        /// Mở form khai báo TSCĐ
        /// </summary>
        public void ShowFFAInit()
        {
            var subSystemCode = "FFAInit";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm(resSystem.Title_35, new FFAInit());
        }

        /// <summary>
        /// Mở form khai báo TSCĐ
        /// </summary>
        public void ShowFFixedAsset()
        {
            var subSystemCode = "FFAInit";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm("Danh mục tài sản cố định", new FFixedAsset());
        }

        /// <summary>
        /// Mở Form Mua TSCĐ và ghi tăng
        /// </summary>
        public void ShowFFABuyAndIncrement()
        {
            var subSystemCode = "FFABuyAndIncrement";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            List<int> lstTypeId = new List<int> { 500, 119, 129, 132, 142, 172 };
            ShowFormAndGetData<FAIncrement>(resSystem.Title_37, subSystemCode, -500, lstTypeId);
        }

        /// <summary>
        /// Mở Form Ghi tăng tài sản cố định khác
        /// </summary>
        public void ShowFFAIncrement()
        {
            var subSystemCode = "FFAIncrement";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            List<int> lstTypeId = new List<int> { 510 };
            ShowFormAndGetData<FAIncrement>(resSystem.Title_36, subSystemCode, -510, lstTypeId);
        }

        /// <summary>
        /// Mở Form Ghi giảm tài sản cố định
        /// </summary>
        public void ShowFFADecrement()
        {
            var subSystemCode = "FFADecrement";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<FADecrement>(resSystem.Title_40, subSystemCode);
        }

        /// <summary>
        /// Mở Form Điều chỉnh TSCĐ
        /// </summary>
        public void ShowFFAAdjustment()
        {
            var subSystemCode = "FFAAdjustment";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<FAAdjustment>(resSystem.Title_38, subSystemCode);
        }

        /// <summary>
        /// Mở form tính khấu hao tài sản cố định
        /// </summary>
        public void ShowFFADepreciation()
        {
            var subSystemCode = "FFADepreciation";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<FADepreciation>(resSystem.Title_39, subSystemCode);
        }

        /// <summary>
        /// Mở form tính khấu hao tài sản cố định
        /// </summary>
        public void ShowFAAudit()
        {
            var subSystemCode = "FAAudit";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<FAAudit>(resSystem.Title_94, subSystemCode);
        }

        /// <summary>
        /// Mở form điều chuyển tài sản cố định
        /// </summary>
        public void ShowFATransfer()
        {
            var subSystemCode = "FATransfer";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<FATransfer>(resSystem.Title_95, subSystemCode);
        }

        /// <summary>
        /// Mở form thẻ tài sản cố định
        /// </summary>
        public void ShowFRS12SDNN()
        {
            var subSystemCode = "Rpt_FRS12SDNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS12SDNN(subSystemCode).ShowDialog(this);
        }

        /// <summary>
        /// Mở form báo cáo FRS10DNN
        /// </summary>
        public void ShowFRS10DNN()
        {
            var subSystemCode = "Rpt_FRS10DNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS10DNN(subSystemCode).ShowDialog(this);
        }

        /// <summary>
        /// mở form báo cáo FRS11DNN
        /// </summary>
        public void ShowFRS11DNN()
        {
            var subSystemCode = "Rpt_FRS11DNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS11DNN(subSystemCode).ShowDialog(this);
        }
        #endregion

        #region Hợp đồng
        /// <summary>
        /// Mở form hợp đồng mua
        /// </summary>
        public void ShowFEMContractBuy()
        {
            var subSystemCode = "FEMContractBuy";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm("", new FEMContractBuy());
        }

        /// <summary>
        /// Mở form Hợp đồng bán
        /// </summary>
        public void ShowFEMContractSale()
        {
            var subSystemCode = "FEMContractSale";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm("", new FEMContractSale());
        }

        /// <summary>
        /// Mở form ContractStatusPurchase
        /// </summary>
        public void ShowFRContractStatusPurchase()
        {
            var subSystemCode = "FRContractStatusPurchase";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRContractStatusPurchase(subSystemCode).ShowDialog(this);
        }

        /// <summary>
        /// Mở form ContractStatusSale
        /// </summary>
        public void ShowFRContractStatusSale()
        {
            var subSystemCode = "FRContractStatusSale";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRContractStatusSale(subSystemCode).ShowDialog(this);
        }
        #endregion

        #region Cổ đông
        /// <summary>
        /// Mở form Đăng ký cố phần
        /// </summary>
        public void ShowFEMRegistration()
        {
            var subSystemCode = "FEMRegistration";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm("", new FEMRegistration());
        }

        /// <summary>
        /// Mở form Số cổ đông
        /// </summary>
        public void ShowFEMShareHolder()
        {
            var subSystemCode = "FEMShareHolder";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm("", new FEMShareHolder());
        }

        /// <summary>
        /// Mở form chuyển nhượng
        /// </summary>
        public void ShowFEMTransfer()
        {
            var subSystemCode = "FEMTransfer";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm("", new FEMTransfer());
        }

        /// <summary>
        /// Mở form Cổ tức phải trả
        /// </summary>
        public void ShowFRDividendPayable()
        {
            var subSystemCode = "Rpt_FRDividendPayable";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRDividendPayable(subSystemCode).ShowDialog(this);
        }
        #endregion

        #region ngân sách
        /// <summary>
        /// Mở form Dự toán chi
        /// </summary>
        public void ShowFEMEstimate()
        {
            var subSystemCode = "FEMEstimate";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm("", new FEMEstimate());
        }

        /// <summary>
        /// Mở form cấp phát ngân sách
        /// </summary>
        public void ShowFEMAllocation()
        {
            var subSystemCode = "FEMAllocation";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm("", new FEMAllocation());
        }

        /// <summary>
        /// Mở form tình hình sử dụng ngân sách
        /// </summary>
        public void ShowFRBUAllocationAndUse()
        {
            var subSystemCode = "FRBUAllocationAndUse";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRBUAllocationAndUse(subSystemCode).ShowDialog(this);
        }
        #endregion

        #region Tổng hợp
        /// <summary>
        /// Mở form Chứng từ ghi sổ
        /// </summary>
        public void ShowFGVoucherList()
        {
            var subSystemCode = "FGVoucherList";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm(resSystem.Title_43, new FGVoucherList());
        }

        /// <summary>
        /// mở form Tính tỉ giá xuất quỹ
        /// </summary>
        public void ShowFGCashBankRate()
        {
            var subSystemCode = "FGCashBankRate";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            //HUYPD Edit Show Form
            //new FGCashBankRate().ShowFormDialog(this);
            new FGCashBankRate().ShowDialogHD(this);
        }
        /// <summary>
        /// mở form Đánh giá lại tài khoản ngoại tệ
        /// </summary>
        public void ShowFGCurrencyAssessment()
        {
            var subSystemCode = "FGCurrencyAssessment";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            //HUYPD Edit Show Form
            //new FGCurrencyAssessment().ShowFormDialog(this);
            new FGCurrencyAssessment().ShowDialogHD(this);
        }

        /// <summary>
        /// Mở form Xử lý chênh lệch tỉ giá
        /// </summary>
        public void ShowFGSolveExchangeRate()
        {
            var subSystemCode = "FGSolveExchangeRate";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FGSolveExchangeRate().ShowFormDialog(this);
        }

        /// <summary>
        /// Mở form Bù trừ công nợ
        /// </summary>
        public void ShowFOffsetLiabilities()
        {
            var subSystemCode = "FOffsetLiabilities";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FOffsetLiabilities().ShowFormDialog(this);
        }

        /// <summary>
        /// Mở form Chứng từ nghiệp vụ khác
        /// </summary>
        public void ShowFGOtherVoucher()
        {
            var subSystemCode = "FGOtherVoucher";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            List<int> lstTypeId = new List<int> { 600, 601, 602, 660, 670 };
            ShowFormAndGetData<GOtherVoucher>(resSystem.Title_41, subSystemCode, -600, lstTypeId);
        }
        public void ShowFPrepaidExpense()
        {
            var subSystemCode = "FPrepaidExpense";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm("Chi phí trả trước", new FPrepaidExpense());

        }
        public void ShowFGOtherVoucherExpense()
        {
            var subSystemCode = "FGOtherVoucherExpense";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            List<int> lstTypeId = new List<int> { 690 };
            ShowFormAndGetData<GOtherVoucher>("Phân bổ chi phí trả trước", subSystemCode, -690, lstTypeId);
        }
        /// <summary>
        /// Mở form Kết chuyển lãi/lỗ
        /// </summary>
        public void ShowFGOInterestAndLossTransfer()
        {
            var subSystemCode = "FGOInterestAndLossTransfer";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            List<int> lstTypeId = new List<int> { 620 };
            ShowFormAndGetData<GOtherVoucher>(resSystem.Title_42, "FGOInterestAndLossTransfer", -620, lstTypeId);
        }

        /// <summary>
        /// Mở form khóa sổ kỳ kế toán
        /// </summary>
        public void ShowFDBDateClosed()
        {
            var subSystemCode = "FDBDateClosed";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            //if (new FGDBDateClosed().ShowDialog(this) != DialogResult.OK) return;
            ////if (string.IsNullOrEmpty(keyUxItem) || !activeUxGroup.HasValue) return;
            ////var grp = uExBar.Groups[activeUxGroup.Value];
            ////if (!grp.Items.Exists(keyUxItem)) return;
            ////uExBarItemClick(uExBar, new ItemEventArgs(grp.Items[keyUxItem]));
            //ReloadBaseBusiness();
            FGDBDateClosed frm = new FGDBDateClosed();
            frm.FormClosed += new FormClosedEventHandler(FBaseBusiness_FormClosed);
            frm.ShowDialogHD(this);
        }
        private void FBaseBusiness_FormClosed(object sender, FormClosedEventArgs e)
        {
            ReloadBaseBusiness();
        }
        /// <summary>
        /// Mở form báo cáo tài chính
        /// </summary>
        public void ShowFBaoCao()
        {
            AddUserControl2(new UC_TreeBaoCao(), "Báo cáo");
        }
        private void ReloadBaseBusiness()
        {
            //MCPayment
            if (palMainMenu.ClientArea.Controls[0] is BaseBusiness<MCPayment>)
            {
                CallButtonReloadBaseBusiness((BaseBusiness<MCPayment>)palMainMenu.ClientArea.Controls[0]);
            }
            //MBDeposit
            else if (palMainMenu.ClientArea.Controls[0] is BaseBusiness<MBDeposit>)
            {
                CallButtonReloadBaseBusiness((BaseBusiness<MBDeposit>)palMainMenu.ClientArea.Controls[0]);
            }
            //MBInternalTransfer
            else if (palMainMenu.ClientArea.Controls[0] is BaseBusiness<MBInternalTransfer>)
            {
                CallButtonReloadBaseBusiness((BaseBusiness<MBInternalTransfer>)palMainMenu.ClientArea.Controls[0]);
            }
            //MBTellerPaper
            else if (palMainMenu.ClientArea.Controls[0] is BaseBusiness<MBTellerPaper>)
            {
                CallButtonReloadBaseBusiness((BaseBusiness<MBTellerPaper>)palMainMenu.ClientArea.Controls[0]);
            }
            //MBCreditCard
            else if (palMainMenu.ClientArea.Controls[0] is BaseBusiness<MBCreditCard>)
            {
                CallButtonReloadBaseBusiness((BaseBusiness<MBCreditCard>)palMainMenu.ClientArea.Controls[0]);
            }
            //MCReceipt
            else if (palMainMenu.ClientArea.Controls[0] is BaseBusiness<MCReceipt>)
            {
                CallButtonReloadBaseBusiness((BaseBusiness<MCReceipt>)palMainMenu.ClientArea.Controls[0]);
            }
            //FAAdjustment
            else if (palMainMenu.ClientArea.Controls[0] is BaseBusiness<FAAdjustment>)
            {
                CallButtonReloadBaseBusiness((BaseBusiness<FAAdjustment>)palMainMenu.ClientArea.Controls[0]);
            }
            //FADepreciation
            else if (palMainMenu.ClientArea.Controls[0] is BaseBusiness<FADepreciation>)
            {
                CallButtonReloadBaseBusiness((BaseBusiness<FADepreciation>)palMainMenu.ClientArea.Controls[0]);
            }
            //FAIncrement
            else if (palMainMenu.ClientArea.Controls[0] is BaseBusiness<FAIncrement>)
            {
                CallButtonReloadBaseBusiness((BaseBusiness<FAIncrement>)palMainMenu.ClientArea.Controls[0]);
            }
            //FADecrement
            else if (palMainMenu.ClientArea.Controls[0] is BaseBusiness<FADecrement>)
            {
                CallButtonReloadBaseBusiness((BaseBusiness<FADecrement>)palMainMenu.ClientArea.Controls[0]);
            }
            //GOtherVoucher
            else if (palMainMenu.ClientArea.Controls[0] is BaseBusiness<GOtherVoucher>)
            {
                CallButtonReloadBaseBusiness((BaseBusiness<GOtherVoucher>)palMainMenu.ClientArea.Controls[0]);
            }
            //RSInwardOutward
            else if (palMainMenu.ClientArea.Controls[0] is BaseBusiness<RSInwardOutward>)
            {
                CallButtonReloadBaseBusiness((BaseBusiness<RSInwardOutward>)palMainMenu.ClientArea.Controls[0]);
            }
            //RSTransfer
            else if (palMainMenu.ClientArea.Controls[0] is BaseBusiness<RSTransfer>)
            {
                CallButtonReloadBaseBusiness((BaseBusiness<RSTransfer>)palMainMenu.ClientArea.Controls[0]);
            }
            //PPInvoice
            else if (palMainMenu.ClientArea.Controls[0] is BaseBusiness<PPInvoice>)
            {
                CallButtonReloadBaseBusiness((BaseBusiness<PPInvoice>)palMainMenu.ClientArea.Controls[0]);
            }
            //PPDiscountReturn
            else if (palMainMenu.ClientArea.Controls[0] is BaseBusiness<PPDiscountReturn>)
            {
                CallButtonReloadBaseBusiness((BaseBusiness<PPDiscountReturn>)palMainMenu.ClientArea.Controls[0]);
            }
            //PPService
            else if (palMainMenu.ClientArea.Controls[0] is BaseBusiness<PPService>)
            {
                CallButtonReloadBaseBusiness((BaseBusiness<PPService>)palMainMenu.ClientArea.Controls[0]);
            }
            //SAInvoice
            else if (palMainMenu.ClientArea.Controls[0] is BaseBusiness<SAInvoice>)
            {
                CallButtonReloadBaseBusiness((BaseBusiness<SAInvoice>)palMainMenu.ClientArea.Controls[0]);
            }
            //SAReturn
            else if (palMainMenu.ClientArea.Controls[0] is BaseBusiness<SAReturn>)
            {
                CallButtonReloadBaseBusiness((BaseBusiness<SAReturn>)palMainMenu.ClientArea.Controls[0]);
            }
            //TIIncrement
            else if (palMainMenu.ClientArea.Controls[0] is BaseBusiness<TIIncrement>)
            {
                CallButtonReloadBaseBusiness((BaseBusiness<TIIncrement>)palMainMenu.ClientArea.Controls[0]);
            }
        }
        private void CallButtonReloadBaseBusiness<T>(BaseBusiness<T> f)
        {
            f.ResetFunctionBase();
        }
        /// <summary>
        /// Mở form bỏ khóa sổ kế toán
        /// </summary>
        public void ShowFDisposeDBDateClosed()
        {
            var subSystemCode = "FGDisposeDbDateClosed";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            //if (new FGDisposeDbDateClosed().ShowDialog(this) != DialogResult.OK) return;
            //ReloadBaseBusiness();
            FGDisposeDbDateClosed frm = new FGDisposeDbDateClosed();
            frm.FormClosed += new FormClosedEventHandler(FBaseBusiness_FormClosed);
            frm.ShowDialogHD(this);
        }

        /// <summary>
        /// Mở form báo cáo FRB01DNN
        /// </summary>
        public void ShowFRB01DNN()
        {
            var subSystemCode = "Rpt_FRB01DNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRB01DNN(subSystemCode).ShowDialog(this);
        }

        /// <summary>
        /// Mở form báo cáo FRB02DNN
        /// </summary>
        public void ShowFRB02DNN()
        {
            var subSystemCode = "Rpt_FRB02DNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRB02DNN(subSystemCode).ShowDialog(this);
        }

        /// <summary>
        /// mở form báo cáo FRB03DNN
        /// </summary>
        public void ShowFRB03DNN()
        {
            var subSystemCode = "Rpt_FRB03DNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRB03DNN(subSystemCode).ShowDialog(this);
        }

        /// <summary>
        /// mở form báo cáo FRF01DNN
        /// </summary>
        public void ShowFRF01DNN()
        {
            var subSystemCode = "FRBANGCANDOITAIKHOAN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new Accounting.Frm.FReport.FRBANGCANDOITAIKHOAN().ShowFormDialog(this);
        }

        /// <summary>
        /// mở form báo cáo FRS03aDNN
        /// </summary>
        public void ShowFRS03aDNN()
        {
            var subSystemCode = "Rpt_FRS03aDNN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRS03aDNN(subSystemCode).ShowDialog(this);
        }

        private void ShowFRBankBalance()
        {
            var subSystemCode = "Rpt_FRBankBalance";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRBankBalance(subSystemCode).ShowDialog(this);
        }

        private void ShowFRBankForeignCurrency()
        {
            var subSystemCode = "Rpt_FRBankForeignCurrency";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRBankForeignCurrency(subSystemCode).ShowDialog(this);
        }
        /// <summary>
        ///  Mở form FPrepaidExpense
        /// </summary>

        #endregion

        #region Tiền lương
        public void ShowFPSTimeSheet()
        {
            var subSystemCode = "FPSTimeSheet";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<PSTimeSheet>("Chấm công", subSystemCode);
        }
        public void ShowFPSTimeSheetSummary()
        {
            var subSystemCode = "FPSTimeSheetSummary";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<PSTimeSheetSummary>("Tổng hợp chấm công", subSystemCode);
        }
        public void ShowFPSalarySheet()
        {
            var subSystemCode = "FPSalarySheet";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<PSSalarySheet>("Bảng lương", subSystemCode);
        }

        public void ShowFInsurancePayment()
        {
            var subSystemCode = "FInsurancePayment";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            //HUYPD Edit Show Form
            //new FInsurancePaymentPopup().ShowDialog(this);
            new FInsurancePaymentPopup().ShowDialogHD(this);
        }

        public void ShowFWagePayment()
        {
            var subSystemCode = "FWagePayment";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            //HUYPD Edit Show Form
            //new FWagePaymentPopup().ShowDialog(this);
            new FWagePaymentPopup().ShowDialogHD(this);
        }



        public void ShowFPSOtherVoucher()
        {
            var subSystemCode = "FPSOtherVoucher";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<GOtherVoucher>("Hạch toán chi phí lương", subSystemCode, -840, new List<int>() { 840 });
        }

        #endregion

        #region Hóa đơn
        public void ShowFTT153Report()
        {
            var subSystemCode = "FTT153Report";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<TT153Report>("Khởi tạo mẫu hóa đơn", subSystemCode);

        }
        public void ShowFTT153RegisterInvoice()
        {
            var subSystemCode = "FTT153RegisterInvoice";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<TT153RegisterInvoice>("Đăng ký sử dụng hóa đơn", subSystemCode);
        }
        public void ShowFTT153PublishInvoice()
        {
            var subSystemCode = "FTT153PublishInvoice";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<TT153PublishInvoice>("Thông báo phát hành hóa đơn", subSystemCode);
        }
        public void ShowFTT153DestructionInvoice()
        {
            var subSystemCode = "FTT153DestructionInvoice";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<TT153DestructionInvoice>("Hủy hóa đơn", subSystemCode);
        }
        public void ShowFTT153LostInvoice()
        {
            var subSystemCode = "FTT153LostInvoices";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<TT153LostInvoice>("Mất, cháy, hỏng hóa đơn", subSystemCode);
        }
        public void ShowFTT153DeletedInvoice()
        {
            var subSystemCode = "FTT153DeletedInvoices";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<TT153DeletedInvoice>("Xóa hóa đơn", subSystemCode);
        }
        public void ShowFTT153AdjustAnnouncement()
        {
            var subSystemCode = "FTT153AdjustAnnouncement";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowFormAndGetData<TT153AdjustAnnouncement>("Điều chỉnh thông báo phát hành", subSystemCode);
        }
        public void ShowFRTinhHinhSuDungHD()
        {
            var subSystemCode = "FRTinhHinhSuDungHD";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRTinhHinhSuDungHD(subSystemCode).ShowDialog(this);
        }
        #endregion

        #region Giá thành
        #region Giá thành theo phương pháp hệ số
        /// <summary>
        /// Mở form FCPFactorMethod
        /// </summary>        
        public void ShowFCPFactorMethod()
        {
            var subSystemCode = "FCPFactorMethod";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm(resSystem.Title_102, new FCPFactorMethod());
        }
        #endregion Giá thành theo phương pháp hệ số

        #region Giá thành theo phương pháp giản đơn
        /// <summary>
        /// Mở form FCPSimpleMethod
        /// </summary>        
        public void ShowFCPSimpleMethod()
        {
            var subSystemCode = "FCPSimpleMethod";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm(resSystem.Title_101, new FCPSimpleMethod());
        }
        #endregion Giá thành theo phương pháp giản đơn

        #region Giá thành theo phương pháp tỷ lệ
        /// <summary>
        /// Mở form FCPSimpleMethod
        /// </summary>        
        public void ShowFCPCostOfRate()
        {
            var subSystemCode = "FCPCostOfRate";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm(resSystem.Title_103, new FCPCostOfRate());
        }
        #endregion Giá thành theo phương pháp tỷ lệ

        #region Giá thành theo công trình 
        /// <summary>
        ///  Mở form FCPCostOfConstruction
        /// </summary>
        public void ShowFCPCostOfConstruction()
        {
            var subSystemCode = "FCPCostOfConstruction";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm(resSystem.Title_97, new FCPCostOfConstruction());

        }
        #endregion Giá thành theo công trình 

        #region Báo cáo
        public void ShowFRCPPeriod()
        {
            var subSystemCode = "FRCPPeriod";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FRCPPeriod(subSystemCode).ShowDialog(this);
        }
        #endregion


        public void ShowFCPProductQuantum()
        {
            var subSystemCode = "FCPProductQuantum";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            //HUYPD Edit Show Form
            //   new FCPProductQuantum().ShowFormDialog(this);
            new FCPProductQuantum().ShowDialogHD(this);

        }
        public void ShowFCPAllocationQuantum()
        {
            var subSystemCode = "FCPAllocationQuantum";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            //HUYPD Edit Show Form
            //new FCPAllocationQuantum().ShowFormDialog(this);
            new FCPAllocationQuantum().ShowDialogHD(this);

        }
        public void ShowFCPOPN()
        {
            var subSystemCode = "FCPOPN";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            //HUYPD Edit Show Form
            //new FCPOPN().ShowFormDialog(this);
            new FCPOPN().ShowDialogHD(this);

        }


        #region Giá thành theo  đơn hàng
        /// <summary>
        /// Mở form FCPCostPerOrder
        /// </summary>
        public void ShowFCPCostPerOrder()
        {
            var subSystemCode = "FCPCostPerOrder";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm(resSystem.Title_98, new FCPCostPerOrder());

        }
        #endregion Giá thành theo  đơn hàng

        #region Giá thành theo hợp đồng
        /// <summary>
        /// Mở form FCPCostOfContract
        /// </summary>
        public void ShowFCPCostOfContract()
        {
            var subSystemCode = "FCPCostOfContract";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ShowForm(resSystem.Title_99, new FCPCostOfContract());

        }
        #endregion Giá thành theo hợp đồng
        #endregion Giá thành

        #region Thuế
        public void ShowFTax(int check, string title)
        {
            try
            {
                var subSystemCode = "FTax";
                if (!Authenticate.Permissions("SD", subSystemCode))
                {
                    MSG.Warning(resSystem.MSG_Permission);
                    return;
                }
                ShowForm(title, new FTax(check));
            }
            catch (Exception ex) { }
        }
        public void ShowTaxPeriod()
        {
            var subSystemCode = "TaxPeriod";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new TaxPeriod().ShowFormDialogHD(this);
        }

        public void ShowTaxSubmit()//add by cuongpv
        {
            var subSystemCode = "FTaxSubmit";
            if (!Authenticate.Permissions("SD", subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            new FTaxSubmit().ShowFormDialogHD(this);
        }
        #endregion
        #endregion
        #endregion

        #region UltraToolbarsManager Even
        private void uToolbarsManagerToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
        {
            ToolBarToolClick(e.Tool.Key);
        }
        public void ToolBarToolClick(string key)
        {
            switch (key)
            {
                case "ButtonTool1": new FTest2().Show(); break; //Danh mục Full
                //Tab He thong
                case "btnDate"://Ngày hạch toán
                    new FPostedDate().ShowFormDialog(this);
                    break;
                case "btnLog"://Nhật ký truy cập
                    ShowForm("Nhật ký truy cập", new FSysLog());
                    break;
                case "btnChangePass"://Đổi mật khẩu
                    new FChangePassword(Authenticate.User).ShowFormDialog(this);
                    break;
                case "btnManagerUsers"://Quản trị ND
                    ShowForm("Quản trị người dùng", new FCatSysUsers());
                    break;
                case "btnChangeProfile"://Thay đổi TT cá nhân
                    new FSysUserProfile(Authenticate.User).ShowFormDialog(this);
                    break;
                case "btnRole"://Vai trò, quyền hạn
                    ShowForm("Vai trò và quyền hạn", new FCatSysRoles());
                    break;
                case "btnAddFunction"://Tính năng bổ sung
                    break;
                case "btnSettings"://Tùy chọn
                    new FSystemOption().ShowFormDialog(this);
                    LoadVisibleEInvoice();
                    break;
                case "Change_sesssion"://Tùy chọn
                    FSession_Show();
                    break;
                case "btnChooseNoteBook"://Tùy chọn
                    FChooseNoteBook();
                    break;
                case "btnImportExcel":
                    FImportExcel();
                    break;
                case "btnImportChungtu":
                    FImportExcelV2();
                    break;
                //Tab Danh muc
                #region Hệ thống tài khoản
                case "CAT_Account"://Hệ thống tài khoản
                    ShowForm(resSystem.Title_50, new FAccount());
                    break;
                case "CAT_AccountTransfer"://Tài khoản kết chuyển
                    ShowForm(resSystem.Title_51, new FAccountTransfer());
                    break;
                case "CAT_AccountGroup"://Nhóm tài khoản
                    ShowForm(resSystem.Title_52, new FAccountGroup());
                    break;
                case "CAT_AutoPrinciple"://Định khoản tự động
                    ShowForm(resSystem.Title_53, new FAutoPrinciple());
                    break;
                #endregion
                #region Nhóm đối tượng
                case "CAT_AccountingObjectCustorm":
                    ShowForm(resSystem.Title_54, new FAccountingObjectCustomers());
                    break;
                case "CAT_Employee":
                    ShowForm(resSystem.Title_55, new FAccountingObjectEmployee());
                    break;
                case "CAT_AccountingObjectGroup":
                    ShowForm(resSystem.Title_56, new FAccountingObjectGroup());
                    break;
                case "CAT_Department":
                    ShowForm(resSystem.Title_57, new FDepartment());
                    break;
                #endregion
                //=========================//
                #region Nhóm VTHH, TSCĐ, Kho
                case "CAT_MaterialGoods":
                    ShowForm(resSystem.Title_58, new FMaterialGoods());
                    break;
                case "CAT_CCDC":
                    ShowForm(resSystem.Title_59, new FMaterialTools());
                    break;
                case "CAT_FixedAsset":
                    ShowForm(resSystem.Title_60, new FFixedAsset());
                    break;
                case "CAT_Repository":
                    ShowForm(resSystem.Title_61, new FRepository());
                    break;
                #endregion
                //=======================//
                #region Nhóm ngân hàng, cổ đông
                case "CAT_BankAccountDetail":
                    ShowForm(resSystem.Title_62, new FBankAccountDetail());
                    break;
                case "CAT_Bank":
                    ShowForm(resSystem.Title_63, new FBank());
                    break;
                case "CAT_CreditCard":
                    ShowForm(resSystem.Title_64, new FCreditCard());
                    break;
                case "CAT_StockCategory":
                    ShowForm(resSystem.Title_65, new FStockCategory());
                    break;
                #endregion
                //=======================//
                #region Lương nhân viên, Chi phí
                case "CAT_CostSet":
                    ShowForm(resSystem.Title_66, new FCostSet());
                    break;
                case "CAT_PersonalSalaryTax":
                    ShowForm(resSystem.Title_67, new FPersonalSalaryTax());
                    break;
                case "CAT_TimeSheetSymbols":
                    ShowForm(resSystem.Title_68, new FTimeSheetSymbols());
                    break;
                case "CAT_ExpenseItem":
                    ShowForm(resSystem.Title_69, new FExpenseItem());
                    break;
                #endregion
                //=======================//
                #region Khác
                case "CAT_AccountingObjectCategory":
                    ShowForm(resSystem.Title_85, new FAccountingObjectCategory());
                    break;
                case "CAT_Currency":
                    ShowForm(resSystem.Title_47, new FCurrency());
                    break;
                case "CAT_BudgetItem":
                    ShowForm(resSystem.Title_48, new FBudgetItem());
                    break;
                case "CAT_Type":
                    ShowForm(resSystem.Title_46, new FType());
                    break;
                case "CAT_PaymentClause":
                    ShowForm(resSystem.Title_49, new FPaymentClause());
                    break;
                case "CAT_AccountDefault":
                    ShowForm(resSystem.Title_70, new FAccountDefault());
                    break;
                case "CAT_MaterialGoodsCategory":
                    ShowForm(resSystem.Title_71, new FMaterialGoodsCategory());
                    break;
                case "CAT_FixedAssetCategory":
                    ShowForm(resSystem.Title_72, new FFixedAssetCategory());
                    break;
                case "CAT_MaterialQuantum":
                    ShowForm(resSystem.Title_73, new FMaterialQuantum());
                    break;
                case "CAT_InvestorGroup":
                    ShowForm(resSystem.Title_74, new FInvestorGroup());
                    break;
                case "CAT_Investor":
                    ShowForm(resSystem.Title_75, new FInvestor());
                    break;
                case "CAT_RegistrationGroup":
                    ShowForm(resSystem.Title_76, new FRegistrationGroup());
                    break;
                case "CAT_ShareHolderGroup":
                    ShowForm(resSystem.Title_77, new FShareHolderGroup());
                    break;
                case "CAT_StatisticsCode":
                    ShowForm(resSystem.Title_78, new FStatisticsCode());
                    break;
                case "CAT_TransportMethod":
                    ShowForm(resSystem.Title_79, new FTransportMethod());
                    break;
                case "CAT_SalePriceGroup":
                    ShowForm(resSystem.Title_80, new FSalePriceGroup());
                    break;
                case "CAT_GoodsServicePurchase":
                    ShowForm(resSystem.Title_81, new FGoodsServicePurchase());
                    break;
                case "CAT_MaterialGoodsSpecialTaxGroup":
                    ShowForm(resSystem.Title_82, new FMaterialGoodsSpecialTaxGroup());
                    break;
                case "CAT_MaterialGoodsResourceTaxGroup":
                    ShowForm(resSystem.Title_83, new FMaterialGoodsResourceTaxGroup());
                    break;
                case "CAT_ContractState":
                    ShowForm(resSystem.Title_84, new FContractState());
                    break;
                case "CAT_InvoiceType":
                    ShowForm(resSystem.Title_86, new FInvoiceType());
                    break;
                case "btnQuyDinhLuongThueBh":
                    ShowForm("Quy định lương, thuế, bảo hiểm", new FPSSalaryTaxInsuranceRegulation());
                    break;
                #endregion
                #region Tab Nghiep vu
                #region Tiền và tương đương tiền
                case "FMCPayment":
                    ShowFMCPayment();
                    break;
                case "FMCReceipt":
                    ShowFMCReceipt();
                    break;
                case "FMBDeposit":
                    ShowFMBDeposit();
                    break;
                case "FMBTellerPaper":
                    ShowFMBTellerPaper();
                    break;
                case "FMCAudit":
                    ShowFMCAudit();
                    break;
                #endregion
                //=========================//
                #region Mua hàng và công nợ phải trả
                case "FPPInvoice1":
                    ShowFPPInvoice1();
                    break;
                case "FPPService":
                    ShowFPPService();
                    break;
                case "FPPPayVendor":
                    ShowFPPPayVendor();
                    break;
                case "FPPDiscountReturn":
                    ShowFPPDiscountReturn();
                    break;
                #endregion
                //=========================//
                #region Bán hàng và công nợ phải thu
                case "FSAInvoicePayment0":
                    ShowFSAInvoicePayment0();
                    break;
                case "FSAInvoicePayment1":
                    ShowFSAInvoicePayment1();
                    break;
                case "FSAReceiptCustomer":
                    ShowFSAReceiptCustomer();
                    break;
                case "FSAReturn":
                    ShowFSAReturn();
                    break;
                case "FSABill":
                    ShowFSABill();
                    break;
                #endregion
                //=========================//
                #region Kho
                case "FRSInwardOutward"://Nhập kho
                    {
                        ShowFRSInwardOutward();
                    }
                    break;
                case "FRSInwardOutwardOutput"://Xuất kho
                    {
                        ShowFRSInwardOutwardOutput();
                    }
                    break;
                case "FRSTransfer"://Chuyển kho
                    ShowFRSTransfer();
                    break;
                case "FRSInventoryAdjustments"://Điều chỉnh tồn kho
                    ShowFRSInventoryAdjustments();
                    break;
                #endregion
                //=========================//
                #region Tài sản cố định
                case "FFixedAsset"://Khai báo TSCĐ
                    ShowFFixedAsset();
                    break;
                case "FFABuyAndIncrement"://Ghi tăng TSCĐ
                    ShowFFABuyAndIncrement();
                    break;
                case "FFADecrement"://Ghi giảm TSCĐ
                    ShowFFADecrement();
                    break;
                case "FFADepreciation"://Tính khấu hao TSCĐ
                    ShowFFADepreciation();
                    break;
                case "FFAAudit"://Tính khấu hao TSCĐ
                    ShowFAAudit();
                    break;
                case "FFATransfer"://Tính khấu hao TSCĐ
                    ShowFATransfer();
                    break;
                #endregion
                //=========================//
                #region Tiền lương
                case "FPSSalarySheet"://Bảng lương
                    ShowFPSalarySheet();
                    break;
                case "FPSTimeSheet"://Bảng chấm công
                    ShowFPSTimeSheet();
                    break;
                case "FPSTimeSheetSummary"://Trả lương
                    ShowFPSTimeSheetSummary();
                    break;
                case "FPSOtherVoucher"://Hạch toán chi phí lương
                    ShowFPSOtherVoucher();
                    break;
                case "FInsurancePayment"://Nộp tiền bh
                    ShowFInsurancePayment();
                    break;
                case "FWagePayment":// thanh toán lương
                    ShowFWagePayment();
                    break;

                #endregion
                //=========================//
                #region Tổng hợp
                case "FGOtherVoucher"://Chứng từ, nghiệp vụ khác
                    ShowFGOtherVoucher();
                    break;
                case "FGOInterestAndLossTransfer"://Kết chuyển lãi lỗ
                    ShowFGOInterestAndLossTransfer();
                    break;
                case "FDBDateClosed"://Khóa số kế toán
                    ShowFDBDateClosed();
                    break;
                case "FGDisposeDbDateClosed":
                    ShowFDisposeDBDateClosed();
                    break;
                case "FGFinancialStatements"://Báo cáo tài chính
                    //ShowForm(new F());
                    break;
                case "FPrepaidExpense"://Chi phí trả trước
                    ShowFPrepaidExpense();
                    break;
                case "FGOtherVoucherExpense"://Phân bổ chi phí trả trước
                    ShowFGOtherVoucherExpense();
                    break;
                #endregion
                //=======================//
                #region Hóa đơn
                case "FTT153Report":
                    ShowFTT153Report();
                    break;
                case "FTT153RegisterInvoice":
                    ShowFTT153RegisterInvoice();
                    break;
                case "FTT153PublishInvoice":
                    ShowFTT153PublishInvoice();
                    break;
                case "FTT153DestructionInvoice":
                    ShowFTT153DestructionInvoice();
                    break;
                case "FTT153LostInvoice":
                    ShowFTT153LostInvoice();
                    break;
                case "FTT153DeletedInvoice":
                    ShowFTT153DeletedInvoice();
                    break;
                case "FTT153AdjustAnnouncement":
                    ShowFTT153AdjustAnnouncement();
                    break;
                case "FRTinhHinhSuDungHD":
                    ShowFRTinhHinhSuDungHD();
                    break;
                #endregion

                #region Giá thành
                case "FCPFactorMethod":
                    ShowFCPFactorMethod();
                    break;
                case "FCPSimpleMethod":
                    ShowFCPSimpleMethod();
                    break;
                case "FCPCostOfConstruction":
                    ShowFCPCostOfConstruction();
                    break;
                case "FCPCostOfContract":
                    ShowFCPCostOfContract();
                    break;
                case "FCPCostPerOrder":
                    ShowFCPCostPerOrder();
                    break;
                case "FCPProductQuantum":
                    ShowFCPProductQuantum();
                    break;
                case "FCPAllocationQuantum":
                    ShowFCPAllocationQuantum();
                    break;
                case "FCPOPN":
                    ShowFCPOPN();
                    break;
                case "FMaterialQuantum":
                    ShowForm(resSystem.Title_73, new FMaterialQuantum());
                    break;
                case "FRCPPeriod":
                    ShowFRCPPeriod();
                    break;
                #endregion
                #endregion
                #region Tab Tien ich
                case "btnConfig"://Thiết lập thông tin ban đầu
                    break;
                case "btnSearch"://Tìm kiếm
                    break;
                case "btnQuickReport"://Báo cáo nhanh
                    break;
                case "btnMaintenanceData"://Bảo trì dữ liệu
                    break;
                case "btnConfigFinancialStatements"://Thiết lập BCTC
                    break;
                case "btnManagerDocuments"://Quản lý tài liệu
                    break;
                case "btnManagerWorking"://Quản lý công việc
                    break;
                case "btnOPAccount"://Nhập số dư đầu kỳ
                    ShowForm("", new FOPAccount()); break;
                case "btnGeneralLedger": new FTest3().ShowFormDialog(this); break; //Sổ cái - Sổ kho
                case "btnRefresh": Utils.ClearCache(); break;// Làm mới Cache
                case "btnAdminTools":
                    ShowForm(resSystem.Title_00, new FTest0());
                    break;
                case "btnChangeStyles":
                    new FChangeStyles().ShowFormDialog(this); break;//Đổi giao diện chương trình
                #endregion
                #region Báo cáo
                case "btnInBaoCao"://In báo cáo
                    AddUserControl2(new UC_TreeBaoCao(), "Báo cáo");
                    break;
                #endregion
                #region Tab Trợ giúp
                //case "btnImportData": new FImportDataCustomer().ShowFormDialog(this); break;
                //case "btnCheckForUpdate": ShowFCheckForUpdate(); break;//Cập nhật phần mềm
                //case "btnInfo": System.Diagnostics.Process.Start("https://xxx.vn/"); break;
                case "btnTeamviewer1": System.Diagnostics.Process.Start("https://www.teamviewer.com/en/teamviewer-automatic-download/"); break;
                case "btnUltraviewer1": System.Diagnostics.Process.Start("https://ultraviewer.net/vi/download.html"); break;
                case "btnHuongDan":
                    try
                    {
                        string path = System.IO.Path.GetDirectoryName(
               System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                        string filePath = string.Format("{0}\\HuongDanSuDungchm\\HDSD5.chm", path);
                        //ProcessStartInfo st = new ProcessStartInfo();
                        //st.FileName = "HDSD.chm";
                        //st.Arguments = filePath;
                        //Process.Start(st);
                        ResourceHelper.MakeFileOutOfAStream(filePath, "HDSD.chm");
                        //Help.ShowHelp(this, filePath, HelpNavigator.TableOfContents);
                    }
                    catch (Exception ex)
                    {
                        MSG.Information(ex.ToString());
                    }
                    break;

                case "btnAdminTools1":
                    try
                    {
                        string path = System.IO.Path.GetDirectoryName(
               System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                        string filePath = string.Format("{0}\\HuongDanSuDungchm\\HDSD5.chm", path);
                        //         ProcessStartInfo st = new ProcessStartInfo();
                        //         st.FileName = "HDSD.chm";
                        //         st.Arguments = filePath;
                        //         Process.st       //Start(st);
                        Help.ShowHelp(this, filePath, HelpNavigator.TableOfContents);
                    }
                    catch (Exception ex)
                    {
                        MSG.Information(ex.ToString());
                    }
                    break;
                #endregion
                default:
                    break;
            }
        }
        #endregion
        void AddUserControl2(Control usercontrol, string grouptext)
        {
            palMainMenu.Dock = DockStyle.Fill;
            palMainMenu.Refresh();
            //palMainMenu.AutoScroll = false;
            palMainMenu.ClientArea.Controls.Clear();
            usercontrol.Dock = DockStyle.Fill;
            ProcessControls(usercontrol);
            palMainMenu.ClientArea.Controls.Add(usercontrol);
            groupMainMenu.Text = grouptext;
            groupMainMenu.Appearance.BackColor = System.Drawing.ColorTranslator.FromHtml("#1b54e4");
            groupMainMenu.Appearance.BackColor2 = System.Drawing.ColorTranslator.FromHtml("#1b54e4");
            FMain_Resize(this, null);
            usercontrol.Show();
        }
        #region UltraExplorerBar Event
        private void uExBarItemClick(object sender, Infragistics.Win.UltraWinExplorerBar.ItemEventArgs e)
        {
            var uExBar = (UltraExplorerBar)sender;
            if (activeUxGroup.HasValue && (uExBar.Groups[activeUxGroup.Value].Visible && uExBar.Groups[activeUxGroup.Value].Enabled))
                uExBar.ActiveGroup = uExBar.Groups[activeUxGroup.Value];
            else uExBar.ActiveGroup = uExBar.Groups.All.Cast<UltraExplorerBarGroup>().FirstOrDefault();
            keyUxItem = e.Item.Key;
            ItemClick(keyUxItem);
        }
        public void ItemClick(string keyUxItem)
        {
            switch (keyUxItem)
            {
                #region Quỹ
                case "FMCReceipt":
                    ShowFMCReceipt();    //ShowForm(new BaseBusiness<MCReceipt>());
                    break;//Phiếu thu
                case "FMCPayment":
                    ShowFMCPayment();    //ShowForm(new BaseBusiness<MCPayment>());
                    break;//Phiếu chi
                case "Rpt_FRS05aDNN":
                    ShowFRS05aDNN();
                    break;

                case "Rpt_FRS03a1DNN":
                    ShowFRS03a1DNN();
                    break;
                case "Rpt_FRS03a2DNN":
                    ShowFRS03a2DNN();
                    break;
                #endregion
                #region Mua hàng
                case "FPPOrder":
                    ShowFPPOrder();    //ShowForm(new BaseBusiness<PPOrder>());
                    break;//Đơn mua hàng
                case "FPPInvoice1":
                    ShowFPPInvoice1();
                    break;//Hóa đơn mua hàng
                case "FPPInvoice2":
                    ShowFPPInvoice2();
                    break;//Mmua hàng ko qua kho
                case "FPPService":
                    ShowFPPService();    //ShowForm(new BaseBusiness<PPService>());
                    break;//nhập hóa đơn dịch vụ
                case "FPPDiscountReturn"://Hàng mua trả lại
                    ShowFPPDiscountReturn();
                    break;
                //case "FPPDiscountReturn1"://Hàng mua giảm giá
                //    {
                //        List<int> lstTypeId = new List<int> { 230 };
                //        ShowFormAndGetData<PPDiscountReturn>(resSystem.Title_14, -230, lstTypeId, 230);    //ShowForm(new BaseBusiness<SAReturn>()); 
                //    }
                //    break;
                case "FPPPayVendor":
                    ShowFPPPayVendor();
                    break;//Trả tiền nhà cung cấp
                case "FPPExceptVoucher":
                    ShowFPPExceptVoucher();
                    break; //Đối trừ chứng từ
                case "FPPGetInvoices":
                    ShowFPPGetInvoices();
                    break; //Nhận hóa đơn
                case "Rpt_FRS03a3DN":
                    ShowFRS03a3DN();
                    break;
                case "Rpt_FRPayableSummary":
                    ShowFRPayableSummary();
                    break;//Tham số báo cáo
                case "Rpt_FRPurchaseDetailSupplerInventoryItem":
                    ShowFRPurchaseDetailSupplerInventoryItem();
                    break;
                #endregion
                #region Bán hàng
                case "FSAQuote":
                    ShowFSAQuote();    //ShowForm(new BaseBusiness<SAQuote>());
                    break;//Báo giá
                case "FSAOrder":
                    ShowFSAOrder();    //ShowForm(new BaseBusiness<SAOrder>());
                    break;//Đơn đặt hàng
                case "FSAInvoicePayment0":
                    ShowFSAInvoicePayment0();
                    break;//Bán hàng chưa thu tiền
                case "FSAInvoicePayment1":
                    ShowFSAInvoicePayment1();
                    break;//Bán hàng thu tiền ngay
                case "FSAInvoicePayment2":
                    ShowFSAInvoicePayment2();
                    break;//Bán hàng đại lý bán đúng giá, nhận ủy thác xuất nhập khẩu
                case "FSABill":
                    ShowFSABill();
                    break;
                case "FSAReturn":
                    ShowFSAReturn();
                    break;//Hàng bán trả lại
                //case "FSAReturn1":
                //    {
                //        List<int> lstTypeId = new List<int> { 340 };
                //        ShowFormAndGetData<SAReturn>(resSystem.Title_28, 1, lstTypeId);    //ShowForm(new BaseBusiness<SAReturn>()); 
                //    }
                //    break;//Hàng bán giảm giá
                case "FSAOverdueInterest":
                    ShowFSAOverdueInterest();
                    break;//Tính lãi nợ
                case "FSAExceptVoucher":
                    ShowFSAExceptVoucher();
                    break;//Đối trừ chứng từ bán hàng
                case "FSALiabilitiesReport":
                    ShowFSALiabilitiesReport();
                    break;//Thông báo công nợ
                case "FSAReceiptCustomer":
                    ShowFSAReceiptCustomer();
                    break;//thu tiền khách hàng 
                case "FSACalculateTheCost":
                    ShowFSACalculateTheCost();
                    break;//Tính giá bán 
                case "FSASetPricePolicy":
                    ShowFSASetPricePolicy();
                    break;//Thiết lập chính sách giá bán
                case "Rpt_FRS17DNN":
                    ShowFRS17DNN();
                    break;
                //FRReceiptableSummary
                case "Rpt_FRReceiptableSummary":
                    ShowFRReceiptableSummary();
                    break;
                case "Rpt_FRS03a4DN":
                    ShowFRS03a4DN();
                    break;
                #endregion
                #region Ngân hàng
                case "FMBTellerPaper": ShowFMBTellerPaper(); break;                  //Séc/Ủy nhiệm chi
                case "FMBDeposit": ShowFMBDeposit(); break;                          //Nộp tiền vào tài khoản
                case "FMBCreditCard": ShowFMBCreditCard(); break;                    //ShowForm(new BaseBusiness<MBCreditCard>()); break;                    //Thẻ tín dụng
                case "FMBInternalTransfer": ShowFMBInternalTransfer(); break;        //ShowForm(new BaseBusiness<MBInternalTransfer>()); break;
                case "FMCompare": ShowFMCompare(); break;
                case "FMCAudit": ShowFMCAudit(); break;
                case "Rpt_FRS06DNN":
                    ShowFRS06DNN();
                    break;
                case "Rpt_FRBankCompare":
                    ShowFRBankCompare();
                    break;
                case "Rpt_FRBankBalance":
                    ShowFRBankBalance();
                    break;
                case "Rpt_FRBankForeignCurrency":
                    ShowFRBankForeignCurrency();
                    break;
                #endregion
                #region Kho
                case "FRSInwardOutward":
                    ShowFRSInwardOutward();
                    break;                    //Nhập kho            
                case "FRSInwardOutwardOutput":
                    ShowFRSInwardOutwardOutput();
                    break;                    //Xuất kho
                case "FRSTransfer": ShowFRSTransfer(); break;    //Chuyển kho
                case "FRSInventoryAdjustments": ShowFRSInventoryAdjustments(); break; //điều chỉnh tồn kho
                case "FRSPricingOW": ShowFRSPricingOW(); break;    //tính giá xuất kho
                case "FRSAssemblyDismantlement": ShowFRSAssemblyDismantlement(); break; //ShowForm(new FRSAssemblyDismantlement()); break; //Lắp ráp, tháo dỡ
                case "FRSUpdatingIW": ShowFRSUpdatingIW(); break; // cập nhật giá nhập kho thành phẩm
                case "FRSInwardOutwardReorganize": ShowFRSInwardOutwardReorganize(); break; // mở form cập nhật thứ tự xuất nhập kho
                case "Rpt_FRS07DNN":
                    ShowFRS07DNN();
                    break;
                case "Rpt_FRS07DNN16":
                    ShowFRS07DNN16();
                    break;
                case "Rpt_FRS09DNN":
                    ShowFRS09DNN();
                    break;
                #endregion
                #region Công cụ dụng cụ
                case "CAT_MaterialTools":
                    ShowForm(resSystem.Title_59, new FMaterialTools());
                    break;// Danh mục công cụ dụng cụ
                case "FTIInit":
                    ShowFTIInit();
                    break;//Khai báo CCDC đầu kỳ
                case "FTIIncrement":
                    ShowFTIBuyIncrement();
                    break;//Ghi tăng công cụ dụng cụ
                case "FTIIncrementAnother":
                    ShowFTIIncrement();
                    break;//ghi tăng công cụ dụng cụ khác
                case "FTIDecrement":
                    ShowFTIDecrement();
                    break;//Ghi giảm công cụ dụng cụ
                case "FTITransfer":
                    ShowFTITransfer();
                    break;//Điều chuyển công cụ dụng cụ
                case "FTIAdjustment":
                    ShowFTIAdjustment();
                    break;//Điều chỉnh công cụ dụng cụ
                case "FTIAllocation":
                    ShowFTIAllocation();
                    break;//Phân bổ công cụ dụng cụ
                case "FTIAudit":
                    ShowFTIAudit();
                    break;
                case "FTILedger":
                    break;
                case "Rpt_FRS11DNN_CCDC":
                    ShowFRS11DNN_CCDC();
                    break;

                #endregion
                #region Tài sản cố định
                case "FFAInit":
                    ShowFFAInit();
                    break;//Khai báo TSCĐ
                case "FFABuyAndIncrement":
                    ShowFFABuyAndIncrement();
                    break;//Mua TSCĐ và ghi tăng
                case "FFAIncrement":
                    ShowFFAIncrement();
                    break;//Ghi tăng tài sản cố định khác
                case "FFADecrement":
                    ShowFFADecrement();
                    break;//Ghi giảm tài sản cố định
                case "FFAAdjustment":
                    ShowFFAAdjustment();
                    break;//Điều chỉnh TSCĐ
                case "FFADepreciation":
                    ShowFFADepreciation();
                    break;//Tính KH TSCĐ
                case "Rpt_FRS12SDNN":
                    ShowFRS12SDNN();
                    break;
                case "Rpt_FRS10DNN":
                    ShowFRS10DNN();
                    break;
                case "Rpt_FRS11DNN":
                    ShowFRS11DNN();
                    break;
                case "FFAAudit":
                    ShowFAAudit();
                    break;
                case "FFATransfer":
                    ShowFATransfer();
                    break;
                #endregion
                #region Hợp đồng
                case "FEMContractBuy":
                    {
                        ShowFEMContractBuy();
                    }
                    break;
                case "FEMContractSale":
                    {
                        ShowFEMContractSale();
                    }
                    break;
                case "FRContractStatusPurchase":
                    ShowFRContractStatusPurchase();
                    break;
                case "FRContractStatusSale":
                    ShowFRContractStatusSale();
                    break;
                #endregion
                #region Cổ đông
                case "FEMRegistration"://Đăng ký mua cổ phần
                    ShowFEMRegistration();
                    break;
                case "FEMShareHolder"://sổ cổ đông
                    ShowFEMShareHolder();
                    break;
                case "FEMTransfer": //chuyển nhượng
                    ShowFEMTransfer();
                    break;
                case "Rpt_FRDividendPayable": //chuyển nhượng
                    ShowFRDividendPayable();
                    break;
                #endregion
                #region Ngân sách
                case "FEMEstimate"://Dự toán chi
                    ShowFEMEstimate();
                    break;
                case "FEMAllocation"://Phân bố công cụ dụng cụ
                    ShowFEMAllocation();
                    break;
                case "FRBUAllocationAndUse":
                    ShowFRBUAllocationAndUse();
                    break;
                #endregion
                #region Tổng hợp
                case "FGVoucherList":
                    ShowFGVoucherList();
                    break;//Chứng từ ghi sổ
                case "FGCashBankRate":
                    ShowFGCashBankRate();
                    break;//Tính tỷ giá xuất quỹ
                case "FGCurrencyAssessment":
                    ShowFGCurrencyAssessment();
                    break;//Tính tỷ giá xuất quỹ
                case "FGSolveExchangeRate":
                    ShowFGSolveExchangeRate();
                    break;//Xử lý chênh lệch tỷ giá
                case "FOffsetLiabilities":
                    ShowFOffsetLiabilities();
                    break;//Bù trừ công nợ
                case "FGOtherVoucher":
                    ShowFGOtherVoucher();
                    break;//Chứng từ nghiệp vụ khác
                case "FGOInterestAndLossTransfer":
                    ShowFGOInterestAndLossTransfer();
                    break;//Kết chuyển lãi, lỗ
                case "FDBDateClosed":
                    ShowFDBDateClosed();
                    break;
                case "FGDisposeDbDateClosed":
                    ShowFDisposeDBDateClosed();
                    break;
                case "Rpt_FRB01DNN":
                    ShowFRB01DNN();
                    break;
                case "Rpt_FRB02DNN":
                    ShowFRB02DNN();
                    break;
                case "Rpt_FRB03DNN":
                    ShowFRB03DNN();
                    break;
                case "Rpt_FRF01DNN":
                    ShowFRF01DNN();
                    break;
                case "Rpt_FRS03aDNN":
                    ShowFRS03aDNN();
                    break;
                case "FPrepaidExpense"://Chi phí trả trước
                    ShowFPrepaidExpense();
                    break;
                case "FGOtherVoucherExpense"://Phân bổ chi phí trả trước
                    ShowFGOtherVoucherExpense();
                    break;
                #endregion
                #region Tiền lương
                case "FPSTimeSheet":
                    ShowFPSTimeSheet();
                    break;
                case "FPSTimeSheetSummary":
                    ShowFPSTimeSheetSummary();
                    break;
                case "FPSalarySheet":
                    ShowFPSalarySheet();
                    break;
                case "FPSOtherVoucher"://Hạch toán chi phí lương
                    ShowFPSOtherVoucher();
                    break;
                case "FInsurancePayment":
                    ShowFInsurancePayment();
                    break;
                case "FWagePayment":// thanh toán lương
                    ShowFWagePayment();
                    break;
                #endregion
                #region Hóa đơn
                case "FTT153Report":
                    ShowFTT153Report();
                    break;
                case "FTT153RegisterInvoice":
                    ShowFTT153RegisterInvoice();
                    break;
                case "FTT153PublishInvoice":
                    ShowFTT153PublishInvoice();
                    break;
                case "FTT153DestructionInvoice":
                    ShowFTT153DestructionInvoice();
                    break;
                case "FTT153LostInvoice":
                    ShowFTT153LostInvoice();
                    break;
                case "FTT153DeletedInvoice":
                    ShowFTT153DeletedInvoice();
                    break;
                case "FTT153AdjustAnnouncement":
                    ShowFTT153AdjustAnnouncement();
                    break;
                case "FRTinhHinhSuDungHD":
                    ShowFRTinhHinhSuDungHD();
                    break;
                #endregion
                #region Giá thành
                case "FCPCostOfRate":
                    ShowFCPCostOfRate();
                    break;
                case "FCPFactorMethod":
                    ShowFCPFactorMethod();
                    break;
                case "FCPSimpleMethod":
                    ShowFCPSimpleMethod();
                    break;
                case "FCPCostOfConstruction":
                    ShowFCPCostOfConstruction();
                    break;
                case "FCPCostOfContract":
                    ShowFCPCostOfContract();
                    break;
                case "FCPCostPerOrder":
                    ShowFCPCostPerOrder();
                    break;
                case "FCPProductQuantum":
                    ShowFCPProductQuantum();
                    break;
                case "FCPAllocationQuantum":
                    ShowFCPAllocationQuantum();
                    break;
                case "FMaterialQuantum":
                    ShowForm(resSystem.Title_73, new FMaterialQuantum());
                    break;
                case "FCPOPN":
                    ShowFCPOPN();
                    break;
                case "FRCPPeriod":
                    ShowFRCPPeriod();
                    break;
                #endregion Giá thành
                #region Thuế
                case "FDeductionGTGT01":
                    ShowFTax(0, "Tờ khai thuế GTGT khấu trừ (Mẫu 01/GTGT)");
                    break;
                case "FTaxInvestmentProjects":
                    ShowFTax(1, "Tờ khai thuế GTGT cho dự án đầu tư (Mẫu 02/GTGT)");
                    break;
                case "FTaxFinalization":
                    ShowFTax(2, "Tờ khai quyết toán thuế thu nhập doanh nghiệp năm (Mẫu 03/TNDN)");
                    break;
                case "FDeductionTTDB01":
                    ShowFTax(3, "Tờ khai thuế tiêu thụ đặc biệt (Mẫu 01/TTDB)");
                    break;
                case "FDeductionResourcesTAIN01":
                    ShowFTax(4, "Tờ khai thuế tài nguyên (Mẫu 01/TAIN)");
                    break;
                case "FTaxFinalizationResourcesTAIN02":
                    ShowFTax(5, "Tờ khai quyết toán thuế tài nguyên (Mẫu 02/TAIN)");
                    break;
                case "FDirectGTGT04":
                    ShowFTax(6, "Tờ khai thuế GTGT trực tiếp (Mẫu 04/GTGT)");
                    break;
                case "TaxPeriod":
                    ShowTaxPeriod();
                    break;
                case "FTaxSubmit"://add by cuongpv
                    ShowTaxSubmit();
                    break;
                    #endregion
            }
        }
        System.ComponentModel.IContainer componentsBb = null;
        private void ShowForm(string title, Form frmChild)
        {
            //add by cuongpv
            if (!_isBasebusiness)
            {
                var baseBn = this.Controls.Find("BaseBSS", true).FirstOrDefault();
                if (baseBn != null)
                {
                    (baseBn as Form).Close();
                }
            }

            _isBasebusiness = false;
            //end add by cuongpv

            palMainMenu.Dock = DockStyle.Fill;
            palMainMenu.Refresh();
            //palMainMenu.AutoScroll = false;
            palMainMenu.ClientArea.Controls.Clear();
            frmChild.TopLevel = false;
            frmChild.Name = "BaseBSS";
            frmChild.FormBorderStyle = FormBorderStyle.None;
            frmChild.Dock = DockStyle.Fill;
            ProcessControls(frmChild);
            //frmChild.Size = palMainMenu.Size;
            try//them try catch de chay qua loi out of memory
            {
                palMainMenu.ClientArea.Controls.Add(frmChild);
            }
            catch (Exception ex)
            {
            }

            groupMainMenu.Text = title;
            FMain_Resize(this, null);
            //currentFormBaseBusiness = frmChild;
            frmChild.Show();
        }
        private void ProcessControls(Control ctrlContainer)
        {
            foreach (Control ctrl in ctrlContainer.Controls)
            {
                if (ctrl.GetType().Name.Contains(typeof(UltraButton).Name))
                {
                    ((UltraButton)ctrl).AcceptsFocus = false;
                    ((UltraButton)ctrl).Padding = new System.Drawing.Size(3, 3);
                    ((UltraButton)ctrl).Width += 6;
                }
                else if (ctrl.GetType().Name.Contains(typeof(UltraDropDownButton).Name))
                {
                    ((UltraDropDownButton)ctrl).AcceptsFocus = false;
                    ((UltraDropDownButton)ctrl).Padding = new System.Drawing.Size(3, 3);
                    ((UltraDropDownButton)ctrl).Width += 6;
                }

                if (ctrl.HasChildren)
                    ProcessControls(ctrl);
            }
        }
        void AddUserControl(Control usercontrol, string grouptext)
        {
            palMainMenu.Dock = DockStyle.Fill;
            palMainMenu.Refresh();
            uTabMain.Visible = false;
            palMainMenu.ClientArea.Controls.Clear();
            palMainMenu.ClientArea.Controls.Add(palUserControl);
            palUserControl.AutoSize = false;
            palUserControl.Dock = DockStyle.None;
            palUserControl.Size = usercontrol.Size;
            palUserControl.ClientArea.Controls.Clear();
            palUserControl.ClientArea.Controls.Add(usercontrol);
            usercontrol.Top = 0;
            usercontrol.Left = 0;
            FMain_Resize(this, null);
            palMainMenu.AutoScroll = true;
            palMainMenu.AutoScrollMinSize = palMainMenu.GetPreferredSize(new Size(1, 1));
            groupMainMenu.Text = grouptext;
        }

        void AddUserControl(List<Control> usercontrols, string[] tabcapture, string grouptext)
        {
            palMainMenu.Dock = DockStyle.Fill;
            palMainMenu.Refresh();
            uTabMain.Visible = true;
            var maxWidth = usercontrols.Max(u => u.Size.Width);
            var maxHeight = usercontrols.Max(u => u.Size.Height);
            uTabMain.MinimumSize = new Size(maxWidth, maxHeight);
            uTabMain.Size = new Size(palMainMenu.Width - 3, palMainMenu.Height - 3);
            uTabMain.TabPadding = new Size(4, 4);
            uTabMain.Tabs.Clear();
            for (int i = 0; i < usercontrols.Count; i++)
            {
                UltraTab tab = uTabMain.Tabs.Add();
                //var panel = new UltraPanel();
                //panel.ClientArea.Controls.Add(usercontrols[i]);
                tab.TabPage.Controls.Add(usercontrols[i]);
                //usercontrols[i].Anchor = AnchorStyles.None;
                var mWidth = uTabMain.Size.Width / 2 - usercontrols[i].Size.Width / 2;
                var mHeight = uTabMain.Size.Height / 2 - usercontrols[i].Size.Height / 2;
                usercontrols[i].Left = mWidth > 0 ? mWidth : 0;
                usercontrols[i].Top = mHeight > 0 ? mHeight : 0;
                tab.Text = tabcapture[i];
                if (usercontrols[i] is UCAccountBalance || usercontrols[i] is UCHealthFinanceEnterprise)
                {
                    usercontrols[i].Dock = DockStyle.Fill;
                    uTabMain.Dock = DockStyle.Fill;
                }

            }
            uTabMain.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Flat;
            uTabMain.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            uTabMain.DrawFilter = new DrawFilter();
            palMainMenu.ClientArea.Controls.Clear();
            palMainMenu.ClientArea.Controls.Add(palUserControl);
            palUserControl.Location = new Point(0, 3);
            palUserControl.Dock = DockStyle.Fill;
            //palUserControl.Size = uTabMain.Size;
            //palUserControl.AutoSize = true;
            palUserControl.ClientArea.Controls.Clear();
            palUserControl.ClientArea.Controls.Add(uTabMain);
            //this.Resize -= FMain_Resize;
            palUserControl.Size = uTabMain.Size;
            palMainMenu.AutoScroll = true;
            palMainMenu.AutoScrollMinSize = palMainMenu.GetPreferredSize(new Size(1, 1));
            groupMainMenu.Text = grouptext;
            var clor = System.Drawing.Color.FromArgb(27, 84, 228);
            groupMainMenu.ForeColor = clor;
            groupMainMenu.HeaderAppearance.BorderColor = Color.Red;
        }
        #endregion

        private void FSession_Show()
        {
            var fS = new FSession();
            fS.ShowFormDialog(this);
            if (fS.NextSession != null && fS.CurrentSession != fS.NextSession)
            {
                NextSession = fS.NextSession;
                this.Close();
            }
        }
        #region Load tabctr bàn làm việc add Hautv
        private void ultraTabControl1_SelectedTabChanged(object sender, SelectedTabChangedEventArgs e)
        {
            if (e.Tab.Text == "Số dư tài khoản")
            {
                try
                {
                    palMainMenu.Hide();
                    //UltraTab tab = uTabMain.Tabs[1];
                    WaitingFrm.StartWaiting();
                    e.Tab.TabPage.Controls.Clear();
                    var UC = new UCAccountBalance();
                    e.Tab.TabPage.Controls.Add(UC);
                    //tab.TabPage.Controls.Clear();
                    //tab.TabPage.Controls.Add(UC);
                    UC.Dock = DockStyle.Fill;
                    WaitingFrm.StopWaiting();
                    palMainMenu.Show();
                }
                catch (Exception)
                {
                    palMainMenu.Show();
                }
            }
            else if (e.Tab.Text == "Sức khỏe tài chính doanh nghiệp")
            {
                try
                {
                    palMainMenu.Hide();
                    WaitingFrm.StartWaiting();
                    e.Tab.TabPage.Controls.Clear();
                    var UC = new UCHealthFinanceEnterprise();
                    e.Tab.TabPage.Controls.Add(UC);
                    UC.Dock = DockStyle.Fill;
                    WaitingFrm.StopWaiting();
                    palMainMenu.Show();
                }
                catch (Exception)
                {
                    palMainMenu.Show();
                }
            }
        }
        #endregion
        private void FChooseNoteBook()
        {
            var fs = new FChooseNoteBook();
            fs.ShowFormDialog(this);
        }

        private void FImportExcel()
        {
            var fi = new FImportExcel();
            fi.ShowFormDialog(this);
            if (fi.closeForm)
            {
                if (palMainMenu.ClientArea.Controls[0] is FAccountingObjectEmployee && fi.cbbCheck == "AccountingObject1")
                {
                    ShowForm(resSystem.Title_55, new FAccountingObjectEmployee());
                }
                else if (palMainMenu.ClientArea.Controls[0] is FAccountingObjectCustomers && (fi.cbbCheck == "AccountingObject2" || fi.cbbCheck == "AccountingObject3"))
                {
                    ShowForm(resSystem.Title_54, new FAccountingObjectCustomers());
                }
                else if (palMainMenu.ClientArea.Controls[0] is FMaterialGoods && fi.cbbCheck == "MaterialGoods")
                {
                    ShowForm(resSystem.Title_58, new FMaterialGoods());
                }
            }
        }
        private void FImportExcelV2()
        {
            var fi = new FImportExcelV2();
            fi.ShowFormDialog(this);
        }

        private void FMain_Layout(object sender, LayoutEventArgs e)
        {
            this.BringToFront();
            this.Activate();
        }

        public bool ProcessCmdKey_Shared { get; set; }

        private void uToolbarsManager_BeforeApplicationMenuDropDown(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
        }

        private void FMain_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            { }
            else if (WindowState == FormWindowState.Maximized)
            { }
            else
            {
                if (uTabMain.Visible)
                {
                    List<Control> usercontrols = new List<Control>();
                    foreach (var tab in uTabMain.Tabs)
                    {
                        var uTab = tab as UltraTab;
                        if (tab.TabPage.Controls.Count == 1)
                            usercontrols.Add(tab.TabPage.Controls[0]);
                    }
                    int maxWidth = 0;// Convert.ToInt32(usercontrols.Select(u => u.Size.Width).Max(u => u));
                    int maxHeight = 0;//Convert.ToInt32(usercontrols.Select(u => u.Size.Height).Max(u => u));
                    foreach (var sizes in usercontrols.Select(u => u.Size))
                    {
                        if (sizes.Width > maxWidth) maxWidth = sizes.Width;
                        if (sizes.Height > maxHeight) maxHeight = sizes.Height;
                    }
                    uTabMain.MinimumSize = new Size(maxWidth, maxHeight);
                    uTabMain.Size = new Size(palMainMenu.Width - 3, palMainMenu.Height - 3);
                    uTabMain.TabPadding = new Size(4, 4);
                    foreach (var control in usercontrols)
                    {
                        var mWidth = uTabMain.Size.Width / 2 - control.Size.Width / 2;
                        var mHeight = uTabMain.Size.Height / 2 - control.Size.Height / 2;
                        control.Left = mWidth > 0 ? mWidth : 0;
                        control.Top = mHeight > 0 ? mHeight : 0;
                        control.Dock = DockStyle.Fill;
                    }
                    palUserControl.Size = uTabMain.Size;
                    return;
                }
            }
            //var width = palMainMenu.ClientArea.Size.Width / 2 - palUserControl.Size.Width / 2;
            //var height = palMainMenu.ClientArea.Size.Height / 2 - palUserControl.Size.Height / 2;
            //palUserControl.Location = new Point(width > 0 ? width : 0, height > 0 ? height : 0);
            //palUserControl.Anchor = AnchorStyles.None;
            //palUserControl.AutoSize = true;
            var newWidth = (palMainMenu.Width - palUserControl.Width) / 2;
            var newHeight = (palMainMenu.Height - palUserControl.Height) / 2;
            palUserControl.Left = newWidth >= 0 ? newWidth : 0;
            palUserControl.Top = newHeight >= 0 ? newHeight : 0;
            palMainMenu.AutoScroll = true;
            palMainMenu.AutoScrollMinSize = palMainMenu.GetPreferredSize(new Size(1, 1));
            //if (WindowState == FormWindowState.Minimized)
            //{
            //    List<Control> usercontrols = new List<Control>();
            //    foreach (var tab in uTabMain.Tabs)
            //    {
            //        var uTab = tab as UltraTab;
            //        if (tab.TabPage.Controls.Count == 1)
            //            usercontrols.Add(tab.TabPage.Controls[0]);
            //    }
            //    foreach (var control in usercontrols)
            //    {

            //        control.Dock = DockStyle.Fill;
            //        //control.Top = mHeight > 0 ? mHeight : 0;
            //    }
            //}
        }

        private void uSttBar_MouseHover(object sender, EventArgs e)
        {
            string stt = DateTime.Now.ToString("dddd, dd MMMM yyyy");
            uSttBar.Panels["Date"].ToolTipText = stt;
            uSttBar.Panels["Time"].ToolTipText = stt;
        }

        private void uExBar_SelectedGroupChanged(object sender, GroupEventArgs e)
        {
            //string strTemp = e.Group.Text;
            //UltraExplorerBar uxBar = (UltraExplorerBar)sender;
            ConfigSubGroupMain(e);
            isUxGroupClick = true;
        }

        private void palUserControl_PaintClient(object sender, PaintEventArgs e)
        {

        }
        private void uExBar_GroupClick(object sender, GroupEventArgs e)
        {
            if (!isUxGroupClick)
                ConfigSubGroupMain(e);
            isUxGroupClick = false;
        }
        public void ConfigSubGroupMain(GroupEventArgs e, bool isHide = true)
        {
            activeUxGroup = e.Group.Index;
            if (isHide)
                palMainMenu.Hide();
            if (e.Group.Key.StartsWith("GroupCustom_"))
            {
                AddUserControl(new UGroupCustom(this, e.Group.Key) { }, e.Group.Text);
            }
            else
                switch (e.Group.Key)
                {
                    //Bàn làm việc
                    case "Desktop":
                        AddUserControl(new List<Control> { new UCDesktop(this) { }, new UCAccountBalance(false) { }, new UCHealthFinanceEnterprise(false) { } }, new[] { "Quy trình nghiệp vụ", "Số dư tài khoản", "Sức khỏe tài chính doanh nghiệp" }, e.Group.Text);//cuongpv add "Sức khỏe tài chính doanh nghiệp" -new UCHealthFinanceEnterprise() { }
                        break;
                    //Module Mua hàng
                    case "SalePriceModule":
                        AddUserControl(new UCSale(this) { }, e.Group.Text);
                        break;
                    //Module Hợp đồng
                    case "EMContractModule":
                        AddUserControl(new UCEM_tab1_(this) { }, e.Group.Text);
                        break;
                    //Module Cổ đông
                    case "EMShareHolderModule":
                        AddUserControl(new UCEM_tab2_(this) { }, e.Group.Text);
                        break;
                    //Module ngân sách
                    case "EMBudgetModule":
                        AddUserControl(new UCEM_tab3_(this) { }, e.Group.Text);
                        break;
                    //Module Giá thành
                    case "CostPriceModule":
                        AddUserControl(new List<Control> { new UCCP_tab1_(this) { }, new UCCP_tab2_(this) { }, new UCCP_tab3_(this) { } }, new[] { "Công trình, vụ việc", "Đơn hàng", "Sản xuất liên tục" }, e.Group.Text);
                        break;
                    //Module tiền và ngân hàng
                    case "Money&BankModule":
                        AddUserControl(new List<Control> { new UCMoney(this) { }, new UCBank(this) { } }, new[] { "Tiền tệ", "Ngân hàng" }, e.Group.Text);
                        break;
                    //Module Bán hàng
                    case "PurchasePriceModule":
                        AddUserControl(new UCPurchasePrice(this) { }, e.Group.Text);
                        break;
                    //Module Kho
                    case "RepositoryModule":
                        AddUserControl(new UCRepository(this) { }, e.Group.Text);
                        break;
                    //Module Tài sản cố định
                    case "FixedAssetModule":
                        AddUserControl(new UCFixedAsset(this) { }, e.Group.Text);
                        break;
                    //Module CCDC
                    case "ToolModule":
                        AddUserControl(new UCTool(this) { }, e.Group.Text);
                        break;
                    //Module CCDC
                    case "SalaryModule":
                        AddUserControl(new UCSalary(this) { }, e.Group.Text);
                        break;
                    //Module giá thành
                    case "CostModule":
                        AddUserControl(new UCCost(this) { }, e.Group.Text);
                        break;
                    //Module Tổng hợp
                    case "GeneralModule":
                        AddUserControl(new UCGeneral(this) { }, e.Group.Text);
                        break;
                    //Module Invoice
                    case "InvoiceModule":
                        AddUserControl(new UCInvoice(this) { }, e.Group.Text);
                        break;
                    case "Tax"://Thuế
                        AddUserControl(new UCTax(this), e.Group.Text);
                        break;
                    case "btnInBaoCao"://In báo cáo
                        AddUserControl2(new UC_TreeBaoCao(), e.Group.Text);
                        break;
                    default:
                        AddUserControl(new List<Control> { new UCDesktop(this) { }, new UCAccountBalance(false) { }, new UCHealthFinanceEnterprise(false) { } }, new[] { "Quy trình nghiệp vụ", "Số dư tài khoản", "Sức khỏe tài chính doanh nghiệp" }, "Bàn làm việc");//cuongpv add "Sức khỏe tài chính doanh nghiệp" -new UCHealthFinanceEnterprise() { }
                        break;
                }
            if (isHide)
                palMainMenu.Show();
        }

        private void palUserControl_Resize(object sender, EventArgs e)
        {

        }

        private void FMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Xml.Serialization.XmlSerializer writer =
                        new System.Xml.Serialization.XmlSerializer(typeof(List<Accounting.Core.Domain.ControlsMapping.Config>));
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            System.IO.TextWriter file = new
               System.IO.StreamWriter(string.Format("{0}\\Config\\control.config", path));
            List<Accounting.Core.Domain.ControlsMapping.Config> overwrite = new List<Accounting.Core.Domain.ControlsMapping.Config>();
            try
            {
                var config = overwrite.FirstOrDefault(x => x.UserId == Authenticate.User.userid);
                if (config == null)
                {
                    config = new Core.Domain.ControlsMapping.Config { UserId = Authenticate.User.userid };
                    overwrite.Add(config);
                }
                foreach (var group in uExBar.Groups)
                {
                    var g = new Core.Domain.ControlsMapping.Group();
                    g.KeyCode = group.Key;
                    g.Position = group.Index;
                    g.IsVisible = group.Visible;
                    if (group.Tag != null && group.Tag.ToString() == "custom")
                    {
                        g.IsSecurity = false;
                        g.Caption = group.Text;
                        g.ToolTip = group.ToolTipText;
                    }
                    else g.IsSecurity = true;
                    config.Groups.Add(g);
                }
                if (ConfigControls != null)
                {
                    var first = ConfigControls.FirstOrDefault(x => x.UserId == Authenticate.User.userid);
                    if (first != null)
                        config.Items.AddRange(first.Items);
                }
                writer.Serialize(file, overwrite);
            }
            catch (Exception)
            {
                file.Close();
                return;
            }
            file.Close();
        }

        private void btnuExBarOption_Click(object sender, EventArgs e)
        {
            uExBar.DisplayNavigationPaneOptionsDialog();
        }

        private void cms4uExBar_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (uExBar.ActiveGroup == null || (uExBar.ActiveGroup != null && uExBar.ActiveGroup.Tag != "custom"))
            {
                btnDeleteGroup.Visible = false;
            }
            else
                btnDeleteGroup.Visible = true;
        }

        private void btnShowAddNewGroup_Paint(object sender, PaintEventArgs e)
        {
            if (!popAddNewGroup.IsDisplayed)
            {
                popAddNewGroup.Show(cms4uExBar, btnAddNewGroup.DropDown.Location);
                btnAddNewGroup.DropDown.Visible = false;
            }
        }

        private void popAddNewGroup_Closed(object sender, EventArgs e)
        {
            if (!_notevent)
            {
                if (!cms4uExBar.IsDisposed)
                {
                    cms4uExBar.AutoClose = true;
                    cms4uExBar.Close();
                }
            }
            else
            {
                cms4uExBar.AutoClose = true;
                cms4uExBar.Refresh();
            }
            _notevent = false;
        }

        private void btnApplyAddNewGroup_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtNewCaptionGroup.Text.Trim())) return;
            try
            {
                popAddNewGroup.Close();
                cms4uExBar.Hide();
                int max = maxGroupIndex;
                var group = new UltraExplorerBarGroup
                {
                    Key = string.Format("GroupCustom_{0}", max),
                    Text = txtNewCaptionGroup.Text.Trim(),
                    ToolTipText = txtToolTipNewGroup.Text.Trim(),
                    Visible = true,
                    Tag = "custom"
                };
                foreach (var gr in uExBar.Groups)
                {
                    var index = gr.Index;
                    uExBar.Groups.Remove(gr);
                    uExBar.Groups.Insert(index++, gr);
                }
                uExBar.Groups.Insert(0, group);
                //configControls.Groups.Clear();
                //foreach (var gr in uExBar.Groups)
                //{
                //    var g = new Core.Domain.ControlsMapping.Group();
                //    g.KeyCode = group.Key;
                //    g.Position = group.Index;
                //    g.IsVisible = group.Visible;
                //    if (group.Tag != null && group.Tag.ToString() == "custom")
                //    {
                //        g.IsSecurity = false;
                //        g.Caption = group.Text;
                //        g.ToolTip = group.ToolTipText;
                //    }
                //    else g.IsSecurity = true;
                //    configControls.Groups.Add(g);
                //}
                maxGroupIndex++;
                uExBar.ActiveGroup = uExBar.Groups[0];
                uExBar.Groups[0].Selected = true;
            }
            catch (Exception)
            {
            }
        }

        private void cms4uExBar_Closing(object sender, ToolStripDropDownClosingEventArgs e)
        {
            if (popAddNewGroup.IsDisplayed)
                e.Cancel = true;
        }

        private void btnDeleteGroup_MouseHover(object sender, EventArgs e)
        {
            _notevent = true;
            if (popAddNewGroup.IsDisplayed)
                popAddNewGroup.Close();
        }

        private void btnuExBarOption_MouseHover(object sender, EventArgs e)
        {
            _notevent = true;
            if (popAddNewGroup.IsDisplayed)
                popAddNewGroup.Close();
        }

        private void btnDeleteGroup_Click(object sender, EventArgs e)
        {
            if (MSG.MessageBoxStand(string.Format("Bạn thực sự muốn xóa mục [ {0} ] này?", uExBar.ActiveGroup.Text), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (uExBar.ActiveGroup == null) return;
                var group = uExBar.ActiveGroup;
                uExBar.Groups.Remove(uExBar.ActiveGroup);
                if (!group.Key.StartsWith("GroupCustom_")) return;
                int index = Convert.ToInt32(group.Key.Split(new[] { "GroupCustom_" }, StringSplitOptions.RemoveEmptyEntries)[0]);
                foreach (var gr in uExBar.Groups.All.Cast<UltraExplorerBarGroup>().Where(g => g.Key.StartsWith("GroupCustom_")).OrderBy(g => g.Key).ToList())
                {
                    var i = Convert.ToInt32(gr.Key.Split(new[] { "GroupCustom_" }, StringSplitOptions.RemoveEmptyEntries)[0]);
                    if (i > index)
                    {
                        i--;
                        uExBar.Groups[gr.Index].Key = string.Format("GroupCustom_{0}", i);
                    }
                }
                if (maxGroupIndex > 0) maxGroupIndex--;
                uExBar.ActiveGroup = uExBar.Groups[0];
                uExBar.Groups[0].Selected = true;
                uExBar_SelectedGroupChanged(uExBar, new GroupEventArgs(uExBar.Groups[0]));
            }
        }

        private void txtNewCaptionGroup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnApplyAddNewGroup_Click(btnApplyAddNewGroup, null);
        }

        private void popAddNewGroup_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            txtNewCaptionGroup.Text = string.Empty;
            txtToolTipNewGroup.Text = string.Empty;
        }

        enum Scheduler
        {
            EveryDay,
            EveryWeek,
            EveryMonth
        }
        CancellationTokenSource m_ctSource;
        /// <summary>
        /// returns next date the code to be run
        /// </summary>
        /// <param name="date"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        private DateTime getNextDate(DateTime date, Scheduler scheduler)
        {
            switch (scheduler)
            {
                case Scheduler.EveryDay:
                    return date.AddDays(1);
                case Scheduler.EveryWeek:
                    return date.AddDays(6);
                case Scheduler.EveryMonth:
                    return date.AddMonths(1);
                default:
                    throw new Exception("Invalid scheduler");
            }
        }

        private async void GetPackage()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:26429/");
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.GetAsync("http://localhost:26429/GetPackageByUser/");
            if (response.IsSuccessStatusCode)
            {
                string listpackage = await response.Content.ReadAsStringAsync();
            }
        }

        /// <summary>
        /// check the package and set period time
        /// </summary>
        /// <returns></returns>
        private Scheduler getScheduler()
        {
            //if (radioButton4.Checked)
            //    return Scheduler.EveryDay;
            //if (radioButton5.Checked)
            //    return Scheduler.EveryWeek;
            //if (radioButton6.Checked)
            //    return Scheduler.EveryMonth;

            //default
            return Scheduler.EveryWeek;
        }
        /// <summary>
        /// method to be called after period of time, create backup file 
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        private void MethodToCall(DateTime time)
        {
            Backups backup = new Backups();
            //setup next call
            var nextTimeToCall = getNextDate(time, getScheduler());

            this.BeginInvoke((Action)(() =>
            {
                backup.Create();
            }));
        }
        /// <summary>
        /// Schedule the time to backup
        /// </summary>
        /// <param name="date"></param>
        /// <param name="scheduler"></param>
        private void runCodeAt(DateTime date, Scheduler scheduler)
        {
            m_ctSource = new CancellationTokenSource();

            var dateNow = DateTime.Now;
            TimeSpan ts;
            if (date > dateNow)
                ts = date - dateNow;
            else
            {
                date = getNextDate(date, scheduler);
                ts = date - dateNow;
            }

            Task.Delay(ts).ContinueWith((x) =>
            {
                MethodToCall(date);
                runCodeAt(getNextDate(date, scheduler), scheduler);

            }, m_ctSource.Token);
        }

        private void HandleBackupFile()
        {
            //Utils.IBackupService.BeginTran();
            //DateTime dt = DateTime.Now;
            //List<Backup> lstBackup = Utils.ListBackup.ToList();
            //var ghidetep = Utils.ListSystemOption.FirstOrDefault(o => o.Code == "SLUU_GhiDeTep").Data;
            //Scheduler scheduler = getScheduler();
            //switch (scheduler)
            //{
            //    case Scheduler.EveryDay:
            //        {
            //            DateTime dt1 = new DateTime();
            //            DateTime dayStart = dt1.AddHours(0).AddMinutes(0).AddSeconds(0);
            //            DateTime dayEnd = dt1.AddHours(23).AddMinutes(59).AddSeconds(59);
            //            lstBackup = lstBackup.Where(x => x.CreateDate >= dayStart).Where(x => x.CreateDate <= dayEnd).ToList();
            //            if (lstBackup.Count > 0)
            //            {
            //                if (ghidetep == "1")
            //                {
            //                    Backup bu = lstBackup.OrderByDescending(x => x.CreateDate).FirstOrDefault();
            //                    Utils.IBackupService.Delete(bu);
            //                }
            //            }
            //            break;
            //        }
            //    case Scheduler.EveryMonth:
            //        {
            //            DateTime monthStart = new DateTime(dt.Year, dt.Month, 1);
            //            DateTime monthEnd = monthStart.AddMonths(1).AddDays(-1);
            //            lstBackup = lstBackup.Where(x => x.CreateDate >= monthStart).Where(x => x.CreateDate <= monthEnd).ToList();
            //            if (lstBackup.Count > 0)
            //            {
            //                if (ghidetep == "1")
            //                {
            //                    Backup bu = lstBackup.OrderByDescending(x => x.CreateDate).FirstOrDefault();
            //                    Utils.IBackupService.Delete(bu);
            //                }
            //            }
            //            break;
            //        }
            //    case Scheduler.EveryWeek:
            //        {
            //            int days = dt.Date.DayOfWeek - DayOfWeek.Monday;
            //            DateTime weekStart = dt.Date.AddDays(-days);
            //            DateTime weekEnd = weekStart.AddDays(7);
            //            lstBackup = lstBackup.Where(x => x.CreateDate >= weekStart).Where(x => x.CreateDate <= weekEnd).ToList();
            //            if (lstBackup.Count > 0)
            //            {
            //                if (ghidetep == "1")
            //                {
            //                    Backup bu = lstBackup.OrderByDescending(x => x.CreateDate).FirstOrDefault();
            //                    Utils.IBackupService.Delete(bu);
            //                }
            //            }
            //            break;
            //        }
            //    default:
            //        {

            //            break;
            //        }
            //}

            //Utils.IBackupService.CommitChanges();
            Backups bak = new Backups();
            bak.Create();
            ConnectAmazon ca = new ConnectAmazon();
            //if (ca.Connet())
            //    ca.UploadFile();
        }

        private void FMain_FormClosing(object sender, FormClosingEventArgs e)
        {


            if (FMain.CheckVer && runCheckVersion)
            {
                try
                {
                    //MessageBox.Show("Gọi Tool Update Tvan");
                    Environment.CurrentDirectory = Application.StartupPath;
                    System.Diagnostics.Process.Start(nameUpdateTool);
                }
                catch
                {
                    MessageBox.Show("Không tìm thấy công cụ cập nhật tự động " + nameUpdateTool + " trong thư mục cài đặt\r\nvui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                FMain.CheckVer = false;
                return;
            }
            if (MSG.MessageBoxStand(resSystem.MSG_System_76, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {

                e.Cancel = true;
            }
            else
            {
                Utils.ClearCacheByType<SystemOption>();
                var autobackup = Utils.ListSystemOption.FirstOrDefault(o => o.Code == "SLUU_SauKhiDongCT").Data;
                var note = Utils.ListSystemOption.FirstOrDefault(o => o.Code == "SLUU_NhacKhiDong").Data;
                var day = Utils.ListSystemOption.FirstOrDefault(o => o.Code == "SLUU_NgaySLUU").Data;
                var notbackup = Utils.ListSystemOption.FirstOrDefault(o => o.Code == "SLUU_KhongLuu").Data;
                var filepath = Utils.ListSystemOption.FirstOrDefault(o => o.Code == "SLUU_TMSaoLuu").Data;
                if (filepath == "")
                {
                    MSG.Information("Chưa chọn thư mục để sao lưu dữ liệu. Vui lòng kiểm tra lại !");
                    e.Cancel = true;
                    return;
                }
                if (autobackup == "1")
                {
                    HandleBackupFile();
                }
                if (note == "1")
                {
                    if (MSG.MessageBoxStand(resSystem.MSG_System_82, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        HandleBackupFile();
                    }
                }
                if (notbackup == "1")
                {
                    DateTime dt = DateTime.Now;
                    List<Backup> lstBackup = Utils.ListBackup.ToList();
                    //Hautv edit
                    Server server;
                    if (Utils.isDuLieuMau)
                        server = new Server(new ServerConnection(Properties.Settings.Default.ServerName));
                    else
                        server = new Server(new ServerConnection(Properties.Settings.Default.ServerName, Properties.Settings.Default.UserName, Properties.Settings.Default.Password));
                    DataSet ds = new DataSet();
                    ds = server.ConnectionContext.ExecuteWithResults(string.Format("exec xp_dirtree '{0}', 1, 1", filepath));
                    ds.Tables[0].DefaultView.Sort = "file ASC"; // list directories first, then files
                    DataTable d = ds.Tables[0].DefaultView.ToTable();
                    int i = 0;
                    foreach (DataRow r in d.Rows)
                    {
                        if (r["file"].ToString() == "1")
                            i++;
                    }
                    var count = i;
                    if (lstBackup.Count == 0)
                        HandleBackupFile();
                    if (count == 0)
                        HandleBackupFile();
                    else
                    {
                        var bu = lstBackup.Max(x => x.CreateDate);
                        var lateday = dt.Day - bu.AddDays(day.ToInt()).Day;
                        var lst = lstBackup.Where(x => x.CreateDate > bu && x.CreateDate <= dt).ToList();
                        var lst1 = lstBackup.Where(x => x.CreateDate == bu.AddDays(day.ToInt())).ToList();
                        if (dt.ToString("dd/MM/yyyy") == bu.AddDays(day.ToInt()).ToString("dd/MM/yyyy"))
                        {
                            if (lst.Count == 0)
                            {
                                MSG.Warning("Dữ liệu chưa được sao lưu trong một khoảng thời gian. Hệ thống sẽ tự động sao lưu để đảm bảo an toàn dữ liệu.");
                                HandleBackupFile();
                            }
                        }
                        if (lateday > 0)
                        {
                            if (dt.ToString("dd/MM/yyyy") == bu.AddDays(day.ToInt() + lateday).ToString("dd/MM/yyyy"))
                            {
                                if (lst1.Count == 0)
                                {
                                    MSG.Warning("Dữ liệu chưa được sao lưu trong một khoảng thời gian. Hệ thống sẽ tự động sao lưu để đảm bảo an toàn dữ liệu.");
                                    HandleBackupFile();
                                }
                            }
                        }
                    }
                }



            }
        }
        private void CheckAutoUpdateBakAndKlib()
        {
            try
            {
                //Check file AutoUpdate có tồn tại không?
                string path = System.IO.Path.GetDirectoryName(
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                string fileAutoUpdate = path + "\\" + nameUpdateTool;
                string fileKib = path + "\\" + nameKlib;
                if (!File.Exists(fileAutoUpdate) || !File.Exists(fileKib)) ;// MessageBox.Show("Không tìm thấy công cụ cập nhật tự động trong thư mục cài đặt!");
                //Check file .bak có tồn tại không?
                string fileAutoUpdateBak = path + "\\" + Path.ChangeExtension(nameUpdateTool, "bak");
                string fileKilbBak = path + "\\" + Path.ChangeExtension(nameKlib, "bak");
                if (File.Exists(fileAutoUpdateBak))
                {
                    if (File.Exists(fileAutoUpdate)) File.Delete(fileAutoUpdate);
                    File.Move(fileAutoUpdateBak, fileAutoUpdate);
                }
                if (!File.Exists(fileKilbBak)) return;
                if (File.Exists(fileKib)) File.Delete(fileKib);
                File.Move(fileKilbBak, fileKib);
            }
            catch { }
        }

        private void FMain_Shown(object sender, EventArgs e)
        {
            ConfigSubGroupMain(new GroupEventArgs(uExBar.ActiveGroup), false);
            //Edit by Hautv _Du lieu mau
            if (!_isDuLieuMau)
                #region Thông báo update
                try
                {
                    string path = System.IO.Path.GetDirectoryName(
                                  System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                    string filePath = string.Format("{0}\\Notify_Update\\{1}.xml", path, Frm.FrmCauHinh.GetLocalVersionNumber());
                    XmlDocument doc = new XmlDocument();
                    // Loading from a XML string (use Load() for file)
                    doc.Load(filePath);
                    XmlNode xmlNode = doc.SelectSingleNode("Notifys/count");
                    int n = int.Parse(xmlNode.InnerText.Replace("\n", String.Empty).Replace("\r", String.Empty).Replace("\t", String.Empty).Trim(' '));
                    if (0 <= n && n < 3)
                    {

                        // Selecting node using XPath syntax
                        XmlNodeList idNodes = doc.SelectNodes("Notifys/Notify");
                        // Filling the list
                        List<string> lst = new List<string>();
                        foreach (XmlNode node in idNodes)
                            lst.Add(node.InnerText/*.Replace("\n", String.Empty).Replace("\r", String.Empty).Replace("\t", String.Empty)*/);

                        //UpdateNotification updateNotification = new UpdateNotification(lst);
                        //updateNotification.ShowDialog(this);
                    }
                }
                catch (Exception ex)
                {

                }
            #endregion
            if (_isDuLieuMau)
            {
                MSG.Information("Đây là dữ liệu mẫu dùng để tham khảo và không có giá trị hoạch toán!");
            }

        }
        bool allowResize = false;

        private void groupMainMenu_MouseMove(object sender, MouseEventArgs e)
        {

        }

        private void groupMainMenu_MouseUp(object sender, MouseEventArgs e)
        {

        }

        private void groupMainMenu_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void groupMainMenu_MouseHover(object sender, EventArgs e)
        {

        }

        private void ultraPanel1_MouseMove(object sender, MouseEventArgs e)
        {
            //if (allowResize)
            //{

            //    this.uExBar.Width = this.uExBar.Width + e.X;

            //    this.palMainMenu.Width = this.palMainMenu.Width - e.X;
            //    this.ultraPanel1.Location= new Point( this.ultraPanel1.Location.X + e.X, this.ultraPanel1.Location.Y);
            //}
        }

        private void ultraPanel1_MouseDown(object sender, MouseEventArgs e)
        {
            allowResize = true;
        }

        private void ultraPanel1_MouseUp(object sender, MouseEventArgs e)
        {
            allowResize = false;
        }

        private void groupMainMenu_Paint(object sender, PaintEventArgs e)
        {
            var ar32 = "";
            groupMainMenu.ViewStyle = GroupBoxViewStyle.Office2007;
        }
    }

    static class FormCus
    {
        public static DialogResult ShowFormDialog(this Form @this, IWin32Window owner)
        {
            try
            {
                WaitingFrm.StartWaiting();
                @this.Layout += new LayoutEventHandler(this_Layout);
                @this.Load += new EventHandler(this_Load);
                @this.LostFocus += new EventHandler(this_LostFocus);
                if (@this is CustormForm)
                    return ((CustormForm)@this).ShowDialog(owner);
                return @this.ShowDialog(owner);
            }
            catch (Exception ex)
            {
                MSG.Warning("Có lỗi xảy ra. Chi tiết lỗi : \r\n " + ex);
                return DialogResult.None;
            }
        }
        //HUYPD Edit Show Form
        public static void ShowFormDialogHD(this Form @this, IWin32Window owner)
        {
            try
            {
                WaitingFrm.StartWaiting();
                @this.Layout += new LayoutEventHandler(this_Layout);
                @this.Load += new EventHandler(this_Load);
                @this.LostFocus += new EventHandler(this_LostFocus);
                if (@this is CustormForm)
                    // return 
                    ((CustormForm)@this).ShowDialogHD(owner);
                //return
                @this.Show(owner);
            }
            catch (Exception ex)
            {
                MSG.Warning("Có lỗi xảy ra. Chi tiết lỗi : \r\n " + ex);
                //return DialogResult.None;
            }
        }
        static void this_LostFocus(object sender, EventArgs e)
        {

        }

        static void this_Load(object sender, EventArgs e)
        {

        }

        static void this_Layout(object sender, LayoutEventArgs e)
        {
            WaitingFrm.StopWaiting();
        }
    }
}
