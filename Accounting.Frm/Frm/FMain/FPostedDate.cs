﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;

namespace Accounting
{
    public partial class FPostedDate : CustormForm
    {
        public FPostedDate()
        {
            InitializeComponent();
            dtePostedDate.Value = Utils.StringToDateTime(Utils.GetDbStartDate());
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            SystemOption system = Utils.ISystemOptionService.GetByCode("NgayHachToan");
            if (system == null) return;
            try
            {
                Utils.ISystemOptionService.BeginTran();
                DateTime postedDate = Convert.ToDateTime(dtePostedDate.Value);
                system.Data = postedDate.Date.ToString("dd/MM/yyyy");
                system.DefaultData = DateTime.Now.ToString("dd/MM/yyyy");
                
                Utils.ISystemOptionService.Update(system);
                Utils.ISystemOptionService.CommitTran();
                Utils.ListSystemOption.Clear();
                TextMessage.ConstFrm.DbStartDate = Utils.GetDbStartDate();
                Close();
            }
            catch (Exception)
            {
                Utils.ISystemOptionService.RolbackTran();
                Accounting.TextMessage.MSG.Warning(Accounting.TextMessage.resSystem.OPN02);
            }
        }
    }
}
