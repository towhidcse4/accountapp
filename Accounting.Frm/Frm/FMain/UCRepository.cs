﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class UCRepository : UserControl
    {
        public UCRepository(FMain @this)
        {
            InitializeComponent();
            btnRSInventoryAdjustments.Click += (s, e) => btnRSInventoryAdjustments_Click(s, e, @this);
            btnRSInwardOutward.Click += (s, e) => btnRSInwardOutward_Click(s, e, @this);
            btnRSInwardOutwardOutput.Click += (s, e) => btnRSInwardOutwardOutput_Click(s, e, @this);
            btnRS09DNN.Click += (s, e) => btnRS09DNN_Click(s, e, @this);
            btnRSPricingOW.Click += (s, e) => btnRSPricingOW_Click(s, e, @this);
        }

        private void btnRSInventoryAdjustments_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFRSInventoryAdjustments();
        }

        private void btnRSInwardOutward_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFRSInwardOutward();
        }

        private void btnRSInwardOutwardOutput_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFRSInwardOutwardOutput();
        }

        private void btnRS09DNN_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFRS09DNN();
        }

        private void btnRSPricingOW_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFRSPricingOW();
        }
    }
}
