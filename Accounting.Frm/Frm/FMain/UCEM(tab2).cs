﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class UCEM_tab2_ : UserControl
    {
        public UCEM_tab2_(FMain @this)
        {
            InitializeComponent();
            btnEMRegistration.Click += (s, e) => btnEMRegistration_Click(s, e, @this);
            btnEMShareHolder.Click += (s, e) => btnEMShareHolder_Click(s, e, @this);
            btnEMTransfer.Click += (s, e) => btnEMTransfer_Click(s, e, @this);
            btnRDividendPayable.Click += (s, e) => btnRDividendPayable_Click(s, e, @this);
        }

        private void btnEMRegistration_Click(object sender, EventArgs e,FMain fMain)
        {
            fMain.ShowFEMRegistration();
        }

        private void btnEMShareHolder_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFEMShareHolder();
        }

        private void btnEMTransfer_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFEMTransfer();
        }

        private void btnRDividendPayable_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFRDividendPayable();
        }
    }
}
