﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Windows.Forms;
using Accounting.TextMessage;
using System.Xml;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using System.Text.RegularExpressions;

namespace Accounting
{
    public partial class FCreateDatabase : DialogForm
    {
        private DataTable _servers;
        string serverOldDB = "";
        string userOldDB = "ADMIN";
        string passOldDB = "";
        public FCreateDatabase()
        {
            InitializeComponent();
            string myServer = Environment.MachineName;
            cbbServers.Text = string.Format(".\\HDSA");
            string path = System.IO.Path.GetDirectoryName(
            System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            //string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var filePatch = string.Format("{0}\\Data", path).Replace("file:\\", "");
            //var filePatch = "C:\\VnptAccountingData";
            txtDataPatch.Text = filePatch;
            tabControl.ConfigTabControlWizard();
            dteDBStartDate.Value = new DateTime(DateTime.Now.Year, 1, 1);
            dteDBStartYear.Value = new DateTime(DateTime.Now.Year, 1, 1);
            txtYear.Value = DateTime.Now.Year;
            serverOldDB = cbbServers.Text;
            userOldDB = txtUsr.Text;
            passOldDB = txtPwd.Text;
            GetDatacbbNhomHHDV(cbbNhomHHDV);
            GetDatacbbNhomNNMD(cbbNhomNNMD);
            ultraOptionSet1.CheckedIndex = 0;
            cbbServers.Focus();
        }
        public static void GetDatacbbNhomHHDV(UltraCombo cbb)
        {
            var lst = new List<GoodsServicePurchase>();
            GoodsServicePurchase md1 = new GoodsServicePurchase()
            {
                GoodsServicePurchaseCode = "1",
                GoodsServicePurchaseName = "Hàng hoá, dịch vụ dùng riêng cho SXKD chịu thuế GTGT và sử dụng cho các hoạt động cung cấp hàng hoá, dịch vụ không kê khai, nộp thuế GTGT đủ điều kiện khấu trừ thuế",
            };
            GoodsServicePurchase md2 = new GoodsServicePurchase()
            {
                GoodsServicePurchaseCode = "2",
                GoodsServicePurchaseName = "Hàng hóa, dịch vụ dùng chung cho SXKD chịu thuế và không chịu thuế đủ điều kiện khấu trừ thuế",
            };
            GoodsServicePurchase md3 = new GoodsServicePurchase()
            {
                GoodsServicePurchaseCode = "3",
                GoodsServicePurchaseName = "Hàng hóa, dịch vụ dùng cho dự án đầu tư đủ điều kiện khấu trừ thuế",
            };
            GoodsServicePurchase md4 = new GoodsServicePurchase()
            {
                GoodsServicePurchaseCode = "4",
                GoodsServicePurchaseName = "Hàng hóa, dịch vụ không đủ điều kiện khấu trừ",
            };
            GoodsServicePurchase md5 = new GoodsServicePurchase()
            {
                GoodsServicePurchaseCode = "5",
                GoodsServicePurchaseName = "Hàng hóa, dịch vụ không phải tổng hợp trên tờ khai 01/GTGT",
            };
            lst.Add(md1); lst.Add(md2); lst.Add(md3); lst.Add(md4); lst.Add(md5);
            cbb.DataSource = new BindingList<GoodsServicePurchase>(lst);
            cbb.DisplayMember = "GoodsServicePurchaseName";
            cbb.ValueMember = "GoodsServicePurchaseCode";
            Utils.ConfigGrid(cbb, ConstDatabase.GoodsServicePurchase_TableName);
            cbb.SelectedRow = cbb.Rows.FirstOrDefault();
        }
        public static void GetDatacbbNhomNNMD(UltraCombo cbb)
        {
            var lst = new List<CareerGroup>();
            CareerGroup md1 = new CareerGroup()
            {
                CareerGroupCode = "1",
                CareerGroupName = "Hàng hóa dịch vụ không chịu thuế GTGT hoặc hàng hóa dịch vụ áp dụng thuế suất 0%",
            };
            CareerGroup md2 = new CareerGroup()
            {
                CareerGroupCode = "2",
                CareerGroupName = "Phân phối, cung cấp hàng hóa áp dụng thuế suất 1%",
            };
            CareerGroup md3 = new CareerGroup()
            {
                CareerGroupCode = "3",
                CareerGroupName = "Dịch vụ, xây dựng không bao thầu nguyên vật liệu áp dụng thuế suất 5%",
            };
            CareerGroup md4 = new CareerGroup()
            {
                CareerGroupCode = "4",
                CareerGroupName = "Sản xuất, vận tải, dịch vụ có gắn với hàng hóa, xây dựng có bao thầu nguyên vật liệu áp dụng thuế suất 3%",
            };
            CareerGroup md5 = new CareerGroup()
            {
                CareerGroupCode = "5",
                CareerGroupName = "Hoạt động kinh doanh khác áp dụng thuế suất 2%",
            };
            lst.Add(md1); lst.Add(md2); lst.Add(md3); lst.Add(md4); lst.Add(md5);
            cbb.DataSource = new BindingList<CareerGroup>(lst);
            cbb.DisplayMember = "CareerGroupName";
            cbb.ValueMember = "CareerGroupCode";
            Utils.ConfigGrid(cbb, ConstDatabase.CareerGroup_TableName);
            cbb.SelectedRow = cbb.Rows.FirstOrDefault();
        }
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            WaitingFrm.StartWaiting();
            if (string.IsNullOrEmpty(cbbServers.Text))
            {
                WaitingFrm.StopWaiting();
                MSG.Warning("Bạn chưa chọn máy chủ SQL.");
                tabControl.Tabs[0].Selected = true;
                cbbServers.Focus();
                return;
            }
            bool isHave = false;
            //if (_servers == null)
            //    _servers = SqlDataSourceEnumerator.Instance.GetDataSources();
            //var array = cbbServers.Text.Split('\\');
            //for (int i = 0; i < _servers.Rows.Count; i++)
            //{
            //    if (array.Length > 1 ? ((_servers.Rows[i]["ServerName"] as string) == array[0] && (_servers.Rows[i]["InstanceName"] as string) == array[1]) : (_servers.Rows[i]["ServerName"] as string) == array[0])
            //        isHave = true;

            //}
            //if (!isHave)
            //{
            //    WaitingFrm.StopWaiting();
            //    MSG.Warning("Máy chủ SQL không tồn tại. Vui lòng chọn máy chủ SQL khác.");
            //    tabControl.Tabs[0].Selected = true;
            //    cbbServers.Focus();
            //    return;
            //}
            if (string.IsNullOrEmpty(txtUsr.Text))
            {
                WaitingFrm.StopWaiting();
                MSG.Warning("Tài khoản quản trị không được để trống.");
                tabControl.Tabs[0].Selected = true;
                txtUsr.Focus();
                return;
            }
            if (!txtUsr.Text.Trim().ToUpper().Equals("ADMIN") && string.IsNullOrEmpty(txtPwd.Text))
            {
                WaitingFrm.StopWaiting();
                MSG.Warning("Mật khẩu quản trị không được để trống.");
                tabControl.Tabs[0].Selected = true;
                txtPwd.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtDatabase.Text))
            {
                WaitingFrm.StopWaiting();
                MSG.Warning("Tên dữ liệu không được để trống.");
                tabControl.Tabs[0].Selected = true;
                txtDatabase.Focus();
                return;
            }
            char[] a = new char[] { '+', '-', '.', ',', '/', '?', '"', '\'', ':', '\\', ';', '=', '@', '$', '%', '#', '^', '*', '(', ')', '[', ']', '|', '!', '~', '`', '^', '<', '>' };
            foreach (char c in txtDatabase.Text)
            {
                if (a.Contains(c))
                {
                    WaitingFrm.StopWaiting();
                    MSG.Warning("Tên dữ liệu không được có các ký tự sau (+ - . , / ? \" \' : \\ ; = @ $ % # ^ * ( ) [ ] | ! ~ ` ^ < >).");
                    tabControl.Tabs[0].Selected = true;
                    txtDatabase.Focus();
                    return;
                }
            }
            if (string.IsNullOrEmpty(txtDataPatch.Text))
            {
                WaitingFrm.StopWaiting();
                MSG.Warning("Bạn chưa chọn nơi lưu trữ cơ sở dữ liệu.");
                tabControl.Tabs[0].Selected = true;
                txtDataPatch.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtCompanyName.Text))
            {
                WaitingFrm.StopWaiting();
                MSG.Warning("Bạn chưa nhập tên công ty.");
                tabControl.Tabs[1].Selected = true;
                txtCompanyName.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtCompanyAdress.Text))
            {
                WaitingFrm.StopWaiting();
                MSG.Warning("Bạn chưa nhập địa chỉ công ty.");
                tabControl.Tabs[1].Selected = true;
                txtCompanyAdress.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtCompanyTaxcode.Text))
            {
                MSG.Warning("Bạn chưa nhập mã số thuế công ty.");
                tabControl.Tabs[1].Selected = true;
                txtCompanyTaxcode.Focus();
                return;
            }
            if (!string.IsNullOrEmpty(txtCompanyTaxcode.Text) && !Core.Utils.CheckMST(txtCompanyTaxcode.Text))
            {
                WaitingFrm.StopWaiting();
                MSG.Warning("Mã số thuế chưa đúng định dạng. Vui lòng nhập lại.");
                tabControl.Tabs[1].Selected = true;
                txtCompanyTaxcode.Focus();
                return;
            }
            //if (string.IsNullOrEmpty(txtPhoneNumber.Text))
            //{
            //    WaitingFrm.StopWaiting();
            //    MSG.Warning("Bạn chưa nhập điện thoại liên hệ.");
            //    tabControl.Tabs[1].Selected = true;
            //    txtPhoneNumber.Focus();
            //    return;
            //}
            //if (string.IsNullOrEmpty(txtEmail.Text))
            //{
            //    WaitingFrm.StopWaiting();
            //    MSG.Warning("Bạn chưa nhập Email liên hệ.");
            //    tabControl.Tabs[1].Selected = true;
            //    txtEmail.Focus();
            //    return;
            //}
            //if (!txtEmail.Text.IsValidEmail())
            //{
            //    WaitingFrm.StopWaiting();
            //    MSG.Warning("Email chưa đúng định dạng. Vui lòng nhập lại.");
            //    tabControl.Tabs[1].Selected = true;
            //    txtEmail.Focus();
            //    return;
            //}
            if (dteDBStartYear.Value == null)
            {
                WaitingFrm.StopWaiting();
                MSG.Warning("Bạn chưa nhập ngày bắt đầu năm tài chính.");
                tabControl.Tabs[2].Selected = true;
                dteDBStartYear.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtYear.Text))
            {
                WaitingFrm.StopWaiting();
                MSG.Warning("Bạn chưa nhập năm tài chính.");
                tabControl.Tabs[0].Selected = true;
                txtDataPatch.Focus();
                return;
            }
            if (dteDBStartDate.Value == null)
            {
                WaitingFrm.StopWaiting();
                MSG.Warning("Bạn chưa nhập ngày hạch toán.");
                tabControl.Tabs[2].Selected = true;
                dteDBStartDate.Focus();
                return;
            }
            //if (string.IsNullOrEmpty(txtEmailForgotPass.Text))
            //{
            //    WaitingFrm.StopWaiting();
            //    MSG.Warning("Bạn nên nhập email để đặt lại mật khẩu đăng nhập tài khoản quản trị khi quên mật khẩu.");
            //}
            string server = cbbServers.Text;
            string database = txtDatabase.Text;
            string user = txtUsr.Text;
            string pass = txtPwd.Text;
            bool @finally = false;
            var con = new SqlConnection();
            #region TH tạo mới CSDL
            string sqlConnectionString =
                string.Format(
                    "Data Source={0};initial catalog=master;Integrated Security=false;User ID={1};Password={2};",
                    server, user, pass);
            if (txtUsr.Text.Trim().ToUpper().Equals("ADMIN") && string.IsNullOrEmpty(txtPwd.Text))
                sqlConnectionString =
                    string.Format("Data Source={0};initial catalog=master;Integrated Security=SSPI;", server);
            con = new SqlConnection(sqlConnectionString);
            SqlTransaction transaction;
            try
            {
                con.Open();
            }
            catch (Exception)
            {
                WaitingFrm.StopWaiting();
                MSG.Warning("Máy chủ SQL không tồn tại. Vui lòng chọn máy chủ SQL khác.");
                tabControl.Tabs[0].Selected = true;
                cbbServers.Focus();
                return;
            }
            try
            {
                //Tạo mới Patch lưu file database nếu chưa có
                bool exist = false;
                if (!System.IO.Directory.Exists(txtDataPatch.Text))
                    System.IO.Directory.CreateDirectory(txtDataPatch.Text);
                //Cấp quyền cho thư mục mới tạo
                DirectorySecurity sec = Directory.GetAccessControl(txtDataPatch.Text);
                SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                Directory.SetAccessControl(txtDataPatch.Text, sec);
                //Tạo Database mới
                //1.Kiểm tra CSDL đã tồn tại chưa. Nếu đã tồn tại thì báo lỗi
                var qr =
                    string.Format(
                        @"SELECT name FROM master.sys.databases WHERE name = N'{0}'",
                        txtDatabase.Text);
                try
                {
                    using (SqlCommand sqlCmd = new SqlCommand(qr, con))
                    {
                        SqlDataReader reader = sqlCmd.ExecuteReader();
                        exist = reader.HasRows;
                        reader.Close();
                    }
                }
                catch (SqlException)
                {
                    WaitingFrm.StopWaiting();
                    MSG.Warning("Máy chủ SQL không tồn tại. Vui lòng chọn máy chủ SQL khác.");
                    tabControl.Tabs[0].Selected = true;
                    cbbServers.Focus();
                    return;
                }
                if (exist)
                {
                    WaitingFrm.StopWaiting();
                    MSG.Warning("Cơ sở dữ liệu này đã tồn tại. Vui lòng tạo mới cơ sở dữ liệu khác");
                    goto FINALLY;
                }
                //2.Nếu CSDL chưa tồn tại thì tạo mới
                var newData = string.Format(@"{1}\{0}.mdf", database, txtDataPatch.Text).Replace("\\\\", @"\");
                var newLog = string.Format(@"{1}\{0}Log.ldf", database, txtDataPatch.Text).Replace("\\\\", @"\");

                //Xóa các file mô tả đã tồn tại
                if (File.Exists(newData))
                {
                    File.Delete(newData);
                }

                if (File.Exists(newLog))
                {
                    File.Delete(newLog);
                }

                qr = string.Format("CREATE DATABASE {0} ON PRIMARY " +
                                        "(NAME = {0}_Data, " +
                                        "FILENAME = '{2}', " +
                                        "SIZE = 10096KB, MAXSIZE = 29687808KB, FILEGROWTH = 262144KB) " +
                                        "LOG ON (NAME = {0}_Log, " +
                                        "FILENAME = '{3}', " +
                                        "SIZE = 10096KB, " +
                                        "MAXSIZE = 29687808KB, " +
                                        "FILEGROWTH = 10%)", database, txtDataPatch.Text, newData, newLog);
                new SqlCommand(qr, con).ExecuteNonQuery();

                //3. Sử dụng CSDL mới tạo
                var qr_use = string.Format(@"USE {0}", database);
                new SqlCommand(qr_use, con).ExecuteNonQuery();
            }
            catch (SqlException sqlError)
            {
                WaitingFrm.StopWaiting();
                MSG.Warning("Có lỗi xảy ra khi khởi tạo cơ sở dữ liệu mới. \r\n " + sqlError);
                goto FINALLY;
            }
            //Thực hiện chạy file srcipt SQL có sẵn
            transaction = con.BeginTransaction();
            try
            {
                using (System.IO.Stream stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Accounting.Data.NewDatabases.sql"))
                {
                    if (stream != null)
                    {
                        using (StreamReader sr = new StreamReader(stream))
                        {
                            var fileContent = sr.ReadToEnd();
                            fileContent = fileContent.Replace("ACCOUNTING", txtDatabase.Text).Replace("\r\n", " ");
                            var sqlqueries = fileContent.Split(new[] { " GO " }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (var query in sqlqueries)
                            {
                                try
                                {
                                    new SqlCommand(query, con, transaction).ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    WaitingFrm.StopWaiting();
                                    MSG.Warning(query + "\r\n" + ex.ToString());
                                    goto ERROR;
                                }

                            }
                            transaction.Commit();
                            @finally = true;
                        }
                    }
                    else goto ERROR;
                }

                //using (System.IO.Stream stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Accounting.Data.Updates.ACCOUNTING_Data_Sync.sql"))
                //{
                //    if (stream != null)
                //    {
                //        using (StreamReader sr = new StreamReader(stream))
                //        {
                //            var fileContent = sr.ReadToEnd();
                //            fileContent = fileContent.Replace("AccountingDevelop", txtDatabase.Text).Replace("\r\n", " ");
                //            var sqlqueries = fileContent.Split(new[] { " GO " }, StringSplitOptions.RemoveEmptyEntries);
                //            foreach (var query in sqlqueries)
                //            {
                //                try
                //                {
                //                    new SqlCommand(query, con, transaction).ExecuteNonQuery();
                //                }
                //                catch (Exception ex)
                //                {
                //                    WaitingFrm.StopWaiting();
                //                    MSG.Warning(query + "\r\n" + ex.ToString());
                //                    goto ERROR;
                //                }

                //            }
                //            @finally = true;
                //        }
                //    }
                //}
            }
            catch (SqlException sqlError)
            {
                WaitingFrm.StopWaiting();
                MSG.Warning(sqlError.ToString());
                goto ERROR;
            }
            goto FINALLY;
        ERROR:
            transaction.Rollback();
            try
            {
                new SqlCommand("USE [master]", con).ExecuteNonQuery();
                new SqlCommand(string.Format("DROP DATABASE {0};", database), con);
            }
            catch (Exception ex)
            {
            }
            WaitingFrm.StopWaiting();
            MSG.Warning("Có lỗi xảy ra khi khởi tạo dữ liệu mới.");
        FINALLY:
            if (con.State == ConnectionState.Open)
            {
                //con.Close();
            }
            #endregion
            #region Tạo mới từ CSDL cũ
            //else
            //{
            //    if (string.IsNullOrEmpty(cbbLastDatabase.Text))
            //    {
            //        WaitingFrm.StopWaiting();
            //        return;
            //    }
            //    var @DB = cbbLastDatabase.Text;
            //    string path = System.IO.Path.GetDirectoryName(
            //        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            //    var @BackupFile = string.Format(@"{0}\{1}.bak", path, @DB).Replace("file:\\", "").Replace("\\", @"\");
            //    var @NewDB = database;
            //    var @RestoreFile = string.Format(@"{1}\{0}", database, txtDataPatch.Text).Replace("\\\\", @"\").Replace("\\", @"\");
            //    var @DataFile = @RestoreFile + ".mdf";
            //    var @LogFile = @RestoreFile + "Log.ldf";
            //    var @query = @"BACKUP DATABASE " + @DB + " TO DISK = N'" + @BackupFile + "'";
            //    var sqlConnectionString =
            //    string.Format(
            //    "Data Source={0};initial catalog=master;Integrated Security=SSPI;User ID={1};Password={2};",
            //    server, user, pass);
            //    if (txtUsr.Text.Trim().ToUpper().Equals("ADMIN") && string.IsNullOrEmpty(txtPwd.Text))
            //        sqlConnectionString =
            //            string.Format("Data Source={0};initial catalog=master;Integrated Security=SSPI;", server);
            //    string sqlCnnStrOldDB =
            //        string.Format(
            //            "Data Source={0};initial catalog=master;Integrated Security=SSPI;User ID={1};Password={2};",
            //            serverOldDB, userOldDB, passOldDB);
            //    if (txtUsr.Text.Trim().ToUpper().Equals("ADMIN") && string.IsNullOrEmpty(txtPwd.Text))
            //        sqlCnnStrOldDB =
            //            string.Format("Data Source={0};initial catalog=master;Integrated Security=SSPI;", serverOldDB);
            //    con = new SqlConnection(sqlConnectionString);
            //    var conOldDB = new SqlConnection(sqlCnnStrOldDB);
            //    con.Open();
            //    bool exist = false;
            //    try
            //    {
            //        //1.Kiểm tra CSDL đã tồn tại chưa. Nếu đã tồn tại thì báo lỗi
            //        var qr =
            //            string.Format(
            //                @"SELECT name FROM master.sys.databases WHERE name = N'{0}'",
            //                txtDatabase.Text);
            //        using (SqlCommand sqlCmd = new SqlCommand(qr, con))
            //        {
            //            SqlDataReader reader = sqlCmd.ExecuteReader();
            //            exist = reader.HasRows;
            //            reader.Close();
            //        }
            //        if (exist)
            //        {
            //            WaitingFrm.StopWaiting();
            //            MSG.Warning("Cơ sở dữ liệu này đã tồn tại. Vui lòng tạo mới cơ sở dữ liệu khác");
            //            goto FINALLY;
            //        }
            //    }
            //    catch (Exception)
            //    {
            //        WaitingFrm.StopWaiting();
            //        MSG.Warning("Có lỗi xảy ra khi khởi tạo dữ liệu mới.");
            //        goto FINALLY;
            //    }
            //    conOldDB.Open();
            //    //SqlTransaction tranOldDB = conOldDB.BeginTransaction();
            //    try
            //    {
            //        //Tạo Database mới
            //        using (SqlCommand sqlCmd = new SqlCommand(@query, conOldDB))
            //        {
            //            sqlCmd.ExecuteNonQuery();
            //        }
            //        //tranOldDB.Commit();
            //        goto NEXT;
            //    }
            //    catch (Exception)
            //    {
            //        WaitingFrm.StopWaiting();
            //        goto ERROR1;
            //    }
            //ERROR1:
            //    //tranOldDB.Rollback();
            //    WaitingFrm.StopWaiting();
            //    MSG.Warning("Có lỗi xảy ra khi khởi tạo dữ liệu mới.");
            //    goto FINALLY;

            //NEXT:
            //    //SqlTransaction transaction = con.BeginTransaction();
            //    try
            //    {
            //        //                        @query = @"CREATE TABLE #restoretemp
            //        //                                    (
            //        //                                     LogicalName varchar(500),
            //        //                                     PhysicalName varchar(500),
            //        //                                     type varchar(10),
            //        //                                     FilegroupName varchar(200),
            //        //                                     size int,
            //        //                                     maxsize bigint
            //        //                                    )";
            //        //                        using (SqlCommand sqlCmd = new SqlCommand(@query, con))
            //        //                        {
            //        //                            sqlCmd.ExecuteNonQuery();
            //        //                        }
            //        //                        @query = @"INSERT #restoretemp";
            //        //                        using (SqlCommand sqlCmd = new SqlCommand(@query, con))
            //        //                        {
            //        //                            sqlCmd.ExecuteNonQuery();
            //        //                        }
            //        //@query = @"RESTORE HEADERONLY FROM DISK = N'" + @BackupFile + "'";
            //        //using (SqlCommand sqlCmd = new SqlCommand(@query, con))
            //        //{
            //        //    sqlCmd.ExecuteNonQuery();
            //        //}
            //        @query = @"RESTORE FILELISTONLY FROM DISK = N'" + @BackupFile + "'";
            //        //using (SqlCommand sqlCmd = new SqlCommand(@query, con))
            //        //{
            //        //    sqlCmd.ExecuteNonQuery();
            //        //}
            //        @query += @" RESTORE DATABASE [" + @NewDB + "] FROM DISK = N'" + @BackupFile +
            //                "' WITH  FILE = 1, MOVE N'" + @DB + "_Data' TO N'" + @DataFile + "', MOVE N'" + @DB
            //                + "_Log' TO N'" + @LogFile + "', NOUNLOAD,  REPLACE,  STATS = 10";
            //        using (SqlCommand sqlCmd = new SqlCommand(@query, con))
            //        {
            //            sqlCmd.ExecuteNonQuery();
            //        }
            //        //transaction.Commit();
            //        if (File.Exists(@BackupFile))
            //        {
            //            File.Delete(@BackupFile);
            //        }
            //        if (chkDeleteOldDB.CheckState == CheckState.Checked)
            //            try
            //            {
            //                new SqlCommand("USE [master]", conOldDB).ExecuteNonQuery();
            //                new SqlCommand(string.Format("DROP DATABASE {0};", @DB), conOldDB);
            //            }
            //            catch (Exception ex)
            //            {
            //                MSG.Warning("Lỗi không thể xóa dữ liệu kế toán năm trước.");
            //            }
            //        @finally = true;
            //    }
            //    catch (SqlException)
            //    {
            //        goto ERROR2;
            //    }
            //    goto FINALLY;
            //ERROR2:
            //    //transaction.Rollback();
            //    try
            //    {
            //        new SqlCommand("USE [master]", con).ExecuteNonQuery();
            //        new SqlCommand(string.Format("DROP DATABASE {0};", database), con);
            //    }
            //    catch (Exception ex)
            //    {
            //    }
            //    WaitingFrm.StopWaiting();
            //    MSG.Warning("Có lỗi xảy ra khi khởi tạo dữ liệu mới.");
            //FINALLY:
            //    if (con.State == ConnectionState.Open)
            //    {
            //        //con.Close();
            //    }
            //    if (conOldDB.State == ConnectionState.Open)
            //    {
            //        conOldDB.Close();
            //    }
            //}
            #endregion
            if (@finally)
            {
                string connString = string.Format("Data Source={0};initial catalog={1};{2}", server, database, (user.ToUpper().Equals("ADMIN") && string.IsNullOrEmpty(pass) ? "Integrated Security=SSPI" : string.Format("User Id={0};Password={1};", user, pass)));
                string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                var filePatch = string.Format("{0}\\Config\\NHibernateConfig", path).Replace("file:\\", "");
                try
                {
                    System.IO.File.SetAttributes(filePatch, System.IO.FileAttributes.Normal);
                    DirectorySecurity sec = Directory.GetAccessControl(filePatch);
                    SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                    sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                    Directory.SetAccessControl(filePatch, sec);
                    FileSecurity security = File.GetAccessControl(filePatch);
                    SecurityIdentifier everybody = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                    security.AddAccessRule(new FileSystemAccessRule(everybody, FileSystemRights.FullControl, AccessControlType.Allow));
                    File.SetAccessControl(filePatch, security);
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(filePatch);
                    XmlNode root = xmlDoc.DocumentElement;
                    XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
                    nsmgr.AddNamespace("ns", "urn:nhibernate-configuration-2.2");
                    if (root != null)
                    {
                        XmlNodeList nodeList = root.SelectNodes("//ns:property", nsmgr);
                        if (nodeList != null)
                            foreach (XmlNode cfgNode in nodeList)
                            {
                                if (cfgNode.Attributes != null && cfgNode.Attributes["name"] != null && cfgNode.Attributes["name"].Value.Equals("connection.connection_string"))
                                {
                                    if (server != null)
                                    {
                                        cfgNode.InnerXml = connString;
                                    }
                                }
                            }
                    }
                    xmlDoc.Save(filePatch);
                    var transactions = con.BeginTransaction();
                    try
                    {

                        var UpdateQueries = new List<string>();
                        var updateString = "UPDATE SystemOption SET Data = N'{0}' WHERE Code = N'{1}'";

                        UpdateQueries.Add(string.Format(updateString, txtCompanyName.Text.Trim(), "TTDN_Tencty"));
                        UpdateQueries.Add(string.Format(updateString, txtCompanyAdress.Text.Trim(), "TTDN_Diachi"));
                        UpdateQueries.Add(string.Format(updateString, txtCompanyTaxcode.Text.Trim(), "TTDN_MST"));
                        UpdateQueries.Add(string.Format(updateString, txtPhoneNumber.Text.Trim(), "TTDN_PhoneNumber"));
                        UpdateQueries.Add(string.Format(updateString, txtFax.Text.Trim(), "TTDN_Fax"));
                        UpdateQueries.Add(string.Format(updateString, txtEmail.Text.Trim(), "TTDN_Email"));
                        UpdateQueries.Add(string.Format(updateString, txtEmailForgotPass.Text.Trim(), "EmailForgotPass"));
                        UpdateQueries.Add(string.Format(updateString, cbbPPTGXK.Text.Trim(), "VTHH_PPTinhGiaXKho"));
                        if (ultraOptionSet1.CheckedIndex == 0)
                        {
                            UpdateQueries.Add(string.Format(updateString, "Phương pháp khấu trừ", "PPTTGTGT"));
                            UpdateQueries.Add(string.Format(updateString, cbbNhomHHDV.Value.ToString().Trim(), "VTHH_NhomHHDV"));
                        }
                        else
                        {
                            UpdateQueries.Add(string.Format(updateString, "Phương pháp trực tiếp trên doanh thu", "PPTTGTGT"));
                            UpdateQueries.Add(string.Format(updateString, cbbNhomNNMD.Value.ToString().Trim(), "VTHH_NhomNNMD"));
                        }
                        //UpdateQueries.Add(string.Format(updateString, txtBankAccount.Text.Trim(), "TTDN_SoTKNH"));
                        //UpdateQueries.Add(string.Format(updateString, txtBankName.Text.Trim(), "TTDN_Nganhang"));
                        //UpdateQueries.Add(string.Format(updateString, txtCompanyDirector.Text.Trim(), "NKY_Giamdoc"));
                        //UpdateQueries.Add(string.Format(updateString, txtCompanyChiefAccountant.Text.Trim(), "NKY_Ketoantruong"));
                        //UpdateQueries.Add(string.Format(updateString, txtCompanyCashier.Text.Trim(), "NKY_Thuquy"));
                        //UpdateQueries.Add(string.Format(updateString, txtCompanyStoreKeeper.Text.Trim(), "NKY_Thukho"));
                        //UpdateQueries.Add(string.Format(updateString, txtCompanyReportPreparer.Text.Trim(), "NKY_Nguoilapbieu"));
                        //UpdateQueries.Add(string.Format(updateString, (chkIsPrintTitleReport.CheckState == CheckState.Checked) ? "1" : "0", "NKY_Inten"));
                        UpdateQueries.Add(string.Format(updateString, dteDBStartYear.DateTime.ToString("dd'/'MM'/'yyyy"), "TCKHAC_NgayBDNamTC"));

                        var dte = dteDBStartYear.DateTime.AddDays(-1);
                        UpdateQueries.Add(string.Format(updateString, dte.ToString("dd'/'MM'/'yyyy"), "DBDateClosed"));

                        UpdateQueries.Add(string.Format(updateString, txtYear.Text.Trim(), "TCKHAC_NamTC"));
                        UpdateQueries.Add(string.Format(updateString, DateTime.Now.ToString("dd'/'MM'/'yyyy"), "NgayHachToan"));
                        UpdateQueries.Add(string.Format(updateString, dteDBStartDate.DateTime.ToString("dd'/'MM'/'yyyy"), "ApplicationStartDate"));

                        foreach (var query in UpdateQueries)
                        {
                            try
                            {
                                new SqlCommand(query, con, transactions).ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                WaitingFrm.StopWaiting();
                                MSG.Warning(query + "\r\n" + ex.ToString());
                                continue;
                            }

                        }
                        transactions.Commit();

                    }
                    catch (SqlException sqlError)
                    {
                        WaitingFrm.StopWaiting();
                        MSG.Warning(sqlError.ToString());
                    }
                }
                catch (Exceptions ex)
                {
                    WaitingFrm.StopWaiting();
                    MSG.Warning("Có lỗi xảy ra khi khởi tạo dữ liệu mới.");
                }
                Properties.Settings.Default.ServerName = server;
                Properties.Settings.Default.DatabaseName = database;
                Properties.Settings.Default.UserName = user;
                Properties.Settings.Default.Password = pass;
                Properties.Settings.Default.UserApp = "ADMIN";
                Properties.Settings.Default.Save();

                // save company info
                //string CustomerFilePath = string.Format("{0}\\customer.xml", path).Replace("file:\\", "");
                //Company objectT = new Company();
                //objectT.CompanyTaxCode = txtCompanyTaxcode.Text.Trim();
                //objectT.CompanyName = txtCompanyName.Text.Trim();
                //objectT.ServerName = server;
                //objectT.Database = database;
                //objectT.Username = user;
                //objectT.Password = pass;
                //objectT.AppUser = "ADMIN";
                //XmlToObject.SaveToFile<Company>(CustomerFilePath, objectT, "Companies");

                WaitingFrm.StopWaiting();
                DialogResult = DialogResult.OK;
                Close();
            }

            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            WaitingFrm.StopWaiting();
        }

        private void btnBrowserFilder_Click(object sender, EventArgs e)
        {
            if (fbrDialog.ShowDialog(this) == DialogResult.OK)
            {
                this.txtDataPatch.Text = fbrDialog.SelectedPath;
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtEmailForgotPass.Text))
            {
                MSG.Warning("Bạn nên nhập email để đặt lại mật khẩu đăng nhập tài khoản quản trị khi quên mật khẩu.");
            }
            tabControl.PerformAction(Infragistics.Win.UltraWinTabControl.UltraTabControlAction.SelectNextTab);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            tabControl.PerformAction(Infragistics.Win.UltraWinTabControl.UltraTabControlAction.SelectPreviousTab);
        }

        private void tabControl_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            btnNext.Enabled = tabControl.ActiveTab.Index != (tabControl.Tabs.Count - 1);
            btnBack.Enabled = tabControl.ActiveTab.Index != 0;
            btnOk.Enabled = tabControl.ActiveTab.Index == (tabControl.Tabs.Count - 1);
            switch (tabControl.ActiveTab.Index)
            {
                case 0: cbbServers.Focus(); break;
                case 1: txtCompanyName.Focus(); break;
                case 2: dteDBStartYear.Focus(); break;
                default:
                    break;
            }
        }

        private void cbbServers_BeforeDropDown(object sender, CancelEventArgs e)
        {
            var combo = (Infragistics.Win.UltraWinEditors.UltraComboEditor)sender;
            //if (_servers == null)
            _servers = SqlDataSourceEnumerator.Instance.GetDataSources();
            combo.Items.Clear();
            for (int i = 0; i < _servers.Rows.Count; i++)
            {
                //if (myServer == servers.Rows[i]["ServerName"].ToString()) ///// used to get the servers in the local machine////
                //{
                if ((_servers.Rows[i]["InstanceName"] as string) != null)
                    combo.Items.Add(string.Format("{0}\\{1}", _servers.Rows[i]["ServerName"], _servers.Rows[i]["InstanceName"]));
                else
                    combo.Items.Add(_servers.Rows[i]["ServerName"]);
                //}
            }
        }

        private void btnSelectOldDB_Click(object sender, EventArgs e)
        {
            if (!popContainer.IsDisplayed)
                popContainer.Show(btnApplyServerOldDB);
        }

        private void btnApplyServerOldDB_Click(object sender, EventArgs e)
        {
            serverOldDB = cbbServerOldDB.Value.ToString();
            userOldDB = txtUsrServerOldDB.Text.Trim();
            passOldDB = txtPwdServerOldDB.Text.Trim();
            popContainer.Close();
        }

        private void popContainer_Opening(object sender, CancelEventArgs e)
        {
            cbbServerOldDB.Text = serverOldDB;
            txtUsrServerOldDB.Value = userOldDB;
            txtPwdServerOldDB.Value = passOldDB;
        }
        private void cbbLastDatabase_BeforeDropDown(object sender, CancelEventArgs e)
        {
            //    if (string.IsNullOrEmpty(serverOldDB)) return;
            //    string strError = string.Empty;
            //    try
            //    {
            //        cbbLastDatabase.Items.Clear();
            //        string serverName = serverOldDB;
            //        string connString = string.Format("Data Source={0};initial catalog=master;{1}", serverName, (userOldDB.ToUpper().Equals("ADMIN") && string.IsNullOrEmpty(passOldDB) ? "Integrated Security=SSPI" : string.Format("User Id={0};Password={1};", userOldDB, passOldDB)));
            //        //
            //        using (var con = new SqlConnection(connString))
            //        {
            //            using (var da = new SqlDataAdapter("SELECT Name FROM master.sys.databases where Name!='master' AND Name!='tempdb' AND Name!='model' AND Name!='msdb'", con))
            //            {
            //                var ds = new DataSet();
            //                da.Fill(ds);
            //                //cbbDatabases.DataSource = ds.Tables[0].Rows[0];
            //                if (ds.Tables[0].Rows.Count == 0) goto Error;
            //                foreach (DataRow row in ds.Tables[0].Rows)
            //                {
            //                    cbbLastDatabase.Items.Add(row["Name"]);
            //                }
            //                //cbbDatabases.DataBind();
            //                //...
            //            }
            //        }
            //        goto End;
            //    }
            //    catch (Exception ex)
            //    {
            //        strError = ex.ToString();
            //        goto Error;
            //    }
            //Error:
            //    strError = string.Format("{0}{1}", resSystem.MSG_System_55,
            //                             !string.IsNullOrEmpty(strError)
            //                                 ? string.Format("\r\n Chi tiết lỗi : \r\n {0}", strError)
            //                                 : "");
            //    MSG.Warning(strError);
            //End:
            //    return;
        }

        private void chkDeleteOldDB_CheckStateChanged(object sender, EventArgs e)
        {
            var check = (Infragistics.Win.UltraWinEditors.UltraCheckEditor)sender;
            if (check.CheckState == CheckState.Checked)
            {
                if (MSG.MessageBoxStand("Bạn có thực sự muốn xóa dữ liệu kế toán năm trước không?\r\nLưu ý: Dữ liệu kế toán năm trước sau khi xóa sẽ không thể lấy lại được.", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    check.CheckState = CheckState.Unchecked;
            }
        }
        //protected override bool ProcessCmdKey(ref Message message, Keys keys)
        //{
        //    switch (keys)
        //    {
        //        case Keys.Enter:
        //            SendKeys.Send("{TAB}");
        //            break;
        //    }
        //    return base.ProcessCmdKey(ref message, keys);
        //}

        private void tabControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left || e.KeyCode == Keys.Right)
                e.Handled = true;
        }

        private void txtYear_ValueChanged(object sender, EventArgs e)
        {
            DateTime dateTime = dteDBStartYear.DateTime;
            DateTime dateTime1 = dteDBStartDate.DateTime;
            dteDBStartYear.DateTime = new DateTime(txtYear.Value.ToInt(), dateTime.Month, dateTime.Day);
            dteDBStartDate.DateTime = new DateTime(txtYear.Value.ToInt(), dateTime1.Month, dateTime1.Day);
        }

        private void dteDBStartYear_ValueChanged(object sender, EventArgs e)
        {
            //txtYear.Value = dteDBStartYear.DateTime.Year;
        }

        private void dteDBStartYear_AfterExitEditMode(object sender, EventArgs e)
        {
            txtYear.Value = dteDBStartYear.DateTime.Year;
        }

        private void ultraOptionSet1_ValueChanged(object sender, EventArgs e)
        {
            if (ultraOptionSet1.CheckedIndex == 0)
            {
                cbbNhomHHDV.Enabled = true;
                cbbNhomNNMD.Enabled = false;
            }
            else
            {
                cbbNhomHHDV.Enabled = false;
                cbbNhomNNMD.Enabled = true;
            }
        }
        private void txtEmail_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtEmail.Text)) return;
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (!re.IsMatch(txtEmail.Text.Trim()))
            {
                MSG.Error("Sai định dạng Email");
                txtEmail.Focus();
            }
        }

        private void txtEmailForgotPass_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtEmailForgotPass.Text)) return;
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (!re.IsMatch(txtEmailForgotPass.Text.Trim()))
            {
                MSG.Error("Sai định dạng Email");
                txtEmailForgotPass.Focus();
            }
        }
    }
}