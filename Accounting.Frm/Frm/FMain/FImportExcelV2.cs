﻿using Accounting.Core.Domain;
using Accounting.Core.Domain.ControlsMapping.ConfigImport;
using Accounting.Core.IService;
using Accounting.TextMessage;
using ClosedXML.Excel;
using ExcelDataReader;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FMain
{
    public partial class FImportExcelV2 : Form
    {
        string path = "";
        DataSet ds;
        //Dictionary<string, string> list_col_template;
        List<COLUMN_MODEL> list_config;
        private readonly IAccountingObjectService _IAccountingObjectService;
        private IMBTellerPaperService _IMBTellerPaperService;
        private IMCReceiptService _IMCReceiptService;
        private IMCPaymentService _IMCPaymentService;
        private IMBDepositService _IMBDepositService;
        private IMBCreditCardService _IMBCreditCardService;
        private ICurrencyService _ICurrencyService;
        private ILog log = LogManager.GetLogger(typeof(FImportExcelV2));
        List<object> data_ip = new List<object>();
        List<object> data_import = new List<object>();
        object obj_ip = new object();
        XLWorkbook wb;
        IXLWorksheet ws;
        public FImportExcelV2()
        {
            InitializeComponent();
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            _IMBDepositService = IoC.Resolve<IMBDepositService>();
            _IMCReceiptService = IoC.Resolve<IMCReceiptService>();
            _IMCPaymentService = IoC.Resolve<IMCPaymentService>();
            _IMBTellerPaperService = IoC.Resolve<IMBTellerPaperService>();
            _IMBCreditCardService = IoC.Resolve<IMBCreditCardService>();
            _ICurrencyService = IoC.Resolve<ICurrencyService>();
            GetListCB();
        }
        private void btn_Browser_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog() { Filter = "Excel Workbook|*.xlsx|Excel Workbook 97-2003|*.xls", ValidateNames = true })
            {                
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {                      
                        using (var stream = File.Open(ofd.FileName, FileMode.Open, FileAccess.Read))
                        {
                            lblFileName.Value = ofd.FileName;

                            IExcelDataReader reader;
                            if (ofd.FilterIndex == 2)
                            {
                                reader = ExcelReaderFactory.CreateBinaryReader(stream);
                            }
                            else
                            {
                                reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                            }
                            WaitingFrm.StartWaiting();
                            ds = reader.AsDataSet(new ExcelDataSetConfiguration()
                            {
                                ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                                {
                                    UseHeaderRow = true
                                }
                            });

                            //cb_sheet.DataSource = null;
                            Dictionary<string, string> lstSheet = new Dictionary<string, string>();
                            foreach (DataTable dt in ds.Tables)
                            {
                                lstSheet.Add(dt.TableName, dt.TableName);
                            }
                            cb_sheet.DataSource = lstSheet;
                            cb_sheet.DisplayMember = "Key";
                            cb_sheet.ValueMember = "Value";
                            Utils.ConfigGrid(cb_sheet, "");
                            cb_sheet.DisplayLayout.Bands[0].Columns[0].Header.Caption = "Tên sheet";
                            cb_sheet.DisplayLayout.Bands[0].Columns[1].Hidden = true;
                            cb_sheet.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                            cb_sheet.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.StartsWith;
                            reader.Close();
                            WaitingFrm.StopWaiting();
                        }                       
                    }
                    catch (Exception ex)
                    {
                        WaitingFrm.StopWaiting();
                        MSG.Error("File này đang được mở bởi ứng dụng khác");
                        return;
                    }
                    ChooseFile(lblFileName.Text);
                }
            }
        }
        private void GetListCB()
        {
            Dictionary<string, string> lstPhanHe = new Dictionary<string, string>()
            {
                {"Tiền mặt và ngân hàng","TMNH"},
                {"Mua hàng","MH"},
                {"Bán hàng","BH"},
                //{ "Kho","KHO"},
                //{"CCDC","CCDC"},
                //{"TSCĐ","TSCD"},
                //{"Hợp đồng","HOPDONG"},
                //{"Tổng hợp","TONGHOP"},
                //{"Tiền lương","TIENLUONG"},
                //{"Hóa đơn","HOADON"},
                //{"Giá thành","GIATHANH"},
                //{"Hóa đơn điện tử","HDDT"},
                //{"Thuế","THUE"},
                //{"Danh mục","DANHMUC"}
            };

            cbbPhanHe.DataSource = lstPhanHe;
            cbbPhanHe.DisplayMember = "Key";
            cbbPhanHe.ValueMember = "Value";
            Utils.ConfigGrid(cbbPhanHe, "");
            cbbPhanHe.DisplayLayout.Bands[0].Columns[0].Header.Caption = "Tên phân hệ";
            cbbPhanHe.DisplayLayout.Bands[0].Columns[1].Hidden = true;
            cbbPhanHe.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            cbbPhanHe.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.StartsWith;
        }
        private Dictionary<string, string> GetDanhMuc(string key_ph)
        {
            Dictionary<string, string> lstDanhMuc = new Dictionary<string, string>();
            switch (key_ph)
            {
                case "TMNH":
                    lstDanhMuc = new Dictionary<string, string>()
            {
                {"Phiếu thu","PHIEUTHU"},
                {"Phiếu chi","PHIEUCHI"},
                {"Séc/UNC","SECUNC"},
                { "Thẻ tín dụng","THETINDUNG"},
                {"Thu qua ngân hàng","THUNGANHANG"},
                {"Chuyển tiền nội bộ","CHUYENTIENNOIBO"},
                {"Kiểm kê quỹ","KIEMKEQUY"}
            };
                    break;
                case "MH":
                    lstDanhMuc = new Dictionary<string, string>()
            {
                {"Đơn mua hàng","DONMUAHANG"},
                {"Mua hàng qua kho","MUAHANGQUAKHO"},
                {"Mua hàng không qua kho","MUAHANGKHONGQUAKHO"},
                { "Mua dịch vụ","MUADICHVU"},
                //{"Hàng mua trả lại","HANGMUATRALAI"},
                //{"Hàng mua giảm giá","HANGMUAGIAMGIA"},
                //{"Trả tiền nhà cung cấp","TRATIENNCC"},
            };
                    break;
                case "BH":
                    lstDanhMuc = new Dictionary<string, string>()
            {
                {"Báo giá","BAOGIA"},
                {"Đơn đặt hàng","DONDATHANG"},
                {"Bán hàng chưa thu tiền","BANHANGCHUATHUTIEN"},
                { "Bán hàng thu tiền ngay","BANHANGTHUTIENNGAY"},
                {"Bán hàng đại lý bán đúng giá, nhận ủy thác XNK","BHDLBANDUNGGIA"},
                //{"Xuất hóa đơn","XUATHOADON"},
                //{"Hàng bán trả lại","HANGBANTRALAI"},
                //{"Hàng bán giảm giá","HANGBANGIAMGIA"},
                //{"Thu tiền khách hàng","THUTIENKHACHHANG"},
                //{"Tính lãi nợ","TINHLAINO"}
            };
                    break;
                //    case "KHO":
                //        lstDanhMuc = new Dictionary<string, string>()
                //{
                //    {"Nhập kho","NHAPKHO"},
                //    {"Xuất kho","XUATKHO"},
                //    {"Lệnh lắp ráp, tháo dỡ","LENHLAPRAPTHAODO"},
                //    { "Chuyển kho","CHUYENKHO"},
                //    {"Điều chỉnh tồn kho","DIEUCHINHTONKHO"}
                //};
                //        break;
                //    case "CCDC":
                //        lstDanhMuc = new Dictionary<string, string>()
                //{
                //    {"Ghi tăng CCDC","GHITANGCCDC"},
                //    {"Ghi tăng khác","GIATANGKHAC"},
                //    {"Điều chỉnh CCDC","DIEUCHINHCCDC"},
                //    {"Điều chuyển CCDC","DIEUCHUYENCCDC"},
                //    {"Kiểm kê CCDC","KIEMKECCDC"},
                //    {"Phân bổ CCDC","PHANBOCCDC"}
                //};
                //        break;
                //    case "TSCD":
                //        lstDanhMuc = new Dictionary<string, string>()
                //{
                //    {"Mua và ghi tăng TSCĐ","MUAGHITANGTCSD"},
                //    {"Ghi tăng TSCĐ khác","GHITANGTSCDKHAC"},
                //    {"Ghi giảm TSCĐ","GHIGIAMTSCD"},
                //    {"Điều chỉnh TSCĐ","DIEUCHINHTSCD"},
                //    {"Tính khấu hao TSCĐ","TINHKHAUHAOTSCD"},
                //    {"Kiểm kê TSCĐ","KIEMKETSCD"},
                //    {"Điều chuyển TSCĐ","DIEUCHUYENTSCD"}
                //};
                //        break;
                //    case "HOPDONG":
                //        lstDanhMuc = new Dictionary<string, string>()
                //{
                //    {"Hợp đồng bán","HOPDONGBAN"},
                //    {"Hợp đồng mua","HOPDONGMUA"}
                //};
                //        break;
                //    case "TONGHOP":
                //        lstDanhMuc = new Dictionary<string, string>()
                //{
                //    {"Chứng từ nghiệp vụ khác","CHUNGTUNGHIEPVUKHAC"},
                //    {"Chi phí trả trước","CHIPHITRATRUOC"},
                //    {"Phân bổ chi phí trả trước","PHANBOCHIPHITRATRUOC"}
                //};
                //        break;
                //    case "TIENLUONG":
                //        lstDanhMuc = new Dictionary<string, string>()
                //{
                //    {"Chấm công","CHAMCONG"},
                //    {"Tổng hợp chấm công","TONGHOPCHAMCONG"},
                //    {"Bảng lương","BANGLUONG"}
                //};
                //        break;
                //    case "HOADON":
                //        lstDanhMuc = new Dictionary<string, string>()
                //{
                //    {"Khởi tạo mẫu hóa đơn","KHOITAOMAUHOADON"}
                //};
                //        break;
                //    case "GIATHANH":
                //        lstDanhMuc = new Dictionary<string, string>()
                //{
                //    {"Định mức NVL","DINHMUCNVL"},
                //    {"Định mức giá thành thành phẩm","DINHMUCGIATHANHTHANHPHAM"},
                //    {"Định mức phân bổ chi phí","DINHMUCPHANBOCHIPHI"},
                //    {"Chi phí dở dang đầu kỳ","CHIPHIDODANGDAUKY"}
                //};
                //        break;
                //    case "HDDT":
                //        lstDanhMuc = new Dictionary<string, string>();
                //        break;
                //    case "THUE":
                //        lstDanhMuc = new Dictionary<string, string>()
                //{
                //    {"Tờ khai thuế GTGT khấu trừ 01/GTGT","TOKHAITHUEGTGTKHAUTRU"},
                //    {"Tờ khai thuế GTGT cho dự án đầu tư 02/GTGT","TOKHAITHUEGTGTCHODUAN"},
                //    {"QT thuế TNDC năm","QTTHUETNDCNAM"},
                //    {"Tờ khai thuế tài nguyên","TOKHAITHUETAINGUYEN"},
                //    {"Tờ khai quyết toán thuế tài nguyên","TOKHAIQUYETTOANTHUETAINGUYEN"},
                //    {"Khấu trừ thuế GTGT","KHAUTRUTHUEGTGT"},
                //    {"Nộp thuế","NOPTHUE"}
                //};
                //        break;
                //    case "DANHMUC":
                //        lstDanhMuc = new Dictionary<string, string>()
                //{
                //    {"Phòng ban","PHONGBAN"},
                //    {"Nhóm khách hàng, nhà cung cấp","NHOMKHACHHANGNHACUNGCAP"},
                //    {"Kho","DANHMUCKHO"},
                //    {"Tài khoản ngân hàng","TAIKHOANNGANHANG"},
                //    {"Thẻ tín dụng","DMTHETINDUNG"},
                //    {"Đối tượng tập hợp CP","DOITUONGTAPHOPCP"},
                //    {"Khoản mục CP","KHOANMUCCP"},
                //    {"Loại VTHH","LOAIVTHH"},
                //    {"Định mức NVL","DMDINHMUCNVL"},
                //    {"Mã thống kê","MATHONGKE"},
                //    {"Điều khoản thanh toán","DIEUKHOANTHANHTOAN"},
                //    {"Phương thức vận chuyển","PHUONGTHUCVANCHUYEN"},
                //    {"Mục thu/chi","MUCTHUCHI"},
                //    {"Nhóm giá bán","NHOMGIABAN"}
                //};
                //        break;
                default:
                    lstDanhMuc = new Dictionary<string, string>();
                    break;
            }
            return lstDanhMuc;
        }
        public void ChooseFile(string path)
        {
            this.path = path;
            wb = new XLWorkbook(@path);
        }
        //Chose Sheet
        public void ChooseSheet(string sheet)
        {
            ws = wb.Worksheet(sheet);
        }
        private void cbbDanhMuc_RowSelected(object sender, RowSelectedEventArgs e)
        {
            lblFileName.Value = null;
            cb_sheet.DataSource = null;
            if (cbbDanhMuc.Value != null)
            {
                FileDemo.Text = cbbDanhMuc.Text;
                list_config = GetListColumn(cbbDanhMuc.Value.ToString());
            }
        }
        private void cbbPhanHe_RowSelected(object sender, RowSelectedEventArgs e)
        {
            lblFileName.Value = null;
            cb_sheet.DataSource = null;
            if (cbbPhanHe.Value != null)
            {
                var list = GetDanhMuc(cbbPhanHe.Value.ToString());
                cbbDanhMuc.DataSource = list;
                cbbDanhMuc.DisplayMember = "Key";
                cbbDanhMuc.ValueMember = "Value";
                Utils.ConfigGrid(cbbDanhMuc, "");
                cbbDanhMuc.DisplayLayout.Bands[0].Columns[0].Header.Caption = "Tên danh mục";
                cbbDanhMuc.DisplayLayout.Bands[0].Columns[1].Hidden = true;
                cbbDanhMuc.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                cbbDanhMuc.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.StartsWith;
            }

        }
        private void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                var dm_chon = cbbDanhMuc.Value;
                if (dm_chon != null)
                {
                    if (lblFileName.Value == null)
                    {
                        MSG.Warning("Bạn chưa chọn sheet cần import!");
                        return;
                    }
                    if (cb_sheet.Value == null)
                    {
                        MSG.Warning("Bạn chưa chọn sheet cần import!");
                        return;
                    }
                    if (!string.IsNullOrEmpty(dm_chon.ToString()))
                    {
                        WaitingFrm.StartWaiting();
                        var checkFile = CheckExcel(cbbDanhMuc.Value.ToString());
                        if (checkFile != "Success")
                        {
                            WaitingFrm.StopWaiting();
                            if (checkFile == "ErrThuTu")
                            {
                                WaitingFrm.StopWaiting();
                                MSG.Warning("Thứ tự cột trong file excel không khớp với file mẫu!");

                                return;
                            }
                            if (checkFile == "Exeption")
                            {
                                WaitingFrm.StopWaiting();
                                MSG.Warning("Có lỗi trong quá trình xử lý dữ liệu!");
                                return;
                            }
                            if (checkFile == "ErrData")
                            {
                                WaitingFrm.StopWaiting();
                                ShowThongbao();
                                return;

                            }
                        }
                        switch (dm_chon)
                        {
                            case "PHIEUTHU":
                                {
                                    ConfigTMNH cf = new ConfigTMNH();
                                    data_ip = cf.SetDataPhieuThu(data_import);
                                    _IMCReceiptService.BeginTran();
                                    try
                                    {
                                        List<MCReceipt> list = data_ip.Cast<MCReceipt>().ToList();
                                        foreach (var item in list)
                                        {
                                            item.ID = Guid.NewGuid();
                                            item.TypeID = 100;
                                            item.Recorded = false;
                                            foreach (var item2 in item.MCReceiptDetails)
                                            {
                                                item2.MCReceiptID = item.ID;
                                            }
                                            _IMCReceiptService.CreateNew(item);
                                        }
                                        _IMCReceiptService.CommitTran();
                                        WaitingFrm.StopWaiting();
                                        MSG.Information("Import dữ liệu thành công");
                                        List<MCReceipt_Model> list_PT = new List<MCReceipt_Model>();
                                        SetNullGrid(list_PT.Cast<object>().ToList());
                                        return;
                                    }
                                    catch (Exception ex)
                                    {
                                        WaitingFrm.StopWaiting();
                                        MSG.Error("Có lỗi trong quá trình import dữ liệu");
                                        _IMCReceiptService.RolbackTran();
                                        return;
                                    }

                                }

                            case "PHIEUCHI":
                                {
                                    ConfigTMNH cf = new ConfigTMNH();
                                    data_ip = cf.SetDataPhieuChi(data_import);
                                    _IMCPaymentService.BeginTran();
                                    try
                                    {
                                        List<MCPayment> list = data_ip.Cast<MCPayment>().ToList();
                                        foreach (var item in list)
                                        {
                                            item.ID = Guid.NewGuid();
                                            item.Recorded = false;
                                            item.TypeID = 110;
                                            foreach (var item2 in item.MCPaymentDetails)
                                            {
                                                item2.MCPaymentID = item.ID;
                                            }
                                            foreach (var item3 in item.MCPaymentDetailTaxs)
                                            {
                                                item3.MCPaymentID = item.ID;
                                            }
                                            _IMCPaymentService.CreateNew(item);
                                        }
                                        _IMCPaymentService.CommitTran();
                                        WaitingFrm.StopWaiting();
                                        MSG.Information("Import dữ liệu thành công");
                                        List<MCPayment_Model> listPC1 = new List<MCPayment_Model>();
                                        SetNullGrid(listPC1.Cast<object>().ToList());
                                        return;
                                    }
                                    catch (Exception ex)
                                    {
                                        WaitingFrm.StopWaiting();
                                        MSG.Error("Có lỗi trong quá trình import dữ liệu");
                                        _IMCPaymentService.RolbackTran();
                                        return;
                                    }

                                }

                            case "SECUNC":
                                {
                                    ConfigTMNH cf = new ConfigTMNH();
                                    data_ip = cf.SetDataSecUNC(data_import);
                                    _IMBTellerPaperService.BeginTran();
                                    try
                                    {
                                        List<MBTellerPaper> list = data_ip.Cast<MBTellerPaper>().ToList();
                                        foreach (var item in list)
                                        {
                                            item.ID = Guid.NewGuid();
                                            item.Recorded = false;
                                            foreach (var item2 in item.MBTellerPaperDetails)
                                            {
                                                item2.MBTellerPaperID = item.ID;
                                            }
                                            foreach (var item3 in item.MBTellerPaperDetailTaxs)
                                            {
                                                item3.MBTellerPaperID = item.ID;
                                            }
                                            _IMBTellerPaperService.CreateNew(item);
                                        }
                                        _IMBTellerPaperService.CommitTran();
                                        WaitingFrm.StopWaiting();
                                        MSG.Information("Import dữ liệu thành công");
                                        List<MBTellerPaper_Model> listSec = new List<MBTellerPaper_Model>();
                                        SetNullGrid(listSec.Cast<object>().ToList());
                                        return;
                                    }
                                    catch (Exception ex)
                                    {
                                        WaitingFrm.StopWaiting();
                                        MSG.Error("Có lỗi trong quá trình import dữ liệu");
                                        _IMBTellerPaperService.RolbackTran();
                                        return;
                                    }

                                }

                            case "THUNGANHANG":
                                {
                                    ConfigTMNH cf = new ConfigTMNH();
                                    data_ip = cf.SetDataThuNganHang(data_import);
                                    _IMBDepositService.BeginTran();
                                    try
                                    {
                                        List<MBDeposit> list = data_ip.Cast<MBDeposit>().ToList();
                                        foreach (var item in list)
                                        {
                                            item.ID = Guid.NewGuid();
                                            item.Recorded = false;
                                            foreach (var item2 in item.MBDepositDetails)
                                            {
                                                item2.MBDepositID = item.ID;
                                            }
                                            _IMBDepositService.CreateNew(item);
                                        }
                                        _IMBDepositService.CommitTran();
                                        WaitingFrm.StopWaiting();
                                        MSG.Information("Import dữ liệu thành công");
                                        List<MBDeposit_Model> listSec = new List<MBDeposit_Model>();
                                        SetNullGrid(listSec.Cast<object>().ToList());
                                        return;
                                    }
                                    catch (Exception ex)
                                    {
                                        WaitingFrm.StopWaiting();
                                        MSG.Error("Có lỗi trong quá trình import dữ liệu");
                                        _IMBDepositService.RolbackTran();
                                        return;
                                    }

                                }

                            case "THETINDUNG":
                                {
                                    ConfigTMNH cf = new ConfigTMNH();
                                    data_ip = cf.SetDataTheTinDung(data_import);
                                    _IMBCreditCardService.BeginTran();
                                    try
                                    {
                                        List<MBCreditCard> list = data_ip.Cast<MBCreditCard>().ToList();
                                        foreach (var item in list)
                                        {
                                            item.ID = Guid.NewGuid();
                                            item.Recorded = false;
                                            foreach (var item2 in item.MBCreditCardDetails)
                                            {
                                                item2.MBCreditCardID = item.ID;
                                            }
                                            foreach (var item3 in item.MBCreditCardDetailTaxs)
                                            {
                                                item3.MBCreditCardID = item.ID;
                                            }
                                            _IMBCreditCardService.CreateNew(item);
                                        }
                                        _IMBCreditCardService.CommitTran();
                                        WaitingFrm.StopWaiting();
                                        MSG.Information("Import dữ liệu thành công");
                                        List<MBCreditCard_Model> listSec = new List<MBCreditCard_Model>();
                                        SetNullGrid(listSec.Cast<object>().ToList());
                                        return;
                                    }
                                    catch (Exception ex)
                                    {
                                        WaitingFrm.StopWaiting();
                                        MSG.Error("Có lỗi trong quá trình import dữ liệu");
                                        _IMBCreditCardService.RolbackTran();
                                        return;
                                    }

                                }

                            case "CHUYENTIENNOIBO":
                                {
                                    ConfigTMNH cf = new ConfigTMNH();
                                    data_ip = cf.SetDataChuyenTienNB(data_import);
                                    IMBInternalTransferService _IMBInternalTransferService = IoC.Resolve<IMBInternalTransferService>();
                                    _IMBInternalTransferService.BeginTran();
                                    try
                                    {
                                        List<MBInternalTransfer> list = data_ip.Cast<MBInternalTransfer>().ToList();
                                        foreach (var item in list)
                                        {
                                            item.ID = Guid.NewGuid();
                                            item.Recorded = false;
                                            foreach (var item2 in item.MBInternalTransferDetails)
                                            {
                                                item2.MBInternalTransferID = item.ID;
                                            }
                                            _IMBInternalTransferService.CreateNew(item);
                                        }
                                        _IMBInternalTransferService.CommitTran();
                                        WaitingFrm.StopWaiting();
                                        MSG.Information("Import dữ liệu thành công");
                                        List<MBInternalTransfer_Model> listSec = new List<MBInternalTransfer_Model>();
                                        SetNullGrid(listSec.Cast<object>().ToList());
                                        return;
                                    }
                                    catch (Exception ex)
                                    {
                                        WaitingFrm.StopWaiting();
                                        MSG.Error("Có lỗi trong quá trình import dữ liệu");
                                        _IMBInternalTransferService.RolbackTran();
                                        return;
                                    }

                                }

                            case "KIEMKEQUY":
                                {
                                    ConfigTMNH cf = new ConfigTMNH();
                                    data_ip = cf.SetDataKiẹmkequy(data_import);
                                    IMCAuditService _IMCAuditService = IoC.Resolve<IMCAuditService>();
                                    ITemplateService _ITemplateService = IoC.Resolve<ITemplateService>();
                                    _IMCAuditService.BeginTran();
                                    try
                                    {
                                        List<MCAudit> list = data_ip.Cast<MCAudit>().ToList();
                                        foreach (var item in list)
                                        {
                                            item.ID = Guid.NewGuid();
                                            item.TemplateID = _ITemplateService.Query.FirstOrDefault(t => t.TypeID == item.TypeID && t.TemplateName == "Mẫu chuẩn").ID;
                                            foreach (var item2 in item.MCAuditDetails)
                                            {
                                                item2.MCAuditID = item.ID;
                                            }
                                            foreach (var item3 in item.MCAuditDetailMembers)
                                            {
                                                item3.MCAuditID = item.ID;
                                            }
                                            _IMCAuditService.CreateNew(item);
                                        }
                                        _IMCAuditService.CommitTran();
                                        WaitingFrm.StopWaiting();
                                        MSG.Information("Import dữ liệu thành công");
                                        List<MCAudit_Model> listSec = new List<MCAudit_Model>();
                                        SetNullGrid(listSec.Cast<object>().ToList());
                                        return;
                                    }
                                    catch (Exception ex)
                                    {
                                        WaitingFrm.StopWaiting();
                                        MSG.Error("Có lỗi trong quá trình import dữ liệu");
                                        _IMCAuditService.RolbackTran();
                                        return;
                                    }

                                }

                            case "BAOGIA":
                                {
                                    ConfigBanHang cf = new ConfigBanHang();
                                    data_ip = cf.SetDataBaoGia(data_import);
                                    ISAQuoteService _ISAQuoteService = IoC.Resolve<ISAQuoteService>();
                                    _ISAQuoteService.BeginTran();
                                    try
                                    {
                                        List<SAQuote> list = data_ip.Cast<SAQuote>().ToList();
                                        foreach (var item in list)
                                        {
                                            item.ID = Guid.NewGuid();
                                            foreach (var item2 in item.SAQuoteDetails)
                                            {
                                                item2.SAQuoteID = item.ID;
                                            }
                                            _ISAQuoteService.CreateNew(item);
                                        }
                                        _ISAQuoteService.CommitTran();
                                        WaitingFrm.StopWaiting();
                                        List<SAQuote_Model> list_baogia = new List<SAQuote_Model>();
                                        SetNullGrid(list_baogia.Cast<object>().ToList());
                                        MSG.Information("Import dữ liệu thành công");
                                        return;
                                    }
                                    catch (Exception ex)
                                    {
                                        WaitingFrm.StopWaiting();
                                        MSG.Error("Có lỗi trong quá trình import dữ liệu");
                                        _ISAQuoteService.RolbackTran();
                                        return;
                                    }

                                }

                            case "DONDATHANG":
                                {
                                    ConfigBanHang cf = new ConfigBanHang();
                                    data_ip = cf.SetDataDonDatHang(data_import);
                                    ISAOrderService _ISAOrderService = IoC.Resolve<ISAOrderService>();
                                    _ISAOrderService.BeginTran();
                                    try
                                    {
                                        List<SAOrder> list = data_ip.Cast<SAOrder>().ToList();
                                        foreach (var item in list)
                                        {
                                            item.ID = Guid.NewGuid();
                                            foreach (var item2 in item.SAOrderDetails)
                                            {
                                                item2.SAOrderID = item.ID;
                                            }
                                            _ISAOrderService.CreateNew(item);
                                        }
                                        _ISAOrderService.CommitTran();
                                        WaitingFrm.StopWaiting();
                                        MSG.Information("Import dữ liệu thành công");
                                        List<SAOrder_Model> listSec = new List<SAOrder_Model>();
                                        SetNullGrid(listSec.Cast<object>().ToList());
                                        return;
                                    }
                                    catch (Exception ex)
                                    {
                                        WaitingFrm.StopWaiting();
                                        MSG.Error("Có lỗi trong quá trình import dữ liệu");
                                        _ISAOrderService.RolbackTran();
                                        return;
                                    }

                                }

                            case "BANHANGCHUATHUTIEN":
                                {
                                    ConfigBanHang cf = new ConfigBanHang();
                                    obj_ip = cf.SetDataBHChuaThuTien(data_import);
                                    ISAInvoiceService _ISAInvoiceService = IoC.Resolve<ISAInvoiceService>();
                                    IRSInwardOutwardService _IRSInwardOutwardService = IoC.Resolve<IRSInwardOutwardService>();
                                    _ISAInvoiceService.BeginTran();
                                    try
                                    {
                                        BH_ThuNgay_model model = obj_ip as BH_ThuNgay_model;
                                        List<SAInvoice> list = model.listSAInvoice;
                                        List<RSInwardOutward> listRS = model.list_RSOut;
                                        foreach (var item in list)
                                        {
                                            item.ID = Guid.NewGuid();
                                            foreach (var item2 in item.SAInvoiceDetails)
                                            {
                                                item2.SAInvoiceID = item.ID;
                                            }
                                            _ISAInvoiceService.CreateNew(item);
                                            var rs = listRS.FirstOrDefault(t => t.No == item.OutwardNo);
                                            if (rs != null)
                                            {
                                                rs.ID = item.ID;
                                                _IRSInwardOutwardService.CreateNew(rs);
                                            }
                                        }
                                        _ISAInvoiceService.CommitTran();
                                        WaitingFrm.StopWaiting();
                                        MSG.Information("Import dữ liệu thành công");
                                        List<SAInvoice_Model> listSec = new List<SAInvoice_Model>();
                                        SetNullGrid(listSec.Cast<object>().ToList());
                                        return;
                                    }
                                    catch (Exception ex)
                                    {
                                        WaitingFrm.StopWaiting();
                                        MSG.Error("Có lỗi trong quá trình import dữ liệu");
                                        _ISAInvoiceService.RolbackTran();
                                        return;
                                    }

                                }

                            case "BANHANGTHUTIENNGAY":
                                {
                                    ConfigBanHang cf = new ConfigBanHang();
                                    obj_ip = cf.SetDataBHThuTienNgay(data_import);
                                    ISAInvoiceService _ISAInvoiceService = IoC.Resolve<ISAInvoiceService>();
                                    IMCReceiptService _IMCReceiptService = IoC.Resolve<IMCReceiptService>();
                                    IMBDepositService _IMBDepositService = IoC.Resolve<IMBDepositService>();
                                    IRSInwardOutwardService _IRSInwardOutwardService = IoC.Resolve<IRSInwardOutwardService>();
                                    _ISAInvoiceService.BeginTran();
                                    try
                                    {
                                        BH_ThuNgay_model model = obj_ip as BH_ThuNgay_model;
                                        List<SAInvoice> list = model.listSAInvoice;
                                        var list_Mno_goc = list.Select(t => t.MNo).ToList();
                                        var list_Mno = list_Mno_goc.Distinct().ToList();
                                        var list_no = from a in list_Mno
                                                      select new
                                                      {
                                                          MNo = a,
                                                          Count_No = CountOccurenceOfValue2(list_Mno_goc, a)
                                                      };
                                        var list_trung = list_no.Where(t => t.Count_No > 1);
                                        if (list_trung.Count() > 0)
                                        {
                                            var list_check = list_trung.Select(t => t.MNo).ToList();
                                            var checkFile2 = CheckNoChungTu(list_check, "Số chứng từ thanh toán");
                                            if (checkFile2 == "ErrData")
                                            {
                                                WaitingFrm.StopWaiting();
                                                ShowThongbao();
                                                return;

                                            }
                                        }

                                        List<MCReceipt> list_MCReceipt = model.listMCReceipt;
                                        var list_no2 = list_MCReceipt.Select(t => t.No);
                                        List<MBDeposit> list_MBDeposit = model.listMBDeposit;
                                        var list_no3 = list_MBDeposit.Select(t => t.No);
                                        List<RSInwardOutward> listRS = model.list_RSOut;
                                        foreach (var item in list)
                                        {
                                            item.ID = Guid.NewGuid();
                                            item.Recorded = false;
                                            foreach (var item2 in item.SAInvoiceDetails)
                                            {
                                                item2.SAInvoiceID = item.ID;
                                            }
                                            _ISAInvoiceService.CreateNew(item);
                                            var itMCReceipt = list_MCReceipt.FirstOrDefault(t => t.No == item.MNo);
                                            if (itMCReceipt != null)
                                            {
                                                itMCReceipt.ID = item.ID;
                                                _IMCReceiptService.CreateNew(itMCReceipt);
                                            }
                                            var itMBDeposit = list_MBDeposit.FirstOrDefault(t => t.No == item.MNo);
                                            if (itMBDeposit != null)
                                            {
                                                itMBDeposit.ID = item.ID;
                                                _IMBDepositService.CreateNew(itMBDeposit);
                                            }
                                            var rs = listRS.FirstOrDefault(t => t.No == item.OutwardNo);
                                            if (rs != null)
                                            {
                                                rs.ID = item.ID;
                                                _IRSInwardOutwardService.CreateNew(rs);
                                            }
                                        }
                                        _ISAInvoiceService.CommitTran();
                                        WaitingFrm.StopWaiting();
                                        MSG.Information("Import dữ liệu thành công");
                                        List<BHTHUTIENNGAY_Model> listBHCHUATHU = new List<BHTHUTIENNGAY_Model>();
                                        SetNullGrid(listBHCHUATHU.Cast<object>().ToList());
                                        return;
                                    }
                                    catch (Exception ex)
                                    {
                                        WaitingFrm.StopWaiting();
                                        MSG.Error("Có lỗi trong quá trình import dữ liệu");
                                        _ISAInvoiceService.RolbackTran();
                                        return;
                                    }
                                }

                            case "BHDLBANDUNGGIA":
                                {
                                    ConfigBanHang cf = new ConfigBanHang();
                                    obj_ip = cf.SetDataBHDaiLy(data_import);
                                    ISAInvoiceService _ISAInvoiceService = IoC.Resolve<ISAInvoiceService>();
                                    IMCReceiptService _IMCReceiptService = IoC.Resolve<IMCReceiptService>();
                                    IMBDepositService _IMBDepositService = IoC.Resolve<IMBDepositService>();
                                    IRSInwardOutwardService _IRSInwardOutwardService = IoC.Resolve<IRSInwardOutwardService>();
                                    _ISAInvoiceService.BeginTran();
                                    try
                                    {
                                        BH_ThuNgay_model model = obj_ip as BH_ThuNgay_model;
                                        List<SAInvoice> list = model.listSAInvoice;
                                        var list_Mno_goc = list.Where(v => v.MNo != null).Select(t => t.MNo).ToList();
                                        var list_Mno = list_Mno_goc.Distinct().ToList();
                                        var list_no = from a in list_Mno
                                                      select new
                                                      {
                                                          MNo = a,
                                                          Count_No = CountOccurenceOfValue2(list_Mno_goc, a)
                                                      };
                                        var list_trung = list_no.Where(t => t.Count_No > 1);
                                        if (list_trung.Count() > 0)
                                        {
                                            var list_check = list_trung.Select(t => t.MNo).ToList();
                                            var checkFile2 = CheckNoChungTu(list_check, "Số chứng từ thanh toán");
                                            if (checkFile2 == "ErrData")
                                            {
                                                WaitingFrm.StopWaiting();
                                                ShowThongbao();
                                                return;

                                            }
                                        }
                                        List<MCReceipt> list_MCReceipt = model.listMCReceipt;
                                        List<MBDeposit> list_MBDeposit = model.listMBDeposit;
                                        List<RSInwardOutward> listRS = model.list_RSOut;
                                        foreach (var item in list)
                                        {
                                            item.ID = Guid.NewGuid();
                                            item.Recorded = false;
                                            foreach (var item2 in item.SAInvoiceDetails)
                                            {
                                                item2.SAInvoiceID = item.ID;
                                            }
                                            _ISAInvoiceService.CreateNew(item);
                                            var itMCReceipt = list_MCReceipt.FirstOrDefault(t => t.No == item.MNo);
                                            if (itMCReceipt != null)
                                            {
                                                itMCReceipt.ID = item.ID;
                                                _IMCReceiptService.CreateNew(itMCReceipt);
                                            }
                                            var itMBDeposit = list_MBDeposit.FirstOrDefault(t => t.No == item.MNo);
                                            if (itMBDeposit != null)
                                            {
                                                itMBDeposit.ID = item.ID;
                                                _IMBDepositService.CreateNew(itMBDeposit);
                                            }
                                            var rs = listRS.FirstOrDefault(t => t.No == item.OutwardNo);
                                            if (rs != null)
                                            {
                                                rs.ID = item.ID;
                                                _IRSInwardOutwardService.CreateNew(rs);
                                            }
                                        }
                                        _ISAInvoiceService.CommitTran();
                                        WaitingFrm.StopWaiting();
                                        MSG.Information("Import dữ liệu thành công");
                                        List<BHDAILYTHUDUNGGIA_Model> listBHDaiLy = new List<BHDAILYTHUDUNGGIA_Model>();
                                        SetNullGrid(listBHDaiLy.Cast<object>().ToList());
                                        return;
                                    }
                                    catch (Exception ex)
                                    {
                                        WaitingFrm.StopWaiting();
                                        MSG.Error("Có lỗi trong quá trình import dữ liệu");
                                        _ISAInvoiceService.RolbackTran();
                                        return;
                                    }

                                }

                            case "DONMUAHANG":
                                {
                                    ConfigMuaHang cf = new ConfigMuaHang();
                                    data_ip = cf.SetDataDonMuaHang(data_import);
                                    IPPOrderService _IPPOrderService = IoC.Resolve<IPPOrderService>();
                                    _IPPOrderService.BeginTran();
                                    try
                                    {
                                        List<PPOrder> list = data_ip.Cast<PPOrder>().ToList();
                                        foreach (var item in list)
                                        {
                                            item.ID = Guid.NewGuid();
                                            foreach (var item2 in item.PPOrderDetails)
                                            {
                                                item2.PPOrderID = item.ID;
                                            }
                                            _IPPOrderService.CreateNew(item);
                                        }
                                        _IPPOrderService.CommitTran();
                                        WaitingFrm.StopWaiting();
                                        MSG.Information("Import dữ liệu thành công");
                                        return;
                                    }
                                    catch (Exception ex)
                                    {
                                        _IPPOrderService.RolbackTran();
                                        WaitingFrm.StopWaiting();
                                        MSG.Error("Có lỗi trong quá trình import dữ liệu");
                                        return;
                                    }
                                }
                            case "MUAHANGQUAKHO":
                                {
                                    ConfigMuaHangQuaKho cf = new ConfigMuaHangQuaKho();
                                    obj_ip = cf.SetDataMuaHangQuaKho(data_import);
                                    IPPInvoiceService _IPPInvoiceService = IoC.Resolve<IPPInvoiceService>();
                                    IMCPaymentService _IMCPaymentService = IoC.Resolve<IMCPaymentService>();
                                    IMBTellerPaperService _IMBTellerPaperService = IoC.Resolve<IMBTellerPaperService>();
                                    IMBCreditCardService _IMBCreditCardService = IoC.Resolve<IMBCreditCardService>();
                                    IRSInwardOutwardService _IRSInwardOutwardService = IoC.Resolve<IRSInwardOutwardService>();
                                    _IPPInvoiceService.BeginTran();
                                    try
                                    {
                                        MH_QuaKho_model model = obj_ip as MH_QuaKho_model;
                                        List<PPInvoice> list = model.listPPInvoice;
                                        List<MCPayment> list_MCPayment = model.listMCPayment;
                                        List<MBTellerPaper> list_MBTellerPaper = model.listMBTellerPaper;
                                        List<MBCreditCard> listMBCreditCard = model.listMBCreditCard;
                                        List<RSInwardOutward> listRSInwardOutward = model.listRSInwardOutward;
                                        foreach (var item in list)
                                        {
                                            item.ID = Guid.NewGuid();
                                            if (string.IsNullOrEmpty(item.No)) item.No = item.InwardNo;
                                            foreach (var item2 in item.PPInvoiceDetails)
                                            {
                                                item2.ID = Guid.NewGuid();
                                                item2.PPInvoiceID = item.ID;
                                            }
                                            _IPPInvoiceService.CreateNew(item);
                                            var itmcpayment = list_MCPayment.FirstOrDefault(t => t.No == item.No);
                                            if (itmcpayment != null)
                                            {
                                                itmcpayment.ID = item.ID;
                                                _IMCPaymentService.CreateNew(itmcpayment);
                                            }
                                            var itmbtele = list_MBTellerPaper.FirstOrDefault(t => t.No == item.No);
                                            if (itmbtele != null)
                                            {
                                                itmbtele.ID = item.ID;
                                                _IMBTellerPaperService.CreateNew(itmbtele);
                                            }
                                            var itmbcer = listMBCreditCard.FirstOrDefault(t => t.No == item.No);
                                            if (itmbcer != null)
                                            {
                                                itmbcer.ID = item.ID;
                                                _IMBCreditCardService.CreateNew(itmbcer);
                                            }
                                            var itRsio = listRSInwardOutward.FirstOrDefault(t => t.No == item.InwardNo);
                                            if (itRsio != null)
                                            {
                                                itRsio.ID = item.ID;
                                                _IRSInwardOutwardService.CreateNew(itRsio);
                                            }
                                        }
                                        _IPPInvoiceService.CommitTran();
                                        WaitingFrm.StopWaiting();
                                        MSG.Information("Import dữ liệu thành công");
                                        return;
                                    }
                                    catch (Exception ex)
                                    {
                                        WaitingFrm.StopWaiting();
                                        MSG.Error("Có lỗi trong quá trình import dữ liệu");
                                        _IPPInvoiceService.RolbackTran();
                                        return;
                                    }
                                }
                            case "MUAHANGKHONGQUAKHO":
                                {
                                    ConfigMuaHangKhongQuaKho cf = new ConfigMuaHangKhongQuaKho();
                                    obj_ip = cf.SetDataMuaHangKhongQuaKho(data_import);
                                    IPPInvoiceService _IPPInvoiceService = IoC.Resolve<IPPInvoiceService>();
                                    IMCPaymentService _IMCPaymentService = IoC.Resolve<IMCPaymentService>();
                                    IMBTellerPaperService _IMBTellerPaperService = IoC.Resolve<IMBTellerPaperService>();
                                    IMBCreditCardService _IMBCreditCardService = IoC.Resolve<IMBCreditCardService>();
                                    _IPPInvoiceService.BeginTran();
                                    try
                                    {
                                        MH_QuaKho_model model = obj_ip as MH_QuaKho_model;
                                        List<PPInvoice> list = model.listPPInvoice;
                                        List<MCPayment> list_MCPayment = model.listMCPayment;
                                        List<MBTellerPaper> list_MBTellerPaper = model.listMBTellerPaper;
                                        List<MBCreditCard> listMBCreditCard = model.listMBCreditCard;

                                        foreach (var item in list)
                                        {
                                            item.ID = Guid.NewGuid();
                                            foreach (var item2 in item.PPInvoiceDetails)
                                            {
                                                item2.ID = Guid.NewGuid();
                                                item2.PPInvoiceID = item.ID;
                                            }
                                            _IPPInvoiceService.CreateNew(item);
                                            var itmcpayment = list_MCPayment.FirstOrDefault(t => t.No == item.No);
                                            if (itmcpayment != null)
                                            {
                                                itmcpayment.ID = item.ID;
                                                _IMCPaymentService.CreateNew(itmcpayment);
                                            }
                                            var itmbtele = list_MBTellerPaper.FirstOrDefault(t => t.No == item.No);
                                            if (itmbtele != null)
                                            {
                                                itmbtele.ID = item.ID;
                                                _IMBTellerPaperService.CreateNew(itmbtele);
                                            }
                                            var itmbcer = listMBCreditCard.FirstOrDefault(t => t.No == item.No);
                                            if (itmbcer != null)
                                            {
                                                itmbcer.ID = item.ID;
                                                _IMBCreditCardService.CreateNew(itmbcer);
                                            }
                                        }
                                        _IPPInvoiceService.CommitTran();
                                        WaitingFrm.StopWaiting();
                                        WaitingFrm.StopWaiting();
                                        MSG.Information("Import dữ liệu thành công");
                                        return;
                                    }
                                    catch (Exception ex)
                                    {

                                        WaitingFrm.StopWaiting();
                                        MSG.Error("Có lỗi trong quá trình import dữ liệu");
                                        _IPPInvoiceService.RolbackTran();
                                        return;
                                    }
                                }
                            case "MUADICHVU":
                                {
                                    ConfigMuaHang cf = new ConfigMuaHang();
                                    obj_ip = cf.SetDataMuaDichVu(data_import);
                                    IPPServiceService _IPPServiceService = IoC.Resolve<IPPServiceService>();
                                    IMCPaymentService _IMCPaymentService = IoC.Resolve<IMCPaymentService>();
                                    IMBTellerPaperService _IMBTellerPaperService = IoC.Resolve<IMBTellerPaperService>();
                                    IMBCreditCardService _IMBCreditCardService = IoC.Resolve<IMBCreditCardService>();
                                    _IPPServiceService.BeginTran();
                                    try
                                    {
                                        MH_DichVu_model model = obj_ip as MH_DichVu_model;
                                        List<PPService> list = model.listPPService;
                                        List<MCPayment> list_MCPayment = model.listMCPayment;
                                        List<MBTellerPaper> list_MBTellerPaper = model.listMBTellerPaper;
                                        List<MBCreditCard> listMBCreditCard = model.listMBCreditCard;
                                        foreach (var item in list)
                                        {
                                            item.ID = Guid.NewGuid();
                                            foreach (var item2 in item.PPServiceDetails)
                                            {
                                                item2.PPServiceID = item.ID;
                                            }
                                            _IPPServiceService.CreateNew(item);
                                            var itmcpayment = list_MCPayment.FirstOrDefault(t => t.No == item.No);
                                            if (itmcpayment != null)
                                            {
                                                itmcpayment.ID = item.ID;
                                                _IMCPaymentService.CreateNew(itmcpayment);
                                            }
                                            var itmbtele = list_MBTellerPaper.FirstOrDefault(t => t.No == item.No);
                                            if (itmbtele != null)
                                            {
                                                itmbtele.ID = item.ID;
                                                _IMBTellerPaperService.CreateNew(itmbtele);
                                            }
                                            var itmbcer = listMBCreditCard.FirstOrDefault(t => t.No == item.No);
                                            if (itmbcer != null)
                                            {
                                                itmbcer.ID = item.ID;
                                                _IMBCreditCardService.CreateNew(itmbcer);
                                            }
                                        }
                                        _IPPServiceService.CommitTran();
                                        WaitingFrm.StopWaiting();
                                        MSG.Information("Import dữ liệu thành công");
                                        return;
                                    }
                                    catch (Exception ex)
                                    {
                                        WaitingFrm.StopWaiting();
                                        MSG.Error("Có lỗi trong quá trình import dữ liệu");
                                        _IPPServiceService.RolbackTran();
                                        return;
                                    }
                                }
                        }
                    }
                }
                else
                {
                    MSG.Error("Bạn chưa chọn danh mục");
                    return;
                }
            }
            catch (Exception ex)
            {
                log.Error("Thực hiện: " + ex.Message);
                WaitingFrm.StopWaiting();
                MSG.Error("Có lỗi trong quá trình lưu dữ liệu!");
            }
        }
        private void ShowThongbao()
        {
            if (MSG.Question("Dữ liệu file không đúng. Bạn có muốn tải file lỗi về không?") == System.Windows.Forms.DialogResult.Yes)
            {
                System.Windows.Forms.SaveFileDialog sf = new System.Windows.Forms.SaveFileDialog
                {
                    FileName = "FileExcelError.xlsx",
                    AddExtension = true,
                    Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
                };
                sf.Title = "Chọn thư mục lưu file";
                if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    try
                    {
                        wb.SaveAs(sf.FileName);
                    }
                    catch
                    {
                        MSG.Error("Lỗi khi lưu");
                    }
                    if (MSG.Question("Tải xuống thành công, Bạn có muốn mở file vừa tải") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(sf.FileName);
                        }
                        catch
                        {
                        }
                    }
                }
                else
                {
                }
            }
            else
            {
                MSG.Error("Thêm dữ liệu không thành công");
            }
        }
        private int CountOccurenceOfValue2(List<string> list, string valueToFind)
        {
            int count = 0;
            if (valueToFind != null)
            {
                count = list.Where(temp => temp.Equals(valueToFind))
                        .Select(temp => temp)
                        .Count();
            }

            return count;
        }
        private void SetNullGrid(List<object> lists)
        {
            cbbPhanHe.Value = null;
            cbbDanhMuc.Value = null;
            SetColumnGrid(list_config, lists);
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void cb_sheet_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (cb_sheet.Value != null)
            {
                if (cbbPhanHe.Value == null || cbbDanhMuc.Value == null)
                {
                    MSG.Error("Bạn chưa chọn phân hệ hoặc danh mục!");
                    return;
                }
                ChooseSheet(cb_sheet.Value.ToString());
                DataTable dtgt = new DataTable();
                List<COLUMN_MODEL> dic_cap = list_config;
                int dem = 0;
                bool check = false;
                while (!check || dem < ds.Tables.Count)
                {
                    if (ds.Tables[dem].TableName == cb_sheet.Value.ToString())
                    {
                        check = true;
                        dtgt = ds.Tables[dem];
                        dem = ds.Tables.Count;
                    }
                    dem++;
                }
                if (dtgt.Columns.Count == 0)
                {
                    MSG.Error("File excel có cột không đặt đúng tên vui lòng kiểm tra lại!");
                    return;
                }
                for (int i = 0; i < dtgt.Columns.Count; i++)
                {
                    dtgt.Columns[i].ColumnName = dtgt.Columns[i].ColumnName.Trim();
                    var ten_cot = dtgt.Columns[i].ColumnName.Trim();
                    var gt = dic_cap.FirstOrDefault(t => t.value.Replace("(*)", "").Trim().ToUpper() == ten_cot.Replace("(*)", "").Trim().ToUpper());
                    if (gt == null)
                    {
                        MSG.Error("File excel có cột không đặt đúng tên vui lòng kiểm tra lại!");
                        return;
                    }
                    dtgt.Columns[i].ColumnName = gt.key;
                }
                //List<object> data_import = new List<object>();
                if (cbbDanhMuc.Value.ToString() == "PHIEUTHU")
                {
                    List<MCReceipt_Model> list = ConvertTo<MCReceipt_Model>(dtgt);
                    list.ForEach(t =>
                    {
                        var accountingobj = _IAccountingObjectService.Query.FirstOrDefault(k => k.AccountingObjectCode == t.MaDoiTuong);
                        if (accountingobj != null)
                        {
                            t.AccountingObjectName = accountingobj.AccountingObjectName;
                            t.AccountingObjectType = accountingobj.ObjectType;
                        }

                    });
                    if (list.Count == 0)
                    {
                        MSG.Error("File excel không có dữ liệu!");
                        return;
                    }
                    data_import = list.Cast<object>().ToList();
                    //ConfigTMNH cf = new ConfigTMNH();
                    //data_ip = cf.SetDataPhieuThu(data_import);
                }
                if (cbbDanhMuc.Value.ToString() == "PHIEUCHI")
                {
                    List<MCPayment_Model> list = ConvertTo<MCPayment_Model>(dtgt);
                    list.ForEach(t =>
                    {
                        var accountingobj = _IAccountingObjectService.Query.FirstOrDefault(k => k.AccountingObjectCode == t.MaDoiTuong);
                        if (accountingobj != null)
                        {
                            t.AccountingObjectName = accountingobj.AccountingObjectName;
                            t.AccountingObjectType = accountingobj.ObjectType;
                        }
                    });
                    if (list.Count == 0)
                    {
                        MSG.Error("File excel không có dữ liệu!");
                        return;
                    }
                    data_import = list.Cast<object>().ToList();
                    //ConfigTMNH cf = new ConfigTMNH();
                    //data_ip = cf.SetDataPhieuChi(data_import);
                }
                if (cbbDanhMuc.Value.ToString() == "SECUNC")
                {
                    List<MBTellerPaper_Model> list = ConvertTo<MBTellerPaper_Model>(dtgt);
                    list.ForEach(t =>
                    {
                        var accountingobj = _IAccountingObjectService.Query.FirstOrDefault(k => k.AccountingObjectCode == t.MaDoiTuong);
                        if (accountingobj != null)
                        {
                            t.AccountingObjectName = accountingobj.AccountingObjectName;
                            t.AccountingObjectType = accountingobj.ObjectType;
                        }

                    });
                    if (list.Count == 0)
                    {
                        MSG.Error("File excel không có dữ liệu!");
                        return;
                    }
                    data_import = list.Cast<object>().ToList();
                    //ConfigTMNH cf = new ConfigTMNH();
                    //data_ip = cf.SetDataSecUNC(data_import);
                }
                if (cbbDanhMuc.Value.ToString() == "THUNGANHANG")
                {
                    List<MBDeposit_Model> list = ConvertTo<MBDeposit_Model>(dtgt);
                    list.ForEach(t =>
                    {
                        var accountingobj = _IAccountingObjectService.Query.FirstOrDefault(k => k.AccountingObjectCode == t.MaDoiTuong);
                        if (accountingobj != null)
                        {
                            t.AccountingObjectName = accountingobj.AccountingObjectName;
                            t.AccountingObjectType = accountingobj.ObjectType;
                        }

                    });
                    if (list.Count == 0)
                    {
                        MSG.Error("File excel không có dữ liệu!");
                        return;
                    }
                    data_import = list.Cast<object>().ToList();
                    //ConfigTMNH cf = new ConfigTMNH();
                    //data_ip = cf.SetDataThuNganHang(data_import);
                }
                if (cbbDanhMuc.Value.ToString() == "THETINDUNG")
                {
                    List<MBCreditCard_Model> list = ConvertTo<MBCreditCard_Model>(dtgt);
                    list.ForEach(t =>
                    {
                        var accountingobj = _IAccountingObjectService.Query.FirstOrDefault(k => k.AccountingObjectCode == t.MaDoiTuong);
                        if (accountingobj != null)
                        {
                            t.AccountingObjectName = accountingobj.AccountingObjectName;
                            t.AccountingObjectType = accountingobj.ObjectType;
                        }

                    });
                    if (list.Count == 0)
                    {
                        MSG.Error("File excel không có dữ liệu!");
                        return;
                    }
                    data_import = list.Cast<object>().ToList();
                    //ConfigTMNH cf = new ConfigTMNH();
                    //data_ip = cf.SetDataTheTinDung(data_import);
                }
                if (cbbDanhMuc.Value.ToString() == "CHUYENTIENNOIBO")
                {
                    List<MBInternalTransfer_Model> list = ConvertTo<MBInternalTransfer_Model>(dtgt);
                    if (list.Count == 0)
                    {
                        MSG.Error("File excel không có dữ liệu!");
                        return;
                    }
                    data_import = list.Cast<object>().ToList();
                    //ConfigTMNH cf = new ConfigTMNH();
                    //data_ip = cf.SetDataChuyenTienNB(data_import);
                }
                if (cbbDanhMuc.Value.ToString() == "KIEMKEQUY")
                {
                    List<MCAudit_Model> list = ConvertTo<MCAudit_Model>(dtgt);
                    if (list.Count == 0)
                    {
                        MSG.Error("File excel không có dữ liệu!");
                        return;
                    }
                    data_import = list.Cast<object>().ToList();
                    //ConfigTMNH cf = new ConfigTMNH();
                    //data_ip = cf.SetDataKiẹmkequy(data_import);
                }
                //Mua Hàng
                if (cbbDanhMuc.Value.ToString() == "DONMUAHANG")
                {
                    List<PPOrder_Model> list = ConvertTo<PPOrder_Model>(dtgt);
                    if (list.Count == 0)
                    {
                        MSG.Error("File excel không có dữ liệu!");
                        return;
                    }
                    data_import = list.Cast<object>().ToList();
                    //SetDataPhieuChi(data_import);`
                }
                if (cbbDanhMuc.Value.ToString() == "MUAHANGQUAKHO")
                {
                    List<MuaHangQuaKho_Model> list = ConvertTo<MuaHangQuaKho_Model>(dtgt);
                    if (list.Count == 0)
                    {
                        MSG.Error("File excel không có dữ liệu!");
                        return;
                    }
                    data_import = list.Cast<object>().ToList();
                    //SetDataPhieuChi(data_import);
                }
                if (cbbDanhMuc.Value.ToString() == "MUAHANGKHONGQUAKHO")
                {
                    List<MuaHangKhongQuaKho_Model> list = ConvertTo<MuaHangKhongQuaKho_Model>(dtgt);
                    if (list.Count == 0)
                    {
                        MSG.Error("File excel không có dữ liệu!");
                        return;
                    }
                    data_import = list.Cast<object>().ToList();
                    //SetDataPhieuChi(data_import);
                }
                if (cbbDanhMuc.Value.ToString() == "MUADICHVU")
                {
                    List<MuaDichVu_Model> list = ConvertTo<MuaDichVu_Model>(dtgt);
                    if (list.Count == 0)
                    {
                        MSG.Error("File excel không có dữ liệu!");
                        return;
                    }
                    data_import = list.Cast<object>().ToList();
                    //SetDataPhieuChi(data_import);
                }
                //Bán hàng
                if (cbbDanhMuc.Value.ToString() == "BAOGIA")
                {
                    List<SAQuote_Model> list = ConvertTo<SAQuote_Model>(dtgt);
                    if (list.Count == 0)
                    {
                        MSG.Error("File excel không có dữ liệu!");
                        return;
                    }
                    data_import = list.Cast<object>().ToList();
                    //ConfigBanHang cf = new ConfigBanHang();
                    //data_ip = cf.SetDataBaoGia(data_import);
                }
                if (cbbDanhMuc.Value.ToString() == "DONDATHANG")
                {
                    List<SAOrder_Model> list = ConvertTo<SAOrder_Model>(dtgt);
                    if (list.Count == 0)
                    {
                        MSG.Error("File excel không có dữ liệu!");
                        return;
                    }
                    data_import = list.Cast<object>().ToList();
                    //ConfigBanHang cf = new ConfigBanHang();
                    //data_ip = cf.SetDataDonDatHang(data_import);
                }
                if (cbbDanhMuc.Value.ToString() == "BANHANGCHUATHUTIEN")
                {
                    List<SAInvoice_Model> list = ConvertTo<SAInvoice_Model>(dtgt);
                    if (list.Count == 0)
                    {
                        MSG.Error("File excel không có dữ liệu!");
                        return;
                    }
                    data_import = list.Cast<object>().ToList();
                    //ConfigBanHang cf = new ConfigBanHang();
                    //data_ip = cf.SetDataBHChuaThuTien(data_import);
                }
                if (cbbDanhMuc.Value.ToString() == "BANHANGTHUTIENNGAY")
                {
                    List<BHTHUTIENNGAY_Model> list = ConvertTo<BHTHUTIENNGAY_Model>(dtgt);
                    if (list.Count == 0)
                    {
                        MSG.Error("File excel không có dữ liệu!");
                        return;
                    }
                    data_import = list.Cast<object>().ToList();
                    //ConfigBanHang cf = new ConfigBanHang();
                    //obj_ip = cf.SetDataBHThuTienNgay(data_import);
                }
                if (cbbDanhMuc.Value.ToString() == "BHDLBANDUNGGIA")
                {
                    List<BHDAILYTHUDUNGGIA_Model> list = ConvertTo<BHDAILYTHUDUNGGIA_Model>(dtgt);
                    if (list.Count == 0)
                    {
                        MSG.Error("File excel không có dữ liệu!");
                        return;
                    }
                    data_import = list.Cast<object>().ToList();
                    //SetDataPhieuChi(data_import);
                }
                SetColumnGrid(list_config, data_import);
            }
        }
        public List<T> ConvertTo<T>(DataTable datatable) where T : new()
        {
            List<T> Temp = new List<T>();
            try
            {
                List<string> columnsNames = new List<string>();
                int i = 0;
                foreach (DataColumn DataColumn in datatable.Columns)
                    columnsNames.Add(DataColumn.ColumnName);
                Temp = datatable.AsEnumerable().ToList().ConvertAll<T>(row => getObject<T>(row, columnsNames));
                return Temp;
            }
            catch
            {
                return Temp;
            }

        }
        public T getObject<T>(DataRow row, List<string> columnsName) where T : new()
        {
            T obj = new T();
            try
            {
                string columnname = "";
                string value = "";
                PropertyInfo[] Properties;
                Properties = typeof(T).GetProperties();
                foreach (PropertyInfo objProperty in Properties)
                {
                    columnname = columnsName.Find(name => name.ToLower() == objProperty.Name.ToLower());
                    if (!string.IsNullOrEmpty(columnname))
                    {
                        value = row[columnname].ToString();
                        var gt = objProperty.PropertyType;
                        if (!string.IsNullOrEmpty(value))
                        {
                            string strType = "";
                            if (System.Nullable.GetUnderlyingType(objProperty.PropertyType) != null)
                            {
                                strType = System.Nullable.GetUnderlyingType(objProperty.PropertyType).ToString();
                                if (strType.IndexOf("String") > -1)
                                    value = value.Replace("$", "").Replace("%", "").Trim();
                            }
                            else
                            {
                                strType = objProperty.PropertyType.ToString();
                                if (strType.IndexOf("String") > -1)
                                    value = value.Replace("$", "").Replace("%", "").Trim();
                            }
                            if (strType.IndexOf("DateTime") > -1)
                                objProperty.SetValue(obj, convertToDateTime(value), null);
                            else
                                objProperty.SetValue(obj, Convert.ChangeType(value, System.Type.GetType(strType), null));
                        }
                    }
                }
                return obj;
            }
            catch (Exception ex)
            {
                return obj;
            }
        }
        private DateTime convertToDateTime(object date)
        {
            var formats = new[] { "dd/MM/yyyy HH:mm:ss", "dd/M/yyyy HH:mm", "dd/MM/yyyy", "MM/dd/yyyy HH:mm:ss", "MM/dd/yyyy" };
            DateTime fromDateValue;
            var gt = DateTime.TryParseExact(date.ToString(), formats, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out fromDateValue);
            return fromDateValue;
        }
        private bool IsDate(string tempDate)
        {
            if (tempDate == "  -  -" || tempDate == "")
            {
                return true;
            }
            DateTime fromDateValue;
            var formats = new[] { "dd/MM/yyyy HH:mm:ss", "dd/M/yyyy HH:mm", "dd/MM/yyyy", "MM/dd/yyyy HH:mm:ss", "MM/dd/yyyy" };
            if (DateTime.TryParseExact(tempDate, formats, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out fromDateValue))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool checkMocTG(string tg)
        {
            var gt = convertToDateTime(tg);
            var tgbd = new DateTime(1970, 1, 1);
            var tgKt = new DateTime(9999, 12, 31);
            if (gt >= tgbd && gt <= tgKt)
                return true;
            return false;
        }

        /// <summary>
        /// Hàm cấu hình gridview
        /// </summary>
        /// <param name="dic_cap">Danh sách cột</param>
        /// <param name="data">Dữ liệu truyền vào từ file excel</param>
        private void SetColumnGrid(List<COLUMN_MODEL> dic_cap, List<object> data)
        {
            string nameTable = string.Empty;
            List<string> strColumnName, strColumnCaption, strColumnToolTip = new List<string>();
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, bolIsColumnHeader = new List<bool>();
            List<int> intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition = new List<int>();
            List<int> VTbolIsVisible, VTbolIsVisibleCbb, VTintVisiblePosition, VTbolIsReadOnly, VTbolIsColumnHeader = new List<int>();
            nameTable = "";
            strColumnName = dic_cap.Select(t => t.key).ToList();
            strColumnCaption = dic_cap.Select(t => t.value).ToList();

            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() { };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 2 };
            for (int i = 0; i < dic_cap.Count; i++)
            {
                Font ft = new Font("Microsoft Sans Serif", 9);
                var width = TextRenderer.MeasureText(dic_cap[i].value, ft).Width + 50;
                bolIsVisible.Add(true);
                bolIsVisibleCbb.Add(true);
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(width);
                intColumnMaxWidth.Add(400);
                intColumnMinWidth.Add(100);
                intVisiblePosition.Add(i);
            }
            List<TemplateColumn> list = new List<TemplateColumn>();
            for (int i = 0; i < strColumnName.Count; i++)
            {
                try
                {
                    var tcTemp = new TemplateColumn
                    {
                        ColumnName = strColumnName[i],
                        ColumnCaption = strColumnCaption[i],
                        //ColumnToolTip = strColumnToolTip[i],
                        IsReadOnly = bolIsReadOnly[i],
                        IsVisible = bolIsVisible[i],
                        IsVisibleCbb = bolIsVisibleCbb[i],
                        ColumnWidth = intColumnWidth[i],
                        ColumnMaxWidth = intColumnMaxWidth[i],
                        ColumnMinWidth = intColumnMinWidth[i],
                        VisiblePosition = intVisiblePosition[i],
                        //IsColumnHeader = bolIsColumnHeader == null ? false : bolIsColumnHeader[i]
                    };
                    list.Add(tcTemp);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    throw;
                }
            }
            uGridMapping.DataSource = data;
            Utils.ConfigGrid(uGridMapping, nameTable, list, false, true, false, false);
        }
        private void FileDemo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (cbbDanhMuc.Value == null)
            {
                MSG.Warning("Bạn chưa chọn danh mục!");
                return;
            }
            var list_dm = GetDanhMuc(cbbPhanHe.Value.ToString());
            var file_mau = list_dm.FirstOrDefault(t => t.Key == FileDemo.Text);
            string ten_file = FileDemo.Text;
            string path_file = file_mau.Value;
            ten_file = ten_file.Replace("/", " ");
            SaveFileDialog sf = new SaveFileDialog
            {
                FileName = ten_file + ".xlsx",
                AddExtension = true,
                Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
            };

            if (sf.ShowDialog() == DialogResult.OK)
            {
                string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                string filePath = string.Format("{0}\\TemplateResource\\excel\\ChungTu\\" + path_file + ".xlsx", path);
                ResourceHelper.MakeFileOutOfAStream(filePath, sf.FileName);
                if (MSG.Question("Tải xuống thành công, Bạn có muốn mở file vừa tải về không?") == System.Windows.Forms.DialogResult.Yes)
                {
                    try
                    {
                        System.Diagnostics.Process.Start(sf.FileName);
                    }
                    catch (Exception ex)
                    {
                        MSG.Error(ex.Message);
                    }

                }
            }
        }
        #region Check data file excel
        private string CheckNoChungTu(List<string> list, string ten_cot)
        {
            string check_file = "";
            var nonEmptyDataRows = ws.RowsUsed();
            var list_content = nonEmptyDataRows.Where(n => n.RowNumber() > 1);
            var index = list_config.FindIndex(t => t.value.ToUpper() == ten_cot.ToUpper());
            foreach (var item in list_content)
            {
                IXLCell xLCell = item.Cell(index + 1);
                xLCell.MergedRange();
                xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                string value = xLCell.Value.ToString();
                if (list.Any(t => t == value))
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    check_file = "ErrData";
                    xLCell.Comment.AddText(list_config[index].value + " phải là duy nhất");
                }
            }
            return check_file;
        }
        private string CheckExcel(string ten_dm)
        {
            string check_file = "";
            string StrError = "";
            try
            {
                var nonEmptyDataRows = ws.RowsUsed();
                var first_row = nonEmptyDataRows.Where(n => n.RowNumber() == 1);
                string str_cell = "";
                foreach (var item in first_row)
                {
                    for (int i = 1; i <= list_config.Count; i++)
                    {
                        IXLCell xLCell = item.Cell(i);
                        string value = xLCell.Value.ToString();
                        str_cell = string.IsNullOrEmpty(str_cell) ? value.Replace("(*)", "").Trim() : str_cell + "," + value.Replace("(*)", "").Trim();
                    }
                }
                string strConfig = string.Join(",", list_config.Select(t => t.value).ToArray()).ToUpper();
                str_cell = str_cell.ToUpper();
                if (strConfig != str_cell)
                {
                    return "ErrThuTu";
                }
                var list_content = nonEmptyDataRows.Where(n => n.RowNumber() > 1);

                foreach (var item in list_content)
                {
                    StrError = "";
                    for (int i = 1; i <= list_config.Count; i++)
                    {
                        IXLCell xLCell = item.Cell(i);
                        xLCell.MergedRange();
                        xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                        string value = xLCell.Value.ToString();
                        var column_cur = list_config[i - 1];
                        if (column_cur.check == 1)
                        {
                            if (string.IsNullOrEmpty(value))
                            {
                                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                StrError = StrError + column_cur.value + " bắt buộc nhập; ";
                                check_file = "ErrData";
                                xLCell.Comment.AddText(column_cur.value + " bắt buộc nhập");
                            }
                        }
                        string check_chung = CheckColumChung(xLCell, column_cur);
                        if (!string.IsNullOrEmpty(check_chung))
                        {
                            StrError = StrError + check_chung;
                            check_file = "ErrData";
                        }
                        switch (ten_dm)
                        {
                            case "PHIEUTHU":
                                string check = CheckPhieuThu(xLCell, column_cur);
                                if (!string.IsNullOrEmpty(check))
                                    StrError = StrError + check;
                                break;
                            case "PHIEUCHI":
                                string checkpc = CheckPhieuChi(xLCell, column_cur);
                                if (!string.IsNullOrEmpty(checkpc))
                                    StrError = StrError + checkpc;
                                break;
                            case "SECUNC":
                                string checksec = CheckSecUNC(xLCell, column_cur);
                                if (!string.IsNullOrEmpty(checksec))
                                    StrError = StrError + checksec;
                                break;
                            case "THUNGANHANG":
                                string checkthunh = CheckThuNganHang(xLCell, column_cur);
                                if (!string.IsNullOrEmpty(checkthunh))
                                    StrError = StrError + checkthunh;
                                break;
                            case "THETINDUNG":
                                string checkthetd = CheckTheTinDung(item, i, column_cur);
                                if (!string.IsNullOrEmpty(checkthetd))
                                    StrError = StrError + checkthetd;
                                break;
                            case "CHUYENTIENNOIBO":
                                string checkchuyentiennb = CheckChuyenTienNB(item, i, column_cur);
                                if (!string.IsNullOrEmpty(checkchuyentiennb))
                                    StrError = StrError + checkchuyentiennb;
                                break;
                            case "KIEMKEQUY":
                                string checkkiemkequy = CheckKiemKeQuy(xLCell, column_cur);
                                if (!string.IsNullOrEmpty(checkkiemkequy))
                                    StrError = StrError + checkkiemkequy;
                                break;
                            //Mua hàng
                            case "DONMUAHANG":
                                string checkDonMuaHang = CheckDonMuaHang(xLCell, column_cur);
                                if (!string.IsNullOrEmpty(checkDonMuaHang))
                                    StrError = StrError + checkDonMuaHang;
                                break;
                            case "MUAHANGQUAKHO":
                                string checkMHQuaKho = CheckMuaHangQuaKho(item, i, column_cur);
                                if (!string.IsNullOrEmpty(checkMHQuaKho))
                                    StrError = StrError + checkMHQuaKho;
                                break;
                            case "MUAHANGKHONGQUAKHO":
                                string checkMHKhongQuaKho = CheckMuaHangKhongQuaKho(item, i, column_cur);
                                if (!string.IsNullOrEmpty(checkMHKhongQuaKho))
                                    StrError = StrError + checkMHKhongQuaKho;
                                break;
                            case "MUADICHVU":
                                string checkMuaDV = CheckMuaDichVu(item, i, column_cur);
                                if (!string.IsNullOrEmpty(checkMuaDV))
                                    StrError = StrError + checkMuaDV;
                                break;
                            //Bán hàng
                            case "BAOGIA":
                                string checkBaoGia = CheckBaoGia(xLCell, column_cur);
                                if (!string.IsNullOrEmpty(checkBaoGia))
                                    StrError = StrError + checkBaoGia;
                                break;
                            case "DONDATHANG":
                                string checkDDH = CheckDonDatHang(xLCell, column_cur);
                                if (!string.IsNullOrEmpty(checkDDH))
                                    StrError = StrError + checkDDH;
                                break;
                            case "BANHANGCHUATHUTIEN":
                                string checkBHCHUATT = CheckBHChuaTT(item, i, column_cur);
                                if (!string.IsNullOrEmpty(checkBHCHUATT))
                                    StrError = StrError + checkBHCHUATT;
                                break;
                            case "BANHANGTHUTIENNGAY":
                                string checkBHThuTN = CheckBHTTNgay(item, i, column_cur);
                                if (!string.IsNullOrEmpty(checkBHThuTN))
                                    StrError = StrError + checkBHThuTN;
                                break;
                            case "BHDLBANDUNGGIA":
                                string checkBHDL = CheckBHDL(item, i, column_cur);
                                if (!string.IsNullOrEmpty(checkBHDL))
                                    StrError = StrError + checkBHDL;
                                break;
                        }
                        if (!string.IsNullOrEmpty(column_cur.value))
                        {
                            string check_length = CheckLengthCol(xLCell, column_cur);
                            if (!string.IsNullOrEmpty(check_length))
                                StrError = StrError + check_length;
                        }
                        if (column_cur.value.ToUpper() == "Ngày hạch toán".ToUpper())
                        {
                            var index = list_config.FindIndex(t => t.value.ToUpper() == "Ngày chứng từ".ToUpper());
                            var gt = xLCell.Style.Fill.BackgroundColor.Color.Name;
                            var gt2 = item.Cell(index + 1).Style.Fill.BackgroundColor.Color.Name;
                            if (gt == "Transparent" && gt2 == "Transparent")
                            {
                                var ngay_ht = convertToDateTime(value);
                                var ngay_ct = convertToDateTime(item.Cell(index + 1).Value);
                                if (ngay_ct > ngay_ht)
                                {
                                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                                    xLCell.Comment.AddText("Ngày chứng từ phải nhỏ hơn hoặc bằng ngày hạch toán; ");
                                    item.Cell(index + 1).Style.Fill.BackgroundColor = XLColor.Red;
                                    item.Cell(index + 1).Comment.AddText("Ngày chứng từ phải nhỏ hơn hoặc bằng ngày hạch toán; ");
                                    StrError = StrError + "Ngày chứng từ phải nhỏ hơn hoặc bằng ngày hạch toán; ";

                                }
                            }

                        }
                        if (!string.IsNullOrEmpty(StrError))
                        {
                            //item.Cell(list_config.Count + 1).Value = StrError;
                            check_file = "ErrData";
                        }


                    }
                }
                if (string.IsNullOrEmpty(check_file))
                    check_file = "Success";
            }
            catch (Exception ex)
            {
                check_file = "Exeption";
            }
            return check_file;
        }
        private string CheckColumChung(IXLCell xLCell, COLUMN_MODEL column)
        {
            IBudgetItemService _IBudgetItemService = IoC.Resolve<IBudgetItemService>();
            IExpenseItemService _IExpenseItemService = IoC.Resolve<IExpenseItemService>();
            IDepartmentService _IDepartmentService = IoC.Resolve<IDepartmentService>();
            ICostSetService _ICostSetService = IoC.Resolve<ICostSetService>();
            IEMContractService _IEMContractService = IoC.Resolve<IEMContractService>();
            IStatisticsCodeService _IStatisticsCodeService = IoC.Resolve<IStatisticsCodeService>();
            IGoodsServicePurchaseService _IGoodsServicePurchaseService = IoC.Resolve<IGoodsServicePurchaseService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            IAccountService _IAccountService = IoC.Resolve<IAccountService>();
            IPaymentClauseService _IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            IRSInwardOutwardService _IRSInwardOutwardService = IoC.Resolve<IRSInwardOutwardService>();
            bool kq = true;
            string ketqua = "";
            string excel_value = xLCell.Value.ToString();
            string db_column = column.key;
            if (!string.IsNullOrEmpty(excel_value))
            {
                switch (db_column)
                {
                    case "DebitAccount":
                        var check_tkno = _IAccountService.Query.Any(t => t.AccountNumber == excel_value && t.IsParentNode == false);
                        if (!check_tkno)
                        {
                            kq = false;
                            ketqua = column.value + " không tồn tại; ";
                        }
                        break;
                    case "CreditAccount":
                        var check_tkco = _IAccountService.Query.Any(t => t.AccountNumber == excel_value && t.IsParentNode == false);
                        if (!check_tkco)
                        {
                            kq = false;
                            ketqua = column.value + " không tồn tại; ";
                        }
                        break;
                    case "Date":
                        var checkdate = IsDate(excel_value);
                        if (!checkdate)
                        {
                            kq = false;
                            ketqua = column.value + " sai định dạng ngày tháng; ";
                        }
                        else
                        {
                            var checkmoctg = checkMocTG(excel_value);
                            if (!checkmoctg)
                            {
                                kq = false;
                                ketqua = column.value + " chỉ có nhập từ năm 1970 hoặc lớn hơn; ";
                            }
                        }
                        break;
                    case "DueDate":
                        var checkDueDate = IsDate(excel_value);
                        if (!checkDueDate)
                        {
                            kq = false;
                            ketqua = column.value + " sai định dạng ngày tháng; ";
                        }
                        else
                        {
                            var checkmoctg = checkMocTG(excel_value);
                            if (!checkmoctg)
                            {
                                kq = false;
                                ketqua = column.value + " chỉ có nhập từ năm 1970 hoặc lớn hơn; ";
                            }
                        }
                        break;
                    case "PostedDate":
                        var checkpostdate = IsDate(excel_value);
                        if (!checkpostdate)
                        {
                            kq = false;
                            ketqua = column.value + " sai định dạng ngày tháng; ";
                        }
                        else
                        {
                            var checkmoctg = checkMocTG(excel_value);
                            if (!checkmoctg)
                            {
                                kq = false;
                                ketqua = column.value + " chỉ có nhập từ năm 1970 hoặc lớn hơn; ";
                            }
                        }
                        break;
                    case "InvoiceDate":
                        var checkInvoice = IsDate(excel_value);
                        if (!checkInvoice)
                        {
                            kq = false;
                            ketqua = column.value + " sai định dạng ngày tháng; ";
                        }
                        else
                        {
                            var checkmoctg = checkMocTG(excel_value);
                            if (!checkmoctg)
                            {
                                kq = false;
                                ketqua = column.value + " chỉ có nhập từ năm 1970 hoặc lớn hơn; ";
                            }
                        }
                        break;

                    case "IssueDate":
                        var checkngaycap = IsDate(excel_value);
                        if (!checkngaycap)
                        {
                            kq = false;
                            ketqua = column.value + " sai định dạng ngày tháng; ";
                        }
                        else
                        {
                            var checkmoctg = checkMocTG(excel_value);
                            if (!checkmoctg)
                            {
                                kq = false;
                                ketqua = column.value + " chỉ có nhập từ năm 1970 hoặc lớn hơn; ";
                            }
                        }
                        break;
                    case "ExpiryDate":
                        var checkExpiryDate = IsDate(excel_value);
                        if (!checkExpiryDate)
                        {
                            kq = false;
                            ketqua = column.value + " sai định dạng ngày tháng; ";
                        }
                        else
                        {
                            var checkmoctg = checkMocTG(excel_value);
                            if (!checkmoctg)
                            {
                                kq = false;
                                ketqua = column.value + " chỉ có nhập từ năm 1970 hoặc lớn hơn; ";
                            }
                        }
                        break;
                    case "MaDoiTuong":
                        var check_doituong = _IAccountingObjectService.Query.Any(t => t.AccountingObjectCode == excel_value);
                        if (!check_doituong)
                        {
                            kq = false;
                            ketqua = "Mã đối tượng không tồn tại; ";
                        }
                        break;
                    case "CurrencyID":
                        var check_loaitien = _ICurrencyService.Query.Any(t => t.ID == excel_value);
                        if (!check_loaitien)
                        {
                            ketqua = "Loại tiền không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaKhoanMucCP":
                        var check_exp = _IExpenseItemService.Query.Any(t => t.ExpenseItemCode == excel_value);
                        if (!check_exp)
                        {
                            ketqua = "Khoản mục CP không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaPhongBan":
                        var department = _IDepartmentService.Query.Any(t => t.DepartmentCode == excel_value);
                        if (!department)
                        {
                            ketqua = "Phòng ban không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaDoiTuongCP":
                        var costSet = _ICostSetService.Query.Any(t => t.CostSetCode == excel_value);
                        if (!costSet)
                        {
                            ketqua = "Đối tượng tập hợp CP không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaHopDong":
                        var contact = _IEMContractService.Query.Any(t => t.Code == excel_value);
                        if (!contact)
                        {
                            ketqua = "Hợp đồng không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaMucThuChi":
                        var BudgetObj = _IBudgetItemService.Query.Any(t => t.BudgetItemCode == excel_value);
                        if (!BudgetObj)
                        {
                            ketqua = "Mục thu/chi không tồn tại; ";
                            kq = false;
                        }
                        break;

                    case "MaThongKe":
                        var StatisticsCodeObj = _IStatisticsCodeService.Query.Any(t => t.StatisticsCode_ == excel_value);
                        if (!StatisticsCodeObj)
                        {
                            ketqua = "Mã thống kê không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaNhomHHDV":
                        var GoodsServicePurchaseObj = _IGoodsServicePurchaseService.Query.Any(t => t.GoodsServicePurchaseCode == excel_value);
                        if (!GoodsServicePurchaseObj)
                        {
                            ketqua = "Nhóm HHDV không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaHang":
                        var hanghoa = _IMaterialGoodsService.Query.Any(t => t.MaterialGoodsCode == excel_value);
                        if (!hanghoa)
                        {
                            ketqua = "Mã hàng không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "IdentificationNo":
                        if (!excel_value.All(char.IsNumber))
                        {
                            ketqua = "Số CMT chỉ có thể nhập số; ";
                            kq = false;
                        }
                        else
                        {
                            if (excel_value.Length != 9 && excel_value.Length != 12)
                            {
                                ketqua = "Số CMT chỉ có 9 chữ số hoặc 12 chữ số; ";
                                kq = false;
                            }
                        }
                        break;
                    case "MaNhanVien":
                        var doituong = _IAccountingObjectService.Query.Any(t => t.AccountingObjectCode == excel_value);
                        if (!doituong)
                        {
                            ketqua = column.value + " không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "ExportTaxAccount":
                        if (!checkaccount_number(excel_value))
                        {
                            kq = false;
                            ketqua = column.value + " không tồn tại; ";
                        }
                        break;
                    case "DiscountAccount":
                        if (!checkaccount_number(excel_value))
                        {
                            kq = false;
                            ketqua = column.value + " không tồn tại; ";
                        }
                        break;
                    case "ExportTaxAccountCorresponding":
                        if (!checkaccount_number(excel_value))
                        {
                            kq = false;
                            ketqua = column.value + " không tồn tại; ";
                        }
                        break;
                    case "VATAccount":
                        if (!checkaccount_number(excel_value))
                        {
                            kq = false;
                            ketqua = column.value + " không tồn tại; ";
                        }
                        break;
                    case "RepositoryAccount":
                        if (!checkaccount_number(excel_value))
                        {
                            kq = false;
                            ketqua = column.value + " không tồn tại; ";
                        }
                        break;
                    case "CostAccount":
                        if (!checkaccount_number(excel_value))
                        {
                            kq = false;
                            ketqua = column.value + " không tồn tại; ";
                        }
                        break;
                    case "ImportTaxAccount":
                        if (!checkaccount_number(excel_value))
                        {
                            kq = false;
                            ketqua = column.value + " không tồn tại; ";
                        }
                        break;
                    case "SpecialConsumeTaxAccount":
                        if (!checkaccount_number(excel_value))
                        {
                            kq = false;
                            ketqua = column.value + " không tồn tại; ";
                        }
                        break;
                    case "DeductionDebitAccount":
                        if (!checkaccount_number(excel_value))
                        {
                            kq = false;
                            ketqua = column.value + " không tồn tại; ";
                        }
                        break;
                    case "MaDKTT":
                        var checkMaDKTT = _IPaymentClauseService.Query.Any(t => t.PaymentClauseCode == excel_value);
                        if (!checkMaDKTT)
                        {
                            kq = false;
                            ketqua = column.value + " không tồn tại; ";
                        }
                        break;
                    case "OutwardNo":
                        var checkKho = _IRSInwardOutwardService.Query.Any(t => t.No == excel_value);
                        if (checkKho)
                        {
                            kq = false;
                            ketqua = column.value + " đã tồn tại; ";
                        }
                        break;
                }
                if (db_column.IndexOf("Amount") > -1 && !excel_value.All(char.IsNumber))
                {
                    ketqua = column.value + " chỉ có thể nhập dạng số; ";
                    kq = false;
                }

                if (db_column.IndexOf("UnitPrice") > -1 && !excel_value.All(char.IsNumber))
                {
                    ketqua = column.value + " chỉ có thể nhập dạng số; ";
                    kq = false;
                }
                if (db_column.IndexOf("Quantity") > -1 && !excel_value.All(char.IsNumber))
                {
                    ketqua = column.value + " chỉ có thể nhập dạng số; ";
                    kq = false;
                }
                if (!kq)
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    xLCell.Comment.AddText(ketqua);
                }
            }
            return ketqua;
        }
        private string CheckPhieuThu(IXLCell xLCell, COLUMN_MODEL column)
        {
            IExpenseItemService _IExpenseItemService = IoC.Resolve<IExpenseItemService>();
            ICostSetService _ICostSetService = IoC.Resolve<ICostSetService>();
            IEMContractService _IEMContractService = IoC.Resolve<IEMContractService>();
            IDepartmentService _IDepartmentService = IoC.Resolve<IDepartmentService>();
            bool kq = true;
            string ketqua = "";
            string excel_value = xLCell.Value.ToString();
            string db_column = column.key;
            if (!string.IsNullOrEmpty(excel_value))
            {
                switch (db_column)
                {
                    case "No":
                        var checkSoCT = _IMCReceiptService.Query.Any(t => t.No == excel_value);
                        if (checkSoCT)
                        {
                            kq = false;
                            ketqua = column.value + " đã tồn tại";
                        }
                        break;
                }
                if (!kq)
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    xLCell.Comment.AddText(ketqua);
                    ketqua = "ErrData";
                }
            }
            return ketqua;
        }
        private string CheckPhieuChi(IXLCell xLCell, COLUMN_MODEL column)
        {
            IAccountingObjectService _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            IStatisticsCodeService _IStatisticsCodeService = IoC.Resolve<IStatisticsCodeService>();
            IGoodsServicePurchaseService _IGoodsServicePurchaseService = IoC.Resolve<IGoodsServicePurchaseService>();
            bool kq = true;
            string ketqua = "";
            string excel_value = xLCell.Value.ToString();
            string db_column = column.key;
            if (!string.IsNullOrEmpty(excel_value))
            {
                switch (db_column)
                {
                    case "No":
                        var checkSoCT = _IMCReceiptService.Query.Any(t => t.No == excel_value);
                        if (checkSoCT)
                        {
                            ketqua = "Số chứng từ đã tồn tại; ";
                            kq = false;
                        }
                        break;
                }
                if (!kq)
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    xLCell.Comment.AddText(ketqua);
                }
            }
            return ketqua;
        }
        private string CheckSecUNC(IXLCell xLCell, COLUMN_MODEL column)
        {
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            IAccountingObjectBankAccountService _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            IGoodsServicePurchaseService _IGoodsServicePurchaseService = IoC.Resolve<IGoodsServicePurchaseService>();
            IInvoiceTypeService _IInvoiceTypeService = IoC.Resolve<IInvoiceTypeService>();
            bool kq = true;
            string ketqua = "";
            string excel_value = xLCell.Value.ToString();
            string db_column = column.key;
            if (!string.IsNullOrEmpty(excel_value))
            {
                switch (db_column)
                {
                    case "No":
                        var checkSoCT = _IMBTellerPaperService.Query.Any(t => t.No == excel_value);
                        if (checkSoCT)
                        {
                            ketqua = "Số chứng từ đã tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaTKChi":
                        var bankChi = _IBankAccountDetailService.Query.Any(t => t.BankAccount == excel_value);
                        if (!bankChi)
                        {
                            ketqua = "Tài khoản chi không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaTKNguoiNhan":
                        var tkNhan = _IBankAccountDetailService.Query.Any(t => t.BankAccount == excel_value);
                        if (!tkNhan)
                        {
                            kq = false;
                            ketqua = "TK người nhận không tồn tại; ";
                        }
                        break;
                }
                if (!kq)
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    xLCell.Comment.AddText(ketqua);
                }
            }
            return ketqua;
        }
        private string CheckThuNganHang(IXLCell xLCell, COLUMN_MODEL column)
        {
            IMBDepositService _IMBDepositService = IoC.Resolve<IMBDepositService>();
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            bool kq = true;
            string ketqua = "";
            string excel_value = xLCell.Value.ToString();
            string db_column = column.key;
            if (!string.IsNullOrEmpty(excel_value))
            {
                switch (db_column)
                {
                    case "No":
                        var checkSoCT = _IMBDepositService.Query.Any(t => t.No == excel_value);
                        if (checkSoCT)
                        {
                            ketqua = "Số chứng từ đã tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "SoTKHuong":
                        var bankChi = _IBankAccountDetailService.Query.Any(t => t.BankAccount == excel_value);
                        if (!bankChi)
                        {
                            ketqua = "Tài khoản hưởng không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaDoiTuong":
                        var check_doituong = _IAccountingObjectService.Query.Any(t => t.AccountingObjectCode == excel_value);
                        if (!check_doituong)
                        {
                            kq = false;
                            ketqua = "Mã đối tượng không tồn tại; ";
                        }
                        break;
                }
                if (!kq)
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    xLCell.Comment.AddText(ketqua);
                }
            }
            return ketqua;
        }
        private string CheckTheTinDung(IXLRow xLRow, int index_cell, COLUMN_MODEL column)
        {
            IMBCreditCardService _IMBCreditCardService = IoC.Resolve<IMBCreditCardService>();
            IAccountingObjectBankAccountService _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            ICreditCardService _ICreditCardService = IoC.Resolve<ICreditCardService>();
            bool kq = true;
            string ketqua = "";
            var xLCell = xLRow.Cell(index_cell);
            string excel_value = xLCell.Value.ToString();
            string db_column = column.key;
            if (!string.IsNullOrEmpty(excel_value))
            {
                switch (db_column)
                {
                    case "No":
                        var checkSoCT = _IMBCreditCardService.Query.Any(t => t.No == excel_value);
                        if (checkSoCT)
                        {
                            ketqua = "Số chứng từ đã tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "AccountingObjectBankAccount":
                        var bankChi = _IAccountingObjectBankAccountService.Query.FirstOrDefault(t => t.BankAccount == excel_value);
                        if (bankChi == null)
                        {
                            ketqua = "Tài khoản nhận không tồn tại; ";
                            kq = false;
                        }
                        else
                        {
                            var index = list_config.FindIndex(t => t.value.ToUpper() == "Mã đối tượng".ToUpper());
                            if (xLRow.Cell(index + 1).Value != null)
                            {
                                var dt = xLRow.Cell(index + 1).Value.ToString();
                                var dtobj = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == dt);
                                if (dtobj != null)
                                {
                                    if (bankChi.AccountingObjectID != dtobj.ID)
                                    {
                                        ketqua = "Tài khoản nhận không khớp với mã đối tượng; ";
                                        kq = false;
                                    }
                                }
                            }
                        }
                        break;
                    case "CreditCardNumber":
                        var checksothe = _ICreditCardService.Query.Any(t => t.CreditCardNumber == excel_value);
                        if (!checksothe)
                        {
                            ketqua = "Số thẻ không tồn tại; ";
                            kq = false;
                        }
                        break;

                }
                if (!kq)
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    xLCell.Comment.AddText(ketqua);
                }
            }
            return ketqua;
        }
        private string CheckChuyenTienNB(IXLRow xLRow, int index_cell, COLUMN_MODEL column)
        {
            IMBInternalTransferService _IMBInternalTransferService = IoC.Resolve<IMBInternalTransferService>();
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            bool kq = true;
            string ketqua = "";
            var xLCell = xLRow.Cell(index_cell);
            string excel_value = xLCell.Value.ToString();
            string db_column = column.key;
            if (!string.IsNullOrEmpty(excel_value))
            {
                switch (db_column)
                {
                    case "No":
                        var checkSoCT = _IMBInternalTransferService.Query.Any(t => t.No == excel_value);
                        if (checkSoCT)
                        {
                            ketqua = "Số chứng từ đã tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "TKNHChuyen":
                        var bankChuyen = _IBankAccountDetailService.Query.Any(t => t.BankAccount == excel_value);
                        if (!bankChuyen)
                        {
                            ketqua = "Tài khoản gửi không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "TKNHNhan":
                        var bankNhan = _IBankAccountDetailService.Query.Any(t => t.BankAccount == excel_value);
                        if (!bankNhan)
                        {
                            ketqua = "Tài khoản nhận không tồn tại; ";
                            kq = false;
                        }
                        else
                        {
                            var index = list_config.FindIndex(t => t.value.ToUpper() == "Từ TK ngân hàng".ToUpper());
                            var tkchuyen_gt = xLRow.Cell(index + 1).Value.ToString();
                            if (tkchuyen_gt == excel_value)
                            {
                                ketqua = "Tài khoản gửi và tài khoản nhận phải khác nhau; ";
                                kq = false;
                            }
                        }
                        break;
                }
                if (!kq)
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    xLCell.Comment.AddText(ketqua);
                }
            }
            return ketqua;
        }
        private string CheckKiemKeQuy(IXLCell xLCell, COLUMN_MODEL column)
        {
            IMCAuditService _IMCAuditService = IoC.Resolve<IMCAuditService>();
            bool kq = true;
            string ketqua = "";
            string excel_value = xLCell.Value.ToString();
            string db_column = column.key;
            if (!string.IsNullOrEmpty(excel_value))
            {
                switch (db_column)
                {
                    case "No":
                        var checkSoCT = _IMCAuditService.Query.Any(t => t.No == excel_value);
                        if (checkSoCT)
                        {
                            ketqua = "Số chứng từ đã tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "ValueOfMoney":
                        List<string> list_menhgia = new List<string>() { "1000", "2000", "5000", "10000", "20000", "50000", "100000", "200000", "500000" };
                        var checkMenhGia = list_menhgia.Any(t => t == excel_value);
                        if (!checkMenhGia)
                        {
                            ketqua = "Mệnh giá tiền không tồn tại; ";
                            kq = false;
                        }
                        break;
                }
            }
            else
            {
                switch (db_column)
                {
                    case "Quantity":
                        ketqua = "Số lượng không được để trống; ";
                        kq = false;
                        break;
                }
            }
            if (!kq)
            {
                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                xLCell.Comment.AddText(ketqua);
            }
            return ketqua;
        }
        private string CheckDonMuaHang(IXLCell xLCell, COLUMN_MODEL column)
        {
            IPPOrderService _IPPOrderService = IoC.Resolve<IPPOrderService>();
            bool kq = true;
            string ketqua = "";
            string excel_value = xLCell.Value.ToString();
            string db_column = column.key;
            if (!string.IsNullOrEmpty(excel_value))
            {
                switch (db_column)
                {
                    case "No":
                        var checkSoCT = _IPPOrderService.Query.Any(t => t.No == excel_value);
                        if (checkSoCT)
                        {
                            ketqua = "Số chứng từ đã tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "DeliverDate":
                        var checkdate = IsDate(excel_value);
                        if (!checkdate)
                        {
                            kq = false;
                            ketqua = column.value + " sai định dạng ngày tháng; ";
                        }
                        else
                        {
                            var checkmoctg = checkMocTG(excel_value);
                            if (!checkmoctg)
                            {
                                kq = false;
                                ketqua = column.value + " chỉ có nhập từ năm 1970 hoặc lớn hơn; ";
                            }
                        }
                        break;
                }
                if (!kq)
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    xLCell.Comment.AddText(ketqua);
                }
            }
            return ketqua;
        }
        private string CheckMuaHangQuaKho(IXLRow xLRow, int index_cell, COLUMN_MODEL column)
        {
            IPPServiceService _IPPServiceService = IoC.Resolve<IPPServiceService>();
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            IAccountingObjectBankAccountService _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            IRepositoryService _IRepositoryService = IoC.Resolve<IRepositoryService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            IPPInvoiceService _IPPInvoiceService = IoC.Resolve<IPPInvoiceService>();
            ICreditCardService _ICreditCardService = IoC.Resolve<ICreditCardService>();
            IRSInwardOutwardService _IRSInwardOutwardService = IoC.Resolve<IRSInwardOutwardService>();
            bool kq = true;
            string ketqua = "";
            var xLCell = xLRow.Cell(index_cell);
            string excel_value = xLCell.Value.ToString();
            string db_column = column.key;
            if (!string.IsNullOrEmpty(excel_value))
            {
                switch (db_column)
                {
                    case "No":
                        var checkSoCT = _IPPInvoiceService.Query.Any(t => t.No == excel_value);
                        if (checkSoCT)
                        {
                            ketqua = column.value + " đã tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "InwardNo":
                        var checkSoCTNhapKho = _IRSInwardOutwardService.Query.Any(t => t.No == excel_value);
                        if (checkSoCTNhapKho)
                        {
                            ketqua = column.value + " đã tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaNhanVien":
                        var doituong = _IAccountingObjectService.Query.Any(t => t.AccountingObjectCode == excel_value);
                        if (!doituong)
                        {
                            ketqua = "Mã nhân viên không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaTKChi":
                        var tkchi = _IBankAccountDetailService.Query.Any(t => t.BankAccount == excel_value);
                        if (!tkchi)
                        {
                            ketqua = "Tài khoản NH chi không tồn tại; ";
                            kq = false;
                        }
                        break;
                    //case "MaTKHuong":
                    //    var tkhuong = _IAccountingObjectBankAccountService.Query.Any(t => t.BankAccount == excel_value);
                    //    if (!tkhuong)
                    //    {
                    //        ketqua = "Tài khoản hưởng không tồn tại; ";
                    //        kq = false;
                    //    }
                    //    break;
                    case "MaTKHuong":
                        var bankChi = _IAccountingObjectBankAccountService.Query.FirstOrDefault(t => t.BankAccount == excel_value);
                        if (bankChi == null)
                        {
                            ketqua = "Tài khoản nhận không tồn tại; ";
                            kq = false;
                        }
                        else
                        {
                            var index = list_config.FindIndex(t => t.value.ToUpper() == "Mã Nhà cung cấp".ToUpper());
                            if (xLRow.Cell(index + 1).Value != null)
                            {
                                var dt = xLRow.Cell(index + 1).Value.ToString();
                                var dtobj = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == dt);
                                if (dtobj != null)
                                {
                                    if (bankChi.AccountingObjectID != dtobj.ID)
                                    {
                                        ketqua = "Tài khoản nhận không khớp với Mã Nhà cung cấp; ";
                                        kq = false;
                                    }
                                }
                            }
                        }
                        break;
                    case "MaKho":
                        var kho = _IRepositoryService.Query.Any(t => t.RepositoryCode == excel_value);
                        if (!kho)
                        {
                            ketqua = "Mã kho không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "PhuongThucTT":
                        List<string> list_phuongthuc = new List<string>() { "210", "260", "261", "262", "263", "264" };
                        var checkphuongthuc = list_phuongthuc.Any(t => t == excel_value);
                        if (!checkphuongthuc)
                        {
                            ketqua = "Phương thức thanh toán không hợp lệ; ";
                            kq = false;
                        }
                        break;
                    case "CreditCardNumber":
                        var checksothe = _ICreditCardService.Query.Any(t => t.CreditCardNumber == excel_value);
                        if (!checksothe)
                        {
                            ketqua = "Số thẻ không tồn tại; ";
                            kq = false;
                        }
                        break;
                }
                if (!kq)
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    xLCell.Comment.AddText(ketqua);
                }
            }
            else
            {
                var index = list_config.FindIndex(t => t.value.ToUpper() == "Phương thức thanh toán".ToUpper());
                var pthuc = xLRow.Cell(index + 1).Value.ToString();
                switch (db_column)
                {
                    case "MaTKChi":
                        if (pthuc == "261" || pthuc == "262" || pthuc == "264")
                        {
                            ketqua = "Tài khoản NH chi bắt buộc nhập; ";
                            kq = false;
                        }
                        break;
                    case "MaTKHuong":
                        if (pthuc == "261" || pthuc == "262" || pthuc == "263")
                        {
                            ketqua = "Tài khoản hưởng bắt buộc nhập; ";
                            kq = false;
                        }
                        break;
                    case "CreditCardNumber":
                        if (pthuc == "263")
                        {
                            ketqua = "Số thẻ tín dụng bắt buộc nhập; ";
                            kq = false;
                        }
                        break;
                    case "No":
                        if (pthuc != "210")
                        {
                            ketqua = "Số chứng từ thanh toán bắt buộc nhập với phương thức khác 210; ";
                            kq = false;
                        }
                        break;
                    default:
                        break;
                }
                if (!kq)
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    xLCell.Comment.AddText(ketqua);
                }

            }
            return ketqua;
        }
        private string CheckMuaHangKhongQuaKho(IXLRow xLRow, int index_cell, COLUMN_MODEL column)
        {
            IPPServiceService _IPPServiceService = IoC.Resolve<IPPServiceService>();
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            IAccountingObjectBankAccountService _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            IRepositoryService _IRepositoryService = IoC.Resolve<IRepositoryService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            IPPInvoiceService _IPPInvoiceService = IoC.Resolve<IPPInvoiceService>();
            ICreditCardService _ICreditCardService = IoC.Resolve<ICreditCardService>();
            bool kq = true;
            string ketqua = "";
            var xLCell = xLRow.Cell(index_cell);
            string excel_value = xLCell.Value.ToString();
            string db_column = column.key;
            if (!string.IsNullOrEmpty(excel_value))
            {
                switch (db_column)
                {
                    case "No":
                        var checkSoCT = _IPPInvoiceService.Query.Any(t => t.No == excel_value);
                        if (checkSoCT)
                        {
                            ketqua = "Số chứng từ đã tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaNCC":
                        var ncc = _IAccountingObjectService.Query.Any(t => t.AccountingObjectCode == excel_value);
                        if (!ncc)
                        {
                            ketqua = "Mã nhà cung cấp không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaNhanVien":
                        var doituong = _IAccountingObjectService.Query.Any(t => t.AccountingObjectCode == excel_value);
                        if (!doituong)
                        {
                            ketqua = "Mã nhân viên không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "SoTKChi":
                        var tkchi = _IBankAccountDetailService.Query.Any(t => t.BankAccount == excel_value);
                        if (!tkchi)
                        {
                            ketqua = "Tài khoản NH chi không tồn tại; ";
                            kq = false;
                        }
                        break;
                    //case "SoTKHuong":
                    //    var tkhuong = _IAccountingObjectBankAccountService.Query.Any(t => t.BankAccount == excel_value);
                    //    if (!tkhuong)
                    //    {
                    //        ketqua = "Tài khoản hưởng không tồn tại; ";
                    //        kq = false;
                    //    }
                    //    break;
                    case "SoTKHuong":
                        var bankChi = _IAccountingObjectBankAccountService.Query.FirstOrDefault(t => t.BankAccount == excel_value);
                        if (bankChi == null)
                        {
                            ketqua = "Tài khoản hưởng không tồn tại; ";
                            kq = false;
                        }
                        else
                        {
                            var index = list_config.FindIndex(t => t.value.ToUpper() == "Mã Nhà cung cấp".ToUpper());
                            if (xLRow.Cell(index + 1).Value != null)
                            {
                                var dt = xLRow.Cell(index + 1).Value.ToString();
                                var dtobj = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == dt);
                                if (dtobj != null)
                                {
                                    if (bankChi.AccountingObjectID != dtobj.ID)
                                    {
                                        ketqua = "Tài khoản hưởng không khớp với Mã Nhà cung cấp; ";
                                        kq = false;
                                    }
                                }
                            }
                        }
                        break;
                    case "MaKho":
                        var kho = _IRepositoryService.Query.Any(t => t.RepositoryCode == excel_value);
                        if (!kho)
                        {
                            ketqua = "Mã kho không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "PhuongThucTT":
                        List<string> list_phuongthuc = new List<string>() { "210", "260", "261", "262", "263", "264" };
                        var checkphuongthuc = list_phuongthuc.Any(t => t == excel_value);
                        if (!checkphuongthuc)
                        {
                            ketqua = "Phương thức thanh toán không hợp lệ; ";
                            kq = false;
                        }
                        break;
                    case "CreditCardNumber":
                        var checksothe = _ICreditCardService.Query.Any(t => t.CreditCardNumber == excel_value);
                        if (!checksothe)
                        {
                            ketqua = "Số thẻ không tồn tại; ";
                            kq = false;
                        }
                        break;
                }
                if (!kq)
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    xLCell.Comment.AddText(ketqua);
                }
            }
            else
            {
                var index = list_config.FindIndex(t => t.value.ToUpper() == "Phương thức thanh toán".ToUpper());
                var pthuc = xLRow.Cell(index + 1).Value.ToString();
                switch (db_column)
                {
                    case "SoTKChi":
                        if (pthuc == "261" || pthuc == "262" || pthuc == "264")
                        {
                            ketqua = "Tài khoản NH chi bắt buộc nhập; ";
                            kq = false;
                        }
                        break;
                    case "CreditCardNumber":
                        if (pthuc == "263")
                        {
                            ketqua = "Số thẻ tín dụng bắt buộc nhập; ";
                            kq = false;
                        }
                        break;
                    default:
                        break;
                }
                if (!kq)
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    xLCell.Comment.AddText(ketqua);
                }
            }
            return ketqua;
        }
        private string CheckMuaDichVu(IXLRow xLRow, int index_cell, COLUMN_MODEL column)
        {
            IPPServiceService _IPPServiceService = IoC.Resolve<IPPServiceService>();
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            IAccountingObjectBankAccountService _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            IRepositoryService _IRepositoryService = IoC.Resolve<IRepositoryService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            ICreditCardService _ICreditCardService = IoC.Resolve<ICreditCardService>();
            bool kq = true;
            string ketqua = "";
            var xLCell = xLRow.Cell(index_cell);
            string excel_value = xLCell.Value.ToString();
            string db_column = column.key;
            if (!string.IsNullOrEmpty(excel_value))
            {
                switch (db_column)
                {
                    case "No":
                        var checkSoCT = _IPPServiceService.Query.Any(t => t.No == excel_value);
                        if (checkSoCT)
                        {
                            ketqua = "Số chứng từ đã tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaNCC":
                        var ncc = _IAccountingObjectService.Query.Any(t => t.AccountingObjectCode == excel_value);
                        if (!ncc)
                        {
                            ketqua = "Mã nhà cung cấp không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "TKNHChi":
                        var tkchi = _IBankAccountDetailService.Query.Any(t => t.BankAccount == excel_value);
                        if (!tkchi)
                        {
                            ketqua = "Tài khoản NH chi không tồn tại; ";
                            kq = false;
                        }
                        break;
                    //case "TKHuong":
                    //    var tkhuong = _IAccountingObjectBankAccountService.Query.Any(t => t.BankAccount == excel_value);
                    //    if (!tkhuong)
                    //    {
                    //        ketqua = "Tài khoản hưởng không tồn tại; ";
                    //        kq = false;
                    //    }
                    //    break;
                    case "TKHuong":
                        var bankChi = _IAccountingObjectBankAccountService.Query.FirstOrDefault(t => t.BankAccount == excel_value);
                        if (bankChi == null)
                        {
                            ketqua = "Tài khoản hưởng không tồn tại; ";
                            kq = false;
                        }
                        else
                        {
                            var index = list_config.FindIndex(t => t.value.ToUpper() == "Mã Nhà cung cấp".ToUpper());
                            if (xLRow.Cell(index + 1).Value != null)
                            {
                                var dt = xLRow.Cell(index + 1).Value.ToString();
                                var dtobj = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == dt);
                                if (dtobj != null)
                                {
                                    if (bankChi.AccountingObjectID != dtobj.ID)
                                    {
                                        ketqua = "Tài khoản hưởng không khớp với Mã Nhà cung cấp; ";
                                        kq = false;
                                    }
                                }
                            }
                        }
                        break;
                    case "MaDichVu":
                        var dichvu = _IMaterialGoodsService.Query.Any(t => t.MaterialGoodsCode == excel_value);
                        if (!dichvu)
                        {
                            ketqua = "Mã dịch vụ không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "PhuongThucTT":
                        List<string> list_phuongthuc = new List<string>() { "240", "116", "126", "133", "143", "173" };
                        var checkphuongthuc = list_phuongthuc.Any(t => t == excel_value);
                        if (!checkphuongthuc)
                        {
                            ketqua = "Phương thức thanh toán không hợp lệ; ";
                            kq = false;
                        }
                        break;
                    case "CreditCardNumber":
                        var checksothe = _ICreditCardService.Query.Any(t => t.CreditCardNumber == excel_value);
                        if (!checksothe)
                        {
                            ketqua = "Số thẻ không tồn tại; ";
                            kq = false;
                        }
                        break;
                }
                if (!kq)
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    xLCell.Comment.AddText(ketqua);
                }
            }
            else
            {
                var index = list_config.FindIndex(t => t.value.ToUpper() == "Phương thức thanh toán".ToUpper());
                var pthuc = xLRow.Cell(index + 1).Value.ToString();
                switch (db_column)
                {
                    case "TKNHChi":
                        if (pthuc == "126" || pthuc == "133" || pthuc == "143")
                        {
                            ketqua = "Tài khoản NH chi bắt buộc nhập; ";
                            kq = false;
                        }
                        break;
                    case "CreditCardNumber":
                        if (pthuc == "173")
                        {
                            ketqua = "Số thẻ tín dụng bắt buộc nhập; ";
                            kq = false;
                        }
                        break;
                    default:
                        break;
                }
                if (!kq)
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    xLCell.Comment.AddText(ketqua);
                }
            }
            return ketqua;
        }
        private string CheckBaoGia(IXLCell xLCell, COLUMN_MODEL column)
        {
            ISAQuoteService _ISAQuoteService = IoC.Resolve<ISAQuoteService>();
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            bool kq = true;
            string ketqua = "";
            string excel_value = xLCell.Value.ToString();
            string db_column = column.key;
            if (!string.IsNullOrEmpty(excel_value))
            {
                switch (db_column)
                {
                    case "No":
                        var checkSoCT = _ISAQuoteService.Query.Any(t => t.No == excel_value);
                        if (checkSoCT)
                        {
                            ketqua = "Số chứng từ đã tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaKhachHang":
                        var khachhang = _IAccountingObjectService.Query.Any(t => t.AccountingObjectCode == excel_value);
                        if (!khachhang)
                        {
                            ketqua = "Mã khách hàng không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "FinalDate":
                        var checkdate = IsDate(excel_value);
                        if (!checkdate)
                        {
                            kq = false;
                            ketqua = column.value + " sai định dạng ngày tháng; ";
                        }
                        else
                        {
                            var checkmoctg = checkMocTG(excel_value);
                            if (!checkmoctg)
                            {
                                kq = false;
                                ketqua = column.value + " chỉ có nhập từ năm 1970 hoặc lớn hơn; ";
                            }
                        }
                        break;
                }
                if (!kq)
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    xLCell.Comment.AddText(ketqua);
                }
            }
            return ketqua;
        }
        private string CheckDonDatHang(IXLCell xLCell, COLUMN_MODEL column)
        {
            ISAOrderService _ISAOrderService = IoC.Resolve<ISAOrderService>();
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            bool kq = true;
            string ketqua = "";
            string excel_value = xLCell.Value.ToString();
            string db_column = column.key;
            if (!string.IsNullOrEmpty(excel_value))
            {
                switch (db_column)
                {
                    case "No":
                        var checkSoCT = _ISAOrderService.Query.Any(t => t.No == excel_value);
                        if (checkSoCT)
                        {
                            ketqua = "Số chứng từ đã tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaKhachHang":
                        var khachhang = _IAccountingObjectService.Query.Any(t => t.AccountingObjectCode == excel_value);
                        if (!khachhang)
                        {
                            ketqua = "Mã khách hàng không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "DeliveDate":
                        var checkdate = IsDate(excel_value);
                        if (!checkdate)
                        {
                            kq = false;
                            ketqua = column.value + " sai định dạng ngày tháng; ";
                        }
                        else
                        {
                            var checkmoctg = checkMocTG(excel_value);
                            if (!checkmoctg)
                            {
                                kq = false;
                                ketqua = column.value + " chỉ có nhập từ năm 1970 hoặc lớn hơn; ";
                            }
                        }
                        break;
                }
                if (!kq)
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    xLCell.Comment.AddText(ketqua);
                }
            }
            return ketqua;
        }
        private string CheckBHChuaTT(IXLRow xLRow, int index_cell, COLUMN_MODEL column)
        {
            ISAInvoiceService _ISAInvoiceService = IoC.Resolve<ISAInvoiceService>();
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            IAccountingObjectBankAccountService _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            bool kq = true;
            string ketqua = "";
            var xLCell = xLRow.Cell(index_cell);
            string excel_value = xLCell.Value.ToString();
            string db_column = column.key;
            if (!string.IsNullOrEmpty(excel_value))
            {
                switch (db_column)
                {
                    case "No":
                        var checkSoCT = _ISAInvoiceService.Query.Any(t => t.No == excel_value);
                        if (checkSoCT)
                        {
                            ketqua = "Số chứng từ đã tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaKhachHang":
                        var khachhang = _IAccountingObjectService.Query.Any(t => t.AccountingObjectCode == excel_value);
                        if (!khachhang)
                        {
                            ketqua = "Mã khách hàng không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "DueDate":
                        var checkdate = IsDate(excel_value);
                        if (!checkdate)
                        {
                            kq = false;
                            ketqua = column.value + " sai định dạng ngày tháng; ";
                        }
                        else
                        {
                            var checkmoctg = checkMocTG(excel_value);
                            if (!checkmoctg)
                            {
                                kq = false;
                                ketqua = column.value + " chỉ có nhập từ năm 1970 hoặc lớn hơn; ";
                            }
                        }
                        break;
                    case "AccountingObjectBankAccount":
                        var checkNH = _IAccountingObjectBankAccountService.Query.FirstOrDefault(t => t.BankAccount == excel_value);
                        if (checkNH == null)
                        {
                            kq = false;
                            ketqua = column.value + " không tồn tại; ";
                        }
                        else
                        {
                            var index = list_config.FindIndex(t => t.value.ToUpper() == "Mã khách hàng".ToUpper());
                            if (xLRow.Cell(index + 1).Value != null)
                            {
                                var dt = xLRow.Cell(index + 1).Value.ToString();
                                var dtobj = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == dt);
                                if (dtobj != null)
                                {
                                    if (checkNH.AccountingObjectID != dtobj.ID)
                                    {
                                        ketqua = "Tài khoản hưởng không khớp với Mã Nhà cung cấp; ";
                                        kq = false;
                                    }
                                }
                            }
                        }
                        break;
                    case "PaymentMethod":
                        List<string> list_pay = new List<string>() { "Tiền mặt", "Chuyển khoản", "TM/CK" };
                        if (!list_pay.Any(t => t.ToUpper() == excel_value.ToUpper()))
                        {
                            ketqua = column.value + " không hợp lệ; ";
                            kq = false;
                        }
                        break;
                    case "Exported":
                        if (excel_value != "0" && excel_value != "1")
                        {
                            ketqua = column.value + " không hợp lệ; ";
                            kq = false;
                        }
                        break;
                    case "IsPromotion":
                        if (excel_value != "0" && excel_value != "1")
                        {
                            ketqua = column.value + " không hợp lệ; ";
                            kq = false;
                        }
                        break;
                    case "InvoiceTemplate":
                        string kq_temp = checkMauSoHD(column, xLRow, excel_value);
                        if (!string.IsNullOrEmpty(kq_temp))
                        {
                            ketqua = kq_temp;
                            kq = false;
                        }
                        break;
                    case "InvoiceSeries":
                        string kq_Ser = checkKyhieuHD(column, xLRow, excel_value);
                        if (!string.IsNullOrEmpty(kq_Ser))
                        {
                            ketqua = kq_Ser;
                            kq = false;
                        }

                        break;
                    case "InvoiceNo":
                        if (excel_value.Length != 7)
                        {
                            ketqua = column.value + " chỉ được nhập 7 ký tự";
                            kq = false;
                        }
                        else
                        {

                            int n;
                            bool isNumeric = int.TryParse(excel_value, out n);
                            if (!isNumeric)
                            {
                                ketqua = column.value + " chỉ được phép nhập ký tự số!";
                                kq = false;
                            }
                            else
                            {
                                string kq_no = checkSoNgayHD(column, xLRow, excel_value, _ISAInvoiceService, "InvoiceNo");
                                if (!string.IsNullOrEmpty(kq_no))
                                {
                                    ketqua = kq_no;
                                    kq = false;
                                }
                            }

                        }

                        break;
                    case "InvoiceDate":
                        string kq_date = checkSoNgayHD(column, xLRow, excel_value, _ISAInvoiceService, "InvoiceDate");
                        if (!string.IsNullOrEmpty(kq_date))
                        {
                            ketqua = kq_date;
                            kq = false;
                        }
                        break;
                }
                if (!kq)
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    xLCell.Comment.AddText(ketqua);
                }
            }
            return ketqua;
        }
        private string checkMauSoHD(COLUMN_MODEL column, IXLRow xLRow, string excel_value)
        {
            string ketqua = "";
            var Template_check = Utils.ListTT153Report.Where(t => t.InvoiceTemplate == excel_value);
            if (Template_check == null || Template_check.Count() == 0)
            {
                ketqua = column.value + " không tồn tại; ";
            }
            else
            {
                var index = list_config.FindIndex(t => t.value.ToUpper() == "Ký hiệu HĐ".ToUpper());
                var xLRowValue = xLRow.Cell(index + 1).Value;
                if (xLRowValue != null)
                {
                    var gt = xLRowValue.ToString();
                    var check_ser = Template_check.Any(t => t.InvoiceSeries == gt);
                    if (!check_ser)
                    {
                        ketqua = column.value + " không khớp với ký hiệu HĐ được nhập";
                    }
                }
            }
            return ketqua;
        }
        private string checkKyhieuHD(COLUMN_MODEL column, IXLRow xLRow, string excel_value)
        {
            string ketqua = "";
            var Series_check = Utils.ListTT153Report.FirstOrDefault(t => t.InvoiceSeries == excel_value);
            if (Series_check == null)
            {
                ketqua = column.value + " không tồn tại; ";
            }
            else
            {
                var index = list_config.FindIndex(t => t.value.ToUpper() == "Mẫu số HĐ".ToUpper());
                var xLRowValue = xLRow.Cell(index + 1).Value;
                if (xLRowValue != null)
                {
                    var gt = xLRowValue.ToString();
                    var check_temp = Series_check.InvoiceTemplate == gt;
                    if (!check_temp)
                    {
                        ketqua = column.value + " không khớp với mẫu số HĐ được nhập";
                    }
                }
            }
            return ketqua;
        }
        private string checkSoNgayHD(COLUMN_MODEL column, IXLRow xLRow, string excel_value, ISAInvoiceService _ISAInvoiceService, string type)
        {
            string ketqua = "";
            var index_ser = list_config.FindIndex(t => t.value.ToUpper() == "Ký hiệu HĐ".ToUpper());
            var index_mauSo = list_config.FindIndex(t => t.value.ToUpper() == "Mẫu số HĐ".ToUpper());
            var index_No = list_config.FindIndex(t => t.value.ToUpper() == "Số hóa đơn".ToUpper());
            var gt_ser = xLRow.Cell(index_ser + 1).Style.Fill.BackgroundColor.Color.Name;
            var gt_mauSo = xLRow.Cell(index_mauSo + 1).Style.Fill.BackgroundColor.Color.Name;
            var gt_No = xLRow.Cell(index_No + 1).Style.Fill.BackgroundColor.Color.Name;
            string ten_truong = "";
            if (gt_ser == "Transparent" && gt_mauSo == "Transparent")
            {
                var Value_ser = xLRow.Cell(index_ser + 1).Value;
                var Value_mauso = xLRow.Cell(index_mauSo + 1).Value;
                var value_no = xLRow.Cell(index_No + 1).Value;
                if (Value_ser != null && Value_mauso != null)
                {
                    var tt153 = Utils.ListTT153PublishInvoiceDetail.FirstOrDefault(t => t.InvoiceSeries == Value_ser.ToString() && t.InvoiceTemplate == Value_mauso.ToString());
                    var md = Utils.ListTT153PublishInvoiceDetail.Where(x => x.InvoiceTypeID == tt153.InvoiceTypeID && x.InvoiceTemplate == tt153.InvoiceTemplate && x.InvoiceForm == tt153.InvoiceForm).ToList();
                    if (md != null && md.Count > 0)
                    {
                        if (type == "InvoiceNo")
                        {
                            ten_truong = " số hóa đơn";
                            int a = int.Parse(excel_value);
                            int b = md.Select(n => int.Parse(n.FromNo)).Min();
                            int c = md.Select(n => int.Parse(n.ToNo)).Max();
                            var first_tt153 = md.FirstOrDefault();
                            List<SAInvoice> ls = _ISAInvoiceService.Query.Where(x => x.InvoiceForm == first_tt153.InvoiceForm && x.InvoiceTypeID == first_tt153.InvoiceTypeID && x.InvoiceTemplate == Value_mauso.ToString() && x.InvoiceNo != null && x.InvoiceNo != "" && x.InvoiceNo == excel_value).ToList();
                            if (a < b || a > c)
                            {
                                ketqua = "Số hóa đơn chưa được thông báo phát hành!";
                            }
                            else if (ls.Count > 0)
                            {
                                ketqua = "Số hóa đơn đã tồn tại!!";
                            }
                            IPPDiscountReturnService _IPPDiscountReturnService = IoC.Resolve<IPPDiscountReturnService>();
                            List<PPDiscountReturn> lstpDr = _IPPDiscountReturnService.Query.Where(x => x.InvoiceForm == first_tt153.InvoiceForm && x.InvoiceTypeID == first_tt153.InvoiceTypeID && x.InvoiceTemplate == Value_mauso.ToString() && x.InvoiceNo != null && x.InvoiceNo != "" && x.InvoiceNo == excel_value).ToList();
                            if (a < b || a > c || lstpDr != null)
                            {
                                if (a < b || a > c)
                                {
                                    ketqua = "Số hóa đơn chưa được thông báo phát hành!";
                                }
                                else if (lstpDr.Count > 0)
                                {
                                    ketqua = "Số hóa đơn đã tồn tại!!";
                                }
                            }

                            ISABillService _ISABillService = IoC.Resolve<ISABillService>();
                            List<SABill> ls_Sabill = _ISABillService.Query.Where(x => x.InvoiceForm == first_tt153.InvoiceForm && x.InvoiceTypeID == first_tt153.InvoiceTypeID && x.InvoiceTemplate == Value_mauso.ToString() && x.InvoiceNo != null && x.InvoiceNo != "" && x.InvoiceNo == excel_value).ToList();
                            if (a < b || a > c || ls_Sabill != null)
                            {
                                if (a < b || a > c)
                                {
                                    ketqua = "Số hóa đơn chưa được thông báo phát hành!";
                                }
                                else if (ls_Sabill.Count > 0)
                                {
                                    ketqua = "Số hóa đơn đã tồn tại!!";
                                }
                            }


                        }
                        else
                        {
                            ten_truong = " ngày hóa đơn";
                            if (value_no != null)
                            {
                                if (gt_No == "Transparent")
                                {
                                    DateTime time = md[0].StartUsing;
                                    if (md != null)
                                    {
                                        if (new DateTime(time.Year, time.Month, time.Day) > Convert.ToDateTime(excel_value))
                                        {
                                            ketqua = "Ngày hóa đơn phải lớn hơn hoặc bằng ngày bắt đầu sử dụng trên thông báo phát hành";
                                        }
                                    }
                                }
                                else
                                {
                                    ketqua = "Số hóa đơn không hợp lệ, không kiểm tra được " + ten_truong;
                                }
                            }
                            else
                            {
                                ketqua = "Số hóa đơn để trống không kiểm tra được ngày hóa đơn";
                            }
                        }
                    }
                    else
                    {
                        ketqua = "Mẫu số HĐ hoặc ký hiệu HĐ không hợp lệ, không kiểm tra được" + ten_truong;
                    }
                }
            }
            else
            {
                ketqua = "Mẫu số HĐ hoặc ký hiệu HĐ không hợp lệ, không kiểm tra được" + ten_truong;
            }
            return ketqua;
        }
        private bool checkaccount_number(string acc_num)
        {
            IAccountService _IAccountService = IoC.Resolve<IAccountService>();
            var check = _IAccountService.Query.Any(t => t.IsParentNode == false && t.AccountNumber == acc_num);
            return check;
        }
        private string CheckBHTTNgay(IXLRow xLRow, int index_cell, COLUMN_MODEL column)
        {
            ISAInvoiceService _ISAInvoiceService = IoC.Resolve<ISAInvoiceService>();
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            IAccountingObjectBankAccountService _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            bool kq = true;
            string ketqua = "";
            var xLCell = xLRow.Cell(index_cell);
            string excel_value = xLCell.Value.ToString();
            string db_column = column.key;
            if (!string.IsNullOrEmpty(excel_value))
            {
                switch (db_column)
                {
                    case "No":
                        var checkSoCT = _ISAInvoiceService.Query.Any(t => t.No == excel_value);
                        if (checkSoCT)
                        {
                            ketqua = "Số chứng từ đã tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MNo":
                        var checMNo = _ISAInvoiceService.Query.Any(t => t.MNo == excel_value);
                        if (checMNo)
                        {
                            ketqua = column.value + " đã tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaKhachHang":
                        var khachhang = _IAccountingObjectService.Query.Any(t => t.AccountingObjectCode == excel_value);
                        if (!khachhang)
                        {
                            ketqua = "Mã khách hàng không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "AccountingObjectBankAccount":
                        var checkNH = _IAccountingObjectBankAccountService.Query.FirstOrDefault(t => t.BankAccount == excel_value);
                        if (checkNH == null)
                        {
                            kq = false;
                            ketqua = column.value + " không tồn tại; ";
                        }
                        else
                        {
                            var index = list_config.FindIndex(t => t.value.ToUpper() == "Mã khách hàng".ToUpper());
                            if (xLRow.Cell(index + 1).Value != null)
                            {
                                var dt = xLRow.Cell(index + 1).Value.ToString();
                                var dtobj = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == dt);
                                if (dtobj != null)
                                {
                                    if (checkNH.AccountingObjectID != dtobj.ID)
                                    {
                                        ketqua = "Tài khoản hưởng không khớp với Mã Nhà cung cấp; ";
                                        kq = false;
                                    }
                                }
                            }
                        }
                        break;
                    case "PaymentMethod":
                        List<string> list_pay = new List<string>() { "Tiền mặt", "Chuyển khoản", "TM/CK" };
                        if (!list_pay.Any(t => t.ToUpper() == excel_value.ToUpper()))
                        {
                            ketqua = column.value + " không hợp lệ; ";
                            kq = false;
                        }
                        break;
                    case "Exported":
                        if (excel_value != "0" && excel_value != "1")
                        {
                            ketqua = column.value + " không hợp lệ; ";
                            kq = false;
                        }
                        break;
                    case "IsPromotion":
                        if (excel_value != "0" && excel_value != "1")
                        {
                            ketqua = column.value + " không hợp lệ; ";
                            kq = false;
                        }
                        break;
                    case "MaTKNopTien":
                        var checktknoptien = _IBankAccountDetailService.Query.Any(t => t.BankAccount == excel_value);
                        if (!checktknoptien)
                        {
                            ketqua = column.value + " không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "PhuongThucTT":
                        if (excel_value != "321" && excel_value != "322")
                        {
                            ketqua = column.value + " không hợp lệ; ";
                            kq = false;
                        }
                        break;
                    case "InvoiceTemplate":
                        string kq_temp = checkMauSoHD(column, xLRow, excel_value);
                        if (!string.IsNullOrEmpty(kq_temp))
                        {
                            ketqua = kq_temp;
                            kq = false;
                        }
                        break;
                    case "InvoiceSeries":
                        string kq_Ser = checkKyhieuHD(column, xLRow, excel_value);
                        if (!string.IsNullOrEmpty(kq_Ser))
                        {
                            ketqua = kq_Ser;
                            kq = false;
                        }

                        break;
                    case "InvoiceNo":
                        if (excel_value.Length != 7)
                        {
                            ketqua = column.value + " chỉ được nhập 7 ký tự";
                            kq = false;
                        }
                        else
                        {

                            int n;
                            bool isNumeric = int.TryParse(excel_value, out n);
                            if (!isNumeric)
                            {
                                ketqua = column.value + " chỉ được phép nhập ký tự số!";
                                kq = false;
                            }
                            else
                            {
                                string kq_no = checkSoNgayHD(column, xLRow, excel_value, _ISAInvoiceService, "InvoiceNo");
                                if (!string.IsNullOrEmpty(kq_no))
                                {
                                    ketqua = kq_no;
                                    kq = false;
                                }
                            }

                        }

                        break;
                    case "InvoiceDate":
                        string kq_date = checkSoNgayHD(column, xLRow, excel_value, _ISAInvoiceService, "InvoiceDate");
                        if (!string.IsNullOrEmpty(kq_date))
                        {
                            ketqua = kq_date;
                            kq = false;
                        }
                        break;
                }
                if (!kq)
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    xLCell.Comment.AddText(ketqua);
                }
            }
            return ketqua;
        }
        private string CheckBHDL(IXLRow xLRow, int index_cell, COLUMN_MODEL column)
        {
            ISAInvoiceService _ISAInvoiceService = IoC.Resolve<ISAInvoiceService>();
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            IAccountingObjectBankAccountService _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            bool kq = true;
            string ketqua = "";
            var xLCell = xLRow.Cell(index_cell);
            string excel_value = xLCell.Value.ToString();
            string db_column = column.key;
            if (!string.IsNullOrEmpty(excel_value))
            {
                switch (db_column)
                {
                    case "No":
                        var checkSoCT = _ISAInvoiceService.Query.Any(t => t.No == excel_value);
                        if (checkSoCT)
                        {
                            ketqua = "Số chứng từ đã tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MNo":
                        var checMNo = _ISAInvoiceService.Query.Any(t => t.MNo == excel_value);
                        if (checMNo)
                        {
                            ketqua = column.value + " đã tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "MaKhachHang":
                        var khachhang = _IAccountingObjectService.Query.Any(t => t.AccountingObjectCode == excel_value);
                        if (!khachhang)
                        {
                            ketqua = "Mã khách hàng không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "AccountingObjectBankAccount":
                        var checkNH = _IAccountingObjectBankAccountService.Query.FirstOrDefault(t => t.BankAccount == excel_value);
                        if (checkNH == null)
                        {
                            kq = false;
                            ketqua = column.value + " không tồn tại; ";
                        }
                        else
                        {
                            var index = list_config.FindIndex(t => t.value.ToUpper() == "Mã khách hàng".ToUpper());
                            if (xLRow.Cell(index + 1).Value != null)
                            {
                                var dt = xLRow.Cell(index + 1).Value.ToString();
                                var dtobj = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == dt);
                                if (dtobj != null)
                                {
                                    if (checkNH.AccountingObjectID != dtobj.ID)
                                    {
                                        ketqua = "Tài khoản hưởng không khớp với Mã Nhà cung cấp; ";
                                        kq = false;
                                    }
                                }
                            }
                        }
                        break;
                    case "PaymentMethod":
                        List<string> list_pay = new List<string>() { "Tiền mặt", "Chuyển khoản", "TM/CK" };
                        if (!list_pay.Any(t => t.ToUpper() == excel_value.ToUpper()))
                        {
                            ketqua = column.value + " không hợp lệ; ";
                            kq = false;
                        }
                        break;
                    case "Exported":
                        if (excel_value != "0" && excel_value != "1")
                        {
                            ketqua = column.value + " không hợp lệ; ";
                            kq = false;
                        }
                        break;
                    case "IsPromotion":
                        if (excel_value != "0" && excel_value != "1")
                        {
                            ketqua = column.value + " không hợp lệ; ";
                            kq = false;
                        }
                        break;
                    case "PhuongThucTT":
                        List<string> list_phuongthuc = new List<string>() { "323", "324", "325" };
                        var checkphuongthuc = list_phuongthuc.Any(t => t == excel_value);
                        if (!checkphuongthuc)
                        {
                            ketqua = "Phương thức thanh toán không hợp lệ; ";
                            kq = false;
                        }
                        break;
                    case "MaTKNopTien":
                        var checktknoptien = _IBankAccountDetailService.Query.Any(t => t.BankAccount == excel_value);
                        if (!checktknoptien)
                        {
                            ketqua = column.value + " không tồn tại; ";
                            kq = false;
                        }
                        break;
                    case "InvoiceTemplate":
                        string kq_temp = checkMauSoHD(column, xLRow, excel_value);
                        if (!string.IsNullOrEmpty(kq_temp))
                        {
                            ketqua = kq_temp;
                            kq = false;
                        }
                        break;
                    case "InvoiceSeries":
                        string kq_Ser = checkKyhieuHD(column, xLRow, excel_value);
                        if (!string.IsNullOrEmpty(kq_Ser))
                        {
                            ketqua = kq_Ser;
                            kq = false;
                        }

                        break;
                    case "InvoiceNo":
                        if (excel_value.Length != 7)
                        {
                            ketqua = column.value + " chỉ được nhập 7 ký tự";
                            kq = false;
                        }
                        else
                        {

                            int n;
                            bool isNumeric = int.TryParse(excel_value, out n);
                            if (!isNumeric)
                            {
                                ketqua = column.value + " chỉ được phép nhập ký tự số!";
                                kq = false;
                            }
                            else
                            {
                                string kq_no = checkSoNgayHD(column, xLRow, excel_value, _ISAInvoiceService, "InvoiceNo");
                                if (!string.IsNullOrEmpty(kq_no))
                                {
                                    ketqua = kq_no;
                                    kq = false;
                                }
                            }

                        }

                        break;
                    case "InvoiceDate":
                        string kq_date = checkSoNgayHD(column, xLRow, excel_value, _ISAInvoiceService, "InvoiceDate");
                        if (!string.IsNullOrEmpty(kq_date))
                        {
                            ketqua = kq_date;
                            kq = false;
                        }
                        break;
                }
            }
            else
            {
                switch (db_column)
                {
                    case "MNo":
                        var index = list_config.FindIndex(t => t.value.ToUpper() == "Phương thức thanh toán".ToUpper());
                        var gt = xLRow.Cell(index + 1).Value == null ? "" : xLRow.Cell(index + 1).Value.ToString();
                        if (gt == "324" || gt == "325")
                        {
                            ketqua = column.value + " bắt buộc nhập; ";
                            kq = false;
                        }
                        break;
                }
            }
            if (!kq)
            {
                xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                xLCell.Comment.AddText(ketqua);
            }
            return ketqua;
        }
        private string CheckLengthCol(IXLCell xLCell, COLUMN_MODEL column)
        {
            string giatri = "";
            string excel_value = xLCell.Value.ToString();
            string db_column = column.key;
            var column_check = ListColumCheck().FirstOrDefault(t => t.key == db_column);
            if (column_check != null)
            {
                if (column_check.check < excel_value.Length)
                {
                    xLCell.Style.Fill.BackgroundColor = XLColor.Red;
                    giatri = column.value + " có độ dài tối đa là " + column_check.check + " ký tự; ";
                    xLCell.Comment.AddText(giatri);
                }
            }
            else
            {
                giatri = "";
            }
            return giatri;
        }
        private List<COLUMN_MODEL> ListColumCheck()
        {
            List<COLUMN_MODEL> dic = new List<COLUMN_MODEL>() {
                new COLUMN_MODEL{key="No",check=25},
                new COLUMN_MODEL{key="MaDoiTuong",check=25},
                new COLUMN_MODEL{key="AccountingObjectName",check=512},
                new COLUMN_MODEL{key="AccountingObjectAddress",check=512},
                new COLUMN_MODEL{key="Payers",check=512},
                new COLUMN_MODEL{key="Reason",check=512},
                new COLUMN_MODEL{key="NumberAttach",check=512},
                new COLUMN_MODEL{key="CurrencyID",check=3},
                new COLUMN_MODEL{key="ExchangeRate",check=36},
                new COLUMN_MODEL{key="DebitAccount",check=25},
                new COLUMN_MODEL{key="CreditAccount",check=25},
                new COLUMN_MODEL{key="AmountOriginal",check=19},
                new COLUMN_MODEL{key="Amount",check=19},
                new COLUMN_MODEL{key="MaKhoanMucCP",check=25},
                new COLUMN_MODEL{key="MaPhongBan",check=25},
                new COLUMN_MODEL{key="MaDoiTuongCP",check=25},
                new COLUMN_MODEL{key="MaHopDong",check=25},
                new COLUMN_MODEL{key="Receiver",check=512},
                new COLUMN_MODEL{key="Description",check=512},
                new COLUMN_MODEL{key="MaMucThuChi",check=25},
                new COLUMN_MODEL{key="MaThongKe",check=25},
                new COLUMN_MODEL{key="VATAccount",check=25},
                new COLUMN_MODEL{key="VATAmountOriginal",check=19},
                new COLUMN_MODEL{key="VATAmount",check=19},
                new COLUMN_MODEL{key="VATRate",check=36},
                new COLUMN_MODEL{key="InvoiceTemplate",check=30},
                new COLUMN_MODEL{key="InvoiceSeries",check=25},
                new COLUMN_MODEL{key="InvoiceNo",check=25},
                new COLUMN_MODEL{key="MaNhomHHDV",check=25},
                new COLUMN_MODEL{key="LoaiGiayBN",check=1},
                new COLUMN_MODEL{key="MaTKChi",check=50},
                new COLUMN_MODEL{key="MaTKNguoiNhan",check=50},
                new COLUMN_MODEL{key="IdentificationNo",check=25},
                new COLUMN_MODEL{key="IssueBy",check=512},
                new COLUMN_MODEL{key="PretaxAmount",check=19},
                new COLUMN_MODEL{key="SoTKHuong",check=50},
                new COLUMN_MODEL{key="CreditCardNumber",check=25},
                new COLUMN_MODEL{key="AccountingObjectBankAccount",check=30},
                new COLUMN_MODEL{key="MaNhanVien",check=25},
                new COLUMN_MODEL{key="PretaxAmountOriginal",check=19},
                new COLUMN_MODEL{key="TKNHChuyen",check=30},
                new COLUMN_MODEL{key="TKNHNhan",check=30},
                new COLUMN_MODEL{key="Summary",check=225},
                new COLUMN_MODEL{key="ValueOfMoney",check=6},
                new COLUMN_MODEL{key="Quantity",check=19},
                new COLUMN_MODEL{key="Description2",check=225},
                new COLUMN_MODEL{key="ShippingPlace",check=512},
                new COLUMN_MODEL{key="MaHang",check=25},
                new COLUMN_MODEL{key="Unit",check=25},
                new COLUMN_MODEL{key="UnitPriceOriginal",check=19},
                new COLUMN_MODEL{key="QuantityReceipt",check=19},
                new COLUMN_MODEL{key="DiscountRate",check=36},
                new COLUMN_MODEL{key="DiscountAmountOriginal",check=19},
                new COLUMN_MODEL{key="DiscountAmount",check=19},
                new COLUMN_MODEL{key="VATDescription",check=512},
                new COLUMN_MODEL{key="MaKhachHang",check=25},
                new COLUMN_MODEL{key="ContactName",check=512},
                new COLUMN_MODEL{key="ContactMobile",check=25},
                new COLUMN_MODEL{key="ContactEmail",check=100},
                new COLUMN_MODEL{key="DeliveryTime",check=225},
                new COLUMN_MODEL{key="GuaranteeDuration",check=225},
                new COLUMN_MODEL{key="DieuKhoanTT",check=25},
                new COLUMN_MODEL{key="UnitPrice",check=19},
                new COLUMN_MODEL{key="DeliveryPlace",check=512},
                new COLUMN_MODEL{key="MaDKTT",check=25},
                new COLUMN_MODEL{key="Exported",check=1},
                new COLUMN_MODEL{key="OutwardNo",check=25},
                new COLUMN_MODEL{key="SReason",check=512},
                new COLUMN_MODEL{key="OriginalNo",check=25},
                new COLUMN_MODEL{key="AccountingObjectBankName",check=50},
                new COLUMN_MODEL{key="PaymentMethod",check=25},
                new COLUMN_MODEL{key="IsPromotion",check=1},
                new COLUMN_MODEL{key="DiscountAccount",check=25},
                new COLUMN_MODEL{key="SAOrderNo",check=25},
                new COLUMN_MODEL{key="LotNo",check=50},
                new COLUMN_MODEL{key="ExportTaxRate",check=36},
                new COLUMN_MODEL{key="ExportTaxAmount",check=19},
                new COLUMN_MODEL{key="ExportTaxAccount",check=25},
                new COLUMN_MODEL{key="ExportTaxAccountCorresponding",check=25},
                new COLUMN_MODEL{key="RepositoryAccount",check=25},
                new COLUMN_MODEL{key="CostAccount",check=25},
                new COLUMN_MODEL{key="SContactName",check=512},
                new COLUMN_MODEL{key="PhuongThucTT",check=3},
                new COLUMN_MODEL{key="MNo",check=25},
                new COLUMN_MODEL{key="MReasonPay",check=512},
                new COLUMN_MODEL{key="MContactName",check=512},
                new COLUMN_MODEL{key="MaTKNopTien",check=30},
                new COLUMN_MODEL{key="InwardNo",check=25},
                new COLUMN_MODEL{key="MaNCC",check=25},
                new COLUMN_MODEL{key="MaTKHuong",check=30},
                new COLUMN_MODEL{key="MaKho",check=25},
                new COLUMN_MODEL{key="FreightAmountOriginal",check=19},
                new COLUMN_MODEL{key="InwardAmountOriginal",check=19},
                new COLUMN_MODEL{key="PPOrderNo",check=25},
                new COLUMN_MODEL{key="ImportTaxExpenseAmount",check=19},
                new COLUMN_MODEL{key="ImportTaxRate",check=36},
                new COLUMN_MODEL{key="ImportTaxAmount",check=19},
                new COLUMN_MODEL{key="ImportTaxAccount",check=25},
                new COLUMN_MODEL{key="SpecialConsumeTaxRate",check=36},
                new COLUMN_MODEL{key="SpecialConsumeTaxAmount",check=19},
                new COLUMN_MODEL{key="SpecialConsumeTaxAccount",check=25},
                new COLUMN_MODEL{key="DeductionDebitAccount",check=25},
                new COLUMN_MODEL{key="SoTKChi",check=30},
                new COLUMN_MODEL{key="IsFeightService",check=1},
                new COLUMN_MODEL{key="TKNHChi",check=30},
                new COLUMN_MODEL{key="TKHuong",check=30},
                new COLUMN_MODEL{key="MaDichVu",check=25},
            };

            return dic;
        }
        #endregion
        #region Config_column
        private List<COLUMN_MODEL> GetListColumn(string strDM)
        {
            List<COLUMN_MODEL> dic = new List<COLUMN_MODEL>();
            switch (strDM)
            {
                case "PHIEUTHU":
                    dic = GetPhieuThu();
                    break;
                case "PHIEUCHI":
                    dic = GetPhieuChi();
                    break;
                case "SECUNC":
                    dic = GetSecUNC();
                    break;
                case "THUNGANHANG":
                    dic = GetThuQuaNganHang();
                    break;
                case "THETINDUNG":
                    dic = GetTheTinDung();
                    break;
                case "CHUYENTIENNOIBO":
                    dic = GetChuyenTienNB();
                    break;
                case "KIEMKEQUY":
                    dic = GetKiemKeQuy();
                    break;
                //Mua hàng
                case "DONMUAHANG":
                    dic = GetDonMuaHang();
                    break;
                case "MUAHANGQUAKHO":
                    dic = GetMuaHangQuaKho();
                    break;
                case "MUAHANGKHONGQUAKHO":
                    dic = GetMuaHangKhongQuaKho();
                    break;
                case "MUADICHVU":
                    dic = GetMuaHangDichVu();
                    break;
                //Bán hàng
                case "BAOGIA":
                    dic = GetBaoGia();
                    break;
                case "DONDATHANG":
                    dic = GetDonDatHang();
                    break;
                case "BANHANGCHUATHUTIEN":
                    dic = GetBHChuaThuTien();
                    break;
                case "BANHANGTHUTIENNGAY":
                    dic = GetBHThuTienNgay();
                    break;
                case "BHDLBANDUNGGIA":
                    dic = GetBHDaiLy();
                    break;
                default:
                    break;
            }

            return dic;
        }
        private List<COLUMN_MODEL> GetPhieuThu()
        {
            List<COLUMN_MODEL> list = new List<COLUMN_MODEL>()
            {
                new COLUMN_MODEL{key="AccountingObjectType",value="Loại đối tượng",check=1},
                new COLUMN_MODEL{key="No",value="Số chứng từ",check=1},
                new COLUMN_MODEL{key="Date",value="Ngày chứng từ",check=1},
                new COLUMN_MODEL{key="PostedDate",value="Ngày hạch toán",check=1},
                new COLUMN_MODEL{key="MaDoiTuong",value="Mã đối tượng",check=0},
                new COLUMN_MODEL{key="AccountingObjectName",value="Tên đối tượng",check=0},
                new COLUMN_MODEL{key="AccountingObjectAddress",value="Địa chỉ",check=0},
                new COLUMN_MODEL{key="Payers",value="Người nộp",check=0},
                new COLUMN_MODEL{key="Reason",value="Lý do nộp/ Diễn giải",check=0},
                new COLUMN_MODEL{key="NumberAttach",value="Số chứng từ gốc kèm theo",check=0},
                new COLUMN_MODEL{key="CurrencyID",value="Loại tiền",check=0},
                new COLUMN_MODEL{key="ExchangeRate",value="Tỷ giá",check=0},
                new COLUMN_MODEL{key="DebitAccount",value="TK Nợ",check=1},
                new COLUMN_MODEL{key="CreditAccount",value="TK Có",check=1},
                new COLUMN_MODEL{key="AmountOriginal",value="Số tiền",check=1},
                new COLUMN_MODEL{key="Amount",value="Quy đổi",check=1},
                new COLUMN_MODEL{key="MaKhoanMucCP",value="Mã khoản mục CP",check=0},
                new COLUMN_MODEL{key="MaPhongBan",value="Mã phòng ban",check=0},
                new COLUMN_MODEL{key="MaDoiTuongCP",value="Mã ĐT tập hợp CP",check=0},
                new COLUMN_MODEL{key="MaHopDong",value="Mã Hợp đồng",check=0},
            };
            return list;
        }
        private List<COLUMN_MODEL> GetPhieuChi()
        {
            List<COLUMN_MODEL> list = new List<COLUMN_MODEL>()
            {
                new COLUMN_MODEL{key="No",value="Số chứng từ",check=1},
                new COLUMN_MODEL{key="Date",value="Ngày chứng từ",check=1},
                new COLUMN_MODEL{key="PostedDate",value="Ngày hạch toán",check=1},
                new COLUMN_MODEL{key="AccountingObjectType",value="Loại đối tượng",check=1},
                new COLUMN_MODEL{key="MaDoiTuong",value="Mã đối tượng",check=0},
                new COLUMN_MODEL{key="AccountingObjectName",value="Tên đối tượng",check=0},
                new COLUMN_MODEL{key="AccountingObjectAddress",value="Địa chỉ",check=0},
                new COLUMN_MODEL{key="Receiver",value="Người nhận",check=0},
                new COLUMN_MODEL{key="Reason",value="Lý do chi",check=0},
                new COLUMN_MODEL{key="NumberAttach",value="Kèm theo",check=0},
                new COLUMN_MODEL{key="CurrencyID",value="Loại tiền",check=0},
                new COLUMN_MODEL{key="ExchangeRate",value="Tỷ giá",check=0},
                new COLUMN_MODEL{key="DebitAccount",value="TK Nợ",check=1},
                new COLUMN_MODEL{key="CreditAccount",value="TK Có",check=1},
                new COLUMN_MODEL{key="AmountOriginal",value="Số tiền",check=1},
                new COLUMN_MODEL{key="Amount",value="Quy đổi",check=1},
                new COLUMN_MODEL{key="MaMucThuChi",value="Mục thu/chi",check=0},
                new COLUMN_MODEL{key="MaKhoanMucCP",value="Khoản mục CP",check=0},
                new COLUMN_MODEL{key="MaPhongBan",value="Phòng ban",check=0},
                new COLUMN_MODEL{key="MaDoiTuongCP",value="ĐT tập hợp CP",check=0},
                new COLUMN_MODEL{key="MaHopDong",value="Hợp đồng",check=0},
                new COLUMN_MODEL{key="MaThongKe",value="Mã thống kê",check=0},
                new COLUMN_MODEL{key="Description",value="Diễn giải tiền thuế",check=0},
                new COLUMN_MODEL{key="VATAccount",value="TK thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmountOriginal",value="Tiền thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmount",value="Tiền thuế GTGT Quy đổi",check=0},
                new COLUMN_MODEL{key="VATRate",value="Tỷ suất thuế GTGT",check=0},
                new COLUMN_MODEL{key="InvoiceTemplate",value="Mẫu số hóa đơn",check=0},
                new COLUMN_MODEL{key="InvoiceSeries",value="Ký hiệu HĐ",check=0},
                new COLUMN_MODEL{key="InvoiceNo",value="Số hóa đơn",check=0},
                new COLUMN_MODEL{key="InvoiceDate",value="Ngày hóa đơn",check=0},
                new COLUMN_MODEL{key="MaNhomHHDV",value="Nhóm HHDV",check=0},
            };
            return list;
        }
        private List<COLUMN_MODEL> GetSecUNC()
        {
            List<COLUMN_MODEL> list = new List<COLUMN_MODEL>()
            {
                new COLUMN_MODEL{key="LoaiGiayBN",value="Loại giấy báo nợ",check=1},
                new COLUMN_MODEL{key="No",value="Số chứng từ",check=1},
                new COLUMN_MODEL{key="Date",value="Ngày chứng từ",check=1},
                new COLUMN_MODEL{key="PostedDate",value="Ngày hạch toán",check=1},
                new COLUMN_MODEL{key="AccountingObjectType",value="Loại đối tượng",check=1},
                new COLUMN_MODEL{key="MaTKChi",value="Tài khoản chi",check=0},
                new COLUMN_MODEL{key="Reason",value="Nội dung TT",check=0},
                new COLUMN_MODEL{key="MaDoiTuong",value="Mã đối tượng",check=0},
                new COLUMN_MODEL{key="AccountingObjectName",value="Tên đối tượng",check=0},
                new COLUMN_MODEL{key="AccountingObjectAddress",value="Địa chỉ",check=0},
                new COLUMN_MODEL{key="MaTKNguoiNhan",value="TK người nhận",check=0},
                new COLUMN_MODEL{key="Receiver",value="Người lĩnh tiền",check=0},
                new COLUMN_MODEL{key="IdentificationNo",value="Số CMT",check=0},
                new COLUMN_MODEL{key="IssueDate",value="Ngày cấp",check=0},
                new COLUMN_MODEL{key="IssueBy",value="Nơi cấp",check=0},
                new COLUMN_MODEL{key="CurrencyID",value="Loại tiền",check=0},
                new COLUMN_MODEL{key="ExchangeRate",value="Tỷ giá",check=0},
                new COLUMN_MODEL{key="DebitAccount",value="TK Nợ",check=1},
                new COLUMN_MODEL{key="CreditAccount",value="TK Có",check=1},
                new COLUMN_MODEL{key="AmountOriginal",value="Số tiền",check=1},
                new COLUMN_MODEL{key="Amount",value="Số tiền quy đổi",check=1},
                new COLUMN_MODEL{key="MaMucThuChi",value="Mục thu/chi",check=0},
                new COLUMN_MODEL{key="MaKhoanMucCP",value="Khoản mục CP",check=0},
                new COLUMN_MODEL{key="MaPhongBan",value="Phòng ban",check=0},
                new COLUMN_MODEL{key="MaDoiTuongCP",value="ĐT tập hợp CP",check=0},
                new COLUMN_MODEL{key="MaHopDong",value="Hợp đồng",check=0},
                new COLUMN_MODEL{key="MaThongKe",value="Mã thống kê",check=0},
                new COLUMN_MODEL{key="Description",value="Diễn giải tiền thuế",check=0},
                new COLUMN_MODEL{key="VATAccount",value="TK thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmount",value="Tiền thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmountOriginal",value="Tiền thuế GTGT quy đổi",check=0},
                new COLUMN_MODEL{key="VATRate",value="Tỷ suất thuế GTGT",check=0},
                new COLUMN_MODEL{key="PretaxAmount",value="Giá tính thuế",check=0},
                new COLUMN_MODEL{key="InvoiceTemplate",value="Mẫu số hóa đơn",check=0},
                new COLUMN_MODEL{key="InvoiceSeries",value="Ký hiệu HĐ",check=0},
                new COLUMN_MODEL{key="InvoiceNo",value="Số HĐ",check=0},
                new COLUMN_MODEL{key="InvoiceDate",value="Ngày hóa đơn",check=0},
                new COLUMN_MODEL{key="MaHHDV",value="Nhóm HHDV",check=0},
            };
            return list;
        }
        private List<COLUMN_MODEL> GetThuQuaNganHang()
        {
            List<COLUMN_MODEL> list = new List<COLUMN_MODEL>()
            {
                new COLUMN_MODEL{key="No",value="Số chứng từ",check=1},
                new COLUMN_MODEL{key="Date",value="Ngày chứng từ",check=1},
                new COLUMN_MODEL{key="PostedDate",value="Ngày hạch toán",check=1},
                new COLUMN_MODEL{key="AccountingObjectType",value="Loại đối tượng",check=1},
                new COLUMN_MODEL{key="MaDoiTuong",value="Mã đối tượng",check=1},
                new COLUMN_MODEL{key="AccountingObjectName",value="Tên đối tượng",check=0},
                new COLUMN_MODEL{key="AccountingObjectAddress",value="Địa chỉ",check=0},
                new COLUMN_MODEL{key="SoTKHuong",value="Số TK hưởng",check=0},
                new COLUMN_MODEL{key="Reason",value="Lý do thu",check=0},
                new COLUMN_MODEL{key="CurrencyID",value="Loại tiền",check=0},
                new COLUMN_MODEL{key="ExchangeRate",value="Tỷ giá",check=0},
                new COLUMN_MODEL{key="DebitAccount",value="TK Nợ",check=1},
                new COLUMN_MODEL{key="CreditAccount",value="TK Có",check=1},
                new COLUMN_MODEL{key="AmountOriginal",value="Số tiền",check=1},
                new COLUMN_MODEL{key="Amount",value="Quy đổi",check=1},
                new COLUMN_MODEL{key="InvoiceTemplate",value="Mẫu số hóa đơn",check=0},
                new COLUMN_MODEL{key="InvoiceSeries",value="Ký hiệu HĐ",check=0},
                new COLUMN_MODEL{key="InvoiceNo",value="Số hóa đơn",check=0},
                new COLUMN_MODEL{key="InvoiceDate",value="Ngày hóa đơn",check=0},
                new COLUMN_MODEL{key="MaMucThuChi",value="Mục thu/chi",check=0},
                new COLUMN_MODEL{key="MaKhoanMucCP",value="Khoản mục CP",check=0},
                new COLUMN_MODEL{key="MaPhongBan",value="Phòng ban",check=0},
                new COLUMN_MODEL{key="MaDoiTuongCP",value="ĐT tập hợp CP",check=0},
                new COLUMN_MODEL{key="MaHopDong",value="Hợp đồng",check=0},
                new COLUMN_MODEL{key="MaThongKe",value="Mã thống kê",check=0},
            };
            return list;
        }
        private List<COLUMN_MODEL> GetTheTinDung()
        {
            List<COLUMN_MODEL> list = new List<COLUMN_MODEL>()
            {
                new COLUMN_MODEL{key="No",value="Số chứng từ",check=1},
                new COLUMN_MODEL{key="Date",value="Ngày chứng từ",check=1},
                new COLUMN_MODEL{key="PostedDate",value="Ngày hạch toán",check=1},
                new COLUMN_MODEL{key="AccountingObjectType",value="Loại đối tượng",check=1},
                new COLUMN_MODEL{key="CreditCardNumber",value="Số thẻ",check=1},
                new COLUMN_MODEL{key="Reason",value="Nội dung TT",check=0},
                new COLUMN_MODEL{key="MaDoiTuong",value="Mã đối tượng",check=0},
                new COLUMN_MODEL{key="AccountingObjectBankAccount",value="Số TK người nhận",check=1},
                new COLUMN_MODEL{key="CurrencyID",value="Loại tiền",check=0},
                new COLUMN_MODEL{key="ExchangeRate",value="Tỷ giá",check=0},
                new COLUMN_MODEL{key="DebitAccount",value="TK Nợ",check=1},
                new COLUMN_MODEL{key="CreditAccount",value="TK Có",check=1},
                new COLUMN_MODEL{key="Amount",value="Số tiền",check=1},
                new COLUMN_MODEL{key="AmountOriginal",value="Số tiền quy đổi",check=1},
                new COLUMN_MODEL{key="MaNhanVien",value="Mã Nhân viên",check=0},
                new COLUMN_MODEL{key="MaMucThuChi",value="Mục thu/chi",check=0},
                new COLUMN_MODEL{key="MaKhoanMucCP",value="Khoản mục CP",check=0},
                new COLUMN_MODEL{key="MaPhongBan",value="Phòng ban",check=0},
                new COLUMN_MODEL{key="MaDoiTuongCP",value="ĐT tập hợp CP",check=0},
                new COLUMN_MODEL{key="MaHopDong",value="Hợp đồng",check=0},
                new COLUMN_MODEL{key="MaThongKe",value="Mã thống kê",check=0},
                new COLUMN_MODEL{key="Description",value="Diễn giải tiền thuế",check=0},
                new COLUMN_MODEL{key="VATAccount",value="TK thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmountOriginal",value="Tiền thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmount",value="Tiền thuế GTGT quy đổi",check=0},
                new COLUMN_MODEL{key="VATRate",value="Tỷ suất thuế GTGT",check=0},
                new COLUMN_MODEL{key="PretaxAmountOriginal",value="Giá tính thuế",check=0},
                new COLUMN_MODEL{key="PretaxAmount",value="Giá tính thuế quy đổi",check=0},
                new COLUMN_MODEL{key="InvoiceTemplate",value="Mẫu số hóa đơn",check=0},
                new COLUMN_MODEL{key="InvoiceSeries",value="Ký hiệu HĐ",check=0},
                new COLUMN_MODEL{key="InvoiceNo",value="Số hóa đơn",check=0},
                new COLUMN_MODEL{key="InvoiceDate",value="Ngày hóa đơn",check=0},
                new COLUMN_MODEL{key="MaNhomHHDV",value="Nhóm HHDV",check=0},
            };
            return list;
        }
        private List<COLUMN_MODEL> GetChuyenTienNB()
        {
            List<COLUMN_MODEL> list = new List<COLUMN_MODEL>()
            {
                new COLUMN_MODEL{key="No",value="Số chứng từ",check=1},
                new COLUMN_MODEL{key="Date",value="Ngày chứng từ",check=1},
                new COLUMN_MODEL{key="PostedDate",value="Ngày hạch toán",check=1},
                new COLUMN_MODEL{key="Reason",value="Diễn giải",check=0},
                new COLUMN_MODEL{key="TKNHChuyen",value="Từ TK ngân hàng",check=1},
                new COLUMN_MODEL{key="TKNHNhan",value="Đến TK ngân hàng",check=1},
                new COLUMN_MODEL{key="CurrencyID",value="Loại tiền",check=0},
                new COLUMN_MODEL{key="ExchangeRate",value="Tỷ giá",check=0},
                new COLUMN_MODEL{key="DebitAccount",value="TK Nợ",check=0},
                new COLUMN_MODEL{key="CreditAccount",value="TK Có",check=0},
                new COLUMN_MODEL{key="AmountOriginal",value="Số tiền",check=1},
                new COLUMN_MODEL{key="Amount",value="Số tiền quy đổi",check=1},
                new COLUMN_MODEL{key="MaNhanVien",value="Nhân viên",check=0},
            };
            return list;
        }
        private List<COLUMN_MODEL> GetKiemKeQuy()
        {
            List<COLUMN_MODEL> list = new List<COLUMN_MODEL>()
            {
                new COLUMN_MODEL{key="No",value="Số chứng từ",check=1},
                new COLUMN_MODEL{key="Date",value="Ngày chứng từ",check=1},
                new COLUMN_MODEL{key="CurrencyID",value="Loại tiền",check=1},
                new COLUMN_MODEL{key="Description",value="Diễn giải 1",check=0},
                new COLUMN_MODEL{key="Summary",value="Kết quả kiểm kê",check=0},
                new COLUMN_MODEL{key="ValueOfMoney",value="Mệnh giá tiền",check=1},
                new COLUMN_MODEL{key="Quantity",value="Số lượng",check=1},
                new COLUMN_MODEL{key="Amount",value="Thành tiền",check=1},
                new COLUMN_MODEL{key="Description2",value="Diễn giải 2",check=0},
                new COLUMN_MODEL{key="MaNhanVien",value="Mã nhân viên tham gia",check=0},

            };
            return list;
        }
        //Mua hàng
        private List<COLUMN_MODEL> GetDonMuaHang()
        {
            List<COLUMN_MODEL> list = new List<COLUMN_MODEL>()
            {
                new COLUMN_MODEL{key="No",value="Số đơn hàng",check=1},
                new COLUMN_MODEL{key="Date",value="Ngày đơn hàng",check=1},
                new COLUMN_MODEL{key="MaDoiTuong",value="Mã đối tượng",check=1},
                new COLUMN_MODEL{key="ShippingPlace",value="Địa điểm giao hàng",check=0},
                new COLUMN_MODEL{key="Reason",value="Diễn giải",check=0},
                new COLUMN_MODEL{key="DeliverDate",value="Ngày giao hàng",check=0},
                new COLUMN_MODEL{key="MaNhanVien",value="NV mua hàng",check=0},
                new COLUMN_MODEL{key="CurrencyID",value="Loại tiền",check=0},
                new COLUMN_MODEL{key="ExchangeRate",value="Tỷ giá",check=0},
                new COLUMN_MODEL{key="MaHang",value="Mã hàng",check=1},
                new COLUMN_MODEL{key="Unit",value="DVT",check=0},
                new COLUMN_MODEL{key="Quantity",value="Số lượng",check=1},
                new COLUMN_MODEL{key="UnitPriceOriginal",value="Đơn giá",check=1},
                new COLUMN_MODEL{key="QuantityReceipt",value="Đơn giá quy đổi",check=1},
                new COLUMN_MODEL{key="AmountOriginal",value="Thành tiền",check=1},
                new COLUMN_MODEL{key="Amount",value="Thành tiền quy đổi",check=1},
                new COLUMN_MODEL{key="DiscountRate",value="Tỷ lệ CK",check=0},
                new COLUMN_MODEL{key="DiscountAmountOriginal",value="Tiền chiết khẩu",check=0},
                new COLUMN_MODEL{key="DiscountAmount",value="Tiền chiết khấu quy đổi",check=0},
                new COLUMN_MODEL{key="VATDescription",value="Diễn giải thuế",check=0},
                new COLUMN_MODEL{key="VATRate",value="Tỷ suất thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmount",value="Tiền thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmountOriginal",value="Tiền thuế GTGT quy đổi",check=0},
                new COLUMN_MODEL{key="MaMucThuChi",value="Mục thu/chi",check=0},
                new COLUMN_MODEL{key="MaKhoanMucCP",value="Khoản mục CP",check=0},
                new COLUMN_MODEL{key="MaPhongBan",value="Phòng ban",check=0},
                new COLUMN_MODEL{key="MaDoiTuongCP",value="ĐT tập hợp CP",check=0},
                new COLUMN_MODEL{key="MaHopDong",value="Hợp đồng",check=0},
                new COLUMN_MODEL{key="MaThongKe",value="Mã thống kê",check=0},
            };
            return list;
        }
        private List<COLUMN_MODEL> GetMuaHangQuaKho()
        {
            List<COLUMN_MODEL> list = new List<COLUMN_MODEL>()
            {
                new COLUMN_MODEL{key="PhuongThucTT",value="Phương thức thanh toán",check=1},
                new COLUMN_MODEL{key="InwardNo",value="Số chứng từ nhập kho",check=1},
                new COLUMN_MODEL{key="No",value="Số chứng từ thanh toán",check=0},
                new COLUMN_MODEL{key="Date",value="Ngày chứng từ",check=1},
                new COLUMN_MODEL{key="PostedDate",value="Ngày hạch toán",check=1},
                new COLUMN_MODEL{key="MaNCC",value="Mã Nhà cung cấp",check=0},
                new COLUMN_MODEL{key="ContactName",value="Họ tên người giao",check=0},
                new COLUMN_MODEL{key="MaNhanVien",value="NV mua hàng",check=0},
                new COLUMN_MODEL{key="DueDate",value="Hạn thanh toán",check=0},
                new COLUMN_MODEL{key="Reason",value="Diễn giải nhập kho",check=0},
                new COLUMN_MODEL{key="OriginalNo",value="Số CT gốc kèm theo",check=0},
                new COLUMN_MODEL{key="MaTKChi",value="Tài khoản NH chi",check=0},
                new COLUMN_MODEL{key="MReasonPay",value="Lý do chi",check=0},
                new COLUMN_MODEL{key="MaTKHuong",value="Tài khoản hưởng",check=0},
                new COLUMN_MODEL{key="IdentificationNo",value="Số CMND người lĩnh",check=0},
                new COLUMN_MODEL{key="IssueDate",value="Ngày cấp",check=0},
                new COLUMN_MODEL{key="IssueBy",value="Nơi cấp",check=0},
                new COLUMN_MODEL{key="CreditCardNumber",value="Số thẻ tín dụng",check=0},
                new COLUMN_MODEL{key="CurrencyID",value="Loại tiền",check=0},
                new COLUMN_MODEL{key="ExchangeRate",value="Tỷ giá",check=0},
                new COLUMN_MODEL{key="MaHang",value="Mã hàng",check=1},
                new COLUMN_MODEL{key="MaKho",value="Mã Kho",check=1},
                new COLUMN_MODEL{key="DebitAccount",value="TK Nợ",check=1},
                new COLUMN_MODEL{key="CreditAccount",value="TK Có",check=1},
                new COLUMN_MODEL{key="Unit",value="DVT",check=0},
                new COLUMN_MODEL{key="Quantity",value="Số lượng",check=1},
                new COLUMN_MODEL{key="UnitPriceOriginal",value="Đơn giá",check=1},
                new COLUMN_MODEL{key="UnitPrice",value="Đơn giá quy đổi",check=1},
                new COLUMN_MODEL{key="AmountOriginal",value="Thành tiền",check=1},
                new COLUMN_MODEL{key="Amount",value="Thành tiền quy đổi",check=1},
                new COLUMN_MODEL{key="DiscountRate",value="Tỷ lệ CK",check=0},
                new COLUMN_MODEL{key="DiscountAmountOriginal",value="Tiền CK",check=0},
                new COLUMN_MODEL{key="DiscountAmount",value="Tiền CK quy đổi",check=0},
                new COLUMN_MODEL{key="FreightAmountOriginal",value="CP mua",check=0},
                new COLUMN_MODEL{key="InwardAmountOriginal",value="Giá trị nhập kho",check=0},
                new COLUMN_MODEL{key="LotNo",value="Số lô",check=0},
                new COLUMN_MODEL{key="ExpiryDate",value="Hạn dùng",check=0},
                new COLUMN_MODEL{key="PPOrderNo",value="Số đơn hàng",check=0},
                new COLUMN_MODEL{key="ImportTaxExpenseAmount",value="Phí trước hải quan quy đổi",check=0},
                new COLUMN_MODEL{key="VATDescription",value="Diễn giải thuế",check=0},
                new COLUMN_MODEL{key="ImportTaxRate",value="Tỷ suất thuế NK",check=0},
                new COLUMN_MODEL{key="ImportTaxAmount",value="Tiền thuế NK (quy đổi)",check=0},
                new COLUMN_MODEL{key="ImportTaxAccount",value="TK thuế NK",check=0},
                new COLUMN_MODEL{key="SpecialConsumeTaxRate",value="Tỷ suất thuế TTĐB",check=0},
                new COLUMN_MODEL{key="SpecialConsumeTaxAmount",value="Tiền thuể TTĐB(quy đổi)",check=0},
                new COLUMN_MODEL{key="SpecialConsumeTaxAccount",value="TK thuế TTĐB",check=0},
                new COLUMN_MODEL{key="VATRate",value="Tý suất thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmount",value="Tiền thuế GTGT (quy đổi)",check=0},
                new COLUMN_MODEL{key="VATAccount",value="TKĐƯ thuế GTGT",check=0},
                new COLUMN_MODEL{key="DeductionDebitAccount",value="TK thuế GTGT",check=0},
                new COLUMN_MODEL{key="InvoiceTemplate",value="Mẫu số hóa đơn",check=0},
                new COLUMN_MODEL{key="InvoiceSeries",value="Ký hiệu HĐ",check=0},
                new COLUMN_MODEL{key="InvoiceNo",value="Số hóa đơn",check=0},
                new COLUMN_MODEL{key="InvoiceDate",value="Ngày hóa đơn",check=0},
                new COLUMN_MODEL{key="MaNhomHHDV",value="Mã Nhóm HHDV",check=0},
                new COLUMN_MODEL{key="MaMucThuChi",value="Mã Mục thu/chi",check=0},
                new COLUMN_MODEL{key="MaKhoanMucCP",value="Khoản mục CP",check=0},
                new COLUMN_MODEL{key="MaDoiTuongCP",value="Mã ĐT tập hợp CP",check=0},
                new COLUMN_MODEL{key="MaHopDong",value="Mã Hợp đồng",check=0},
                new COLUMN_MODEL{key="MaThongKe",value="Mã thống kê",check=0},
            };
            return list;
        }
        private List<COLUMN_MODEL> GetMuaHangKhongQuaKho()
        {
            List<COLUMN_MODEL> list = new List<COLUMN_MODEL>()
            {
                new COLUMN_MODEL{key="PhuongThucTT",value="Phương thức thanh toán",check=1},
                new COLUMN_MODEL{key="No",value="Số chứng từ thanh toán",check=1},
                new COLUMN_MODEL{key="Date",value="Ngày chứng từ",check=1},
                new COLUMN_MODEL{key="PostedDate",value="Ngày hạch toán",check=1},
                new COLUMN_MODEL{key="MaNCC",value="Mã Nhà cung cấp",check=0},
                new COLUMN_MODEL{key="ContactName",value="Họ tên người nhận tiền",check=0},
                new COLUMN_MODEL{key="Reason",value="Diễn giải",check=0},
                new COLUMN_MODEL{key="MaNhanVien",value="NV mua hàng",check=0},
                new COLUMN_MODEL{key="DueDate",value="Hạn thanh toán",check=0},
                new COLUMN_MODEL{key="NumberAttach",value="Số CT gốc kèm theo",check=0},
                new COLUMN_MODEL{key="SoTKChi",value="Tài khoản NH chi",check=0},
                new COLUMN_MODEL{key="SoTKHuong",value="Tài khoản hưởng",check=0},
                new COLUMN_MODEL{key="IdentificationNo",value="Số CMND người lĩnh",check=0},
                new COLUMN_MODEL{key="IssueDate",value="Ngày cấp",check=0},
                new COLUMN_MODEL{key="IssueBy",value="Nơi cấp",check=0},
                new COLUMN_MODEL{key="CreditCardNumber",value="Số thẻ tín dụng",check=0},
                new COLUMN_MODEL{key="CurrencyID",value="Loại tiền",check=0},
                new COLUMN_MODEL{key="ExchangeRate",value="Tỷ giá",check=0},
                new COLUMN_MODEL{key="MaHang",value="Mã hàng",check=1},
                new COLUMN_MODEL{key="DebitAccount",value="TK Nợ",check=1},
                new COLUMN_MODEL{key="CreditAccount",value="TK Có",check=1},
                new COLUMN_MODEL{key="Unit",value="DVT",check=0},
                new COLUMN_MODEL{key="Quantity",value="Số lượng",check=1},
                new COLUMN_MODEL{key="UnitPriceOriginal",value="Đơn giá",check=1},
                new COLUMN_MODEL{key="UnitPrice",value="Đơn giá quy đổi",check=1},
                new COLUMN_MODEL{key="AmountOriginal",value="Thành tiền",check=1},
                new COLUMN_MODEL{key="Amount",value="Thành tiền quy đổi",check=1},
                new COLUMN_MODEL{key="DiscountRate",value="Tỷ lệ CK",check=0},
                new COLUMN_MODEL{key="DiscountAmountOriginal",value="Tiền CK",check=0},
                new COLUMN_MODEL{key="DiscountAmount",value="Tiền CK quy đổi",check=0},
                new COLUMN_MODEL{key="PPOrderNo",value="Số đơn hàng",check=0},
                new COLUMN_MODEL{key="FreightAmountOriginal",value="CP mua",check=0},
                new COLUMN_MODEL{key="InwardAmountOriginal",value="Giá trị nhập kho",check=0},
                new COLUMN_MODEL{key="ImportTaxExpenseAmount",value="Phí trước hải quan quy đổi",check=0},
                new COLUMN_MODEL{key="VATDescription",value="Diển giải thuế",check=0},
                new COLUMN_MODEL{key="ImportTaxRate",value="Tỷ suất thuế NK",check=0},
                new COLUMN_MODEL{key="ImportTaxAmount",value="Tiền thuế NK (quy đổi)",check=0},
                new COLUMN_MODEL{key="ImportTaxAccount",value="TK thuế NK",check=0},
                new COLUMN_MODEL{key="SpecialConsumeTaxRate",value="Tỷ suất thuế TTĐB",check=0},
                new COLUMN_MODEL{key="SpecialConsumeTaxAmount",value="Tiền thuể TTĐB(quy đổi)",check=0},
                new COLUMN_MODEL{key="SpecialConsumeTaxAccount",value="TK thuế TTĐB",check=0},
                new COLUMN_MODEL{key="VATRate",value="Tý suất thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmount",value="Tiền thuế GTGT (quy đổi)",check=0},
                new COLUMN_MODEL{key="VATAccount",value="TKĐƯ thuế GTGT",check=0},
                new COLUMN_MODEL{key="DeductionDebitAccount",value="TK thuế GTGT",check=0},
                new COLUMN_MODEL{key="InvoiceTemplate",value="Mẫu số hóa đơn",check=0},
                new COLUMN_MODEL{key="InvoiceSeries",value="Ký hiệu HĐ",check=0},
                new COLUMN_MODEL{key="InvoiceNo",value="Số hóa đơn",check=0},
                new COLUMN_MODEL{key="InvoiceDate",value="Ngày hóa đơn",check=0},
                new COLUMN_MODEL{key="MaNhomHHDV",value="Mã Nhóm HHDV",check=0},
                new COLUMN_MODEL{key="MaMucThuChi",value="Mã Mục thu/chi",check=0},
                new COLUMN_MODEL{key="MaKhoanMucCP",value="Khoản mục CP",check=0},
                new COLUMN_MODEL{key="MaDoiTuongCP",value="Mã ĐT tập hợp CP",check=0},
                new COLUMN_MODEL{key="MaHopDong",value="Mã Hợp đồng",check=0},
                new COLUMN_MODEL{key="MaThongKe",value="Mã Mã thống kê",check=0},
            };
            return list;
        }
        private List<COLUMN_MODEL> GetMuaHangDichVu()
        {
            List<COLUMN_MODEL> list = new List<COLUMN_MODEL>()
            {
                new COLUMN_MODEL{key="PhuongThucTT",value="Phương thức thanh toán",check=1},
                new COLUMN_MODEL{key="IsFeightService",value="Là CP mua hàng",check=1},
                new COLUMN_MODEL{key="No",value="Số chứng từ",check=1},
                new COLUMN_MODEL{key="Date",value="Ngày chứng từ",check=1},
                new COLUMN_MODEL{key="PostedDate",value="Ngày hạch toán",check=1},
                new COLUMN_MODEL{key="MaNCC",value="Mã Nhà cung cấp",check=0},
                new COLUMN_MODEL{key="Reason",value="Diễn giải",check=0},
                new COLUMN_MODEL{key="ContactName",value="Họ tên người nhận tiền",check=0},
                new COLUMN_MODEL{key="NumberAttach",value="Số CT gốc kèm theo",check=0},
                new COLUMN_MODEL{key="TKNHChi",value="Tài khoản NH chi",check=0},
                new COLUMN_MODEL{key="TKHuong",value="Tài khoản hưởng",check=0},
                new COLUMN_MODEL{key="IdentificationNo",value="Số CMND người lĩnh",check=0},
                new COLUMN_MODEL{key="IssueDate",value="Ngày cấp",check=0},
                new COLUMN_MODEL{key="IssueBy",value="Nơi cấp",check=0},
                new COLUMN_MODEL{key="CreditCardNumber",value="Số thẻ tín dụng",check=0},
                new COLUMN_MODEL{key="DueDate",value="Hạn thanh toán",check=0},
                new COLUMN_MODEL{key="CurrencyID",value="Loại tiền",check=0},
                new COLUMN_MODEL{key="ExchangeRate",value="Tỷ giá",check=0},
                new COLUMN_MODEL{key="MaNhanVien",value="Mã NV mua hàng",check=0},
                new COLUMN_MODEL{key="MaDichVu",value="Mã dịch vụ",check=1},
                new COLUMN_MODEL{key="DebitAccount",value="TK Nợ",check=1},
                new COLUMN_MODEL{key="CreditAccount",value="TK Có",check=1},
                new COLUMN_MODEL{key="Quantity",value="Số lượng",check=1},
                new COLUMN_MODEL{key="UnitPriceOriginal",value="Đơn giá",check=1},
                new COLUMN_MODEL{key="UnitPrice",value="Đơn giá quy đổi",check=1},
                new COLUMN_MODEL{key="AmountOriginal",value="Thành tiền",check=1},
                new COLUMN_MODEL{key="Amount",value="Thành tiền quy đổi",check=1},
                new COLUMN_MODEL{key="DiscountRate",value="Tỷ lệ CK",check=0},
                new COLUMN_MODEL{key="DiscountAmountOriginal",value="Tiền CK",check=0},
                new COLUMN_MODEL{key="DiscountAmount",value="Tiền CK quy đổi",check=0},
                new COLUMN_MODEL{key="VATDescription",value="Diển giải thuế",check=0},
                new COLUMN_MODEL{key="VATRate",value="Thuế suất",check=0},
                new COLUMN_MODEL{key="VATAmountOriginal",value="Tiền thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmount",value="Tiền thuế GTGT quy đổi",check=0},
                new COLUMN_MODEL{key="VATAccount",value="TK thuế",check=0},
                new COLUMN_MODEL{key="InvoiceTemplate",value="Mẫu số hóa đơn",check=0},
                new COLUMN_MODEL{key="InvoiceSeries",value="Ký hiệu HĐ",check=0},
                new COLUMN_MODEL{key="InvoiceNo",value="Số hóa đơn",check=0},
                new COLUMN_MODEL{key="InvoiceDate",value="Ngày hóa đơn",check=0},
                new COLUMN_MODEL{key="MaNhomHHDV",value="Nhóm HHDV",check=0},
                new COLUMN_MODEL{key="MaMucThuChi",value="Mã mục thu/chi",check=0},
                new COLUMN_MODEL{key="MaKhoanMucCP",value="Khoản mục CP",check=0},
                new COLUMN_MODEL{key="MaDoiTuongCP",value="Mã ĐT tập hợp CP",check=0},
                new COLUMN_MODEL{key="MaPhongBan",value="Mã Phòng ban",check=0},
                new COLUMN_MODEL{key="MaHopDong",value="Mã Hợp đồng",check=0},
                new COLUMN_MODEL{key="MaThongKe",value="Mã Mã thống kê",check=0},
            };
            return list;
        }
        //Bán hàng
        private List<COLUMN_MODEL> GetBaoGia()
        {
            List<COLUMN_MODEL> list = new List<COLUMN_MODEL>()
            {
                new COLUMN_MODEL{key="No",value="Số báo giá",check=1},
                new COLUMN_MODEL{key="Date",value="Ngày báo giá",check=1},
                new COLUMN_MODEL{key="FinalDate",value="Hiệu lực",check=0},
                new COLUMN_MODEL{key="MaKhachHang",value="Mã khách hàng",check=0},
                new COLUMN_MODEL{key="Reason",value="Diễn giải",check=0},
                new COLUMN_MODEL{key="ContactName",value="Người liên hệ",check=0},
                new COLUMN_MODEL{key="ContactMobile",value="SĐT người liên hệ",check=0},
                new COLUMN_MODEL{key="ContactEmail",value="Email người liên hệ",check=0},
                new COLUMN_MODEL{key="DeliveryTime",value="Thời gian giao hàng",check=0},
                new COLUMN_MODEL{key="GuaranteeDuration",value="Thời gian bảo hành",check=0},
                new COLUMN_MODEL{key="Description",value="Ghi chú",check=0},
                new COLUMN_MODEL{key="MaNhanVien",value="NV bán hàng",check=0},
                new COLUMN_MODEL{key="DieuKhoanTT",value="Điều khoản TT",check=0},
                new COLUMN_MODEL{key="CurrencyID",value="Loại tiền",check=0},
                new COLUMN_MODEL{key="ExchangeRate",value="Tỷ giá",check=0},
                new COLUMN_MODEL{key="MaHang",value="Mã hàng",check=1},
                new COLUMN_MODEL{key="Quantity",value="Số lượng",check=1},
                new COLUMN_MODEL{key="UnitPriceOriginal",value="Đơn giá",check=1},
                new COLUMN_MODEL{key="UnitPrice",value="Đơn giá quy đổi",check=1},
                new COLUMN_MODEL{key="AmountOriginal",value="Thành tiền",check=1},
                new COLUMN_MODEL{key="Amount",value="Thành tiền QĐ",check=1},
                new COLUMN_MODEL{key="DiscountRate",value="Tỷ lệ CK",check=0},
                new COLUMN_MODEL{key="DiscountAmountOriginal",value="Tiền chiết khấu",check=0},
                new COLUMN_MODEL{key="DiscountAmount",value="Tiền chiết khấu QĐ",check=0},
                new COLUMN_MODEL{key="VATDescription",value="Diễn giải thuế",check=0},
                new COLUMN_MODEL{key="VATRate",value="Tỷ lệ thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmountOriginal",value="Tiền thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmount",value="Tiền thuế GTGT QĐ",check=0},
            };
            return list;
        }
        private List<COLUMN_MODEL> GetDonDatHang()
        {
            List<COLUMN_MODEL> list = new List<COLUMN_MODEL>()
            {
                new COLUMN_MODEL{key="Date",value="Ngày đơn hàng",check=1},
                new COLUMN_MODEL{key="No",value="Số đơn hàng",check=1},
                new COLUMN_MODEL{key="MaKhachHang",value="Mã khách hàng",check=1},
                new COLUMN_MODEL{key="DeliveryPlace",value="Địa điểm giao hàng",check=0},
                new COLUMN_MODEL{key="ContactName",value="Họ tên người liên hệ",check=0},
                new COLUMN_MODEL{key="Reason",value="Diễn giải",check=0},
                new COLUMN_MODEL{key="DeliveDate",value="Ngày giao hàng",check=0},
                new COLUMN_MODEL{key="MaNhanVien",value="NV bán hàng",check=0},
                new COLUMN_MODEL{key="CurrencyID",value="Loại tiền",check=0},
                new COLUMN_MODEL{key="ExchangeRate",value="Tỷ giá",check=0},
                new COLUMN_MODEL{key="MaHang",value="Mã hàng",check=1},
                new COLUMN_MODEL{key="Quantity",value="Số lượng",check=1},
                new COLUMN_MODEL{key="UnitPriceOriginal",value="Đơn giá",check=1},
                new COLUMN_MODEL{key="UnitPrice",value="Đơn giá quy đổi",check=1},
                new COLUMN_MODEL{key="AmountOriginal",value="Thành tiền",check=1},
                new COLUMN_MODEL{key="Amount",value="Thành tiền quy đổi",check=1},
                new COLUMN_MODEL{key="DiscountRate",value="Tỷ lệ CK",check=0},
                new COLUMN_MODEL{key="DiscountAmountOriginal",value="Tiền chiết khấu",check=0},
                new COLUMN_MODEL{key="DiscountAmount",value="Tiền chiết khấu quy đổi",check=0},
                new COLUMN_MODEL{key="VATDescription",value="Diễn giải thuế",check=0},
                new COLUMN_MODEL{key="VATRate",value="Tỷ lệ thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmountOriginal",value="Tiền thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmount",value="Tiền thuế GTGT quy đổi",check=0},
                new COLUMN_MODEL{key="MaPhongBan",value="Phòng ban",check=0},
                new COLUMN_MODEL{key="MaMucThuChi",value="Mục thu/Chi",check=0},
                new COLUMN_MODEL{key="MaKhoanMucCP",value="Khoản mục CP",check=0},
                new COLUMN_MODEL{key="MaDoiTuongCP",value="ĐT tập hợp CP",check=0},
                new COLUMN_MODEL{key="MaHopDong",value="Hợp đồng",check=0},
                new COLUMN_MODEL{key="MaThongKe",value="Mã thống kê",check=0},

            };
            return list;
        }
        private List<COLUMN_MODEL> GetBHChuaThuTien()
        {
            List<COLUMN_MODEL> list = new List<COLUMN_MODEL>()
            {
                new COLUMN_MODEL{key="No",value="Số chứng từ bán hàng",check=1},
                new COLUMN_MODEL{key="Date",value="Ngày chứng từ",check=1},
                new COLUMN_MODEL{key="PostedDate",value="Ngày hạch toán",check=1},
                new COLUMN_MODEL{key="MaKhachHang",value="Mã khách hàng",check=1},
                new COLUMN_MODEL{key="ContactName",value="Họ tên người liên hệ",check=0},
                new COLUMN_MODEL{key="Reason",value="Diễn giải",check=0},
                new COLUMN_MODEL{key="MaNhanVien",value="NV bán hàng",check=0},
                new COLUMN_MODEL{key="MaDKTT",value="Điều khoản TT",check=0},
                new COLUMN_MODEL{key="Exported",value="Hình thức bán hàng",check=1},
                new COLUMN_MODEL{key="DueDate",value="Hạn thanh toán",check=0},
                new COLUMN_MODEL{key="OutwardNo",value="Số phiếu xuất kho",check=1},
                new COLUMN_MODEL{key="SReason",value="Lý do xuất kho",check=0},
                new COLUMN_MODEL{key="OriginalNo",value="Kèm theo",check=0},
                new COLUMN_MODEL{key="CurrencyID",value="Loại tiền",check=0},
                new COLUMN_MODEL{key="ExchangeRate",value="Tỷ giá",check=0},
                new COLUMN_MODEL{key="InvoiceTemplate",value="Mẫu số HĐ",check=1},
                new COLUMN_MODEL{key="InvoiceSeries",value="Ký hiệu HĐ",check=1},
                new COLUMN_MODEL{key="InvoiceNo",value="Số hóa đơn",check=1},
                new COLUMN_MODEL{key="InvoiceDate",value="Ngày hóa đơn",check=1},
                new COLUMN_MODEL{key="AccountingObjectBankAccount",value="TK ngân hàng",check=0},
                new COLUMN_MODEL{key="AccountingObjectBankName",value="Tên Ngân hàng",check=0},
                new COLUMN_MODEL{key="PaymentMethod",value="Hình thức thanh toán",check=0},
                new COLUMN_MODEL{key="MaHang",value="Mã hàng",check=1},
                new COLUMN_MODEL{key="IsPromotion",value="Hàng khuyến mại",check=1},
                new COLUMN_MODEL{key="DebitAccount",value="TK Nợ",check=1},
                new COLUMN_MODEL{key="CreditAccount",value="TK Có",check=1},
                new COLUMN_MODEL{key="Quantity",value="Số lượng",check=1},
                new COLUMN_MODEL{key="UnitPriceOriginal",value="Đơn giá",check=1},
                new COLUMN_MODEL{key="UnitPrice",value="Đơn giá quy đổi",check=1},
                new COLUMN_MODEL{key="AmountOriginal",value="Thành tiền",check=1},
                new COLUMN_MODEL{key="Amount",value="Thành tiền quy đổi",check=1},
                new COLUMN_MODEL{key="DiscountRate",value="Tỷ lệ CK",check=0},
                new COLUMN_MODEL{key="DiscountAmountOriginal",value="Tiền chiết khấu",check=0},
                new COLUMN_MODEL{key="DiscountAmount",value="Tiền chiết khấu quy đổi",check=0},
                new COLUMN_MODEL{key="DiscountAccount",value="TK chiết khấu",check=0},
                new COLUMN_MODEL{key="SAOrderNo",value="Số đơn hàng",check=0},
                new COLUMN_MODEL{key="LotNo",value="Số lô",check=0},
                new COLUMN_MODEL{key="ExpiryDate",value="Hạn dùng",check=0},
                new COLUMN_MODEL{key="VATDescription",value="Diễn giải thuế",check=0},
                new COLUMN_MODEL{key="ExportTaxRate",value="Tỷ suất thuế XK",check=0},
                new COLUMN_MODEL{key="ExportTaxAmount",value="Tiền thuế XK quy đổi",check=0},
                new COLUMN_MODEL{key="ExportTaxAccount",value="TK thuế XK",check=0},
                new COLUMN_MODEL{key="ExportTaxAccountCorresponding",value="TK đối ứng thuế XK",check=0},
                new COLUMN_MODEL{key="VATRate",value="Tỷ suất thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmountOriginal",value="Tiền thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmount",value="Tiền thuế GTGT quy đổi",check=0},
                new COLUMN_MODEL{key="VATAccount",value="TK thuế GTGT",check=0},
                new COLUMN_MODEL{key="RepositoryAccount",value="TK kho",check=0},
                new COLUMN_MODEL{key="CostAccount",value="TK Giá vốn",check=0},
                new COLUMN_MODEL{key="MaDoiTuongCP",value="ĐT tập hợp chi phí",check=0},
                new COLUMN_MODEL{key="MaHopDong",value="Hợp đồng",check=0},
                new COLUMN_MODEL{key="MaPhongBan",value="Phòng ban",check=0},
                new COLUMN_MODEL{key="MaKhoanMucCP",value="Khoản mục CP",check=0},
                new COLUMN_MODEL{key="MaThongKe",value="Mã thống kê",check=0},
            };
            return list;
        }
        private List<COLUMN_MODEL> GetBHThuTienNgay()
        {
            List<COLUMN_MODEL> list = new List<COLUMN_MODEL>()
            {
                new COLUMN_MODEL{key="No",value="Số chứng từ bán hàng",check=1},
                new COLUMN_MODEL{key="Date",value="Ngày chứng từ",check=1},
                new COLUMN_MODEL{key="PostedDate",value="Ngày hạch toán",check=1},
                new COLUMN_MODEL{key="MaKhachHang",value="Mã khách hàng",check=1},
                new COLUMN_MODEL{key="ContactName",value="Họ tên người liên hệ",check=0},
                new COLUMN_MODEL{key="Reason",value="Diễn giải",check=0},
                new COLUMN_MODEL{key="MaNhanVien",value="NV bán hàng",check=0},
                new COLUMN_MODEL{key="MaDKTT",value="Điều khoản TT",check=0},
                new COLUMN_MODEL{key="Exported",value="Hình thức bán hàng",check=1},
                new COLUMN_MODEL{key="DueDate",value="Hạn thanh toán",check=0},
                new COLUMN_MODEL{key="OutwardNo",value="Số phiếu xuất kho",check=1},
                new COLUMN_MODEL{key="SReason",value="Lý do xuất kho",check=0},
                new COLUMN_MODEL{key="SContactName",value="Họ tên người nhận hàng",check=0},
                new COLUMN_MODEL{key="OriginalNo",value="Kèm theo chứng từ xuất kho",check=0},
                new COLUMN_MODEL{key="CurrencyID",value="Loại tiền",check=0},
                new COLUMN_MODEL{key="ExchangeRate",value="Tỷ giá",check=0},
                new COLUMN_MODEL{key="PhuongThucTT",value="Phương thức thanh toán",check=1},
                new COLUMN_MODEL{key="MNo",value="Số chứng từ thanh toán",check=1},
                new COLUMN_MODEL{key="MReasonPay",value="Lý do nộp tiền",check=0},
                new COLUMN_MODEL{key="MContactName",value="Họ tên người nộp tiền",check=0},
                new COLUMN_MODEL{key="MaTKNopTien",value="Nộp vào tài khoản",check=0},
                new COLUMN_MODEL{key="NumberAttach",value="Kèm theo ct thanh toán",check=0},
                new COLUMN_MODEL{key="InvoiceTemplate",value="Mẫu số HĐ",check=1},
                new COLUMN_MODEL{key="InvoiceSeries",value="Ký hiệu HĐ",check=1},
                new COLUMN_MODEL{key="InvoiceNo",value="Số hóa đơn",check=1},
                new COLUMN_MODEL{key="InvoiceDate",value="Ngày hóa đơn",check=1},
                new COLUMN_MODEL{key="AccountingObjectBankAccount",value="TK ngân hàng",check=0},
                new COLUMN_MODEL{key="AccountingObjectBankName",value="Tên Ngân hàng",check=0},
                new COLUMN_MODEL{key="PaymentMethod",value="Hình thức thanh toán",check=1},
                new COLUMN_MODEL{key="MaHang",value="Mã hàng",check=1},
                new COLUMN_MODEL{key="IsPromotion",value="Hàng khuyến mại",check=1},
                new COLUMN_MODEL{key="DebitAccount",value="TK Nợ",check=1},
                new COLUMN_MODEL{key="CreditAccount",value="TK Có",check=1},
                new COLUMN_MODEL{key="Quantity",value="Số lượng",check=1},
                new COLUMN_MODEL{key="UnitPriceOriginal",value="Đơn giá",check=1},
                new COLUMN_MODEL{key="UnitPrice",value="Đơn giá quy đổi",check=1},
                new COLUMN_MODEL{key="AmountOriginal",value="Thành tiền",check=1},
                new COLUMN_MODEL{key="Amount",value="Thành tiền quy đổi",check=1},
                new COLUMN_MODEL{key="DiscountRate",value="Tỷ lệ CK",check=0},
                new COLUMN_MODEL{key="DiscountAmountOriginal",value="Tiền chiết khấu",check=0},
                new COLUMN_MODEL{key="DiscountAmount",value="Tiền chiết khấu quy đổi",check=0},
                new COLUMN_MODEL{key="DiscountAccount",value="TK chiết khấu",check=0},
                new COLUMN_MODEL{key="SAOrderNo",value="Số đơn hàng",check=0},
                new COLUMN_MODEL{key="LotNo",value="Số lô",check=0},
                new COLUMN_MODEL{key="ExpiryDate",value="Hạn dùng",check=0},
                new COLUMN_MODEL{key="VATDescription",value="Diễn giải thuế",check=0},
                new COLUMN_MODEL{key="ExportTaxRate",value="Tỷ suất thuế XK",check=0},
                new COLUMN_MODEL{key="ExportTaxAmount",value="Tiền thuế XK quy đổi",check=0},
                new COLUMN_MODEL{key="ExportTaxAccount",value="TK thuế XK",check=0},
                new COLUMN_MODEL{key="ExportTaxAccountCorresponding",value="TK đối ứng thuế XK",check=0},
                new COLUMN_MODEL{key="VATRate",value="Tỷ suất thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmountOriginal",value="Tiền thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmount",value="Tiền thuế GTGT quy đổi",check=0},
                new COLUMN_MODEL{key="VATAccount",value="TK thuế GTGT",check=0},
                new COLUMN_MODEL{key="RepositoryAccount",value="TK kho",check=0},
                new COLUMN_MODEL{key="CostAccount",value="TK Giá vốn",check=0},
                new COLUMN_MODEL{key="MaMucThuChi",value="Mục thu/chi",check=0},
                new COLUMN_MODEL{key="MaDoiTuongCP",value="ĐT tập hợp chi phí",check=0},
                new COLUMN_MODEL{key="MaHopDong",value="Hợp đồng",check=0},
                new COLUMN_MODEL{key="MaPhongBan",value="Phòng ban",check=0},
                new COLUMN_MODEL{key="MaKhoanMucCP",value="Khoản mục CP",check=0},
                new COLUMN_MODEL{key="MaThongke",value="Mã thống kê",check=0},
            };
            return list;
        }
        private List<COLUMN_MODEL> GetBHDaiLy()
        {
            List<COLUMN_MODEL> list = new List<COLUMN_MODEL>()
            {
                new COLUMN_MODEL{key="No",value="Số chứng từ bán hàng",check=1},
                new COLUMN_MODEL{key="Date",value="Ngày chứng từ",check=1},
                new COLUMN_MODEL{key="PostedDate",value="Ngày hạch toán",check=1},
                new COLUMN_MODEL{key="MaKhachHang",value="Mã khách hàng",check=1},
                new COLUMN_MODEL{key="ContactName",value="Họ tên người liên hệ",check=0},
                new COLUMN_MODEL{key="Reason",value="Diễn giải",check=0},
                new COLUMN_MODEL{key="MaNhanVien",value="NV bán hàng",check=0},
                new COLUMN_MODEL{key="MaDKTT",value="Điều khoản TT",check=0},
                new COLUMN_MODEL{key="Exported",value="Hình thức bán hàng",check=1},
                new COLUMN_MODEL{key="DueDate",value="Hạn thanh toán",check=0},
                new COLUMN_MODEL{key="OutwardNo",value="Số phiếu xuất kho",check=1},
                new COLUMN_MODEL{key="SReason",value="Lý do xuất kho",check=0},
                new COLUMN_MODEL{key="SContactName",value="Họ tên người nhận hàng",check=0},
                new COLUMN_MODEL{key="OriginalNo",value="Kèm theo chứng từ xuất kho",check=0},
                new COLUMN_MODEL{key="CurrencyID",value="Loại tiền",check=0},
                new COLUMN_MODEL{key="ExchangeRate",value="Tỷ giá",check=0},
                new COLUMN_MODEL{key="PhuongThucTT",value="Phương thức thanh toán",check=1},
                new COLUMN_MODEL{key="MNo",value="Số chứng từ thanh toán",check=0},
                new COLUMN_MODEL{key="MReasonPay",value="Lý do nộp tiền",check=0},
                new COLUMN_MODEL{key="MContactName",value="Họ tên người nộp tiền",check=0},
                new COLUMN_MODEL{key="MaTKNopTien",value="Nộp vào tài khoản",check=0},
                new COLUMN_MODEL{key="NumberAttach",value="Kèm theo ct thanh toán",check=0},
                new COLUMN_MODEL{key="InvoiceTemplate",value="Mẫu số HĐ",check=1},
                new COLUMN_MODEL{key="InvoiceSeries",value="Ký hiệu HĐ",check=1},
                new COLUMN_MODEL{key="InvoiceNo",value="Số hóa đơn",check=1},
                new COLUMN_MODEL{key="InvoiceDate",value="Ngày hóa đơn",check=1},
                new COLUMN_MODEL{key="AccountingObjectBankAccount",value="TK ngân hàng",check=0},
                new COLUMN_MODEL{key="AccountingObjectBankName",value="Tên Ngân hàng",check=0},
                new COLUMN_MODEL{key="PaymentMethod",value="Hình thức thanh toán",check=1},
                new COLUMN_MODEL{key="MaHang",value="Mã hàng",check=1},
                new COLUMN_MODEL{key="IsPromotion",value="Hàng khuyến mại",check=1},
                new COLUMN_MODEL{key="DebitAccount",value="TK Nợ",check=1},
                new COLUMN_MODEL{key="CreditAccount",value="TK Có",check=1},
                new COLUMN_MODEL{key="Quantity",value="Số lượng",check=1},
                new COLUMN_MODEL{key="UnitPriceOriginal",value="Đơn giá",check=1},
                new COLUMN_MODEL{key="UnitPrice",value="Đơn giá quy đổi",check=1},
                new COLUMN_MODEL{key="AmountOriginal",value="Thành tiền",check=1},
                new COLUMN_MODEL{key="Amount",value="Thành tiền quy đổi",check=1},
                new COLUMN_MODEL{key="DiscountRate",value="Tỷ lệ CK",check=0},
                new COLUMN_MODEL{key="DiscountAmountOriginal",value="Tiền chiết khấu",check=0},
                new COLUMN_MODEL{key="DiscountAmount",value="Tiền chiết khấu quy đổi",check=0},
                new COLUMN_MODEL{key="DiscountAccount",value="TK chiết khấu",check=0},
                new COLUMN_MODEL{key="SAOrderNo",value="Số đơn hàng",check=0},
                new COLUMN_MODEL{key="LotNo",value="Số lô",check=0},
                new COLUMN_MODEL{key="ExpiryDate",value="Hạn dùng",check=0},
                new COLUMN_MODEL{key="VATDescription",value="Diễn giải thuế",check=0},
                new COLUMN_MODEL{key="ExportTaxRate",value="Tỷ suất thuế XK",check=0},
                new COLUMN_MODEL{key="ExportTaxAmount",value="Tiền thuế XK quy đổi",check=0},
                new COLUMN_MODEL{key="ExportTaxAccount",value="TK thuế XK",check=0},
                new COLUMN_MODEL{key="ExportTaxAccountCorresponding",value="TK đối ứng thuế XK",check=0},
                new COLUMN_MODEL{key="VATRate",value="Tỷ suất thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmountOriginal",value="Tiền thuế GTGT",check=0},
                new COLUMN_MODEL{key="VATAmount",value="Tiền thuế GTGT quy đổi",check=0},
                new COLUMN_MODEL{key="VATAccount",value="TK thuế GTGT",check=0},
                new COLUMN_MODEL{key="RepositoryAccount",value="TK kho",check=0},
                new COLUMN_MODEL{key="CostAccount",value="TK Giá vốn",check=0},
                new COLUMN_MODEL{key="MaMucThuChi",value="Mục thu/chi",check=0},
                new COLUMN_MODEL{key="MaDoiTuongCP",value="ĐT tập hợp chi phí",check=0},
                new COLUMN_MODEL{key="MaHopDong",value="Hợp đồng",check=0},
                new COLUMN_MODEL{key="MaPhongBan",value="Phòng ban",check=0},
                new COLUMN_MODEL{key="MaKhoanMucCP",value="Khoản mục CP",check=0},
                new COLUMN_MODEL{key="MaThongke",value="Mã thống kê",check=0},
            };
            return list;
        }
        #endregion
    }
}
