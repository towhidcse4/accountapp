﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using Infragistics.Win;
using Infragistics.Win.UltraWinTree;

namespace Accounting
{
    public partial class FolderServer : Form
    {
        public string path = "";
        public Server server = new Server(new ServerConnection(Properties.Settings.Default.ServerName, Properties.Settings.Default.UserName, Properties.Settings.Default.Password));
        public FolderServer(string link)
        {
            if (Utils.isDuLieuMau || string.IsNullOrEmpty(Properties.Settings.Default.Password))//edit by cuongpv: add "|| string.IsNullOrEmpty(Properties.Settings.Default.Password)"
            {
                server = new Server(new ServerConnection(Properties.Settings.Default.ServerName));
            }
            path = link;
            InitializeComponent();
            getServerDrives();
            //treeView.SelectedNode = treeView.Nodes[0];
            //TreeNode SelectedNode = SearchNode(link, treeView.Nodes[0]);
            //treeView.SelectedNode = SelectedNode;

            //treeView.SelectedNode.Expand();
            //treeView.Select();
        }
        public void getServerDrives()
        {
            DataTable d = server.EnumAvailableMedia();
            foreach (DataRow r in d.Rows)
                treeView.Nodes.Add(new TreeNode(r["Name"].ToString(), 0, 0));
            if (!Utils.ISystemOptionService.GetByCode("SLUU_TMSaoLuu").Data.IsNullOrEmpty())
            {
                var c = path.Split('\\');
                string l = c[0];
                TreeNode node = new TreeNode();
                foreach (TreeNode item in treeView.Nodes)
                {
                    if (item.Text == c[0]) node = item;
                    continue;
                }
                for (int i = 1; i < c.Count(); i++)
                {
                    AddNodeFromPath(node, l);
                    foreach (TreeNode item in node.Nodes)
                    {
                        if (item.Text.Equals(c[i])) node = item;
                        continue;
                    }
                    l += "\\" + c[i];
                }
                treeView.SelectedNode = node;
            }
            else
            {


            }

        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            e.Node.Nodes.Clear();
            DataSet ds = new DataSet();
            ds = server.ConnectionContext.ExecuteWithResults(string.Format("exec xp_dirtree '{0}', 1, 1", e.Node.FullPath));
            ds.Tables[0].DefaultView.Sort = "file ASC"; // list directories first, then files
            DataTable d = ds.Tables[0].DefaultView.ToTable();
            foreach (DataRow r in d.Rows)
            {
                if (r["file"].ToString() == "0")
                    e.Node.Nodes.Add(new TreeNode(r["subdirectory"].ToString(), 1, 1));
                if (r["file"].ToString() == "1")
                    e.Node.Nodes.Add(new TreeNode(r["subdirectory"].ToString(), 2, 2));

            }


        }

        private void ButDongY_Click(object sender, EventArgs e)
        {
            TreeNode node = treeView.SelectedNode;
            path = node.FullPath;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void treeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            e.Node.Nodes.Clear();
            DataSet ds = new DataSet();
            ds = server.ConnectionContext.ExecuteWithResults(string.Format("exec xp_dirtree '{0}', 1, 1", e.Node.FullPath));
            ds.Tables[0].DefaultView.Sort = "file ASC"; // list directories first, then files
            DataTable d = ds.Tables[0].DefaultView.ToTable();
            foreach (DataRow r in d.Rows)
            {
                if (r["file"].ToString() == "0")
                    e.Node.Nodes.Add(new TreeNode(r["subdirectory"].ToString(), 1, 1));
                if (r["file"].ToString() == "1")
                    e.Node.Nodes.Add(new TreeNode(r["subdirectory"].ToString(), 2, 2));

            }

        }
        private void AddNodeFromPath(TreeNode Node, string path)
        {
            DataSet ds = new DataSet();
            ds = server.ConnectionContext.ExecuteWithResults(string.Format("exec xp_dirtree '{0}', 1, 1", path));
            ds.Tables[0].DefaultView.Sort = "file ASC"; // list directories first, then files
            DataTable d = ds.Tables[0].DefaultView.ToTable();
            foreach (DataRow r in d.Rows)
            {
                if (r["file"].ToString() == "0")
                    Node.Nodes.Add(new TreeNode(r["subdirectory"].ToString(), 1, 1));
                if (r["file"].ToString() == "1")
                    Node.Nodes.Add(new TreeNode(r["subdirectory"].ToString(), 2, 2));

            }
        }
        private TreeNode SearchNode(string SearchText, TreeNode StartNode)
        {
            TreeNode node = null;
            while (StartNode != null)
            {
                foreach (var item in SearchText.Split('\\'))
                {
                    if (StartNode.Text.ToLower().Contains(item.ToLower()))
                    {
                        node = StartNode;
                        break;
                    };
                }
                if (StartNode.Nodes.Count != 0)
                {
                    node = SearchNode(SearchText, StartNode.Nodes[0]);//Recursive Search
                    if (node != null)
                    {
                        break;
                    };
                };
                StartNode = StartNode.NextNode;
            };
            return node;
        }
    }
}
