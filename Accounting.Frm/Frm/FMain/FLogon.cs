﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using Accounting.TextMessage;
using System.Diagnostics;
using Accounting.Core.Domain;
using System.Net.Mail;
using System.Net;
using Accounting.Core;

namespace Accounting
{
    public partial class FLogon : CustormForm
    {
        public bool isMail = true;
        private bool isSetting = false;
        private static Thread ThdLogOn;
        private DataTable _servers;
        static FLogon logOn;
        private Company _company { get; set; }
        public Company CurrentCompany
        {
            get
            {
                return _company;
            }
        }
        static System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();
        private void setConnectInfo()
        {
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            String solutionName = Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName);
            var filePath = string.Format("{0}\\{1}.config", path, solutionName).Replace("file:\\", "");

            XmlDocument xmlDoc = Utils.readXmlConfig(filePath);
            XmlNode root = xmlDoc.DocumentElement;
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);

            if (root != null)
            {
                XmlNodeList nodeList = root.SelectNodes("descendant::setting");
                if (nodeList != null)
                    foreach (XmlNode cfgNode in nodeList)
                    {
                        //int age = int.Parse(userNode.Attributes["age"].Value);
                        if (cfgNode.Attributes != null && cfgNode.Attributes["name"] != null)
                        {
                            #region save connection info
                            if (cfgNode.Attributes["name"].Value.Equals("ServerName"))
                            {
                                var value = cfgNode.SelectNodes("value");
                                if (value.Count > 0)
                                {
                                    value[0].InnerText = cbbServers.Text;
                                }
                                //cfgNode.SetProperty("value", cbbServers.Text);
                            }

                            if (cfgNode.Attributes["name"].Value.Equals("DatabaseName"))
                            {
                                var value = cfgNode.SelectNodes("value");
                                if (value.Count > 0)
                                {
                                    value[0].InnerText = cbbDatabases.Text;
                                }
                                //cfgNode.SetProperty("value", cbbDatabases.Text);
                            }

                            if (cfgNode.Attributes["name"].Value.Equals("UserName"))
                            {
                                var value = cfgNode.SelectNodes("value");
                                if (value.Count > 0)
                                {
                                    value[0].InnerText = txtUsrServer.Text;
                                }
                                //cfgNode.SetProperty("value", txtUsrServer.Text);
                            }

                            if (cfgNode.Attributes["name"].Value.Equals("Password"))
                            {
                                var value = cfgNode.SelectNodes("value");
                                if (value.Count > 0)
                                {
                                    value[0].InnerText = Core.ITripleDes.TripleDesDefaultEncryption(txtPwdServer.Text);
                                }
                                //cfgNode.SetProperty("value", txtPwdServer.Text);
                            }

                            if (cfgNode.Attributes["name"].Value.Equals("UserApp"))
                            {
                                var value = cfgNode.SelectNodes("value");
                                if (value.Count > 0)
                                {
                                    value[0].InnerText = txtUser.Text;
                                }
                                //cfgNode.SetProperty("value", txtUser.Text);
                            }

                            if (cfgNode.Attributes["name"].Value.Equals("PassApp"))
                            {
                                var value = cfgNode.SelectNodes("value");
                                if (value.Count > 0)
                                {
                                    value[0].InnerText = Core.ITripleDes.TripleDesDefaultEncryption(txtPassword.Text);
                                }
                                //cfgNode.SetProperty("value", txtPassword.Text);
                            }
                            #endregion
                        }
                    }
                #region Đặt lại connectionString cho phần báo cáo trong file app.conf
                XmlNodeList nodeReportConnect = root.SelectNodes("descendant::connectionStrings/add");
                string connString = string.Format("Data Source={0};initial catalog={1};Persist Security Info=True;{2}", cbbServers.Text, cbbDatabases.Text, (txtUsrServer.Text.Trim().ToUpper().Equals("ADMIN") && string.IsNullOrEmpty(txtPwdServer.Text.Trim()) ? "Integrated Security=SSPI" : string.Format("User Id={0};Password={1};", txtUsrServer.Text, txtPwdServer.Text)));
                foreach (XmlNode nodeConnect in nodeReportConnect)
                {

                    if (nodeConnect.Attributes != null && nodeConnect.Attributes["name"] != null)
                    {
                        if (nodeConnect.Attributes["name"].Value.Equals("ConnectionString"))
                        {
                            nodeConnect.Attributes["connectionString"].Value = connString;
                        }
                    }

                }
                #endregion
            }
            xmlDoc.Save(filePath);
            System.Configuration.ConfigurationManager.RefreshSection("connectionStrings");
        }

        private void SaveCompany()
        {

            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string CustomerFilePath = string.Format("{0}\\companies.xml", path).Replace("file:\\", "");
            var companies = XmlToObject.DeserializeFromFile<Company>(CustomerFilePath) ?? new List<Company>();
            //var companies = Utils.ListCompanies ?? new List<Company>();

            Company objectT = companies.FirstOrDefault(o => o.ServerName == cbbServers.Text && o.Database == cbbDatabases.Text);

            if (objectT == null)
            {
                objectT = new Company();
                objectT.ID = Guid.NewGuid();
                objectT.ServerName = cbbServers.Text;
                objectT.Database = cbbDatabases.Text;
                companies.Add(objectT);
            }

            objectT.CompanyName = Utils.ListSystemOption.FirstOrDefault(p => p.Code.Equals("TTDN_Tencty")).Data;
            objectT.CompanyTaxCode = Utils.ListSystemOption.FirstOrDefault(p => p.Code.Equals("TTDN_MST")).Data;
            objectT.Username = txtUsrServer.Text;
            objectT.Password = Core.ITripleDes.TripleDesDefaultEncryption(txtPwdServer.Text);
            objectT.AppUser = txtUser.Text;
            objectT.AppPass = Core.ITripleDes.TripleDesDefaultEncryption(txtPassword.Text);

            XmlToObject.RewriteFile(CustomerFilePath, companies, "Companies");
            _company = objectT;
        }

        private void getConnectInfo()
        {
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            String solutionName = Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName);
            var filePath = string.Format("{0}\\{1}.config", path, solutionName).Replace("file:\\", "");

            XmlDocument xmlDoc = Utils.readXmlConfig(filePath);
            XmlNode root = xmlDoc.DocumentElement;
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);

            if (root != null)
            {
                XmlNodeList nodeList = root.SelectNodes("descendant::setting");
                if (nodeList != null)
                    foreach (XmlNode cfgNode in nodeList)
                    {
                        //int age = int.Parse(userNode.Attributes["age"].Value);
                        if (cfgNode.Attributes != null && cfgNode.Attributes["name"] != null)
                        {
                            #region setup connection info
                            if (cfgNode.Attributes["name"].Value.Equals("ServerName"))
                            {
                                var value = cfgNode.SelectNodes("value");
                                if (value.Count > 0)
                                {
                                    var serverName = value[0].InnerText;
                                    cbbServers.Text = string.IsNullOrEmpty(serverName) ? Properties.Settings.Default.ServerName : serverName;
                                }
                            }

                            if (cfgNode.Attributes["name"].Value.Equals("DatabaseName"))
                            {
                                var value = cfgNode.SelectNodes("value");
                                if (value.Count > 0)
                                {
                                    var database = value[0].InnerText;
                                    cbbDatabases.Value = string.IsNullOrEmpty(database) ? Properties.Settings.Default.DatabaseName : database;
                                }

                            }

                            if (cfgNode.Attributes["name"].Value.Equals("UserName"))
                            {
                                var value = cfgNode.SelectNodes("value");
                                if (value.Count > 0)
                                {
                                    var userServer = value[0].InnerText;
                                    txtUsrServer.Value = string.IsNullOrEmpty(userServer) ? Properties.Settings.Default.UserName : userServer;
                                }
                            }

                            if (cfgNode.Attributes["name"].Value.Equals("Password"))
                            {
                                var value = cfgNode.SelectNodes("value");
                                if (value.Count > 0)
                                {
                                    var passServer = value[0].InnerText;
                                    txtPwdServer.Value = string.IsNullOrEmpty(passServer) ? Properties.Settings.Default.Password : Core.ITripleDes.TripleDesDefaultDecryption(passServer);
                                }
                            }

                            if (cfgNode.Attributes["name"].Value.Equals("UserApp"))
                            {
                                var value = cfgNode.SelectNodes("value");
                                if (value.Count > 0)
                                {
                                    var userApp = value[0].InnerText;
                                    txtUser.Value = string.IsNullOrEmpty(userApp) ? Properties.Settings.Default.UserApp : userApp;
                                }
                            }

                            if (cfgNode.Attributes["name"].Value.Equals("PassApp"))
                            {
                                var value = cfgNode.SelectNodes("value");
                                if (value.Count > 0)
                                {
                                    var passApp = value[0].InnerText;
                                    //txtPassword.Value = string.IsNullOrEmpty(passApp) ? Properties.Settings.Default.PassApp : Core.ITripleDes.TripleDesDefaultDecryption(passApp);
                                }

                            }
                            #endregion
                        }
                    }
            }
            xmlDoc.Save(filePath);
        }
        public FLogon()
        {
            InitializeComponent();
            try
            {
                getConnectInfo();
            }
            catch (Exception ex)
            {
                MSG.Warning(resSystem.MSG_System_55);
            }
        }
        private bool IsProcessOpen(string name)
        {
            foreach (Process clsProcess in Process.GetProcesses())
            {
                if (clsProcess.ProcessName.Contains(name) && !clsProcess.ProcessName.Contains("vshost"))
                {
                    return true;
                }
            }

            return false;
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
        private void btnLogOn_Click(object sender, EventArgs e)
        {
            // Logon Accounting Software
            try
            {

                var process = LogOnProcess(cbbServers.Text.Trim(), txtUsrServer.Text.Trim(), txtPwdServer.Text.Trim(), cbbDatabases.Text.Trim(), txtUser.Text.Trim(), txtPassword.Text.Trim());

                if (process)
                {
                    SaveCompany();
                    Close();
                }
            }
            catch (Exception ex)
            {
                viewException(ex);
                MSG.Warning(resSystem.MSG_System_55);
            }
        }
        public DateTime GetTimeUse()
        {
            DateTime time = new DateTime(2021, 05, 31); //YYYY MM DD
            return time;
        }
        public bool LogOnProcess(string server, string user, string pass, string database, string appUser, string appPass)
        {
            cbbServers.Text = server;
            txtUsrServer.Text = user;
            txtPwdServer.Text = pass;
            cbbDatabases.Text = database;
            txtUser.Text = appUser;
            txtPassword.Text = appPass;
            DateTime dateExp = GetTimeUse();
            // Update new version database 
            Update update = new Update(cbbServers.Text, txtUsrServer.Text, txtPwdServer.Text, cbbDatabases.Text);
            update.Exec();

            // Logon Accounting Software
            string connString = string.Format("Data Source={0};initial catalog={1};{2}", server, database, (user.ToUpper().Equals("ADMIN") && string.IsNullOrEmpty(pass) ? "Integrated Security=SSPI" : string.Format("User Id={0};Password={1};", user, pass)));
            string path = System.IO.Path.GetDirectoryName(
            System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            var filePatch = string.Format("{0}\\Config\\NHibernateConfig", path).Replace("file:\\", "");
            try
            {
                System.IO.File.SetAttributes(filePatch, System.IO.FileAttributes.Normal);
                DirectorySecurity sec = Directory.GetAccessControl(filePatch);
                SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                Directory.SetAccessControl(filePatch, sec);
                FileSecurity security = File.GetAccessControl(filePatch);
                SecurityIdentifier everybody = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                security.AddAccessRule(new FileSystemAccessRule(everybody, FileSystemRights.FullControl, AccessControlType.Allow));
                File.SetAccessControl(filePatch, security);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(filePatch);
                XmlNode root = xmlDoc.DocumentElement;
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
                nsmgr.AddNamespace("ns", "urn:nhibernate-configuration-2.2");
                if (root != null)
                {
                    XmlNodeList nodeList = root.SelectNodes("//ns:property", nsmgr);
                    if (nodeList != null)
                        foreach (XmlNode cfgNode in nodeList)
                        {
                            //int age = int.Parse(userNode.Attributes["age"].Value);
                            if (cfgNode.Attributes != null && cfgNode.Attributes["name"] != null && cfgNode.Attributes["name"].Value.Equals("connection.connection_string"))
                            {
                                if (server != string.Empty)
                                {
                                    cfgNode.InnerXml = connString;
                                }
                                else
                                {
                                    connString = cfgNode.InnerXml;
                                }
                            }
                        }
                }
                xmlDoc.Save(filePatch);
                setConnectInfo();

                #region Load config dữ liệu thiết lập hệ thống (Định dạng thời gian, số, chuỗi, ngôn ngữ....)
                //số chữ số sau dấu phẩy, ký hiệu ngăn cách, ký hiệu tiền tệ, giao diện nhập liệu, màn hình hiển thị.....
                // Creates a CultureInfo for US in en.
                var culInfo = Utils.Config();
                try
                {
                    Thread.CurrentThread.CurrentCulture = culInfo;
                }
                catch (Exceptions ex)
                {
                    MSG.Warning(resSystem.MSG_System_57);
                    return false;
                }
                #endregion

                ConstFrm.DbStartDate = Utils.GetDbStartDate();
                if (appUser.ToUpper().Equals("ADMIN") && string.IsNullOrEmpty(appPass))
                    appPass = "tvan@123";
                var checkAcc = Authenticate.CheckUser(appUser, appPass);
                //if (!checkAcc)
                //{
                //    MSG.Warning(resSystem.MSG_System_56);
                //    return false;
                //}
                //if (DateTime.Now.Date > dateExp)
                //{
                //    MSG.Warning(resSystem.MSG_System_83);
                //    return false;
                //}
                DialogResult = DialogResult.OK;
                Authenticate.Server = server;
                Authenticate.Database = database;
                Properties.Settings.Default.ServerName = server;
                Properties.Settings.Default.DatabaseName = database;
                Properties.Settings.Default.UserName = user;
                Properties.Settings.Default.Password = pass;
                Properties.Settings.Default.UserApp = appUser;
                Properties.Settings.Default.PassApp = appPass;
                Properties.Settings.Default.Save();

                Utils.AutoSetDBStartDate();
                Utils.CheckRegistration();

                return true;
            }
            catch (Exception ex)
            {
                MSG.Warning(resSystem.MSG_System_55);
                return false;
            }
        }
        private void viewException(Exception ex)
        {
            var mes = ex.Message;
            var trace = ex.StackTrace;
        }
        static private void ShowFormThread()
        {
            logOn = new FLogon();
            //Application.Run(logOn);
            var frm = Application.OpenForms.Cast<Form>().FirstOrDefault(x => x.Name == "FStartApp");
            if (null != frm)
                logOn.ShowDialog(frm);
            else
                logOn.ShowDialog();
            _logOn = logOn.DialogResult == DialogResult.OK;
        }

        private static bool? _logOn;
        public static bool? LogOn
        {
            get
            {
                if (_logOn.HasValue) return _logOn.Value;
                //ThdLogOn = new Thread(new ThreadStart(ShowFormThread));
                //ThdLogOn.IsBackground = true;
                //ThdLogOn.SetApartmentState(ApartmentState.STA);
                //ThdLogOn.Start();
                //ThdLogOn.Join();
                //while (!_logOn.HasValue)
                //{
                //    Thread.Sleep(1);
                //}
                ShowFormThread();
                var status = _logOn.Value;
                //ThdLogOn.Abort();
                return status;
            }
            set { _logOn = value; }
        }

        private void FLogon_Load(object sender, EventArgs e)
        {
            this.BringToFront();
            this.Activate();
        }

        private void cbbServers_BeforeDropDown(object sender, CancelEventArgs e)
        {
            if (_servers == null)
                _servers = SqlDataSourceEnumerator.Instance.GetDataSources();
            cbbServers.Items.Clear();
            for (int i = 0; i < _servers.Rows.Count; i++)
            {
                //if (myServer == servers.Rows[i]["ServerName"].ToString()) ///// used to get the servers in the local machine////
                //{
                if ((_servers.Rows[i]["InstanceName"] as string) != null)
                    cbbServers.Items.Add(string.Format("{0}\\{1}", _servers.Rows[i]["ServerName"], _servers.Rows[i]["InstanceName"]));
                else
                    cbbServers.Items.Add(_servers.Rows[i]["ServerName"]);
                //}
            }
        }

        private void cbbDatabases_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string strError = string.Empty;
            try
            {
                cbbDatabases.Items.Clear();
                string serverName = cbbServers.Text;
                string connString = string.Format("Data Source={0};initial catalog=master;{1}", serverName, (txtUsrServer.Text.Trim().ToUpper().Equals("ADMIN") && string.IsNullOrEmpty(txtPwdServer.Text.Trim()) ? "Integrated Security=SSPI" : string.Format("User Id={0};Password={1};", txtUsrServer.Text, txtPwdServer.Text)));
                //
                string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                var filePatch = string.Format("{0}\\Config\\NHibernateConfig", path).Replace("file:\\", "");
                System.IO.File.SetAttributes(filePatch, System.IO.FileAttributes.Normal);
                DirectorySecurity sec = Directory.GetAccessControl(filePatch);
                SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                Directory.SetAccessControl(filePatch, sec);
                FileSecurity security = File.GetAccessControl(filePatch);
                SecurityIdentifier everybody = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                security.AddAccessRule(new FileSystemAccessRule(everybody, FileSystemRights.FullControl, AccessControlType.Allow));
                File.SetAccessControl(filePatch, security);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(filePatch);
                XmlNode root = xmlDoc.DocumentElement;
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
                nsmgr.AddNamespace("ns", "urn:nhibernate-configuration-2.2");
                if (root != null)
                {
                    XmlNodeList nodeList = root.SelectNodes("//ns:property", nsmgr);
                    if (nodeList != null)
                        foreach (XmlNode cfgNode in nodeList)
                        {
                            if (cfgNode.Attributes != null && cfgNode.Attributes["name"] != null && cfgNode.Attributes["name"].Value.Equals("connection.connection_string"))
                                connString = cfgNode.InnerXml;
                        }
                }
                xmlDoc.Save(filePatch);
                //MSG.Warning(connString);
                //return;
                using (var con = new SqlConnection(connString))
                {
                    using (var da = new SqlDataAdapter("SELECT Name FROM master.sys.databases where Name!='master' AND Name!='tempdb' AND Name!='model' AND Name!='msdb'", con))
                    {
                        var ds = new DataSet();
                        da.Fill(ds);
                        //cbbDatabases.DataSource = ds.Tables[0].Rows[0];
                        if (ds.Tables[0].Rows.Count == 0) goto Error;
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            cbbDatabases.Items.Add(row["Name"]);
                        }
                        //cbbDatabases.DataBind();
                        //...
                    }
                }
                goto End;
            }
            catch (Exception ex)
            {
                strError = ex.ToString();
                goto Error;
            }
        Error:
            strError = string.Format("{0}", resSystem.MSG_System_55);
            //!string.IsNullOrEmpty(strError)
            //    ? string.Format("\r\n Chi tiết lỗi : \r\n {0}", strError)
            //    : "");
            MSG.Warning(strError);
        End:
            return;
        }

        protected override bool ProcessCmdKey(ref Message message, Keys keys)
        {
            switch (keys)
            {
                case Keys.Enter: btnLogOn.PerformClick(); break;
            }
            return base.ProcessCmdKey(ref message, keys);
        }

        private void btnSetting_Click(object sender, EventArgs e)
        {
            isSetting = !isSetting;
            Size = new Size(Size.Width, isSetting ? 466 : 355);
        }

        private void btnRefreshServer_Click(object sender, EventArgs e)
        {
            _servers = SqlDataSourceEnumerator.Instance.GetDataSources();
        }

        private void lblForgotpas_Click(object sender, EventArgs e) // chuongnv chức năng quên mật khẩu
        {
            if (!isMail)
            {
                MSG.Warning("Hệ thống đã gửi mật khẩu mới vào địa chỉ Email đã khai báo. Vui lòng kiểm tra hòm thư để lấy mật khẩu truy cập hoặc thử lại chức năng này sau 5 phút!");
                return;
            }
            string kytu = "!@#$%^&*?.;";
            if (txtUser.Text.ToUpper().Any(x => kytu.Any(c => c == x)))
            {
                MSG.Warning("Tài khoản không được phép có ký tự đặc biệt!");
                return;
            }
            if (MSG.MessageBoxStand("Bạn có chắc chắn muốn lấy lại mật khẩu hay không?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
            string connString = string.Format("Data Source={0};initial catalog={1};{2}", cbbServers.Text, cbbDatabases.Text, (txtUsrServer.Text.ToUpper().Equals("ADMIN") && string.IsNullOrEmpty(txtPwdServer.Text) ? "Integrated Security=SSPI" : string.Format("User Id={0};Password={1};", txtUsrServer.Text, txtPwdServer.Text)));
            SqlConnection con = new SqlConnection(connString);
            con.Open();
            string Email = "";
            bool isAdmin = false;
            string Pass = "";
            try
            {
                using (SqlCommand sqlCmd1 = new SqlCommand("SELECT Data From SystemOption where Code = 'EmailForgotPass'", con))
                {
                    SqlDataReader reader1 = sqlCmd1.ExecuteReader();
                    if (reader1.HasRows)
                    {
                        while (reader1.Read())
                        {
                            Email = reader1[0].ToString();
                        }
                    }
                    reader1.Close();

                }
                using (SqlCommand sqlCmd2 = new SqlCommand("SELECT IsSystem, password From Sys_User where username = '" + txtUser.Text.Trim().ToUpper() + "'", con))
                {
                    SqlDataReader reader2 = sqlCmd2.ExecuteReader();
                    if (reader2.HasRows)
                    {
                        while (reader2.Read())
                        {
                            isAdmin = Convert.ToBoolean(reader2[0].ToString());
                            Pass = reader2[1].ToString();
                        }
                    }
                    reader2.Close();
                }
            }
            catch (SqlException)
            {
                MSG.Warning("Máy chủ SQL không tồn tại. Vui lòng chọn máy chủ SQL khác.");
                return;
            }
            if (!isAdmin)
            {
                MSG.Warning("Chức năng này chỉ để thay đổi mật khẩu cho tài khoản ADMIN. Vui lòng liên hệ người quản trị để đổi mật khẩu!");
                return;
            }
            if (string.IsNullOrEmpty(Pass) && isAdmin)
            {
                MSG.Information("Tài khoản ADMIN hiện không sử dụng mật khẩu!");
                return;
            }
            if (string.IsNullOrEmpty(Email))
            {
                MSG.Warning("Không thể sử dụng chức năng này vì tài khoản quản trị không khai báo email lấy lại mật khẩu đăng nhập khi quên mật khẩu." +
                    " Vui lòng liên hệ với đơn vị cung cấp phần mềm để được trợ giúp!");
                return;
            }
            string pass = GenNewPass();
            try
            {
                SendEmail(Email, pass);
                string s = FX.Utils.Encryption.StringToMD5Hash(pass);
                SqlTransaction transaction = con.BeginTransaction();
                try
                {
                    new SqlCommand("UPDATE Sys_User SET password = '" + s + "' WHERE username = '" + txtUser.Text.Trim().ToUpper() + "'", con, transaction).ExecuteNonQuery();
                    transaction.Commit();
                    con.Close();
                }
                catch (Exception ex)
                {
                    MSG.Warning("Lỗi hệ thống!");
                    return;
                }
                MSG.Information("Hệ thống đã gửi mật khẩu đăng nhập mới vào email " + Email + ". Bạn vui lòng kiểm tra hộp thư và đăng nhập lại theo mật khẩu mới!");
                isMail = false;
                Thread newThread = new Thread(Action);
                newThread.Start();
            }    
            catch(Exception ex)
            {
                MSG.Warning("Lỗi gửi mail. Vui lòng thử lại!");
            }
        }
        public void Action()
        {
            Thread.Sleep(250000);
            isMail = true;
        }
        public static void SendEmail(string toEmail, string pass) // chuongnv Gửi mail
        {
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress("info@hdstech.vn");
                message.To.Add(new MailAddress(toEmail));
                message.Subject = "[] Đặt lại mật khẩu đăng nhập phần mềm kế toán hdstech";
                message.IsBodyHtml = true;   
                message.Body = EmailBody(pass);
                smtp.Port = 587;
                smtp.Host = "mail.xxx.vn"; 
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("info@xxx.vn", "Sds@12345678");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
        }
        public static string EmailBody(string pass) //chuongnv nội dung mail
        {
            string msg =
                   "<h3 style=\"text-align:center;\">ĐẶT LẠI MẬT KHẨU ĐĂNG NHẬP PHẦN MỀM KẾ TOÁN</h3>"
                 + "<p style=\"margin-left:20px;\"><i>Kính gửi: Quý khách hàng!</i></p>"
                 + "<p>Hệ thống nhận được yêu cầu đặt lại mật khẩu tài khoản quản trị(ADMIN) của phần mềm kế toán </p>"
                 + "<p>Mật khẩu mới của quý khách là: <b>" + pass + "</b></p>"
                 + "<p>Quý khách vui lòng nhập mật khẩu mới này để truy cập phần mềm</p>"
                 + "<p>(Đây là email tự động, vui lòng không phản hồi lại email này)</p>"
                 + "<p><i>Trân trọng!</i></p>"
                 + "<p><b>Công ty CP đầu tư công nghệ và thương mại xxx</b></p>";
            return msg;
        }
        public static string GenNewPass() //chuongnv Sinh pass tự động
        {
            string validChars = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            Random random = new Random();
            char[] chars = new char[8];
            for (int i = 0; i < 8; i++)
            {
                chars[i] = validChars[random.Next(0, validChars.Length)];
            }
            return new string(chars);
        }
    }
}
