﻿namespace Accounting
{
    partial class UCGeneral
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCGeneral));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnGOtherVoucher = new Infragistics.Win.Misc.UltraButton();
            this.btnFGOInterestAndLossTransfer = new Infragistics.Win.Misc.UltraButton();
            this.btnGDBDateClosed = new Infragistics.Win.Misc.UltraButton();
            this.btnBaoCao = new Infragistics.Win.Misc.UltraButton();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(176, 196);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 35);
            this.label1.TabIndex = 29;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(406, 196);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 35);
            this.label2.TabIndex = 31;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.Location = new System.Drawing.Point(636, 196);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 35);
            this.label3.TabIndex = 33;
            // 
            // btnGOtherVoucher
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance1.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnGOtherVoucher.Appearance = appearance1;
            this.btnGOtherVoucher.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
            this.btnGOtherVoucher.HotTrackAppearance = appearance2;
            this.btnGOtherVoucher.ImageSize = new System.Drawing.Size(80, 80);
            this.btnGOtherVoucher.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnGOtherVoucher.Location = new System.Drawing.Point(44, 165);
            this.btnGOtherVoucher.Name = "btnGOtherVoucher";
            this.btnGOtherVoucher.Size = new System.Drawing.Size(92, 97);
            this.btnGOtherVoucher.TabIndex = 39;
            // 
            // btnFGOInterestAndLossTransfer
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnFGOInterestAndLossTransfer.Appearance = appearance3;
            this.btnFGOInterestAndLossTransfer.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
            this.btnFGOInterestAndLossTransfer.HotTrackAppearance = appearance4;
            this.btnFGOInterestAndLossTransfer.ImageSize = new System.Drawing.Size(80, 80);
            this.btnFGOInterestAndLossTransfer.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnFGOInterestAndLossTransfer.Location = new System.Drawing.Point(274, 165);
            this.btnFGOInterestAndLossTransfer.Name = "btnFGOInterestAndLossTransfer";
            this.btnFGOInterestAndLossTransfer.Size = new System.Drawing.Size(92, 97);
            this.btnFGOInterestAndLossTransfer.TabIndex = 40;
            // 
            // btnGDBDateClosed
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance5.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnGDBDateClosed.Appearance = appearance5;
            this.btnGDBDateClosed.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
            this.btnGDBDateClosed.HotTrackAppearance = appearance6;
            this.btnGDBDateClosed.ImageSize = new System.Drawing.Size(80, 80);
            this.btnGDBDateClosed.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnGDBDateClosed.Location = new System.Drawing.Point(504, 165);
            this.btnGDBDateClosed.Name = "btnGDBDateClosed";
            this.btnGDBDateClosed.Size = new System.Drawing.Size(92, 97);
            this.btnGDBDateClosed.TabIndex = 41;
            // 
            // btnBaoCao
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.Image = ((object)(resources.GetObject("appearance7.Image")));
            appearance7.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance7.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance7.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnBaoCao.Appearance = appearance7;
            this.btnBaoCao.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance8.Image = ((object)(resources.GetObject("appearance8.Image")));
            this.btnBaoCao.HotTrackAppearance = appearance8;
            this.btnBaoCao.ImageSize = new System.Drawing.Size(80, 80);
            this.btnBaoCao.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnBaoCao.Location = new System.Drawing.Point(734, 165);
            this.btnBaoCao.Name = "btnBaoCao";
            this.btnBaoCao.Size = new System.Drawing.Size(92, 97);
            this.btnBaoCao.TabIndex = 42;
            // 
            // UCGeneral
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.btnBaoCao);
            this.Controls.Add(this.btnGDBDateClosed);
            this.Controls.Add(this.btnFGOInterestAndLossTransfer);
            this.Controls.Add(this.btnGOtherVoucher);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "UCGeneral";
            this.Size = new System.Drawing.Size(890, 424);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Infragistics.Win.Misc.UltraButton btnGOtherVoucher;
        private Infragistics.Win.Misc.UltraButton btnFGOInterestAndLossTransfer;
        private Infragistics.Win.Misc.UltraButton btnGDBDateClosed;
        private Infragistics.Win.Misc.UltraButton btnBaoCao;
    }
}
