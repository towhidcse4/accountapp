﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.TextMessage;

namespace Accounting
{
    public partial class UCDesktop : UserControl
    {
        FMain main;
        public UCDesktop(FMain @this)
        {
            InitializeComponent();
            btnMuaHang.Click += (s, e) => btnMuaHang_Click(s, e, @this);
            btnBanHang.Click += (s, e) => btnBanHang_Click(s, e, @this);
            btnKho.Click += (s, e) => btnKho_Click(s, e, @this);
            btnCDDC.Click += (s, e) => btnCDDC_Click(s, e, @this);
            btnTaiSanCoDinh.Click += (s, e) => btnTaiSanCoDinh_Click(s, e, @this);
            btnTienVaNH.Click += (s, e) => btnTienVaNH_Click(s, e, @this);
            btnHoaDon.Click += (s, e) => btnHoaDon_Click(s, e, @this);
            btnKeToanGiaThanh.Click += (s, e) => btnKeToanGiaThanh_Click(s, e, @this);
            btnBaoCao.Click += (s, e) => btnBaoCao_Click(s, e, @this);
            btnHopDong.Click += (s, e) => btnHopDong_Click(s, e, @this);
            btnKeToanThue.Click += (s, e) => btnKeToanThue_Click(s, e, @this);

            main = @this;
        }

        private void btnMuaHang_Click(object sender, EventArgs e, FMain fMain)
        {
            //fMain.ConfigSubGroupMain(new Infragistics.Win.UltraWinExplorerBar.GroupEventArgs(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup { Key = "PurchasePriceModule", Text = "Mua hàng" }));
            fMain.uExBar.Groups["PurchasePriceModule"].Active = true;
            fMain.uExBar.Groups["PurchasePriceModule"].Selected = true;
        }

        private void btnBanHang_Click(object sender, EventArgs e, FMain fMain)
        {
            //fMain.ConfigSubGroupMain(new Infragistics.Win.UltraWinExplorerBar.GroupEventArgs(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup { Key = "SalePriceModule", Text = "Bán hàng" }));
            fMain.uExBar.Groups["SalePriceModule"].Active = true;
            fMain.uExBar.Groups["SalePriceModule"].Selected = true;
        }

        private void btnKho_Click(object sender, EventArgs e, FMain fMain)
        {
            //fMain.ConfigSubGroupMain(new Infragistics.Win.UltraWinExplorerBar.GroupEventArgs(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup { Key = "RepositoryModule", Text = "Kho" }));
            fMain.uExBar.Groups["RepositoryModule"].Active = true;
            fMain.uExBar.Groups["RepositoryModule"].Selected = true;
        }

        private void btnCDDC_Click(object sender, EventArgs e, FMain fMain)
        {
            //fMain.ConfigSubGroupMain(new Infragistics.Win.UltraWinExplorerBar.GroupEventArgs(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup { Key = "ToolModule", Text = "Công cụ dụng cụ" }));
            fMain.uExBar.Groups["ToolModule"].Active = true;
            fMain.uExBar.Groups["ToolModule"].Selected = true;
        }

        private void btnTaiSanCoDinh_Click(object sender, EventArgs e, FMain fMain)
        {
            //fMain.ConfigSubGroupMain(new Infragistics.Win.UltraWinExplorerBar.GroupEventArgs(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup { Key = "FixedAssetModule", Text = "Tài sản cố định" }));
            fMain.uExBar.Groups["FixedAssetModule"].Active = true;
            fMain.uExBar.Groups["FixedAssetModule"].Selected = true;
        }

        private void btnTienVaNH_Click(object sender, EventArgs e, FMain fMain)
        {
            //fMain.ConfigSubGroupMain(new Infragistics.Win.UltraWinExplorerBar.GroupEventArgs(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup { Key = "Money&BankModule", Text = "Tiền mặt và Ngân hàng" }));
            fMain.uExBar.Groups["Money&BankModule"].Active = true;
            fMain.uExBar.Groups["Money&BankModule"].Selected = true;
        }

        private void btnKeToanTienLuong_Click(object sender, EventArgs e)
        {
            //main.ConfigSubGroupMain(new Infragistics.Win.UltraWinExplorerBar.GroupEventArgs(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup { Key = "SalaryModule", Text = "Tiền lương" }));
            main.uExBar.Groups["SalaryModule"].Active = true;
            main.uExBar.Groups["SalaryModule"].Selected = true;
        }

        private void btnKeToanTongHop_Click(object sender, EventArgs e)
        {
            main.ConfigSubGroupMain(new Infragistics.Win.UltraWinExplorerBar.GroupEventArgs(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup { Key = "GeneralModule", Text = "Tổng hợp" }));
        }
        private void btnHoaDon_Click(object sender, EventArgs e, FMain fMain)
        {
            //fMain.ConfigSubGroupMain(new Infragistics.Win.UltraWinExplorerBar.GroupEventArgs(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup { Key = "InvoiceModule", Text = "Quản lý hóa đơn" }));
            fMain.uExBar.Groups["InvoiceModule"].Active = true;
            fMain.uExBar.Groups["InvoiceModule"].Selected = true;
        }
        private void btnKeToanGiaThanh_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.uExBar.Groups["CostModule"].Active = true;
            fMain.uExBar.Groups["CostModule"].Selected = true;
            //fMain.ConfigSubGroupMain(new Infragistics.Win.UltraWinExplorerBar.GroupEventArgs(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup { Key = "CostModule", Text = "Giá thành" }));
        }

        private void btnBaoCao_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ConfigSubGroupMain(new Infragistics.Win.UltraWinExplorerBar.GroupEventArgs(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup { Key = "btnInBaoCao", Text = "Báo cáo" }));
        }
        private void btnHopDong_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.uExBar.Groups["EMContractModule"].Active = true;
            fMain.uExBar.Groups["EMContractModule"].Selected = true;
            //fMain.ConfigSubGroupMain(new Infragistics.Win.UltraWinExplorerBar.GroupEventArgs(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup { Key = "EMContractModule", Text = "Hợp Đồng" }));
        }
        private void btnEInvoice_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.uExBar.Groups["EInvoiceModule"].Active = true;
            fMain.uExBar.Groups["EInvoiceModule"].Selected = true;
            //if(!fMain.uExBar.Groups["EInvoiceModule"].Visible)
            //{
            //    MSG.Warning("Phần mềm chưa được tích hợp hóa đơn điện tử vui lòng kiểm tra lại");
            //    return;
            //}
            //else
            //{
            //    try
            //    {
            //        fMain.uExBar.Groups["EInvoiceModule"].Active = true;
            //        fMain.uExBar.Groups["EInvoiceModule"].Selected = true;
            //    }
            //    catch (Exception ex)
            //    {
            //        MSG.Warning("Phần mềm chưa được tích hợp hóa đơn điện tử vui lòng kiểm tra lại");
            //        return;
            //    }

            //}
            //fMain.ConfigSubGroupMain(new Infragistics.Win.UltraWinExplorerBar.GroupEventArgs(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup { Key = "EInvoiceModule", Text = "Hóa đơn điện tử" }));
        }
        private void btnKeToanThue_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.uExBar.Groups["Tax"].Active = true;
            fMain.uExBar.Groups["Tax"].Selected = true;
            //fMain.ConfigSubGroupMain(new Infragistics.Win.UltraWinExplorerBar.GroupEventArgs(new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup { Key = "Tax", Text = "Thuế" }));
        }
    }
}
