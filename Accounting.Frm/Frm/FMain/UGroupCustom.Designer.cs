﻿namespace Accounting
{
    partial class UGroupCustom
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            this.btnAddItem = new Infragistics.Win.Misc.UltraButton();
            this.btnSettings = new Infragistics.Win.Misc.UltraButton();
            this.popButton = new Infragistics.Win.Misc.UltraPopupControlContainer(this.components);
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.SuspendLayout();
            // 
            // btnAddItem
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.Image = global::Accounting.Properties.Resources.basics_15;
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance4.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance4.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnAddItem.Appearance = appearance4;
            this.btnAddItem.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddItem.ImageSize = new System.Drawing.Size(80, 80);
            this.btnAddItem.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnAddItem.Location = new System.Drawing.Point(38, 30);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(92, 97);
            this.btnAddItem.TabIndex = 43;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            this.btnAddItem.MouseHover += new System.EventHandler(this.btnAddItem_MouseHover);
            // 
            // btnSettings
            // 
            this.btnSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.Image = global::Accounting.Properties.Resources.settings;
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnSettings.Appearance = appearance5;
            this.btnSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSettings.Location = new System.Drawing.Point(922, 0);
            this.btnSettings.Margin = new System.Windows.Forms.Padding(0);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(26, 26);
            this.btnSettings.TabIndex = 44;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // popButton
            // 
            this.popButton.PopupControl = this.btnDelete;
            // 
            // btnDelete
            // 
            appearance6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            appearance6.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            appearance6.BackColorAlpha = Infragistics.Win.Alpha.Opaque;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance6.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance6.ForeColor = System.Drawing.Color.White;
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            appearance6.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnDelete.Appearance = appearance6;
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.Location = new System.Drawing.Point(344, 306);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(16, 16);
            this.btnDelete.TabIndex = 45;
            this.btnDelete.Text = "x";
            this.btnDelete.UseAppStyling = false;
            this.btnDelete.UseFlatMode = Infragistics.Win.DefaultableBoolean.True;
            this.btnDelete.UseHotTracking = Infragistics.Win.DefaultableBoolean.True;
            this.btnDelete.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // UGroupCustom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.btnAddItem);
            this.Name = "UGroupCustom";
            this.Size = new System.Drawing.Size(948, 538);
            this.MouseHover += new System.EventHandler(this.UGroupCustom_MouseHover);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnAddItem;
        private Infragistics.Win.Misc.UltraButton btnSettings;
        private Infragistics.Win.Misc.UltraPopupControlContainer popButton;
        private Infragistics.Win.Misc.UltraButton btnDelete;

    }
}
