﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FChangeStyles : DialogForm
    {
        public FChangeStyles()
        {
            InitializeComponent();
            cbbStyles.Items.Add("easy.isl", "EASY 01");
            cbbStyles.Items.Add("easy2.isl", "EASY 02");
            cbbStyles.Items.Add("easy3.isl", "EASY 03");
            //cbbStyles.Items.Add("blue.isl", "Blue");
            cbbStyles.Items.Add("black.isl", "Black");
            cbbStyles.Items.Add("officeBlack.isl", "Office Black");
            cbbStyles.Items.Add("officeBlue.isl", "Office Blue");
            cbbStyles.Items.Add("officeSilver.isl", "Office Silver");
            cbbStyles.Items.Add("Office2010Blue.isl", "Office 2010 Blue");
            cbbStyles.Value = Properties.Settings.Default.Style;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Style = (string)cbbStyles.Value;
            Properties.Settings.Default.Save();
            string islFile = Properties.Settings.Default.Style;
            using (System.IO.Stream stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(string.Format("Accounting.Styles.{0}", islFile)))
                Infragistics.Win.AppStyling.StyleManager.Load(stream);
            Close();
        }
    }
}
