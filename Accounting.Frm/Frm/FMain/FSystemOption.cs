﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.AppStyling;
using System.IO;
using FX.Core;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinTabs;
using Accounting;
using System.Text.RegularExpressions;

namespace Accounting
{
    public partial class FSystemOption : CustormForm
    {
        #region Khai Báo
        readonly ISystemOptionService _ISystemOptionService;
        //Kích hoạt kết nối đến IService
        SystemOption _Select = new SystemOption();
        //Khởi tạo 1 lớp SytemOption
        readonly BindingList<BankAccountDetail> _lstaObjectBankAccounts = Utils.ListBankAccountDetail;
        readonly BindingList<Repository> _lstaObjectRepository = Utils.ListRepository;
        readonly BindingList<GoodsServicePurchase> _lstaObjectGoodsServicePurchase = Utils.ListGoodsServicePurchase;
        readonly BindingList<Account> _lstaObjectAccount = Utils.ListAccount;
        List<SystemOption> dl = new List<SystemOption>();
        List<SystemOption> lstSystemOptions = new List<SystemOption>();
        private List<SystemOption> dl1 = new List<SystemOption>();

        private string _groupSeparator;
        private string _decimalSeparator;
        private string _groupSeparatorNew;
        private string _decimalSeparatorNew;

        #endregion
        #region Main
        public FSystemOption()
        {
            InitializeComponent();
            //AddEvents(this);
            #region Khởi Tạo
            #region Thiết Lập Ban Đầu
            //Khai báo các Iservice
            _ISystemOptionService = IoC.Resolve<ISystemOptionService>();
            dl1 = Utils.CloneObject(_ISystemOptionService.GetAll());
            _groupSeparator = dl1.Where(p => p.Code == "DDSo_NCachHangNghin").Select(p => p.Data).FirstOrDefault();
            _decimalSeparator = dl1.Where(p => p.Code == "DDSo_NCachHangDVi").Select(p => p.Data).FirstOrDefault();
            ButApDung.Enabled = false;
            #endregion
            #endregion
            LoadDuLieu();
            lstSystemOptions = _ISystemOptionService.GetAll();
            dl1 = dl1.OrderBy(c => c.ID).ToList();
            lstSystemOptions = lstSystemOptions.OrderBy(c => c.ID).ToList();
            Utils.SortFormControls(this);
            ultraGroupBox9.Visible = false;
            if (!Utils.ISysUserService.Query.FirstOrDefault(x => x.username == Properties.Settings.Default.UserApp).IsSystem) txtEmailForgotPass.Enabled = false;

        }
        #endregion
        #region Hàm Chung
        #region Load Dữ liệu
        private void LoadDuLieu()
        {
            #region Lấy Dữ Liệu Từ CSDL
            dl = _ISystemOptionService.GetAll();


            GetDataComboByOption(cbcTTDN_SoTKNH);
            GetDatacbcVTHH_KhoMacDinh(cbcVTHH_KhoMacDinh);
            GetDatacbcVTHH_NhomHHDV(cbcVTHH_NhomHHDV);
            GetDatacbcVTHH_NhomNNMDV(cbcVTHH_NhomNNMD);
            LoadcomboxPPTTGTGT();
            GetDataComboTCK(cbcTCKHAC_TKCLechLai);
            GetDataComboTCK(cbcTCKHAC_TKCLechLo);
            LoadcomboxViTriThongTinDoanhNghiep();
            LoadcomboxViTriChuKy();
            LoadcomboxViTriThongTinDoanhNghiepVTHH();
            LoadcomboxKyHieuTienTe();
            LoadcomboxHienThiSoAm();
            LoadcomboxTinhGia();
            LoadcomboxCachDocSoTienLe();
            LoadcomboxInHoaDon();
            LoadcomboxKieuGiaoDien();
            ProcessControls(this, dl);
            txtTTDN_Tendvichuquan.Enabled = txtTTDN_MSTdvichuquan.Enabled = chkTTDN_TTDvichuquan.Checked;
            txtTTDN_TenDLT.Enabled =
                txtTTDN_MSTDLT.Enabled =
                    txtTTDN_DiachiDLT.Enabled =
                        txtTTDN_QuanDLT.Enabled =
                            txtTTDN_TinhDLT.Enabled =
                                txtTTDN_DienthoaiDLT.Enabled =
                                    txtTTDN_FaxDLT.Enabled =
                                        txtTTDN_HDDLso.Enabled =
                                            dateTTDN_NgayDLT.Enabled =
                                                txtTTDN_NVDLT.Enabled =
                                                    txtTTDN_CCHNDLT.Enabled = chkTTDN_TTDailythue.Checked;
            txtProductID.Enabled = true;
            optGia.Enabled = (cbcVTHH_PPTinhGiaXKho.Text == "Nhập trước xuất trước" || cbcVTHH_PPTinhGiaXKho.Text == "Bình quân tức thời");
            LoadCheckEdiotTTDN();
            LoadCheckEdiotDaiLyThue();
            //var notbackup = Utils.ListSystemOption.FirstOrDefault(o => o.Code == "SLUU_KhongLuu").Data;
            //if (notbackup == "1")
            //    ultraPanel1.Visible = true;

            #endregion
            ButDDS_XemThu_Click(ButDDS_XemThu, null);
        }
        #endregion
        #region Hàm fill Control trên form với database
        private void ProcessControls(Control ctrlContainer, List<SystemOption> dl)
        {
            foreach (Control ctrl in ctrlContainer.Controls)
            {
                try
                {
                    if (ctrl.GetType().Name.Contains(typeof(UltraTextEditor).Name))
                    {
                        UltraTextEditor control = (UltraTextEditor)ctrl;
                        control.ValueChanged += CheckControlLeave;
                        control.Leave += CheckControlLeave;
                        foreach (SystemOption sys in dl)
                        {
                            if (control.Name.Replace("txt", "") == sys.Code)
                            {
                                control.Text = sys.Data ?? "";
                            }
                        }

                    }
                    else if (ctrl.GetType().Name.Contains(typeof(UltraCheckEditor).Name))
                    {
                        UltraCheckEditor control = (UltraCheckEditor)ctrl;
                        control.CheckedChanged += CheckControlLeave;
                        control.Leave += CheckControlLeave;
                        foreach (SystemOption sys in dl)
                        {
                            if (control.Name.Replace("chk", "") == sys.Code)
                            {
                                control.Checked = sys.Data == "0" ? false : true;
                            }
                        }

                    }

                    else if (ctrl.GetType().Name.Contains(typeof(UltraMaskedEdit).Name))
                    {
                        UltraMaskedEdit control = (UltraMaskedEdit)ctrl;
                        control.ValueChanged += CheckControlLeave;
                        control.Leave += CheckControlLeave;
                        foreach (SystemOption sys in dl)
                        {
                            if (control.Name.Replace("me", "") == sys.Code && !string.IsNullOrEmpty(sys.Data))
                            {
                                control.Value = sys.Data ?? "";
                            }
                        }

                    }
                    else if (ctrl.GetType().Name.Contains(typeof(UltraDateTimeEditor).Name))
                    {
                        UltraDateTimeEditor control = (UltraDateTimeEditor)ctrl;
                        control.ValueChanged += CheckControlLeave;
                        control.Leave += CheckControlLeave;
                        foreach (SystemOption sys in dl)
                        {
                            if (control.Name.Replace("date", "") == sys.Code && !string.IsNullOrEmpty(sys.Data))
                            {
                                control.Value = sys.Data ?? "";
                            }
                        }

                    }
                    else if (ctrl.GetType().Name.Contains(typeof(UltraCombo).Name))
                    {
                        UltraCombo control = (UltraCombo)ctrl;
                        control.ValueChanged += CheckControlLeave;
                        control.Leave += CheckControlLeave;
                        if (control.Name == "cbcTTDN_SoTKNH")
                        {
                            foreach (var item in control.Rows)
                            {
                                var bankAccountDetail = item.ListObject as BankAccountDetail;
                                if (bankAccountDetail != null && bankAccountDetail.BankAccount == dl.Single(c => c.Code == control.Name.Replace("cbc", "")).Data)
                                    control.SelectedRow = item;
                            }
                        }
                        else if (control.Name == "cbcTCKHAC_KieuGiaoDien")
                        {
                            foreach (var item in control.Rows)
                            {
                                if ((item.ListObject as string) == dl.Single(c => c.Code == control.Name.Replace("cbc", "")).Data)
                                    control.SelectedRow = item;
                            }
                        }
                        else if (control.Name == "cbcNKY_vitrikyDT")
                        {
                            foreach (var item in control.Rows)
                            {
                                if ((item.ListObject as string) == dl.Single(c => c.Code == control.Name.Replace("cbc", "")).Data)
                                    control.SelectedRow = item;
                            }
                        }
                        else if (control.Name == "cbcBC_VtriTtinDN")
                        {
                            foreach (var item in control.Rows)
                            {
                                if ((item.ListObject as string) == dl.Single(c => c.Code == control.Name.Replace("cbc", "")).Data)
                                    control.SelectedRow = item;
                            }
                        }
                        else if (control.Name == "cbcVTHH_PPTinhGiaXKho")
                        {
                            foreach (var item in control.Rows)
                            {
                                if ((item.ListObject as string) == dl.Single(c => c.Code == control.Name.Replace("cbc", "")).Data)
                                    control.SelectedRow = item;
                            }
                        }
                        else if (control.Name == "cbcDDSo_KyHieuTien")
                        {
                            foreach (var item in control.Rows)
                            {
                                if ((item.ListObject as string) == dl.Single(c => c.Code == control.Name.Replace("cbc", "")).Data)
                                    control.SelectedRow = item;
                            }
                        }
                        else if (control.Name == "cbcDDSo_SoAm")
                        {
                            foreach (var item in control.Rows)
                            {
                                if ((item.ListObject as string) == dl.Single(c => c.Code == control.Name.Replace("cbc", "")).Data)
                                    control.SelectedRow = item;
                            }
                        }
                        else if (control.Name == "cbcTCKHAC_PPTinhGiaXQuy")
                        {
                            foreach (var item in control.Rows)
                            {
                                if ((item.ListObject as string) == dl.Single(c => c.Code == control.Name.Replace("cbc", "")).Data)
                                    control.SelectedRow = item;
                            }
                        }
                        else if (control.Name == "cbcPPTTGTGT")
                        {
                            foreach (var item in control.Rows)
                            {
                                if ((item.ListObject as string) == dl.Single(c => c.Code == control.Name.Replace("cbc", "")).Data)
                                    control.SelectedRow = item;
                            }
                        }
                        else if (control.Name == "cbcTCKHAC_DocTienLe")
                        {
                            foreach (var item in control.Rows)
                            {
                                if ((item.ListObject as string) == dl.Single(c => c.Code == control.Name.Replace("cbc", "")).Data)
                                    control.SelectedRow = item;
                            }
                        }
                        else if (control.Name == "cbcTCKHAC_ChuHThiInHDLan2")
                        {
                            foreach (var item in control.Rows)
                            {
                                if ((item.ListObject as string) == dl.Single(c => c.Code == control.Name.Replace("cbc", "")).Data)
                                    control.SelectedRow = item;
                            }
                        }
                        else if (control.Name == "cbcVTHH_KhoMacDinh")
                        {
                            foreach (var item in control.Rows)
                            {
                                var Repository = item.ListObject as Repository;
                                if (Repository != null && Repository.RepositoryCode == dl.Single(c => c.Code == control.Name.Replace("cbc", "")).Data)
                                    control.SelectedRow = item;
                            }
                        }
                        else if (control.Name == "cbcVTHH_NhomHHDV")
                        {
                            foreach (var item in control.Rows)
                            {
                                var GoodsServicePurchase = item.ListObject as GoodsServicePurchase;
                                if (GoodsServicePurchase != null && GoodsServicePurchase.GoodsServicePurchaseCode == dl.Single(c => c.Code == control.Name.Replace("cbc", "")).Data)
                                    control.SelectedRow = item;
                            }
                        }
                        else if (control.Name == "cbcVTHH_NhomNNMD")
                        {
                            foreach (var item in control.Rows)
                            {
                                var CareerGroup = item.ListObject as CareerGroup;
                                if (CareerGroup != null && CareerGroup.CareerGroupCode == dl.Single(c => c.Code == control.Name.Replace("cbc", "")).Data)
                                    control.SelectedRow = item;
                            }
                        }
                        else if (control.Name == "cbcTCKHAC_TKCLechLai")
                        {
                            foreach (var item in control.Rows)
                            {
                                var Account = item.ListObject as Account;
                                if (Account != null && Account.AccountNumber == dl.Single(c => c.Code == control.Name.Replace("cbc", "")).Data)
                                    control.SelectedRow = item;
                            }
                        }
                        else if (control.Name == "cbcTCKHAC_TKCLechLo")
                        {
                            foreach (var item in control.Rows)
                            {
                                var Account = item.ListObject as Account;
                                if (Account != null && Account.AccountNumber == dl.Single(c => c.Code == control.Name.Replace("cbc", "")).Data)
                                    control.SelectedRow = item;
                            }
                        }

                    }
                    else if (ctrl.GetType().Name.Contains(typeof(UltraNumericEditor).Name))
                    {
                        UltraNumericEditor control = (UltraNumericEditor)ctrl;
                        control.ValueChanged += CheckControlLeave;
                        control.Leave += CheckControlLeave;
                        foreach (SystemOption sys in dl)
                        {
                            if (control.Name.Replace("num", "") == sys.Code && !string.IsNullOrEmpty(sys.Data))
                            {
                                control.Value = sys.Data ?? "0";
                            }
                        }

                    }
                    else if (ctrl.GetType().Name.Contains(typeof(UltraColorPicker).Name))
                    {
                        UltraColorPicker control = (UltraColorPicker)ctrl;
                        control.ValueChanged += CheckControlLeave;
                        control.Leave += CheckControlLeave;
                        foreach (SystemOption sys in dl)
                        {
                            if (control.Name.Replace("clp", "") == sys.Code/* && !string.IsNullOrEmpty(sys.Data)*/)
                            {
                                if (sys.Data != null && sys.Data != "")
                                    control.Value = Color.FromName(sys.Data.Substring(7, sys.Data.Length - 8));
                                else
                                {
                                    if (control.Name == "clpTCKHAC_MauCTuChuaGS")
                                        control.Value = Color.Green;
                                    if (control.Name == "clpTCKHAC_MauSoAm")
                                        control.Value = Color.Red;
                                }
                            }
                        }

                    }
                    else if (ctrl.GetType().Name.Contains(typeof(UltraOptionSet).Name))
                    {
                        UltraOptionSet control = (UltraOptionSet)ctrl;
                        control.ValueChanged += CheckControlLeave;
                        control.Leave += CheckControlLeave;
                        foreach (SystemOption sys in dl)
                        {
                            foreach (ValueListItem valueListItem in control.Items)
                            {
                                if (valueListItem.Tag == null) continue;
                                if (valueListItem.Tag.ToString().Replace("opt", "") == sys.Code && sys.Data != null && sys.Data.Equals("1"))
                                {
                                    control.CheckedItem = valueListItem;
                                    break; ;
                                }
                            }
                        }

                    }

                    if (ctrl.HasChildren)
                        ProcessControls(ctrl, dl);
                }
                catch (Exception ex)
                {
                    MSG.Error(ex.ToString());
                }

            }

        }


        #endregion
        #region Nut Dong Y , Huy Bo ,Ap Dung
        private void ButDongY_Click(object sender, EventArgs e)
        {

            SetControl(this, dl);
            _groupSeparatorNew = dl.Where(p => p.Code == "DDSo_NCachHangNghin").Select(p => p.Data).FirstOrDefault();
            _decimalSeparatorNew = dl.Where(p => p.Code == "DDSo_NCachHangDVi").Select(p => p.Data).FirstOrDefault();
            if (_groupSeparatorNew == _decimalSeparatorNew)
            {
                MessageBox.Show("Phân cách hàng nghìn không được phép trùng phân cách hàng đơn vị", "Cảnh báo",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            foreach (SystemOption x in dl)
            {
                #region Thao tác CSDL
                _ISystemOptionService.BeginTran();
                if (x.Type == 1)
                {
                    if (_groupSeparator != _groupSeparatorNew)
                        x.Data = x.Data.Replace(_groupSeparator, _groupSeparatorNew);
                    else if (_decimalSeparator != _decimalSeparatorNew)
                        x.Data = x.Data.Replace(_decimalSeparator, _decimalSeparatorNew);
                }
                _ISystemOptionService.Save(x);
                _ISystemOptionService.CommitTran();
                #endregion
            }
            Thread.CurrentThread.CurrentCulture = Utils.Config();
            this.Close();

        }
        private void ButHuyBo_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void ButApDung_Click(object sender, EventArgs e)
        {
            SetControl(this, dl);
            _groupSeparatorNew = dl.Where(p => p.Code == "DDSo_NCachHangNghin").Select(p => p.Data).FirstOrDefault();
            _decimalSeparatorNew = dl.Where(p => p.Code == "DDSo_NCachHangDVi").Select(p => p.Data).FirstOrDefault();
            if (_groupSeparatorNew == _decimalSeparatorNew)
            {
                MessageBox.Show("Phân cách hàng nghìn không được phép trùng phân cách hàng đơn vị", "Cảnh báo",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            foreach (SystemOption x in dl)
            {
                try
                {
                    #region Thao tác CSDL
                    _ISystemOptionService.BeginTran();
                    if (x.Type == 1)
                    {
                        if (_groupSeparator != _groupSeparatorNew)
                            x.Data = x.Data.Replace(_groupSeparator, _groupSeparatorNew);
                        else if (_decimalSeparator != _decimalSeparatorNew)
                            x.Data = x.Data.Replace(_decimalSeparator, _decimalSeparatorNew);
                    }
                    _ISystemOptionService.Save(x);
                    _ISystemOptionService.CommitTran();
                    ButApDung.Enabled = false;
                    #endregion
                }
                catch (Exception)
                {

                    _ISystemOptionService.RolbackTran();
                }

            }
            Thread.CurrentThread.CurrentCulture = Utils.Config();
        }
        private void SetControl(Control ctrlContainer, List<SystemOption> dl)
        {
            foreach (Control ctrl in ctrlContainer.Controls)
            {
                if (ctrl.GetType().Name.Contains(typeof(UltraTextEditor).Name))
                {
                    UltraTextEditor control = (UltraTextEditor)ctrl;
                    foreach (SystemOption sys in dl)
                    {
                        if (control.Name.Replace("txt", "") == sys.Code)
                        {
                            sys.Data = control.Text;
                        }
                    }

                }
                else if (ctrl.GetType().Name.Contains(typeof(UltraCheckEditor).Name))
                {
                    UltraCheckEditor control = (UltraCheckEditor)ctrl;
                    foreach (SystemOption sys in dl)
                    {
                        if (control.Name.Replace("chk", "") == sys.Code)
                        {
                            sys.Data = control.Checked == true ? "1" : "0";
                        }
                    }

                }
                else if (ctrl.GetType().Name.Contains(typeof(UltraDateTimeEditor).Name))
                {
                    UltraDateTimeEditor control = (UltraDateTimeEditor)ctrl;
                    foreach (SystemOption sys in dl)
                    {
                        if (control.Name.Replace("date", "") == sys.Code)
                        {
                            sys.Data = control.Text ?? "";
                        }
                    }

                }
                else if (ctrl.GetType().Name.Contains(typeof(UltraCombo).Name))
                {
                    UltraCombo control = (UltraCombo)ctrl;
                    foreach (SystemOption sys in dl)
                    {
                        if (control.Name.Replace("cbc", "") == sys.Code)
                        {
                            sys.Data = (string)control.Value ?? "";
                        }
                    }

                }
                else if (ctrl.GetType().Name.Contains(typeof(UltraNumericEditor).Name))
                {
                    UltraNumericEditor control = (UltraNumericEditor)ctrl;
                    foreach (SystemOption sys in dl)
                    {
                        if (control.Name.Replace("num", "") == sys.Code)
                        {
                            sys.Data = control.Value == null ? "0" : control.Value.ToString();
                        }
                    }

                }
                else if (ctrl.GetType().Name.Contains(typeof(UltraColorPicker).Name))
                {
                    UltraColorPicker control = (UltraColorPicker)ctrl;
                    foreach (SystemOption sys in dl)
                    {
                        if (control.Name.Replace("clp", "") == sys.Code)
                        {
                            sys.Data = control.Value == null ? "" : control.Value.ToString();
                        }
                    }


                }
                else if (ctrl.GetType().Name.Contains(typeof(UltraMaskedEdit).Name))
                {
                    UltraMaskedEdit control = (UltraMaskedEdit)ctrl;
                    foreach (SystemOption sys in dl)
                    {
                        if (control.Name.Replace("me", "") == sys.Code)
                        {
                            sys.Data = control.Value == null ? "" : control.Value.ToString();
                        }
                    }

                }
                else if (ctrl.GetType().Name.Contains(typeof(UltraOptionSet).Name))
                {
                    UltraOptionSet control = (UltraOptionSet)ctrl;
                    foreach (ValueListItem valueListItem in control.Items)
                    {
                        foreach (SystemOption systemOption in dl.Where(systemOption => valueListItem.Tag.ToString().Replace("opt", "").Equals(systemOption.Code)))
                            systemOption.Data = valueListItem.Tag.ToString().Equals(control.CheckedItem.Tag.ToString()) ? "1" : "0";
                    }
                }
                if (ctrl.HasChildren)
                    SetControl(ctrl, dl);
            }
        }
        #endregion
        #region Sự Kiện
        #region Tab thông tin doanh nghiệp
        #region Đơn vị chủ quản
        private void chkTTDN_TTDvichuquan_CheckedChanged(object sender, EventArgs e)
        {

            LoadCheckEdiotTTDN();
        }

        private void LoadCheckEdiotTTDN()
        {
            txtTTDN_Tendvichuquan.Enabled = txtTTDN_MSTdvichuquan.Enabled = chkTTDN_TTDvichuquan.Checked;
            if (chkTTDN_TTDvichuquan.Checked)
            {
                ProcessControls(this.ultraPanelDonViChuQuan, dl);
            }
            else
            {
                txtTTDN_Tendvichuquan.Text = "";
                txtTTDN_MSTdvichuquan.Text = "";
            }
        }
        #endregion
        #region Đại lý thuế
        private void chkTTDN_TTDailythue_CheckedChanged(object sender, EventArgs e)
        {
            LoadCheckEdiotDaiLyThue();
        }
        private void LoadCheckEditorIsMinimized()
        {
            ProcessControls(chkIsMinimized, dl);
        }
        private void LoadCheckEdiotDaiLyThue()
        {
            txtTTDN_TenDLT.Enabled =
                txtTTDN_MSTDLT.Enabled =
                    txtTTDN_DiachiDLT.Enabled =
                        txtTTDN_QuanDLT.Enabled =
                            txtTTDN_TinhDLT.Enabled =
                                txtTTDN_DienthoaiDLT.Enabled =
                                    txtTTDN_FaxDLT.Enabled =
                                        txtTTDN_HDDLso.Enabled =
                                            dateTTDN_NgayDLT.Enabled =
                                                txtTTDN_NVDLT.Enabled =
                                                    txtTTDN_CCHNDLT.Enabled = chkTTDN_TTDailythue.Checked;
            if (chkTTDN_TTDailythue.Checked)
            {
                ProcessControls(this.ultraPanelDaiLyThue, dl);
            }
            else
            {
                txtTTDN_TenDLT.Text = "";
                txtTTDN_MSTDLT.Text = "";
                txtTTDN_DiachiDLT.Text = "";
                txtTTDN_QuanDLT.Text = "";
                txtTTDN_TinhDLT.Text = "";
                txtTTDN_DienthoaiDLT.Text = "";
                txtTTDN_FaxDLT.Text = "";
                txtTTDN_HDDLso.Text = "";
                dateTTDN_NgayDLT.Text = "";
                txtTTDN_NVDLT.Text = "";
                txtTTDN_CCHNDLT.Text = "";
            }
        }
        #endregion
        #endregion
        #endregion
        #region Hàm kiểm tra dữ liệu bị sửa
        private void CheckControlLeave(object sender, EventArgs e)
        {
            ButApDung.Enabled = CheckEdit();
        }
        public bool CheckEdit()
        {
            SetControl(this, dl1);
            return dl1.Count >= 0 && dl1.Where((t, i) => !dl[i].Data.Equals(t.Data)).Any();
        }
        #endregion
        #endregion
        #region                                                         Xử Lý Các Tab
        #region                                                 Tab Thông Tin Doanh Nghiệp TTDN
        #region Hàm Load Dữ Liệu Vào ComboBox
        public static void GetDataComboByOption(UltraCombo cbb)
        {

            cbb.DataSource = Utils.ListBankAccountDetail;
            cbb.DisplayMember = "BankAccount";
            Utils.ConfigGrid(cbb, ConstDatabase.BankAccountDetail_TableName);
            cbb.SelectedRow = cbb.Rows.FirstOrDefault();
        }
        #endregion
        #region Select tên ngân hàng vào textbox ngân hàng theo số tài khoản ngân hàng

        private void cbcTTDN_SoTKNH_ValueChanged(object sender, EventArgs e)
        {


            BankAccountDetail bank = (BankAccountDetail)cbcTTDN_SoTKNH.SelectedRow.ListObject;
            txtTTDN_Nganhang.Text = bank.BankName;

        }
        #endregion
        #endregion
        #region                                                         Tab Người Ký
        #region Load dữ liệu vào combobox
        public void LoadcomboxViTriChuKy()
        {
            List<string> lItem = new List<string>() { "Phải Dưới", "Trái Dưới", "Trái Trên", "Phải Trên" };
            cbcNKY_vitrikyDT.DataSource = lItem;
            //Ẩn đi header khi click vào sổ ra combobox
            cbcNKY_vitrikyDT.DisplayLayout.Bands[0].ColHeadersVisible = false;
        }

        #endregion
        #region Nút Mở nơi lưu
        private void ButNK_ChonNoiLuTep_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = folderBrowserDialogNoiLuuTepDaKy.ShowDialog(this);
                if (result == DialogResult.OK)
                {
                    string[] files = Directory.GetFiles(folderBrowserDialogNoiLuuTepDaKy.SelectedPath);
                    txtNKY_NoiluukyDT.Text = folderBrowserDialogNoiLuuTepDaKy.SelectedPath;
                }
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                return;
            }
        }
        #endregion


        #endregion
        #region Tab Báo Cáo
        private void ButChonFontTenCongTy_Click(object sender, EventArgs e)
        {
            try
            {
                FontDialog fSelect = new FontDialog();

                if (fSelect.ShowDialog(this) == DialogResult.OK)
                {
                    txtBC_FontTencty.Text = fSelect.Font.Name + "   ;      " + fSelect.Font.Style + "   ;      " + fSelect.Font.Size;
                    txtBC_FontTencty.Font = fSelect.Font;
                }
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                return;
            }

        }
        private void ButFontDiaChi_Click(object sender, EventArgs e)
        {
            try
            {
                FontDialog fSelect = new FontDialog();

                if (fSelect.ShowDialog(this) == DialogResult.OK)
                {
                    txtBC_FontDChi.Text = fSelect.Font.Name + "     ;    " + fSelect.Font.Style + "      ;     " + fSelect.Font.Size;
                    txtBC_FontDChi.Font = fSelect.Font;
                }
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                return;
            }
        }
        private void ButFontDonViChuQuan_Click(object sender, EventArgs e)
        {
            FontDialog fSelect = new FontDialog();

            if (fSelect.ShowDialog(this) == DialogResult.OK)
            {
                txtBC_FontTenchuquan.Text = fSelect.Font.Name + "   ;     " + fSelect.Font.Style + "    ;     " + fSelect.Font.Size;
                txtBC_FontTenchuquan.Font = fSelect.Font;
            }
        }
        #region Load dữ liệu vào combobox
        void LoadcomboxViTriThongTinDoanhNghiep()
        {
            List<string> lItem = new List<string>() { "Trái Trên", "Phải Trên", "Giữa Trên" };
            cbcBC_VtriTtinDN.DataSource = lItem;
            cbcBC_VtriTtinDN.DisplayLayout.Bands[0].ColHeadersVisible = false;
            setColorItemInCbb(cbcBC_VtriTtinDN);
        }

        #endregion
        #endregion
        #region Tab Vật Tư Hàng Hóa
        void LoadcomboxViTriThongTinDoanhNghiepVTHH()
        {
            List<string> lItem = new List<string>() { "Bình quân cuối kỳ", "Bình quân tức thời", "Nhập trước xuất trước", "Đích danh" };
            cbcVTHH_PPTinhGiaXKho.DataSource = lItem;
            cbcVTHH_PPTinhGiaXKho.DisplayLayout.Bands[0].ColHeadersVisible = false;
        }
        public static void GetDatacbcVTHH_KhoMacDinh(UltraCombo cbb)
        {

            cbb.DataSource = Utils.ListRepository;
            cbb.DisplayMember = "RepositoryCode";
            Utils.ConfigGrid(cbb, ConstDatabase.Repository_TableName);
            cbb.SelectedRow = cbb.Rows.FirstOrDefault();
        }

        private void cbcVTHH_PPTinhGiaXKho_TextChanged_1(object sender, EventArgs e)
        {
            optGia.Enabled = (cbcVTHH_PPTinhGiaXKho.Text == "Nhập trước xuất trước" || cbcVTHH_PPTinhGiaXKho.Text == "Bình quân tức thời");
            setColorItemInCbb(cbcVTHH_PPTinhGiaXKho);
        }


        #endregion
        #region Tab Định Dạng Số
        void LoadcomboxKyHieuTienTe()
        {
            List<string> lItem = new List<string>() { "Đ", "đ", "$", "€" };
            cbcDDSo_KyHieuTien.DataSource = lItem;
            cbcDDSo_KyHieuTien.DisplayLayout.Bands[0].ColHeadersVisible = false;
        }
        void LoadcomboxHienThiSoAm()
        {
            List<string> lItem = new List<string>() { "(n)", "-n" };
            cbcDDSo_SoAm.DataSource = lItem;
            cbcDDSo_SoAm.DisplayLayout.Bands[0].ColHeadersVisible = false;
            setColorItemInCbb(cbcDDSo_SoAm);
        }
        private void ButDDS_XemThu_Click(object sender, EventArgs e)
        {
            //Cách ghi tiền tệ theo dầu phẩy
            double value = 1234567.89;
            double value1 = 123456789.00;
            System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalDigits = (int)numDDSo_TienVND.Value;
            System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol = cbcDDSo_KyHieuTien.Text;
            System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyPositivePattern = 3;
            lblTienVietNam.Text = value.ToString("C");

            System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalDigits = (int)numDDSo_NgoaiTe.Value;
            System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol = cbcDDSo_KyHieuTien.Text;
            System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyPositivePattern = 3;
            lblTienNgoaiTe.Text = value.ToString("C");
            System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalDigits = (int)numDDSo_DonGia.Value;
            lblDonGia.Text = value.ToString("N");
            System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalDigits = (int)numDDSo_DonGiaNT.Value;
            lblDonGiaNT.Text = value.ToString("N");
            System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalDigits = (int)numDDSo_SoLuong.Value;
            lblSoLuong.Text = value.ToString("N");
            System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalDigits = (int)numDDSo_TyGia.Value;
            lblTyGia.Text = value1.ToString("N");
            System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalDigits = (int)numDDSo_TyLe.Value;
            lblHeSoTyLe.Text = value1.ToString("N");
            System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalDigits = (int)numDDSo_TyLePBo.Value;
            lblTyLePhanBo.Text = value1.ToString("N");
            if (cbcDDSo_SoAm.Text == "(n)")
            {
                //lblHienThiSoAm.Text = " "(" 1.234.567 ")" ";
                lblHienThiSoAm.Text = value.ToString("(#,###,###)");
            }
            else if (cbcDDSo_SoAm.Text == "-n")
            {
                //lblHienThiSoAm.Text = "-1.234.567";
                lblHienThiSoAm.Text = value.ToString("-#,###,###");
            }
            else
            {
                lblHienThiSoAm.Text = "";
            }
        }
        private void ButDDS_MacDinh_Click(object sender, EventArgs e)
        {
            DefaultControl(this.ultraTabPageControl6, dl);

        }
        private void DefaultControl(Control ctrlContainer, List<SystemOption> dl)
        {
            foreach (Control ctrl in ctrlContainer.Controls)
            {
                if (ctrl.GetType().Name.Contains(typeof(UltraTextEditor).Name))
                {
                    UltraTextEditor control = (UltraTextEditor)ctrl;
                    foreach (SystemOption sys in dl)
                    {
                        if (control.Name.Replace("txt", "") == sys.Code)
                        {
                            control.Text = sys.DefaultData ?? "";
                        }
                    }

                }
                else if (ctrl.GetType().Name.Contains(typeof(UltraCombo).Name))
                {
                    UltraCombo control = (UltraCombo)ctrl;
                    if (control.Name == "cbcDDSo_KyHieuTien")
                    {
                        foreach (var item in control.Rows)
                        {
                            if ((item.ListObject as string) == dl.Single(c => c.Code == control.Name.Replace("cbc", "")).DefaultData)
                                control.SelectedRow = item;
                        }
                    }
                    else if (control.Name == "cbcDDSo_SoAm")
                    {
                        foreach (var item in control.Rows)
                        {
                            if ((item.ListObject as string) == dl.Single(c => c.Code == control.Name.Replace("cbc", "")).DefaultData)
                                control.SelectedRow = item;
                        }
                    }
                }
                else if (ctrl.GetType().Name.Contains(typeof(UltraNumericEditor).Name))
                {
                    UltraNumericEditor control = (UltraNumericEditor)ctrl;
                    foreach (SystemOption sys in dl)
                    {
                        if (control.Name.Replace("num", "") == sys.Code)
                        {
                            control.Value = sys.DefaultData ?? "0";
                        }
                    }

                }
                if (ctrl.HasChildren)
                    DefaultControl(ctrl, dl);
            }
        }
        #endregion
        #region Tab Sao Lưu
        private void ButSL_Chon_Click(object sender, EventArgs e)
        {
            //DialogResult result = folderBrowserDialogNoiLuuTepDaKy.ShowDialog(this);
            //if (result == DialogResult.OK)
            //{
            //    string[] files = Directory.GetFiles(folderBrowserDialogNoiLuuTepDaKy.SelectedPath);
            //    txtSLUU_TMSaoLuu.Text = folderBrowserDialogNoiLuuTepDaKy.SelectedPath;
            //}
            FolderServer fm = new FolderServer(txtSLUU_TMSaoLuu.Text);
            fm.FormClosed += new FormClosedEventHandler(fFolderServer_FormClosed);
            fm.ShowDialog(this);

        }
        private void fFolderServer_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FolderServer)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    txtSLUU_TMSaoLuu.Text = f.path;
                }

            }


        }
        #endregion
        #region Tab tùy chọn khác
        public static void GetDatacbcVTHH_NhomHHDV(UltraCombo cbb)
        {
            cbb.DataSource = Utils.ListGoodsServicePurchase;
            cbb.DisplayMember = "GoodsServicePurchaseName";
            cbb.ValueMember = "GoodsServicePurchaseCode";
            Utils.ConfigGrid(cbb, ConstDatabase.GoodsServicePurchase_TableName);
            //cbb.SelectedRow = cbb.Rows.FirstOrDefault();
        }

        public static void GetDatacbcVTHH_NhomNNMDV(UltraCombo cbb)
        {
            cbb.DataSource = Utils.ListCareerGroup;
            cbb.DisplayMember = "CareerGroupName";
            cbb.ValueMember = "CareerGroupCode";
            Utils.ConfigGrid(cbb, ConstDatabase.CareerGroup_TableName);
            //cbb.SelectedRow = cbb.Rows.FirstOrDefault();
        }
        void LoadcomboxPPTTGTGT()
        {
            List<string> lItem = new List<string>() { "Phương pháp khấu trừ", "Phương pháp trực tiếp trên doanh thu" };
            cbcPPTTGTGT.DataSource = lItem;
            cbcPPTTGTGT.DisplayLayout.Bands[0].ColHeadersVisible = false;
        }
        void LoadcomboxTinhGia()
        {
            List<string> lItem = new List<string>() { "Bình quân cuối kỳ", "Bình quân tức thời" };
            cbcTCKHAC_PPTinhGiaXQuy.DataSource = lItem;
            cbcTCKHAC_PPTinhGiaXQuy.DisplayLayout.Bands[0].ColHeadersVisible = false;
            setColorItemInCbb(cbcTCKHAC_PPTinhGiaXQuy);
        }
        #region Hàm Load Dữ Liệu Vào ComboBox
        public static void GetDataComboTCK(UltraCombo cbb)
        {

            cbb.DataSource = Utils.ListAccount.Where(c => c.IsParentNode == false).ToList();
            cbb.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbb, ConstDatabase.Account_TableName);
            cbb.SelectedRow = cbb.Rows.FirstOrDefault();
        }
        #endregion
        void LoadcomboxCachDocSoTienLe()
        {
            List<string> lItem = new List<string>() { "Linh", "Lẻ" };
            cbcTCKHAC_DocTienLe.DataSource = lItem;
            cbcTCKHAC_DocTienLe.DisplayLayout.Bands[0].ColHeadersVisible = false;
            setColorItemInCbb(cbcTCKHAC_DocTienLe);
        }
        void LoadcomboxInHoaDon()
        {
            List<string> lItem = new List<string>() { "Bản sao", "Bản copy" };
            cbcTCKHAC_ChuHThiInHDLan2.DataSource = lItem;
            cbcTCKHAC_ChuHThiInHDLan2.DisplayLayout.Bands[0].ColHeadersVisible = false;
            setColorItemInCbb(cbcTCKHAC_ChuHThiInHDLan2);
        }
        void LoadcomboxKieuGiaoDien()
        {
            List<string> lItem = new List<string>() { "Mặc định", "Office2010Blue", "officeBlue", "blue" };
            cbcTCKHAC_KieuGiaoDien.DataSource = lItem;
            cbcTCKHAC_KieuGiaoDien.DisplayLayout.Bands[0].ColHeadersVisible = false;
            setColorItemInCbb(cbcTCKHAC_KieuGiaoDien);
        }
        #endregion

        private void clpTCKHAC_MauCTuChuaGS_ColorChanged(object sender, EventArgs e)
        {

        }
        #endregion


        private bool ConpareList(List<SystemOption> lstSystemOptionsBanDau, List<SystemOption> lstSystemOptionsThayDoi)
        {
            for (int i = 0; i < lstSystemOptionsBanDau.Count; i++)
            {
                if (lstSystemOptionsBanDau[i].ID == lstSystemOptionsThayDoi[i].ID && lstSystemOptionsBanDau[i].Data == lstSystemOptionsThayDoi[i].Data)
                {
                    return true;
                }
            }
            return false;
        }
        #region Cách khác
        void MyHandler(object obj, EventArgs e)
        {
            ButApDung.Enabled = true;
        }
        void AddEvents(Control control)
        {
            foreach (Control c in control.Controls)
            {

                if (c is UltraTextEditor)
                {
                    ((UltraTextEditor)c).TextChanged += new EventHandler(MyHandler);

                }
                else if (c is CheckBox)
                {
                    ((CheckBox)c).CheckedChanged += new EventHandler(MyHandler);
                }
                else if (c is DateTimePicker)
                {
                    ((DateTimePicker)c).ValueChanged += new EventHandler(MyHandler);
                }
            }
        }
        #endregion

        private void chkTCKHAC_ChiQuaTonQuy_CheckStateChanged(object sender, EventArgs e)
        {
            //chkTCKHAC_ChiLuuKoGSo.Enabled = chkTCKHAC_ChiQuaTonQuy.CheckState == CheckState.Checked;
        }

        private void setColorItemInCbb(UltraCombo cbc)
        {
            UltraGridBand band = cbc.DisplayLayout.Bands[0];
            band.Override.SelectedRowAppearance.ForeColor = Color.Black;
            band.Override.ActiveRowAppearance.ForeColor = Color.Black;
            band.Override.CellAppearance.BackColor = System.Drawing.Color.LightYellow;
        }

        private void cbcPPTTGTGT_TextChanged(object sender, EventArgs e)
        {
            if (cbcPPTTGTGT.Text == "Phương pháp khấu trừ")
            {
                ultraLabel96.Visible = false;
                cbcVTHH_NhomNNMD.Visible = false;
                cbcVTHH_NhomNNMD.Value = "";
                ultraLabel51.Visible = true;
                cbcVTHH_NhomHHDV.Visible = true;
            }
            else
            {
                ultraLabel96.Visible = true;
                cbcVTHH_NhomNNMD.Visible = true;
                ultraLabel51.Visible = false;
                cbcVTHH_NhomHHDV.Visible = false;
                cbcVTHH_NhomHHDV.Value = "";
            }
        }

        private void opthinhthucsaoluu_ValueChanged(object sender, EventArgs e)
        {
            if (opthinhthucsaoluu.CheckedIndex == 2)
                ultraPanel1.Visible = true;
            else
                ultraPanel1.Visible = false;
        }

        private void numSLUU_NgaySLUU_ValueChanged(object sender, EventArgs e)
        {
            //if (numSLUU_NgaySLUU.Value.ToInt() < 5)
            //    numSLUU_NgaySLUU.Value = 5;

        }

        private void txtEmailForgotPass_AfterExitEditMode(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtEmailForgotPass.Text)) return;
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (!re.IsMatch(txtEmailForgotPass.Text.Trim()))
            {
                MessageBox.Show("Sai định dạng Email", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEmailForgotPass.Focus();
            }
        }
    }
}
