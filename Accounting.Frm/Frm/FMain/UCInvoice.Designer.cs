﻿namespace Accounting
{
    partial class UCInvoice
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCInvoice));
            this.btnInitializationReport = new Infragistics.Win.Misc.UltraButton();
            this.btnRegisterInvoice = new Infragistics.Win.Misc.UltraButton();
            this.btnPublishInvoice = new Infragistics.Win.Misc.UltraButton();
            this.btnAdjustAnnouncement = new Infragistics.Win.Misc.UltraButton();
            this.label7 = new System.Windows.Forms.Label();
            this.btnSituationInvoice = new Infragistics.Win.Misc.UltraButton();
            this.btnLostInvoice = new Infragistics.Win.Misc.UltraButton();
            this.btnDestructionInvoice = new Infragistics.Win.Misc.UltraButton();
            this.btnDeletedInvoice = new Infragistics.Win.Misc.UltraButton();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnInitializationReport
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.Image = global::Accounting.Properties.Resources.KhoiTaoHD1;
            appearance9.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance9.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance9.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnInitializationReport.Appearance = appearance9;
            this.btnInitializationReport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnInitializationReport.ImageSize = new System.Drawing.Size(80, 80);
            this.btnInitializationReport.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnInitializationReport.Location = new System.Drawing.Point(72, 186);
            this.btnInitializationReport.Name = "btnInitializationReport";
            this.btnInitializationReport.Size = new System.Drawing.Size(92, 97);
            this.btnInitializationReport.TabIndex = 40;
            // 
            // btnRegisterInvoice
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.Image = global::Accounting.Properties.Resources.DangKiSuDungHD;
            appearance10.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance10.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance10.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnRegisterInvoice.Appearance = appearance10;
            this.btnRegisterInvoice.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRegisterInvoice.ImageSize = new System.Drawing.Size(80, 80);
            this.btnRegisterInvoice.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnRegisterInvoice.Location = new System.Drawing.Point(282, 186);
            this.btnRegisterInvoice.Name = "btnRegisterInvoice";
            this.btnRegisterInvoice.Size = new System.Drawing.Size(92, 97);
            this.btnRegisterInvoice.TabIndex = 41;
            // 
            // btnPublishInvoice
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.Image = global::Accounting.Properties.Resources.ThongBaoPHHD;
            appearance11.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance11.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance11.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnPublishInvoice.Appearance = appearance11;
            this.btnPublishInvoice.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPublishInvoice.ImageSize = new System.Drawing.Size(80, 80);
            this.btnPublishInvoice.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnPublishInvoice.Location = new System.Drawing.Point(497, 186);
            this.btnPublishInvoice.Name = "btnPublishInvoice";
            this.btnPublishInvoice.Size = new System.Drawing.Size(92, 97);
            this.btnPublishInvoice.TabIndex = 48;
            // 
            // btnAdjustAnnouncement
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.Image = global::Accounting.Properties.Resources.DieuChinhTBPH;
            appearance12.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance12.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance12.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnAdjustAnnouncement.Appearance = appearance12;
            this.btnAdjustAnnouncement.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAdjustAnnouncement.ImageSize = new System.Drawing.Size(80, 80);
            this.btnAdjustAnnouncement.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnAdjustAnnouncement.Location = new System.Drawing.Point(497, 400);
            this.btnAdjustAnnouncement.Name = "btnAdjustAnnouncement";
            this.btnAdjustAnnouncement.Size = new System.Drawing.Size(92, 97);
            this.btnAdjustAnnouncement.TabIndex = 51;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.HotTrack;
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label7.Location = new System.Drawing.Point(631, 233);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(259, 6);
            this.label7.TabIndex = 54;
            // 
            // btnSituationInvoice
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.Image = global::Accounting.Properties.Resources.TinhHinhSDHD;
            appearance13.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance13.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance13.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnSituationInvoice.Appearance = appearance13;
            this.btnSituationInvoice.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSituationInvoice.ImageSize = new System.Drawing.Size(80, 80);
            this.btnSituationInvoice.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnSituationInvoice.Location = new System.Drawing.Point(1022, 186);
            this.btnSituationInvoice.Name = "btnSituationInvoice";
            this.btnSituationInvoice.Size = new System.Drawing.Size(92, 97);
            this.btnSituationInvoice.TabIndex = 56;
            // 
            // btnLostInvoice
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.Image = global::Accounting.Properties.Resources.BaoMatChayHongHD;
            appearance14.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance14.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance14.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnLostInvoice.Appearance = appearance14;
            this.btnLostInvoice.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLostInvoice.ImageSize = new System.Drawing.Size(80, 80);
            this.btnLostInvoice.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnLostInvoice.Location = new System.Drawing.Point(716, 344);
            this.btnLostInvoice.Name = "btnLostInvoice";
            this.btnLostInvoice.Size = new System.Drawing.Size(92, 97);
            this.btnLostInvoice.TabIndex = 58;
            // 
            // btnDestructionInvoice
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.Image = global::Accounting.Properties.Resources.HuyHD;
            appearance15.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance15.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance15.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnDestructionInvoice.Appearance = appearance15;
            this.btnDestructionInvoice.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDestructionInvoice.ImageSize = new System.Drawing.Size(80, 80);
            this.btnDestructionInvoice.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnDestructionInvoice.Location = new System.Drawing.Point(634, 13);
            this.btnDestructionInvoice.Name = "btnDestructionInvoice";
            this.btnDestructionInvoice.Size = new System.Drawing.Size(92, 97);
            this.btnDestructionInvoice.TabIndex = 60;
            // 
            // btnDeletedInvoice
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.Image = global::Accounting.Properties.Resources.XoaHD;
            appearance16.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance16.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance16.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnDeletedInvoice.Appearance = appearance16;
            this.btnDeletedInvoice.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDeletedInvoice.ImageSize = new System.Drawing.Size(80, 80);
            this.btnDeletedInvoice.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnDeletedInvoice.Location = new System.Drawing.Point(798, 13);
            this.btnDeletedInvoice.Name = "btnDeletedInvoice";
            this.btnDeletedInvoice.Size = new System.Drawing.Size(92, 97);
            this.btnDeletedInvoice.TabIndex = 62;
            // 
            // label8
            // 
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Image = ((System.Drawing.Image)(resources.GetObject("label8.Image")));
            this.label8.Location = new System.Drawing.Point(827, 140);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 63);
            this.label8.TabIndex = 61;
            // 
            // label6
            // 
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Image = ((System.Drawing.Image)(resources.GetObject("label6.Image")));
            this.label6.Location = new System.Drawing.Point(663, 140);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 63);
            this.label6.TabIndex = 59;
            // 
            // label5
            // 
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Image = ((System.Drawing.Image)(resources.GetObject("label5.Image")));
            this.label5.Location = new System.Drawing.Point(745, 253);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 63);
            this.label5.TabIndex = 57;
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Image = ((System.Drawing.Image)(resources.GetObject("label4.Image")));
            this.label4.Location = new System.Drawing.Point(912, 220);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 33);
            this.label4.TabIndex = 55;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.Location = new System.Drawing.Point(526, 309);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 63);
            this.label3.TabIndex = 50;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(410, 220);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 33);
            this.label2.TabIndex = 49;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(195, 220);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 33);
            this.label1.TabIndex = 47;
            // 
            // UCInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnDeletedInvoice);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnDestructionInvoice);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnLostInvoice);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSituationInvoice);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnAdjustAnnouncement);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnPublishInvoice);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnRegisterInvoice);
            this.Controls.Add(this.btnInitializationReport);
            this.Name = "UCInvoice";
            this.Size = new System.Drawing.Size(1282, 505);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnInitializationReport;
        private Infragistics.Win.Misc.UltraButton btnRegisterInvoice;
        private System.Windows.Forms.Label label1;
        private Infragistics.Win.Misc.UltraButton btnPublishInvoice;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Infragistics.Win.Misc.UltraButton btnAdjustAnnouncement;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private Infragistics.Win.Misc.UltraButton btnSituationInvoice;
        private Infragistics.Win.Misc.UltraButton btnLostInvoice;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private Infragistics.Win.Misc.UltraButton btnDestructionInvoice;
        private Infragistics.Win.Misc.UltraButton btnDeletedInvoice;
        private System.Windows.Forms.Label label8;
    }
}
