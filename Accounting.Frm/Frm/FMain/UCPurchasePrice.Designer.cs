﻿namespace Accounting
{
    partial class UCPurchasePrice
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCPurchasePrice));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.btnPPInvoice1 = new Infragistics.Win.Misc.UltraButton();
            this.btnPPGetInvoices = new Infragistics.Win.Misc.UltraButton();
            this.btnPPService = new Infragistics.Win.Misc.UltraButton();
            this.btnPPDiscountReturn = new Infragistics.Win.Misc.UltraButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnPPPayVendor = new Infragistics.Win.Misc.UltraButton();
            this.btnPPPayableSummary = new Infragistics.Win.Misc.UltraButton();
            this.btnPurchaseDetailSupplerInventoryItem = new Infragistics.Win.Misc.UltraButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnPPInvoice1
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance1.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnPPInvoice1.Appearance = appearance1;
            this.btnPPInvoice1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPPInvoice1.ImageSize = new System.Drawing.Size(80, 80);
            this.btnPPInvoice1.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnPPInvoice1.Location = new System.Drawing.Point(32, 4);
            this.btnPPInvoice1.Name = "btnPPInvoice1";
            this.btnPPInvoice1.Size = new System.Drawing.Size(92, 97);
            this.btnPPInvoice1.TabIndex = 34;
            // 
            // btnPPGetInvoices
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance2.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance2.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnPPGetInvoices.Appearance = appearance2;
            this.btnPPGetInvoices.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
            this.btnPPGetInvoices.HotTrackAppearance = appearance3;
            this.btnPPGetInvoices.ImageSize = new System.Drawing.Size(80, 80);
            this.btnPPGetInvoices.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnPPGetInvoices.Location = new System.Drawing.Point(230, 4);
            this.btnPPGetInvoices.Name = "btnPPGetInvoices";
            this.btnPPGetInvoices.Size = new System.Drawing.Size(92, 97);
            this.btnPPGetInvoices.TabIndex = 35;
            // 
            // btnPPService
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance4.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance4.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnPPService.Appearance = appearance4;
            this.btnPPService.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
            this.btnPPService.HotTrackAppearance = appearance5;
            this.btnPPService.ImageSize = new System.Drawing.Size(80, 80);
            this.btnPPService.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnPPService.Location = new System.Drawing.Point(86, 198);
            this.btnPPService.Name = "btnPPService";
            this.btnPPService.Size = new System.Drawing.Size(92, 97);
            this.btnPPService.TabIndex = 36;
            // 
            // btnPPDiscountReturn
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
            appearance6.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance6.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance6.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnPPDiscountReturn.Appearance = appearance6;
            this.btnPPDiscountReturn.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance7.Image = ((object)(resources.GetObject("appearance7.Image")));
            this.btnPPDiscountReturn.HotTrackAppearance = appearance7;
            this.btnPPDiscountReturn.ImageSize = new System.Drawing.Size(80, 80);
            this.btnPPDiscountReturn.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnPPDiscountReturn.Location = new System.Drawing.Point(230, 387);
            this.btnPPDiscountReturn.Name = "btnPPDiscountReturn";
            this.btnPPDiscountReturn.Size = new System.Drawing.Size(92, 97);
            this.btnPPDiscountReturn.TabIndex = 37;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(244, 230);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 33);
            this.label2.TabIndex = 40;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(150, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 33);
            this.label1.TabIndex = 41;
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Image = ((System.Drawing.Image)(resources.GetObject("label4.Image")));
            this.label4.Location = new System.Drawing.Point(260, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 63);
            this.label4.TabIndex = 43;
            // 
            // label5
            // 
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Image = ((System.Drawing.Image)(resources.GetObject("label5.Image")));
            this.label5.Location = new System.Drawing.Point(260, 303);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 63);
            this.label5.TabIndex = 44;
            // 
            // btnPPPayVendor
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.Image = ((object)(resources.GetObject("appearance8.Image")));
            appearance8.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance8.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance8.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnPPPayVendor.Appearance = appearance8;
            this.btnPPPayVendor.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance9.Image = ((object)(resources.GetObject("appearance9.Image")));
            this.btnPPPayVendor.HotTrackAppearance = appearance9;
            this.btnPPPayVendor.ImageSize = new System.Drawing.Size(80, 80);
            this.btnPPPayVendor.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnPPPayVendor.Location = new System.Drawing.Point(373, 198);
            this.btnPPPayVendor.Name = "btnPPPayVendor";
            this.btnPPPayVendor.Size = new System.Drawing.Size(92, 97);
            this.btnPPPayVendor.TabIndex = 45;
            // 
            // btnPPPayableSummary
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.Image = ((object)(resources.GetObject("appearance10.Image")));
            appearance10.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance10.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance10.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnPPPayableSummary.Appearance = appearance10;
            this.btnPPPayableSummary.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance11.Image = ((object)(resources.GetObject("appearance11.Image")));
            this.btnPPPayableSummary.HotTrackAppearance = appearance11;
            this.btnPPPayableSummary.ImageSize = new System.Drawing.Size(80, 80);
            this.btnPPPayableSummary.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnPPPayableSummary.Location = new System.Drawing.Point(655, 4);
            this.btnPPPayableSummary.Name = "btnPPPayableSummary";
            this.btnPPPayableSummary.Size = new System.Drawing.Size(92, 97);
            this.btnPPPayableSummary.TabIndex = 46;
            // 
            // btnPurchaseDetailSupplerInventoryItem
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.Image = ((object)(resources.GetObject("appearance12.Image")));
            appearance12.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance12.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance12.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnPurchaseDetailSupplerInventoryItem.Appearance = appearance12;
            this.btnPurchaseDetailSupplerInventoryItem.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance13.Image = ((object)(resources.GetObject("appearance13.Image")));
            this.btnPurchaseDetailSupplerInventoryItem.HotTrackAppearance = appearance13;
            this.btnPurchaseDetailSupplerInventoryItem.ImageSize = new System.Drawing.Size(80, 80);
            this.btnPurchaseDetailSupplerInventoryItem.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnPurchaseDetailSupplerInventoryItem.Location = new System.Drawing.Point(655, 387);
            this.btnPurchaseDetailSupplerInventoryItem.Name = "btnPurchaseDetailSupplerInventoryItem";
            this.btnPurchaseDetailSupplerInventoryItem.Size = new System.Drawing.Size(92, 97);
            this.btnPurchaseDetailSupplerInventoryItem.TabIndex = 47;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.Location = new System.Drawing.Point(481, 230);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 33);
            this.label3.TabIndex = 48;
            // 
            // label6
            // 
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Image = ((System.Drawing.Image)(resources.GetObject("label6.Image")));
            this.label6.Location = new System.Drawing.Point(578, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 33);
            this.label6.TabIndex = 49;
            // 
            // label7
            // 
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Image = ((System.Drawing.Image)(resources.GetObject("label7.Image")));
            this.label7.Location = new System.Drawing.Point(578, 425);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 33);
            this.label7.TabIndex = 50;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(108)))), ((int)(((byte)(158)))));
            this.label8.Location = new System.Drawing.Point(566, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(6, 394);
            this.label8.TabIndex = 51;
            // 
            // UCPurchasePrice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnPurchaseDetailSupplerInventoryItem);
            this.Controls.Add(this.btnPPPayableSummary);
            this.Controls.Add(this.btnPPPayVendor);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnPPDiscountReturn);
            this.Controls.Add(this.btnPPService);
            this.Controls.Add(this.btnPPGetInvoices);
            this.Controls.Add(this.btnPPInvoice1);
            this.Name = "UCPurchasePrice";
            this.Size = new System.Drawing.Size(823, 496);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnPPInvoice1;
        private Infragistics.Win.Misc.UltraButton btnPPGetInvoices;
        private Infragistics.Win.Misc.UltraButton btnPPService;
        private Infragistics.Win.Misc.UltraButton btnPPDiscountReturn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private Infragistics.Win.Misc.UltraButton btnPPPayVendor;
        private Infragistics.Win.Misc.UltraButton btnPPPayableSummary;
        private Infragistics.Win.Misc.UltraButton btnPurchaseDetailSupplerInventoryItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}
