﻿namespace Accounting
{
    partial class UCTool
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCTool));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.btnTool = new Infragistics.Win.Misc.UltraButton();
            this.btnTIBuyAndIncrement = new Infragistics.Win.Misc.UltraButton();
            this.btnTITransfer = new Infragistics.Win.Misc.UltraButton();
            this.btnTIAllocation = new Infragistics.Win.Misc.UltraButton();
            this.btnTIAdjustment = new Infragistics.Win.Misc.UltraButton();
            this.btnTIDecrement = new Infragistics.Win.Misc.UltraButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnTool
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance1.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnTool.Appearance = appearance1;
            this.btnTool.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
            this.btnTool.HotTrackAppearance = appearance2;
            this.btnTool.ImageSize = new System.Drawing.Size(80, 80);
            this.btnTool.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnTool.Location = new System.Drawing.Point(41, 23);
            this.btnTool.Name = "btnTool";
            this.btnTool.Size = new System.Drawing.Size(92, 97);
            this.btnTool.TabIndex = 38;
            // 
            // btnTIBuyAndIncrement
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnTIBuyAndIncrement.Appearance = appearance3;
            this.btnTIBuyAndIncrement.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
            this.btnTIBuyAndIncrement.HotTrackAppearance = appearance4;
            this.btnTIBuyAndIncrement.ImageSize = new System.Drawing.Size(80, 80);
            this.btnTIBuyAndIncrement.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnTIBuyAndIncrement.Location = new System.Drawing.Point(261, 23);
            this.btnTIBuyAndIncrement.Name = "btnTIBuyAndIncrement";
            this.btnTIBuyAndIncrement.Size = new System.Drawing.Size(92, 97);
            this.btnTIBuyAndIncrement.TabIndex = 39;
            // 
            // btnTIDecrement
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance5.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnTITransfer.Appearance = appearance5;
            this.btnTITransfer.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
            this.btnTITransfer.HotTrackAppearance = appearance6;
            this.btnTITransfer.ImageSize = new System.Drawing.Size(80, 80);
            this.btnTITransfer.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnTITransfer.Location = new System.Drawing.Point(41, 218);
            this.btnTITransfer.Name = "btnTIDecrement";
            this.btnTITransfer.Size = new System.Drawing.Size(92, 97);
            this.btnTITransfer.TabIndex = 40;
            // 
            // btnTIAllocation
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.Image = ((object)(resources.GetObject("appearance7.Image")));
            appearance7.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance7.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance7.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnTIAllocation.Appearance = appearance7;
            this.btnTIAllocation.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance8.Image = ((object)(resources.GetObject("appearance8.Image")));
            this.btnTIAllocation.HotTrackAppearance = appearance8;
            this.btnTIAllocation.ImageSize = new System.Drawing.Size(80, 80);
            this.btnTIAllocation.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnTIAllocation.Location = new System.Drawing.Point(261, 217);
            this.btnTIAllocation.Name = "btnTIAllocation";
            this.btnTIAllocation.Size = new System.Drawing.Size(92, 97);
            this.btnTIAllocation.TabIndex = 41;
            // 
            // btnTIAdjustment
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.Image = ((object)(resources.GetObject("appearance9.Image")));
            appearance9.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance9.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance9.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnTIAdjustment.Appearance = appearance9;
            this.btnTIAdjustment.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance10.Image = ((object)(resources.GetObject("appearance10.Image")));
            this.btnTIAdjustment.HotTrackAppearance = appearance10;
            this.btnTIAdjustment.ImageSize = new System.Drawing.Size(80, 80);
            this.btnTIAdjustment.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnTIAdjustment.Location = new System.Drawing.Point(261, 411);
            this.btnTIAdjustment.Name = "btnTIAdjustment";
            this.btnTIAdjustment.Size = new System.Drawing.Size(92, 97);
            this.btnTIAdjustment.TabIndex = 42;
            // 
            // btnRS11SDNN
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.Image = ((object)(resources.GetObject("appearance11.Image")));
            appearance11.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance11.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance11.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnTIDecrement.Appearance = appearance11;
            this.btnTIDecrement.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance12.Image = ((object)(resources.GetObject("appearance12.Image")));
            this.btnTIDecrement.HotTrackAppearance = appearance12;
            this.btnTIDecrement.ImageSize = new System.Drawing.Size(80, 80);
            this.btnTIDecrement.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnTIDecrement.Location = new System.Drawing.Point(475, 218);
            this.btnTIDecrement.Name = "btnRS11SDNN";
            this.btnTIDecrement.Size = new System.Drawing.Size(92, 97);
            this.btnTIDecrement.TabIndex = 43;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(171, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 33);
            this.label2.TabIndex = 45;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(171, 250);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 33);
            this.label1.TabIndex = 46;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.Location = new System.Drawing.Point(385, 250);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 33);
            this.label3.TabIndex = 47;
            // 
            // label5
            // 
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Image = ((System.Drawing.Image)(resources.GetObject("label5.Image")));
            this.label5.Location = new System.Drawing.Point(291, 331);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 63);
            this.label5.TabIndex = 48;
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Image = ((System.Drawing.Image)(resources.GetObject("label4.Image")));
            this.label4.Location = new System.Drawing.Point(291, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 63);
            this.label4.TabIndex = 49;
            // 
            // UCTool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnTIDecrement);
            this.Controls.Add(this.btnTIAdjustment);
            this.Controls.Add(this.btnTIAllocation);
            this.Controls.Add(this.btnTITransfer);
            this.Controls.Add(this.btnTIBuyAndIncrement);
            this.Controls.Add(this.btnTool);
            this.Name = "UCTool";
            this.Size = new System.Drawing.Size(673, 521);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnTool;
        private Infragistics.Win.Misc.UltraButton btnTIBuyAndIncrement;
        private Infragistics.Win.Misc.UltraButton btnTITransfer;
        private Infragistics.Win.Misc.UltraButton btnTIAllocation;
        private Infragistics.Win.Misc.UltraButton btnTIAdjustment;
        private Infragistics.Win.Misc.UltraButton btnTIDecrement;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
    }
}
