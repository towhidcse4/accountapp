﻿namespace Accounting.Frm.FMain
{
    partial class FWarningImportUpdateData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FWarningImportUpdateData));
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            this.LabelNotifi = new Infragistics.Win.Misc.UltraLabel();
            this.uGridExport = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.btnImport = new Infragistics.Win.Misc.UltraButton();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.optHienThi = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            ((System.ComponentModel.ISupportInitialize)(this.uGridExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optHienThi)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelNotifi
            // 
            this.LabelNotifi.AutoSize = true;
            this.LabelNotifi.Location = new System.Drawing.Point(12, 12);
            this.LabelNotifi.Name = "LabelNotifi";
            this.LabelNotifi.Size = new System.Drawing.Size(60, 14);
            this.LabelNotifi.TabIndex = 0;
            this.LabelNotifi.Text = "ultraLabel1";
            // 
            // uGridExport
            // 
            this.uGridExport.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridExport.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridExport.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridExport.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridExport.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGridExport.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGridExport.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridExport.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridExport.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridExport.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridExport.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGridExport.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGridExport.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGridExport.Location = new System.Drawing.Point(261, 12);
            this.uGridExport.Name = "uGridExport";
            this.uGridExport.Size = new System.Drawing.Size(179, 106);
            this.uGridExport.TabIndex = 3037;
            this.uGridExport.Text = "ultraGrid1";
            this.uGridExport.Visible = false;
            // 
            // btnImport
            // 
            this.btnImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
            this.btnImport.Appearance = appearance4;
            this.btnImport.Location = new System.Drawing.Point(245, 82);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(94, 26);
            this.btnImport.TabIndex = 3038;
            this.btnImport.Text = "Đồng ý";
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
            this.btnCancel.Appearance = appearance5;
            this.btnCancel.Location = new System.Drawing.Point(349, 82);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(91, 26);
            this.btnCancel.TabIndex = 3039;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // optHienThi
            // 
            this.optHienThi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            appearance6.FontData.BoldAsString = "False";
            this.optHienThi.Appearance = appearance6;
            this.optHienThi.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.optHienThi.CheckedIndex = 0;
            valueListItem5.DataValue = "Default Item";
            valueListItem5.DisplayText = "Cập nhật thông tin với mã trùng";
            valueListItem5.Tag = "CapNhat";
            valueListItem4.DataValue = "ValueListItem1";
            valueListItem4.DisplayText = "Không cập nhật thông tin mã trùng";
            valueListItem4.Tag = "KhongCapNhat";
            this.optHienThi.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem5,
            valueListItem4});
            this.optHienThi.Location = new System.Drawing.Point(12, 38);
            this.optHienThi.Name = "optHienThi";
            this.optHienThi.Size = new System.Drawing.Size(207, 38);
            this.optHienThi.TabIndex = 0;
            this.optHienThi.Text = "Cập nhật thông tin với mã trùng";
            // 
            // FWarningImportUpdateData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(452, 120);
            this.Controls.Add(this.optHienThi);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.uGridExport);
            this.Controls.Add(this.LabelNotifi);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FWarningImportUpdateData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thông báo";
            ((System.ComponentModel.ISupportInitialize)(this.uGridExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optHienThi)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel LabelNotifi;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridExport;
        private Infragistics.Win.Misc.UltraButton btnImport;
        private Infragistics.Win.Misc.UltraButton btnCancel;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet optHienThi;
    }
}