﻿namespace Accounting
{
    partial class UCBank
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCBank));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            this.btnMBDeposit = new Infragistics.Win.Misc.UltraButton();
            this.btnMBTellerPaper = new Infragistics.Win.Misc.UltraButton();
            this.btnMCompare = new Infragistics.Win.Misc.UltraButton();
            this.btnRS06DNN = new Infragistics.Win.Misc.UltraButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnMBDeposit
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance1.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnMBDeposit.Appearance = appearance1;
            this.btnMBDeposit.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
            this.btnMBDeposit.HotTrackAppearance = appearance2;
            this.btnMBDeposit.ImageSize = new System.Drawing.Size(80, 80);
            this.btnMBDeposit.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnMBDeposit.Location = new System.Drawing.Point(3, 3);
            this.btnMBDeposit.Name = "btnMBDeposit";
            this.btnMBDeposit.Size = new System.Drawing.Size(92, 97);
            this.btnMBDeposit.TabIndex = 40;
            // 
            // btnMBTellerPaper
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnMBTellerPaper.Appearance = appearance3;
            this.btnMBTellerPaper.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
            this.btnMBTellerPaper.HotTrackAppearance = appearance4;
            this.btnMBTellerPaper.ImageSize = new System.Drawing.Size(80, 80);
            this.btnMBTellerPaper.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnMBTellerPaper.Location = new System.Drawing.Point(3, 289);
            this.btnMBTellerPaper.Name = "btnMBTellerPaper";
            this.btnMBTellerPaper.Size = new System.Drawing.Size(92, 97);
            this.btnMBTellerPaper.TabIndex = 41;
            this.btnMBTellerPaper.Click += new System.EventHandler(this.btnMBTellerPaper_Click);
            // 
            // btnMCompare
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance5.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnMCompare.Appearance = appearance5;
            this.btnMCompare.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
            this.btnMCompare.HotTrackAppearance = appearance6;
            this.btnMCompare.ImageSize = new System.Drawing.Size(80, 80);
            this.btnMCompare.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnMCompare.Location = new System.Drawing.Point(268, 146);
            this.btnMCompare.Name = "btnMCompare";
            this.btnMCompare.Size = new System.Drawing.Size(92, 97);
            this.btnMCompare.TabIndex = 42;
            // 
            // btnRS06DNN
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.Image = ((object)(resources.GetObject("appearance7.Image")));
            appearance7.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance7.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance7.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnRS06DNN.Appearance = appearance7;
            this.btnRS06DNN.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance8.Image = ((object)(resources.GetObject("appearance8.Image")));
            this.btnRS06DNN.HotTrackAppearance = appearance8;
            this.btnRS06DNN.ImageSize = new System.Drawing.Size(80, 80);
            this.btnRS06DNN.ImageTransparentColor = System.Drawing.SystemColors.Control;
            this.btnRS06DNN.Location = new System.Drawing.Point(480, 146);
            this.btnRS06DNN.Name = "btnRS06DNN";
            this.btnRS06DNN.Size = new System.Drawing.Size(92, 97);
            this.btnRS06DNN.TabIndex = 43;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.Location = new System.Drawing.Point(101, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 33);
            this.label3.TabIndex = 49;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(101, 321);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 33);
            this.label1.TabIndex = 50;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(108)))), ((int)(((byte)(158)))));
            this.label8.Location = new System.Drawing.Point(166, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(6, 288);
            this.label8.TabIndex = 52;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(192, 178);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 33);
            this.label2.TabIndex = 53;
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Image = ((System.Drawing.Image)(resources.GetObject("label4.Image")));
            this.label4.Location = new System.Drawing.Point(391, 178);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 33);
            this.label4.TabIndex = 54;
            // 
            // UCBank
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnRS06DNN);
            this.Controls.Add(this.btnMCompare);
            this.Controls.Add(this.btnMBTellerPaper);
            this.Controls.Add(this.btnMBDeposit);
            this.Name = "UCBank";
            this.Size = new System.Drawing.Size(580, 392);
            this.ResumeLayout(false);

        }

        #endregion

        public Infragistics.Win.Misc.UltraButton btnMBDeposit;
        public Infragistics.Win.Misc.UltraButton btnMBTellerPaper;
        public Infragistics.Win.Misc.UltraButton btnMCompare;
        public Infragistics.Win.Misc.UltraButton btnRS06DNN;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
    }
}
