﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class UCTax : UserControl
    {
        public UCTax(FMain @this)
        {
            InitializeComponent();
            btnKhauTruThue.Click += (s, e) => btnKhauTruThue_Click(s, e, @this);
            btnNopThue.Click += (s, e) => btnNopThue_Click(s, e, @this);
            btnToKhai.Click += (s, e) => btnToKhai_Click(s, e, @this);
        }

        private void btnKhauTruThue_Click(object sender, EventArgs e,FMain fMain)
        {
            fMain.ShowTaxPeriod();
            
        }

        private void btnNopThue_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowTaxSubmit();//add by cuongpv
        }
        private void btnToKhai_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFTax(0, "Tờ khai thuế GTGT khấu trừ (Mẫu 01/GTGT)");
        }
    }
}
