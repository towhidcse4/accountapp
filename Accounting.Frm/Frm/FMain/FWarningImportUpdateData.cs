﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using ClosedXML.Excel;

namespace Accounting.Frm.FMain
{
    public partial class FWarningImportUpdateData : Form
    {
        public List<AccountingObject> lstAccountingObjectUpdate = new List<AccountingObject>();
        public List<AccountingObject> lstAccountingObjectAdd = new List<AccountingObject>();
        XLWorkbook wbook;
        public FWarningImportUpdateData(string notifi, List<AccountingObject> lstAccountingObjectsUpdate, List<AccountingObject> lstAccountingObjectsAdd, XLWorkbook wb)
        {
            InitializeComponent();
            LabelNotifi.Text = notifi;
            lstAccountingObjectUpdate = lstAccountingObjectsUpdate;
            lstAccountingObjectAdd = lstAccountingObjectsAdd;
            wbook = wb;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private bool ExecQueries(string query)
        {
            string serverName = Properties.Settings.Default.ServerName;
            string database = Properties.Settings.Default.DatabaseName;
            string user = Properties.Settings.Default.UserName;
            string pass = Properties.Settings.Default.Password;

            if (serverName == null || database == null || user == null || pass == null)
            {
                return false;
            }
            SqlConnection sqlConnection = new SqlConnection("");
            if (!string.IsNullOrEmpty(serverName) && !string.IsNullOrEmpty(database) && !string.IsNullOrEmpty(pass) && !serverName.Equals("14.225.3.208") && !serverName.Equals("14.225.3.218"))
            {
                string _ConnectionString = "Data Source=" + serverName + ";Initial Catalog=" + database + ";User ID=" + user + ";Password=" + pass + "";

                sqlConnection = new SqlConnection(_ConnectionString);

                SqlTransaction transaction;
                try
                {
                    sqlConnection.Open();
                }
                catch (Exception)
                {
                    WaitingFrm.StopWaiting();
                    MSG.Warning("Máy chủ SQL không tồn tại. Vui lòng chọn máy chủ SQL khác.");
                    return false;
                }
                transaction = sqlConnection.BeginTransaction();
                try
                {
                    //string q = "";
                    //foreach (var query in sqlqueries)
                    //{
                    //    q += query + " ";
                    //}
                    new SqlCommand(query, sqlConnection, transaction).ExecuteNonQuery();
                    transaction.Commit();
                    //transaction.Dispose();
                    //UnbindSess
                }
                catch (SqlException sqlError)
                {
                    WaitingFrm.StopWaiting();
                    MSG.Warning(sqlError.ToString());
                    transaction.Rollback();
                    return false;
                }
                sqlConnection.Close();
                //sqlConnection.Dispose();
            }
            else return false;
            return true;
        }
        private void btnImport_Click(object sender, EventArgs e)
        {
            
            if (optHienThi.CheckedItem.Tag.ToString().Equals("CapNhat"))
            {//update database
                WaitingFrm.StartWaiting();
                string query = "";
                try
                {
                    //query += "Set IDENTITY_INSERT AccountingObjectBankAccount on";
                    var count = lstAccountingObjectUpdate.GroupBy(n => n.AccountingObjectCode);
                    foreach (var item in lstAccountingObjectUpdate)
                    {
                        item.IsActive = true;
                        item.ID = Guid.NewGuid();
                        AccountingObjectBankAccount accountingObjectBankAccount = new AccountingObjectBankAccount();
                        if (!item.BankName.IsNullOrEmpty() || !item.BankAccount.IsNullOrEmpty())
                        {
                            accountingObjectBankAccount.ID = Guid.NewGuid();
                            accountingObjectBankAccount.AccountingObjectID = item.ID;
                            accountingObjectBankAccount.BankName = item.BankName;
                            accountingObjectBankAccount.BankAccount = item.BankAccount;
                            item.BankAccounts.Add(accountingObjectBankAccount);
                        }
                        query += " " + string.Format("UPDATE AccountingObject SET AccountingObjectName = N'{0}' , EmployeeBirthday = {1} , Address = {2} , Tel = {3} , Email = {4} , Website = {5} , BankAccount = {6} , BankName = {7} , TaxCode = {8} , ContactTitle = {9} , ContactSex = {10} , ObjectType = {11} , IdentificationNo = {12} , IssueDate = {13} , IssueBy = {14} , DepartmentID = {15} , IsInsured = {16} , IsLabourUnionFree = {17} , IsActive = {18} , NumberOfDependent = {19} , AgreementSalary = {20} , InsuranceSalary = {21} , SalaryCoefficient = {22} , IsEmployee = {23} , ScaleType = {24} , Fax = {25} , ContactEmail = {26} WHERE AccountingObjectCode = N'{27}'",
                            
                            item.AccountingObjectName.Replace("'", "'+CHAR(39)+N'"),
                            item.EmployeeBirthday == null ? "NULL" : "'" + item.EmployeeBirthday.ToString() + "'",
                            item.Address == null ? "NULL" : "N" + "'" + item.Address.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                            item.Tel == null ? "NULL" : "N" + "'" + item.Tel.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                            item.Email == null ? "NULL" : "N" + "'" + item.Email.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                            item.Website == null ? "NULL" : "N" + "'" + item.Website.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                            item.BankAccount == null ? "NULL" : "N" + "'" + item.BankAccount.Replace("'", "'+CHAR(39)+N'") + "'",
                            item.BankName == null ? "NULL" : "N" + "'" + item.BankName.Replace("'", "'+CHAR(39)+N'") + "'",
                            item.TaxCode == null ? "NULL" : "N" + "'" + item.TaxCode.Replace("'", "'+CHAR(39)+N'") + "'",
                            item.ContactTitle == null ? "NULL" : "N" + "'" + item.ContactTitle.Replace("'", "'+CHAR(39)+N'") + "'",
                            item.ContactSex == null ? "NULL" : "'" + "" + item.ContactSex + "'",
                            item.ObjectType == null ? "NULL" : "'" + "" + item.ObjectType + "'",
                            item.IdentificationNo == null ? "NULL" : "N" + "'" + item.IdentificationNo.Replace("'", "'+CHAR(39)+N'") + "'",
                            item.IssueDate == null ? "NULL" : "'" + "" + item.IssueDate + "'",
                            item.IssueBy == null ? "NULL" : "N" + "'" + item.IssueBy.Replace("'", "'+CHAR(39)+N'") + "'",
                            item.DepartmentID == null ? "NULL" : "" + "'" + item.DepartmentID + "'",
                            item.IsInsured == null ? "NULL" : "'" + (item.IsInsured == true ? "1" : "0") + "'",
                            item.IsLabourUnionFree == null ? "NULL" : "'" + (item.IsLabourUnionFree == true ? "1" : "0") + "'",
                            item.IsActive == null ? "NULL" : "'" + (item.IsActive == true ? "1" : "0") + "'",
                            item.NumberOfDependent == null ? "NULL" : "'" + "" + item.NumberOfDependent + "'",
                            item.AgreementSalary == null ? "NULL" : "" + "'" + item.AgreementSalary.ToString().Replace(',', '.') + "'",
                            item.InsuranceSalary == null ? "NULL" : "" + "'" + item.InsuranceSalary.ToString().Replace(',', '.') + "'",
                            item.SalaryCoefficient == null ? "NULL" : "" + "'" + item.SalaryCoefficient.ToString().Replace(',', '.') + "'",
                            item.IsEmployee == null ? "NULL" : "'" + (item.IsEmployee == true ? "1" : "0") + "'",
                            item.ScaleType == null ? "NULL" : "'" + "" + item.ScaleType + "'",
                            item.Fax == null ? "NULL" : "'" + "" + item.Fax.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                            item.ContactEmail == null ? "NULL" : "N" + "'" + item.ContactEmail.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                             item.AccountingObjectCode.Replace("'", "'+CHAR(39)+N'")
                            );
                        if (!item.BankName.IsNullOrEmpty() || !item.BankAccount.IsNullOrEmpty())
                        {
                            query += " " + string.Format("INSERT INTO AccountingObjectBankAccount(" +
                                "ID," +
                                "AccountingObjectID," +
                                "BankName," +
                                "BankAccount," +
                                "IsSelect) VALUES ('{0}',N'{1}',N'{2}',N'{3}','{4}');",
                                accountingObjectBankAccount.ID,
                                accountingObjectBankAccount.AccountingObjectID == null ? "NULL" : accountingObjectBankAccount.AccountingObjectID.ToString(),
                                accountingObjectBankAccount.BankName == null ? "NULL" : accountingObjectBankAccount.BankName.ToString().Replace("'", "'+CHAR(39)+N'"),
                                accountingObjectBankAccount.BankAccount == null ? "NULL" : accountingObjectBankAccount.BankAccount.ToString().Replace("'", "'+CHAR(39)+N'"),
                                "1");
                        }

                    }

                    foreach (var item in lstAccountingObjectAdd)
                    {
                        item.IsActive = true;
                        item.ID = Guid.NewGuid();
                        AccountingObjectBankAccount accountingObjectBankAccount = new AccountingObjectBankAccount();
                        if (!item.BankName.IsNullOrEmpty() || !item.BankAccount.IsNullOrEmpty())
                        {
                            accountingObjectBankAccount.ID = Guid.NewGuid();
                            accountingObjectBankAccount.AccountingObjectID = item.ID;
                            accountingObjectBankAccount.BankName = item.BankName;
                            accountingObjectBankAccount.BankAccount = item.BankAccount;
                            item.BankAccounts.Add(accountingObjectBankAccount);
                        }
                        query += " " + string.Format("INSERT INTO AccountingObject(" +
                            "ID, " +
                            "AccountingObjectCode, " +
                            "AccountingObjectName, " +
                            "EmployeeBirthday, " +
                            "Address, " +
                            "Tel, " +
                            "Email, " +
                            "Website, " +
                            "BankAccount, " +
                            "BankName, " +
                            "TaxCode, " +
                            "ContactTitle, " +
                            "ContactSex, " +
                            "ObjectType, " +
                            "IdentificationNo, " +
                            "IssueDate, " +
                            "IssueBy, " +
                            "DepartmentID, " +
                            "IsInsured, " +
                            "IsLabourUnionFree, " +
                            "IsActive, " +
                            "NumberOfDependent," +
                            " AgreementSalary, " +
                            "InsuranceSalary, " +
                            "SalaryCoefficient, " +
                            "IsEmployee, " +
                            "ScaleType," +
                            "Fax," +
                            "ContactEmail) VALUES ('{0}',N'{1}',N'{2}',{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28});",
                            item.ID,
                            item.AccountingObjectCode.Replace("'", "'+CHAR(39)+N'"),
                            item.AccountingObjectName.Replace("'", "'+CHAR(39)+N'"),
                            item.EmployeeBirthday == null ? "NULL" : "'" + item.EmployeeBirthday.ToString() + "'",
                            item.Address == null ? "NULL" : "N" + "'" + item.Address.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                            item.Tel == null ? "NULL" : "N" + "'" + item.Tel.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                            item.Email == null ? "NULL" : "N" + "'" + item.Email.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                            item.Website == null ? "NULL" : "N" + "'" + item.Website.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                            item.BankAccount == null ? "NULL" : "N" + "'" + item.BankAccount.Replace("'", "'+CHAR(39)+N'") + "'",
                            item.BankName == null ? "NULL" : "N" + "'" + item.BankName.Replace("'", "'+CHAR(39)+N'") + "'",
                            item.TaxCode == null ? "NULL" : "N" + "'" + item.TaxCode.Replace("'", "'+CHAR(39)+N'") + "'",
                            item.ContactTitle == null ? "NULL" : "N" + "'" + item.ContactTitle.Replace("'", "'+CHAR(39)+N'") + "'",
                            item.ContactSex == null ? "NULL" : "'" + "" + item.ContactSex + "'",
                            item.ObjectType == null ? "NULL" : "'" + "" + item.ObjectType + "'",
                            item.IdentificationNo == null ? "NULL" : "N" + "'" + item.IdentificationNo.Replace("'", "'+CHAR(39)+N'") + "'",
                            item.IssueDate == null ? "NULL" : "'" + "" + item.IssueDate + "'",
                            item.IssueBy == null ? "NULL" : "N" + "'" + item.IssueBy.Replace("'", "'+CHAR(39)+N'") + "'",
                            item.DepartmentID == null ? "NULL" : "" + "'" + item.DepartmentID + "'",
                            item.IsInsured == null ? "NULL" : "'" + (item.IsInsured == true ? "1" : "0") + "'",
                            item.IsLabourUnionFree == null ? "NULL" : "'" + (item.IsLabourUnionFree == true ? "1" : "0") + "'",
                            item.IsActive == null ? "NULL" : "'" + (item.IsActive == true ? "1" : "0") + "'",
                            item.NumberOfDependent == null ? "NULL" : "'" + "" + item.NumberOfDependent + "'",
                            item.AgreementSalary == null ? "NULL" : "" + "'" + item.AgreementSalary.ToString().Replace(',', '.') + "'",
                            item.InsuranceSalary == null ? "NULL" : "" + "'" + item.InsuranceSalary.ToString().Replace(',', '.') + "'",
                            item.SalaryCoefficient == null ? "NULL" : "" + "'" + item.SalaryCoefficient.ToString().Replace(',', '.') + "'",
                            item.IsEmployee == null ? "NULL" : "'" + (item.IsEmployee == true ? "1" : "0") + "'",
                            item.ScaleType == null ? "NULL" : "'" + "" + item.ScaleType + "'",
                            item.Fax == null ? "NULL" : "'" + "" + item.Fax.ToString().Replace("'", "'+CHAR(39)+N'") + "'",
                            item.ContactEmail == null ? "NULL" : "N" + "'" + item.ContactEmail.ToString().Replace("'", "'+CHAR(39)+N'") + "'"
                            );
                        if (!item.BankName.IsNullOrEmpty() || !item.BankAccount.IsNullOrEmpty())
                        {
                            query += " " + string.Format("INSERT INTO AccountingObjectBankAccount(" +
                                "ID," +
                                "AccountingObjectID," +
                                "BankName," +
                                "BankAccount," +
                                "IsSelect) VALUES ('{0}',N'{1}',N'{2}',N'{3}','{4}');",
                                accountingObjectBankAccount.ID,
                                accountingObjectBankAccount.AccountingObjectID == null ? "NULL" : accountingObjectBankAccount.AccountingObjectID.ToString(),
                                accountingObjectBankAccount.BankName == null ? "NULL" : accountingObjectBankAccount.BankName.ToString().Replace("'", "'+CHAR(39)+N'"),
                                accountingObjectBankAccount.BankAccount == null ? "NULL" : accountingObjectBankAccount.BankAccount.ToString().Replace("'", "'+CHAR(39)+N'"),
                                "1");
                        }

                    }
                    //query += " Set IDENTITY_INSERT AccountingObjectBankAccount off";

                    //Utils.ISABillService.CommitTran();
                    if (!ExecQueries(query))
                    {
                        WaitingFrm.StopWaiting();
                        MSG.Error("Cập nhật dữ liệu không thành công");
                        return;
                    }
                    //var t = Utils.IAccountingObjectService.Query.ToList();
                    //Utils.IAccountingObjectService.UnbindSession(Utils.ListAccountingObject);
                    Utils.IAccountingObjectService.RolbackTran();
                    Utils.IAccountingObjectBankAccountService.RolbackTran();
                    Utils.ClearCacheByType<AccountingObject>();
                    //Utils.IAccountingObjectBankAccountService.UnbindSession(Utils.ListAccountingObjectBank);
                    Utils.ClearCacheByType<AccountingObjectBankAccount>();
                    WaitingFrm.StopWaiting();
                    MSG.Information("Cập nhật dữ liệu thành công");
                    this.Close();


                }
                catch (Exception ex)
                {
                    Utils.ISABillService.RolbackTran();
                    WaitingFrm.StopWaiting();
                }
            }
            else if (optHienThi.CheckedItem.Tag.ToString().Equals("KhongCapNhat"))
            {

                System.Windows.Forms.SaveFileDialog sf = new System.Windows.Forms.SaveFileDialog
                {
                    FileName = "DSdoituongtrung.xlsx",
                    AddExtension = true,
                    Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
                };
                sf.Title = "Chọn thư mục lưu file";
                if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    //XLWorkbook xLWorkbook = new XLWorkbook();
                    //xLWorkbook.AddWorksheet(ws);
                    try
                    {
                        wbook.SaveAs(sf.FileName);
                        //xLWorkbook.SaveAs(sf.FileName);
                        //xLWorkbook.Dispose();
                    }
                    catch
                    {
                        MSG.Error("Lỗi khi lưu");
                    }
                    if (MSG.Question("Tải xuống thành công, Bạn có muốn mở file vừa tải") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(sf.FileName);
                        }
                        catch
                        {
                        }

                    }
                }
            }
            
        }
    }
}
