﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class UCPurchasePrice : UserControl
    {
        public UCPurchasePrice(FMain @this)
        {
            InitializeComponent();
            btnPPInvoice1.Click += (s, e) => btnPPInvoice1_Click(s, e, @this);
            btnPPGetInvoices.Click += (s, e) => btnPPGetInvoices_Click(s, e, @this);
            btnPPService.Click += (s, e) => btnPPService_Click(s, e, @this);
            btnPPPayVendor.Click += (s, e) => btnPPPayVendor_Click(s, e, @this);
            btnPPDiscountReturn.Click += (s, e) => btnPPDiscountReturn_Click(s, e, @this);
            btnPPPayableSummary.Click += (s, e) => btnPPPayableSummary_Click(s, e, @this);
            btnPurchaseDetailSupplerInventoryItem.Click += (s, e) => btnPurchaseDetailSupplerInventoryItem_Click(s, e, @this);
        }

        private void btnPPInvoice1_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFPPInvoice1();
        }

        private void btnPPGetInvoices_Click(object sender, EventArgs e,FMain fMain)
        {
            fMain.ShowFPPGetInvoices();
        }

        private void btnPPService_Click(object sender, EventArgs e,FMain fMain)
        {
            fMain.ShowFPPService();
        }

        private void btnPPPayVendor_Click(object sender, EventArgs e,FMain fMain)
        {
            fMain.ShowFPPPayVendor();
        }

        private void btnPPDiscountReturn_Click(object sender, EventArgs e,FMain fMain)
        {
            fMain.ShowFPPDiscountReturn();
        }

        private void btnPPPayableSummary_Click(object sender, EventArgs e,FMain fMain)
        {
            new Accounting.Frm.FReport.FRTONGCONGNOPHAITRA().ShowFormDialog(this);
        }

        private void btnPurchaseDetailSupplerInventoryItem_Click(object sender, EventArgs e,FMain fMain)
        {
            new Accounting.Frm.FReport.FRSOCHITIETMUAHANG().ShowFormDialog(this);
        }
    }
}
