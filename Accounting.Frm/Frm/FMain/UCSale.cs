﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class UCSale : UserControl
    {
        public UCSale(FMain @this)
        {
            InitializeComponent();
            btnSAInvoicePayment0.Click += (s, e) => btnSAInvoicePayment0_Click(s, e, @this);
            btnSALiabilitiesReport.Click += (s, e) => btnSALiabilitiesReport_Click(s, e, @this);
            btnSAReceiptCustomer.Click += (s, e) => btnSAReceiptCustomer_Click(s, e, @this);
            btnSAInvoicePayment1.Click += (s, e) => btnSAInvoicePayment1_Click(s, e, @this);
            btnS17DNN.Click += (s, e) => btnS17DNN_Click(s, e, @this);
            btnReceiptableSummary.Click += (s, e) => btnReceiptableSummary_Click(s, e, @this);
        }

        private void btnSAInvoicePayment0_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFSAInvoicePayment0();
        }

        private void btnSALiabilitiesReport_Click(object sender, EventArgs e,FMain fMain)
        {
            fMain.ShowFSALiabilitiesReport();
        }

        private void btnSAReceiptCustomer_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFSAReceiptCustomer();
        }

        private void btnSAInvoicePayment1_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFSAInvoicePayment1();
        }

        private void btnS17DNN_Click(object sender, EventArgs e, FMain fMain)
        {
            new Accounting.Frm.FReport.FRSoChiTietBanHang().ShowFormDialog(this);
        }

        private void btnReceiptableSummary_Click(object sender, EventArgs e, FMain fMain)
        {
            new Accounting.Frm.FReport.FRTHCONGNOPHAITHU().ShowFormDialog(this);
        }
    }
}
