﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class UCFixedAsset : UserControl
    {
        public UCFixedAsset(FMain @this)
        {
            InitializeComponent();
            btnFixedAsset.Click += (s, e) => btnFixedAsset_Click(s, e, @this);
            btnFABuyAndIncrement.Click += (s, e) => btnFABuyAndIncrement_Click(s, e, @this);
            btnFADecrement.Click += (s, e) => btnFADecrement_Click(s, e, @this);
            btnFADepreciation.Click += (s, e) => btnFADepreciation_Click(s, e, @this);
            btnRS12SDNN.Click += (s, e) => btnRS12SDNN_Click(s, e, @this);
            btnFAAdjustment.Click += (s, e) => btnFAAdjustment_Click(s, e, @this);
        }

        private void btnFixedAsset_Click(object sender, EventArgs e,FMain fMain)
        {
            fMain.ShowFFixedAsset();
        }

        private void btnFABuyAndIncrement_Click(object sender, EventArgs e,FMain fMain)
        {
            fMain.ShowFFABuyAndIncrement();
        }

        private void btnFADecrement_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFFADecrement();
        }

        private void btnFADepreciation_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFFADepreciation();
        }

        private void btnRS12SDNN_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFRS12SDNN();
        }

        private void btnFAAdjustment_Click(object sender, EventArgs e, FMain fMain)
        {
            fMain.ShowFFAAdjustment();
        }
    }
}
