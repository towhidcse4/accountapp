﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Accounting.Core.Domain;
using Accounting.Core.DAO;
using Accounting.Core.Domain.obj.Report;

namespace Accounting.Frm.FReport
{
    public partial class FRTongHopCongNoNhanVien : Form
    {
        List<AccountingObjectReport> _lstAccountingObjectReport = new List<AccountingObjectReport>();
        //readonly List<AccountReport> _lstAccountPayReport = ReportUtils.LstAccountPayReport;
        private readonly List<AccountingObjectReport> _lstnew = ReportUtils.LstEmployee;
        private readonly ICurrencyService _ICurrencyService;
        private string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRTongHopCongNoNhanVien()
        {
            InitializeComponent();

            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot2.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\TongHopCongNoNhanVien.rst", path);

            foreach (var item in _lstnew) { item.Check = false; }
            _lstAccountingObjectReport = _lstnew.OrderBy(n=>n.AccountingObjectCode).OrderBy(n => n.DepartmentName).ToList();
            
            ugridEmployee.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
            ReportUtils.ProcessControls(this);

            //load data to cbbFromAccount
            var listacount = Utils.ListAccount.Where(n => n.DetailType.Equals("2")).OrderBy(n=>n.AccountNumber).ToList();
            listacount.Insert(0, new Account { AccountNumber = "Tất cả", AccountName = "Tất cả" });//AccountNumber = all -> Tất cả
            cbbFromAccount.DataSource = listacount;
            cbbFromAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbFromAccount, ConstDatabase.Account_TableName);
            cbbFromAccount.SelectedRow = cbbFromAccount.Rows[0];
            #region Đếm số dòng
            Infragistics.Win.UltraWinGrid.UltraGridBand band = ugridEmployee.DisplayLayout.Bands[0];
            ugridEmployee.DisplayLayout.Override.AllowRowSummaries = Infragistics.Win.UltraWinGrid.AllowRowSummaries.
            Default; //Ẩn ký hiệu tổng trên Header Caption
            if (band.Summaries.Count != 0) band.Summaries.Clear();
            Infragistics.Win.UltraWinGrid.SummarySettings summary = band.Summaries.Add("Count", Infragistics.Win.UltraWinGrid.SummaryType.Count, band.Columns["Check"]);
            summary.DisplayFormat = "Số dòng dữ liệu: {0:N0}";
            summary.SummaryPosition = Infragistics.Win.UltraWinGrid.SummaryPosition.Left;
            ugridEmployee.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;   //ẩn 
            summary.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.BottomFixed | Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.GroupByRowsFooter;
            ugridEmployee.DisplayLayout.Override.GroupBySummaryDisplayStyle = Infragistics.Win.UltraWinGrid.GroupBySummaryDisplayStyle.SummaryCells;
            ugridEmployee.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
            ugridEmployee.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            ugridEmployee.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Header.Caption = "Mã nhân viên";
            ugridEmployee.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Caption = "Tên nhân viên";
            #endregion

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            string acount = cbbFromAccount.Value != null ? cbbFromAccount.Value.ToString() : string.Empty;
            if (string.IsNullOrEmpty(acount))
            {
                MSG.Warning("Bạn chưa chọn tài khoản.");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            string list_acount = "";

            try
            {
                var dsacc = _lstAccountingObjectReport != null ? _lstAccountingObjectReport.Where(c => c.Check).Select(c => c.ID.ToString()) : new List<string>();
                list_acount = string.Join(",", dsacc.ToArray());
            }
            catch (Exception ex)
            {
                list_acount = "";
            }

            if (list_acount == "")
            {
                MSG.Warning("Bạn chưa chọn đối tượng");
                return;
            }
            try
            {            
                FRTONGHOPCONGNONVex fm = new FRTONGHOPCONGNONVex((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, acount, list_acount);
                fm.Show(this);                
            }
            catch (Exception ex)
            {

            }
            
        }

        
        private void btnExit_Click(object sender, EventArgs e)
        {
            foreach (var item in ugridEmployee.Rows)
            {
                item.Cells["Check"].Value = false;
            }
            Close();
        }

        private void reportManager1_GetReportParameter(object sender, PerpetuumSoft.Reporting.Components.GetReportParameterEventArgs e)
        {
            //string typeCurrency = cbbCurrencyType.Value != null ? cbbCurrencyType.Value.ToString() : string.Empty;
            //e.Parameters["CurrencyID"].Value = typeCurrency;
        }
    }
}
