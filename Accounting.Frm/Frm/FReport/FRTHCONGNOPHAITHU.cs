﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRTHCONGNOPHAITHU : Form
    {
        List<AccountingObjectReport> _lstAccountingObjectReport = new List<AccountingObjectReport>();
        readonly List<AccountReport> _lstAccountPayReport = ReportUtils.LstAccountPayReport;
        private readonly List<AccountingObjectReport> _lstnew = ReportUtils.LstCustomers.Where(t => t.ObjectType == 0 || t.ObjectType == 2).ToList();
        private readonly IAccountService _IAccountService;
        private string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRTHCONGNOPHAITHU()
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\TongHopCongNoPhaiThu.rst", path);
            _IAccountService = IoC.Resolve<IAccountService>();
            foreach(var item in _lstnew) { item.Check = false; }
            _lstAccountingObjectReport = _lstnew;
            ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
            cbbFromAccount.DataSource = _lstAccountPayReport;
            cbbFromAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbFromAccount, ConstDatabase.Account_TableName);
            ReportUtils.ProcessControls(this);
            foreach (var item in cbbCurrency.Rows)
            {
                if (((Currency)item.ListObject).ID == "VND")
                {
                    cbbCurrency.SelectedRow = item;
                    break;
                }
                cbbCurrency.SelectedRow = cbbCurrency.Rows[0];
            }
            ConfigCombocbbNhomKH(cbbNhomKH);
            cbbNhomKH.ValueChanged += cbbAccountingObjectGroupID_ValueChanged;
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }
        }       
        private static void ConfigCombocbbNhomKH(UltraCombo cbb)
        {
            var accountingObjectGroupService = IoC.Resolve<IAccountingObjectGroupService>();
            var accountingObjectGroups = new List<AccountingObjectGroup>();
            AccountingObjectGroup item = new AccountingObjectGroup
            {
                AccountingObjectGroupCode = "Khác",
                AccountingObjectGroupName = "Khác"
            };
            AccountingObjectGroup itemTC = new AccountingObjectGroup
            {
                AccountingObjectGroupCode = "Tất cả",//edit by cuongpv 20190419 TC -> Tất cả
                AccountingObjectGroupName = "Tất cả"
            };
            accountingObjectGroups.Add(itemTC);
            accountingObjectGroups.Add(item);
            accountingObjectGroups.AddRange(accountingObjectGroupService.GetListByIsActiveOrderCode(true));
            cbb.DataSource = accountingObjectGroups;
            cbb.DisplayMember = "AccountingObjectGroupName";
            Utils.ConfigGrid(cbb, ConstDatabase.AccountingObjectGroup_TableName);
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            ReportProcedureSDS sp = new ReportProcedureSDS();

            string acount = cbbFromAccount.Value == null ? string.Empty : cbbFromAccount.Value.ToString();

            if (acount == null || acount == string.Empty)
            {
                MSG.Warning("Bạn chưa chọn tài khoản");
                return;
            }

            string list_acount = "";

            try
            {
                var dsacc = _lstAccountingObjectReport != null ? _lstAccountingObjectReport.Where(c => c.Check).Select(c => c.ID.ToString()) : new List<string>();
                list_acount = string.Join(",", dsacc.ToArray());
            }
            catch (Exception ex)
            {
                list_acount = "";
            }

            FTONGHOPCONGNOPHAITHUex fm = new FTONGHOPCONGNOPHAITHUex((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, cbbCurrency.Value.ToString(), acount, list_acount);
            fm.Show(this);
            
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        private bool _check1 = false;
        private void cbbAccountingObjectGroupID_ValueChanged(object sender, EventArgs e)
        {
            _check1 = ((AccountingObjectGroup)cbbNhomKH.SelectedRow.ListObject).AccountingObjectGroupCode != "Tất cả";//edit by cuongpv 20190419 TC -> Tất cả
            Filter();
        }
        private void Filter()
        {
            _lstAccountingObjectReport = _lstnew;
            AccountingObjectGroup accountingObjectGroup;
            if (_check1)
            {
                if (((AccountingObjectGroup)cbbNhomKH.SelectedRow.ListObject).AccountingObjectGroupCode == "Khác")
                {
                    _lstAccountingObjectReport = _lstAccountingObjectReport.Where(c => c.AccountObjectGroupID == Guid.Empty).ToList();
                }
                else
                {
                    accountingObjectGroup = (AccountingObjectGroup)cbbNhomKH.SelectedRow.ListObject;
                    _lstAccountingObjectReport = _lstAccountingObjectReport.Where(c => c.AccountObjectGroupID == accountingObjectGroup.ID).ToList();
                }
            }
            ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
        }

        private void reportManager1_GetReportParameter(object sender, PerpetuumSoft.Reporting.Components.GetReportParameterEventArgs e)
        {
            string typeCurrency = cbbCurrency.Value != null ? cbbCurrency.Value.ToString() : string.Empty;
            e.Parameters["CurrencyID"].Value = typeCurrency;
        }
    }
}
