﻿namespace Accounting.Frm.FReport
{
    partial class FRSOCHITIETTIENVAY
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.lblBeginDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblEndDate = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDateTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dtBeginDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.ugridAccountingObject = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ugridAccount = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.btnOk = new Infragistics.Win.Misc.UltraButton();
            this.btnExit = new Infragistics.Win.Misc.UltraButton();
            this.reportManager1 = new PerpetuumSoft.Reporting.Components.ReportManager(this.components);
            this.fileReportSlot1 = new PerpetuumSoft.Reporting.Components.FileReportSlot(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ugridAccountingObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugridAccount)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ultraGroupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.ultraGroupBox2);
            this.splitContainer1.Size = new System.Drawing.Size(764, 629);
            this.splitContainer1.SplitterDistance = 571;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 0;
            this.splitContainer1.TabStop = false;
            // 
            // ultraGroupBox1
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox1.Appearance = appearance1;
            this.ultraGroupBox1.Controls.Add(this.splitContainer2);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(764, 571);
            this.ultraGroupBox1.TabIndex = 2;
            this.ultraGroupBox1.Text = "Tham số";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 19);
            this.splitContainer2.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.ultraLabel1);
            this.splitContainer2.Panel1.Controls.Add(this.lblBeginDate);
            this.splitContainer2.Panel1.Controls.Add(this.lblEndDate);
            this.splitContainer2.Panel1.Controls.Add(this.cbbDateTime);
            this.splitContainer2.Panel1.Controls.Add(this.dtBeginDate);
            this.splitContainer2.Panel1.Controls.Add(this.dtEndDate);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(758, 549);
            this.splitContainer2.SplitterDistance = 43;
            this.splitContainer2.SplitterWidth = 1;
            this.splitContainer2.TabIndex = 0;
            this.splitContainer2.TabStop = false;
            // 
            // ultraLabel1
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextHAlignAsString = "Left";
            appearance2.TextVAlignAsString = "Bottom";
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Location = new System.Drawing.Point(9, 6);
            this.ultraLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(89, 28);
            this.ultraLabel1.TabIndex = 49;
            this.ultraLabel1.Text = "Kỳ báo cáo";
            // 
            // lblBeginDate
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextHAlignAsString = "Left";
            appearance3.TextVAlignAsString = "Bottom";
            this.lblBeginDate.Appearance = appearance3;
            this.lblBeginDate.Location = new System.Drawing.Point(339, 9);
            this.lblBeginDate.Margin = new System.Windows.Forms.Padding(4);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.Size = new System.Drawing.Size(72, 26);
            this.lblBeginDate.TabIndex = 44;
            this.lblBeginDate.Text = "Từ ngày";
            // 
            // lblEndDate
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextHAlignAsString = "Left";
            appearance4.TextVAlignAsString = "Bottom";
            this.lblEndDate.Appearance = appearance4;
            this.lblEndDate.Location = new System.Drawing.Point(557, 9);
            this.lblEndDate.Margin = new System.Windows.Forms.Padding(4);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(71, 26);
            this.lblEndDate.TabIndex = 45;
            this.lblEndDate.Text = "Đến ngày";
            // 
            // cbbDateTime
            // 
            this.cbbDateTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDateTime.Location = new System.Drawing.Point(107, 9);
            this.cbbDateTime.Margin = new System.Windows.Forms.Padding(4);
            this.cbbDateTime.Name = "cbbDateTime";
            this.cbbDateTime.NullText = "<chọn dữ liệu>";
            this.cbbDateTime.Size = new System.Drawing.Size(224, 25);
            this.cbbDateTime.TabIndex = 1;
            // 
            // dtBeginDate
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.dtBeginDate.Appearance = appearance5;
            this.dtBeginDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtBeginDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dtBeginDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtBeginDate.Location = new System.Drawing.Point(416, 9);
            this.dtBeginDate.Margin = new System.Windows.Forms.Padding(4);
            this.dtBeginDate.MaskInput = "";
            this.dtBeginDate.Name = "dtBeginDate";
            this.dtBeginDate.Size = new System.Drawing.Size(124, 24);
            this.dtBeginDate.TabIndex = 2;
            this.dtBeginDate.Value = null;
            // 
            // dtEndDate
            // 
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.dtEndDate.Appearance = appearance6;
            this.dtEndDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtEndDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dtEndDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtEndDate.Location = new System.Drawing.Point(637, 10);
            this.dtEndDate.Margin = new System.Windows.Forms.Padding(4);
            this.dtEndDate.MaskInput = "";
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.Size = new System.Drawing.Size(109, 24);
            this.dtEndDate.TabIndex = 3;
            this.dtEndDate.Value = null;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.ugridAccount);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.ugridAccountingObject);
            this.splitContainer3.Size = new System.Drawing.Size(758, 505);
            this.splitContainer3.SplitterDistance = 246;
            this.splitContainer3.SplitterWidth = 1;
            this.splitContainer3.TabIndex = 53;
            //this.splitContainer3.TabStop = false;
            // 
            // ugridAccountingObject
            // 
            this.ugridAccountingObject.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ugridAccountingObject.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.ugridAccountingObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ugridAccountingObject.Location = new System.Drawing.Point(0, 0);
            this.ugridAccountingObject.Margin = new System.Windows.Forms.Padding(4);
            this.ugridAccountingObject.Name = "ugridAccountingObject";
            this.ugridAccountingObject.Size = new System.Drawing.Size(758, 258);
            this.ugridAccountingObject.TabIndex = 5;
            this.ugridAccountingObject.TabStop = false;
            this.ugridAccountingObject.Text = "Danh sách nhà cung cấp";
            this.ugridAccountingObject.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // ugridAccount
            // 
            this.ugridAccount.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ugridAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ugridAccount.Location = new System.Drawing.Point(0, 0);
            this.ugridAccount.Margin = new System.Windows.Forms.Padding(4);
            this.ugridAccount.Name = "ugridAccount";
            this.ugridAccount.Size = new System.Drawing.Size(758, 246);
            this.ugridAccount.TabIndex = 4;
            this.ugridAccount.TabStop = false;
            this.ugridAccount.Text = "Danh sách tài khoản";
            this.ugridAccount.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // ultraGroupBox2
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox2.Appearance = appearance7;
            this.ultraGroupBox2.Controls.Add(this.btnOk);
            this.ultraGroupBox2.Controls.Add(this.btnExit);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(764, 53);
            this.ultraGroupBox2.TabIndex = 50;
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnOk.Appearance = appearance8;
            this.btnOk.Location = new System.Drawing.Point(543, 10);
            this.btnOk.Margin = new System.Windows.Forms.Padding(4);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(100, 28);
            this.btnOk.TabIndex = 51;
            this.btnOk.Text = "Đồng ý";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnExit.Appearance = appearance9;
            this.btnExit.Location = new System.Drawing.Point(651, 10);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(100, 28);
            this.btnExit.TabIndex = 52;
            this.btnExit.Text = "Hủy bỏ";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // reportManager1
            // 
            this.reportManager1.DataSources = new PerpetuumSoft.Reporting.Components.ObjectPointerCollection(new string[0], new object[0]);
            this.reportManager1.OwnerForm = this;
            this.reportManager1.Reports.AddRange(new PerpetuumSoft.Reporting.Components.ReportSlot[] {
            this.fileReportSlot1});
            // 
            // fileReportSlot1
            // 
            this.fileReportSlot1.FilePath = "";
            this.fileReportSlot1.ReportName = "";
            this.fileReportSlot1.ReportScriptType = typeof(PerpetuumSoft.Reporting.Rendering.ReportScriptBase);
            // 
            // FRSOCHITIETTIENVAY
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 629);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FRSOCHITIETTIENVAY";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "S15-DNN: Sổ chi tiết tiền vay";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_FormClosed);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox2.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).EndInit();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ugridAccountingObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugridAccount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel lblBeginDate;
        private Infragistics.Win.Misc.UltraLabel lblEndDate;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDateTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtBeginDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtEndDate;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugridAccountingObject;
        private PerpetuumSoft.Reporting.Components.ReportManager reportManager1;
        private PerpetuumSoft.Reporting.Components.FileReportSlot fileReportSlot1;
        private Infragistics.Win.Misc.UltraButton btnOk;
        private Infragistics.Win.Misc.UltraButton btnExit;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugridAccount;
    }
}