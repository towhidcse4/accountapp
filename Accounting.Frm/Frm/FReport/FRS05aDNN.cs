﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Accounting.Core.Domain;

namespace Accounting
{
    public partial class FRS05aDNN : CustormForm
    {
        readonly IGeneralLedgerService _glSrv = IoC.Resolve<IGeneralLedgerService>();
        readonly List<AccountReport> _lstAccountReport = ReportUtils.LstAccountCashrReport;
        string _subSystemCode;
        public FRS05aDNN(string subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\S05a-DNN.rst", path);
            ugridAccount.SetDataBinding(_lstAccountReport.ToList(), "");
            ReportUtils.ProcessControls(this);

            btnExit.Click += btnExit_Click;
            btnOk.Click += btnOk_Click;
            WaitingFrm.StopWaiting();
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }
            if (!_lstAccountReport.Any(c => c.Check))
            {
                MSG.Error(resSystem.Report_01);
                return;
            }
            List<string> lstGuids = _lstAccountReport.Where(c => c.Check).Select(c => c.AccountNumber).ToList();
            string idCurentcy = ((Currency)cbbCurrency.SelectedRow.ListObject).ID;
            List<S05aDNN> data = _glSrv.ReportS05aDNN((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, idCurentcy, lstGuids);
            var rD = new S05aDNNDetail();
            rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
            var f = new ReportForm<S05aDNN>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("S05aDNN", data, true);
            f.AddDatasource("Detail", rD);
            f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }
    }
}
