﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRCHITIETCONGNOPHAITHUKHACHHANG : Form
    {
        List<AccountingObjectReport> _lstAccountingObjectReport = new List<AccountingObjectReport>();
        readonly List<AccountReport> _lstAccountPayReport = ReportUtils.LstAccountPayReport;
        private readonly List<AccountingObjectReport> _lstnew = ReportUtils.LstCustomers;
        private readonly IAccountService _IAccountService;
        private string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRCHITIETCONGNOPHAITHUKHACHHANG()
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\ChiTietCongNoPhaiThuKH.rst", path);
            _IAccountService = IoC.Resolve<IAccountService>();
            foreach (var item in _lstnew) { item.Check = false; }
            _lstAccountingObjectReport = _lstnew;
            ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
            cbbFromAccount.DataSource = _lstAccountPayReport;
            cbbFromAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbFromAccount, ConstDatabase.Account_TableName);
            ReportUtils.ProcessControls(this);
            cbbAccountingObjectGroupID.ValueChanged += cbbAccountingObjectGroupID_ValueChanged;
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }
        }
      
        private bool _check1 = false;
        private void cbbAccountingObjectGroupID_ValueChanged(object sender, EventArgs e)
        {
            _check1 = ((AccountingObjectGroup)cbbAccountingObjectGroupID.SelectedRow.ListObject).AccountingObjectGroupCode != "TC";
            Filter();
        }
        private void Filter()
        {
            _lstAccountingObjectReport = _lstnew;
            AccountingObjectGroup accountingObjectGroup;
            if (_check1)
            {
                accountingObjectGroup = (AccountingObjectGroup)cbbAccountingObjectGroupID.SelectedRow.ListObject;
                _lstAccountingObjectReport = _lstAccountingObjectReport.Where(c => c.AccountObjectGroupID == accountingObjectGroup.ID).ToList();
            }
            ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            ReportProcedureSDS sp = new ReportProcedureSDS();
            string acount = cbbFromAccount.Value != null ? cbbFromAccount.Value.ToString() : string.Empty;

            if(acount == null || acount == string.Empty)
            {
                MSG.Warning("Tài khoản không được để trống");
                return;
            }

            string list_acount = "";

            try
            {
                var dsacc = _lstAccountingObjectReport != null ? _lstAccountingObjectReport.Where(c => c.Check).Select(c => c.ID.ToString()) : new List<string>();
                list_acount = string.Join(",", dsacc.ToArray());
            }
            catch (Exception ex)
            {
                list_acount = "";
            }

            //add by cuongpv
            string currencyID = cbbCurrency.Value != null ? cbbCurrency.Value.ToString() : string.Empty;
            if (string.IsNullOrEmpty(currencyID))
            {
                MSG.Warning("Bạn chưa chọn loại tiền, xin kiểm tra lại.");
                return;
            }
            //end add by cuongpv
            FCHITIETCONGNOPHAITHUKHex frm = new FCHITIETCONGNOPHAITHUKHex((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, currencyID, acount, list_acount);
            frm.Show(this);
        }

        private void cbbAccountingObjectGroupID_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {

        }
        private Decimal Covert_Money(decimal gt)
        {
            Decimal ketqua = 0;
            string dv = gt.ToString().Split(',')[0];
            gt = Convert.ToDecimal(dv);
            string s = "n";
            if (gt < 0)
            {
                string gt_am = String.Format("{0:" + Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND) + ";" + Utils.GetFormatNegative().Replace("n", Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND)) + "}", Convert.ToDecimal(gt));
                gt_am = gt_am.Replace('(', '-').Replace(")", string.Empty);
                ketqua = Convert.ToDecimal(gt_am);
            }
            else
            {
                string kq = String.Format("{0:" + Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND) + ";" + s.Replace("n", Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND)) + "}", Convert.ToDecimal(gt));
                ketqua = Convert.ToDecimal(kq);
            }

            return ketqua;
        }
    }
}
