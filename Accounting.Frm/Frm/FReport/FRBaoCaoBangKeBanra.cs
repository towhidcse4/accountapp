﻿using Accounting.Core.DAO;
using Accounting.Core.Domain.obj.Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accounting.TextMessage;
using Accounting.Core.IService;
using FX.Core;
using Accounting.Core.Domain;

namespace Accounting.Frm.FReport
{
    public partial class FRBaoCaoBangKeBanra : CustormForm
    {
        private string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRBaoCaoBangKeBanra()
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\BangKeHoaDonThueBanRa.rst", path);
            ReportUtils.ProcessControls(this);
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }

        private void btnOk_Click(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private string GetNameGroup(decimal ma)
        {
            string ten = "";
            switch (ma)
            {
                case -2:
                    ten = "Hàng hoá, dịch vụ không tính thuế GTGT";
                    break;
                case -1:
                    ten = "Hàng hoá, dịch vụ không chịu thuế GTGT";
                    break;
                case 0:
                    ten = "Hàng hoá, dịch vụ chịu thuế GTGT 0%";
                    break;
                case 5:
                    ten = "Hàng hoá, dịch vụ chịu thuế GTGT 5%";
                    break;
                case 10:
                    ten = "Hàng hoá, dịch vụ chịu thuế GTGT 10%";
                    break;
                default:
                    break;
            }
            return ten;
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            ReportProcedureSDS sp = new ReportProcedureSDS();
            List<BangKeBanRa_Model> data = new List<BangKeBanRa_Model>();
            int check = cbb_CongGop.Checked == true ? 1 : 0;
            data = sp.GetProc_GetBkeInv_Sale_QT((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, check, 0, "");
            data.ForEach(
                t =>
                {
                    t.VATRATENAME = GetNameGroup(t.VATRATE);
                    if (t.FLAG == -1)
                    {
                        t.DOANH_SO = -t.DOANH_SO;
                        t.THUE_GTGT = -t.THUE_GTGT;
                    }
                });
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo!");
            }
            else
            {
                var rD = new BangKeBanRa_ModelDetail();
                rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
               
               data=data.OrderByDescending(n => n.VATRATE).ThenBy(n => n.NGAY_HD).ThenBy(n => n.SO_HD).ToList();
                var f = new FRBaoCaoBangKeBanraTruocIn(data, rD.Period, check, (DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }
    }
}
