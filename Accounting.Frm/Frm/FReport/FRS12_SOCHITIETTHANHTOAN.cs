﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRS12_SOCHITIETTHANHTOAN : Form
    {
        readonly List<Account> _lstAccount = new List<Account>();
        List<AccountingObjectReport> _lstAccountingObjectReport = new List<AccountingObjectReport>();
        IAccountService _IAccountService;
        IAccountingObjectService _IAccountingObjectService;
        readonly List<AccountReport> _lstAccountReport = ReportUtils.LstAccountBankReport;
        private string _subSystemCode;
        private bool _check1 = false;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRS12_SOCHITIETTHANHTOAN()
        {
            InitializeComponent();
            _IAccountService = IoC.Resolve<IAccountService>();
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoChiTietThanhToan_S12.rst", path);
            _lstAccount = _IAccountService.GetListAccountAccountNumberIsActive("131;331", true);
            _lstAccountReport = _lstAccount.Select(c => new AccountReport()
            {
                AccountNumber = c.AccountNumber,
                AccountGroupID = c.AccountGroupID,
                AccountGroupKindView = c.AccountGroupKindView,
                AccountGroupKind = c.AccountGroupKind,
                AccountName = c.AccountName,
                AccountNameGlobal = c.AccountNameGlobal,
                IsActive = c.IsActive,
                Description = c.Description,
                DetailType = c.DetailType,
                ID = c.ID,
                ParentID = c.ParentID,
                IsParentNode = c.IsParentNode,
                Grade = c.Grade
            }).OrderBy(c => c.AccountNumber).ToList();

            ugridAccount.SetDataBinding(_lstAccountReport.ToList(), "");
            //_lstAccountingObjectReport = _IAccountingObjectService.GetAccountingObjects(3, true).Select(c => new AccountingObjectReport()
            //{
            //    Check = false,
            //    ID = c.ID,
            //    AccountingObjectCode = c.AccountingObjectCode,
            //    AccountingObjectName = c.AccountingObjectName,
            //    Address = c.Address,
            //    ObjectType = c.ObjectType ?? 0,
            //    AccountingObjectCategory = c.AccountingObjectCategory,
            //    AccountObjectGroupID = c.AccountObjectGroupID ?? Guid.Empty

            //}).ToList();
            //ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
            Filter();
            ReportUtils.ProcessControls(this);
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

            WaitingFrm.StopWaiting();
        }

        private void Filter()
        {
            _lstAccountingObjectReport = _IAccountingObjectService.GetAccountingObjects(3, true).Select(c => new AccountingObjectReport()
            {
                Check = false,
                ID = c.ID,
                AccountingObjectCode = c.AccountingObjectCode,
                AccountingObjectName = c.AccountingObjectName,
                Address = c.Address,
                ObjectType = c.ObjectType ?? 0,
                AccountingObjectCategory = c.AccountingObjectCategory,
                AccountObjectGroupID = c.AccountObjectGroupID ?? Guid.Empty
            }).ToList();
            ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
      
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
          
        }

        private void btnOk_Click_1(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            string listaccount = "";
            string listKH = "";
            string loaitien = cbbCurrency.Value.ToString();
            List<SA_GetDetailPayS12> list = new List<SA_GetDetailPayS12>();
            List<SA_GetDetailPayS12> list2 = new List<SA_GetDetailPayS12>();
            var list_nhacc = _lstAccountingObjectReport.Where(t => t.Check);
            if (list_nhacc.Count() == 0)
            {
                MSG.Warning("Bạn chưa chọn đối tượng");
                return;
            }
            var dsnhacc = list_nhacc.Select(c => c.ID.ToString());
            var list_account = _lstAccountReport.Where(t => t.Check);
            if (list_account.Count() == 0)
            {
                MSG.Warning("Bạn chưa chọn tài khoản");
                return;
            }
            var dsaccount = list_account.Select(c => c.AccountNumber.ToString());
            listaccount = "," + string.Join(",", dsaccount.ToArray()) + ",";
            listKH = "," + string.Join(",", dsnhacc.ToArray()) + ",";
            ReportProcedureSDS sp = new ReportProcedureSDS();
            list = sp.Get_SoChiTietThanhToan((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, listaccount, loaitien, listKH);
            if (list.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo");
            }
            else
            {
                foreach (var item in list)
                {
                    if (item.ngayCTu != null)
                    {
                        if (item.DuNo > item.DuCo)
                        {
                            item.DuCo = 0;
                        }
                        else if (item.DuNo < item.DuCo)
                        {
                            item.DuNo = 0;
                        }
                    }
                    var tk = list_nhacc.FirstOrDefault(t => t.ID == item.AccountObjectID);
                    item.AccountingObjectCode = tk.AccountingObjectCode;
                    item.AccountingObjectName = tk.AccountingObjectName;
                    var kh = list_account.FirstOrDefault(t => t.AccountNumber == item.AccountNumber);
                    item.AccountName = kh.AccountName;
                }

                foreach (var item in list_account)
                {
                    if (list.Any(t => t.AccountNumber == item.AccountNumber))
                    {
                        SA_GetDetailPayS12 it = new SA_GetDetailPayS12();
                        it.AccountNumber = item.AccountNumber;
                        it.AccountName = item.AccountName;
                        list2.Add(it);
                        var list_kh = list.Where(t => t.AccountNumber == item.AccountNumber).Select(m => m.AccountingObjectCode).Distinct().ToList();
                        foreach (var item1 in list_kh)
                        {
                            var list1 = list.Where(t => t.AccountingObjectCode == item1 && t.AccountNumber == item.AccountNumber).ToList();/*edit by cuongpv add: && t.AccountNumber == item.AccountNumber*/
                            list1[list1.Count() - 1].Sum_DuNo = list1[list1.Count() - 1].DuNo;
                            list1[list1.Count() - 1].Sum_DuCo = list1[list1.Count() - 1].DuCo;
                        }
                    }
                }
                var rD = new SA_GetDetailPayS12_Detail();
                rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                rD.LoaiTien = "Loại tiền: " + loaitien;
                var f = new ReportForm<SA_GetDetailPayS12>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBCHITIET", list);
                f.AddDatasource("DBTAIKHOAN", list2);
                f.AddDatasource("Detail", rD);
                f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void reportManager1_GetReportParameter(object sender, PerpetuumSoft.Reporting.Components.GetReportParameterEventArgs e)
        {
            string typeCurrency = cbbCurrency.Value != null ? cbbCurrency.Value.ToString() : string.Empty;
            e.Parameters["CurrencyID"].Value = typeCurrency;
        }
    }
}
