﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accounting.Frm.FReport.Tools;
using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Accounting.Frm.FReport
{
    public partial class FRB09DNNDetail : CustormForm
    {
        private B09DNN b09DNN;
        public FRB09DNNDetail(DateTime startDate, DateTime endDate)
        {
            InitializeComponent();
            b09DNN = new B09DNN();
            b09DNN.Period = ReportUtils.GetPeriod(startDate, endDate);

            Sec_II_1.Text += String.Format(" Từ ngày {0} Đến ngày {1}", startDate.ToString("dd/MM/yyyy"), endDate.ToString("dd/MM/yyyy"));

            SectorVData(startDate, endDate);
            Block_VI_Data(startDate, endDate);

            fillSectorVData();
        }

        private void SectorVData(DateTime startDate, DateTime endDate)
        {
            try
            {
                ReportProcedureSDS sp = new ReportProcedureSDS();
                List<BalanceAccountF01> data = new List<BalanceAccountF01>();
                data = sp.GetBalanceAccountF01(startDate, endDate, 2, true);
                if (data.Count == 0)
                {
                    MSG.Warning("Không có dữ liệu để lên báo cáo!");
                }
                else
                {
                    // V.1
                    if (data.Any(o => o.AccountNumber == "111"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "111");
                        b09DNN.b09Num.V_1_1_1 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_1_2_1 = (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "112"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "112");
                        b09DNN.b09Num.V_1_1_2 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_1_2_2 = (decimal)row.OpeningDebitAmount;
                    }

                    // V.2
                    if (data.Any(o => o.AccountNumber == "121"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "121");
                        b09DNN.b09Num.V_2_a_1 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_2_a_2 = (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "1281"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "1281");
                        b09DNN.b09Num.V_2_b_1_1 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_2_b_2_1 = (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "1288"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "1288");
                        b09DNN.b09Num.V_2_b_1_2 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_2_b_2_2 = (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "2291"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "2291");
                        b09DNN.b09Num.V_2_c_1_1 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_2_c_2_1 = (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "2292"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "2292");
                        b09DNN.b09Num.V_2_c_1_2 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_2_c_2_2 = (decimal)row.OpeningCreditAmount;
                    }

                    // V.3
                    if (data.Any(o => o.AccountNumber == "131"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "131");
                        b09DNN.b09Num.V_3_a_1 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_3_a_2 = (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "331"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "331");
                        b09DNN.b09Num.V_3_b_1 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_3_b_2 = (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "141"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "141");
                        b09DNN.b09Num.V_3_c_1_2 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_3_c_2_2 = (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "1368"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "1368");
                        b09DNN.b09Num.V_3_c_1_3 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_3_c_2_3 = (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "334"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "334");
                        b09DNN.b09Num.V_3_c_1_4 = (b09DNN.b09Num.V_3_c_1_4 ) + (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_3_c_2_4 = (b09DNN.b09Num.V_3_c_2_4 ) + (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "338"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "338");
                        b09DNN.b09Num.V_3_c_1_4 = (b09DNN.b09Num.V_3_c_1_4 ) + (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_3_c_2_4 = (b09DNN.b09Num.V_3_c_2_4 ) + (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "1386"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "1386");
                        b09DNN.b09Num.V_3_c_1_4 = (b09DNN.b09Num.V_3_c_1_4 ) + (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_3_c_2_4 = (b09DNN.b09Num.V_3_c_2_4 ) + (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "1388"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "1388");
                        b09DNN.b09Num.V_3_c_1_4 = (b09DNN.b09Num.V_3_c_1_4 ) + (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_3_c_2_4 = (b09DNN.b09Num.V_3_c_2_4 ) + (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "1381"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "1381");
                        b09DNN.b09Num.V_3_d_1 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_3_d_2 = (decimal)row.OpeningDebitAmount;
                    }

                    // V.4
                    if (data.Any(o => o.AccountNumber == "151"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "151");
                        b09DNN.b09Num.V_4_1_1 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_4_2_1 = (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "152"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "152");
                        b09DNN.b09Num.V_4_1_2 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_4_2_2 = (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "153"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "153");
                        b09DNN.b09Num.V_4_1_3 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_4_2_3 = (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "154"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "154");
                        b09DNN.b09Num.V_4_1_4 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_4_2_4 = (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "155"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "155");
                        b09DNN.b09Num.V_4_1_5 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_4_2_5 = (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "156"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "156");
                        b09DNN.b09Num.V_4_1_6 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_4_2_6 = (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "157"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "157");
                        b09DNN.b09Num.V_4_1_7 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_4_2_7 = (decimal)row.OpeningDebitAmount;
                    }

                    // V.5
                    if (data.Any(o => o.AccountNumber == "2111"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "2111");
                        b09DNN.b09Num.V_5_a_1_1 = (decimal)row.OpeningDebitAmount;
                        b09DNN.b09Num.V_5_a_2_1 = (decimal)row.DebitAmount;
                        b09DNN.b09Num.V_5_a_3_1 = (decimal)row.CreditAmount;
                        b09DNN.b09Num.V_5_a_4_1 = (decimal)row.ClosingDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "2141"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "2141");
                        b09DNN.b09Num.V_5_a_1_2 = (decimal)row.OpeningCreditAmount;
                        b09DNN.b09Num.V_5_a_2_2 = (decimal)row.CreditAmount; 
                        b09DNN.b09Num.V_5_a_3_2 = (decimal)row.DebitAmount;
                        b09DNN.b09Num.V_5_a_4_2 = (decimal)row.ClosingCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "2113"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "2113");
                        b09DNN.b09Num.V_5_b_1_1 = (decimal)row.OpeningDebitAmount;
                        b09DNN.b09Num.V_5_b_2_1 = (decimal)row.DebitAmount;
                        b09DNN.b09Num.V_5_b_3_1 = (decimal)row.CreditAmount;
                        b09DNN.b09Num.V_5_b_4_1 = (decimal)row.ClosingDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "2143"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "2143");
                        b09DNN.b09Num.V_5_b_1_2 = (decimal)row.OpeningCreditAmount;
                        b09DNN.b09Num.V_5_b_2_2 = (decimal)row.CreditAmount; 
                        b09DNN.b09Num.V_5_b_3_2 = (decimal)row.DebitAmount;
                        b09DNN.b09Num.V_5_b_4_2 = (decimal)row.ClosingCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "2112"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "2112");
                        b09DNN.b09Num.V_5_c_1_1 = (decimal)row.OpeningDebitAmount;
                        b09DNN.b09Num.V_5_c_2_1 = (decimal)row.DebitAmount;
                        b09DNN.b09Num.V_5_c_3_1 = (decimal)row.CreditAmount;
                        b09DNN.b09Num.V_5_c_4_1 = (decimal)row.ClosingDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "2142"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "2142");
                        b09DNN.b09Num.V_5_c_1_2 = (decimal)row.OpeningCreditAmount;
                        b09DNN.b09Num.V_5_c_2_2 = (decimal)row.CreditAmount; 
                        b09DNN.b09Num.V_5_c_3_2 = (decimal)row.DebitAmount;
                        b09DNN.b09Num.V_5_c_4_2 = (decimal)row.ClosingCreditAmount;
                    }

                    // V.6
                    if (data.Any(o => o.AccountNumber == "217"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "217");
                        b09DNN.b09Num.V_6_a_1_1 = (decimal)row.OpeningDebitAmount;
                        b09DNN.b09Num.V_6_a_2_1 = (decimal)row.DebitAmount; 
                        b09DNN.b09Num.V_6_a_3_1 = (decimal)row.CreditAmount;
                        b09DNN.b09Num.V_6_a_4_1 = (decimal)row.ClosingDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "2147"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "2147");
                        b09DNN.b09Num.V_6_a_1_1 = (decimal)row.OpeningCreditAmount;
                        b09DNN.b09Num.V_6_a_2_1 = (decimal)row.CreditAmount;
                        b09DNN.b09Num.V_6_a_3_1 = (decimal)row.DebitAmount;
                        b09DNN.b09Num.V_6_a_4_1 = (decimal)row.ClosingCreditAmount;
                    }

                    // V.7
                    if (data.Any(o => o.AccountNumber == "2411"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "2411");
                        b09DNN.b09Num.V_7_1_1 = (decimal)row.ClosingDebitAmount; 
                        b09DNN.b09Num.V_7_2_1 = (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "2412"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "2412");
                        b09DNN.b09Num.V_7_1_2 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_7_2_2 = (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "2413"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "2413");
                        b09DNN.b09Num.V_7_1_3 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_7_2_3 = (decimal)row.OpeningDebitAmount;
                    }

                    // V.8
                    if (data.Any(o => o.AccountNumber == "242"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "242");
                        b09DNN.b09Num.V_8_1_1 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_8_1_2 = (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "333"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "333");
                        b09DNN.b09Num.V_8_2_1 = (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_8_2_2 = (decimal)row.OpeningDebitAmount;
                    }

                    // V.9
                    if (data.Any(o => o.AccountNumber == "331"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "331");
                        b09DNN.b09Num.V_9_a_1 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_9_a_2 = (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "131"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "131");
                        b09DNN.b09Num.V_9_b_1 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_9_b_2 = (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "331"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "331");
                        b09DNN.b09Num.V_9_a_1 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_9_a_2 = (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "335"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "335");
                        b09DNN.b09Num.V_9_c_1_1 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_9_c_2_1 = (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "3368"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3368");
                        b09DNN.b09Num.V_9_c_1_2 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_9_c_2_2 = (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "3381"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3381");
                        b09DNN.b09Num.V_9_c_1_3_1 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_9_c_2_3_1 = (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "3382"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3382");
                        b09DNN.b09Num.V_9_c_1_3_2 = (b09DNN.b09Num.V_9_c_1_3_2 ) + (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_9_c_2_3_2 = (b09DNN.b09Num.V_9_c_2_3_2 ) + (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "3383"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3383");
                        b09DNN.b09Num.V_9_c_1_3_2 = (b09DNN.b09Num.V_9_c_1_3_2 ) + (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_9_c_2_3_2 = (b09DNN.b09Num.V_9_c_2_3_2 ) + (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "3384"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3384");
                        b09DNN.b09Num.V_9_c_1_3_2 = (b09DNN.b09Num.V_9_c_1_3_2 ) + (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_9_c_2_3_2 = (b09DNN.b09Num.V_9_c_2_3_2 ) + (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "3385"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3385");
                        b09DNN.b09Num.V_9_c_1_3_2 = (b09DNN.b09Num.V_9_c_1_3_2 ) + (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_9_c_2_3_2 = (b09DNN.b09Num.V_9_c_2_3_2 ) + (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "1388"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "1388");
                        b09DNN.b09Num.V_9_c_1_3_3 = (b09DNN.b09Num.V_9_c_1_3_3 ) + (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_9_c_2_3_3 = (b09DNN.b09Num.V_9_c_2_3_3 ) + (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "3399"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3388");
                        b09DNN.b09Num.V_9_c_1_3_3 = (b09DNN.b09Num.V_9_c_1_3_3 ) + (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_9_c_2_3_3 = (b09DNN.b09Num.V_9_c_2_3_3 ) + (decimal)row.OpeningCreditAmount;
                    }

                    // V.10
                    if (data.Any(o => o.AccountNumber == "3331"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3331");
                        b09DNN.b09Num.V_10_1_1 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_10_2_1 = (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "3332"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3332");
                        b09DNN.b09Num.V_10_1_2 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_10_2_2 = (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "3333"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3333");
                        b09DNN.b09Num.V_10_1_3 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_10_2_3 = (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "3334"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3334");
                        b09DNN.b09Num.V_10_1_4 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_10_2_4 = (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "3335"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3335");
                        b09DNN.b09Num.V_10_1_5 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_10_2_5 = (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "3336"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3336");
                        b09DNN.b09Num.V_10_1_6 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_10_2_6 = (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "3337"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3337");
                        b09DNN.b09Num.V_10_1_7 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_10_2_7 = (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "3338"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3338");
                        b09DNN.b09Num.V_10_1_8 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_10_2_8 = (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "3339"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3339");
                        b09DNN.b09Num.V_10_1_9 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_10_2_9 = (decimal)row.OpeningCreditAmount;
                    }

                    // V.11
                    if (data.Any(o => o.AccountNumber == "3411"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3411");
                        b09DNN.b09Num.V_11_a_1 = (decimal)row.ClosingCreditAmount - (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_11_a_2 = (decimal)row.CreditAmount;
                        b09DNN.b09Num.V_11_a_3 = (decimal)row.DebitAmount;
                        b09DNN.b09Num.V_11_a_4 = (decimal)row.OpeningCreditAmount - (decimal)row.OpeningDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "3412"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3412");
                        b09DNN.b09Num.V_11_c_1 = (decimal)row.ClosingCreditAmount - (decimal)row.ClosingDebitAmount;
                        b09DNN.b09Num.V_11_c_2 = (decimal)row.CreditAmount;
                        b09DNN.b09Num.V_11_c_3 = (decimal)row.DebitAmount;
                        b09DNN.b09Num.V_11_c_4 = (decimal)row.OpeningCreditAmount - (decimal)row.OpeningDebitAmount;
                    }

                    // V.12
                    if (data.Any(o => o.AccountNumber == "3521"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3521");
                        b09DNN.b09Num.V_12_1_1 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_12_2_1 = (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "3522"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3522");
                        b09DNN.b09Num.V_12_1_2 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_12_2_2 = (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "3524"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3524");
                        b09DNN.b09Num.V_12_1_3 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_12_2_3 = (decimal)row.OpeningCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "3524"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "3524");
                        b09DNN.b09Num.V_12_1_3 = (decimal)row.ClosingCreditAmount;
                        b09DNN.b09Num.V_12_2_3 = (decimal)row.OpeningCreditAmount;
                    }

                    // V.13
                    if (data.Any(o => o.AccountNumber == "4111"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "4111");
                        b09DNN.b09Num.V_13_1_1 = (decimal)row.OpeningCreditAmount;
                        b09DNN.b09Num.V_13_1_2 = (decimal)row.CreditAmount;
                        b09DNN.b09Num.V_13_1_3 = (decimal)row.DebitAmount;
                        b09DNN.b09Num.V_13_1_4 = (decimal)row.ClosingCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "4112"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "4112");
                        b09DNN.b09Num.V_13_2_1 = (decimal)row.OpeningCreditAmount - (decimal)row.OpeningDebitAmount;
                        b09DNN.b09Num.V_13_2_2 = (decimal)row.CreditAmount;
                        b09DNN.b09Num.V_13_2_3 = (decimal)row.DebitAmount;
                        b09DNN.b09Num.V_13_2_4 = (decimal)row.ClosingCreditAmount - (decimal)row.ClosingDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "4118"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "4118");
                        b09DNN.b09Num.V_13_3_1 = (decimal)row.OpeningCreditAmount;
                        b09DNN.b09Num.V_13_3_2 = (decimal)row.CreditAmount;
                        b09DNN.b09Num.V_13_3_3 = (decimal)row.DebitAmount;
                        b09DNN.b09Num.V_13_3_4 = (decimal)row.ClosingCreditAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "4119"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "4119");
                        b09DNN.b09Num.V_13_4_1 = (decimal)row.OpeningDebitAmount;
                        b09DNN.b09Num.V_13_4_2 = (decimal)row.DebitAmount; 
                        b09DNN.b09Num.V_13_4_3 = (decimal)row.CreditAmount;
                        b09DNN.b09Num.V_13_4_4 = (decimal)row.ClosingDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "4113"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "4113");
                        b09DNN.b09Num.V_13_5_1 = (decimal)row.OpeningCreditAmount - (decimal)row.OpeningDebitAmount;
                        b09DNN.b09Num.V_13_5_2 = (decimal)row.CreditAmount;
                        b09DNN.b09Num.V_13_5_3 = (decimal)row.DebitAmount;
                        b09DNN.b09Num.V_13_5_4 = (decimal)row.ClosingCreditAmount - (decimal)row.ClosingDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "418"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "418");
                        b09DNN.b09Num.V_13_6_1 = b09DNN.b09Num.V_13_6_1 + (decimal)row.OpeningCreditAmount - (decimal)row.OpeningDebitAmount;
                        b09DNN.b09Num.V_13_6_2 = b09DNN.b09Num.V_13_6_2 + (decimal)row.CreditAmount;
                        b09DNN.b09Num.V_13_6_3 = b09DNN.b09Num.V_13_6_3 + (decimal)row.DebitAmount;
                        b09DNN.b09Num.V_13_6_4 = b09DNN.b09Num.V_13_6_4 + (decimal)row.ClosingCreditAmount - (decimal)row.ClosingDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "421"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "421");
                        b09DNN.b09Num.V_13_6_1 = b09DNN.b09Num.V_13_6_1 + (decimal)row.OpeningCreditAmount - (decimal)row.OpeningDebitAmount;
                        //b09DNN.b09Num.V_13_6_2 = b09DNN.b09Num.V_13_6_2 + row.CreditAmount;
                        //b09DNN.b09Num.V_13_6_3 = b09DNN.b09Num.V_13_6_3 + row.DebitAmount;
                        b09DNN.b09Num.V_13_6_4 = b09DNN.b09Num.V_13_6_4 + (decimal)row.ClosingCreditAmount - (decimal)row.ClosingDebitAmount;
                    }

                    if (data.Any(o => o.AccountNumber == "4212"))
                    {
                        var row = data.FirstOrDefault(o => o.AccountNumber == "4212");
                        b09DNN.b09Num.V_13_6_2 = b09DNN.b09Num.V_13_6_2 + (decimal)row.CreditAmount;
                        b09DNN.b09Num.V_13_6_3 = b09DNN.b09Num.V_13_6_3 + (decimal)row.DebitAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                return;
            }
            
        }

        private void fillSectorVData()
        {
            try
            {
                foreach (var prop in b09DNN.b09Num.GetType().GetProperties())
                {
                    Label lb = this.Controls.Find(prop.Name, true).FirstOrDefault() as Label;
                    if (lb != null)
                    {
                        //lb.Text = prop.GetValue(b09DNN.b09Num, null) != null ? prop.GetValue(b09DNN.b09Num, null).ToString() : null;
                        var number = prop.GetValue(b09DNN.b09Num, null) != null ? prop.GetValue(b09DNN.b09Num, null) : 0;
                        Utils.FormatNumberic(lb, number, ConstDatabase.Format_TienVND);
                    }

                }
            }
            catch(Exception ex)
            {
                return;
            }
            
        }

        private void Block_VI_Data(DateTime startDate, DateTime endDate)
        {
            try
            {
                ReportProcedureSDS sp = new ReportProcedureSDS();
                List<B09BlockVI> data = new List<B09BlockVI>();
                data = sp.GetB09_SecVI(startDate, endDate);
                if (data.Count == 0)
                {
                    MSG.Warning("Không có dữ liệu để lên báo cáo!");
                }
                else
                {
                    foreach(var row in data)
                    {
                        try
                        {
                            PropertyInfo propertyInfo = b09DNN.b09Num.GetType().GetProperty(GetPropName(row.Name, "1"));
                            propertyInfo.SetValue(b09DNN.b09Num, Convert.ChangeType(row.ThisYear ?? 0, propertyInfo.PropertyType), null);

                            PropertyInfo propertyInfo2 = b09DNN.b09Num.GetType().GetProperty(GetPropName(row.Name, "2"));
                            propertyInfo2.SetValue(b09DNN.b09Num, Convert.ChangeType(row.LastYear ?? 0, propertyInfo.PropertyType), null);
                        }
                        catch(Exception ex)
                        {
                            return;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                return;
            }
        }
        private string GetPropName(string Name, string column)
        {
            var split = Name.Split('_');
            var regex = "\\d";
            if(Regex.IsMatch(split[2], regex))
            {
                split[2] = string.Format("{0}_{1}", column, split[2]);
            }
            else
            {
                split[2] = string.Format("{0}_{1}", split[2], column);
            }
            return string.Join("_", split);
        }
        private void label_edit(object sender, EventArgs e)
        {
            var label = (Label)sender;
            var editform = new UEditBox(label.Text, 0, false);
            editform.ShowDialog(this);
            if(editform.Output != null)
            {
                label.Text = editform.Output;
            }
        }

        private void number_edit(object sender, EventArgs e)
        {
            var label = (Label)sender;
            PropertyInfo prop = b09DNN.b09Num.GetType().GetProperty(label.Name);
            var number = prop.GetValue(b09DNN.b09Num, null) != null ? (decimal)prop.GetValue(b09DNN.b09Num, null) : 0;

            var editform = new UEditBox(null, number, true);
            editform.ShowDialog(this);
            if (editform.OutputNum != null)
            {
                prop.SetValue(b09DNN.b09Num, Convert.ChangeType(editform.OutputNum, prop.PropertyType), null);
                fillSectorVData();
            }
        }

        private void btnPrint_click(object sender, EventArgs e)
        {
            try
            {
                b09DNN.B09SectorI = getInputData(b09DNN.b09SecI);
                b09DNN.B09SectorII = getInputData(b09DNN.b09SecII);
                b09DNN.B09SectorIII = getInputData(b09DNN.b09SecIII);
                b09DNN.B09SectorIV = getInputData(b09DNN.b09SecIV);
                b09DNN.B09SectorV4 = getInputData(b09DNN.b09SecV4);
                b09DNN.B09SectorV5 = getInputData(b09DNN.b09SecV5);
                b09DNN.B09SectorV6 = getInputData(b09DNN.b09SecV6);
                b09DNN.B09SectorV13 = getInputData(b09DNN.b09SecV13);
                b09DNN.B09SectorV14 = getInputData(b09DNN.b09SecV14);
                b09DNN.B09SectorV15 = getInputData(b09DNN.b09SecV15);
                b09DNN.B09SectorV16 = getInputData(b09DNN.b09SecV16);
                b09DNN.B09SectorVII = getInputData(b09DNN.b09SecVII);
                b09DNN.B09SectorVIII = getInputData(b09DNN.b09SecVIII);
                b09DNN.B09Number = new List<B09Num>() { b09DNN.b09Num };

                string path = System.IO.Path.GetDirectoryName(
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                string filePath = string.Format("{0}\\Frm\\FReport\\Template\\B09-DNN.rst", path);

                PerpetuumSoft.Reporting.Components.ReportManager rm = new PerpetuumSoft.Reporting.Components.ReportManager();
                PerpetuumSoft.Reporting.Components.FileReportSlot frs = new PerpetuumSoft.Reporting.Components.FileReportSlot();
                rm.Reports.Add(frs);
                frs.FilePath = filePath;
                ReportForm<B09DNN> f = new ReportForm<B09DNN>(frs, "Rpt_B09DNN");

                f.AddDatasource("B09DNN", new List<B09DNN> { b09DNN}, true);
                //f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private List<B09Sector> getInputData(object data)
        {
            var sector = new List<B09Sector>();
            try
            {
                foreach (var prop in data.GetType().GetProperties())
                {
                    Label lb = this.Controls.Find(prop.Name, true).FirstOrDefault() as Label;
                    if (lb != null)
                    {
                        prop.SetValue(data, Convert.ChangeType(lb.Text, prop.PropertyType), null);
                        sector.Add(new B09Sector() { Key = prop.Name, Value = lb.Text });
                    }

                }

                return sector;
            }
            catch (Exception ex)
            {
                return sector;
            }
        }
    }
}
