﻿using Accounting.Core.DAO;
using Accounting.Core.Domain.obj.Report;
using Accounting.TextMessage;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSONHATKYCHUNGTruocIn : Form
    {
        string tieude1;
        string _subSystemCode;
        List<GL_GeneralDiaryBook_S03a> data = new List<GL_GeneralDiaryBook_S03a>();
        DateTime begin1;
        DateTime end1;
        int Check;
        int Check2;
        public FRSONHATKYCHUNGTruocIn(string tieude,string _subSystemCode, List<GL_GeneralDiaryBook_S03a> list, DateTime begin, DateTime end, int check, int check2)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoNhatKyChung.rst", path);
            ReportUtils.ProcessControls(this);
            data = list;
            Check = check;
            Check2 = check2;
            begin1 = begin;
            end1 = end;
            tieude1 = tieude;
            List<GL_GeneralDiaryBook_S03a> data1 = new List<GL_GeneralDiaryBook_S03a>();
            ReportProcedureSDS db = new ReportProcedureSDS();
            data1 = db.Get_GL_GeneralDiaryBook_S03a(begin, end, check, check2);
            uGridDuLieu.DataSource = data1;
            InDamGridVaGanGiaTriCongNhom(uGridDuLieu, data1);
            Utils.ConfigGrid(uGridDuLieu, ConstDatabase.GL_GeneralDiaryBook_S03a_TableName);
            uGridDuLieu.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            UltraGridBand parentBand = this.uGridDuLieu.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;
            UltraGridGroup parentBandGroup1 = parentBand.Groups.Add("ParentBandGroup1", "SỔ NHẬT KÝ CHUNG");
            parentBandGroup1.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroup1.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroup1.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroup1.Header.Appearance.BorderColor = Color.Black;
            //string tieude = ReportUtils.GetPeriod(begin, end);
            UltraGridGroup parentBandGroup = parentBand.Groups.Add("ParentBandGroup", tieude);
            parentBandGroup.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroup.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroup.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroup.Header.Appearance.BorderColor = Color.Black;
            parentBandGroup.RowLayoutGroupInfo.ParentGroup = parentBandGroup1;
            parentBand.Columns[0].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[1].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[2].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[3].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[4].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[5].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[6].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[7].RowLayoutColumnInfo.ParentGroup = parentBandGroup;

            parentBand.Columns[5].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[5].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[5].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[5].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[5].Header.Appearance.BorderColor = Color.Black;
            parentBand.Columns[5].CellAppearance.TextHAlign = HAlign.Center;

            parentBand.Columns[4].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[4].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[4].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[4].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[4].Header.Appearance.BorderColor = Color.Black;
            parentBand.Columns[4].CellAppearance.TextHAlign = HAlign.Center;

            parentBand.Columns[3].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[3].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[3].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[3].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[3].Header.Appearance.BorderColor = Color.Black; 
            parentBand.Columns[3].CellAppearance.TextHAlign = HAlign.Left;

            parentBand.Columns[2].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[2].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[2].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[2].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[2].Header.Appearance.BorderColor = Color.Black;
            parentBand.Columns[2].CellAppearance.TextHAlign = HAlign.Center;

            parentBand.Columns[1].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[1].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[1].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[1].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[1].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[0].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[0].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[0].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[0].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[0].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[6].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[6].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[6].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[6].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[6].Header.Appearance.BorderColor = Color.Black;
            parentBand.Columns[6].FormatNumberic(ConstDatabase.Format_TienVND);

            parentBand.Columns[7].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[7].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[7].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[7].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[7].Header.Appearance.BorderColor = Color.Black;
            parentBand.Columns[7].FormatNumberic(ConstDatabase.Format_TienVND);
            decimal? a = data1.Where(n => n.OderType != 0).Sum(n => n.DebitAmount == null ? 0:n.DebitAmount);
            decimal? b = data1.Where(n => n.OderType != 0).Sum(n => n.CreditAmount ==null ? 0 : n.CreditAmount);

            Utils.AddSumColumn(uGridDuLieu, "RefDate", false);
            Utils.AddSumColumn(uGridDuLieu, "RefNo", false);
            Utils.AddSumColumn(uGridDuLieu, "JournalMemo", false);
            Utils.AddSumColumn(uGridDuLieu, "AccountNumber", false);
            Utils.AddSumColumn(uGridDuLieu, "CorrespondingAccountNumber", false);

            uGridDuLieu.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[5].DisplayFormat = " ";

            Utils.AddSumColumn1(a,uGridDuLieu, "DebitAmount",false,"",1);
            Utils.AddSumColumn1(b, uGridDuLieu, "CreditAmount",false, "", 1);
            SetDoSoAm(uGridDuLieu);
            this.WindowState = FormWindowState.Maximized;
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;


                this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
                this.Location = new System.Drawing.Point(((Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2), ((Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2));

            }
            if (Utils.isDemo)
            {
                ultraButton3.Visible = false;
            }
            WaitingFrm.StopWaiting();
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            //if (dtBeginDate.Value == null || dtEndDate.Value == null)
            //{
            //    MSG.Warning("Ngày không được để trống");
            //    return;
            //}

            //if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            //{
            //    MSG.Warning("Từ ngày không được lớn hơn đến ngày");
            //    return;
            //}

            //List<GL_GeneralDiaryBook_S03a> data = new List<GL_GeneralDiaryBook_S03a>();
            //int check = cbCongGopButToan.Checked == false ? 0 : 1;
            //int check2 = cbHienThiLyke.Checked == false ? 0 : 1;
            //ReportProcedureSDS db = new ReportProcedureSDS();
            //data = db.Get_GL_GeneralDiaryBook_S03a((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, check, check2);
            //if (data.Count == 0)
            //{
            //    MSG.Warning("Không có dữ liệu để lên báo cáo");
            //}
            //else
            //{
                var rD = new GL_GeneralDiaryBook_S03a_Detail();
            rD.Period = tieude1;
                var f = new ReportForm<GL_GeneralDiaryBook_S03a>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("S03ADNN", data);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            //}
        }
        private void InDamGridVaGanGiaTriCongNhom(UltraGrid data, List<GL_GeneralDiaryBook_S03a> list)
        {
            UltraGridRow ultraGridRow = data.DisplayLayout.Rows[0];
            for (int i = 0; i < list.Count; i++)
            {
                if (data.DisplayLayout.Rows[i].Cells[3].Value != null)
                {
                    if (data.DisplayLayout.Rows[i].Cells[3].Value.ToString() == "Số lũy kế kỳ trước chuyển sang")
                    {
                        data.DisplayLayout.Rows[i].Cells[0].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[1].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[2].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[3].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[4].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[5].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[6].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[7].Appearance.FontData.Bold = DefaultableBoolean.True;

                    }
                    
                }

            }
        }
        private void SetDoSoAm(UltraGrid ultraGrid)
        {
            foreach (var item in ultraGrid.Rows)
            {
                if (item.Cells["CreditAmount"].Value != null)
                {
                    if ((decimal)item.Cells["CreditAmount"].Value < 0)
                        item.Cells["CreditAmount"].Appearance.ForeColor = Color.Red;
                }
                if (item.Cells["DebitAmount"].Value != null)
                {
                    if ((decimal)item.Cells["DebitAmount"].Value < 0)
                        item.Cells["DebitAmount"].Appearance.ForeColor = Color.Red;
                }
            }
        }
        private void ultraButton3_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "SoNhatKyChung.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGridDuLieu, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }

        private void ultraButton2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Column.Format))
            {
                e.ExcelFormatStr = e.Column.Format;
            }
        }

        private void uGridDuLieu_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.ListObject != null)
            {
                if (e.Row.ListObject.HasProperty("TypeID"))
                {
                    if (!e.Row.ListObject.GetProperty("TypeID").IsNullOrEmpty())
                    {
                        int typeid = e.Row.ListObject.GetProperty("TypeID").ToInt();

                        if (e.Row.ListObject.HasProperty("ReferenceID"))
                        {
                            if (!e.Row.ListObject.GetProperty("ReferenceID").IsNullOrEmpty())
                            {
                                Guid id = (Guid)e.Row.ListObject.GetProperty("ReferenceID");
                                var f = Utils.ViewVoucherSelected1(id, typeid);
                                //f.Visible = false;
                                //f.ShowDialog();
                                //this.Close();
                                //if (f.IsDisposed)
                                //{
                                //    new FRSONHATKYCHUNGTruocIn( tieude1, _subSystemCode,data, begin1, end1, Check, Check2).Show();
                                //}
                            }
                        }
                    }

                }
            }
        }

        private void uGridDuLieu_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGridDuLieu.Rows)
            {
                foreach (var column in uGridDuLieu.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;

        }
    }
}
