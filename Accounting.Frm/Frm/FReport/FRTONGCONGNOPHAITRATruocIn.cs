﻿using Accounting.Core.DAO;
using Accounting.Core.Domain.obj.Report;
using Accounting.TextMessage;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRTONGCONGNOPHAITRATruocIn : Form
    {
        DateTime begin1;
        DateTime end1;
        string currencyID1;
        string account1;
        string listncc1;
        string Period1;
        public FRTONGCONGNOPHAITRATruocIn(DateTime begin, DateTime end,string currencyID,string account,string listncc,string Period)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            begin1 = begin;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\TongHopCongNoPhaiTra.rst", path);
            end1 = end;
            currencyID1 = currencyID;
            account1 = account;
            listncc1 = listncc;
            Period1 = Period;
            ReportProcedureSDS sp = new ReportProcedureSDS();
            List<PO_PayableDetail> data1 = sp.GetTongHopCongNoPhaiTraNCC(begin, end, currencyID, account, listncc);
            uGridDuLieu.DataSource = data1.ToList();
            Utils.ConfigGrid(uGridDuLieu, ConstDatabase.PO_PayableDetail_TableName);
            uGridDuLieu.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            Utils.AddSumColumn(uGridDuLieu, "AccountObjectName", false);
            Utils.AddSumColumn(uGridDuLieu, "AccountNumber", false);

            uGridDuLieu.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";

            if (currencyID=="VND")
            {
                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpeningDebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpeningCreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);

                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpeningDebitAmount"].CellAppearance.TextHAlign = HAlign.Right;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpeningCreditAmount"].CellAppearance.TextHAlign = HAlign.Right;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmount"].CellAppearance.TextHAlign = HAlign.Right;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmount"].CellAppearance.TextHAlign = HAlign.Right;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].CellAppearance.TextHAlign = HAlign.Right;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].CellAppearance.TextHAlign = HAlign.Right;

                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpeningDebitAmountOC"].Hidden = true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpeningCreditAmountOC"].Hidden = true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Hidden = true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Hidden = true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CloseDebitAmountOC"].Hidden = true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CloseCreditAmountOC"].Hidden = true;

                Utils.AddSumColumn(uGridDuLieu, "OpeningDebitAmount", false);
                Utils.AddSumColumn(uGridDuLieu, "OpeningCreditAmount", false);
                Utils.AddSumColumn(uGridDuLieu, "DebitAmount", false);
                Utils.AddSumColumn(uGridDuLieu, "CreditAmount", false);
                Utils.AddSumColumn(uGridDuLieu, "ClosingDebitAmount", false);
                Utils.AddSumColumn(uGridDuLieu, "ClosingCreditAmount", false);
            }
            else
            {
                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpeningDebitAmountOC"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpeningCreditAmountOC"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmountOC"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmountOC"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CloseDebitAmountOC"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CloseCreditAmountOC"].FormatNumberic(ConstDatabase.Format_TienVND);

                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpeningDebitAmountOC"].CellAppearance.TextHAlign = HAlign.Right;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpeningCreditAmountOC"].CellAppearance.TextHAlign = HAlign.Right;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmountOC"].CellAppearance.TextHAlign = HAlign.Right;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmountOC"].CellAppearance.TextHAlign = HAlign.Right;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CloseDebitAmountOC"].CellAppearance.TextHAlign = HAlign.Right;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CloseCreditAmountOC"].CellAppearance.TextHAlign = HAlign.Right;

                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpeningDebitAmount"].Hidden = true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpeningCreditAmount"].Hidden = true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmount"].Hidden = true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmount"].Hidden = true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].Hidden = true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].Hidden = true;

                Utils.AddSumColumn(uGridDuLieu, "OpeningDebitAmountOC", false);
                Utils.AddSumColumn(uGridDuLieu, "OpeningCreditAmountOC", false);
                Utils.AddSumColumn(uGridDuLieu, "DebitAmountOC", false);
                Utils.AddSumColumn(uGridDuLieu, "CreditAmountOC", false);
                Utils.AddSumColumn(uGridDuLieu, "CloseDebitAmountOC", false);
                Utils.AddSumColumn(uGridDuLieu, "CloseCreditAmountOC", false);
            }
            UltraGridBand parentBand = this.uGridDuLieu.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;
            UltraGridGroup parentBandGroup1 = parentBand.Groups.Add("ParentBandGroup1", "TỔNG CÔNG NỢ PHẢI TRẢ");
            parentBandGroup1.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroup1.Header.Appearance.TextHAlign = HAlign.Center;
            //string tieude = string.Format("Tài khoản: ");
            UltraGridGroup parentBandGroup = parentBand.Groups.Add("ParentBandGroup", Period);
            parentBandGroup.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroup.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroup.RowLayoutGroupInfo.ParentGroup = parentBandGroup1;

            UltraGridGroup parentBandGroupDK = parentBand.Groups.Add("ParentBandGroupDK", "Đầu kỳ");
            parentBandGroupDK.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupDK.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupDK.RowLayoutGroupInfo.ParentGroup = parentBandGroup;

            UltraGridGroup parentBandGroupPS = parentBand.Groups.Add("ParentBandGroupPS", "Phát sinh");
            parentBandGroupPS.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupPS.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPS.RowLayoutGroupInfo.ParentGroup = parentBandGroup;

            UltraGridGroup parentBandGroupCK = parentBand.Groups.Add("ParentBandGroupCK", "Cuối kỳ");
            parentBandGroupCK.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupCK.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupCK.RowLayoutGroupInfo.ParentGroup = parentBandGroup;

            parentBandGroupDK.Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBandGroupDK.Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBandGroupDK.Header.Appearance.ForeColor = Color.Black;
            parentBandGroupDK.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupDK.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPS.Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPS.Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBandGroupPS.Header.Appearance.ForeColor = Color.Black;
            parentBandGroupPS.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPS.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupCK.Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBandGroupCK.Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBandGroupCK.Header.Appearance.ForeColor = Color.Black;
            parentBandGroupCK.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupCK.Header.Appearance.BorderColor = Color.Black;

            parentBandGroup.Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBandGroup.Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBandGroup.Header.Appearance.ForeColor = Color.Black;
            parentBandGroup.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroup.Header.Appearance.BorderColor = Color.Black;

            parentBandGroup1.Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBandGroup1.Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBandGroup1.Header.Appearance.ForeColor = Color.Black;
            parentBandGroup1.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroup1.Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[0].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[0].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[0].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[0].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[0].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[1].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[1].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[1].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[1].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[1].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[2].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[2].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[2].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[2].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[2].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[3].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[3].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[3].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[3].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[3].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[4].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[4].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[4].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[4].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[4].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[5].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[5].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[5].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[5].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[5].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[6].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[6].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[6].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[6].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[6].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[7].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[7].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[7].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[7].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[7].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[8].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[8].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[8].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[8].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[8].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[9].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[9].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[9].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[9].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[9].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[10].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[10].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[10].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[10].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[10].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[11].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[11].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[11].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[11].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[11].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[12].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[12].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[12].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[12].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[12].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[13].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[13].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[13].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[13].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[13].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[1].RowLayoutColumnInfo.SpanY = parentBand.Columns[1].RowLayoutColumnInfo.SpanY + parentBandGroupDK.RowLayoutGroupInfo.SpanY;
            parentBand.Columns[0].RowLayoutColumnInfo.SpanY = parentBand.Columns[0].RowLayoutColumnInfo.SpanY + parentBandGroupDK.RowLayoutGroupInfo.SpanY;
            parentBand.Columns[2].RowLayoutColumnInfo.SpanY = parentBand.Columns[2].RowLayoutColumnInfo.SpanY + parentBandGroupDK.RowLayoutGroupInfo.SpanY;

            parentBand.Columns[1].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[0].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[2].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns["OpeningDebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDK;
            parentBand.Columns["OpeningCreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDK;
            parentBand.Columns["OpeningDebitAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDK;
            parentBand.Columns["OpeningCreditAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDK;
            parentBand.Columns["DebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
            parentBand.Columns["CreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
            parentBand.Columns["DebitAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
            parentBand.Columns["CreditAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
            //parentBand.Columns[8].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
            parentBand.Columns["ClosingDebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;
            parentBand.Columns["ClosingCreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;
            parentBand.Columns["CloseDebitAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;
            parentBand.Columns["CloseCreditAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;
            parentBandGroupCK.RowLayoutGroupInfo.ParentGroup = parentBandGroup;
            parentBandGroupPS.RowLayoutGroupInfo.ParentGroup = parentBandGroup;
            parentBandGroupDK.RowLayoutGroupInfo.ParentGroup = parentBandGroup;

            parentBand.Columns[0].Header.VisiblePosition = 1;
            parentBand.Columns[1].Header.VisiblePosition = 2;
            parentBand.Columns[2].Header.VisiblePosition = 3;
            parentBand.Columns["OpeningDebitAmount"].Header.VisiblePosition = 4;
            parentBand.Columns["OpeningCreditAmount"].Header.VisiblePosition = 5;
            parentBand.Columns["DebitAmount"].Header.VisiblePosition = 6;
            parentBand.Columns["CreditAmount"].Header.VisiblePosition = 7;
            parentBand.Columns["ClosingDebitAmount"].Header.VisiblePosition = 8;
            parentBand.Columns["ClosingCreditAmount"].Header.VisiblePosition = 9;
            parentBand.Columns["OpeningDebitAmountOC"].Header.VisiblePosition = 10;
            parentBand.Columns["OpeningCreditAmountOC"].Header.VisiblePosition = 11;
            
            
            parentBand.Columns["DebitAmountOC"].Header.VisiblePosition = 12;
            parentBand.Columns["CreditAmountOC"].Header.VisiblePosition = 13;
            //parentBand.Columns[8].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
           
            parentBand.Columns["CloseDebitAmountOC"].Header.VisiblePosition = 14;
            parentBand.Columns["CloseCreditAmountOC"].Header.VisiblePosition = 15;

            parentBand.Columns[0].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns[0].RowLayoutColumnInfo.OriginY = 0;
            //parentBand.Columns[0].RowLayoutColumnInfo.SpanX = 40;
            parentBand.Columns[1].RowLayoutColumnInfo.OriginX = parentBand.Columns[0].RowLayoutColumnInfo.SpanX;
            parentBand.Columns[1].RowLayoutColumnInfo.OriginY = 0;
            //parentBand.Columns[1].RowLayoutColumnInfo.SpanX = 40;
            parentBand.Columns[2].RowLayoutColumnInfo.OriginX = parentBand.Columns[1].RowLayoutColumnInfo.OriginX + parentBand.Columns[1].RowLayoutColumnInfo.SpanX;
            parentBand.Columns[2].RowLayoutColumnInfo.OriginY = 0;
            //parentBand.Columns[2].RowLayoutColumnInfo.SpanX = 40;
            parentBandGroupDK.RowLayoutGroupInfo.OriginX = parentBand.Columns[2].RowLayoutColumnInfo.OriginX + parentBand.Columns[2].RowLayoutColumnInfo.SpanX;
            parentBandGroupDK.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupDK.RowLayoutGroupInfo.SpanX = 300;
            parentBandGroupPS.RowLayoutGroupInfo.OriginX = parentBandGroupDK.RowLayoutGroupInfo.OriginX + parentBandGroupDK.RowLayoutGroupInfo.SpanX;
            parentBandGroupPS.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupPS.RowLayoutGroupInfo.SpanX = 300;
            parentBandGroupCK.RowLayoutGroupInfo.OriginX = parentBandGroupPS.RowLayoutGroupInfo.OriginX + parentBandGroupPS.RowLayoutGroupInfo.SpanX;
            parentBandGroupCK.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupCK.RowLayoutGroupInfo.SpanX = 300;
            

            SetDoSoAm(uGridDuLieu);
            this.WindowState = FormWindowState.Maximized;
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;


                this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
                this.Location = new System.Drawing.Point(((Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2), ((Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2));

            }
            if (Utils.isDemo)
            {
                ultraButton2.Visible = false;
            }
            WaitingFrm.StopWaiting();
        }
        private void SetDoSoAm(UltraGrid ultraGrid)
        {
            foreach (var item in ultraGrid.Rows)
            {
                if (currencyID1 == "VND")
                {
                    if ((decimal)item.Cells["OpeningDebitAmount"].Value < 0)
                        item.Cells["OpeningDebitAmount"].Appearance.ForeColor = Color.Red;


                    if ((decimal)item.Cells["OpeningCreditAmount"].Value < 0)
                        item.Cells["OpeningCreditAmount"].Appearance.ForeColor = Color.Red;
                    if ((decimal)item.Cells["DebitAmount"].Value < 0)
                        item.Cells["DebitAmount"].Appearance.ForeColor = Color.Red;


                    if ((decimal)item.Cells["CreditAmount"].Value < 0)
                        item.Cells["CreditAmount"].Appearance.ForeColor = Color.Red;
                    if ((decimal)item.Cells["ClosingDebitAmount"].Value < 0)
                        item.Cells["ClosingDebitAmount"].Appearance.ForeColor = Color.Red;


                    if ((decimal)item.Cells["ClosingCreditAmount"].Value < 0)
                        item.Cells["ClosingCreditAmount"].Appearance.ForeColor = Color.Red;
                }
                else
                {
                    if ((decimal)item.Cells["OpeningDebitAmountOC"].Value < 0)
                        item.Cells["OpeningDebitAmountOC"].Appearance.ForeColor = Color.Red;


                    if ((decimal)item.Cells["OpeningCreditAmountOC"].Value < 0)
                        item.Cells["OpeningCreditAmountOC"].Appearance.ForeColor = Color.Red;
                    if ((decimal)item.Cells["DebitAmountOC"].Value < 0)
                        item.Cells["DebitAmountOC"].Appearance.ForeColor = Color.Red;


                    if ((decimal)item.Cells["CreditAmountOC"].Value < 0)
                        item.Cells["CreditAmountOC"].Appearance.ForeColor = Color.Red;
                    if ((decimal)item.Cells["CloseDebitAmountOC"].Value < 0)
                        item.Cells["CloseDebitAmountOC"].Appearance.ForeColor = Color.Red;


                    if ((decimal)item.Cells["CloseCreditAmountOC"].Value < 0)
                        item.Cells["CloseCreditAmountOC"].Appearance.ForeColor = Color.Red;
                }
            }
        }
        private void ultraButton3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            var rD = new PO_PayableDetail_SP();
            rD.Period = ReportUtils.GetPeriod(begin1, end1);
            ReportProcedureSDS sp = new ReportProcedureSDS();
            List<PO_PayableDetail> data = sp.GetTongHopCongNoPhaiTraNCC(begin1, end1, currencyID1, account1, listncc1);
            var f = new ReportForm<PO_PayableDetail>(fileReportSlot1, "");
            f.AddDatasource("DBTONGCONGNOPT", data, true);
            f.AddDatasource("Detail", rD);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }

        private void ultraButton2_Click(object sender, EventArgs e)
        {
            string a = Period1.Replace(" ", "");
            a = a.Replace("/", "-");
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = string.Format("TongCongNoPhaiTra{0}.xls",a)
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGridDuLieu, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGridDuLieu.Rows)
            {
                foreach (var column in uGridDuLieu.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;
        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Column.Format))
            {
                e.ExcelFormatStr = e.Column.Format;
            }
        }

        private void reportManager1_GetReportParameter(object sender, PerpetuumSoft.Reporting.Components.GetReportParameterEventArgs e)
        {
            //string typeCurrency = cbbCurrency.Value != null ? cbbCurrency.Value.ToString() : string.Empty;
            e.Parameters["CurrencyID"].Value = currencyID1;
        }

        private void uGridDuLieu_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;
        }

        private void uGridDuLieu_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            //if (e.Row.ListObject.HasProperty("TypeID"))
            //{
            //    if (!e.Row.ListObject.GetProperty("TypeID").IsNullOrEmpty())
            //    {
            //        int typeid = e.Row.ListObject.GetProperty("TypeID").ToInt();

            //        if (e.Row.ListObject.HasProperty("ID"))
            //        {
            //            if (!e.Row.ListObject.GetProperty("ID").IsNullOrEmpty())
            //            {
            //                Guid id = (Guid)e.Row.ListObject.GetProperty("ID");
            //                var f = Utils.ViewVoucherSelected1(id, typeid);
            //                this.Close();
            //                if (f.IsDisposed)
            //                {
            //                    new FRTONGCONGNOPHAITRATruocIn(begin1, end1, currencyID1, account1, listncc1, Period1).ShowDialog();
            //                }
            //            }
            //        }
            //    }

            //}
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }
}
