﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRCHITIETCONGNOPHAITHUNHANVIEN : Form
    {
        List<AccountingObjectReport> _lstAccountingObjectReport = new List<AccountingObjectReport>();
        readonly List<AccountReport> _lstAccountEmployeesReport = ReportUtils.LstAccountEmployeesReport;
        private readonly List<AccountingObjectReport> _lstnew = ReportUtils.LstEmployee;
        private readonly IAccountService _IAccountService;
        private string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRCHITIETCONGNOPHAITHUNHANVIEN()
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\ChiTietCongNoPhaiThuNV.rst", path);
            _IAccountService = IoC.Resolve<IAccountService>();
            foreach (var item in _lstnew)
            {
                item.Check = false;
            }
            _lstAccountingObjectReport = _lstnew;
            ugridAccountingObject2.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
            ReportUtils.ProcessControls(this);
            _lstAccountEmployeesReport.Insert(0, new AccountReport { AccountNumber = "Tất cả", AccountName = "Tất cả" });
            cbbFromAccount.DataSource = _lstAccountEmployeesReport;
            cbbFromAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbFromAccount, ConstDatabase.Account_TableName);
            cbbFromAccount.SelectedRow = cbbFromAccount.Rows[0];
            ugridAccountingObject2.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Header.Caption = "Mã nhân viên";
            ugridAccountingObject2.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Caption = "Tên nhân viên";

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }
        }

  

        private void btnOk_Click_1(object sender, EventArgs e)
        {

        }
        private void GanTKDuNoTKDuCo(List<SA_ReceivableDetail> list)
        {
            foreach (var item in list)
            {
                if (!item.AccountNumber.IsNullOrEmpty())
                {
                    int? AccountGroupKind = Utils.ListAccount.FirstOrDefault(n => n.AccountNumber == item.AccountNumber).AccountGroupKind;
                    if (AccountGroupKind == null) { continue; }
                    if (AccountGroupKind == 0)
                    {
                        if (item.ClosingCreditAmount != null )
                        {
                            if (item.ClosingCreditAmount > 0)
                            {
                                item.ClosingDebitAmount = -item.ClosingCreditAmount;
                                item.ClosingCreditAmount = 0;
                            }
                        }
                    }
                    if (AccountGroupKind == 1)
                    {
                        if ( item.ClosingDebitAmount != null)
                        {
                            if (item.ClosingDebitAmount > 0)
                            {
                                item.ClosingCreditAmount = -item.ClosingDebitAmount;
                                item.ClosingDebitAmount = 0;
                            }
                        }
                    }
                    if (AccountGroupKind == 3)
                    {
                        if (item.ClosingDebitAmount != null)
                        {
                            if (item.AccountNumber[0].ToInt() == 5)
                            {
                                if (item.ClosingDebitAmount > 0)
                                {
                                    item.ClosingCreditAmount = -item.ClosingDebitAmount;
                                    item.ClosingDebitAmount = 0;
                                }
                            }
                            if (item.AccountNumber[0].ToInt() == 7)
                            {
                                if (item.ClosingDebitAmount > 0)
                                {
                                    item.ClosingCreditAmount = -item.ClosingDebitAmount;
                                    item.ClosingDebitAmount = 0;
                                }
                            }
                        }
                        if (item.ClosingCreditAmount != null)
                        {
                            if (item.AccountNumber[0].ToInt() == 6)
                            {
                                if (item.ClosingCreditAmount > 0)
                                {
                                    item.ClosingDebitAmount = -item.ClosingCreditAmount;
                                    item.ClosingCreditAmount = 0;
                                }
                            }
                            if (item.AccountNumber[0].ToInt() == 8)
                            {
                                if (item.ClosingCreditAmount > 0)
                                {
                                    item.ClosingDebitAmount = -item.ClosingCreditAmount;
                                    item.ClosingCreditAmount = 0;
                                }
                            }
                        }
                    }

                }
            }
            
        }
        private void btnExit_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            ReportProcedureSDS sp = new ReportProcedureSDS();
            string acount = cbbFromAccount.Value != null ? cbbFromAccount.Value.ToString() : string.Empty;

            if (acount == null || acount == string.Empty)
            {
                MSG.Warning("Tài khoản không được để trống");
                return;
            }


            string list_acount = "";

            try
            {
                var dsacc = _lstAccountingObjectReport != null ? _lstAccountingObjectReport.Where(c => c.Check).Select(c => c.ID.ToString()) : new List<string>();
                list_acount = string.Join(",", dsacc.ToArray());
            }
            catch (Exception ex)
            {
                list_acount = "";
            }

            if (list_acount == "")
            {
                MSG.Warning("Bạn chưa chọn mã nhân viên! ");
                return;
            }
            List<SA_ReceivableDetail> data = sp.GetCHICONGNOPHAITHUNV((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, acount, list_acount, false);//editby cuongpv cbbCurrency.Value.ToString() -> currencyID
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                var data_group = data.Where(t => t.AccountObjectCode != null).ToList();
                GanTKDuNoTKDuCo(data);
                List<SA_ReceivableDetail> listCongNhom = data.Where(m => m.IDGroup == 1).ToList();
                List<SA_ReceivableDetail> listChiTiet = data.Where(m => m.IDGroup == 2).ToList();
                foreach (var item in listCongNhom)
                {
                    item.ClosingCreditAmount = listChiTiet.Where(n => n.AccountObjectID == item.AccountObjectID).Sum(n => n.ClosingCreditAmount);
                    item.ClosingDebitAmount = listChiTiet.Where(n => n.AccountObjectID == item.AccountObjectID).Sum(n => n.ClosingDebitAmount);
                }
                var rD = new SA_ReceivableDetail_period();
                string txtThoiGian = "Tài khoản: " + acount;
                rD.Period = txtThoiGian + "; " + ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                var f = new FRCHITIETCONGNOPHAITHUNHANVIENTruocIn((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, acount, list_acount);
                //var f = new ReportForm<SA_ReceivableDetail>(fileReportSlot1, _subSystemCode);
                //f.AddDatasource("dbCongNhom", listCongNhom, true);
                //f.AddDatasource("dbChiTiet", listChiTiet, true);
                //f.AddDatasource("Detail", rD);
                //f.LoadReport();
                //f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }
    }
}
