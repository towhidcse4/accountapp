﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTree;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSoChiTietDoanhThuTheoNhanVien : Form
    {
        List<AccountingObjectReport> _lstAccountingObjectReport = new List<AccountingObjectReport>();
        //readonly List<AccountReport> _lstAccountPayReport = ReportUtils.LstAccountPayReport;
        private readonly List<AccountingObjectReport> _lstnew = ReportUtils.LstEmployee;
        private readonly ICurrencyService _ICurrencyService;
        private string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRSoChiTietDoanhThuTheoNhanVien()
        {
            InitializeComponent();
            _subSystemCode = "";
            _lstAccountingObjectReport = _lstnew;            
            foreach (var item in _lstAccountingObjectReport) { item.Check = false; }
            _lstAccountingObjectReport = _lstAccountingObjectReport.OrderBy(n => n.AccountingObjectCode).OrderBy(n => n.DepartmentName).ToList();

            ugridEmployee.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
            ReportUtils.ProcessControls(this);
            #region Đếm số dòng
            Infragistics.Win.UltraWinGrid.UltraGridBand band = ugridEmployee.DisplayLayout.Bands[0];
            ugridEmployee.DisplayLayout.Override.AllowRowSummaries = Infragistics.Win.UltraWinGrid.AllowRowSummaries.
            Default; //Ẩn ký hiệu tổng trên Header Caption
            if (band.Summaries.Count != 0) band.Summaries.Clear();
            Infragistics.Win.UltraWinGrid.SummarySettings summary = band.Summaries.Add("Count", Infragistics.Win.UltraWinGrid.SummaryType.Count, band.Columns["Check"]);
            summary.DisplayFormat = "Số dòng dữ liệu: {0:N0}";
            summary.SummaryPosition = Infragistics.Win.UltraWinGrid.SummaryPosition.Left;
            ugridEmployee.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;   //ẩn 
            summary.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.BottomFixed | Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.GroupByRowsFooter;
            ugridEmployee.DisplayLayout.Override.GroupBySummaryDisplayStyle = Infragistics.Win.UltraWinGrid.GroupBySummaryDisplayStyle.SummaryCells;
            ugridEmployee.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
            ugridEmployee.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            #endregion
            ugridEmployee.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Header.Caption = "Mã nhân viên";
            ugridEmployee.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Caption = "Tên nhân viên";


            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }



        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            foreach (var item in ugridEmployee.Rows)
            {
                item.Cells["Check"].Value = false;
            }
            Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if (dtBeginDate.DateTime.Date > dtEndDate.DateTime.Date)
            {
                //MessageBox.Show("Ngày bắt đầu không thể lớn hơn ngày kết thúc", "Cảnh báo");
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            var lstAccountingObjectReport = _lstAccountingObjectReport.Where(x => x.Check).ToList();
            if (lstAccountingObjectReport.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn mã nhân viên!");
                return;
            }

            string list_item = "";

            try
            {
                var dsitemChoose = lstAccountingObjectReport != null ? lstAccountingObjectReport.Select(c => c.ID.ToString()) : new List<string>();
                list_item = string.Join(",", dsitemChoose.ToArray());
                list_item = "," + list_item + ",";
            }
            catch (Exception ex)
            {
                list_item = "";
            }
            FRSOCHITIETDOANHTHUTHEONVex fm = new FRSOCHITIETDOANHTHUTHEONVex((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, list_item);
            fm.Show(this);
            
        }
    }
}
