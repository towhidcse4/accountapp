﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;

namespace Accounting
{
    public partial class FRS17DNN : CustormForm
    {
        readonly List<MaterialGoodsReport> _lstMaterialGoodsReport = ReportUtils.LstMaterialGoodsReport;
        private List<MaterialGoodsReport> _lstMaterialGoodsReports = new List<MaterialGoodsReport>();
        string _subSystemCode;
        public FRS17DNN(string subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\S17-DNN.rst", path);
            _lstMaterialGoodsReports = _lstMaterialGoodsReport;
            ugridMaterialGoodsCategory.SetDataBinding(_lstMaterialGoodsReports.ToList(), "");
            ReportUtils.ProcessControls(this);
            btnExit.Click += btnExit_Click;
            //btnOk.Click += btnOk_Click;
            cbbMaterialGoodsCategory.ValueChanged += cbbMaterialGoodsSpecialTaxGroup_ValueChanged;
            WaitingFrm.StopWaiting();
        }

        private void cbbMaterialGoodsSpecialTaxGroup_ValueChanged(object sender, EventArgs e)
        {
            var mt = (MaterialGoodsSpecialTaxGroup)cbbMaterialGoodsCategory.SelectedRow.ListObject;
            _lstMaterialGoodsReports = _lstMaterialGoodsReport;
            if (mt.MaterialGoodsSpecialTaxGroupCode != "TC")
            {
                _lstMaterialGoodsReports = _lstMaterialGoodsReports.Where(c => c.MaterialGoodsGSTID == mt.ID).ToList();
            }
            ugridMaterialGoodsCategory.SetDataBinding(_lstMaterialGoodsReports.ToList(), "");
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }
            var saiSrv = IoC.Resolve<ISAInvoiceService>();
            List<Guid> lstGoodID = _lstMaterialGoodsReports.Where(c => c.Check).Select(c => c.ID).ToList();
            var data = saiSrv.ReportS35DN((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, lstGoodID);
            var rD = new S17DNNDetail();
            rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
            var f = new ReportForm<S17DNN>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("S17DNN", data, true);
            f.AddDatasource("Detail", rD);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }
    }
}
