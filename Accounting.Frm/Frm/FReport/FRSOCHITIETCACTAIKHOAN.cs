﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSOCHITIETCACTAIKHOAN : Form
    {
        readonly List<Account> _lstAccount = new List<Account>();
        IAccountService _IAccountService;
        readonly List<AccountReport> _lstAccountReport = ReportUtils.LstAccountBankReport;
        private string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRSOCHITIETCACTAIKHOAN()
        {
            InitializeComponent();
            _subSystemCode = "";
            //ultraLabel1.Font = new Font(ultraLabel1.Font, FontStyle.Bold);
            _IAccountService = IoC.Resolve<IAccountService>();
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoChiTietCacTaiKhoan.rst", path);
            _lstAccount = _IAccountService.GetListAccountIsActive(true);
            _lstAccountReport = _lstAccount.Select(c => new AccountReport()
            {
                AccountNumber = c.AccountNumber,
                AccountGroupID = c.AccountGroupID,
                AccountGroupKindView = c.AccountGroupKindView,
                AccountGroupKind = c.AccountGroupKind,
                AccountName = c.AccountName,
                AccountNameGlobal = c.AccountNameGlobal,
                IsActive = c.IsActive,
                Description = c.Description,
                DetailType = c.DetailType,
                ID = c.ID,
                ParentID = c.ParentID,
                IsParentNode = c.IsParentNode,
                Grade = c.Grade
            }).OrderBy(c => c.AccountNumber).ToList();
            ugridAccount.SetDataBinding(_lstAccountReport.ToList(), "");
            ReportUtils.ProcessControls(this);
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

            WaitingFrm.StopWaiting();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {

        }

        private void ultraLabel1_Click(object sender, EventArgs e)
        {

        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            int check = cbCongGopButToan.Checked == false ? 0 : 1;
            string listaccount = "";
            //string loaitien = cbbCurrency.Value.ToString();//comment by cuongpv
            if (!_lstAccountReport.Any(c => c.Check))
            {
                MSG.Warning(resSystem.Report_01);
                return;
            }
            var dsaccount = _lstAccountReport.Where(t => t.Check).Select(c => c.AccountNumber.ToString());
            listaccount = string.Join(",", dsaccount.ToArray());
            ReportProcedureSDS sp = new ReportProcedureSDS();
            List<GL_GetBookDetailPaymentByAccountNumber> data = sp.GetSoChiTietCacTaiKhoan1("", "", (DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, listaccount, check);
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                var list_accountnumber = data.Select(t => t.AccountNumber).Distinct().ToList();
                foreach (var item in list_accountnumber)
                {
                    var list1 = data.Where(t => t.AccountNumber == item).ToList();
                    list1[list1.Count() - 1].Sum_DuNo = list1[list1.Count() - 1].ClosingDebitAmount;
                    list1[list1.Count() - 1].Sum_DuCo = list1[list1.Count() - 1].ClosingCreditAmount;
                }
                var rD = new GL_GetBookDetailPaymentByAccountNumber_Detail();
                rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                //if (dsaccount.Count() > 1) comment by cuongpv
                //    rD.LoaiTien = "Loại tiền: " + loaitien;
                //else
                //    rD.LoaiTien = "Loại tiền: " + loaitien + "; Tài khoản: " + listaccount;
                var f = new FRSOCHITIETCACTAIKHOANTruocIn(data, (DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, check, listaccount);
                f.Show();
            }
           
        }

    }
}