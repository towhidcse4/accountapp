﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Accounting.Core.Domain;
using Accounting.Core.DAO;
using Accounting.Core.Domain.obj.Report;

namespace Accounting.Frm.FReport
{
    public partial class FRSoTheoDoiThanhToanNgoaiTe : Form
    {
        List<AccountingObjectReport> _lstAccountingObjectReport = new List<AccountingObjectReport>();
        //readonly List<AccountReport> _lstAccountPayReport = ReportUtils.LstAccountPayReport;
        private readonly List<AccountingObjectReport> _lstnew = ReportUtils.LstCustomers;
        private readonly IAccountService _IAccountService;
        private readonly ICurrencyService _ICurrencyService;
        private string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRSoTheoDoiThanhToanNgoaiTe()
        {
            InitializeComponent();

            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot2.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoTheoDoiThanhToanNgoaiTe.rst", path);
            _IAccountService = IoC.Resolve<IAccountService>();

            foreach (var item in _lstnew) { item.Check = false; }
            _lstAccountingObjectReport = _lstnew;

            ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
            ReportUtils.ProcessControls(this);

            //load data to cbbCurrencyType
            _ICurrencyService = IoC.Resolve<ICurrencyService>();
            var listTypeCurrency = _ICurrencyService.GetAll().Where(m => m.IsActive == true && m.ID != "VND").ToList();
            cbbCurrencyType.DataSource = listTypeCurrency;
            cbbCurrencyType.DisplayMember = "ID";
            Utils.ConfigGrid(cbbCurrencyType, ConstDatabase.Currency_TableName);
            cbbCurrencyType.SelectedRow = cbbCurrencyType.Rows[0];

            //load data to cbbFromAccount
            var listacount = _IAccountService.GetAll().Where(p => p.IsActive == true && (p.AccountNumber.StartsWith("131") || p.AccountNumber.StartsWith("141") || p.AccountNumber.StartsWith("331"))).OrderBy(p => p.AccountNumber).ToList();
            listacount.Insert(0, new Account { AccountNumber = "Tất cả", AccountName = "Tất cả" });//AccountNumber = all -> Tất cả
            cbbFromAccount.DataSource = listacount;
            cbbFromAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbFromAccount, ConstDatabase.Account_TableName);
            cbbFromAccount.SelectedRow = cbbFromAccount.Rows[0];
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            string typeCurrency = cbbCurrencyType.Value != null ? cbbCurrencyType.Value.ToString() : string.Empty;
            if (string.IsNullOrEmpty(typeCurrency))
            {
                MSG.Warning("Bạn chưa chọn loại tiền.");
                return;
            }

            string acount = cbbFromAccount.Value != null ? cbbFromAccount.Value.ToString() : string.Empty;
            if (string.IsNullOrEmpty(acount))
            {
                MSG.Warning("Bạn chưa chọn tài khoản.");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            string list_acount = "";

            try
            {
                var dsacc = _lstAccountingObjectReport != null ? _lstAccountingObjectReport.Where(c => c.Check).Select(c => c.ID.ToString()) : new List<string>();
                list_acount = string.Join(",", dsacc.ToArray());
            }
            catch (Exception ex)
            {
                list_acount = "";
            }

            if(list_acount == "")
            {
                MSG.Warning("Bạn chưa chọn đối tượng");
                return;
            }

            ReportProcedureSDS sp = new ReportProcedureSDS();
            List<SoTheoDoiThanhToanNgoaiTe> listAll = sp.GetSoTheoDoiThanhToanNgoaiTe((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, typeCurrency, acount, list_acount);

            if(listAll.Count == 0)
                MSG.Warning("Không có dữ liệu.");
            else
            {
                List<SoTheoDoiThanhToanNgoaiTe> listCongNhomDT = listAll.Where(m => m.IdGroup == 1).ToList();
                List<SoTheoDoiThanhToanNgoaiTe> listCongNhomTK = listAll.Where(m => m.IdGroup == 3).ToList();
                List<SoTheoDoiThanhToanNgoaiTe> listChiTiet = listAll.Where(m => m.IdGroup == 5).ToList();

                var rD = new SoTheoDoiThanhToanNgoaiTe_period();
                string txtThoiGian = string.Empty;
                //if (acount=="all")
                //    txtThoiGian = "Tài khoản: <Tất cả>; Loại tiền: " + typeCurrency;
                //else
                txtThoiGian = "Tài khoản: " + acount + "; Loại tiền: " + typeCurrency;

                rD.Period = txtThoiGian + "; " + ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                //fileReportSlot2.Document.Parameters.Add("CurrencyID", typeCurrency);
                var f = new ReportForm<SoTheoDoiThanhToanNgoaiTe>(fileReportSlot2, _subSystemCode);
                f.AddDatasource("DBCONGNHOMDT", listCongNhomDT, true);
                f.AddDatasource("DBCONGNHOMTK", listCongNhomTK, true);
                f.AddDatasource("DBChiTiet", listChiTiet, true);
                f.AddDatasource("Detail", rD);
                f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            foreach (var item in ugridAccountingObject.Rows)
            {
                item.Cells["Check"].Value = false;
            }
            Close();
        }

        private void reportManager1_GetReportParameter(object sender, PerpetuumSoft.Reporting.Components.GetReportParameterEventArgs e)
        {
            string typeCurrency = cbbCurrencyType.Value != null ? cbbCurrencyType.Value.ToString() : string.Empty;
            e.Parameters["CurrencyID"].Value = typeCurrency;
        }
    }
}
