﻿using System;
using System.Collections.Generic;
using System.Linq;
using FX.Core;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;

namespace Accounting
{
    public partial class FRS03a3DNN : CustormForm
    {
        string _subSystemCode;
        public FRS03a3DNN(string subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\S03a3-DNN.rst", path);
            ReportUtils.ProcessControls(this);
            btnExit.Click += btnExit_Click;
            btnOk.Click += btnOk_Click;
            WaitingFrm.StopWaiting();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }
            var ppInvoiceSrv = IoC.Resolve<IPPInvoiceService>();
            List<S03A3DNN> data = new List<S03A3DNN>();
            //for (int i = 0; i < 200; i++)
            //{
            //    data.Add(new S03A3DNN(i));
            //}
            data = ppInvoiceSrv.ReportS03A3Dn((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
            var rD = new S03A3DNNDetail();
            rD.Period = ReportUtils.GetPeriod(DateTime.Now, DateTime.UtcNow);
            var f = new ReportForm<S03A3DNN>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("S03a3DNN", data, true);
            f.AddDatasource("Detail", rD);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
