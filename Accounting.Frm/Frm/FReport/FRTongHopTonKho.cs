﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Accounting.Core.Domain;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Accounting.Frm.FReport;

namespace Accounting
{
    public partial class FRTongHopTonKho : CustormForm
    {
        readonly List<RepositoryReport> _lstnew = ReportUtils.LstRepositoryReport;
        private List<RepositoryReport> _lstRepositoryReport = new List<RepositoryReport>();
        public readonly IRepositoryLedgerService _IRepositoryLedgerService;
        public readonly IRepositoryService _IRepositoryService;
        public readonly IMaterialGoodsService _IMaterialGoodsService;
        string _subSystemCode;
        private bool _check1 = false;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRTongHopTonKho(string subSystemCode)
        {
            _IRepositoryLedgerService = IoC.Resolve<IRepositoryLedgerService>();
            _IRepositoryService = IoC.Resolve<IRepositoryService>();
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            
            _lstRepositoryReport = _lstnew;
            ugridRepository.SetDataBinding(_lstRepositoryReport.OrderBy(n =>n.RepositoryCode).ToList(), "");
            ReportUtils.ProcessControls(this);
            btnExit.Click += btnExit_Click;
         //   btnOk.Click += btnOk_Click;
            cbbMaterialGoodsCategory1.ValueChanged += cbbMaterialGoodsCategory1_ValueChanged;
            radInA4.Checked = true;
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

            WaitingFrm.StopWaiting();
        }
        #region lọc nhóm vật tư


        private void cbbMaterialGoodsCategory1_ValueChanged(object sender, EventArgs e)
        {
            _check1 = ((MaterialGoodsCategory)cbbMaterialGoodsCategory1.SelectedRow.ListObject).MaterialGoodsCategoryCode != "TC";
        }
        #endregion
        private void btnExit_Click(object sender, EventArgs e)
        {
            foreach (var item in ugridRepository.Rows)
            {
                item.Cells["Check"].Value = false;
            }
            this.Close();
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            if(radInA3.Checked==true)
            {
                string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\TongHopTonKho.rst", path);
            }
            else if (radInA4.Checked ==true)
            {
                string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\TongHopTonKhoA4.rst", path);
            }
            else
            {
                MSG.Warning("Chọn mẫu in báo cáo trước !");
            }
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }
            
            if (dtBeginDate.DateTime.Date > dtEndDate.DateTime.Date)
            {
                //MessageBox.Show("Ngày bắt đầu không thể lớn hơn ngày kết thúc", "Cảnh báo");
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }
            if (_lstRepositoryReport == null || !_lstRepositoryReport.Any(x => x.Check))
            {
                MessageBox.Show("Chưa chọn kho để xem", "Cảnh báo");
                return;
            }
            var rD = new Detail_Period
            {
                Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value)
            };
            List<TongHopTonKho> data1 = new List<TongHopTonKho>();
            var lstRepos = Utils.ListRepository.Where(c => (_lstRepositoryReport.Where(a => a.Check).ToList()).Any(d => d.ID == c.ID)).ToList();
            if (_check1) data1 = _IMaterialGoodsService.getMaterialGood(dtBeginDate.DateTime, dtEndDate.DateTime, lstRepos, (MaterialGoodsCategory)cbbMaterialGoodsCategory1.SelectedRow.ListObject);
            else data1 = _IMaterialGoodsService.getMaterialGood(dtBeginDate.DateTime, dtEndDate.DateTime, lstRepos);
            var f = new ReportForm<TongHopTonKho>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("TongHopTonKho", data1, true);
            f.AddDatasource("Detail", rD);
            f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
            {
                foreach (var item in ugridRepository.Rows)
                {
                    item.Cells["Check"].Value = false;
                }
            }
        }

        private void btnOk_Click_1(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if (dtBeginDate.DateTime.Date > dtEndDate.DateTime.Date)
            {
                //MessageBox.Show("Ngày bắt đầu không thể lớn hơn ngày kết thúc", "Cảnh báo");
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            if (_lstRepositoryReport == null || !_lstRepositoryReport.Any(x => x.Check))
            {
                MessageBox.Show("Chưa chọn kho để xem", "Cảnh báo");
                return;
            }
            var rD = new Detail_Period
            {
                Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value)
            };
            List<TongHopTonKho> data1 = new List<TongHopTonKho>();
            var lstRepos = Utils.ListRepository.Where(c => (_lstRepositoryReport.Where(a => a.Check).ToList()).Any(d => d.ID == c.ID)).ToList();
            if (_check1) data1 = _IMaterialGoodsService.getMaterialGood(dtBeginDate.DateTime, dtEndDate.DateTime, lstRepos, (MaterialGoodsCategory)cbbMaterialGoodsCategory1.SelectedRow.ListObject);
            else data1 = _IMaterialGoodsService.getMaterialGood(dtBeginDate.DateTime, dtEndDate.DateTime, lstRepos);
            var f = new FRTongHopTonKhoTruocIn(data1, rD.Period, _subSystemCode);
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }
    }
}
