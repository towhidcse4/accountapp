﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Accounting.Core.Domain;

namespace Accounting
{
    public partial class FRS08DNN : CustormForm
    {
        private List<MaterialGoodsReport> _lstMaterialGoodsReport = new List<MaterialGoodsReport>();
        private readonly List<BankAccountDetail> _lstBankAccountDetail = ReportUtils.LstBankAccountDetailDb;
        string _subSystemCode;
        public FRS08DNN(string subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\S08-DNN.rst", path);
            ReportUtils.LstAccountBankReport.Clear();
            LoadData();
            ReportUtils.ProcessControls(this);
            btnExit.Click += btnExit_Click;
            btnOk.Click += btnOk_Click;
            WaitingFrm.StopWaiting();
        }

        private void LoadData()
        {
            _lstMaterialGoodsReport = Utils.IRepositoryLedgerService.getMaterialGoodsInLedger(dtBeginDate.DateTime, dtEndDate.DateTime, (Guid?)cbbRepository.Value, (Guid?)cbbMaterialCategory.Value);
            ugridAccount.SetDataBinding(_lstMaterialGoodsReport.ToList(), "");
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            //var generalLedgerService = IoC.Resolve<IGeneralLedgerService>();
            //List<string> lstAccountNumber = _lstAccountBankReport.Where(c => c.Check).Select(c => c.AccountNumber).ToList();
            //if (lstAccountNumber.Count == 0)
            //{
            //    MSG.Warning(resSystem.Report_01);
            //    return;
            //}
            //List<Guid> lstIdBankAccountDetail = new List<Guid>();
            //if (cbbMaterialCategory.SelectedRow == cbbMaterialCategory.Rows[0])
            //{
            //    if (_lstBankAccountDetail.Count > 0)
            //    {
            //        foreach (var bankAccountDetail in _lstBankAccountDetail)
            //        {
            //            lstIdBankAccountDetail.Add(bankAccountDetail.ID);
            //        }
            //    }

            //}
            //else
            //{
            //    lstIdBankAccountDetail.Add(_lstBankAccountDetail.Where(p => p.BankName == cbbMaterialCategory.Text).Select(c => c.ID).SingleOrDefault());
            //}
            //if (lstIdBankAccountDetail.Count > 0)
            //{
            //    #region Code test Report
            //    //List<S06DNN> data = new List<S06DNN>();
            //    //for (int i = 0; i < 2; i++)
            //    //{
            //    //    data.Add(new S06DNN(i));
            //    //}
            //    #endregion
            //    List<S08DNN> lstS08Dns = new List<S08DNN>();
            //    foreach (var guid in lstIdBankAccountDetail)
            //    {
            //        //lstS06Dns = generalLedgerService.ReportS06DNN(lstAccountNumber, (DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, ((Currency)cbbCurrency.SelectedRow.ListObject).ID, guid, chk.Checked).ToList();
            //    }

            //    var rD = new S08DNNDetail();


            //    rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
            //    var f = new ReportForm<S08DNN>(fileReportSlot1, _subSystemCode);
            //    f.AddDatasource("S08DNN", lstS08Dns, true);
            //    f.AddDatasource("Detail", rD);
            //    //f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
            //    f.LoadReport();
            //    f.WindowState = FormWindowState.Maximized;
            //    f.Show();
            //}

        }
    }
}
