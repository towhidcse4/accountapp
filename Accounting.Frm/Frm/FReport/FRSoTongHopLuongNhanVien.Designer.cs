﻿namespace Accounting.Frm.FReport
{
    partial class FRSoTongHopLuongNhanVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRSoTongHopLuongNhanVien));
            this.ugridEmployee = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.lblBeginDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblEndDate = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDateTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dtEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtBeginDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.reportManager1 = new PerpetuumSoft.Reporting.Components.ReportManager(this.components);
            this.fileReportSlot1 = new PerpetuumSoft.Reporting.Components.FileReportSlot(this.components);
            this.btnExit = new Infragistics.Win.Misc.UltraButton();
            this.btnOK = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.ugridEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // ugridEmployee
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ugridEmployee.DisplayLayout.Appearance = appearance1;
            this.ugridEmployee.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ugridEmployee.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ugridEmployee.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ugridEmployee.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ugridEmployee.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ugridEmployee.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ugridEmployee.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ugridEmployee.DisplayLayout.MaxColScrollRegions = 1;
            this.ugridEmployee.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ugridEmployee.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ugridEmployee.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ugridEmployee.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ugridEmployee.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ugridEmployee.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ugridEmployee.DisplayLayout.Override.CellAppearance = appearance8;
            this.ugridEmployee.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ugridEmployee.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ugridEmployee.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ugridEmployee.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ugridEmployee.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ugridEmployee.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ugridEmployee.DisplayLayout.Override.RowAppearance = appearance11;
            this.ugridEmployee.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.SeparateElement;
            this.ugridEmployee.DisplayLayout.Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.Fixed;
            this.ugridEmployee.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.ExtendedAutoDrag;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ugridEmployee.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ugridEmployee.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ugridEmployee.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ugridEmployee.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.ugridEmployee.Location = new System.Drawing.Point(0, 126);
            this.ugridEmployee.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ugridEmployee.Name = "ugridEmployee";
            this.ugridEmployee.Size = new System.Drawing.Size(1067, 385);
            this.ugridEmployee.TabIndex = 22;
            this.ugridEmployee.TabStop = false;
            this.ugridEmployee.Text = "Danh sách nhóm hàng hoá";
            // 
            // ultraGroupBox1
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox1.Appearance = appearance13;
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Controls.Add(this.lblBeginDate);
            this.ultraGroupBox1.Controls.Add(this.lblEndDate);
            this.ultraGroupBox1.Controls.Add(this.cbbDateTime);
            this.ultraGroupBox1.Controls.Add(this.dtEndDate);
            this.ultraGroupBox1.Controls.Add(this.dtBeginDate);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1067, 126);
            this.ultraGroupBox1.TabIndex = 1;
            this.ultraGroupBox1.Text = "Tham số";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel1
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextHAlignAsString = "Left";
            appearance14.TextVAlignAsString = "Bottom";
            this.ultraLabel1.Appearance = appearance14;
            this.ultraLabel1.Location = new System.Drawing.Point(23, 33);
            this.ultraLabel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(91, 21);
            this.ultraLabel1.TabIndex = 61;
            this.ultraLabel1.Text = "Kỳ báo cáo";
            // 
            // lblBeginDate
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextHAlignAsString = "Left";
            appearance15.TextVAlignAsString = "Bottom";
            this.lblBeginDate.Appearance = appearance15;
            this.lblBeginDate.Location = new System.Drawing.Point(389, 33);
            this.lblBeginDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.Size = new System.Drawing.Size(65, 21);
            this.lblBeginDate.TabIndex = 43;
            this.lblBeginDate.Text = "Từ ngày";
            // 
            // lblEndDate
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.TextHAlignAsString = "Left";
            appearance16.TextVAlignAsString = "Bottom";
            this.lblEndDate.Appearance = appearance16;
            this.lblEndDate.Location = new System.Drawing.Point(585, 33);
            this.lblEndDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(76, 21);
            this.lblEndDate.TabIndex = 44;
            this.lblEndDate.Text = "Đến ngày";
            // 
            // cbbDateTime
            // 
            this.cbbDateTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDateTime.Location = new System.Drawing.Point(23, 66);
            this.cbbDateTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbbDateTime.Name = "cbbDateTime";
            this.cbbDateTime.NullText = "<chọn dữ liệu>";
            this.cbbDateTime.Size = new System.Drawing.Size(300, 25);
            this.cbbDateTime.TabIndex = 19;
            // 
            // dtEndDate
            // 
            appearance17.TextHAlignAsString = "Center";
            appearance17.TextVAlignAsString = "Middle";
            this.dtEndDate.Appearance = appearance17;
            this.dtEndDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtEndDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dtEndDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtEndDate.Location = new System.Drawing.Point(557, 68);
            this.dtEndDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtEndDate.MaskInput = "";
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.Size = new System.Drawing.Size(157, 24);
            this.dtEndDate.TabIndex = 21;
            this.dtEndDate.Value = null;
            // 
            // dtBeginDate
            // 
            appearance18.TextHAlignAsString = "Center";
            appearance18.TextVAlignAsString = "Middle";
            this.dtBeginDate.Appearance = appearance18;
            this.dtBeginDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtBeginDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dtBeginDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtBeginDate.Location = new System.Drawing.Point(357, 68);
            this.dtBeginDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtBeginDate.MaskInput = "";
            this.dtBeginDate.Name = "dtBeginDate";
            this.dtBeginDate.Size = new System.Drawing.Size(157, 24);
            this.dtBeginDate.TabIndex = 20;
            this.dtBeginDate.Value = null;
            // 
            // reportManager1
            // 
            this.reportManager1.DataSources = new PerpetuumSoft.Reporting.Components.ObjectPointerCollection(new string[0], new object[0]);
            this.reportManager1.OwnerForm = this;
            this.reportManager1.Reports.AddRange(new PerpetuumSoft.Reporting.Components.ReportSlot[] {
            this.fileReportSlot1});
            // 
            // fileReportSlot1
            // 
            this.fileReportSlot1.FilePath = "D:\\EasyBook\\ea_master\\04_Code\\Accounting_V3\\Accounting.Frm\\Frm\\FReport\\Template\\S" +
    "oChiTietTheoDoiTuongTHCP.rst";
            this.fileReportSlot1.ReportName = "";
            this.fileReportSlot1.ReportScriptType = typeof(PerpetuumSoft.Reporting.Rendering.ReportScriptBase);
            // 
            // btnExit
            // 
            appearance19.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnExit.Appearance = appearance19;
            this.btnExit.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaToolbarButton;
            this.btnExit.Location = new System.Drawing.Point(951, 9);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(100, 28);
            this.btnExit.TabIndex = 25;
            this.btnExit.Text = "Hủy bỏ";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnOK
            // 
            appearance20.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnOK.Appearance = appearance20;
            this.btnOK.Location = new System.Drawing.Point(843, 9);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 28);
            this.btnOK.TabIndex = 24;
            this.btnOK.Text = "Đồng ý";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.btnExit);
            this.ultraGroupBox3.Controls.Add(this.btnOK);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 509);
            this.ultraGroupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(1067, 45);
            this.ultraGroupBox3.TabIndex = 23;
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // FRSoTongHopLuongNhanVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.ugridEmployee);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.ultraGroupBox3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FRSoTongHopLuongNhanVien";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sổ tổng hợp lương nhân viên";
            ((System.ComponentModel.ISupportInitialize)(this.ugridEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid ugridEmployee;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel lblBeginDate;
        private Infragistics.Win.Misc.UltraLabel lblEndDate;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDateTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtEndDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtBeginDate;
        private PerpetuumSoft.Reporting.Components.ReportManager reportManager1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraButton btnExit;
        private Infragistics.Win.Misc.UltraButton btnOK;
        private PerpetuumSoft.Reporting.Components.FileReportSlot fileReportSlot1;
    }
}