﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FRReceiptableSummary : CustormForm
    {
        readonly List<AccountReport> _lstAccountPayReport = ReportUtils.LstAccountPayReport;
        private readonly List<AccountingObjectReport> _lstnew = ReportUtils.LstCustomers;
        List<AccountingObjectReport> _lstAccountingObjectReport = new List<AccountingObjectReport>();
        string _subSystemCode;
        public FRReceiptableSummary(string subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\ReceiptableSummary.rst", path);
            _lstAccountingObjectReport = _lstnew;
            ugridAccount.SetDataBinding(_lstAccountPayReport.ToList(), "");
            ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
            ReportUtils.ProcessControls(this);
            btnExit.Click += btnExit_Click;
            btnOk.Click += btnOk_Click;
            cbbAccountingObjectCategoryID.ValueChanged += cbbAccountingObjectCategoryID_ValueChanged;
            cbbAccountingObjectGroupID.ValueChanged += cbbAccountingObjectGroupID_ValueChanged;
            WaitingFrm.StopWaiting();
        }
        #region lọc nhóm nhà cung cấp
        private bool _check1 = false;
        private bool _check2 = false;
        private void cbbAccountingObjectGroupID_ValueChanged(object sender, EventArgs e)
        {
            _check1 = ((AccountingObjectGroup)cbbAccountingObjectGroupID.SelectedRow.ListObject).AccountingObjectGroupCode != "TC";
            Filter();
        }

        private void cbbAccountingObjectCategoryID_ValueChanged(object sender, EventArgs e)
        {
            _check2 = ((AccountingObjectCategory)cbbAccountingObjectCategoryID.SelectedRow.ListObject).AccountingObjectCategoryCode != "TC";
            Filter();
        }

        private void Filter()
        {
            _lstAccountingObjectReport = _lstnew;
            AccountingObjectCategory accountingObjectCategory;
            AccountingObjectGroup accountingObjectGroup;
            if (_check1 && !_check2)
            {
                accountingObjectGroup = (AccountingObjectGroup)cbbAccountingObjectGroupID.SelectedRow.ListObject;
                _lstAccountingObjectReport = _lstAccountingObjectReport.Where(c => c.AccountObjectGroupID == accountingObjectGroup.ID).ToList();
            }
            else if (_check2 && !_check1)
            {
                accountingObjectCategory = (AccountingObjectCategory)cbbAccountingObjectCategoryID.SelectedRow.ListObject;
                _lstAccountingObjectReport = _lstAccountingObjectReport.Where(c => c.AccountingObjectCategory == accountingObjectCategory.ID.ToString()).ToList();
            }
            else if (_check1 && _check2)
            {
                accountingObjectGroup = (AccountingObjectGroup)cbbAccountingObjectGroupID.SelectedRow.ListObject;
                accountingObjectCategory = (AccountingObjectCategory)cbbAccountingObjectCategoryID.SelectedRow.ListObject;
                _lstAccountingObjectReport = _lstAccountingObjectReport.Where(c => c.AccountingObjectCategory == accountingObjectCategory.ID.ToString() && c.AccountObjectGroupID == accountingObjectGroup.ID).ToList();
            }
            ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
        }
        #endregion
        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }
            if (!_lstAccountPayReport.Any(c => c.Check))
            {
                MSG.Warning(resSystem.Report_01);
                return;
            }
            if (!_lstAccountingObjectReport.Any(c => c.Check))
            {
                MSG.Warning(resSystem.Report_04);
                return;
            }
            var iGeneralLedgerService = IoC.Resolve<IGeneralLedgerService>();
            List<string> lstGuids = _lstAccountPayReport.Where(c => c.Check).Select(c => c.AccountNumber).ToList();
            var idCurentcy = ((Currency)cbbCurrency.SelectedRow.ListObject).ID;
            var dsacc = _lstAccountingObjectReport.Where(c => c.Check).Select(c => c.ID).ToList();
            List<PayableReceiptableSummary> data = iGeneralLedgerService.ReportsPayableReceiptableSummary((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, idCurentcy,
                lstGuids, dsacc);
            for (int i = 0; i < 300; i++)
                data.Add(new PayableReceiptableSummary(i));

            var rD = new PayableReceiptableSummaryDetail();
            rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
            var f = new ReportForm<PayableReceiptableSummary>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("ReceiptableSummary", data, true);
            f.AddDatasource("Detail", rD);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }
    }
}
