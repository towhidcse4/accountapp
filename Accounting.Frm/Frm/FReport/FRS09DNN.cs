﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Accounting.Core.Domain;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FRS09DNN : CustormForm
    {
        readonly List<MaterialGoodsReport> _lstnew = ReportUtils.LstMaterialGoodsReport;
        private List<MaterialGoodsReport> _lstMaterialGoodsReports = new List<MaterialGoodsReport>();
        string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRS09DNN(string subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\S09-DNN.rst", path);
            Text = "Thẻ kho";
            foreach(var item in _lstnew)
            {
                if (Utils.ListRepositoryLedger.Any(x => x.MaterialGoodsID == item.ID))
                    _lstMaterialGoodsReports.Add(item);
            }
            //_lstMaterialGoodsReports = _lstnew;
            ugridMaterialGoodsCategory.SetDataBinding(_lstMaterialGoodsReports.OrderBy(n => n.MaterialGoodsCode).ToList(), "");
            ReportUtils.ProcessControls(this);
            btnExit.Click += btnExit_Click;
            btnOk.Click += btnOk_Click;
            cbbMaterialGoodsCategory1.ValueChanged += cbbMaterialGoodsCategory1_ValueChanged;
            cbbRepository.ValueChanged += cbbRepository_ValueChanged;
            WaitingFrm.StopWaiting();
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }
        #region lọc nhóm nhà cung cấp
        private bool _check1 = false;
        private bool _check2 = false;
        private void cbbMaterialGoodsCategory1_ValueChanged(object sender, EventArgs e)
        {
            if (cbbMaterialGoodsCategory1.Text != null && !string.IsNullOrEmpty(cbbMaterialGoodsCategory1.Text))
            {
                _check1 = ((MaterialGoodsCategory)cbbMaterialGoodsCategory1.SelectedRow.ListObject).MaterialGoodsCategoryCode != "TC";
                Filter();
            }
        }

        private void cbbRepository_ValueChanged(object sender, EventArgs e)
        {
            if (cbbRepository.Text != null && !string.IsNullOrEmpty(cbbRepository.Text))
            {
                _check2 = ((Repository)cbbRepository.SelectedRow.ListObject).RepositoryCode != "TC";
                Filter();
            }
        }

        private void Filter()
        {
            _lstMaterialGoodsReports = _lstnew;
            MaterialGoodsCategory materialGoodsCategory;
            Repository repository;
            List<Guid> lstID;
            if (_check1 && !_check2)
            {
                materialGoodsCategory = (MaterialGoodsCategory)cbbMaterialGoodsCategory1.SelectedRow.ListObject;
                _lstMaterialGoodsReports = _lstMaterialGoodsReports.Where(c => c.MaterialGoodsCategoryID == materialGoodsCategory.ID).ToList();
            }
            else if (_check2 && !_check1)
            {
                repository = (Repository)cbbRepository.SelectedRow.ListObject;
                lstID = Utils.ListRepositoryLedger.Where(x => x.RepositoryID == repository.ID).Select(x => x.MaterialGoodsID).ToList();
                _lstMaterialGoodsReports = _lstMaterialGoodsReports.Where(c => lstID.Any(d => d == c.ID)).ToList();
            }
            else if (_check1 && _check2)
            {
                repository = (Repository)cbbRepository.SelectedRow.ListObject;
                lstID = Utils.ListRepositoryLedger.Where(x => x.RepositoryID == repository.ID).Select(x => x.MaterialGoodsID).ToList();
                materialGoodsCategory = (MaterialGoodsCategory)cbbMaterialGoodsCategory1.SelectedRow.ListObject;
                _lstMaterialGoodsReports = _lstMaterialGoodsReports.Where(c => c.MaterialGoodsCategoryID == materialGoodsCategory.ID && lstID.Any(d => d == c.ID)).ToList();
            }
            ugridMaterialGoodsCategory.SetDataBinding(_lstMaterialGoodsReports.ToList(), "");
        }
        #endregion
        private void btnExit_Click(object sender, EventArgs e)
        {
            foreach (var item in ugridMaterialGoodsCategory.Rows)
            {
                item.Cells["Check"].Value = false;
            }
            this.Close();
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }
            if (dtBeginDate.DateTime.Date > dtEndDate.DateTime.Date)
            {
                //MessageBox.Show("Ngày bắt đầu không thể lớn hơn ngày kết thúc", "Cảnh báo");
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            Repository repository = new Repository();
            if (_check2) repository = (Repository)cbbRepository.SelectedRow.ListObject;
            var repositoryLedgerService = IoC.Resolve<IRepositoryLedgerService>();
            List<S09DNN> data1 = repositoryLedgerService.ReportS09DNN((DateTime)dtBeginDate.Value,
                (DateTime)dtEndDate.Value, _lstMaterialGoodsReports.Where(c => c.Check).Select(c => c.ID).ToList(), repository.ID);
            var rD = new S09DNNDetail
            {
                Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value)
            };
            var f = new ReportForm<S09DNN>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("S09DNN", data1, true);
            f.AddDatasource("Detail", rD);
            f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
            {
                foreach (var item in ugridMaterialGoodsCategory.Rows)
                {
                    item.Cells["Check"].Value = false;
                }
            }
        }
    }
}
